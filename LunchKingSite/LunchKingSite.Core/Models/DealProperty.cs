using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the DealProperty class.
	/// </summary>
    [Serializable]
	public partial class DealPropertyCollection : RepositoryList<DealProperty, DealPropertyCollection>
	{	   
		public DealPropertyCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>DealPropertyCollection</returns>
		public DealPropertyCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                DealProperty o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the deal_property table.
	/// </summary>
	[Serializable]
	public partial class DealProperty : RepositoryRecord<DealProperty>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public DealProperty()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public DealProperty(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("deal_property", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarBusinessHourGuid = new TableSchema.TableColumn(schema);
				colvarBusinessHourGuid.ColumnName = "business_hour_guid";
				colvarBusinessHourGuid.DataType = DbType.Guid;
				colvarBusinessHourGuid.MaxLength = 0;
				colvarBusinessHourGuid.AutoIncrement = false;
				colvarBusinessHourGuid.IsNullable = false;
				colvarBusinessHourGuid.IsPrimaryKey = true;
                colvarBusinessHourGuid.IsForeignKey = false;
                colvarBusinessHourGuid.IsReadOnly = false;
                colvarBusinessHourGuid.DefaultSetting = @"";

                colvarBusinessHourGuid.ForeignKeyTableName = "";
                schema.Columns.Add(colvarBusinessHourGuid);

                TableSchema.TableColumn colvarDealType = new TableSchema.TableColumn(schema);
				colvarDealType.ColumnName = "deal_type";
				colvarDealType.DataType = DbType.Int32;
				colvarDealType.MaxLength = 0;
				colvarDealType.AutoIncrement = false;
				colvarDealType.IsNullable = true;
				colvarDealType.IsPrimaryKey = false;
				colvarDealType.IsForeignKey = false;
				colvarDealType.IsReadOnly = false;
				colvarDealType.DefaultSetting = @"";
				colvarDealType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDealType);
				
				TableSchema.TableColumn colvarSellerVerifyType = new TableSchema.TableColumn(schema);
				colvarSellerVerifyType.ColumnName = "seller_verify_type";
				colvarSellerVerifyType.DataType = DbType.Int32;
				colvarSellerVerifyType.MaxLength = 0;
				colvarSellerVerifyType.AutoIncrement = false;
				colvarSellerVerifyType.IsNullable = true;
				colvarSellerVerifyType.IsPrimaryKey = false;
				colvarSellerVerifyType.IsForeignKey = false;
				colvarSellerVerifyType.IsReadOnly = false;
				colvarSellerVerifyType.DefaultSetting = @"";
				colvarSellerVerifyType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSellerVerifyType);
				
				TableSchema.TableColumn colvarDealAccBusinessGroupId = new TableSchema.TableColumn(schema);
				colvarDealAccBusinessGroupId.ColumnName = "deal_acc_business_group_id";
				colvarDealAccBusinessGroupId.DataType = DbType.Int32;
				colvarDealAccBusinessGroupId.MaxLength = 0;
				colvarDealAccBusinessGroupId.AutoIncrement = false;
				colvarDealAccBusinessGroupId.IsNullable = true;
				colvarDealAccBusinessGroupId.IsPrimaryKey = false;
                colvarDealAccBusinessGroupId.IsForeignKey = false;
                colvarDealAccBusinessGroupId.IsReadOnly = false;
                colvarDealAccBusinessGroupId.DefaultSetting = @"";

                colvarDealAccBusinessGroupId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarDealAccBusinessGroupId);
				
				TableSchema.TableColumn colvarDealEmpName = new TableSchema.TableColumn(schema);
				colvarDealEmpName.ColumnName = "deal_emp_name";
				colvarDealEmpName.DataType = DbType.String;
				colvarDealEmpName.MaxLength = 50;
				colvarDealEmpName.AutoIncrement = false;
				colvarDealEmpName.IsNullable = true;
				colvarDealEmpName.IsPrimaryKey = false;
				colvarDealEmpName.IsForeignKey = false;
				colvarDealEmpName.IsReadOnly = false;
				colvarDealEmpName.DefaultSetting = @"";
				colvarDealEmpName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDealEmpName);
				
				TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
				colvarCreateId.ColumnName = "create_id";
				colvarCreateId.DataType = DbType.String;
				colvarCreateId.MaxLength = 255;
				colvarCreateId.AutoIncrement = false;
				colvarCreateId.IsNullable = true;
				colvarCreateId.IsPrimaryKey = false;
				colvarCreateId.IsForeignKey = false;
				colvarCreateId.IsReadOnly = false;
				colvarCreateId.DefaultSetting = @"";
				colvarCreateId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateId);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = true;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarModifyId = new TableSchema.TableColumn(schema);
				colvarModifyId.ColumnName = "modify_id";
				colvarModifyId.DataType = DbType.String;
				colvarModifyId.MaxLength = 255;
				colvarModifyId.AutoIncrement = false;
				colvarModifyId.IsNullable = true;
				colvarModifyId.IsPrimaryKey = false;
				colvarModifyId.IsForeignKey = false;
				colvarModifyId.IsReadOnly = false;
				colvarModifyId.DefaultSetting = @"";
				colvarModifyId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyId);
				
				TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
				colvarModifyTime.ColumnName = "modify_time";
				colvarModifyTime.DataType = DbType.DateTime;
				colvarModifyTime.MaxLength = 0;
				colvarModifyTime.AutoIncrement = false;
				colvarModifyTime.IsNullable = true;
				colvarModifyTime.IsPrimaryKey = false;
				colvarModifyTime.IsForeignKey = false;
				colvarModifyTime.IsReadOnly = false;
				colvarModifyTime.DefaultSetting = @"";
				colvarModifyTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyTime);
				
				TableSchema.TableColumn colvarUniqueId = new TableSchema.TableColumn(schema);
				colvarUniqueId.ColumnName = "unique_id";
				colvarUniqueId.DataType = DbType.Int32;
				colvarUniqueId.MaxLength = 0;
				colvarUniqueId.AutoIncrement = true;
				colvarUniqueId.IsNullable = false;
				colvarUniqueId.IsPrimaryKey = false;
				colvarUniqueId.IsForeignKey = false;
				colvarUniqueId.IsReadOnly = false;
				colvarUniqueId.DefaultSetting = @"";
				colvarUniqueId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUniqueId);
				
				TableSchema.TableColumn colvarCityList = new TableSchema.TableColumn(schema);
				colvarCityList.ColumnName = "city_list";
				colvarCityList.DataType = DbType.AnsiString;
				colvarCityList.MaxLength = 255;
				colvarCityList.AutoIncrement = false;
				colvarCityList.IsNullable = true;
				colvarCityList.IsPrimaryKey = false;
				colvarCityList.IsForeignKey = false;
				colvarCityList.IsReadOnly = false;
				colvarCityList.DefaultSetting = @"";
				colvarCityList.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCityList);
				
				TableSchema.TableColumn colvarDeliveryType = new TableSchema.TableColumn(schema);
				colvarDeliveryType.ColumnName = "delivery_type";
				colvarDeliveryType.DataType = DbType.Int32;
				colvarDeliveryType.MaxLength = 0;
				colvarDeliveryType.AutoIncrement = false;
				colvarDeliveryType.IsNullable = true;
				colvarDeliveryType.IsPrimaryKey = false;
				colvarDeliveryType.IsForeignKey = false;
				colvarDeliveryType.IsReadOnly = false;
				colvarDeliveryType.DefaultSetting = @"";
				colvarDeliveryType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDeliveryType);
				
				TableSchema.TableColumn colvarShoppingCart = new TableSchema.TableColumn(schema);
				colvarShoppingCart.ColumnName = "shopping_cart";
				colvarShoppingCart.DataType = DbType.Boolean;
				colvarShoppingCart.MaxLength = 0;
				colvarShoppingCart.AutoIncrement = false;
				colvarShoppingCart.IsNullable = false;
				colvarShoppingCart.IsPrimaryKey = false;
				colvarShoppingCart.IsForeignKey = false;
				colvarShoppingCart.IsReadOnly = false;
				
						colvarShoppingCart.DefaultSetting = @"((0))";
				colvarShoppingCart.ForeignKeyTableName = "";
				schema.Columns.Add(colvarShoppingCart);
				
				TableSchema.TableColumn colvarComboPackCount = new TableSchema.TableColumn(schema);
				colvarComboPackCount.ColumnName = "combo_pack_count";
				colvarComboPackCount.DataType = DbType.Int32;
				colvarComboPackCount.MaxLength = 0;
				colvarComboPackCount.AutoIncrement = false;
				colvarComboPackCount.IsNullable = false;
				colvarComboPackCount.IsPrimaryKey = false;
				colvarComboPackCount.IsForeignKey = false;
				colvarComboPackCount.IsReadOnly = false;
				
						colvarComboPackCount.DefaultSetting = @"((1))";
				colvarComboPackCount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarComboPackCount);
				
				TableSchema.TableColumn colvarAncestorBusinessHourGuid = new TableSchema.TableColumn(schema);
				colvarAncestorBusinessHourGuid.ColumnName = "ancestor_business_hour_guid";
				colvarAncestorBusinessHourGuid.DataType = DbType.Guid;
				colvarAncestorBusinessHourGuid.MaxLength = 0;
				colvarAncestorBusinessHourGuid.AutoIncrement = false;
				colvarAncestorBusinessHourGuid.IsNullable = true;
				colvarAncestorBusinessHourGuid.IsPrimaryKey = false;
				colvarAncestorBusinessHourGuid.IsForeignKey = false;
				colvarAncestorBusinessHourGuid.IsReadOnly = false;
				colvarAncestorBusinessHourGuid.DefaultSetting = @"";
				colvarAncestorBusinessHourGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAncestorBusinessHourGuid);
				
				TableSchema.TableColumn colvarIsTravelcard = new TableSchema.TableColumn(schema);
				colvarIsTravelcard.ColumnName = "is_travelcard";
				colvarIsTravelcard.DataType = DbType.Boolean;
				colvarIsTravelcard.MaxLength = 0;
				colvarIsTravelcard.AutoIncrement = false;
				colvarIsTravelcard.IsNullable = false;
				colvarIsTravelcard.IsPrimaryKey = false;
				colvarIsTravelcard.IsForeignKey = false;
				colvarIsTravelcard.IsReadOnly = false;
				
						colvarIsTravelcard.DefaultSetting = @"((0))";
				colvarIsTravelcard.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsTravelcard);
				
				TableSchema.TableColumn colvarEntrustSell = new TableSchema.TableColumn(schema);
				colvarEntrustSell.ColumnName = "entrust_sell";
				colvarEntrustSell.DataType = DbType.Int32;
				colvarEntrustSell.MaxLength = 0;
				colvarEntrustSell.AutoIncrement = false;
				colvarEntrustSell.IsNullable = false;
				colvarEntrustSell.IsPrimaryKey = false;
				colvarEntrustSell.IsForeignKey = false;
				colvarEntrustSell.IsReadOnly = false;
				
						colvarEntrustSell.DefaultSetting = @"((0))";
				colvarEntrustSell.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEntrustSell);
				
				TableSchema.TableColumn colvarTravelPlace = new TableSchema.TableColumn(schema);
				colvarTravelPlace.ColumnName = "travel_place";
				colvarTravelPlace.DataType = DbType.String;
				colvarTravelPlace.MaxLength = 100;
				colvarTravelPlace.AutoIncrement = false;
				colvarTravelPlace.IsNullable = true;
				colvarTravelPlace.IsPrimaryKey = false;
				colvarTravelPlace.IsForeignKey = false;
				colvarTravelPlace.IsReadOnly = false;
				colvarTravelPlace.DefaultSetting = @"";
				colvarTravelPlace.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTravelPlace);
				
				TableSchema.TableColumn colvarMultipleBranch = new TableSchema.TableColumn(schema);
				colvarMultipleBranch.ColumnName = "multiple_branch";
				colvarMultipleBranch.DataType = DbType.Boolean;
				colvarMultipleBranch.MaxLength = 0;
				colvarMultipleBranch.AutoIncrement = false;
				colvarMultipleBranch.IsNullable = false;
				colvarMultipleBranch.IsPrimaryKey = false;
				colvarMultipleBranch.IsForeignKey = false;
				colvarMultipleBranch.IsReadOnly = false;
				
						colvarMultipleBranch.DefaultSetting = @"((0))";
				colvarMultipleBranch.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMultipleBranch);
				
				TableSchema.TableColumn colvarEmpNo = new TableSchema.TableColumn(schema);
				colvarEmpNo.ColumnName = "emp_no";
				colvarEmpNo.DataType = DbType.AnsiString;
				colvarEmpNo.MaxLength = 50;
				colvarEmpNo.AutoIncrement = false;
				colvarEmpNo.IsNullable = true;
				colvarEmpNo.IsPrimaryKey = false;
				colvarEmpNo.IsForeignKey = false;
				colvarEmpNo.IsReadOnly = false;
				colvarEmpNo.DefaultSetting = @"";
				colvarEmpNo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEmpNo);
				
				TableSchema.TableColumn colvarQuantityMultiplier = new TableSchema.TableColumn(schema);
				colvarQuantityMultiplier.ColumnName = "quantity_multiplier";
				colvarQuantityMultiplier.DataType = DbType.Int32;
				colvarQuantityMultiplier.MaxLength = 0;
				colvarQuantityMultiplier.AutoIncrement = false;
				colvarQuantityMultiplier.IsNullable = true;
				colvarQuantityMultiplier.IsPrimaryKey = false;
				colvarQuantityMultiplier.IsForeignKey = false;
				colvarQuantityMultiplier.IsReadOnly = false;
				colvarQuantityMultiplier.DefaultSetting = @"";
				colvarQuantityMultiplier.ForeignKeyTableName = "";
				schema.Columns.Add(colvarQuantityMultiplier);
				
				TableSchema.TableColumn colvarIsDailyRestriction = new TableSchema.TableColumn(schema);
				colvarIsDailyRestriction.ColumnName = "is_daily_restriction";
				colvarIsDailyRestriction.DataType = DbType.Boolean;
				colvarIsDailyRestriction.MaxLength = 0;
				colvarIsDailyRestriction.AutoIncrement = false;
				colvarIsDailyRestriction.IsNullable = false;
				colvarIsDailyRestriction.IsPrimaryKey = false;
				colvarIsDailyRestriction.IsForeignKey = false;
				colvarIsDailyRestriction.IsReadOnly = false;
				
						colvarIsDailyRestriction.DefaultSetting = @"((0))";
				colvarIsDailyRestriction.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsDailyRestriction);
				
				TableSchema.TableColumn colvarIsContinuedSequence = new TableSchema.TableColumn(schema);
				colvarIsContinuedSequence.ColumnName = "is_continued_sequence";
				colvarIsContinuedSequence.DataType = DbType.Boolean;
				colvarIsContinuedSequence.MaxLength = 0;
				colvarIsContinuedSequence.AutoIncrement = false;
				colvarIsContinuedSequence.IsNullable = false;
				colvarIsContinuedSequence.IsPrimaryKey = false;
				colvarIsContinuedSequence.IsForeignKey = false;
				colvarIsContinuedSequence.IsReadOnly = false;
				
						colvarIsContinuedSequence.DefaultSetting = @"((0))";
				colvarIsContinuedSequence.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsContinuedSequence);
				
				TableSchema.TableColumn colvarIsContinuedQuantity = new TableSchema.TableColumn(schema);
				colvarIsContinuedQuantity.ColumnName = "is_continued_quantity";
				colvarIsContinuedQuantity.DataType = DbType.Boolean;
				colvarIsContinuedQuantity.MaxLength = 0;
				colvarIsContinuedQuantity.AutoIncrement = false;
				colvarIsContinuedQuantity.IsNullable = false;
				colvarIsContinuedQuantity.IsPrimaryKey = false;
				colvarIsContinuedQuantity.IsForeignKey = false;
				colvarIsContinuedQuantity.IsReadOnly = false;
				
						colvarIsContinuedQuantity.DefaultSetting = @"((0))";
				colvarIsContinuedQuantity.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsContinuedQuantity);
				
				TableSchema.TableColumn colvarContinuedQuantity = new TableSchema.TableColumn(schema);
				colvarContinuedQuantity.ColumnName = "continued_quantity";
				colvarContinuedQuantity.DataType = DbType.Int32;
				colvarContinuedQuantity.MaxLength = 0;
				colvarContinuedQuantity.AutoIncrement = false;
				colvarContinuedQuantity.IsNullable = false;
				colvarContinuedQuantity.IsPrimaryKey = false;
				colvarContinuedQuantity.IsForeignKey = false;
				colvarContinuedQuantity.IsReadOnly = false;
				
						colvarContinuedQuantity.DefaultSetting = @"((0))";
				colvarContinuedQuantity.ForeignKeyTableName = "";
				schema.Columns.Add(colvarContinuedQuantity);
				
				TableSchema.TableColumn colvarActivityUrl = new TableSchema.TableColumn(schema);
				colvarActivityUrl.ColumnName = "activity_url";
				colvarActivityUrl.DataType = DbType.String;
				colvarActivityUrl.MaxLength = 256;
				colvarActivityUrl.AutoIncrement = false;
				colvarActivityUrl.IsNullable = true;
				colvarActivityUrl.IsPrimaryKey = false;
				colvarActivityUrl.IsForeignKey = false;
				colvarActivityUrl.IsReadOnly = false;
				colvarActivityUrl.DefaultSetting = @"";
				colvarActivityUrl.ForeignKeyTableName = "";
				schema.Columns.Add(colvarActivityUrl);
				
				TableSchema.TableColumn colvarLabelTagList = new TableSchema.TableColumn(schema);
				colvarLabelTagList.ColumnName = "label_tag_list";
				colvarLabelTagList.DataType = DbType.AnsiString;
				colvarLabelTagList.MaxLength = 255;
				colvarLabelTagList.AutoIncrement = false;
				colvarLabelTagList.IsNullable = true;
				colvarLabelTagList.IsPrimaryKey = false;
				colvarLabelTagList.IsForeignKey = false;
				colvarLabelTagList.IsReadOnly = false;
				colvarLabelTagList.DefaultSetting = @"";
				colvarLabelTagList.ForeignKeyTableName = "";
				schema.Columns.Add(colvarLabelTagList);
				
				TableSchema.TableColumn colvarLabelIconList = new TableSchema.TableColumn(schema);
				colvarLabelIconList.ColumnName = "label_icon_list";
				colvarLabelIconList.DataType = DbType.AnsiString;
				colvarLabelIconList.MaxLength = 255;
				colvarLabelIconList.AutoIncrement = false;
				colvarLabelIconList.IsNullable = true;
				colvarLabelIconList.IsPrimaryKey = false;
				colvarLabelIconList.IsForeignKey = false;
				colvarLabelIconList.IsReadOnly = false;
				colvarLabelIconList.DefaultSetting = @"";
				colvarLabelIconList.ForeignKeyTableName = "";
				schema.Columns.Add(colvarLabelIconList);
				
				TableSchema.TableColumn colvarCouponCodeType = new TableSchema.TableColumn(schema);
				colvarCouponCodeType.ColumnName = "coupon_code_type";
				colvarCouponCodeType.DataType = DbType.Int32;
				colvarCouponCodeType.MaxLength = 0;
				colvarCouponCodeType.AutoIncrement = false;
				colvarCouponCodeType.IsNullable = false;
				colvarCouponCodeType.IsPrimaryKey = false;
				colvarCouponCodeType.IsForeignKey = false;
				colvarCouponCodeType.IsReadOnly = false;
				
						colvarCouponCodeType.DefaultSetting = @"((0))";
				colvarCouponCodeType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCouponCodeType);
				
				TableSchema.TableColumn colvarPinType = new TableSchema.TableColumn(schema);
				colvarPinType.ColumnName = "pin_type";
				colvarPinType.DataType = DbType.Int32;
				colvarPinType.MaxLength = 0;
				colvarPinType.AutoIncrement = false;
				colvarPinType.IsNullable = false;
				colvarPinType.IsPrimaryKey = false;
				colvarPinType.IsForeignKey = false;
				colvarPinType.IsReadOnly = false;
				
						colvarPinType.DefaultSetting = @"((0))";
				colvarPinType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPinType);
				
				TableSchema.TableColumn colvarBookingSystemType = new TableSchema.TableColumn(schema);
				colvarBookingSystemType.ColumnName = "booking_system_type";
				colvarBookingSystemType.DataType = DbType.Int32;
				colvarBookingSystemType.MaxLength = 0;
				colvarBookingSystemType.AutoIncrement = false;
				colvarBookingSystemType.IsNullable = false;
				colvarBookingSystemType.IsPrimaryKey = false;
				colvarBookingSystemType.IsForeignKey = false;
				colvarBookingSystemType.IsReadOnly = false;
				
						colvarBookingSystemType.DefaultSetting = @"((0))";
				colvarBookingSystemType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBookingSystemType);
				
				TableSchema.TableColumn colvarAdvanceReservationDays = new TableSchema.TableColumn(schema);
				colvarAdvanceReservationDays.ColumnName = "advance_reservation_days";
				colvarAdvanceReservationDays.DataType = DbType.Int32;
				colvarAdvanceReservationDays.MaxLength = 0;
				colvarAdvanceReservationDays.AutoIncrement = false;
				colvarAdvanceReservationDays.IsNullable = false;
				colvarAdvanceReservationDays.IsPrimaryKey = false;
				colvarAdvanceReservationDays.IsForeignKey = false;
				colvarAdvanceReservationDays.IsReadOnly = false;
				
						colvarAdvanceReservationDays.DefaultSetting = @"((0))";
				colvarAdvanceReservationDays.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAdvanceReservationDays);
				
				TableSchema.TableColumn colvarCouponUsers = new TableSchema.TableColumn(schema);
				colvarCouponUsers.ColumnName = "coupon_users";
				colvarCouponUsers.DataType = DbType.Int32;
				colvarCouponUsers.MaxLength = 0;
				colvarCouponUsers.AutoIncrement = false;
				colvarCouponUsers.IsNullable = false;
				colvarCouponUsers.IsPrimaryKey = false;
				colvarCouponUsers.IsForeignKey = false;
				colvarCouponUsers.IsReadOnly = false;
				
						colvarCouponUsers.DefaultSetting = @"((0))";
				colvarCouponUsers.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCouponUsers);
				
				TableSchema.TableColumn colvarIsReserveLock = new TableSchema.TableColumn(schema);
				colvarIsReserveLock.ColumnName = "is_reserve_lock";
				colvarIsReserveLock.DataType = DbType.Boolean;
				colvarIsReserveLock.MaxLength = 0;
				colvarIsReserveLock.AutoIncrement = false;
				colvarIsReserveLock.IsNullable = false;
				colvarIsReserveLock.IsPrimaryKey = false;
				colvarIsReserveLock.IsForeignKey = false;
				colvarIsReserveLock.IsReadOnly = false;
				
						colvarIsReserveLock.DefaultSetting = @"((0))";
				colvarIsReserveLock.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsReserveLock);
				
				TableSchema.TableColumn colvarCustomTag = new TableSchema.TableColumn(schema);
				colvarCustomTag.ColumnName = "custom_tag";
				colvarCustomTag.DataType = DbType.String;
				colvarCustomTag.MaxLength = 10;
				colvarCustomTag.AutoIncrement = false;
				colvarCustomTag.IsNullable = true;
				colvarCustomTag.IsPrimaryKey = false;
				colvarCustomTag.IsForeignKey = false;
				colvarCustomTag.IsReadOnly = false;
				
						colvarCustomTag.DefaultSetting = @"('')";
				colvarCustomTag.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCustomTag);
				
				TableSchema.TableColumn colvarTmallRmbPrice = new TableSchema.TableColumn(schema);
				colvarTmallRmbPrice.ColumnName = "tmall_rmb_price";
				colvarTmallRmbPrice.DataType = DbType.Decimal;
				colvarTmallRmbPrice.MaxLength = 0;
				colvarTmallRmbPrice.AutoIncrement = false;
				colvarTmallRmbPrice.IsNullable = false;
				colvarTmallRmbPrice.IsPrimaryKey = false;
				colvarTmallRmbPrice.IsForeignKey = false;
				colvarTmallRmbPrice.IsReadOnly = false;
				
						colvarTmallRmbPrice.DefaultSetting = @"((0))";
				colvarTmallRmbPrice.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTmallRmbPrice);
				
				TableSchema.TableColumn colvarTmallRmbExchangeRate = new TableSchema.TableColumn(schema);
				colvarTmallRmbExchangeRate.ColumnName = "tmall_rmb_exchange_rate";
				colvarTmallRmbExchangeRate.DataType = DbType.Decimal;
				colvarTmallRmbExchangeRate.MaxLength = 0;
				colvarTmallRmbExchangeRate.AutoIncrement = false;
				colvarTmallRmbExchangeRate.IsNullable = false;
				colvarTmallRmbExchangeRate.IsPrimaryKey = false;
				colvarTmallRmbExchangeRate.IsForeignKey = false;
				colvarTmallRmbExchangeRate.IsReadOnly = false;
				
						colvarTmallRmbExchangeRate.DefaultSetting = @"((0))";
				colvarTmallRmbExchangeRate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTmallRmbExchangeRate);
				
				TableSchema.TableColumn colvarExchangePrice = new TableSchema.TableColumn(schema);
				colvarExchangePrice.ColumnName = "exchange_price";
				colvarExchangePrice.DataType = DbType.Decimal;
				colvarExchangePrice.MaxLength = 0;
				colvarExchangePrice.AutoIncrement = false;
				colvarExchangePrice.IsNullable = false;
				colvarExchangePrice.IsPrimaryKey = false;
				colvarExchangePrice.IsForeignKey = false;
				colvarExchangePrice.IsReadOnly = false;
				
						colvarExchangePrice.DefaultSetting = @"((0))";
				colvarExchangePrice.ForeignKeyTableName = "";
				schema.Columns.Add(colvarExchangePrice);
				
				TableSchema.TableColumn colvarIsQuantityMultiplier = new TableSchema.TableColumn(schema);
				colvarIsQuantityMultiplier.ColumnName = "is_quantity_multiplier";
				colvarIsQuantityMultiplier.DataType = DbType.Boolean;
				colvarIsQuantityMultiplier.MaxLength = 0;
				colvarIsQuantityMultiplier.AutoIncrement = false;
				colvarIsQuantityMultiplier.IsNullable = false;
				colvarIsQuantityMultiplier.IsPrimaryKey = false;
				colvarIsQuantityMultiplier.IsForeignKey = false;
				colvarIsQuantityMultiplier.IsReadOnly = false;
				
						colvarIsQuantityMultiplier.DefaultSetting = @"((0))";
				colvarIsQuantityMultiplier.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsQuantityMultiplier);
				
				TableSchema.TableColumn colvarIsAveragePrice = new TableSchema.TableColumn(schema);
				colvarIsAveragePrice.ColumnName = "is_average_price";
				colvarIsAveragePrice.DataType = DbType.Boolean;
				colvarIsAveragePrice.MaxLength = 0;
				colvarIsAveragePrice.AutoIncrement = false;
				colvarIsAveragePrice.IsNullable = false;
				colvarIsAveragePrice.IsPrimaryKey = false;
				colvarIsAveragePrice.IsForeignKey = false;
				colvarIsAveragePrice.IsReadOnly = false;
				
						colvarIsAveragePrice.DefaultSetting = @"((0))";
				colvarIsAveragePrice.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsAveragePrice);
				
				TableSchema.TableColumn colvarIsMergeCount = new TableSchema.TableColumn(schema);
				colvarIsMergeCount.ColumnName = "is_merge_count";
				colvarIsMergeCount.DataType = DbType.Boolean;
				colvarIsMergeCount.MaxLength = 0;
				colvarIsMergeCount.AutoIncrement = false;
				colvarIsMergeCount.IsNullable = false;
				colvarIsMergeCount.IsPrimaryKey = false;
				colvarIsMergeCount.IsForeignKey = false;
				colvarIsMergeCount.IsReadOnly = false;
				
						colvarIsMergeCount.DefaultSetting = @"((1))";
				colvarIsMergeCount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsMergeCount);
				
				TableSchema.TableColumn colvarIsZeroActivityShowCoupon = new TableSchema.TableColumn(schema);
				colvarIsZeroActivityShowCoupon.ColumnName = "is_zero_activity_show_coupon";
				colvarIsZeroActivityShowCoupon.DataType = DbType.Boolean;
				colvarIsZeroActivityShowCoupon.MaxLength = 0;
				colvarIsZeroActivityShowCoupon.AutoIncrement = false;
				colvarIsZeroActivityShowCoupon.IsNullable = false;
				colvarIsZeroActivityShowCoupon.IsPrimaryKey = false;
				colvarIsZeroActivityShowCoupon.IsForeignKey = false;
				colvarIsZeroActivityShowCoupon.IsReadOnly = false;
				
						colvarIsZeroActivityShowCoupon.DefaultSetting = @"((0))";
				colvarIsZeroActivityShowCoupon.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsZeroActivityShowCoupon);
				
				TableSchema.TableColumn colvarIsTravelDeal = new TableSchema.TableColumn(schema);
				colvarIsTravelDeal.ColumnName = "is_travel_deal";
				colvarIsTravelDeal.DataType = DbType.Boolean;
				colvarIsTravelDeal.MaxLength = 0;
				colvarIsTravelDeal.AutoIncrement = false;
				colvarIsTravelDeal.IsNullable = false;
				colvarIsTravelDeal.IsPrimaryKey = false;
				colvarIsTravelDeal.IsForeignKey = false;
				colvarIsTravelDeal.IsReadOnly = false;
				colvarIsTravelDeal.DefaultSetting = @"";
				colvarIsTravelDeal.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsTravelDeal);
				
				TableSchema.TableColumn colvarAncestorSequenceBusinessHourGuid = new TableSchema.TableColumn(schema);
				colvarAncestorSequenceBusinessHourGuid.ColumnName = "ancestor_sequence_business_hour_guid";
				colvarAncestorSequenceBusinessHourGuid.DataType = DbType.Guid;
				colvarAncestorSequenceBusinessHourGuid.MaxLength = 0;
				colvarAncestorSequenceBusinessHourGuid.AutoIncrement = false;
				colvarAncestorSequenceBusinessHourGuid.IsNullable = true;
				colvarAncestorSequenceBusinessHourGuid.IsPrimaryKey = false;
				colvarAncestorSequenceBusinessHourGuid.IsForeignKey = false;
				colvarAncestorSequenceBusinessHourGuid.IsReadOnly = false;
				colvarAncestorSequenceBusinessHourGuid.DefaultSetting = @"";
				colvarAncestorSequenceBusinessHourGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAncestorSequenceBusinessHourGuid);
				
				TableSchema.TableColumn colvarIsLongContract = new TableSchema.TableColumn(schema);
				colvarIsLongContract.ColumnName = "is_long_contract";
				colvarIsLongContract.DataType = DbType.Boolean;
				colvarIsLongContract.MaxLength = 0;
				colvarIsLongContract.AutoIncrement = false;
				colvarIsLongContract.IsNullable = false;
				colvarIsLongContract.IsPrimaryKey = false;
				colvarIsLongContract.IsForeignKey = false;
				colvarIsLongContract.IsReadOnly = false;
				
						colvarIsLongContract.DefaultSetting = @"((0))";
				colvarIsLongContract.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsLongContract);
				
				TableSchema.TableColumn colvarInstallment3months = new TableSchema.TableColumn(schema);
				colvarInstallment3months.ColumnName = "installment_3months";
				colvarInstallment3months.DataType = DbType.Boolean;
				colvarInstallment3months.MaxLength = 0;
				colvarInstallment3months.AutoIncrement = false;
				colvarInstallment3months.IsNullable = false;
				colvarInstallment3months.IsPrimaryKey = false;
				colvarInstallment3months.IsForeignKey = false;
				colvarInstallment3months.IsReadOnly = false;
				
						colvarInstallment3months.DefaultSetting = @"((0))";
				colvarInstallment3months.ForeignKeyTableName = "";
				schema.Columns.Add(colvarInstallment3months);
				
				TableSchema.TableColumn colvarInstallment6months = new TableSchema.TableColumn(schema);
				colvarInstallment6months.ColumnName = "installment_6months";
				colvarInstallment6months.DataType = DbType.Boolean;
				colvarInstallment6months.MaxLength = 0;
				colvarInstallment6months.AutoIncrement = false;
				colvarInstallment6months.IsNullable = false;
				colvarInstallment6months.IsPrimaryKey = false;
				colvarInstallment6months.IsForeignKey = false;
				colvarInstallment6months.IsReadOnly = false;
				
						colvarInstallment6months.DefaultSetting = @"((0))";
				colvarInstallment6months.ForeignKeyTableName = "";
				schema.Columns.Add(colvarInstallment6months);
				
				TableSchema.TableColumn colvarInstallment12months = new TableSchema.TableColumn(schema);
				colvarInstallment12months.ColumnName = "installment_12months";
				colvarInstallment12months.DataType = DbType.Boolean;
				colvarInstallment12months.MaxLength = 0;
				colvarInstallment12months.AutoIncrement = false;
				colvarInstallment12months.IsNullable = false;
				colvarInstallment12months.IsPrimaryKey = false;
				colvarInstallment12months.IsForeignKey = false;
				colvarInstallment12months.IsReadOnly = false;
				
						colvarInstallment12months.DefaultSetting = @"((0))";
				colvarInstallment12months.ForeignKeyTableName = "";
				schema.Columns.Add(colvarInstallment12months);
				
				TableSchema.TableColumn colvarDenyInstallment = new TableSchema.TableColumn(schema);
				colvarDenyInstallment.ColumnName = "deny_installment";
				colvarDenyInstallment.DataType = DbType.Boolean;
				colvarDenyInstallment.MaxLength = 0;
				colvarDenyInstallment.AutoIncrement = false;
				colvarDenyInstallment.IsNullable = false;
				colvarDenyInstallment.IsPrimaryKey = false;
				colvarDenyInstallment.IsForeignKey = false;
				colvarDenyInstallment.IsReadOnly = false;
				
						colvarDenyInstallment.DefaultSetting = @"((0))";
				colvarDenyInstallment.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDenyInstallment);
				
				TableSchema.TableColumn colvarSaleMultipleBase = new TableSchema.TableColumn(schema);
				colvarSaleMultipleBase.ColumnName = "sale_multiple_base";
				colvarSaleMultipleBase.DataType = DbType.Int32;
				colvarSaleMultipleBase.MaxLength = 0;
				colvarSaleMultipleBase.AutoIncrement = false;
				colvarSaleMultipleBase.IsNullable = true;
				colvarSaleMultipleBase.IsPrimaryKey = false;
				colvarSaleMultipleBase.IsForeignKey = false;
				colvarSaleMultipleBase.IsReadOnly = false;
				colvarSaleMultipleBase.DefaultSetting = @"";
				colvarSaleMultipleBase.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSaleMultipleBase);
				
				TableSchema.TableColumn colvarShipType = new TableSchema.TableColumn(schema);
				colvarShipType.ColumnName = "ship_type";
				colvarShipType.DataType = DbType.Int32;
				colvarShipType.MaxLength = 0;
				colvarShipType.AutoIncrement = false;
				colvarShipType.IsNullable = true;
				colvarShipType.IsPrimaryKey = false;
				colvarShipType.IsForeignKey = false;
				colvarShipType.IsReadOnly = false;
				
						colvarShipType.DefaultSetting = @"((0))";
				colvarShipType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarShipType);
				
				TableSchema.TableColumn colvarShippingdateType = new TableSchema.TableColumn(schema);
				colvarShippingdateType.ColumnName = "shippingdate_type";
				colvarShippingdateType.DataType = DbType.Int32;
				colvarShippingdateType.MaxLength = 0;
				colvarShippingdateType.AutoIncrement = false;
				colvarShippingdateType.IsNullable = true;
				colvarShippingdateType.IsPrimaryKey = false;
				colvarShippingdateType.IsForeignKey = false;
				colvarShippingdateType.IsReadOnly = false;
				
						colvarShippingdateType.DefaultSetting = @"((0))";
				colvarShippingdateType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarShippingdateType);
				
				TableSchema.TableColumn colvarShippingdate = new TableSchema.TableColumn(schema);
				colvarShippingdate.ColumnName = "shippingdate";
				colvarShippingdate.DataType = DbType.Int32;
				colvarShippingdate.MaxLength = 0;
				colvarShippingdate.AutoIncrement = false;
				colvarShippingdate.IsNullable = true;
				colvarShippingdate.IsPrimaryKey = false;
				colvarShippingdate.IsForeignKey = false;
				colvarShippingdate.IsReadOnly = false;
				colvarShippingdate.DefaultSetting = @"";
				colvarShippingdate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarShippingdate);
				
				TableSchema.TableColumn colvarProductUseDateStartSet = new TableSchema.TableColumn(schema);
				colvarProductUseDateStartSet.ColumnName = "product_use_date_start_set";
				colvarProductUseDateStartSet.DataType = DbType.Int32;
				colvarProductUseDateStartSet.MaxLength = 0;
				colvarProductUseDateStartSet.AutoIncrement = false;
				colvarProductUseDateStartSet.IsNullable = true;
				colvarProductUseDateStartSet.IsPrimaryKey = false;
				colvarProductUseDateStartSet.IsForeignKey = false;
				colvarProductUseDateStartSet.IsReadOnly = false;
				colvarProductUseDateStartSet.DefaultSetting = @"";
				colvarProductUseDateStartSet.ForeignKeyTableName = "";
				schema.Columns.Add(colvarProductUseDateStartSet);
				
				TableSchema.TableColumn colvarProductUseDateEndSet = new TableSchema.TableColumn(schema);
				colvarProductUseDateEndSet.ColumnName = "product_use_date_end_set";
				colvarProductUseDateEndSet.DataType = DbType.Int32;
				colvarProductUseDateEndSet.MaxLength = 0;
				colvarProductUseDateEndSet.AutoIncrement = false;
				colvarProductUseDateEndSet.IsNullable = true;
				colvarProductUseDateEndSet.IsPrimaryKey = false;
				colvarProductUseDateEndSet.IsForeignKey = false;
				colvarProductUseDateEndSet.IsReadOnly = false;
				colvarProductUseDateEndSet.DefaultSetting = @"";
				colvarProductUseDateEndSet.ForeignKeyTableName = "";
				schema.Columns.Add(colvarProductUseDateEndSet);
				
				TableSchema.TableColumn colvarPresentQuantity = new TableSchema.TableColumn(schema);
				colvarPresentQuantity.ColumnName = "present_quantity";
				colvarPresentQuantity.DataType = DbType.Int32;
				colvarPresentQuantity.MaxLength = 0;
				colvarPresentQuantity.AutoIncrement = false;
				colvarPresentQuantity.IsNullable = false;
				colvarPresentQuantity.IsPrimaryKey = false;
				colvarPresentQuantity.IsForeignKey = false;
				colvarPresentQuantity.IsReadOnly = false;
				
						colvarPresentQuantity.DefaultSetting = @"((0))";
				colvarPresentQuantity.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPresentQuantity);
				
				TableSchema.TableColumn colvarDevelopeSalesId = new TableSchema.TableColumn(schema);
				colvarDevelopeSalesId.ColumnName = "develope_sales_id";
				colvarDevelopeSalesId.DataType = DbType.Int32;
				colvarDevelopeSalesId.MaxLength = 0;
				colvarDevelopeSalesId.AutoIncrement = false;
				colvarDevelopeSalesId.IsNullable = false;
				colvarDevelopeSalesId.IsPrimaryKey = false;
				colvarDevelopeSalesId.IsForeignKey = false;
				colvarDevelopeSalesId.IsReadOnly = false;
				
						colvarDevelopeSalesId.DefaultSetting = @"((0))";
				colvarDevelopeSalesId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDevelopeSalesId);
				
				TableSchema.TableColumn colvarGroupCouponAppStyle = new TableSchema.TableColumn(schema);
				colvarGroupCouponAppStyle.ColumnName = "group_coupon_app_style";
				colvarGroupCouponAppStyle.DataType = DbType.Int32;
				colvarGroupCouponAppStyle.MaxLength = 0;
				colvarGroupCouponAppStyle.AutoIncrement = false;
				colvarGroupCouponAppStyle.IsNullable = false;
				colvarGroupCouponAppStyle.IsPrimaryKey = false;
				colvarGroupCouponAppStyle.IsForeignKey = false;
				colvarGroupCouponAppStyle.IsReadOnly = false;
				
						colvarGroupCouponAppStyle.DefaultSetting = @"((0))";
				colvarGroupCouponAppStyle.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGroupCouponAppStyle);
				
				TableSchema.TableColumn colvarNewDealType = new TableSchema.TableColumn(schema);
				colvarNewDealType.ColumnName = "new_deal_type";
				colvarNewDealType.DataType = DbType.Int32;
				colvarNewDealType.MaxLength = 0;
				colvarNewDealType.AutoIncrement = false;
				colvarNewDealType.IsNullable = true;
				colvarNewDealType.IsPrimaryKey = false;
				colvarNewDealType.IsForeignKey = false;
				colvarNewDealType.IsReadOnly = false;
				colvarNewDealType.DefaultSetting = @"";
				colvarNewDealType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarNewDealType);
				
				TableSchema.TableColumn colvarVerifyType = new TableSchema.TableColumn(schema);
				colvarVerifyType.ColumnName = "verify_type";
				colvarVerifyType.DataType = DbType.Int32;
				colvarVerifyType.MaxLength = 0;
				colvarVerifyType.AutoIncrement = false;
				colvarVerifyType.IsNullable = true;
				colvarVerifyType.IsPrimaryKey = false;
				colvarVerifyType.IsForeignKey = false;
				colvarVerifyType.IsReadOnly = false;
				colvarVerifyType.DefaultSetting = @"";
				colvarVerifyType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarVerifyType);
				
				TableSchema.TableColumn colvarDiscountType = new TableSchema.TableColumn(schema);
				colvarDiscountType.ColumnName = "discount_type";
				colvarDiscountType.DataType = DbType.Int32;
				colvarDiscountType.MaxLength = 0;
				colvarDiscountType.AutoIncrement = false;
				colvarDiscountType.IsNullable = false;
				colvarDiscountType.IsPrimaryKey = false;
				colvarDiscountType.IsForeignKey = false;
				colvarDiscountType.IsReadOnly = false;
				
						colvarDiscountType.DefaultSetting = @"((0))";
				colvarDiscountType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDiscountType);
				
				TableSchema.TableColumn colvarDiscount = new TableSchema.TableColumn(schema);
				colvarDiscount.ColumnName = "discount";
				colvarDiscount.DataType = DbType.Int32;
				colvarDiscount.MaxLength = 0;
				colvarDiscount.AutoIncrement = false;
				colvarDiscount.IsNullable = false;
				colvarDiscount.IsPrimaryKey = false;
				colvarDiscount.IsForeignKey = false;
				colvarDiscount.IsReadOnly = false;
				
						colvarDiscount.DefaultSetting = @"((0))";
				colvarDiscount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDiscount);
				
				TableSchema.TableColumn colvarIsExperience = new TableSchema.TableColumn(schema);
				colvarIsExperience.ColumnName = "is_experience";
				colvarIsExperience.DataType = DbType.Boolean;
				colvarIsExperience.MaxLength = 0;
				colvarIsExperience.AutoIncrement = false;
				colvarIsExperience.IsNullable = true;
				colvarIsExperience.IsPrimaryKey = false;
				colvarIsExperience.IsForeignKey = false;
				colvarIsExperience.IsReadOnly = false;
				colvarIsExperience.DefaultSetting = @"";
				colvarIsExperience.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsExperience);
				
				TableSchema.TableColumn colvarCompleteCopy = new TableSchema.TableColumn(schema);
				colvarCompleteCopy.ColumnName = "complete_copy";
				colvarCompleteCopy.DataType = DbType.Boolean;
				colvarCompleteCopy.MaxLength = 0;
				colvarCompleteCopy.AutoIncrement = false;
				colvarCompleteCopy.IsNullable = false;
				colvarCompleteCopy.IsPrimaryKey = false;
				colvarCompleteCopy.IsForeignKey = false;
				colvarCompleteCopy.IsReadOnly = false;
				
						colvarCompleteCopy.DefaultSetting = @"((0))";
				colvarCompleteCopy.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCompleteCopy);
				
				TableSchema.TableColumn colvarGroupCouponDealType = new TableSchema.TableColumn(schema);
				colvarGroupCouponDealType.ColumnName = "group_coupon_deal_type";
				colvarGroupCouponDealType.DataType = DbType.Int32;
				colvarGroupCouponDealType.MaxLength = 0;
				colvarGroupCouponDealType.AutoIncrement = false;
				colvarGroupCouponDealType.IsNullable = true;
				colvarGroupCouponDealType.IsPrimaryKey = false;
				colvarGroupCouponDealType.IsForeignKey = false;
				colvarGroupCouponDealType.IsReadOnly = false;
				colvarGroupCouponDealType.DefaultSetting = @"";
				colvarGroupCouponDealType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGroupCouponDealType);
				
				TableSchema.TableColumn colvarCategoryList = new TableSchema.TableColumn(schema);
				colvarCategoryList.ColumnName = "category_list";
				colvarCategoryList.DataType = DbType.AnsiString;
				colvarCategoryList.MaxLength = 2000;
				colvarCategoryList.AutoIncrement = false;
				colvarCategoryList.IsNullable = false;
				colvarCategoryList.IsPrimaryKey = false;
				colvarCategoryList.IsForeignKey = false;
				colvarCategoryList.IsReadOnly = false;
				
						colvarCategoryList.DefaultSetting = @"('[]')";
				colvarCategoryList.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCategoryList);
				
				TableSchema.TableColumn colvarIsFreightsDeal = new TableSchema.TableColumn(schema);
				colvarIsFreightsDeal.ColumnName = "is_freights_deal";
				colvarIsFreightsDeal.DataType = DbType.Boolean;
				colvarIsFreightsDeal.MaxLength = 0;
				colvarIsFreightsDeal.AutoIncrement = false;
				colvarIsFreightsDeal.IsNullable = false;
				colvarIsFreightsDeal.IsPrimaryKey = false;
				colvarIsFreightsDeal.IsForeignKey = false;
				colvarIsFreightsDeal.IsReadOnly = false;
				
						colvarIsFreightsDeal.DefaultSetting = @"((0))";
				colvarIsFreightsDeal.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsFreightsDeal);
				
				TableSchema.TableColumn colvarConsignment = new TableSchema.TableColumn(schema);
				colvarConsignment.ColumnName = "consignment";
				colvarConsignment.DataType = DbType.Boolean;
				colvarConsignment.MaxLength = 0;
				colvarConsignment.AutoIncrement = false;
				colvarConsignment.IsNullable = false;
				colvarConsignment.IsPrimaryKey = false;
				colvarConsignment.IsForeignKey = false;
				colvarConsignment.IsReadOnly = false;
				
						colvarConsignment.DefaultSetting = @"((0))";
				colvarConsignment.ForeignKeyTableName = "";
				schema.Columns.Add(colvarConsignment);
				
				TableSchema.TableColumn colvarCouponSeparateDigits = new TableSchema.TableColumn(schema);
				colvarCouponSeparateDigits.ColumnName = "coupon_separate_digits";
				colvarCouponSeparateDigits.DataType = DbType.Int32;
				colvarCouponSeparateDigits.MaxLength = 0;
				colvarCouponSeparateDigits.AutoIncrement = false;
				colvarCouponSeparateDigits.IsNullable = true;
				colvarCouponSeparateDigits.IsPrimaryKey = false;
				colvarCouponSeparateDigits.IsForeignKey = false;
				colvarCouponSeparateDigits.IsReadOnly = false;
				colvarCouponSeparateDigits.DefaultSetting = @"";
				colvarCouponSeparateDigits.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCouponSeparateDigits);
				
				TableSchema.TableColumn colvarFreightsId = new TableSchema.TableColumn(schema);
				colvarFreightsId.ColumnName = "freights_id";
				colvarFreightsId.DataType = DbType.Int32;
				colvarFreightsId.MaxLength = 0;
				colvarFreightsId.AutoIncrement = false;
				colvarFreightsId.IsNullable = false;
				colvarFreightsId.IsPrimaryKey = false;
				colvarFreightsId.IsForeignKey = false;
				colvarFreightsId.IsReadOnly = false;
				
						colvarFreightsId.DefaultSetting = @"((0))";
				colvarFreightsId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFreightsId);
				
				TableSchema.TableColumn colvarOperationSalesId = new TableSchema.TableColumn(schema);
				colvarOperationSalesId.ColumnName = "operation_sales_id";
				colvarOperationSalesId.DataType = DbType.Int32;
				colvarOperationSalesId.MaxLength = 0;
				colvarOperationSalesId.AutoIncrement = false;
				colvarOperationSalesId.IsNullable = true;
				colvarOperationSalesId.IsPrimaryKey = false;
				colvarOperationSalesId.IsForeignKey = false;
				colvarOperationSalesId.IsReadOnly = false;
				colvarOperationSalesId.DefaultSetting = @"";
				colvarOperationSalesId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOperationSalesId);
				
				TableSchema.TableColumn colvarIsDepositCoffee = new TableSchema.TableColumn(schema);
				colvarIsDepositCoffee.ColumnName = "is_deposit_coffee";
				colvarIsDepositCoffee.DataType = DbType.Boolean;
				colvarIsDepositCoffee.MaxLength = 0;
				colvarIsDepositCoffee.AutoIncrement = false;
				colvarIsDepositCoffee.IsNullable = false;
				colvarIsDepositCoffee.IsPrimaryKey = false;
				colvarIsDepositCoffee.IsForeignKey = false;
				colvarIsDepositCoffee.IsReadOnly = false;
				
						colvarIsDepositCoffee.DefaultSetting = @"((0))";
				colvarIsDepositCoffee.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsDepositCoffee);
				
				TableSchema.TableColumn colvarIsPromotionDeal = new TableSchema.TableColumn(schema);
				colvarIsPromotionDeal.ColumnName = "is_promotion_deal";
				colvarIsPromotionDeal.DataType = DbType.Boolean;
				colvarIsPromotionDeal.MaxLength = 0;
				colvarIsPromotionDeal.AutoIncrement = false;
				colvarIsPromotionDeal.IsNullable = false;
				colvarIsPromotionDeal.IsPrimaryKey = false;
				colvarIsPromotionDeal.IsForeignKey = false;
				colvarIsPromotionDeal.IsReadOnly = false;
				
						colvarIsPromotionDeal.DefaultSetting = @"((0))";
				colvarIsPromotionDeal.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsPromotionDeal);
				
				TableSchema.TableColumn colvarIsExhibitionDeal = new TableSchema.TableColumn(schema);
				colvarIsExhibitionDeal.ColumnName = "is_exhibition_deal";
				colvarIsExhibitionDeal.DataType = DbType.Boolean;
				colvarIsExhibitionDeal.MaxLength = 0;
				colvarIsExhibitionDeal.AutoIncrement = false;
				colvarIsExhibitionDeal.IsNullable = false;
				colvarIsExhibitionDeal.IsPrimaryKey = false;
				colvarIsExhibitionDeal.IsForeignKey = false;
				colvarIsExhibitionDeal.IsReadOnly = false;
				
						colvarIsExhibitionDeal.DefaultSetting = @"((0))";
				colvarIsExhibitionDeal.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsExhibitionDeal);
				
				TableSchema.TableColumn colvarVerifyActionType = new TableSchema.TableColumn(schema);
				colvarVerifyActionType.ColumnName = "verify_action_type";
				colvarVerifyActionType.DataType = DbType.Int32;
				colvarVerifyActionType.MaxLength = 0;
				colvarVerifyActionType.AutoIncrement = false;
				colvarVerifyActionType.IsNullable = false;
				colvarVerifyActionType.IsPrimaryKey = false;
				colvarVerifyActionType.IsForeignKey = false;
				colvarVerifyActionType.IsReadOnly = false;
				
						colvarVerifyActionType.DefaultSetting = @"((1))";
				colvarVerifyActionType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarVerifyActionType);
				
				TableSchema.TableColumn colvarIsHouseNewVer = new TableSchema.TableColumn(schema);
				colvarIsHouseNewVer.ColumnName = "is_house_new_ver";
				colvarIsHouseNewVer.DataType = DbType.Boolean;
				colvarIsHouseNewVer.MaxLength = 0;
				colvarIsHouseNewVer.AutoIncrement = false;
				colvarIsHouseNewVer.IsNullable = false;
				colvarIsHouseNewVer.IsPrimaryKey = false;
				colvarIsHouseNewVer.IsForeignKey = false;
				colvarIsHouseNewVer.IsReadOnly = false;
				
						colvarIsHouseNewVer.DefaultSetting = @"((0))";
				colvarIsHouseNewVer.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsHouseNewVer);
				
				TableSchema.TableColumn colvarCchannel = new TableSchema.TableColumn(schema);
				colvarCchannel.ColumnName = "cchannel";
				colvarCchannel.DataType = DbType.Boolean;
				colvarCchannel.MaxLength = 0;
				colvarCchannel.AutoIncrement = false;
				colvarCchannel.IsNullable = false;
				colvarCchannel.IsPrimaryKey = false;
				colvarCchannel.IsForeignKey = false;
				colvarCchannel.IsReadOnly = false;
				
						colvarCchannel.DefaultSetting = @"((0))";
				colvarCchannel.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCchannel);
				
				TableSchema.TableColumn colvarCchannelLink = new TableSchema.TableColumn(schema);
				colvarCchannelLink.ColumnName = "cchannel_link";
				colvarCchannelLink.DataType = DbType.AnsiString;
				colvarCchannelLink.MaxLength = 255;
				colvarCchannelLink.AutoIncrement = false;
				colvarCchannelLink.IsNullable = true;
				colvarCchannelLink.IsPrimaryKey = false;
				colvarCchannelLink.IsForeignKey = false;
				colvarCchannelLink.IsReadOnly = false;
				colvarCchannelLink.DefaultSetting = @"";
				colvarCchannelLink.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCchannelLink);
				
				TableSchema.TableColumn colvarIsExpiredDeal = new TableSchema.TableColumn(schema);
				colvarIsExpiredDeal.ColumnName = "is_expired_deal";
				colvarIsExpiredDeal.DataType = DbType.Boolean;
				colvarIsExpiredDeal.MaxLength = 0;
				colvarIsExpiredDeal.AutoIncrement = false;
				colvarIsExpiredDeal.IsNullable = false;
				colvarIsExpiredDeal.IsPrimaryKey = false;
				colvarIsExpiredDeal.IsForeignKey = false;
				colvarIsExpiredDeal.IsReadOnly = false;
				
						colvarIsExpiredDeal.DefaultSetting = @"((0))";
				colvarIsExpiredDeal.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsExpiredDeal);
				
				TableSchema.TableColumn colvarDealTypeDetail = new TableSchema.TableColumn(schema);
				colvarDealTypeDetail.ColumnName = "deal_type_detail";
				colvarDealTypeDetail.DataType = DbType.Int32;
				colvarDealTypeDetail.MaxLength = 0;
				colvarDealTypeDetail.AutoIncrement = false;
				colvarDealTypeDetail.IsNullable = true;
				colvarDealTypeDetail.IsPrimaryKey = false;
				colvarDealTypeDetail.IsForeignKey = false;
				colvarDealTypeDetail.IsReadOnly = false;
				
						colvarDealTypeDetail.DefaultSetting = @"((0))";
				colvarDealTypeDetail.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDealTypeDetail);
				
				TableSchema.TableColumn colvarIsGame = new TableSchema.TableColumn(schema);
				colvarIsGame.ColumnName = "is_game";
				colvarIsGame.DataType = DbType.Boolean;
				colvarIsGame.MaxLength = 0;
				colvarIsGame.AutoIncrement = false;
				colvarIsGame.IsNullable = false;
				colvarIsGame.IsPrimaryKey = false;
				colvarIsGame.IsForeignKey = false;
				colvarIsGame.IsReadOnly = false;
				
						colvarIsGame.DefaultSetting = @"('0')";
				colvarIsGame.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsGame);

                TableSchema.TableColumn colvarIsWMS = new TableSchema.TableColumn(schema);
                colvarIsWMS.ColumnName = "is_wms";
                colvarIsWMS.DataType = DbType.Boolean;
                colvarIsWMS.MaxLength = 0;
                colvarIsWMS.AutoIncrement = false;
                colvarIsWMS.IsNullable = false;
                colvarIsWMS.IsPrimaryKey = false;
                colvarIsWMS.IsForeignKey = false;
                colvarIsWMS.IsReadOnly = false;

                colvarIsWMS.DefaultSetting = @"('0')";
                colvarIsWMS.ForeignKeyTableName = "";
                schema.Columns.Add(colvarIsWMS);

                TableSchema.TableColumn colvarAgentChannels = new TableSchema.TableColumn(schema);
				colvarAgentChannels.ColumnName = "agent_channels";
				colvarAgentChannels.DataType = DbType.AnsiString;
				colvarAgentChannels.MaxLength = 50;
				colvarAgentChannels.AutoIncrement = false;
				colvarAgentChannels.IsNullable = false;
				colvarAgentChannels.IsPrimaryKey = false;
				colvarAgentChannels.IsForeignKey = false;
				colvarAgentChannels.IsReadOnly = false;
				
						colvarAgentChannels.DefaultSetting = @"('')";
				colvarAgentChannels.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAgentChannels);
				
				TableSchema.TableColumn colvarIsChannelGift = new TableSchema.TableColumn(schema);
				colvarIsChannelGift.ColumnName = "is_channel_gift";
				colvarIsChannelGift.DataType = DbType.Boolean;
				colvarIsChannelGift.MaxLength = 0;
				colvarIsChannelGift.AutoIncrement = false;
				colvarIsChannelGift.IsNullable = false;
				colvarIsChannelGift.IsPrimaryKey = false;
				colvarIsChannelGift.IsForeignKey = false;
				colvarIsChannelGift.IsReadOnly = false;
				
						colvarIsChannelGift.DefaultSetting = @"((0))";
				colvarIsChannelGift.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsChannelGift);

				TableSchema.TableColumn colvarIsTaishinChosen = new TableSchema.TableColumn(schema);
				colvarIsTaishinChosen.ColumnName = "is_taishin_chosen";
				colvarIsTaishinChosen.DataType = DbType.Boolean;
				colvarIsTaishinChosen.MaxLength = 0;
				colvarIsTaishinChosen.AutoIncrement = false;
				colvarIsTaishinChosen.IsNullable = false;
				colvarIsTaishinChosen.IsPrimaryKey = false;
				colvarIsTaishinChosen.IsForeignKey = false;
				colvarIsTaishinChosen.IsReadOnly = false;
				colvarIsTaishinChosen.DefaultSetting = @"((0))";
				colvarIsTaishinChosen.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsTaishinChosen);

				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("deal_property",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("BusinessHourGuid")]
		[Bindable(true)]
		public Guid BusinessHourGuid 
		{
			get { return GetColumnValue<Guid>(Columns.BusinessHourGuid); }
			set { SetColumnValue(Columns.BusinessHourGuid, value); }
		}
		  
		[XmlAttribute("DealType")]
		[Bindable(true)]
		public int? DealType 
		{
			get { return GetColumnValue<int?>(Columns.DealType); }
			set { SetColumnValue(Columns.DealType, value); }
		}
		  
		[XmlAttribute("SellerVerifyType")]
		[Bindable(true)]
		public int? SellerVerifyType 
		{
			get { return GetColumnValue<int?>(Columns.SellerVerifyType); }
			set { SetColumnValue(Columns.SellerVerifyType, value); }
		}
		  
		[XmlAttribute("DealAccBusinessGroupId")]
		[Bindable(true)]
		public int? DealAccBusinessGroupId 
		{
			get { return GetColumnValue<int?>(Columns.DealAccBusinessGroupId); }
			set { SetColumnValue(Columns.DealAccBusinessGroupId, value); }
		}
		  
		[XmlAttribute("DealEmpName")]
		[Bindable(true)]
		public string DealEmpName 
		{
			get { return GetColumnValue<string>(Columns.DealEmpName); }
			set { SetColumnValue(Columns.DealEmpName, value); }
		}
		  
		[XmlAttribute("CreateId")]
		[Bindable(true)]
		public string CreateId 
		{
			get { return GetColumnValue<string>(Columns.CreateId); }
			set { SetColumnValue(Columns.CreateId, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime? CreateTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		  
		[XmlAttribute("ModifyId")]
		[Bindable(true)]
		public string ModifyId 
		{
			get { return GetColumnValue<string>(Columns.ModifyId); }
			set { SetColumnValue(Columns.ModifyId, value); }
		}
		  
		[XmlAttribute("ModifyTime")]
		[Bindable(true)]
		public DateTime? ModifyTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.ModifyTime); }
			set { SetColumnValue(Columns.ModifyTime, value); }
		}
		  
		[XmlAttribute("UniqueId")]
		[Bindable(true)]
		public int UniqueId 
		{
			get { return GetColumnValue<int>(Columns.UniqueId); }
			set { SetColumnValue(Columns.UniqueId, value); }
		}
		  
		[XmlAttribute("CityList")]
		[Bindable(true)]
		public string CityList 
		{
			get { return GetColumnValue<string>(Columns.CityList); }
			set { SetColumnValue(Columns.CityList, value); }
		}
		  
		[XmlAttribute("DeliveryType")]
		[Bindable(true)]
		public int? DeliveryType 
		{
			get { return GetColumnValue<int?>(Columns.DeliveryType); }
			set { SetColumnValue(Columns.DeliveryType, value); }
		}
		  
		[XmlAttribute("ShoppingCart")]
		[Bindable(true)]
		public bool ShoppingCart 
		{
			get { return GetColumnValue<bool>(Columns.ShoppingCart); }
			set { SetColumnValue(Columns.ShoppingCart, value); }
		}
		  
		[XmlAttribute("ComboPackCount")]
		[Bindable(true)]
		public int ComboPackCount 
		{
			get { return GetColumnValue<int>(Columns.ComboPackCount); }
			set { SetColumnValue(Columns.ComboPackCount, value); }
		}
		  
		[XmlAttribute("AncestorBusinessHourGuid")]
		[Bindable(true)]
		public Guid? AncestorBusinessHourGuid 
		{
			get { return GetColumnValue<Guid?>(Columns.AncestorBusinessHourGuid); }
			set { SetColumnValue(Columns.AncestorBusinessHourGuid, value); }
		}
		  
		[XmlAttribute("IsTravelcard")]
		[Bindable(true)]
		public bool IsTravelcard 
		{
			get { return GetColumnValue<bool>(Columns.IsTravelcard); }
			set { SetColumnValue(Columns.IsTravelcard, value); }
		}
		  
		[XmlAttribute("EntrustSell")]
		[Bindable(true)]
		public int EntrustSell 
		{
			get { return GetColumnValue<int>(Columns.EntrustSell); }
			set { SetColumnValue(Columns.EntrustSell, value); }
		}
		  
		[XmlAttribute("TravelPlace")]
		[Bindable(true)]
		public string TravelPlace 
		{
			get { return GetColumnValue<string>(Columns.TravelPlace); }
			set { SetColumnValue(Columns.TravelPlace, value); }
		}
		  
		[XmlAttribute("MultipleBranch")]
		[Bindable(true)]
		public bool MultipleBranch 
		{
			get { return GetColumnValue<bool>(Columns.MultipleBranch); }
			set { SetColumnValue(Columns.MultipleBranch, value); }
		}
		  
		[XmlAttribute("EmpNo")]
		[Bindable(true)]
		public string EmpNo 
		{
			get { return GetColumnValue<string>(Columns.EmpNo); }
			set { SetColumnValue(Columns.EmpNo, value); }
		}
		  
		[XmlAttribute("QuantityMultiplier")]
		[Bindable(true)]
		public int? QuantityMultiplier 
		{
			get { return GetColumnValue<int?>(Columns.QuantityMultiplier); }
			set { SetColumnValue(Columns.QuantityMultiplier, value); }
		}
		  
		[XmlAttribute("IsDailyRestriction")]
		[Bindable(true)]
		public bool IsDailyRestriction 
		{
			get { return GetColumnValue<bool>(Columns.IsDailyRestriction); }
			set { SetColumnValue(Columns.IsDailyRestriction, value); }
		}
		  
		[XmlAttribute("IsContinuedSequence")]
		[Bindable(true)]
		public bool IsContinuedSequence 
		{
			get { return GetColumnValue<bool>(Columns.IsContinuedSequence); }
			set { SetColumnValue(Columns.IsContinuedSequence, value); }
		}
		  
		[XmlAttribute("IsContinuedQuantity")]
		[Bindable(true)]
		public bool IsContinuedQuantity 
		{
			get { return GetColumnValue<bool>(Columns.IsContinuedQuantity); }
			set { SetColumnValue(Columns.IsContinuedQuantity, value); }
		}
		  
		[XmlAttribute("ContinuedQuantity")]
		[Bindable(true)]
		public int ContinuedQuantity 
		{
			get { return GetColumnValue<int>(Columns.ContinuedQuantity); }
			set { SetColumnValue(Columns.ContinuedQuantity, value); }
		}
		  
		[XmlAttribute("ActivityUrl")]
		[Bindable(true)]
		public string ActivityUrl 
		{
			get { return GetColumnValue<string>(Columns.ActivityUrl); }
			set { SetColumnValue(Columns.ActivityUrl, value); }
		}
		  
		[XmlAttribute("LabelTagList")]
		[Bindable(true)]
		public string LabelTagList 
		{
			get { return GetColumnValue<string>(Columns.LabelTagList); }
			set { SetColumnValue(Columns.LabelTagList, value); }
		}
		  
		[XmlAttribute("LabelIconList")]
		[Bindable(true)]
		public string LabelIconList 
		{
			get { return GetColumnValue<string>(Columns.LabelIconList); }
			set { SetColumnValue(Columns.LabelIconList, value); }
		}
		  
		[XmlAttribute("CouponCodeType")]
		[Bindable(true)]
		public int CouponCodeType 
		{
			get { return GetColumnValue<int>(Columns.CouponCodeType); }
			set { SetColumnValue(Columns.CouponCodeType, value); }
		}
		  
		[XmlAttribute("PinType")]
		[Bindable(true)]
		public int PinType 
		{
			get { return GetColumnValue<int>(Columns.PinType); }
			set { SetColumnValue(Columns.PinType, value); }
		}
		  
		[XmlAttribute("BookingSystemType")]
		[Bindable(true)]
		public int BookingSystemType 
		{
			get { return GetColumnValue<int>(Columns.BookingSystemType); }
			set { SetColumnValue(Columns.BookingSystemType, value); }
		}
		  
		[XmlAttribute("AdvanceReservationDays")]
		[Bindable(true)]
		public int AdvanceReservationDays 
		{
			get { return GetColumnValue<int>(Columns.AdvanceReservationDays); }
			set { SetColumnValue(Columns.AdvanceReservationDays, value); }
		}
		  
		[XmlAttribute("CouponUsers")]
		[Bindable(true)]
		public int CouponUsers 
		{
			get { return GetColumnValue<int>(Columns.CouponUsers); }
			set { SetColumnValue(Columns.CouponUsers, value); }
		}
		  
		[XmlAttribute("IsReserveLock")]
		[Bindable(true)]
		public bool IsReserveLock 
		{
			get { return GetColumnValue<bool>(Columns.IsReserveLock); }
			set { SetColumnValue(Columns.IsReserveLock, value); }
		}
		  
		[XmlAttribute("CustomTag")]
		[Bindable(true)]
		public string CustomTag 
		{
			get { return GetColumnValue<string>(Columns.CustomTag); }
			set { SetColumnValue(Columns.CustomTag, value); }
		}
		  
		[XmlAttribute("TmallRmbPrice")]
		[Bindable(true)]
		public decimal TmallRmbPrice 
		{
			get { return GetColumnValue<decimal>(Columns.TmallRmbPrice); }
			set { SetColumnValue(Columns.TmallRmbPrice, value); }
		}
		  
		[XmlAttribute("TmallRmbExchangeRate")]
		[Bindable(true)]
		public decimal TmallRmbExchangeRate 
		{
			get { return GetColumnValue<decimal>(Columns.TmallRmbExchangeRate); }
			set { SetColumnValue(Columns.TmallRmbExchangeRate, value); }
		}
		  
		[XmlAttribute("ExchangePrice")]
		[Bindable(true)]
		public decimal ExchangePrice 
		{
			get { return GetColumnValue<decimal>(Columns.ExchangePrice); }
			set { SetColumnValue(Columns.ExchangePrice, value); }
		}
		  
		[XmlAttribute("IsQuantityMultiplier")]
		[Bindable(true)]
		public bool IsQuantityMultiplier 
		{
			get { return GetColumnValue<bool>(Columns.IsQuantityMultiplier); }
			set { SetColumnValue(Columns.IsQuantityMultiplier, value); }
		}
		  
		[XmlAttribute("IsAveragePrice")]
		[Bindable(true)]
		public bool IsAveragePrice 
		{
			get { return GetColumnValue<bool>(Columns.IsAveragePrice); }
			set { SetColumnValue(Columns.IsAveragePrice, value); }
		}
		  
		[XmlAttribute("IsMergeCount")]
		[Bindable(true)]
		public bool IsMergeCount 
		{
			get { return GetColumnValue<bool>(Columns.IsMergeCount); }
			set { SetColumnValue(Columns.IsMergeCount, value); }
		}
		  
		[XmlAttribute("IsZeroActivityShowCoupon")]
		[Bindable(true)]
		public bool IsZeroActivityShowCoupon 
		{
			get { return GetColumnValue<bool>(Columns.IsZeroActivityShowCoupon); }
			set { SetColumnValue(Columns.IsZeroActivityShowCoupon, value); }
		}
		  
		[XmlAttribute("IsTravelDeal")]
		[Bindable(true)]
		public bool IsTravelDeal 
		{
			get { return GetColumnValue<bool>(Columns.IsTravelDeal); }
			set { SetColumnValue(Columns.IsTravelDeal, value); }
		}
		  
		[XmlAttribute("AncestorSequenceBusinessHourGuid")]
		[Bindable(true)]
		public Guid? AncestorSequenceBusinessHourGuid 
		{
			get { return GetColumnValue<Guid?>(Columns.AncestorSequenceBusinessHourGuid); }
			set { SetColumnValue(Columns.AncestorSequenceBusinessHourGuid, value); }
		}
		  
		[XmlAttribute("IsLongContract")]
		[Bindable(true)]
		public bool IsLongContract 
		{
			get { return GetColumnValue<bool>(Columns.IsLongContract); }
			set { SetColumnValue(Columns.IsLongContract, value); }
		}
		  
		[XmlAttribute("Installment3months")]
		[Bindable(true)]
		public bool Installment3months 
		{
			get { return GetColumnValue<bool>(Columns.Installment3months); }
			set { SetColumnValue(Columns.Installment3months, value); }
		}
		  
		[XmlAttribute("Installment6months")]
		[Bindable(true)]
		public bool Installment6months 
		{
			get { return GetColumnValue<bool>(Columns.Installment6months); }
			set { SetColumnValue(Columns.Installment6months, value); }
		}
		  
		[XmlAttribute("Installment12months")]
		[Bindable(true)]
		public bool Installment12months 
		{
			get { return GetColumnValue<bool>(Columns.Installment12months); }
			set { SetColumnValue(Columns.Installment12months, value); }
		}
		  
		[XmlAttribute("DenyInstallment")]
		[Bindable(true)]
		public bool DenyInstallment 
		{
			get { return GetColumnValue<bool>(Columns.DenyInstallment); }
			set { SetColumnValue(Columns.DenyInstallment, value); }
		}
		  
		[XmlAttribute("SaleMultipleBase")]
		[Bindable(true)]
		public int? SaleMultipleBase 
		{
			get { return GetColumnValue<int?>(Columns.SaleMultipleBase); }
			set { SetColumnValue(Columns.SaleMultipleBase, value); }
		}
		  
		[XmlAttribute("ShipType")]
		[Bindable(true)]
		public int? ShipType 
		{
			get { return GetColumnValue<int?>(Columns.ShipType); }
			set { SetColumnValue(Columns.ShipType, value); }
		}
		  
		[XmlAttribute("ShippingdateType")]
		[Bindable(true)]
		public int? ShippingdateType 
		{
			get { return GetColumnValue<int?>(Columns.ShippingdateType); }
			set { SetColumnValue(Columns.ShippingdateType, value); }
		}
		  
		[XmlAttribute("Shippingdate")]
		[Bindable(true)]
		public int? Shippingdate 
		{
			get { return GetColumnValue<int?>(Columns.Shippingdate); }
			set { SetColumnValue(Columns.Shippingdate, value); }
		}
		  
		[XmlAttribute("ProductUseDateStartSet")]
		[Bindable(true)]
		public int? ProductUseDateStartSet 
		{
			get { return GetColumnValue<int?>(Columns.ProductUseDateStartSet); }
			set { SetColumnValue(Columns.ProductUseDateStartSet, value); }
		}
		  
		[XmlAttribute("ProductUseDateEndSet")]
		[Bindable(true)]
		public int? ProductUseDateEndSet 
		{
			get { return GetColumnValue<int?>(Columns.ProductUseDateEndSet); }
			set { SetColumnValue(Columns.ProductUseDateEndSet, value); }
		}
		  
		[XmlAttribute("PresentQuantity")]
		[Bindable(true)]
		public int PresentQuantity 
		{
			get { return GetColumnValue<int>(Columns.PresentQuantity); }
			set { SetColumnValue(Columns.PresentQuantity, value); }
		}
		  
		[XmlAttribute("DevelopeSalesId")]
		[Bindable(true)]
		public int DevelopeSalesId 
		{
			get { return GetColumnValue<int>(Columns.DevelopeSalesId); }
			set { SetColumnValue(Columns.DevelopeSalesId, value); }
		}
		  
		[XmlAttribute("GroupCouponAppStyle")]
		[Bindable(true)]
		public int GroupCouponAppStyle 
		{
			get { return GetColumnValue<int>(Columns.GroupCouponAppStyle); }
			set { SetColumnValue(Columns.GroupCouponAppStyle, value); }
		}
		  
		[XmlAttribute("NewDealType")]
		[Bindable(true)]
		public int? NewDealType 
		{
			get { return GetColumnValue<int?>(Columns.NewDealType); }
			set { SetColumnValue(Columns.NewDealType, value); }
		}
		  
		[XmlAttribute("VerifyType")]
		[Bindable(true)]
		public int? VerifyType 
		{
			get { return GetColumnValue<int?>(Columns.VerifyType); }
			set { SetColumnValue(Columns.VerifyType, value); }
		}
		  
		[XmlAttribute("DiscountType")]
		[Bindable(true)]
		public int DiscountType 
		{
			get { return GetColumnValue<int>(Columns.DiscountType); }
			set { SetColumnValue(Columns.DiscountType, value); }
		}
		  
		[XmlAttribute("Discount")]
		[Bindable(true)]
		public int Discount 
		{
			get { return GetColumnValue<int>(Columns.Discount); }
			set { SetColumnValue(Columns.Discount, value); }
		}
		  
		[XmlAttribute("IsExperience")]
		[Bindable(true)]
		public bool? IsExperience 
		{
			get { return GetColumnValue<bool?>(Columns.IsExperience); }
			set { SetColumnValue(Columns.IsExperience, value); }
		}
		  
		[XmlAttribute("CompleteCopy")]
		[Bindable(true)]
		public bool CompleteCopy 
		{
			get { return GetColumnValue<bool>(Columns.CompleteCopy); }
			set { SetColumnValue(Columns.CompleteCopy, value); }
		}
		  
		[XmlAttribute("GroupCouponDealType")]
		[Bindable(true)]
		public int? GroupCouponDealType 
		{
			get { return GetColumnValue<int?>(Columns.GroupCouponDealType); }
			set { SetColumnValue(Columns.GroupCouponDealType, value); }
		}
		  
		[XmlAttribute("CategoryList")]
		[Bindable(true)]
		public string CategoryList 
		{
			get { return GetColumnValue<string>(Columns.CategoryList); }
			set { SetColumnValue(Columns.CategoryList, value); }
		}
		  
		[XmlAttribute("IsFreightsDeal")]
		[Bindable(true)]
		public bool IsFreightsDeal 
		{
			get { return GetColumnValue<bool>(Columns.IsFreightsDeal); }
			set { SetColumnValue(Columns.IsFreightsDeal, value); }
		}
		  
		[XmlAttribute("Consignment")]
		[Bindable(true)]
		public bool Consignment 
		{
			get { return GetColumnValue<bool>(Columns.Consignment); }
			set { SetColumnValue(Columns.Consignment, value); }
		}
		  
		[XmlAttribute("CouponSeparateDigits")]
		[Bindable(true)]
		public int? CouponSeparateDigits 
		{
			get { return GetColumnValue<int?>(Columns.CouponSeparateDigits); }
			set { SetColumnValue(Columns.CouponSeparateDigits, value); }
		}
		  
		[XmlAttribute("FreightsId")]
		[Bindable(true)]
		public int FreightsId 
		{
			get { return GetColumnValue<int>(Columns.FreightsId); }
			set { SetColumnValue(Columns.FreightsId, value); }
		}
		  
		[XmlAttribute("OperationSalesId")]
		[Bindable(true)]
		public int? OperationSalesId 
		{
			get { return GetColumnValue<int?>(Columns.OperationSalesId); }
			set { SetColumnValue(Columns.OperationSalesId, value); }
		}
		  
		[XmlAttribute("IsDepositCoffee")]
		[Bindable(true)]
		public bool IsDepositCoffee 
		{
			get { return GetColumnValue<bool>(Columns.IsDepositCoffee); }
			set { SetColumnValue(Columns.IsDepositCoffee, value); }
		}
		  
		[XmlAttribute("IsPromotionDeal")]
		[Bindable(true)]
		public bool IsPromotionDeal 
		{
			get { return GetColumnValue<bool>(Columns.IsPromotionDeal); }
			set { SetColumnValue(Columns.IsPromotionDeal, value); }
		}
		  
		[XmlAttribute("IsExhibitionDeal")]
		[Bindable(true)]
		public bool IsExhibitionDeal 
		{
			get { return GetColumnValue<bool>(Columns.IsExhibitionDeal); }
			set { SetColumnValue(Columns.IsExhibitionDeal, value); }
		}
		  
		[XmlAttribute("VerifyActionType")]
		[Bindable(true)]
		public int VerifyActionType 
		{
			get { return GetColumnValue<int>(Columns.VerifyActionType); }
			set { SetColumnValue(Columns.VerifyActionType, value); }
		}
		  
		[XmlAttribute("IsHouseNewVer")]
		[Bindable(true)]
		public bool IsHouseNewVer 
		{
			get { return GetColumnValue<bool>(Columns.IsHouseNewVer); }
			set { SetColumnValue(Columns.IsHouseNewVer, value); }
		}
		  
		[XmlAttribute("Cchannel")]
		[Bindable(true)]
		public bool Cchannel 
		{
			get { return GetColumnValue<bool>(Columns.Cchannel); }
			set { SetColumnValue(Columns.Cchannel, value); }
		}
		  
		[XmlAttribute("CchannelLink")]
		[Bindable(true)]
		public string CchannelLink 
		{
			get { return GetColumnValue<string>(Columns.CchannelLink); }
			set { SetColumnValue(Columns.CchannelLink, value); }
		}
		  
		[XmlAttribute("IsExpiredDeal")]
		[Bindable(true)]
		public bool IsExpiredDeal 
		{
			get { return GetColumnValue<bool>(Columns.IsExpiredDeal); }
			set { SetColumnValue(Columns.IsExpiredDeal, value); }
		}
		  
		[XmlAttribute("DealTypeDetail")]
		[Bindable(true)]
		public int? DealTypeDetail 
		{
			get { return GetColumnValue<int?>(Columns.DealTypeDetail); }
			set { SetColumnValue(Columns.DealTypeDetail, value); }
		}
		  
		[XmlAttribute("IsGame")]
		[Bindable(true)]
		public bool IsGame 
		{
			get { return GetColumnValue<bool>(Columns.IsGame); }
			set { SetColumnValue(Columns.IsGame, value); }
		}
		  
		[XmlAttribute("IsWms")]
		[Bindable(true)]
        public bool IsWms
        {
            get { return GetColumnValue<bool>(Columns.IsWMS); }
            set { SetColumnValue(Columns.IsWMS, value); }
        }

        [XmlAttribute("AgentChannels")]
		[Bindable(true)]
		public string AgentChannels 
		{
			get { return GetColumnValue<string>(Columns.AgentChannels); }
			set { SetColumnValue(Columns.AgentChannels, value); }
		}
		  
		[XmlAttribute("IsChannelGift")]
		[Bindable(true)]
		public bool IsChannelGift 
		{
			get { return GetColumnValue<bool>(Columns.IsChannelGift); }
			set { SetColumnValue(Columns.IsChannelGift, value); }
		}

		[XmlAttribute("IsTaishinChosen")]
		[Bindable(true)]
		public bool IsTaishinChosen
		{
			get { return GetColumnValue<bool>(Columns.IsTaishinChosen); }
			set { SetColumnValue(Columns.IsTaishinChosen, value); }
		}

		#endregion




		//no foreign key tables defined (2)



		//no ManyToMany tables defined (0)





		#region Typed Columns


		public static TableSchema.TableColumn BusinessHourGuidColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn DealTypeColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn SellerVerifyTypeColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn DealAccBusinessGroupIdColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn DealEmpNameColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyIdColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn UniqueIdColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn CityListColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        public static TableSchema.TableColumn DeliveryTypeColumn
        {
            get { return Schema.Columns[11]; }
        }
        
        
        
        public static TableSchema.TableColumn ShoppingCartColumn
        {
            get { return Schema.Columns[12]; }
        }
        
        
        
        public static TableSchema.TableColumn ComboPackCountColumn
        {
            get { return Schema.Columns[13]; }
        }
        
        
        
        public static TableSchema.TableColumn AncestorBusinessHourGuidColumn
        {
            get { return Schema.Columns[14]; }
        }
        
        
        
        public static TableSchema.TableColumn IsTravelcardColumn
        {
            get { return Schema.Columns[15]; }
        }
        
        
        
        public static TableSchema.TableColumn EntrustSellColumn
        {
            get { return Schema.Columns[16]; }
        }
        
        
        
        public static TableSchema.TableColumn TravelPlaceColumn
        {
            get { return Schema.Columns[17]; }
        }
        
        
        
        public static TableSchema.TableColumn MultipleBranchColumn
        {
            get { return Schema.Columns[18]; }
        }
        
        
        
        public static TableSchema.TableColumn EmpNoColumn
        {
            get { return Schema.Columns[19]; }
        }
        
        
        
        public static TableSchema.TableColumn QuantityMultiplierColumn
        {
            get { return Schema.Columns[20]; }
        }
        
        
        
        public static TableSchema.TableColumn IsDailyRestrictionColumn
        {
            get { return Schema.Columns[21]; }
        }
        
        
        
        public static TableSchema.TableColumn IsContinuedSequenceColumn
        {
            get { return Schema.Columns[22]; }
        }
        
        
        
        public static TableSchema.TableColumn IsContinuedQuantityColumn
        {
            get { return Schema.Columns[23]; }
        }
        
        
        
        public static TableSchema.TableColumn ContinuedQuantityColumn
        {
            get { return Schema.Columns[24]; }
        }
        
        
        
        public static TableSchema.TableColumn ActivityUrlColumn
        {
            get { return Schema.Columns[25]; }
        }
        
        
        
        public static TableSchema.TableColumn LabelTagListColumn
        {
            get { return Schema.Columns[26]; }
        }
        
        
        
        public static TableSchema.TableColumn LabelIconListColumn
        {
            get { return Schema.Columns[27]; }
        }
        
        
        
        public static TableSchema.TableColumn CouponCodeTypeColumn
        {
            get { return Schema.Columns[28]; }
        }
        
        
        
        public static TableSchema.TableColumn PinTypeColumn
        {
            get { return Schema.Columns[29]; }
        }
        
        
        
        public static TableSchema.TableColumn BookingSystemTypeColumn
        {
            get { return Schema.Columns[30]; }
        }
        
        
        
        public static TableSchema.TableColumn AdvanceReservationDaysColumn
        {
            get { return Schema.Columns[31]; }
        }
        
        
        
        public static TableSchema.TableColumn CouponUsersColumn
        {
            get { return Schema.Columns[32]; }
        }
        
        
        
        public static TableSchema.TableColumn IsReserveLockColumn
        {
            get { return Schema.Columns[33]; }
        }
        
        
        
        public static TableSchema.TableColumn CustomTagColumn
        {
            get { return Schema.Columns[34]; }
        }
        
        
        
        public static TableSchema.TableColumn TmallRmbPriceColumn
        {
            get { return Schema.Columns[35]; }
        }
        
        
        
        public static TableSchema.TableColumn TmallRmbExchangeRateColumn
        {
            get { return Schema.Columns[36]; }
        }
        
        
        
        public static TableSchema.TableColumn ExchangePriceColumn
        {
            get { return Schema.Columns[37]; }
        }
        
        
        
        public static TableSchema.TableColumn IsQuantityMultiplierColumn
        {
            get { return Schema.Columns[38]; }
        }
        
        
        
        public static TableSchema.TableColumn IsAveragePriceColumn
        {
            get { return Schema.Columns[39]; }
        }
        
        
        
        public static TableSchema.TableColumn IsMergeCountColumn
        {
            get { return Schema.Columns[40]; }
        }
        
        
        
        public static TableSchema.TableColumn IsZeroActivityShowCouponColumn
        {
            get { return Schema.Columns[41]; }
        }
        
        
        
        public static TableSchema.TableColumn IsTravelDealColumn
        {
            get { return Schema.Columns[42]; }
        }
        
        
        
        public static TableSchema.TableColumn AncestorSequenceBusinessHourGuidColumn
        {
            get { return Schema.Columns[43]; }
        }
        
        
        
        public static TableSchema.TableColumn IsLongContractColumn
        {
            get { return Schema.Columns[44]; }
        }
        
        
        
        public static TableSchema.TableColumn Installment3monthsColumn
        {
            get { return Schema.Columns[45]; }
        }
        
        
        
        public static TableSchema.TableColumn Installment6monthsColumn
        {
            get { return Schema.Columns[46]; }
        }
        
        
        
        public static TableSchema.TableColumn Installment12monthsColumn
        {
            get { return Schema.Columns[47]; }
        }
        
        
        
        public static TableSchema.TableColumn DenyInstallmentColumn
        {
            get { return Schema.Columns[48]; }
        }
        
        
        
        public static TableSchema.TableColumn SaleMultipleBaseColumn
        {
            get { return Schema.Columns[49]; }
        }
        
        
        
        public static TableSchema.TableColumn ShipTypeColumn
        {
            get { return Schema.Columns[50]; }
        }
        
        
        
        public static TableSchema.TableColumn ShippingdateTypeColumn
        {
            get { return Schema.Columns[51]; }
        }
        
        
        
        public static TableSchema.TableColumn ShippingdateColumn
        {
            get { return Schema.Columns[52]; }
        }
        
        
        
        public static TableSchema.TableColumn ProductUseDateStartSetColumn
        {
            get { return Schema.Columns[53]; }
        }
        
        
        
        public static TableSchema.TableColumn ProductUseDateEndSetColumn
        {
            get { return Schema.Columns[54]; }
        }
        
        
        
        public static TableSchema.TableColumn PresentQuantityColumn
        {
            get { return Schema.Columns[55]; }
        }
        
        
        
        public static TableSchema.TableColumn DevelopeSalesIdColumn
        {
            get { return Schema.Columns[56]; }
        }
        
        
        
        public static TableSchema.TableColumn GroupCouponAppStyleColumn
        {
            get { return Schema.Columns[57]; }
        }
        
        
        
        public static TableSchema.TableColumn NewDealTypeColumn
        {
            get { return Schema.Columns[58]; }
        }
        
        
        
        public static TableSchema.TableColumn VerifyTypeColumn
        {
            get { return Schema.Columns[59]; }
        }
        
        
        
        public static TableSchema.TableColumn DiscountTypeColumn
        {
            get { return Schema.Columns[60]; }
        }
        
        
        
        public static TableSchema.TableColumn DiscountColumn
        {
            get { return Schema.Columns[61]; }
        }
        
        
        
        public static TableSchema.TableColumn IsExperienceColumn
        {
            get { return Schema.Columns[62]; }
        }
        
        
        
        public static TableSchema.TableColumn CompleteCopyColumn
        {
            get { return Schema.Columns[63]; }
        }
        
        
        
        public static TableSchema.TableColumn GroupCouponDealTypeColumn
        {
            get { return Schema.Columns[64]; }
        }
        
        
        
        public static TableSchema.TableColumn CategoryListColumn
        {
            get { return Schema.Columns[65]; }
        }
        
        
        
        public static TableSchema.TableColumn IsFreightsDealColumn
        {
            get { return Schema.Columns[66]; }
        }
        
        
        
        public static TableSchema.TableColumn ConsignmentColumn
        {
            get { return Schema.Columns[67]; }
        }
        
        
        
        public static TableSchema.TableColumn CouponSeparateDigitsColumn
        {
            get { return Schema.Columns[68]; }
        }
        
        
        
        public static TableSchema.TableColumn FreightsIdColumn
        {
            get { return Schema.Columns[69]; }
        }
        
        
        
        public static TableSchema.TableColumn OperationSalesIdColumn
        {
            get { return Schema.Columns[70]; }
        }
        
        
        
        public static TableSchema.TableColumn IsDepositCoffeeColumn
        {
            get { return Schema.Columns[71]; }
        }
        
        
        
        public static TableSchema.TableColumn IsPromotionDealColumn
        {
            get { return Schema.Columns[72]; }
        }
        
        
        
        public static TableSchema.TableColumn IsExhibitionDealColumn
        {
            get { return Schema.Columns[73]; }
        }
        
        
        
        public static TableSchema.TableColumn VerifyActionTypeColumn
        {
            get { return Schema.Columns[74]; }
        }
        
        
        
        public static TableSchema.TableColumn IsHouseNewVerColumn
        {
            get { return Schema.Columns[75]; }
        }
        
        
        
        public static TableSchema.TableColumn CchannelColumn
        {
            get { return Schema.Columns[76]; }
        }
        
        
        
        public static TableSchema.TableColumn CchannelLinkColumn
        {
            get { return Schema.Columns[77]; }
        }
        
        
        
        public static TableSchema.TableColumn IsExpiredDealColumn
        {
            get { return Schema.Columns[78]; }
        }
        
        
        
        public static TableSchema.TableColumn DealTypeDetailColumn
        {
            get { return Schema.Columns[79]; }
        }
        
        
        
        public static TableSchema.TableColumn IsGameColumn
        {
            get { return Schema.Columns[80]; }
        }
        
        public static TableSchema.TableColumn IsWmsColumn
        {
            get { return Schema.Columns[81]; }
        }
        
        
        
        public static TableSchema.TableColumn AgentChannelsColumn
        {
            get { return Schema.Columns[82]; }
        }
        
        
        
        public static TableSchema.TableColumn IsChannelGiftColumn
        {
            get { return Schema.Columns[83]; }
		}

		public static TableSchema.TableColumn IsTaishinChosenColumn
		{
			get { return Schema.Columns[84]; }
		}

		#endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string BusinessHourGuid = @"business_hour_guid";
			 public static string DealType = @"deal_type";
			 public static string SellerVerifyType = @"seller_verify_type";
			 public static string DealAccBusinessGroupId = @"deal_acc_business_group_id";
			 public static string DealEmpName = @"deal_emp_name";
			 public static string CreateId = @"create_id";
			 public static string CreateTime = @"create_time";
			 public static string ModifyId = @"modify_id";
			 public static string ModifyTime = @"modify_time";
			 public static string UniqueId = @"unique_id";
			 public static string CityList = @"city_list";
			 public static string DeliveryType = @"delivery_type";
			 public static string ShoppingCart = @"shopping_cart";
			 public static string ComboPackCount = @"combo_pack_count";
			 public static string AncestorBusinessHourGuid = @"ancestor_business_hour_guid";
			 public static string IsTravelcard = @"is_travelcard";
			 public static string EntrustSell = @"entrust_sell";
			 public static string TravelPlace = @"travel_place";
			 public static string MultipleBranch = @"multiple_branch";
			 public static string EmpNo = @"emp_no";
			 public static string QuantityMultiplier = @"quantity_multiplier";
			 public static string IsDailyRestriction = @"is_daily_restriction";
			 public static string IsContinuedSequence = @"is_continued_sequence";
			 public static string IsContinuedQuantity = @"is_continued_quantity";
			 public static string ContinuedQuantity = @"continued_quantity";
			 public static string ActivityUrl = @"activity_url";
			 public static string LabelTagList = @"label_tag_list";
			 public static string LabelIconList = @"label_icon_list";
			 public static string CouponCodeType = @"coupon_code_type";
			 public static string PinType = @"pin_type";
			 public static string BookingSystemType = @"booking_system_type";
			 public static string AdvanceReservationDays = @"advance_reservation_days";
			 public static string CouponUsers = @"coupon_users";
			 public static string IsReserveLock = @"is_reserve_lock";
			 public static string CustomTag = @"custom_tag";
			 public static string TmallRmbPrice = @"tmall_rmb_price";
			 public static string TmallRmbExchangeRate = @"tmall_rmb_exchange_rate";
			 public static string ExchangePrice = @"exchange_price";
			 public static string IsQuantityMultiplier = @"is_quantity_multiplier";
			 public static string IsAveragePrice = @"is_average_price";
			 public static string IsMergeCount = @"is_merge_count";
			 public static string IsZeroActivityShowCoupon = @"is_zero_activity_show_coupon";
			 public static string IsTravelDeal = @"is_travel_deal";
			 public static string AncestorSequenceBusinessHourGuid = @"ancestor_sequence_business_hour_guid";
			 public static string IsLongContract = @"is_long_contract";
			 public static string Installment3months = @"installment_3months";
			 public static string Installment6months = @"installment_6months";
			 public static string Installment12months = @"installment_12months";
			 public static string DenyInstallment = @"deny_installment";
			 public static string SaleMultipleBase = @"sale_multiple_base";
			 public static string ShipType = @"ship_type";
			 public static string ShippingdateType = @"shippingdate_type";
			 public static string Shippingdate = @"shippingdate";
			 public static string ProductUseDateStartSet = @"product_use_date_start_set";
			 public static string ProductUseDateEndSet = @"product_use_date_end_set";
			 public static string PresentQuantity = @"present_quantity";
			 public static string DevelopeSalesId = @"develope_sales_id";
			 public static string GroupCouponAppStyle = @"group_coupon_app_style";
			 public static string NewDealType = @"new_deal_type";
			 public static string VerifyType = @"verify_type";
			 public static string DiscountType = @"discount_type";
			 public static string Discount = @"discount";
			 public static string IsExperience = @"is_experience";
			 public static string CompleteCopy = @"complete_copy";
			 public static string GroupCouponDealType = @"group_coupon_deal_type";
			 public static string CategoryList = @"category_list";
			 public static string IsFreightsDeal = @"is_freights_deal";
			 public static string Consignment = @"consignment";
			 public static string CouponSeparateDigits = @"coupon_separate_digits";
			 public static string FreightsId = @"freights_id";
			 public static string OperationSalesId = @"operation_sales_id";
			 public static string IsDepositCoffee = @"is_deposit_coffee";
			 public static string IsPromotionDeal = @"is_promotion_deal";
			 public static string IsExhibitionDeal = @"is_exhibition_deal";
			 public static string VerifyActionType = @"verify_action_type";
			 public static string IsHouseNewVer = @"is_house_new_ver";
			 public static string Cchannel = @"cchannel";
			 public static string CchannelLink = @"cchannel_link";
			 public static string IsExpiredDeal = @"is_expired_deal";
			 public static string DealTypeDetail = @"deal_type_detail";
			 public static string IsGame = @"is_game";
			 public static string IsWMS = @"is_wms";
			 public static string AgentChannels = @"agent_channels";
			 public static string IsChannelGift = @"is_channel_gift";
			 public static string IsTaishinChosen = @"is_taishin_chosen";

		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

using System;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using SubSonic;

namespace LunchKingSite.DataOrm
{
	[Serializable]
	public partial class SmsLogCollection : RepositoryList<SmsLog, SmsLogCollection>
	{
			public SmsLogCollection() {}

			public SmsLogCollection Filter()
			{
					for (int i = this.Count - 1; i > -1; i--)
					{
							SmsLog o = this[i];
							foreach (SubSonic.Where w in this.wheres)
							{
									bool remove = false;
									System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
									if(pi.CanRead)
									{
											object val = pi.GetValue(o, null);
											switch(w.Comparison)
											{
													case SubSonic.Comparison.Equals:
															if(!val.Equals(w.ParameterValue))
															{
																	remove = true;
															}
															break;
											}
									}
									if(remove)
									{
											this.Remove(o);
											break;
									}
							}
					}
					return this;
			}
	}

	[Serializable]
	public partial class SmsLog : RepositoryRecord<SmsLog>, IRecordBase
	{
		#region .ctors and Default Settings
		public SmsLog()
		{
			SetSQLProps();
			InitSetDefaults();
			MarkNew();
		}

		private void InitSetDefaults() { SetDefaults(); }

		public SmsLog(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}

		protected static void SetSQLProps() { GetTableSchema(); }

		#endregion

		#region Schema and Query Accessor
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}

		private static void GetTableSchema()
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("sms_log", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);

				TableSchema.TableColumn colvarMessage = new TableSchema.TableColumn(schema);
				colvarMessage.ColumnName = "message";
				colvarMessage.DataType = DbType.String;
				colvarMessage.MaxLength = 500;
				colvarMessage.AutoIncrement = false;
				colvarMessage.IsNullable = true;
				colvarMessage.IsPrimaryKey = false;
				colvarMessage.IsForeignKey = false;
				colvarMessage.IsReadOnly = false;
				colvarMessage.DefaultSetting = @"";
				colvarMessage.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMessage);

				TableSchema.TableColumn colvarType = new TableSchema.TableColumn(schema);
				colvarType.ColumnName = "type";
				colvarType.DataType = DbType.Int32;
				colvarType.MaxLength = 0;
				colvarType.AutoIncrement = false;
				colvarType.IsNullable = true;
				colvarType.IsPrimaryKey = false;
				colvarType.IsForeignKey = false;
				colvarType.IsReadOnly = false;
				colvarType.DefaultSetting = @"";
				colvarType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarType);

				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = true;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);

				TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
				colvarCreateId.ColumnName = "create_id";
				colvarCreateId.DataType = DbType.AnsiString;
				colvarCreateId.MaxLength = 100;
				colvarCreateId.AutoIncrement = false;
				colvarCreateId.IsNullable = true;
				colvarCreateId.IsPrimaryKey = false;
				colvarCreateId.IsForeignKey = false;
				colvarCreateId.IsReadOnly = false;
				colvarCreateId.DefaultSetting = @"";
				colvarCreateId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateId);

				TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
				colvarStatus.ColumnName = "status";
				colvarStatus.DataType = DbType.Int32;
				colvarStatus.MaxLength = 0;
				colvarStatus.AutoIncrement = false;
				colvarStatus.IsNullable = true;
				colvarStatus.IsPrimaryKey = false;
				colvarStatus.IsForeignKey = false;
				colvarStatus.IsReadOnly = false;
				colvarStatus.DefaultSetting = @"";
				colvarStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStatus);

				TableSchema.TableColumn colvarMobile = new TableSchema.TableColumn(schema);
				colvarMobile.ColumnName = "mobile";
				colvarMobile.DataType = DbType.AnsiString;
				colvarMobile.MaxLength = 20;
				colvarMobile.AutoIncrement = false;
				colvarMobile.IsNullable = true;
				colvarMobile.IsPrimaryKey = false;
				colvarMobile.IsForeignKey = false;
				colvarMobile.IsReadOnly = false;
				colvarMobile.DefaultSetting = @"";
				colvarMobile.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMobile);

				TableSchema.TableColumn colvarBusinessHourGuid = new TableSchema.TableColumn(schema);
				colvarBusinessHourGuid.ColumnName = "business_hour_guid";
				colvarBusinessHourGuid.DataType = DbType.Guid;
				colvarBusinessHourGuid.MaxLength = 0;
				colvarBusinessHourGuid.AutoIncrement = false;
				colvarBusinessHourGuid.IsNullable = true;
				colvarBusinessHourGuid.IsPrimaryKey = false;
				colvarBusinessHourGuid.IsForeignKey = false;
				colvarBusinessHourGuid.IsReadOnly = false;
				colvarBusinessHourGuid.DefaultSetting = @"";
				colvarBusinessHourGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBusinessHourGuid);

				TableSchema.TableColumn colvarCouponId = new TableSchema.TableColumn(schema);
				colvarCouponId.ColumnName = "coupon_id";
				colvarCouponId.DataType = DbType.AnsiString;
				colvarCouponId.MaxLength = 50;
				colvarCouponId.AutoIncrement = false;
				colvarCouponId.IsNullable = true;
				colvarCouponId.IsPrimaryKey = false;
				colvarCouponId.IsForeignKey = false;
				colvarCouponId.IsReadOnly = false;
				colvarCouponId.DefaultSetting = @"";
				colvarCouponId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCouponId);

				TableSchema.TableColumn colvarProvider = new TableSchema.TableColumn(schema);
				colvarProvider.ColumnName = "provider";
				colvarProvider.DataType = DbType.Int32;
				colvarProvider.MaxLength = 0;
				colvarProvider.AutoIncrement = false;
				colvarProvider.IsNullable = true;
				colvarProvider.IsPrimaryKey = false;
				colvarProvider.IsForeignKey = false;
				colvarProvider.IsReadOnly = false;
				colvarProvider.DefaultSetting = @"((-1))";
				colvarProvider.ForeignKeyTableName = "";
				schema.Columns.Add(colvarProvider);

				TableSchema.TableColumn colvarSmsOrderId = new TableSchema.TableColumn(schema);
				colvarSmsOrderId.ColumnName = "sms_order_id";
				colvarSmsOrderId.DataType = DbType.String;
				colvarSmsOrderId.MaxLength = 50;
				colvarSmsOrderId.AutoIncrement = false;
				colvarSmsOrderId.IsNullable = true;
				colvarSmsOrderId.IsPrimaryKey = false;
				colvarSmsOrderId.IsForeignKey = false;
				colvarSmsOrderId.IsReadOnly = false;
				colvarSmsOrderId.DefaultSetting = @"";
				colvarSmsOrderId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSmsOrderId);

				TableSchema.TableColumn colvarSmsOrderCount = new TableSchema.TableColumn(schema);
				colvarSmsOrderCount.ColumnName = "sms_order_count";
				colvarSmsOrderCount.DataType = DbType.Int32;
				colvarSmsOrderCount.MaxLength = 0;
				colvarSmsOrderCount.AutoIncrement = false;
				colvarSmsOrderCount.IsNullable = false;
				colvarSmsOrderCount.IsPrimaryKey = false;
				colvarSmsOrderCount.IsForeignKey = false;
				colvarSmsOrderCount.IsReadOnly = false;
				colvarSmsOrderCount.DefaultSetting = @"";
				colvarSmsOrderCount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSmsOrderCount);

				TableSchema.TableColumn colvarTaskId = new TableSchema.TableColumn(schema);
				colvarTaskId.ColumnName = "task_id";
				colvarTaskId.DataType = DbType.Guid;
				colvarTaskId.MaxLength = 0;
				colvarTaskId.AutoIncrement = false;
				colvarTaskId.IsNullable = false;
				colvarTaskId.IsPrimaryKey = false;
				colvarTaskId.IsForeignKey = false;
				colvarTaskId.IsReadOnly = false;
				colvarTaskId.DefaultSetting = @"";
				colvarTaskId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTaskId);

				TableSchema.TableColumn colvarFailReason = new TableSchema.TableColumn(schema);
				colvarFailReason.ColumnName = "fail_reason";
				colvarFailReason.DataType = DbType.Int32;
				colvarFailReason.MaxLength = 0;
				colvarFailReason.AutoIncrement = false;
				colvarFailReason.IsNullable = false;
				colvarFailReason.IsPrimaryKey = false;
				colvarFailReason.IsForeignKey = false;
				colvarFailReason.IsReadOnly = false;
				colvarFailReason.DefaultSetting = @"";
				colvarFailReason.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFailReason);

				TableSchema.TableColumn colvarResultTime = new TableSchema.TableColumn(schema);
				colvarResultTime.ColumnName = "result_time";
				colvarResultTime.DataType = DbType.DateTime;
				colvarResultTime.MaxLength = 0;
				colvarResultTime.AutoIncrement = false;
				colvarResultTime.IsNullable = true;
				colvarResultTime.IsPrimaryKey = false;
				colvarResultTime.IsForeignKey = false;
				colvarResultTime.IsReadOnly = false;
				colvarResultTime.DefaultSetting = @"";
				colvarResultTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarResultTime);

				TableSchema.TableColumn colvarAcceptableStartHour = new TableSchema.TableColumn(schema);
				colvarAcceptableStartHour.ColumnName = "acceptable_start_hour";
				colvarAcceptableStartHour.DataType = DbType.Int32;
				colvarAcceptableStartHour.MaxLength = 0;
				colvarAcceptableStartHour.AutoIncrement = false;
				colvarAcceptableStartHour.IsNullable = false;
				colvarAcceptableStartHour.IsPrimaryKey = false;
				colvarAcceptableStartHour.IsForeignKey = false;
				colvarAcceptableStartHour.IsReadOnly = false;
				colvarAcceptableStartHour.DefaultSetting = @"((0))";
				colvarAcceptableStartHour.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAcceptableStartHour);

				TableSchema.TableColumn colvarAcceptableEndHour = new TableSchema.TableColumn(schema);
				colvarAcceptableEndHour.ColumnName = "acceptable_end_hour";
				colvarAcceptableEndHour.DataType = DbType.Int32;
				colvarAcceptableEndHour.MaxLength = 0;
				colvarAcceptableEndHour.AutoIncrement = false;
				colvarAcceptableEndHour.IsNullable = false;
				colvarAcceptableEndHour.IsPrimaryKey = false;
				colvarAcceptableEndHour.IsForeignKey = false;
				colvarAcceptableEndHour.IsReadOnly = false;
				colvarAcceptableEndHour.DefaultSetting = @"((24))";
				colvarAcceptableEndHour.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAcceptableEndHour);

				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("sms_log",schema);
			}
		}
		#endregion

		#region Props

		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}

		[XmlAttribute("Message")]
		[Bindable(true)]
		public string Message
		{
			get { return GetColumnValue<string>(Columns.Message); }
			set { SetColumnValue(Columns.Message, value); }
		}

		[XmlAttribute("Type")]
		[Bindable(true)]
		public int? Type
		{
			get { return GetColumnValue<int?>(Columns.Type); }
			set { SetColumnValue(Columns.Type, value); }
		}

		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime? CreateTime
		{
			get { return GetColumnValue<DateTime?>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}

		[XmlAttribute("CreateId")]
		[Bindable(true)]
		public string CreateId
		{
			get { return GetColumnValue<string>(Columns.CreateId); }
			set { SetColumnValue(Columns.CreateId, value); }
		}

		[XmlAttribute("Status")]
		[Bindable(true)]
		public int? Status
		{
			get { return GetColumnValue<int?>(Columns.Status); }
			set { SetColumnValue(Columns.Status, value); }
		}

		[XmlAttribute("Mobile")]
		[Bindable(true)]
		public string Mobile
		{
			get { return GetColumnValue<string>(Columns.Mobile); }
			set { SetColumnValue(Columns.Mobile, value); }
		}

		[XmlAttribute("BusinessHourGuid")]
		[Bindable(true)]
		public Guid? BusinessHourGuid
		{
			get { return GetColumnValue<Guid?>(Columns.BusinessHourGuid); }
			set { SetColumnValue(Columns.BusinessHourGuid, value); }
		}

		[XmlAttribute("CouponId")]
		[Bindable(true)]
		public string CouponId
		{
			get { return GetColumnValue<string>(Columns.CouponId); }
			set { SetColumnValue(Columns.CouponId, value); }
		}

		[XmlAttribute("Provider")]
		[Bindable(true)]
		public int? Provider
		{
			get { return GetColumnValue<int?>(Columns.Provider); }
			set { SetColumnValue(Columns.Provider, value); }
		}

		[XmlAttribute("SmsOrderId")]
		[Bindable(true)]
		public string SmsOrderId
		{
			get { return GetColumnValue<string>(Columns.SmsOrderId); }
			set { SetColumnValue(Columns.SmsOrderId, value); }
		}

		[XmlAttribute("SmsOrderCount")]
		[Bindable(true)]
		public int SmsOrderCount
		{
			get { return GetColumnValue<int>(Columns.SmsOrderCount); }
			set { SetColumnValue(Columns.SmsOrderCount, value); }
		}

		[XmlAttribute("TaskId")]
		[Bindable(true)]
		public Guid TaskId
		{
			get { return GetColumnValue<Guid>(Columns.TaskId); }
			set { SetColumnValue(Columns.TaskId, value); }
		}

		[XmlAttribute("FailReason")]
		[Bindable(true)]
		public int FailReason
		{
			get { return GetColumnValue<int>(Columns.FailReason); }
			set { SetColumnValue(Columns.FailReason, value); }
		}

		[XmlAttribute("ResultTime")]
		[Bindable(true)]
		public DateTime? ResultTime
		{
			get { return GetColumnValue<DateTime?>(Columns.ResultTime); }
			set { SetColumnValue(Columns.ResultTime, value); }
		}

		[XmlAttribute("AcceptableStartHour")]
		[Bindable(true)]
		public int AcceptableStartHour
		{
			get { return GetColumnValue<int>(Columns.AcceptableStartHour); }
			set { SetColumnValue(Columns.AcceptableStartHour, value); }
		}

		[XmlAttribute("AcceptableEndHour")]
		[Bindable(true)]
		public int AcceptableEndHour
		{
			get { return GetColumnValue<int>(Columns.AcceptableEndHour); }
			set { SetColumnValue(Columns.AcceptableEndHour, value); }
		}

		#endregion

		#region Typed Columns

		public static TableSchema.TableColumn IdColumn
		{
			get { return Schema.Columns[0]; }
		}

		public static TableSchema.TableColumn MessageColumn
		{
			get { return Schema.Columns[1]; }
		}

		public static TableSchema.TableColumn TypeColumn
		{
			get { return Schema.Columns[2]; }
		}

		public static TableSchema.TableColumn CreateTimeColumn
		{
			get { return Schema.Columns[3]; }
		}

		public static TableSchema.TableColumn CreateIdColumn
		{
			get { return Schema.Columns[4]; }
		}

		public static TableSchema.TableColumn StatusColumn
		{
			get { return Schema.Columns[5]; }
		}

		public static TableSchema.TableColumn MobileColumn
		{
			get { return Schema.Columns[6]; }
		}

		public static TableSchema.TableColumn BusinessHourGuidColumn
		{
			get { return Schema.Columns[7]; }
		}

		public static TableSchema.TableColumn CouponIdColumn
		{
			get { return Schema.Columns[8]; }
		}

		public static TableSchema.TableColumn ProviderColumn
		{
			get { return Schema.Columns[9]; }
		}

		public static TableSchema.TableColumn SmsOrderIdColumn
		{
			get { return Schema.Columns[10]; }
		}

		public static TableSchema.TableColumn SmsOrderCountColumn
		{
			get { return Schema.Columns[11]; }
		}

		public static TableSchema.TableColumn TaskIdColumn
		{
			get { return Schema.Columns[12]; }
		}

		public static TableSchema.TableColumn FailReasonColumn
		{
			get { return Schema.Columns[13]; }
		}

		public static TableSchema.TableColumn ResultTimeColumn
		{
			get { return Schema.Columns[14]; }
		}

		public static TableSchema.TableColumn AcceptableStartHourColumn
		{
			get { return Schema.Columns[15]; }
		}

		public static TableSchema.TableColumn AcceptableEndHourColumn
		{
			get { return Schema.Columns[16]; }
		}

		#endregion

		#region Columns Struct

		public struct Columns
		{
			public static string Id = @"id";
			public static string Message = @"message";
			public static string Type = @"type";
			public static string CreateTime = @"create_time";
			public static string CreateId = @"create_id";
			public static string Status = @"status";
			public static string Mobile = @"mobile";
			public static string BusinessHourGuid = @"business_hour_guid";
			public static string CouponId = @"coupon_id";
			public static string Provider = @"provider";
			public static string SmsOrderId = @"sms_order_id";
			public static string SmsOrderCount = @"sms_order_count";
			public static string TaskId = @"task_id";
			public static string FailReason = @"fail_reason";
			public static string ResultTime = @"result_time";
			public static string AcceptableStartHour = @"acceptable_start_hour";
			public static string AcceptableEndHour = @"acceptable_end_hour";
		}

		#endregion

	}
}

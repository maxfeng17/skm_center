using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the PcpIntro class.
	/// </summary>
    [Serializable]
	public partial class PcpIntroCollection : RepositoryList<PcpIntro, PcpIntroCollection>
	{	   
		public PcpIntroCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>PcpIntroCollection</returns>
		public PcpIntroCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                PcpIntro o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the pcp_intro table.
	/// </summary>
	[Serializable]
	public partial class PcpIntro : RepositoryRecord<PcpIntro>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public PcpIntro()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public PcpIntro(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("pcp_intro", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarIntroType = new TableSchema.TableColumn(schema);
				colvarIntroType.ColumnName = "intro_type";
				colvarIntroType.DataType = DbType.Byte;
				colvarIntroType.MaxLength = 0;
				colvarIntroType.AutoIncrement = false;
				colvarIntroType.IsNullable = false;
				colvarIntroType.IsPrimaryKey = false;
				colvarIntroType.IsForeignKey = false;
				colvarIntroType.IsReadOnly = false;
				
						colvarIntroType.DefaultSetting = @"((0))";
				colvarIntroType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIntroType);
				
				TableSchema.TableColumn colvarKeyValue = new TableSchema.TableColumn(schema);
				colvarKeyValue.ColumnName = "key_value";
				colvarKeyValue.DataType = DbType.Int32;
				colvarKeyValue.MaxLength = 0;
				colvarKeyValue.AutoIncrement = false;
				colvarKeyValue.IsNullable = false;
				colvarKeyValue.IsPrimaryKey = false;
				colvarKeyValue.IsForeignKey = false;
				colvarKeyValue.IsReadOnly = false;
				colvarKeyValue.DefaultSetting = @"";
				colvarKeyValue.ForeignKeyTableName = "";
				schema.Columns.Add(colvarKeyValue);
				
				TableSchema.TableColumn colvarIntroContent = new TableSchema.TableColumn(schema);
				colvarIntroContent.ColumnName = "intro_content";
				colvarIntroContent.DataType = DbType.String;
				colvarIntroContent.MaxLength = -1;
				colvarIntroContent.AutoIncrement = false;
				colvarIntroContent.IsNullable = false;
				colvarIntroContent.IsPrimaryKey = false;
				colvarIntroContent.IsForeignKey = false;
				colvarIntroContent.IsReadOnly = false;
				
						colvarIntroContent.DefaultSetting = @"('')";
				colvarIntroContent.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIntroContent);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				
						colvarCreateTime.DefaultSetting = @"(getdate())";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarGroupId = new TableSchema.TableColumn(schema);
				colvarGroupId.ColumnName = "group_id";
				colvarGroupId.DataType = DbType.Int32;
				colvarGroupId.MaxLength = 0;
				colvarGroupId.AutoIncrement = false;
				colvarGroupId.IsNullable = false;
				colvarGroupId.IsPrimaryKey = false;
				colvarGroupId.IsForeignKey = false;
				colvarGroupId.IsReadOnly = false;
				
						colvarGroupId.DefaultSetting = @"((0))";
				colvarGroupId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGroupId);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("pcp_intro",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("IntroType")]
		[Bindable(true)]
		public byte IntroType 
		{
			get { return GetColumnValue<byte>(Columns.IntroType); }
			set { SetColumnValue(Columns.IntroType, value); }
		}
		  
		[XmlAttribute("KeyValue")]
		[Bindable(true)]
		public int KeyValue 
		{
			get { return GetColumnValue<int>(Columns.KeyValue); }
			set { SetColumnValue(Columns.KeyValue, value); }
		}
		  
		[XmlAttribute("IntroContent")]
		[Bindable(true)]
		public string IntroContent 
		{
			get { return GetColumnValue<string>(Columns.IntroContent); }
			set { SetColumnValue(Columns.IntroContent, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		  
		[XmlAttribute("GroupId")]
		[Bindable(true)]
		public int GroupId 
		{
			get { return GetColumnValue<int>(Columns.GroupId); }
			set { SetColumnValue(Columns.GroupId, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn IntroTypeColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn KeyValueColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn IntroContentColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn GroupIdColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string IntroType = @"intro_type";
			 public static string KeyValue = @"key_value";
			 public static string IntroContent = @"intro_content";
			 public static string CreateTime = @"create_time";
			 public static string GroupId = @"group_id";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

using System;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using SubSonic;

namespace LunchKingSite.DataOrm
{
	[Serializable]
	public partial class OrderCollection : RepositoryList<Order, OrderCollection>
	{
			public OrderCollection() {}

			public OrderCollection Filter()
			{
					for (int i = this.Count - 1; i > -1; i--)
					{
							Order o = this[i];
							foreach (SubSonic.Where w in this.wheres)
							{
									bool remove = false;
									System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
									if(pi.CanRead)
									{
											object val = pi.GetValue(o, null);
											switch(w.Comparison)
											{
													case SubSonic.Comparison.Equals:
															if(!val.Equals(w.ParameterValue))
															{
																	remove = true;
															}
															break;
											}
									}
									if(remove)
									{
											this.Remove(o);
											break;
									}
							}
					}
					return this;
			}
	}

	[Serializable]
	public partial class Order : RepositoryRecord<Order>, IRecordBase
	{
		#region .ctors and Default Settings
		public Order()
		{
			SetSQLProps();
			InitSetDefaults();
			MarkNew();
		}

		private void InitSetDefaults() { SetDefaults(); }

		public Order(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}

		protected static void SetSQLProps() { GetTableSchema(); }

		#endregion

		#region Schema and Query Accessor
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}

		private static void GetTableSchema()
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("order", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				TableSchema.TableColumn colvarGuid = new TableSchema.TableColumn(schema);
				colvarGuid.ColumnName = "GUID";
				colvarGuid.DataType = DbType.Guid;
				colvarGuid.MaxLength = 0;
				colvarGuid.AutoIncrement = false;
				colvarGuid.IsNullable = false;
				colvarGuid.IsPrimaryKey = true;
				colvarGuid.IsForeignKey = false;
				colvarGuid.IsReadOnly = false;
				colvarGuid.DefaultSetting = @"";
				colvarGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGuid);

				TableSchema.TableColumn colvarOrderId = new TableSchema.TableColumn(schema);
				colvarOrderId.ColumnName = "order_id";
				colvarOrderId.DataType = DbType.AnsiString;
				colvarOrderId.MaxLength = 30;
				colvarOrderId.AutoIncrement = false;
				colvarOrderId.IsNullable = true;
				colvarOrderId.IsPrimaryKey = false;
				colvarOrderId.IsForeignKey = false;
				colvarOrderId.IsReadOnly = false;
				colvarOrderId.DefaultSetting = @"";
				colvarOrderId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderId);

				TableSchema.TableColumn colvarSellerGuid = new TableSchema.TableColumn(schema);
				colvarSellerGuid.ColumnName = "seller_GUID";
				colvarSellerGuid.DataType = DbType.Guid;
				colvarSellerGuid.MaxLength = 0;
				colvarSellerGuid.AutoIncrement = false;
				colvarSellerGuid.IsNullable = false;
				colvarSellerGuid.IsPrimaryKey = false;
				colvarSellerGuid.IsForeignKey = false;
				colvarSellerGuid.IsReadOnly = false;
				colvarSellerGuid.DefaultSetting = @"";
				colvarSellerGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSellerGuid);

				TableSchema.TableColumn colvarSellerName = new TableSchema.TableColumn(schema);
				colvarSellerName.ColumnName = "seller_name";
				colvarSellerName.DataType = DbType.String;
				colvarSellerName.MaxLength = 50;
				colvarSellerName.AutoIncrement = false;
				colvarSellerName.IsNullable = false;
				colvarSellerName.IsPrimaryKey = false;
				colvarSellerName.IsForeignKey = false;
				colvarSellerName.IsReadOnly = false;
				colvarSellerName.DefaultSetting = @"";
				colvarSellerName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSellerName);

				TableSchema.TableColumn colvarMemberName = new TableSchema.TableColumn(schema);
				colvarMemberName.ColumnName = "member_name";
				colvarMemberName.DataType = DbType.String;
				colvarMemberName.MaxLength = 50;
				colvarMemberName.AutoIncrement = false;
				colvarMemberName.IsNullable = false;
				colvarMemberName.IsPrimaryKey = false;
				colvarMemberName.IsForeignKey = false;
				colvarMemberName.IsReadOnly = false;
				colvarMemberName.DefaultSetting = @"";
				colvarMemberName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMemberName);

				TableSchema.TableColumn colvarPhoneNumber = new TableSchema.TableColumn(schema);
				colvarPhoneNumber.ColumnName = "phone_number";
				colvarPhoneNumber.DataType = DbType.AnsiString;
				colvarPhoneNumber.MaxLength = 50;
				colvarPhoneNumber.AutoIncrement = false;
				colvarPhoneNumber.IsNullable = true;
				colvarPhoneNumber.IsPrimaryKey = false;
				colvarPhoneNumber.IsForeignKey = false;
				colvarPhoneNumber.IsReadOnly = false;
				colvarPhoneNumber.DefaultSetting = @"";
				colvarPhoneNumber.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPhoneNumber);

				TableSchema.TableColumn colvarMobileNumber = new TableSchema.TableColumn(schema);
				colvarMobileNumber.ColumnName = "mobile_number";
				colvarMobileNumber.DataType = DbType.AnsiString;
				colvarMobileNumber.MaxLength = 50;
				colvarMobileNumber.AutoIncrement = false;
				colvarMobileNumber.IsNullable = true;
				colvarMobileNumber.IsPrimaryKey = false;
				colvarMobileNumber.IsForeignKey = false;
				colvarMobileNumber.IsReadOnly = false;
				colvarMobileNumber.DefaultSetting = @"";
				colvarMobileNumber.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMobileNumber);

				TableSchema.TableColumn colvarDeliveryAddress = new TableSchema.TableColumn(schema);
				colvarDeliveryAddress.ColumnName = "delivery_address";
				colvarDeliveryAddress.DataType = DbType.String;
				colvarDeliveryAddress.MaxLength = 200;
				colvarDeliveryAddress.AutoIncrement = false;
				colvarDeliveryAddress.IsNullable = true;
				colvarDeliveryAddress.IsPrimaryKey = false;
				colvarDeliveryAddress.IsForeignKey = false;
				colvarDeliveryAddress.IsReadOnly = false;
				colvarDeliveryAddress.DefaultSetting = @"";
				colvarDeliveryAddress.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDeliveryAddress);

				TableSchema.TableColumn colvarDeliveryTime = new TableSchema.TableColumn(schema);
				colvarDeliveryTime.ColumnName = "delivery_time";
				colvarDeliveryTime.DataType = DbType.DateTime;
				colvarDeliveryTime.MaxLength = 0;
				colvarDeliveryTime.AutoIncrement = false;
				colvarDeliveryTime.IsNullable = true;
				colvarDeliveryTime.IsPrimaryKey = false;
				colvarDeliveryTime.IsForeignKey = false;
				colvarDeliveryTime.IsReadOnly = false;
				colvarDeliveryTime.DefaultSetting = @"";
				colvarDeliveryTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDeliveryTime);

				TableSchema.TableColumn colvarSubtotal = new TableSchema.TableColumn(schema);
				colvarSubtotal.ColumnName = "subtotal";
				colvarSubtotal.DataType = DbType.Currency;
				colvarSubtotal.MaxLength = 0;
				colvarSubtotal.AutoIncrement = false;
				colvarSubtotal.IsNullable = false;
				colvarSubtotal.IsPrimaryKey = false;
				colvarSubtotal.IsForeignKey = false;
				colvarSubtotal.IsReadOnly = false;
				colvarSubtotal.DefaultSetting = @"";
				colvarSubtotal.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSubtotal);

				TableSchema.TableColumn colvarTotal = new TableSchema.TableColumn(schema);
				colvarTotal.ColumnName = "total";
				colvarTotal.DataType = DbType.Currency;
				colvarTotal.MaxLength = 0;
				colvarTotal.AutoIncrement = false;
				colvarTotal.IsNullable = false;
				colvarTotal.IsPrimaryKey = false;
				colvarTotal.IsForeignKey = false;
				colvarTotal.IsReadOnly = false;
				colvarTotal.DefaultSetting = @"";
				colvarTotal.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTotal);

				TableSchema.TableColumn colvarUserMemo = new TableSchema.TableColumn(schema);
				colvarUserMemo.ColumnName = "user_memo";
				colvarUserMemo.DataType = DbType.String;
				colvarUserMemo.MaxLength = 1073741823;
				colvarUserMemo.AutoIncrement = false;
				colvarUserMemo.IsNullable = true;
				colvarUserMemo.IsPrimaryKey = false;
				colvarUserMemo.IsForeignKey = false;
				colvarUserMemo.IsReadOnly = false;
				colvarUserMemo.DefaultSetting = @"";
				colvarUserMemo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUserMemo);

				TableSchema.TableColumn colvarOrderMemo = new TableSchema.TableColumn(schema);
				colvarOrderMemo.ColumnName = "order_memo";
				colvarOrderMemo.DataType = DbType.String;
				colvarOrderMemo.MaxLength = 1073741823;
				colvarOrderMemo.AutoIncrement = false;
				colvarOrderMemo.IsNullable = true;
				colvarOrderMemo.IsPrimaryKey = false;
				colvarOrderMemo.IsForeignKey = false;
				colvarOrderMemo.IsReadOnly = false;
				colvarOrderMemo.DefaultSetting = @"";
				colvarOrderMemo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderMemo);

				TableSchema.TableColumn colvarOrderStatus = new TableSchema.TableColumn(schema);
				colvarOrderStatus.ColumnName = "order_status";
				colvarOrderStatus.DataType = DbType.Int32;
				colvarOrderStatus.MaxLength = 0;
				colvarOrderStatus.AutoIncrement = false;
				colvarOrderStatus.IsNullable = false;
				colvarOrderStatus.IsPrimaryKey = false;
				colvarOrderStatus.IsForeignKey = false;
				colvarOrderStatus.IsReadOnly = false;
				colvarOrderStatus.DefaultSetting = @"";
				colvarOrderStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderStatus);

				TableSchema.TableColumn colvarOrderStage = new TableSchema.TableColumn(schema);
				colvarOrderStage.ColumnName = "order_stage";
				colvarOrderStage.DataType = DbType.Int32;
				colvarOrderStage.MaxLength = 0;
				colvarOrderStage.AutoIncrement = false;
				colvarOrderStage.IsNullable = false;
				colvarOrderStage.IsPrimaryKey = false;
				colvarOrderStage.IsForeignKey = false;
				colvarOrderStage.IsReadOnly = false;
				colvarOrderStage.DefaultSetting = @"((0))";
				colvarOrderStage.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderStage);

				TableSchema.TableColumn colvarBonusPoint = new TableSchema.TableColumn(schema);
				colvarBonusPoint.ColumnName = "bonus_point";
				colvarBonusPoint.DataType = DbType.AnsiString;
				colvarBonusPoint.MaxLength = 10;
				colvarBonusPoint.AutoIncrement = false;
				colvarBonusPoint.IsNullable = true;
				colvarBonusPoint.IsPrimaryKey = false;
				colvarBonusPoint.IsForeignKey = false;
				colvarBonusPoint.IsReadOnly = false;
				colvarBonusPoint.DefaultSetting = @"";
				colvarBonusPoint.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBonusPoint);

				TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
				colvarCreateId.ColumnName = "create_id";
				colvarCreateId.DataType = DbType.String;
				colvarCreateId.MaxLength = 30;
				colvarCreateId.AutoIncrement = false;
				colvarCreateId.IsNullable = false;
				colvarCreateId.IsPrimaryKey = false;
				colvarCreateId.IsForeignKey = false;
				colvarCreateId.IsReadOnly = false;
				colvarCreateId.DefaultSetting = @"";
				colvarCreateId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateId);

				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);

				TableSchema.TableColumn colvarModifyId = new TableSchema.TableColumn(schema);
				colvarModifyId.ColumnName = "modify_id";
				colvarModifyId.DataType = DbType.String;
				colvarModifyId.MaxLength = 30;
				colvarModifyId.AutoIncrement = false;
				colvarModifyId.IsNullable = true;
				colvarModifyId.IsPrimaryKey = false;
				colvarModifyId.IsForeignKey = false;
				colvarModifyId.IsReadOnly = false;
				colvarModifyId.DefaultSetting = @"";
				colvarModifyId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyId);

				TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
				colvarModifyTime.ColumnName = "modify_time";
				colvarModifyTime.DataType = DbType.DateTime;
				colvarModifyTime.MaxLength = 0;
				colvarModifyTime.AutoIncrement = false;
				colvarModifyTime.IsNullable = true;
				colvarModifyTime.IsPrimaryKey = false;
				colvarModifyTime.IsForeignKey = false;
				colvarModifyTime.IsReadOnly = false;
				colvarModifyTime.DefaultSetting = @"";
				colvarModifyTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyTime);

				TableSchema.TableColumn colvarAccessLock = new TableSchema.TableColumn(schema);
				colvarAccessLock.ColumnName = "access_lock";
				colvarAccessLock.DataType = DbType.String;
				colvarAccessLock.MaxLength = 50;
				colvarAccessLock.AutoIncrement = false;
				colvarAccessLock.IsNullable = true;
				colvarAccessLock.IsPrimaryKey = false;
				colvarAccessLock.IsForeignKey = false;
				colvarAccessLock.IsReadOnly = false;
				colvarAccessLock.DefaultSetting = @"";
				colvarAccessLock.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAccessLock);

				TableSchema.TableColumn colvarCityId = new TableSchema.TableColumn(schema);
				colvarCityId.ColumnName = "city_id";
				colvarCityId.DataType = DbType.Int32;
				colvarCityId.MaxLength = 0;
				colvarCityId.AutoIncrement = false;
				colvarCityId.IsNullable = true;
				colvarCityId.IsPrimaryKey = false;
				colvarCityId.IsForeignKey = false;
				colvarCityId.IsReadOnly = false;
				colvarCityId.DefaultSetting = @"";
				colvarCityId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCityId);

				TableSchema.TableColumn colvarParentOrderId = new TableSchema.TableColumn(schema);
				colvarParentOrderId.ColumnName = "parent_order_id";
				colvarParentOrderId.DataType = DbType.Guid;
				colvarParentOrderId.MaxLength = 0;
				colvarParentOrderId.AutoIncrement = false;
				colvarParentOrderId.IsNullable = true;
				colvarParentOrderId.IsPrimaryKey = false;
				colvarParentOrderId.IsForeignKey = false;
				colvarParentOrderId.IsReadOnly = false;
				colvarParentOrderId.DefaultSetting = @"";
				colvarParentOrderId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarParentOrderId);

				TableSchema.TableColumn colvarInvoiceNo = new TableSchema.TableColumn(schema);
				colvarInvoiceNo.ColumnName = "invoice_no";
				colvarInvoiceNo.DataType = DbType.String;
				colvarInvoiceNo.MaxLength = 10;
				colvarInvoiceNo.AutoIncrement = false;
				colvarInvoiceNo.IsNullable = true;
				colvarInvoiceNo.IsPrimaryKey = false;
				colvarInvoiceNo.IsForeignKey = false;
				colvarInvoiceNo.IsReadOnly = false;
				colvarInvoiceNo.DefaultSetting = @"";
				colvarInvoiceNo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarInvoiceNo);

				TableSchema.TableColumn colvarReturnPaper = new TableSchema.TableColumn(schema);
				colvarReturnPaper.ColumnName = "return_paper";
				colvarReturnPaper.DataType = DbType.Boolean;
				colvarReturnPaper.MaxLength = 0;
				colvarReturnPaper.AutoIncrement = false;
				colvarReturnPaper.IsNullable = false;
				colvarReturnPaper.IsPrimaryKey = false;
				colvarReturnPaper.IsForeignKey = false;
				colvarReturnPaper.IsReadOnly = false;
				colvarReturnPaper.DefaultSetting = @"((0))";
				colvarReturnPaper.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReturnPaper);

				TableSchema.TableColumn colvarExportLog = new TableSchema.TableColumn(schema);
				colvarExportLog.ColumnName = "export_log";
				colvarExportLog.DataType = DbType.String;
				colvarExportLog.MaxLength = 2147483647;
				colvarExportLog.AutoIncrement = false;
				colvarExportLog.IsNullable = true;
				colvarExportLog.IsPrimaryKey = false;
				colvarExportLog.IsForeignKey = false;
				colvarExportLog.IsReadOnly = false;
				colvarExportLog.DefaultSetting = @"";
				colvarExportLog.ForeignKeyTableName = "";
				schema.Columns.Add(colvarExportLog);

				TableSchema.TableColumn colvarPaidByTravelcard = new TableSchema.TableColumn(schema);
				colvarPaidByTravelcard.ColumnName = "paid_by_travelcard";
				colvarPaidByTravelcard.DataType = DbType.Boolean;
				colvarPaidByTravelcard.MaxLength = 0;
				colvarPaidByTravelcard.AutoIncrement = false;
				colvarPaidByTravelcard.IsNullable = false;
				colvarPaidByTravelcard.IsPrimaryKey = false;
				colvarPaidByTravelcard.IsForeignKey = false;
				colvarPaidByTravelcard.IsReadOnly = false;
				colvarPaidByTravelcard.DefaultSetting = @"((0))";
				colvarPaidByTravelcard.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPaidByTravelcard);

				TableSchema.TableColumn colvarOrderFromType = new TableSchema.TableColumn(schema);
				colvarOrderFromType.ColumnName = "order_from_type";
				colvarOrderFromType.DataType = DbType.Int32;
				colvarOrderFromType.MaxLength = 0;
				colvarOrderFromType.AutoIncrement = false;
				colvarOrderFromType.IsNullable = false;
				colvarOrderFromType.IsPrimaryKey = false;
				colvarOrderFromType.IsForeignKey = false;
				colvarOrderFromType.IsReadOnly = false;
				colvarOrderFromType.DefaultSetting = @"((1))";
				colvarOrderFromType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderFromType);

				TableSchema.TableColumn colvarUserId = new TableSchema.TableColumn(schema);
				colvarUserId.ColumnName = "user_id";
				colvarUserId.DataType = DbType.Int32;
				colvarUserId.MaxLength = 0;
				colvarUserId.AutoIncrement = false;
				colvarUserId.IsNullable = false;
				colvarUserId.IsPrimaryKey = false;
				colvarUserId.IsForeignKey = false;
				colvarUserId.IsReadOnly = false;
				colvarUserId.DefaultSetting = @"((0))";
				colvarUserId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUserId);

				TableSchema.TableColumn colvarIsPartialCancel = new TableSchema.TableColumn(schema);
				colvarIsPartialCancel.ColumnName = "is_partial_cancel";
				colvarIsPartialCancel.DataType = DbType.Boolean;
				colvarIsPartialCancel.MaxLength = 0;
				colvarIsPartialCancel.AutoIncrement = false;
				colvarIsPartialCancel.IsNullable = false;
				colvarIsPartialCancel.IsPrimaryKey = false;
				colvarIsPartialCancel.IsForeignKey = false;
				colvarIsPartialCancel.IsReadOnly = false;
				colvarIsPartialCancel.DefaultSetting = @"";
				colvarIsPartialCancel.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsPartialCancel);

				TableSchema.TableColumn colvarIsCanceling = new TableSchema.TableColumn(schema);
				colvarIsCanceling.ColumnName = "is_canceling";
				colvarIsCanceling.DataType = DbType.Boolean;
				colvarIsCanceling.MaxLength = 0;
				colvarIsCanceling.AutoIncrement = false;
				colvarIsCanceling.IsNullable = false;
				colvarIsCanceling.IsPrimaryKey = false;
				colvarIsCanceling.IsForeignKey = false;
				colvarIsCanceling.IsReadOnly = false;
				colvarIsCanceling.DefaultSetting = @"";
				colvarIsCanceling.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsCanceling);

				TableSchema.TableColumn colvarSerialNo = new TableSchema.TableColumn(schema);
				colvarSerialNo.ColumnName = "serial_no";
				colvarSerialNo.DataType = DbType.Int32;
				colvarSerialNo.MaxLength = 0;
				colvarSerialNo.AutoIncrement = true;
				colvarSerialNo.IsNullable = false;
				colvarSerialNo.IsPrimaryKey = false;
				colvarSerialNo.IsForeignKey = false;
				colvarSerialNo.IsReadOnly = false;
				colvarSerialNo.DefaultSetting = @"";
				colvarSerialNo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSerialNo);

				TableSchema.TableColumn colvarGuestBuy = new TableSchema.TableColumn(schema);
				colvarGuestBuy.ColumnName = "guest_buy";
				colvarGuestBuy.DataType = DbType.Int32;
				colvarGuestBuy.MaxLength = 0;
				colvarGuestBuy.AutoIncrement = false;
				colvarGuestBuy.IsNullable = false;
				colvarGuestBuy.IsPrimaryKey = false;
				colvarGuestBuy.IsForeignKey = false;
				colvarGuestBuy.IsReadOnly = false;
				colvarGuestBuy.DefaultSetting = @"";
				colvarGuestBuy.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGuestBuy);

				TableSchema.TableColumn colvarInstallment = new TableSchema.TableColumn(schema);
				colvarInstallment.ColumnName = "installment";
				colvarInstallment.DataType = DbType.Int32;
				colvarInstallment.MaxLength = 0;
				colvarInstallment.AutoIncrement = false;
				colvarInstallment.IsNullable = false;
				colvarInstallment.IsPrimaryKey = false;
				colvarInstallment.IsForeignKey = false;
				colvarInstallment.IsReadOnly = false;
				colvarInstallment.DefaultSetting = @"((0))";
				colvarInstallment.ForeignKeyTableName = "";
				schema.Columns.Add(colvarInstallment);

				TableSchema.TableColumn colvarOrderSettlementTime = new TableSchema.TableColumn(schema);
				colvarOrderSettlementTime.ColumnName = "order_settlement_time";
				colvarOrderSettlementTime.DataType = DbType.DateTime;
				colvarOrderSettlementTime.MaxLength = 0;
				colvarOrderSettlementTime.AutoIncrement = false;
				colvarOrderSettlementTime.IsNullable = true;
				colvarOrderSettlementTime.IsPrimaryKey = false;
				colvarOrderSettlementTime.IsForeignKey = false;
				colvarOrderSettlementTime.IsReadOnly = false;
				colvarOrderSettlementTime.DefaultSetting = @"";
				colvarOrderSettlementTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderSettlementTime);

				TableSchema.TableColumn colvarCartDGuid = new TableSchema.TableColumn(schema);
				colvarCartDGuid.ColumnName = "cart_d_guid";
				colvarCartDGuid.DataType = DbType.Guid;
				colvarCartDGuid.MaxLength = 0;
				colvarCartDGuid.AutoIncrement = false;
				colvarCartDGuid.IsNullable = true;
				colvarCartDGuid.IsPrimaryKey = false;
				colvarCartDGuid.IsForeignKey = false;
				colvarCartDGuid.IsReadOnly = false;
				colvarCartDGuid.DefaultSetting = @"";
				colvarCartDGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCartDGuid);

				TableSchema.TableColumn colvarCartGuid = new TableSchema.TableColumn(schema);
				colvarCartGuid.ColumnName = "cart_guid";
				colvarCartGuid.DataType = DbType.Guid;
				colvarCartGuid.MaxLength = 0;
				colvarCartGuid.AutoIncrement = false;
				colvarCartGuid.IsNullable = true;
				colvarCartGuid.IsPrimaryKey = false;
				colvarCartGuid.IsForeignKey = false;
				colvarCartGuid.IsReadOnly = false;
				colvarCartGuid.DefaultSetting = @"";
				colvarCartGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCartGuid);

				TableSchema.TableColumn colvarIsFreightsOrder = new TableSchema.TableColumn(schema);
				colvarIsFreightsOrder.ColumnName = "is_freights_order";
				colvarIsFreightsOrder.DataType = DbType.Boolean;
				colvarIsFreightsOrder.MaxLength = 0;
				colvarIsFreightsOrder.AutoIncrement = false;
				colvarIsFreightsOrder.IsNullable = true;
				colvarIsFreightsOrder.IsPrimaryKey = false;
				colvarIsFreightsOrder.IsForeignKey = false;
				colvarIsFreightsOrder.IsReadOnly = false;
				colvarIsFreightsOrder.DefaultSetting = @"";
				colvarIsFreightsOrder.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsFreightsOrder);

				TableSchema.TableColumn colvarMobilePayType = new TableSchema.TableColumn(schema);
				colvarMobilePayType.ColumnName = "mobile_pay_type";
				colvarMobilePayType.DataType = DbType.Int32;
				colvarMobilePayType.MaxLength = 0;
				colvarMobilePayType.AutoIncrement = false;
				colvarMobilePayType.IsNullable = true;
				colvarMobilePayType.IsPrimaryKey = false;
				colvarMobilePayType.IsForeignKey = false;
				colvarMobilePayType.IsReadOnly = false;
				colvarMobilePayType.DefaultSetting = @"";
				colvarMobilePayType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMobilePayType);

				TableSchema.TableColumn colvarTurnover = new TableSchema.TableColumn(schema);
				colvarTurnover.ColumnName = "turnover";
				colvarTurnover.DataType = DbType.Int32;
				colvarTurnover.MaxLength = 0;
				colvarTurnover.AutoIncrement = false;
				colvarTurnover.IsNullable = false;
				colvarTurnover.IsPrimaryKey = false;
				colvarTurnover.IsForeignKey = false;
				colvarTurnover.IsReadOnly = false;
				colvarTurnover.DefaultSetting = @"((0))";
				colvarTurnover.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTurnover);

				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("order",schema);
			}
		}
		#endregion

		#region Props

		[XmlAttribute("Guid")]
		[Bindable(true)]
		public Guid Guid
		{
			get { return GetColumnValue<Guid>(Columns.Guid); }
			set { SetColumnValue(Columns.Guid, value); }
		}

		[XmlAttribute("OrderId")]
		[Bindable(true)]
		public string OrderId
		{
			get { return GetColumnValue<string>(Columns.OrderId); }
			set { SetColumnValue(Columns.OrderId, value); }
		}

		[XmlAttribute("SellerGuid")]
		[Bindable(true)]
		public Guid SellerGuid
		{
			get { return GetColumnValue<Guid>(Columns.SellerGuid); }
			set { SetColumnValue(Columns.SellerGuid, value); }
		}

		[XmlAttribute("SellerName")]
		[Bindable(true)]
		public string SellerName
		{
			get { return GetColumnValue<string>(Columns.SellerName); }
			set { SetColumnValue(Columns.SellerName, value); }
		}

		[XmlAttribute("MemberName")]
		[Bindable(true)]
		public string MemberName
		{
			get { return GetColumnValue<string>(Columns.MemberName); }
			set { SetColumnValue(Columns.MemberName, value); }
		}

		[XmlAttribute("PhoneNumber")]
		[Bindable(true)]
		public string PhoneNumber
		{
			get { return GetColumnValue<string>(Columns.PhoneNumber); }
			set { SetColumnValue(Columns.PhoneNumber, value); }
		}

		[XmlAttribute("MobileNumber")]
		[Bindable(true)]
		public string MobileNumber
		{
			get { return GetColumnValue<string>(Columns.MobileNumber); }
			set { SetColumnValue(Columns.MobileNumber, value); }
		}

		[XmlAttribute("DeliveryAddress")]
		[Bindable(true)]
		public string DeliveryAddress
		{
			get { return GetColumnValue<string>(Columns.DeliveryAddress); }
			set { SetColumnValue(Columns.DeliveryAddress, value); }
		}

		[XmlAttribute("DeliveryTime")]
		[Bindable(true)]
		public DateTime? DeliveryTime
		{
			get { return GetColumnValue<DateTime?>(Columns.DeliveryTime); }
			set { SetColumnValue(Columns.DeliveryTime, value); }
		}

		[XmlAttribute("Subtotal")]
		[Bindable(true)]
		public decimal Subtotal
		{
			get { return GetColumnValue<decimal>(Columns.Subtotal); }
			set { SetColumnValue(Columns.Subtotal, value); }
		}

		[XmlAttribute("Total")]
		[Bindable(true)]
		public decimal Total
		{
			get { return GetColumnValue<decimal>(Columns.Total); }
			set { SetColumnValue(Columns.Total, value); }
		}

		[XmlAttribute("UserMemo")]
		[Bindable(true)]
		public string UserMemo
		{
			get { return GetColumnValue<string>(Columns.UserMemo); }
			set { SetColumnValue(Columns.UserMemo, value); }
		}

		[XmlAttribute("OrderMemo")]
		[Bindable(true)]
		public string OrderMemo
		{
			get { return GetColumnValue<string>(Columns.OrderMemo); }
			set { SetColumnValue(Columns.OrderMemo, value); }
		}

		[XmlAttribute("OrderStatus")]
		[Bindable(true)]
		public int OrderStatus
		{
			get { return GetColumnValue<int>(Columns.OrderStatus); }
			set { SetColumnValue(Columns.OrderStatus, value); }
		}

		[XmlAttribute("OrderStage")]
		[Bindable(true)]
		public int OrderStage
		{
			get { return GetColumnValue<int>(Columns.OrderStage); }
			set { SetColumnValue(Columns.OrderStage, value); }
		}

		[XmlAttribute("BonusPoint")]
		[Bindable(true)]
		public string BonusPoint
		{
			get { return GetColumnValue<string>(Columns.BonusPoint); }
			set { SetColumnValue(Columns.BonusPoint, value); }
		}

		[XmlAttribute("CreateId")]
		[Bindable(true)]
		public string CreateId
		{
			get { return GetColumnValue<string>(Columns.CreateId); }
			set { SetColumnValue(Columns.CreateId, value); }
		}

		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}

		[XmlAttribute("ModifyId")]
		[Bindable(true)]
		public string ModifyId
		{
			get { return GetColumnValue<string>(Columns.ModifyId); }
			set { SetColumnValue(Columns.ModifyId, value); }
		}

		[XmlAttribute("ModifyTime")]
		[Bindable(true)]
		public DateTime? ModifyTime
		{
			get { return GetColumnValue<DateTime?>(Columns.ModifyTime); }
			set { SetColumnValue(Columns.ModifyTime, value); }
		}

		[XmlAttribute("AccessLock")]
		[Bindable(true)]
		public string AccessLock
		{
			get { return GetColumnValue<string>(Columns.AccessLock); }
			set { SetColumnValue(Columns.AccessLock, value); }
		}

		[XmlAttribute("CityId")]
		[Bindable(true)]
		public int? CityId
		{
			get { return GetColumnValue<int?>(Columns.CityId); }
			set { SetColumnValue(Columns.CityId, value); }
		}

		[XmlAttribute("ParentOrderId")]
		[Bindable(true)]
		public Guid? ParentOrderId
		{
			get { return GetColumnValue<Guid?>(Columns.ParentOrderId); }
			set { SetColumnValue(Columns.ParentOrderId, value); }
		}

		[XmlAttribute("InvoiceNo")]
		[Bindable(true)]
		public string InvoiceNo
		{
			get { return GetColumnValue<string>(Columns.InvoiceNo); }
			set { SetColumnValue(Columns.InvoiceNo, value); }
		}

		[XmlAttribute("ReturnPaper")]
		[Bindable(true)]
		public bool ReturnPaper
		{
			get { return GetColumnValue<bool>(Columns.ReturnPaper); }
			set { SetColumnValue(Columns.ReturnPaper, value); }
		}

		[XmlAttribute("ExportLog")]
		[Bindable(true)]
		public string ExportLog
		{
			get { return GetColumnValue<string>(Columns.ExportLog); }
			set { SetColumnValue(Columns.ExportLog, value); }
		}

		[XmlAttribute("PaidByTravelcard")]
		[Bindable(true)]
		public bool PaidByTravelcard
		{
			get { return GetColumnValue<bool>(Columns.PaidByTravelcard); }
			set { SetColumnValue(Columns.PaidByTravelcard, value); }
		}

		[XmlAttribute("OrderFromType")]
		[Bindable(true)]
		public int OrderFromType
		{
			get { return GetColumnValue<int>(Columns.OrderFromType); }
			set { SetColumnValue(Columns.OrderFromType, value); }
		}

		[XmlAttribute("UserId")]
		[Bindable(true)]
		public int UserId
		{
			get { return GetColumnValue<int>(Columns.UserId); }
			set { SetColumnValue(Columns.UserId, value); }
		}

		[XmlAttribute("IsPartialCancel")]
		[Bindable(true)]
		public bool IsPartialCancel
		{
			get { return GetColumnValue<bool>(Columns.IsPartialCancel); }
			set { SetColumnValue(Columns.IsPartialCancel, value); }
		}

		[XmlAttribute("IsCanceling")]
		[Bindable(true)]
		public bool IsCanceling
		{
			get { return GetColumnValue<bool>(Columns.IsCanceling); }
			set { SetColumnValue(Columns.IsCanceling, value); }
		}

		[XmlAttribute("SerialNo")]
		[Bindable(true)]
		public int SerialNo
		{
			get { return GetColumnValue<int>(Columns.SerialNo); }
			set { SetColumnValue(Columns.SerialNo, value); }
		}

		[XmlAttribute("GuestBuy")]
		[Bindable(true)]
		public int GuestBuy
		{
			get { return GetColumnValue<int>(Columns.GuestBuy); }
			set { SetColumnValue(Columns.GuestBuy, value); }
		}

		[XmlAttribute("Installment")]
		[Bindable(true)]
		public int Installment
		{
			get { return GetColumnValue<int>(Columns.Installment); }
			set { SetColumnValue(Columns.Installment, value); }
		}

		[XmlAttribute("OrderSettlementTime")]
		[Bindable(true)]
		public DateTime? OrderSettlementTime
		{
			get { return GetColumnValue<DateTime?>(Columns.OrderSettlementTime); }
			set { SetColumnValue(Columns.OrderSettlementTime, value); }
		}

		[XmlAttribute("CartDGuid")]
		[Bindable(true)]
		public Guid? CartDGuid
		{
			get { return GetColumnValue<Guid?>(Columns.CartDGuid); }
			set { SetColumnValue(Columns.CartDGuid, value); }
		}

		[XmlAttribute("CartGuid")]
		[Bindable(true)]
		public Guid? CartGuid
		{
			get { return GetColumnValue<Guid?>(Columns.CartGuid); }
			set { SetColumnValue(Columns.CartGuid, value); }
		}

		[XmlAttribute("IsFreightsOrder")]
		[Bindable(true)]
		public bool? IsFreightsOrder
		{
			get { return GetColumnValue<bool?>(Columns.IsFreightsOrder); }
			set { SetColumnValue(Columns.IsFreightsOrder, value); }
		}

		[XmlAttribute("MobilePayType")]
		[Bindable(true)]
		public int? MobilePayType
		{
			get { return GetColumnValue<int?>(Columns.MobilePayType); }
			set { SetColumnValue(Columns.MobilePayType, value); }
		}

		[XmlAttribute("Turnover")]
		[Bindable(true)]
		public int Turnover
		{
			get { return GetColumnValue<int>(Columns.Turnover); }
			set { SetColumnValue(Columns.Turnover, value); }
		}

		#endregion

		#region Typed Columns

		public static TableSchema.TableColumn GuidColumn
		{
			get { return Schema.Columns[0]; }
		}

		public static TableSchema.TableColumn OrderIdColumn
		{
			get { return Schema.Columns[1]; }
		}

		public static TableSchema.TableColumn SellerGuidColumn
		{
			get { return Schema.Columns[2]; }
		}

		public static TableSchema.TableColumn SellerNameColumn
		{
			get { return Schema.Columns[3]; }
		}

		public static TableSchema.TableColumn MemberNameColumn
		{
			get { return Schema.Columns[4]; }
		}

		public static TableSchema.TableColumn PhoneNumberColumn
		{
			get { return Schema.Columns[5]; }
		}

		public static TableSchema.TableColumn MobileNumberColumn
		{
			get { return Schema.Columns[6]; }
		}

		public static TableSchema.TableColumn DeliveryAddressColumn
		{
			get { return Schema.Columns[7]; }
		}

		public static TableSchema.TableColumn DeliveryTimeColumn
		{
			get { return Schema.Columns[8]; }
		}

		public static TableSchema.TableColumn SubtotalColumn
		{
			get { return Schema.Columns[9]; }
		}

		public static TableSchema.TableColumn TotalColumn
		{
			get { return Schema.Columns[10]; }
		}

		public static TableSchema.TableColumn UserMemoColumn
		{
			get { return Schema.Columns[11]; }
		}

		public static TableSchema.TableColumn OrderMemoColumn
		{
			get { return Schema.Columns[12]; }
		}

		public static TableSchema.TableColumn OrderStatusColumn
		{
			get { return Schema.Columns[13]; }
		}

		public static TableSchema.TableColumn OrderStageColumn
		{
			get { return Schema.Columns[14]; }
		}

		public static TableSchema.TableColumn BonusPointColumn
		{
			get { return Schema.Columns[15]; }
		}

		public static TableSchema.TableColumn CreateIdColumn
		{
			get { return Schema.Columns[16]; }
		}

		public static TableSchema.TableColumn CreateTimeColumn
		{
			get { return Schema.Columns[17]; }
		}

		public static TableSchema.TableColumn ModifyIdColumn
		{
			get { return Schema.Columns[18]; }
		}

		public static TableSchema.TableColumn ModifyTimeColumn
		{
			get { return Schema.Columns[19]; }
		}

		public static TableSchema.TableColumn AccessLockColumn
		{
			get { return Schema.Columns[20]; }
		}

		public static TableSchema.TableColumn CityIdColumn
		{
			get { return Schema.Columns[21]; }
		}

		public static TableSchema.TableColumn ParentOrderIdColumn
		{
			get { return Schema.Columns[22]; }
		}

		public static TableSchema.TableColumn InvoiceNoColumn
		{
			get { return Schema.Columns[23]; }
		}

		public static TableSchema.TableColumn ReturnPaperColumn
		{
			get { return Schema.Columns[24]; }
		}

		public static TableSchema.TableColumn ExportLogColumn
		{
			get { return Schema.Columns[25]; }
		}

		public static TableSchema.TableColumn PaidByTravelcardColumn
		{
			get { return Schema.Columns[26]; }
		}

		public static TableSchema.TableColumn OrderFromTypeColumn
		{
			get { return Schema.Columns[27]; }
		}

		public static TableSchema.TableColumn UserIdColumn
		{
			get { return Schema.Columns[28]; }
		}

		public static TableSchema.TableColumn IsPartialCancelColumn
		{
			get { return Schema.Columns[29]; }
		}

		public static TableSchema.TableColumn IsCancelingColumn
		{
			get { return Schema.Columns[30]; }
		}

		public static TableSchema.TableColumn SerialNoColumn
		{
			get { return Schema.Columns[31]; }
		}

		public static TableSchema.TableColumn GuestBuyColumn
		{
			get { return Schema.Columns[32]; }
		}

		public static TableSchema.TableColumn InstallmentColumn
		{
			get { return Schema.Columns[33]; }
		}

		public static TableSchema.TableColumn OrderSettlementTimeColumn
		{
			get { return Schema.Columns[34]; }
		}

		public static TableSchema.TableColumn CartDGuidColumn
		{
			get { return Schema.Columns[35]; }
		}

		public static TableSchema.TableColumn CartGuidColumn
		{
			get { return Schema.Columns[36]; }
		}

		public static TableSchema.TableColumn IsFreightsOrderColumn
		{
			get { return Schema.Columns[37]; }
		}

		public static TableSchema.TableColumn MobilePayTypeColumn
		{
			get { return Schema.Columns[38]; }
		}

		public static TableSchema.TableColumn TurnoverColumn
		{
			get { return Schema.Columns[39]; }
		}

		#endregion

		#region Columns Struct

		public struct Columns
		{
			public static string Guid = @"GUID";
			public static string OrderId = @"order_id";
			public static string SellerGuid = @"seller_GUID";
			public static string SellerName = @"seller_name";
			public static string MemberName = @"member_name";
			public static string PhoneNumber = @"phone_number";
			public static string MobileNumber = @"mobile_number";
			public static string DeliveryAddress = @"delivery_address";
			public static string DeliveryTime = @"delivery_time";
			public static string Subtotal = @"subtotal";
			public static string Total = @"total";
			public static string UserMemo = @"user_memo";
			public static string OrderMemo = @"order_memo";
			public static string OrderStatus = @"order_status";
			public static string OrderStage = @"order_stage";
			public static string BonusPoint = @"bonus_point";
			public static string CreateId = @"create_id";
			public static string CreateTime = @"create_time";
			public static string ModifyId = @"modify_id";
			public static string ModifyTime = @"modify_time";
			public static string AccessLock = @"access_lock";
			public static string CityId = @"city_id";
			public static string ParentOrderId = @"parent_order_id";
			public static string InvoiceNo = @"invoice_no";
			public static string ReturnPaper = @"return_paper";
			public static string ExportLog = @"export_log";
			public static string PaidByTravelcard = @"paid_by_travelcard";
			public static string OrderFromType = @"order_from_type";
			public static string UserId = @"user_id";
			public static string IsPartialCancel = @"is_partial_cancel";
			public static string IsCanceling = @"is_canceling";
			public static string SerialNo = @"serial_no";
			public static string GuestBuy = @"guest_buy";
			public static string Installment = @"installment";
			public static string OrderSettlementTime = @"order_settlement_time";
			public static string CartDGuid = @"cart_d_guid";
			public static string CartGuid = @"cart_guid";
			public static string IsFreightsOrder = @"is_freights_order";
			public static string MobilePayType = @"mobile_pay_type";
			public static string Turnover = @"turnover";
		}

		#endregion

	}
}

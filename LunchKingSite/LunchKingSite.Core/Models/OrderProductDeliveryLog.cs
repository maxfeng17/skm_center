using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the OrderProductDeliveryLog class.
	/// </summary>
    [Serializable]
	public partial class OrderProductDeliveryLogCollection : RepositoryList<OrderProductDeliveryLog, OrderProductDeliveryLogCollection>
	{	   
		public OrderProductDeliveryLogCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>OrderProductDeliveryLogCollection</returns>
		public OrderProductDeliveryLogCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                OrderProductDeliveryLog o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the order_product_delivery_log table.
	/// </summary>
	[Serializable]
	public partial class OrderProductDeliveryLog : RepositoryRecord<OrderProductDeliveryLog>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public OrderProductDeliveryLog()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public OrderProductDeliveryLog(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("order_product_delivery_log", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarOrderProductDeliveryId = new TableSchema.TableColumn(schema);
				colvarOrderProductDeliveryId.ColumnName = "order_product_delivery_id";
				colvarOrderProductDeliveryId.DataType = DbType.Int32;
				colvarOrderProductDeliveryId.MaxLength = 0;
				colvarOrderProductDeliveryId.AutoIncrement = false;
				colvarOrderProductDeliveryId.IsNullable = false;
				colvarOrderProductDeliveryId.IsPrimaryKey = false;
				colvarOrderProductDeliveryId.IsForeignKey = false;
				colvarOrderProductDeliveryId.IsReadOnly = false;
				colvarOrderProductDeliveryId.DefaultSetting = @"";
				colvarOrderProductDeliveryId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderProductDeliveryId);
				
				TableSchema.TableColumn colvarGoodsStatus = new TableSchema.TableColumn(schema);
				colvarGoodsStatus.ColumnName = "goods_status";
				colvarGoodsStatus.DataType = DbType.Int32;
				colvarGoodsStatus.MaxLength = 0;
				colvarGoodsStatus.AutoIncrement = false;
				colvarGoodsStatus.IsNullable = false;
				colvarGoodsStatus.IsPrimaryKey = false;
				colvarGoodsStatus.IsForeignKey = false;
				colvarGoodsStatus.IsReadOnly = false;
				colvarGoodsStatus.DefaultSetting = @"";
				colvarGoodsStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGoodsStatus);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("order_product_delivery_log",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("OrderProductDeliveryId")]
		[Bindable(true)]
		public int OrderProductDeliveryId 
		{
			get { return GetColumnValue<int>(Columns.OrderProductDeliveryId); }
			set { SetColumnValue(Columns.OrderProductDeliveryId, value); }
		}
		  
		[XmlAttribute("GoodsStatus")]
		[Bindable(true)]
		public int GoodsStatus 
		{
			get { return GetColumnValue<int>(Columns.GoodsStatus); }
			set { SetColumnValue(Columns.GoodsStatus, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn OrderProductDeliveryIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn GoodsStatusColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string OrderProductDeliveryId = @"order_product_delivery_id";
			 public static string GoodsStatus = @"goods_status";
			 public static string CreateTime = @"create_time";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

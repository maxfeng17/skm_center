using System;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using SubSonic;

namespace LunchKingSite.DataOrm
{
	[Serializable]
	public partial class CashTrustLogCollection : RepositoryList<CashTrustLog, CashTrustLogCollection>
	{
			public CashTrustLogCollection() {}

			public CashTrustLogCollection Filter()
			{
					for (int i = this.Count - 1; i > -1; i--)
					{
							CashTrustLog o = this[i];
							foreach (SubSonic.Where w in this.wheres)
							{
									bool remove = false;
									System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
									if(pi.CanRead)
									{
											object val = pi.GetValue(o, null);
											switch(w.Comparison)
											{
													case SubSonic.Comparison.Equals:
															if(!val.Equals(w.ParameterValue))
															{
																	remove = true;
															}
															break;
											}
									}
									if(remove)
									{
											this.Remove(o);
											break;
									}
							}
					}
					return this;
			}
	}

	[Serializable]
	public partial class CashTrustLog : RepositoryRecord<CashTrustLog>, IRecordBase
	{
		#region .ctors and Default Settings
		public CashTrustLog()
		{
			SetSQLProps();
			InitSetDefaults();
			MarkNew();
		}

		private void InitSetDefaults() { SetDefaults(); }

		public CashTrustLog(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}

		protected static void SetSQLProps() { GetTableSchema(); }

		#endregion

		#region Schema and Query Accessor
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}

		private static void GetTableSchema()
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("cash_trust_log", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				TableSchema.TableColumn colvarTrustId = new TableSchema.TableColumn(schema);
				colvarTrustId.ColumnName = "trust_id";
				colvarTrustId.DataType = DbType.Guid;
				colvarTrustId.MaxLength = 0;
				colvarTrustId.AutoIncrement = false;
				colvarTrustId.IsNullable = false;
				colvarTrustId.IsPrimaryKey = true;
				colvarTrustId.IsForeignKey = false;
				colvarTrustId.IsReadOnly = false;
				colvarTrustId.DefaultSetting = @"";
				colvarTrustId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTrustId);

				TableSchema.TableColumn colvarAmount = new TableSchema.TableColumn(schema);
				colvarAmount.ColumnName = "amount";
				colvarAmount.DataType = DbType.Int32;
				colvarAmount.MaxLength = 0;
				colvarAmount.AutoIncrement = false;
				colvarAmount.IsNullable = false;
				colvarAmount.IsPrimaryKey = false;
				colvarAmount.IsForeignKey = false;
				colvarAmount.IsReadOnly = false;
				colvarAmount.DefaultSetting = @"";
				colvarAmount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAmount);

				TableSchema.TableColumn colvarTrustProvider = new TableSchema.TableColumn(schema);
				colvarTrustProvider.ColumnName = "trust_provider";
				colvarTrustProvider.DataType = DbType.Int32;
				colvarTrustProvider.MaxLength = 0;
				colvarTrustProvider.AutoIncrement = false;
				colvarTrustProvider.IsNullable = false;
				colvarTrustProvider.IsPrimaryKey = false;
				colvarTrustProvider.IsForeignKey = false;
				colvarTrustProvider.IsReadOnly = false;
				colvarTrustProvider.DefaultSetting = @"";
				colvarTrustProvider.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTrustProvider);

				TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
				colvarStatus.ColumnName = "status";
				colvarStatus.DataType = DbType.Int32;
				colvarStatus.MaxLength = 0;
				colvarStatus.AutoIncrement = false;
				colvarStatus.IsNullable = false;
				colvarStatus.IsPrimaryKey = false;
				colvarStatus.IsForeignKey = false;
				colvarStatus.IsReadOnly = false;
				colvarStatus.DefaultSetting = @"";
				colvarStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStatus);

				TableSchema.TableColumn colvarBankStatus = new TableSchema.TableColumn(schema);
				colvarBankStatus.ColumnName = "bank_status";
				colvarBankStatus.DataType = DbType.Int32;
				colvarBankStatus.MaxLength = 0;
				colvarBankStatus.AutoIncrement = false;
				colvarBankStatus.IsNullable = false;
				colvarBankStatus.IsPrimaryKey = false;
				colvarBankStatus.IsForeignKey = false;
				colvarBankStatus.IsReadOnly = false;
				colvarBankStatus.DefaultSetting = @"((0))";
				colvarBankStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBankStatus);

				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);

				TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
				colvarModifyTime.ColumnName = "modify_time";
				colvarModifyTime.DataType = DbType.DateTime;
				colvarModifyTime.MaxLength = 0;
				colvarModifyTime.AutoIncrement = false;
				colvarModifyTime.IsNullable = false;
				colvarModifyTime.IsPrimaryKey = false;
				colvarModifyTime.IsForeignKey = false;
				colvarModifyTime.IsReadOnly = false;
				colvarModifyTime.DefaultSetting = @"";
				colvarModifyTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyTime);

				TableSchema.TableColumn colvarTrustedTime = new TableSchema.TableColumn(schema);
				colvarTrustedTime.ColumnName = "trusted_time";
				colvarTrustedTime.DataType = DbType.DateTime;
				colvarTrustedTime.MaxLength = 0;
				colvarTrustedTime.AutoIncrement = false;
				colvarTrustedTime.IsNullable = true;
				colvarTrustedTime.IsPrimaryKey = false;
				colvarTrustedTime.IsForeignKey = false;
				colvarTrustedTime.IsReadOnly = false;
				colvarTrustedTime.DefaultSetting = @"";
				colvarTrustedTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTrustedTime);

				TableSchema.TableColumn colvarTrustedBankTime = new TableSchema.TableColumn(schema);
				colvarTrustedBankTime.ColumnName = "trusted_bank_time";
				colvarTrustedBankTime.DataType = DbType.DateTime;
				colvarTrustedBankTime.MaxLength = 0;
				colvarTrustedBankTime.AutoIncrement = false;
				colvarTrustedBankTime.IsNullable = true;
				colvarTrustedBankTime.IsPrimaryKey = false;
				colvarTrustedBankTime.IsForeignKey = false;
				colvarTrustedBankTime.IsReadOnly = false;
				colvarTrustedBankTime.DefaultSetting = @"";
				colvarTrustedBankTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTrustedBankTime);

				TableSchema.TableColumn colvarVerifiedTime = new TableSchema.TableColumn(schema);
				colvarVerifiedTime.ColumnName = "verified_time";
				colvarVerifiedTime.DataType = DbType.DateTime;
				colvarVerifiedTime.MaxLength = 0;
				colvarVerifiedTime.AutoIncrement = false;
				colvarVerifiedTime.IsNullable = true;
				colvarVerifiedTime.IsPrimaryKey = false;
				colvarVerifiedTime.IsForeignKey = false;
				colvarVerifiedTime.IsReadOnly = false;
				colvarVerifiedTime.DefaultSetting = @"";
				colvarVerifiedTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarVerifiedTime);

				TableSchema.TableColumn colvarVerifiedBankTime = new TableSchema.TableColumn(schema);
				colvarVerifiedBankTime.ColumnName = "verified_bank_time";
				colvarVerifiedBankTime.DataType = DbType.DateTime;
				colvarVerifiedBankTime.MaxLength = 0;
				colvarVerifiedBankTime.AutoIncrement = false;
				colvarVerifiedBankTime.IsNullable = true;
				colvarVerifiedBankTime.IsPrimaryKey = false;
				colvarVerifiedBankTime.IsForeignKey = false;
				colvarVerifiedBankTime.IsReadOnly = false;
				colvarVerifiedBankTime.DefaultSetting = @"";
				colvarVerifiedBankTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarVerifiedBankTime);

				TableSchema.TableColumn colvarCouponSequenceNumber = new TableSchema.TableColumn(schema);
				colvarCouponSequenceNumber.ColumnName = "coupon_sequence_number";
				colvarCouponSequenceNumber.DataType = DbType.String;
				colvarCouponSequenceNumber.MaxLength = 50;
				colvarCouponSequenceNumber.AutoIncrement = false;
				colvarCouponSequenceNumber.IsNullable = true;
				colvarCouponSequenceNumber.IsPrimaryKey = false;
				colvarCouponSequenceNumber.IsForeignKey = false;
				colvarCouponSequenceNumber.IsReadOnly = false;
				colvarCouponSequenceNumber.DefaultSetting = @"";
				colvarCouponSequenceNumber.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCouponSequenceNumber);

				TableSchema.TableColumn colvarCouponId = new TableSchema.TableColumn(schema);
				colvarCouponId.ColumnName = "coupon_id";
				colvarCouponId.DataType = DbType.Int32;
				colvarCouponId.MaxLength = 0;
				colvarCouponId.AutoIncrement = false;
				colvarCouponId.IsNullable = true;
				colvarCouponId.IsPrimaryKey = false;
				colvarCouponId.IsForeignKey = false;
				colvarCouponId.IsReadOnly = false;
				colvarCouponId.DefaultSetting = @"";
				colvarCouponId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCouponId);

				TableSchema.TableColumn colvarUserId = new TableSchema.TableColumn(schema);
				colvarUserId.ColumnName = "user_id";
				colvarUserId.DataType = DbType.Int32;
				colvarUserId.MaxLength = 0;
				colvarUserId.AutoIncrement = false;
				colvarUserId.IsNullable = false;
				colvarUserId.IsPrimaryKey = false;
				colvarUserId.IsForeignKey = false;
				colvarUserId.IsReadOnly = false;
				colvarUserId.DefaultSetting = @"";
				colvarUserId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUserId);

				TableSchema.TableColumn colvarOrderGuid = new TableSchema.TableColumn(schema);
				colvarOrderGuid.ColumnName = "order_guid";
				colvarOrderGuid.DataType = DbType.Guid;
				colvarOrderGuid.MaxLength = 0;
				colvarOrderGuid.AutoIncrement = false;
				colvarOrderGuid.IsNullable = false;
				colvarOrderGuid.IsPrimaryKey = false;
				colvarOrderGuid.IsForeignKey = false;
				colvarOrderGuid.IsReadOnly = false;
				colvarOrderGuid.DefaultSetting = @"";
				colvarOrderGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderGuid);

				TableSchema.TableColumn colvarOrderDetailGuid = new TableSchema.TableColumn(schema);
				colvarOrderDetailGuid.ColumnName = "order_detail_guid";
				colvarOrderDetailGuid.DataType = DbType.Guid;
				colvarOrderDetailGuid.MaxLength = 0;
				colvarOrderDetailGuid.AutoIncrement = false;
				colvarOrderDetailGuid.IsNullable = false;
				colvarOrderDetailGuid.IsPrimaryKey = false;
				colvarOrderDetailGuid.IsForeignKey = false;
				colvarOrderDetailGuid.IsReadOnly = false;
				colvarOrderDetailGuid.DefaultSetting = @"";
				colvarOrderDetailGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderDetailGuid);

				TableSchema.TableColumn colvarItemName = new TableSchema.TableColumn(schema);
				colvarItemName.ColumnName = "item_name";
				colvarItemName.DataType = DbType.String;
				colvarItemName.MaxLength = 2147483647;
				colvarItemName.AutoIncrement = false;
				colvarItemName.IsNullable = false;
				colvarItemName.IsPrimaryKey = false;
				colvarItemName.IsForeignKey = false;
				colvarItemName.IsReadOnly = false;
				colvarItemName.DefaultSetting = @"";
				colvarItemName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarItemName);

				TableSchema.TableColumn colvarPcash = new TableSchema.TableColumn(schema);
				colvarPcash.ColumnName = "pcash";
				colvarPcash.DataType = DbType.Int32;
				colvarPcash.MaxLength = 0;
				colvarPcash.AutoIncrement = false;
				colvarPcash.IsNullable = false;
				colvarPcash.IsPrimaryKey = false;
				colvarPcash.IsForeignKey = false;
				colvarPcash.IsReadOnly = false;
				colvarPcash.DefaultSetting = @"";
				colvarPcash.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPcash);

				TableSchema.TableColumn colvarScash = new TableSchema.TableColumn(schema);
				colvarScash.ColumnName = "scash";
				colvarScash.DataType = DbType.Int32;
				colvarScash.MaxLength = 0;
				colvarScash.AutoIncrement = false;
				colvarScash.IsNullable = false;
				colvarScash.IsPrimaryKey = false;
				colvarScash.IsForeignKey = false;
				colvarScash.IsReadOnly = false;
				colvarScash.DefaultSetting = @"";
				colvarScash.ForeignKeyTableName = "";
				schema.Columns.Add(colvarScash);

				TableSchema.TableColumn colvarBcash = new TableSchema.TableColumn(schema);
				colvarBcash.ColumnName = "bcash";
				colvarBcash.DataType = DbType.Int32;
				colvarBcash.MaxLength = 0;
				colvarBcash.AutoIncrement = false;
				colvarBcash.IsNullable = false;
				colvarBcash.IsPrimaryKey = false;
				colvarBcash.IsForeignKey = false;
				colvarBcash.IsReadOnly = false;
				colvarBcash.DefaultSetting = @"";
				colvarBcash.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBcash);

				TableSchema.TableColumn colvarCreditCard = new TableSchema.TableColumn(schema);
				colvarCreditCard.ColumnName = "credit_card";
				colvarCreditCard.DataType = DbType.Int32;
				colvarCreditCard.MaxLength = 0;
				colvarCreditCard.AutoIncrement = false;
				colvarCreditCard.IsNullable = false;
				colvarCreditCard.IsPrimaryKey = false;
				colvarCreditCard.IsForeignKey = false;
				colvarCreditCard.IsReadOnly = false;
				colvarCreditCard.DefaultSetting = @"";
				colvarCreditCard.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreditCard);

				TableSchema.TableColumn colvarReportTrustedGuid = new TableSchema.TableColumn(schema);
				colvarReportTrustedGuid.ColumnName = "report_trusted_guid";
				colvarReportTrustedGuid.DataType = DbType.Guid;
				colvarReportTrustedGuid.MaxLength = 0;
				colvarReportTrustedGuid.AutoIncrement = false;
				colvarReportTrustedGuid.IsNullable = true;
				colvarReportTrustedGuid.IsPrimaryKey = false;
				colvarReportTrustedGuid.IsForeignKey = false;
				colvarReportTrustedGuid.IsReadOnly = false;
				colvarReportTrustedGuid.DefaultSetting = @"";
				colvarReportTrustedGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReportTrustedGuid);

				TableSchema.TableColumn colvarReportVerifiedGuid = new TableSchema.TableColumn(schema);
				colvarReportVerifiedGuid.ColumnName = "report_verified_guid";
				colvarReportVerifiedGuid.DataType = DbType.Guid;
				colvarReportVerifiedGuid.MaxLength = 0;
				colvarReportVerifiedGuid.AutoIncrement = false;
				colvarReportVerifiedGuid.IsNullable = true;
				colvarReportVerifiedGuid.IsPrimaryKey = false;
				colvarReportVerifiedGuid.IsForeignKey = false;
				colvarReportVerifiedGuid.IsReadOnly = false;
				colvarReportVerifiedGuid.DefaultSetting = @"";
				colvarReportVerifiedGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReportVerifiedGuid);

				TableSchema.TableColumn colvarDiscountAmount = new TableSchema.TableColumn(schema);
				colvarDiscountAmount.ColumnName = "discount_amount";
				colvarDiscountAmount.DataType = DbType.Int32;
				colvarDiscountAmount.MaxLength = 0;
				colvarDiscountAmount.AutoIncrement = false;
				colvarDiscountAmount.IsNullable = false;
				colvarDiscountAmount.IsPrimaryKey = false;
				colvarDiscountAmount.IsForeignKey = false;
				colvarDiscountAmount.IsReadOnly = false;
				colvarDiscountAmount.DefaultSetting = @"((0))";
				colvarDiscountAmount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDiscountAmount);

				TableSchema.TableColumn colvarSpecialStatus = new TableSchema.TableColumn(schema);
				colvarSpecialStatus.ColumnName = "special_status";
				colvarSpecialStatus.DataType = DbType.Int32;
				colvarSpecialStatus.MaxLength = 0;
				colvarSpecialStatus.AutoIncrement = false;
				colvarSpecialStatus.IsNullable = false;
				colvarSpecialStatus.IsPrimaryKey = false;
				colvarSpecialStatus.IsForeignKey = false;
				colvarSpecialStatus.IsReadOnly = false;
				colvarSpecialStatus.DefaultSetting = @"((0))";
				colvarSpecialStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSpecialStatus);

				TableSchema.TableColumn colvarAtm = new TableSchema.TableColumn(schema);
				colvarAtm.ColumnName = "atm";
				colvarAtm.DataType = DbType.Int32;
				colvarAtm.MaxLength = 0;
				colvarAtm.AutoIncrement = false;
				colvarAtm.IsNullable = false;
				colvarAtm.IsPrimaryKey = false;
				colvarAtm.IsForeignKey = false;
				colvarAtm.IsReadOnly = false;
				colvarAtm.DefaultSetting = @"((0))";
				colvarAtm.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAtm);

				TableSchema.TableColumn colvarStoreVerifiedGuid = new TableSchema.TableColumn(schema);
				colvarStoreVerifiedGuid.ColumnName = "store_verified_guid";
				colvarStoreVerifiedGuid.DataType = DbType.Guid;
				colvarStoreVerifiedGuid.MaxLength = 0;
				colvarStoreVerifiedGuid.AutoIncrement = false;
				colvarStoreVerifiedGuid.IsNullable = true;
				colvarStoreVerifiedGuid.IsPrimaryKey = false;
				colvarStoreVerifiedGuid.IsForeignKey = false;
				colvarStoreVerifiedGuid.IsReadOnly = false;
				colvarStoreVerifiedGuid.DefaultSetting = @"";
				colvarStoreVerifiedGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStoreVerifiedGuid);

				TableSchema.TableColumn colvarSpecialOperatedTime = new TableSchema.TableColumn(schema);
				colvarSpecialOperatedTime.ColumnName = "special_operated_time";
				colvarSpecialOperatedTime.DataType = DbType.DateTime;
				colvarSpecialOperatedTime.MaxLength = 0;
				colvarSpecialOperatedTime.AutoIncrement = false;
				colvarSpecialOperatedTime.IsNullable = true;
				colvarSpecialOperatedTime.IsPrimaryKey = false;
				colvarSpecialOperatedTime.IsForeignKey = false;
				colvarSpecialOperatedTime.IsReadOnly = false;
				colvarSpecialOperatedTime.DefaultSetting = @"";
				colvarSpecialOperatedTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSpecialOperatedTime);

				TableSchema.TableColumn colvarOrderClassification = new TableSchema.TableColumn(schema);
				colvarOrderClassification.ColumnName = "order_classification";
				colvarOrderClassification.DataType = DbType.Int32;
				colvarOrderClassification.MaxLength = 0;
				colvarOrderClassification.AutoIncrement = false;
				colvarOrderClassification.IsNullable = false;
				colvarOrderClassification.IsPrimaryKey = false;
				colvarOrderClassification.IsForeignKey = false;
				colvarOrderClassification.IsReadOnly = false;
				colvarOrderClassification.DefaultSetting = @"((1))";
				colvarOrderClassification.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderClassification);

				TableSchema.TableColumn colvarBusinessHourGuid = new TableSchema.TableColumn(schema);
				colvarBusinessHourGuid.ColumnName = "business_hour_guid";
				colvarBusinessHourGuid.DataType = DbType.Guid;
				colvarBusinessHourGuid.MaxLength = 0;
				colvarBusinessHourGuid.AutoIncrement = false;
				colvarBusinessHourGuid.IsNullable = true;
				colvarBusinessHourGuid.IsPrimaryKey = false;
				colvarBusinessHourGuid.IsForeignKey = false;
				colvarBusinessHourGuid.IsReadOnly = false;
				colvarBusinessHourGuid.DefaultSetting = @"";
				colvarBusinessHourGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBusinessHourGuid);

				TableSchema.TableColumn colvarCheckoutType = new TableSchema.TableColumn(schema);
				colvarCheckoutType.ColumnName = "checkout_type";
				colvarCheckoutType.DataType = DbType.Int32;
				colvarCheckoutType.MaxLength = 0;
				colvarCheckoutType.AutoIncrement = false;
				colvarCheckoutType.IsNullable = false;
				colvarCheckoutType.IsPrimaryKey = false;
				colvarCheckoutType.IsForeignKey = false;
				colvarCheckoutType.IsReadOnly = false;
				colvarCheckoutType.DefaultSetting = @"((0))";
				colvarCheckoutType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCheckoutType);

				TableSchema.TableColumn colvarCost = new TableSchema.TableColumn(schema);
				colvarCost.ColumnName = "cost";
				colvarCost.DataType = DbType.Int32;
				colvarCost.MaxLength = 0;
				colvarCost.AutoIncrement = false;
				colvarCost.IsNullable = false;
				colvarCost.IsPrimaryKey = false;
				colvarCost.IsForeignKey = false;
				colvarCost.IsReadOnly = false;
				colvarCost.DefaultSetting = @"((0))";
				colvarCost.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCost);

				TableSchema.TableColumn colvarReceivableId = new TableSchema.TableColumn(schema);
				colvarReceivableId.ColumnName = "receivable_id";
				colvarReceivableId.DataType = DbType.Int32;
				colvarReceivableId.MaxLength = 0;
				colvarReceivableId.AutoIncrement = false;
				colvarReceivableId.IsNullable = true;
				colvarReceivableId.IsPrimaryKey = false;
				colvarReceivableId.IsForeignKey = false;
				colvarReceivableId.IsReadOnly = false;
				colvarReceivableId.DefaultSetting = @"";
				colvarReceivableId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReceivableId);

				TableSchema.TableColumn colvarDeliveryType = new TableSchema.TableColumn(schema);
				colvarDeliveryType.ColumnName = "delivery_type";
				colvarDeliveryType.DataType = DbType.Int32;
				colvarDeliveryType.MaxLength = 0;
				colvarDeliveryType.AutoIncrement = false;
				colvarDeliveryType.IsNullable = false;
				colvarDeliveryType.IsPrimaryKey = false;
				colvarDeliveryType.IsForeignKey = false;
				colvarDeliveryType.IsReadOnly = false;
				colvarDeliveryType.DefaultSetting = @"((0))";
				colvarDeliveryType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDeliveryType);

				TableSchema.TableColumn colvarPrefix = new TableSchema.TableColumn(schema);
				colvarPrefix.ColumnName = "prefix";
				colvarPrefix.DataType = DbType.AnsiString;
				colvarPrefix.MaxLength = 10;
				colvarPrefix.AutoIncrement = false;
				colvarPrefix.IsNullable = true;
				colvarPrefix.IsPrimaryKey = false;
				colvarPrefix.IsForeignKey = false;
				colvarPrefix.IsReadOnly = false;
				colvarPrefix.DefaultSetting = @"";
				colvarPrefix.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPrefix);

				TableSchema.TableColumn colvarUninvoicedAmount = new TableSchema.TableColumn(schema);
				colvarUninvoicedAmount.ColumnName = "uninvoiced_amount";
				colvarUninvoicedAmount.DataType = DbType.Int32;
				colvarUninvoicedAmount.MaxLength = 0;
				colvarUninvoicedAmount.AutoIncrement = false;
				colvarUninvoicedAmount.IsNullable = false;
				colvarUninvoicedAmount.IsPrimaryKey = false;
				colvarUninvoicedAmount.IsForeignKey = false;
				colvarUninvoicedAmount.IsReadOnly = false;
				colvarUninvoicedAmount.DefaultSetting = @"((0))";
				colvarUninvoicedAmount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUninvoicedAmount);

				TableSchema.TableColumn colvarStoreGuid = new TableSchema.TableColumn(schema);
				colvarStoreGuid.ColumnName = "store_guid";
				colvarStoreGuid.DataType = DbType.Guid;
				colvarStoreGuid.MaxLength = 0;
				colvarStoreGuid.AutoIncrement = false;
				colvarStoreGuid.IsNullable = true;
				colvarStoreGuid.IsPrimaryKey = false;
				colvarStoreGuid.IsForeignKey = false;
				colvarStoreGuid.IsReadOnly = false;
				colvarStoreGuid.DefaultSetting = @"";
				colvarStoreGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStoreGuid);

				TableSchema.TableColumn colvarUsageVerifiedTime = new TableSchema.TableColumn(schema);
				colvarUsageVerifiedTime.ColumnName = "usage_verified_time";
				colvarUsageVerifiedTime.DataType = DbType.DateTime;
				colvarUsageVerifiedTime.MaxLength = 0;
				colvarUsageVerifiedTime.AutoIncrement = false;
				colvarUsageVerifiedTime.IsNullable = true;
				colvarUsageVerifiedTime.IsPrimaryKey = false;
				colvarUsageVerifiedTime.IsForeignKey = false;
				colvarUsageVerifiedTime.IsReadOnly = false;
				colvarUsageVerifiedTime.DefaultSetting = @"";
				colvarUsageVerifiedTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUsageVerifiedTime);

				TableSchema.TableColumn colvarLcash = new TableSchema.TableColumn(schema);
				colvarLcash.ColumnName = "lcash";
				colvarLcash.DataType = DbType.Int32;
				colvarLcash.MaxLength = 0;
				colvarLcash.AutoIncrement = false;
				colvarLcash.IsNullable = false;
				colvarLcash.IsPrimaryKey = false;
				colvarLcash.IsForeignKey = false;
				colvarLcash.IsReadOnly = false;
				colvarLcash.DefaultSetting = @"((0))";
				colvarLcash.ForeignKeyTableName = "";
				schema.Columns.Add(colvarLcash);

				TableSchema.TableColumn colvarReturnedTime = new TableSchema.TableColumn(schema);
				colvarReturnedTime.ColumnName = "returned_time";
				colvarReturnedTime.DataType = DbType.DateTime;
				colvarReturnedTime.MaxLength = 0;
				colvarReturnedTime.AutoIncrement = false;
				colvarReturnedTime.IsNullable = true;
				colvarReturnedTime.IsPrimaryKey = false;
				colvarReturnedTime.IsForeignKey = false;
				colvarReturnedTime.IsReadOnly = false;
				colvarReturnedTime.DefaultSetting = @"";
				colvarReturnedTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReturnedTime);

				TableSchema.TableColumn colvarTcash = new TableSchema.TableColumn(schema);
				colvarTcash.ColumnName = "tcash";
				colvarTcash.DataType = DbType.Int32;
				colvarTcash.MaxLength = 0;
				colvarTcash.AutoIncrement = false;
				colvarTcash.IsNullable = false;
				colvarTcash.IsPrimaryKey = false;
				colvarTcash.IsForeignKey = false;
				colvarTcash.IsReadOnly = false;
				colvarTcash.DefaultSetting = @"((0))";
				colvarTcash.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTcash);

				TableSchema.TableColumn colvarThirdPartyPayment = new TableSchema.TableColumn(schema);
				colvarThirdPartyPayment.ColumnName = "third_party_payment";
				colvarThirdPartyPayment.DataType = DbType.Byte;
				colvarThirdPartyPayment.MaxLength = 0;
				colvarThirdPartyPayment.AutoIncrement = false;
				colvarThirdPartyPayment.IsNullable = false;
				colvarThirdPartyPayment.IsPrimaryKey = false;
				colvarThirdPartyPayment.IsForeignKey = false;
				colvarThirdPartyPayment.IsReadOnly = false;
				colvarThirdPartyPayment.DefaultSetting = @"((0))";
				colvarThirdPartyPayment.ForeignKeyTableName = "";
				schema.Columns.Add(colvarThirdPartyPayment);

				TableSchema.TableColumn colvarVerifiedStoreGuid = new TableSchema.TableColumn(schema);
				colvarVerifiedStoreGuid.ColumnName = "verified_store_guid";
				colvarVerifiedStoreGuid.DataType = DbType.Guid;
				colvarVerifiedStoreGuid.MaxLength = 0;
				colvarVerifiedStoreGuid.AutoIncrement = false;
				colvarVerifiedStoreGuid.IsNullable = true;
				colvarVerifiedStoreGuid.IsPrimaryKey = false;
				colvarVerifiedStoreGuid.IsForeignKey = false;
				colvarVerifiedStoreGuid.IsReadOnly = false;
				colvarVerifiedStoreGuid.DefaultSetting = @"";
				colvarVerifiedStoreGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarVerifiedStoreGuid);

				TableSchema.TableColumn colvarTrustSequenceNumber = new TableSchema.TableColumn(schema);
				colvarTrustSequenceNumber.ColumnName = "trust_sequence_number";
				colvarTrustSequenceNumber.DataType = DbType.String;
				colvarTrustSequenceNumber.MaxLength = 50;
				colvarTrustSequenceNumber.AutoIncrement = false;
				colvarTrustSequenceNumber.IsNullable = true;
				colvarTrustSequenceNumber.IsPrimaryKey = false;
				colvarTrustSequenceNumber.IsForeignKey = false;
				colvarTrustSequenceNumber.IsReadOnly = false;
				colvarTrustSequenceNumber.DefaultSetting = @"";
				colvarTrustSequenceNumber.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTrustSequenceNumber);

				TableSchema.TableColumn colvarUsageVerifyId = new TableSchema.TableColumn(schema);
				colvarUsageVerifyId.ColumnName = "usage_verify_id";
				colvarUsageVerifyId.DataType = DbType.String;
				colvarUsageVerifyId.MaxLength = 256;
				colvarUsageVerifyId.AutoIncrement = false;
				colvarUsageVerifyId.IsNullable = true;
				colvarUsageVerifyId.IsPrimaryKey = false;
				colvarUsageVerifyId.IsForeignKey = false;
				colvarUsageVerifyId.IsReadOnly = false;
				colvarUsageVerifyId.DefaultSetting = @"";
				colvarUsageVerifyId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUsageVerifyId);

				TableSchema.TableColumn colvarFamilyIsp = new TableSchema.TableColumn(schema);
				colvarFamilyIsp.ColumnName = "family_isp";
				colvarFamilyIsp.DataType = DbType.Int32;
				colvarFamilyIsp.MaxLength = 0;
				colvarFamilyIsp.AutoIncrement = false;
				colvarFamilyIsp.IsNullable = false;
				colvarFamilyIsp.IsPrimaryKey = false;
				colvarFamilyIsp.IsForeignKey = false;
				colvarFamilyIsp.IsReadOnly = false;
				colvarFamilyIsp.DefaultSetting = @"((0))";
				colvarFamilyIsp.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFamilyIsp);

				TableSchema.TableColumn colvarSevenIsp = new TableSchema.TableColumn(schema);
				colvarSevenIsp.ColumnName = "seven_isp";
				colvarSevenIsp.DataType = DbType.Int32;
				colvarSevenIsp.MaxLength = 0;
				colvarSevenIsp.AutoIncrement = false;
				colvarSevenIsp.IsNullable = false;
				colvarSevenIsp.IsPrimaryKey = false;
				colvarSevenIsp.IsForeignKey = false;
				colvarSevenIsp.IsReadOnly = false;
				colvarSevenIsp.DefaultSetting = @"((0))";
				colvarSevenIsp.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSevenIsp);

				TableSchema.TableColumn colvarReserve01 = new TableSchema.TableColumn(schema);
				colvarReserve01.ColumnName = "reserve01";
				colvarReserve01.DataType = DbType.Int32;
				colvarReserve01.MaxLength = 0;
				colvarReserve01.AutoIncrement = false;
				colvarReserve01.IsNullable = false;
				colvarReserve01.IsPrimaryKey = false;
				colvarReserve01.IsForeignKey = false;
				colvarReserve01.IsReadOnly = false;
				colvarReserve01.DefaultSetting = @"((0))";
				colvarReserve01.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReserve01);

				TableSchema.TableColumn colvarReserve02 = new TableSchema.TableColumn(schema);
				colvarReserve02.ColumnName = "reserve02";
				colvarReserve02.DataType = DbType.Int32;
				colvarReserve02.MaxLength = 0;
				colvarReserve02.AutoIncrement = false;
				colvarReserve02.IsNullable = false;
				colvarReserve02.IsPrimaryKey = false;
				colvarReserve02.IsForeignKey = false;
				colvarReserve02.IsReadOnly = false;
				colvarReserve02.DefaultSetting = @"((0))";
				colvarReserve02.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReserve02);

				TableSchema.TableColumn colvarPscash = new TableSchema.TableColumn(schema);
				colvarPscash.ColumnName = "pscash";
				colvarPscash.DataType = DbType.Int32;
				colvarPscash.MaxLength = 0;
				colvarPscash.AutoIncrement = false;
				colvarPscash.IsNullable = false;
				colvarPscash.IsPrimaryKey = false;
				colvarPscash.IsForeignKey = false;
				colvarPscash.IsReadOnly = false;
				colvarPscash.DefaultSetting = @"((0))";
				colvarPscash.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPscash);

				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("cash_trust_log",schema);
			}
		}
		#endregion

		#region Props

		[XmlAttribute("TrustId")]
		[Bindable(true)]
		public Guid TrustId
		{
			get { return GetColumnValue<Guid>(Columns.TrustId); }
			set { SetColumnValue(Columns.TrustId, value); }
		}

		[XmlAttribute("Amount")]
		[Bindable(true)]
		public int Amount
		{
			get { return GetColumnValue<int>(Columns.Amount); }
			set { SetColumnValue(Columns.Amount, value); }
		}

		[XmlAttribute("TrustProvider")]
		[Bindable(true)]
		public int TrustProvider
		{
			get { return GetColumnValue<int>(Columns.TrustProvider); }
			set { SetColumnValue(Columns.TrustProvider, value); }
		}

		[XmlAttribute("Status")]
		[Bindable(true)]
		public int Status
		{
			get { return GetColumnValue<int>(Columns.Status); }
			set { SetColumnValue(Columns.Status, value); }
		}

		[XmlAttribute("BankStatus")]
		[Bindable(true)]
		public int BankStatus
		{
			get { return GetColumnValue<int>(Columns.BankStatus); }
			set { SetColumnValue(Columns.BankStatus, value); }
		}

		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}

		[XmlAttribute("ModifyTime")]
		[Bindable(true)]
		public DateTime ModifyTime
		{
			get { return GetColumnValue<DateTime>(Columns.ModifyTime); }
			set { SetColumnValue(Columns.ModifyTime, value); }
		}

		[XmlAttribute("TrustedTime")]
		[Bindable(true)]
		public DateTime? TrustedTime
		{
			get { return GetColumnValue<DateTime?>(Columns.TrustedTime); }
			set { SetColumnValue(Columns.TrustedTime, value); }
		}

		[XmlAttribute("TrustedBankTime")]
		[Bindable(true)]
		public DateTime? TrustedBankTime
		{
			get { return GetColumnValue<DateTime?>(Columns.TrustedBankTime); }
			set { SetColumnValue(Columns.TrustedBankTime, value); }
		}

		[XmlAttribute("VerifiedTime")]
		[Bindable(true)]
		public DateTime? VerifiedTime
		{
			get { return GetColumnValue<DateTime?>(Columns.VerifiedTime); }
			set { SetColumnValue(Columns.VerifiedTime, value); }
		}

		[XmlAttribute("VerifiedBankTime")]
		[Bindable(true)]
		public DateTime? VerifiedBankTime
		{
			get { return GetColumnValue<DateTime?>(Columns.VerifiedBankTime); }
			set { SetColumnValue(Columns.VerifiedBankTime, value); }
		}

		[XmlAttribute("CouponSequenceNumber")]
		[Bindable(true)]
		public string CouponSequenceNumber
		{
			get { return GetColumnValue<string>(Columns.CouponSequenceNumber); }
			set { SetColumnValue(Columns.CouponSequenceNumber, value); }
		}

		[XmlAttribute("CouponId")]
		[Bindable(true)]
		public int? CouponId
		{
			get { return GetColumnValue<int?>(Columns.CouponId); }
			set { SetColumnValue(Columns.CouponId, value); }
		}

		[XmlAttribute("UserId")]
		[Bindable(true)]
		public int UserId
		{
			get { return GetColumnValue<int>(Columns.UserId); }
			set { SetColumnValue(Columns.UserId, value); }
		}

		[XmlAttribute("OrderGuid")]
		[Bindable(true)]
		public Guid OrderGuid
		{
			get { return GetColumnValue<Guid>(Columns.OrderGuid); }
			set { SetColumnValue(Columns.OrderGuid, value); }
		}

		[XmlAttribute("OrderDetailGuid")]
		[Bindable(true)]
		public Guid OrderDetailGuid
		{
			get { return GetColumnValue<Guid>(Columns.OrderDetailGuid); }
			set { SetColumnValue(Columns.OrderDetailGuid, value); }
		}

		[XmlAttribute("ItemName")]
		[Bindable(true)]
		public string ItemName
		{
			get { return GetColumnValue<string>(Columns.ItemName); }
			set { SetColumnValue(Columns.ItemName, value); }
		}

		[XmlAttribute("Pcash")]
		[Bindable(true)]
		public int Pcash
		{
			get { return GetColumnValue<int>(Columns.Pcash); }
			set { SetColumnValue(Columns.Pcash, value); }
		}

		[XmlAttribute("Scash")]
		[Bindable(true)]
		public int Scash
		{
			get { return GetColumnValue<int>(Columns.Scash); }
			set { SetColumnValue(Columns.Scash, value); }
		}

		[XmlAttribute("Bcash")]
		[Bindable(true)]
		public int Bcash
		{
			get { return GetColumnValue<int>(Columns.Bcash); }
			set { SetColumnValue(Columns.Bcash, value); }
		}

		[XmlAttribute("CreditCard")]
		[Bindable(true)]
		public int CreditCard
		{
			get { return GetColumnValue<int>(Columns.CreditCard); }
			set { SetColumnValue(Columns.CreditCard, value); }
		}

		[XmlAttribute("ReportTrustedGuid")]
		[Bindable(true)]
		public Guid? ReportTrustedGuid
		{
			get { return GetColumnValue<Guid?>(Columns.ReportTrustedGuid); }
			set { SetColumnValue(Columns.ReportTrustedGuid, value); }
		}

		[XmlAttribute("ReportVerifiedGuid")]
		[Bindable(true)]
		public Guid? ReportVerifiedGuid
		{
			get { return GetColumnValue<Guid?>(Columns.ReportVerifiedGuid); }
			set { SetColumnValue(Columns.ReportVerifiedGuid, value); }
		}

		[XmlAttribute("DiscountAmount")]
		[Bindable(true)]
		public int DiscountAmount
		{
			get { return GetColumnValue<int>(Columns.DiscountAmount); }
			set { SetColumnValue(Columns.DiscountAmount, value); }
		}

		[XmlAttribute("SpecialStatus")]
		[Bindable(true)]
		public int SpecialStatus
		{
			get { return GetColumnValue<int>(Columns.SpecialStatus); }
			set { SetColumnValue(Columns.SpecialStatus, value); }
		}

		[XmlAttribute("Atm")]
		[Bindable(true)]
		public int Atm
		{
			get { return GetColumnValue<int>(Columns.Atm); }
			set { SetColumnValue(Columns.Atm, value); }
		}

		[XmlAttribute("StoreVerifiedGuid")]
		[Bindable(true)]
		public Guid? StoreVerifiedGuid
		{
			get { return GetColumnValue<Guid?>(Columns.StoreVerifiedGuid); }
			set { SetColumnValue(Columns.StoreVerifiedGuid, value); }
		}

		[XmlAttribute("SpecialOperatedTime")]
		[Bindable(true)]
		public DateTime? SpecialOperatedTime
		{
			get { return GetColumnValue<DateTime?>(Columns.SpecialOperatedTime); }
			set { SetColumnValue(Columns.SpecialOperatedTime, value); }
		}

		[XmlAttribute("OrderClassification")]
		[Bindable(true)]
		public int OrderClassification
		{
			get { return GetColumnValue<int>(Columns.OrderClassification); }
			set { SetColumnValue(Columns.OrderClassification, value); }
		}

		[XmlAttribute("BusinessHourGuid")]
		[Bindable(true)]
		public Guid? BusinessHourGuid
		{
			get { return GetColumnValue<Guid?>(Columns.BusinessHourGuid); }
			set { SetColumnValue(Columns.BusinessHourGuid, value); }
		}

		[XmlAttribute("CheckoutType")]
		[Bindable(true)]
		public int CheckoutType
		{
			get { return GetColumnValue<int>(Columns.CheckoutType); }
			set { SetColumnValue(Columns.CheckoutType, value); }
		}

		[XmlAttribute("Cost")]
		[Bindable(true)]
		public int Cost
		{
			get { return GetColumnValue<int>(Columns.Cost); }
			set { SetColumnValue(Columns.Cost, value); }
		}

		[XmlAttribute("ReceivableId")]
		[Bindable(true)]
		public int? ReceivableId
		{
			get { return GetColumnValue<int?>(Columns.ReceivableId); }
			set { SetColumnValue(Columns.ReceivableId, value); }
		}

		[XmlAttribute("DeliveryType")]
		[Bindable(true)]
		public int DeliveryType
		{
			get { return GetColumnValue<int>(Columns.DeliveryType); }
			set { SetColumnValue(Columns.DeliveryType, value); }
		}

		[XmlAttribute("Prefix")]
		[Bindable(true)]
		public string Prefix
		{
			get { return GetColumnValue<string>(Columns.Prefix); }
			set { SetColumnValue(Columns.Prefix, value); }
		}

		[XmlAttribute("UninvoicedAmount")]
		[Bindable(true)]
		public int UninvoicedAmount
		{
			get { return GetColumnValue<int>(Columns.UninvoicedAmount); }
			set { SetColumnValue(Columns.UninvoicedAmount, value); }
		}

		[XmlAttribute("StoreGuid")]
		[Bindable(true)]
		public Guid? StoreGuid
		{
			get { return GetColumnValue<Guid?>(Columns.StoreGuid); }
			set { SetColumnValue(Columns.StoreGuid, value); }
		}

		[XmlAttribute("UsageVerifiedTime")]
		[Bindable(true)]
		public DateTime? UsageVerifiedTime
		{
			get { return GetColumnValue<DateTime?>(Columns.UsageVerifiedTime); }
			set { SetColumnValue(Columns.UsageVerifiedTime, value); }
		}

		[XmlAttribute("Lcash")]
		[Bindable(true)]
		public int Lcash
		{
			get { return GetColumnValue<int>(Columns.Lcash); }
			set { SetColumnValue(Columns.Lcash, value); }
		}

		[XmlAttribute("ReturnedTime")]
		[Bindable(true)]
		public DateTime? ReturnedTime
		{
			get { return GetColumnValue<DateTime?>(Columns.ReturnedTime); }
			set { SetColumnValue(Columns.ReturnedTime, value); }
		}

		[XmlAttribute("Tcash")]
		[Bindable(true)]
		public int Tcash
		{
			get { return GetColumnValue<int>(Columns.Tcash); }
			set { SetColumnValue(Columns.Tcash, value); }
		}

		[XmlAttribute("ThirdPartyPayment")]
		[Bindable(true)]
		public byte ThirdPartyPayment
		{
			get { return GetColumnValue<byte>(Columns.ThirdPartyPayment); }
			set { SetColumnValue(Columns.ThirdPartyPayment, value); }
		}

		[XmlAttribute("VerifiedStoreGuid")]
		[Bindable(true)]
		public Guid? VerifiedStoreGuid
		{
			get { return GetColumnValue<Guid?>(Columns.VerifiedStoreGuid); }
			set { SetColumnValue(Columns.VerifiedStoreGuid, value); }
		}

		[XmlAttribute("TrustSequenceNumber")]
		[Bindable(true)]
		public string TrustSequenceNumber
		{
			get { return GetColumnValue<string>(Columns.TrustSequenceNumber); }
			set { SetColumnValue(Columns.TrustSequenceNumber, value); }
		}

		[XmlAttribute("UsageVerifyId")]
		[Bindable(true)]
		public string UsageVerifyId
		{
			get { return GetColumnValue<string>(Columns.UsageVerifyId); }
			set { SetColumnValue(Columns.UsageVerifyId, value); }
		}

		[XmlAttribute("FamilyIsp")]
		[Bindable(true)]
		public int FamilyIsp
		{
			get { return GetColumnValue<int>(Columns.FamilyIsp); }
			set { SetColumnValue(Columns.FamilyIsp, value); }
		}

		[XmlAttribute("SevenIsp")]
		[Bindable(true)]
		public int SevenIsp
		{
			get { return GetColumnValue<int>(Columns.SevenIsp); }
			set { SetColumnValue(Columns.SevenIsp, value); }
		}

		[XmlAttribute("Reserve01")]
		[Bindable(true)]
		public int Reserve01
		{
			get { return GetColumnValue<int>(Columns.Reserve01); }
			set { SetColumnValue(Columns.Reserve01, value); }
		}

		[XmlAttribute("Reserve02")]
		[Bindable(true)]
		public int Reserve02
		{
			get { return GetColumnValue<int>(Columns.Reserve02); }
			set { SetColumnValue(Columns.Reserve02, value); }
		}

		[XmlAttribute("Pscash")]
		[Bindable(true)]
		public int Pscash
		{
			get { return GetColumnValue<int>(Columns.Pscash); }
			set { SetColumnValue(Columns.Pscash, value); }
		}

		#endregion

		#region Typed Columns

		public static TableSchema.TableColumn TrustIdColumn
		{
			get { return Schema.Columns[0]; }
		}

		public static TableSchema.TableColumn AmountColumn
		{
			get { return Schema.Columns[1]; }
		}

		public static TableSchema.TableColumn TrustProviderColumn
		{
			get { return Schema.Columns[2]; }
		}

		public static TableSchema.TableColumn StatusColumn
		{
			get { return Schema.Columns[3]; }
		}

		public static TableSchema.TableColumn BankStatusColumn
		{
			get { return Schema.Columns[4]; }
		}

		public static TableSchema.TableColumn CreateTimeColumn
		{
			get { return Schema.Columns[5]; }
		}

		public static TableSchema.TableColumn ModifyTimeColumn
		{
			get { return Schema.Columns[6]; }
		}

		public static TableSchema.TableColumn TrustedTimeColumn
		{
			get { return Schema.Columns[7]; }
		}

		public static TableSchema.TableColumn TrustedBankTimeColumn
		{
			get { return Schema.Columns[8]; }
		}

		public static TableSchema.TableColumn VerifiedTimeColumn
		{
			get { return Schema.Columns[9]; }
		}

		public static TableSchema.TableColumn VerifiedBankTimeColumn
		{
			get { return Schema.Columns[10]; }
		}

		public static TableSchema.TableColumn CouponSequenceNumberColumn
		{
			get { return Schema.Columns[11]; }
		}

		public static TableSchema.TableColumn CouponIdColumn
		{
			get { return Schema.Columns[12]; }
		}

		public static TableSchema.TableColumn UserIdColumn
		{
			get { return Schema.Columns[13]; }
		}

		public static TableSchema.TableColumn OrderGuidColumn
		{
			get { return Schema.Columns[14]; }
		}

		public static TableSchema.TableColumn OrderDetailGuidColumn
		{
			get { return Schema.Columns[15]; }
		}

		public static TableSchema.TableColumn ItemNameColumn
		{
			get { return Schema.Columns[16]; }
		}

		public static TableSchema.TableColumn PcashColumn
		{
			get { return Schema.Columns[17]; }
		}

		public static TableSchema.TableColumn ScashColumn
		{
			get { return Schema.Columns[18]; }
		}

		public static TableSchema.TableColumn BcashColumn
		{
			get { return Schema.Columns[19]; }
		}

		public static TableSchema.TableColumn CreditCardColumn
		{
			get { return Schema.Columns[20]; }
		}

		public static TableSchema.TableColumn ReportTrustedGuidColumn
		{
			get { return Schema.Columns[21]; }
		}

		public static TableSchema.TableColumn ReportVerifiedGuidColumn
		{
			get { return Schema.Columns[22]; }
		}

		public static TableSchema.TableColumn DiscountAmountColumn
		{
			get { return Schema.Columns[23]; }
		}

		public static TableSchema.TableColumn SpecialStatusColumn
		{
			get { return Schema.Columns[24]; }
		}

		public static TableSchema.TableColumn AtmColumn
		{
			get { return Schema.Columns[25]; }
		}

		public static TableSchema.TableColumn StoreVerifiedGuidColumn
		{
			get { return Schema.Columns[26]; }
		}

		public static TableSchema.TableColumn SpecialOperatedTimeColumn
		{
			get { return Schema.Columns[27]; }
		}

		public static TableSchema.TableColumn OrderClassificationColumn
		{
			get { return Schema.Columns[28]; }
		}

		public static TableSchema.TableColumn BusinessHourGuidColumn
		{
			get { return Schema.Columns[29]; }
		}

		public static TableSchema.TableColumn CheckoutTypeColumn
		{
			get { return Schema.Columns[30]; }
		}

		public static TableSchema.TableColumn CostColumn
		{
			get { return Schema.Columns[31]; }
		}

		public static TableSchema.TableColumn ReceivableIdColumn
		{
			get { return Schema.Columns[32]; }
		}

		public static TableSchema.TableColumn DeliveryTypeColumn
		{
			get { return Schema.Columns[33]; }
		}

		public static TableSchema.TableColumn PrefixColumn
		{
			get { return Schema.Columns[34]; }
		}

		public static TableSchema.TableColumn UninvoicedAmountColumn
		{
			get { return Schema.Columns[35]; }
		}

		public static TableSchema.TableColumn StoreGuidColumn
		{
			get { return Schema.Columns[36]; }
		}

		public static TableSchema.TableColumn UsageVerifiedTimeColumn
		{
			get { return Schema.Columns[37]; }
		}

		public static TableSchema.TableColumn LcashColumn
		{
			get { return Schema.Columns[38]; }
		}

		public static TableSchema.TableColumn ReturnedTimeColumn
		{
			get { return Schema.Columns[39]; }
		}

		public static TableSchema.TableColumn TcashColumn
		{
			get { return Schema.Columns[40]; }
		}

		public static TableSchema.TableColumn ThirdPartyPaymentColumn
		{
			get { return Schema.Columns[41]; }
		}

		public static TableSchema.TableColumn VerifiedStoreGuidColumn
		{
			get { return Schema.Columns[42]; }
		}

		public static TableSchema.TableColumn TrustSequenceNumberColumn
		{
			get { return Schema.Columns[43]; }
		}

		public static TableSchema.TableColumn UsageVerifyIdColumn
		{
			get { return Schema.Columns[44]; }
		}

		public static TableSchema.TableColumn FamilyIspColumn
		{
			get { return Schema.Columns[45]; }
		}

		public static TableSchema.TableColumn SevenIspColumn
		{
			get { return Schema.Columns[46]; }
		}

		public static TableSchema.TableColumn Reserve01Column
		{
			get { return Schema.Columns[47]; }
		}

		public static TableSchema.TableColumn Reserve02Column
		{
			get { return Schema.Columns[48]; }
		}

		public static TableSchema.TableColumn PscashColumn
		{
			get { return Schema.Columns[49]; }
		}

		#endregion

		#region Columns Struct

		public struct Columns
		{
			public static string TrustId = @"trust_id";
			public static string Amount = @"amount";
			public static string TrustProvider = @"trust_provider";
			public static string Status = @"status";
			public static string BankStatus = @"bank_status";
			public static string CreateTime = @"create_time";
			public static string ModifyTime = @"modify_time";
			public static string TrustedTime = @"trusted_time";
			public static string TrustedBankTime = @"trusted_bank_time";
			public static string VerifiedTime = @"verified_time";
			public static string VerifiedBankTime = @"verified_bank_time";
			public static string CouponSequenceNumber = @"coupon_sequence_number";
			public static string CouponId = @"coupon_id";
			public static string UserId = @"user_id";
			public static string OrderGuid = @"order_guid";
			public static string OrderDetailGuid = @"order_detail_guid";
			public static string ItemName = @"item_name";
			public static string Pcash = @"pcash";
			public static string Scash = @"scash";
			public static string Bcash = @"bcash";
			public static string CreditCard = @"credit_card";
			public static string ReportTrustedGuid = @"report_trusted_guid";
			public static string ReportVerifiedGuid = @"report_verified_guid";
			public static string DiscountAmount = @"discount_amount";
			public static string SpecialStatus = @"special_status";
			public static string Atm = @"atm";
			public static string StoreVerifiedGuid = @"store_verified_guid";
			public static string SpecialOperatedTime = @"special_operated_time";
			public static string OrderClassification = @"order_classification";
			public static string BusinessHourGuid = @"business_hour_guid";
			public static string CheckoutType = @"checkout_type";
			public static string Cost = @"cost";
			public static string ReceivableId = @"receivable_id";
			public static string DeliveryType = @"delivery_type";
			public static string Prefix = @"prefix";
			public static string UninvoicedAmount = @"uninvoiced_amount";
			public static string StoreGuid = @"store_guid";
			public static string UsageVerifiedTime = @"usage_verified_time";
			public static string Lcash = @"lcash";
			public static string ReturnedTime = @"returned_time";
			public static string Tcash = @"tcash";
			public static string ThirdPartyPayment = @"third_party_payment";
			public static string VerifiedStoreGuid = @"verified_store_guid";
			public static string TrustSequenceNumber = @"trust_sequence_number";
			public static string UsageVerifyId = @"usage_verify_id";
			public static string FamilyIsp = @"family_isp";
			public static string SevenIsp = @"seven_isp";
			public static string Reserve01 = @"reserve01";
			public static string Reserve02 = @"reserve02";
			public static string Pscash = @"pscash";
		}

		#endregion

	}
}

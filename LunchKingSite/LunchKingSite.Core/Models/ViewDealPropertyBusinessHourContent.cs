using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewDealPropertyBusinessHourContent class.
    /// </summary>
    [Serializable]
    public partial class ViewDealPropertyBusinessHourContentCollection : ReadOnlyList<ViewDealPropertyBusinessHourContent, ViewDealPropertyBusinessHourContentCollection>
    {        
        public ViewDealPropertyBusinessHourContentCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_deal_property_business_hour_content view.
    /// </summary>
    [Serializable]
    public partial class ViewDealPropertyBusinessHourContent : ReadOnlyRecord<ViewDealPropertyBusinessHourContent>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_deal_property_business_hour_content", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarBusinessHourGuid = new TableSchema.TableColumn(schema);
                colvarBusinessHourGuid.ColumnName = "business_hour_guid";
                colvarBusinessHourGuid.DataType = DbType.Guid;
                colvarBusinessHourGuid.MaxLength = 0;
                colvarBusinessHourGuid.AutoIncrement = false;
                colvarBusinessHourGuid.IsNullable = false;
                colvarBusinessHourGuid.IsPrimaryKey = false;
                colvarBusinessHourGuid.IsForeignKey = false;
                colvarBusinessHourGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourGuid);
                
                TableSchema.TableColumn colvarBusinessHourOrderTimeS = new TableSchema.TableColumn(schema);
                colvarBusinessHourOrderTimeS.ColumnName = "business_hour_order_time_s";
                colvarBusinessHourOrderTimeS.DataType = DbType.DateTime;
                colvarBusinessHourOrderTimeS.MaxLength = 0;
                colvarBusinessHourOrderTimeS.AutoIncrement = false;
                colvarBusinessHourOrderTimeS.IsNullable = false;
                colvarBusinessHourOrderTimeS.IsPrimaryKey = false;
                colvarBusinessHourOrderTimeS.IsForeignKey = false;
                colvarBusinessHourOrderTimeS.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourOrderTimeS);
                
                TableSchema.TableColumn colvarBusinessHourOrderTimeE = new TableSchema.TableColumn(schema);
                colvarBusinessHourOrderTimeE.ColumnName = "business_hour_order_time_e";
                colvarBusinessHourOrderTimeE.DataType = DbType.DateTime;
                colvarBusinessHourOrderTimeE.MaxLength = 0;
                colvarBusinessHourOrderTimeE.AutoIncrement = false;
                colvarBusinessHourOrderTimeE.IsNullable = false;
                colvarBusinessHourOrderTimeE.IsPrimaryKey = false;
                colvarBusinessHourOrderTimeE.IsForeignKey = false;
                colvarBusinessHourOrderTimeE.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourOrderTimeE);
                
                TableSchema.TableColumn colvarBusinessHourDeliverTimeS = new TableSchema.TableColumn(schema);
                colvarBusinessHourDeliverTimeS.ColumnName = "business_hour_deliver_time_s";
                colvarBusinessHourDeliverTimeS.DataType = DbType.DateTime;
                colvarBusinessHourDeliverTimeS.MaxLength = 0;
                colvarBusinessHourDeliverTimeS.AutoIncrement = false;
                colvarBusinessHourDeliverTimeS.IsNullable = true;
                colvarBusinessHourDeliverTimeS.IsPrimaryKey = false;
                colvarBusinessHourDeliverTimeS.IsForeignKey = false;
                colvarBusinessHourDeliverTimeS.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourDeliverTimeS);
                
                TableSchema.TableColumn colvarBusinessHourDeliverTimeE = new TableSchema.TableColumn(schema);
                colvarBusinessHourDeliverTimeE.ColumnName = "business_hour_deliver_time_e";
                colvarBusinessHourDeliverTimeE.DataType = DbType.DateTime;
                colvarBusinessHourDeliverTimeE.MaxLength = 0;
                colvarBusinessHourDeliverTimeE.AutoIncrement = false;
                colvarBusinessHourDeliverTimeE.IsNullable = true;
                colvarBusinessHourDeliverTimeE.IsPrimaryKey = false;
                colvarBusinessHourDeliverTimeE.IsForeignKey = false;
                colvarBusinessHourDeliverTimeE.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourDeliverTimeE);
                
                TableSchema.TableColumn colvarBusinessHourOrderMinimum = new TableSchema.TableColumn(schema);
                colvarBusinessHourOrderMinimum.ColumnName = "business_hour_order_minimum";
                colvarBusinessHourOrderMinimum.DataType = DbType.Currency;
                colvarBusinessHourOrderMinimum.MaxLength = 0;
                colvarBusinessHourOrderMinimum.AutoIncrement = false;
                colvarBusinessHourOrderMinimum.IsNullable = false;
                colvarBusinessHourOrderMinimum.IsPrimaryKey = false;
                colvarBusinessHourOrderMinimum.IsForeignKey = false;
                colvarBusinessHourOrderMinimum.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourOrderMinimum);
                
                TableSchema.TableColumn colvarChangedExpireDate = new TableSchema.TableColumn(schema);
                colvarChangedExpireDate.ColumnName = "changed_expire_date";
                colvarChangedExpireDate.DataType = DbType.DateTime;
                colvarChangedExpireDate.MaxLength = 0;
                colvarChangedExpireDate.AutoIncrement = false;
                colvarChangedExpireDate.IsNullable = true;
                colvarChangedExpireDate.IsPrimaryKey = false;
                colvarChangedExpireDate.IsForeignKey = false;
                colvarChangedExpireDate.IsReadOnly = false;
                
                schema.Columns.Add(colvarChangedExpireDate);
                
                TableSchema.TableColumn colvarBusinessHourStatus = new TableSchema.TableColumn(schema);
                colvarBusinessHourStatus.ColumnName = "business_hour_status";
                colvarBusinessHourStatus.DataType = DbType.Int32;
                colvarBusinessHourStatus.MaxLength = 0;
                colvarBusinessHourStatus.AutoIncrement = false;
                colvarBusinessHourStatus.IsNullable = false;
                colvarBusinessHourStatus.IsPrimaryKey = false;
                colvarBusinessHourStatus.IsForeignKey = false;
                colvarBusinessHourStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourStatus);
                
                TableSchema.TableColumn colvarDealName = new TableSchema.TableColumn(schema);
                colvarDealName.ColumnName = "deal_name";
                colvarDealName.DataType = DbType.String;
                colvarDealName.MaxLength = 400;
                colvarDealName.AutoIncrement = false;
                colvarDealName.IsNullable = true;
                colvarDealName.IsPrimaryKey = false;
                colvarDealName.IsForeignKey = false;
                colvarDealName.IsReadOnly = false;
                
                schema.Columns.Add(colvarDealName);
                
                TableSchema.TableColumn colvarImagePath = new TableSchema.TableColumn(schema);
                colvarImagePath.ColumnName = "image_path";
                colvarImagePath.DataType = DbType.String;
                colvarImagePath.MaxLength = 1000;
                colvarImagePath.AutoIncrement = false;
                colvarImagePath.IsNullable = true;
                colvarImagePath.IsPrimaryKey = false;
                colvarImagePath.IsForeignKey = false;
                colvarImagePath.IsReadOnly = false;
                
                schema.Columns.Add(colvarImagePath);
                
                TableSchema.TableColumn colvarCouponUsage = new TableSchema.TableColumn(schema);
                colvarCouponUsage.ColumnName = "coupon_usage";
                colvarCouponUsage.DataType = DbType.String;
                colvarCouponUsage.MaxLength = 500;
                colvarCouponUsage.AutoIncrement = false;
                colvarCouponUsage.IsNullable = true;
                colvarCouponUsage.IsPrimaryKey = false;
                colvarCouponUsage.IsForeignKey = false;
                colvarCouponUsage.IsReadOnly = false;
                
                schema.Columns.Add(colvarCouponUsage);
                
                TableSchema.TableColumn colvarAvailability = new TableSchema.TableColumn(schema);
                colvarAvailability.ColumnName = "availability";
                colvarAvailability.DataType = DbType.String;
                colvarAvailability.MaxLength = 1073741823;
                colvarAvailability.AutoIncrement = false;
                colvarAvailability.IsNullable = true;
                colvarAvailability.IsPrimaryKey = false;
                colvarAvailability.IsForeignKey = false;
                colvarAvailability.IsReadOnly = false;
                
                schema.Columns.Add(colvarAvailability);
                
                TableSchema.TableColumn colvarTitle = new TableSchema.TableColumn(schema);
                colvarTitle.ColumnName = "title";
                colvarTitle.DataType = DbType.String;
                colvarTitle.MaxLength = 100;
                colvarTitle.AutoIncrement = false;
                colvarTitle.IsNullable = true;
                colvarTitle.IsPrimaryKey = false;
                colvarTitle.IsForeignKey = false;
                colvarTitle.IsReadOnly = false;
                
                schema.Columns.Add(colvarTitle);
                
                TableSchema.TableColumn colvarDealType = new TableSchema.TableColumn(schema);
                colvarDealType.ColumnName = "deal_type";
                colvarDealType.DataType = DbType.Int32;
                colvarDealType.MaxLength = 0;
                colvarDealType.AutoIncrement = false;
                colvarDealType.IsNullable = true;
                colvarDealType.IsPrimaryKey = false;
                colvarDealType.IsForeignKey = false;
                colvarDealType.IsReadOnly = false;
                
                schema.Columns.Add(colvarDealType);
                
                TableSchema.TableColumn colvarSellerVerifyType = new TableSchema.TableColumn(schema);
                colvarSellerVerifyType.ColumnName = "seller_verify_type";
                colvarSellerVerifyType.DataType = DbType.Int32;
                colvarSellerVerifyType.MaxLength = 0;
                colvarSellerVerifyType.AutoIncrement = false;
                colvarSellerVerifyType.IsNullable = true;
                colvarSellerVerifyType.IsPrimaryKey = false;
                colvarSellerVerifyType.IsForeignKey = false;
                colvarSellerVerifyType.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerVerifyType);
                
                TableSchema.TableColumn colvarDealAccBusinessGroupId = new TableSchema.TableColumn(schema);
                colvarDealAccBusinessGroupId.ColumnName = "deal_acc_business_group_id";
                colvarDealAccBusinessGroupId.DataType = DbType.Int32;
                colvarDealAccBusinessGroupId.MaxLength = 0;
                colvarDealAccBusinessGroupId.AutoIncrement = false;
                colvarDealAccBusinessGroupId.IsNullable = true;
                colvarDealAccBusinessGroupId.IsPrimaryKey = false;
                colvarDealAccBusinessGroupId.IsForeignKey = false;
                colvarDealAccBusinessGroupId.IsReadOnly = false;
                
                schema.Columns.Add(colvarDealAccBusinessGroupId);
                
                TableSchema.TableColumn colvarUniqueId = new TableSchema.TableColumn(schema);
                colvarUniqueId.ColumnName = "unique_id";
                colvarUniqueId.DataType = DbType.Int32;
                colvarUniqueId.MaxLength = 0;
                colvarUniqueId.AutoIncrement = false;
                colvarUniqueId.IsNullable = false;
                colvarUniqueId.IsPrimaryKey = false;
                colvarUniqueId.IsForeignKey = false;
                colvarUniqueId.IsReadOnly = false;
                
                schema.Columns.Add(colvarUniqueId);
                
                TableSchema.TableColumn colvarCityList = new TableSchema.TableColumn(schema);
                colvarCityList.ColumnName = "city_list";
                colvarCityList.DataType = DbType.AnsiString;
                colvarCityList.MaxLength = 255;
                colvarCityList.AutoIncrement = false;
                colvarCityList.IsNullable = true;
                colvarCityList.IsPrimaryKey = false;
                colvarCityList.IsForeignKey = false;
                colvarCityList.IsReadOnly = false;
                
                schema.Columns.Add(colvarCityList);
                
                TableSchema.TableColumn colvarDeliveryType = new TableSchema.TableColumn(schema);
                colvarDeliveryType.ColumnName = "delivery_type";
                colvarDeliveryType.DataType = DbType.Int32;
                colvarDeliveryType.MaxLength = 0;
                colvarDeliveryType.AutoIncrement = false;
                colvarDeliveryType.IsNullable = true;
                colvarDeliveryType.IsPrimaryKey = false;
                colvarDeliveryType.IsForeignKey = false;
                colvarDeliveryType.IsReadOnly = false;
                
                schema.Columns.Add(colvarDeliveryType);
                
                TableSchema.TableColumn colvarShoppingCart = new TableSchema.TableColumn(schema);
                colvarShoppingCart.ColumnName = "shopping_cart";
                colvarShoppingCart.DataType = DbType.Boolean;
                colvarShoppingCart.MaxLength = 0;
                colvarShoppingCart.AutoIncrement = false;
                colvarShoppingCart.IsNullable = false;
                colvarShoppingCart.IsPrimaryKey = false;
                colvarShoppingCart.IsForeignKey = false;
                colvarShoppingCart.IsReadOnly = false;
                
                schema.Columns.Add(colvarShoppingCart);
                
                TableSchema.TableColumn colvarComboPackCount = new TableSchema.TableColumn(schema);
                colvarComboPackCount.ColumnName = "combo_pack_count";
                colvarComboPackCount.DataType = DbType.Int32;
                colvarComboPackCount.MaxLength = 0;
                colvarComboPackCount.AutoIncrement = false;
                colvarComboPackCount.IsNullable = false;
                colvarComboPackCount.IsPrimaryKey = false;
                colvarComboPackCount.IsForeignKey = false;
                colvarComboPackCount.IsReadOnly = false;
                
                schema.Columns.Add(colvarComboPackCount);
                
                TableSchema.TableColumn colvarSlug = new TableSchema.TableColumn(schema);
                colvarSlug.ColumnName = "slug";
                colvarSlug.DataType = DbType.String;
                colvarSlug.MaxLength = 100;
                colvarSlug.AutoIncrement = false;
                colvarSlug.IsNullable = true;
                colvarSlug.IsPrimaryKey = false;
                colvarSlug.IsForeignKey = false;
                colvarSlug.IsReadOnly = false;
                
                schema.Columns.Add(colvarSlug);
                
                TableSchema.TableColumn colvarGroupOrderStatus = new TableSchema.TableColumn(schema);
                colvarGroupOrderStatus.ColumnName = "group_order_status";
                colvarGroupOrderStatus.DataType = DbType.Int32;
                colvarGroupOrderStatus.MaxLength = 0;
                colvarGroupOrderStatus.AutoIncrement = false;
                colvarGroupOrderStatus.IsNullable = true;
                colvarGroupOrderStatus.IsPrimaryKey = false;
                colvarGroupOrderStatus.IsForeignKey = false;
                colvarGroupOrderStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarGroupOrderStatus);
                
                TableSchema.TableColumn colvarDeEmpName = new TableSchema.TableColumn(schema);
                colvarDeEmpName.ColumnName = "de_emp_name";
                colvarDeEmpName.DataType = DbType.String;
                colvarDeEmpName.MaxLength = 50;
                colvarDeEmpName.AutoIncrement = false;
                colvarDeEmpName.IsNullable = true;
                colvarDeEmpName.IsPrimaryKey = false;
                colvarDeEmpName.IsForeignKey = false;
                colvarDeEmpName.IsReadOnly = false;
                
                schema.Columns.Add(colvarDeEmpName);
                
                TableSchema.TableColumn colvarOpEmpName = new TableSchema.TableColumn(schema);
                colvarOpEmpName.ColumnName = "op_emp_name";
                colvarOpEmpName.DataType = DbType.String;
                colvarOpEmpName.MaxLength = 50;
                colvarOpEmpName.AutoIncrement = false;
                colvarOpEmpName.IsNullable = true;
                colvarOpEmpName.IsPrimaryKey = false;
                colvarOpEmpName.IsForeignKey = false;
                colvarOpEmpName.IsReadOnly = false;
                
                schema.Columns.Add(colvarOpEmpName);
                
                TableSchema.TableColumn colvarEmail = new TableSchema.TableColumn(schema);
                colvarEmail.ColumnName = "email";
                colvarEmail.DataType = DbType.String;
                colvarEmail.MaxLength = 256;
                colvarEmail.AutoIncrement = false;
                colvarEmail.IsNullable = true;
                colvarEmail.IsPrimaryKey = false;
                colvarEmail.IsForeignKey = false;
                colvarEmail.IsReadOnly = false;
                
                schema.Columns.Add(colvarEmail);
                
                TableSchema.TableColumn colvarSellerEmail = new TableSchema.TableColumn(schema);
                colvarSellerEmail.ColumnName = "seller_email";
                colvarSellerEmail.DataType = DbType.AnsiString;
                colvarSellerEmail.MaxLength = 200;
                colvarSellerEmail.AutoIncrement = false;
                colvarSellerEmail.IsNullable = true;
                colvarSellerEmail.IsPrimaryKey = false;
                colvarSellerEmail.IsForeignKey = false;
                colvarSellerEmail.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerEmail);
                
                TableSchema.TableColumn colvarReturnedPersonEmail = new TableSchema.TableColumn(schema);
                colvarReturnedPersonEmail.ColumnName = "returned_person_email";
                colvarReturnedPersonEmail.DataType = DbType.AnsiString;
                colvarReturnedPersonEmail.MaxLength = 200;
                colvarReturnedPersonEmail.AutoIncrement = false;
                colvarReturnedPersonEmail.IsNullable = true;
                colvarReturnedPersonEmail.IsPrimaryKey = false;
                colvarReturnedPersonEmail.IsForeignKey = false;
                colvarReturnedPersonEmail.IsReadOnly = false;
                
                schema.Columns.Add(colvarReturnedPersonEmail);
                
                TableSchema.TableColumn colvarSellerName = new TableSchema.TableColumn(schema);
                colvarSellerName.ColumnName = "seller_name";
                colvarSellerName.DataType = DbType.String;
                colvarSellerName.MaxLength = 100;
                colvarSellerName.AutoIncrement = false;
                colvarSellerName.IsNullable = false;
                colvarSellerName.IsPrimaryKey = false;
                colvarSellerName.IsForeignKey = false;
                colvarSellerName.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerName);
                
                TableSchema.TableColumn colvarVendorBillingModel = new TableSchema.TableColumn(schema);
                colvarVendorBillingModel.ColumnName = "vendor_billing_model";
                colvarVendorBillingModel.DataType = DbType.Int32;
                colvarVendorBillingModel.MaxLength = 0;
                colvarVendorBillingModel.AutoIncrement = false;
                colvarVendorBillingModel.IsNullable = false;
                colvarVendorBillingModel.IsPrimaryKey = false;
                colvarVendorBillingModel.IsForeignKey = false;
                colvarVendorBillingModel.IsReadOnly = false;
                
                schema.Columns.Add(colvarVendorBillingModel);
                
                TableSchema.TableColumn colvarPaytocompany = new TableSchema.TableColumn(schema);
                colvarPaytocompany.ColumnName = "paytocompany";
                colvarPaytocompany.DataType = DbType.Int32;
                colvarPaytocompany.MaxLength = 0;
                colvarPaytocompany.AutoIncrement = false;
                colvarPaytocompany.IsNullable = false;
                colvarPaytocompany.IsPrimaryKey = false;
                colvarPaytocompany.IsForeignKey = false;
                colvarPaytocompany.IsReadOnly = false;
                
                schema.Columns.Add(colvarPaytocompany);
                
                TableSchema.TableColumn colvarCompanyEmail = new TableSchema.TableColumn(schema);
                colvarCompanyEmail.ColumnName = "CompanyEmail";
                colvarCompanyEmail.DataType = DbType.String;
                colvarCompanyEmail.MaxLength = 250;
                colvarCompanyEmail.AutoIncrement = false;
                colvarCompanyEmail.IsNullable = true;
                colvarCompanyEmail.IsPrimaryKey = false;
                colvarCompanyEmail.IsForeignKey = false;
                colvarCompanyEmail.IsReadOnly = false;
                
                schema.Columns.Add(colvarCompanyEmail);
                
                TableSchema.TableColumn colvarSellerGuid = new TableSchema.TableColumn(schema);
                colvarSellerGuid.ColumnName = "seller_guid";
                colvarSellerGuid.DataType = DbType.Guid;
                colvarSellerGuid.MaxLength = 0;
                colvarSellerGuid.AutoIncrement = false;
                colvarSellerGuid.IsNullable = false;
                colvarSellerGuid.IsPrimaryKey = false;
                colvarSellerGuid.IsForeignKey = false;
                colvarSellerGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerGuid);
                
                TableSchema.TableColumn colvarRemittanceType = new TableSchema.TableColumn(schema);
                colvarRemittanceType.ColumnName = "remittance_type";
                colvarRemittanceType.DataType = DbType.Int32;
                colvarRemittanceType.MaxLength = 0;
                colvarRemittanceType.AutoIncrement = false;
                colvarRemittanceType.IsNullable = false;
                colvarRemittanceType.IsPrimaryKey = false;
                colvarRemittanceType.IsForeignKey = false;
                colvarRemittanceType.IsReadOnly = false;
                
                schema.Columns.Add(colvarRemittanceType);
                
                TableSchema.TableColumn colvarReturnedPersonName = new TableSchema.TableColumn(schema);
                colvarReturnedPersonName.ColumnName = "returned_person_name";
                colvarReturnedPersonName.DataType = DbType.String;
                colvarReturnedPersonName.MaxLength = 100;
                colvarReturnedPersonName.AutoIncrement = false;
                colvarReturnedPersonName.IsNullable = true;
                colvarReturnedPersonName.IsPrimaryKey = false;
                colvarReturnedPersonName.IsForeignKey = false;
                colvarReturnedPersonName.IsReadOnly = false;
                
                schema.Columns.Add(colvarReturnedPersonName);
                
                TableSchema.TableColumn colvarReturnedPersonTel = new TableSchema.TableColumn(schema);
                colvarReturnedPersonTel.ColumnName = "returned_person_tel";
                colvarReturnedPersonTel.DataType = DbType.AnsiString;
                colvarReturnedPersonTel.MaxLength = 100;
                colvarReturnedPersonTel.AutoIncrement = false;
                colvarReturnedPersonTel.IsNullable = true;
                colvarReturnedPersonTel.IsPrimaryKey = false;
                colvarReturnedPersonTel.IsForeignKey = false;
                colvarReturnedPersonTel.IsReadOnly = false;
                
                schema.Columns.Add(colvarReturnedPersonTel);
                
                TableSchema.TableColumn colvarShippedDate = new TableSchema.TableColumn(schema);
                colvarShippedDate.ColumnName = "shipped_date";
                colvarShippedDate.DataType = DbType.DateTime;
                colvarShippedDate.MaxLength = 0;
                colvarShippedDate.AutoIncrement = false;
                colvarShippedDate.IsNullable = true;
                colvarShippedDate.IsPrimaryKey = false;
                colvarShippedDate.IsForeignKey = false;
                colvarShippedDate.IsReadOnly = false;
                
                schema.Columns.Add(colvarShippedDate);
                
                TableSchema.TableColumn colvarOrderedQuantity = new TableSchema.TableColumn(schema);
                colvarOrderedQuantity.ColumnName = "ordered_quantity";
                colvarOrderedQuantity.DataType = DbType.Int32;
                colvarOrderedQuantity.MaxLength = 0;
                colvarOrderedQuantity.AutoIncrement = false;
                colvarOrderedQuantity.IsNullable = true;
                colvarOrderedQuantity.IsPrimaryKey = false;
                colvarOrderedQuantity.IsForeignKey = false;
                colvarOrderedQuantity.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderedQuantity);
                
                TableSchema.TableColumn colvarVendorReceiptType = new TableSchema.TableColumn(schema);
                colvarVendorReceiptType.ColumnName = "vendor_receipt_type";
                colvarVendorReceiptType.DataType = DbType.Int32;
                colvarVendorReceiptType.MaxLength = 0;
                colvarVendorReceiptType.AutoIncrement = false;
                colvarVendorReceiptType.IsNullable = false;
                colvarVendorReceiptType.IsPrimaryKey = false;
                colvarVendorReceiptType.IsForeignKey = false;
                colvarVendorReceiptType.IsReadOnly = false;
                
                schema.Columns.Add(colvarVendorReceiptType);
                
                TableSchema.TableColumn colvarFinalBalanceSheetDate = new TableSchema.TableColumn(schema);
                colvarFinalBalanceSheetDate.ColumnName = "final_balance_sheet_date";
                colvarFinalBalanceSheetDate.DataType = DbType.DateTime;
                colvarFinalBalanceSheetDate.MaxLength = 0;
                colvarFinalBalanceSheetDate.AutoIncrement = false;
                colvarFinalBalanceSheetDate.IsNullable = true;
                colvarFinalBalanceSheetDate.IsPrimaryKey = false;
                colvarFinalBalanceSheetDate.IsForeignKey = false;
                colvarFinalBalanceSheetDate.IsReadOnly = false;
                
                schema.Columns.Add(colvarFinalBalanceSheetDate);
                
                TableSchema.TableColumn colvarBalanceSheetCreateDate = new TableSchema.TableColumn(schema);
                colvarBalanceSheetCreateDate.ColumnName = "balance_sheet_create_date";
                colvarBalanceSheetCreateDate.DataType = DbType.DateTime;
                colvarBalanceSheetCreateDate.MaxLength = 0;
                colvarBalanceSheetCreateDate.AutoIncrement = false;
                colvarBalanceSheetCreateDate.IsNullable = true;
                colvarBalanceSheetCreateDate.IsPrimaryKey = false;
                colvarBalanceSheetCreateDate.IsForeignKey = false;
                colvarBalanceSheetCreateDate.IsReadOnly = false;
                
                schema.Columns.Add(colvarBalanceSheetCreateDate);
                
                TableSchema.TableColumn colvarPartiallyPaymentDate = new TableSchema.TableColumn(schema);
                colvarPartiallyPaymentDate.ColumnName = "partially_payment_date";
                colvarPartiallyPaymentDate.DataType = DbType.DateTime;
                colvarPartiallyPaymentDate.MaxLength = 0;
                colvarPartiallyPaymentDate.AutoIncrement = false;
                colvarPartiallyPaymentDate.IsNullable = true;
                colvarPartiallyPaymentDate.IsPrimaryKey = false;
                colvarPartiallyPaymentDate.IsForeignKey = false;
                colvarPartiallyPaymentDate.IsReadOnly = false;
                
                schema.Columns.Add(colvarPartiallyPaymentDate);
                
                TableSchema.TableColumn colvarIsTax = new TableSchema.TableColumn(schema);
                colvarIsTax.ColumnName = "is_tax";
                colvarIsTax.DataType = DbType.Boolean;
                colvarIsTax.MaxLength = 0;
                colvarIsTax.AutoIncrement = false;
                colvarIsTax.IsNullable = false;
                colvarIsTax.IsPrimaryKey = false;
                colvarIsTax.IsForeignKey = false;
                colvarIsTax.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsTax);
                
                TableSchema.TableColumn colvarCompanyName = new TableSchema.TableColumn(schema);
                colvarCompanyName.ColumnName = "CompanyName";
                colvarCompanyName.DataType = DbType.String;
                colvarCompanyName.MaxLength = 200;
                colvarCompanyName.AutoIncrement = false;
                colvarCompanyName.IsNullable = true;
                colvarCompanyName.IsPrimaryKey = false;
                colvarCompanyName.IsForeignKey = false;
                colvarCompanyName.IsReadOnly = false;
                
                schema.Columns.Add(colvarCompanyName);
                
                TableSchema.TableColumn colvarSignCompanyID = new TableSchema.TableColumn(schema);
                colvarSignCompanyID.ColumnName = "SignCompanyID";
                colvarSignCompanyID.DataType = DbType.AnsiString;
                colvarSignCompanyID.MaxLength = 20;
                colvarSignCompanyID.AutoIncrement = false;
                colvarSignCompanyID.IsNullable = true;
                colvarSignCompanyID.IsPrimaryKey = false;
                colvarSignCompanyID.IsForeignKey = false;
                colvarSignCompanyID.IsReadOnly = false;
                
                schema.Columns.Add(colvarSignCompanyID);
                
                TableSchema.TableColumn colvarCompanyBossName = new TableSchema.TableColumn(schema);
                colvarCompanyBossName.ColumnName = "CompanyBossName";
                colvarCompanyBossName.DataType = DbType.String;
                colvarCompanyBossName.MaxLength = 100;
                colvarCompanyBossName.AutoIncrement = false;
                colvarCompanyBossName.IsNullable = true;
                colvarCompanyBossName.IsPrimaryKey = false;
                colvarCompanyBossName.IsForeignKey = false;
                colvarCompanyBossName.IsReadOnly = false;
                
                schema.Columns.Add(colvarCompanyBossName);
                
                TableSchema.TableColumn colvarCompanyAccountName = new TableSchema.TableColumn(schema);
                colvarCompanyAccountName.ColumnName = "CompanyAccountName";
                colvarCompanyAccountName.DataType = DbType.String;
                colvarCompanyAccountName.MaxLength = 100;
                colvarCompanyAccountName.AutoIncrement = false;
                colvarCompanyAccountName.IsNullable = true;
                colvarCompanyAccountName.IsPrimaryKey = false;
                colvarCompanyAccountName.IsForeignKey = false;
                colvarCompanyAccountName.IsReadOnly = false;
                
                schema.Columns.Add(colvarCompanyAccountName);
                
                TableSchema.TableColumn colvarCompanyID = new TableSchema.TableColumn(schema);
                colvarCompanyID.ColumnName = "CompanyID";
                colvarCompanyID.DataType = DbType.AnsiString;
                colvarCompanyID.MaxLength = 20;
                colvarCompanyID.AutoIncrement = false;
                colvarCompanyID.IsNullable = true;
                colvarCompanyID.IsPrimaryKey = false;
                colvarCompanyID.IsForeignKey = false;
                colvarCompanyID.IsReadOnly = false;
                
                schema.Columns.Add(colvarCompanyID);
                
                TableSchema.TableColumn colvarCompanyAccount = new TableSchema.TableColumn(schema);
                colvarCompanyAccount.ColumnName = "CompanyAccount";
                colvarCompanyAccount.DataType = DbType.AnsiString;
                colvarCompanyAccount.MaxLength = 50;
                colvarCompanyAccount.AutoIncrement = false;
                colvarCompanyAccount.IsNullable = true;
                colvarCompanyAccount.IsPrimaryKey = false;
                colvarCompanyAccount.IsForeignKey = false;
                colvarCompanyAccount.IsReadOnly = false;
                
                schema.Columns.Add(colvarCompanyAccount);
                
                TableSchema.TableColumn colvarCompanyBankCode = new TableSchema.TableColumn(schema);
                colvarCompanyBankCode.ColumnName = "CompanyBankCode";
                colvarCompanyBankCode.DataType = DbType.AnsiString;
                colvarCompanyBankCode.MaxLength = 20;
                colvarCompanyBankCode.AutoIncrement = false;
                colvarCompanyBankCode.IsNullable = true;
                colvarCompanyBankCode.IsPrimaryKey = false;
                colvarCompanyBankCode.IsForeignKey = false;
                colvarCompanyBankCode.IsReadOnly = false;
                
                schema.Columns.Add(colvarCompanyBankCode);
                
                TableSchema.TableColumn colvarCompanyBranchCode = new TableSchema.TableColumn(schema);
                colvarCompanyBranchCode.ColumnName = "CompanyBranchCode";
                colvarCompanyBranchCode.DataType = DbType.AnsiString;
                colvarCompanyBranchCode.MaxLength = 20;
                colvarCompanyBranchCode.AutoIncrement = false;
                colvarCompanyBranchCode.IsNullable = true;
                colvarCompanyBranchCode.IsPrimaryKey = false;
                colvarCompanyBranchCode.IsForeignKey = false;
                colvarCompanyBranchCode.IsReadOnly = false;
                
                schema.Columns.Add(colvarCompanyBranchCode);
                
                TableSchema.TableColumn colvarFinanceGetDate = new TableSchema.TableColumn(schema);
                colvarFinanceGetDate.ColumnName = "finance_get_date";
                colvarFinanceGetDate.DataType = DbType.DateTime;
                colvarFinanceGetDate.MaxLength = 0;
                colvarFinanceGetDate.AutoIncrement = false;
                colvarFinanceGetDate.IsNullable = true;
                colvarFinanceGetDate.IsPrimaryKey = false;
                colvarFinanceGetDate.IsForeignKey = false;
                colvarFinanceGetDate.IsReadOnly = false;
                
                schema.Columns.Add(colvarFinanceGetDate);
                
                TableSchema.TableColumn colvarInvoiceComId = new TableSchema.TableColumn(schema);
                colvarInvoiceComId.ColumnName = "invoice_com_id";
                colvarInvoiceComId.DataType = DbType.String;
                colvarInvoiceComId.MaxLength = 10;
                colvarInvoiceComId.AutoIncrement = false;
                colvarInvoiceComId.IsNullable = true;
                colvarInvoiceComId.IsPrimaryKey = false;
                colvarInvoiceComId.IsForeignKey = false;
                colvarInvoiceComId.IsReadOnly = false;
                
                schema.Columns.Add(colvarInvoiceComId);
                
                TableSchema.TableColumn colvarBillNumber = new TableSchema.TableColumn(schema);
                colvarBillNumber.ColumnName = "bill_number";
                colvarBillNumber.DataType = DbType.String;
                colvarBillNumber.MaxLength = 10;
                colvarBillNumber.AutoIncrement = false;
                colvarBillNumber.IsNullable = true;
                colvarBillNumber.IsPrimaryKey = false;
                colvarBillNumber.IsForeignKey = false;
                colvarBillNumber.IsReadOnly = false;
                
                schema.Columns.Add(colvarBillNumber);
                
                TableSchema.TableColumn colvarMainDealShippedDate = new TableSchema.TableColumn(schema);
                colvarMainDealShippedDate.ColumnName = "main_deal_shipped_date";
                colvarMainDealShippedDate.DataType = DbType.DateTime;
                colvarMainDealShippedDate.MaxLength = 0;
                colvarMainDealShippedDate.AutoIncrement = false;
                colvarMainDealShippedDate.IsNullable = true;
                colvarMainDealShippedDate.IsPrimaryKey = false;
                colvarMainDealShippedDate.IsForeignKey = false;
                colvarMainDealShippedDate.IsReadOnly = false;
                
                schema.Columns.Add(colvarMainDealShippedDate);
                
                TableSchema.TableColumn colvarMainDealBalanceSheetCreateDate = new TableSchema.TableColumn(schema);
                colvarMainDealBalanceSheetCreateDate.ColumnName = "main_deal_balance_sheet_create_date";
                colvarMainDealBalanceSheetCreateDate.DataType = DbType.DateTime;
                colvarMainDealBalanceSheetCreateDate.MaxLength = 0;
                colvarMainDealBalanceSheetCreateDate.AutoIncrement = false;
                colvarMainDealBalanceSheetCreateDate.IsNullable = true;
                colvarMainDealBalanceSheetCreateDate.IsPrimaryKey = false;
                colvarMainDealBalanceSheetCreateDate.IsForeignKey = false;
                colvarMainDealBalanceSheetCreateDate.IsReadOnly = false;
                
                schema.Columns.Add(colvarMainDealBalanceSheetCreateDate);
                
                TableSchema.TableColumn colvarHasVendorPaymentChange = new TableSchema.TableColumn(schema);
                colvarHasVendorPaymentChange.ColumnName = "has_vendor_payment_change";
                colvarHasVendorPaymentChange.DataType = DbType.Int32;
                colvarHasVendorPaymentChange.MaxLength = 0;
                colvarHasVendorPaymentChange.AutoIncrement = false;
                colvarHasVendorPaymentChange.IsNullable = false;
                colvarHasVendorPaymentChange.IsPrimaryKey = false;
                colvarHasVendorPaymentChange.IsForeignKey = false;
                colvarHasVendorPaymentChange.IsReadOnly = false;
                
                schema.Columns.Add(colvarHasVendorPaymentChange);
                
                TableSchema.TableColumn colvarFlag = new TableSchema.TableColumn(schema);
                colvarFlag.ColumnName = "flag";
                colvarFlag.DataType = DbType.Int32;
                colvarFlag.MaxLength = 0;
                colvarFlag.AutoIncrement = false;
                colvarFlag.IsNullable = false;
                colvarFlag.IsPrimaryKey = false;
                colvarFlag.IsForeignKey = false;
                colvarFlag.IsReadOnly = false;
                
                schema.Columns.Add(colvarFlag);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_deal_property_business_hour_content",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewDealPropertyBusinessHourContent()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewDealPropertyBusinessHourContent(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewDealPropertyBusinessHourContent(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewDealPropertyBusinessHourContent(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("BusinessHourGuid")]
        [Bindable(true)]
        public Guid BusinessHourGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("business_hour_guid");
		    }
            set 
		    {
			    SetColumnValue("business_hour_guid", value);
            }
        }
	      
        [XmlAttribute("BusinessHourOrderTimeS")]
        [Bindable(true)]
        public DateTime BusinessHourOrderTimeS 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("business_hour_order_time_s");
		    }
            set 
		    {
			    SetColumnValue("business_hour_order_time_s", value);
            }
        }
	      
        [XmlAttribute("BusinessHourOrderTimeE")]
        [Bindable(true)]
        public DateTime BusinessHourOrderTimeE 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("business_hour_order_time_e");
		    }
            set 
		    {
			    SetColumnValue("business_hour_order_time_e", value);
            }
        }
	      
        [XmlAttribute("BusinessHourDeliverTimeS")]
        [Bindable(true)]
        public DateTime? BusinessHourDeliverTimeS 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("business_hour_deliver_time_s");
		    }
            set 
		    {
			    SetColumnValue("business_hour_deliver_time_s", value);
            }
        }
	      
        [XmlAttribute("BusinessHourDeliverTimeE")]
        [Bindable(true)]
        public DateTime? BusinessHourDeliverTimeE 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("business_hour_deliver_time_e");
		    }
            set 
		    {
			    SetColumnValue("business_hour_deliver_time_e", value);
            }
        }
	      
        [XmlAttribute("BusinessHourOrderMinimum")]
        [Bindable(true)]
        public decimal BusinessHourOrderMinimum 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("business_hour_order_minimum");
		    }
            set 
		    {
			    SetColumnValue("business_hour_order_minimum", value);
            }
        }
	      
        [XmlAttribute("ChangedExpireDate")]
        [Bindable(true)]
        public DateTime? ChangedExpireDate 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("changed_expire_date");
		    }
            set 
		    {
			    SetColumnValue("changed_expire_date", value);
            }
        }
	      
        [XmlAttribute("BusinessHourStatus")]
        [Bindable(true)]
        public int BusinessHourStatus 
	    {
		    get
		    {
			    return GetColumnValue<int>("business_hour_status");
		    }
            set 
		    {
			    SetColumnValue("business_hour_status", value);
            }
        }
	      
        [XmlAttribute("DealName")]
        [Bindable(true)]
        public string DealName 
	    {
		    get
		    {
			    return GetColumnValue<string>("deal_name");
		    }
            set 
		    {
			    SetColumnValue("deal_name", value);
            }
        }
	      
        [XmlAttribute("ImagePath")]
        [Bindable(true)]
        public string ImagePath 
	    {
		    get
		    {
			    return GetColumnValue<string>("image_path");
		    }
            set 
		    {
			    SetColumnValue("image_path", value);
            }
        }
	      
        [XmlAttribute("CouponUsage")]
        [Bindable(true)]
        public string CouponUsage 
	    {
		    get
		    {
			    return GetColumnValue<string>("coupon_usage");
		    }
            set 
		    {
			    SetColumnValue("coupon_usage", value);
            }
        }
	      
        [XmlAttribute("Availability")]
        [Bindable(true)]
        public string Availability 
	    {
		    get
		    {
			    return GetColumnValue<string>("availability");
		    }
            set 
		    {
			    SetColumnValue("availability", value);
            }
        }
	      
        [XmlAttribute("Title")]
        [Bindable(true)]
        public string Title 
	    {
		    get
		    {
			    return GetColumnValue<string>("title");
		    }
            set 
		    {
			    SetColumnValue("title", value);
            }
        }
	      
        [XmlAttribute("DealType")]
        [Bindable(true)]
        public int? DealType 
	    {
		    get
		    {
			    return GetColumnValue<int?>("deal_type");
		    }
            set 
		    {
			    SetColumnValue("deal_type", value);
            }
        }
	      
        [XmlAttribute("SellerVerifyType")]
        [Bindable(true)]
        public int? SellerVerifyType 
	    {
		    get
		    {
			    return GetColumnValue<int?>("seller_verify_type");
		    }
            set 
		    {
			    SetColumnValue("seller_verify_type", value);
            }
        }
	      
        [XmlAttribute("DealAccBusinessGroupId")]
        [Bindable(true)]
        public int? DealAccBusinessGroupId 
	    {
		    get
		    {
			    return GetColumnValue<int?>("deal_acc_business_group_id");
		    }
            set 
		    {
			    SetColumnValue("deal_acc_business_group_id", value);
            }
        }
	      
        [XmlAttribute("UniqueId")]
        [Bindable(true)]
        public int UniqueId 
	    {
		    get
		    {
			    return GetColumnValue<int>("unique_id");
		    }
            set 
		    {
			    SetColumnValue("unique_id", value);
            }
        }
	      
        [XmlAttribute("CityList")]
        [Bindable(true)]
        public string CityList 
	    {
		    get
		    {
			    return GetColumnValue<string>("city_list");
		    }
            set 
		    {
			    SetColumnValue("city_list", value);
            }
        }
	      
        [XmlAttribute("DeliveryType")]
        [Bindable(true)]
        public int? DeliveryType 
	    {
		    get
		    {
			    return GetColumnValue<int?>("delivery_type");
		    }
            set 
		    {
			    SetColumnValue("delivery_type", value);
            }
        }
	      
        [XmlAttribute("ShoppingCart")]
        [Bindable(true)]
        public bool ShoppingCart 
	    {
		    get
		    {
			    return GetColumnValue<bool>("shopping_cart");
		    }
            set 
		    {
			    SetColumnValue("shopping_cart", value);
            }
        }
	      
        [XmlAttribute("ComboPackCount")]
        [Bindable(true)]
        public int ComboPackCount 
	    {
		    get
		    {
			    return GetColumnValue<int>("combo_pack_count");
		    }
            set 
		    {
			    SetColumnValue("combo_pack_count", value);
            }
        }
	      
        [XmlAttribute("Slug")]
        [Bindable(true)]
        public string Slug 
	    {
		    get
		    {
			    return GetColumnValue<string>("slug");
		    }
            set 
		    {
			    SetColumnValue("slug", value);
            }
        }
	      
        [XmlAttribute("GroupOrderStatus")]
        [Bindable(true)]
        public int? GroupOrderStatus 
	    {
		    get
		    {
			    return GetColumnValue<int?>("group_order_status");
		    }
            set 
		    {
			    SetColumnValue("group_order_status", value);
            }
        }
	      
        [XmlAttribute("DeEmpName")]
        [Bindable(true)]
        public string DeEmpName 
	    {
		    get
		    {
			    return GetColumnValue<string>("de_emp_name");
		    }
            set 
		    {
			    SetColumnValue("de_emp_name", value);
            }
        }
	      
        [XmlAttribute("OpEmpName")]
        [Bindable(true)]
        public string OpEmpName 
	    {
		    get
		    {
			    return GetColumnValue<string>("op_emp_name");
		    }
            set 
		    {
			    SetColumnValue("op_emp_name", value);
            }
        }
	      
        [XmlAttribute("Email")]
        [Bindable(true)]
        public string Email 
	    {
		    get
		    {
			    return GetColumnValue<string>("email");
		    }
            set 
		    {
			    SetColumnValue("email", value);
            }
        }
	      
        [XmlAttribute("SellerEmail")]
        [Bindable(true)]
        public string SellerEmail 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_email");
		    }
            set 
		    {
			    SetColumnValue("seller_email", value);
            }
        }
	      
        [XmlAttribute("ReturnedPersonEmail")]
        [Bindable(true)]
        public string ReturnedPersonEmail 
	    {
		    get
		    {
			    return GetColumnValue<string>("returned_person_email");
		    }
            set 
		    {
			    SetColumnValue("returned_person_email", value);
            }
        }
	      
        [XmlAttribute("SellerName")]
        [Bindable(true)]
        public string SellerName 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_name");
		    }
            set 
		    {
			    SetColumnValue("seller_name", value);
            }
        }
	      
        [XmlAttribute("VendorBillingModel")]
        [Bindable(true)]
        public int VendorBillingModel 
	    {
		    get
		    {
			    return GetColumnValue<int>("vendor_billing_model");
		    }
            set 
		    {
			    SetColumnValue("vendor_billing_model", value);
            }
        }
	      
        [XmlAttribute("Paytocompany")]
        [Bindable(true)]
        public int Paytocompany 
	    {
		    get
		    {
			    return GetColumnValue<int>("paytocompany");
		    }
            set 
		    {
			    SetColumnValue("paytocompany", value);
            }
        }
	      
        [XmlAttribute("CompanyEmail")]
        [Bindable(true)]
        public string CompanyEmail 
	    {
		    get
		    {
			    return GetColumnValue<string>("CompanyEmail");
		    }
            set 
		    {
			    SetColumnValue("CompanyEmail", value);
            }
        }
	      
        [XmlAttribute("SellerGuid")]
        [Bindable(true)]
        public Guid SellerGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("seller_guid");
		    }
            set 
		    {
			    SetColumnValue("seller_guid", value);
            }
        }
	      
        [XmlAttribute("RemittanceType")]
        [Bindable(true)]
        public int RemittanceType 
	    {
		    get
		    {
			    return GetColumnValue<int>("remittance_type");
		    }
            set 
		    {
			    SetColumnValue("remittance_type", value);
            }
        }
	      
        [XmlAttribute("ReturnedPersonName")]
        [Bindable(true)]
        public string ReturnedPersonName 
	    {
		    get
		    {
			    return GetColumnValue<string>("returned_person_name");
		    }
            set 
		    {
			    SetColumnValue("returned_person_name", value);
            }
        }
	      
        [XmlAttribute("ReturnedPersonTel")]
        [Bindable(true)]
        public string ReturnedPersonTel 
	    {
		    get
		    {
			    return GetColumnValue<string>("returned_person_tel");
		    }
            set 
		    {
			    SetColumnValue("returned_person_tel", value);
            }
        }
	      
        [XmlAttribute("ShippedDate")]
        [Bindable(true)]
        public DateTime? ShippedDate 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("shipped_date");
		    }
            set 
		    {
			    SetColumnValue("shipped_date", value);
            }
        }
	      
        [XmlAttribute("OrderedQuantity")]
        [Bindable(true)]
        public int? OrderedQuantity 
	    {
		    get
		    {
			    return GetColumnValue<int?>("ordered_quantity");
		    }
            set 
		    {
			    SetColumnValue("ordered_quantity", value);
            }
        }
	      
        [XmlAttribute("VendorReceiptType")]
        [Bindable(true)]
        public int VendorReceiptType 
	    {
		    get
		    {
			    return GetColumnValue<int>("vendor_receipt_type");
		    }
            set 
		    {
			    SetColumnValue("vendor_receipt_type", value);
            }
        }
	      
        [XmlAttribute("FinalBalanceSheetDate")]
        [Bindable(true)]
        public DateTime? FinalBalanceSheetDate 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("final_balance_sheet_date");
		    }
            set 
		    {
			    SetColumnValue("final_balance_sheet_date", value);
            }
        }
	      
        [XmlAttribute("BalanceSheetCreateDate")]
        [Bindable(true)]
        public DateTime? BalanceSheetCreateDate 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("balance_sheet_create_date");
		    }
            set 
		    {
			    SetColumnValue("balance_sheet_create_date", value);
            }
        }
	      
        [XmlAttribute("PartiallyPaymentDate")]
        [Bindable(true)]
        public DateTime? PartiallyPaymentDate 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("partially_payment_date");
		    }
            set 
		    {
			    SetColumnValue("partially_payment_date", value);
            }
        }
	      
        [XmlAttribute("IsTax")]
        [Bindable(true)]
        public bool IsTax 
	    {
		    get
		    {
			    return GetColumnValue<bool>("is_tax");
		    }
            set 
		    {
			    SetColumnValue("is_tax", value);
            }
        }
	      
        [XmlAttribute("CompanyName")]
        [Bindable(true)]
        public string CompanyName 
	    {
		    get
		    {
			    return GetColumnValue<string>("CompanyName");
		    }
            set 
		    {
			    SetColumnValue("CompanyName", value);
            }
        }
	      
        [XmlAttribute("SignCompanyID")]
        [Bindable(true)]
        public string SignCompanyID 
	    {
		    get
		    {
			    return GetColumnValue<string>("SignCompanyID");
		    }
            set 
		    {
			    SetColumnValue("SignCompanyID", value);
            }
        }
	      
        [XmlAttribute("CompanyBossName")]
        [Bindable(true)]
        public string CompanyBossName 
	    {
		    get
		    {
			    return GetColumnValue<string>("CompanyBossName");
		    }
            set 
		    {
			    SetColumnValue("CompanyBossName", value);
            }
        }
	      
        [XmlAttribute("CompanyAccountName")]
        [Bindable(true)]
        public string CompanyAccountName 
	    {
		    get
		    {
			    return GetColumnValue<string>("CompanyAccountName");
		    }
            set 
		    {
			    SetColumnValue("CompanyAccountName", value);
            }
        }
	      
        [XmlAttribute("CompanyID")]
        [Bindable(true)]
        public string CompanyID 
	    {
		    get
		    {
			    return GetColumnValue<string>("CompanyID");
		    }
            set 
		    {
			    SetColumnValue("CompanyID", value);
            }
        }
	      
        [XmlAttribute("CompanyAccount")]
        [Bindable(true)]
        public string CompanyAccount 
	    {
		    get
		    {
			    return GetColumnValue<string>("CompanyAccount");
		    }
            set 
		    {
			    SetColumnValue("CompanyAccount", value);
            }
        }
	      
        [XmlAttribute("CompanyBankCode")]
        [Bindable(true)]
        public string CompanyBankCode 
	    {
		    get
		    {
			    return GetColumnValue<string>("CompanyBankCode");
		    }
            set 
		    {
			    SetColumnValue("CompanyBankCode", value);
            }
        }
	      
        [XmlAttribute("CompanyBranchCode")]
        [Bindable(true)]
        public string CompanyBranchCode 
	    {
		    get
		    {
			    return GetColumnValue<string>("CompanyBranchCode");
		    }
            set 
		    {
			    SetColumnValue("CompanyBranchCode", value);
            }
        }
	      
        [XmlAttribute("FinanceGetDate")]
        [Bindable(true)]
        public DateTime? FinanceGetDate 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("finance_get_date");
		    }
            set 
		    {
			    SetColumnValue("finance_get_date", value);
            }
        }
	      
        [XmlAttribute("InvoiceComId")]
        [Bindable(true)]
        public string InvoiceComId 
	    {
		    get
		    {
			    return GetColumnValue<string>("invoice_com_id");
		    }
            set 
		    {
			    SetColumnValue("invoice_com_id", value);
            }
        }
	      
        [XmlAttribute("BillNumber")]
        [Bindable(true)]
        public string BillNumber 
	    {
		    get
		    {
			    return GetColumnValue<string>("bill_number");
		    }
            set 
		    {
			    SetColumnValue("bill_number", value);
            }
        }
	      
        [XmlAttribute("MainDealShippedDate")]
        [Bindable(true)]
        public DateTime? MainDealShippedDate 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("main_deal_shipped_date");
		    }
            set 
		    {
			    SetColumnValue("main_deal_shipped_date", value);
            }
        }
	      
        [XmlAttribute("MainDealBalanceSheetCreateDate")]
        [Bindable(true)]
        public DateTime? MainDealBalanceSheetCreateDate 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("main_deal_balance_sheet_create_date");
		    }
            set 
		    {
			    SetColumnValue("main_deal_balance_sheet_create_date", value);
            }
        }
	      
        [XmlAttribute("HasVendorPaymentChange")]
        [Bindable(true)]
        public int HasVendorPaymentChange 
	    {
		    get
		    {
			    return GetColumnValue<int>("has_vendor_payment_change");
		    }
            set 
		    {
			    SetColumnValue("has_vendor_payment_change", value);
            }
        }
	      
        [XmlAttribute("Flag")]
        [Bindable(true)]
        public int Flag 
	    {
		    get
		    {
			    return GetColumnValue<int>("flag");
		    }
            set 
		    {
			    SetColumnValue("flag", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string BusinessHourGuid = @"business_hour_guid";
            
            public static string BusinessHourOrderTimeS = @"business_hour_order_time_s";
            
            public static string BusinessHourOrderTimeE = @"business_hour_order_time_e";
            
            public static string BusinessHourDeliverTimeS = @"business_hour_deliver_time_s";
            
            public static string BusinessHourDeliverTimeE = @"business_hour_deliver_time_e";
            
            public static string BusinessHourOrderMinimum = @"business_hour_order_minimum";
            
            public static string ChangedExpireDate = @"changed_expire_date";
            
            public static string BusinessHourStatus = @"business_hour_status";
            
            public static string DealName = @"deal_name";
            
            public static string ImagePath = @"image_path";
            
            public static string CouponUsage = @"coupon_usage";
            
            public static string Availability = @"availability";
            
            public static string Title = @"title";
            
            public static string DealType = @"deal_type";
            
            public static string SellerVerifyType = @"seller_verify_type";
            
            public static string DealAccBusinessGroupId = @"deal_acc_business_group_id";
            
            public static string UniqueId = @"unique_id";
            
            public static string CityList = @"city_list";
            
            public static string DeliveryType = @"delivery_type";
            
            public static string ShoppingCart = @"shopping_cart";
            
            public static string ComboPackCount = @"combo_pack_count";
            
            public static string Slug = @"slug";
            
            public static string GroupOrderStatus = @"group_order_status";
            
            public static string DeEmpName = @"de_emp_name";
            
            public static string OpEmpName = @"op_emp_name";
            
            public static string Email = @"email";
            
            public static string SellerEmail = @"seller_email";
            
            public static string ReturnedPersonEmail = @"returned_person_email";
            
            public static string SellerName = @"seller_name";
            
            public static string VendorBillingModel = @"vendor_billing_model";
            
            public static string Paytocompany = @"paytocompany";
            
            public static string CompanyEmail = @"CompanyEmail";
            
            public static string SellerGuid = @"seller_guid";
            
            public static string RemittanceType = @"remittance_type";
            
            public static string ReturnedPersonName = @"returned_person_name";
            
            public static string ReturnedPersonTel = @"returned_person_tel";
            
            public static string ShippedDate = @"shipped_date";
            
            public static string OrderedQuantity = @"ordered_quantity";
            
            public static string VendorReceiptType = @"vendor_receipt_type";
            
            public static string FinalBalanceSheetDate = @"final_balance_sheet_date";
            
            public static string BalanceSheetCreateDate = @"balance_sheet_create_date";
            
            public static string PartiallyPaymentDate = @"partially_payment_date";
            
            public static string IsTax = @"is_tax";
            
            public static string CompanyName = @"CompanyName";
            
            public static string SignCompanyID = @"SignCompanyID";
            
            public static string CompanyBossName = @"CompanyBossName";
            
            public static string CompanyAccountName = @"CompanyAccountName";
            
            public static string CompanyID = @"CompanyID";
            
            public static string CompanyAccount = @"CompanyAccount";
            
            public static string CompanyBankCode = @"CompanyBankCode";
            
            public static string CompanyBranchCode = @"CompanyBranchCode";
            
            public static string FinanceGetDate = @"finance_get_date";
            
            public static string InvoiceComId = @"invoice_com_id";
            
            public static string BillNumber = @"bill_number";
            
            public static string MainDealShippedDate = @"main_deal_shipped_date";
            
            public static string MainDealBalanceSheetCreateDate = @"main_deal_balance_sheet_create_date";
            
            public static string HasVendorPaymentChange = @"has_vendor_payment_change";
            
            public static string Flag = @"flag";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

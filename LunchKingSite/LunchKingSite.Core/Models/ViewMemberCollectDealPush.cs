using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewMemberCollectDealPush class.
    /// </summary>
    [Serializable]
    public partial class ViewMemberCollectDealPushCollection : ReadOnlyList<ViewMemberCollectDealPush, ViewMemberCollectDealPushCollection>
    {        
        public ViewMemberCollectDealPushCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_member_collect_deal_push view.
    /// </summary>
    [Serializable]
    public partial class ViewMemberCollectDealPush : ReadOnlyRecord<ViewMemberCollectDealPush>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_member_collect_deal_push", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarMemberUniqueId = new TableSchema.TableColumn(schema);
                colvarMemberUniqueId.ColumnName = "member_unique_id";
                colvarMemberUniqueId.DataType = DbType.Int32;
                colvarMemberUniqueId.MaxLength = 0;
                colvarMemberUniqueId.AutoIncrement = false;
                colvarMemberUniqueId.IsNullable = false;
                colvarMemberUniqueId.IsPrimaryKey = false;
                colvarMemberUniqueId.IsForeignKey = false;
                colvarMemberUniqueId.IsReadOnly = false;
                
                schema.Columns.Add(colvarMemberUniqueId);
                
                TableSchema.TableColumn colvarBusinessHourGuid = new TableSchema.TableColumn(schema);
                colvarBusinessHourGuid.ColumnName = "business_hour_guid";
                colvarBusinessHourGuid.DataType = DbType.Guid;
                colvarBusinessHourGuid.MaxLength = 0;
                colvarBusinessHourGuid.AutoIncrement = false;
                colvarBusinessHourGuid.IsNullable = false;
                colvarBusinessHourGuid.IsPrimaryKey = false;
                colvarBusinessHourGuid.IsForeignKey = false;
                colvarBusinessHourGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourGuid);
                
                TableSchema.TableColumn colvarCityId = new TableSchema.TableColumn(schema);
                colvarCityId.ColumnName = "city_id";
                colvarCityId.DataType = DbType.Int32;
                colvarCityId.MaxLength = 0;
                colvarCityId.AutoIncrement = false;
                colvarCityId.IsNullable = false;
                colvarCityId.IsPrimaryKey = false;
                colvarCityId.IsForeignKey = false;
                colvarCityId.IsReadOnly = false;
                
                schema.Columns.Add(colvarCityId);
                
                TableSchema.TableColumn colvarCollectStatus = new TableSchema.TableColumn(schema);
                colvarCollectStatus.ColumnName = "collect_status";
                colvarCollectStatus.DataType = DbType.Byte;
                colvarCollectStatus.MaxLength = 0;
                colvarCollectStatus.AutoIncrement = false;
                colvarCollectStatus.IsNullable = false;
                colvarCollectStatus.IsPrimaryKey = false;
                colvarCollectStatus.IsForeignKey = false;
                colvarCollectStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarCollectStatus);
                
                TableSchema.TableColumn colvarAppNotice = new TableSchema.TableColumn(schema);
                colvarAppNotice.ColumnName = "app_notice";
                colvarAppNotice.DataType = DbType.Boolean;
                colvarAppNotice.MaxLength = 0;
                colvarAppNotice.AutoIncrement = false;
                colvarAppNotice.IsNullable = false;
                colvarAppNotice.IsPrimaryKey = false;
                colvarAppNotice.IsForeignKey = false;
                colvarAppNotice.IsReadOnly = false;
                
                schema.Columns.Add(colvarAppNotice);
                
                TableSchema.TableColumn colvarBusinessHourOrderTimeE = new TableSchema.TableColumn(schema);
                colvarBusinessHourOrderTimeE.ColumnName = "business_hour_order_time_e";
                colvarBusinessHourOrderTimeE.DataType = DbType.DateTime;
                colvarBusinessHourOrderTimeE.MaxLength = 0;
                colvarBusinessHourOrderTimeE.AutoIncrement = false;
                colvarBusinessHourOrderTimeE.IsNullable = false;
                colvarBusinessHourOrderTimeE.IsPrimaryKey = false;
                colvarBusinessHourOrderTimeE.IsForeignKey = false;
                colvarBusinessHourOrderTimeE.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourOrderTimeE);
                
                TableSchema.TableColumn colvarName = new TableSchema.TableColumn(schema);
                colvarName.ColumnName = "name";
                colvarName.DataType = DbType.String;
                colvarName.MaxLength = 400;
                colvarName.AutoIncrement = false;
                colvarName.IsNullable = true;
                colvarName.IsPrimaryKey = false;
                colvarName.IsForeignKey = false;
                colvarName.IsReadOnly = false;
                
                schema.Columns.Add(colvarName);
                
                TableSchema.TableColumn colvarTitle = new TableSchema.TableColumn(schema);
                colvarTitle.ColumnName = "title";
                colvarTitle.DataType = DbType.String;
                colvarTitle.MaxLength = 100;
                colvarTitle.AutoIncrement = false;
                colvarTitle.IsNullable = true;
                colvarTitle.IsPrimaryKey = false;
                colvarTitle.IsForeignKey = false;
                colvarTitle.IsReadOnly = false;
                
                schema.Columns.Add(colvarTitle);
                
                TableSchema.TableColumn colvarSubjectName = new TableSchema.TableColumn(schema);
                colvarSubjectName.ColumnName = "subject_name";
                colvarSubjectName.DataType = DbType.String;
                colvarSubjectName.MaxLength = 150;
                colvarSubjectName.AutoIncrement = false;
                colvarSubjectName.IsNullable = true;
                colvarSubjectName.IsPrimaryKey = false;
                colvarSubjectName.IsForeignKey = false;
                colvarSubjectName.IsReadOnly = false;
                
                schema.Columns.Add(colvarSubjectName);
                
                TableSchema.TableColumn colvarAppTitle = new TableSchema.TableColumn(schema);
                colvarAppTitle.ColumnName = "app_title";
                colvarAppTitle.DataType = DbType.String;
                colvarAppTitle.MaxLength = 40;
                colvarAppTitle.AutoIncrement = false;
                colvarAppTitle.IsNullable = true;
                colvarAppTitle.IsPrimaryKey = false;
                colvarAppTitle.IsForeignKey = false;
                colvarAppTitle.IsReadOnly = false;
                
                schema.Columns.Add(colvarAppTitle);
                
                TableSchema.TableColumn colvarDevice = new TableSchema.TableColumn(schema);
                colvarDevice.ColumnName = "device";
                colvarDevice.DataType = DbType.AnsiString;
                colvarDevice.MaxLength = 50;
                colvarDevice.AutoIncrement = false;
                colvarDevice.IsNullable = false;
                colvarDevice.IsPrimaryKey = false;
                colvarDevice.IsForeignKey = false;
                colvarDevice.IsReadOnly = false;
                
                schema.Columns.Add(colvarDevice);
                
                TableSchema.TableColumn colvarToken = new TableSchema.TableColumn(schema);
                colvarToken.ColumnName = "token";
                colvarToken.DataType = DbType.String;
                colvarToken.MaxLength = 256;
                colvarToken.AutoIncrement = false;
                colvarToken.IsNullable = false;
                colvarToken.IsPrimaryKey = false;
                colvarToken.IsForeignKey = false;
                colvarToken.IsReadOnly = false;
                
                schema.Columns.Add(colvarToken);
                
                TableSchema.TableColumn colvarTokenStatus = new TableSchema.TableColumn(schema);
                colvarTokenStatus.ColumnName = "token_status";
                colvarTokenStatus.DataType = DbType.Int32;
                colvarTokenStatus.MaxLength = 0;
                colvarTokenStatus.AutoIncrement = false;
                colvarTokenStatus.IsNullable = false;
                colvarTokenStatus.IsPrimaryKey = false;
                colvarTokenStatus.IsForeignKey = false;
                colvarTokenStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarTokenStatus);
                
                TableSchema.TableColumn colvarMobileOsType = new TableSchema.TableColumn(schema);
                colvarMobileOsType.ColumnName = "mobile_os_type";
                colvarMobileOsType.DataType = DbType.Int32;
                colvarMobileOsType.MaxLength = 0;
                colvarMobileOsType.AutoIncrement = false;
                colvarMobileOsType.IsNullable = false;
                colvarMobileOsType.IsPrimaryKey = false;
                colvarMobileOsType.IsForeignKey = false;
                colvarMobileOsType.IsReadOnly = false;
                
                schema.Columns.Add(colvarMobileOsType);
                
                TableSchema.TableColumn colvarCouponUsage = new TableSchema.TableColumn(schema);
                colvarCouponUsage.ColumnName = "coupon_usage";
                colvarCouponUsage.DataType = DbType.String;
                colvarCouponUsage.MaxLength = 500;
                colvarCouponUsage.AutoIncrement = false;
                colvarCouponUsage.IsNullable = true;
                colvarCouponUsage.IsPrimaryKey = false;
                colvarCouponUsage.IsForeignKey = false;
                colvarCouponUsage.IsReadOnly = false;
                
                schema.Columns.Add(colvarCouponUsage);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_member_collect_deal_push",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewMemberCollectDealPush()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewMemberCollectDealPush(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewMemberCollectDealPush(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewMemberCollectDealPush(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("MemberUniqueId")]
        [Bindable(true)]
        public int MemberUniqueId 
	    {
		    get
		    {
			    return GetColumnValue<int>("member_unique_id");
		    }
            set 
		    {
			    SetColumnValue("member_unique_id", value);
            }
        }
	      
        [XmlAttribute("BusinessHourGuid")]
        [Bindable(true)]
        public Guid BusinessHourGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("business_hour_guid");
		    }
            set 
		    {
			    SetColumnValue("business_hour_guid", value);
            }
        }
	      
        [XmlAttribute("CityId")]
        [Bindable(true)]
        public int CityId 
	    {
		    get
		    {
			    return GetColumnValue<int>("city_id");
		    }
            set 
		    {
			    SetColumnValue("city_id", value);
            }
        }
	      
        [XmlAttribute("CollectStatus")]
        [Bindable(true)]
        public byte CollectStatus 
	    {
		    get
		    {
			    return GetColumnValue<byte>("collect_status");
		    }
            set 
		    {
			    SetColumnValue("collect_status", value);
            }
        }
	      
        [XmlAttribute("AppNotice")]
        [Bindable(true)]
        public bool AppNotice 
	    {
		    get
		    {
			    return GetColumnValue<bool>("app_notice");
		    }
            set 
		    {
			    SetColumnValue("app_notice", value);
            }
        }
	      
        [XmlAttribute("BusinessHourOrderTimeE")]
        [Bindable(true)]
        public DateTime BusinessHourOrderTimeE 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("business_hour_order_time_e");
		    }
            set 
		    {
			    SetColumnValue("business_hour_order_time_e", value);
            }
        }
	      
        [XmlAttribute("Name")]
        [Bindable(true)]
        public string Name 
	    {
		    get
		    {
			    return GetColumnValue<string>("name");
		    }
            set 
		    {
			    SetColumnValue("name", value);
            }
        }
	      
        [XmlAttribute("Title")]
        [Bindable(true)]
        public string Title 
	    {
		    get
		    {
			    return GetColumnValue<string>("title");
		    }
            set 
		    {
			    SetColumnValue("title", value);
            }
        }
	      
        [XmlAttribute("SubjectName")]
        [Bindable(true)]
        public string SubjectName 
	    {
		    get
		    {
			    return GetColumnValue<string>("subject_name");
		    }
            set 
		    {
			    SetColumnValue("subject_name", value);
            }
        }
	      
        [XmlAttribute("AppTitle")]
        [Bindable(true)]
        public string AppTitle 
	    {
		    get
		    {
			    return GetColumnValue<string>("app_title");
		    }
            set 
		    {
			    SetColumnValue("app_title", value);
            }
        }
	      
        [XmlAttribute("Device")]
        [Bindable(true)]
        public string Device 
	    {
		    get
		    {
			    return GetColumnValue<string>("device");
		    }
            set 
		    {
			    SetColumnValue("device", value);
            }
        }
	      
        [XmlAttribute("Token")]
        [Bindable(true)]
        public string Token 
	    {
		    get
		    {
			    return GetColumnValue<string>("token");
		    }
            set 
		    {
			    SetColumnValue("token", value);
            }
        }
	      
        [XmlAttribute("TokenStatus")]
        [Bindable(true)]
        public int TokenStatus 
	    {
		    get
		    {
			    return GetColumnValue<int>("token_status");
		    }
            set 
		    {
			    SetColumnValue("token_status", value);
            }
        }
	      
        [XmlAttribute("MobileOsType")]
        [Bindable(true)]
        public int MobileOsType 
	    {
		    get
		    {
			    return GetColumnValue<int>("mobile_os_type");
		    }
            set 
		    {
			    SetColumnValue("mobile_os_type", value);
            }
        }
	      
        [XmlAttribute("CouponUsage")]
        [Bindable(true)]
        public string CouponUsage 
	    {
		    get
		    {
			    return GetColumnValue<string>("coupon_usage");
		    }
            set 
		    {
			    SetColumnValue("coupon_usage", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string MemberUniqueId = @"member_unique_id";
            
            public static string BusinessHourGuid = @"business_hour_guid";
            
            public static string CityId = @"city_id";
            
            public static string CollectStatus = @"collect_status";
            
            public static string AppNotice = @"app_notice";
            
            public static string BusinessHourOrderTimeE = @"business_hour_order_time_e";
            
            public static string Name = @"name";
            
            public static string Title = @"title";
            
            public static string SubjectName = @"subject_name";
            
            public static string AppTitle = @"app_title";
            
            public static string Device = @"device";
            
            public static string Token = @"token";
            
            public static string TokenStatus = @"token_status";
            
            public static string MobileOsType = @"mobile_os_type";
            
            public static string CouponUsage = @"coupon_usage";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

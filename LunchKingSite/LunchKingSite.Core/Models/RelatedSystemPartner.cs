using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    [Serializable]
	public partial class RelatedSystemPartnerCollection : RepositoryList<RelatedSystemPartner, RelatedSystemPartnerCollection>
	{	   
		public RelatedSystemPartnerCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>RelatedSystemPartnerCollection</returns>
		public RelatedSystemPartnerCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                RelatedSystemPartner o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the related_system_partner table.
	/// </summary>
	[Serializable]
	public partial class RelatedSystemPartner : RepositoryRecord<RelatedSystemPartner>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public RelatedSystemPartner()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public RelatedSystemPartner(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("related_system_partner", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarPartnerName = new TableSchema.TableColumn(schema);
				colvarPartnerName.ColumnName = "partner_name";
				colvarPartnerName.DataType = DbType.String;
				colvarPartnerName.MaxLength = 200;
				colvarPartnerName.AutoIncrement = false;
				colvarPartnerName.IsNullable = true;
				colvarPartnerName.IsPrimaryKey = false;
				colvarPartnerName.IsForeignKey = false;
				colvarPartnerName.IsReadOnly = false;
				colvarPartnerName.DefaultSetting = @"";
				colvarPartnerName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPartnerName);
				
				TableSchema.TableColumn colvarPartnerContact = new TableSchema.TableColumn(schema);
				colvarPartnerContact.ColumnName = "partner_contact";
				colvarPartnerContact.DataType = DbType.String;
				colvarPartnerContact.MaxLength = 200;
				colvarPartnerContact.AutoIncrement = false;
				colvarPartnerContact.IsNullable = true;
				colvarPartnerContact.IsPrimaryKey = false;
				colvarPartnerContact.IsForeignKey = false;
				colvarPartnerContact.IsReadOnly = false;
				colvarPartnerContact.DefaultSetting = @"";
				colvarPartnerContact.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPartnerContact);
				
				TableSchema.TableColumn colvarPartnerMobile = new TableSchema.TableColumn(schema);
				colvarPartnerMobile.ColumnName = "partner_mobile";
				colvarPartnerMobile.DataType = DbType.String;
				colvarPartnerMobile.MaxLength = 50;
				colvarPartnerMobile.AutoIncrement = false;
				colvarPartnerMobile.IsNullable = true;
				colvarPartnerMobile.IsPrimaryKey = false;
				colvarPartnerMobile.IsForeignKey = false;
				colvarPartnerMobile.IsReadOnly = false;
				colvarPartnerMobile.DefaultSetting = @"";
				colvarPartnerMobile.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPartnerMobile);
				
				TableSchema.TableColumn colvarPartnerEmail = new TableSchema.TableColumn(schema);
				colvarPartnerEmail.ColumnName = "partner_email";
				colvarPartnerEmail.DataType = DbType.String;
				colvarPartnerEmail.MaxLength = 256;
				colvarPartnerEmail.AutoIncrement = false;
				colvarPartnerEmail.IsNullable = true;
				colvarPartnerEmail.IsPrimaryKey = false;
				colvarPartnerEmail.IsForeignKey = false;
				colvarPartnerEmail.IsReadOnly = false;
				colvarPartnerEmail.DefaultSetting = @"";
				colvarPartnerEmail.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPartnerEmail);
				
				TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
				colvarStatus.ColumnName = "status";
				colvarStatus.DataType = DbType.Boolean;
				colvarStatus.MaxLength = 0;
				colvarStatus.AutoIncrement = false;
				colvarStatus.IsNullable = true;
				colvarStatus.IsPrimaryKey = false;
				colvarStatus.IsForeignKey = false;
				colvarStatus.IsReadOnly = false;
				colvarStatus.DefaultSetting = @"";
				colvarStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStatus);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("related_system_partner",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("PartnerName")]
		[Bindable(true)]
		public string PartnerName 
		{
			get { return GetColumnValue<string>(Columns.PartnerName); }
			set { SetColumnValue(Columns.PartnerName, value); }
		}
		  
		[XmlAttribute("PartnerContact")]
		[Bindable(true)]
		public string PartnerContact 
		{
			get { return GetColumnValue<string>(Columns.PartnerContact); }
			set { SetColumnValue(Columns.PartnerContact, value); }
		}
		  
		[XmlAttribute("PartnerMobile")]
		[Bindable(true)]
		public string PartnerMobile 
		{
			get { return GetColumnValue<string>(Columns.PartnerMobile); }
			set { SetColumnValue(Columns.PartnerMobile, value); }
		}
		  
		[XmlAttribute("PartnerEmail")]
		[Bindable(true)]
		public string PartnerEmail 
		{
			get { return GetColumnValue<string>(Columns.PartnerEmail); }
			set { SetColumnValue(Columns.PartnerEmail, value); }
		}
		  
		[XmlAttribute("Status")]
		[Bindable(true)]
		public bool? Status 
		{
			get { return GetColumnValue<bool?>(Columns.Status); }
			set { SetColumnValue(Columns.Status, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn PartnerNameColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn PartnerContactColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn PartnerMobileColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn PartnerEmailColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn StatusColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string PartnerName = @"partner_name";
			 public static string PartnerContact = @"partner_contact";
			 public static string PartnerMobile = @"partner_mobile";
			 public static string PartnerEmail = @"partner_email";
			 public static string Status = @"status";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

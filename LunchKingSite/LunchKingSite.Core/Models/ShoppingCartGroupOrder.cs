﻿using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the ShoppingCartGroupOrder class.
    /// </summary>
    [Serializable]
    public partial class ShoppingCartGroupOrderCollection : RepositoryList<ShoppingCartGroupOrder, ShoppingCartGroupOrderCollection>
    {
        public ShoppingCartGroupOrderCollection() { }

        /// <summary>
        /// Filters an existing collection based on the set criteria. This is an in-memory filter
        /// Thanks to developingchris for this!
        /// </summary>
        /// <returns>ShoppingCartGroupOrderCollection</returns>
        public ShoppingCartGroupOrderCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                ShoppingCartGroupOrder o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }


    }
    /// <summary>
    /// This is an ActiveRecord class which wraps the shopping_cart_group_order table.
    /// </summary>
    [Serializable]
    public partial class ShoppingCartGroupOrder : RepositoryRecord<ShoppingCartGroupOrder>, IRecordBase
    {
        #region .ctors and Default Settings

        public ShoppingCartGroupOrder()
        {
            SetSQLProps();
            InitSetDefaults();
            MarkNew();
        }

        private void InitSetDefaults() { SetDefaults(); }

        public ShoppingCartGroupOrder(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
                ForceDefaults();
            MarkNew();
        }

        protected static void SetSQLProps() { GetTableSchema(); }

        #endregion

        #region Schema and Query Accessor	
        public static Query CreateQuery() { return new Query(Schema); }
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                    SetSQLProps();
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("shopping_cart_group_order", TableType.Table, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = true;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = true;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                colvarId.DefaultSetting = @"";
                colvarId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarId);

                TableSchema.TableColumn colvarOrderGuid = new TableSchema.TableColumn(schema);
                colvarOrderGuid.ColumnName = "order_guid";
                colvarOrderGuid.DataType = DbType.Guid;
                colvarOrderGuid.MaxLength = 0;
                colvarOrderGuid.AutoIncrement = false;
                colvarOrderGuid.IsNullable = false;
                colvarOrderGuid.IsPrimaryKey = false;
                colvarOrderGuid.IsForeignKey = false;
                colvarOrderGuid.IsReadOnly = false;
                colvarOrderGuid.DefaultSetting = @"";
                colvarOrderGuid.ForeignKeyTableName = "";
                schema.Columns.Add(colvarOrderGuid);

                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = false;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                colvarCreateTime.DefaultSetting = @"";
                colvarCreateTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateTime);

                TableSchema.TableColumn colvarCartTransId = new TableSchema.TableColumn(schema);
                colvarCartTransId.ColumnName = "cart_trans_id";
                colvarCartTransId.DataType = DbType.Guid;
                colvarCartTransId.MaxLength = 0;
                colvarCartTransId.AutoIncrement = false;
                colvarCartTransId.IsNullable = false;
                colvarCartTransId.IsPrimaryKey = false;
                colvarCartTransId.IsForeignKey = false;
                colvarCartTransId.IsReadOnly = false;

                colvarCartTransId.DefaultSetting = @"('00000000-0000-0000-0000-000000000000')";
                colvarCartTransId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCartTransId);

                TableSchema.TableColumn colvarSubCartTransId = new TableSchema.TableColumn(schema);
                colvarSubCartTransId.ColumnName = "sub_cart_trans_id";
                colvarSubCartTransId.DataType = DbType.Guid;
                colvarSubCartTransId.MaxLength = 0;
                colvarSubCartTransId.AutoIncrement = false;
                colvarSubCartTransId.IsNullable = false;
                colvarSubCartTransId.IsPrimaryKey = false;
                colvarSubCartTransId.IsForeignKey = false;
                colvarSubCartTransId.IsReadOnly = false;

                colvarSubCartTransId.DefaultSetting = @"('00000000-0000-0000-0000-000000000000')";
                colvarSubCartTransId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarSubCartTransId);

                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("shopping_cart_group_order", schema);
            }
        }
        #endregion

        #region Props

        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id
        {
            get { return GetColumnValue<int>(Columns.Id); }
            set { SetColumnValue(Columns.Id, value); }
        }

        [XmlAttribute("OrderGuid")]
        [Bindable(true)]
        public Guid OrderGuid
        {
            get { return GetColumnValue<Guid>(Columns.OrderGuid); }
            set { SetColumnValue(Columns.OrderGuid, value); }
        }

        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime CreateTime
        {
            get { return GetColumnValue<DateTime>(Columns.CreateTime); }
            set { SetColumnValue(Columns.CreateTime, value); }
        }

        [XmlAttribute("CartTransId")]
        [Bindable(true)]
        public Guid CartTransId
        {
            get { return GetColumnValue<Guid>(Columns.CartTransId); }
            set { SetColumnValue(Columns.CartTransId, value); }
        }

        [XmlAttribute("SubCartTransId")]
        [Bindable(true)]
        public Guid SubCartTransId
        {
            get { return GetColumnValue<Guid>(Columns.SubCartTransId); }
            set { SetColumnValue(Columns.SubCartTransId, value); }
        }

        #endregion




        //no foreign key tables defined (0)



        //no ManyToMany tables defined (0)





        #region Typed Columns


        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }



        public static TableSchema.TableColumn OrderGuidColumn
        {
            get { return Schema.Columns[1]; }
        }



        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[2]; }
        }



        public static TableSchema.TableColumn CartTransIdColumn
        {
            get { return Schema.Columns[3]; }
        }



        public static TableSchema.TableColumn SubCartTransIdColumn
        {
            get { return Schema.Columns[4]; }
        }



        #endregion
        #region Columns Struct
        public struct Columns
        {
            public static string Id = @"id";
            public static string OrderGuid = @"order_guid";
            public static string CreateTime = @"create_time";
            public static string CartTransId = @"cart_trans_id";
            public static string SubCartTransId = @"sub_cart_trans_id";

        }
        #endregion

        #region Update PK Collections

        #endregion

        #region Deep Save

        #endregion
    }
}

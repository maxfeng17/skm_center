using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the WmsRefundOrder class.
	/// </summary>
    [Serializable]
	public partial class WmsRefundOrderCollection : RepositoryList<WmsRefundOrder, WmsRefundOrderCollection>
	{	   
		public WmsRefundOrderCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>WmsRefundOrderCollection</returns>
		public WmsRefundOrderCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                WmsRefundOrder o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the wms_refund_order table.
	/// </summary>
	[Serializable]
	public partial class WmsRefundOrder : RepositoryRecord<WmsRefundOrder>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public WmsRefundOrder()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public WmsRefundOrder(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("wms_refund_order", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarReturnFormId = new TableSchema.TableColumn(schema);
				colvarReturnFormId.ColumnName = "return_form_id";
				colvarReturnFormId.DataType = DbType.Int32;
				colvarReturnFormId.MaxLength = 0;
				colvarReturnFormId.AutoIncrement = false;
				colvarReturnFormId.IsNullable = false;
				colvarReturnFormId.IsPrimaryKey = false;
				colvarReturnFormId.IsForeignKey = false;
				colvarReturnFormId.IsReadOnly = false;
				colvarReturnFormId.DefaultSetting = @"";
				colvarReturnFormId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReturnFormId);
				
				TableSchema.TableColumn colvarPchomeRefundId = new TableSchema.TableColumn(schema);
				colvarPchomeRefundId.ColumnName = "pchome_refund_id";
				colvarPchomeRefundId.DataType = DbType.String;
				colvarPchomeRefundId.MaxLength = 20;
				colvarPchomeRefundId.AutoIncrement = false;
				colvarPchomeRefundId.IsNullable = false;
				colvarPchomeRefundId.IsPrimaryKey = false;
				colvarPchomeRefundId.IsForeignKey = false;
				colvarPchomeRefundId.IsReadOnly = false;
				colvarPchomeRefundId.DefaultSetting = @"";
				colvarPchomeRefundId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPchomeRefundId);
				
				TableSchema.TableColumn colvarRefundFrom = new TableSchema.TableColumn(schema);
				colvarRefundFrom.ColumnName = "refund_from";
				colvarRefundFrom.DataType = DbType.Int32;
				colvarRefundFrom.MaxLength = 0;
				colvarRefundFrom.AutoIncrement = false;
				colvarRefundFrom.IsNullable = false;
				colvarRefundFrom.IsPrimaryKey = false;
				colvarRefundFrom.IsForeignKey = false;
				colvarRefundFrom.IsReadOnly = false;
				colvarRefundFrom.DefaultSetting = @"";
				colvarRefundFrom.ForeignKeyTableName = "";
				schema.Columns.Add(colvarRefundFrom);
				
				TableSchema.TableColumn colvarIsReceive = new TableSchema.TableColumn(schema);
				colvarIsReceive.ColumnName = "is_receive";
				colvarIsReceive.DataType = DbType.Boolean;
				colvarIsReceive.MaxLength = 0;
				colvarIsReceive.AutoIncrement = false;
				colvarIsReceive.IsNullable = false;
				colvarIsReceive.IsPrimaryKey = false;
				colvarIsReceive.IsForeignKey = false;
				colvarIsReceive.IsReadOnly = false;
				colvarIsReceive.DefaultSetting = @"";
				colvarIsReceive.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsReceive);
				
				TableSchema.TableColumn colvarHasPickupTime = new TableSchema.TableColumn(schema);
				colvarHasPickupTime.ColumnName = "has_pickup_time";
				colvarHasPickupTime.DataType = DbType.DateTime;
				colvarHasPickupTime.MaxLength = 0;
				colvarHasPickupTime.AutoIncrement = false;
				colvarHasPickupTime.IsNullable = true;
				colvarHasPickupTime.IsPrimaryKey = false;
				colvarHasPickupTime.IsForeignKey = false;
				colvarHasPickupTime.IsReadOnly = false;
				colvarHasPickupTime.DefaultSetting = @"";
				colvarHasPickupTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarHasPickupTime);
				
				TableSchema.TableColumn colvarArrivalTime = new TableSchema.TableColumn(schema);
				colvarArrivalTime.ColumnName = "arrival_time";
				colvarArrivalTime.DataType = DbType.DateTime;
				colvarArrivalTime.MaxLength = 0;
				colvarArrivalTime.AutoIncrement = false;
				colvarArrivalTime.IsNullable = true;
				colvarArrivalTime.IsPrimaryKey = false;
				colvarArrivalTime.IsForeignKey = false;
				colvarArrivalTime.IsReadOnly = false;
				colvarArrivalTime.DefaultSetting = @"";
				colvarArrivalTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarArrivalTime);
				
				TableSchema.TableColumn colvarShipInfo = new TableSchema.TableColumn(schema);
				colvarShipInfo.ColumnName = "ship_info";
				colvarShipInfo.DataType = DbType.String;
				colvarShipInfo.MaxLength = -1;
				colvarShipInfo.AutoIncrement = false;
				colvarShipInfo.IsNullable = true;
				colvarShipInfo.IsPrimaryKey = false;
				colvarShipInfo.IsForeignKey = false;
				colvarShipInfo.IsReadOnly = false;
				colvarShipInfo.DefaultSetting = @"";
				colvarShipInfo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarShipInfo);
				
				TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
				colvarStatus.ColumnName = "status";
				colvarStatus.DataType = DbType.Int32;
				colvarStatus.MaxLength = 0;
				colvarStatus.AutoIncrement = false;
				colvarStatus.IsNullable = false;
				colvarStatus.IsPrimaryKey = false;
				colvarStatus.IsForeignKey = false;
				colvarStatus.IsReadOnly = false;
				colvarStatus.DefaultSetting = @"";
				colvarStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStatus);
				
				TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
				colvarCreateId.ColumnName = "create_id";
				colvarCreateId.DataType = DbType.String;
				colvarCreateId.MaxLength = 256;
				colvarCreateId.AutoIncrement = false;
				colvarCreateId.IsNullable = false;
				colvarCreateId.IsPrimaryKey = false;
				colvarCreateId.IsForeignKey = false;
				colvarCreateId.IsReadOnly = false;
				colvarCreateId.DefaultSetting = @"";
				colvarCreateId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateId);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarModifyId = new TableSchema.TableColumn(schema);
				colvarModifyId.ColumnName = "modify_id";
				colvarModifyId.DataType = DbType.String;
				colvarModifyId.MaxLength = 256;
				colvarModifyId.AutoIncrement = false;
				colvarModifyId.IsNullable = true;
				colvarModifyId.IsPrimaryKey = false;
				colvarModifyId.IsForeignKey = false;
				colvarModifyId.IsReadOnly = false;
				colvarModifyId.DefaultSetting = @"";
				colvarModifyId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyId);
				
				TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
				colvarModifyTime.ColumnName = "modify_time";
				colvarModifyTime.DataType = DbType.DateTime;
				colvarModifyTime.MaxLength = 0;
				colvarModifyTime.AutoIncrement = false;
				colvarModifyTime.IsNullable = true;
				colvarModifyTime.IsPrimaryKey = false;
				colvarModifyTime.IsForeignKey = false;
				colvarModifyTime.IsReadOnly = false;
				colvarModifyTime.DefaultSetting = @"";
				colvarModifyTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyTime);
				
				TableSchema.TableColumn colvarCsConfirmedTime = new TableSchema.TableColumn(schema);
				colvarCsConfirmedTime.ColumnName = "cs_confirmed_time";
				colvarCsConfirmedTime.DataType = DbType.DateTime;
				colvarCsConfirmedTime.MaxLength = 0;
				colvarCsConfirmedTime.AutoIncrement = false;
				colvarCsConfirmedTime.IsNullable = true;
				colvarCsConfirmedTime.IsPrimaryKey = false;
				colvarCsConfirmedTime.IsForeignKey = false;
				colvarCsConfirmedTime.IsReadOnly = false;
				colvarCsConfirmedTime.DefaultSetting = @"";
				colvarCsConfirmedTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCsConfirmedTime);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("wms_refund_order",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("ReturnFormId")]
		[Bindable(true)]
		public int ReturnFormId 
		{
			get { return GetColumnValue<int>(Columns.ReturnFormId); }
			set { SetColumnValue(Columns.ReturnFormId, value); }
		}
		  
		[XmlAttribute("PchomeRefundId")]
		[Bindable(true)]
		public string PchomeRefundId 
		{
			get { return GetColumnValue<string>(Columns.PchomeRefundId); }
			set { SetColumnValue(Columns.PchomeRefundId, value); }
		}
		  
		[XmlAttribute("RefundFrom")]
		[Bindable(true)]
		public int RefundFrom 
		{
			get { return GetColumnValue<int>(Columns.RefundFrom); }
			set { SetColumnValue(Columns.RefundFrom, value); }
		}
		  
		[XmlAttribute("IsReceive")]
		[Bindable(true)]
		public bool IsReceive 
		{
			get { return GetColumnValue<bool>(Columns.IsReceive); }
			set { SetColumnValue(Columns.IsReceive, value); }
		}
		  
		[XmlAttribute("HasPickupTime")]
		[Bindable(true)]
		public DateTime? HasPickupTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.HasPickupTime); }
			set { SetColumnValue(Columns.HasPickupTime, value); }
		}
		  
		[XmlAttribute("ArrivalTime")]
		[Bindable(true)]
		public DateTime? ArrivalTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.ArrivalTime); }
			set { SetColumnValue(Columns.ArrivalTime, value); }
		}
		  
		[XmlAttribute("ShipInfo")]
		[Bindable(true)]
		public string ShipInfo 
		{
			get { return GetColumnValue<string>(Columns.ShipInfo); }
			set { SetColumnValue(Columns.ShipInfo, value); }
		}
		  
		[XmlAttribute("Status")]
		[Bindable(true)]
		public int Status 
		{
			get { return GetColumnValue<int>(Columns.Status); }
			set { SetColumnValue(Columns.Status, value); }
		}
		  
		[XmlAttribute("CreateId")]
		[Bindable(true)]
		public string CreateId 
		{
			get { return GetColumnValue<string>(Columns.CreateId); }
			set { SetColumnValue(Columns.CreateId, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		  
		[XmlAttribute("ModifyId")]
		[Bindable(true)]
		public string ModifyId 
		{
			get { return GetColumnValue<string>(Columns.ModifyId); }
			set { SetColumnValue(Columns.ModifyId, value); }
		}
		  
		[XmlAttribute("ModifyTime")]
		[Bindable(true)]
		public DateTime? ModifyTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.ModifyTime); }
			set { SetColumnValue(Columns.ModifyTime, value); }
		}
		  
		[XmlAttribute("CsConfirmedTime")]
		[Bindable(true)]
		public DateTime? CsConfirmedTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.CsConfirmedTime); }
			set { SetColumnValue(Columns.CsConfirmedTime, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn ReturnFormIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn PchomeRefundIdColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn RefundFromColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn IsReceiveColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn HasPickupTimeColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn ArrivalTimeColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn ShipInfoColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn StatusColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyIdColumn
        {
            get { return Schema.Columns[11]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[12]; }
        }
        
        
        
        public static TableSchema.TableColumn CsConfirmedTimeColumn
        {
            get { return Schema.Columns[13]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string ReturnFormId = @"return_form_id";
			 public static string PchomeRefundId = @"pchome_refund_id";
			 public static string RefundFrom = @"refund_from";
			 public static string IsReceive = @"is_receive";
			 public static string HasPickupTime = @"has_pickup_time";
			 public static string ArrivalTime = @"arrival_time";
			 public static string ShipInfo = @"ship_info";
			 public static string Status = @"status";
			 public static string CreateId = @"create_id";
			 public static string CreateTime = @"create_time";
			 public static string ModifyId = @"modify_id";
			 public static string ModifyTime = @"modify_time";
			 public static string CsConfirmedTime = @"cs_confirmed_time";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the MemberContact class.
	/// </summary>
    [Serializable]
	public partial class MemberContactCollection : RepositoryList<MemberContact, MemberContactCollection>
	{	   
		public MemberContactCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>MemberContactCollection</returns>
		public MemberContactCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                MemberContact o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the member_contact table.
	/// </summary>
	[Serializable]
	public partial class MemberContact : RepositoryRecord<MemberContact>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public MemberContact()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public MemberContact(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("member_contact", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarUserId = new TableSchema.TableColumn(schema);
				colvarUserId.ColumnName = "user_id";
				colvarUserId.DataType = DbType.Int32;
				colvarUserId.MaxLength = 0;
				colvarUserId.AutoIncrement = false;
				colvarUserId.IsNullable = false;
				colvarUserId.IsPrimaryKey = false;
				colvarUserId.IsForeignKey = false;
				colvarUserId.IsReadOnly = false;
				colvarUserId.DefaultSetting = @"";
				colvarUserId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUserId);
				
				TableSchema.TableColumn colvarContactUserId = new TableSchema.TableColumn(schema);
				colvarContactUserId.ColumnName = "contact_user_id";
				colvarContactUserId.DataType = DbType.Int32;
				colvarContactUserId.MaxLength = 0;
				colvarContactUserId.AutoIncrement = false;
				colvarContactUserId.IsNullable = true;
				colvarContactUserId.IsPrimaryKey = false;
				colvarContactUserId.IsForeignKey = false;
				colvarContactUserId.IsReadOnly = false;
				colvarContactUserId.DefaultSetting = @"";
				colvarContactUserId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarContactUserId);
				
				TableSchema.TableColumn colvarContactName = new TableSchema.TableColumn(schema);
				colvarContactName.ColumnName = "contact_name";
				colvarContactName.DataType = DbType.String;
				colvarContactName.MaxLength = 256;
				colvarContactName.AutoIncrement = false;
				colvarContactName.IsNullable = false;
				colvarContactName.IsPrimaryKey = false;
				colvarContactName.IsForeignKey = false;
				colvarContactName.IsReadOnly = false;
				colvarContactName.DefaultSetting = @"";
				colvarContactName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarContactName);
				
				TableSchema.TableColumn colvarContactNickName = new TableSchema.TableColumn(schema);
				colvarContactNickName.ColumnName = "contact_nick_name";
				colvarContactNickName.DataType = DbType.String;
				colvarContactNickName.MaxLength = 256;
				colvarContactNickName.AutoIncrement = false;
				colvarContactNickName.IsNullable = true;
				colvarContactNickName.IsPrimaryKey = false;
				colvarContactNickName.IsForeignKey = false;
				colvarContactNickName.IsReadOnly = false;
				colvarContactNickName.DefaultSetting = @"";
				colvarContactNickName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarContactNickName);
				
				TableSchema.TableColumn colvarContactValue = new TableSchema.TableColumn(schema);
				colvarContactValue.ColumnName = "contact_value";
				colvarContactValue.DataType = DbType.String;
				colvarContactValue.MaxLength = 256;
				colvarContactValue.AutoIncrement = false;
				colvarContactValue.IsNullable = true;
				colvarContactValue.IsPrimaryKey = false;
				colvarContactValue.IsForeignKey = false;
				colvarContactValue.IsReadOnly = false;
				colvarContactValue.DefaultSetting = @"";
				colvarContactValue.ForeignKeyTableName = "";
				schema.Columns.Add(colvarContactValue);
				
				TableSchema.TableColumn colvarContactType = new TableSchema.TableColumn(schema);
				colvarContactType.ColumnName = "contact_type";
				colvarContactType.DataType = DbType.Int32;
				colvarContactType.MaxLength = 0;
				colvarContactType.AutoIncrement = false;
				colvarContactType.IsNullable = true;
				colvarContactType.IsPrimaryKey = false;
				colvarContactType.IsForeignKey = false;
				colvarContactType.IsReadOnly = false;
				colvarContactType.DefaultSetting = @"";
				colvarContactType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarContactType);
				
				TableSchema.TableColumn colvarContactBirthday = new TableSchema.TableColumn(schema);
				colvarContactBirthday.ColumnName = "contact_birthday";
				colvarContactBirthday.DataType = DbType.DateTime;
				colvarContactBirthday.MaxLength = 0;
				colvarContactBirthday.AutoIncrement = false;
				colvarContactBirthday.IsNullable = true;
				colvarContactBirthday.IsPrimaryKey = false;
				colvarContactBirthday.IsForeignKey = false;
				colvarContactBirthday.IsReadOnly = false;
				colvarContactBirthday.DefaultSetting = @"";
				colvarContactBirthday.ForeignKeyTableName = "";
				schema.Columns.Add(colvarContactBirthday);
				
				TableSchema.TableColumn colvarGuid = new TableSchema.TableColumn(schema);
				colvarGuid.ColumnName = "guid";
				colvarGuid.DataType = DbType.Guid;
				colvarGuid.MaxLength = 0;
				colvarGuid.AutoIncrement = false;
				colvarGuid.IsNullable = false;
				colvarGuid.IsPrimaryKey = false;
				colvarGuid.IsForeignKey = false;
				colvarGuid.IsReadOnly = false;
				colvarGuid.DefaultSetting = @"";
				colvarGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGuid);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("member_contact",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("UserId")]
		[Bindable(true)]
		public int UserId 
		{
			get { return GetColumnValue<int>(Columns.UserId); }
			set { SetColumnValue(Columns.UserId, value); }
		}
		  
		[XmlAttribute("ContactUserId")]
		[Bindable(true)]
		public int? ContactUserId 
		{
			get { return GetColumnValue<int?>(Columns.ContactUserId); }
			set { SetColumnValue(Columns.ContactUserId, value); }
		}
		  
		[XmlAttribute("ContactName")]
		[Bindable(true)]
		public string ContactName 
		{
			get { return GetColumnValue<string>(Columns.ContactName); }
			set { SetColumnValue(Columns.ContactName, value); }
		}
		  
		[XmlAttribute("ContactNickName")]
		[Bindable(true)]
		public string ContactNickName 
		{
			get { return GetColumnValue<string>(Columns.ContactNickName); }
			set { SetColumnValue(Columns.ContactNickName, value); }
		}
		  
		[XmlAttribute("ContactValue")]
		[Bindable(true)]
		public string ContactValue 
		{
			get { return GetColumnValue<string>(Columns.ContactValue); }
			set { SetColumnValue(Columns.ContactValue, value); }
		}
		  
		[XmlAttribute("ContactType")]
		[Bindable(true)]
		public int? ContactType 
		{
			get { return GetColumnValue<int?>(Columns.ContactType); }
			set { SetColumnValue(Columns.ContactType, value); }
		}
		  
		[XmlAttribute("ContactBirthday")]
		[Bindable(true)]
		public DateTime? ContactBirthday 
		{
			get { return GetColumnValue<DateTime?>(Columns.ContactBirthday); }
			set { SetColumnValue(Columns.ContactBirthday, value); }
		}
		  
		[XmlAttribute("Guid")]
		[Bindable(true)]
		public Guid Guid 
		{
			get { return GetColumnValue<Guid>(Columns.Guid); }
			set { SetColumnValue(Columns.Guid, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn UserIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn ContactUserIdColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn ContactNameColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn ContactNickNameColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn ContactValueColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn ContactTypeColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn ContactBirthdayColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn GuidColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string UserId = @"user_id";
			 public static string ContactUserId = @"contact_user_id";
			 public static string ContactName = @"contact_name";
			 public static string ContactNickName = @"contact_nick_name";
			 public static string ContactValue = @"contact_value";
			 public static string ContactType = @"contact_type";
			 public static string ContactBirthday = @"contact_birthday";
			 public static string Guid = @"guid";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the AppVersion class.
    /// </summary>
    [Serializable]
    public partial class AppVersionCollection : RepositoryList<AppVersion, AppVersionCollection>
    {
        public AppVersionCollection() { }

        /// <summary>
        /// Filters an existing collection based on the set criteria. This is an in-memory filter
        /// Thanks to developingchris for this!
        /// </summary>
        /// <returns>AppVersionCollection</returns>
        public AppVersionCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                AppVersion o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }


    }
    /// <summary>
    /// This is an ActiveRecord class which wraps the app_version table.
    /// </summary>
    [Serializable]
    public partial class AppVersion : RepositoryRecord<AppVersion>, IRecordBase
    {
        #region .ctors and Default Settings

        public AppVersion()
        {
            SetSQLProps();
            InitSetDefaults();
            MarkNew();
        }

        private void InitSetDefaults() { SetDefaults(); }

        public AppVersion(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
                ForceDefaults();
            MarkNew();
        }

        protected static void SetSQLProps() { GetTableSchema(); }

        #endregion

        #region Schema and Query Accessor
        public static Query CreateQuery() { return new Query(Schema); }
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                    SetSQLProps();
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("app_version", TableType.Table, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarAppName = new TableSchema.TableColumn(schema);
                colvarAppName.ColumnName = "app_name";
                colvarAppName.DataType = DbType.String;
                colvarAppName.MaxLength = 50;
                colvarAppName.AutoIncrement = false;
                colvarAppName.IsNullable = false;
                colvarAppName.IsPrimaryKey = true;
                colvarAppName.IsForeignKey = false;
                colvarAppName.IsReadOnly = false;
                colvarAppName.DefaultSetting = @"";
                colvarAppName.ForeignKeyTableName = "";
                schema.Columns.Add(colvarAppName);

                TableSchema.TableColumn colvarLatestVersion = new TableSchema.TableColumn(schema);
                colvarLatestVersion.ColumnName = "latest_version";
                colvarLatestVersion.DataType = DbType.AnsiString;
                colvarLatestVersion.MaxLength = 50;
                colvarLatestVersion.AutoIncrement = false;
                colvarLatestVersion.IsNullable = true;
                colvarLatestVersion.IsPrimaryKey = false;
                colvarLatestVersion.IsForeignKey = false;
                colvarLatestVersion.IsReadOnly = false;
                colvarLatestVersion.DefaultSetting = @"";
                colvarLatestVersion.ForeignKeyTableName = "";
                schema.Columns.Add(colvarLatestVersion);

                TableSchema.TableColumn colvarMinimumVersion = new TableSchema.TableColumn(schema);
                colvarMinimumVersion.ColumnName = "minimum_version";
                colvarMinimumVersion.DataType = DbType.AnsiString;
                colvarMinimumVersion.MaxLength = 50;
                colvarMinimumVersion.AutoIncrement = false;
                colvarMinimumVersion.IsNullable = true;
                colvarMinimumVersion.IsPrimaryKey = false;
                colvarMinimumVersion.IsForeignKey = false;
                colvarMinimumVersion.IsReadOnly = false;
                colvarMinimumVersion.DefaultSetting = @"";
                colvarMinimumVersion.ForeignKeyTableName = "";
                schema.Columns.Add(colvarMinimumVersion);

                TableSchema.TableColumn colvarDeviceType = new TableSchema.TableColumn(schema);
                colvarDeviceType.ColumnName = "device_type";
                colvarDeviceType.DataType = DbType.Int32;
                colvarDeviceType.MaxLength = 0;
                colvarDeviceType.AutoIncrement = false;
                colvarDeviceType.IsNullable = true;
                colvarDeviceType.IsPrimaryKey = false;
                colvarDeviceType.IsForeignKey = false;
                colvarDeviceType.IsReadOnly = false;
                colvarDeviceType.DefaultSetting = @"";
                colvarDeviceType.ForeignKeyTableName = "";
                schema.Columns.Add(colvarDeviceType);

                TableSchema.TableColumn colvarOsVersion = new TableSchema.TableColumn(schema);
                colvarOsVersion.ColumnName = "os_version";
                colvarOsVersion.DataType = DbType.AnsiString;
                colvarOsVersion.MaxLength = 50;
                colvarOsVersion.AutoIncrement = false;
                colvarOsVersion.IsNullable = true;
                colvarOsVersion.IsPrimaryKey = false;
                colvarOsVersion.IsForeignKey = false;
                colvarOsVersion.IsReadOnly = false;
                colvarOsVersion.DefaultSetting = @"";
                colvarOsVersion.ForeignKeyTableName = "";
                schema.Columns.Add(colvarOsVersion);

                TableSchema.TableColumn colvarOsMinimumVersion = new TableSchema.TableColumn(schema);
                colvarOsMinimumVersion.ColumnName = "os_minimum_version";
                colvarOsMinimumVersion.DataType = DbType.AnsiString;
                colvarOsMinimumVersion.MaxLength = 50;
                colvarOsMinimumVersion.AutoIncrement = false;
                colvarOsMinimumVersion.IsNullable = true;
                colvarOsMinimumVersion.IsPrimaryKey = false;
                colvarOsMinimumVersion.IsForeignKey = false;
                colvarOsMinimumVersion.IsReadOnly = false;
                colvarOsMinimumVersion.DefaultSetting = @"";
                colvarOsMinimumVersion.ForeignKeyTableName = "";
                schema.Columns.Add(colvarOsMinimumVersion);

                TableSchema.TableColumn colvarAppConfig = new TableSchema.TableColumn(schema);
                colvarAppConfig.ColumnName = "app_config";
                colvarAppConfig.DataType = DbType.String;
                colvarAppConfig.MaxLength = -1;
                colvarAppConfig.AutoIncrement = false;
                colvarAppConfig.IsNullable = true;
                colvarAppConfig.IsPrimaryKey = false;
                colvarAppConfig.IsForeignKey = false;
                colvarAppConfig.IsReadOnly = false;
                colvarAppConfig.DefaultSetting = @"";
                colvarAppConfig.ForeignKeyTableName = "";
                schema.Columns.Add(colvarAppConfig);

                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("app_version", schema);
            }
        }
        #endregion

        #region Props

        [XmlAttribute("AppName")]
        [Bindable(true)]
        public string AppName
        {
            get { return GetColumnValue<string>(Columns.AppName); }
            set { SetColumnValue(Columns.AppName, value); }
        }

        [XmlAttribute("LatestVersion")]
        [Bindable(true)]
        public string LatestVersion
        {
            get { return GetColumnValue<string>(Columns.LatestVersion); }
            set { SetColumnValue(Columns.LatestVersion, value); }
        }

        [XmlAttribute("MinimumVersion")]
        [Bindable(true)]
        public string MinimumVersion
        {
            get { return GetColumnValue<string>(Columns.MinimumVersion); }
            set { SetColumnValue(Columns.MinimumVersion, value); }
        }

        [XmlAttribute("DeviceType")]
        [Bindable(true)]
        public int? DeviceType
        {
            get { return GetColumnValue<int?>(Columns.DeviceType); }
            set { SetColumnValue(Columns.DeviceType, value); }
        }

        [XmlAttribute("OsVersion")]
        [Bindable(true)]
        public string OsVersion
        {
            get { return GetColumnValue<string>(Columns.OsVersion); }
            set { SetColumnValue(Columns.OsVersion, value); }
        }

        [XmlAttribute("OsMinimumVersion")]
        [Bindable(true)]
        public string OsMinimumVersion
        {
            get { return GetColumnValue<string>(Columns.OsMinimumVersion); }
            set { SetColumnValue(Columns.OsMinimumVersion, value); }
        }

        [XmlAttribute("AppConfig")]
        [Bindable(true)]
        public string AppConfig
        {
            get { return GetColumnValue<string>(Columns.AppConfig); }
            set { SetColumnValue(Columns.AppConfig, value); }
        }

        #endregion




        //no foreign key tables defined (0)



        //no ManyToMany tables defined (0)





        #region Typed Columns


        public static TableSchema.TableColumn AppNameColumn
        {
            get { return Schema.Columns[0]; }
        }



        public static TableSchema.TableColumn LatestVersionColumn
        {
            get { return Schema.Columns[1]; }
        }



        public static TableSchema.TableColumn MinimumVersionColumn
        {
            get { return Schema.Columns[2]; }
        }



        public static TableSchema.TableColumn DeviceTypeColumn
        {
            get { return Schema.Columns[3]; }
        }



        public static TableSchema.TableColumn OsVersionColumn
        {
            get { return Schema.Columns[4]; }
        }



        public static TableSchema.TableColumn OsMinimumVersionColumn
        {
            get { return Schema.Columns[5]; }
        }



        public static TableSchema.TableColumn AppConfigColumn
        {
            get { return Schema.Columns[6]; }
        }



        #endregion
        #region Columns Struct
        public struct Columns
        {
            public static string AppName = @"app_name";
            public static string LatestVersion = @"latest_version";
            public static string MinimumVersion = @"minimum_version";
            public static string DeviceType = @"device_type";
            public static string OsVersion = @"os_version";
            public static string OsMinimumVersion = @"os_minimum_version";
            public static string AppConfig = @"app_config";

        }
        #endregion

        #region Update PK Collections

        #endregion

        #region Deep Save

        #endregion
    }
}

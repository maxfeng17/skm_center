using System;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using SubSonic;

namespace LunchKingSite.DataOrm
{
	[Serializable]
	public partial class ThemeCurationMainCollection : RepositoryList<ThemeCurationMain, ThemeCurationMainCollection>
	{
			public ThemeCurationMainCollection() {}

			public ThemeCurationMainCollection Filter()
			{
					for (int i = this.Count - 1; i > -1; i--)
					{
							ThemeCurationMain o = this[i];
							foreach (SubSonic.Where w in this.wheres)
							{
									bool remove = false;
									System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
									if(pi.CanRead)
									{
											object val = pi.GetValue(o, null);
											switch(w.Comparison)
											{
													case SubSonic.Comparison.Equals:
															if(!val.Equals(w.ParameterValue))
															{
																	remove = true;
															}
															break;
											}
									}
									if(remove)
									{
											this.Remove(o);
											break;
									}
							}
					}
					return this;
			}
	}

	[Serializable]
	public partial class ThemeCurationMain : RepositoryRecord<ThemeCurationMain>, IRecordBase
	{
		#region .ctors and Default Settings
		public ThemeCurationMain()
		{
			SetSQLProps();
			InitSetDefaults();
			MarkNew();
		}

		private void InitSetDefaults() { SetDefaults(); }

		public ThemeCurationMain(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}

		protected static void SetSQLProps() { GetTableSchema(); }

		#endregion

		#region Schema and Query Accessor
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}

		private static void GetTableSchema()
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("theme_curation_main", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);

				TableSchema.TableColumn colvarTitle = new TableSchema.TableColumn(schema);
				colvarTitle.ColumnName = "title";
				colvarTitle.DataType = DbType.String;
				colvarTitle.MaxLength = 100;
				colvarTitle.AutoIncrement = false;
				colvarTitle.IsNullable = false;
				colvarTitle.IsPrimaryKey = false;
				colvarTitle.IsForeignKey = false;
				colvarTitle.IsReadOnly = false;
				colvarTitle.DefaultSetting = @"";
				colvarTitle.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTitle);

				TableSchema.TableColumn colvarMainImage = new TableSchema.TableColumn(schema);
				colvarMainImage.ColumnName = "main_image";
				colvarMainImage.DataType = DbType.String;
				colvarMainImage.MaxLength = 255;
				colvarMainImage.AutoIncrement = false;
				colvarMainImage.IsNullable = true;
				colvarMainImage.IsPrimaryKey = false;
				colvarMainImage.IsForeignKey = false;
				colvarMainImage.IsReadOnly = false;
				colvarMainImage.DefaultSetting = @"";
				colvarMainImage.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMainImage);

				TableSchema.TableColumn colvarMobileMainImage = new TableSchema.TableColumn(schema);
				colvarMobileMainImage.ColumnName = "mobile_main_image";
				colvarMobileMainImage.DataType = DbType.String;
				colvarMobileMainImage.MaxLength = 255;
				colvarMobileMainImage.AutoIncrement = false;
				colvarMobileMainImage.IsNullable = true;
				colvarMobileMainImage.IsPrimaryKey = false;
				colvarMobileMainImage.IsForeignKey = false;
				colvarMobileMainImage.IsReadOnly = false;
				colvarMobileMainImage.DefaultSetting = @"";
				colvarMobileMainImage.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMobileMainImage);

				TableSchema.TableColumn colvarStartDate = new TableSchema.TableColumn(schema);
				colvarStartDate.ColumnName = "start_date";
				colvarStartDate.DataType = DbType.DateTime;
				colvarStartDate.MaxLength = 0;
				colvarStartDate.AutoIncrement = false;
				colvarStartDate.IsNullable = false;
				colvarStartDate.IsPrimaryKey = false;
				colvarStartDate.IsForeignKey = false;
				colvarStartDate.IsReadOnly = false;
				colvarStartDate.DefaultSetting = @"";
				colvarStartDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStartDate);

				TableSchema.TableColumn colvarEndTime = new TableSchema.TableColumn(schema);
				colvarEndTime.ColumnName = "end_time";
				colvarEndTime.DataType = DbType.DateTime;
				colvarEndTime.MaxLength = 0;
				colvarEndTime.AutoIncrement = false;
				colvarEndTime.IsNullable = false;
				colvarEndTime.IsPrimaryKey = false;
				colvarEndTime.IsForeignKey = false;
				colvarEndTime.IsReadOnly = false;
				colvarEndTime.DefaultSetting = @"";
				colvarEndTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEndTime);

				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"(getdate())";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);

				TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
				colvarModifyTime.ColumnName = "modify_time";
				colvarModifyTime.DataType = DbType.DateTime;
				colvarModifyTime.MaxLength = 0;
				colvarModifyTime.AutoIncrement = false;
				colvarModifyTime.IsNullable = true;
				colvarModifyTime.IsPrimaryKey = false;
				colvarModifyTime.IsForeignKey = false;
				colvarModifyTime.IsReadOnly = false;
				colvarModifyTime.DefaultSetting = @"";
				colvarModifyTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyTime);

				TableSchema.TableColumn colvarModifyUser = new TableSchema.TableColumn(schema);
				colvarModifyUser.ColumnName = "modify_user";
				colvarModifyUser.DataType = DbType.AnsiString;
				colvarModifyUser.MaxLength = 50;
				colvarModifyUser.AutoIncrement = false;
				colvarModifyUser.IsNullable = true;
				colvarModifyUser.IsPrimaryKey = false;
				colvarModifyUser.IsForeignKey = false;
				colvarModifyUser.IsReadOnly = false;
				colvarModifyUser.DefaultSetting = @"";
				colvarModifyUser.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyUser);

				TableSchema.TableColumn colvarMainBackgroundImage = new TableSchema.TableColumn(schema);
				colvarMainBackgroundImage.ColumnName = "main_background_image";
				colvarMainBackgroundImage.DataType = DbType.String;
				colvarMainBackgroundImage.MaxLength = 255;
				colvarMainBackgroundImage.AutoIncrement = false;
				colvarMainBackgroundImage.IsNullable = true;
				colvarMainBackgroundImage.IsPrimaryKey = false;
				colvarMainBackgroundImage.IsForeignKey = false;
				colvarMainBackgroundImage.IsReadOnly = false;
				colvarMainBackgroundImage.DefaultSetting = @"";
				colvarMainBackgroundImage.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMainBackgroundImage);

				TableSchema.TableColumn colvarMainSpacerImage = new TableSchema.TableColumn(schema);
				colvarMainSpacerImage.ColumnName = "main_spacer_image";
				colvarMainSpacerImage.DataType = DbType.String;
				colvarMainSpacerImage.MaxLength = 255;
				colvarMainSpacerImage.AutoIncrement = false;
				colvarMainSpacerImage.IsNullable = true;
				colvarMainSpacerImage.IsPrimaryKey = false;
				colvarMainSpacerImage.IsForeignKey = false;
				colvarMainSpacerImage.IsReadOnly = false;
				colvarMainSpacerImage.DefaultSetting = @"";
				colvarMainSpacerImage.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMainSpacerImage);

				TableSchema.TableColumn colvarWebRuleContent = new TableSchema.TableColumn(schema);
				colvarWebRuleContent.ColumnName = "web_rule_content";
				colvarWebRuleContent.DataType = DbType.String;
				colvarWebRuleContent.MaxLength = 2147483647;
				colvarWebRuleContent.AutoIncrement = false;
				colvarWebRuleContent.IsNullable = true;
				colvarWebRuleContent.IsPrimaryKey = false;
				colvarWebRuleContent.IsForeignKey = false;
				colvarWebRuleContent.IsReadOnly = false;
				colvarWebRuleContent.DefaultSetting = @"";
				colvarWebRuleContent.ForeignKeyTableName = "";
				schema.Columns.Add(colvarWebRuleContent);

				TableSchema.TableColumn colvarMobileRuleContent = new TableSchema.TableColumn(schema);
				colvarMobileRuleContent.ColumnName = "mobile_rule_content";
				colvarMobileRuleContent.DataType = DbType.String;
				colvarMobileRuleContent.MaxLength = 2147483647;
				colvarMobileRuleContent.AutoIncrement = false;
				colvarMobileRuleContent.IsNullable = true;
				colvarMobileRuleContent.IsPrimaryKey = false;
				colvarMobileRuleContent.IsForeignKey = false;
				colvarMobileRuleContent.IsReadOnly = false;
				colvarMobileRuleContent.DefaultSetting = @"";
				colvarMobileRuleContent.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMobileRuleContent);

				TableSchema.TableColumn colvarThemeOtherPic = new TableSchema.TableColumn(schema);
				colvarThemeOtherPic.ColumnName = "theme_other_pic";
				colvarThemeOtherPic.DataType = DbType.String;
				colvarThemeOtherPic.MaxLength = 1000;
				colvarThemeOtherPic.AutoIncrement = false;
				colvarThemeOtherPic.IsNullable = true;
				colvarThemeOtherPic.IsPrimaryKey = false;
				colvarThemeOtherPic.IsForeignKey = false;
				colvarThemeOtherPic.IsReadOnly = false;
				colvarThemeOtherPic.DefaultSetting = @"";
				colvarThemeOtherPic.ForeignKeyTableName = "";
				schema.Columns.Add(colvarThemeOtherPic);

				TableSchema.TableColumn colvarShowInWeb = new TableSchema.TableColumn(schema);
				colvarShowInWeb.ColumnName = "show_in_web";
				colvarShowInWeb.DataType = DbType.Boolean;
				colvarShowInWeb.MaxLength = 0;
				colvarShowInWeb.AutoIncrement = false;
				colvarShowInWeb.IsNullable = false;
				colvarShowInWeb.IsPrimaryKey = false;
				colvarShowInWeb.IsForeignKey = false;
				colvarShowInWeb.IsReadOnly = false;
				colvarShowInWeb.DefaultSetting = @"((1))";
				colvarShowInWeb.ForeignKeyTableName = "";
				schema.Columns.Add(colvarShowInWeb);

				TableSchema.TableColumn colvarShowInApp = new TableSchema.TableColumn(schema);
				colvarShowInApp.ColumnName = "show_in_app";
				colvarShowInApp.DataType = DbType.Boolean;
				colvarShowInApp.MaxLength = 0;
				colvarShowInApp.AutoIncrement = false;
				colvarShowInApp.IsNullable = false;
				colvarShowInApp.IsPrimaryKey = false;
				colvarShowInApp.IsForeignKey = false;
				colvarShowInApp.IsReadOnly = false;
				colvarShowInApp.DefaultSetting = @"((1))";
				colvarShowInApp.ForeignKeyTableName = "";
				schema.Columns.Add(colvarShowInApp);

				TableSchema.TableColumn colvarFbShareTitle = new TableSchema.TableColumn(schema);
				colvarFbShareTitle.ColumnName = "fb_share_title";
				colvarFbShareTitle.DataType = DbType.String;
				colvarFbShareTitle.MaxLength = 100;
				colvarFbShareTitle.AutoIncrement = false;
				colvarFbShareTitle.IsNullable = true;
				colvarFbShareTitle.IsPrimaryKey = false;
				colvarFbShareTitle.IsForeignKey = false;
				colvarFbShareTitle.IsReadOnly = false;
				colvarFbShareTitle.DefaultSetting = @"";
				colvarFbShareTitle.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFbShareTitle);

				TableSchema.TableColumn colvarFbShareIamge = new TableSchema.TableColumn(schema);
				colvarFbShareIamge.ColumnName = "fb_share_iamge";
				colvarFbShareIamge.DataType = DbType.String;
				colvarFbShareIamge.MaxLength = 150;
				colvarFbShareIamge.AutoIncrement = false;
				colvarFbShareIamge.IsNullable = true;
				colvarFbShareIamge.IsPrimaryKey = false;
				colvarFbShareIamge.IsForeignKey = false;
				colvarFbShareIamge.IsReadOnly = false;
				colvarFbShareIamge.DefaultSetting = @"";
				colvarFbShareIamge.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFbShareIamge);

				TableSchema.TableColumn colvarCssStyle = new TableSchema.TableColumn(schema);
				colvarCssStyle.ColumnName = "css_style";
				colvarCssStyle.DataType = DbType.String;
				colvarCssStyle.MaxLength = 2147483647;
				colvarCssStyle.AutoIncrement = false;
				colvarCssStyle.IsNullable = true;
				colvarCssStyle.IsPrimaryKey = false;
				colvarCssStyle.IsForeignKey = false;
				colvarCssStyle.IsReadOnly = false;
				colvarCssStyle.DefaultSetting = @"";
				colvarCssStyle.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCssStyle);

				TableSchema.TableColumn colvarThemeType = new TableSchema.TableColumn(schema);
				colvarThemeType.ColumnName = "theme_type";
				colvarThemeType.DataType = DbType.Int32;
				colvarThemeType.MaxLength = 0;
				colvarThemeType.AutoIncrement = false;
				colvarThemeType.IsNullable = false;
				colvarThemeType.IsPrimaryKey = false;
				colvarThemeType.IsForeignKey = false;
				colvarThemeType.IsReadOnly = false;
				colvarThemeType.DefaultSetting = @"((0))";
				colvarThemeType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarThemeType);

				TableSchema.TableColumn colvarChannelTitle = new TableSchema.TableColumn(schema);
				colvarChannelTitle.ColumnName = "channel_title";
				colvarChannelTitle.DataType = DbType.String;
				colvarChannelTitle.MaxLength = 50;
				colvarChannelTitle.AutoIncrement = false;
				colvarChannelTitle.IsNullable = true;
				colvarChannelTitle.IsPrimaryKey = false;
				colvarChannelTitle.IsForeignKey = false;
				colvarChannelTitle.IsReadOnly = false;
				colvarChannelTitle.DefaultSetting = @"";
				colvarChannelTitle.ForeignKeyTableName = "";
				schema.Columns.Add(colvarChannelTitle);

				TableSchema.TableColumn colvarChannelImage = new TableSchema.TableColumn(schema);
				colvarChannelImage.ColumnName = "channel_image";
				colvarChannelImage.DataType = DbType.String;
				colvarChannelImage.MaxLength = 150;
				colvarChannelImage.AutoIncrement = false;
				colvarChannelImage.IsNullable = true;
				colvarChannelImage.IsPrimaryKey = false;
				colvarChannelImage.IsForeignKey = false;
				colvarChannelImage.IsReadOnly = false;
				colvarChannelImage.DefaultSetting = @"";
				colvarChannelImage.ForeignKeyTableName = "";
				schema.Columns.Add(colvarChannelImage);

				TableSchema.TableColumn colvarWebCategoryImage = new TableSchema.TableColumn(schema);
				colvarWebCategoryImage.ColumnName = "web_category_image";
				colvarWebCategoryImage.DataType = DbType.String;
				colvarWebCategoryImage.MaxLength = 150;
				colvarWebCategoryImage.AutoIncrement = false;
				colvarWebCategoryImage.IsNullable = true;
				colvarWebCategoryImage.IsPrimaryKey = false;
				colvarWebCategoryImage.IsForeignKey = false;
				colvarWebCategoryImage.IsReadOnly = false;
				colvarWebCategoryImage.DefaultSetting = @"";
				colvarWebCategoryImage.ForeignKeyTableName = "";
				schema.Columns.Add(colvarWebCategoryImage);

				TableSchema.TableColumn colvarWebRuleBanner = new TableSchema.TableColumn(schema);
				colvarWebRuleBanner.ColumnName = "web_rule_banner";
				colvarWebRuleBanner.DataType = DbType.String;
				colvarWebRuleBanner.MaxLength = 150;
				colvarWebRuleBanner.AutoIncrement = false;
				colvarWebRuleBanner.IsNullable = true;
				colvarWebRuleBanner.IsPrimaryKey = false;
				colvarWebRuleBanner.IsForeignKey = false;
				colvarWebRuleBanner.IsReadOnly = false;
				colvarWebRuleBanner.DefaultSetting = @"";
				colvarWebRuleBanner.ForeignKeyTableName = "";
				schema.Columns.Add(colvarWebRuleBanner);

				TableSchema.TableColumn colvarMobileCategoryImage = new TableSchema.TableColumn(schema);
				colvarMobileCategoryImage.ColumnName = "mobile_category_image";
				colvarMobileCategoryImage.DataType = DbType.String;
				colvarMobileCategoryImage.MaxLength = 150;
				colvarMobileCategoryImage.AutoIncrement = false;
				colvarMobileCategoryImage.IsNullable = true;
				colvarMobileCategoryImage.IsPrimaryKey = false;
				colvarMobileCategoryImage.IsForeignKey = false;
				colvarMobileCategoryImage.IsReadOnly = false;
				colvarMobileCategoryImage.DefaultSetting = @"";
				colvarMobileCategoryImage.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMobileCategoryImage);

				TableSchema.TableColumn colvarMobileRuleBanner = new TableSchema.TableColumn(schema);
				colvarMobileRuleBanner.ColumnName = "mobile_rule_banner";
				colvarMobileRuleBanner.DataType = DbType.String;
				colvarMobileRuleBanner.MaxLength = 150;
				colvarMobileRuleBanner.AutoIncrement = false;
				colvarMobileRuleBanner.IsNullable = true;
				colvarMobileRuleBanner.IsPrimaryKey = false;
				colvarMobileRuleBanner.IsForeignKey = false;
				colvarMobileRuleBanner.IsReadOnly = false;
				colvarMobileRuleBanner.DefaultSetting = @"";
				colvarMobileRuleBanner.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMobileRuleBanner);

				TableSchema.TableColumn colvarMobileCover = new TableSchema.TableColumn(schema);
				colvarMobileCover.ColumnName = "mobile_cover";
				colvarMobileCover.DataType = DbType.String;
				colvarMobileCover.MaxLength = 150;
				colvarMobileCover.AutoIncrement = false;
				colvarMobileCover.IsNullable = true;
				colvarMobileCover.IsPrimaryKey = false;
				colvarMobileCover.IsForeignKey = false;
				colvarMobileCover.IsReadOnly = false;
				colvarMobileCover.DefaultSetting = @"";
				colvarMobileCover.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMobileCover);

				TableSchema.TableColumn colvarAppIndexImage = new TableSchema.TableColumn(schema);
				colvarAppIndexImage.ColumnName = "app_index_image";
				colvarAppIndexImage.DataType = DbType.String;
				colvarAppIndexImage.MaxLength = 150;
				colvarAppIndexImage.AutoIncrement = false;
				colvarAppIndexImage.IsNullable = true;
				colvarAppIndexImage.IsPrimaryKey = false;
				colvarAppIndexImage.IsForeignKey = false;
				colvarAppIndexImage.IsReadOnly = false;
				colvarAppIndexImage.DefaultSetting = @"";
				colvarAppIndexImage.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAppIndexImage);

				TableSchema.TableColumn colvarRedirectUrl = new TableSchema.TableColumn(schema);
				colvarRedirectUrl.ColumnName = "redirect_url";
				colvarRedirectUrl.DataType = DbType.String;
				colvarRedirectUrl.MaxLength = 250;
				colvarRedirectUrl.AutoIncrement = false;
				colvarRedirectUrl.IsNullable = true;
				colvarRedirectUrl.IsPrimaryKey = false;
				colvarRedirectUrl.IsForeignKey = false;
				colvarRedirectUrl.IsReadOnly = false;
				colvarRedirectUrl.DefaultSetting = @"";
				colvarRedirectUrl.ForeignKeyTableName = "";
				schema.Columns.Add(colvarRedirectUrl);

				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("theme_curation_main",schema);
			}
		}
		#endregion

		#region Props

		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}

		[XmlAttribute("Title")]
		[Bindable(true)]
		public string Title
		{
			get { return GetColumnValue<string>(Columns.Title); }
			set { SetColumnValue(Columns.Title, value); }
		}

		[XmlAttribute("MainImage")]
		[Bindable(true)]
		public string MainImage
		{
			get { return GetColumnValue<string>(Columns.MainImage); }
			set { SetColumnValue(Columns.MainImage, value); }
		}

		[XmlAttribute("MobileMainImage")]
		[Bindable(true)]
		public string MobileMainImage
		{
			get { return GetColumnValue<string>(Columns.MobileMainImage); }
			set { SetColumnValue(Columns.MobileMainImage, value); }
		}

		[XmlAttribute("StartDate")]
		[Bindable(true)]
		public DateTime StartDate
		{
			get { return GetColumnValue<DateTime>(Columns.StartDate); }
			set { SetColumnValue(Columns.StartDate, value); }
		}

		[XmlAttribute("EndTime")]
		[Bindable(true)]
		public DateTime EndTime
		{
			get { return GetColumnValue<DateTime>(Columns.EndTime); }
			set { SetColumnValue(Columns.EndTime, value); }
		}

		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}

		[XmlAttribute("ModifyTime")]
		[Bindable(true)]
		public DateTime? ModifyTime
		{
			get { return GetColumnValue<DateTime?>(Columns.ModifyTime); }
			set { SetColumnValue(Columns.ModifyTime, value); }
		}

		[XmlAttribute("ModifyUser")]
		[Bindable(true)]
		public string ModifyUser
		{
			get { return GetColumnValue<string>(Columns.ModifyUser); }
			set { SetColumnValue(Columns.ModifyUser, value); }
		}

		[XmlAttribute("MainBackgroundImage")]
		[Bindable(true)]
		public string MainBackgroundImage
		{
			get { return GetColumnValue<string>(Columns.MainBackgroundImage); }
			set { SetColumnValue(Columns.MainBackgroundImage, value); }
		}

		[XmlAttribute("MainSpacerImage")]
		[Bindable(true)]
		public string MainSpacerImage
		{
			get { return GetColumnValue<string>(Columns.MainSpacerImage); }
			set { SetColumnValue(Columns.MainSpacerImage, value); }
		}

		[XmlAttribute("WebRuleContent")]
		[Bindable(true)]
		public string WebRuleContent
		{
			get { return GetColumnValue<string>(Columns.WebRuleContent); }
			set { SetColumnValue(Columns.WebRuleContent, value); }
		}

		[XmlAttribute("MobileRuleContent")]
		[Bindable(true)]
		public string MobileRuleContent
		{
			get { return GetColumnValue<string>(Columns.MobileRuleContent); }
			set { SetColumnValue(Columns.MobileRuleContent, value); }
		}

		[XmlAttribute("ThemeOtherPic")]
		[Bindable(true)]
		public string ThemeOtherPic
		{
			get { return GetColumnValue<string>(Columns.ThemeOtherPic); }
			set { SetColumnValue(Columns.ThemeOtherPic, value); }
		}

		[XmlAttribute("ShowInWeb")]
		[Bindable(true)]
		public bool ShowInWeb
		{
			get { return GetColumnValue<bool>(Columns.ShowInWeb); }
			set { SetColumnValue(Columns.ShowInWeb, value); }
		}

		[XmlAttribute("ShowInApp")]
		[Bindable(true)]
		public bool ShowInApp
		{
			get { return GetColumnValue<bool>(Columns.ShowInApp); }
			set { SetColumnValue(Columns.ShowInApp, value); }
		}

		[XmlAttribute("FbShareTitle")]
		[Bindable(true)]
		public string FbShareTitle
		{
			get { return GetColumnValue<string>(Columns.FbShareTitle); }
			set { SetColumnValue(Columns.FbShareTitle, value); }
		}

		[XmlAttribute("FbShareIamge")]
		[Bindable(true)]
		public string FbShareIamge
		{
			get { return GetColumnValue<string>(Columns.FbShareIamge); }
			set { SetColumnValue(Columns.FbShareIamge, value); }
		}

		[XmlAttribute("CssStyle")]
		[Bindable(true)]
		public string CssStyle
		{
			get { return GetColumnValue<string>(Columns.CssStyle); }
			set { SetColumnValue(Columns.CssStyle, value); }
		}

		[XmlAttribute("ThemeType")]
		[Bindable(true)]
		public int ThemeType
		{
			get { return GetColumnValue<int>(Columns.ThemeType); }
			set { SetColumnValue(Columns.ThemeType, value); }
		}

		[XmlAttribute("ChannelTitle")]
		[Bindable(true)]
		public string ChannelTitle
		{
			get { return GetColumnValue<string>(Columns.ChannelTitle); }
			set { SetColumnValue(Columns.ChannelTitle, value); }
		}

		[XmlAttribute("ChannelImage")]
		[Bindable(true)]
		public string ChannelImage
		{
			get { return GetColumnValue<string>(Columns.ChannelImage); }
			set { SetColumnValue(Columns.ChannelImage, value); }
		}

		[XmlAttribute("WebCategoryImage")]
		[Bindable(true)]
		public string WebCategoryImage
		{
			get { return GetColumnValue<string>(Columns.WebCategoryImage); }
			set { SetColumnValue(Columns.WebCategoryImage, value); }
		}

		[XmlAttribute("WebRuleBanner")]
		[Bindable(true)]
		public string WebRuleBanner
		{
			get { return GetColumnValue<string>(Columns.WebRuleBanner); }
			set { SetColumnValue(Columns.WebRuleBanner, value); }
		}

		[XmlAttribute("MobileCategoryImage")]
		[Bindable(true)]
		public string MobileCategoryImage
		{
			get { return GetColumnValue<string>(Columns.MobileCategoryImage); }
			set { SetColumnValue(Columns.MobileCategoryImage, value); }
		}

		[XmlAttribute("MobileRuleBanner")]
		[Bindable(true)]
		public string MobileRuleBanner
		{
			get { return GetColumnValue<string>(Columns.MobileRuleBanner); }
			set { SetColumnValue(Columns.MobileRuleBanner, value); }
		}

		[XmlAttribute("MobileCover")]
		[Bindable(true)]
		public string MobileCover
		{
			get { return GetColumnValue<string>(Columns.MobileCover); }
			set { SetColumnValue(Columns.MobileCover, value); }
		}

		[XmlAttribute("AppIndexImage")]
		[Bindable(true)]
		public string AppIndexImage
		{
			get { return GetColumnValue<string>(Columns.AppIndexImage); }
			set { SetColumnValue(Columns.AppIndexImage, value); }
		}

		[XmlAttribute("RedirectUrl")]
		[Bindable(true)]
		public string RedirectUrl
		{
			get { return GetColumnValue<string>(Columns.RedirectUrl); }
			set { SetColumnValue(Columns.RedirectUrl, value); }
		}

		#endregion

		#region Typed Columns

		public static TableSchema.TableColumn IdColumn
		{
			get { return Schema.Columns[0]; }
		}

		public static TableSchema.TableColumn TitleColumn
		{
			get { return Schema.Columns[1]; }
		}

		public static TableSchema.TableColumn MainImageColumn
		{
			get { return Schema.Columns[2]; }
		}

		public static TableSchema.TableColumn MobileMainImageColumn
		{
			get { return Schema.Columns[3]; }
		}

		public static TableSchema.TableColumn StartDateColumn
		{
			get { return Schema.Columns[4]; }
		}

		public static TableSchema.TableColumn EndTimeColumn
		{
			get { return Schema.Columns[5]; }
		}

		public static TableSchema.TableColumn CreateTimeColumn
		{
			get { return Schema.Columns[6]; }
		}

		public static TableSchema.TableColumn ModifyTimeColumn
		{
			get { return Schema.Columns[7]; }
		}

		public static TableSchema.TableColumn ModifyUserColumn
		{
			get { return Schema.Columns[8]; }
		}

		public static TableSchema.TableColumn MainBackgroundImageColumn
		{
			get { return Schema.Columns[9]; }
		}

		public static TableSchema.TableColumn MainSpacerImageColumn
		{
			get { return Schema.Columns[10]; }
		}

		public static TableSchema.TableColumn WebRuleContentColumn
		{
			get { return Schema.Columns[11]; }
		}

		public static TableSchema.TableColumn MobileRuleContentColumn
		{
			get { return Schema.Columns[12]; }
		}

		public static TableSchema.TableColumn ThemeOtherPicColumn
		{
			get { return Schema.Columns[13]; }
		}

		public static TableSchema.TableColumn ShowInWebColumn
		{
			get { return Schema.Columns[14]; }
		}

		public static TableSchema.TableColumn ShowInAppColumn
		{
			get { return Schema.Columns[15]; }
		}

		public static TableSchema.TableColumn FbShareTitleColumn
		{
			get { return Schema.Columns[16]; }
		}

		public static TableSchema.TableColumn FbShareIamgeColumn
		{
			get { return Schema.Columns[17]; }
		}

		public static TableSchema.TableColumn CssStyleColumn
		{
			get { return Schema.Columns[18]; }
		}

		public static TableSchema.TableColumn ThemeTypeColumn
		{
			get { return Schema.Columns[19]; }
		}

		public static TableSchema.TableColumn ChannelTitleColumn
		{
			get { return Schema.Columns[20]; }
		}

		public static TableSchema.TableColumn ChannelImageColumn
		{
			get { return Schema.Columns[21]; }
		}

		public static TableSchema.TableColumn WebCategoryImageColumn
		{
			get { return Schema.Columns[22]; }
		}

		public static TableSchema.TableColumn WebRuleBannerColumn
		{
			get { return Schema.Columns[23]; }
		}

		public static TableSchema.TableColumn MobileCategoryImageColumn
		{
			get { return Schema.Columns[24]; }
		}

		public static TableSchema.TableColumn MobileRuleBannerColumn
		{
			get { return Schema.Columns[25]; }
		}

		public static TableSchema.TableColumn MobileCoverColumn
		{
			get { return Schema.Columns[26]; }
		}

		public static TableSchema.TableColumn AppIndexImageColumn
		{
			get { return Schema.Columns[27]; }
		}

		public static TableSchema.TableColumn RedirectUrlColumn
		{
			get { return Schema.Columns[28]; }
		}

		#endregion

		#region Columns Struct

		public struct Columns
		{
			public static string Id = @"id";
			public static string Title = @"title";
			public static string MainImage = @"main_image";
			public static string MobileMainImage = @"mobile_main_image";
			public static string StartDate = @"start_date";
			public static string EndTime = @"end_time";
			public static string CreateTime = @"create_time";
			public static string ModifyTime = @"modify_time";
			public static string ModifyUser = @"modify_user";
			public static string MainBackgroundImage = @"main_background_image";
			public static string MainSpacerImage = @"main_spacer_image";
			public static string WebRuleContent = @"web_rule_content";
			public static string MobileRuleContent = @"mobile_rule_content";
			public static string ThemeOtherPic = @"theme_other_pic";
			public static string ShowInWeb = @"show_in_web";
			public static string ShowInApp = @"show_in_app";
			public static string FbShareTitle = @"fb_share_title";
			public static string FbShareIamge = @"fb_share_iamge";
			public static string CssStyle = @"css_style";
			public static string ThemeType = @"theme_type";
			public static string ChannelTitle = @"channel_title";
			public static string ChannelImage = @"channel_image";
			public static string WebCategoryImage = @"web_category_image";
			public static string WebRuleBanner = @"web_rule_banner";
			public static string MobileCategoryImage = @"mobile_category_image";
			public static string MobileRuleBanner = @"mobile_rule_banner";
			public static string MobileCover = @"mobile_cover";
			public static string AppIndexImage = @"app_index_image";
			public static string RedirectUrl = @"redirect_url";
		}

		#endregion

	}
}

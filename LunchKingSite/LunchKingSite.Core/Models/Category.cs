using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the Category class.
	/// </summary>
    [Serializable]
	public partial class CategoryCollection : RepositoryList<Category, CategoryCollection>
	{	   
		public CategoryCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>CategoryCollection</returns>
		public CategoryCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                Category o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the category table.
	/// </summary>
	[Serializable]
	public partial class Category : RepositoryRecord<Category>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public Category()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public Category(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("category", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = false;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarName = new TableSchema.TableColumn(schema);
				colvarName.ColumnName = "name";
				colvarName.DataType = DbType.String;
				colvarName.MaxLength = 50;
				colvarName.AutoIncrement = false;
				colvarName.IsNullable = true;
				colvarName.IsPrimaryKey = false;
				colvarName.IsForeignKey = false;
				colvarName.IsReadOnly = false;
				colvarName.DefaultSetting = @"";
				colvarName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarName);
				
				TableSchema.TableColumn colvarRank = new TableSchema.TableColumn(schema);
				colvarRank.ColumnName = "rank";
				colvarRank.DataType = DbType.Int32;
				colvarRank.MaxLength = 0;
				colvarRank.AutoIncrement = false;
				colvarRank.IsNullable = true;
				colvarRank.IsPrimaryKey = false;
				colvarRank.IsForeignKey = false;
				colvarRank.IsReadOnly = false;
				colvarRank.DefaultSetting = @"";
				colvarRank.ForeignKeyTableName = "";
				schema.Columns.Add(colvarRank);
				
				TableSchema.TableColumn colvarCode = new TableSchema.TableColumn(schema);
				colvarCode.ColumnName = "code";
				colvarCode.DataType = DbType.Int32;
				colvarCode.MaxLength = 0;
				colvarCode.AutoIncrement = false;
				colvarCode.IsNullable = true;
				colvarCode.IsPrimaryKey = false;
				colvarCode.IsForeignKey = false;
				colvarCode.IsReadOnly = false;
				colvarCode.DefaultSetting = @"";
				colvarCode.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCode);
				
				TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
				colvarCreateId.ColumnName = "create_id";
				colvarCreateId.DataType = DbType.String;
				colvarCreateId.MaxLength = 50;
				colvarCreateId.AutoIncrement = false;
				colvarCreateId.IsNullable = true;
				colvarCreateId.IsPrimaryKey = false;
				colvarCreateId.IsForeignKey = false;
				colvarCreateId.IsReadOnly = false;
				colvarCreateId.DefaultSetting = @"";
				colvarCreateId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateId);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = true;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarModifyId = new TableSchema.TableColumn(schema);
				colvarModifyId.ColumnName = "modify_id";
				colvarModifyId.DataType = DbType.String;
				colvarModifyId.MaxLength = 50;
				colvarModifyId.AutoIncrement = false;
				colvarModifyId.IsNullable = true;
				colvarModifyId.IsPrimaryKey = false;
				colvarModifyId.IsForeignKey = false;
				colvarModifyId.IsReadOnly = false;
				colvarModifyId.DefaultSetting = @"";
				colvarModifyId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyId);
				
				TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
				colvarModifyTime.ColumnName = "modify_time";
				colvarModifyTime.DataType = DbType.DateTime;
				colvarModifyTime.MaxLength = 0;
				colvarModifyTime.AutoIncrement = false;
				colvarModifyTime.IsNullable = true;
				colvarModifyTime.IsPrimaryKey = false;
				colvarModifyTime.IsForeignKey = false;
				colvarModifyTime.IsReadOnly = false;
				colvarModifyTime.DefaultSetting = @"";
				colvarModifyTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyTime);
				
				TableSchema.TableColumn colvarType = new TableSchema.TableColumn(schema);
				colvarType.ColumnName = "type";
				colvarType.DataType = DbType.Int32;
				colvarType.MaxLength = 0;
				colvarType.AutoIncrement = false;
				colvarType.IsNullable = true;
				colvarType.IsPrimaryKey = false;
				colvarType.IsForeignKey = false;
				colvarType.IsReadOnly = false;
				
						colvarType.DefaultSetting = @"((0))";
				colvarType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarType);
				
				TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
				colvarStatus.ColumnName = "status";
				colvarStatus.DataType = DbType.Int32;
				colvarStatus.MaxLength = 0;
				colvarStatus.AutoIncrement = false;
				colvarStatus.IsNullable = false;
				colvarStatus.IsPrimaryKey = false;
				colvarStatus.IsForeignKey = false;
				colvarStatus.IsReadOnly = false;
				
						colvarStatus.DefaultSetting = @"((1))";
				colvarStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStatus);
				
				TableSchema.TableColumn colvarParentCode = new TableSchema.TableColumn(schema);
				colvarParentCode.ColumnName = "parent_code";
				colvarParentCode.DataType = DbType.Int32;
				colvarParentCode.MaxLength = 0;
				colvarParentCode.AutoIncrement = false;
				colvarParentCode.IsNullable = true;
				colvarParentCode.IsPrimaryKey = false;
				colvarParentCode.IsForeignKey = false;
				colvarParentCode.IsReadOnly = false;
				colvarParentCode.DefaultSetting = @"";
				colvarParentCode.ForeignKeyTableName = "";
				schema.Columns.Add(colvarParentCode);
				
				TableSchema.TableColumn colvarIsFinal = new TableSchema.TableColumn(schema);
				colvarIsFinal.ColumnName = "is_final";
				colvarIsFinal.DataType = DbType.Boolean;
				colvarIsFinal.MaxLength = 0;
				colvarIsFinal.AutoIncrement = false;
				colvarIsFinal.IsNullable = false;
				colvarIsFinal.IsPrimaryKey = false;
				colvarIsFinal.IsForeignKey = false;
				colvarIsFinal.IsReadOnly = false;
				
						colvarIsFinal.DefaultSetting = @"((1))";
				colvarIsFinal.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsFinal);
				
				TableSchema.TableColumn colvarNameInConsole = new TableSchema.TableColumn(schema);
				colvarNameInConsole.ColumnName = "name_in_console";
				colvarNameInConsole.DataType = DbType.String;
				colvarNameInConsole.MaxLength = 50;
				colvarNameInConsole.AutoIncrement = false;
				colvarNameInConsole.IsNullable = true;
				colvarNameInConsole.IsPrimaryKey = false;
				colvarNameInConsole.IsForeignKey = false;
				colvarNameInConsole.IsReadOnly = false;
				colvarNameInConsole.DefaultSetting = @"";
				colvarNameInConsole.ForeignKeyTableName = "";
				schema.Columns.Add(colvarNameInConsole);
				
				TableSchema.TableColumn colvarIsShowFrontEnd = new TableSchema.TableColumn(schema);
				colvarIsShowFrontEnd.ColumnName = "is_show_front_end";
				colvarIsShowFrontEnd.DataType = DbType.Boolean;
				colvarIsShowFrontEnd.MaxLength = 0;
				colvarIsShowFrontEnd.AutoIncrement = false;
				colvarIsShowFrontEnd.IsNullable = false;
				colvarIsShowFrontEnd.IsPrimaryKey = false;
				colvarIsShowFrontEnd.IsForeignKey = false;
				colvarIsShowFrontEnd.IsReadOnly = false;
				
						colvarIsShowFrontEnd.DefaultSetting = @"((0))";
				colvarIsShowFrontEnd.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsShowFrontEnd);
				
				TableSchema.TableColumn colvarIconType = new TableSchema.TableColumn(schema);
				colvarIconType.ColumnName = "icon_type";
				colvarIconType.DataType = DbType.Int32;
				colvarIconType.MaxLength = 0;
				colvarIconType.AutoIncrement = false;
				colvarIconType.IsNullable = false;
				colvarIconType.IsPrimaryKey = false;
				colvarIconType.IsForeignKey = false;
				colvarIconType.IsReadOnly = false;
				
						colvarIconType.DefaultSetting = @"((0))";
				colvarIconType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIconType);
				
				TableSchema.TableColumn colvarIsShowBackEnd = new TableSchema.TableColumn(schema);
				colvarIsShowBackEnd.ColumnName = "is_show_back_end";
				colvarIsShowBackEnd.DataType = DbType.Boolean;
				colvarIsShowBackEnd.MaxLength = 0;
				colvarIsShowBackEnd.AutoIncrement = false;
				colvarIsShowBackEnd.IsNullable = false;
				colvarIsShowBackEnd.IsPrimaryKey = false;
				colvarIsShowBackEnd.IsForeignKey = false;
				colvarIsShowBackEnd.IsReadOnly = false;
				
						colvarIsShowBackEnd.DefaultSetting = @"((0))";
				colvarIsShowBackEnd.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsShowBackEnd);
				
				TableSchema.TableColumn colvarImage = new TableSchema.TableColumn(schema);
				colvarImage.ColumnName = "image";
				colvarImage.DataType = DbType.String;
				colvarImage.MaxLength = 200;
				colvarImage.AutoIncrement = false;
				colvarImage.IsNullable = true;
				colvarImage.IsPrimaryKey = false;
				colvarImage.IsForeignKey = false;
				colvarImage.IsReadOnly = false;
				colvarImage.DefaultSetting = @"";
				colvarImage.ForeignKeyTableName = "";
				schema.Columns.Add(colvarImage);

				TableSchema.TableColumn colvarAlibabaId = new TableSchema.TableColumn(schema);
				colvarAlibabaId.ColumnName = "alibaba_id";
				colvarAlibabaId.DataType = DbType.Int64;
				colvarAlibabaId.MaxLength = 0;
				colvarAlibabaId.AutoIncrement = false;
				colvarAlibabaId.IsNullable = true;
				colvarAlibabaId.IsPrimaryKey = false;
				colvarAlibabaId.IsForeignKey = false;
				colvarAlibabaId.IsReadOnly = false;
				colvarAlibabaId.DefaultSetting = @"";
				colvarAlibabaId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAlibabaId);

				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("category",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("Name")]
		[Bindable(true)]
		public string Name 
		{
			get { return GetColumnValue<string>(Columns.Name); }
			set { SetColumnValue(Columns.Name, value); }
		}
		  
		[XmlAttribute("Rank")]
		[Bindable(true)]
		public int? Rank 
		{
			get { return GetColumnValue<int?>(Columns.Rank); }
			set { SetColumnValue(Columns.Rank, value); }
		}
		  
		[XmlAttribute("Code")]
		[Bindable(true)]
		public int? Code 
		{
			get { return GetColumnValue<int?>(Columns.Code); }
			set { SetColumnValue(Columns.Code, value); }
		}
		  
		[XmlAttribute("CreateId")]
		[Bindable(true)]
		public string CreateId 
		{
			get { return GetColumnValue<string>(Columns.CreateId); }
			set { SetColumnValue(Columns.CreateId, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime? CreateTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		  
		[XmlAttribute("ModifyId")]
		[Bindable(true)]
		public string ModifyId 
		{
			get { return GetColumnValue<string>(Columns.ModifyId); }
			set { SetColumnValue(Columns.ModifyId, value); }
		}
		  
		[XmlAttribute("ModifyTime")]
		[Bindable(true)]
		public DateTime? ModifyTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.ModifyTime); }
			set { SetColumnValue(Columns.ModifyTime, value); }
		}
		  
		[XmlAttribute("Type")]
		[Bindable(true)]
		public int? Type 
		{
			get { return GetColumnValue<int?>(Columns.Type); }
			set { SetColumnValue(Columns.Type, value); }
		}
		  
		[XmlAttribute("Status")]
		[Bindable(true)]
		public int Status 
		{
			get { return GetColumnValue<int>(Columns.Status); }
			set { SetColumnValue(Columns.Status, value); }
		}
		  
		[XmlAttribute("ParentCode")]
		[Bindable(true)]
		public int? ParentCode 
		{
			get { return GetColumnValue<int?>(Columns.ParentCode); }
			set { SetColumnValue(Columns.ParentCode, value); }
		}
		  
		[XmlAttribute("IsFinal")]
		[Bindable(true)]
		public bool IsFinal 
		{
			get { return GetColumnValue<bool>(Columns.IsFinal); }
			set { SetColumnValue(Columns.IsFinal, value); }
		}
		  
		[XmlAttribute("NameInConsole")]
		[Bindable(true)]
		public string NameInConsole 
		{
			get { return GetColumnValue<string>(Columns.NameInConsole); }
			set { SetColumnValue(Columns.NameInConsole, value); }
		}
		  
		[XmlAttribute("IsShowFrontEnd")]
		[Bindable(true)]
		public bool IsShowFrontEnd 
		{
			get { return GetColumnValue<bool>(Columns.IsShowFrontEnd); }
			set { SetColumnValue(Columns.IsShowFrontEnd, value); }
		}
		  
		[XmlAttribute("IconType")]
		[Bindable(true)]
		public int IconType 
		{
			get { return GetColumnValue<int>(Columns.IconType); }
			set { SetColumnValue(Columns.IconType, value); }
		}
		  
		[XmlAttribute("IsShowBackEnd")]
		[Bindable(true)]
		public bool IsShowBackEnd 
		{
			get { return GetColumnValue<bool>(Columns.IsShowBackEnd); }
			set { SetColumnValue(Columns.IsShowBackEnd, value); }
		}
		  
		[XmlAttribute("Image")]
		[Bindable(true)]
		public string Image 
		{
			get { return GetColumnValue<string>(Columns.Image); }
			set { SetColumnValue(Columns.Image, value); }
		}

		[XmlAttribute("AlibabaId")]
		[Bindable(true)]
		public long? AlibabaId
		{
			get { return GetColumnValue<long?>(Columns.AlibabaId); }
			set { SetColumnValue(Columns.AlibabaId, value); }
		}

		#endregion




		//no foreign key tables defined (0)



		//no ManyToMany tables defined (1)





		#region Typed Columns


		public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn NameColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn RankColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn CodeColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyIdColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn TypeColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn StatusColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn ParentCodeColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        public static TableSchema.TableColumn IsFinalColumn
        {
            get { return Schema.Columns[11]; }
        }
        
        
        
        public static TableSchema.TableColumn NameInConsoleColumn
        {
            get { return Schema.Columns[12]; }
        }
        
        
        
        public static TableSchema.TableColumn IsShowFrontEndColumn
        {
            get { return Schema.Columns[13]; }
        }
        
        
        
        public static TableSchema.TableColumn IconTypeColumn
        {
            get { return Schema.Columns[14]; }
        }
        
        
        
        public static TableSchema.TableColumn IsShowBackEndColumn
        {
            get { return Schema.Columns[15]; }
        }
        
        
        
        public static TableSchema.TableColumn ImageColumn
        {
            get { return Schema.Columns[16]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string Name = @"name";
			 public static string Rank = @"rank";
			 public static string Code = @"code";
			 public static string CreateId = @"create_id";
			 public static string CreateTime = @"create_time";
			 public static string ModifyId = @"modify_id";
			 public static string ModifyTime = @"modify_time";
			 public static string Type = @"type";
			 public static string Status = @"status";
			 public static string ParentCode = @"parent_code";
			 public static string IsFinal = @"is_final";
			 public static string NameInConsole = @"name_in_console";
			 public static string IsShowFrontEnd = @"is_show_front_end";
			 public static string IconType = @"icon_type";
			 public static string IsShowBackEnd = @"is_show_back_end";
			 public static string Image = @"image";
			 public static string AlibabaId = @"alibaba_id";


		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewYahooVoucherCategory class.
    /// </summary>
    [Serializable]
    public partial class ViewYahooVoucherCategoryCollection : ReadOnlyList<ViewYahooVoucherCategory, ViewYahooVoucherCategoryCollection>
    {        
        public ViewYahooVoucherCategoryCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_yahoo_voucher_category view.
    /// </summary>
    [Serializable]
    public partial class ViewYahooVoucherCategory : ReadOnlyRecord<ViewYahooVoucherCategory>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_yahoo_voucher_category", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarName = new TableSchema.TableColumn(schema);
                colvarName.ColumnName = "name";
                colvarName.DataType = DbType.String;
                colvarName.MaxLength = 50;
                colvarName.AutoIncrement = false;
                colvarName.IsNullable = true;
                colvarName.IsPrimaryKey = false;
                colvarName.IsForeignKey = false;
                colvarName.IsReadOnly = false;
                
                schema.Columns.Add(colvarName);
                
                TableSchema.TableColumn colvarCategoryId = new TableSchema.TableColumn(schema);
                colvarCategoryId.ColumnName = "category_id";
                colvarCategoryId.DataType = DbType.Int32;
                colvarCategoryId.MaxLength = 0;
                colvarCategoryId.AutoIncrement = false;
                colvarCategoryId.IsNullable = false;
                colvarCategoryId.IsPrimaryKey = false;
                colvarCategoryId.IsForeignKey = false;
                colvarCategoryId.IsReadOnly = false;
                
                schema.Columns.Add(colvarCategoryId);
                
                TableSchema.TableColumn colvarCategoryType = new TableSchema.TableColumn(schema);
                colvarCategoryType.ColumnName = "category_type";
                colvarCategoryType.DataType = DbType.Int32;
                colvarCategoryType.MaxLength = 0;
                colvarCategoryType.AutoIncrement = false;
                colvarCategoryType.IsNullable = true;
                colvarCategoryType.IsPrimaryKey = false;
                colvarCategoryType.IsForeignKey = false;
                colvarCategoryType.IsReadOnly = false;
                
                schema.Columns.Add(colvarCategoryType);
                
                TableSchema.TableColumn colvarEventId = new TableSchema.TableColumn(schema);
                colvarEventId.ColumnName = "event_id";
                colvarEventId.DataType = DbType.Int32;
                colvarEventId.MaxLength = 0;
                colvarEventId.AutoIncrement = false;
                colvarEventId.IsNullable = false;
                colvarEventId.IsPrimaryKey = false;
                colvarEventId.IsForeignKey = false;
                colvarEventId.IsReadOnly = false;
                
                schema.Columns.Add(colvarEventId);
                
                TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
                colvarStatus.ColumnName = "status";
                colvarStatus.DataType = DbType.Int32;
                colvarStatus.MaxLength = 0;
                colvarStatus.AutoIncrement = false;
                colvarStatus.IsNullable = false;
                colvarStatus.IsPrimaryKey = false;
                colvarStatus.IsForeignKey = false;
                colvarStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarStatus);
                
                TableSchema.TableColumn colvarEnable = new TableSchema.TableColumn(schema);
                colvarEnable.ColumnName = "enable";
                colvarEnable.DataType = DbType.Boolean;
                colvarEnable.MaxLength = 0;
                colvarEnable.AutoIncrement = false;
                colvarEnable.IsNullable = false;
                colvarEnable.IsPrimaryKey = false;
                colvarEnable.IsForeignKey = false;
                colvarEnable.IsReadOnly = false;
                
                schema.Columns.Add(colvarEnable);
                
                TableSchema.TableColumn colvarType = new TableSchema.TableColumn(schema);
                colvarType.ColumnName = "type";
                colvarType.DataType = DbType.Int32;
                colvarType.MaxLength = 0;
                colvarType.AutoIncrement = false;
                colvarType.IsNullable = false;
                colvarType.IsPrimaryKey = false;
                colvarType.IsForeignKey = false;
                colvarType.IsReadOnly = false;
                
                schema.Columns.Add(colvarType);
                
                TableSchema.TableColumn colvarStartDate = new TableSchema.TableColumn(schema);
                colvarStartDate.ColumnName = "start_date";
                colvarStartDate.DataType = DbType.DateTime;
                colvarStartDate.MaxLength = 0;
                colvarStartDate.AutoIncrement = false;
                colvarStartDate.IsNullable = true;
                colvarStartDate.IsPrimaryKey = false;
                colvarStartDate.IsForeignKey = false;
                colvarStartDate.IsReadOnly = false;
                
                schema.Columns.Add(colvarStartDate);
                
                TableSchema.TableColumn colvarEndDate = new TableSchema.TableColumn(schema);
                colvarEndDate.ColumnName = "end_date";
                colvarEndDate.DataType = DbType.DateTime;
                colvarEndDate.MaxLength = 0;
                colvarEndDate.AutoIncrement = false;
                colvarEndDate.IsNullable = true;
                colvarEndDate.IsPrimaryKey = false;
                colvarEndDate.IsForeignKey = false;
                colvarEndDate.IsReadOnly = false;
                
                schema.Columns.Add(colvarEndDate);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_yahoo_voucher_category",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewYahooVoucherCategory()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewYahooVoucherCategory(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewYahooVoucherCategory(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewYahooVoucherCategory(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("Name")]
        [Bindable(true)]
        public string Name 
	    {
		    get
		    {
			    return GetColumnValue<string>("name");
		    }
            set 
		    {
			    SetColumnValue("name", value);
            }
        }
	      
        [XmlAttribute("CategoryId")]
        [Bindable(true)]
        public int CategoryId 
	    {
		    get
		    {
			    return GetColumnValue<int>("category_id");
		    }
            set 
		    {
			    SetColumnValue("category_id", value);
            }
        }
	      
        [XmlAttribute("CategoryType")]
        [Bindable(true)]
        public int? CategoryType 
	    {
		    get
		    {
			    return GetColumnValue<int?>("category_type");
		    }
            set 
		    {
			    SetColumnValue("category_type", value);
            }
        }
	      
        [XmlAttribute("EventId")]
        [Bindable(true)]
        public int EventId 
	    {
		    get
		    {
			    return GetColumnValue<int>("event_id");
		    }
            set 
		    {
			    SetColumnValue("event_id", value);
            }
        }
	      
        [XmlAttribute("Status")]
        [Bindable(true)]
        public int Status 
	    {
		    get
		    {
			    return GetColumnValue<int>("status");
		    }
            set 
		    {
			    SetColumnValue("status", value);
            }
        }
	      
        [XmlAttribute("Enable")]
        [Bindable(true)]
        public bool Enable 
	    {
		    get
		    {
			    return GetColumnValue<bool>("enable");
		    }
            set 
		    {
			    SetColumnValue("enable", value);
            }
        }
	      
        [XmlAttribute("Type")]
        [Bindable(true)]
        public int Type 
	    {
		    get
		    {
			    return GetColumnValue<int>("type");
		    }
            set 
		    {
			    SetColumnValue("type", value);
            }
        }
	      
        [XmlAttribute("StartDate")]
        [Bindable(true)]
        public DateTime? StartDate 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("start_date");
		    }
            set 
		    {
			    SetColumnValue("start_date", value);
            }
        }
	      
        [XmlAttribute("EndDate")]
        [Bindable(true)]
        public DateTime? EndDate 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("end_date");
		    }
            set 
		    {
			    SetColumnValue("end_date", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string Name = @"name";
            
            public static string CategoryId = @"category_id";
            
            public static string CategoryType = @"category_type";
            
            public static string EventId = @"event_id";
            
            public static string Status = @"status";
            
            public static string Enable = @"enable";
            
            public static string Type = @"type";
            
            public static string StartDate = @"start_date";
            
            public static string EndDate = @"end_date";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

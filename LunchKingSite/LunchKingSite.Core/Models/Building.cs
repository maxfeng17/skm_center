using System;
using System.ComponentModel;
using System.Data;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using Microsoft.SqlServer.Types;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the Building class.
	/// </summary>
    [Serializable]
	public partial class BuildingCollection : RepositoryList<Building, BuildingCollection>
	{	   
		public BuildingCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>BuildingCollection</returns>
		public BuildingCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                Building o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the building table.
	/// </summary>
	[Serializable]
	public partial class Building : RepositoryRecord<Building>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public Building()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public Building(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("building", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarGuid = new TableSchema.TableColumn(schema);
				colvarGuid.ColumnName = "GUID";
				colvarGuid.DataType = DbType.Guid;
				colvarGuid.MaxLength = 0;
				colvarGuid.AutoIncrement = false;
				colvarGuid.IsNullable = false;
				colvarGuid.IsPrimaryKey = true;
				colvarGuid.IsForeignKey = false;
				colvarGuid.IsReadOnly = false;
				colvarGuid.DefaultSetting = @"";
				colvarGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGuid);
				
				TableSchema.TableColumn colvarBuildingId = new TableSchema.TableColumn(schema);
				colvarBuildingId.ColumnName = "building_id";
				colvarBuildingId.DataType = DbType.AnsiString;
				colvarBuildingId.MaxLength = 20;
				colvarBuildingId.AutoIncrement = false;
				colvarBuildingId.IsNullable = true;
				colvarBuildingId.IsPrimaryKey = false;
				colvarBuildingId.IsForeignKey = false;
				colvarBuildingId.IsReadOnly = false;
				colvarBuildingId.DefaultSetting = @"";
				colvarBuildingId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBuildingId);
				
				TableSchema.TableColumn colvarBuildingName = new TableSchema.TableColumn(schema);
				colvarBuildingName.ColumnName = "building_name";
				colvarBuildingName.DataType = DbType.String;
				colvarBuildingName.MaxLength = 50;
				colvarBuildingName.AutoIncrement = false;
				colvarBuildingName.IsNullable = false;
				colvarBuildingName.IsPrimaryKey = false;
				colvarBuildingName.IsForeignKey = false;
				colvarBuildingName.IsReadOnly = false;
				colvarBuildingName.DefaultSetting = @"";
				colvarBuildingName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBuildingName);
				
				TableSchema.TableColumn colvarCityId = new TableSchema.TableColumn(schema);
				colvarCityId.ColumnName = "city_id";
				colvarCityId.DataType = DbType.Int32;
				colvarCityId.MaxLength = 0;
				colvarCityId.AutoIncrement = false;
				colvarCityId.IsNullable = false;
				colvarCityId.IsPrimaryKey = false;
				colvarCityId.IsForeignKey = true;
				colvarCityId.IsReadOnly = false;
				colvarCityId.DefaultSetting = @"";
				
					colvarCityId.ForeignKeyTableName = "city";
				schema.Columns.Add(colvarCityId);
				
				TableSchema.TableColumn colvarBuildingStreetName = new TableSchema.TableColumn(schema);
				colvarBuildingStreetName.ColumnName = "building_street_name";
				colvarBuildingStreetName.DataType = DbType.String;
				colvarBuildingStreetName.MaxLength = 100;
				colvarBuildingStreetName.AutoIncrement = false;
				colvarBuildingStreetName.IsNullable = false;
				colvarBuildingStreetName.IsPrimaryKey = false;
				colvarBuildingStreetName.IsForeignKey = false;
				colvarBuildingStreetName.IsReadOnly = false;
				colvarBuildingStreetName.DefaultSetting = @"";
				colvarBuildingStreetName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBuildingStreetName);
				
				TableSchema.TableColumn colvarBuildingAddressNumber = new TableSchema.TableColumn(schema);
				colvarBuildingAddressNumber.ColumnName = "building_address_number";
				colvarBuildingAddressNumber.DataType = DbType.String;
				colvarBuildingAddressNumber.MaxLength = 100;
				colvarBuildingAddressNumber.AutoIncrement = false;
				colvarBuildingAddressNumber.IsNullable = true;
				colvarBuildingAddressNumber.IsPrimaryKey = false;
				colvarBuildingAddressNumber.IsForeignKey = false;
				colvarBuildingAddressNumber.IsReadOnly = false;
				colvarBuildingAddressNumber.DefaultSetting = @"";
				colvarBuildingAddressNumber.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBuildingAddressNumber);
				
				TableSchema.TableColumn colvarBuildingOnline = new TableSchema.TableColumn(schema);
				colvarBuildingOnline.ColumnName = "building_online";
				colvarBuildingOnline.DataType = DbType.Boolean;
				colvarBuildingOnline.MaxLength = 0;
				colvarBuildingOnline.AutoIncrement = false;
				colvarBuildingOnline.IsNullable = false;
				colvarBuildingOnline.IsPrimaryKey = false;
				colvarBuildingOnline.IsForeignKey = false;
				colvarBuildingOnline.IsReadOnly = false;
				colvarBuildingOnline.DefaultSetting = @"";
				colvarBuildingOnline.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBuildingOnline);
				
				TableSchema.TableColumn colvarBuildingRank = new TableSchema.TableColumn(schema);
				colvarBuildingRank.ColumnName = "building_rank";
				colvarBuildingRank.DataType = DbType.Int32;
				colvarBuildingRank.MaxLength = 0;
				colvarBuildingRank.AutoIncrement = false;
				colvarBuildingRank.IsNullable = false;
				colvarBuildingRank.IsPrimaryKey = false;
				colvarBuildingRank.IsForeignKey = false;
				colvarBuildingRank.IsReadOnly = false;
				
						colvarBuildingRank.DefaultSetting = @"((0))";
				colvarBuildingRank.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBuildingRank);
				
				TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
				colvarCreateId.ColumnName = "create_id";
				colvarCreateId.DataType = DbType.String;
				colvarCreateId.MaxLength = 30;
				colvarCreateId.AutoIncrement = false;
				colvarCreateId.IsNullable = false;
				colvarCreateId.IsPrimaryKey = false;
				colvarCreateId.IsForeignKey = false;
				colvarCreateId.IsReadOnly = false;
				colvarCreateId.DefaultSetting = @"";
				colvarCreateId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateId);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarModifyId = new TableSchema.TableColumn(schema);
				colvarModifyId.ColumnName = "modify_id";
				colvarModifyId.DataType = DbType.String;
				colvarModifyId.MaxLength = 30;
				colvarModifyId.AutoIncrement = false;
				colvarModifyId.IsNullable = true;
				colvarModifyId.IsPrimaryKey = false;
				colvarModifyId.IsForeignKey = false;
				colvarModifyId.IsReadOnly = false;
				colvarModifyId.DefaultSetting = @"";
				colvarModifyId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyId);
				
				TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
				colvarModifyTime.ColumnName = "modify_time";
				colvarModifyTime.DataType = DbType.DateTime;
				colvarModifyTime.MaxLength = 0;
				colvarModifyTime.AutoIncrement = false;
				colvarModifyTime.IsNullable = true;
				colvarModifyTime.IsPrimaryKey = false;
				colvarModifyTime.IsForeignKey = false;
				colvarModifyTime.IsReadOnly = false;
				colvarModifyTime.DefaultSetting = @"";
				colvarModifyTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyTime);
				
				TableSchema.TableColumn colvarCoordinate = new TableSchema.TableColumn(schema);
				colvarCoordinate.ColumnName = "coordinate";
                colvarCoordinate.DataType = DbType.AnsiString;
				colvarCoordinate.MaxLength = -1;
				colvarCoordinate.AutoIncrement = false;
				colvarCoordinate.IsNullable = true;
				colvarCoordinate.IsPrimaryKey = false;
				colvarCoordinate.IsForeignKey = false;
				colvarCoordinate.IsReadOnly = false;
				colvarCoordinate.DefaultSetting = @"";
				colvarCoordinate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCoordinate);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("building",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Guid")]
		[Bindable(true)]
		public Guid Guid 
		{
			get { return GetColumnValue<Guid>(Columns.Guid); }
			set { SetColumnValue(Columns.Guid, value); }
		}
		  
		[XmlAttribute("BuildingId")]
		[Bindable(true)]
		public string BuildingId 
		{
			get { return GetColumnValue<string>(Columns.BuildingId); }
			set { SetColumnValue(Columns.BuildingId, value); }
		}
		  
		[XmlAttribute("BuildingName")]
		[Bindable(true)]
		public string BuildingName 
		{
			get { return GetColumnValue<string>(Columns.BuildingName); }
			set { SetColumnValue(Columns.BuildingName, value); }
		}
		  
		[XmlAttribute("CityId")]
		[Bindable(true)]
		public int CityId 
		{
			get { return GetColumnValue<int>(Columns.CityId); }
			set { SetColumnValue(Columns.CityId, value); }
		}
		  
		[XmlAttribute("BuildingStreetName")]
		[Bindable(true)]
		public string BuildingStreetName 
		{
			get { return GetColumnValue<string>(Columns.BuildingStreetName); }
			set { SetColumnValue(Columns.BuildingStreetName, value); }
		}
		  
		[XmlAttribute("BuildingAddressNumber")]
		[Bindable(true)]
		public string BuildingAddressNumber 
		{
			get { return GetColumnValue<string>(Columns.BuildingAddressNumber); }
			set { SetColumnValue(Columns.BuildingAddressNumber, value); }
		}
		  
		[XmlAttribute("BuildingOnline")]
		[Bindable(true)]
		public bool BuildingOnline 
		{
			get { return GetColumnValue<bool>(Columns.BuildingOnline); }
			set { SetColumnValue(Columns.BuildingOnline, value); }
		}
		  
		[XmlAttribute("BuildingRank")]
		[Bindable(true)]
		public int BuildingRank 
		{
			get { return GetColumnValue<int>(Columns.BuildingRank); }
			set { SetColumnValue(Columns.BuildingRank, value); }
		}
		  
		[XmlAttribute("CreateId")]
		[Bindable(true)]
		public string CreateId 
		{
			get { return GetColumnValue<string>(Columns.CreateId); }
			set { SetColumnValue(Columns.CreateId, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		  
		[XmlAttribute("ModifyId")]
		[Bindable(true)]
		public string ModifyId 
		{
			get { return GetColumnValue<string>(Columns.ModifyId); }
			set { SetColumnValue(Columns.ModifyId, value); }
		}
		  
		[XmlAttribute("ModifyTime")]
		[Bindable(true)]
		public DateTime? ModifyTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.ModifyTime); }
			set { SetColumnValue(Columns.ModifyTime, value); }
		}
		  
		[XmlAttribute("Coordinate")]
		[Bindable(true)]
        public string Coordinate 
		{
			get { return GetColumnValue<object>(Columns.Coordinate) != null ? GetColumnValue<object>(Columns.Coordinate).ToString(): string.Empty; }
			set { SetColumnValue(Columns.Coordinate, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (1)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn GuidColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn BuildingIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn BuildingNameColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn CityIdColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn BuildingStreetNameColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn BuildingAddressNumberColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn BuildingOnlineColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn BuildingRankColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyIdColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[11]; }
        }
        
        
        
        public static TableSchema.TableColumn CoordinateColumn
        {
            get { return Schema.Columns[12]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Guid = @"GUID";
			 public static string BuildingId = @"building_id";
			 public static string BuildingName = @"building_name";
			 public static string CityId = @"city_id";
			 public static string BuildingStreetName = @"building_street_name";
			 public static string BuildingAddressNumber = @"building_address_number";
			 public static string BuildingOnline = @"building_online";
			 public static string BuildingRank = @"building_rank";
			 public static string CreateId = @"create_id";
			 public static string CreateTime = @"create_time";
			 public static string ModifyId = @"modify_id";
			 public static string ModifyTime = @"modify_time";
			 public static string Coordinate = @"coordinate";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

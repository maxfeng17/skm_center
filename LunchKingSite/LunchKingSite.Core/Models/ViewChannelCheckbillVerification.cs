using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewChannelCheckBillVerification class.
    /// </summary>
    [Serializable]
    public partial class ViewChannelCheckBillVerificationCollection : ReadOnlyList<ViewChannelCheckBillVerification, ViewChannelCheckBillVerificationCollection>
    {        
        public ViewChannelCheckBillVerificationCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_channel_check_bill_verification view.
    /// </summary>
    [Serializable]
    public partial class ViewChannelCheckBillVerification : ReadOnlyRecord<ViewChannelCheckBillVerification>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_channel_check_bill_verification", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarCId = new TableSchema.TableColumn(schema);
                colvarCId.ColumnName = "c_id";
                colvarCId.DataType = DbType.String;
                colvarCId.MaxLength = 50;
                colvarCId.AutoIncrement = false;
                colvarCId.IsNullable = true;
                colvarCId.IsPrimaryKey = false;
                colvarCId.IsForeignKey = false;
                colvarCId.IsReadOnly = false;
                
                schema.Columns.Add(colvarCId);
                
                TableSchema.TableColumn colvarItemPrice = new TableSchema.TableColumn(schema);
                colvarItemPrice.ColumnName = "item_price";
                colvarItemPrice.DataType = DbType.Currency;
                colvarItemPrice.MaxLength = 0;
                colvarItemPrice.AutoIncrement = false;
                colvarItemPrice.IsNullable = false;
                colvarItemPrice.IsPrimaryKey = false;
                colvarItemPrice.IsForeignKey = false;
                colvarItemPrice.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemPrice);
                
                TableSchema.TableColumn colvarCost = new TableSchema.TableColumn(schema);
                colvarCost.ColumnName = "cost";
                colvarCost.DataType = DbType.Currency;
                colvarCost.MaxLength = 0;
                colvarCost.AutoIncrement = false;
                colvarCost.IsNullable = true;
                colvarCost.IsPrimaryKey = false;
                colvarCost.IsForeignKey = false;
                colvarCost.IsReadOnly = false;
                
                schema.Columns.Add(colvarCost);
                
                TableSchema.TableColumn colvarVDate = new TableSchema.TableColumn(schema);
                colvarVDate.ColumnName = "v_date";
                colvarVDate.DataType = DbType.DateTime;
                colvarVDate.MaxLength = 0;
                colvarVDate.AutoIncrement = false;
                colvarVDate.IsNullable = true;
                colvarVDate.IsPrimaryKey = false;
                colvarVDate.IsForeignKey = false;
                colvarVDate.IsReadOnly = false;
                
                schema.Columns.Add(colvarVDate);
                
                TableSchema.TableColumn colvarUId = new TableSchema.TableColumn(schema);
                colvarUId.ColumnName = "u_id";
                colvarUId.DataType = DbType.Int32;
                colvarUId.MaxLength = 0;
                colvarUId.AutoIncrement = false;
                colvarUId.IsNullable = false;
                colvarUId.IsPrimaryKey = false;
                colvarUId.IsForeignKey = false;
                colvarUId.IsReadOnly = false;
                
                schema.Columns.Add(colvarUId);
                
                TableSchema.TableColumn colvarUName = new TableSchema.TableColumn(schema);
                colvarUName.ColumnName = "u_name";
                colvarUName.DataType = DbType.String;
                colvarUName.MaxLength = 500;
                colvarUName.AutoIncrement = false;
                colvarUName.IsNullable = true;
                colvarUName.IsPrimaryKey = false;
                colvarUName.IsForeignKey = false;
                colvarUName.IsReadOnly = false;
                
                schema.Columns.Add(colvarUName);
                
                TableSchema.TableColumn colvarStoreName = new TableSchema.TableColumn(schema);
                colvarStoreName.ColumnName = "store_name";
                colvarStoreName.DataType = DbType.String;
                colvarStoreName.MaxLength = 256;
                colvarStoreName.AutoIncrement = false;
                colvarStoreName.IsNullable = true;
                colvarStoreName.IsPrimaryKey = false;
                colvarStoreName.IsForeignKey = false;
                colvarStoreName.IsReadOnly = false;
                
                schema.Columns.Add(colvarStoreName);
                
                TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
                colvarCreateId.ColumnName = "create_id";
                colvarCreateId.DataType = DbType.String;
                colvarCreateId.MaxLength = 50;
                colvarCreateId.AutoIncrement = false;
                colvarCreateId.IsNullable = true;
                colvarCreateId.IsPrimaryKey = false;
                colvarCreateId.IsForeignKey = false;
                colvarCreateId.IsReadOnly = false;
                
                schema.Columns.Add(colvarCreateId);
                
                TableSchema.TableColumn colvarType = new TableSchema.TableColumn(schema);
                colvarType.ColumnName = "type";
                colvarType.DataType = DbType.Int32;
                colvarType.MaxLength = 0;
                colvarType.AutoIncrement = false;
                colvarType.IsNullable = false;
                colvarType.IsPrimaryKey = false;
                colvarType.IsForeignKey = false;
                colvarType.IsReadOnly = false;
                
                schema.Columns.Add(colvarType);
                
                TableSchema.TableColumn colvarBid = new TableSchema.TableColumn(schema);
                colvarBid.ColumnName = "BID";
                colvarBid.DataType = DbType.Guid;
                colvarBid.MaxLength = 0;
                colvarBid.AutoIncrement = false;
                colvarBid.IsNullable = false;
                colvarBid.IsPrimaryKey = false;
                colvarBid.IsForeignKey = false;
                colvarBid.IsReadOnly = false;
                
                schema.Columns.Add(colvarBid);
                
                TableSchema.TableColumn colvarGrossMargin = new TableSchema.TableColumn(schema);
                colvarGrossMargin.ColumnName = "gross_margin";
                colvarGrossMargin.DataType = DbType.Decimal;
                colvarGrossMargin.MaxLength = 0;
                colvarGrossMargin.AutoIncrement = false;
                colvarGrossMargin.IsNullable = true;
                colvarGrossMargin.IsPrimaryKey = false;
                colvarGrossMargin.IsForeignKey = false;
                colvarGrossMargin.IsReadOnly = false;
                
                schema.Columns.Add(colvarGrossMargin);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_channel_check_bill_verification",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewChannelCheckBillVerification()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewChannelCheckBillVerification(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewChannelCheckBillVerification(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewChannelCheckBillVerification(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("CId")]
        [Bindable(true)]
        public string CId 
	    {
		    get
		    {
			    return GetColumnValue<string>("c_id");
		    }
            set 
		    {
			    SetColumnValue("c_id", value);
            }
        }
	      
        [XmlAttribute("ItemPrice")]
        [Bindable(true)]
        public decimal ItemPrice 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("item_price");
		    }
            set 
		    {
			    SetColumnValue("item_price", value);
            }
        }
	      
        [XmlAttribute("Cost")]
        [Bindable(true)]
        public decimal? Cost 
	    {
		    get
		    {
			    return GetColumnValue<decimal?>("cost");
		    }
            set 
		    {
			    SetColumnValue("cost", value);
            }
        }
	      
        [XmlAttribute("VDate")]
        [Bindable(true)]
        public DateTime? VDate 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("v_date");
		    }
            set 
		    {
			    SetColumnValue("v_date", value);
            }
        }
	      
        [XmlAttribute("UId")]
        [Bindable(true)]
        public int UId 
	    {
		    get
		    {
			    return GetColumnValue<int>("u_id");
		    }
            set 
		    {
			    SetColumnValue("u_id", value);
            }
        }
	      
        [XmlAttribute("UName")]
        [Bindable(true)]
        public string UName 
	    {
		    get
		    {
			    return GetColumnValue<string>("u_name");
		    }
            set 
		    {
			    SetColumnValue("u_name", value);
            }
        }
	      
        [XmlAttribute("StoreName")]
        [Bindable(true)]
        public string StoreName 
	    {
		    get
		    {
			    return GetColumnValue<string>("store_name");
		    }
            set 
		    {
			    SetColumnValue("store_name", value);
            }
        }
	      
        [XmlAttribute("CreateId")]
        [Bindable(true)]
        public string CreateId 
	    {
		    get
		    {
			    return GetColumnValue<string>("create_id");
		    }
            set 
		    {
			    SetColumnValue("create_id", value);
            }
        }
	      
        [XmlAttribute("Type")]
        [Bindable(true)]
        public int Type 
	    {
		    get
		    {
			    return GetColumnValue<int>("type");
		    }
            set 
		    {
			    SetColumnValue("type", value);
            }
        }
	      
        [XmlAttribute("Bid")]
        [Bindable(true)]
        public Guid Bid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("BID");
		    }
            set 
		    {
			    SetColumnValue("BID", value);
            }
        }
	      
        [XmlAttribute("GrossMargin")]
        [Bindable(true)]
        public decimal? GrossMargin 
	    {
		    get
		    {
			    return GetColumnValue<decimal?>("gross_margin");
		    }
            set 
		    {
			    SetColumnValue("gross_margin", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string CId = @"c_id";
            
            public static string ItemPrice = @"item_price";
            
            public static string Cost = @"cost";
            
            public static string VDate = @"v_date";
            
            public static string UId = @"u_id";
            
            public static string UName = @"u_name";
            
            public static string StoreName = @"store_name";
            
            public static string CreateId = @"create_id";
            
            public static string Type = @"type";
            
            public static string Bid = @"BID";
            
            public static string GrossMargin = @"gross_margin";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

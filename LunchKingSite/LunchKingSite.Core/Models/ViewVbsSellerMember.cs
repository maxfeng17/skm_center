using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewVbsSellerMember class.
    /// </summary>
    [Serializable]
    public partial class ViewVbsSellerMemberCollection : ReadOnlyList<ViewVbsSellerMember, ViewVbsSellerMemberCollection>
    {        
        public ViewVbsSellerMemberCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_vbs_seller_member view.
    /// </summary>
    [Serializable]
    public partial class ViewVbsSellerMember : ReadOnlyRecord<ViewVbsSellerMember>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_vbs_seller_member", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarUserId = new TableSchema.TableColumn(schema);
                colvarUserId.ColumnName = "user_id";
                colvarUserId.DataType = DbType.Int32;
                colvarUserId.MaxLength = 0;
                colvarUserId.AutoIncrement = false;
                colvarUserId.IsNullable = false;
                colvarUserId.IsPrimaryKey = false;
                colvarUserId.IsForeignKey = false;
                colvarUserId.IsReadOnly = false;
                
                schema.Columns.Add(colvarUserId);
                
                TableSchema.TableColumn colvarUserMembershipCardId = new TableSchema.TableColumn(schema);
                colvarUserMembershipCardId.ColumnName = "user_membership_card_id";
                colvarUserMembershipCardId.DataType = DbType.Int32;
                colvarUserMembershipCardId.MaxLength = 0;
                colvarUserMembershipCardId.AutoIncrement = false;
                colvarUserMembershipCardId.IsNullable = false;
                colvarUserMembershipCardId.IsPrimaryKey = false;
                colvarUserMembershipCardId.IsForeignKey = false;
                colvarUserMembershipCardId.IsReadOnly = false;
                
                schema.Columns.Add(colvarUserMembershipCardId);
                
                TableSchema.TableColumn colvarLevel = new TableSchema.TableColumn(schema);
                colvarLevel.ColumnName = "level";
                colvarLevel.DataType = DbType.Int32;
                colvarLevel.MaxLength = 0;
                colvarLevel.AutoIncrement = false;
                colvarLevel.IsNullable = false;
                colvarLevel.IsPrimaryKey = false;
                colvarLevel.IsForeignKey = false;
                colvarLevel.IsReadOnly = false;
                
                schema.Columns.Add(colvarLevel);
                
                TableSchema.TableColumn colvarOrderCount = new TableSchema.TableColumn(schema);
                colvarOrderCount.ColumnName = "order_count";
                colvarOrderCount.DataType = DbType.Int32;
                colvarOrderCount.MaxLength = 0;
                colvarOrderCount.AutoIncrement = false;
                colvarOrderCount.IsNullable = false;
                colvarOrderCount.IsPrimaryKey = false;
                colvarOrderCount.IsForeignKey = false;
                colvarOrderCount.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderCount);
                
                TableSchema.TableColumn colvarAmountTotal = new TableSchema.TableColumn(schema);
                colvarAmountTotal.ColumnName = "amount_total";
                colvarAmountTotal.DataType = DbType.Currency;
                colvarAmountTotal.MaxLength = 0;
                colvarAmountTotal.AutoIncrement = false;
                colvarAmountTotal.IsNullable = false;
                colvarAmountTotal.IsPrimaryKey = false;
                colvarAmountTotal.IsForeignKey = false;
                colvarAmountTotal.IsReadOnly = false;
                
                schema.Columns.Add(colvarAmountTotal);
                
                TableSchema.TableColumn colvarPaymentPercent = new TableSchema.TableColumn(schema);
                colvarPaymentPercent.ColumnName = "payment_percent";
                colvarPaymentPercent.DataType = DbType.Double;
                colvarPaymentPercent.MaxLength = 0;
                colvarPaymentPercent.AutoIncrement = false;
                colvarPaymentPercent.IsNullable = false;
                colvarPaymentPercent.IsPrimaryKey = false;
                colvarPaymentPercent.IsForeignKey = false;
                colvarPaymentPercent.IsReadOnly = false;
                
                schema.Columns.Add(colvarPaymentPercent);
                
                TableSchema.TableColumn colvarInstruction = new TableSchema.TableColumn(schema);
                colvarInstruction.ColumnName = "instruction";
                colvarInstruction.DataType = DbType.String;
                colvarInstruction.MaxLength = -1;
                colvarInstruction.AutoIncrement = false;
                colvarInstruction.IsNullable = false;
                colvarInstruction.IsPrimaryKey = false;
                colvarInstruction.IsForeignKey = false;
                colvarInstruction.IsReadOnly = false;
                
                schema.Columns.Add(colvarInstruction);
                
                TableSchema.TableColumn colvarOtherPremiums = new TableSchema.TableColumn(schema);
                colvarOtherPremiums.ColumnName = "other_premiums";
                colvarOtherPremiums.DataType = DbType.String;
                colvarOtherPremiums.MaxLength = -1;
                colvarOtherPremiums.AutoIncrement = false;
                colvarOtherPremiums.IsNullable = true;
                colvarOtherPremiums.IsPrimaryKey = false;
                colvarOtherPremiums.IsForeignKey = false;
                colvarOtherPremiums.IsReadOnly = false;
                
                schema.Columns.Add(colvarOtherPremiums);
                
                TableSchema.TableColumn colvarSellerMemberId = new TableSchema.TableColumn(schema);
                colvarSellerMemberId.ColumnName = "seller_member_id";
                colvarSellerMemberId.DataType = DbType.Int32;
                colvarSellerMemberId.MaxLength = 0;
                colvarSellerMemberId.AutoIncrement = false;
                colvarSellerMemberId.IsNullable = false;
                colvarSellerMemberId.IsPrimaryKey = false;
                colvarSellerMemberId.IsForeignKey = false;
                colvarSellerMemberId.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerMemberId);
                
                TableSchema.TableColumn colvarSellerMemberFirstName = new TableSchema.TableColumn(schema);
                colvarSellerMemberFirstName.ColumnName = "seller_member_first_name";
                colvarSellerMemberFirstName.DataType = DbType.String;
                colvarSellerMemberFirstName.MaxLength = 50;
                colvarSellerMemberFirstName.AutoIncrement = false;
                colvarSellerMemberFirstName.IsNullable = true;
                colvarSellerMemberFirstName.IsPrimaryKey = false;
                colvarSellerMemberFirstName.IsForeignKey = false;
                colvarSellerMemberFirstName.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerMemberFirstName);
                
                TableSchema.TableColumn colvarSellerMemberLastName = new TableSchema.TableColumn(schema);
                colvarSellerMemberLastName.ColumnName = "seller_member_last_name";
                colvarSellerMemberLastName.DataType = DbType.String;
                colvarSellerMemberLastName.MaxLength = 50;
                colvarSellerMemberLastName.AutoIncrement = false;
                colvarSellerMemberLastName.IsNullable = true;
                colvarSellerMemberLastName.IsPrimaryKey = false;
                colvarSellerMemberLastName.IsForeignKey = false;
                colvarSellerMemberLastName.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerMemberLastName);
                
                TableSchema.TableColumn colvarSellerMemberMobile = new TableSchema.TableColumn(schema);
                colvarSellerMemberMobile.ColumnName = "seller_member_mobile";
                colvarSellerMemberMobile.DataType = DbType.AnsiString;
                colvarSellerMemberMobile.MaxLength = 50;
                colvarSellerMemberMobile.AutoIncrement = false;
                colvarSellerMemberMobile.IsNullable = true;
                colvarSellerMemberMobile.IsPrimaryKey = false;
                colvarSellerMemberMobile.IsForeignKey = false;
                colvarSellerMemberMobile.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerMemberMobile);
                
                TableSchema.TableColumn colvarSellerMemberGender = new TableSchema.TableColumn(schema);
                colvarSellerMemberGender.ColumnName = "seller_member_gender";
                colvarSellerMemberGender.DataType = DbType.Int32;
                colvarSellerMemberGender.MaxLength = 0;
                colvarSellerMemberGender.AutoIncrement = false;
                colvarSellerMemberGender.IsNullable = true;
                colvarSellerMemberGender.IsPrimaryKey = false;
                colvarSellerMemberGender.IsForeignKey = false;
                colvarSellerMemberGender.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerMemberGender);
                
                TableSchema.TableColumn colvarSellerMemberBirthday = new TableSchema.TableColumn(schema);
                colvarSellerMemberBirthday.ColumnName = "seller_member_birthday";
                colvarSellerMemberBirthday.DataType = DbType.DateTime;
                colvarSellerMemberBirthday.MaxLength = 0;
                colvarSellerMemberBirthday.AutoIncrement = false;
                colvarSellerMemberBirthday.IsNullable = true;
                colvarSellerMemberBirthday.IsPrimaryKey = false;
                colvarSellerMemberBirthday.IsForeignKey = false;
                colvarSellerMemberBirthday.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerMemberBirthday);
                
                TableSchema.TableColumn colvarRequestTime = new TableSchema.TableColumn(schema);
                colvarRequestTime.ColumnName = "request_time";
                colvarRequestTime.DataType = DbType.DateTime;
                colvarRequestTime.MaxLength = 0;
                colvarRequestTime.AutoIncrement = false;
                colvarRequestTime.IsNullable = false;
                colvarRequestTime.IsPrimaryKey = false;
                colvarRequestTime.IsForeignKey = false;
                colvarRequestTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarRequestTime);
                
                TableSchema.TableColumn colvarRemarks = new TableSchema.TableColumn(schema);
                colvarRemarks.ColumnName = "remarks";
                colvarRemarks.DataType = DbType.String;
                colvarRemarks.MaxLength = 256;
                colvarRemarks.AutoIncrement = false;
                colvarRemarks.IsNullable = true;
                colvarRemarks.IsPrimaryKey = false;
                colvarRemarks.IsForeignKey = false;
                colvarRemarks.IsReadOnly = false;
                
                schema.Columns.Add(colvarRemarks);
                
                TableSchema.TableColumn colvarSellerUserId = new TableSchema.TableColumn(schema);
                colvarSellerUserId.ColumnName = "seller_user_id";
                colvarSellerUserId.DataType = DbType.Int32;
                colvarSellerUserId.MaxLength = 0;
                colvarSellerUserId.AutoIncrement = false;
                colvarSellerUserId.IsNullable = false;
                colvarSellerUserId.IsPrimaryKey = false;
                colvarSellerUserId.IsForeignKey = false;
                colvarSellerUserId.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerUserId);
                
                TableSchema.TableColumn colvarAvailableDateType = new TableSchema.TableColumn(schema);
                colvarAvailableDateType.ColumnName = "available_date_type";
                colvarAvailableDateType.DataType = DbType.Int32;
                colvarAvailableDateType.MaxLength = 0;
                colvarAvailableDateType.AutoIncrement = false;
                colvarAvailableDateType.IsNullable = false;
                colvarAvailableDateType.IsPrimaryKey = false;
                colvarAvailableDateType.IsForeignKey = false;
                colvarAvailableDateType.IsReadOnly = false;
                
                schema.Columns.Add(colvarAvailableDateType);
                
                TableSchema.TableColumn colvarLastUseTime = new TableSchema.TableColumn(schema);
                colvarLastUseTime.ColumnName = "last_use_time";
                colvarLastUseTime.DataType = DbType.DateTime;
                colvarLastUseTime.MaxLength = 0;
                colvarLastUseTime.AutoIncrement = false;
                colvarLastUseTime.IsNullable = true;
                colvarLastUseTime.IsPrimaryKey = false;
                colvarLastUseTime.IsForeignKey = false;
                colvarLastUseTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarLastUseTime);
                
                TableSchema.TableColumn colvarCardNo = new TableSchema.TableColumn(schema);
                colvarCardNo.ColumnName = "card_no";
                colvarCardNo.DataType = DbType.AnsiString;
                colvarCardNo.MaxLength = 15;
                colvarCardNo.AutoIncrement = false;
                colvarCardNo.IsNullable = false;
                colvarCardNo.IsPrimaryKey = false;
                colvarCardNo.IsForeignKey = false;
                colvarCardNo.IsReadOnly = false;
                
                schema.Columns.Add(colvarCardNo);
                
                TableSchema.TableColumn colvarUserEmail = new TableSchema.TableColumn(schema);
                colvarUserEmail.ColumnName = "user_email";
                colvarUserEmail.DataType = DbType.String;
                colvarUserEmail.MaxLength = 256;
                colvarUserEmail.AutoIncrement = false;
                colvarUserEmail.IsNullable = false;
                colvarUserEmail.IsPrimaryKey = false;
                colvarUserEmail.IsForeignKey = false;
                colvarUserEmail.IsReadOnly = false;
                
                schema.Columns.Add(colvarUserEmail);
                
                TableSchema.TableColumn colvarCardGroupId = new TableSchema.TableColumn(schema);
                colvarCardGroupId.ColumnName = "card_group_id";
                colvarCardGroupId.DataType = DbType.Int32;
                colvarCardGroupId.MaxLength = 0;
                colvarCardGroupId.AutoIncrement = false;
                colvarCardGroupId.IsNullable = false;
                colvarCardGroupId.IsPrimaryKey = false;
                colvarCardGroupId.IsForeignKey = false;
                colvarCardGroupId.IsReadOnly = false;
                
                schema.Columns.Add(colvarCardGroupId);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_vbs_seller_member",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewVbsSellerMember()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewVbsSellerMember(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewVbsSellerMember(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewVbsSellerMember(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("UserId")]
        [Bindable(true)]
        public int UserId 
	    {
		    get
		    {
			    return GetColumnValue<int>("user_id");
		    }
            set 
		    {
			    SetColumnValue("user_id", value);
            }
        }
	      
        [XmlAttribute("UserMembershipCardId")]
        [Bindable(true)]
        public int UserMembershipCardId 
	    {
		    get
		    {
			    return GetColumnValue<int>("user_membership_card_id");
		    }
            set 
		    {
			    SetColumnValue("user_membership_card_id", value);
            }
        }
	      
        [XmlAttribute("Level")]
        [Bindable(true)]
        public int Level 
	    {
		    get
		    {
			    return GetColumnValue<int>("level");
		    }
            set 
		    {
			    SetColumnValue("level", value);
            }
        }
	      
        [XmlAttribute("OrderCount")]
        [Bindable(true)]
        public int OrderCount 
	    {
		    get
		    {
			    return GetColumnValue<int>("order_count");
		    }
            set 
		    {
			    SetColumnValue("order_count", value);
            }
        }
	      
        [XmlAttribute("AmountTotal")]
        [Bindable(true)]
        public decimal AmountTotal 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("amount_total");
		    }
            set 
		    {
			    SetColumnValue("amount_total", value);
            }
        }
	      
        [XmlAttribute("PaymentPercent")]
        [Bindable(true)]
        public double PaymentPercent 
	    {
		    get
		    {
			    return GetColumnValue<double>("payment_percent");
		    }
            set 
		    {
			    SetColumnValue("payment_percent", value);
            }
        }
	      
        [XmlAttribute("Instruction")]
        [Bindable(true)]
        public string Instruction 
	    {
		    get
		    {
			    return GetColumnValue<string>("instruction");
		    }
            set 
		    {
			    SetColumnValue("instruction", value);
            }
        }
	      
        [XmlAttribute("OtherPremiums")]
        [Bindable(true)]
        public string OtherPremiums 
	    {
		    get
		    {
			    return GetColumnValue<string>("other_premiums");
		    }
            set 
		    {
			    SetColumnValue("other_premiums", value);
            }
        }
	      
        [XmlAttribute("SellerMemberId")]
        [Bindable(true)]
        public int SellerMemberId 
	    {
		    get
		    {
			    return GetColumnValue<int>("seller_member_id");
		    }
            set 
		    {
			    SetColumnValue("seller_member_id", value);
            }
        }
	      
        [XmlAttribute("SellerMemberFirstName")]
        [Bindable(true)]
        public string SellerMemberFirstName 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_member_first_name");
		    }
            set 
		    {
			    SetColumnValue("seller_member_first_name", value);
            }
        }
	      
        [XmlAttribute("SellerMemberLastName")]
        [Bindable(true)]
        public string SellerMemberLastName 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_member_last_name");
		    }
            set 
		    {
			    SetColumnValue("seller_member_last_name", value);
            }
        }
	      
        [XmlAttribute("SellerMemberMobile")]
        [Bindable(true)]
        public string SellerMemberMobile 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_member_mobile");
		    }
            set 
		    {
			    SetColumnValue("seller_member_mobile", value);
            }
        }
	      
        [XmlAttribute("SellerMemberGender")]
        [Bindable(true)]
        public int? SellerMemberGender 
	    {
		    get
		    {
			    return GetColumnValue<int?>("seller_member_gender");
		    }
            set 
		    {
			    SetColumnValue("seller_member_gender", value);
            }
        }
	      
        [XmlAttribute("SellerMemberBirthday")]
        [Bindable(true)]
        public DateTime? SellerMemberBirthday 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("seller_member_birthday");
		    }
            set 
		    {
			    SetColumnValue("seller_member_birthday", value);
            }
        }
	      
        [XmlAttribute("RequestTime")]
        [Bindable(true)]
        public DateTime RequestTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("request_time");
		    }
            set 
		    {
			    SetColumnValue("request_time", value);
            }
        }
	      
        [XmlAttribute("Remarks")]
        [Bindable(true)]
        public string Remarks 
	    {
		    get
		    {
			    return GetColumnValue<string>("remarks");
		    }
            set 
		    {
			    SetColumnValue("remarks", value);
            }
        }
	      
        [XmlAttribute("SellerUserId")]
        [Bindable(true)]
        public int SellerUserId 
	    {
		    get
		    {
			    return GetColumnValue<int>("seller_user_id");
		    }
            set 
		    {
			    SetColumnValue("seller_user_id", value);
            }
        }
	      
        [XmlAttribute("AvailableDateType")]
        [Bindable(true)]
        public int AvailableDateType 
	    {
		    get
		    {
			    return GetColumnValue<int>("available_date_type");
		    }
            set 
		    {
			    SetColumnValue("available_date_type", value);
            }
        }
	      
        [XmlAttribute("LastUseTime")]
        [Bindable(true)]
        public DateTime? LastUseTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("last_use_time");
		    }
            set 
		    {
			    SetColumnValue("last_use_time", value);
            }
        }
	      
        [XmlAttribute("CardNo")]
        [Bindable(true)]
        public string CardNo 
	    {
		    get
		    {
			    return GetColumnValue<string>("card_no");
		    }
            set 
		    {
			    SetColumnValue("card_no", value);
            }
        }
	      
        [XmlAttribute("UserEmail")]
        [Bindable(true)]
        public string UserEmail 
	    {
		    get
		    {
			    return GetColumnValue<string>("user_email");
		    }
            set 
		    {
			    SetColumnValue("user_email", value);
            }
        }
	      
        [XmlAttribute("CardGroupId")]
        [Bindable(true)]
        public int CardGroupId 
	    {
		    get
		    {
			    return GetColumnValue<int>("card_group_id");
		    }
            set 
		    {
			    SetColumnValue("card_group_id", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string UserId = @"user_id";
            
            public static string UserMembershipCardId = @"user_membership_card_id";
            
            public static string Level = @"level";
            
            public static string OrderCount = @"order_count";
            
            public static string AmountTotal = @"amount_total";
            
            public static string PaymentPercent = @"payment_percent";
            
            public static string Instruction = @"instruction";
            
            public static string OtherPremiums = @"other_premiums";
            
            public static string SellerMemberId = @"seller_member_id";
            
            public static string SellerMemberFirstName = @"seller_member_first_name";
            
            public static string SellerMemberLastName = @"seller_member_last_name";
            
            public static string SellerMemberMobile = @"seller_member_mobile";
            
            public static string SellerMemberGender = @"seller_member_gender";
            
            public static string SellerMemberBirthday = @"seller_member_birthday";
            
            public static string RequestTime = @"request_time";
            
            public static string Remarks = @"remarks";
            
            public static string SellerUserId = @"seller_user_id";
            
            public static string AvailableDateType = @"available_date_type";
            
            public static string LastUseTime = @"last_use_time";
            
            public static string CardNo = @"card_no";
            
            public static string UserEmail = @"user_email";
            
            public static string CardGroupId = @"card_group_id";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

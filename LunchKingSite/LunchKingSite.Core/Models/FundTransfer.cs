using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the FundTransfer class.
	/// </summary>
    [Serializable]
	public partial class FundTransferCollection : RepositoryList<FundTransfer, FundTransferCollection>
	{	   
		public FundTransferCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>FundTransferCollection</returns>
		public FundTransferCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                FundTransfer o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the fund_transfer table.
	/// </summary>
	[Serializable]
	public partial class FundTransfer : RepositoryRecord<FundTransfer>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public FundTransfer()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public FundTransfer(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("fund_transfer", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarFundTransferType = new TableSchema.TableColumn(schema);
				colvarFundTransferType.ColumnName = "fund_transfer_type";
				colvarFundTransferType.DataType = DbType.Int32;
				colvarFundTransferType.MaxLength = 0;
				colvarFundTransferType.AutoIncrement = false;
				colvarFundTransferType.IsNullable = false;
				colvarFundTransferType.IsPrimaryKey = false;
				colvarFundTransferType.IsForeignKey = false;
				colvarFundTransferType.IsReadOnly = false;
				colvarFundTransferType.DefaultSetting = @"";
				colvarFundTransferType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFundTransferType);
				
				TableSchema.TableColumn colvarDealUid = new TableSchema.TableColumn(schema);
				colvarDealUid.ColumnName = "deal_uid";
				colvarDealUid.DataType = DbType.Int32;
				colvarDealUid.MaxLength = 0;
				colvarDealUid.AutoIncrement = false;
				colvarDealUid.IsNullable = false;
				colvarDealUid.IsPrimaryKey = false;
				colvarDealUid.IsForeignKey = false;
				colvarDealUid.IsReadOnly = false;
				colvarDealUid.DefaultSetting = @"";
				colvarDealUid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDealUid);
				
				TableSchema.TableColumn colvarBizModel = new TableSchema.TableColumn(schema);
				colvarBizModel.ColumnName = "biz_model";
				colvarBizModel.DataType = DbType.Int32;
				colvarBizModel.MaxLength = 0;
				colvarBizModel.AutoIncrement = false;
				colvarBizModel.IsNullable = false;
				colvarBizModel.IsPrimaryKey = false;
				colvarBizModel.IsForeignKey = false;
				colvarBizModel.IsReadOnly = false;
				colvarBizModel.DefaultSetting = @"";
				colvarBizModel.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBizModel);
				
				TableSchema.TableColumn colvarIntervalStart = new TableSchema.TableColumn(schema);
				colvarIntervalStart.ColumnName = "interval_start";
				colvarIntervalStart.DataType = DbType.DateTime;
				colvarIntervalStart.MaxLength = 0;
				colvarIntervalStart.AutoIncrement = false;
				colvarIntervalStart.IsNullable = false;
				colvarIntervalStart.IsPrimaryKey = false;
				colvarIntervalStart.IsForeignKey = false;
				colvarIntervalStart.IsReadOnly = false;
				colvarIntervalStart.DefaultSetting = @"";
				colvarIntervalStart.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIntervalStart);
				
				TableSchema.TableColumn colvarIntervalEnd = new TableSchema.TableColumn(schema);
				colvarIntervalEnd.ColumnName = "interval_end";
				colvarIntervalEnd.DataType = DbType.DateTime;
				colvarIntervalEnd.MaxLength = 0;
				colvarIntervalEnd.AutoIncrement = false;
				colvarIntervalEnd.IsNullable = false;
				colvarIntervalEnd.IsPrimaryKey = false;
				colvarIntervalEnd.IsForeignKey = false;
				colvarIntervalEnd.IsReadOnly = false;
				colvarIntervalEnd.DefaultSetting = @"";
				colvarIntervalEnd.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIntervalEnd);
				
				TableSchema.TableColumn colvarMemo = new TableSchema.TableColumn(schema);
				colvarMemo.ColumnName = "memo";
				colvarMemo.DataType = DbType.String;
				colvarMemo.MaxLength = 200;
				colvarMemo.AutoIncrement = false;
				colvarMemo.IsNullable = true;
				colvarMemo.IsPrimaryKey = false;
				colvarMemo.IsForeignKey = false;
				colvarMemo.IsReadOnly = false;
				colvarMemo.DefaultSetting = @"";
				colvarMemo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMemo);
				
				TableSchema.TableColumn colvarAchResult = new TableSchema.TableColumn(schema);
				colvarAchResult.ColumnName = "ach_result";
				colvarAchResult.DataType = DbType.String;
				colvarAchResult.MaxLength = 10;
				colvarAchResult.AutoIncrement = false;
				colvarAchResult.IsNullable = true;
				colvarAchResult.IsPrimaryKey = false;
				colvarAchResult.IsForeignKey = false;
				colvarAchResult.IsReadOnly = false;
				colvarAchResult.DefaultSetting = @"";
				colvarAchResult.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAchResult);
				
				TableSchema.TableColumn colvarTransferTime = new TableSchema.TableColumn(schema);
				colvarTransferTime.ColumnName = "transfer_time";
				colvarTransferTime.DataType = DbType.DateTime;
				colvarTransferTime.MaxLength = 0;
				colvarTransferTime.AutoIncrement = false;
				colvarTransferTime.IsNullable = true;
				colvarTransferTime.IsPrimaryKey = false;
				colvarTransferTime.IsForeignKey = false;
				colvarTransferTime.IsReadOnly = false;
				colvarTransferTime.DefaultSetting = @"";
				colvarTransferTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTransferTime);
				
				TableSchema.TableColumn colvarTransferAmount = new TableSchema.TableColumn(schema);
				colvarTransferAmount.ColumnName = "transfer_amount";
				colvarTransferAmount.DataType = DbType.Int32;
				colvarTransferAmount.MaxLength = 0;
				colvarTransferAmount.AutoIncrement = false;
				colvarTransferAmount.IsNullable = true;
				colvarTransferAmount.IsPrimaryKey = false;
				colvarTransferAmount.IsForeignKey = false;
				colvarTransferAmount.IsReadOnly = false;
				colvarTransferAmount.DefaultSetting = @"";
				colvarTransferAmount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTransferAmount);
				
				TableSchema.TableColumn colvarIsTransferComplete = new TableSchema.TableColumn(schema);
				colvarIsTransferComplete.ColumnName = "is_transfer_complete";
				colvarIsTransferComplete.DataType = DbType.Boolean;
				colvarIsTransferComplete.MaxLength = 0;
				colvarIsTransferComplete.AutoIncrement = false;
				colvarIsTransferComplete.IsNullable = false;
				colvarIsTransferComplete.IsPrimaryKey = false;
				colvarIsTransferComplete.IsForeignKey = false;
				colvarIsTransferComplete.IsReadOnly = false;
				colvarIsTransferComplete.DefaultSetting = @"";
				colvarIsTransferComplete.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsTransferComplete);
				
				TableSchema.TableColumn colvarUntransferredId = new TableSchema.TableColumn(schema);
				colvarUntransferredId.ColumnName = "untransferred_id";
				colvarUntransferredId.DataType = DbType.Int32;
				colvarUntransferredId.MaxLength = 0;
				colvarUntransferredId.AutoIncrement = false;
				colvarUntransferredId.IsNullable = true;
				colvarUntransferredId.IsPrimaryKey = false;
				colvarUntransferredId.IsForeignKey = false;
				colvarUntransferredId.IsReadOnly = false;
				colvarUntransferredId.DefaultSetting = @"";
				colvarUntransferredId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUntransferredId);
				
				TableSchema.TableColumn colvarBankAccountId = new TableSchema.TableColumn(schema);
				colvarBankAccountId.ColumnName = "bank_account_id";
				colvarBankAccountId.DataType = DbType.Int32;
				colvarBankAccountId.MaxLength = 0;
				colvarBankAccountId.AutoIncrement = false;
				colvarBankAccountId.IsNullable = true;
				colvarBankAccountId.IsPrimaryKey = false;
				colvarBankAccountId.IsForeignKey = true;
				colvarBankAccountId.IsReadOnly = false;
				colvarBankAccountId.DefaultSetting = @"";
				
					colvarBankAccountId.ForeignKeyTableName = "bank_account";
				schema.Columns.Add(colvarBankAccountId);
				
				TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
				colvarCreateId.ColumnName = "create_id";
				colvarCreateId.DataType = DbType.Int32;
				colvarCreateId.MaxLength = 0;
				colvarCreateId.AutoIncrement = false;
				colvarCreateId.IsNullable = false;
				colvarCreateId.IsPrimaryKey = false;
				colvarCreateId.IsForeignKey = false;
				colvarCreateId.IsReadOnly = false;
				colvarCreateId.DefaultSetting = @"";
				colvarCreateId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateId);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarModifyId = new TableSchema.TableColumn(schema);
				colvarModifyId.ColumnName = "modify_id";
				colvarModifyId.DataType = DbType.Int32;
				colvarModifyId.MaxLength = 0;
				colvarModifyId.AutoIncrement = false;
				colvarModifyId.IsNullable = true;
				colvarModifyId.IsPrimaryKey = false;
				colvarModifyId.IsForeignKey = false;
				colvarModifyId.IsReadOnly = false;
				colvarModifyId.DefaultSetting = @"";
				colvarModifyId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyId);
				
				TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
				colvarModifyTime.ColumnName = "modify_time";
				colvarModifyTime.DataType = DbType.DateTime;
				colvarModifyTime.MaxLength = 0;
				colvarModifyTime.AutoIncrement = false;
				colvarModifyTime.IsNullable = true;
				colvarModifyTime.IsPrimaryKey = false;
				colvarModifyTime.IsForeignKey = false;
				colvarModifyTime.IsReadOnly = false;
				colvarModifyTime.DefaultSetting = @"";
				colvarModifyTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyTime);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("fund_transfer",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("FundTransferType")]
		[Bindable(true)]
		public int FundTransferType 
		{
			get { return GetColumnValue<int>(Columns.FundTransferType); }
			set { SetColumnValue(Columns.FundTransferType, value); }
		}
		  
		[XmlAttribute("DealUid")]
		[Bindable(true)]
		public int DealUid 
		{
			get { return GetColumnValue<int>(Columns.DealUid); }
			set { SetColumnValue(Columns.DealUid, value); }
		}
		  
		[XmlAttribute("BizModel")]
		[Bindable(true)]
		public int BizModel 
		{
			get { return GetColumnValue<int>(Columns.BizModel); }
			set { SetColumnValue(Columns.BizModel, value); }
		}
		  
		[XmlAttribute("IntervalStart")]
		[Bindable(true)]
		public DateTime IntervalStart 
		{
			get { return GetColumnValue<DateTime>(Columns.IntervalStart); }
			set { SetColumnValue(Columns.IntervalStart, value); }
		}
		  
		[XmlAttribute("IntervalEnd")]
		[Bindable(true)]
		public DateTime IntervalEnd 
		{
			get { return GetColumnValue<DateTime>(Columns.IntervalEnd); }
			set { SetColumnValue(Columns.IntervalEnd, value); }
		}
		  
		[XmlAttribute("Memo")]
		[Bindable(true)]
		public string Memo 
		{
			get { return GetColumnValue<string>(Columns.Memo); }
			set { SetColumnValue(Columns.Memo, value); }
		}
		  
		[XmlAttribute("AchResult")]
		[Bindable(true)]
		public string AchResult 
		{
			get { return GetColumnValue<string>(Columns.AchResult); }
			set { SetColumnValue(Columns.AchResult, value); }
		}
		  
		[XmlAttribute("TransferTime")]
		[Bindable(true)]
		public DateTime? TransferTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.TransferTime); }
			set { SetColumnValue(Columns.TransferTime, value); }
		}
		  
		[XmlAttribute("TransferAmount")]
		[Bindable(true)]
		public int? TransferAmount 
		{
			get { return GetColumnValue<int?>(Columns.TransferAmount); }
			set { SetColumnValue(Columns.TransferAmount, value); }
		}
		  
		[XmlAttribute("IsTransferComplete")]
		[Bindable(true)]
		public bool IsTransferComplete 
		{
			get { return GetColumnValue<bool>(Columns.IsTransferComplete); }
			set { SetColumnValue(Columns.IsTransferComplete, value); }
		}
		  
		[XmlAttribute("UntransferredId")]
		[Bindable(true)]
		public int? UntransferredId 
		{
			get { return GetColumnValue<int?>(Columns.UntransferredId); }
			set { SetColumnValue(Columns.UntransferredId, value); }
		}
		  
		[XmlAttribute("BankAccountId")]
		[Bindable(true)]
		public int? BankAccountId 
		{
			get { return GetColumnValue<int?>(Columns.BankAccountId); }
			set { SetColumnValue(Columns.BankAccountId, value); }
		}
		  
		[XmlAttribute("CreateId")]
		[Bindable(true)]
		public int CreateId 
		{
			get { return GetColumnValue<int>(Columns.CreateId); }
			set { SetColumnValue(Columns.CreateId, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		  
		[XmlAttribute("ModifyId")]
		[Bindable(true)]
		public int? ModifyId 
		{
			get { return GetColumnValue<int?>(Columns.ModifyId); }
			set { SetColumnValue(Columns.ModifyId, value); }
		}
		  
		[XmlAttribute("ModifyTime")]
		[Bindable(true)]
		public DateTime? ModifyTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.ModifyTime); }
			set { SetColumnValue(Columns.ModifyTime, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (1)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn FundTransferTypeColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn DealUidColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn BizModelColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn IntervalStartColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn IntervalEndColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn MemoColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn AchResultColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn TransferTimeColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn TransferAmountColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn IsTransferCompleteColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        public static TableSchema.TableColumn UntransferredIdColumn
        {
            get { return Schema.Columns[11]; }
        }
        
        
        
        public static TableSchema.TableColumn BankAccountIdColumn
        {
            get { return Schema.Columns[12]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[13]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[14]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyIdColumn
        {
            get { return Schema.Columns[15]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[16]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string FundTransferType = @"fund_transfer_type";
			 public static string DealUid = @"deal_uid";
			 public static string BizModel = @"biz_model";
			 public static string IntervalStart = @"interval_start";
			 public static string IntervalEnd = @"interval_end";
			 public static string Memo = @"memo";
			 public static string AchResult = @"ach_result";
			 public static string TransferTime = @"transfer_time";
			 public static string TransferAmount = @"transfer_amount";
			 public static string IsTransferComplete = @"is_transfer_complete";
			 public static string UntransferredId = @"untransferred_id";
			 public static string BankAccountId = @"bank_account_id";
			 public static string CreateId = @"create_id";
			 public static string CreateTime = @"create_time";
			 public static string ModifyId = @"modify_id";
			 public static string ModifyTime = @"modify_time";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

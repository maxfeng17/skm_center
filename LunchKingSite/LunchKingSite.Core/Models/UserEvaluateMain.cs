﻿using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the UserEvaluateMain class.
    /// </summary>
    [Serializable]
    public partial class UserEvaluateMainCollection : RepositoryList<UserEvaluateMain, UserEvaluateMainCollection>
    {
        public UserEvaluateMainCollection() { }

        /// <summary>
        /// Filters an existing collection based on the set criteria. This is an in-memory filter
        /// Thanks to developingchris for this!
        /// </summary>
        /// <returns>UserEvaluateMainCollection</returns>
        public UserEvaluateMainCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                UserEvaluateMain o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }


    }
    /// <summary>
    /// This is an ActiveRecord class which wraps the user_evaluate_main table.
    /// </summary>
    [Serializable]
    public partial class UserEvaluateMain : RepositoryRecord<UserEvaluateMain>, IRecordBase
    {
        #region .ctors and Default Settings

        public UserEvaluateMain()
        {
            SetSQLProps();
            InitSetDefaults();
            MarkNew();
        }

        private void InitSetDefaults() { SetDefaults(); }

        public UserEvaluateMain(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
                ForceDefaults();
            MarkNew();
        }

        public UserEvaluateMain(object keyID)
        {
            SetSQLProps();
            InitSetDefaults();
        }

        public UserEvaluateMain(string columnName, object columnValue)
        {
            SetSQLProps();
            InitSetDefaults();
        }

        protected static void SetSQLProps() { GetTableSchema(); }

        #endregion

        #region Schema and Query Accessor
        public static Query CreateQuery() { return new Query(Schema); }
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                    SetSQLProps();
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("user_evaluate_main", TableType.Table, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = true;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = true;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                colvarId.DefaultSetting = @"";
                colvarId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarId);

                TableSchema.TableColumn colvarTrustId = new TableSchema.TableColumn(schema);
                colvarTrustId.ColumnName = "trust_id";
                colvarTrustId.DataType = DbType.Guid;
                colvarTrustId.MaxLength = 0;
                colvarTrustId.AutoIncrement = false;
                colvarTrustId.IsNullable = false;
                colvarTrustId.IsPrimaryKey = false;
                colvarTrustId.IsForeignKey = false;
                colvarTrustId.IsReadOnly = false;
                colvarTrustId.DefaultSetting = @"";
                colvarTrustId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarTrustId);

                TableSchema.TableColumn colvarSourceType = new TableSchema.TableColumn(schema);
                colvarSourceType.ColumnName = "source_type";
                colvarSourceType.DataType = DbType.Int32;
                colvarSourceType.MaxLength = 0;
                colvarSourceType.AutoIncrement = false;
                colvarSourceType.IsNullable = false;
                colvarSourceType.IsPrimaryKey = false;
                colvarSourceType.IsForeignKey = false;
                colvarSourceType.IsReadOnly = false;
                colvarSourceType.DefaultSetting = @"";
                colvarSourceType.ForeignKeyTableName = "";
                schema.Columns.Add(colvarSourceType);

                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = false;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                colvarCreateTime.DefaultSetting = @"";
                colvarCreateTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateTime);

                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("user_evaluate_main", schema);
            }
        }
        #endregion

        #region Props

        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id
        {
            get { return GetColumnValue<int>(Columns.Id); }
            set { SetColumnValue(Columns.Id, value); }
        }

        [XmlAttribute("TrustId")]
        [Bindable(true)]
        public Guid TrustId
        {
            get { return GetColumnValue<Guid>(Columns.TrustId); }
            set { SetColumnValue(Columns.TrustId, value); }
        }

        [XmlAttribute("SourceType")]
        [Bindable(true)]
        public int SourceType
        {
            get { return GetColumnValue<int>(Columns.SourceType); }
            set { SetColumnValue(Columns.SourceType, value); }
        }

        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime CreateTime
        {
            get { return GetColumnValue<DateTime>(Columns.CreateTime); }
            set { SetColumnValue(Columns.CreateTime, value); }
        }

        #endregion


        #region Typed Columns


        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }



        public static TableSchema.TableColumn TrustIdColumn
        {
            get { return Schema.Columns[1]; }
        }



        public static TableSchema.TableColumn SourceTypeColumn
        {
            get { return Schema.Columns[2]; }
        }



        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[3]; }
        }



        #endregion
        #region Columns Struct
        public struct Columns
        {
            public static string Id = @"id";
            public static string TrustId = @"trust_id";
            public static string SourceType = @"source_type";
            public static string CreateTime = @"create_time";

        }
        #endregion

        #region Update PK Collections

        #endregion

        #region Deep Save

        #endregion
    }
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the SkmLatestSellerDeal class.
	/// </summary>
    [Serializable]
	public partial class SkmLatestSellerDealCollection : RepositoryList<SkmLatestSellerDeal, SkmLatestSellerDealCollection>
	{	   
		public SkmLatestSellerDealCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>SkmLatestSellerDealCollection</returns>
		public SkmLatestSellerDealCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                SkmLatestSellerDeal o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the skm_latest_seller_deal table.
	/// </summary>
	[Serializable]
	public partial class SkmLatestSellerDeal : RepositoryRecord<SkmLatestSellerDeal>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public SkmLatestSellerDeal()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public SkmLatestSellerDeal(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("skm_latest_seller_deal", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarDealTitle = new TableSchema.TableColumn(schema);
				colvarDealTitle.ColumnName = "deal_title";
				colvarDealTitle.DataType = DbType.String;
				colvarDealTitle.MaxLength = 200;
				colvarDealTitle.AutoIncrement = false;
				colvarDealTitle.IsNullable = true;
				colvarDealTitle.IsPrimaryKey = false;
				colvarDealTitle.IsForeignKey = false;
				colvarDealTitle.IsReadOnly = false;
				colvarDealTitle.DefaultSetting = @"";
				colvarDealTitle.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDealTitle);
				
				TableSchema.TableColumn colvarLinkDealGuid = new TableSchema.TableColumn(schema);
				colvarLinkDealGuid.ColumnName = "link_deal_guid";
				colvarLinkDealGuid.DataType = DbType.Guid;
				colvarLinkDealGuid.MaxLength = 0;
				colvarLinkDealGuid.AutoIncrement = false;
				colvarLinkDealGuid.IsNullable = false;
				colvarLinkDealGuid.IsPrimaryKey = false;
				colvarLinkDealGuid.IsForeignKey = false;
				colvarLinkDealGuid.IsReadOnly = false;
				colvarLinkDealGuid.DefaultSetting = @"";
				colvarLinkDealGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarLinkDealGuid);
				
				TableSchema.TableColumn colvarImageUrl = new TableSchema.TableColumn(schema);
				colvarImageUrl.ColumnName = "image_url";
				colvarImageUrl.DataType = DbType.AnsiString;
				colvarImageUrl.MaxLength = -1;
				colvarImageUrl.AutoIncrement = false;
				colvarImageUrl.IsNullable = true;
				colvarImageUrl.IsPrimaryKey = false;
				colvarImageUrl.IsForeignKey = false;
				colvarImageUrl.IsReadOnly = false;
				colvarImageUrl.DefaultSetting = @"";
				colvarImageUrl.ForeignKeyTableName = "";
				schema.Columns.Add(colvarImageUrl);
				
				TableSchema.TableColumn colvarSDate = new TableSchema.TableColumn(schema);
				colvarSDate.ColumnName = "s_date";
				colvarSDate.DataType = DbType.DateTime;
				colvarSDate.MaxLength = 0;
				colvarSDate.AutoIncrement = false;
				colvarSDate.IsNullable = false;
				colvarSDate.IsPrimaryKey = false;
				colvarSDate.IsForeignKey = false;
				colvarSDate.IsReadOnly = false;
				colvarSDate.DefaultSetting = @"";
				colvarSDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSDate);
				
				TableSchema.TableColumn colvarEDate = new TableSchema.TableColumn(schema);
				colvarEDate.ColumnName = "e_date";
				colvarEDate.DataType = DbType.DateTime;
				colvarEDate.MaxLength = 0;
				colvarEDate.AutoIncrement = false;
				colvarEDate.IsNullable = false;
				colvarEDate.IsPrimaryKey = false;
				colvarEDate.IsForeignKey = false;
				colvarEDate.IsReadOnly = false;
				colvarEDate.DefaultSetting = @"";
				colvarEDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEDate);
				
				TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
				colvarStatus.ColumnName = "status";
				colvarStatus.DataType = DbType.Int32;
				colvarStatus.MaxLength = 0;
				colvarStatus.AutoIncrement = false;
				colvarStatus.IsNullable = true;
				colvarStatus.IsPrimaryKey = false;
				colvarStatus.IsForeignKey = false;
				colvarStatus.IsReadOnly = false;
				colvarStatus.DefaultSetting = @"";
				colvarStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStatus);
				
				TableSchema.TableColumn colvarDealDescription = new TableSchema.TableColumn(schema);
				colvarDealDescription.ColumnName = "deal_description";
				colvarDealDescription.DataType = DbType.String;
				colvarDealDescription.MaxLength = 200;
				colvarDealDescription.AutoIncrement = false;
				colvarDealDescription.IsNullable = true;
				colvarDealDescription.IsPrimaryKey = false;
				colvarDealDescription.IsForeignKey = false;
				colvarDealDescription.IsReadOnly = false;
				colvarDealDescription.DefaultSetting = @"";
				colvarDealDescription.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDealDescription);
				
				TableSchema.TableColumn colvarSellerGuid = new TableSchema.TableColumn(schema);
				colvarSellerGuid.ColumnName = "seller_guid";
				colvarSellerGuid.DataType = DbType.Guid;
				colvarSellerGuid.MaxLength = 0;
				colvarSellerGuid.AutoIncrement = false;
				colvarSellerGuid.IsNullable = true;
				colvarSellerGuid.IsPrimaryKey = false;
				colvarSellerGuid.IsForeignKey = false;
				colvarSellerGuid.IsReadOnly = false;
				colvarSellerGuid.DefaultSetting = @"";
				colvarSellerGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSellerGuid);
				
				TableSchema.TableColumn colvarCategoryId = new TableSchema.TableColumn(schema);
				colvarCategoryId.ColumnName = "category_id";
				colvarCategoryId.DataType = DbType.Int32;
				colvarCategoryId.MaxLength = 0;
				colvarCategoryId.AutoIncrement = false;
				colvarCategoryId.IsNullable = true;
				colvarCategoryId.IsPrimaryKey = false;
				colvarCategoryId.IsForeignKey = false;
				colvarCategoryId.IsReadOnly = false;
				colvarCategoryId.DefaultSetting = @"";
				colvarCategoryId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCategoryId);
				
				TableSchema.TableColumn colvarGuid = new TableSchema.TableColumn(schema);
				colvarGuid.ColumnName = "guid";
				colvarGuid.DataType = DbType.Guid;
				colvarGuid.MaxLength = 0;
				colvarGuid.AutoIncrement = false;
				colvarGuid.IsNullable = true;
				colvarGuid.IsPrimaryKey = false;
				colvarGuid.IsForeignKey = false;
				colvarGuid.IsReadOnly = false;
				colvarGuid.DefaultSetting = @"";
				colvarGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGuid);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("skm_latest_seller_deal",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("DealTitle")]
		[Bindable(true)]
		public string DealTitle 
		{
			get { return GetColumnValue<string>(Columns.DealTitle); }
			set { SetColumnValue(Columns.DealTitle, value); }
		}
		  
		[XmlAttribute("LinkDealGuid")]
		[Bindable(true)]
		public Guid LinkDealGuid 
		{
			get { return GetColumnValue<Guid>(Columns.LinkDealGuid); }
			set { SetColumnValue(Columns.LinkDealGuid, value); }
		}
		  
		[XmlAttribute("ImageUrl")]
		[Bindable(true)]
		public string ImageUrl 
		{
			get { return GetColumnValue<string>(Columns.ImageUrl); }
			set { SetColumnValue(Columns.ImageUrl, value); }
		}
		  
		[XmlAttribute("SDate")]
		[Bindable(true)]
		public DateTime SDate 
		{
			get { return GetColumnValue<DateTime>(Columns.SDate); }
			set { SetColumnValue(Columns.SDate, value); }
		}
		  
		[XmlAttribute("EDate")]
		[Bindable(true)]
		public DateTime EDate 
		{
			get { return GetColumnValue<DateTime>(Columns.EDate); }
			set { SetColumnValue(Columns.EDate, value); }
		}
		  
		[XmlAttribute("Status")]
		[Bindable(true)]
		public int? Status 
		{
			get { return GetColumnValue<int?>(Columns.Status); }
			set { SetColumnValue(Columns.Status, value); }
		}
		  
		[XmlAttribute("DealDescription")]
		[Bindable(true)]
		public string DealDescription 
		{
			get { return GetColumnValue<string>(Columns.DealDescription); }
			set { SetColumnValue(Columns.DealDescription, value); }
		}
		  
		[XmlAttribute("SellerGuid")]
		[Bindable(true)]
		public Guid? SellerGuid 
		{
			get { return GetColumnValue<Guid?>(Columns.SellerGuid); }
			set { SetColumnValue(Columns.SellerGuid, value); }
		}
		  
		[XmlAttribute("CategoryId")]
		[Bindable(true)]
		public int? CategoryId 
		{
			get { return GetColumnValue<int?>(Columns.CategoryId); }
			set { SetColumnValue(Columns.CategoryId, value); }
		}
		  
		[XmlAttribute("Guid")]
		[Bindable(true)]
		public Guid? Guid 
		{
			get { return GetColumnValue<Guid?>(Columns.Guid); }
			set { SetColumnValue(Columns.Guid, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn DealTitleColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn LinkDealGuidColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn ImageUrlColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn SDateColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn EDateColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn StatusColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn DealDescriptionColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn SellerGuidColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn CategoryIdColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn GuidColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string DealTitle = @"deal_title";
			 public static string LinkDealGuid = @"link_deal_guid";
			 public static string ImageUrl = @"image_url";
			 public static string SDate = @"s_date";
			 public static string EDate = @"e_date";
			 public static string Status = @"status";
			 public static string DealDescription = @"deal_description";
			 public static string SellerGuid = @"seller_guid";
			 public static string CategoryId = @"category_id";
			 public static string Guid = @"guid";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

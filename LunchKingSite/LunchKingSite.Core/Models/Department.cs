using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the Department class.
	/// </summary>
    [Serializable]
	public partial class DepartmentCollection : RepositoryList<Department, DepartmentCollection>
	{	   
		public DepartmentCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>DepartmentCollection</returns>
		public DepartmentCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                Department o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the department table.
	/// </summary>
	[Serializable]
	public partial class Department : RepositoryRecord<Department>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public Department()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public Department(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("department", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarDeptId = new TableSchema.TableColumn(schema);
				colvarDeptId.ColumnName = "dept_id";
				colvarDeptId.DataType = DbType.AnsiString;
				colvarDeptId.MaxLength = 10;
				colvarDeptId.AutoIncrement = false;
				colvarDeptId.IsNullable = false;
				colvarDeptId.IsPrimaryKey = true;
				colvarDeptId.IsForeignKey = false;
				colvarDeptId.IsReadOnly = false;
				colvarDeptId.DefaultSetting = @"";
				colvarDeptId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDeptId);
				
				TableSchema.TableColumn colvarDeptName = new TableSchema.TableColumn(schema);
				colvarDeptName.ColumnName = "dept_name";
				colvarDeptName.DataType = DbType.String;
				colvarDeptName.MaxLength = 100;
				colvarDeptName.AutoIncrement = false;
				colvarDeptName.IsNullable = true;
				colvarDeptName.IsPrimaryKey = false;
				colvarDeptName.IsForeignKey = false;
				colvarDeptName.IsReadOnly = false;
				colvarDeptName.DefaultSetting = @"";
				colvarDeptName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDeptName);
				
				TableSchema.TableColumn colvarEnabled = new TableSchema.TableColumn(schema);
				colvarEnabled.ColumnName = "enabled";
				colvarEnabled.DataType = DbType.Boolean;
				colvarEnabled.MaxLength = 0;
				colvarEnabled.AutoIncrement = false;
				colvarEnabled.IsNullable = true;
				colvarEnabled.IsPrimaryKey = false;
				colvarEnabled.IsForeignKey = false;
				colvarEnabled.IsReadOnly = false;
				colvarEnabled.DefaultSetting = @"";
				colvarEnabled.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEnabled);
				
				TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
				colvarCreateId.ColumnName = "create_id";
				colvarCreateId.DataType = DbType.String;
				colvarCreateId.MaxLength = 256;
				colvarCreateId.AutoIncrement = false;
				colvarCreateId.IsNullable = true;
				colvarCreateId.IsPrimaryKey = false;
				colvarCreateId.IsForeignKey = false;
				colvarCreateId.IsReadOnly = false;
				colvarCreateId.DefaultSetting = @"";
				colvarCreateId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateId);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = true;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarModifyId = new TableSchema.TableColumn(schema);
				colvarModifyId.ColumnName = "modify_id";
				colvarModifyId.DataType = DbType.String;
				colvarModifyId.MaxLength = 256;
				colvarModifyId.AutoIncrement = false;
				colvarModifyId.IsNullable = true;
				colvarModifyId.IsPrimaryKey = false;
				colvarModifyId.IsForeignKey = false;
				colvarModifyId.IsReadOnly = false;
				colvarModifyId.DefaultSetting = @"";
				colvarModifyId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyId);
				
				TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
				colvarModifyTime.ColumnName = "modify_time";
				colvarModifyTime.DataType = DbType.DateTime;
				colvarModifyTime.MaxLength = 0;
				colvarModifyTime.AutoIncrement = false;
				colvarModifyTime.IsNullable = true;
				colvarModifyTime.IsPrimaryKey = false;
				colvarModifyTime.IsForeignKey = false;
				colvarModifyTime.IsReadOnly = false;
				colvarModifyTime.DefaultSetting = @"";
				colvarModifyTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyTime);
				
				TableSchema.TableColumn colvarDeptAddress = new TableSchema.TableColumn(schema);
				colvarDeptAddress.ColumnName = "dept_address";
				colvarDeptAddress.DataType = DbType.String;
				colvarDeptAddress.MaxLength = 100;
				colvarDeptAddress.AutoIncrement = false;
				colvarDeptAddress.IsNullable = true;
				colvarDeptAddress.IsPrimaryKey = false;
				colvarDeptAddress.IsForeignKey = false;
				colvarDeptAddress.IsReadOnly = false;
				colvarDeptAddress.DefaultSetting = @"";
				colvarDeptAddress.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDeptAddress);
				
				TableSchema.TableColumn colvarDeptPhone = new TableSchema.TableColumn(schema);
				colvarDeptPhone.ColumnName = "dept_phone";
				colvarDeptPhone.DataType = DbType.AnsiString;
				colvarDeptPhone.MaxLength = 50;
				colvarDeptPhone.AutoIncrement = false;
				colvarDeptPhone.IsNullable = true;
				colvarDeptPhone.IsPrimaryKey = false;
				colvarDeptPhone.IsForeignKey = false;
				colvarDeptPhone.IsReadOnly = false;
				colvarDeptPhone.DefaultSetting = @"";
				colvarDeptPhone.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDeptPhone);
				
				TableSchema.TableColumn colvarParentDeptId = new TableSchema.TableColumn(schema);
				colvarParentDeptId.ColumnName = "parent_dept_id";
				colvarParentDeptId.DataType = DbType.String;
				colvarParentDeptId.MaxLength = 10;
				colvarParentDeptId.AutoIncrement = false;
				colvarParentDeptId.IsNullable = true;
				colvarParentDeptId.IsPrimaryKey = false;
				colvarParentDeptId.IsForeignKey = false;
				colvarParentDeptId.IsReadOnly = false;
				colvarParentDeptId.DefaultSetting = @"";
				colvarParentDeptId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarParentDeptId);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("department",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("DeptId")]
		[Bindable(true)]
		public string DeptId 
		{
			get { return GetColumnValue<string>(Columns.DeptId); }
			set { SetColumnValue(Columns.DeptId, value); }
		}
		  
		[XmlAttribute("DeptName")]
		[Bindable(true)]
		public string DeptName 
		{
			get { return GetColumnValue<string>(Columns.DeptName); }
			set { SetColumnValue(Columns.DeptName, value); }
		}
		  
		[XmlAttribute("Enabled")]
		[Bindable(true)]
		public bool? Enabled 
		{
			get { return GetColumnValue<bool?>(Columns.Enabled); }
			set { SetColumnValue(Columns.Enabled, value); }
		}
		  
		[XmlAttribute("CreateId")]
		[Bindable(true)]
		public string CreateId 
		{
			get { return GetColumnValue<string>(Columns.CreateId); }
			set { SetColumnValue(Columns.CreateId, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime? CreateTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		  
		[XmlAttribute("ModifyId")]
		[Bindable(true)]
		public string ModifyId 
		{
			get { return GetColumnValue<string>(Columns.ModifyId); }
			set { SetColumnValue(Columns.ModifyId, value); }
		}
		  
		[XmlAttribute("ModifyTime")]
		[Bindable(true)]
		public DateTime? ModifyTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.ModifyTime); }
			set { SetColumnValue(Columns.ModifyTime, value); }
		}
		  
		[XmlAttribute("DeptAddress")]
		[Bindable(true)]
		public string DeptAddress 
		{
			get { return GetColumnValue<string>(Columns.DeptAddress); }
			set { SetColumnValue(Columns.DeptAddress, value); }
		}
		  
		[XmlAttribute("DeptPhone")]
		[Bindable(true)]
		public string DeptPhone 
		{
			get { return GetColumnValue<string>(Columns.DeptPhone); }
			set { SetColumnValue(Columns.DeptPhone, value); }
		}
		  
		[XmlAttribute("ParentDeptId")]
		[Bindable(true)]
		public string ParentDeptId 
		{
			get { return GetColumnValue<string>(Columns.ParentDeptId); }
			set { SetColumnValue(Columns.ParentDeptId, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn DeptIdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn DeptNameColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn EnabledColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyIdColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn DeptAddressColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn DeptPhoneColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn ParentDeptIdColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string DeptId = @"dept_id";
			 public static string DeptName = @"dept_name";
			 public static string Enabled = @"enabled";
			 public static string CreateId = @"create_id";
			 public static string CreateTime = @"create_time";
			 public static string ModifyId = @"modify_id";
			 public static string ModifyTime = @"modify_time";
			 public static string DeptAddress = @"dept_address";
			 public static string DeptPhone = @"dept_phone";
			 public static string ParentDeptId = @"parent_dept_id";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

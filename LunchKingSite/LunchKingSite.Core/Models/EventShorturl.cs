using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the EventShorturl class.
	/// </summary>
    [Serializable]
	public partial class EventShorturlCollection : RepositoryList<EventShorturl, EventShorturlCollection>
	{	   
		public EventShorturlCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>EventShorturlCollection</returns>
		public EventShorturlCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                EventShorturl o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the event_shorturl table.
	/// </summary>
	[Serializable]
	public partial class EventShorturl : RepositoryRecord<EventShorturl>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public EventShorturl()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public EventShorturl(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("event_shorturl", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarSi = new TableSchema.TableColumn(schema);
				colvarSi.ColumnName = "si";
				colvarSi.DataType = DbType.Int64;
				colvarSi.MaxLength = 0;
				colvarSi.AutoIncrement = true;
				colvarSi.IsNullable = false;
				colvarSi.IsPrimaryKey = true;
				colvarSi.IsForeignKey = false;
				colvarSi.IsReadOnly = false;
				colvarSi.DefaultSetting = @"";
				colvarSi.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSi);
				
				TableSchema.TableColumn colvarEid = new TableSchema.TableColumn(schema);
				colvarEid.ColumnName = "eid";
				colvarEid.DataType = DbType.Guid;
				colvarEid.MaxLength = 0;
				colvarEid.AutoIncrement = false;
				colvarEid.IsNullable = true;
				colvarEid.IsPrimaryKey = false;
				colvarEid.IsForeignKey = false;
				colvarEid.IsReadOnly = false;
				colvarEid.DefaultSetting = @"";
				colvarEid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEid);
				
				TableSchema.TableColumn colvarMemberSource = new TableSchema.TableColumn(schema);
				colvarMemberSource.ColumnName = "member_source";
				colvarMemberSource.DataType = DbType.AnsiString;
				colvarMemberSource.MaxLength = 50;
				colvarMemberSource.AutoIncrement = false;
				colvarMemberSource.IsNullable = true;
				colvarMemberSource.IsPrimaryKey = false;
				colvarMemberSource.IsForeignKey = false;
				colvarMemberSource.IsReadOnly = false;
				colvarMemberSource.DefaultSetting = @"";
				colvarMemberSource.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMemberSource);
				
				TableSchema.TableColumn colvarMemberKey = new TableSchema.TableColumn(schema);
				colvarMemberKey.ColumnName = "member_key";
				colvarMemberKey.DataType = DbType.AnsiString;
				colvarMemberKey.MaxLength = 50;
				colvarMemberKey.AutoIncrement = false;
				colvarMemberKey.IsNullable = true;
				colvarMemberKey.IsPrimaryKey = false;
				colvarMemberKey.IsForeignKey = false;
				colvarMemberKey.IsReadOnly = false;
				colvarMemberKey.DefaultSetting = @"";
				colvarMemberKey.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMemberKey);
				
				TableSchema.TableColumn colvarUrl = new TableSchema.TableColumn(schema);
				colvarUrl.ColumnName = "url";
				colvarUrl.DataType = DbType.AnsiString;
				colvarUrl.MaxLength = 50;
				colvarUrl.AutoIncrement = false;
				colvarUrl.IsNullable = true;
				colvarUrl.IsPrimaryKey = false;
				colvarUrl.IsForeignKey = false;
				colvarUrl.IsReadOnly = false;
				colvarUrl.DefaultSetting = @"";
				colvarUrl.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUrl);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("event_shorturl",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Si")]
		[Bindable(true)]
		public long Si 
		{
			get { return GetColumnValue<long>(Columns.Si); }
			set { SetColumnValue(Columns.Si, value); }
		}
		  
		[XmlAttribute("Eid")]
		[Bindable(true)]
		public Guid? Eid 
		{
			get { return GetColumnValue<Guid?>(Columns.Eid); }
			set { SetColumnValue(Columns.Eid, value); }
		}
		  
		[XmlAttribute("MemberSource")]
		[Bindable(true)]
		public string MemberSource 
		{
			get { return GetColumnValue<string>(Columns.MemberSource); }
			set { SetColumnValue(Columns.MemberSource, value); }
		}
		  
		[XmlAttribute("MemberKey")]
		[Bindable(true)]
		public string MemberKey 
		{
			get { return GetColumnValue<string>(Columns.MemberKey); }
			set { SetColumnValue(Columns.MemberKey, value); }
		}
		  
		[XmlAttribute("Url")]
		[Bindable(true)]
		public string Url 
		{
			get { return GetColumnValue<string>(Columns.Url); }
			set { SetColumnValue(Columns.Url, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn SiColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn EidColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn MemberSourceColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn MemberKeyColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn UrlColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Si = @"si";
			 public static string Eid = @"eid";
			 public static string MemberSource = @"member_source";
			 public static string MemberKey = @"member_key";
			 public static string Url = @"url";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

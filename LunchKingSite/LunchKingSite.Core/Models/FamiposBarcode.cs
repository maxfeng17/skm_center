using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the FamiposBarcode class.
	/// </summary>
    [Serializable]
	public partial class FamiposBarcodeCollection : RepositoryList<FamiposBarcode, FamiposBarcodeCollection>
	{	   
		public FamiposBarcodeCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>FamiposBarcodeCollection</returns>
		public FamiposBarcodeCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                FamiposBarcode o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the famipos_barcode table.
	/// </summary>
	[Serializable]
	public partial class FamiposBarcode : RepositoryRecord<FamiposBarcode>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public FamiposBarcode()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public FamiposBarcode(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("famipos_barcode", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = false;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarTranNo = new TableSchema.TableColumn(schema);
				colvarTranNo.ColumnName = "tran_no";
				colvarTranNo.DataType = DbType.AnsiString;
				colvarTranNo.MaxLength = 11;
				colvarTranNo.AutoIncrement = false;
				colvarTranNo.IsNullable = false;
				colvarTranNo.IsPrimaryKey = true;
				colvarTranNo.IsForeignKey = false;
				colvarTranNo.IsReadOnly = false;
				colvarTranNo.DefaultSetting = @"";
				colvarTranNo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTranNo);
				
				TableSchema.TableColumn colvarFamiportId = new TableSchema.TableColumn(schema);
				colvarFamiportId.ColumnName = "famiport_id";
				colvarFamiportId.DataType = DbType.Int32;
				colvarFamiportId.MaxLength = 0;
				colvarFamiportId.AutoIncrement = false;
				colvarFamiportId.IsNullable = false;
				colvarFamiportId.IsPrimaryKey = false;
				colvarFamiportId.IsForeignKey = false;
				colvarFamiportId.IsReadOnly = false;
				colvarFamiportId.DefaultSetting = @"";
				colvarFamiportId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFamiportId);
				
				TableSchema.TableColumn colvarPeztempId = new TableSchema.TableColumn(schema);
				colvarPeztempId.ColumnName = "peztemp_id";
				colvarPeztempId.DataType = DbType.Int32;
				colvarPeztempId.MaxLength = 0;
				colvarPeztempId.AutoIncrement = false;
				colvarPeztempId.IsNullable = false;
				colvarPeztempId.IsPrimaryKey = false;
				colvarPeztempId.IsForeignKey = false;
				colvarPeztempId.IsReadOnly = false;
				colvarPeztempId.DefaultSetting = @"";
				colvarPeztempId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPeztempId);
				
				TableSchema.TableColumn colvarBarcode1 = new TableSchema.TableColumn(schema);
				colvarBarcode1.ColumnName = "barcode1";
				colvarBarcode1.DataType = DbType.AnsiString;
				colvarBarcode1.MaxLength = 17;
				colvarBarcode1.AutoIncrement = false;
				colvarBarcode1.IsNullable = true;
				colvarBarcode1.IsPrimaryKey = false;
				colvarBarcode1.IsForeignKey = false;
				colvarBarcode1.IsReadOnly = false;
				colvarBarcode1.DefaultSetting = @"";
				colvarBarcode1.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBarcode1);
				
				TableSchema.TableColumn colvarBarcode2 = new TableSchema.TableColumn(schema);
				colvarBarcode2.ColumnName = "barcode2";
				colvarBarcode2.DataType = DbType.AnsiString;
				colvarBarcode2.MaxLength = 18;
				colvarBarcode2.AutoIncrement = false;
				colvarBarcode2.IsNullable = true;
				colvarBarcode2.IsPrimaryKey = false;
				colvarBarcode2.IsForeignKey = false;
				colvarBarcode2.IsReadOnly = false;
				colvarBarcode2.DefaultSetting = @"";
				colvarBarcode2.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBarcode2);
				
				TableSchema.TableColumn colvarBarcode3 = new TableSchema.TableColumn(schema);
				colvarBarcode3.ColumnName = "barcode3";
				colvarBarcode3.DataType = DbType.AnsiString;
				colvarBarcode3.MaxLength = 18;
				colvarBarcode3.AutoIncrement = false;
				colvarBarcode3.IsNullable = true;
				colvarBarcode3.IsPrimaryKey = false;
				colvarBarcode3.IsForeignKey = false;
				colvarBarcode3.IsReadOnly = false;
				colvarBarcode3.DefaultSetting = @"";
				colvarBarcode3.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBarcode3);
				
				TableSchema.TableColumn colvarBarcode4 = new TableSchema.TableColumn(schema);
				colvarBarcode4.ColumnName = "barcode4";
				colvarBarcode4.DataType = DbType.AnsiString;
				colvarBarcode4.MaxLength = 20;
				colvarBarcode4.AutoIncrement = false;
				colvarBarcode4.IsNullable = true;
				colvarBarcode4.IsPrimaryKey = false;
				colvarBarcode4.IsForeignKey = false;
				colvarBarcode4.IsReadOnly = false;
				colvarBarcode4.DefaultSetting = @"";
				colvarBarcode4.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBarcode4);
				
				TableSchema.TableColumn colvarBarcode5 = new TableSchema.TableColumn(schema);
				colvarBarcode5.ColumnName = "barcode5";
				colvarBarcode5.DataType = DbType.AnsiString;
				colvarBarcode5.MaxLength = 20;
				colvarBarcode5.AutoIncrement = false;
				colvarBarcode5.IsNullable = true;
				colvarBarcode5.IsPrimaryKey = false;
				colvarBarcode5.IsForeignKey = false;
				colvarBarcode5.IsReadOnly = false;
				colvarBarcode5.DefaultSetting = @"";
				colvarBarcode5.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBarcode5);
				
				TableSchema.TableColumn colvarMmkId = new TableSchema.TableColumn(schema);
				colvarMmkId.ColumnName = "mmk_id";
				colvarMmkId.DataType = DbType.AnsiString;
				colvarMmkId.MaxLength = 3;
				colvarMmkId.AutoIncrement = false;
				colvarMmkId.IsNullable = true;
				colvarMmkId.IsPrimaryKey = false;
				colvarMmkId.IsForeignKey = false;
				colvarMmkId.IsReadOnly = false;
				colvarMmkId.DefaultSetting = @"";
				colvarMmkId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMmkId);
				
				TableSchema.TableColumn colvarTenCode = new TableSchema.TableColumn(schema);
				colvarTenCode.ColumnName = "ten_code";
				colvarTenCode.DataType = DbType.AnsiString;
				colvarTenCode.MaxLength = 6;
				colvarTenCode.AutoIncrement = false;
				colvarTenCode.IsNullable = true;
				colvarTenCode.IsPrimaryKey = false;
				colvarTenCode.IsForeignKey = false;
				colvarTenCode.IsReadOnly = false;
				colvarTenCode.DefaultSetting = @"";
				colvarTenCode.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTenCode);
				
				TableSchema.TableColumn colvarConfirmPayDate = new TableSchema.TableColumn(schema);
				colvarConfirmPayDate.ColumnName = "confirm_pay_date";
				colvarConfirmPayDate.DataType = DbType.AnsiString;
				colvarConfirmPayDate.MaxLength = 8;
				colvarConfirmPayDate.AutoIncrement = false;
				colvarConfirmPayDate.IsNullable = true;
				colvarConfirmPayDate.IsPrimaryKey = false;
				colvarConfirmPayDate.IsForeignKey = false;
				colvarConfirmPayDate.IsReadOnly = false;
				colvarConfirmPayDate.DefaultSetting = @"";
				colvarConfirmPayDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarConfirmPayDate);
				
				TableSchema.TableColumn colvarConfirmPayDttm = new TableSchema.TableColumn(schema);
				colvarConfirmPayDttm.ColumnName = "confirm_pay_dttm";
				colvarConfirmPayDttm.DataType = DbType.AnsiString;
				colvarConfirmPayDttm.MaxLength = 6;
				colvarConfirmPayDttm.AutoIncrement = false;
				colvarConfirmPayDttm.IsNullable = true;
				colvarConfirmPayDttm.IsPrimaryKey = false;
				colvarConfirmPayDttm.IsForeignKey = false;
				colvarConfirmPayDttm.IsReadOnly = false;
				colvarConfirmPayDttm.DefaultSetting = @"";
				colvarConfirmPayDttm.ForeignKeyTableName = "";
				schema.Columns.Add(colvarConfirmPayDttm);
				
				TableSchema.TableColumn colvarTmTenName = new TableSchema.TableColumn(schema);
				colvarTmTenName.ColumnName = "tm_ten_name";
				colvarTmTenName.DataType = DbType.String;
				colvarTmTenName.MaxLength = 20;
				colvarTmTenName.AutoIncrement = false;
				colvarTmTenName.IsNullable = true;
				colvarTmTenName.IsPrimaryKey = false;
				colvarTmTenName.IsForeignKey = false;
				colvarTmTenName.IsReadOnly = false;
				colvarTmTenName.DefaultSetting = @"";
				colvarTmTenName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTmTenName);
				
				TableSchema.TableColumn colvarMachineNo = new TableSchema.TableColumn(schema);
				colvarMachineNo.ColumnName = "machine_no";
				colvarMachineNo.DataType = DbType.AnsiString;
				colvarMachineNo.MaxLength = 2;
				colvarMachineNo.AutoIncrement = false;
				colvarMachineNo.IsNullable = true;
				colvarMachineNo.IsPrimaryKey = false;
				colvarMachineNo.IsForeignKey = false;
				colvarMachineNo.IsReadOnly = false;
				colvarMachineNo.DefaultSetting = @"";
				colvarMachineNo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMachineNo);
				
				TableSchema.TableColumn colvarTmTranNo = new TableSchema.TableColumn(schema);
				colvarTmTranNo.ColumnName = "tm_tran_no";
				colvarTmTranNo.DataType = DbType.AnsiString;
				colvarTmTranNo.MaxLength = 10;
				colvarTmTranNo.AutoIncrement = false;
				colvarTmTranNo.IsNullable = true;
				colvarTmTranNo.IsPrimaryKey = false;
				colvarTmTranNo.IsForeignKey = false;
				colvarTmTranNo.IsReadOnly = false;
				colvarTmTranNo.DefaultSetting = @"";
				colvarTmTranNo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTmTranNo);
				
				TableSchema.TableColumn colvarConfirmPayDatetime = new TableSchema.TableColumn(schema);
				colvarConfirmPayDatetime.ColumnName = "confirm_pay_datetime";
				colvarConfirmPayDatetime.DataType = DbType.DateTime;
				colvarConfirmPayDatetime.MaxLength = 0;
				colvarConfirmPayDatetime.AutoIncrement = false;
				colvarConfirmPayDatetime.IsNullable = true;
				colvarConfirmPayDatetime.IsPrimaryKey = false;
				colvarConfirmPayDatetime.IsForeignKey = false;
				colvarConfirmPayDatetime.IsReadOnly = false;
				colvarConfirmPayDatetime.DefaultSetting = @"";
				colvarConfirmPayDatetime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarConfirmPayDatetime);
				
				TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
				colvarStatus.ColumnName = "status";
				colvarStatus.DataType = DbType.Byte;
				colvarStatus.MaxLength = 0;
				colvarStatus.AutoIncrement = false;
				colvarStatus.IsNullable = false;
				colvarStatus.IsPrimaryKey = false;
				colvarStatus.IsForeignKey = false;
				colvarStatus.IsReadOnly = false;
				
						colvarStatus.DefaultSetting = @"((0))";
				colvarStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStatus);
				
				TableSchema.TableColumn colvarSerialNo = new TableSchema.TableColumn(schema);
				colvarSerialNo.ColumnName = "serial_no";
				colvarSerialNo.DataType = DbType.AnsiString;
				colvarSerialNo.MaxLength = 2;
				colvarSerialNo.AutoIncrement = false;
				colvarSerialNo.IsNullable = true;
				colvarSerialNo.IsPrimaryKey = false;
				colvarSerialNo.IsForeignKey = false;
				colvarSerialNo.IsReadOnly = false;
				colvarSerialNo.DefaultSetting = @"";
				colvarSerialNo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSerialNo);
				
				TableSchema.TableColumn colvarCdFmcode = new TableSchema.TableColumn(schema);
				colvarCdFmcode.ColumnName = "cd_fmcode";
				colvarCdFmcode.DataType = DbType.AnsiString;
				colvarCdFmcode.MaxLength = 7;
				colvarCdFmcode.AutoIncrement = false;
				colvarCdFmcode.IsNullable = true;
				colvarCdFmcode.IsPrimaryKey = false;
				colvarCdFmcode.IsForeignKey = false;
				colvarCdFmcode.IsReadOnly = false;
				colvarCdFmcode.DefaultSetting = @"";
				colvarCdFmcode.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCdFmcode);
				
				TableSchema.TableColumn colvarFirmNo = new TableSchema.TableColumn(schema);
				colvarFirmNo.ColumnName = "firm_no";
				colvarFirmNo.DataType = DbType.AnsiString;
				colvarFirmNo.MaxLength = 10;
				colvarFirmNo.AutoIncrement = false;
				colvarFirmNo.IsNullable = true;
				colvarFirmNo.IsPrimaryKey = false;
				colvarFirmNo.IsForeignKey = false;
				colvarFirmNo.IsReadOnly = false;
				colvarFirmNo.DefaultSetting = @"";
				colvarFirmNo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFirmNo);
				
				TableSchema.TableColumn colvarEventNo = new TableSchema.TableColumn(schema);
				colvarEventNo.ColumnName = "event_no";
				colvarEventNo.DataType = DbType.AnsiString;
				colvarEventNo.MaxLength = 20;
				colvarEventNo.AutoIncrement = false;
				colvarEventNo.IsNullable = true;
				colvarEventNo.IsPrimaryKey = false;
				colvarEventNo.IsForeignKey = false;
				colvarEventNo.IsReadOnly = false;
				colvarEventNo.DefaultSetting = @"";
				colvarEventNo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEventNo);
				
				TableSchema.TableColumn colvarPayDate = new TableSchema.TableColumn(schema);
				colvarPayDate.ColumnName = "pay_date";
				colvarPayDate.DataType = DbType.AnsiString;
				colvarPayDate.MaxLength = 8;
				colvarPayDate.AutoIncrement = false;
				colvarPayDate.IsNullable = true;
				colvarPayDate.IsPrimaryKey = false;
				colvarPayDate.IsForeignKey = false;
				colvarPayDate.IsReadOnly = false;
				colvarPayDate.DefaultSetting = @"";
				colvarPayDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPayDate);
				
				TableSchema.TableColumn colvarPayTime = new TableSchema.TableColumn(schema);
				colvarPayTime.ColumnName = "pay_time";
				colvarPayTime.DataType = DbType.AnsiString;
				colvarPayTime.MaxLength = 6;
				colvarPayTime.AutoIncrement = false;
				colvarPayTime.IsNullable = true;
				colvarPayTime.IsPrimaryKey = false;
				colvarPayTime.IsForeignKey = false;
				colvarPayTime.IsReadOnly = false;
				colvarPayTime.DefaultSetting = @"";
				colvarPayTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPayTime);
				
				TableSchema.TableColumn colvarPayDatetime = new TableSchema.TableColumn(schema);
				colvarPayDatetime.ColumnName = "pay_datetime";
				colvarPayDatetime.DataType = DbType.DateTime;
				colvarPayDatetime.MaxLength = 0;
				colvarPayDatetime.AutoIncrement = false;
				colvarPayDatetime.IsNullable = true;
				colvarPayDatetime.IsPrimaryKey = false;
				colvarPayDatetime.IsForeignKey = false;
				colvarPayDatetime.IsReadOnly = false;
				colvarPayDatetime.DefaultSetting = @"";
				colvarPayDatetime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPayDatetime);
				
				TableSchema.TableColumn colvarUpdateConfirmPayTime = new TableSchema.TableColumn(schema);
				colvarUpdateConfirmPayTime.ColumnName = "update_confirm_pay_time";
				colvarUpdateConfirmPayTime.DataType = DbType.DateTime;
				colvarUpdateConfirmPayTime.MaxLength = 0;
				colvarUpdateConfirmPayTime.AutoIncrement = false;
				colvarUpdateConfirmPayTime.IsNullable = true;
				colvarUpdateConfirmPayTime.IsPrimaryKey = false;
				colvarUpdateConfirmPayTime.IsForeignKey = false;
				colvarUpdateConfirmPayTime.IsReadOnly = false;
				colvarUpdateConfirmPayTime.DefaultSetting = @"";
				colvarUpdateConfirmPayTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUpdateConfirmPayTime);
				
				TableSchema.TableColumn colvarUpdatePayTime = new TableSchema.TableColumn(schema);
				colvarUpdatePayTime.ColumnName = "update_pay_time";
				colvarUpdatePayTime.DataType = DbType.DateTime;
				colvarUpdatePayTime.MaxLength = 0;
				colvarUpdatePayTime.AutoIncrement = false;
				colvarUpdatePayTime.IsNullable = true;
				colvarUpdatePayTime.IsPrimaryKey = false;
				colvarUpdatePayTime.IsForeignKey = false;
				colvarUpdatePayTime.IsReadOnly = false;
				colvarUpdatePayTime.DefaultSetting = @"";
				colvarUpdatePayTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUpdatePayTime);
				
				TableSchema.TableColumn colvarModifyDatetime = new TableSchema.TableColumn(schema);
				colvarModifyDatetime.ColumnName = "modify_datetime";
				colvarModifyDatetime.DataType = DbType.DateTime;
				colvarModifyDatetime.MaxLength = 0;
				colvarModifyDatetime.AutoIncrement = false;
				colvarModifyDatetime.IsNullable = true;
				colvarModifyDatetime.IsPrimaryKey = false;
				colvarModifyDatetime.IsForeignKey = false;
				colvarModifyDatetime.IsReadOnly = false;
				colvarModifyDatetime.DefaultSetting = @"";
				colvarModifyDatetime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyDatetime);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("famipos_barcode",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("TranNo")]
		[Bindable(true)]
		public string TranNo 
		{
			get { return GetColumnValue<string>(Columns.TranNo); }
			set { SetColumnValue(Columns.TranNo, value); }
		}
		  
		[XmlAttribute("FamiportId")]
		[Bindable(true)]
		public int FamiportId 
		{
			get { return GetColumnValue<int>(Columns.FamiportId); }
			set { SetColumnValue(Columns.FamiportId, value); }
		}
		  
		[XmlAttribute("PeztempId")]
		[Bindable(true)]
		public int PeztempId 
		{
			get { return GetColumnValue<int>(Columns.PeztempId); }
			set { SetColumnValue(Columns.PeztempId, value); }
		}
		  
		[XmlAttribute("Barcode1")]
		[Bindable(true)]
		public string Barcode1 
		{
			get { return GetColumnValue<string>(Columns.Barcode1); }
			set { SetColumnValue(Columns.Barcode1, value); }
		}
		  
		[XmlAttribute("Barcode2")]
		[Bindable(true)]
		public string Barcode2 
		{
			get { return GetColumnValue<string>(Columns.Barcode2); }
			set { SetColumnValue(Columns.Barcode2, value); }
		}
		  
		[XmlAttribute("Barcode3")]
		[Bindable(true)]
		public string Barcode3 
		{
			get { return GetColumnValue<string>(Columns.Barcode3); }
			set { SetColumnValue(Columns.Barcode3, value); }
		}
		  
		[XmlAttribute("Barcode4")]
		[Bindable(true)]
		public string Barcode4 
		{
			get { return GetColumnValue<string>(Columns.Barcode4); }
			set { SetColumnValue(Columns.Barcode4, value); }
		}
		  
		[XmlAttribute("Barcode5")]
		[Bindable(true)]
		public string Barcode5 
		{
			get { return GetColumnValue<string>(Columns.Barcode5); }
			set { SetColumnValue(Columns.Barcode5, value); }
		}
		  
		[XmlAttribute("MmkId")]
		[Bindable(true)]
		public string MmkId 
		{
			get { return GetColumnValue<string>(Columns.MmkId); }
			set { SetColumnValue(Columns.MmkId, value); }
		}
		  
		[XmlAttribute("TenCode")]
		[Bindable(true)]
		public string TenCode 
		{
			get { return GetColumnValue<string>(Columns.TenCode); }
			set { SetColumnValue(Columns.TenCode, value); }
		}
		  
		[XmlAttribute("ConfirmPayDate")]
		[Bindable(true)]
		public string ConfirmPayDate 
		{
			get { return GetColumnValue<string>(Columns.ConfirmPayDate); }
			set { SetColumnValue(Columns.ConfirmPayDate, value); }
		}
		  
		[XmlAttribute("ConfirmPayDttm")]
		[Bindable(true)]
		public string ConfirmPayDttm 
		{
			get { return GetColumnValue<string>(Columns.ConfirmPayDttm); }
			set { SetColumnValue(Columns.ConfirmPayDttm, value); }
		}
		  
		[XmlAttribute("TmTenName")]
		[Bindable(true)]
		public string TmTenName 
		{
			get { return GetColumnValue<string>(Columns.TmTenName); }
			set { SetColumnValue(Columns.TmTenName, value); }
		}
		  
		[XmlAttribute("MachineNo")]
		[Bindable(true)]
		public string MachineNo 
		{
			get { return GetColumnValue<string>(Columns.MachineNo); }
			set { SetColumnValue(Columns.MachineNo, value); }
		}
		  
		[XmlAttribute("TmTranNo")]
		[Bindable(true)]
		public string TmTranNo 
		{
			get { return GetColumnValue<string>(Columns.TmTranNo); }
			set { SetColumnValue(Columns.TmTranNo, value); }
		}
		  
		[XmlAttribute("ConfirmPayDatetime")]
		[Bindable(true)]
		public DateTime? ConfirmPayDatetime 
		{
			get { return GetColumnValue<DateTime?>(Columns.ConfirmPayDatetime); }
			set { SetColumnValue(Columns.ConfirmPayDatetime, value); }
		}
		  
		[XmlAttribute("Status")]
		[Bindable(true)]
		public byte Status 
		{
			get { return GetColumnValue<byte>(Columns.Status); }
			set { SetColumnValue(Columns.Status, value); }
		}
		  
		[XmlAttribute("SerialNo")]
		[Bindable(true)]
		public string SerialNo 
		{
			get { return GetColumnValue<string>(Columns.SerialNo); }
			set { SetColumnValue(Columns.SerialNo, value); }
		}
		  
		[XmlAttribute("CdFmcode")]
		[Bindable(true)]
		public string CdFmcode 
		{
			get { return GetColumnValue<string>(Columns.CdFmcode); }
			set { SetColumnValue(Columns.CdFmcode, value); }
		}
		  
		[XmlAttribute("FirmNo")]
		[Bindable(true)]
		public string FirmNo 
		{
			get { return GetColumnValue<string>(Columns.FirmNo); }
			set { SetColumnValue(Columns.FirmNo, value); }
		}
		  
		[XmlAttribute("EventNo")]
		[Bindable(true)]
		public string EventNo 
		{
			get { return GetColumnValue<string>(Columns.EventNo); }
			set { SetColumnValue(Columns.EventNo, value); }
		}
		  
		[XmlAttribute("PayDate")]
		[Bindable(true)]
		public string PayDate 
		{
			get { return GetColumnValue<string>(Columns.PayDate); }
			set { SetColumnValue(Columns.PayDate, value); }
		}
		  
		[XmlAttribute("PayTime")]
		[Bindable(true)]
		public string PayTime 
		{
			get { return GetColumnValue<string>(Columns.PayTime); }
			set { SetColumnValue(Columns.PayTime, value); }
		}
		  
		[XmlAttribute("PayDatetime")]
		[Bindable(true)]
		public DateTime? PayDatetime 
		{
			get { return GetColumnValue<DateTime?>(Columns.PayDatetime); }
			set { SetColumnValue(Columns.PayDatetime, value); }
		}
		  
		[XmlAttribute("UpdateConfirmPayTime")]
		[Bindable(true)]
		public DateTime? UpdateConfirmPayTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.UpdateConfirmPayTime); }
			set { SetColumnValue(Columns.UpdateConfirmPayTime, value); }
		}
		  
		[XmlAttribute("UpdatePayTime")]
		[Bindable(true)]
		public DateTime? UpdatePayTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.UpdatePayTime); }
			set { SetColumnValue(Columns.UpdatePayTime, value); }
		}
		  
		[XmlAttribute("ModifyDatetime")]
		[Bindable(true)]
		public DateTime? ModifyDatetime 
		{
			get { return GetColumnValue<DateTime?>(Columns.ModifyDatetime); }
			set { SetColumnValue(Columns.ModifyDatetime, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn TranNoColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn FamiportIdColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn PeztempIdColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn Barcode1Column
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn Barcode2Column
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn Barcode3Column
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn Barcode4Column
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn Barcode5Column
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn MmkIdColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn TenCodeColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        public static TableSchema.TableColumn ConfirmPayDateColumn
        {
            get { return Schema.Columns[11]; }
        }
        
        
        
        public static TableSchema.TableColumn ConfirmPayDttmColumn
        {
            get { return Schema.Columns[12]; }
        }
        
        
        
        public static TableSchema.TableColumn TmTenNameColumn
        {
            get { return Schema.Columns[13]; }
        }
        
        
        
        public static TableSchema.TableColumn MachineNoColumn
        {
            get { return Schema.Columns[14]; }
        }
        
        
        
        public static TableSchema.TableColumn TmTranNoColumn
        {
            get { return Schema.Columns[15]; }
        }
        
        
        
        public static TableSchema.TableColumn ConfirmPayDatetimeColumn
        {
            get { return Schema.Columns[16]; }
        }
        
        
        
        public static TableSchema.TableColumn StatusColumn
        {
            get { return Schema.Columns[17]; }
        }
        
        
        
        public static TableSchema.TableColumn SerialNoColumn
        {
            get { return Schema.Columns[18]; }
        }
        
        
        
        public static TableSchema.TableColumn CdFmcodeColumn
        {
            get { return Schema.Columns[19]; }
        }
        
        
        
        public static TableSchema.TableColumn FirmNoColumn
        {
            get { return Schema.Columns[20]; }
        }
        
        
        
        public static TableSchema.TableColumn EventNoColumn
        {
            get { return Schema.Columns[21]; }
        }
        
        
        
        public static TableSchema.TableColumn PayDateColumn
        {
            get { return Schema.Columns[22]; }
        }
        
        
        
        public static TableSchema.TableColumn PayTimeColumn
        {
            get { return Schema.Columns[23]; }
        }
        
        
        
        public static TableSchema.TableColumn PayDatetimeColumn
        {
            get { return Schema.Columns[24]; }
        }
        
        
        
        public static TableSchema.TableColumn UpdateConfirmPayTimeColumn
        {
            get { return Schema.Columns[25]; }
        }
        
        
        
        public static TableSchema.TableColumn UpdatePayTimeColumn
        {
            get { return Schema.Columns[26]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyDatetimeColumn
        {
            get { return Schema.Columns[27]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string TranNo = @"tran_no";
			 public static string FamiportId = @"famiport_id";
			 public static string PeztempId = @"peztemp_id";
			 public static string Barcode1 = @"barcode1";
			 public static string Barcode2 = @"barcode2";
			 public static string Barcode3 = @"barcode3";
			 public static string Barcode4 = @"barcode4";
			 public static string Barcode5 = @"barcode5";
			 public static string MmkId = @"mmk_id";
			 public static string TenCode = @"ten_code";
			 public static string ConfirmPayDate = @"confirm_pay_date";
			 public static string ConfirmPayDttm = @"confirm_pay_dttm";
			 public static string TmTenName = @"tm_ten_name";
			 public static string MachineNo = @"machine_no";
			 public static string TmTranNo = @"tm_tran_no";
			 public static string ConfirmPayDatetime = @"confirm_pay_datetime";
			 public static string Status = @"status";
			 public static string SerialNo = @"serial_no";
			 public static string CdFmcode = @"cd_fmcode";
			 public static string FirmNo = @"firm_no";
			 public static string EventNo = @"event_no";
			 public static string PayDate = @"pay_date";
			 public static string PayTime = @"pay_time";
			 public static string PayDatetime = @"pay_datetime";
			 public static string UpdateConfirmPayTime = @"update_confirm_pay_time";
			 public static string UpdatePayTime = @"update_pay_time";
			 public static string ModifyDatetime = @"modify_datetime";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

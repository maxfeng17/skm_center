using System;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using SubSonic;

namespace LunchKingSite.DataOrm
{
	[Serializable]
	public partial class ViewPaymentTransactionLeftjoinOrderCollection : ReadOnlyList<ViewPaymentTransactionLeftjoinOrder, ViewPaymentTransactionLeftjoinOrderCollection>
	{
			public ViewPaymentTransactionLeftjoinOrderCollection() {}

	}

	[Serializable]
	public partial class ViewPaymentTransactionLeftjoinOrder : ReadOnlyRecord<ViewPaymentTransactionLeftjoinOrder>, IReadOnlyRecord
	{
		#region .ctors and Default Settings
		public ViewPaymentTransactionLeftjoinOrder()
		{
			SetSQLProps();
			InitSetDefaults();
			MarkNew();
		}

		private void InitSetDefaults() { SetDefaults(); }

		public ViewPaymentTransactionLeftjoinOrder(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}

		protected static void SetSQLProps() { GetTableSchema(); }

		public ViewPaymentTransactionLeftjoinOrder(object keyID)
		{
				SetSQLProps();
				LoadByKey(keyID);
		}

		public ViewPaymentTransactionLeftjoinOrder(string columnName, object columnValue)
		{
				SetSQLProps();
				LoadByParam(columnName,columnValue);
		}
		#endregion

		#region Schema and Query Accessor
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}

		private static void GetTableSchema()
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("view_payment_transaction_leftjoin_order", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				TableSchema.TableColumn colvarPaymentTransactionId = new TableSchema.TableColumn(schema);
				colvarPaymentTransactionId.ColumnName = "payment_transaction_id";
				colvarPaymentTransactionId.DataType = DbType.Int32;
				colvarPaymentTransactionId.MaxLength = 0;
				colvarPaymentTransactionId.AutoIncrement = false;
				colvarPaymentTransactionId.IsNullable = false;
				colvarPaymentTransactionId.IsPrimaryKey = false;
				colvarPaymentTransactionId.IsForeignKey = false;
				colvarPaymentTransactionId.IsReadOnly = false;
				colvarPaymentTransactionId.DefaultSetting = @"";
				colvarPaymentTransactionId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPaymentTransactionId);

				TableSchema.TableColumn colvarTransId = new TableSchema.TableColumn(schema);
				colvarTransId.ColumnName = "trans_id";
				colvarTransId.DataType = DbType.AnsiString;
				colvarTransId.MaxLength = 40;
				colvarTransId.AutoIncrement = false;
				colvarTransId.IsNullable = false;
				colvarTransId.IsPrimaryKey = false;
				colvarTransId.IsForeignKey = false;
				colvarTransId.IsReadOnly = false;
				colvarTransId.DefaultSetting = @"";
				colvarTransId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTransId);

				TableSchema.TableColumn colvarPaymentType = new TableSchema.TableColumn(schema);
				colvarPaymentType.ColumnName = "payment_type";
				colvarPaymentType.DataType = DbType.Int32;
				colvarPaymentType.MaxLength = 0;
				colvarPaymentType.AutoIncrement = false;
				colvarPaymentType.IsNullable = false;
				colvarPaymentType.IsPrimaryKey = false;
				colvarPaymentType.IsForeignKey = false;
				colvarPaymentType.IsReadOnly = false;
				colvarPaymentType.DefaultSetting = @"";
				colvarPaymentType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPaymentType);

				TableSchema.TableColumn colvarAmount = new TableSchema.TableColumn(schema);
				colvarAmount.ColumnName = "amount";
				colvarAmount.DataType = DbType.Currency;
				colvarAmount.MaxLength = 0;
				colvarAmount.AutoIncrement = false;
				colvarAmount.IsNullable = false;
				colvarAmount.IsPrimaryKey = false;
				colvarAmount.IsForeignKey = false;
				colvarAmount.IsReadOnly = false;
				colvarAmount.DefaultSetting = @"";
				colvarAmount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAmount);

				TableSchema.TableColumn colvarAuthCode = new TableSchema.TableColumn(schema);
				colvarAuthCode.ColumnName = "auth_code";
				colvarAuthCode.DataType = DbType.AnsiString;
				colvarAuthCode.MaxLength = 20;
				colvarAuthCode.AutoIncrement = false;
				colvarAuthCode.IsNullable = true;
				colvarAuthCode.IsPrimaryKey = false;
				colvarAuthCode.IsForeignKey = false;
				colvarAuthCode.IsReadOnly = false;
				colvarAuthCode.DefaultSetting = @"";
				colvarAuthCode.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAuthCode);

				TableSchema.TableColumn colvarTransType = new TableSchema.TableColumn(schema);
				colvarTransType.ColumnName = "trans_type";
				colvarTransType.DataType = DbType.Int32;
				colvarTransType.MaxLength = 0;
				colvarTransType.AutoIncrement = false;
				colvarTransType.IsNullable = true;
				colvarTransType.IsPrimaryKey = false;
				colvarTransType.IsForeignKey = false;
				colvarTransType.IsReadOnly = false;
				colvarTransType.DefaultSetting = @"";
				colvarTransType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTransType);

				TableSchema.TableColumn colvarTransTime = new TableSchema.TableColumn(schema);
				colvarTransTime.ColumnName = "trans_time";
				colvarTransTime.DataType = DbType.DateTime;
				colvarTransTime.MaxLength = 0;
				colvarTransTime.AutoIncrement = false;
				colvarTransTime.IsNullable = true;
				colvarTransTime.IsPrimaryKey = false;
				colvarTransTime.IsForeignKey = false;
				colvarTransTime.IsReadOnly = false;
				colvarTransTime.DefaultSetting = @"";
				colvarTransTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTransTime);

				TableSchema.TableColumn colvarResult = new TableSchema.TableColumn(schema);
				colvarResult.ColumnName = "result";
				colvarResult.DataType = DbType.Int32;
				colvarResult.MaxLength = 0;
				colvarResult.AutoIncrement = false;
				colvarResult.IsNullable = true;
				colvarResult.IsPrimaryKey = false;
				colvarResult.IsForeignKey = false;
				colvarResult.IsReadOnly = false;
				colvarResult.DefaultSetting = @"";
				colvarResult.ForeignKeyTableName = "";
				schema.Columns.Add(colvarResult);

				TableSchema.TableColumn colvarPaymentStatus = new TableSchema.TableColumn(schema);
				colvarPaymentStatus.ColumnName = "payment_status";
				colvarPaymentStatus.DataType = DbType.Int32;
				colvarPaymentStatus.MaxLength = 0;
				colvarPaymentStatus.AutoIncrement = false;
				colvarPaymentStatus.IsNullable = false;
				colvarPaymentStatus.IsPrimaryKey = false;
				colvarPaymentStatus.IsForeignKey = false;
				colvarPaymentStatus.IsReadOnly = false;
				colvarPaymentStatus.DefaultSetting = @"";
				colvarPaymentStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPaymentStatus);

				TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
				colvarCreateId.ColumnName = "create_id";
				colvarCreateId.DataType = DbType.String;
				colvarCreateId.MaxLength = 256;
				colvarCreateId.AutoIncrement = false;
				colvarCreateId.IsNullable = true;
				colvarCreateId.IsPrimaryKey = false;
				colvarCreateId.IsForeignKey = false;
				colvarCreateId.IsReadOnly = false;
				colvarCreateId.DefaultSetting = @"";
				colvarCreateId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateId);

				TableSchema.TableColumn colvarMessage = new TableSchema.TableColumn(schema);
				colvarMessage.ColumnName = "message";
				colvarMessage.DataType = DbType.String;
				colvarMessage.MaxLength = 4000;
				colvarMessage.AutoIncrement = false;
				colvarMessage.IsNullable = true;
				colvarMessage.IsPrimaryKey = false;
				colvarMessage.IsForeignKey = false;
				colvarMessage.IsReadOnly = false;
				colvarMessage.DefaultSetting = @"";
				colvarMessage.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMessage);

				TableSchema.TableColumn colvarSellerGuid = new TableSchema.TableColumn(schema);
				colvarSellerGuid.ColumnName = "seller_GUID";
				colvarSellerGuid.DataType = DbType.Guid;
				colvarSellerGuid.MaxLength = 0;
				colvarSellerGuid.AutoIncrement = false;
				colvarSellerGuid.IsNullable = true;
				colvarSellerGuid.IsPrimaryKey = false;
				colvarSellerGuid.IsForeignKey = false;
				colvarSellerGuid.IsReadOnly = false;
				colvarSellerGuid.DefaultSetting = @"";
				colvarSellerGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSellerGuid);

				TableSchema.TableColumn colvarSellerName = new TableSchema.TableColumn(schema);
				colvarSellerName.ColumnName = "seller_name";
				colvarSellerName.DataType = DbType.String;
				colvarSellerName.MaxLength = 50;
				colvarSellerName.AutoIncrement = false;
				colvarSellerName.IsNullable = true;
				colvarSellerName.IsPrimaryKey = false;
				colvarSellerName.IsForeignKey = false;
				colvarSellerName.IsReadOnly = false;
				colvarSellerName.DefaultSetting = @"";
				colvarSellerName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSellerName);

				TableSchema.TableColumn colvarMemberName = new TableSchema.TableColumn(schema);
				colvarMemberName.ColumnName = "member_name";
				colvarMemberName.DataType = DbType.String;
				colvarMemberName.MaxLength = 50;
				colvarMemberName.AutoIncrement = false;
				colvarMemberName.IsNullable = true;
				colvarMemberName.IsPrimaryKey = false;
				colvarMemberName.IsForeignKey = false;
				colvarMemberName.IsReadOnly = false;
				colvarMemberName.DefaultSetting = @"";
				colvarMemberName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMemberName);

				TableSchema.TableColumn colvarPhoneNumber = new TableSchema.TableColumn(schema);
				colvarPhoneNumber.ColumnName = "phone_number";
				colvarPhoneNumber.DataType = DbType.AnsiString;
				colvarPhoneNumber.MaxLength = 50;
				colvarPhoneNumber.AutoIncrement = false;
				colvarPhoneNumber.IsNullable = true;
				colvarPhoneNumber.IsPrimaryKey = false;
				colvarPhoneNumber.IsForeignKey = false;
				colvarPhoneNumber.IsReadOnly = false;
				colvarPhoneNumber.DefaultSetting = @"";
				colvarPhoneNumber.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPhoneNumber);

				TableSchema.TableColumn colvarMobileNumber = new TableSchema.TableColumn(schema);
				colvarMobileNumber.ColumnName = "mobile_number";
				colvarMobileNumber.DataType = DbType.AnsiString;
				colvarMobileNumber.MaxLength = 50;
				colvarMobileNumber.AutoIncrement = false;
				colvarMobileNumber.IsNullable = true;
				colvarMobileNumber.IsPrimaryKey = false;
				colvarMobileNumber.IsForeignKey = false;
				colvarMobileNumber.IsReadOnly = false;
				colvarMobileNumber.DefaultSetting = @"";
				colvarMobileNumber.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMobileNumber);

				TableSchema.TableColumn colvarDeliveryAddress = new TableSchema.TableColumn(schema);
				colvarDeliveryAddress.ColumnName = "delivery_address";
				colvarDeliveryAddress.DataType = DbType.String;
				colvarDeliveryAddress.MaxLength = 200;
				colvarDeliveryAddress.AutoIncrement = false;
				colvarDeliveryAddress.IsNullable = true;
				colvarDeliveryAddress.IsPrimaryKey = false;
				colvarDeliveryAddress.IsForeignKey = false;
				colvarDeliveryAddress.IsReadOnly = false;
				colvarDeliveryAddress.DefaultSetting = @"";
				colvarDeliveryAddress.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDeliveryAddress);

				TableSchema.TableColumn colvarDeliveryTime = new TableSchema.TableColumn(schema);
				colvarDeliveryTime.ColumnName = "delivery_time";
				colvarDeliveryTime.DataType = DbType.DateTime;
				colvarDeliveryTime.MaxLength = 0;
				colvarDeliveryTime.AutoIncrement = false;
				colvarDeliveryTime.IsNullable = true;
				colvarDeliveryTime.IsPrimaryKey = false;
				colvarDeliveryTime.IsForeignKey = false;
				colvarDeliveryTime.IsReadOnly = false;
				colvarDeliveryTime.DefaultSetting = @"";
				colvarDeliveryTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDeliveryTime);

				TableSchema.TableColumn colvarSubtotal = new TableSchema.TableColumn(schema);
				colvarSubtotal.ColumnName = "subtotal";
				colvarSubtotal.DataType = DbType.Currency;
				colvarSubtotal.MaxLength = 0;
				colvarSubtotal.AutoIncrement = false;
				colvarSubtotal.IsNullable = true;
				colvarSubtotal.IsPrimaryKey = false;
				colvarSubtotal.IsForeignKey = false;
				colvarSubtotal.IsReadOnly = false;
				colvarSubtotal.DefaultSetting = @"";
				colvarSubtotal.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSubtotal);

				TableSchema.TableColumn colvarTotal = new TableSchema.TableColumn(schema);
				colvarTotal.ColumnName = "total";
				colvarTotal.DataType = DbType.Currency;
				colvarTotal.MaxLength = 0;
				colvarTotal.AutoIncrement = false;
				colvarTotal.IsNullable = true;
				colvarTotal.IsPrimaryKey = false;
				colvarTotal.IsForeignKey = false;
				colvarTotal.IsReadOnly = false;
				colvarTotal.DefaultSetting = @"";
				colvarTotal.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTotal);

				TableSchema.TableColumn colvarUserMemo = new TableSchema.TableColumn(schema);
				colvarUserMemo.ColumnName = "user_memo";
				colvarUserMemo.DataType = DbType.String;
				colvarUserMemo.MaxLength = 1073741823;
				colvarUserMemo.AutoIncrement = false;
				colvarUserMemo.IsNullable = true;
				colvarUserMemo.IsPrimaryKey = false;
				colvarUserMemo.IsForeignKey = false;
				colvarUserMemo.IsReadOnly = false;
				colvarUserMemo.DefaultSetting = @"";
				colvarUserMemo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUserMemo);

				TableSchema.TableColumn colvarOrderMemo = new TableSchema.TableColumn(schema);
				colvarOrderMemo.ColumnName = "order_memo";
				colvarOrderMemo.DataType = DbType.String;
				colvarOrderMemo.MaxLength = 1073741823;
				colvarOrderMemo.AutoIncrement = false;
				colvarOrderMemo.IsNullable = true;
				colvarOrderMemo.IsPrimaryKey = false;
				colvarOrderMemo.IsForeignKey = false;
				colvarOrderMemo.IsReadOnly = false;
				colvarOrderMemo.DefaultSetting = @"";
				colvarOrderMemo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderMemo);

				TableSchema.TableColumn colvarOrderStatus = new TableSchema.TableColumn(schema);
				colvarOrderStatus.ColumnName = "order_status";
				colvarOrderStatus.DataType = DbType.Int32;
				colvarOrderStatus.MaxLength = 0;
				colvarOrderStatus.AutoIncrement = false;
				colvarOrderStatus.IsNullable = true;
				colvarOrderStatus.IsPrimaryKey = false;
				colvarOrderStatus.IsForeignKey = false;
				colvarOrderStatus.IsReadOnly = false;
				colvarOrderStatus.DefaultSetting = @"";
				colvarOrderStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderStatus);

				TableSchema.TableColumn colvarOrderStage = new TableSchema.TableColumn(schema);
				colvarOrderStage.ColumnName = "order_stage";
				colvarOrderStage.DataType = DbType.Int32;
				colvarOrderStage.MaxLength = 0;
				colvarOrderStage.AutoIncrement = false;
				colvarOrderStage.IsNullable = true;
				colvarOrderStage.IsPrimaryKey = false;
				colvarOrderStage.IsForeignKey = false;
				colvarOrderStage.IsReadOnly = false;
				colvarOrderStage.DefaultSetting = @"";
				colvarOrderStage.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderStage);

				TableSchema.TableColumn colvarBonusPoint = new TableSchema.TableColumn(schema);
				colvarBonusPoint.ColumnName = "bonus_point";
				colvarBonusPoint.DataType = DbType.AnsiString;
				colvarBonusPoint.MaxLength = 10;
				colvarBonusPoint.AutoIncrement = false;
				colvarBonusPoint.IsNullable = true;
				colvarBonusPoint.IsPrimaryKey = false;
				colvarBonusPoint.IsForeignKey = false;
				colvarBonusPoint.IsReadOnly = false;
				colvarBonusPoint.DefaultSetting = @"";
				colvarBonusPoint.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBonusPoint);

				TableSchema.TableColumn colvarOrderCreateTime = new TableSchema.TableColumn(schema);
				colvarOrderCreateTime.ColumnName = "order_create_time";
				colvarOrderCreateTime.DataType = DbType.DateTime;
				colvarOrderCreateTime.MaxLength = 0;
				colvarOrderCreateTime.AutoIncrement = false;
				colvarOrderCreateTime.IsNullable = true;
				colvarOrderCreateTime.IsPrimaryKey = false;
				colvarOrderCreateTime.IsForeignKey = false;
				colvarOrderCreateTime.IsReadOnly = false;
				colvarOrderCreateTime.DefaultSetting = @"";
				colvarOrderCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderCreateTime);

				TableSchema.TableColumn colvarOrderModifyId = new TableSchema.TableColumn(schema);
				colvarOrderModifyId.ColumnName = "order_modify_id";
				colvarOrderModifyId.DataType = DbType.String;
				colvarOrderModifyId.MaxLength = 30;
				colvarOrderModifyId.AutoIncrement = false;
				colvarOrderModifyId.IsNullable = true;
				colvarOrderModifyId.IsPrimaryKey = false;
				colvarOrderModifyId.IsForeignKey = false;
				colvarOrderModifyId.IsReadOnly = false;
				colvarOrderModifyId.DefaultSetting = @"";
				colvarOrderModifyId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderModifyId);

				TableSchema.TableColumn colvarOrderModifyTime = new TableSchema.TableColumn(schema);
				colvarOrderModifyTime.ColumnName = "order_modify_time";
				colvarOrderModifyTime.DataType = DbType.DateTime;
				colvarOrderModifyTime.MaxLength = 0;
				colvarOrderModifyTime.AutoIncrement = false;
				colvarOrderModifyTime.IsNullable = true;
				colvarOrderModifyTime.IsPrimaryKey = false;
				colvarOrderModifyTime.IsForeignKey = false;
				colvarOrderModifyTime.IsReadOnly = false;
				colvarOrderModifyTime.DefaultSetting = @"";
				colvarOrderModifyTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderModifyTime);

				TableSchema.TableColumn colvarAccessLock = new TableSchema.TableColumn(schema);
				colvarAccessLock.ColumnName = "access_lock";
				colvarAccessLock.DataType = DbType.String;
				colvarAccessLock.MaxLength = 50;
				colvarAccessLock.AutoIncrement = false;
				colvarAccessLock.IsNullable = true;
				colvarAccessLock.IsPrimaryKey = false;
				colvarAccessLock.IsForeignKey = false;
				colvarAccessLock.IsReadOnly = false;
				colvarAccessLock.DefaultSetting = @"";
				colvarAccessLock.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAccessLock);

				TableSchema.TableColumn colvarCityId = new TableSchema.TableColumn(schema);
				colvarCityId.ColumnName = "city_id";
				colvarCityId.DataType = DbType.Int32;
				colvarCityId.MaxLength = 0;
				colvarCityId.AutoIncrement = false;
				colvarCityId.IsNullable = true;
				colvarCityId.IsPrimaryKey = false;
				colvarCityId.IsForeignKey = false;
				colvarCityId.IsReadOnly = false;
				colvarCityId.DefaultSetting = @"";
				colvarCityId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCityId);

				TableSchema.TableColumn colvarParentOrderId = new TableSchema.TableColumn(schema);
				colvarParentOrderId.ColumnName = "parent_order_id";
				colvarParentOrderId.DataType = DbType.Guid;
				colvarParentOrderId.MaxLength = 0;
				colvarParentOrderId.AutoIncrement = false;
				colvarParentOrderId.IsNullable = true;
				colvarParentOrderId.IsPrimaryKey = false;
				colvarParentOrderId.IsForeignKey = false;
				colvarParentOrderId.IsReadOnly = false;
				colvarParentOrderId.DefaultSetting = @"";
				colvarParentOrderId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarParentOrderId);

				TableSchema.TableColumn colvarOrderClassification = new TableSchema.TableColumn(schema);
				colvarOrderClassification.ColumnName = "order_classification";
				colvarOrderClassification.DataType = DbType.Int32;
				colvarOrderClassification.MaxLength = 0;
				colvarOrderClassification.AutoIncrement = false;
				colvarOrderClassification.IsNullable = true;
				colvarOrderClassification.IsPrimaryKey = false;
				colvarOrderClassification.IsForeignKey = false;
				colvarOrderClassification.IsReadOnly = false;
				colvarOrderClassification.DefaultSetting = @"";
				colvarOrderClassification.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderClassification);

				TableSchema.TableColumn colvarOrderId = new TableSchema.TableColumn(schema);
				colvarOrderId.ColumnName = "order_id";
				colvarOrderId.DataType = DbType.AnsiString;
				colvarOrderId.MaxLength = 40;
				colvarOrderId.AutoIncrement = false;
				colvarOrderId.IsNullable = true;
				colvarOrderId.IsPrimaryKey = false;
				colvarOrderId.IsForeignKey = false;
				colvarOrderId.IsReadOnly = false;
				colvarOrderId.DefaultSetting = @"";
				colvarOrderId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderId);

				TableSchema.TableColumn colvarOrderGuid = new TableSchema.TableColumn(schema);
				colvarOrderGuid.ColumnName = "order_guid";
				colvarOrderGuid.DataType = DbType.Guid;
				colvarOrderGuid.MaxLength = 0;
				colvarOrderGuid.AutoIncrement = false;
				colvarOrderGuid.IsNullable = true;
				colvarOrderGuid.IsPrimaryKey = false;
				colvarOrderGuid.IsForeignKey = false;
				colvarOrderGuid.IsReadOnly = false;
				colvarOrderGuid.DefaultSetting = @"";
				colvarOrderGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderGuid);

				TableSchema.TableColumn colvarApiProvider = new TableSchema.TableColumn(schema);
				colvarApiProvider.ColumnName = "api_provider";
				colvarApiProvider.DataType = DbType.Int32;
				colvarApiProvider.MaxLength = 0;
				colvarApiProvider.AutoIncrement = false;
				colvarApiProvider.IsNullable = true;
				colvarApiProvider.IsPrimaryKey = false;
				colvarApiProvider.IsForeignKey = false;
				colvarApiProvider.IsReadOnly = false;
				colvarApiProvider.DefaultSetting = @"";
				colvarApiProvider.ForeignKeyTableName = "";
				schema.Columns.Add(colvarApiProvider);

				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("view_payment_transaction_leftjoin_order",schema);
			}
		}
		#endregion

		#region Props

		[XmlAttribute("PaymentTransactionId")]
		[Bindable(true)]
		public int PaymentTransactionId
		{
			get { return GetColumnValue<int>(Columns.PaymentTransactionId); }
			set { SetColumnValue(Columns.PaymentTransactionId, value); }
		}

		[XmlAttribute("TransId")]
		[Bindable(true)]
		public string TransId
		{
			get { return GetColumnValue<string>(Columns.TransId); }
			set { SetColumnValue(Columns.TransId, value); }
		}

		[XmlAttribute("PaymentType")]
		[Bindable(true)]
		public int PaymentType
		{
			get { return GetColumnValue<int>(Columns.PaymentType); }
			set { SetColumnValue(Columns.PaymentType, value); }
		}

		[XmlAttribute("Amount")]
		[Bindable(true)]
		public decimal Amount
		{
			get { return GetColumnValue<decimal>(Columns.Amount); }
			set { SetColumnValue(Columns.Amount, value); }
		}

		[XmlAttribute("AuthCode")]
		[Bindable(true)]
		public string AuthCode
		{
			get { return GetColumnValue<string>(Columns.AuthCode); }
			set { SetColumnValue(Columns.AuthCode, value); }
		}

		[XmlAttribute("TransType")]
		[Bindable(true)]
		public int? TransType
		{
			get { return GetColumnValue<int?>(Columns.TransType); }
			set { SetColumnValue(Columns.TransType, value); }
		}

		[XmlAttribute("TransTime")]
		[Bindable(true)]
		public DateTime? TransTime
		{
			get { return GetColumnValue<DateTime?>(Columns.TransTime); }
			set { SetColumnValue(Columns.TransTime, value); }
		}

		[XmlAttribute("Result")]
		[Bindable(true)]
		public int? Result
		{
			get { return GetColumnValue<int?>(Columns.Result); }
			set { SetColumnValue(Columns.Result, value); }
		}

		[XmlAttribute("PaymentStatus")]
		[Bindable(true)]
		public int PaymentStatus
		{
			get { return GetColumnValue<int>(Columns.PaymentStatus); }
			set { SetColumnValue(Columns.PaymentStatus, value); }
		}

		[XmlAttribute("CreateId")]
		[Bindable(true)]
		public string CreateId
		{
			get { return GetColumnValue<string>(Columns.CreateId); }
			set { SetColumnValue(Columns.CreateId, value); }
		}

		[XmlAttribute("Message")]
		[Bindable(true)]
		public string Message
		{
			get { return GetColumnValue<string>(Columns.Message); }
			set { SetColumnValue(Columns.Message, value); }
		}

		[XmlAttribute("SellerGuid")]
		[Bindable(true)]
		public Guid? SellerGuid
		{
			get { return GetColumnValue<Guid?>(Columns.SellerGuid); }
			set { SetColumnValue(Columns.SellerGuid, value); }
		}

		[XmlAttribute("SellerName")]
		[Bindable(true)]
		public string SellerName
		{
			get { return GetColumnValue<string>(Columns.SellerName); }
			set { SetColumnValue(Columns.SellerName, value); }
		}

		[XmlAttribute("MemberName")]
		[Bindable(true)]
		public string MemberName
		{
			get { return GetColumnValue<string>(Columns.MemberName); }
			set { SetColumnValue(Columns.MemberName, value); }
		}

		[XmlAttribute("PhoneNumber")]
		[Bindable(true)]
		public string PhoneNumber
		{
			get { return GetColumnValue<string>(Columns.PhoneNumber); }
			set { SetColumnValue(Columns.PhoneNumber, value); }
		}

		[XmlAttribute("MobileNumber")]
		[Bindable(true)]
		public string MobileNumber
		{
			get { return GetColumnValue<string>(Columns.MobileNumber); }
			set { SetColumnValue(Columns.MobileNumber, value); }
		}

		[XmlAttribute("DeliveryAddress")]
		[Bindable(true)]
		public string DeliveryAddress
		{
			get { return GetColumnValue<string>(Columns.DeliveryAddress); }
			set { SetColumnValue(Columns.DeliveryAddress, value); }
		}

		[XmlAttribute("DeliveryTime")]
		[Bindable(true)]
		public DateTime? DeliveryTime
		{
			get { return GetColumnValue<DateTime?>(Columns.DeliveryTime); }
			set { SetColumnValue(Columns.DeliveryTime, value); }
		}

		[XmlAttribute("Subtotal")]
		[Bindable(true)]
		public decimal? Subtotal
		{
			get { return GetColumnValue<decimal?>(Columns.Subtotal); }
			set { SetColumnValue(Columns.Subtotal, value); }
		}

		[XmlAttribute("Total")]
		[Bindable(true)]
		public decimal? Total
		{
			get { return GetColumnValue<decimal?>(Columns.Total); }
			set { SetColumnValue(Columns.Total, value); }
		}

		[XmlAttribute("UserMemo")]
		[Bindable(true)]
		public string UserMemo
		{
			get { return GetColumnValue<string>(Columns.UserMemo); }
			set { SetColumnValue(Columns.UserMemo, value); }
		}

		[XmlAttribute("OrderMemo")]
		[Bindable(true)]
		public string OrderMemo
		{
			get { return GetColumnValue<string>(Columns.OrderMemo); }
			set { SetColumnValue(Columns.OrderMemo, value); }
		}

		[XmlAttribute("OrderStatus")]
		[Bindable(true)]
		public int? OrderStatus
		{
			get { return GetColumnValue<int?>(Columns.OrderStatus); }
			set { SetColumnValue(Columns.OrderStatus, value); }
		}

		[XmlAttribute("OrderStage")]
		[Bindable(true)]
		public int? OrderStage
		{
			get { return GetColumnValue<int?>(Columns.OrderStage); }
			set { SetColumnValue(Columns.OrderStage, value); }
		}

		[XmlAttribute("BonusPoint")]
		[Bindable(true)]
		public string BonusPoint
		{
			get { return GetColumnValue<string>(Columns.BonusPoint); }
			set { SetColumnValue(Columns.BonusPoint, value); }
		}

		[XmlAttribute("OrderCreateTime")]
		[Bindable(true)]
		public DateTime? OrderCreateTime
		{
			get { return GetColumnValue<DateTime?>(Columns.OrderCreateTime); }
			set { SetColumnValue(Columns.OrderCreateTime, value); }
		}

		[XmlAttribute("OrderModifyId")]
		[Bindable(true)]
		public string OrderModifyId
		{
			get { return GetColumnValue<string>(Columns.OrderModifyId); }
			set { SetColumnValue(Columns.OrderModifyId, value); }
		}

		[XmlAttribute("OrderModifyTime")]
		[Bindable(true)]
		public DateTime? OrderModifyTime
		{
			get { return GetColumnValue<DateTime?>(Columns.OrderModifyTime); }
			set { SetColumnValue(Columns.OrderModifyTime, value); }
		}

		[XmlAttribute("AccessLock")]
		[Bindable(true)]
		public string AccessLock
		{
			get { return GetColumnValue<string>(Columns.AccessLock); }
			set { SetColumnValue(Columns.AccessLock, value); }
		}

		[XmlAttribute("CityId")]
		[Bindable(true)]
		public int? CityId
		{
			get { return GetColumnValue<int?>(Columns.CityId); }
			set { SetColumnValue(Columns.CityId, value); }
		}

		[XmlAttribute("ParentOrderId")]
		[Bindable(true)]
		public Guid? ParentOrderId
		{
			get { return GetColumnValue<Guid?>(Columns.ParentOrderId); }
			set { SetColumnValue(Columns.ParentOrderId, value); }
		}

		[XmlAttribute("OrderClassification")]
		[Bindable(true)]
		public int? OrderClassification
		{
			get { return GetColumnValue<int?>(Columns.OrderClassification); }
			set { SetColumnValue(Columns.OrderClassification, value); }
		}

		[XmlAttribute("OrderId")]
		[Bindable(true)]
		public string OrderId
		{
			get { return GetColumnValue<string>(Columns.OrderId); }
			set { SetColumnValue(Columns.OrderId, value); }
		}

		[XmlAttribute("OrderGuid")]
		[Bindable(true)]
		public Guid? OrderGuid
		{
			get { return GetColumnValue<Guid?>(Columns.OrderGuid); }
			set { SetColumnValue(Columns.OrderGuid, value); }
		}

		[XmlAttribute("ApiProvider")]
		[Bindable(true)]
		public int? ApiProvider
		{
			get { return GetColumnValue<int?>(Columns.ApiProvider); }
			set { SetColumnValue(Columns.ApiProvider, value); }
		}

		#endregion

		#region Typed Columns

		public static TableSchema.TableColumn PaymentTransactionIdColumn
		{
			get { return Schema.Columns[0]; }
		}

		public static TableSchema.TableColumn TransIdColumn
		{
			get { return Schema.Columns[1]; }
		}

		public static TableSchema.TableColumn PaymentTypeColumn
		{
			get { return Schema.Columns[2]; }
		}

		public static TableSchema.TableColumn AmountColumn
		{
			get { return Schema.Columns[3]; }
		}

		public static TableSchema.TableColumn AuthCodeColumn
		{
			get { return Schema.Columns[4]; }
		}

		public static TableSchema.TableColumn TransTypeColumn
		{
			get { return Schema.Columns[5]; }
		}

		public static TableSchema.TableColumn TransTimeColumn
		{
			get { return Schema.Columns[6]; }
		}

		public static TableSchema.TableColumn ResultColumn
		{
			get { return Schema.Columns[7]; }
		}

		public static TableSchema.TableColumn PaymentStatusColumn
		{
			get { return Schema.Columns[8]; }
		}

		public static TableSchema.TableColumn CreateIdColumn
		{
			get { return Schema.Columns[9]; }
		}

		public static TableSchema.TableColumn MessageColumn
		{
			get { return Schema.Columns[10]; }
		}

		public static TableSchema.TableColumn SellerGuidColumn
		{
			get { return Schema.Columns[11]; }
		}

		public static TableSchema.TableColumn SellerNameColumn
		{
			get { return Schema.Columns[12]; }
		}

		public static TableSchema.TableColumn MemberNameColumn
		{
			get { return Schema.Columns[13]; }
		}

		public static TableSchema.TableColumn PhoneNumberColumn
		{
			get { return Schema.Columns[14]; }
		}

		public static TableSchema.TableColumn MobileNumberColumn
		{
			get { return Schema.Columns[15]; }
		}

		public static TableSchema.TableColumn DeliveryAddressColumn
		{
			get { return Schema.Columns[16]; }
		}

		public static TableSchema.TableColumn DeliveryTimeColumn
		{
			get { return Schema.Columns[17]; }
		}

		public static TableSchema.TableColumn SubtotalColumn
		{
			get { return Schema.Columns[18]; }
		}

		public static TableSchema.TableColumn TotalColumn
		{
			get { return Schema.Columns[19]; }
		}

		public static TableSchema.TableColumn UserMemoColumn
		{
			get { return Schema.Columns[20]; }
		}

		public static TableSchema.TableColumn OrderMemoColumn
		{
			get { return Schema.Columns[21]; }
		}

		public static TableSchema.TableColumn OrderStatusColumn
		{
			get { return Schema.Columns[22]; }
		}

		public static TableSchema.TableColumn OrderStageColumn
		{
			get { return Schema.Columns[23]; }
		}

		public static TableSchema.TableColumn BonusPointColumn
		{
			get { return Schema.Columns[24]; }
		}

		public static TableSchema.TableColumn OrderCreateTimeColumn
		{
			get { return Schema.Columns[25]; }
		}

		public static TableSchema.TableColumn OrderModifyIdColumn
		{
			get { return Schema.Columns[26]; }
		}

		public static TableSchema.TableColumn OrderModifyTimeColumn
		{
			get { return Schema.Columns[27]; }
		}

		public static TableSchema.TableColumn AccessLockColumn
		{
			get { return Schema.Columns[28]; }
		}

		public static TableSchema.TableColumn CityIdColumn
		{
			get { return Schema.Columns[29]; }
		}

		public static TableSchema.TableColumn ParentOrderIdColumn
		{
			get { return Schema.Columns[30]; }
		}

		public static TableSchema.TableColumn OrderClassificationColumn
		{
			get { return Schema.Columns[31]; }
		}

		public static TableSchema.TableColumn OrderIdColumn
		{
			get { return Schema.Columns[32]; }
		}

		public static TableSchema.TableColumn OrderGuidColumn
		{
			get { return Schema.Columns[33]; }
		}

		public static TableSchema.TableColumn ApiProviderColumn
		{
			get { return Schema.Columns[34]; }
		}

		#endregion

		#region Columns Struct

		public struct Columns
		{
			public static string PaymentTransactionId = @"payment_transaction_id";
			public static string TransId = @"trans_id";
			public static string PaymentType = @"payment_type";
			public static string Amount = @"amount";
			public static string AuthCode = @"auth_code";
			public static string TransType = @"trans_type";
			public static string TransTime = @"trans_time";
			public static string Result = @"result";
			public static string PaymentStatus = @"payment_status";
			public static string CreateId = @"create_id";
			public static string Message = @"message";
			public static string SellerGuid = @"seller_GUID";
			public static string SellerName = @"seller_name";
			public static string MemberName = @"member_name";
			public static string PhoneNumber = @"phone_number";
			public static string MobileNumber = @"mobile_number";
			public static string DeliveryAddress = @"delivery_address";
			public static string DeliveryTime = @"delivery_time";
			public static string Subtotal = @"subtotal";
			public static string Total = @"total";
			public static string UserMemo = @"user_memo";
			public static string OrderMemo = @"order_memo";
			public static string OrderStatus = @"order_status";
			public static string OrderStage = @"order_stage";
			public static string BonusPoint = @"bonus_point";
			public static string OrderCreateTime = @"order_create_time";
			public static string OrderModifyId = @"order_modify_id";
			public static string OrderModifyTime = @"order_modify_time";
			public static string AccessLock = @"access_lock";
			public static string CityId = @"city_id";
			public static string ParentOrderId = @"parent_order_id";
			public static string OrderClassification = @"order_classification";
			public static string OrderId = @"order_id";
			public static string OrderGuid = @"order_guid";
			public static string ApiProvider = @"api_provider";
		}

		#endregion

		#region IAbstractRecord Members
		public new CT GetColumnValue<CT>(string columnName)
		{
			return base.GetColumnValue<CT>(columnName);
		}
		public object GetColumnValue(string columnName)
		{
			return base.GetColumnValue<object>(columnName);
		}
		#endregion

	}
}

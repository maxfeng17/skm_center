using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewBalanceSheetWmsBillList class.
    /// </summary>
    [Serializable]
    public partial class ViewBalanceSheetWmsBillListCollection : ReadOnlyList<ViewBalanceSheetWmsBillList, ViewBalanceSheetWmsBillListCollection>
    {        
        public ViewBalanceSheetWmsBillListCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_balance_sheet_wms_bill_list view.
    /// </summary>
    [Serializable]
    public partial class ViewBalanceSheetWmsBillList : ReadOnlyRecord<ViewBalanceSheetWmsBillList>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_balance_sheet_wms_bill_list", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = false;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = false;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                
                schema.Columns.Add(colvarId);
                
                TableSchema.TableColumn colvarYear = new TableSchema.TableColumn(schema);
                colvarYear.ColumnName = "year";
                colvarYear.DataType = DbType.Int32;
                colvarYear.MaxLength = 0;
                colvarYear.AutoIncrement = false;
                colvarYear.IsNullable = false;
                colvarYear.IsPrimaryKey = false;
                colvarYear.IsForeignKey = false;
                colvarYear.IsReadOnly = false;
                
                schema.Columns.Add(colvarYear);
                
                TableSchema.TableColumn colvarMonth = new TableSchema.TableColumn(schema);
                colvarMonth.ColumnName = "month";
                colvarMonth.DataType = DbType.Int32;
                colvarMonth.MaxLength = 0;
                colvarMonth.AutoIncrement = false;
                colvarMonth.IsNullable = false;
                colvarMonth.IsPrimaryKey = false;
                colvarMonth.IsForeignKey = false;
                colvarMonth.IsReadOnly = false;
                
                schema.Columns.Add(colvarMonth);
                
                TableSchema.TableColumn colvarName = new TableSchema.TableColumn(schema);
                colvarName.ColumnName = "name";
                colvarName.DataType = DbType.String;
                colvarName.MaxLength = 6;
                colvarName.AutoIncrement = false;
                colvarName.IsNullable = false;
                colvarName.IsPrimaryKey = false;
                colvarName.IsForeignKey = false;
                colvarName.IsReadOnly = false;
                
                schema.Columns.Add(colvarName);
                
                TableSchema.TableColumn colvarBillId = new TableSchema.TableColumn(schema);
                colvarBillId.ColumnName = "bill_id";
                colvarBillId.DataType = DbType.Int32;
                colvarBillId.MaxLength = 0;
                colvarBillId.AutoIncrement = false;
                colvarBillId.IsNullable = true;
                colvarBillId.IsPrimaryKey = false;
                colvarBillId.IsForeignKey = false;
                colvarBillId.IsReadOnly = false;
                
                schema.Columns.Add(colvarBillId);
                
                TableSchema.TableColumn colvarBillNumber = new TableSchema.TableColumn(schema);
                colvarBillNumber.ColumnName = "bill_number";
                colvarBillNumber.DataType = DbType.String;
                colvarBillNumber.MaxLength = 10;
                colvarBillNumber.AutoIncrement = false;
                colvarBillNumber.IsNullable = true;
                colvarBillNumber.IsPrimaryKey = false;
                colvarBillNumber.IsForeignKey = false;
                colvarBillNumber.IsReadOnly = false;
                
                schema.Columns.Add(colvarBillNumber);
                
                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = true;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarCreateTime);
                
                TableSchema.TableColumn colvarInvoiceDate = new TableSchema.TableColumn(schema);
                colvarInvoiceDate.ColumnName = "invoice_date";
                colvarInvoiceDate.DataType = DbType.DateTime;
                colvarInvoiceDate.MaxLength = 0;
                colvarInvoiceDate.AutoIncrement = false;
                colvarInvoiceDate.IsNullable = true;
                colvarInvoiceDate.IsPrimaryKey = false;
                colvarInvoiceDate.IsForeignKey = false;
                colvarInvoiceDate.IsReadOnly = false;
                
                schema.Columns.Add(colvarInvoiceDate);
                
                TableSchema.TableColumn colvarBuyerType = new TableSchema.TableColumn(schema);
                colvarBuyerType.ColumnName = "buyer_type";
                colvarBuyerType.DataType = DbType.Int32;
                colvarBuyerType.MaxLength = 0;
                colvarBuyerType.AutoIncrement = false;
                colvarBuyerType.IsNullable = true;
                colvarBuyerType.IsPrimaryKey = false;
                colvarBuyerType.IsForeignKey = false;
                colvarBuyerType.IsReadOnly = false;
                
                schema.Columns.Add(colvarBuyerType);
                
                TableSchema.TableColumn colvarBillSentDate = new TableSchema.TableColumn(schema);
                colvarBillSentDate.ColumnName = "bill_sent_date";
                colvarBillSentDate.DataType = DbType.DateTime;
                colvarBillSentDate.MaxLength = 0;
                colvarBillSentDate.AutoIncrement = false;
                colvarBillSentDate.IsNullable = true;
                colvarBillSentDate.IsPrimaryKey = false;
                colvarBillSentDate.IsForeignKey = false;
                colvarBillSentDate.IsReadOnly = false;
                
                schema.Columns.Add(colvarBillSentDate);
                
                TableSchema.TableColumn colvarBillMoney = new TableSchema.TableColumn(schema);
                colvarBillMoney.ColumnName = "bill_money";
                colvarBillMoney.DataType = DbType.Int32;
                colvarBillMoney.MaxLength = 0;
                colvarBillMoney.AutoIncrement = false;
                colvarBillMoney.IsNullable = true;
                colvarBillMoney.IsPrimaryKey = false;
                colvarBillMoney.IsForeignKey = false;
                colvarBillMoney.IsReadOnly = false;
                
                schema.Columns.Add(colvarBillMoney);
                
                TableSchema.TableColumn colvarBillMoneyNotaxed = new TableSchema.TableColumn(schema);
                colvarBillMoneyNotaxed.ColumnName = "bill_money_notaxed";
                colvarBillMoneyNotaxed.DataType = DbType.Int32;
                colvarBillMoneyNotaxed.MaxLength = 0;
                colvarBillMoneyNotaxed.AutoIncrement = false;
                colvarBillMoneyNotaxed.IsNullable = true;
                colvarBillMoneyNotaxed.IsPrimaryKey = false;
                colvarBillMoneyNotaxed.IsForeignKey = false;
                colvarBillMoneyNotaxed.IsReadOnly = false;
                
                schema.Columns.Add(colvarBillMoneyNotaxed);
                
                TableSchema.TableColumn colvarBillTax = new TableSchema.TableColumn(schema);
                colvarBillTax.ColumnName = "bill_tax";
                colvarBillTax.DataType = DbType.Int32;
                colvarBillTax.MaxLength = 0;
                colvarBillTax.AutoIncrement = false;
                colvarBillTax.IsNullable = true;
                colvarBillTax.IsPrimaryKey = false;
                colvarBillTax.IsForeignKey = false;
                colvarBillTax.IsReadOnly = false;
                
                schema.Columns.Add(colvarBillTax);
                
                TableSchema.TableColumn colvarRemark = new TableSchema.TableColumn(schema);
                colvarRemark.ColumnName = "remark";
                colvarRemark.DataType = DbType.String;
                colvarRemark.MaxLength = 250;
                colvarRemark.AutoIncrement = false;
                colvarRemark.IsNullable = true;
                colvarRemark.IsPrimaryKey = false;
                colvarRemark.IsForeignKey = false;
                colvarRemark.IsReadOnly = false;
                
                schema.Columns.Add(colvarRemark);
                
                TableSchema.TableColumn colvarDealStartTime = new TableSchema.TableColumn(schema);
                colvarDealStartTime.ColumnName = "deal_start_time";
                colvarDealStartTime.DataType = DbType.Int32;
                colvarDealStartTime.MaxLength = 0;
                colvarDealStartTime.AutoIncrement = false;
                colvarDealStartTime.IsNullable = true;
                colvarDealStartTime.IsPrimaryKey = false;
                colvarDealStartTime.IsForeignKey = false;
                colvarDealStartTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarDealStartTime);
                
                TableSchema.TableColumn colvarDealEndTime = new TableSchema.TableColumn(schema);
                colvarDealEndTime.ColumnName = "deal_end_time";
                colvarDealEndTime.DataType = DbType.Int32;
                colvarDealEndTime.MaxLength = 0;
                colvarDealEndTime.AutoIncrement = false;
                colvarDealEndTime.IsNullable = true;
                colvarDealEndTime.IsPrimaryKey = false;
                colvarDealEndTime.IsForeignKey = false;
                colvarDealEndTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarDealEndTime);
                
                TableSchema.TableColumn colvarUseStartTime = new TableSchema.TableColumn(schema);
                colvarUseStartTime.ColumnName = "use_start_time";
                colvarUseStartTime.DataType = DbType.Int32;
                colvarUseStartTime.MaxLength = 0;
                colvarUseStartTime.AutoIncrement = false;
                colvarUseStartTime.IsNullable = true;
                colvarUseStartTime.IsPrimaryKey = false;
                colvarUseStartTime.IsForeignKey = false;
                colvarUseStartTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarUseStartTime);
                
                TableSchema.TableColumn colvarUseEndTime = new TableSchema.TableColumn(schema);
                colvarUseEndTime.ColumnName = "use_end_time";
                colvarUseEndTime.DataType = DbType.Int32;
                colvarUseEndTime.MaxLength = 0;
                colvarUseEndTime.AutoIncrement = false;
                colvarUseEndTime.IsNullable = true;
                colvarUseEndTime.IsPrimaryKey = false;
                colvarUseEndTime.IsForeignKey = false;
                colvarUseEndTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarUseEndTime);
                
                TableSchema.TableColumn colvarVendorReceiptType = new TableSchema.TableColumn(schema);
                colvarVendorReceiptType.ColumnName = "vendor_receipt_type";
                colvarVendorReceiptType.DataType = DbType.Int32;
                colvarVendorReceiptType.MaxLength = 0;
                colvarVendorReceiptType.AutoIncrement = false;
                colvarVendorReceiptType.IsNullable = true;
                colvarVendorReceiptType.IsPrimaryKey = false;
                colvarVendorReceiptType.IsForeignKey = false;
                colvarVendorReceiptType.IsReadOnly = false;
                
                schema.Columns.Add(colvarVendorReceiptType);
                
                TableSchema.TableColumn colvarCompanyAccountName = new TableSchema.TableColumn(schema);
                colvarCompanyAccountName.ColumnName = "CompanyAccountName";
                colvarCompanyAccountName.DataType = DbType.String;
                colvarCompanyAccountName.MaxLength = 50;
                colvarCompanyAccountName.AutoIncrement = false;
                colvarCompanyAccountName.IsNullable = true;
                colvarCompanyAccountName.IsPrimaryKey = false;
                colvarCompanyAccountName.IsForeignKey = false;
                colvarCompanyAccountName.IsReadOnly = false;
                
                schema.Columns.Add(colvarCompanyAccountName);
                
                TableSchema.TableColumn colvarCompanyName = new TableSchema.TableColumn(schema);
                colvarCompanyName.ColumnName = "CompanyName";
                colvarCompanyName.DataType = DbType.String;
                colvarCompanyName.MaxLength = 50;
                colvarCompanyName.AutoIncrement = false;
                colvarCompanyName.IsNullable = true;
                colvarCompanyName.IsPrimaryKey = false;
                colvarCompanyName.IsForeignKey = false;
                colvarCompanyName.IsReadOnly = false;
                
                schema.Columns.Add(colvarCompanyName);
                
                TableSchema.TableColumn colvarCompanyAccount = new TableSchema.TableColumn(schema);
                colvarCompanyAccount.ColumnName = "CompanyAccount";
                colvarCompanyAccount.DataType = DbType.AnsiString;
                colvarCompanyAccount.MaxLength = 20;
                colvarCompanyAccount.AutoIncrement = false;
                colvarCompanyAccount.IsNullable = true;
                colvarCompanyAccount.IsPrimaryKey = false;
                colvarCompanyAccount.IsForeignKey = false;
                colvarCompanyAccount.IsReadOnly = false;
                
                schema.Columns.Add(colvarCompanyAccount);
                
                TableSchema.TableColumn colvarCompanyBankCode = new TableSchema.TableColumn(schema);
                colvarCompanyBankCode.ColumnName = "CompanyBankCode";
                colvarCompanyBankCode.DataType = DbType.AnsiString;
                colvarCompanyBankCode.MaxLength = 10;
                colvarCompanyBankCode.AutoIncrement = false;
                colvarCompanyBankCode.IsNullable = true;
                colvarCompanyBankCode.IsPrimaryKey = false;
                colvarCompanyBankCode.IsForeignKey = false;
                colvarCompanyBankCode.IsReadOnly = false;
                
                schema.Columns.Add(colvarCompanyBankCode);
                
                TableSchema.TableColumn colvarCompanyBranchCode = new TableSchema.TableColumn(schema);
                colvarCompanyBranchCode.ColumnName = "CompanyBranchCode";
                colvarCompanyBranchCode.DataType = DbType.AnsiString;
                colvarCompanyBranchCode.MaxLength = 10;
                colvarCompanyBranchCode.AutoIncrement = false;
                colvarCompanyBranchCode.IsNullable = true;
                colvarCompanyBranchCode.IsPrimaryKey = false;
                colvarCompanyBranchCode.IsForeignKey = false;
                colvarCompanyBranchCode.IsReadOnly = false;
                
                schema.Columns.Add(colvarCompanyBranchCode);
                
                TableSchema.TableColumn colvarCompanyID = new TableSchema.TableColumn(schema);
                colvarCompanyID.ColumnName = "CompanyID";
                colvarCompanyID.DataType = DbType.AnsiString;
                colvarCompanyID.MaxLength = 20;
                colvarCompanyID.AutoIncrement = false;
                colvarCompanyID.IsNullable = true;
                colvarCompanyID.IsPrimaryKey = false;
                colvarCompanyID.IsForeignKey = false;
                colvarCompanyID.IsReadOnly = false;
                
                schema.Columns.Add(colvarCompanyID);
                
                TableSchema.TableColumn colvarSignCompanyID = new TableSchema.TableColumn(schema);
                colvarSignCompanyID.ColumnName = "SignCompanyID";
                colvarSignCompanyID.DataType = DbType.AnsiString;
                colvarSignCompanyID.MaxLength = 20;
                colvarSignCompanyID.AutoIncrement = false;
                colvarSignCompanyID.IsNullable = true;
                colvarSignCompanyID.IsPrimaryKey = false;
                colvarSignCompanyID.IsForeignKey = false;
                colvarSignCompanyID.IsReadOnly = false;
                
                schema.Columns.Add(colvarSignCompanyID);
                
                TableSchema.TableColumn colvarCompanyBossName = new TableSchema.TableColumn(schema);
                colvarCompanyBossName.ColumnName = "CompanyBossName";
                colvarCompanyBossName.DataType = DbType.String;
                colvarCompanyBossName.MaxLength = 30;
                colvarCompanyBossName.AutoIncrement = false;
                colvarCompanyBossName.IsNullable = true;
                colvarCompanyBossName.IsPrimaryKey = false;
                colvarCompanyBossName.IsForeignKey = false;
                colvarCompanyBossName.IsReadOnly = false;
                
                schema.Columns.Add(colvarCompanyBossName);
                
                TableSchema.TableColumn colvarAccountantName = new TableSchema.TableColumn(schema);
                colvarAccountantName.ColumnName = "accountant_name";
                colvarAccountantName.DataType = DbType.String;
                colvarAccountantName.MaxLength = 20;
                colvarAccountantName.AutoIncrement = false;
                colvarAccountantName.IsNullable = true;
                colvarAccountantName.IsPrimaryKey = false;
                colvarAccountantName.IsForeignKey = false;
                colvarAccountantName.IsReadOnly = false;
                
                schema.Columns.Add(colvarAccountantName);
                
                TableSchema.TableColumn colvarAccountantTel = new TableSchema.TableColumn(schema);
                colvarAccountantTel.ColumnName = "accountant_tel";
                colvarAccountantTel.DataType = DbType.AnsiString;
                colvarAccountantTel.MaxLength = 100;
                colvarAccountantTel.AutoIncrement = false;
                colvarAccountantTel.IsNullable = true;
                colvarAccountantTel.IsPrimaryKey = false;
                colvarAccountantTel.IsForeignKey = false;
                colvarAccountantTel.IsReadOnly = false;
                
                schema.Columns.Add(colvarAccountantTel);
                
                TableSchema.TableColumn colvarAccountantEmail = new TableSchema.TableColumn(schema);
                colvarAccountantEmail.ColumnName = "accountant_email";
                colvarAccountantEmail.DataType = DbType.String;
                colvarAccountantEmail.MaxLength = 250;
                colvarAccountantEmail.AutoIncrement = false;
                colvarAccountantEmail.IsNullable = true;
                colvarAccountantEmail.IsPrimaryKey = false;
                colvarAccountantEmail.IsForeignKey = false;
                colvarAccountantEmail.IsReadOnly = false;
                
                schema.Columns.Add(colvarAccountantEmail);
                
                TableSchema.TableColumn colvarIsTransferComplete = new TableSchema.TableColumn(schema);
                colvarIsTransferComplete.ColumnName = "is_transfer_complete";
                colvarIsTransferComplete.DataType = DbType.Boolean;
                colvarIsTransferComplete.MaxLength = 0;
                colvarIsTransferComplete.AutoIncrement = false;
                colvarIsTransferComplete.IsNullable = true;
                colvarIsTransferComplete.IsPrimaryKey = false;
                colvarIsTransferComplete.IsForeignKey = false;
                colvarIsTransferComplete.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsTransferComplete);
                
                TableSchema.TableColumn colvarResponseTime = new TableSchema.TableColumn(schema);
                colvarResponseTime.ColumnName = "response_time";
                colvarResponseTime.DataType = DbType.DateTime;
                colvarResponseTime.MaxLength = 0;
                colvarResponseTime.AutoIncrement = false;
                colvarResponseTime.IsNullable = true;
                colvarResponseTime.IsPrimaryKey = false;
                colvarResponseTime.IsForeignKey = false;
                colvarResponseTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarResponseTime);
                
                TableSchema.TableColumn colvarProductGuid = new TableSchema.TableColumn(schema);
                colvarProductGuid.ColumnName = "product_guid";
                colvarProductGuid.DataType = DbType.Int32;
                colvarProductGuid.MaxLength = 0;
                colvarProductGuid.AutoIncrement = false;
                colvarProductGuid.IsNullable = true;
                colvarProductGuid.IsPrimaryKey = false;
                colvarProductGuid.IsForeignKey = false;
                colvarProductGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarProductGuid);
                
                TableSchema.TableColumn colvarStoreGuid = new TableSchema.TableColumn(schema);
                colvarStoreGuid.ColumnName = "store_guid";
                colvarStoreGuid.DataType = DbType.Int32;
                colvarStoreGuid.MaxLength = 0;
                colvarStoreGuid.AutoIncrement = false;
                colvarStoreGuid.IsNullable = true;
                colvarStoreGuid.IsPrimaryKey = false;
                colvarStoreGuid.IsForeignKey = false;
                colvarStoreGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarStoreGuid);
                
                TableSchema.TableColumn colvarRemittanceType = new TableSchema.TableColumn(schema);
                colvarRemittanceType.ColumnName = "remittance_type";
                colvarRemittanceType.DataType = DbType.Int32;
                colvarRemittanceType.MaxLength = 0;
                colvarRemittanceType.AutoIncrement = false;
                colvarRemittanceType.IsNullable = true;
                colvarRemittanceType.IsPrimaryKey = false;
                colvarRemittanceType.IsForeignKey = false;
                colvarRemittanceType.IsReadOnly = false;
                
                schema.Columns.Add(colvarRemittanceType);
                
                TableSchema.TableColumn colvarUniqueId = new TableSchema.TableColumn(schema);
                colvarUniqueId.ColumnName = "unique_id";
                colvarUniqueId.DataType = DbType.Int32;
                colvarUniqueId.MaxLength = 0;
                colvarUniqueId.AutoIncrement = false;
                colvarUniqueId.IsNullable = true;
                colvarUniqueId.IsPrimaryKey = false;
                colvarUniqueId.IsForeignKey = false;
                colvarUniqueId.IsReadOnly = false;
                
                schema.Columns.Add(colvarUniqueId);
                
                TableSchema.TableColumn colvarHiDealDealId = new TableSchema.TableColumn(schema);
                colvarHiDealDealId.ColumnName = "hi_deal_deal_id";
                colvarHiDealDealId.DataType = DbType.Int32;
                colvarHiDealDealId.MaxLength = 0;
                colvarHiDealDealId.AutoIncrement = false;
                colvarHiDealDealId.IsNullable = false;
                colvarHiDealDealId.IsPrimaryKey = false;
                colvarHiDealDealId.IsForeignKey = false;
                colvarHiDealDealId.IsReadOnly = false;
                
                schema.Columns.Add(colvarHiDealDealId);
                
                TableSchema.TableColumn colvarGenerationFrequency = new TableSchema.TableColumn(schema);
                colvarGenerationFrequency.ColumnName = "generation_frequency";
                colvarGenerationFrequency.DataType = DbType.Int32;
                colvarGenerationFrequency.MaxLength = 0;
                colvarGenerationFrequency.AutoIncrement = false;
                colvarGenerationFrequency.IsNullable = false;
                colvarGenerationFrequency.IsPrimaryKey = false;
                colvarGenerationFrequency.IsForeignKey = false;
                colvarGenerationFrequency.IsReadOnly = false;
                
                schema.Columns.Add(colvarGenerationFrequency);
                
                TableSchema.TableColumn colvarDeEmpName = new TableSchema.TableColumn(schema);
                colvarDeEmpName.ColumnName = "de_emp_name";
                colvarDeEmpName.DataType = DbType.AnsiString;
                colvarDeEmpName.MaxLength = 1;
                colvarDeEmpName.AutoIncrement = false;
                colvarDeEmpName.IsNullable = false;
                colvarDeEmpName.IsPrimaryKey = false;
                colvarDeEmpName.IsForeignKey = false;
                colvarDeEmpName.IsReadOnly = false;
                
                schema.Columns.Add(colvarDeEmpName);
                
                TableSchema.TableColumn colvarOpEmpName = new TableSchema.TableColumn(schema);
                colvarOpEmpName.ColumnName = "op_emp_name";
                colvarOpEmpName.DataType = DbType.AnsiString;
                colvarOpEmpName.MaxLength = 1;
                colvarOpEmpName.AutoIncrement = false;
                colvarOpEmpName.IsNullable = false;
                colvarOpEmpName.IsPrimaryKey = false;
                colvarOpEmpName.IsForeignKey = false;
                colvarOpEmpName.IsReadOnly = false;
                
                schema.Columns.Add(colvarOpEmpName);
                
                TableSchema.TableColumn colvarDeptId = new TableSchema.TableColumn(schema);
                colvarDeptId.ColumnName = "dept_id";
                colvarDeptId.DataType = DbType.Int32;
                colvarDeptId.MaxLength = 0;
                colvarDeptId.AutoIncrement = false;
                colvarDeptId.IsNullable = true;
                colvarDeptId.IsPrimaryKey = false;
                colvarDeptId.IsForeignKey = false;
                colvarDeptId.IsReadOnly = false;
                
                schema.Columns.Add(colvarDeptId);
                
                TableSchema.TableColumn colvarStoreName = new TableSchema.TableColumn(schema);
                colvarStoreName.ColumnName = "store_name";
                colvarStoreName.DataType = DbType.Int32;
                colvarStoreName.MaxLength = 0;
                colvarStoreName.AutoIncrement = false;
                colvarStoreName.IsNullable = true;
                colvarStoreName.IsPrimaryKey = false;
                colvarStoreName.IsForeignKey = false;
                colvarStoreName.IsReadOnly = false;
                
                schema.Columns.Add(colvarStoreName);
                
                TableSchema.TableColumn colvarSellerGuid = new TableSchema.TableColumn(schema);
                colvarSellerGuid.ColumnName = "seller_guid";
                colvarSellerGuid.DataType = DbType.Guid;
                colvarSellerGuid.MaxLength = 0;
                colvarSellerGuid.AutoIncrement = false;
                colvarSellerGuid.IsNullable = false;
                colvarSellerGuid.IsPrimaryKey = false;
                colvarSellerGuid.IsForeignKey = false;
                colvarSellerGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerGuid);
                
                TableSchema.TableColumn colvarSellerName = new TableSchema.TableColumn(schema);
                colvarSellerName.ColumnName = "seller_name";
                colvarSellerName.DataType = DbType.String;
                colvarSellerName.MaxLength = 100;
                colvarSellerName.AutoIncrement = false;
                colvarSellerName.IsNullable = false;
                colvarSellerName.IsPrimaryKey = false;
                colvarSellerName.IsForeignKey = false;
                colvarSellerName.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerName);
                
                TableSchema.TableColumn colvarIsConfirmedReadyToPay = new TableSchema.TableColumn(schema);
                colvarIsConfirmedReadyToPay.ColumnName = "is_confirmed_ready_to_pay";
                colvarIsConfirmedReadyToPay.DataType = DbType.Boolean;
                colvarIsConfirmedReadyToPay.MaxLength = 0;
                colvarIsConfirmedReadyToPay.AutoIncrement = false;
                colvarIsConfirmedReadyToPay.IsNullable = false;
                colvarIsConfirmedReadyToPay.IsPrimaryKey = false;
                colvarIsConfirmedReadyToPay.IsForeignKey = false;
                colvarIsConfirmedReadyToPay.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsConfirmedReadyToPay);
                
                TableSchema.TableColumn colvarFinanceGetDate = new TableSchema.TableColumn(schema);
                colvarFinanceGetDate.ColumnName = "finance_get_date";
                colvarFinanceGetDate.DataType = DbType.DateTime;
                colvarFinanceGetDate.MaxLength = 0;
                colvarFinanceGetDate.AutoIncrement = false;
                colvarFinanceGetDate.IsNullable = true;
                colvarFinanceGetDate.IsPrimaryKey = false;
                colvarFinanceGetDate.IsForeignKey = false;
                colvarFinanceGetDate.IsReadOnly = false;
                
                schema.Columns.Add(colvarFinanceGetDate);
                
                TableSchema.TableColumn colvarMessage = new TableSchema.TableColumn(schema);
                colvarMessage.ColumnName = "message";
                colvarMessage.DataType = DbType.AnsiString;
                colvarMessage.MaxLength = 1;
                colvarMessage.AutoIncrement = false;
                colvarMessage.IsNullable = false;
                colvarMessage.IsPrimaryKey = false;
                colvarMessage.IsForeignKey = false;
                colvarMessage.IsReadOnly = false;
                
                schema.Columns.Add(colvarMessage);
                
                TableSchema.TableColumn colvarTotalSum = new TableSchema.TableColumn(schema);
                colvarTotalSum.ColumnName = "total_sum";
                colvarTotalSum.DataType = DbType.Decimal;
                colvarTotalSum.MaxLength = 0;
                colvarTotalSum.AutoIncrement = false;
                colvarTotalSum.IsNullable = true;
                colvarTotalSum.IsPrimaryKey = false;
                colvarTotalSum.IsForeignKey = false;
                colvarTotalSum.IsReadOnly = false;
                
                schema.Columns.Add(colvarTotalSum);
                
                TableSchema.TableColumn colvarBillSum = new TableSchema.TableColumn(schema);
                colvarBillSum.ColumnName = "bill_sum";
                colvarBillSum.DataType = DbType.Int32;
                colvarBillSum.MaxLength = 0;
                colvarBillSum.AutoIncrement = false;
                colvarBillSum.IsNullable = true;
                colvarBillSum.IsPrimaryKey = false;
                colvarBillSum.IsForeignKey = false;
                colvarBillSum.IsReadOnly = false;
                
                schema.Columns.Add(colvarBillSum);
                
                TableSchema.TableColumn colvarBillSumNotaxed = new TableSchema.TableColumn(schema);
                colvarBillSumNotaxed.ColumnName = "bill_sum_notaxed";
                colvarBillSumNotaxed.DataType = DbType.Int32;
                colvarBillSumNotaxed.MaxLength = 0;
                colvarBillSumNotaxed.AutoIncrement = false;
                colvarBillSumNotaxed.IsNullable = true;
                colvarBillSumNotaxed.IsPrimaryKey = false;
                colvarBillSumNotaxed.IsForeignKey = false;
                colvarBillSumNotaxed.IsReadOnly = false;
                
                schema.Columns.Add(colvarBillSumNotaxed);
                
                TableSchema.TableColumn colvarBlTax = new TableSchema.TableColumn(schema);
                colvarBlTax.ColumnName = "bl_tax";
                colvarBlTax.DataType = DbType.Int32;
                colvarBlTax.MaxLength = 0;
                colvarBlTax.AutoIncrement = false;
                colvarBlTax.IsNullable = true;
                colvarBlTax.IsPrimaryKey = false;
                colvarBlTax.IsForeignKey = false;
                colvarBlTax.IsReadOnly = false;
                
                schema.Columns.Add(colvarBlTax);
                
                TableSchema.TableColumn colvarBlRemark = new TableSchema.TableColumn(schema);
                colvarBlRemark.ColumnName = "bl_remark";
                colvarBlRemark.DataType = DbType.String;
                colvarBlRemark.MaxLength = 250;
                colvarBlRemark.AutoIncrement = false;
                colvarBlRemark.IsNullable = true;
                colvarBlRemark.IsPrimaryKey = false;
                colvarBlRemark.IsForeignKey = false;
                colvarBlRemark.IsReadOnly = false;
                
                schema.Columns.Add(colvarBlRemark);
                
                TableSchema.TableColumn colvarInvoiceComId = new TableSchema.TableColumn(schema);
                colvarInvoiceComId.ColumnName = "invoice_com_id";
                colvarInvoiceComId.DataType = DbType.String;
                colvarInvoiceComId.MaxLength = 10;
                colvarInvoiceComId.AutoIncrement = false;
                colvarInvoiceComId.IsNullable = true;
                colvarInvoiceComId.IsPrimaryKey = false;
                colvarInvoiceComId.IsForeignKey = false;
                colvarInvoiceComId.IsReadOnly = false;
                
                schema.Columns.Add(colvarInvoiceComId);
                
                TableSchema.TableColumn colvarIntervalStart = new TableSchema.TableColumn(schema);
                colvarIntervalStart.ColumnName = "interval_start";
                colvarIntervalStart.DataType = DbType.DateTime;
                colvarIntervalStart.MaxLength = 0;
                colvarIntervalStart.AutoIncrement = false;
                colvarIntervalStart.IsNullable = false;
                colvarIntervalStart.IsPrimaryKey = false;
                colvarIntervalStart.IsForeignKey = false;
                colvarIntervalStart.IsReadOnly = false;
                
                schema.Columns.Add(colvarIntervalStart);
                
                TableSchema.TableColumn colvarIntervalEnd = new TableSchema.TableColumn(schema);
                colvarIntervalEnd.ColumnName = "interval_end";
                colvarIntervalEnd.DataType = DbType.DateTime;
                colvarIntervalEnd.MaxLength = 0;
                colvarIntervalEnd.AutoIncrement = false;
                colvarIntervalEnd.IsNullable = false;
                colvarIntervalEnd.IsPrimaryKey = false;
                colvarIntervalEnd.IsForeignKey = false;
                colvarIntervalEnd.IsReadOnly = false;
                
                schema.Columns.Add(colvarIntervalEnd);
                
                TableSchema.TableColumn colvarProductType = new TableSchema.TableColumn(schema);
                colvarProductType.ColumnName = "product_type";
                colvarProductType.DataType = DbType.Int32;
                colvarProductType.MaxLength = 0;
                colvarProductType.AutoIncrement = false;
                colvarProductType.IsNullable = true;
                colvarProductType.IsPrimaryKey = false;
                colvarProductType.IsForeignKey = false;
                colvarProductType.IsReadOnly = false;
                
                schema.Columns.Add(colvarProductType);
                
                TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
                colvarCreateId.ColumnName = "create_id";
                colvarCreateId.DataType = DbType.String;
                colvarCreateId.MaxLength = 256;
                colvarCreateId.AutoIncrement = false;
                colvarCreateId.IsNullable = true;
                colvarCreateId.IsPrimaryKey = false;
                colvarCreateId.IsForeignKey = false;
                colvarCreateId.IsReadOnly = false;
                
                schema.Columns.Add(colvarCreateId);
                
                TableSchema.TableColumn colvarBalanceSheetType = new TableSchema.TableColumn(schema);
                colvarBalanceSheetType.ColumnName = "balance_sheet_type";
                colvarBalanceSheetType.DataType = DbType.Int32;
                colvarBalanceSheetType.MaxLength = 0;
                colvarBalanceSheetType.AutoIncrement = false;
                colvarBalanceSheetType.IsNullable = false;
                colvarBalanceSheetType.IsPrimaryKey = false;
                colvarBalanceSheetType.IsForeignKey = false;
                colvarBalanceSheetType.IsReadOnly = false;
                
                schema.Columns.Add(colvarBalanceSheetType);
                
                TableSchema.TableColumn colvarDeliveryType = new TableSchema.TableColumn(schema);
                colvarDeliveryType.ColumnName = "delivery_type";
                colvarDeliveryType.DataType = DbType.Int32;
                colvarDeliveryType.MaxLength = 0;
                colvarDeliveryType.AutoIncrement = false;
                colvarDeliveryType.IsNullable = false;
                colvarDeliveryType.IsPrimaryKey = false;
                colvarDeliveryType.IsForeignKey = false;
                colvarDeliveryType.IsReadOnly = false;
                
                schema.Columns.Add(colvarDeliveryType);
                
                TableSchema.TableColumn colvarDefaultPaymentTime = new TableSchema.TableColumn(schema);
                colvarDefaultPaymentTime.ColumnName = "default_payment_time";
                colvarDefaultPaymentTime.DataType = DbType.DateTime;
                colvarDefaultPaymentTime.MaxLength = 0;
                colvarDefaultPaymentTime.AutoIncrement = false;
                colvarDefaultPaymentTime.IsNullable = true;
                colvarDefaultPaymentTime.IsPrimaryKey = false;
                colvarDefaultPaymentTime.IsForeignKey = false;
                colvarDefaultPaymentTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarDefaultPaymentTime);
                
                TableSchema.TableColumn colvarEstAmount = new TableSchema.TableColumn(schema);
                colvarEstAmount.ColumnName = "est_amount";
                colvarEstAmount.DataType = DbType.Int32;
                colvarEstAmount.MaxLength = 0;
                colvarEstAmount.AutoIncrement = false;
                colvarEstAmount.IsNullable = false;
                colvarEstAmount.IsPrimaryKey = false;
                colvarEstAmount.IsForeignKey = false;
                colvarEstAmount.IsReadOnly = false;
                
                schema.Columns.Add(colvarEstAmount);
                
                TableSchema.TableColumn colvarIspFamilyAmount = new TableSchema.TableColumn(schema);
                colvarIspFamilyAmount.ColumnName = "isp_family_amount";
                colvarIspFamilyAmount.DataType = DbType.Int32;
                colvarIspFamilyAmount.MaxLength = 0;
                colvarIspFamilyAmount.AutoIncrement = false;
                colvarIspFamilyAmount.IsNullable = false;
                colvarIspFamilyAmount.IsPrimaryKey = false;
                colvarIspFamilyAmount.IsForeignKey = false;
                colvarIspFamilyAmount.IsReadOnly = false;
                
                schema.Columns.Add(colvarIspFamilyAmount);
                
                TableSchema.TableColumn colvarIspSevenAmount = new TableSchema.TableColumn(schema);
                colvarIspSevenAmount.ColumnName = "isp_seven_amount";
                colvarIspSevenAmount.DataType = DbType.Int32;
                colvarIspSevenAmount.MaxLength = 0;
                colvarIspSevenAmount.AutoIncrement = false;
                colvarIspSevenAmount.IsNullable = false;
                colvarIspSevenAmount.IsPrimaryKey = false;
                colvarIspSevenAmount.IsForeignKey = false;
                colvarIspSevenAmount.IsReadOnly = false;
                
                schema.Columns.Add(colvarIspSevenAmount);
                
                TableSchema.TableColumn colvarVendorInvoiceNumber = new TableSchema.TableColumn(schema);
                colvarVendorInvoiceNumber.ColumnName = "vendor_invoice_number";
                colvarVendorInvoiceNumber.DataType = DbType.String;
                colvarVendorInvoiceNumber.MaxLength = 10;
                colvarVendorInvoiceNumber.AutoIncrement = false;
                colvarVendorInvoiceNumber.IsNullable = true;
                colvarVendorInvoiceNumber.IsPrimaryKey = false;
                colvarVendorInvoiceNumber.IsForeignKey = false;
                colvarVendorInvoiceNumber.IsReadOnly = false;
                
                schema.Columns.Add(colvarVendorInvoiceNumber);
                
                TableSchema.TableColumn colvarVendorInvoiceNumberTime = new TableSchema.TableColumn(schema);
                colvarVendorInvoiceNumberTime.ColumnName = "vendor_invoice_number_time";
                colvarVendorInvoiceNumberTime.DataType = DbType.DateTime;
                colvarVendorInvoiceNumberTime.MaxLength = 0;
                colvarVendorInvoiceNumberTime.AutoIncrement = false;
                colvarVendorInvoiceNumberTime.IsNullable = true;
                colvarVendorInvoiceNumberTime.IsPrimaryKey = false;
                colvarVendorInvoiceNumberTime.IsForeignKey = false;
                colvarVendorInvoiceNumberTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarVendorInvoiceNumberTime);
                
                TableSchema.TableColumn colvarVendorEinvoiceId = new TableSchema.TableColumn(schema);
                colvarVendorEinvoiceId.ColumnName = "vendor_einvoice_id";
                colvarVendorEinvoiceId.DataType = DbType.Int32;
                colvarVendorEinvoiceId.MaxLength = 0;
                colvarVendorEinvoiceId.AutoIncrement = false;
                colvarVendorEinvoiceId.IsNullable = true;
                colvarVendorEinvoiceId.IsPrimaryKey = false;
                colvarVendorEinvoiceId.IsForeignKey = false;
                colvarVendorEinvoiceId.IsReadOnly = false;
                
                schema.Columns.Add(colvarVendorEinvoiceId);
                
                TableSchema.TableColumn colvarOverdueAmount = new TableSchema.TableColumn(schema);
                colvarOverdueAmount.ColumnName = "overdue_amount";
                colvarOverdueAmount.DataType = DbType.Int32;
                colvarOverdueAmount.MaxLength = 0;
                colvarOverdueAmount.AutoIncrement = false;
                colvarOverdueAmount.IsNullable = false;
                colvarOverdueAmount.IsPrimaryKey = false;
                colvarOverdueAmount.IsForeignKey = false;
                colvarOverdueAmount.IsReadOnly = false;
                
                schema.Columns.Add(colvarOverdueAmount);
                
                TableSchema.TableColumn colvarWmsAmount = new TableSchema.TableColumn(schema);
                colvarWmsAmount.ColumnName = "wms_amount";
                colvarWmsAmount.DataType = DbType.Int32;
                colvarWmsAmount.MaxLength = 0;
                colvarWmsAmount.AutoIncrement = false;
                colvarWmsAmount.IsNullable = false;
                colvarWmsAmount.IsPrimaryKey = false;
                colvarWmsAmount.IsForeignKey = false;
                colvarWmsAmount.IsReadOnly = false;
                
                schema.Columns.Add(colvarWmsAmount);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_balance_sheet_wms_bill_list",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewBalanceSheetWmsBillList()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewBalanceSheetWmsBillList(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewBalanceSheetWmsBillList(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewBalanceSheetWmsBillList(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id 
	    {
		    get
		    {
			    return GetColumnValue<int>("id");
		    }
            set 
		    {
			    SetColumnValue("id", value);
            }
        }
	      
        [XmlAttribute("Year")]
        [Bindable(true)]
        public int Year 
	    {
		    get
		    {
			    return GetColumnValue<int>("year");
		    }
            set 
		    {
			    SetColumnValue("year", value);
            }
        }
	      
        [XmlAttribute("Month")]
        [Bindable(true)]
        public int Month 
	    {
		    get
		    {
			    return GetColumnValue<int>("month");
		    }
            set 
		    {
			    SetColumnValue("month", value);
            }
        }
	      
        [XmlAttribute("Name")]
        [Bindable(true)]
        public string Name 
	    {
		    get
		    {
			    return GetColumnValue<string>("name");
		    }
            set 
		    {
			    SetColumnValue("name", value);
            }
        }
	      
        [XmlAttribute("BillId")]
        [Bindable(true)]
        public int? BillId 
	    {
		    get
		    {
			    return GetColumnValue<int?>("bill_id");
		    }
            set 
		    {
			    SetColumnValue("bill_id", value);
            }
        }
	      
        [XmlAttribute("BillNumber")]
        [Bindable(true)]
        public string BillNumber 
	    {
		    get
		    {
			    return GetColumnValue<string>("bill_number");
		    }
            set 
		    {
			    SetColumnValue("bill_number", value);
            }
        }
	      
        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime? CreateTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("create_time");
		    }
            set 
		    {
			    SetColumnValue("create_time", value);
            }
        }
	      
        [XmlAttribute("InvoiceDate")]
        [Bindable(true)]
        public DateTime? InvoiceDate 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("invoice_date");
		    }
            set 
		    {
			    SetColumnValue("invoice_date", value);
            }
        }
	      
        [XmlAttribute("BuyerType")]
        [Bindable(true)]
        public int? BuyerType 
	    {
		    get
		    {
			    return GetColumnValue<int?>("buyer_type");
		    }
            set 
		    {
			    SetColumnValue("buyer_type", value);
            }
        }
	      
        [XmlAttribute("BillSentDate")]
        [Bindable(true)]
        public DateTime? BillSentDate 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("bill_sent_date");
		    }
            set 
		    {
			    SetColumnValue("bill_sent_date", value);
            }
        }
	      
        [XmlAttribute("BillMoney")]
        [Bindable(true)]
        public int? BillMoney 
	    {
		    get
		    {
			    return GetColumnValue<int?>("bill_money");
		    }
            set 
		    {
			    SetColumnValue("bill_money", value);
            }
        }
	      
        [XmlAttribute("BillMoneyNotaxed")]
        [Bindable(true)]
        public int? BillMoneyNotaxed 
	    {
		    get
		    {
			    return GetColumnValue<int?>("bill_money_notaxed");
		    }
            set 
		    {
			    SetColumnValue("bill_money_notaxed", value);
            }
        }
	      
        [XmlAttribute("BillTax")]
        [Bindable(true)]
        public int? BillTax 
	    {
		    get
		    {
			    return GetColumnValue<int?>("bill_tax");
		    }
            set 
		    {
			    SetColumnValue("bill_tax", value);
            }
        }
	      
        [XmlAttribute("Remark")]
        [Bindable(true)]
        public string Remark 
	    {
		    get
		    {
			    return GetColumnValue<string>("remark");
		    }
            set 
		    {
			    SetColumnValue("remark", value);
            }
        }
	      
        [XmlAttribute("DealStartTime")]
        [Bindable(true)]
        public int? DealStartTime 
	    {
		    get
		    {
			    return GetColumnValue<int?>("deal_start_time");
		    }
            set 
		    {
			    SetColumnValue("deal_start_time", value);
            }
        }
	      
        [XmlAttribute("DealEndTime")]
        [Bindable(true)]
        public int? DealEndTime 
	    {
		    get
		    {
			    return GetColumnValue<int?>("deal_end_time");
		    }
            set 
		    {
			    SetColumnValue("deal_end_time", value);
            }
        }
	      
        [XmlAttribute("UseStartTime")]
        [Bindable(true)]
        public int? UseStartTime 
	    {
		    get
		    {
			    return GetColumnValue<int?>("use_start_time");
		    }
            set 
		    {
			    SetColumnValue("use_start_time", value);
            }
        }
	      
        [XmlAttribute("UseEndTime")]
        [Bindable(true)]
        public int? UseEndTime 
	    {
		    get
		    {
			    return GetColumnValue<int?>("use_end_time");
		    }
            set 
		    {
			    SetColumnValue("use_end_time", value);
            }
        }
	      
        [XmlAttribute("VendorReceiptType")]
        [Bindable(true)]
        public int? VendorReceiptType 
	    {
		    get
		    {
			    return GetColumnValue<int?>("vendor_receipt_type");
		    }
            set 
		    {
			    SetColumnValue("vendor_receipt_type", value);
            }
        }
	      
        [XmlAttribute("CompanyAccountName")]
        [Bindable(true)]
        public string CompanyAccountName 
	    {
		    get
		    {
			    return GetColumnValue<string>("CompanyAccountName");
		    }
            set 
		    {
			    SetColumnValue("CompanyAccountName", value);
            }
        }
	      
        [XmlAttribute("CompanyName")]
        [Bindable(true)]
        public string CompanyName 
	    {
		    get
		    {
			    return GetColumnValue<string>("CompanyName");
		    }
            set 
		    {
			    SetColumnValue("CompanyName", value);
            }
        }
	      
        [XmlAttribute("CompanyAccount")]
        [Bindable(true)]
        public string CompanyAccount 
	    {
		    get
		    {
			    return GetColumnValue<string>("CompanyAccount");
		    }
            set 
		    {
			    SetColumnValue("CompanyAccount", value);
            }
        }
	      
        [XmlAttribute("CompanyBankCode")]
        [Bindable(true)]
        public string CompanyBankCode 
	    {
		    get
		    {
			    return GetColumnValue<string>("CompanyBankCode");
		    }
            set 
		    {
			    SetColumnValue("CompanyBankCode", value);
            }
        }
	      
        [XmlAttribute("CompanyBranchCode")]
        [Bindable(true)]
        public string CompanyBranchCode 
	    {
		    get
		    {
			    return GetColumnValue<string>("CompanyBranchCode");
		    }
            set 
		    {
			    SetColumnValue("CompanyBranchCode", value);
            }
        }
	      
        [XmlAttribute("CompanyID")]
        [Bindable(true)]
        public string CompanyID 
	    {
		    get
		    {
			    return GetColumnValue<string>("CompanyID");
		    }
            set 
		    {
			    SetColumnValue("CompanyID", value);
            }
        }
	      
        [XmlAttribute("SignCompanyID")]
        [Bindable(true)]
        public string SignCompanyID 
	    {
		    get
		    {
			    return GetColumnValue<string>("SignCompanyID");
		    }
            set 
		    {
			    SetColumnValue("SignCompanyID", value);
            }
        }
	      
        [XmlAttribute("CompanyBossName")]
        [Bindable(true)]
        public string CompanyBossName 
	    {
		    get
		    {
			    return GetColumnValue<string>("CompanyBossName");
		    }
            set 
		    {
			    SetColumnValue("CompanyBossName", value);
            }
        }
	      
        [XmlAttribute("AccountantName")]
        [Bindable(true)]
        public string AccountantName 
	    {
		    get
		    {
			    return GetColumnValue<string>("accountant_name");
		    }
            set 
		    {
			    SetColumnValue("accountant_name", value);
            }
        }
	      
        [XmlAttribute("AccountantTel")]
        [Bindable(true)]
        public string AccountantTel 
	    {
		    get
		    {
			    return GetColumnValue<string>("accountant_tel");
		    }
            set 
		    {
			    SetColumnValue("accountant_tel", value);
            }
        }
	      
        [XmlAttribute("AccountantEmail")]
        [Bindable(true)]
        public string AccountantEmail 
	    {
		    get
		    {
			    return GetColumnValue<string>("accountant_email");
		    }
            set 
		    {
			    SetColumnValue("accountant_email", value);
            }
        }
	      
        [XmlAttribute("IsTransferComplete")]
        [Bindable(true)]
        public bool? IsTransferComplete 
	    {
		    get
		    {
			    return GetColumnValue<bool?>("is_transfer_complete");
		    }
            set 
		    {
			    SetColumnValue("is_transfer_complete", value);
            }
        }
	      
        [XmlAttribute("ResponseTime")]
        [Bindable(true)]
        public DateTime? ResponseTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("response_time");
		    }
            set 
		    {
			    SetColumnValue("response_time", value);
            }
        }
	      
        [XmlAttribute("ProductGuid")]
        [Bindable(true)]
        public int? ProductGuid 
	    {
		    get
		    {
			    return GetColumnValue<int?>("product_guid");
		    }
            set 
		    {
			    SetColumnValue("product_guid", value);
            }
        }
	      
        [XmlAttribute("StoreGuid")]
        [Bindable(true)]
        public int? StoreGuid 
	    {
		    get
		    {
			    return GetColumnValue<int?>("store_guid");
		    }
            set 
		    {
			    SetColumnValue("store_guid", value);
            }
        }
	      
        [XmlAttribute("RemittanceType")]
        [Bindable(true)]
        public int? RemittanceType 
	    {
		    get
		    {
			    return GetColumnValue<int?>("remittance_type");
		    }
            set 
		    {
			    SetColumnValue("remittance_type", value);
            }
        }
	      
        [XmlAttribute("UniqueId")]
        [Bindable(true)]
        public int? UniqueId 
	    {
		    get
		    {
			    return GetColumnValue<int?>("unique_id");
		    }
            set 
		    {
			    SetColumnValue("unique_id", value);
            }
        }
	      
        [XmlAttribute("HiDealDealId")]
        [Bindable(true)]
        public int HiDealDealId 
	    {
		    get
		    {
			    return GetColumnValue<int>("hi_deal_deal_id");
		    }
            set 
		    {
			    SetColumnValue("hi_deal_deal_id", value);
            }
        }
	      
        [XmlAttribute("GenerationFrequency")]
        [Bindable(true)]
        public int GenerationFrequency 
	    {
		    get
		    {
			    return GetColumnValue<int>("generation_frequency");
		    }
            set 
		    {
			    SetColumnValue("generation_frequency", value);
            }
        }
	      
        [XmlAttribute("DeEmpName")]
        [Bindable(true)]
        public string DeEmpName 
	    {
		    get
		    {
			    return GetColumnValue<string>("de_emp_name");
		    }
            set 
		    {
			    SetColumnValue("de_emp_name", value);
            }
        }
	      
        [XmlAttribute("OpEmpName")]
        [Bindable(true)]
        public string OpEmpName 
	    {
		    get
		    {
			    return GetColumnValue<string>("op_emp_name");
		    }
            set 
		    {
			    SetColumnValue("op_emp_name", value);
            }
        }
	      
        [XmlAttribute("DeptId")]
        [Bindable(true)]
        public int? DeptId 
	    {
		    get
		    {
			    return GetColumnValue<int?>("dept_id");
		    }
            set 
		    {
			    SetColumnValue("dept_id", value);
            }
        }
	      
        [XmlAttribute("StoreName")]
        [Bindable(true)]
        public int? StoreName 
	    {
		    get
		    {
			    return GetColumnValue<int?>("store_name");
		    }
            set 
		    {
			    SetColumnValue("store_name", value);
            }
        }
	      
        [XmlAttribute("SellerGuid")]
        [Bindable(true)]
        public Guid SellerGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("seller_guid");
		    }
            set 
		    {
			    SetColumnValue("seller_guid", value);
            }
        }
	      
        [XmlAttribute("SellerName")]
        [Bindable(true)]
        public string SellerName 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_name");
		    }
            set 
		    {
			    SetColumnValue("seller_name", value);
            }
        }
	      
        [XmlAttribute("IsConfirmedReadyToPay")]
        [Bindable(true)]
        public bool IsConfirmedReadyToPay 
	    {
		    get
		    {
			    return GetColumnValue<bool>("is_confirmed_ready_to_pay");
		    }
            set 
		    {
			    SetColumnValue("is_confirmed_ready_to_pay", value);
            }
        }
	      
        [XmlAttribute("FinanceGetDate")]
        [Bindable(true)]
        public DateTime? FinanceGetDate 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("finance_get_date");
		    }
            set 
		    {
			    SetColumnValue("finance_get_date", value);
            }
        }
	      
        [XmlAttribute("Message")]
        [Bindable(true)]
        public string Message 
	    {
		    get
		    {
			    return GetColumnValue<string>("message");
		    }
            set 
		    {
			    SetColumnValue("message", value);
            }
        }
	      
        [XmlAttribute("TotalSum")]
        [Bindable(true)]
        public decimal? TotalSum 
	    {
		    get
		    {
			    return GetColumnValue<decimal?>("total_sum");
		    }
            set 
		    {
			    SetColumnValue("total_sum", value);
            }
        }
	      
        [XmlAttribute("BillSum")]
        [Bindable(true)]
        public int? BillSum 
	    {
		    get
		    {
			    return GetColumnValue<int?>("bill_sum");
		    }
            set 
		    {
			    SetColumnValue("bill_sum", value);
            }
        }
	      
        [XmlAttribute("BillSumNotaxed")]
        [Bindable(true)]
        public int? BillSumNotaxed 
	    {
		    get
		    {
			    return GetColumnValue<int?>("bill_sum_notaxed");
		    }
            set 
		    {
			    SetColumnValue("bill_sum_notaxed", value);
            }
        }
	      
        [XmlAttribute("BlTax")]
        [Bindable(true)]
        public int? BlTax 
	    {
		    get
		    {
			    return GetColumnValue<int?>("bl_tax");
		    }
            set 
		    {
			    SetColumnValue("bl_tax", value);
            }
        }
	      
        [XmlAttribute("BlRemark")]
        [Bindable(true)]
        public string BlRemark 
	    {
		    get
		    {
			    return GetColumnValue<string>("bl_remark");
		    }
            set 
		    {
			    SetColumnValue("bl_remark", value);
            }
        }
	      
        [XmlAttribute("InvoiceComId")]
        [Bindable(true)]
        public string InvoiceComId 
	    {
		    get
		    {
			    return GetColumnValue<string>("invoice_com_id");
		    }
            set 
		    {
			    SetColumnValue("invoice_com_id", value);
            }
        }
	      
        [XmlAttribute("IntervalStart")]
        [Bindable(true)]
        public DateTime IntervalStart 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("interval_start");
		    }
            set 
		    {
			    SetColumnValue("interval_start", value);
            }
        }
	      
        [XmlAttribute("IntervalEnd")]
        [Bindable(true)]
        public DateTime IntervalEnd 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("interval_end");
		    }
            set 
		    {
			    SetColumnValue("interval_end", value);
            }
        }
	      
        [XmlAttribute("ProductType")]
        [Bindable(true)]
        public int? ProductType 
	    {
		    get
		    {
			    return GetColumnValue<int?>("product_type");
		    }
            set 
		    {
			    SetColumnValue("product_type", value);
            }
        }
	      
        [XmlAttribute("CreateId")]
        [Bindable(true)]
        public string CreateId 
	    {
		    get
		    {
			    return GetColumnValue<string>("create_id");
		    }
            set 
		    {
			    SetColumnValue("create_id", value);
            }
        }
	      
        [XmlAttribute("BalanceSheetType")]
        [Bindable(true)]
        public int BalanceSheetType 
	    {
		    get
		    {
			    return GetColumnValue<int>("balance_sheet_type");
		    }
            set 
		    {
			    SetColumnValue("balance_sheet_type", value);
            }
        }
	      
        [XmlAttribute("DeliveryType")]
        [Bindable(true)]
        public int DeliveryType 
	    {
		    get
		    {
			    return GetColumnValue<int>("delivery_type");
		    }
            set 
		    {
			    SetColumnValue("delivery_type", value);
            }
        }
	      
        [XmlAttribute("DefaultPaymentTime")]
        [Bindable(true)]
        public DateTime? DefaultPaymentTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("default_payment_time");
		    }
            set 
		    {
			    SetColumnValue("default_payment_time", value);
            }
        }
	      
        [XmlAttribute("EstAmount")]
        [Bindable(true)]
        public int EstAmount 
	    {
		    get
		    {
			    return GetColumnValue<int>("est_amount");
		    }
            set 
		    {
			    SetColumnValue("est_amount", value);
            }
        }
	      
        [XmlAttribute("IspFamilyAmount")]
        [Bindable(true)]
        public int IspFamilyAmount 
	    {
		    get
		    {
			    return GetColumnValue<int>("isp_family_amount");
		    }
            set 
		    {
			    SetColumnValue("isp_family_amount", value);
            }
        }
	      
        [XmlAttribute("IspSevenAmount")]
        [Bindable(true)]
        public int IspSevenAmount 
	    {
		    get
		    {
			    return GetColumnValue<int>("isp_seven_amount");
		    }
            set 
		    {
			    SetColumnValue("isp_seven_amount", value);
            }
        }
	      
        [XmlAttribute("VendorInvoiceNumber")]
        [Bindable(true)]
        public string VendorInvoiceNumber 
	    {
		    get
		    {
			    return GetColumnValue<string>("vendor_invoice_number");
		    }
            set 
		    {
			    SetColumnValue("vendor_invoice_number", value);
            }
        }
	      
        [XmlAttribute("VendorInvoiceNumberTime")]
        [Bindable(true)]
        public DateTime? VendorInvoiceNumberTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("vendor_invoice_number_time");
		    }
            set 
		    {
			    SetColumnValue("vendor_invoice_number_time", value);
            }
        }
	      
        [XmlAttribute("VendorEinvoiceId")]
        [Bindable(true)]
        public int? VendorEinvoiceId 
	    {
		    get
		    {
			    return GetColumnValue<int?>("vendor_einvoice_id");
		    }
            set 
		    {
			    SetColumnValue("vendor_einvoice_id", value);
            }
        }
	      
        [XmlAttribute("OverdueAmount")]
        [Bindable(true)]
        public int OverdueAmount 
	    {
		    get
		    {
			    return GetColumnValue<int>("overdue_amount");
		    }
            set 
		    {
			    SetColumnValue("overdue_amount", value);
            }
        }
	      
        [XmlAttribute("WmsAmount")]
        [Bindable(true)]
        public int WmsAmount 
	    {
		    get
		    {
			    return GetColumnValue<int>("wms_amount");
		    }
            set 
		    {
			    SetColumnValue("wms_amount", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string Id = @"id";
            
            public static string Year = @"year";
            
            public static string Month = @"month";
            
            public static string Name = @"name";
            
            public static string BillId = @"bill_id";
            
            public static string BillNumber = @"bill_number";
            
            public static string CreateTime = @"create_time";
            
            public static string InvoiceDate = @"invoice_date";
            
            public static string BuyerType = @"buyer_type";
            
            public static string BillSentDate = @"bill_sent_date";
            
            public static string BillMoney = @"bill_money";
            
            public static string BillMoneyNotaxed = @"bill_money_notaxed";
            
            public static string BillTax = @"bill_tax";
            
            public static string Remark = @"remark";
            
            public static string DealStartTime = @"deal_start_time";
            
            public static string DealEndTime = @"deal_end_time";
            
            public static string UseStartTime = @"use_start_time";
            
            public static string UseEndTime = @"use_end_time";
            
            public static string VendorReceiptType = @"vendor_receipt_type";
            
            public static string CompanyAccountName = @"CompanyAccountName";
            
            public static string CompanyName = @"CompanyName";
            
            public static string CompanyAccount = @"CompanyAccount";
            
            public static string CompanyBankCode = @"CompanyBankCode";
            
            public static string CompanyBranchCode = @"CompanyBranchCode";
            
            public static string CompanyID = @"CompanyID";
            
            public static string SignCompanyID = @"SignCompanyID";
            
            public static string CompanyBossName = @"CompanyBossName";
            
            public static string AccountantName = @"accountant_name";
            
            public static string AccountantTel = @"accountant_tel";
            
            public static string AccountantEmail = @"accountant_email";
            
            public static string IsTransferComplete = @"is_transfer_complete";
            
            public static string ResponseTime = @"response_time";
            
            public static string ProductGuid = @"product_guid";
            
            public static string StoreGuid = @"store_guid";
            
            public static string RemittanceType = @"remittance_type";
            
            public static string UniqueId = @"unique_id";
            
            public static string HiDealDealId = @"hi_deal_deal_id";
            
            public static string GenerationFrequency = @"generation_frequency";
            
            public static string DeEmpName = @"de_emp_name";
            
            public static string OpEmpName = @"op_emp_name";
            
            public static string DeptId = @"dept_id";
            
            public static string StoreName = @"store_name";
            
            public static string SellerGuid = @"seller_guid";
            
            public static string SellerName = @"seller_name";
            
            public static string IsConfirmedReadyToPay = @"is_confirmed_ready_to_pay";
            
            public static string FinanceGetDate = @"finance_get_date";
            
            public static string Message = @"message";
            
            public static string TotalSum = @"total_sum";
            
            public static string BillSum = @"bill_sum";
            
            public static string BillSumNotaxed = @"bill_sum_notaxed";
            
            public static string BlTax = @"bl_tax";
            
            public static string BlRemark = @"bl_remark";
            
            public static string InvoiceComId = @"invoice_com_id";
            
            public static string IntervalStart = @"interval_start";
            
            public static string IntervalEnd = @"interval_end";
            
            public static string ProductType = @"product_type";
            
            public static string CreateId = @"create_id";
            
            public static string BalanceSheetType = @"balance_sheet_type";
            
            public static string DeliveryType = @"delivery_type";
            
            public static string DefaultPaymentTime = @"default_payment_time";
            
            public static string EstAmount = @"est_amount";
            
            public static string IspFamilyAmount = @"isp_family_amount";
            
            public static string IspSevenAmount = @"isp_seven_amount";
            
            public static string VendorInvoiceNumber = @"vendor_invoice_number";
            
            public static string VendorInvoiceNumberTime = @"vendor_invoice_number_time";
            
            public static string VendorEinvoiceId = @"vendor_einvoice_id";
            
            public static string OverdueAmount = @"overdue_amount";
            
            public static string WmsAmount = @"wms_amount";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

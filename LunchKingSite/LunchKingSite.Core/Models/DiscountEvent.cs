using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the DiscountEvent class.
    /// </summary>
    [Serializable]
    public partial class DiscountEventCollection : RepositoryList<DiscountEvent, DiscountEventCollection>
    {
        public DiscountEventCollection() { }

        /// <summary>
        /// Filters an existing collection based on the set criteria. This is an in-memory filter
        /// Thanks to developingchris for this!
        /// </summary>
        /// <returns>DiscountEventCollection</returns>
        public DiscountEventCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                DiscountEvent o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }


    }
    /// <summary>
    /// This is an ActiveRecord class which wraps the discount_event table.
    /// </summary>
    [Serializable]
    public partial class DiscountEvent : RepositoryRecord<DiscountEvent>, IRecordBase
    {
        #region .ctors and Default Settings

        public DiscountEvent()
        {
            SetSQLProps();
            InitSetDefaults();
            MarkNew();
        }

        private void InitSetDefaults() { SetDefaults(); }

        public DiscountEvent(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
                ForceDefaults();
            MarkNew();
        }

        protected static void SetSQLProps() { GetTableSchema(); }

        #endregion

        #region Schema and Query Accessor
        public static Query CreateQuery() { return new Query(Schema); }
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                    SetSQLProps();
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("discount_event", TableType.Table, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = true;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = true;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                colvarId.DefaultSetting = @"";
                colvarId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarId);

                TableSchema.TableColumn colvarName = new TableSchema.TableColumn(schema);
                colvarName.ColumnName = "name";
                colvarName.DataType = DbType.String;
                colvarName.MaxLength = 200;
                colvarName.AutoIncrement = false;
                colvarName.IsNullable = false;
                colvarName.IsPrimaryKey = false;
                colvarName.IsForeignKey = false;
                colvarName.IsReadOnly = false;
                colvarName.DefaultSetting = @"";
                colvarName.ForeignKeyTableName = "";
                schema.Columns.Add(colvarName);

                TableSchema.TableColumn colvarStartTime = new TableSchema.TableColumn(schema);
                colvarStartTime.ColumnName = "start_time";
                colvarStartTime.DataType = DbType.DateTime;
                colvarStartTime.MaxLength = 0;
                colvarStartTime.AutoIncrement = false;
                colvarStartTime.IsNullable = false;
                colvarStartTime.IsPrimaryKey = false;
                colvarStartTime.IsForeignKey = false;
                colvarStartTime.IsReadOnly = false;
                colvarStartTime.DefaultSetting = @"";
                colvarStartTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarStartTime);

                TableSchema.TableColumn colvarEndTime = new TableSchema.TableColumn(schema);
                colvarEndTime.ColumnName = "end_time";
                colvarEndTime.DataType = DbType.DateTime;
                colvarEndTime.MaxLength = 0;
                colvarEndTime.AutoIncrement = false;
                colvarEndTime.IsNullable = false;
                colvarEndTime.IsPrimaryKey = false;
                colvarEndTime.IsForeignKey = false;
                colvarEndTime.IsReadOnly = false;
                colvarEndTime.DefaultSetting = @"";
                colvarEndTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarEndTime);

                TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
                colvarStatus.ColumnName = "status";
                colvarStatus.DataType = DbType.Int32;
                colvarStatus.MaxLength = 0;
                colvarStatus.AutoIncrement = false;
                colvarStatus.IsNullable = false;
                colvarStatus.IsPrimaryKey = false;
                colvarStatus.IsForeignKey = false;
                colvarStatus.IsReadOnly = false;

                colvarStatus.DefaultSetting = @"((0))";
                colvarStatus.ForeignKeyTableName = "";
                schema.Columns.Add(colvarStatus);

                TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
                colvarCreateId.ColumnName = "create_id";
                colvarCreateId.DataType = DbType.String;
                colvarCreateId.MaxLength = 250;
                colvarCreateId.AutoIncrement = false;
                colvarCreateId.IsNullable = false;
                colvarCreateId.IsPrimaryKey = false;
                colvarCreateId.IsForeignKey = false;
                colvarCreateId.IsReadOnly = false;
                colvarCreateId.DefaultSetting = @"";
                colvarCreateId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateId);

                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = false;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                colvarCreateTime.DefaultSetting = @"";
                colvarCreateTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateTime);

                TableSchema.TableColumn colvarModifyId = new TableSchema.TableColumn(schema);
                colvarModifyId.ColumnName = "modify_id";
                colvarModifyId.DataType = DbType.String;
                colvarModifyId.MaxLength = 250;
                colvarModifyId.AutoIncrement = false;
                colvarModifyId.IsNullable = true;
                colvarModifyId.IsPrimaryKey = false;
                colvarModifyId.IsForeignKey = false;
                colvarModifyId.IsReadOnly = false;
                colvarModifyId.DefaultSetting = @"";
                colvarModifyId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarModifyId);

                TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
                colvarModifyTime.ColumnName = "modify_time";
                colvarModifyTime.DataType = DbType.DateTime;
                colvarModifyTime.MaxLength = 0;
                colvarModifyTime.AutoIncrement = false;
                colvarModifyTime.IsNullable = true;
                colvarModifyTime.IsPrimaryKey = false;
                colvarModifyTime.IsForeignKey = false;
                colvarModifyTime.IsReadOnly = false;
                colvarModifyTime.DefaultSetting = @"";
                colvarModifyTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarModifyTime);

                TableSchema.TableColumn colvarType = new TableSchema.TableColumn(schema);
                colvarType.ColumnName = "type";
                colvarType.DataType = DbType.Int32;
                colvarType.MaxLength = 0;
                colvarType.AutoIncrement = false;
                colvarType.IsNullable = false;
                colvarType.IsPrimaryKey = false;
                colvarType.IsForeignKey = false;
                colvarType.IsReadOnly = false;

                colvarType.DefaultSetting = @"((0))";
                colvarType.ForeignKeyTableName = "";
                schema.Columns.Add(colvarType);

                TableSchema.TableColumn colvarEventCode = new TableSchema.TableColumn(schema);
                colvarEventCode.ColumnName = "event_code";
                colvarEventCode.DataType = DbType.AnsiString;
                colvarEventCode.MaxLength = 50;
                colvarEventCode.AutoIncrement = false;
                colvarEventCode.IsNullable = true;
                colvarEventCode.IsPrimaryKey = false;
                colvarEventCode.IsForeignKey = false;
                colvarEventCode.IsReadOnly = false;
                colvarEventCode.DefaultSetting = @"";
                colvarEventCode.ForeignKeyTableName = "";
                schema.Columns.Add(colvarEventCode);

                TableSchema.TableColumn colvarCode = new TableSchema.TableColumn(schema);
                colvarCode.ColumnName = "code";
                colvarCode.DataType = DbType.AnsiString;
                colvarCode.MaxLength = 20;
                colvarCode.AutoIncrement = false;
                colvarCode.IsNullable = true;
                colvarCode.IsPrimaryKey = false;
                colvarCode.IsForeignKey = false;
                colvarCode.IsReadOnly = false;
                colvarCode.DefaultSetting = @"";
                colvarCode.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCode);

                TableSchema.TableColumn colvarEventType = new TableSchema.TableColumn(schema);
                colvarEventType.ColumnName = "event_type";
                colvarEventType.DataType = DbType.Int32;
                colvarEventType.MaxLength = 0;
                colvarEventType.AutoIncrement = false;
                colvarEventType.IsNullable = false;
                colvarEventType.IsPrimaryKey = false;
                colvarEventType.IsForeignKey = false;
                colvarEventType.IsReadOnly = false;

                colvarEventType.DefaultSetting = @"((0))";
                colvarEventType.ForeignKeyTableName = "";
                schema.Columns.Add(colvarEventType);

                TableSchema.TableColumn colvarApplyTime = new TableSchema.TableColumn(schema);
                colvarApplyTime.ColumnName = "apply_time";
                colvarApplyTime.DataType = DbType.DateTime;
                colvarApplyTime.MaxLength = 0;
                colvarApplyTime.AutoIncrement = false;
                colvarApplyTime.IsNullable = true;
                colvarApplyTime.IsPrimaryKey = false;
                colvarApplyTime.IsForeignKey = false;
                colvarApplyTime.IsReadOnly = false;
                colvarApplyTime.DefaultSetting = @"";
                colvarApplyTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarApplyTime);

                TableSchema.TableColumn colvarQty = new TableSchema.TableColumn(schema);
                colvarQty.ColumnName = "qty";
                colvarQty.DataType = DbType.Int32;
                colvarQty.MaxLength = 0;
                colvarQty.AutoIncrement = false;
                colvarQty.IsNullable = true;
                colvarQty.IsPrimaryKey = false;
                colvarQty.IsForeignKey = false;
                colvarQty.IsReadOnly = false;
                colvarQty.DefaultSetting = @"";
                colvarQty.ForeignKeyTableName = "";
                schema.Columns.Add(colvarQty);

                TableSchema.TableColumn colvarTotal = new TableSchema.TableColumn(schema);
                colvarTotal.ColumnName = "total";
                colvarTotal.DataType = DbType.Int32;
                colvarTotal.MaxLength = 0;
                colvarTotal.AutoIncrement = false;
                colvarTotal.IsNullable = true;
                colvarTotal.IsPrimaryKey = false;
                colvarTotal.IsForeignKey = false;
                colvarTotal.IsReadOnly = false;
                colvarTotal.DefaultSetting = @"";
                colvarTotal.ForeignKeyTableName = "";
                schema.Columns.Add(colvarTotal);

                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("discount_event", schema);
            }
        }
        #endregion

        #region Props

        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id
        {
            get { return GetColumnValue<int>(Columns.Id); }
            set { SetColumnValue(Columns.Id, value); }
        }

        [XmlAttribute("Name")]
        [Bindable(true)]
        public string Name
        {
            get { return GetColumnValue<string>(Columns.Name); }
            set { SetColumnValue(Columns.Name, value); }
        }

        [XmlAttribute("StartTime")]
        [Bindable(true)]
        public DateTime StartTime
        {
            get { return GetColumnValue<DateTime>(Columns.StartTime); }
            set { SetColumnValue(Columns.StartTime, value); }
        }

        [XmlAttribute("EndTime")]
        [Bindable(true)]
        public DateTime EndTime
        {
            get { return GetColumnValue<DateTime>(Columns.EndTime); }
            set { SetColumnValue(Columns.EndTime, value); }
        }

        [XmlAttribute("Status")]
        [Bindable(true)]
        public int Status
        {
            get { return GetColumnValue<int>(Columns.Status); }
            set { SetColumnValue(Columns.Status, value); }
        }

        [XmlAttribute("CreateId")]
        [Bindable(true)]
        public string CreateId
        {
            get { return GetColumnValue<string>(Columns.CreateId); }
            set { SetColumnValue(Columns.CreateId, value); }
        }

        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime CreateTime
        {
            get { return GetColumnValue<DateTime>(Columns.CreateTime); }
            set { SetColumnValue(Columns.CreateTime, value); }
        }

        [XmlAttribute("ModifyId")]
        [Bindable(true)]
        public string ModifyId
        {
            get { return GetColumnValue<string>(Columns.ModifyId); }
            set { SetColumnValue(Columns.ModifyId, value); }
        }

        [XmlAttribute("ModifyTime")]
        [Bindable(true)]
        public DateTime? ModifyTime
        {
            get { return GetColumnValue<DateTime?>(Columns.ModifyTime); }
            set { SetColumnValue(Columns.ModifyTime, value); }
        }

        [XmlAttribute("Type")]
        [Bindable(true)]
        public int Type
        {
            get { return GetColumnValue<int>(Columns.Type); }
            set { SetColumnValue(Columns.Type, value); }
        }

        [XmlAttribute("EventCode")]
        [Bindable(true)]
        public string EventCode
        {
            get { return GetColumnValue<string>(Columns.EventCode); }
            set { SetColumnValue(Columns.EventCode, value); }
        }

        [XmlAttribute("Code")]
        [Bindable(true)]
        public string Code
        {
            get { return GetColumnValue<string>(Columns.Code); }
            set { SetColumnValue(Columns.Code, value); }
        }

        [XmlAttribute("EventType")]
        [Bindable(true)]
        public int EventType
        {
            get { return GetColumnValue<int>(Columns.EventType); }
            set { SetColumnValue(Columns.EventType, value); }
        }

        [XmlAttribute("ApplyTime")]
        [Bindable(true)]
        public DateTime? ApplyTime
        {
            get { return GetColumnValue<DateTime?>(Columns.ApplyTime); }
            set { SetColumnValue(Columns.ApplyTime, value); }
        }

        [XmlAttribute("Qty")]
        [Bindable(true)]
        public int? Qty
        {
            get { return GetColumnValue<int?>(Columns.Qty); }
            set { SetColumnValue(Columns.Qty, value); }
        }

        [XmlAttribute("Total")]
        [Bindable(true)]
        public int? Total
        {
            get { return GetColumnValue<int?>(Columns.Total); }
            set { SetColumnValue(Columns.Total, value); }
        }

        #endregion




        //no foreign key tables defined (0)



        //no ManyToMany tables defined (0)





        #region Typed Columns


        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }



        public static TableSchema.TableColumn NameColumn
        {
            get { return Schema.Columns[1]; }
        }



        public static TableSchema.TableColumn StartTimeColumn
        {
            get { return Schema.Columns[2]; }
        }



        public static TableSchema.TableColumn EndTimeColumn
        {
            get { return Schema.Columns[3]; }
        }



        public static TableSchema.TableColumn StatusColumn
        {
            get { return Schema.Columns[4]; }
        }



        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[5]; }
        }



        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[6]; }
        }



        public static TableSchema.TableColumn ModifyIdColumn
        {
            get { return Schema.Columns[7]; }
        }



        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[8]; }
        }



        public static TableSchema.TableColumn TypeColumn
        {
            get { return Schema.Columns[9]; }
        }



        public static TableSchema.TableColumn EventCodeColumn
        {
            get { return Schema.Columns[10]; }
        }



        public static TableSchema.TableColumn CodeColumn
        {
            get { return Schema.Columns[11]; }
        }



        public static TableSchema.TableColumn EventTypeColumn
        {
            get { return Schema.Columns[12]; }
        }



        public static TableSchema.TableColumn ApplyTimeColumn
        {
            get { return Schema.Columns[13]; }
        }



        public static TableSchema.TableColumn QtyColumn
        {
            get { return Schema.Columns[14]; }
        }



        public static TableSchema.TableColumn TotalColumn
        {
            get { return Schema.Columns[15]; }
        }



        #endregion
        #region Columns Struct
        public struct Columns
        {
            public static string Id = @"id";
            public static string Name = @"name";
            public static string StartTime = @"start_time";
            public static string EndTime = @"end_time";
            public static string Status = @"status";
            public static string CreateId = @"create_id";
            public static string CreateTime = @"create_time";
            public static string ModifyId = @"modify_id";
            public static string ModifyTime = @"modify_time";
            public static string Type = @"type";
            public static string EventCode = @"event_code";
            public static string Code = @"code";
            public static string EventType = @"event_type";
            public static string ApplyTime = @"apply_time";
            public static string Qty = @"qty";
            public static string Total = @"total";

        }
        #endregion

        #region Update PK Collections

        #endregion

        #region Deep Save

        #endregion
    }
}

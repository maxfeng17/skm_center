using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the UserCashTrustLog class.
    /// </summary>
    [Serializable]
    public partial class UserCashTrustLogCollection : RepositoryList<UserCashTrustLog, UserCashTrustLogCollection>
    {
        public UserCashTrustLogCollection() { }

        /// <summary>
        /// Filters an existing collection based on the set criteria. This is an in-memory filter
        /// Thanks to developingchris for this!
        /// </summary>
        /// <returns>UserCashTrustLogCollection</returns>
        public UserCashTrustLogCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                UserCashTrustLog o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }


    }
    /// <summary>
    /// This is an ActiveRecord class which wraps the user_cash_trust_log table.
    /// </summary>
    [Serializable]
    public partial class UserCashTrustLog : RepositoryRecord<UserCashTrustLog>, IRecordBase
    {
        #region .ctors and Default Settings

        public UserCashTrustLog()
        {
            SetSQLProps();
            InitSetDefaults();
            MarkNew();
        }

        private void InitSetDefaults() { SetDefaults(); }

        public UserCashTrustLog(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
                ForceDefaults();
            MarkNew();
        }

        protected static void SetSQLProps() { GetTableSchema(); }

        #endregion

        #region Schema and Query Accessor
        public static Query CreateQuery() { return new Query(Schema); }
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                    SetSQLProps();
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("user_cash_trust_log", TableType.Table, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = true;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = true;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                colvarId.DefaultSetting = @"";
                colvarId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarId);

                TableSchema.TableColumn colvarUserId = new TableSchema.TableColumn(schema);
                colvarUserId.ColumnName = "user_id";
                colvarUserId.DataType = DbType.Int32;
                colvarUserId.MaxLength = 0;
                colvarUserId.AutoIncrement = false;
                colvarUserId.IsNullable = false;
                colvarUserId.IsPrimaryKey = false;
                colvarUserId.IsForeignKey = false;
                colvarUserId.IsReadOnly = false;
                colvarUserId.DefaultSetting = @"";
                colvarUserId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarUserId);

                TableSchema.TableColumn colvarAmount = new TableSchema.TableColumn(schema);
                colvarAmount.ColumnName = "amount";
                colvarAmount.DataType = DbType.Int32;
                colvarAmount.MaxLength = 0;
                colvarAmount.AutoIncrement = false;
                colvarAmount.IsNullable = false;
                colvarAmount.IsPrimaryKey = false;
                colvarAmount.IsForeignKey = false;
                colvarAmount.IsReadOnly = false;
                colvarAmount.DefaultSetting = @"";
                colvarAmount.ForeignKeyTableName = "";
                schema.Columns.Add(colvarAmount);

                TableSchema.TableColumn colvarBankStatus = new TableSchema.TableColumn(schema);
                colvarBankStatus.ColumnName = "bank_status";
                colvarBankStatus.DataType = DbType.Int32;
                colvarBankStatus.MaxLength = 0;
                colvarBankStatus.AutoIncrement = false;
                colvarBankStatus.IsNullable = false;
                colvarBankStatus.IsPrimaryKey = false;
                colvarBankStatus.IsForeignKey = false;
                colvarBankStatus.IsReadOnly = false;
                colvarBankStatus.DefaultSetting = @"";
                colvarBankStatus.ForeignKeyTableName = "";
                schema.Columns.Add(colvarBankStatus);

                TableSchema.TableColumn colvarTrustedTime = new TableSchema.TableColumn(schema);
                colvarTrustedTime.ColumnName = "trusted_time";
                colvarTrustedTime.DataType = DbType.DateTime;
                colvarTrustedTime.MaxLength = 0;
                colvarTrustedTime.AutoIncrement = false;
                colvarTrustedTime.IsNullable = true;
                colvarTrustedTime.IsPrimaryKey = false;
                colvarTrustedTime.IsForeignKey = false;
                colvarTrustedTime.IsReadOnly = false;
                colvarTrustedTime.DefaultSetting = @"";
                colvarTrustedTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarTrustedTime);

                TableSchema.TableColumn colvarTrustedBankTime = new TableSchema.TableColumn(schema);
                colvarTrustedBankTime.ColumnName = "trusted_bank_time";
                colvarTrustedBankTime.DataType = DbType.DateTime;
                colvarTrustedBankTime.MaxLength = 0;
                colvarTrustedBankTime.AutoIncrement = false;
                colvarTrustedBankTime.IsNullable = true;
                colvarTrustedBankTime.IsPrimaryKey = false;
                colvarTrustedBankTime.IsForeignKey = false;
                colvarTrustedBankTime.IsReadOnly = false;
                colvarTrustedBankTime.DefaultSetting = @"";
                colvarTrustedBankTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarTrustedBankTime);

                TableSchema.TableColumn colvarVerifiedTime = new TableSchema.TableColumn(schema);
                colvarVerifiedTime.ColumnName = "verified_time";
                colvarVerifiedTime.DataType = DbType.DateTime;
                colvarVerifiedTime.MaxLength = 0;
                colvarVerifiedTime.AutoIncrement = false;
                colvarVerifiedTime.IsNullable = true;
                colvarVerifiedTime.IsPrimaryKey = false;
                colvarVerifiedTime.IsForeignKey = false;
                colvarVerifiedTime.IsReadOnly = false;
                colvarVerifiedTime.DefaultSetting = @"";
                colvarVerifiedTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarVerifiedTime);

                TableSchema.TableColumn colvarVerifiedBankTime = new TableSchema.TableColumn(schema);
                colvarVerifiedBankTime.ColumnName = "verified_bank_time";
                colvarVerifiedBankTime.DataType = DbType.DateTime;
                colvarVerifiedBankTime.MaxLength = 0;
                colvarVerifiedBankTime.AutoIncrement = false;
                colvarVerifiedBankTime.IsNullable = true;
                colvarVerifiedBankTime.IsPrimaryKey = false;
                colvarVerifiedBankTime.IsForeignKey = false;
                colvarVerifiedBankTime.IsReadOnly = false;
                colvarVerifiedBankTime.DefaultSetting = @"";
                colvarVerifiedBankTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarVerifiedBankTime);

                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = false;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                colvarCreateTime.DefaultSetting = @"";
                colvarCreateTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateTime);

                TableSchema.TableColumn colvarMessage = new TableSchema.TableColumn(schema);
                colvarMessage.ColumnName = "message";
                colvarMessage.DataType = DbType.String;
                colvarMessage.MaxLength = -1;
                colvarMessage.AutoIncrement = false;
                colvarMessage.IsNullable = true;
                colvarMessage.IsPrimaryKey = false;
                colvarMessage.IsForeignKey = false;
                colvarMessage.IsReadOnly = false;
                colvarMessage.DefaultSetting = @"";
                colvarMessage.ForeignKeyTableName = "";
                schema.Columns.Add(colvarMessage);

                TableSchema.TableColumn colvarTrustProvider = new TableSchema.TableColumn(schema);
                colvarTrustProvider.ColumnName = "trust_provider";
                colvarTrustProvider.DataType = DbType.Int32;
                colvarTrustProvider.MaxLength = 0;
                colvarTrustProvider.AutoIncrement = false;
                colvarTrustProvider.IsNullable = false;
                colvarTrustProvider.IsPrimaryKey = false;
                colvarTrustProvider.IsForeignKey = false;
                colvarTrustProvider.IsReadOnly = false;
                colvarTrustProvider.DefaultSetting = @"";
                colvarTrustProvider.ForeignKeyTableName = "";
                schema.Columns.Add(colvarTrustProvider);

                TableSchema.TableColumn colvarTrustId = new TableSchema.TableColumn(schema);
                colvarTrustId.ColumnName = "trust_id";
                colvarTrustId.DataType = DbType.Guid;
                colvarTrustId.MaxLength = 0;
                colvarTrustId.AutoIncrement = false;
                colvarTrustId.IsNullable = false;
                colvarTrustId.IsPrimaryKey = false;
                colvarTrustId.IsForeignKey = true;
                colvarTrustId.IsReadOnly = false;
                colvarTrustId.DefaultSetting = @"";

                colvarTrustId.ForeignKeyTableName = "cash_trust_log";
                schema.Columns.Add(colvarTrustId);

                TableSchema.TableColumn colvarCouponId = new TableSchema.TableColumn(schema);
                colvarCouponId.ColumnName = "coupon_id";
                colvarCouponId.DataType = DbType.Int32;
                colvarCouponId.MaxLength = 0;
                colvarCouponId.AutoIncrement = false;
                colvarCouponId.IsNullable = true;
                colvarCouponId.IsPrimaryKey = false;
                colvarCouponId.IsForeignKey = false;
                colvarCouponId.IsReadOnly = false;
                colvarCouponId.DefaultSetting = @"";
                colvarCouponId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCouponId);

                TableSchema.TableColumn colvarCouponSequenceNumber = new TableSchema.TableColumn(schema);
                colvarCouponSequenceNumber.ColumnName = "coupon_sequence_number";
                colvarCouponSequenceNumber.DataType = DbType.String;
                colvarCouponSequenceNumber.MaxLength = 50;
                colvarCouponSequenceNumber.AutoIncrement = false;
                colvarCouponSequenceNumber.IsNullable = true;
                colvarCouponSequenceNumber.IsPrimaryKey = false;
                colvarCouponSequenceNumber.IsForeignKey = false;
                colvarCouponSequenceNumber.IsReadOnly = false;
                colvarCouponSequenceNumber.DefaultSetting = @"";
                colvarCouponSequenceNumber.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCouponSequenceNumber);

                TableSchema.TableColumn colvarType = new TableSchema.TableColumn(schema);
                colvarType.ColumnName = "type";
                colvarType.DataType = DbType.Int32;
                colvarType.MaxLength = 0;
                colvarType.AutoIncrement = false;
                colvarType.IsNullable = false;
                colvarType.IsPrimaryKey = false;
                colvarType.IsForeignKey = false;
                colvarType.IsReadOnly = false;
                colvarType.DefaultSetting = @"";
                colvarType.ForeignKeyTableName = "";
                schema.Columns.Add(colvarType);

                TableSchema.TableColumn colvarReportTrustedGuid = new TableSchema.TableColumn(schema);
                colvarReportTrustedGuid.ColumnName = "report_trusted_guid";
                colvarReportTrustedGuid.DataType = DbType.Guid;
                colvarReportTrustedGuid.MaxLength = 0;
                colvarReportTrustedGuid.AutoIncrement = false;
                colvarReportTrustedGuid.IsNullable = true;
                colvarReportTrustedGuid.IsPrimaryKey = false;
                colvarReportTrustedGuid.IsForeignKey = false;
                colvarReportTrustedGuid.IsReadOnly = false;
                colvarReportTrustedGuid.DefaultSetting = @"";
                colvarReportTrustedGuid.ForeignKeyTableName = "";
                schema.Columns.Add(colvarReportTrustedGuid);

                TableSchema.TableColumn colvarReportVerifiedGuid = new TableSchema.TableColumn(schema);
                colvarReportVerifiedGuid.ColumnName = "report_verified_guid";
                colvarReportVerifiedGuid.DataType = DbType.Guid;
                colvarReportVerifiedGuid.MaxLength = 0;
                colvarReportVerifiedGuid.AutoIncrement = false;
                colvarReportVerifiedGuid.IsNullable = true;
                colvarReportVerifiedGuid.IsPrimaryKey = false;
                colvarReportVerifiedGuid.IsForeignKey = false;
                colvarReportVerifiedGuid.IsReadOnly = false;
                colvarReportVerifiedGuid.DefaultSetting = @"";
                colvarReportVerifiedGuid.ForeignKeyTableName = "";
                schema.Columns.Add(colvarReportVerifiedGuid);

                TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
                colvarStatus.ColumnName = "status";
                colvarStatus.DataType = DbType.Int32;
                colvarStatus.MaxLength = 0;
                colvarStatus.AutoIncrement = false;
                colvarStatus.IsNullable = true;
                colvarStatus.IsPrimaryKey = false;
                colvarStatus.IsForeignKey = false;
                colvarStatus.IsReadOnly = false;
                colvarStatus.DefaultSetting = @"";
                colvarStatus.ForeignKeyTableName = "";
                schema.Columns.Add(colvarStatus);

                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("user_cash_trust_log", schema);
            }
        }
        #endregion

        #region Props

        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id
        {
            get { return GetColumnValue<int>(Columns.Id); }
            set { SetColumnValue(Columns.Id, value); }
        }

        [XmlAttribute("UserId")]
        [Bindable(true)]
        public int UserId
        {
            get { return GetColumnValue<int>(Columns.UserId); }
            set { SetColumnValue(Columns.UserId, value); }
        }

        [XmlAttribute("Amount")]
        [Bindable(true)]
        public int Amount
        {
            get { return GetColumnValue<int>(Columns.Amount); }
            set { SetColumnValue(Columns.Amount, value); }
        }

        [XmlAttribute("BankStatus")]
        [Bindable(true)]
        public int BankStatus
        {
            get { return GetColumnValue<int>(Columns.BankStatus); }
            set { SetColumnValue(Columns.BankStatus, value); }
        }

        [XmlAttribute("TrustedTime")]
        [Bindable(true)]
        public DateTime? TrustedTime
        {
            get { return GetColumnValue<DateTime?>(Columns.TrustedTime); }
            set { SetColumnValue(Columns.TrustedTime, value); }
        }

        [XmlAttribute("TrustedBankTime")]
        [Bindable(true)]
        public DateTime? TrustedBankTime
        {
            get { return GetColumnValue<DateTime?>(Columns.TrustedBankTime); }
            set { SetColumnValue(Columns.TrustedBankTime, value); }
        }

        [XmlAttribute("VerifiedTime")]
        [Bindable(true)]
        public DateTime? VerifiedTime
        {
            get { return GetColumnValue<DateTime?>(Columns.VerifiedTime); }
            set { SetColumnValue(Columns.VerifiedTime, value); }
        }

        [XmlAttribute("VerifiedBankTime")]
        [Bindable(true)]
        public DateTime? VerifiedBankTime
        {
            get { return GetColumnValue<DateTime?>(Columns.VerifiedBankTime); }
            set { SetColumnValue(Columns.VerifiedBankTime, value); }
        }

        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime CreateTime
        {
            get { return GetColumnValue<DateTime>(Columns.CreateTime); }
            set { SetColumnValue(Columns.CreateTime, value); }
        }

        [XmlAttribute("Message")]
        [Bindable(true)]
        public string Message
        {
            get { return GetColumnValue<string>(Columns.Message); }
            set { SetColumnValue(Columns.Message, value); }
        }

        [XmlAttribute("TrustProvider")]
        [Bindable(true)]
        public int TrustProvider
        {
            get { return GetColumnValue<int>(Columns.TrustProvider); }
            set { SetColumnValue(Columns.TrustProvider, value); }
        }

        [XmlAttribute("TrustId")]
        [Bindable(true)]
        public Guid TrustId
        {
            get { return GetColumnValue<Guid>(Columns.TrustId); }
            set { SetColumnValue(Columns.TrustId, value); }
        }

        [XmlAttribute("CouponId")]
        [Bindable(true)]
        public int? CouponId
        {
            get { return GetColumnValue<int?>(Columns.CouponId); }
            set { SetColumnValue(Columns.CouponId, value); }
        }

        [XmlAttribute("CouponSequenceNumber")]
        [Bindable(true)]
        public string CouponSequenceNumber
        {
            get { return GetColumnValue<string>(Columns.CouponSequenceNumber); }
            set { SetColumnValue(Columns.CouponSequenceNumber, value); }
        }

        [XmlAttribute("Type")]
        [Bindable(true)]
        public int Type
        {
            get { return GetColumnValue<int>(Columns.Type); }
            set { SetColumnValue(Columns.Type, value); }
        }

        [XmlAttribute("ReportTrustedGuid")]
        [Bindable(true)]
        public Guid? ReportTrustedGuid
        {
            get { return GetColumnValue<Guid?>(Columns.ReportTrustedGuid); }
            set { SetColumnValue(Columns.ReportTrustedGuid, value); }
        }

        [XmlAttribute("ReportVerifiedGuid")]
        [Bindable(true)]
        public Guid? ReportVerifiedGuid
        {
            get { return GetColumnValue<Guid?>(Columns.ReportVerifiedGuid); }
            set { SetColumnValue(Columns.ReportVerifiedGuid, value); }
        }

        [XmlAttribute("Status")]
        [Bindable(true)]
        public int? Status
        {
            get { return GetColumnValue<int?>(Columns.Status); }
            set { SetColumnValue(Columns.Status, value); }
        }

        #endregion




        //no foreign key tables defined (1)



        //no ManyToMany tables defined (0)





        #region Typed Columns


        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }



        public static TableSchema.TableColumn UserIdColumn
        {
            get { return Schema.Columns[1]; }
        }



        public static TableSchema.TableColumn AmountColumn
        {
            get { return Schema.Columns[2]; }
        }



        public static TableSchema.TableColumn BankStatusColumn
        {
            get { return Schema.Columns[3]; }
        }



        public static TableSchema.TableColumn TrustedTimeColumn
        {
            get { return Schema.Columns[4]; }
        }



        public static TableSchema.TableColumn TrustedBankTimeColumn
        {
            get { return Schema.Columns[5]; }
        }



        public static TableSchema.TableColumn VerifiedTimeColumn
        {
            get { return Schema.Columns[6]; }
        }



        public static TableSchema.TableColumn VerifiedBankTimeColumn
        {
            get { return Schema.Columns[7]; }
        }



        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[8]; }
        }



        public static TableSchema.TableColumn MessageColumn
        {
            get { return Schema.Columns[9]; }
        }



        public static TableSchema.TableColumn TrustProviderColumn
        {
            get { return Schema.Columns[10]; }
        }



        public static TableSchema.TableColumn TrustIdColumn
        {
            get { return Schema.Columns[11]; }
        }



        public static TableSchema.TableColumn CouponIdColumn
        {
            get { return Schema.Columns[12]; }
        }



        public static TableSchema.TableColumn CouponSequenceNumberColumn
        {
            get { return Schema.Columns[13]; }
        }



        public static TableSchema.TableColumn TypeColumn
        {
            get { return Schema.Columns[14]; }
        }



        public static TableSchema.TableColumn ReportTrustedGuidColumn
        {
            get { return Schema.Columns[15]; }
        }



        public static TableSchema.TableColumn ReportVerifiedGuidColumn
        {
            get { return Schema.Columns[16]; }
        }



        public static TableSchema.TableColumn StatusColumn
        {
            get { return Schema.Columns[17]; }
        }



        #endregion
        #region Columns Struct
        public struct Columns
        {
            public static string Id = @"id";
            public static string UserId = @"user_id";
            public static string Amount = @"amount";
            public static string BankStatus = @"bank_status";
            public static string TrustedTime = @"trusted_time";
            public static string TrustedBankTime = @"trusted_bank_time";
            public static string VerifiedTime = @"verified_time";
            public static string VerifiedBankTime = @"verified_bank_time";
            public static string CreateTime = @"create_time";
            public static string Message = @"message";
            public static string TrustProvider = @"trust_provider";
            public static string TrustId = @"trust_id";
            public static string CouponId = @"coupon_id";
            public static string CouponSequenceNumber = @"coupon_sequence_number";
            public static string Type = @"type";
            public static string ReportTrustedGuid = @"report_trusted_guid";
            public static string ReportVerifiedGuid = @"report_verified_guid";
            public static string Status = @"status";

        }
        #endregion

        #region Update PK Collections

        #endregion

        #region Deep Save

        #endregion
    }
}

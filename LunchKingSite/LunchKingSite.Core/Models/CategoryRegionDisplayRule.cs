using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the CategoryRegionDisplayRule class.
	/// </summary>
    [Serializable]
	public partial class CategoryRegionDisplayRuleCollection : RepositoryList<CategoryRegionDisplayRule, CategoryRegionDisplayRuleCollection>
	{	   
		public CategoryRegionDisplayRuleCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>CategoryRegionDisplayRuleCollection</returns>
		public CategoryRegionDisplayRuleCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                CategoryRegionDisplayRule o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the category_region_display_rule table.
	/// </summary>
	[Serializable]
	public partial class CategoryRegionDisplayRule : RepositoryRecord<CategoryRegionDisplayRule>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public CategoryRegionDisplayRule()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public CategoryRegionDisplayRule(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("category_region_display_rule", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarCategoryId = new TableSchema.TableColumn(schema);
				colvarCategoryId.ColumnName = "category_id";
				colvarCategoryId.DataType = DbType.Int32;
				colvarCategoryId.MaxLength = 0;
				colvarCategoryId.AutoIncrement = false;
				colvarCategoryId.IsNullable = false;
				colvarCategoryId.IsPrimaryKey = true;
				colvarCategoryId.IsForeignKey = false;
				colvarCategoryId.IsReadOnly = false;
				colvarCategoryId.DefaultSetting = @"";
				colvarCategoryId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCategoryId);
				
				TableSchema.TableColumn colvarSpecialRuleFlag = new TableSchema.TableColumn(schema);
				colvarSpecialRuleFlag.ColumnName = "special_rule_flag";
				colvarSpecialRuleFlag.DataType = DbType.Int32;
				colvarSpecialRuleFlag.MaxLength = 0;
				colvarSpecialRuleFlag.AutoIncrement = false;
				colvarSpecialRuleFlag.IsNullable = false;
				colvarSpecialRuleFlag.IsPrimaryKey = false;
				colvarSpecialRuleFlag.IsForeignKey = false;
				colvarSpecialRuleFlag.IsReadOnly = false;
				
						colvarSpecialRuleFlag.DefaultSetting = @"((0))";
				colvarSpecialRuleFlag.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSpecialRuleFlag);
				
				TableSchema.TableColumn colvarCategoryDisplayName = new TableSchema.TableColumn(schema);
				colvarCategoryDisplayName.ColumnName = "category_display_name";
				colvarCategoryDisplayName.DataType = DbType.String;
				colvarCategoryDisplayName.MaxLength = 50;
				colvarCategoryDisplayName.AutoIncrement = false;
				colvarCategoryDisplayName.IsNullable = true;
				colvarCategoryDisplayName.IsPrimaryKey = false;
				colvarCategoryDisplayName.IsForeignKey = false;
				colvarCategoryDisplayName.IsReadOnly = false;
				colvarCategoryDisplayName.DefaultSetting = @"";
				colvarCategoryDisplayName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCategoryDisplayName);
				
				TableSchema.TableColumn colvarSummaryItemName = new TableSchema.TableColumn(schema);
				colvarSummaryItemName.ColumnName = "summary_item_name";
				colvarSummaryItemName.DataType = DbType.String;
				colvarSummaryItemName.MaxLength = 50;
				colvarSummaryItemName.AutoIncrement = false;
				colvarSummaryItemName.IsNullable = true;
				colvarSummaryItemName.IsPrimaryKey = false;
				colvarSummaryItemName.IsForeignKey = false;
				colvarSummaryItemName.IsReadOnly = false;
				colvarSummaryItemName.DefaultSetting = @"";
				colvarSummaryItemName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSummaryItemName);
				
				TableSchema.TableColumn colvarSummaryItemCssClass = new TableSchema.TableColumn(schema);
				colvarSummaryItemCssClass.ColumnName = "summary_item_css_class";
				colvarSummaryItemCssClass.DataType = DbType.String;
				colvarSummaryItemCssClass.MaxLength = 50;
				colvarSummaryItemCssClass.AutoIncrement = false;
				colvarSummaryItemCssClass.IsNullable = true;
				colvarSummaryItemCssClass.IsPrimaryKey = false;
				colvarSummaryItemCssClass.IsForeignKey = false;
				colvarSummaryItemCssClass.IsReadOnly = false;
				colvarSummaryItemCssClass.DefaultSetting = @"";
				colvarSummaryItemCssClass.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSummaryItemCssClass);
				
				TableSchema.TableColumn colvarSubListCssClass = new TableSchema.TableColumn(schema);
				colvarSubListCssClass.ColumnName = "sub_list_css_class";
				colvarSubListCssClass.DataType = DbType.String;
				colvarSubListCssClass.MaxLength = 50;
				colvarSubListCssClass.AutoIncrement = false;
				colvarSubListCssClass.IsNullable = true;
				colvarSubListCssClass.IsPrimaryKey = false;
				colvarSubListCssClass.IsForeignKey = false;
				colvarSubListCssClass.IsReadOnly = false;
				colvarSubListCssClass.DefaultSetting = @"";
				colvarSubListCssClass.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSubListCssClass);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("category_region_display_rule",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("CategoryId")]
		[Bindable(true)]
		public int CategoryId 
		{
			get { return GetColumnValue<int>(Columns.CategoryId); }
			set { SetColumnValue(Columns.CategoryId, value); }
		}
		  
		[XmlAttribute("SpecialRuleFlag")]
		[Bindable(true)]
		public int SpecialRuleFlag 
		{
			get { return GetColumnValue<int>(Columns.SpecialRuleFlag); }
			set { SetColumnValue(Columns.SpecialRuleFlag, value); }
		}
		  
		[XmlAttribute("CategoryDisplayName")]
		[Bindable(true)]
		public string CategoryDisplayName 
		{
			get { return GetColumnValue<string>(Columns.CategoryDisplayName); }
			set { SetColumnValue(Columns.CategoryDisplayName, value); }
		}
		  
		[XmlAttribute("SummaryItemName")]
		[Bindable(true)]
		public string SummaryItemName 
		{
			get { return GetColumnValue<string>(Columns.SummaryItemName); }
			set { SetColumnValue(Columns.SummaryItemName, value); }
		}
		  
		[XmlAttribute("SummaryItemCssClass")]
		[Bindable(true)]
		public string SummaryItemCssClass 
		{
			get { return GetColumnValue<string>(Columns.SummaryItemCssClass); }
			set { SetColumnValue(Columns.SummaryItemCssClass, value); }
		}
		  
		[XmlAttribute("SubListCssClass")]
		[Bindable(true)]
		public string SubListCssClass 
		{
			get { return GetColumnValue<string>(Columns.SubListCssClass); }
			set { SetColumnValue(Columns.SubListCssClass, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn CategoryIdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn SpecialRuleFlagColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn CategoryDisplayNameColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn SummaryItemNameColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn SummaryItemCssClassColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn SubListCssClassColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string CategoryId = @"category_id";
			 public static string SpecialRuleFlag = @"special_rule_flag";
			 public static string CategoryDisplayName = @"category_display_name";
			 public static string SummaryItemName = @"summary_item_name";
			 public static string SummaryItemCssClass = @"summary_item_css_class";
			 public static string SubListCssClass = @"sub_list_css_class";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

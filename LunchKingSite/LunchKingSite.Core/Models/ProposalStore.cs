using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the ProposalStore class.
    /// </summary>
    [Serializable]
    public partial class ProposalStoreCollection : RepositoryList<ProposalStore, ProposalStoreCollection>
    {
        public ProposalStoreCollection() { }

        /// <summary>
        /// Filters an existing collection based on the set criteria. This is an in-memory filter
        /// Thanks to developingchris for this!
        /// </summary>
        /// <returns>ProposalStoreCollection</returns>
        public ProposalStoreCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                ProposalStore o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }


    }
    /// <summary>
    /// This is an ActiveRecord class which wraps the proposal_store table.
    /// </summary>
    [Serializable]
    public partial class ProposalStore : RepositoryRecord<ProposalStore>, IRecordBase
    {
        #region .ctors and Default Settings

        public ProposalStore()
        {
            SetSQLProps();
            InitSetDefaults();
            MarkNew();
        }

        private void InitSetDefaults() { SetDefaults(); }

        public ProposalStore(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
                ForceDefaults();
            MarkNew();
        }

        protected static void SetSQLProps() { GetTableSchema(); }

        #endregion

        #region Schema and Query Accessor
        public static Query CreateQuery() { return new Query(Schema); }
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                    SetSQLProps();
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("proposal_store", TableType.Table, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarProposalId = new TableSchema.TableColumn(schema);
                colvarProposalId.ColumnName = "proposal_id";
                colvarProposalId.DataType = DbType.Int32;
                colvarProposalId.MaxLength = 0;
                colvarProposalId.AutoIncrement = false;
                colvarProposalId.IsNullable = false;
                colvarProposalId.IsPrimaryKey = true;
                colvarProposalId.IsForeignKey = false;
                colvarProposalId.IsReadOnly = false;
                colvarProposalId.DefaultSetting = @"";
                colvarProposalId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarProposalId);

                TableSchema.TableColumn colvarStoreGuid = new TableSchema.TableColumn(schema);
                colvarStoreGuid.ColumnName = "store_guid";
                colvarStoreGuid.DataType = DbType.Guid;
                colvarStoreGuid.MaxLength = 0;
                colvarStoreGuid.AutoIncrement = false;
                colvarStoreGuid.IsNullable = false;
                colvarStoreGuid.IsPrimaryKey = true;
                colvarStoreGuid.IsForeignKey = false;
                colvarStoreGuid.IsReadOnly = false;
                colvarStoreGuid.DefaultSetting = @"";
                colvarStoreGuid.ForeignKeyTableName = "";
                schema.Columns.Add(colvarStoreGuid);

                TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
                colvarCreateId.ColumnName = "create_id";
                colvarCreateId.DataType = DbType.String;
                colvarCreateId.MaxLength = 255;
                colvarCreateId.AutoIncrement = false;
                colvarCreateId.IsNullable = true;
                colvarCreateId.IsPrimaryKey = false;
                colvarCreateId.IsForeignKey = false;
                colvarCreateId.IsReadOnly = false;
                colvarCreateId.DefaultSetting = @"";
                colvarCreateId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateId);

                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = true;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                colvarCreateTime.DefaultSetting = @"";
                colvarCreateTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateTime);

                TableSchema.TableColumn colvarTotalQuantity = new TableSchema.TableColumn(schema);
                colvarTotalQuantity.ColumnName = "total_quantity";
                colvarTotalQuantity.DataType = DbType.Int32;
                colvarTotalQuantity.MaxLength = 0;
                colvarTotalQuantity.AutoIncrement = false;
                colvarTotalQuantity.IsNullable = true;
                colvarTotalQuantity.IsPrimaryKey = false;
                colvarTotalQuantity.IsForeignKey = false;
                colvarTotalQuantity.IsReadOnly = false;
                colvarTotalQuantity.DefaultSetting = @"";
                colvarTotalQuantity.ForeignKeyTableName = "";
                schema.Columns.Add(colvarTotalQuantity);

                TableSchema.TableColumn colvarOrderedQuantity = new TableSchema.TableColumn(schema);
                colvarOrderedQuantity.ColumnName = "ordered_quantity";
                colvarOrderedQuantity.DataType = DbType.Int32;
                colvarOrderedQuantity.MaxLength = 0;
                colvarOrderedQuantity.AutoIncrement = false;
                colvarOrderedQuantity.IsNullable = true;
                colvarOrderedQuantity.IsPrimaryKey = false;
                colvarOrderedQuantity.IsForeignKey = false;
                colvarOrderedQuantity.IsReadOnly = false;
                colvarOrderedQuantity.DefaultSetting = @"";
                colvarOrderedQuantity.ForeignKeyTableName = "";
                schema.Columns.Add(colvarOrderedQuantity);

                TableSchema.TableColumn colvarSortOrder = new TableSchema.TableColumn(schema);
                colvarSortOrder.ColumnName = "sort_order";
                colvarSortOrder.DataType = DbType.Int32;
                colvarSortOrder.MaxLength = 0;
                colvarSortOrder.AutoIncrement = false;
                colvarSortOrder.IsNullable = true;
                colvarSortOrder.IsPrimaryKey = false;
                colvarSortOrder.IsForeignKey = false;
                colvarSortOrder.IsReadOnly = false;
                colvarSortOrder.DefaultSetting = @"";
                colvarSortOrder.ForeignKeyTableName = "";
                schema.Columns.Add(colvarSortOrder);

                TableSchema.TableColumn colvarChangedExpireDate = new TableSchema.TableColumn(schema);
                colvarChangedExpireDate.ColumnName = "changed_expire_date";
                colvarChangedExpireDate.DataType = DbType.DateTime;
                colvarChangedExpireDate.MaxLength = 0;
                colvarChangedExpireDate.AutoIncrement = false;
                colvarChangedExpireDate.IsNullable = true;
                colvarChangedExpireDate.IsPrimaryKey = false;
                colvarChangedExpireDate.IsForeignKey = false;
                colvarChangedExpireDate.IsReadOnly = false;
                colvarChangedExpireDate.DefaultSetting = @"";
                colvarChangedExpireDate.ForeignKeyTableName = "";
                schema.Columns.Add(colvarChangedExpireDate);

                TableSchema.TableColumn colvarCouponSequenceCount = new TableSchema.TableColumn(schema);
                colvarCouponSequenceCount.ColumnName = "coupon_sequence_count";
                colvarCouponSequenceCount.DataType = DbType.Int32;
                colvarCouponSequenceCount.MaxLength = 0;
                colvarCouponSequenceCount.AutoIncrement = false;
                colvarCouponSequenceCount.IsNullable = false;
                colvarCouponSequenceCount.IsPrimaryKey = false;
                colvarCouponSequenceCount.IsForeignKey = false;
                colvarCouponSequenceCount.IsReadOnly = false;

                colvarCouponSequenceCount.DefaultSetting = @"((0))";
                colvarCouponSequenceCount.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCouponSequenceCount);

                TableSchema.TableColumn colvarResourceGuid = new TableSchema.TableColumn(schema);
                colvarResourceGuid.ColumnName = "resource_guid";
                colvarResourceGuid.DataType = DbType.Guid;
                colvarResourceGuid.MaxLength = 0;
                colvarResourceGuid.AutoIncrement = false;
                colvarResourceGuid.IsNullable = false;
                colvarResourceGuid.IsPrimaryKey = false;
                colvarResourceGuid.IsForeignKey = false;
                colvarResourceGuid.IsReadOnly = false;

                colvarResourceGuid.DefaultSetting = @"(newid())";
                colvarResourceGuid.ForeignKeyTableName = "";
                schema.Columns.Add(colvarResourceGuid);

                TableSchema.TableColumn colvarUseTime = new TableSchema.TableColumn(schema);
                colvarUseTime.ColumnName = "use_time";
                colvarUseTime.DataType = DbType.String;
                colvarUseTime.MaxLength = 250;
                colvarUseTime.AutoIncrement = false;
                colvarUseTime.IsNullable = true;
                colvarUseTime.IsPrimaryKey = false;
                colvarUseTime.IsForeignKey = false;
                colvarUseTime.IsReadOnly = false;
                colvarUseTime.DefaultSetting = @"";
                colvarUseTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarUseTime);


                TableSchema.TableColumn colvarVbsRight = new TableSchema.TableColumn(schema);
                colvarVbsRight.ColumnName = "vbs_right";
                colvarVbsRight.DataType = DbType.Int32;
                colvarVbsRight.MaxLength = 0;
                colvarVbsRight.AutoIncrement = false;
                colvarVbsRight.IsNullable = false;
                colvarVbsRight.IsPrimaryKey = false;
                colvarVbsRight.IsForeignKey = false;
                colvarVbsRight.IsReadOnly = false;

                colvarVbsRight.DefaultSetting = @"((1))";
                colvarVbsRight.ForeignKeyTableName = "";
                schema.Columns.Add(colvarVbsRight);

                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("proposal_store", schema);
            }
        }
        #endregion

        #region Props

        [XmlAttribute("ProposalId")]
        [Bindable(true)]
        public int ProposalId
        {
            get { return GetColumnValue<int>(Columns.ProposalId); }
            set { SetColumnValue(Columns.ProposalId, value); }
        }

        [XmlAttribute("StoreGuid")]
        [Bindable(true)]
        public Guid StoreGuid
        {
            get { return GetColumnValue<Guid>(Columns.StoreGuid); }
            set { SetColumnValue(Columns.StoreGuid, value); }
        }

        [XmlAttribute("CreateId")]
        [Bindable(true)]
        public string CreateId
        {
            get { return GetColumnValue<string>(Columns.CreateId); }
            set { SetColumnValue(Columns.CreateId, value); }
        }

        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime? CreateTime
        {
            get { return GetColumnValue<DateTime?>(Columns.CreateTime); }
            set { SetColumnValue(Columns.CreateTime, value); }
        }

        [XmlAttribute("TotalQuantity")]
        [Bindable(true)]
        public int? TotalQuantity
        {
            get { return GetColumnValue<int?>(Columns.TotalQuantity); }
            set { SetColumnValue(Columns.TotalQuantity, value); }
        }

        [XmlAttribute("OrderedQuantity")]
        [Bindable(true)]
        public int? OrderedQuantity
        {
            get { return GetColumnValue<int?>(Columns.OrderedQuantity); }
            set { SetColumnValue(Columns.OrderedQuantity, value); }
        }

        [XmlAttribute("SortOrder")]
        [Bindable(true)]
        public int? SortOrder
        {
            get { return GetColumnValue<int?>(Columns.SortOrder); }
            set { SetColumnValue(Columns.SortOrder, value); }
        }

        [XmlAttribute("ChangedExpireDate")]
        [Bindable(true)]
        public DateTime? ChangedExpireDate
        {
            get { return GetColumnValue<DateTime?>(Columns.ChangedExpireDate); }
            set { SetColumnValue(Columns.ChangedExpireDate, value); }
        }

        [XmlAttribute("CouponSequenceCount")]
        [Bindable(true)]
        public int CouponSequenceCount
        {
            get { return GetColumnValue<int>(Columns.CouponSequenceCount); }
            set { SetColumnValue(Columns.CouponSequenceCount, value); }
        }

        [XmlAttribute("ResourceGuid")]
        [Bindable(true)]
        public Guid ResourceGuid
        {
            get { return GetColumnValue<Guid>(Columns.ResourceGuid); }
            set { SetColumnValue(Columns.ResourceGuid, value); }
        }

        [XmlAttribute("UseTime")]
        [Bindable(true)]
        public string UseTime
        {
            get { return GetColumnValue<string>(Columns.UseTime); }
            set { SetColumnValue(Columns.UseTime, value); }
        }

        [XmlAttribute("VbsRight")]
        [Bindable(true)]
        public int VbsRight
        {
            get { return GetColumnValue<int>(Columns.VbsRight); }
            set { SetColumnValue(Columns.VbsRight, value); }
        }

        #endregion




        //no foreign key tables defined (0)



        //no ManyToMany tables defined (0)





        #region Typed Columns


        public static TableSchema.TableColumn ProposalIdColumn
        {
            get { return Schema.Columns[0]; }
        }



        public static TableSchema.TableColumn StoreGuidColumn
        {
            get { return Schema.Columns[1]; }
        }



        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[2]; }
        }



        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[3]; }
        }



        public static TableSchema.TableColumn TotalQuantityColumn
        {
            get { return Schema.Columns[4]; }
        }



        public static TableSchema.TableColumn OrderedQuantityColumn
        {
            get { return Schema.Columns[5]; }
        }



        public static TableSchema.TableColumn SortOrderColumn
        {
            get { return Schema.Columns[6]; }
        }



        public static TableSchema.TableColumn ChangedExpireDateColumn
        {
            get { return Schema.Columns[7]; }
        }



        public static TableSchema.TableColumn CouponSequenceCountColumn
        {
            get { return Schema.Columns[8]; }
        }



        public static TableSchema.TableColumn ResourceGuidColumn
        {
            get { return Schema.Columns[9]; }
        }



        public static TableSchema.TableColumn UseTimeColumn
        {
            get { return Schema.Columns[10]; }
        }



        public static TableSchema.TableColumn VbsRightColumn
        {
            get { return Schema.Columns[11]; }
        }


        #endregion
        #region Columns Struct
        public struct Columns
        {
            public static string ProposalId = @"proposal_id";
            public static string StoreGuid = @"store_guid";
            public static string CreateId = @"create_id";
            public static string CreateTime = @"create_time";
            public static string TotalQuantity = @"total_quantity";
            public static string OrderedQuantity = @"ordered_quantity";
            public static string SortOrder = @"sort_order";
            public static string ChangedExpireDate = @"changed_expire_date";
            public static string CouponSequenceCount = @"coupon_sequence_count";
            public static string ResourceGuid = @"resource_guid";
            public static string UseTime = @"use_time";
            public static string VbsRight = @"vbs_right";

        }
        #endregion

        #region Update PK Collections

        #endregion

        #region Deep Save

        #endregion
    }
}

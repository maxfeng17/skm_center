using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the EventPromo class.
	/// </summary>
    [Serializable]
	public partial class EventPromoCollection : RepositoryList<EventPromo, EventPromoCollection>
	{	   
		public EventPromoCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>EventPromoCollection</returns>
		public EventPromoCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                EventPromo o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the event_promo table.
	/// </summary>
	[Serializable]
	public partial class EventPromo : RepositoryRecord<EventPromo>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public EventPromo()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public EventPromo(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("event_promo", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "Id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarTitle = new TableSchema.TableColumn(schema);
				colvarTitle.ColumnName = "Title";
				colvarTitle.DataType = DbType.String;
				colvarTitle.MaxLength = 500;
				colvarTitle.AutoIncrement = false;
				colvarTitle.IsNullable = false;
				colvarTitle.IsPrimaryKey = false;
				colvarTitle.IsForeignKey = false;
				colvarTitle.IsReadOnly = false;
				colvarTitle.DefaultSetting = @"";
				colvarTitle.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTitle);
				
				TableSchema.TableColumn colvarUrl = new TableSchema.TableColumn(schema);
				colvarUrl.ColumnName = "Url";
				colvarUrl.DataType = DbType.AnsiString;
				colvarUrl.MaxLength = 100;
				colvarUrl.AutoIncrement = false;
				colvarUrl.IsNullable = false;
				colvarUrl.IsPrimaryKey = false;
				colvarUrl.IsForeignKey = false;
				colvarUrl.IsReadOnly = false;
				colvarUrl.DefaultSetting = @"";
				colvarUrl.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUrl);
				
				TableSchema.TableColumn colvarCpa = new TableSchema.TableColumn(schema);
				colvarCpa.ColumnName = "Cpa";
				colvarCpa.DataType = DbType.AnsiString;
				colvarCpa.MaxLength = 100;
				colvarCpa.AutoIncrement = false;
				colvarCpa.IsNullable = true;
				colvarCpa.IsPrimaryKey = false;
				colvarCpa.IsForeignKey = false;
				colvarCpa.IsReadOnly = false;
				colvarCpa.DefaultSetting = @"";
				colvarCpa.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCpa);
				
				TableSchema.TableColumn colvarStartDate = new TableSchema.TableColumn(schema);
				colvarStartDate.ColumnName = "StartDate";
				colvarStartDate.DataType = DbType.DateTime;
				colvarStartDate.MaxLength = 0;
				colvarStartDate.AutoIncrement = false;
				colvarStartDate.IsNullable = false;
				colvarStartDate.IsPrimaryKey = false;
				colvarStartDate.IsForeignKey = false;
				colvarStartDate.IsReadOnly = false;
				colvarStartDate.DefaultSetting = @"";
				colvarStartDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStartDate);
				
				TableSchema.TableColumn colvarEndDate = new TableSchema.TableColumn(schema);
				colvarEndDate.ColumnName = "EndDate";
				colvarEndDate.DataType = DbType.DateTime;
				colvarEndDate.MaxLength = 0;
				colvarEndDate.AutoIncrement = false;
				colvarEndDate.IsNullable = false;
				colvarEndDate.IsPrimaryKey = false;
				colvarEndDate.IsForeignKey = false;
				colvarEndDate.IsReadOnly = false;
				colvarEndDate.DefaultSetting = @"";
				colvarEndDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEndDate);
				
				TableSchema.TableColumn colvarMainPic = new TableSchema.TableColumn(schema);
				colvarMainPic.ColumnName = "MainPic";
				colvarMainPic.DataType = DbType.String;
				colvarMainPic.MaxLength = 1073741823;
				colvarMainPic.AutoIncrement = false;
				colvarMainPic.IsNullable = false;
				colvarMainPic.IsPrimaryKey = false;
				colvarMainPic.IsForeignKey = false;
				colvarMainPic.IsReadOnly = false;
				colvarMainPic.DefaultSetting = @"";
				colvarMainPic.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMainPic);
				
				TableSchema.TableColumn colvarBgPic = new TableSchema.TableColumn(schema);
				colvarBgPic.ColumnName = "BgPic";
				colvarBgPic.DataType = DbType.AnsiString;
				colvarBgPic.MaxLength = 200;
				colvarBgPic.AutoIncrement = false;
				colvarBgPic.IsNullable = true;
				colvarBgPic.IsPrimaryKey = false;
				colvarBgPic.IsForeignKey = false;
				colvarBgPic.IsReadOnly = false;
				colvarBgPic.DefaultSetting = @"";
				colvarBgPic.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBgPic);
				
				TableSchema.TableColumn colvarBgColor = new TableSchema.TableColumn(schema);
				colvarBgColor.ColumnName = "BgColor";
				colvarBgColor.DataType = DbType.AnsiString;
				colvarBgColor.MaxLength = 20;
				colvarBgColor.AutoIncrement = false;
				colvarBgColor.IsNullable = true;
				colvarBgColor.IsPrimaryKey = false;
				colvarBgColor.IsForeignKey = false;
				colvarBgColor.IsReadOnly = false;
				colvarBgColor.DefaultSetting = @"";
				colvarBgColor.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBgColor);
				
				TableSchema.TableColumn colvarType = new TableSchema.TableColumn(schema);
				colvarType.ColumnName = "Type";
				colvarType.DataType = DbType.Int32;
				colvarType.MaxLength = 0;
				colvarType.AutoIncrement = false;
				colvarType.IsNullable = false;
				colvarType.IsPrimaryKey = false;
				colvarType.IsForeignKey = false;
				colvarType.IsReadOnly = false;
				
						colvarType.DefaultSetting = @"((0))";
				colvarType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarType);
				
				TableSchema.TableColumn colvarDescription = new TableSchema.TableColumn(schema);
				colvarDescription.ColumnName = "Description";
				colvarDescription.DataType = DbType.String;
				colvarDescription.MaxLength = 1073741823;
				colvarDescription.AutoIncrement = false;
				colvarDescription.IsNullable = true;
				colvarDescription.IsPrimaryKey = false;
				colvarDescription.IsForeignKey = false;
				colvarDescription.IsReadOnly = false;
				colvarDescription.DefaultSetting = @"";
				colvarDescription.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDescription);
				
				TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
				colvarStatus.ColumnName = "Status";
				colvarStatus.DataType = DbType.Boolean;
				colvarStatus.MaxLength = 0;
				colvarStatus.AutoIncrement = false;
				colvarStatus.IsNullable = false;
				colvarStatus.IsPrimaryKey = false;
				colvarStatus.IsForeignKey = false;
				colvarStatus.IsReadOnly = false;
				
						colvarStatus.DefaultSetting = @"((1))";
				colvarStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStatus);
				
				TableSchema.TableColumn colvarCreator = new TableSchema.TableColumn(schema);
				colvarCreator.ColumnName = "Creator";
				colvarCreator.DataType = DbType.AnsiString;
				colvarCreator.MaxLength = 100;
				colvarCreator.AutoIncrement = false;
				colvarCreator.IsNullable = false;
				colvarCreator.IsPrimaryKey = false;
				colvarCreator.IsForeignKey = false;
				colvarCreator.IsReadOnly = false;
				colvarCreator.DefaultSetting = @"";
				colvarCreator.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreator);
				
				TableSchema.TableColumn colvarCdt = new TableSchema.TableColumn(schema);
				colvarCdt.ColumnName = "CDT";
				colvarCdt.DataType = DbType.DateTime;
				colvarCdt.MaxLength = 0;
				colvarCdt.AutoIncrement = false;
				colvarCdt.IsNullable = false;
				colvarCdt.IsPrimaryKey = false;
				colvarCdt.IsForeignKey = false;
				colvarCdt.IsReadOnly = false;
				colvarCdt.DefaultSetting = @"";
				colvarCdt.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCdt);
				
				TableSchema.TableColumn colvarMessage = new TableSchema.TableColumn(schema);
				colvarMessage.ColumnName = "Message";
				colvarMessage.DataType = DbType.String;
				colvarMessage.MaxLength = 1073741823;
				colvarMessage.AutoIncrement = false;
				colvarMessage.IsNullable = true;
				colvarMessage.IsPrimaryKey = false;
				colvarMessage.IsForeignKey = false;
				colvarMessage.IsReadOnly = false;
				colvarMessage.DefaultSetting = @"";
				colvarMessage.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMessage);
				
				TableSchema.TableColumn colvarBtnOriginal = new TableSchema.TableColumn(schema);
				colvarBtnOriginal.ColumnName = "btn_original";
				colvarBtnOriginal.DataType = DbType.AnsiString;
				colvarBtnOriginal.MaxLength = 200;
				colvarBtnOriginal.AutoIncrement = false;
				colvarBtnOriginal.IsNullable = true;
				colvarBtnOriginal.IsPrimaryKey = false;
				colvarBtnOriginal.IsForeignKey = false;
				colvarBtnOriginal.IsReadOnly = false;
				colvarBtnOriginal.DefaultSetting = @"";
				colvarBtnOriginal.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBtnOriginal);
				
				TableSchema.TableColumn colvarBtnHover = new TableSchema.TableColumn(schema);
				colvarBtnHover.ColumnName = "btn_hover";
				colvarBtnHover.DataType = DbType.AnsiString;
				colvarBtnHover.MaxLength = 200;
				colvarBtnHover.AutoIncrement = false;
				colvarBtnHover.IsNullable = true;
				colvarBtnHover.IsPrimaryKey = false;
				colvarBtnHover.IsForeignKey = false;
				colvarBtnHover.IsReadOnly = false;
				colvarBtnHover.DefaultSetting = @"";
				colvarBtnHover.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBtnHover);
				
				TableSchema.TableColumn colvarBtnActive = new TableSchema.TableColumn(schema);
				colvarBtnActive.ColumnName = "btn_active";
				colvarBtnActive.DataType = DbType.AnsiString;
				colvarBtnActive.MaxLength = 200;
				colvarBtnActive.AutoIncrement = false;
				colvarBtnActive.IsNullable = true;
				colvarBtnActive.IsPrimaryKey = false;
				colvarBtnActive.IsForeignKey = false;
				colvarBtnActive.IsReadOnly = false;
				colvarBtnActive.DefaultSetting = @"";
				colvarBtnActive.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBtnActive);
				
				TableSchema.TableColumn colvarBtnFontColor = new TableSchema.TableColumn(schema);
				colvarBtnFontColor.ColumnName = "btn_font_color";
				colvarBtnFontColor.DataType = DbType.AnsiString;
				colvarBtnFontColor.MaxLength = 20;
				colvarBtnFontColor.AutoIncrement = false;
				colvarBtnFontColor.IsNullable = true;
				colvarBtnFontColor.IsPrimaryKey = false;
				colvarBtnFontColor.IsForeignKey = false;
				colvarBtnFontColor.IsReadOnly = false;
				colvarBtnFontColor.DefaultSetting = @"";
				colvarBtnFontColor.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBtnFontColor);
				
				TableSchema.TableColumn colvarTemplateType = new TableSchema.TableColumn(schema);
				colvarTemplateType.ColumnName = "template_type";
				colvarTemplateType.DataType = DbType.Int32;
				colvarTemplateType.MaxLength = 0;
				colvarTemplateType.AutoIncrement = false;
				colvarTemplateType.IsNullable = false;
				colvarTemplateType.IsPrimaryKey = false;
				colvarTemplateType.IsForeignKey = false;
				colvarTemplateType.IsReadOnly = false;
				
						colvarTemplateType.DefaultSetting = @"((0))";
				colvarTemplateType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTemplateType);
				
				TableSchema.TableColumn colvarAppBannerImage = new TableSchema.TableColumn(schema);
				colvarAppBannerImage.ColumnName = "app_banner_image";
				colvarAppBannerImage.DataType = DbType.String;
				colvarAppBannerImage.MaxLength = 200;
				colvarAppBannerImage.AutoIncrement = false;
				colvarAppBannerImage.IsNullable = true;
				colvarAppBannerImage.IsPrimaryKey = false;
				colvarAppBannerImage.IsForeignKey = false;
				colvarAppBannerImage.IsReadOnly = false;
				colvarAppBannerImage.DefaultSetting = @"";
				colvarAppBannerImage.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAppBannerImage);
				
				TableSchema.TableColumn colvarAppPromoImage = new TableSchema.TableColumn(schema);
				colvarAppPromoImage.ColumnName = "app_promo_Image";
				colvarAppPromoImage.DataType = DbType.String;
				colvarAppPromoImage.MaxLength = 200;
				colvarAppPromoImage.AutoIncrement = false;
				colvarAppPromoImage.IsNullable = true;
				colvarAppPromoImage.IsPrimaryKey = false;
				colvarAppPromoImage.IsForeignKey = false;
				colvarAppPromoImage.IsReadOnly = false;
				colvarAppPromoImage.DefaultSetting = @"";
				colvarAppPromoImage.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAppPromoImage);
				
				TableSchema.TableColumn colvarCoordinate = new TableSchema.TableColumn(schema);
				colvarCoordinate.ColumnName = "coordinate";
				colvarCoordinate.DataType = DbType.AnsiString;
				colvarCoordinate.MaxLength = -1;
				colvarCoordinate.AutoIncrement = false;
				colvarCoordinate.IsNullable = true;
				colvarCoordinate.IsPrimaryKey = false;
				colvarCoordinate.IsForeignKey = false;
				colvarCoordinate.IsReadOnly = false;
				colvarCoordinate.DefaultSetting = @"";
				colvarCoordinate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCoordinate);
				
				TableSchema.TableColumn colvarParentId = new TableSchema.TableColumn(schema);
				colvarParentId.ColumnName = "parent_id";
				colvarParentId.DataType = DbType.Int32;
				colvarParentId.MaxLength = 0;
				colvarParentId.AutoIncrement = false;
				colvarParentId.IsNullable = true;
				colvarParentId.IsPrimaryKey = false;
				colvarParentId.IsForeignKey = false;
				colvarParentId.IsReadOnly = false;
				colvarParentId.DefaultSetting = @"";
				colvarParentId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarParentId);
				
				TableSchema.TableColumn colvarShowInWeb = new TableSchema.TableColumn(schema);
				colvarShowInWeb.ColumnName = "show_in_web";
				colvarShowInWeb.DataType = DbType.Boolean;
				colvarShowInWeb.MaxLength = 0;
				colvarShowInWeb.AutoIncrement = false;
				colvarShowInWeb.IsNullable = false;
				colvarShowInWeb.IsPrimaryKey = false;
				colvarShowInWeb.IsForeignKey = false;
				colvarShowInWeb.IsReadOnly = false;
				
						colvarShowInWeb.DefaultSetting = @"((1))";
				colvarShowInWeb.ForeignKeyTableName = "";
				schema.Columns.Add(colvarShowInWeb);
				
				TableSchema.TableColumn colvarShowInApp = new TableSchema.TableColumn(schema);
				colvarShowInApp.ColumnName = "show_in_app";
				colvarShowInApp.DataType = DbType.Boolean;
				colvarShowInApp.MaxLength = 0;
				colvarShowInApp.AutoIncrement = false;
				colvarShowInApp.IsNullable = false;
				colvarShowInApp.IsPrimaryKey = false;
				colvarShowInApp.IsForeignKey = false;
				colvarShowInApp.IsReadOnly = false;
				
						colvarShowInApp.DefaultSetting = @"((0))";
				colvarShowInApp.ForeignKeyTableName = "";
				schema.Columns.Add(colvarShowInApp);
				
				TableSchema.TableColumn colvarShowInIdeas = new TableSchema.TableColumn(schema);
				colvarShowInIdeas.ColumnName = "show_in_ideas";
				colvarShowInIdeas.DataType = DbType.Boolean;
				colvarShowInIdeas.MaxLength = 0;
				colvarShowInIdeas.AutoIncrement = false;
				colvarShowInIdeas.IsNullable = false;
				colvarShowInIdeas.IsPrimaryKey = false;
				colvarShowInIdeas.IsForeignKey = false;
				colvarShowInIdeas.IsReadOnly = false;
				
						colvarShowInIdeas.DefaultSetting = @"((0))";
				colvarShowInIdeas.ForeignKeyTableName = "";
				schema.Columns.Add(colvarShowInIdeas);
				
				TableSchema.TableColumn colvarBindCategoryList = new TableSchema.TableColumn(schema);
				colvarBindCategoryList.ColumnName = "bind_category_list";
				colvarBindCategoryList.DataType = DbType.String;
				colvarBindCategoryList.MaxLength = 30;
				colvarBindCategoryList.AutoIncrement = false;
				colvarBindCategoryList.IsNullable = true;
				colvarBindCategoryList.IsPrimaryKey = false;
				colvarBindCategoryList.IsForeignKey = false;
				colvarBindCategoryList.IsReadOnly = false;
				colvarBindCategoryList.DefaultSetting = @"";
				colvarBindCategoryList.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBindCategoryList);
				
				TableSchema.TableColumn colvarDealPromoDescription = new TableSchema.TableColumn(schema);
				colvarDealPromoDescription.ColumnName = "deal_promo_description";
				colvarDealPromoDescription.DataType = DbType.String;
				colvarDealPromoDescription.MaxLength = -1;
				colvarDealPromoDescription.AutoIncrement = false;
				colvarDealPromoDescription.IsNullable = true;
				colvarDealPromoDescription.IsPrimaryKey = false;
				colvarDealPromoDescription.IsForeignKey = false;
				colvarDealPromoDescription.IsReadOnly = false;
				colvarDealPromoDescription.DefaultSetting = @"";
				colvarDealPromoDescription.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDealPromoDescription);
				
				TableSchema.TableColumn colvarDealPromoTitle = new TableSchema.TableColumn(schema);
				colvarDealPromoTitle.ColumnName = "deal_promo_title";
				colvarDealPromoTitle.DataType = DbType.String;
				colvarDealPromoTitle.MaxLength = 50;
				colvarDealPromoTitle.AutoIncrement = false;
				colvarDealPromoTitle.IsNullable = true;
				colvarDealPromoTitle.IsPrimaryKey = false;
				colvarDealPromoTitle.IsForeignKey = false;
				colvarDealPromoTitle.IsReadOnly = false;
				colvarDealPromoTitle.DefaultSetting = @"";
				colvarDealPromoTitle.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDealPromoTitle);
				
				TableSchema.TableColumn colvarDealPromoImage = new TableSchema.TableColumn(schema);
				colvarDealPromoImage.ColumnName = "deal_promo_image";
				colvarDealPromoImage.DataType = DbType.String;
				colvarDealPromoImage.MaxLength = 120;
				colvarDealPromoImage.AutoIncrement = false;
				colvarDealPromoImage.IsNullable = true;
				colvarDealPromoImage.IsPrimaryKey = false;
				colvarDealPromoImage.IsForeignKey = false;
				colvarDealPromoImage.IsReadOnly = false;
				colvarDealPromoImage.DefaultSetting = @"";
				colvarDealPromoImage.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDealPromoImage);
				
				TableSchema.TableColumn colvarMobileMainPic = new TableSchema.TableColumn(schema);
				colvarMobileMainPic.ColumnName = "mobile_main_pic";
				colvarMobileMainPic.DataType = DbType.String;
				colvarMobileMainPic.MaxLength = -1;
				colvarMobileMainPic.AutoIncrement = false;
				colvarMobileMainPic.IsNullable = true;
				colvarMobileMainPic.IsPrimaryKey = false;
				colvarMobileMainPic.IsForeignKey = false;
				colvarMobileMainPic.IsReadOnly = false;
				colvarMobileMainPic.DefaultSetting = @"";
				colvarMobileMainPic.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMobileMainPic);
				
				TableSchema.TableColumn colvarSubCategoryBgColor = new TableSchema.TableColumn(schema);
				colvarSubCategoryBgColor.ColumnName = "sub_category_bg_color";
				colvarSubCategoryBgColor.DataType = DbType.AnsiString;
				colvarSubCategoryBgColor.MaxLength = 6;
				colvarSubCategoryBgColor.AutoIncrement = false;
				colvarSubCategoryBgColor.IsNullable = true;
				colvarSubCategoryBgColor.IsPrimaryKey = false;
				colvarSubCategoryBgColor.IsForeignKey = false;
				colvarSubCategoryBgColor.IsReadOnly = false;
				colvarSubCategoryBgColor.DefaultSetting = @"";
				colvarSubCategoryBgColor.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSubCategoryBgColor);
				
				TableSchema.TableColumn colvarSubCategoryFontColor = new TableSchema.TableColumn(schema);
				colvarSubCategoryFontColor.ColumnName = "sub_category_font_color";
				colvarSubCategoryFontColor.DataType = DbType.AnsiString;
				colvarSubCategoryFontColor.MaxLength = 6;
				colvarSubCategoryFontColor.AutoIncrement = false;
				colvarSubCategoryFontColor.IsNullable = true;
				colvarSubCategoryFontColor.IsPrimaryKey = false;
				colvarSubCategoryFontColor.IsForeignKey = false;
				colvarSubCategoryFontColor.IsReadOnly = false;
				colvarSubCategoryFontColor.DefaultSetting = @"";
				colvarSubCategoryFontColor.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSubCategoryFontColor);
				
				TableSchema.TableColumn colvarSubCategoryShowImage = new TableSchema.TableColumn(schema);
				colvarSubCategoryShowImage.ColumnName = "sub_category_show_image";
				colvarSubCategoryShowImage.DataType = DbType.Boolean;
				colvarSubCategoryShowImage.MaxLength = 0;
				colvarSubCategoryShowImage.AutoIncrement = false;
				colvarSubCategoryShowImage.IsNullable = false;
				colvarSubCategoryShowImage.IsPrimaryKey = false;
				colvarSubCategoryShowImage.IsForeignKey = false;
				colvarSubCategoryShowImage.IsReadOnly = false;
				
						colvarSubCategoryShowImage.DefaultSetting = @"((0))";
				colvarSubCategoryShowImage.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSubCategoryShowImage);
				
				TableSchema.TableColumn colvarSubCategoryBtnColorOriginal = new TableSchema.TableColumn(schema);
				colvarSubCategoryBtnColorOriginal.ColumnName = "sub_category_btn_color_original";
				colvarSubCategoryBtnColorOriginal.DataType = DbType.AnsiString;
				colvarSubCategoryBtnColorOriginal.MaxLength = 6;
				colvarSubCategoryBtnColorOriginal.AutoIncrement = false;
				colvarSubCategoryBtnColorOriginal.IsNullable = true;
				colvarSubCategoryBtnColorOriginal.IsPrimaryKey = false;
				colvarSubCategoryBtnColorOriginal.IsForeignKey = false;
				colvarSubCategoryBtnColorOriginal.IsReadOnly = false;
				colvarSubCategoryBtnColorOriginal.DefaultSetting = @"";
				colvarSubCategoryBtnColorOriginal.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSubCategoryBtnColorOriginal);
				
				TableSchema.TableColumn colvarSubCategoryBtnColorHover = new TableSchema.TableColumn(schema);
				colvarSubCategoryBtnColorHover.ColumnName = "sub_category_btn_color_hover";
				colvarSubCategoryBtnColorHover.DataType = DbType.AnsiString;
				colvarSubCategoryBtnColorHover.MaxLength = 6;
				colvarSubCategoryBtnColorHover.AutoIncrement = false;
				colvarSubCategoryBtnColorHover.IsNullable = true;
				colvarSubCategoryBtnColorHover.IsPrimaryKey = false;
				colvarSubCategoryBtnColorHover.IsForeignKey = false;
				colvarSubCategoryBtnColorHover.IsReadOnly = false;
				colvarSubCategoryBtnColorHover.DefaultSetting = @"";
				colvarSubCategoryBtnColorHover.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSubCategoryBtnColorHover);
				
				TableSchema.TableColumn colvarSubCategoryBtnColorActive = new TableSchema.TableColumn(schema);
				colvarSubCategoryBtnColorActive.ColumnName = "sub_category_btn_color_active";
				colvarSubCategoryBtnColorActive.DataType = DbType.AnsiString;
				colvarSubCategoryBtnColorActive.MaxLength = 6;
				colvarSubCategoryBtnColorActive.AutoIncrement = false;
				colvarSubCategoryBtnColorActive.IsNullable = true;
				colvarSubCategoryBtnColorActive.IsPrimaryKey = false;
				colvarSubCategoryBtnColorActive.IsForeignKey = false;
				colvarSubCategoryBtnColorActive.IsReadOnly = false;
				colvarSubCategoryBtnColorActive.DefaultSetting = @"";
				colvarSubCategoryBtnColorActive.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSubCategoryBtnColorActive);
				
				TableSchema.TableColumn colvarSubCategoryBtnImageOriginal = new TableSchema.TableColumn(schema);
				colvarSubCategoryBtnImageOriginal.ColumnName = "sub_category_btn_image_original";
				colvarSubCategoryBtnImageOriginal.DataType = DbType.AnsiString;
				colvarSubCategoryBtnImageOriginal.MaxLength = 150;
				colvarSubCategoryBtnImageOriginal.AutoIncrement = false;
				colvarSubCategoryBtnImageOriginal.IsNullable = true;
				colvarSubCategoryBtnImageOriginal.IsPrimaryKey = false;
				colvarSubCategoryBtnImageOriginal.IsForeignKey = false;
				colvarSubCategoryBtnImageOriginal.IsReadOnly = false;
				colvarSubCategoryBtnImageOriginal.DefaultSetting = @"";
				colvarSubCategoryBtnImageOriginal.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSubCategoryBtnImageOriginal);
				
				TableSchema.TableColumn colvarSubCategoryBtnImageHover = new TableSchema.TableColumn(schema);
				colvarSubCategoryBtnImageHover.ColumnName = "sub_category_btn_image_hover";
				colvarSubCategoryBtnImageHover.DataType = DbType.AnsiString;
				colvarSubCategoryBtnImageHover.MaxLength = 150;
				colvarSubCategoryBtnImageHover.AutoIncrement = false;
				colvarSubCategoryBtnImageHover.IsNullable = true;
				colvarSubCategoryBtnImageHover.IsPrimaryKey = false;
				colvarSubCategoryBtnImageHover.IsForeignKey = false;
				colvarSubCategoryBtnImageHover.IsReadOnly = false;
				colvarSubCategoryBtnImageHover.DefaultSetting = @"";
				colvarSubCategoryBtnImageHover.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSubCategoryBtnImageHover);
				
				TableSchema.TableColumn colvarSubCategoryBtnImageActive = new TableSchema.TableColumn(schema);
				colvarSubCategoryBtnImageActive.ColumnName = "sub_category_btn_image_active";
				colvarSubCategoryBtnImageActive.DataType = DbType.AnsiString;
				colvarSubCategoryBtnImageActive.MaxLength = 150;
				colvarSubCategoryBtnImageActive.AutoIncrement = false;
				colvarSubCategoryBtnImageActive.IsNullable = true;
				colvarSubCategoryBtnImageActive.IsPrimaryKey = false;
				colvarSubCategoryBtnImageActive.IsForeignKey = false;
				colvarSubCategoryBtnImageActive.IsReadOnly = false;
				colvarSubCategoryBtnImageActive.DefaultSetting = @"";
				colvarSubCategoryBtnImageActive.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSubCategoryBtnImageActive);
				
				TableSchema.TableColumn colvarSubCategoryBtnFontOriginal = new TableSchema.TableColumn(schema);
				colvarSubCategoryBtnFontOriginal.ColumnName = "sub_category_btn_font_original";
				colvarSubCategoryBtnFontOriginal.DataType = DbType.AnsiString;
				colvarSubCategoryBtnFontOriginal.MaxLength = 6;
				colvarSubCategoryBtnFontOriginal.AutoIncrement = false;
				colvarSubCategoryBtnFontOriginal.IsNullable = true;
				colvarSubCategoryBtnFontOriginal.IsPrimaryKey = false;
				colvarSubCategoryBtnFontOriginal.IsForeignKey = false;
				colvarSubCategoryBtnFontOriginal.IsReadOnly = false;
				colvarSubCategoryBtnFontOriginal.DefaultSetting = @"";
				colvarSubCategoryBtnFontOriginal.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSubCategoryBtnFontOriginal);
				
				TableSchema.TableColumn colvarSubCategoryBtnFontHover = new TableSchema.TableColumn(schema);
				colvarSubCategoryBtnFontHover.ColumnName = "sub_category_btn_font_hover";
				colvarSubCategoryBtnFontHover.DataType = DbType.AnsiString;
				colvarSubCategoryBtnFontHover.MaxLength = 6;
				colvarSubCategoryBtnFontHover.AutoIncrement = false;
				colvarSubCategoryBtnFontHover.IsNullable = true;
				colvarSubCategoryBtnFontHover.IsPrimaryKey = false;
				colvarSubCategoryBtnFontHover.IsForeignKey = false;
				colvarSubCategoryBtnFontHover.IsReadOnly = false;
				colvarSubCategoryBtnFontHover.DefaultSetting = @"";
				colvarSubCategoryBtnFontHover.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSubCategoryBtnFontHover);
				
				TableSchema.TableColumn colvarSubCategoryBtnFontActive = new TableSchema.TableColumn(schema);
				colvarSubCategoryBtnFontActive.ColumnName = "sub_category_btn_font_active";
				colvarSubCategoryBtnFontActive.DataType = DbType.AnsiString;
				colvarSubCategoryBtnFontActive.MaxLength = 6;
				colvarSubCategoryBtnFontActive.AutoIncrement = false;
				colvarSubCategoryBtnFontActive.IsNullable = true;
				colvarSubCategoryBtnFontActive.IsPrimaryKey = false;
				colvarSubCategoryBtnFontActive.IsForeignKey = false;
				colvarSubCategoryBtnFontActive.IsReadOnly = false;
				colvarSubCategoryBtnFontActive.DefaultSetting = @"";
				colvarSubCategoryBtnFontActive.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSubCategoryBtnFontActive);
				
				TableSchema.TableColumn colvarBtnFontColorHover = new TableSchema.TableColumn(schema);
				colvarBtnFontColorHover.ColumnName = "btn_font_color_hover";
				colvarBtnFontColorHover.DataType = DbType.AnsiString;
				colvarBtnFontColorHover.MaxLength = 6;
				colvarBtnFontColorHover.AutoIncrement = false;
				colvarBtnFontColorHover.IsNullable = true;
				colvarBtnFontColorHover.IsPrimaryKey = false;
				colvarBtnFontColorHover.IsForeignKey = false;
				colvarBtnFontColorHover.IsReadOnly = false;
				colvarBtnFontColorHover.DefaultSetting = @"";
				colvarBtnFontColorHover.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBtnFontColorHover);
				
				TableSchema.TableColumn colvarBtnFontColorActive = new TableSchema.TableColumn(schema);
				colvarBtnFontColorActive.ColumnName = "btn_font_color_active";
				colvarBtnFontColorActive.DataType = DbType.AnsiString;
				colvarBtnFontColorActive.MaxLength = 6;
				colvarBtnFontColorActive.AutoIncrement = false;
				colvarBtnFontColorActive.IsNullable = true;
				colvarBtnFontColorActive.IsPrimaryKey = false;
				colvarBtnFontColorActive.IsForeignKey = false;
				colvarBtnFontColorActive.IsReadOnly = false;
				colvarBtnFontColorActive.DefaultSetting = @"";
				colvarBtnFontColorActive.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBtnFontColorActive);
				
				TableSchema.TableColumn colvarVoteCycleType = new TableSchema.TableColumn(schema);
				colvarVoteCycleType.ColumnName = "vote_cycle_type";
				colvarVoteCycleType.DataType = DbType.Byte;
				colvarVoteCycleType.MaxLength = 0;
				colvarVoteCycleType.AutoIncrement = false;
				colvarVoteCycleType.IsNullable = false;
				colvarVoteCycleType.IsPrimaryKey = false;
				colvarVoteCycleType.IsForeignKey = false;
				colvarVoteCycleType.IsReadOnly = false;
				
						colvarVoteCycleType.DefaultSetting = @"((0))";
				colvarVoteCycleType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarVoteCycleType);
				
				TableSchema.TableColumn colvarMaxVotes = new TableSchema.TableColumn(schema);
				colvarMaxVotes.ColumnName = "max_votes";
				colvarMaxVotes.DataType = DbType.Int32;
				colvarMaxVotes.MaxLength = 0;
				colvarMaxVotes.AutoIncrement = false;
				colvarMaxVotes.IsNullable = false;
				colvarMaxVotes.IsPrimaryKey = false;
				colvarMaxVotes.IsForeignKey = false;
				colvarMaxVotes.IsReadOnly = false;
				
						colvarMaxVotes.DefaultSetting = @"((0))";
				colvarMaxVotes.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMaxVotes);
				
				TableSchema.TableColumn colvarCampaignId = new TableSchema.TableColumn(schema);
				colvarCampaignId.ColumnName = "campaign_id";
				colvarCampaignId.DataType = DbType.Int32;
				colvarCampaignId.MaxLength = 0;
				colvarCampaignId.AutoIncrement = false;
				colvarCampaignId.IsNullable = false;
				colvarCampaignId.IsPrimaryKey = false;
				colvarCampaignId.IsForeignKey = false;
				colvarCampaignId.IsReadOnly = false;
				
						colvarCampaignId.DefaultSetting = @"((0))";
				colvarCampaignId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCampaignId);
				
				TableSchema.TableColumn colvarEventPromoTitle = new TableSchema.TableColumn(schema);
				colvarEventPromoTitle.ColumnName = "event_promo_title";
				colvarEventPromoTitle.DataType = DbType.String;
				colvarEventPromoTitle.MaxLength = 60;
				colvarEventPromoTitle.AutoIncrement = false;
				colvarEventPromoTitle.IsNullable = true;
				colvarEventPromoTitle.IsPrimaryKey = false;
				colvarEventPromoTitle.IsForeignKey = false;
				colvarEventPromoTitle.IsReadOnly = false;
				colvarEventPromoTitle.DefaultSetting = @"";
				colvarEventPromoTitle.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEventPromoTitle);
				
				TableSchema.TableColumn colvarEventIntroduction = new TableSchema.TableColumn(schema);
				colvarEventIntroduction.ColumnName = "event_introduction";
				colvarEventIntroduction.DataType = DbType.String;
				colvarEventIntroduction.MaxLength = -1;
				colvarEventIntroduction.AutoIncrement = false;
				colvarEventIntroduction.IsNullable = true;
				colvarEventIntroduction.IsPrimaryKey = false;
				colvarEventIntroduction.IsForeignKey = false;
				colvarEventIntroduction.IsReadOnly = false;
				colvarEventIntroduction.DefaultSetting = @"";
				colvarEventIntroduction.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEventIntroduction);
				
				TableSchema.TableColumn colvarEventType = new TableSchema.TableColumn(schema);
				colvarEventType.ColumnName = "event_type";
				colvarEventType.DataType = DbType.Int32;
				colvarEventType.MaxLength = 0;
				colvarEventType.AutoIncrement = false;
				colvarEventType.IsNullable = false;
				colvarEventType.IsPrimaryKey = false;
				colvarEventType.IsForeignKey = false;
				colvarEventType.IsReadOnly = false;
				
						colvarEventType.DefaultSetting = @"((0))";
				colvarEventType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEventType);
				
				TableSchema.TableColumn colvarBannerType = new TableSchema.TableColumn(schema);
				colvarBannerType.ColumnName = "banner_type";
				colvarBannerType.DataType = DbType.Int32;
				colvarBannerType.MaxLength = 0;
				colvarBannerType.AutoIncrement = false;
				colvarBannerType.IsNullable = false;
				colvarBannerType.IsPrimaryKey = false;
				colvarBannerType.IsForeignKey = false;
				colvarBannerType.IsReadOnly = false;
				
						colvarBannerType.DefaultSetting = @"((0))";
				colvarBannerType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBannerType);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("event_promo",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("Title")]
		[Bindable(true)]
		public string Title 
		{
			get { return GetColumnValue<string>(Columns.Title); }
			set { SetColumnValue(Columns.Title, value); }
		}
		  
		[XmlAttribute("Url")]
		[Bindable(true)]
		public string Url 
		{
			get { return GetColumnValue<string>(Columns.Url); }
			set { SetColumnValue(Columns.Url, value); }
		}
		  
		[XmlAttribute("Cpa")]
		[Bindable(true)]
		public string Cpa 
		{
			get { return GetColumnValue<string>(Columns.Cpa); }
			set { SetColumnValue(Columns.Cpa, value); }
		}
		  
		[XmlAttribute("StartDate")]
		[Bindable(true)]
		public DateTime StartDate 
		{
			get { return GetColumnValue<DateTime>(Columns.StartDate); }
			set { SetColumnValue(Columns.StartDate, value); }
		}
		  
		[XmlAttribute("EndDate")]
		[Bindable(true)]
		public DateTime EndDate 
		{
			get { return GetColumnValue<DateTime>(Columns.EndDate); }
			set { SetColumnValue(Columns.EndDate, value); }
		}
		  
		[XmlAttribute("MainPic")]
		[Bindable(true)]
		public string MainPic 
		{
			get { return GetColumnValue<string>(Columns.MainPic); }
			set { SetColumnValue(Columns.MainPic, value); }
		}
		  
		[XmlAttribute("BgPic")]
		[Bindable(true)]
		public string BgPic 
		{
			get { return GetColumnValue<string>(Columns.BgPic); }
			set { SetColumnValue(Columns.BgPic, value); }
		}
		  
		[XmlAttribute("BgColor")]
		[Bindable(true)]
		public string BgColor 
		{
			get { return GetColumnValue<string>(Columns.BgColor); }
			set { SetColumnValue(Columns.BgColor, value); }
		}
		  
		[XmlAttribute("Type")]
		[Bindable(true)]
		public int Type 
		{
			get { return GetColumnValue<int>(Columns.Type); }
			set { SetColumnValue(Columns.Type, value); }
		}
		  
		[XmlAttribute("Description")]
		[Bindable(true)]
		public string Description 
		{
			get { return GetColumnValue<string>(Columns.Description); }
			set { SetColumnValue(Columns.Description, value); }
		}
		  
		[XmlAttribute("Status")]
		[Bindable(true)]
		public bool Status 
		{
			get { return GetColumnValue<bool>(Columns.Status); }
			set { SetColumnValue(Columns.Status, value); }
		}
		  
		[XmlAttribute("Creator")]
		[Bindable(true)]
		public string Creator 
		{
			get { return GetColumnValue<string>(Columns.Creator); }
			set { SetColumnValue(Columns.Creator, value); }
		}
		  
		[XmlAttribute("Cdt")]
		[Bindable(true)]
		public DateTime Cdt 
		{
			get { return GetColumnValue<DateTime>(Columns.Cdt); }
			set { SetColumnValue(Columns.Cdt, value); }
		}
		  
		[XmlAttribute("Message")]
		[Bindable(true)]
		public string Message 
		{
			get { return GetColumnValue<string>(Columns.Message); }
			set { SetColumnValue(Columns.Message, value); }
		}
		  
		[XmlAttribute("BtnOriginal")]
		[Bindable(true)]
		public string BtnOriginal 
		{
			get { return GetColumnValue<string>(Columns.BtnOriginal); }
			set { SetColumnValue(Columns.BtnOriginal, value); }
		}
		  
		[XmlAttribute("BtnHover")]
		[Bindable(true)]
		public string BtnHover 
		{
			get { return GetColumnValue<string>(Columns.BtnHover); }
			set { SetColumnValue(Columns.BtnHover, value); }
		}
		  
		[XmlAttribute("BtnActive")]
		[Bindable(true)]
		public string BtnActive 
		{
			get { return GetColumnValue<string>(Columns.BtnActive); }
			set { SetColumnValue(Columns.BtnActive, value); }
		}
		  
		[XmlAttribute("BtnFontColor")]
		[Bindable(true)]
		public string BtnFontColor 
		{
			get { return GetColumnValue<string>(Columns.BtnFontColor); }
			set { SetColumnValue(Columns.BtnFontColor, value); }
		}
		  
		[XmlAttribute("TemplateType")]
		[Bindable(true)]
		public int TemplateType 
		{
			get { return GetColumnValue<int>(Columns.TemplateType); }
			set { SetColumnValue(Columns.TemplateType, value); }
		}
		  
		[XmlAttribute("AppBannerImage")]
		[Bindable(true)]
		public string AppBannerImage 
		{
			get { return GetColumnValue<string>(Columns.AppBannerImage); }
			set { SetColumnValue(Columns.AppBannerImage, value); }
		}
		  
		[XmlAttribute("AppPromoImage")]
		[Bindable(true)]
		public string AppPromoImage 
		{
			get { return GetColumnValue<string>(Columns.AppPromoImage); }
			set { SetColumnValue(Columns.AppPromoImage, value); }
		}
		  
		[XmlAttribute("Coordinate")]
		[Bindable(true)]
		public string Coordinate 
		{
			get { return GetColumnValue<string>(Columns.Coordinate); }
			set { SetColumnValue(Columns.Coordinate, value); }
		}
		  
		[XmlAttribute("ParentId")]
		[Bindable(true)]
		public int? ParentId 
		{
			get { return GetColumnValue<int?>(Columns.ParentId); }
			set { SetColumnValue(Columns.ParentId, value); }
		}
		  
		[XmlAttribute("ShowInWeb")]
		[Bindable(true)]
		public bool ShowInWeb 
		{
			get { return GetColumnValue<bool>(Columns.ShowInWeb); }
			set { SetColumnValue(Columns.ShowInWeb, value); }
		}
		  
		[XmlAttribute("ShowInApp")]
		[Bindable(true)]
		public bool ShowInApp 
		{
			get { return GetColumnValue<bool>(Columns.ShowInApp); }
			set { SetColumnValue(Columns.ShowInApp, value); }
		}
		  
		[XmlAttribute("ShowInIdeas")]
		[Bindable(true)]
		public bool ShowInIdeas 
		{
			get { return GetColumnValue<bool>(Columns.ShowInIdeas); }
			set { SetColumnValue(Columns.ShowInIdeas, value); }
		}
		  
		[XmlAttribute("BindCategoryList")]
		[Bindable(true)]
		public string BindCategoryList 
		{
			get { return GetColumnValue<string>(Columns.BindCategoryList); }
			set { SetColumnValue(Columns.BindCategoryList, value); }
		}
		  
		[XmlAttribute("DealPromoDescription")]
		[Bindable(true)]
		public string DealPromoDescription 
		{
			get { return GetColumnValue<string>(Columns.DealPromoDescription); }
			set { SetColumnValue(Columns.DealPromoDescription, value); }
		}
		  
		[XmlAttribute("DealPromoTitle")]
		[Bindable(true)]
		public string DealPromoTitle 
		{
			get { return GetColumnValue<string>(Columns.DealPromoTitle); }
			set { SetColumnValue(Columns.DealPromoTitle, value); }
		}
		  
		[XmlAttribute("DealPromoImage")]
		[Bindable(true)]
		public string DealPromoImage 
		{
			get { return GetColumnValue<string>(Columns.DealPromoImage); }
			set { SetColumnValue(Columns.DealPromoImage, value); }
		}
		  
		[XmlAttribute("MobileMainPic")]
		[Bindable(true)]
		public string MobileMainPic 
		{
			get { return GetColumnValue<string>(Columns.MobileMainPic); }
			set { SetColumnValue(Columns.MobileMainPic, value); }
		}
		  
		[XmlAttribute("SubCategoryBgColor")]
		[Bindable(true)]
		public string SubCategoryBgColor 
		{
			get { return GetColumnValue<string>(Columns.SubCategoryBgColor); }
			set { SetColumnValue(Columns.SubCategoryBgColor, value); }
		}
		  
		[XmlAttribute("SubCategoryFontColor")]
		[Bindable(true)]
		public string SubCategoryFontColor 
		{
			get { return GetColumnValue<string>(Columns.SubCategoryFontColor); }
			set { SetColumnValue(Columns.SubCategoryFontColor, value); }
		}
		  
		[XmlAttribute("SubCategoryShowImage")]
		[Bindable(true)]
		public bool SubCategoryShowImage 
		{
			get { return GetColumnValue<bool>(Columns.SubCategoryShowImage); }
			set { SetColumnValue(Columns.SubCategoryShowImage, value); }
		}
		  
		[XmlAttribute("SubCategoryBtnColorOriginal")]
		[Bindable(true)]
		public string SubCategoryBtnColorOriginal 
		{
			get { return GetColumnValue<string>(Columns.SubCategoryBtnColorOriginal); }
			set { SetColumnValue(Columns.SubCategoryBtnColorOriginal, value); }
		}
		  
		[XmlAttribute("SubCategoryBtnColorHover")]
		[Bindable(true)]
		public string SubCategoryBtnColorHover 
		{
			get { return GetColumnValue<string>(Columns.SubCategoryBtnColorHover); }
			set { SetColumnValue(Columns.SubCategoryBtnColorHover, value); }
		}
		  
		[XmlAttribute("SubCategoryBtnColorActive")]
		[Bindable(true)]
		public string SubCategoryBtnColorActive 
		{
			get { return GetColumnValue<string>(Columns.SubCategoryBtnColorActive); }
			set { SetColumnValue(Columns.SubCategoryBtnColorActive, value); }
		}
		  
		[XmlAttribute("SubCategoryBtnImageOriginal")]
		[Bindable(true)]
		public string SubCategoryBtnImageOriginal 
		{
			get { return GetColumnValue<string>(Columns.SubCategoryBtnImageOriginal); }
			set { SetColumnValue(Columns.SubCategoryBtnImageOriginal, value); }
		}
		  
		[XmlAttribute("SubCategoryBtnImageHover")]
		[Bindable(true)]
		public string SubCategoryBtnImageHover 
		{
			get { return GetColumnValue<string>(Columns.SubCategoryBtnImageHover); }
			set { SetColumnValue(Columns.SubCategoryBtnImageHover, value); }
		}
		  
		[XmlAttribute("SubCategoryBtnImageActive")]
		[Bindable(true)]
		public string SubCategoryBtnImageActive 
		{
			get { return GetColumnValue<string>(Columns.SubCategoryBtnImageActive); }
			set { SetColumnValue(Columns.SubCategoryBtnImageActive, value); }
		}
		  
		[XmlAttribute("SubCategoryBtnFontOriginal")]
		[Bindable(true)]
		public string SubCategoryBtnFontOriginal 
		{
			get { return GetColumnValue<string>(Columns.SubCategoryBtnFontOriginal); }
			set { SetColumnValue(Columns.SubCategoryBtnFontOriginal, value); }
		}
		  
		[XmlAttribute("SubCategoryBtnFontHover")]
		[Bindable(true)]
		public string SubCategoryBtnFontHover 
		{
			get { return GetColumnValue<string>(Columns.SubCategoryBtnFontHover); }
			set { SetColumnValue(Columns.SubCategoryBtnFontHover, value); }
		}
		  
		[XmlAttribute("SubCategoryBtnFontActive")]
		[Bindable(true)]
		public string SubCategoryBtnFontActive 
		{
			get { return GetColumnValue<string>(Columns.SubCategoryBtnFontActive); }
			set { SetColumnValue(Columns.SubCategoryBtnFontActive, value); }
		}
		  
		[XmlAttribute("BtnFontColorHover")]
		[Bindable(true)]
		public string BtnFontColorHover 
		{
			get { return GetColumnValue<string>(Columns.BtnFontColorHover); }
			set { SetColumnValue(Columns.BtnFontColorHover, value); }
		}
		  
		[XmlAttribute("BtnFontColorActive")]
		[Bindable(true)]
		public string BtnFontColorActive 
		{
			get { return GetColumnValue<string>(Columns.BtnFontColorActive); }
			set { SetColumnValue(Columns.BtnFontColorActive, value); }
		}
		  
		[XmlAttribute("VoteCycleType")]
		[Bindable(true)]
		public byte VoteCycleType 
		{
			get { return GetColumnValue<byte>(Columns.VoteCycleType); }
			set { SetColumnValue(Columns.VoteCycleType, value); }
		}
		  
		[XmlAttribute("MaxVotes")]
		[Bindable(true)]
		public int MaxVotes 
		{
			get { return GetColumnValue<int>(Columns.MaxVotes); }
			set { SetColumnValue(Columns.MaxVotes, value); }
		}
		  
		[XmlAttribute("CampaignId")]
		[Bindable(true)]
		public int CampaignId 
		{
			get { return GetColumnValue<int>(Columns.CampaignId); }
			set { SetColumnValue(Columns.CampaignId, value); }
		}
		  
		[XmlAttribute("EventPromoTitle")]
		[Bindable(true)]
		public string EventPromoTitle 
		{
			get { return GetColumnValue<string>(Columns.EventPromoTitle); }
			set { SetColumnValue(Columns.EventPromoTitle, value); }
		}
		  
		[XmlAttribute("EventIntroduction")]
		[Bindable(true)]
		public string EventIntroduction 
		{
			get { return GetColumnValue<string>(Columns.EventIntroduction); }
			set { SetColumnValue(Columns.EventIntroduction, value); }
		}
		  
		[XmlAttribute("EventType")]
		[Bindable(true)]
		public int EventType 
		{
			get { return GetColumnValue<int>(Columns.EventType); }
			set { SetColumnValue(Columns.EventType, value); }
		}
		  
		[XmlAttribute("BannerType")]
		[Bindable(true)]
		public int BannerType 
		{
			get { return GetColumnValue<int>(Columns.BannerType); }
			set { SetColumnValue(Columns.BannerType, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn TitleColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn UrlColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn CpaColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn StartDateColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn EndDateColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn MainPicColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn BgPicColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn BgColorColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn TypeColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn DescriptionColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        public static TableSchema.TableColumn StatusColumn
        {
            get { return Schema.Columns[11]; }
        }
        
        
        
        public static TableSchema.TableColumn CreatorColumn
        {
            get { return Schema.Columns[12]; }
        }
        
        
        
        public static TableSchema.TableColumn CdtColumn
        {
            get { return Schema.Columns[13]; }
        }
        
        
        
        public static TableSchema.TableColumn MessageColumn
        {
            get { return Schema.Columns[14]; }
        }
        
        
        
        public static TableSchema.TableColumn BtnOriginalColumn
        {
            get { return Schema.Columns[15]; }
        }
        
        
        
        public static TableSchema.TableColumn BtnHoverColumn
        {
            get { return Schema.Columns[16]; }
        }
        
        
        
        public static TableSchema.TableColumn BtnActiveColumn
        {
            get { return Schema.Columns[17]; }
        }
        
        
        
        public static TableSchema.TableColumn BtnFontColorColumn
        {
            get { return Schema.Columns[18]; }
        }
        
        
        
        public static TableSchema.TableColumn TemplateTypeColumn
        {
            get { return Schema.Columns[19]; }
        }
        
        
        
        public static TableSchema.TableColumn AppBannerImageColumn
        {
            get { return Schema.Columns[20]; }
        }
        
        
        
        public static TableSchema.TableColumn AppPromoImageColumn
        {
            get { return Schema.Columns[21]; }
        }
        
        
        
        public static TableSchema.TableColumn CoordinateColumn
        {
            get { return Schema.Columns[22]; }
        }
        
        
        
        public static TableSchema.TableColumn ParentIdColumn
        {
            get { return Schema.Columns[23]; }
        }
        
        
        
        public static TableSchema.TableColumn ShowInWebColumn
        {
            get { return Schema.Columns[24]; }
        }
        
        
        
        public static TableSchema.TableColumn ShowInAppColumn
        {
            get { return Schema.Columns[25]; }
        }
        
        
        
        public static TableSchema.TableColumn ShowInIdeasColumn
        {
            get { return Schema.Columns[26]; }
        }
        
        
        
        public static TableSchema.TableColumn BindCategoryListColumn
        {
            get { return Schema.Columns[27]; }
        }
        
        
        
        public static TableSchema.TableColumn DealPromoDescriptionColumn
        {
            get { return Schema.Columns[28]; }
        }
        
        
        
        public static TableSchema.TableColumn DealPromoTitleColumn
        {
            get { return Schema.Columns[29]; }
        }
        
        
        
        public static TableSchema.TableColumn DealPromoImageColumn
        {
            get { return Schema.Columns[30]; }
        }
        
        
        
        public static TableSchema.TableColumn MobileMainPicColumn
        {
            get { return Schema.Columns[31]; }
        }
        
        
        
        public static TableSchema.TableColumn SubCategoryBgColorColumn
        {
            get { return Schema.Columns[32]; }
        }
        
        
        
        public static TableSchema.TableColumn SubCategoryFontColorColumn
        {
            get { return Schema.Columns[33]; }
        }
        
        
        
        public static TableSchema.TableColumn SubCategoryShowImageColumn
        {
            get { return Schema.Columns[34]; }
        }
        
        
        
        public static TableSchema.TableColumn SubCategoryBtnColorOriginalColumn
        {
            get { return Schema.Columns[35]; }
        }
        
        
        
        public static TableSchema.TableColumn SubCategoryBtnColorHoverColumn
        {
            get { return Schema.Columns[36]; }
        }
        
        
        
        public static TableSchema.TableColumn SubCategoryBtnColorActiveColumn
        {
            get { return Schema.Columns[37]; }
        }
        
        
        
        public static TableSchema.TableColumn SubCategoryBtnImageOriginalColumn
        {
            get { return Schema.Columns[38]; }
        }
        
        
        
        public static TableSchema.TableColumn SubCategoryBtnImageHoverColumn
        {
            get { return Schema.Columns[39]; }
        }
        
        
        
        public static TableSchema.TableColumn SubCategoryBtnImageActiveColumn
        {
            get { return Schema.Columns[40]; }
        }
        
        
        
        public static TableSchema.TableColumn SubCategoryBtnFontOriginalColumn
        {
            get { return Schema.Columns[41]; }
        }
        
        
        
        public static TableSchema.TableColumn SubCategoryBtnFontHoverColumn
        {
            get { return Schema.Columns[42]; }
        }
        
        
        
        public static TableSchema.TableColumn SubCategoryBtnFontActiveColumn
        {
            get { return Schema.Columns[43]; }
        }
        
        
        
        public static TableSchema.TableColumn BtnFontColorHoverColumn
        {
            get { return Schema.Columns[44]; }
        }
        
        
        
        public static TableSchema.TableColumn BtnFontColorActiveColumn
        {
            get { return Schema.Columns[45]; }
        }
        
        
        
        public static TableSchema.TableColumn VoteCycleTypeColumn
        {
            get { return Schema.Columns[46]; }
        }
        
        
        
        public static TableSchema.TableColumn MaxVotesColumn
        {
            get { return Schema.Columns[47]; }
        }
        
        
        
        public static TableSchema.TableColumn CampaignIdColumn
        {
            get { return Schema.Columns[48]; }
        }
        
        
        
        public static TableSchema.TableColumn EventPromoTitleColumn
        {
            get { return Schema.Columns[49]; }
        }
        
        
        
        public static TableSchema.TableColumn EventIntroductionColumn
        {
            get { return Schema.Columns[50]; }
        }
        
        
        
        public static TableSchema.TableColumn EventTypeColumn
        {
            get { return Schema.Columns[51]; }
        }
        
        
        
        public static TableSchema.TableColumn BannerTypeColumn
        {
            get { return Schema.Columns[52]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"Id";
			 public static string Title = @"Title";
			 public static string Url = @"Url";
			 public static string Cpa = @"Cpa";
			 public static string StartDate = @"StartDate";
			 public static string EndDate = @"EndDate";
			 public static string MainPic = @"MainPic";
			 public static string BgPic = @"BgPic";
			 public static string BgColor = @"BgColor";
			 public static string Type = @"Type";
			 public static string Description = @"Description";
			 public static string Status = @"Status";
			 public static string Creator = @"Creator";
			 public static string Cdt = @"CDT";
			 public static string Message = @"Message";
			 public static string BtnOriginal = @"btn_original";
			 public static string BtnHover = @"btn_hover";
			 public static string BtnActive = @"btn_active";
			 public static string BtnFontColor = @"btn_font_color";
			 public static string TemplateType = @"template_type";
			 public static string AppBannerImage = @"app_banner_image";
			 public static string AppPromoImage = @"app_promo_Image";
			 public static string Coordinate = @"coordinate";
			 public static string ParentId = @"parent_id";
			 public static string ShowInWeb = @"show_in_web";
			 public static string ShowInApp = @"show_in_app";
			 public static string ShowInIdeas = @"show_in_ideas";
			 public static string BindCategoryList = @"bind_category_list";
			 public static string DealPromoDescription = @"deal_promo_description";
			 public static string DealPromoTitle = @"deal_promo_title";
			 public static string DealPromoImage = @"deal_promo_image";
			 public static string MobileMainPic = @"mobile_main_pic";
			 public static string SubCategoryBgColor = @"sub_category_bg_color";
			 public static string SubCategoryFontColor = @"sub_category_font_color";
			 public static string SubCategoryShowImage = @"sub_category_show_image";
			 public static string SubCategoryBtnColorOriginal = @"sub_category_btn_color_original";
			 public static string SubCategoryBtnColorHover = @"sub_category_btn_color_hover";
			 public static string SubCategoryBtnColorActive = @"sub_category_btn_color_active";
			 public static string SubCategoryBtnImageOriginal = @"sub_category_btn_image_original";
			 public static string SubCategoryBtnImageHover = @"sub_category_btn_image_hover";
			 public static string SubCategoryBtnImageActive = @"sub_category_btn_image_active";
			 public static string SubCategoryBtnFontOriginal = @"sub_category_btn_font_original";
			 public static string SubCategoryBtnFontHover = @"sub_category_btn_font_hover";
			 public static string SubCategoryBtnFontActive = @"sub_category_btn_font_active";
			 public static string BtnFontColorHover = @"btn_font_color_hover";
			 public static string BtnFontColorActive = @"btn_font_color_active";
			 public static string VoteCycleType = @"vote_cycle_type";
			 public static string MaxVotes = @"max_votes";
			 public static string CampaignId = @"campaign_id";
			 public static string EventPromoTitle = @"event_promo_title";
			 public static string EventIntroduction = @"event_introduction";
			 public static string EventType = @"event_type";
			 public static string BannerType = @"banner_type";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

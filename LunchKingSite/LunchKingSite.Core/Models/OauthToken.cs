using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the OauthToken class.
	/// </summary>
    [Serializable]
	public partial class OauthTokenCollection : RepositoryList<OauthToken, OauthTokenCollection>
	{	   
		public OauthTokenCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>OauthTokenCollection</returns>
		public OauthTokenCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                OauthToken o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the oauth_token table.
	/// </summary>
	[Serializable]
	public partial class OauthToken : RepositoryRecord<OauthToken>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public OauthToken()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public OauthToken(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("oauth_token", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarCode = new TableSchema.TableColumn(schema);
				colvarCode.ColumnName = "code";
				colvarCode.DataType = DbType.String;
				colvarCode.MaxLength = 200;
				colvarCode.AutoIncrement = false;
				colvarCode.IsNullable = true;
				colvarCode.IsPrimaryKey = false;
				colvarCode.IsForeignKey = false;
				colvarCode.IsReadOnly = false;
				colvarCode.DefaultSetting = @"";
				colvarCode.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCode);
				
				TableSchema.TableColumn colvarIsCodeExchanged = new TableSchema.TableColumn(schema);
				colvarIsCodeExchanged.ColumnName = "is_code_exchanged";
				colvarIsCodeExchanged.DataType = DbType.Boolean;
				colvarIsCodeExchanged.MaxLength = 0;
				colvarIsCodeExchanged.AutoIncrement = false;
				colvarIsCodeExchanged.IsNullable = false;
				colvarIsCodeExchanged.IsPrimaryKey = false;
				colvarIsCodeExchanged.IsForeignKey = false;
				colvarIsCodeExchanged.IsReadOnly = false;
				colvarIsCodeExchanged.DefaultSetting = @"";
				colvarIsCodeExchanged.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsCodeExchanged);
				
				TableSchema.TableColumn colvarAccessToken = new TableSchema.TableColumn(schema);
				colvarAccessToken.ColumnName = "access_token";
				colvarAccessToken.DataType = DbType.String;
				colvarAccessToken.MaxLength = 100;
				colvarAccessToken.AutoIncrement = false;
				colvarAccessToken.IsNullable = true;
				colvarAccessToken.IsPrimaryKey = false;
				colvarAccessToken.IsForeignKey = false;
				colvarAccessToken.IsReadOnly = false;
				colvarAccessToken.DefaultSetting = @"";
				colvarAccessToken.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAccessToken);
				
				TableSchema.TableColumn colvarUserId = new TableSchema.TableColumn(schema);
				colvarUserId.ColumnName = "user_id";
				colvarUserId.DataType = DbType.Int32;
				colvarUserId.MaxLength = 0;
				colvarUserId.AutoIncrement = false;
				colvarUserId.IsNullable = true;
				colvarUserId.IsPrimaryKey = false;
				colvarUserId.IsForeignKey = false;
				colvarUserId.IsReadOnly = false;
				colvarUserId.DefaultSetting = @"";
				colvarUserId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUserId);
				
				TableSchema.TableColumn colvarAppId = new TableSchema.TableColumn(schema);
				colvarAppId.ColumnName = "app_id";
				colvarAppId.DataType = DbType.String;
				colvarAppId.MaxLength = 100;
				colvarAppId.AutoIncrement = false;
				colvarAppId.IsNullable = true;
				colvarAppId.IsPrimaryKey = false;
				colvarAppId.IsForeignKey = false;
				colvarAppId.IsReadOnly = false;
				colvarAppId.DefaultSetting = @"";
				colvarAppId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAppId);
				
				TableSchema.TableColumn colvarTarget = new TableSchema.TableColumn(schema);
				colvarTarget.ColumnName = "target";
				colvarTarget.DataType = DbType.Int32;
				colvarTarget.MaxLength = 0;
				colvarTarget.AutoIncrement = false;
				colvarTarget.IsNullable = false;
				colvarTarget.IsPrimaryKey = false;
				colvarTarget.IsForeignKey = false;
				colvarTarget.IsReadOnly = false;
				colvarTarget.DefaultSetting = @"";
				colvarTarget.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTarget);
				
				TableSchema.TableColumn colvarExpiredTime = new TableSchema.TableColumn(schema);
				colvarExpiredTime.ColumnName = "expired_time";
				colvarExpiredTime.DataType = DbType.DateTime;
				colvarExpiredTime.MaxLength = 0;
				colvarExpiredTime.AutoIncrement = false;
				colvarExpiredTime.IsNullable = false;
				colvarExpiredTime.IsPrimaryKey = false;
				colvarExpiredTime.IsForeignKey = false;
				colvarExpiredTime.IsReadOnly = false;
				colvarExpiredTime.DefaultSetting = @"";
				colvarExpiredTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarExpiredTime);
				
				TableSchema.TableColumn colvarIsValid = new TableSchema.TableColumn(schema);
				colvarIsValid.ColumnName = "is_valid";
				colvarIsValid.DataType = DbType.Boolean;
				colvarIsValid.MaxLength = 0;
				colvarIsValid.AutoIncrement = false;
				colvarIsValid.IsNullable = false;
				colvarIsValid.IsPrimaryKey = false;
				colvarIsValid.IsForeignKey = false;
				colvarIsValid.IsReadOnly = false;
				colvarIsValid.DefaultSetting = @"";
				colvarIsValid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsValid);
				
				TableSchema.TableColumn colvarCreatedTime = new TableSchema.TableColumn(schema);
				colvarCreatedTime.ColumnName = "created_time";
				colvarCreatedTime.DataType = DbType.DateTime;
				colvarCreatedTime.MaxLength = 0;
				colvarCreatedTime.AutoIncrement = false;
				colvarCreatedTime.IsNullable = false;
				colvarCreatedTime.IsPrimaryKey = false;
				colvarCreatedTime.IsForeignKey = false;
				colvarCreatedTime.IsReadOnly = false;
				colvarCreatedTime.DefaultSetting = @"";
				colvarCreatedTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreatedTime);
				
				TableSchema.TableColumn colvarModifiedTime = new TableSchema.TableColumn(schema);
				colvarModifiedTime.ColumnName = "modified_time";
				colvarModifiedTime.DataType = DbType.DateTime;
				colvarModifiedTime.MaxLength = 0;
				colvarModifiedTime.AutoIncrement = false;
				colvarModifiedTime.IsNullable = true;
				colvarModifiedTime.IsPrimaryKey = false;
				colvarModifiedTime.IsForeignKey = false;
				colvarModifiedTime.IsReadOnly = false;
				colvarModifiedTime.DefaultSetting = @"";
				colvarModifiedTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifiedTime);
				
				TableSchema.TableColumn colvarRefreshToken = new TableSchema.TableColumn(schema);
				colvarRefreshToken.ColumnName = "refresh_token";
				colvarRefreshToken.DataType = DbType.String;
				colvarRefreshToken.MaxLength = 100;
				colvarRefreshToken.AutoIncrement = false;
				colvarRefreshToken.IsNullable = true;
				colvarRefreshToken.IsPrimaryKey = false;
				colvarRefreshToken.IsForeignKey = false;
				colvarRefreshToken.IsReadOnly = false;
				colvarRefreshToken.DefaultSetting = @"";
				colvarRefreshToken.ForeignKeyTableName = "";
				schema.Columns.Add(colvarRefreshToken);
				
				TableSchema.TableColumn colvarIsTokenRefreshed = new TableSchema.TableColumn(schema);
				colvarIsTokenRefreshed.ColumnName = "is_token_refreshed";
				colvarIsTokenRefreshed.DataType = DbType.Boolean;
				colvarIsTokenRefreshed.MaxLength = 0;
				colvarIsTokenRefreshed.AutoIncrement = false;
				colvarIsTokenRefreshed.IsNullable = false;
				colvarIsTokenRefreshed.IsPrimaryKey = false;
				colvarIsTokenRefreshed.IsForeignKey = false;
				colvarIsTokenRefreshed.IsReadOnly = false;
				colvarIsTokenRefreshed.DefaultSetting = @"";
				colvarIsTokenRefreshed.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsTokenRefreshed);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("oauth_token",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("Code")]
		[Bindable(true)]
		public string Code 
		{
			get { return GetColumnValue<string>(Columns.Code); }
			set { SetColumnValue(Columns.Code, value); }
		}
		  
		[XmlAttribute("IsCodeExchanged")]
		[Bindable(true)]
		public bool IsCodeExchanged 
		{
			get { return GetColumnValue<bool>(Columns.IsCodeExchanged); }
			set { SetColumnValue(Columns.IsCodeExchanged, value); }
		}
		  
		[XmlAttribute("AccessToken")]
		[Bindable(true)]
		public string AccessToken 
		{
			get { return GetColumnValue<string>(Columns.AccessToken); }
			set { SetColumnValue(Columns.AccessToken, value); }
		}
		  
		[XmlAttribute("UserId")]
		[Bindable(true)]
		public int? UserId 
		{
			get { return GetColumnValue<int?>(Columns.UserId); }
			set { SetColumnValue(Columns.UserId, value); }
		}
		  
		[XmlAttribute("AppId")]
		[Bindable(true)]
		public string AppId 
		{
			get { return GetColumnValue<string>(Columns.AppId); }
			set { SetColumnValue(Columns.AppId, value); }
		}
		  
		[XmlAttribute("Target")]
		[Bindable(true)]
		public int Target 
		{
			get { return GetColumnValue<int>(Columns.Target); }
			set { SetColumnValue(Columns.Target, value); }
		}
		  
		[XmlAttribute("ExpiredTime")]
		[Bindable(true)]
		public DateTime ExpiredTime 
		{
			get { return GetColumnValue<DateTime>(Columns.ExpiredTime); }
			set { SetColumnValue(Columns.ExpiredTime, value); }
		}
		  
		[XmlAttribute("IsValid")]
		[Bindable(true)]
		public bool IsValid 
		{
			get { return GetColumnValue<bool>(Columns.IsValid); }
			set { SetColumnValue(Columns.IsValid, value); }
		}
		  
		[XmlAttribute("CreatedTime")]
		[Bindable(true)]
		public DateTime CreatedTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreatedTime); }
			set { SetColumnValue(Columns.CreatedTime, value); }
		}
		  
		[XmlAttribute("ModifiedTime")]
		[Bindable(true)]
		public DateTime? ModifiedTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.ModifiedTime); }
			set { SetColumnValue(Columns.ModifiedTime, value); }
		}
		  
		[XmlAttribute("RefreshToken")]
		[Bindable(true)]
		public string RefreshToken 
		{
			get { return GetColumnValue<string>(Columns.RefreshToken); }
			set { SetColumnValue(Columns.RefreshToken, value); }
		}
		  
		[XmlAttribute("IsTokenRefreshed")]
		[Bindable(true)]
		public bool IsTokenRefreshed 
		{
			get { return GetColumnValue<bool>(Columns.IsTokenRefreshed); }
			set { SetColumnValue(Columns.IsTokenRefreshed, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn CodeColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn IsCodeExchangedColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn AccessTokenColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn UserIdColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn AppIdColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn TargetColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn ExpiredTimeColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn IsValidColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn CreatedTimeColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifiedTimeColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        public static TableSchema.TableColumn RefreshTokenColumn
        {
            get { return Schema.Columns[11]; }
        }
        
        
        
        public static TableSchema.TableColumn IsTokenRefreshedColumn
        {
            get { return Schema.Columns[12]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string Code = @"code";
			 public static string IsCodeExchanged = @"is_code_exchanged";
			 public static string AccessToken = @"access_token";
			 public static string UserId = @"user_id";
			 public static string AppId = @"app_id";
			 public static string Target = @"target";
			 public static string ExpiredTime = @"expired_time";
			 public static string IsValid = @"is_valid";
			 public static string CreatedTime = @"created_time";
			 public static string ModifiedTime = @"modified_time";
			 public static string RefreshToken = @"refresh_token";
			 public static string IsTokenRefreshed = @"is_token_refreshed";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the IdeasStation class.
	/// </summary>
    [Serializable]
	public partial class IdeasStationCollection : RepositoryList<IdeasStation, IdeasStationCollection>
	{	   
		public IdeasStationCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>IdeasStationCollection</returns>
		public IdeasStationCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                IdeasStation o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the IDEAS_Station table.
	/// </summary>
	[Serializable]
	public partial class IdeasStation : RepositoryRecord<IdeasStation>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public IdeasStation()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public IdeasStation(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("IDEAS_Station", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "Id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = false;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarTitle = new TableSchema.TableColumn(schema);
				colvarTitle.ColumnName = "Title";
				colvarTitle.DataType = DbType.String;
				colvarTitle.MaxLength = 50;
				colvarTitle.AutoIncrement = false;
				colvarTitle.IsNullable = false;
				colvarTitle.IsPrimaryKey = false;
				colvarTitle.IsForeignKey = false;
				colvarTitle.IsReadOnly = false;
				colvarTitle.DefaultSetting = @"";
				colvarTitle.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTitle);
				
				TableSchema.TableColumn colvarCouponEventUrl = new TableSchema.TableColumn(schema);
				colvarCouponEventUrl.ColumnName = "CouponEventUrl";
				colvarCouponEventUrl.DataType = DbType.AnsiString;
				colvarCouponEventUrl.MaxLength = 50;
				colvarCouponEventUrl.AutoIncrement = false;
				colvarCouponEventUrl.IsNullable = true;
				colvarCouponEventUrl.IsPrimaryKey = false;
				colvarCouponEventUrl.IsForeignKey = false;
				colvarCouponEventUrl.IsReadOnly = false;
				colvarCouponEventUrl.DefaultSetting = @"";
				colvarCouponEventUrl.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCouponEventUrl);
				
				TableSchema.TableColumn colvarCouponDealBroadcastMinutes = new TableSchema.TableColumn(schema);
				colvarCouponDealBroadcastMinutes.ColumnName = "CouponDealBroadcastMinutes";
				colvarCouponDealBroadcastMinutes.DataType = DbType.Byte;
				colvarCouponDealBroadcastMinutes.MaxLength = 0;
				colvarCouponDealBroadcastMinutes.AutoIncrement = false;
				colvarCouponDealBroadcastMinutes.IsNullable = true;
				colvarCouponDealBroadcastMinutes.IsPrimaryKey = false;
				colvarCouponDealBroadcastMinutes.IsForeignKey = false;
				colvarCouponDealBroadcastMinutes.IsReadOnly = false;
				colvarCouponDealBroadcastMinutes.DefaultSetting = @"";
				colvarCouponDealBroadcastMinutes.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCouponDealBroadcastMinutes);
				
				TableSchema.TableColumn colvarDeliveryEventUrl = new TableSchema.TableColumn(schema);
				colvarDeliveryEventUrl.ColumnName = "DeliveryEventUrl";
				colvarDeliveryEventUrl.DataType = DbType.AnsiString;
				colvarDeliveryEventUrl.MaxLength = 50;
				colvarDeliveryEventUrl.AutoIncrement = false;
				colvarDeliveryEventUrl.IsNullable = true;
				colvarDeliveryEventUrl.IsPrimaryKey = false;
				colvarDeliveryEventUrl.IsForeignKey = false;
				colvarDeliveryEventUrl.IsReadOnly = false;
				colvarDeliveryEventUrl.DefaultSetting = @"";
				colvarDeliveryEventUrl.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDeliveryEventUrl);
				
				TableSchema.TableColumn colvarDeliveryDealBroadcastMinutes = new TableSchema.TableColumn(schema);
				colvarDeliveryDealBroadcastMinutes.ColumnName = "DeliveryDealBroadcastMinutes";
				colvarDeliveryDealBroadcastMinutes.DataType = DbType.Byte;
				colvarDeliveryDealBroadcastMinutes.MaxLength = 0;
				colvarDeliveryDealBroadcastMinutes.AutoIncrement = false;
				colvarDeliveryDealBroadcastMinutes.IsNullable = true;
				colvarDeliveryDealBroadcastMinutes.IsPrimaryKey = false;
				colvarDeliveryDealBroadcastMinutes.IsForeignKey = false;
				colvarDeliveryDealBroadcastMinutes.IsReadOnly = false;
				colvarDeliveryDealBroadcastMinutes.DefaultSetting = @"";
				colvarDeliveryDealBroadcastMinutes.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDeliveryDealBroadcastMinutes);
				
				TableSchema.TableColumn colvarSpecialEventUrl = new TableSchema.TableColumn(schema);
				colvarSpecialEventUrl.ColumnName = "SpecialEventUrl";
				colvarSpecialEventUrl.DataType = DbType.AnsiString;
				colvarSpecialEventUrl.MaxLength = 50;
				colvarSpecialEventUrl.AutoIncrement = false;
				colvarSpecialEventUrl.IsNullable = true;
				colvarSpecialEventUrl.IsPrimaryKey = false;
				colvarSpecialEventUrl.IsForeignKey = false;
				colvarSpecialEventUrl.IsReadOnly = false;
				colvarSpecialEventUrl.DefaultSetting = @"";
				colvarSpecialEventUrl.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSpecialEventUrl);
				
				TableSchema.TableColumn colvarSpecialDealBroadcastTimeStartTimeFormat1 = new TableSchema.TableColumn(schema);
				colvarSpecialDealBroadcastTimeStartTimeFormat1.ColumnName = "SpecialDealBroadcastTimeStartTimeFormat1";
				colvarSpecialDealBroadcastTimeStartTimeFormat1.DataType = DbType.AnsiString;
				colvarSpecialDealBroadcastTimeStartTimeFormat1.MaxLength = 50;
				colvarSpecialDealBroadcastTimeStartTimeFormat1.AutoIncrement = false;
				colvarSpecialDealBroadcastTimeStartTimeFormat1.IsNullable = true;
				colvarSpecialDealBroadcastTimeStartTimeFormat1.IsPrimaryKey = false;
				colvarSpecialDealBroadcastTimeStartTimeFormat1.IsForeignKey = false;
				colvarSpecialDealBroadcastTimeStartTimeFormat1.IsReadOnly = false;
				colvarSpecialDealBroadcastTimeStartTimeFormat1.DefaultSetting = @"";
				colvarSpecialDealBroadcastTimeStartTimeFormat1.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSpecialDealBroadcastTimeStartTimeFormat1);
				
				TableSchema.TableColumn colvarSpecialDealBroadcastTimeEndTimeFormat1 = new TableSchema.TableColumn(schema);
				colvarSpecialDealBroadcastTimeEndTimeFormat1.ColumnName = "SpecialDealBroadcastTimeEndTimeFormat1";
				colvarSpecialDealBroadcastTimeEndTimeFormat1.DataType = DbType.AnsiString;
				colvarSpecialDealBroadcastTimeEndTimeFormat1.MaxLength = 50;
				colvarSpecialDealBroadcastTimeEndTimeFormat1.AutoIncrement = false;
				colvarSpecialDealBroadcastTimeEndTimeFormat1.IsNullable = true;
				colvarSpecialDealBroadcastTimeEndTimeFormat1.IsPrimaryKey = false;
				colvarSpecialDealBroadcastTimeEndTimeFormat1.IsForeignKey = false;
				colvarSpecialDealBroadcastTimeEndTimeFormat1.IsReadOnly = false;
				colvarSpecialDealBroadcastTimeEndTimeFormat1.DefaultSetting = @"";
				colvarSpecialDealBroadcastTimeEndTimeFormat1.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSpecialDealBroadcastTimeEndTimeFormat1);
				
				TableSchema.TableColumn colvarSpecialDealBroadcastTimeStartTimeFormat2 = new TableSchema.TableColumn(schema);
				colvarSpecialDealBroadcastTimeStartTimeFormat2.ColumnName = "SpecialDealBroadcastTimeStartTimeFormat2";
				colvarSpecialDealBroadcastTimeStartTimeFormat2.DataType = DbType.AnsiString;
				colvarSpecialDealBroadcastTimeStartTimeFormat2.MaxLength = 50;
				colvarSpecialDealBroadcastTimeStartTimeFormat2.AutoIncrement = false;
				colvarSpecialDealBroadcastTimeStartTimeFormat2.IsNullable = true;
				colvarSpecialDealBroadcastTimeStartTimeFormat2.IsPrimaryKey = false;
				colvarSpecialDealBroadcastTimeStartTimeFormat2.IsForeignKey = false;
				colvarSpecialDealBroadcastTimeStartTimeFormat2.IsReadOnly = false;
				colvarSpecialDealBroadcastTimeStartTimeFormat2.DefaultSetting = @"";
				colvarSpecialDealBroadcastTimeStartTimeFormat2.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSpecialDealBroadcastTimeStartTimeFormat2);
				
				TableSchema.TableColumn colvarSpecialDealBroadcastTimeEndTimeFormat2 = new TableSchema.TableColumn(schema);
				colvarSpecialDealBroadcastTimeEndTimeFormat2.ColumnName = "SpecialDealBroadcastTimeEndTimeFormat2";
				colvarSpecialDealBroadcastTimeEndTimeFormat2.DataType = DbType.AnsiString;
				colvarSpecialDealBroadcastTimeEndTimeFormat2.MaxLength = 50;
				colvarSpecialDealBroadcastTimeEndTimeFormat2.AutoIncrement = false;
				colvarSpecialDealBroadcastTimeEndTimeFormat2.IsNullable = true;
				colvarSpecialDealBroadcastTimeEndTimeFormat2.IsPrimaryKey = false;
				colvarSpecialDealBroadcastTimeEndTimeFormat2.IsForeignKey = false;
				colvarSpecialDealBroadcastTimeEndTimeFormat2.IsReadOnly = false;
				colvarSpecialDealBroadcastTimeEndTimeFormat2.DefaultSetting = @"";
				colvarSpecialDealBroadcastTimeEndTimeFormat2.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSpecialDealBroadcastTimeEndTimeFormat2);
				
				TableSchema.TableColumn colvarVourcherEventUrl = new TableSchema.TableColumn(schema);
				colvarVourcherEventUrl.ColumnName = "VourcherEventUrl";
				colvarVourcherEventUrl.DataType = DbType.AnsiString;
				colvarVourcherEventUrl.MaxLength = 50;
				colvarVourcherEventUrl.AutoIncrement = false;
				colvarVourcherEventUrl.IsNullable = true;
				colvarVourcherEventUrl.IsPrimaryKey = false;
				colvarVourcherEventUrl.IsForeignKey = false;
				colvarVourcherEventUrl.IsReadOnly = false;
				colvarVourcherEventUrl.DefaultSetting = @"";
				colvarVourcherEventUrl.ForeignKeyTableName = "";
				schema.Columns.Add(colvarVourcherEventUrl);
				
				TableSchema.TableColumn colvarVourcherStoreBroadcastMinutes = new TableSchema.TableColumn(schema);
				colvarVourcherStoreBroadcastMinutes.ColumnName = "VourcherStoreBroadcastMinutes";
				colvarVourcherStoreBroadcastMinutes.DataType = DbType.Byte;
				colvarVourcherStoreBroadcastMinutes.MaxLength = 0;
				colvarVourcherStoreBroadcastMinutes.AutoIncrement = false;
				colvarVourcherStoreBroadcastMinutes.IsNullable = true;
				colvarVourcherStoreBroadcastMinutes.IsPrimaryKey = false;
				colvarVourcherStoreBroadcastMinutes.IsForeignKey = false;
				colvarVourcherStoreBroadcastMinutes.IsReadOnly = false;
				colvarVourcherStoreBroadcastMinutes.DefaultSetting = @"";
				colvarVourcherStoreBroadcastMinutes.ForeignKeyTableName = "";
				schema.Columns.Add(colvarVourcherStoreBroadcastMinutes);
				
				TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
				colvarStatus.ColumnName = "Status";
				colvarStatus.DataType = DbType.Boolean;
				colvarStatus.MaxLength = 0;
				colvarStatus.AutoIncrement = false;
				colvarStatus.IsNullable = false;
				colvarStatus.IsPrimaryKey = false;
				colvarStatus.IsForeignKey = false;
				colvarStatus.IsReadOnly = false;
				colvarStatus.DefaultSetting = @"";
				colvarStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStatus);
				
				TableSchema.TableColumn colvarCreator = new TableSchema.TableColumn(schema);
				colvarCreator.ColumnName = "Creator";
				colvarCreator.DataType = DbType.String;
				colvarCreator.MaxLength = 50;
				colvarCreator.AutoIncrement = false;
				colvarCreator.IsNullable = false;
				colvarCreator.IsPrimaryKey = false;
				colvarCreator.IsForeignKey = false;
				colvarCreator.IsReadOnly = false;
				colvarCreator.DefaultSetting = @"";
				colvarCreator.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreator);
				
				TableSchema.TableColumn colvarCreateDate = new TableSchema.TableColumn(schema);
				colvarCreateDate.ColumnName = "CreateDate";
				colvarCreateDate.DataType = DbType.DateTime;
				colvarCreateDate.MaxLength = 0;
				colvarCreateDate.AutoIncrement = false;
				colvarCreateDate.IsNullable = false;
				colvarCreateDate.IsPrimaryKey = false;
				colvarCreateDate.IsForeignKey = false;
				colvarCreateDate.IsReadOnly = false;
				
						colvarCreateDate.DefaultSetting = @"(getdate())";
				colvarCreateDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateDate);
				
				TableSchema.TableColumn colvarModifyDate = new TableSchema.TableColumn(schema);
				colvarModifyDate.ColumnName = "ModifyDate";
				colvarModifyDate.DataType = DbType.DateTime;
				colvarModifyDate.MaxLength = 0;
				colvarModifyDate.AutoIncrement = false;
				colvarModifyDate.IsNullable = true;
				colvarModifyDate.IsPrimaryKey = false;
				colvarModifyDate.IsForeignKey = false;
				colvarModifyDate.IsReadOnly = false;
				colvarModifyDate.DefaultSetting = @"";
				colvarModifyDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyDate);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("IDEAS_Station",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("Title")]
		[Bindable(true)]
		public string Title 
		{
			get { return GetColumnValue<string>(Columns.Title); }
			set { SetColumnValue(Columns.Title, value); }
		}
		  
		[XmlAttribute("CouponEventUrl")]
		[Bindable(true)]
		public string CouponEventUrl 
		{
			get { return GetColumnValue<string>(Columns.CouponEventUrl); }
			set { SetColumnValue(Columns.CouponEventUrl, value); }
		}
		  
		[XmlAttribute("CouponDealBroadcastMinutes")]
		[Bindable(true)]
		public byte? CouponDealBroadcastMinutes 
		{
			get { return GetColumnValue<byte?>(Columns.CouponDealBroadcastMinutes); }
			set { SetColumnValue(Columns.CouponDealBroadcastMinutes, value); }
		}
		  
		[XmlAttribute("DeliveryEventUrl")]
		[Bindable(true)]
		public string DeliveryEventUrl 
		{
			get { return GetColumnValue<string>(Columns.DeliveryEventUrl); }
			set { SetColumnValue(Columns.DeliveryEventUrl, value); }
		}
		  
		[XmlAttribute("DeliveryDealBroadcastMinutes")]
		[Bindable(true)]
		public byte? DeliveryDealBroadcastMinutes 
		{
			get { return GetColumnValue<byte?>(Columns.DeliveryDealBroadcastMinutes); }
			set { SetColumnValue(Columns.DeliveryDealBroadcastMinutes, value); }
		}
		  
		[XmlAttribute("SpecialEventUrl")]
		[Bindable(true)]
		public string SpecialEventUrl 
		{
			get { return GetColumnValue<string>(Columns.SpecialEventUrl); }
			set { SetColumnValue(Columns.SpecialEventUrl, value); }
		}
		  
		[XmlAttribute("SpecialDealBroadcastTimeStartTimeFormat1")]
		[Bindable(true)]
		public string SpecialDealBroadcastTimeStartTimeFormat1 
		{
			get { return GetColumnValue<string>(Columns.SpecialDealBroadcastTimeStartTimeFormat1); }
			set { SetColumnValue(Columns.SpecialDealBroadcastTimeStartTimeFormat1, value); }
		}
		  
		[XmlAttribute("SpecialDealBroadcastTimeEndTimeFormat1")]
		[Bindable(true)]
		public string SpecialDealBroadcastTimeEndTimeFormat1 
		{
			get { return GetColumnValue<string>(Columns.SpecialDealBroadcastTimeEndTimeFormat1); }
			set { SetColumnValue(Columns.SpecialDealBroadcastTimeEndTimeFormat1, value); }
		}
		  
		[XmlAttribute("SpecialDealBroadcastTimeStartTimeFormat2")]
		[Bindable(true)]
		public string SpecialDealBroadcastTimeStartTimeFormat2 
		{
			get { return GetColumnValue<string>(Columns.SpecialDealBroadcastTimeStartTimeFormat2); }
			set { SetColumnValue(Columns.SpecialDealBroadcastTimeStartTimeFormat2, value); }
		}
		  
		[XmlAttribute("SpecialDealBroadcastTimeEndTimeFormat2")]
		[Bindable(true)]
		public string SpecialDealBroadcastTimeEndTimeFormat2 
		{
			get { return GetColumnValue<string>(Columns.SpecialDealBroadcastTimeEndTimeFormat2); }
			set { SetColumnValue(Columns.SpecialDealBroadcastTimeEndTimeFormat2, value); }
		}
		  
		[XmlAttribute("VourcherEventUrl")]
		[Bindable(true)]
		public string VourcherEventUrl 
		{
			get { return GetColumnValue<string>(Columns.VourcherEventUrl); }
			set { SetColumnValue(Columns.VourcherEventUrl, value); }
		}
		  
		[XmlAttribute("VourcherStoreBroadcastMinutes")]
		[Bindable(true)]
		public byte? VourcherStoreBroadcastMinutes 
		{
			get { return GetColumnValue<byte?>(Columns.VourcherStoreBroadcastMinutes); }
			set { SetColumnValue(Columns.VourcherStoreBroadcastMinutes, value); }
		}
		  
		[XmlAttribute("Status")]
		[Bindable(true)]
		public bool Status 
		{
			get { return GetColumnValue<bool>(Columns.Status); }
			set { SetColumnValue(Columns.Status, value); }
		}
		  
		[XmlAttribute("Creator")]
		[Bindable(true)]
		public string Creator 
		{
			get { return GetColumnValue<string>(Columns.Creator); }
			set { SetColumnValue(Columns.Creator, value); }
		}
		  
		[XmlAttribute("CreateDate")]
		[Bindable(true)]
		public DateTime CreateDate 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateDate); }
			set { SetColumnValue(Columns.CreateDate, value); }
		}
		  
		[XmlAttribute("ModifyDate")]
		[Bindable(true)]
		public DateTime? ModifyDate 
		{
			get { return GetColumnValue<DateTime?>(Columns.ModifyDate); }
			set { SetColumnValue(Columns.ModifyDate, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn TitleColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn CouponEventUrlColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn CouponDealBroadcastMinutesColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn DeliveryEventUrlColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn DeliveryDealBroadcastMinutesColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn SpecialEventUrlColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn SpecialDealBroadcastTimeStartTimeFormat1Column
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn SpecialDealBroadcastTimeEndTimeFormat1Column
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn SpecialDealBroadcastTimeStartTimeFormat2Column
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn SpecialDealBroadcastTimeEndTimeFormat2Column
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        public static TableSchema.TableColumn VourcherEventUrlColumn
        {
            get { return Schema.Columns[11]; }
        }
        
        
        
        public static TableSchema.TableColumn VourcherStoreBroadcastMinutesColumn
        {
            get { return Schema.Columns[12]; }
        }
        
        
        
        public static TableSchema.TableColumn StatusColumn
        {
            get { return Schema.Columns[13]; }
        }
        
        
        
        public static TableSchema.TableColumn CreatorColumn
        {
            get { return Schema.Columns[14]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateDateColumn
        {
            get { return Schema.Columns[15]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyDateColumn
        {
            get { return Schema.Columns[16]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"Id";
			 public static string Title = @"Title";
			 public static string CouponEventUrl = @"CouponEventUrl";
			 public static string CouponDealBroadcastMinutes = @"CouponDealBroadcastMinutes";
			 public static string DeliveryEventUrl = @"DeliveryEventUrl";
			 public static string DeliveryDealBroadcastMinutes = @"DeliveryDealBroadcastMinutes";
			 public static string SpecialEventUrl = @"SpecialEventUrl";
			 public static string SpecialDealBroadcastTimeStartTimeFormat1 = @"SpecialDealBroadcastTimeStartTimeFormat1";
			 public static string SpecialDealBroadcastTimeEndTimeFormat1 = @"SpecialDealBroadcastTimeEndTimeFormat1";
			 public static string SpecialDealBroadcastTimeStartTimeFormat2 = @"SpecialDealBroadcastTimeStartTimeFormat2";
			 public static string SpecialDealBroadcastTimeEndTimeFormat2 = @"SpecialDealBroadcastTimeEndTimeFormat2";
			 public static string VourcherEventUrl = @"VourcherEventUrl";
			 public static string VourcherStoreBroadcastMinutes = @"VourcherStoreBroadcastMinutes";
			 public static string Status = @"Status";
			 public static string Creator = @"Creator";
			 public static string CreateDate = @"CreateDate";
			 public static string ModifyDate = @"ModifyDate";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

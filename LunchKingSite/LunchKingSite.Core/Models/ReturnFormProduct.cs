using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the ReturnFormProduct class.
	/// </summary>
    [Serializable]
	public partial class ReturnFormProductCollection : RepositoryList<ReturnFormProduct, ReturnFormProductCollection>
	{	   
		public ReturnFormProductCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>ReturnFormProductCollection</returns>
		public ReturnFormProductCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                ReturnFormProduct o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the return_form_product table.
	/// </summary>
	[Serializable]
	public partial class ReturnFormProduct : RepositoryRecord<ReturnFormProduct>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public ReturnFormProduct()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public ReturnFormProduct(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("return_form_product", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarReturnFormId = new TableSchema.TableColumn(schema);
				colvarReturnFormId.ColumnName = "return_form_id";
				colvarReturnFormId.DataType = DbType.Int32;
				colvarReturnFormId.MaxLength = 0;
				colvarReturnFormId.AutoIncrement = false;
				colvarReturnFormId.IsNullable = false;
				colvarReturnFormId.IsPrimaryKey = true;
				colvarReturnFormId.IsForeignKey = true;
				colvarReturnFormId.IsReadOnly = false;
				colvarReturnFormId.DefaultSetting = @"";
				
					colvarReturnFormId.ForeignKeyTableName = "return_form";
				schema.Columns.Add(colvarReturnFormId);
				
				TableSchema.TableColumn colvarOrderProductId = new TableSchema.TableColumn(schema);
				colvarOrderProductId.ColumnName = "order_product_id";
				colvarOrderProductId.DataType = DbType.Int32;
				colvarOrderProductId.MaxLength = 0;
				colvarOrderProductId.AutoIncrement = false;
				colvarOrderProductId.IsNullable = false;
				colvarOrderProductId.IsPrimaryKey = true;
				colvarOrderProductId.IsForeignKey = true;
				colvarOrderProductId.IsReadOnly = false;
				colvarOrderProductId.DefaultSetting = @"";
				
					colvarOrderProductId.ForeignKeyTableName = "order_product";
				schema.Columns.Add(colvarOrderProductId);
				
				TableSchema.TableColumn colvarIsCollected = new TableSchema.TableColumn(schema);
				colvarIsCollected.ColumnName = "is_collected";
				colvarIsCollected.DataType = DbType.Boolean;
				colvarIsCollected.MaxLength = 0;
				colvarIsCollected.AutoIncrement = false;
				colvarIsCollected.IsNullable = true;
				colvarIsCollected.IsPrimaryKey = false;
				colvarIsCollected.IsForeignKey = false;
				colvarIsCollected.IsReadOnly = false;
				colvarIsCollected.DefaultSetting = @"";
				colvarIsCollected.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsCollected);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("return_form_product",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("ReturnFormId")]
		[Bindable(true)]
		public int ReturnFormId 
		{
			get { return GetColumnValue<int>(Columns.ReturnFormId); }
			set { SetColumnValue(Columns.ReturnFormId, value); }
		}
		  
		[XmlAttribute("OrderProductId")]
		[Bindable(true)]
		public int OrderProductId 
		{
			get { return GetColumnValue<int>(Columns.OrderProductId); }
			set { SetColumnValue(Columns.OrderProductId, value); }
		}
		  
		[XmlAttribute("IsCollected")]
		[Bindable(true)]
		public bool? IsCollected 
		{
			get { return GetColumnValue<bool?>(Columns.IsCollected); }
			set { SetColumnValue(Columns.IsCollected, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (2)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn ReturnFormIdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn OrderProductIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn IsCollectedColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string ReturnFormId = @"return_form_id";
			 public static string OrderProductId = @"order_product_id";
			 public static string IsCollected = @"is_collected";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

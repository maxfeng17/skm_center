using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the CategoryDeal class.
	/// </summary>
    [Serializable]
	public partial class SkmMemberPhCollection : RepositoryList<SkmMemberPh, SkmMemberPhCollection>
	{	   
		public SkmMemberPhCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>CategoryDealCollection</returns>
		public SkmMemberPhCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                SkmMemberPh o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the category_deals table.
	/// </summary>
	[Serializable]
	public partial class SkmMemberPh : RepositoryRecord<SkmMemberPh>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public SkmMemberPh()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public SkmMemberPh(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("skm_member_ph", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarSkmToken = new TableSchema.TableColumn(schema);
                colvarSkmToken.ColumnName = "skm_token";
                colvarSkmToken.DataType = DbType.String;
                colvarSkmToken.MaxLength = 50;
                colvarSkmToken.AutoIncrement = false;
                colvarSkmToken.IsNullable = false;
                colvarSkmToken.IsPrimaryKey = false;
                colvarSkmToken.IsForeignKey = false;
                colvarSkmToken.IsReadOnly = false;
                colvarSkmToken.DefaultSetting = @"";
                colvarSkmToken.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSkmToken);
				
				TableSchema.TableColumn colvarPh1 = new TableSchema.TableColumn(schema);
                colvarPh1.ColumnName = "ph1";
                colvarPh1.DataType = DbType.String;
                colvarPh1.MaxLength = 2;
                colvarPh1.AutoIncrement = false;
                colvarPh1.IsNullable = true;
                colvarPh1.IsPrimaryKey = false;
                colvarPh1.IsForeignKey = false;
                colvarPh1.IsReadOnly = false;
                colvarPh1.DefaultSetting = @"";
                colvarPh1.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPh1);

                TableSchema.TableColumn colvarPh2 = new TableSchema.TableColumn(schema);
                colvarPh2.ColumnName = "ph2";
                colvarPh2.DataType = DbType.String;
                colvarPh2.MaxLength = 2;
                colvarPh2.AutoIncrement = false;
                colvarPh2.IsNullable = true;
                colvarPh2.IsPrimaryKey = false;
                colvarPh2.IsForeignKey = false;
                colvarPh2.IsReadOnly = false;
                colvarPh2.DefaultSetting = @"";
                colvarPh2.ForeignKeyTableName = "";
                schema.Columns.Add(colvarPh2);

                TableSchema.TableColumn colvarPh3 = new TableSchema.TableColumn(schema);
                colvarPh3.ColumnName = "ph3";
                colvarPh3.DataType = DbType.String;
                colvarPh3.MaxLength = 2;
                colvarPh3.AutoIncrement = false;
                colvarPh3.IsNullable = true;
                colvarPh3.IsPrimaryKey = false;
                colvarPh3.IsForeignKey = false;
                colvarPh3.IsReadOnly = false;
                colvarPh3.DefaultSetting = @"";
                colvarPh3.ForeignKeyTableName = "";
                schema.Columns.Add(colvarPh3);

                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = true;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                colvarCreateTime.DefaultSetting = @"";
                colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
			
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("skm_member_ph", schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("SkmToken")]
		[Bindable(true)]
		public string SkmToken
        {
			get { return GetColumnValue<string>(Columns.SkmToken); }
			set { SetColumnValue(Columns.SkmToken, value); }
		}
		  
		[XmlAttribute("Ph1")]
		[Bindable(true)]
		public string Ph1
        {
			get { return GetColumnValue<string>(Columns.Ph1); }
			set { SetColumnValue(Columns.Ph1, value); }
		}

        [XmlAttribute("Ph2")]
        [Bindable(true)]
        public string Ph2
        {
            get { return GetColumnValue<string>(Columns.Ph2); }
            set { SetColumnValue(Columns.Ph2, value); }
        }

        [XmlAttribute("Ph3")]
        [Bindable(true)]
        public string Ph3
        {
            get { return GetColumnValue<string>(Columns.Ph3); }
            set { SetColumnValue(Columns.Ph3, value); }
        }

        [XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime? CreateTime
        {
			get { return GetColumnValue<DateTime?>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn SkmTokenColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn PH1Column
        {
            get { return Schema.Columns[2]; }
        }

        public static TableSchema.TableColumn PH2Column
        {
            get { return Schema.Columns[3]; }
        }

        public static TableSchema.TableColumn PH3Column
        {
            get { return Schema.Columns[4]; }
        }
      
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string SkmToken = @"skm_token";
			 public static string Ph1 = @"ph1";
			 public static string Ph2 = @"ph2";
			 public static string Ph3 = @"ph3";
             public static string CreateTime = @"create_time";

        }
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the DiscountEventCampaign class.
	/// </summary>
    [Serializable]
	public partial class DiscountEventCampaignCollection : RepositoryList<DiscountEventCampaign, DiscountEventCampaignCollection>
	{	   
		public DiscountEventCampaignCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>DiscountEventCampaignCollection</returns>
		public DiscountEventCampaignCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                DiscountEventCampaign o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the discount_event_campaign table.
	/// </summary>
	[Serializable]
	public partial class DiscountEventCampaign : RepositoryRecord<DiscountEventCampaign>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public DiscountEventCampaign()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public DiscountEventCampaign(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("discount_event_campaign", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarEventId = new TableSchema.TableColumn(schema);
				colvarEventId.ColumnName = "event_id";
				colvarEventId.DataType = DbType.Int32;
				colvarEventId.MaxLength = 0;
				colvarEventId.AutoIncrement = false;
				colvarEventId.IsNullable = false;
				colvarEventId.IsPrimaryKey = false;
				colvarEventId.IsForeignKey = true;
				colvarEventId.IsReadOnly = false;
				colvarEventId.DefaultSetting = @"";
				
					colvarEventId.ForeignKeyTableName = "discount_event";
				schema.Columns.Add(colvarEventId);
				
				TableSchema.TableColumn colvarCampaignId = new TableSchema.TableColumn(schema);
				colvarCampaignId.ColumnName = "campaign_id";
				colvarCampaignId.DataType = DbType.Int32;
				colvarCampaignId.MaxLength = 0;
				colvarCampaignId.AutoIncrement = false;
				colvarCampaignId.IsNullable = false;
				colvarCampaignId.IsPrimaryKey = false;
				colvarCampaignId.IsForeignKey = true;
				colvarCampaignId.IsReadOnly = false;
				colvarCampaignId.DefaultSetting = @"";
				
					colvarCampaignId.ForeignKeyTableName = "discount_campaign";
				schema.Columns.Add(colvarCampaignId);
				
				TableSchema.TableColumn colvarQty = new TableSchema.TableColumn(schema);
				colvarQty.ColumnName = "qty";
				colvarQty.DataType = DbType.Int32;
				colvarQty.MaxLength = 0;
				colvarQty.AutoIncrement = false;
				colvarQty.IsNullable = false;
				colvarQty.IsPrimaryKey = false;
				colvarQty.IsForeignKey = false;
				colvarQty.IsReadOnly = false;
				
						colvarQty.DefaultSetting = @"((0))";
				colvarQty.ForeignKeyTableName = "";
				schema.Columns.Add(colvarQty);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("discount_event_campaign",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("EventId")]
		[Bindable(true)]
		public int EventId 
		{
			get { return GetColumnValue<int>(Columns.EventId); }
			set { SetColumnValue(Columns.EventId, value); }
		}
		  
		[XmlAttribute("CampaignId")]
		[Bindable(true)]
		public int CampaignId 
		{
			get { return GetColumnValue<int>(Columns.CampaignId); }
			set { SetColumnValue(Columns.CampaignId, value); }
		}
		  
		[XmlAttribute("Qty")]
		[Bindable(true)]
		public int Qty 
		{
			get { return GetColumnValue<int>(Columns.Qty); }
			set { SetColumnValue(Columns.Qty, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (2)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn EventIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn CampaignIdColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn QtyColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string EventId = @"event_id";
			 public static string CampaignId = @"campaign_id";
			 public static string Qty = @"qty";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

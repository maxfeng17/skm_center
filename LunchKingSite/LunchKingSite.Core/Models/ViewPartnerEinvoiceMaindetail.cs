using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the ViewPartnerEinvoiceMaindetail class.
    /// </summary>
    [Serializable]
    public partial class ViewPartnerEinvoiceMaindetailCollection : ReadOnlyList<ViewPartnerEinvoiceMaindetail, ViewPartnerEinvoiceMaindetailCollection>
    {
        public ViewPartnerEinvoiceMaindetailCollection() { }
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_partner_einvoice_maindetail view.
    /// </summary>
    [Serializable]
    public partial class ViewPartnerEinvoiceMaindetail : ReadOnlyRecord<ViewPartnerEinvoiceMaindetail>, IReadOnlyRecord
    {

        #region Default Settings
        protected static void SetSQLProps()
        {
            GetTableSchema();
        }
        #endregion
        #region Schema Accessor
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_partner_einvoice_maindetail", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = false;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = false;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;

                schema.Columns.Add(colvarId);

                TableSchema.TableColumn colvarInvoiceNumber = new TableSchema.TableColumn(schema);
                colvarInvoiceNumber.ColumnName = "invoice_number";
                colvarInvoiceNumber.DataType = DbType.String;
                colvarInvoiceNumber.MaxLength = 10;
                colvarInvoiceNumber.AutoIncrement = false;
                colvarInvoiceNumber.IsNullable = true;
                colvarInvoiceNumber.IsPrimaryKey = false;
                colvarInvoiceNumber.IsForeignKey = false;
                colvarInvoiceNumber.IsReadOnly = false;

                schema.Columns.Add(colvarInvoiceNumber);

                TableSchema.TableColumn colvarInvoiceNumberTime = new TableSchema.TableColumn(schema);
                colvarInvoiceNumberTime.ColumnName = "invoice_number_time";
                colvarInvoiceNumberTime.DataType = DbType.DateTime;
                colvarInvoiceNumberTime.MaxLength = 0;
                colvarInvoiceNumberTime.AutoIncrement = false;
                colvarInvoiceNumberTime.IsNullable = true;
                colvarInvoiceNumberTime.IsPrimaryKey = false;
                colvarInvoiceNumberTime.IsForeignKey = false;
                colvarInvoiceNumberTime.IsReadOnly = false;

                schema.Columns.Add(colvarInvoiceNumberTime);

                TableSchema.TableColumn colvarInvoiceComId = new TableSchema.TableColumn(schema);
                colvarInvoiceComId.ColumnName = "invoice_com_id";
                colvarInvoiceComId.DataType = DbType.String;
                colvarInvoiceComId.MaxLength = 10;
                colvarInvoiceComId.AutoIncrement = false;
                colvarInvoiceComId.IsNullable = true;
                colvarInvoiceComId.IsPrimaryKey = false;
                colvarInvoiceComId.IsForeignKey = false;
                colvarInvoiceComId.IsReadOnly = false;

                schema.Columns.Add(colvarInvoiceComId);

                TableSchema.TableColumn colvarInvoiceComName = new TableSchema.TableColumn(schema);
                colvarInvoiceComName.ColumnName = "invoice_com_name";
                colvarInvoiceComName.DataType = DbType.String;
                colvarInvoiceComName.MaxLength = 250;
                colvarInvoiceComName.AutoIncrement = false;
                colvarInvoiceComName.IsNullable = true;
                colvarInvoiceComName.IsPrimaryKey = false;
                colvarInvoiceComName.IsForeignKey = false;
                colvarInvoiceComName.IsReadOnly = false;

                schema.Columns.Add(colvarInvoiceComName);

                TableSchema.TableColumn colvarInvoiceBuyerName = new TableSchema.TableColumn(schema);
                colvarInvoiceBuyerName.ColumnName = "invoice_buyer_name";
                colvarInvoiceBuyerName.DataType = DbType.String;
                colvarInvoiceBuyerName.MaxLength = 50;
                colvarInvoiceBuyerName.AutoIncrement = false;
                colvarInvoiceBuyerName.IsNullable = true;
                colvarInvoiceBuyerName.IsPrimaryKey = false;
                colvarInvoiceBuyerName.IsForeignKey = false;
                colvarInvoiceBuyerName.IsReadOnly = false;

                schema.Columns.Add(colvarInvoiceBuyerName);

                TableSchema.TableColumn colvarInvoiceBuyerAddress = new TableSchema.TableColumn(schema);
                colvarInvoiceBuyerAddress.ColumnName = "invoice_buyer_address";
                colvarInvoiceBuyerAddress.DataType = DbType.String;
                colvarInvoiceBuyerAddress.MaxLength = 250;
                colvarInvoiceBuyerAddress.AutoIncrement = false;
                colvarInvoiceBuyerAddress.IsNullable = true;
                colvarInvoiceBuyerAddress.IsPrimaryKey = false;
                colvarInvoiceBuyerAddress.IsForeignKey = false;
                colvarInvoiceBuyerAddress.IsReadOnly = false;

                schema.Columns.Add(colvarInvoiceBuyerAddress);

                TableSchema.TableColumn colvarInvoicePass = new TableSchema.TableColumn(schema);
                colvarInvoicePass.ColumnName = "invoice_pass";
                colvarInvoicePass.DataType = DbType.AnsiStringFixedLength;
                colvarInvoicePass.MaxLength = 4;
                colvarInvoicePass.AutoIncrement = false;
                colvarInvoicePass.IsNullable = false;
                colvarInvoicePass.IsPrimaryKey = false;
                colvarInvoicePass.IsForeignKey = false;
                colvarInvoicePass.IsReadOnly = false;

                schema.Columns.Add(colvarInvoicePass);

                TableSchema.TableColumn colvarOrderItem = new TableSchema.TableColumn(schema);
                colvarOrderItem.ColumnName = "order_item";
                colvarOrderItem.DataType = DbType.String;
                colvarOrderItem.MaxLength = 250;
                colvarOrderItem.AutoIncrement = false;
                colvarOrderItem.IsNullable = true;
                colvarOrderItem.IsPrimaryKey = false;
                colvarOrderItem.IsForeignKey = false;
                colvarOrderItem.IsReadOnly = false;

                schema.Columns.Add(colvarOrderItem);

                TableSchema.TableColumn colvarOrderAmount = new TableSchema.TableColumn(schema);
                colvarOrderAmount.ColumnName = "order_amount";
                colvarOrderAmount.DataType = DbType.Currency;
                colvarOrderAmount.MaxLength = 0;
                colvarOrderAmount.AutoIncrement = false;
                colvarOrderAmount.IsNullable = false;
                colvarOrderAmount.IsPrimaryKey = false;
                colvarOrderAmount.IsForeignKey = false;
                colvarOrderAmount.IsReadOnly = false;

                schema.Columns.Add(colvarOrderAmount);

                TableSchema.TableColumn colvarInvoiceSumAmount = new TableSchema.TableColumn(schema);
                colvarInvoiceSumAmount.ColumnName = "invoice_sum_amount";
                colvarInvoiceSumAmount.DataType = DbType.Currency;
                colvarInvoiceSumAmount.MaxLength = 0;
                colvarInvoiceSumAmount.AutoIncrement = false;
                colvarInvoiceSumAmount.IsNullable = false;
                colvarInvoiceSumAmount.IsPrimaryKey = false;
                colvarInvoiceSumAmount.IsForeignKey = false;
                colvarInvoiceSumAmount.IsReadOnly = false;

                schema.Columns.Add(colvarInvoiceSumAmount);

                TableSchema.TableColumn colvarInvoiceTax = new TableSchema.TableColumn(schema);
                colvarInvoiceTax.ColumnName = "invoice_tax";
                colvarInvoiceTax.DataType = DbType.Decimal;
                colvarInvoiceTax.MaxLength = 0;
                colvarInvoiceTax.AutoIncrement = false;
                colvarInvoiceTax.IsNullable = false;
                colvarInvoiceTax.IsPrimaryKey = false;
                colvarInvoiceTax.IsForeignKey = false;
                colvarInvoiceTax.IsReadOnly = false;

                schema.Columns.Add(colvarInvoiceTax);

                TableSchema.TableColumn colvarInvoiceRequestTime = new TableSchema.TableColumn(schema);
                colvarInvoiceRequestTime.ColumnName = "invoice_request_time";
                colvarInvoiceRequestTime.DataType = DbType.DateTime;
                colvarInvoiceRequestTime.MaxLength = 0;
                colvarInvoiceRequestTime.AutoIncrement = false;
                colvarInvoiceRequestTime.IsNullable = true;
                colvarInvoiceRequestTime.IsPrimaryKey = false;
                colvarInvoiceRequestTime.IsForeignKey = false;
                colvarInvoiceRequestTime.IsReadOnly = false;

                schema.Columns.Add(colvarInvoiceRequestTime);

                TableSchema.TableColumn colvarInvoicePaperedTime = new TableSchema.TableColumn(schema);
                colvarInvoicePaperedTime.ColumnName = "invoice_papered_time";
                colvarInvoicePaperedTime.DataType = DbType.DateTime;
                colvarInvoicePaperedTime.MaxLength = 0;
                colvarInvoicePaperedTime.AutoIncrement = false;
                colvarInvoicePaperedTime.IsNullable = true;
                colvarInvoicePaperedTime.IsPrimaryKey = false;
                colvarInvoicePaperedTime.IsForeignKey = false;
                colvarInvoicePaperedTime.IsReadOnly = false;

                schema.Columns.Add(colvarInvoicePaperedTime);

                TableSchema.TableColumn colvarInvoiceWinning = new TableSchema.TableColumn(schema);
                colvarInvoiceWinning.ColumnName = "invoice_winning";
                colvarInvoiceWinning.DataType = DbType.Boolean;
                colvarInvoiceWinning.MaxLength = 0;
                colvarInvoiceWinning.AutoIncrement = false;
                colvarInvoiceWinning.IsNullable = false;
                colvarInvoiceWinning.IsPrimaryKey = false;
                colvarInvoiceWinning.IsForeignKey = false;
                colvarInvoiceWinning.IsReadOnly = false;

                schema.Columns.Add(colvarInvoiceWinning);

                TableSchema.TableColumn colvarInvoicePapered = new TableSchema.TableColumn(schema);
                colvarInvoicePapered.ColumnName = "invoice_papered";
                colvarInvoicePapered.DataType = DbType.Boolean;
                colvarInvoicePapered.MaxLength = 0;
                colvarInvoicePapered.AutoIncrement = false;
                colvarInvoicePapered.IsNullable = false;
                colvarInvoicePapered.IsPrimaryKey = false;
                colvarInvoicePapered.IsForeignKey = false;
                colvarInvoicePapered.IsReadOnly = false;

                schema.Columns.Add(colvarInvoicePapered);

                TableSchema.TableColumn colvarInvoiceSerialId = new TableSchema.TableColumn(schema);
                colvarInvoiceSerialId.ColumnName = "invoice_serial_id";
                colvarInvoiceSerialId.DataType = DbType.Int32;
                colvarInvoiceSerialId.MaxLength = 0;
                colvarInvoiceSerialId.AutoIncrement = false;
                colvarInvoiceSerialId.IsNullable = false;
                colvarInvoiceSerialId.IsPrimaryKey = false;
                colvarInvoiceSerialId.IsForeignKey = false;
                colvarInvoiceSerialId.IsReadOnly = false;

                schema.Columns.Add(colvarInvoiceSerialId);

                TableSchema.TableColumn colvarInvoiceWinnerresponseTime = new TableSchema.TableColumn(schema);
                colvarInvoiceWinnerresponseTime.ColumnName = "invoice_winnerresponse_time";
                colvarInvoiceWinnerresponseTime.DataType = DbType.DateTime;
                colvarInvoiceWinnerresponseTime.MaxLength = 0;
                colvarInvoiceWinnerresponseTime.AutoIncrement = false;
                colvarInvoiceWinnerresponseTime.IsNullable = true;
                colvarInvoiceWinnerresponseTime.IsPrimaryKey = false;
                colvarInvoiceWinnerresponseTime.IsForeignKey = false;
                colvarInvoiceWinnerresponseTime.IsReadOnly = false;

                schema.Columns.Add(colvarInvoiceWinnerresponseTime);

                TableSchema.TableColumn colvarInvoiceWinnerresponsePhone = new TableSchema.TableColumn(schema);
                colvarInvoiceWinnerresponsePhone.ColumnName = "invoice_winnerresponse_phone";
                colvarInvoiceWinnerresponsePhone.DataType = DbType.String;
                colvarInvoiceWinnerresponsePhone.MaxLength = 100;
                colvarInvoiceWinnerresponsePhone.AutoIncrement = false;
                colvarInvoiceWinnerresponsePhone.IsNullable = true;
                colvarInvoiceWinnerresponsePhone.IsPrimaryKey = false;
                colvarInvoiceWinnerresponsePhone.IsForeignKey = false;
                colvarInvoiceWinnerresponsePhone.IsReadOnly = false;

                schema.Columns.Add(colvarInvoiceWinnerresponsePhone);

                TableSchema.TableColumn colvarInvoiceStatus = new TableSchema.TableColumn(schema);
                colvarInvoiceStatus.ColumnName = "invoice_status";
                colvarInvoiceStatus.DataType = DbType.Int32;
                colvarInvoiceStatus.MaxLength = 0;
                colvarInvoiceStatus.AutoIncrement = false;
                colvarInvoiceStatus.IsNullable = true;
                colvarInvoiceStatus.IsPrimaryKey = false;
                colvarInvoiceStatus.IsForeignKey = false;
                colvarInvoiceStatus.IsReadOnly = false;

                schema.Columns.Add(colvarInvoiceStatus);

                TableSchema.TableColumn colvarInvoiceMailbackPaper = new TableSchema.TableColumn(schema);
                colvarInvoiceMailbackPaper.ColumnName = "invoice_mailback_paper";
                colvarInvoiceMailbackPaper.DataType = DbType.Boolean;
                colvarInvoiceMailbackPaper.MaxLength = 0;
                colvarInvoiceMailbackPaper.AutoIncrement = false;
                colvarInvoiceMailbackPaper.IsNullable = true;
                colvarInvoiceMailbackPaper.IsPrimaryKey = false;
                colvarInvoiceMailbackPaper.IsForeignKey = false;
                colvarInvoiceMailbackPaper.IsReadOnly = false;

                schema.Columns.Add(colvarInvoiceMailbackPaper);

                TableSchema.TableColumn colvarInvoiceMailbackAllowance = new TableSchema.TableColumn(schema);
                colvarInvoiceMailbackAllowance.ColumnName = "invoice_mailback_allowance";
                colvarInvoiceMailbackAllowance.DataType = DbType.Boolean;
                colvarInvoiceMailbackAllowance.MaxLength = 0;
                colvarInvoiceMailbackAllowance.AutoIncrement = false;
                colvarInvoiceMailbackAllowance.IsNullable = true;
                colvarInvoiceMailbackAllowance.IsPrimaryKey = false;
                colvarInvoiceMailbackAllowance.IsForeignKey = false;
                colvarInvoiceMailbackAllowance.IsReadOnly = false;

                schema.Columns.Add(colvarInvoiceMailbackAllowance);

                TableSchema.TableColumn colvarDetailId = new TableSchema.TableColumn(schema);
                colvarDetailId.ColumnName = "detail_id";
                colvarDetailId.DataType = DbType.Int32;
                colvarDetailId.MaxLength = 0;
                colvarDetailId.AutoIncrement = false;
                colvarDetailId.IsNullable = false;
                colvarDetailId.IsPrimaryKey = false;
                colvarDetailId.IsForeignKey = false;
                colvarDetailId.IsReadOnly = false;

                schema.Columns.Add(colvarDetailId);

                TableSchema.TableColumn colvarDetailInvoiceAmount = new TableSchema.TableColumn(schema);
                colvarDetailInvoiceAmount.ColumnName = "detail_invoice_amount";
                colvarDetailInvoiceAmount.DataType = DbType.Currency;
                colvarDetailInvoiceAmount.MaxLength = 0;
                colvarDetailInvoiceAmount.AutoIncrement = false;
                colvarDetailInvoiceAmount.IsNullable = false;
                colvarDetailInvoiceAmount.IsPrimaryKey = false;
                colvarDetailInvoiceAmount.IsForeignKey = false;
                colvarDetailInvoiceAmount.IsReadOnly = false;

                schema.Columns.Add(colvarDetailInvoiceAmount);

                TableSchema.TableColumn colvarDetailCreateTime = new TableSchema.TableColumn(schema);
                colvarDetailCreateTime.ColumnName = "detail_create_time";
                colvarDetailCreateTime.DataType = DbType.DateTime;
                colvarDetailCreateTime.MaxLength = 0;
                colvarDetailCreateTime.AutoIncrement = false;
                colvarDetailCreateTime.IsNullable = false;
                colvarDetailCreateTime.IsPrimaryKey = false;
                colvarDetailCreateTime.IsForeignKey = false;
                colvarDetailCreateTime.IsReadOnly = false;

                schema.Columns.Add(colvarDetailCreateTime);

                TableSchema.TableColumn colvarDetailCreator = new TableSchema.TableColumn(schema);
                colvarDetailCreator.ColumnName = "detail_creator";
                colvarDetailCreator.DataType = DbType.String;
                colvarDetailCreator.MaxLength = 50;
                colvarDetailCreator.AutoIncrement = false;
                colvarDetailCreator.IsNullable = false;
                colvarDetailCreator.IsPrimaryKey = false;
                colvarDetailCreator.IsForeignKey = false;
                colvarDetailCreator.IsReadOnly = false;

                schema.Columns.Add(colvarDetailCreator);

                TableSchema.TableColumn colvarDetailMessage = new TableSchema.TableColumn(schema);
                colvarDetailMessage.ColumnName = "detail_message";
                colvarDetailMessage.DataType = DbType.String;
                colvarDetailMessage.MaxLength = 250;
                colvarDetailMessage.AutoIncrement = false;
                colvarDetailMessage.IsNullable = true;
                colvarDetailMessage.IsPrimaryKey = false;
                colvarDetailMessage.IsForeignKey = false;
                colvarDetailMessage.IsReadOnly = false;

                schema.Columns.Add(colvarDetailMessage);

                TableSchema.TableColumn colvarDetailStatus = new TableSchema.TableColumn(schema);
                colvarDetailStatus.ColumnName = "detail_status";
                colvarDetailStatus.DataType = DbType.Boolean;
                colvarDetailStatus.MaxLength = 0;
                colvarDetailStatus.AutoIncrement = false;
                colvarDetailStatus.IsNullable = false;
                colvarDetailStatus.IsPrimaryKey = false;
                colvarDetailStatus.IsForeignKey = false;
                colvarDetailStatus.IsReadOnly = false;

                schema.Columns.Add(colvarDetailStatus);

                TableSchema.TableColumn colvarInvoiceType = new TableSchema.TableColumn(schema);
                colvarInvoiceType.ColumnName = "invoice_type";
                colvarInvoiceType.DataType = DbType.Int32;
                colvarInvoiceType.MaxLength = 0;
                colvarInvoiceType.AutoIncrement = false;
                colvarInvoiceType.IsNullable = false;
                colvarInvoiceType.IsPrimaryKey = false;
                colvarInvoiceType.IsForeignKey = false;
                colvarInvoiceType.IsReadOnly = false;

                schema.Columns.Add(colvarInvoiceType);

                TableSchema.TableColumn colvarCouponId = new TableSchema.TableColumn(schema);
                colvarCouponId.ColumnName = "coupon_id";
                colvarCouponId.DataType = DbType.Int32;
                colvarCouponId.MaxLength = 0;
                colvarCouponId.AutoIncrement = false;
                colvarCouponId.IsNullable = true;
                colvarCouponId.IsPrimaryKey = false;
                colvarCouponId.IsForeignKey = false;
                colvarCouponId.IsReadOnly = false;

                schema.Columns.Add(colvarCouponId);

                TableSchema.TableColumn colvarVerifiedTime = new TableSchema.TableColumn(schema);
                colvarVerifiedTime.ColumnName = "verified_time";
                colvarVerifiedTime.DataType = DbType.DateTime;
                colvarVerifiedTime.MaxLength = 0;
                colvarVerifiedTime.AutoIncrement = false;
                colvarVerifiedTime.IsNullable = true;
                colvarVerifiedTime.IsPrimaryKey = false;
                colvarVerifiedTime.IsForeignKey = false;
                colvarVerifiedTime.IsReadOnly = false;

                schema.Columns.Add(colvarVerifiedTime);

                TableSchema.TableColumn colvarInvoiceMode2 = new TableSchema.TableColumn(schema);
                colvarInvoiceMode2.ColumnName = "invoice_mode2";
                colvarInvoiceMode2.DataType = DbType.Int32;
                colvarInvoiceMode2.MaxLength = 0;
                colvarInvoiceMode2.AutoIncrement = false;
                colvarInvoiceMode2.IsNullable = false;
                colvarInvoiceMode2.IsPrimaryKey = false;
                colvarInvoiceMode2.IsForeignKey = false;
                colvarInvoiceMode2.IsReadOnly = false;

                schema.Columns.Add(colvarInvoiceMode2);

                TableSchema.TableColumn colvarCarrierType = new TableSchema.TableColumn(schema);
                colvarCarrierType.ColumnName = "carrier_type";
                colvarCarrierType.DataType = DbType.Int32;
                colvarCarrierType.MaxLength = 0;
                colvarCarrierType.AutoIncrement = false;
                colvarCarrierType.IsNullable = false;
                colvarCarrierType.IsPrimaryKey = false;
                colvarCarrierType.IsForeignKey = false;
                colvarCarrierType.IsReadOnly = false;

                schema.Columns.Add(colvarCarrierType);

                TableSchema.TableColumn colvarCarrierId = new TableSchema.TableColumn(schema);
                colvarCarrierId.ColumnName = "carrier_id";
                colvarCarrierId.DataType = DbType.String;
                colvarCarrierId.MaxLength = 64;
                colvarCarrierId.AutoIncrement = false;
                colvarCarrierId.IsNullable = true;
                colvarCarrierId.IsPrimaryKey = false;
                colvarCarrierId.IsForeignKey = false;
                colvarCarrierId.IsReadOnly = false;

                schema.Columns.Add(colvarCarrierId);

                TableSchema.TableColumn colvarLoveCode = new TableSchema.TableColumn(schema);
                colvarLoveCode.ColumnName = "love_code";
                colvarLoveCode.DataType = DbType.String;
                colvarLoveCode.MaxLength = 8;
                colvarLoveCode.AutoIncrement = false;
                colvarLoveCode.IsNullable = true;
                colvarLoveCode.IsPrimaryKey = false;
                colvarLoveCode.IsForeignKey = false;
                colvarLoveCode.IsReadOnly = false;

                schema.Columns.Add(colvarLoveCode);

                TableSchema.TableColumn colvarInvoiceVoidTime = new TableSchema.TableColumn(schema);
                colvarInvoiceVoidTime.ColumnName = "invoice_void_time";
                colvarInvoiceVoidTime.DataType = DbType.DateTime;
                colvarInvoiceVoidTime.MaxLength = 0;
                colvarInvoiceVoidTime.AutoIncrement = false;
                colvarInvoiceVoidTime.IsNullable = true;
                colvarInvoiceVoidTime.IsPrimaryKey = false;
                colvarInvoiceVoidTime.IsForeignKey = false;
                colvarInvoiceVoidTime.IsReadOnly = false;

                schema.Columns.Add(colvarInvoiceVoidTime);

                TableSchema.TableColumn colvarInvoiceVoidMsg = new TableSchema.TableColumn(schema);
                colvarInvoiceVoidMsg.ColumnName = "invoice_void_msg";
                colvarInvoiceVoidMsg.DataType = DbType.String;
                colvarInvoiceVoidMsg.MaxLength = 250;
                colvarInvoiceVoidMsg.AutoIncrement = false;
                colvarInvoiceVoidMsg.IsNullable = true;
                colvarInvoiceVoidMsg.IsPrimaryKey = false;
                colvarInvoiceVoidMsg.IsForeignKey = false;
                colvarInvoiceVoidMsg.IsReadOnly = false;

                schema.Columns.Add(colvarInvoiceVoidMsg);

                TableSchema.TableColumn colvarPartnerOrderId = new TableSchema.TableColumn(schema);
                colvarPartnerOrderId.ColumnName = "partner_order_id";
                colvarPartnerOrderId.DataType = DbType.AnsiString;
                colvarPartnerOrderId.MaxLength = 30;
                colvarPartnerOrderId.AutoIncrement = false;
                colvarPartnerOrderId.IsNullable = false;
                colvarPartnerOrderId.IsPrimaryKey = false;
                colvarPartnerOrderId.IsForeignKey = false;
                colvarPartnerOrderId.IsReadOnly = false;

                schema.Columns.Add(colvarPartnerOrderId);

                TableSchema.TableColumn colvarWinnerMail = new TableSchema.TableColumn(schema);
                colvarWinnerMail.ColumnName = "winner_mail";
                colvarWinnerMail.DataType = DbType.String;
                colvarWinnerMail.MaxLength = 50;
                colvarWinnerMail.AutoIncrement = false;
                colvarWinnerMail.IsNullable = true;
                colvarWinnerMail.IsPrimaryKey = false;
                colvarWinnerMail.IsForeignKey = false;
                colvarWinnerMail.IsReadOnly = false;

                schema.Columns.Add(colvarWinnerMail);


                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_partner_einvoice_maindetail", schema);
            }
        }
        #endregion

        #region Query Accessor
        public static Query CreateQuery()
        {
            return new Query(Schema);
        }
        #endregion

        #region .ctors
        public ViewPartnerEinvoiceMaindetail()
        {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewPartnerEinvoiceMaindetail(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
            {
                ForceDefaults();
            }
            MarkNew();
        }

        public ViewPartnerEinvoiceMaindetail(object keyID)
        {
            SetSQLProps();
            LoadByKey(keyID);
        }

        public ViewPartnerEinvoiceMaindetail(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName, columnValue);
        }

        #endregion

        #region Props


        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id
        {
            get
            {
                return GetColumnValue<int>("id");
            }
            set
            {
                SetColumnValue("id", value);
            }
        }

        [XmlAttribute("InvoiceNumber")]
        [Bindable(true)]
        public string InvoiceNumber
        {
            get
            {
                return GetColumnValue<string>("invoice_number");
            }
            set
            {
                SetColumnValue("invoice_number", value);
            }
        }

        [XmlAttribute("InvoiceNumberTime")]
        [Bindable(true)]
        public DateTime? InvoiceNumberTime
        {
            get
            {
                return GetColumnValue<DateTime?>("invoice_number_time");
            }
            set
            {
                SetColumnValue("invoice_number_time", value);
            }
        }

        [XmlAttribute("InvoiceComId")]
        [Bindable(true)]
        public string InvoiceComId
        {
            get
            {
                return GetColumnValue<string>("invoice_com_id");
            }
            set
            {
                SetColumnValue("invoice_com_id", value);
            }
        }

        [XmlAttribute("InvoiceComName")]
        [Bindable(true)]
        public string InvoiceComName
        {
            get
            {
                return GetColumnValue<string>("invoice_com_name");
            }
            set
            {
                SetColumnValue("invoice_com_name", value);
            }
        }

        [XmlAttribute("InvoiceBuyerName")]
        [Bindable(true)]
        public string InvoiceBuyerName
        {
            get
            {
                return GetColumnValue<string>("invoice_buyer_name");
            }
            set
            {
                SetColumnValue("invoice_buyer_name", value);
            }
        }

        [XmlAttribute("InvoiceBuyerAddress")]
        [Bindable(true)]
        public string InvoiceBuyerAddress
        {
            get
            {
                return GetColumnValue<string>("invoice_buyer_address");
            }
            set
            {
                SetColumnValue("invoice_buyer_address", value);
            }
        }

        [XmlAttribute("InvoicePass")]
        [Bindable(true)]
        public string InvoicePass
        {
            get
            {
                return GetColumnValue<string>("invoice_pass");
            }
            set
            {
                SetColumnValue("invoice_pass", value);
            }
        }

        [XmlAttribute("OrderItem")]
        [Bindable(true)]
        public string OrderItem
        {
            get
            {
                return GetColumnValue<string>("order_item");
            }
            set
            {
                SetColumnValue("order_item", value);
            }
        }

        [XmlAttribute("OrderAmount")]
        [Bindable(true)]
        public decimal OrderAmount
        {
            get
            {
                return GetColumnValue<decimal>("order_amount");
            }
            set
            {
                SetColumnValue("order_amount", value);
            }
        }

        [XmlAttribute("InvoiceSumAmount")]
        [Bindable(true)]
        public decimal InvoiceSumAmount
        {
            get
            {
                return GetColumnValue<decimal>("invoice_sum_amount");
            }
            set
            {
                SetColumnValue("invoice_sum_amount", value);
            }
        }

        [XmlAttribute("InvoiceTax")]
        [Bindable(true)]
        public decimal InvoiceTax
        {
            get
            {
                return GetColumnValue<decimal>("invoice_tax");
            }
            set
            {
                SetColumnValue("invoice_tax", value);
            }
        }

        [XmlAttribute("InvoiceRequestTime")]
        [Bindable(true)]
        public DateTime? InvoiceRequestTime
        {
            get
            {
                return GetColumnValue<DateTime?>("invoice_request_time");
            }
            set
            {
                SetColumnValue("invoice_request_time", value);
            }
        }

        [XmlAttribute("InvoicePaperedTime")]
        [Bindable(true)]
        public DateTime? InvoicePaperedTime
        {
            get
            {
                return GetColumnValue<DateTime?>("invoice_papered_time");
            }
            set
            {
                SetColumnValue("invoice_papered_time", value);
            }
        }

        [XmlAttribute("InvoiceWinning")]
        [Bindable(true)]
        public bool InvoiceWinning
        {
            get
            {
                return GetColumnValue<bool>("invoice_winning");
            }
            set
            {
                SetColumnValue("invoice_winning", value);
            }
        }

        [XmlAttribute("InvoicePapered")]
        [Bindable(true)]
        public bool InvoicePapered
        {
            get
            {
                return GetColumnValue<bool>("invoice_papered");
            }
            set
            {
                SetColumnValue("invoice_papered", value);
            }
        }

        [XmlAttribute("InvoiceSerialId")]
        [Bindable(true)]
        public int InvoiceSerialId
        {
            get
            {
                return GetColumnValue<int>("invoice_serial_id");
            }
            set
            {
                SetColumnValue("invoice_serial_id", value);
            }
        }

        [XmlAttribute("InvoiceWinnerresponseTime")]
        [Bindable(true)]
        public DateTime? InvoiceWinnerresponseTime
        {
            get
            {
                return GetColumnValue<DateTime?>("invoice_winnerresponse_time");
            }
            set
            {
                SetColumnValue("invoice_winnerresponse_time", value);
            }
        }

        [XmlAttribute("InvoiceWinnerresponsePhone")]
        [Bindable(true)]
        public string InvoiceWinnerresponsePhone
        {
            get
            {
                return GetColumnValue<string>("invoice_winnerresponse_phone");
            }
            set
            {
                SetColumnValue("invoice_winnerresponse_phone", value);
            }
        }

        [XmlAttribute("InvoiceStatus")]
        [Bindable(true)]
        public int? InvoiceStatus
        {
            get
            {
                return GetColumnValue<int?>("invoice_status");
            }
            set
            {
                SetColumnValue("invoice_status", value);
            }
        }

        [XmlAttribute("InvoiceMailbackPaper")]
        [Bindable(true)]
        public bool? InvoiceMailbackPaper
        {
            get
            {
                return GetColumnValue<bool?>("invoice_mailback_paper");
            }
            set
            {
                SetColumnValue("invoice_mailback_paper", value);
            }
        }

        [XmlAttribute("InvoiceMailbackAllowance")]
        [Bindable(true)]
        public bool? InvoiceMailbackAllowance
        {
            get
            {
                return GetColumnValue<bool?>("invoice_mailback_allowance");
            }
            set
            {
                SetColumnValue("invoice_mailback_allowance", value);
            }
        }

        [XmlAttribute("DetailId")]
        [Bindable(true)]
        public int DetailId
        {
            get
            {
                return GetColumnValue<int>("detail_id");
            }
            set
            {
                SetColumnValue("detail_id", value);
            }
        }

        [XmlAttribute("DetailInvoiceAmount")]
        [Bindable(true)]
        public decimal DetailInvoiceAmount
        {
            get
            {
                return GetColumnValue<decimal>("detail_invoice_amount");
            }
            set
            {
                SetColumnValue("detail_invoice_amount", value);
            }
        }

        [XmlAttribute("DetailCreateTime")]
        [Bindable(true)]
        public DateTime DetailCreateTime
        {
            get
            {
                return GetColumnValue<DateTime>("detail_create_time");
            }
            set
            {
                SetColumnValue("detail_create_time", value);
            }
        }

        [XmlAttribute("DetailCreator")]
        [Bindable(true)]
        public string DetailCreator
        {
            get
            {
                return GetColumnValue<string>("detail_creator");
            }
            set
            {
                SetColumnValue("detail_creator", value);
            }
        }

        [XmlAttribute("DetailMessage")]
        [Bindable(true)]
        public string DetailMessage
        {
            get
            {
                return GetColumnValue<string>("detail_message");
            }
            set
            {
                SetColumnValue("detail_message", value);
            }
        }

        [XmlAttribute("DetailStatus")]
        [Bindable(true)]
        public bool DetailStatus
        {
            get
            {
                return GetColumnValue<bool>("detail_status");
            }
            set
            {
                SetColumnValue("detail_status", value);
            }
        }

        [XmlAttribute("InvoiceType")]
        [Bindable(true)]
        public int InvoiceType
        {
            get
            {
                return GetColumnValue<int>("invoice_type");
            }
            set
            {
                SetColumnValue("invoice_type", value);
            }
        }

        [XmlAttribute("CouponId")]
        [Bindable(true)]
        public int? CouponId
        {
            get
            {
                return GetColumnValue<int?>("coupon_id");
            }
            set
            {
                SetColumnValue("coupon_id", value);
            }
        }

        [XmlAttribute("VerifiedTime")]
        [Bindable(true)]
        public DateTime? VerifiedTime
        {
            get
            {
                return GetColumnValue<DateTime?>("verified_time");
            }
            set
            {
                SetColumnValue("verified_time", value);
            }
        }

        [XmlAttribute("InvoiceMode2")]
        [Bindable(true)]
        public int InvoiceMode2
        {
            get
            {
                return GetColumnValue<int>("invoice_mode2");
            }
            set
            {
                SetColumnValue("invoice_mode2", value);
            }
        }

        [XmlAttribute("CarrierType")]
        [Bindable(true)]
        public int CarrierType
        {
            get
            {
                return GetColumnValue<int>("carrier_type");
            }
            set
            {
                SetColumnValue("carrier_type", value);
            }
        }

        [XmlAttribute("CarrierId")]
        [Bindable(true)]
        public string CarrierId
        {
            get
            {
                return GetColumnValue<string>("carrier_id");
            }
            set
            {
                SetColumnValue("carrier_id", value);
            }
        }

        [XmlAttribute("LoveCode")]
        [Bindable(true)]
        public string LoveCode
        {
            get
            {
                return GetColumnValue<string>("love_code");
            }
            set
            {
                SetColumnValue("love_code", value);
            }
        }

        [XmlAttribute("InvoiceVoidTime")]
        [Bindable(true)]
        public DateTime? InvoiceVoidTime
        {
            get
            {
                return GetColumnValue<DateTime?>("invoice_void_time");
            }
            set
            {
                SetColumnValue("invoice_void_time", value);
            }
        }

        [XmlAttribute("InvoiceVoidMsg")]
        [Bindable(true)]
        public string InvoiceVoidMsg
        {
            get
            {
                return GetColumnValue<string>("invoice_void_msg");
            }
            set
            {
                SetColumnValue("invoice_void_msg", value);
            }
        }

        [XmlAttribute("PartnerOrderId")]
        [Bindable(true)]
        public string PartnerOrderId
        {
            get
            {
                return GetColumnValue<string>("partner_order_id");
            }
            set
            {
                SetColumnValue("partner_order_id", value);
            }
        }

        [XmlAttribute("WinnerMail")]
        [Bindable(true)]
        public string WinnerMail
        {
            get
            {
                return GetColumnValue<string>("winner_mail");
            }
            set
            {
                SetColumnValue("winner_mail", value);
            }
        }

        #endregion

        #region Columns Struct
        public struct Columns
        {


            public static string Id = @"id";

            public static string InvoiceNumber = @"invoice_number";

            public static string InvoiceNumberTime = @"invoice_number_time";

            public static string InvoiceComId = @"invoice_com_id";

            public static string InvoiceComName = @"invoice_com_name";

            public static string InvoiceBuyerName = @"invoice_buyer_name";

            public static string InvoiceBuyerAddress = @"invoice_buyer_address";

            public static string InvoicePass = @"invoice_pass";

            public static string OrderItem = @"order_item";

            public static string OrderAmount = @"order_amount";

            public static string InvoiceSumAmount = @"invoice_sum_amount";

            public static string InvoiceTax = @"invoice_tax";

            public static string InvoiceRequestTime = @"invoice_request_time";

            public static string InvoicePaperedTime = @"invoice_papered_time";

            public static string InvoiceWinning = @"invoice_winning";

            public static string InvoicePapered = @"invoice_papered";

            public static string InvoiceSerialId = @"invoice_serial_id";

            public static string InvoiceWinnerresponseTime = @"invoice_winnerresponse_time";

            public static string InvoiceWinnerresponsePhone = @"invoice_winnerresponse_phone";

            public static string InvoiceStatus = @"invoice_status";

            public static string InvoiceMailbackPaper = @"invoice_mailback_paper";

            public static string InvoiceMailbackAllowance = @"invoice_mailback_allowance";

            public static string DetailId = @"detail_id";

            public static string DetailInvoiceAmount = @"detail_invoice_amount";

            public static string DetailCreateTime = @"detail_create_time";

            public static string DetailCreator = @"detail_creator";

            public static string DetailMessage = @"detail_message";

            public static string DetailStatus = @"detail_status";

            public static string InvoiceType = @"invoice_type";

            public static string CouponId = @"coupon_id";

            public static string VerifiedTime = @"verified_time";

            public static string InvoiceMode2 = @"invoice_mode2";

            public static string CarrierType = @"carrier_type";

            public static string CarrierId = @"carrier_id";

            public static string LoveCode = @"love_code";

            public static string InvoiceVoidTime = @"invoice_void_time";

            public static string InvoiceVoidMsg = @"invoice_void_msg";

            public static string PartnerOrderId = @"partner_order_id";

            public static string WinnerMail = @"winner_mail";

        }
        #endregion


        #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName)
        {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName)
        {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion

    }
}

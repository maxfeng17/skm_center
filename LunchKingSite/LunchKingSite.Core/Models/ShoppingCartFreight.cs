﻿using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the ShoppingCartFreight class.
    /// </summary>
    [Serializable]
    public partial class ShoppingCartFreightCollection : RepositoryList<ShoppingCartFreight, ShoppingCartFreightCollection>
    {
        public ShoppingCartFreightCollection() { }

        /// <summary>
        /// Filters an existing collection based on the set criteria. This is an in-memory filter
        /// Thanks to developingchris for this!
        /// </summary>
        /// <returns>ShoppingCartFreightCollection</returns>
        public ShoppingCartFreightCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                ShoppingCartFreight o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }


    }
    /// <summary>
    /// This is an ActiveRecord class which wraps the shopping_cart_freights table.
    /// </summary>
    [Serializable]
    public partial class ShoppingCartFreight : RepositoryRecord<ShoppingCartFreight>, IRecordBase
    {
        #region .ctors and Default Settings

        public ShoppingCartFreight()
        {
            SetSQLProps();
            InitSetDefaults();
            MarkNew();
        }

        private void InitSetDefaults() { SetDefaults(); }

        public ShoppingCartFreight(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
                ForceDefaults();
            MarkNew();
        }

        protected static void SetSQLProps() { GetTableSchema(); }

        #endregion

        #region Schema and Query Accessor	
        public static Query CreateQuery() { return new Query(Schema); }
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                    SetSQLProps();
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("shopping_cart_freights", TableType.Table, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = true;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = true;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                colvarId.DefaultSetting = @"";
                colvarId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarId);

                TableSchema.TableColumn colvarUniqueId = new TableSchema.TableColumn(schema);
                colvarUniqueId.ColumnName = "unique_id";
                colvarUniqueId.DataType = DbType.AnsiString;
                colvarUniqueId.MaxLength = 20;
                colvarUniqueId.AutoIncrement = false;
                colvarUniqueId.IsNullable = false;
                colvarUniqueId.IsPrimaryKey = false;
                colvarUniqueId.IsForeignKey = false;
                colvarUniqueId.IsReadOnly = false;
                colvarUniqueId.DefaultSetting = @"";
                colvarUniqueId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarUniqueId);

                TableSchema.TableColumn colvarSellerGuid = new TableSchema.TableColumn(schema);
                colvarSellerGuid.ColumnName = "seller_guid";
                colvarSellerGuid.DataType = DbType.Guid;
                colvarSellerGuid.MaxLength = 0;
                colvarSellerGuid.AutoIncrement = false;
                colvarSellerGuid.IsNullable = false;
                colvarSellerGuid.IsPrimaryKey = false;
                colvarSellerGuid.IsForeignKey = false;
                colvarSellerGuid.IsReadOnly = false;
                colvarSellerGuid.DefaultSetting = @"";
                colvarSellerGuid.ForeignKeyTableName = "";
                schema.Columns.Add(colvarSellerGuid);

                TableSchema.TableColumn colvarFreightsStatus = new TableSchema.TableColumn(schema);
                colvarFreightsStatus.ColumnName = "freights_status";
                colvarFreightsStatus.DataType = DbType.Int32;
                colvarFreightsStatus.MaxLength = 0;
                colvarFreightsStatus.AutoIncrement = false;
                colvarFreightsStatus.IsNullable = false;
                colvarFreightsStatus.IsPrimaryKey = false;
                colvarFreightsStatus.IsForeignKey = false;
                colvarFreightsStatus.IsReadOnly = false;
                colvarFreightsStatus.DefaultSetting = @"";
                colvarFreightsStatus.ForeignKeyTableName = "";
                schema.Columns.Add(colvarFreightsStatus);

                TableSchema.TableColumn colvarFreightsName = new TableSchema.TableColumn(schema);
                colvarFreightsName.ColumnName = "freights_name";
                colvarFreightsName.DataType = DbType.String;
                colvarFreightsName.MaxLength = 50;
                colvarFreightsName.AutoIncrement = false;
                colvarFreightsName.IsNullable = false;
                colvarFreightsName.IsPrimaryKey = false;
                colvarFreightsName.IsForeignKey = false;
                colvarFreightsName.IsReadOnly = false;
                colvarFreightsName.DefaultSetting = @"";
                colvarFreightsName.ForeignKeyTableName = "";
                schema.Columns.Add(colvarFreightsName);

                TableSchema.TableColumn colvarNoFreightLimit = new TableSchema.TableColumn(schema);
                colvarNoFreightLimit.ColumnName = "no_freight_limit";
                colvarNoFreightLimit.DataType = DbType.Int32;
                colvarNoFreightLimit.MaxLength = 0;
                colvarNoFreightLimit.AutoIncrement = false;
                colvarNoFreightLimit.IsNullable = false;
                colvarNoFreightLimit.IsPrimaryKey = false;
                colvarNoFreightLimit.IsForeignKey = false;
                colvarNoFreightLimit.IsReadOnly = false;
                colvarNoFreightLimit.DefaultSetting = @"";
                colvarNoFreightLimit.ForeignKeyTableName = "";
                schema.Columns.Add(colvarNoFreightLimit);

                TableSchema.TableColumn colvarFreights = new TableSchema.TableColumn(schema);
                colvarFreights.ColumnName = "freights";
                colvarFreights.DataType = DbType.Int32;
                colvarFreights.MaxLength = 0;
                colvarFreights.AutoIncrement = false;
                colvarFreights.IsNullable = false;
                colvarFreights.IsPrimaryKey = false;
                colvarFreights.IsForeignKey = false;
                colvarFreights.IsReadOnly = false;
                colvarFreights.DefaultSetting = @"";
                colvarFreights.ForeignKeyTableName = "";
                schema.Columns.Add(colvarFreights);

                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = false;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                colvarCreateTime.DefaultSetting = @"";
                colvarCreateTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateTime);

                TableSchema.TableColumn colvarCreateUser = new TableSchema.TableColumn(schema);
                colvarCreateUser.ColumnName = "create_user";
                colvarCreateUser.DataType = DbType.AnsiString;
                colvarCreateUser.MaxLength = 50;
                colvarCreateUser.AutoIncrement = false;
                colvarCreateUser.IsNullable = false;
                colvarCreateUser.IsPrimaryKey = false;
                colvarCreateUser.IsForeignKey = false;
                colvarCreateUser.IsReadOnly = false;
                colvarCreateUser.DefaultSetting = @"";
                colvarCreateUser.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateUser);

                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("shopping_cart_freights", schema);
            }
        }
        #endregion

        #region Props

        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id
        {
            get { return GetColumnValue<int>(Columns.Id); }
            set { SetColumnValue(Columns.Id, value); }
        }

        [XmlAttribute("UniqueId")]
        [Bindable(true)]
        public string UniqueId
        {
            get { return GetColumnValue<string>(Columns.UniqueId); }
            set { SetColumnValue(Columns.UniqueId, value); }
        }

        [XmlAttribute("SellerGuid")]
        [Bindable(true)]
        public Guid SellerGuid
        {
            get { return GetColumnValue<Guid>(Columns.SellerGuid); }
            set { SetColumnValue(Columns.SellerGuid, value); }
        }

        [XmlAttribute("FreightsStatus")]
        [Bindable(true)]
        public int FreightsStatus
        {
            get { return GetColumnValue<int>(Columns.FreightsStatus); }
            set { SetColumnValue(Columns.FreightsStatus, value); }
        }

        [XmlAttribute("FreightsName")]
        [Bindable(true)]
        public string FreightsName
        {
            get { return GetColumnValue<string>(Columns.FreightsName); }
            set { SetColumnValue(Columns.FreightsName, value); }
        }

        [XmlAttribute("NoFreightLimit")]
        [Bindable(true)]
        public int NoFreightLimit
        {
            get { return GetColumnValue<int>(Columns.NoFreightLimit); }
            set { SetColumnValue(Columns.NoFreightLimit, value); }
        }

        [XmlAttribute("Freights")]
        [Bindable(true)]
        public int Freights
        {
            get { return GetColumnValue<int>(Columns.Freights); }
            set { SetColumnValue(Columns.Freights, value); }
        }

        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime CreateTime
        {
            get { return GetColumnValue<DateTime>(Columns.CreateTime); }
            set { SetColumnValue(Columns.CreateTime, value); }
        }

        [XmlAttribute("CreateUser")]
        [Bindable(true)]
        public string CreateUser
        {
            get { return GetColumnValue<string>(Columns.CreateUser); }
            set { SetColumnValue(Columns.CreateUser, value); }
        }

        #endregion




        //no foreign key tables defined (0)



        //no ManyToMany tables defined (0)





        #region Typed Columns


        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }



        public static TableSchema.TableColumn UniqueIdColumn
        {
            get { return Schema.Columns[1]; }
        }



        public static TableSchema.TableColumn SellerGuidColumn
        {
            get { return Schema.Columns[2]; }
        }



        public static TableSchema.TableColumn FreightsStatusColumn
        {
            get { return Schema.Columns[3]; }
        }



        public static TableSchema.TableColumn FreightsNameColumn
        {
            get { return Schema.Columns[4]; }
        }



        public static TableSchema.TableColumn NoFreightLimitColumn
        {
            get { return Schema.Columns[5]; }
        }



        public static TableSchema.TableColumn FreightsColumn
        {
            get { return Schema.Columns[6]; }
        }



        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[7]; }
        }



        public static TableSchema.TableColumn CreateUserColumn
        {
            get { return Schema.Columns[8]; }
        }



        #endregion
        #region Columns Struct
        public struct Columns
        {
            public static string Id = @"id";
            public static string UniqueId = @"unique_id";
            public static string SellerGuid = @"seller_guid";
            public static string FreightsStatus = @"freights_status";
            public static string FreightsName = @"freights_name";
            public static string NoFreightLimit = @"no_freight_limit";
            public static string Freights = @"freights";
            public static string CreateTime = @"create_time";
            public static string CreateUser = @"create_user";

        }
        #endregion

        #region Update PK Collections

        #endregion

        #region Deep Save

        #endregion
    }
}

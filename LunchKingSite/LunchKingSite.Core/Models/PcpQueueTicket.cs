using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the PcpQueueTicket class.
	/// </summary>
    [Serializable]
	public partial class PcpQueueTicketCollection : RepositoryList<PcpQueueTicket, PcpQueueTicketCollection>
	{	   
		public PcpQueueTicketCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>PcpQueueTicketCollection</returns>
		public PcpQueueTicketCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                PcpQueueTicket o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the pcp_queue_ticket table.
	/// </summary>
	[Serializable]
	public partial class PcpQueueTicket : RepositoryRecord<PcpQueueTicket>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public PcpQueueTicket()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public PcpQueueTicket(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("pcp_queue_ticket", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarTicketCode = new TableSchema.TableColumn(schema);
				colvarTicketCode.ColumnName = "ticket_code";
				colvarTicketCode.DataType = DbType.String;
				colvarTicketCode.MaxLength = 2;
				colvarTicketCode.AutoIncrement = false;
				colvarTicketCode.IsNullable = false;
				colvarTicketCode.IsPrimaryKey = true;
				colvarTicketCode.IsForeignKey = false;
				colvarTicketCode.IsReadOnly = false;
				colvarTicketCode.DefaultSetting = @"";
				colvarTicketCode.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTicketCode);
				
				TableSchema.TableColumn colvarTicketDate = new TableSchema.TableColumn(schema);
				colvarTicketDate.ColumnName = "ticket_date";
				colvarTicketDate.DataType = DbType.DateTime;
				colvarTicketDate.MaxLength = 0;
				colvarTicketDate.AutoIncrement = false;
				colvarTicketDate.IsNullable = false;
				colvarTicketDate.IsPrimaryKey = true;
				colvarTicketDate.IsForeignKey = false;
				colvarTicketDate.IsReadOnly = false;
				colvarTicketDate.DefaultSetting = @"";
				colvarTicketDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTicketDate);
				
				TableSchema.TableColumn colvarQueueNumber = new TableSchema.TableColumn(schema);
				colvarQueueNumber.ColumnName = "queue_number";
				colvarQueueNumber.DataType = DbType.Int32;
				colvarQueueNumber.MaxLength = 0;
				colvarQueueNumber.AutoIncrement = false;
				colvarQueueNumber.IsNullable = false;
				colvarQueueNumber.IsPrimaryKey = false;
				colvarQueueNumber.IsForeignKey = false;
				colvarQueueNumber.IsReadOnly = false;
				
						colvarQueueNumber.DefaultSetting = @"((1))";
				colvarQueueNumber.ForeignKeyTableName = "";
				schema.Columns.Add(colvarQueueNumber);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("pcp_queue_ticket",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("TicketCode")]
		[Bindable(true)]
		public string TicketCode 
		{
			get { return GetColumnValue<string>(Columns.TicketCode); }
			set { SetColumnValue(Columns.TicketCode, value); }
		}
		  
		[XmlAttribute("TicketDate")]
		[Bindable(true)]
		public DateTime TicketDate 
		{
			get { return GetColumnValue<DateTime>(Columns.TicketDate); }
			set { SetColumnValue(Columns.TicketDate, value); }
		}
		  
		[XmlAttribute("QueueNumber")]
		[Bindable(true)]
		public int QueueNumber 
		{
			get { return GetColumnValue<int>(Columns.QueueNumber); }
			set { SetColumnValue(Columns.QueueNumber, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn TicketCodeColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn TicketDateColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn QueueNumberColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string TicketCode = @"ticket_code";
			 public static string TicketDate = @"ticket_date";
			 public static string QueueNumber = @"queue_number";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

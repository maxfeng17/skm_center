using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the SerialNumber class.
	/// </summary>
    [Serializable]
	public partial class SerialNumberCollection : RepositoryList<SerialNumber, SerialNumberCollection>
	{	   
		public SerialNumberCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>SerialNumberCollection</returns>
		public SerialNumberCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                SerialNumber o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the serial_number table.
	/// </summary>
	[Serializable]
	public partial class SerialNumber : RepositoryRecord<SerialNumber>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public SerialNumber()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public SerialNumber(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("serial_number", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarSerialName = new TableSchema.TableColumn(schema);
				colvarSerialName.ColumnName = "serial_name";
				colvarSerialName.DataType = DbType.String;
				colvarSerialName.MaxLength = 100;
				colvarSerialName.AutoIncrement = false;
				colvarSerialName.IsNullable = false;
				colvarSerialName.IsPrimaryKey = true;
				colvarSerialName.IsForeignKey = false;
				colvarSerialName.IsReadOnly = false;
				colvarSerialName.DefaultSetting = @"";
				colvarSerialName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSerialName);
				
				TableSchema.TableColumn colvarLastValue = new TableSchema.TableColumn(schema);
				colvarLastValue.ColumnName = "last_value";
				colvarLastValue.DataType = DbType.Int32;
				colvarLastValue.MaxLength = 0;
				colvarLastValue.AutoIncrement = false;
				colvarLastValue.IsNullable = false;
				colvarLastValue.IsPrimaryKey = false;
				colvarLastValue.IsForeignKey = false;
				colvarLastValue.IsReadOnly = false;
				colvarLastValue.DefaultSetting = @"";
				colvarLastValue.ForeignKeyTableName = "";
				schema.Columns.Add(colvarLastValue);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("serial_number",schema);
			}
		}
		#endregion
		
		#region Props
		
		[XmlAttribute("SerialName")]
		[Bindable(true)]
		public string SerialName 
		{
			get { return GetColumnValue<string>(Columns.SerialName); }
			set { SetColumnValue(Columns.SerialName, value); }
		}
		
		[XmlAttribute("LastValue")]
		[Bindable(true)]
		public int LastValue 
		{
			get { return GetColumnValue<int>(Columns.LastValue); }
			set { SetColumnValue(Columns.LastValue, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn SerialNameColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn LastValueColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string SerialName = @"serial_name";
			 public static string LastValue = @"last_value";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

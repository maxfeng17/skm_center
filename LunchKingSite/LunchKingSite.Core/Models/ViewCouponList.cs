using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewCouponList class.
    /// </summary>
    [Serializable]
    public partial class ViewCouponListCollection : ReadOnlyList<ViewCouponList, ViewCouponListCollection>
    {        
        public ViewCouponListCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_coupon_list view.
    /// </summary>
    [Serializable]
    public partial class ViewCouponList : ReadOnlyRecord<ViewCouponList>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_coupon_list", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = false;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = false;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                
                schema.Columns.Add(colvarId);
                
                TableSchema.TableColumn colvarOrderDetailId = new TableSchema.TableColumn(schema);
                colvarOrderDetailId.ColumnName = "order_detail_id";
                colvarOrderDetailId.DataType = DbType.Guid;
                colvarOrderDetailId.MaxLength = 0;
                colvarOrderDetailId.AutoIncrement = false;
                colvarOrderDetailId.IsNullable = false;
                colvarOrderDetailId.IsPrimaryKey = false;
                colvarOrderDetailId.IsForeignKey = false;
                colvarOrderDetailId.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderDetailId);
                
                TableSchema.TableColumn colvarSequenceNumber = new TableSchema.TableColumn(schema);
                colvarSequenceNumber.ColumnName = "sequence_number";
                colvarSequenceNumber.DataType = DbType.AnsiString;
                colvarSequenceNumber.MaxLength = 50;
                colvarSequenceNumber.AutoIncrement = false;
                colvarSequenceNumber.IsNullable = false;
                colvarSequenceNumber.IsPrimaryKey = false;
                colvarSequenceNumber.IsForeignKey = false;
                colvarSequenceNumber.IsReadOnly = false;
                
                schema.Columns.Add(colvarSequenceNumber);
                
                TableSchema.TableColumn colvarCode = new TableSchema.TableColumn(schema);
                colvarCode.ColumnName = "code";
                colvarCode.DataType = DbType.AnsiString;
                colvarCode.MaxLength = 50;
                colvarCode.AutoIncrement = false;
                colvarCode.IsNullable = false;
                colvarCode.IsPrimaryKey = false;
                colvarCode.IsForeignKey = false;
                colvarCode.IsReadOnly = false;
                
                schema.Columns.Add(colvarCode);
                
                TableSchema.TableColumn colvarDescription = new TableSchema.TableColumn(schema);
                colvarDescription.ColumnName = "description";
                colvarDescription.DataType = DbType.String;
                colvarDescription.MaxLength = 50;
                colvarDescription.AutoIncrement = false;
                colvarDescription.IsNullable = true;
                colvarDescription.IsPrimaryKey = false;
                colvarDescription.IsForeignKey = false;
                colvarDescription.IsReadOnly = false;
                
                schema.Columns.Add(colvarDescription);
                
                TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
                colvarStatus.ColumnName = "status";
                colvarStatus.DataType = DbType.Int32;
                colvarStatus.MaxLength = 0;
                colvarStatus.AutoIncrement = false;
                colvarStatus.IsNullable = false;
                colvarStatus.IsPrimaryKey = false;
                colvarStatus.IsForeignKey = false;
                colvarStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarStatus);
                
                TableSchema.TableColumn colvarAvailable = new TableSchema.TableColumn(schema);
                colvarAvailable.ColumnName = "available";
                colvarAvailable.DataType = DbType.Boolean;
                colvarAvailable.MaxLength = 0;
                colvarAvailable.AutoIncrement = false;
                colvarAvailable.IsNullable = true;
                colvarAvailable.IsPrimaryKey = false;
                colvarAvailable.IsForeignKey = false;
                colvarAvailable.IsReadOnly = false;
                
                schema.Columns.Add(colvarAvailable);
                
                TableSchema.TableColumn colvarStoreSequence = new TableSchema.TableColumn(schema);
                colvarStoreSequence.ColumnName = "store_sequence";
                colvarStoreSequence.DataType = DbType.Int32;
                colvarStoreSequence.MaxLength = 0;
                colvarStoreSequence.AutoIncrement = false;
                colvarStoreSequence.IsNullable = true;
                colvarStoreSequence.IsPrimaryKey = false;
                colvarStoreSequence.IsForeignKey = false;
                colvarStoreSequence.IsReadOnly = false;
                
                schema.Columns.Add(colvarStoreSequence);
                
                TableSchema.TableColumn colvarIsReservationLock = new TableSchema.TableColumn(schema);
                colvarIsReservationLock.ColumnName = "is_reservation_lock";
                colvarIsReservationLock.DataType = DbType.Boolean;
                colvarIsReservationLock.MaxLength = 0;
                colvarIsReservationLock.AutoIncrement = false;
                colvarIsReservationLock.IsNullable = false;
                colvarIsReservationLock.IsPrimaryKey = false;
                colvarIsReservationLock.IsForeignKey = false;
                colvarIsReservationLock.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsReservationLock);
                
                TableSchema.TableColumn colvarOrderGuid = new TableSchema.TableColumn(schema);
                colvarOrderGuid.ColumnName = "order_guid";
                colvarOrderGuid.DataType = DbType.Guid;
                colvarOrderGuid.MaxLength = 0;
                colvarOrderGuid.AutoIncrement = false;
                colvarOrderGuid.IsNullable = false;
                colvarOrderGuid.IsPrimaryKey = false;
                colvarOrderGuid.IsForeignKey = false;
                colvarOrderGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderGuid);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_coupon_list",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewCouponList()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewCouponList(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewCouponList(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewCouponList(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id 
	    {
		    get
		    {
			    return GetColumnValue<int>("id");
		    }
            set 
		    {
			    SetColumnValue("id", value);
            }
        }
	      
        [XmlAttribute("OrderDetailId")]
        [Bindable(true)]
        public Guid OrderDetailId 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("order_detail_id");
		    }
            set 
		    {
			    SetColumnValue("order_detail_id", value);
            }
        }
	      
        [XmlAttribute("SequenceNumber")]
        [Bindable(true)]
        public string SequenceNumber 
	    {
		    get
		    {
			    return GetColumnValue<string>("sequence_number");
		    }
            set 
		    {
			    SetColumnValue("sequence_number", value);
            }
        }
	      
        [XmlAttribute("Code")]
        [Bindable(true)]
        public string Code 
	    {
		    get
		    {
			    return GetColumnValue<string>("code");
		    }
            set 
		    {
			    SetColumnValue("code", value);
            }
        }
	      
        [XmlAttribute("Description")]
        [Bindable(true)]
        public string Description 
	    {
		    get
		    {
			    return GetColumnValue<string>("description");
		    }
            set 
		    {
			    SetColumnValue("description", value);
            }
        }
	      
        [XmlAttribute("Status")]
        [Bindable(true)]
        public int Status 
	    {
		    get
		    {
			    return GetColumnValue<int>("status");
		    }
            set 
		    {
			    SetColumnValue("status", value);
            }
        }
	      
        [XmlAttribute("Available")]
        [Bindable(true)]
        public bool? Available 
	    {
		    get
		    {
			    return GetColumnValue<bool?>("available");
		    }
            set 
		    {
			    SetColumnValue("available", value);
            }
        }
	      
        [XmlAttribute("StoreSequence")]
        [Bindable(true)]
        public int? StoreSequence 
	    {
		    get
		    {
			    return GetColumnValue<int?>("store_sequence");
		    }
            set 
		    {
			    SetColumnValue("store_sequence", value);
            }
        }
	      
        [XmlAttribute("IsReservationLock")]
        [Bindable(true)]
        public bool IsReservationLock 
	    {
		    get
		    {
			    return GetColumnValue<bool>("is_reservation_lock");
		    }
            set 
		    {
			    SetColumnValue("is_reservation_lock", value);
            }
        }
	      
        [XmlAttribute("OrderGuid")]
        [Bindable(true)]
        public Guid OrderGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("order_guid");
		    }
            set 
		    {
			    SetColumnValue("order_guid", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string Id = @"id";
            
            public static string OrderDetailId = @"order_detail_id";
            
            public static string SequenceNumber = @"sequence_number";
            
            public static string Code = @"code";
            
            public static string Description = @"description";
            
            public static string Status = @"status";
            
            public static string Available = @"available";
            
            public static string StoreSequence = @"store_sequence";
            
            public static string IsReservationLock = @"is_reservation_lock";
            
            public static string OrderGuid = @"order_guid";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

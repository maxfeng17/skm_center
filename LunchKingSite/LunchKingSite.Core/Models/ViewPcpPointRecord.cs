﻿using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the ViewPcpPointRecord class.
    /// </summary>
    [Serializable]
    public partial class ViewPcpPointRecordCollection : ReadOnlyList<ViewPcpPointRecord, ViewPcpPointRecordCollection>
    {
        public ViewPcpPointRecordCollection() { }
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_pcp_point_record view.
    /// </summary>
    [Serializable]
    public partial class ViewPcpPointRecord : ReadOnlyRecord<ViewPcpPointRecord>, IReadOnlyRecord
    {

        #region Default Settings
        protected static void SetSQLProps()
        {
            GetTableSchema();
        }
        #endregion
        #region Schema Accessor
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_pcp_point_record", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarDepositId = new TableSchema.TableColumn(schema);
                colvarDepositId.ColumnName = "deposit_id";
                colvarDepositId.DataType = DbType.Int32;
                colvarDepositId.MaxLength = 0;
                colvarDepositId.AutoIncrement = false;
                colvarDepositId.IsNullable = true;
                colvarDepositId.IsPrimaryKey = false;
                colvarDepositId.IsForeignKey = false;
                colvarDepositId.IsReadOnly = false;

                schema.Columns.Add(colvarDepositId);

                TableSchema.TableColumn colvarWithdrawaloverallId = new TableSchema.TableColumn(schema);
                colvarWithdrawaloverallId.ColumnName = "withdrawaloverall_id";
                colvarWithdrawaloverallId.DataType = DbType.Int32;
                colvarWithdrawaloverallId.MaxLength = 0;
                colvarWithdrawaloverallId.AutoIncrement = false;
                colvarWithdrawaloverallId.IsNullable = false;
                colvarWithdrawaloverallId.IsPrimaryKey = false;
                colvarWithdrawaloverallId.IsForeignKey = false;
                colvarWithdrawaloverallId.IsReadOnly = false;

                schema.Columns.Add(colvarWithdrawaloverallId);

                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = false;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;

                schema.Columns.Add(colvarCreateTime);

                TableSchema.TableColumn colvarExpireTime = new TableSchema.TableColumn(schema);
                colvarExpireTime.ColumnName = "expire_time";
                colvarExpireTime.DataType = DbType.DateTime;
                colvarExpireTime.MaxLength = 0;
                colvarExpireTime.AutoIncrement = false;
                colvarExpireTime.IsNullable = true;
                colvarExpireTime.IsPrimaryKey = false;
                colvarExpireTime.IsForeignKey = false;
                colvarExpireTime.IsReadOnly = false;

                schema.Columns.Add(colvarExpireTime);

                TableSchema.TableColumn colvarMessage = new TableSchema.TableColumn(schema);
                colvarMessage.ColumnName = "message";
                colvarMessage.DataType = DbType.String;
                colvarMessage.MaxLength = -1;
                colvarMessage.AutoIncrement = false;
                colvarMessage.IsNullable = true;
                colvarMessage.IsPrimaryKey = false;
                colvarMessage.IsForeignKey = false;
                colvarMessage.IsReadOnly = false;

                schema.Columns.Add(colvarMessage);

                TableSchema.TableColumn colvarPointIn = new TableSchema.TableColumn(schema);
                colvarPointIn.ColumnName = "pointIn";
                colvarPointIn.DataType = DbType.Int32;
                colvarPointIn.MaxLength = 0;
                colvarPointIn.AutoIncrement = false;
                colvarPointIn.IsNullable = false;
                colvarPointIn.IsPrimaryKey = false;
                colvarPointIn.IsForeignKey = false;
                colvarPointIn.IsReadOnly = false;

                schema.Columns.Add(colvarPointIn);

                TableSchema.TableColumn colvarPointOut = new TableSchema.TableColumn(schema);
                colvarPointOut.ColumnName = "pointOut";
                colvarPointOut.DataType = DbType.Int32;
                colvarPointOut.MaxLength = 0;
                colvarPointOut.AutoIncrement = false;
                colvarPointOut.IsNullable = false;
                colvarPointOut.IsPrimaryKey = false;
                colvarPointOut.IsForeignKey = false;
                colvarPointOut.IsReadOnly = false;

                schema.Columns.Add(colvarPointOut);

                TableSchema.TableColumn colvarTotal = new TableSchema.TableColumn(schema);
                colvarTotal.ColumnName = "total";
                colvarTotal.DataType = DbType.Int32;
                colvarTotal.MaxLength = 0;
                colvarTotal.AutoIncrement = false;
                colvarTotal.IsNullable = true;
                colvarTotal.IsPrimaryKey = false;
                colvarTotal.IsForeignKey = false;
                colvarTotal.IsReadOnly = false;

                schema.Columns.Add(colvarTotal);

                TableSchema.TableColumn colvarPointType = new TableSchema.TableColumn(schema);
                colvarPointType.ColumnName = "point_type";
                colvarPointType.DataType = DbType.Byte;
                colvarPointType.MaxLength = 0;
                colvarPointType.AutoIncrement = false;
                colvarPointType.IsNullable = false;
                colvarPointType.IsPrimaryKey = false;
                colvarPointType.IsForeignKey = false;
                colvarPointType.IsReadOnly = false;

                schema.Columns.Add(colvarPointType);

                TableSchema.TableColumn colvarAccountId = new TableSchema.TableColumn(schema);
                colvarAccountId.ColumnName = "account_id";
                colvarAccountId.DataType = DbType.String;
                colvarAccountId.MaxLength = 256;
                colvarAccountId.AutoIncrement = false;
                colvarAccountId.IsNullable = true;
                colvarAccountId.IsPrimaryKey = false;
                colvarAccountId.IsForeignKey = false;
                colvarAccountId.IsReadOnly = false;

                schema.Columns.Add(colvarAccountId);

                TableSchema.TableColumn colvarUserId = new TableSchema.TableColumn(schema);
                colvarUserId.ColumnName = "user_id";
                colvarUserId.DataType = DbType.Int32;
                colvarUserId.MaxLength = 0;
                colvarUserId.AutoIncrement = false;
                colvarUserId.IsNullable = false;
                colvarUserId.IsPrimaryKey = false;
                colvarUserId.IsForeignKey = false;
                colvarUserId.IsReadOnly = false;

                schema.Columns.Add(colvarUserId);


                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_pcp_point_record", schema);
            }
        }
        #endregion

        #region Query Accessor
        public static Query CreateQuery()
        {
            return new Query(Schema);
        }
        #endregion

        #region .ctors
        public ViewPcpPointRecord()
        {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewPcpPointRecord(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
            {
                ForceDefaults();
            }
            MarkNew();
        }

        public ViewPcpPointRecord(object keyID)
        {
            SetSQLProps();
            LoadByKey(keyID);
        }

        public ViewPcpPointRecord(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName, columnValue);
        }

        #endregion

        #region Props


        [XmlAttribute("DepositId")]
        [Bindable(true)]
        public int? DepositId
        {
            get
            {
                return GetColumnValue<int?>("deposit_id");
            }
            set
            {
                SetColumnValue("deposit_id", value);
            }
        }

        [XmlAttribute("WithdrawaloverallId")]
        [Bindable(true)]
        public int WithdrawaloverallId
        {
            get
            {
                return GetColumnValue<int>("withdrawaloverall_id");
            }
            set
            {
                SetColumnValue("withdrawaloverall_id", value);
            }
        }

        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime CreateTime
        {
            get
            {
                return GetColumnValue<DateTime>("create_time");
            }
            set
            {
                SetColumnValue("create_time", value);
            }
        }

        [XmlAttribute("ExpireTime")]
        [Bindable(true)]
        public DateTime? ExpireTime
        {
            get
            {
                return GetColumnValue<DateTime?>("expire_time");
            }
            set
            {
                SetColumnValue("expire_time", value);
            }
        }

        [XmlAttribute("Message")]
        [Bindable(true)]
        public string Message
        {
            get
            {
                return GetColumnValue<string>("message");
            }
            set
            {
                SetColumnValue("message", value);
            }
        }

        [XmlAttribute("PointIn")]
        [Bindable(true)]
        public int PointIn
        {
            get
            {
                return GetColumnValue<int>("pointIn");
            }
            set
            {
                SetColumnValue("pointIn", value);
            }
        }

        [XmlAttribute("PointOut")]
        [Bindable(true)]
        public int PointOut
        {
            get
            {
                return GetColumnValue<int>("pointOut");
            }
            set
            {
                SetColumnValue("pointOut", value);
            }
        }

        [XmlAttribute("Total")]
        [Bindable(true)]
        public int? Total
        {
            get
            {
                return GetColumnValue<int?>("total");
            }
            set
            {
                SetColumnValue("total", value);
            }
        }

        [XmlAttribute("PointType")]
        [Bindable(true)]
        public byte PointType
        {
            get
            {
                return GetColumnValue<byte>("point_type");
            }
            set
            {
                SetColumnValue("point_type", value);
            }
        }

        [XmlAttribute("AccountId")]
        [Bindable(true)]
        public string AccountId
        {
            get
            {
                return GetColumnValue<string>("account_id");
            }
            set
            {
                SetColumnValue("account_id", value);
            }
        }

        [XmlAttribute("UserId")]
        [Bindable(true)]
        public int UserId
        {
            get
            {
                return GetColumnValue<int>("user_id");
            }
            set
            {
                SetColumnValue("user_id", value);
            }
        }

        #endregion

        #region Columns Struct
        public struct Columns
        {


            public static string DepositId = @"deposit_id";

            public static string WithdrawaloverallId = @"withdrawaloverall_id";

            public static string CreateTime = @"create_time";

            public static string ExpireTime = @"expire_time";

            public static string Message = @"message";

            public static string PointIn = @"pointIn";

            public static string PointOut = @"pointOut";

            public static string Total = @"total";

            public static string PointType = @"point_type";

            public static string AccountId = @"account_id";

            public static string UserId = @"user_id";

        }
        #endregion


        #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName)
        {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName)
        {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion

    }
}

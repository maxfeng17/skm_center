using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the IdentityCode class.
	/// </summary>
    [Serializable]
	public partial class IdentityCodeCollection : RepositoryList<IdentityCode, IdentityCodeCollection>
	{	   
		public IdentityCodeCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>IdentityCodeCollection</returns>
		public IdentityCodeCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                IdentityCode o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the identity_code table.
	/// </summary>
	[Serializable]
	public partial class IdentityCode : RepositoryRecord<IdentityCode>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public IdentityCode()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public IdentityCode(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("identity_code", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int64;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarCardId = new TableSchema.TableColumn(schema);
				colvarCardId.ColumnName = "card_id";
				colvarCardId.DataType = DbType.Int32;
				colvarCardId.MaxLength = 0;
				colvarCardId.AutoIncrement = false;
				colvarCardId.IsNullable = false;
				colvarCardId.IsPrimaryKey = false;
				colvarCardId.IsForeignKey = false;
				colvarCardId.IsReadOnly = false;
				colvarCardId.DefaultSetting = @"";
				colvarCardId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCardId);
				
				TableSchema.TableColumn colvarCode = new TableSchema.TableColumn(schema);
				colvarCode.ColumnName = "code";
				colvarCode.DataType = DbType.String;
				colvarCode.MaxLength = 10;
				colvarCode.AutoIncrement = false;
				colvarCode.IsNullable = false;
				colvarCode.IsPrimaryKey = false;
				colvarCode.IsForeignKey = false;
				colvarCode.IsReadOnly = false;
				colvarCode.DefaultSetting = @"";
				colvarCode.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCode);
				
				TableSchema.TableColumn colvarUserId = new TableSchema.TableColumn(schema);
				colvarUserId.ColumnName = "user_id";
				colvarUserId.DataType = DbType.Int32;
				colvarUserId.MaxLength = 0;
				colvarUserId.AutoIncrement = false;
				colvarUserId.IsNullable = false;
				colvarUserId.IsPrimaryKey = false;
				colvarUserId.IsForeignKey = false;
				colvarUserId.IsReadOnly = false;
				colvarUserId.DefaultSetting = @"";
				colvarUserId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUserId);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarExpiredTime = new TableSchema.TableColumn(schema);
				colvarExpiredTime.ColumnName = "expired_time";
				colvarExpiredTime.DataType = DbType.DateTime;
				colvarExpiredTime.MaxLength = 0;
				colvarExpiredTime.AutoIncrement = false;
				colvarExpiredTime.IsNullable = true;
				colvarExpiredTime.IsPrimaryKey = false;
				colvarExpiredTime.IsForeignKey = false;
				colvarExpiredTime.IsReadOnly = false;
				colvarExpiredTime.DefaultSetting = @"";
				colvarExpiredTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarExpiredTime);
				
				TableSchema.TableColumn colvarDiscountCodeId = new TableSchema.TableColumn(schema);
				colvarDiscountCodeId.ColumnName = "discount_code_id";
				colvarDiscountCodeId.DataType = DbType.Int32;
				colvarDiscountCodeId.MaxLength = 0;
				colvarDiscountCodeId.AutoIncrement = false;
				colvarDiscountCodeId.IsNullable = true;
				colvarDiscountCodeId.IsPrimaryKey = false;
				colvarDiscountCodeId.IsForeignKey = false;
				colvarDiscountCodeId.IsReadOnly = false;
				colvarDiscountCodeId.DefaultSetting = @"";
				colvarDiscountCodeId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDiscountCodeId);
				
				TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
				colvarStatus.ColumnName = "status";
				colvarStatus.DataType = DbType.Byte;
				colvarStatus.MaxLength = 0;
				colvarStatus.AutoIncrement = false;
				colvarStatus.IsNullable = false;
				colvarStatus.IsPrimaryKey = false;
				colvarStatus.IsForeignKey = false;
				colvarStatus.IsReadOnly = false;
				
						colvarStatus.DefaultSetting = @"((0))";
				colvarStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStatus);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("identity_code",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public long Id 
		{
			get { return GetColumnValue<long>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("CardId")]
		[Bindable(true)]
		public int CardId 
		{
			get { return GetColumnValue<int>(Columns.CardId); }
			set { SetColumnValue(Columns.CardId, value); }
		}
		  
		[XmlAttribute("Code")]
		[Bindable(true)]
		public string Code 
		{
			get { return GetColumnValue<string>(Columns.Code); }
			set { SetColumnValue(Columns.Code, value); }
		}
		  
		[XmlAttribute("UserId")]
		[Bindable(true)]
		public int UserId 
		{
			get { return GetColumnValue<int>(Columns.UserId); }
			set { SetColumnValue(Columns.UserId, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		  
		[XmlAttribute("ExpiredTime")]
		[Bindable(true)]
		public DateTime? ExpiredTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.ExpiredTime); }
			set { SetColumnValue(Columns.ExpiredTime, value); }
		}
		  
		[XmlAttribute("DiscountCodeId")]
		[Bindable(true)]
		public int? DiscountCodeId 
		{
			get { return GetColumnValue<int?>(Columns.DiscountCodeId); }
			set { SetColumnValue(Columns.DiscountCodeId, value); }
		}
		  
		[XmlAttribute("Status")]
		[Bindable(true)]
		public byte Status 
		{
			get { return GetColumnValue<byte>(Columns.Status); }
			set { SetColumnValue(Columns.Status, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn CardIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn CodeColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn UserIdColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn ExpiredTimeColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn DiscountCodeIdColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn StatusColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string CardId = @"card_id";
			 public static string Code = @"code";
			 public static string UserId = @"user_id";
			 public static string CreateTime = @"create_time";
			 public static string ExpiredTime = @"expired_time";
			 public static string DiscountCodeId = @"discount_code_id";
			 public static string Status = @"status";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

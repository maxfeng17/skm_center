using System;
using System.ComponentModel;
using System.Data;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the AccessoryCategory class.
	/// </summary>
    [Serializable]
	public partial class AccessoryCategoryCollection : RepositoryList<AccessoryCategory, AccessoryCategoryCollection>
	{	   
		public AccessoryCategoryCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>AccessoryCategoryCollection</returns>
		public AccessoryCategoryCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                AccessoryCategory o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the accessory_category table.
	/// </summary>
	[Serializable]
	public partial class AccessoryCategory : RepositoryRecord<AccessoryCategory>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public AccessoryCategory()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public AccessoryCategory(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("accessory_category", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarGuid = new TableSchema.TableColumn(schema);
				colvarGuid.ColumnName = "GUID";
				colvarGuid.DataType = DbType.Guid;
				colvarGuid.MaxLength = 0;
				colvarGuid.AutoIncrement = false;
				colvarGuid.IsNullable = false;
				colvarGuid.IsPrimaryKey = true;
				colvarGuid.IsForeignKey = false;
				colvarGuid.IsReadOnly = false;
				colvarGuid.DefaultSetting = @"";
				colvarGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGuid);
				
				TableSchema.TableColumn colvarAccessoryCategoryId = new TableSchema.TableColumn(schema);
				colvarAccessoryCategoryId.ColumnName = "accessory_category_id";
				colvarAccessoryCategoryId.DataType = DbType.AnsiString;
				colvarAccessoryCategoryId.MaxLength = 20;
				colvarAccessoryCategoryId.AutoIncrement = false;
				colvarAccessoryCategoryId.IsNullable = true;
				colvarAccessoryCategoryId.IsPrimaryKey = false;
				colvarAccessoryCategoryId.IsForeignKey = false;
				colvarAccessoryCategoryId.IsReadOnly = false;
				colvarAccessoryCategoryId.DefaultSetting = @"";
				colvarAccessoryCategoryId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAccessoryCategoryId);
				
				TableSchema.TableColumn colvarAccessoryCategoryName = new TableSchema.TableColumn(schema);
				colvarAccessoryCategoryName.ColumnName = "accessory_category_name";
				colvarAccessoryCategoryName.DataType = DbType.String;
				colvarAccessoryCategoryName.MaxLength = 50;
				colvarAccessoryCategoryName.AutoIncrement = false;
				colvarAccessoryCategoryName.IsNullable = false;
				colvarAccessoryCategoryName.IsPrimaryKey = false;
				colvarAccessoryCategoryName.IsForeignKey = false;
				colvarAccessoryCategoryName.IsReadOnly = false;
				colvarAccessoryCategoryName.DefaultSetting = @"";
				colvarAccessoryCategoryName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAccessoryCategoryName);
				
				TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
				colvarCreateId.ColumnName = "create_id";
				colvarCreateId.DataType = DbType.String;
				colvarCreateId.MaxLength = 30;
				colvarCreateId.AutoIncrement = false;
				colvarCreateId.IsNullable = false;
				colvarCreateId.IsPrimaryKey = false;
				colvarCreateId.IsForeignKey = false;
				colvarCreateId.IsReadOnly = false;
				colvarCreateId.DefaultSetting = @"";
				colvarCreateId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateId);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarModifyId = new TableSchema.TableColumn(schema);
				colvarModifyId.ColumnName = "modify_id";
				colvarModifyId.DataType = DbType.String;
				colvarModifyId.MaxLength = 30;
				colvarModifyId.AutoIncrement = false;
				colvarModifyId.IsNullable = true;
				colvarModifyId.IsPrimaryKey = false;
				colvarModifyId.IsForeignKey = false;
				colvarModifyId.IsReadOnly = false;
				colvarModifyId.DefaultSetting = @"";
				colvarModifyId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyId);
				
				TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
				colvarModifyTime.ColumnName = "modify_time";
				colvarModifyTime.DataType = DbType.DateTime;
				colvarModifyTime.MaxLength = 0;
				colvarModifyTime.AutoIncrement = false;
				colvarModifyTime.IsNullable = true;
				colvarModifyTime.IsPrimaryKey = false;
				colvarModifyTime.IsForeignKey = false;
				colvarModifyTime.IsReadOnly = false;
				colvarModifyTime.DefaultSetting = @"";
				colvarModifyTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyTime);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("accessory_category",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Guid")]
		[Bindable(true)]
		public Guid Guid 
		{
			get { return GetColumnValue<Guid>(Columns.Guid); }
			set { SetColumnValue(Columns.Guid, value); }
		}
		  
		[XmlAttribute("AccessoryCategoryId")]
		[Bindable(true)]
		public string AccessoryCategoryId 
		{
			get { return GetColumnValue<string>(Columns.AccessoryCategoryId); }
			set { SetColumnValue(Columns.AccessoryCategoryId, value); }
		}
		  
		[XmlAttribute("AccessoryCategoryName")]
		[Bindable(true)]
		public string AccessoryCategoryName 
		{
			get { return GetColumnValue<string>(Columns.AccessoryCategoryName); }
			set { SetColumnValue(Columns.AccessoryCategoryName, value); }
		}
		  
		[XmlAttribute("CreateId")]
		[Bindable(true)]
		public string CreateId 
		{
			get { return GetColumnValue<string>(Columns.CreateId); }
			set { SetColumnValue(Columns.CreateId, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		  
		[XmlAttribute("ModifyId")]
		[Bindable(true)]
		public string ModifyId 
		{
			get { return GetColumnValue<string>(Columns.ModifyId); }
			set { SetColumnValue(Columns.ModifyId, value); }
		}
		  
		[XmlAttribute("ModifyTime")]
		[Bindable(true)]
		public DateTime? ModifyTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.ModifyTime); }
			set { SetColumnValue(Columns.ModifyTime, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn GuidColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn AccessoryCategoryIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn AccessoryCategoryNameColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyIdColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Guid = @"GUID";
			 public static string AccessoryCategoryId = @"accessory_category_id";
			 public static string AccessoryCategoryName = @"accessory_category_name";
			 public static string CreateId = @"create_id";
			 public static string CreateTime = @"create_time";
			 public static string ModifyId = @"modify_id";
			 public static string ModifyTime = @"modify_time";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

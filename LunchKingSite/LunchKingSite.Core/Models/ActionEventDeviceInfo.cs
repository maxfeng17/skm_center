using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the ActionEventDeviceInfo class.
    /// </summary>
    [Serializable]
    public partial class ActionEventDeviceInfoCollection : RepositoryList<ActionEventDeviceInfo, ActionEventDeviceInfoCollection>
    {
        public ActionEventDeviceInfoCollection() { }

        /// <summary>
        /// Filters an existing collection based on the set criteria. This is an in-memory filter
        /// Thanks to developingchris for this!
        /// </summary>
        /// <returns>ActionEventDeviceInfoCollection</returns>
        public ActionEventDeviceInfoCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                ActionEventDeviceInfo o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }


    }
    /// <summary>
    /// This is an ActiveRecord class which wraps the action_event_device_info table.
    /// </summary>
    [Serializable]
    public partial class ActionEventDeviceInfo : RepositoryRecord<ActionEventDeviceInfo>, IRecordBase
    {
        #region .ctors and Default Settings

        public ActionEventDeviceInfo()
        {
            SetSQLProps();
            InitSetDefaults();
            MarkNew();
        }

        private void InitSetDefaults() { SetDefaults(); }

        public ActionEventDeviceInfo(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
                ForceDefaults();
            MarkNew();
        }

        protected static void SetSQLProps() { GetTableSchema(); }

        #endregion

        #region Schema and Query Accessor
        public static Query CreateQuery() { return new Query(Schema); }
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                    SetSQLProps();
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("action_event_device_info", TableType.Table, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = true;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = true;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                colvarId.DefaultSetting = @"";
                colvarId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarId);

                TableSchema.TableColumn colvarActionEventInfoId = new TableSchema.TableColumn(schema);
                colvarActionEventInfoId.ColumnName = "action_event_info_id";
                colvarActionEventInfoId.DataType = DbType.Int32;
                colvarActionEventInfoId.MaxLength = 0;
                colvarActionEventInfoId.AutoIncrement = false;
                colvarActionEventInfoId.IsNullable = false;
                colvarActionEventInfoId.IsPrimaryKey = false;
                colvarActionEventInfoId.IsForeignKey = false;
                colvarActionEventInfoId.IsReadOnly = false;
                colvarActionEventInfoId.DefaultSetting = @"";
                colvarActionEventInfoId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarActionEventInfoId);

                TableSchema.TableColumn colvarMajor = new TableSchema.TableColumn(schema);
                colvarMajor.ColumnName = "major";
                colvarMajor.DataType = DbType.Int32;
                colvarMajor.MaxLength = 0;
                colvarMajor.AutoIncrement = false;
                colvarMajor.IsNullable = false;
                colvarMajor.IsPrimaryKey = false;
                colvarMajor.IsForeignKey = false;
                colvarMajor.IsReadOnly = false;

                colvarMajor.DefaultSetting = @"((0))";
                colvarMajor.ForeignKeyTableName = "";
                schema.Columns.Add(colvarMajor);

                TableSchema.TableColumn colvarMinor = new TableSchema.TableColumn(schema);
                colvarMinor.ColumnName = "minor";
                colvarMinor.DataType = DbType.Int32;
                colvarMinor.MaxLength = 0;
                colvarMinor.AutoIncrement = false;
                colvarMinor.IsNullable = false;
                colvarMinor.IsPrimaryKey = false;
                colvarMinor.IsForeignKey = false;
                colvarMinor.IsReadOnly = false;
                colvarMinor.DefaultSetting = @"";
                colvarMinor.ForeignKeyTableName = "";
                schema.Columns.Add(colvarMinor);

                TableSchema.TableColumn colvarElectricMacAddress = new TableSchema.TableColumn(schema);
                colvarElectricMacAddress.ColumnName = "electric_mac_address";
                colvarElectricMacAddress.DataType = DbType.AnsiString;
                colvarElectricMacAddress.MaxLength = 50;
                colvarElectricMacAddress.AutoIncrement = false;
                colvarElectricMacAddress.IsNullable = false;
                colvarElectricMacAddress.IsPrimaryKey = false;
                colvarElectricMacAddress.IsForeignKey = false;
                colvarElectricMacAddress.IsReadOnly = false;
                colvarElectricMacAddress.DefaultSetting = @"";
                colvarElectricMacAddress.ForeignKeyTableName = "";
                schema.Columns.Add(colvarElectricMacAddress);

                TableSchema.TableColumn colvarElectricPower = new TableSchema.TableColumn(schema);
                colvarElectricPower.ColumnName = "electric_power";
                colvarElectricPower.DataType = DbType.Double;
                colvarElectricPower.MaxLength = 0;
                colvarElectricPower.AutoIncrement = false;
                colvarElectricPower.IsNullable = true;
                colvarElectricPower.IsPrimaryKey = false;
                colvarElectricPower.IsForeignKey = false;
                colvarElectricPower.IsReadOnly = false;
                colvarElectricPower.DefaultSetting = @"";
                colvarElectricPower.ForeignKeyTableName = "";
                schema.Columns.Add(colvarElectricPower);

                TableSchema.TableColumn colvarLastUpdateTime = new TableSchema.TableColumn(schema);
                colvarLastUpdateTime.ColumnName = "last_update_time";
                colvarLastUpdateTime.DataType = DbType.DateTime;
                colvarLastUpdateTime.MaxLength = 0;
                colvarLastUpdateTime.AutoIncrement = false;
                colvarLastUpdateTime.IsNullable = true;
                colvarLastUpdateTime.IsPrimaryKey = false;
                colvarLastUpdateTime.IsForeignKey = false;
                colvarLastUpdateTime.IsReadOnly = false;
                colvarLastUpdateTime.DefaultSetting = @"";
                colvarLastUpdateTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarLastUpdateTime);

                TableSchema.TableColumn colvarDeviceName = new TableSchema.TableColumn(schema);
                colvarDeviceName.ColumnName = "device_name";
                colvarDeviceName.DataType = DbType.String;
                colvarDeviceName.MaxLength = 250;
                colvarDeviceName.AutoIncrement = false;
                colvarDeviceName.IsNullable = true;
                colvarDeviceName.IsPrimaryKey = false;
                colvarDeviceName.IsForeignKey = false;
                colvarDeviceName.IsReadOnly = false;
                colvarDeviceName.DefaultSetting = @"";
                colvarDeviceName.ForeignKeyTableName = "";
                schema.Columns.Add(colvarDeviceName);

                TableSchema.TableColumn colvarShopCode = new TableSchema.TableColumn(schema);
                colvarShopCode.ColumnName = "shop_code";
                colvarShopCode.DataType = DbType.AnsiString;
                colvarShopCode.MaxLength = 4;
                colvarShopCode.AutoIncrement = false;
                colvarShopCode.IsNullable = true;
                colvarShopCode.IsPrimaryKey = false;
                colvarShopCode.IsForeignKey = false;
                colvarShopCode.IsReadOnly = false;
                colvarShopCode.DefaultSetting = @"";
                colvarShopCode.ForeignKeyTableName = "";
                schema.Columns.Add(colvarShopCode);

                TableSchema.TableColumn colvarFloor = new TableSchema.TableColumn(schema);
                colvarFloor.ColumnName = "floor";
                colvarFloor.DataType = DbType.String;
                colvarFloor.MaxLength = 10;
                colvarFloor.AutoIncrement = false;
                colvarFloor.IsNullable = true;
                colvarFloor.IsPrimaryKey = false;
                colvarFloor.IsForeignKey = false;
                colvarFloor.IsReadOnly = false;
                colvarFloor.DefaultSetting = @"";
                colvarFloor.ForeignKeyTableName = "";
                schema.Columns.Add(colvarFloor);

                TableSchema.TableColumn colvarXtop = new TableSchema.TableColumn(schema);
                colvarXtop.ColumnName = "xtop";
                colvarXtop.DataType = DbType.Int32;
                colvarXtop.MaxLength = 0;
                colvarXtop.AutoIncrement = false;
                colvarXtop.IsNullable = true;
                colvarXtop.IsPrimaryKey = false;
                colvarXtop.IsForeignKey = false;
                colvarXtop.IsReadOnly = false;
                colvarXtop.DefaultSetting = @"";
                colvarXtop.ForeignKeyTableName = "";
                schema.Columns.Add(colvarXtop);

                TableSchema.TableColumn colvarXleft = new TableSchema.TableColumn(schema);
                colvarXleft.ColumnName = "xleft";
                colvarXleft.DataType = DbType.Int32;
                colvarXleft.MaxLength = 0;
                colvarXleft.AutoIncrement = false;
                colvarXleft.IsNullable = true;
                colvarXleft.IsPrimaryKey = false;
                colvarXleft.IsForeignKey = false;
                colvarXleft.IsReadOnly = false;
                colvarXleft.DefaultSetting = @"";
                colvarXleft.ForeignKeyTableName = "";
                schema.Columns.Add(colvarXleft);

                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("action_event_device_info", schema);
            }
        }
        #endregion

        #region Props

        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id
        {
            get { return GetColumnValue<int>(Columns.Id); }
            set { SetColumnValue(Columns.Id, value); }
        }

        [XmlAttribute("ActionEventInfoId")]
        [Bindable(true)]
        public int ActionEventInfoId
        {
            get { return GetColumnValue<int>(Columns.ActionEventInfoId); }
            set { SetColumnValue(Columns.ActionEventInfoId, value); }
        }

        [XmlAttribute("Major")]
        [Bindable(true)]
        public int Major
        {
            get { return GetColumnValue<int>(Columns.Major); }
            set { SetColumnValue(Columns.Major, value); }
        }

        [XmlAttribute("Minor")]
        [Bindable(true)]
        public int Minor
        {
            get { return GetColumnValue<int>(Columns.Minor); }
            set { SetColumnValue(Columns.Minor, value); }
        }

        [XmlAttribute("ElectricMacAddress")]
        [Bindable(true)]
        public string ElectricMacAddress
        {
            get { return GetColumnValue<string>(Columns.ElectricMacAddress); }
            set { SetColumnValue(Columns.ElectricMacAddress, value); }
        }

        [XmlAttribute("ElectricPower")]
        [Bindable(true)]
        public double? ElectricPower
        {
            get { return GetColumnValue<double?>(Columns.ElectricPower); }
            set { SetColumnValue(Columns.ElectricPower, value); }
        }

        [XmlAttribute("LastUpdateTime")]
        [Bindable(true)]
        public DateTime? LastUpdateTime
        {
            get { return GetColumnValue<DateTime?>(Columns.LastUpdateTime); }
            set { SetColumnValue(Columns.LastUpdateTime, value); }
        }

        [XmlAttribute("DeviceName")]
        [Bindable(true)]
        public string DeviceName
        {
            get { return GetColumnValue<string>(Columns.DeviceName); }
            set { SetColumnValue(Columns.DeviceName, value); }
        }

        [XmlAttribute("ShopCode")]
        [Bindable(true)]
        public string ShopCode
        {
            get { return GetColumnValue<string>(Columns.ShopCode); }
            set { SetColumnValue(Columns.ShopCode, value); }
        }

        [XmlAttribute("Floor")]
        [Bindable(true)]
        public string Floor
        {
            get { return GetColumnValue<string>(Columns.Floor); }
            set { SetColumnValue(Columns.Floor, value); }
        }

        [XmlAttribute("Xtop")]
        [Bindable(true)]
        public int? Xtop
        {
            get { return GetColumnValue<int?>(Columns.Xtop); }
            set { SetColumnValue(Columns.Xtop, value); }
        }

        [XmlAttribute("Xleft")]
        [Bindable(true)]
        public int? Xleft
        {
            get { return GetColumnValue<int?>(Columns.Xleft); }
            set { SetColumnValue(Columns.Xleft, value); }
        }

        #endregion




        //no foreign key tables defined (0)



        //no ManyToMany tables defined (0)





        #region Typed Columns


        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }



        public static TableSchema.TableColumn ActionEventInfoIdColumn
        {
            get { return Schema.Columns[1]; }
        }



        public static TableSchema.TableColumn MajorColumn
        {
            get { return Schema.Columns[2]; }
        }



        public static TableSchema.TableColumn MinorColumn
        {
            get { return Schema.Columns[3]; }
        }



        public static TableSchema.TableColumn ElectricMacAddressColumn
        {
            get { return Schema.Columns[4]; }
        }



        public static TableSchema.TableColumn ElectricPowerColumn
        {
            get { return Schema.Columns[5]; }
        }



        public static TableSchema.TableColumn LastUpdateTimeColumn
        {
            get { return Schema.Columns[6]; }
        }



        public static TableSchema.TableColumn DeviceNameColumn
        {
            get { return Schema.Columns[7]; }
        }



        public static TableSchema.TableColumn ShopCodeColumn
        {
            get { return Schema.Columns[8]; }
        }



        public static TableSchema.TableColumn FloorColumn
        {
            get { return Schema.Columns[9]; }
        }



        public static TableSchema.TableColumn XtopColumn
        {
            get { return Schema.Columns[10]; }
        }



        public static TableSchema.TableColumn XleftColumn
        {
            get { return Schema.Columns[11]; }
        }



        #endregion
        #region Columns Struct
        public struct Columns
        {
            public static string Id = @"id";
            public static string ActionEventInfoId = @"action_event_info_id";
            public static string Major = @"major";
            public static string Minor = @"minor";
            public static string ElectricMacAddress = @"electric_mac_address";
            public static string ElectricPower = @"electric_power";
            public static string LastUpdateTime = @"last_update_time";
            public static string DeviceName = @"device_name";
            public static string ShopCode = @"shop_code";
            public static string Floor = @"floor";
            public static string Xtop = @"xtop";
            public static string Xleft = @"xleft";

        }
        #endregion

        #region Update PK Collections

        #endregion

        #region Deep Save

        #endregion
    }
}

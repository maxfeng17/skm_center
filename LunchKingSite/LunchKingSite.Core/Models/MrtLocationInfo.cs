using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
using Microsoft.SqlServer.Types;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the MrtLocationInfo class.
	/// </summary>
    [Serializable]
	public partial class MrtLocationInfoCollection : RepositoryList<MrtLocationInfo, MrtLocationInfoCollection>
	{	   
		public MrtLocationInfoCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>MrtLocationInfoCollection</returns>
		public MrtLocationInfoCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                MrtLocationInfo o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the mrt_location_info table.
	/// </summary>
	[Serializable]
	public partial class MrtLocationInfo : RepositoryRecord<MrtLocationInfo>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public MrtLocationInfo()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public MrtLocationInfo(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("mrt_location_info", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarName = new TableSchema.TableColumn(schema);
				colvarName.ColumnName = "name";
				colvarName.DataType = DbType.String;
				colvarName.MaxLength = 256;
				colvarName.AutoIncrement = false;
				colvarName.IsNullable = false;
				colvarName.IsPrimaryKey = false;
				colvarName.IsForeignKey = false;
				colvarName.IsReadOnly = false;
				colvarName.DefaultSetting = @"";
				colvarName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarName);
				
				TableSchema.TableColumn colvarCode = new TableSchema.TableColumn(schema);
				colvarCode.ColumnName = "code";
				colvarCode.DataType = DbType.Int32;
				colvarCode.MaxLength = 0;
				colvarCode.AutoIncrement = false;
				colvarCode.IsNullable = false;
				colvarCode.IsPrimaryKey = false;
				colvarCode.IsForeignKey = false;
				colvarCode.IsReadOnly = false;
				colvarCode.DefaultSetting = @"";
				colvarCode.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCode);
				
				TableSchema.TableColumn colvarMrtType = new TableSchema.TableColumn(schema);
				colvarMrtType.ColumnName = "mrt_type";
				colvarMrtType.DataType = DbType.Int32;
				colvarMrtType.MaxLength = 0;
				colvarMrtType.AutoIncrement = false;
				colvarMrtType.IsNullable = false;
				colvarMrtType.IsPrimaryKey = false;
				colvarMrtType.IsForeignKey = false;
				colvarMrtType.IsReadOnly = false;
				
						colvarMrtType.DefaultSetting = @"((0))";
				colvarMrtType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMrtType);
				
				TableSchema.TableColumn colvarLineType = new TableSchema.TableColumn(schema);
				colvarLineType.ColumnName = "line_type";
				colvarLineType.DataType = DbType.Int32;
				colvarLineType.MaxLength = 0;
				colvarLineType.AutoIncrement = false;
				colvarLineType.IsNullable = false;
				colvarLineType.IsPrimaryKey = false;
				colvarLineType.IsForeignKey = false;
				colvarLineType.IsReadOnly = false;
				
						colvarLineType.DefaultSetting = @"((0))";
				colvarLineType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarLineType);
				
				TableSchema.TableColumn colvarLatitude = new TableSchema.TableColumn(schema);
				colvarLatitude.ColumnName = "latitude";
				colvarLatitude.DataType = DbType.Decimal;
				colvarLatitude.MaxLength = 0;
				colvarLatitude.AutoIncrement = false;
				colvarLatitude.IsNullable = true;
				colvarLatitude.IsPrimaryKey = false;
				colvarLatitude.IsForeignKey = false;
				colvarLatitude.IsReadOnly = false;
				colvarLatitude.DefaultSetting = @"";
				colvarLatitude.ForeignKeyTableName = "";
				schema.Columns.Add(colvarLatitude);
				
				TableSchema.TableColumn colvarLongitude = new TableSchema.TableColumn(schema);
				colvarLongitude.ColumnName = "longitude";
				colvarLongitude.DataType = DbType.Decimal;
				colvarLongitude.MaxLength = 0;
				colvarLongitude.AutoIncrement = false;
				colvarLongitude.IsNullable = true;
				colvarLongitude.IsPrimaryKey = false;
				colvarLongitude.IsForeignKey = false;
				colvarLongitude.IsReadOnly = false;
				colvarLongitude.DefaultSetting = @"";
				colvarLongitude.ForeignKeyTableName = "";
				schema.Columns.Add(colvarLongitude);
				
				TableSchema.TableColumn colvarCoordinate = new TableSchema.TableColumn(schema);
				colvarCoordinate.ColumnName = "coordinate";
				colvarCoordinate.DataType = DbType.AnsiString;
				colvarCoordinate.MaxLength = -1;
				colvarCoordinate.AutoIncrement = false;
				colvarCoordinate.IsNullable = true;
				colvarCoordinate.IsPrimaryKey = false;
				colvarCoordinate.IsForeignKey = false;
				colvarCoordinate.IsReadOnly = false;
				colvarCoordinate.DefaultSetting = @"";
				colvarCoordinate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCoordinate);
				
				TableSchema.TableColumn colvarModifyId = new TableSchema.TableColumn(schema);
				colvarModifyId.ColumnName = "modify_id";
				colvarModifyId.DataType = DbType.String;
				colvarModifyId.MaxLength = 256;
				colvarModifyId.AutoIncrement = false;
				colvarModifyId.IsNullable = true;
				colvarModifyId.IsPrimaryKey = false;
				colvarModifyId.IsForeignKey = false;
				colvarModifyId.IsReadOnly = false;
				colvarModifyId.DefaultSetting = @"";
				colvarModifyId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyId);
				
				TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
				colvarModifyTime.ColumnName = "modify_time";
				colvarModifyTime.DataType = DbType.DateTime;
				colvarModifyTime.MaxLength = 0;
				colvarModifyTime.AutoIncrement = false;
				colvarModifyTime.IsNullable = true;
				colvarModifyTime.IsPrimaryKey = false;
				colvarModifyTime.IsForeignKey = false;
				colvarModifyTime.IsReadOnly = false;
				colvarModifyTime.DefaultSetting = @"";
				colvarModifyTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyTime);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("mrt_location_info",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("Name")]
		[Bindable(true)]
		public string Name 
		{
			get { return GetColumnValue<string>(Columns.Name); }
			set { SetColumnValue(Columns.Name, value); }
		}
		  
		[XmlAttribute("Code")]
		[Bindable(true)]
		public int Code 
		{
			get { return GetColumnValue<int>(Columns.Code); }
			set { SetColumnValue(Columns.Code, value); }
		}
		  
		[XmlAttribute("MrtType")]
		[Bindable(true)]
		public int MrtType 
		{
			get { return GetColumnValue<int>(Columns.MrtType); }
			set { SetColumnValue(Columns.MrtType, value); }
		}
		  
		[XmlAttribute("LineType")]
		[Bindable(true)]
		public int LineType 
		{
			get { return GetColumnValue<int>(Columns.LineType); }
			set { SetColumnValue(Columns.LineType, value); }
		}
		  
		[XmlAttribute("Latitude")]
		[Bindable(true)]
		public decimal? Latitude 
		{
			get { return GetColumnValue<decimal?>(Columns.Latitude); }
			set { SetColumnValue(Columns.Latitude, value); }
		}
		  
		[XmlAttribute("Longitude")]
		[Bindable(true)]
		public decimal? Longitude 
		{
			get { return GetColumnValue<decimal?>(Columns.Longitude); }
			set { SetColumnValue(Columns.Longitude, value); }
		}
		  
		[XmlAttribute("Coordinate")]
		[Bindable(true)]
		public string Coordinate 
		{
            get { return GetColumnValue<object>(Columns.Coordinate) != null ? GetColumnValue<object>(Columns.Coordinate).ToString() : string.Empty; }
			set { SetColumnValue(Columns.Coordinate, value); }
		}
		  
		[XmlAttribute("ModifyId")]
		[Bindable(true)]
		public string ModifyId 
		{
			get { return GetColumnValue<string>(Columns.ModifyId); }
			set { SetColumnValue(Columns.ModifyId, value); }
		}
		  
		[XmlAttribute("ModifyTime")]
		[Bindable(true)]
		public DateTime? ModifyTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.ModifyTime); }
			set { SetColumnValue(Columns.ModifyTime, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn NameColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn CodeColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn MrtTypeColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn LineTypeColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn LatitudeColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn LongitudeColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn CoordinateColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyIdColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string Name = @"name";
			 public static string Code = @"code";
			 public static string MrtType = @"mrt_type";
			 public static string LineType = @"line_type";
			 public static string Latitude = @"latitude";
			 public static string Longitude = @"longitude";
			 public static string Coordinate = @"coordinate";
			 public static string ModifyId = @"modify_id";
			 public static string ModifyTime = @"modify_time";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

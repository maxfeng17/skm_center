using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the HiDealDeal class.
	/// </summary>
    [Serializable]
	public partial class HiDealDealCollection : RepositoryList<HiDealDeal, HiDealDealCollection>
	{	   
		public HiDealDealCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>HiDealDealCollection</returns>
		public HiDealDealCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                HiDealDeal o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the hi_deal_deal table.
	/// </summary>
	[Serializable]
	public partial class HiDealDeal : RepositoryRecord<HiDealDeal>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public HiDealDeal()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public HiDealDeal(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("hi_deal_deal", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarHiDealGuid = new TableSchema.TableColumn(schema);
				colvarHiDealGuid.ColumnName = "hi_deal_guid";
				colvarHiDealGuid.DataType = DbType.Guid;
				colvarHiDealGuid.MaxLength = 0;
				colvarHiDealGuid.AutoIncrement = false;
				colvarHiDealGuid.IsNullable = false;
				colvarHiDealGuid.IsPrimaryKey = false;
				colvarHiDealGuid.IsForeignKey = false;
				colvarHiDealGuid.IsReadOnly = false;
				colvarHiDealGuid.DefaultSetting = @"";
				colvarHiDealGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarHiDealGuid);
				
				TableSchema.TableColumn colvarSellerGuid = new TableSchema.TableColumn(schema);
				colvarSellerGuid.ColumnName = "seller_guid";
				colvarSellerGuid.DataType = DbType.Guid;
				colvarSellerGuid.MaxLength = 0;
				colvarSellerGuid.AutoIncrement = false;
				colvarSellerGuid.IsNullable = false;
				colvarSellerGuid.IsPrimaryKey = false;
				colvarSellerGuid.IsForeignKey = true;
				colvarSellerGuid.IsReadOnly = false;
				colvarSellerGuid.DefaultSetting = @"";
				
					colvarSellerGuid.ForeignKeyTableName = "seller";
				schema.Columns.Add(colvarSellerGuid);
				
				TableSchema.TableColumn colvarDealType = new TableSchema.TableColumn(schema);
				colvarDealType.ColumnName = "deal_type";
				colvarDealType.DataType = DbType.Int32;
				colvarDealType.MaxLength = 0;
				colvarDealType.AutoIncrement = false;
				colvarDealType.IsNullable = true;
				colvarDealType.IsPrimaryKey = false;
				colvarDealType.IsForeignKey = false;
				colvarDealType.IsReadOnly = false;
				colvarDealType.DefaultSetting = @"";
				colvarDealType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDealType);
				
				TableSchema.TableColumn colvarName = new TableSchema.TableColumn(schema);
				colvarName.ColumnName = "name";
				colvarName.DataType = DbType.String;
				colvarName.MaxLength = 50;
				colvarName.AutoIncrement = false;
				colvarName.IsNullable = true;
				colvarName.IsPrimaryKey = false;
				colvarName.IsForeignKey = false;
				colvarName.IsReadOnly = false;
				colvarName.DefaultSetting = @"";
				colvarName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarName);
				
				TableSchema.TableColumn colvarPromoLongDesc = new TableSchema.TableColumn(schema);
				colvarPromoLongDesc.ColumnName = "promo_long_desc";
				colvarPromoLongDesc.DataType = DbType.String;
				colvarPromoLongDesc.MaxLength = 255;
				colvarPromoLongDesc.AutoIncrement = false;
				colvarPromoLongDesc.IsNullable = true;
				colvarPromoLongDesc.IsPrimaryKey = false;
				colvarPromoLongDesc.IsForeignKey = false;
				colvarPromoLongDesc.IsReadOnly = false;
				colvarPromoLongDesc.DefaultSetting = @"";
				colvarPromoLongDesc.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPromoLongDesc);
				
				TableSchema.TableColumn colvarPromoShortDesc = new TableSchema.TableColumn(schema);
				colvarPromoShortDesc.ColumnName = "promo_short_desc";
				colvarPromoShortDesc.DataType = DbType.String;
				colvarPromoShortDesc.MaxLength = 100;
				colvarPromoShortDesc.AutoIncrement = false;
				colvarPromoShortDesc.IsNullable = true;
				colvarPromoShortDesc.IsPrimaryKey = false;
				colvarPromoShortDesc.IsForeignKey = false;
				colvarPromoShortDesc.IsReadOnly = false;
				colvarPromoShortDesc.DefaultSetting = @"";
				colvarPromoShortDesc.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPromoShortDesc);
				
				TableSchema.TableColumn colvarPicture = new TableSchema.TableColumn(schema);
				colvarPicture.ColumnName = "picture";
				colvarPicture.DataType = DbType.String;
				colvarPicture.MaxLength = -1;
				colvarPicture.AutoIncrement = false;
				colvarPicture.IsNullable = true;
				colvarPicture.IsPrimaryKey = false;
				colvarPicture.IsForeignKey = false;
				colvarPicture.IsReadOnly = false;
				colvarPicture.DefaultSetting = @"";
				colvarPicture.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPicture);
				
				TableSchema.TableColumn colvarDealStartTime = new TableSchema.TableColumn(schema);
				colvarDealStartTime.ColumnName = "deal_start_time";
				colvarDealStartTime.DataType = DbType.DateTime;
				colvarDealStartTime.MaxLength = 0;
				colvarDealStartTime.AutoIncrement = false;
				colvarDealStartTime.IsNullable = true;
				colvarDealStartTime.IsPrimaryKey = false;
				colvarDealStartTime.IsForeignKey = false;
				colvarDealStartTime.IsReadOnly = false;
				colvarDealStartTime.DefaultSetting = @"";
				colvarDealStartTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDealStartTime);
				
				TableSchema.TableColumn colvarDealEndTime = new TableSchema.TableColumn(schema);
				colvarDealEndTime.ColumnName = "deal_end_time";
				colvarDealEndTime.DataType = DbType.DateTime;
				colvarDealEndTime.MaxLength = 0;
				colvarDealEndTime.AutoIncrement = false;
				colvarDealEndTime.IsNullable = true;
				colvarDealEndTime.IsPrimaryKey = false;
				colvarDealEndTime.IsForeignKey = false;
				colvarDealEndTime.IsReadOnly = false;
				colvarDealEndTime.DefaultSetting = @"";
				colvarDealEndTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDealEndTime);
				
				TableSchema.TableColumn colvarIsOpen = new TableSchema.TableColumn(schema);
				colvarIsOpen.ColumnName = "IsOpen";
				colvarIsOpen.DataType = DbType.Boolean;
				colvarIsOpen.MaxLength = 0;
				colvarIsOpen.AutoIncrement = false;
				colvarIsOpen.IsNullable = false;
				colvarIsOpen.IsPrimaryKey = false;
				colvarIsOpen.IsForeignKey = false;
				colvarIsOpen.IsReadOnly = false;
				
						colvarIsOpen.DefaultSetting = @"((1))";
				colvarIsOpen.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsOpen);
				
				TableSchema.TableColumn colvarIsShow = new TableSchema.TableColumn(schema);
				colvarIsShow.ColumnName = "IsShow";
				colvarIsShow.DataType = DbType.Boolean;
				colvarIsShow.MaxLength = 0;
				colvarIsShow.AutoIncrement = false;
				colvarIsShow.IsNullable = false;
				colvarIsShow.IsPrimaryKey = false;
				colvarIsShow.IsForeignKey = false;
				colvarIsShow.IsReadOnly = false;
				
						colvarIsShow.DefaultSetting = @"((0))";
				colvarIsShow.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsShow);
				
				TableSchema.TableColumn colvarIsSoldout = new TableSchema.TableColumn(schema);
				colvarIsSoldout.ColumnName = "is_soldout";
				colvarIsSoldout.DataType = DbType.Boolean;
				colvarIsSoldout.MaxLength = 0;
				colvarIsSoldout.AutoIncrement = false;
				colvarIsSoldout.IsNullable = true;
				colvarIsSoldout.IsPrimaryKey = false;
				colvarIsSoldout.IsForeignKey = false;
				colvarIsSoldout.IsReadOnly = false;
				colvarIsSoldout.DefaultSetting = @"";
				colvarIsSoldout.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsSoldout);
				
				TableSchema.TableColumn colvarIsAlwaysMain = new TableSchema.TableColumn(schema);
				colvarIsAlwaysMain.ColumnName = "is_always_main";
				colvarIsAlwaysMain.DataType = DbType.Boolean;
				colvarIsAlwaysMain.MaxLength = 0;
				colvarIsAlwaysMain.AutoIncrement = false;
				colvarIsAlwaysMain.IsNullable = false;
				colvarIsAlwaysMain.IsPrimaryKey = false;
				colvarIsAlwaysMain.IsForeignKey = false;
				colvarIsAlwaysMain.IsReadOnly = false;
				
						colvarIsAlwaysMain.DefaultSetting = @"((0))";
				colvarIsAlwaysMain.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsAlwaysMain);
				
				TableSchema.TableColumn colvarIsVerifyByList = new TableSchema.TableColumn(schema);
				colvarIsVerifyByList.ColumnName = "is_verify_by_list";
				colvarIsVerifyByList.DataType = DbType.Boolean;
				colvarIsVerifyByList.MaxLength = 0;
				colvarIsVerifyByList.AutoIncrement = false;
				colvarIsVerifyByList.IsNullable = false;
				colvarIsVerifyByList.IsPrimaryKey = false;
				colvarIsVerifyByList.IsForeignKey = false;
				colvarIsVerifyByList.IsReadOnly = false;
				
						colvarIsVerifyByList.DefaultSetting = @"((1))";
				colvarIsVerifyByList.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsVerifyByList);
				
				TableSchema.TableColumn colvarIsVerifyByPad = new TableSchema.TableColumn(schema);
				colvarIsVerifyByPad.ColumnName = "is_verify_by_pad";
				colvarIsVerifyByPad.DataType = DbType.Boolean;
				colvarIsVerifyByPad.MaxLength = 0;
				colvarIsVerifyByPad.AutoIncrement = false;
				colvarIsVerifyByPad.IsNullable = false;
				colvarIsVerifyByPad.IsPrimaryKey = false;
				colvarIsVerifyByPad.IsForeignKey = false;
				colvarIsVerifyByPad.IsReadOnly = false;
				
						colvarIsVerifyByPad.DefaultSetting = @"((0))";
				colvarIsVerifyByPad.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsVerifyByPad);
				
				TableSchema.TableColumn colvarSales = new TableSchema.TableColumn(schema);
				colvarSales.ColumnName = "sales";
				colvarSales.DataType = DbType.String;
				colvarSales.MaxLength = 100;
				colvarSales.AutoIncrement = false;
				colvarSales.IsNullable = true;
				colvarSales.IsPrimaryKey = false;
				colvarSales.IsForeignKey = false;
				colvarSales.IsReadOnly = false;
				colvarSales.DefaultSetting = @"";
				colvarSales.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSales);
				
				TableSchema.TableColumn colvarSalesGroupId = new TableSchema.TableColumn(schema);
				colvarSalesGroupId.ColumnName = "sales_group_id";
				colvarSalesGroupId.DataType = DbType.Int32;
				colvarSalesGroupId.MaxLength = 0;
				colvarSalesGroupId.AutoIncrement = false;
				colvarSalesGroupId.IsNullable = true;
				colvarSalesGroupId.IsPrimaryKey = false;
				colvarSalesGroupId.IsForeignKey = true;
				colvarSalesGroupId.IsReadOnly = false;
				colvarSalesGroupId.DefaultSetting = @"";
				
					colvarSalesGroupId.ForeignKeyTableName = "acc_business_group";
				schema.Columns.Add(colvarSalesGroupId);
				
				TableSchema.TableColumn colvarSalesGroupName = new TableSchema.TableColumn(schema);
				colvarSalesGroupName.ColumnName = "sales_group_name";
				colvarSalesGroupName.DataType = DbType.String;
				colvarSalesGroupName.MaxLength = 255;
				colvarSalesGroupName.AutoIncrement = false;
				colvarSalesGroupName.IsNullable = true;
				colvarSalesGroupName.IsPrimaryKey = false;
				colvarSalesGroupName.IsForeignKey = false;
				colvarSalesGroupName.IsReadOnly = false;
				colvarSalesGroupName.DefaultSetting = @"";
				colvarSalesGroupName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSalesGroupName);
				
				TableSchema.TableColumn colvarSalesDeptId = new TableSchema.TableColumn(schema);
				colvarSalesDeptId.ColumnName = "sales_dept_id";
				colvarSalesDeptId.DataType = DbType.AnsiString;
				colvarSalesDeptId.MaxLength = 10;
				colvarSalesDeptId.AutoIncrement = false;
				colvarSalesDeptId.IsNullable = true;
				colvarSalesDeptId.IsPrimaryKey = false;
				colvarSalesDeptId.IsForeignKey = true;
				colvarSalesDeptId.IsReadOnly = false;
				colvarSalesDeptId.DefaultSetting = @"";
				
					colvarSalesDeptId.ForeignKeyTableName = "department";
				schema.Columns.Add(colvarSalesDeptId);
				
				TableSchema.TableColumn colvarSalesDept = new TableSchema.TableColumn(schema);
				colvarSalesDept.ColumnName = "sales_dept";
				colvarSalesDept.DataType = DbType.String;
				colvarSalesDept.MaxLength = 100;
				colvarSalesDept.AutoIncrement = false;
				colvarSalesDept.IsNullable = true;
				colvarSalesDept.IsPrimaryKey = false;
				colvarSalesDept.IsForeignKey = false;
				colvarSalesDept.IsReadOnly = false;
				colvarSalesDept.DefaultSetting = @"";
				colvarSalesDept.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSalesDept);
				
				TableSchema.TableColumn colvarSalesLocationId = new TableSchema.TableColumn(schema);
				colvarSalesLocationId.ColumnName = "sales_location_id";
				colvarSalesLocationId.DataType = DbType.Int32;
				colvarSalesLocationId.MaxLength = 0;
				colvarSalesLocationId.AutoIncrement = false;
				colvarSalesLocationId.IsNullable = true;
				colvarSalesLocationId.IsPrimaryKey = false;
				colvarSalesLocationId.IsForeignKey = true;
				colvarSalesLocationId.IsReadOnly = false;
				colvarSalesLocationId.DefaultSetting = @"";
				
					colvarSalesLocationId.ForeignKeyTableName = "city";
				schema.Columns.Add(colvarSalesLocationId);
				
				TableSchema.TableColumn colvarSalesLocation = new TableSchema.TableColumn(schema);
				colvarSalesLocation.ColumnName = "sales_location";
				colvarSalesLocation.DataType = DbType.String;
				colvarSalesLocation.MaxLength = 25;
				colvarSalesLocation.AutoIncrement = false;
				colvarSalesLocation.IsNullable = true;
				colvarSalesLocation.IsPrimaryKey = false;
				colvarSalesLocation.IsForeignKey = false;
				colvarSalesLocation.IsReadOnly = false;
				colvarSalesLocation.DefaultSetting = @"";
				colvarSalesLocation.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSalesLocation);
				
				TableSchema.TableColumn colvarSalesCatgCodeGroup = new TableSchema.TableColumn(schema);
				colvarSalesCatgCodeGroup.ColumnName = "sales_catg_code_group";
				colvarSalesCatgCodeGroup.DataType = DbType.AnsiString;
				colvarSalesCatgCodeGroup.MaxLength = 50;
				colvarSalesCatgCodeGroup.AutoIncrement = false;
				colvarSalesCatgCodeGroup.IsNullable = true;
				colvarSalesCatgCodeGroup.IsPrimaryKey = false;
				colvarSalesCatgCodeGroup.IsForeignKey = true;
				colvarSalesCatgCodeGroup.IsReadOnly = false;
				colvarSalesCatgCodeGroup.DefaultSetting = @"";
				
					colvarSalesCatgCodeGroup.ForeignKeyTableName = "system_code";
				schema.Columns.Add(colvarSalesCatgCodeGroup);
				
				TableSchema.TableColumn colvarSalesCatgCodeId = new TableSchema.TableColumn(schema);
				colvarSalesCatgCodeId.ColumnName = "sales_catg_code_id";
				colvarSalesCatgCodeId.DataType = DbType.Int32;
				colvarSalesCatgCodeId.MaxLength = 0;
				colvarSalesCatgCodeId.AutoIncrement = false;
				colvarSalesCatgCodeId.IsNullable = true;
				colvarSalesCatgCodeId.IsPrimaryKey = false;
				colvarSalesCatgCodeId.IsForeignKey = true;
				colvarSalesCatgCodeId.IsReadOnly = false;
				colvarSalesCatgCodeId.DefaultSetting = @"";
				
					colvarSalesCatgCodeId.ForeignKeyTableName = "system_code";
				schema.Columns.Add(colvarSalesCatgCodeId);
				
				TableSchema.TableColumn colvarSalesCatgCodeName = new TableSchema.TableColumn(schema);
				colvarSalesCatgCodeName.ColumnName = "sales_catg_code_name";
				colvarSalesCatgCodeName.DataType = DbType.String;
				colvarSalesCatgCodeName.MaxLength = 50;
				colvarSalesCatgCodeName.AutoIncrement = false;
				colvarSalesCatgCodeName.IsNullable = true;
				colvarSalesCatgCodeName.IsPrimaryKey = false;
				colvarSalesCatgCodeName.IsForeignKey = false;
				colvarSalesCatgCodeName.IsReadOnly = false;
				colvarSalesCatgCodeName.DefaultSetting = @"";
				colvarSalesCatgCodeName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSalesCatgCodeName);
				
				TableSchema.TableColumn colvarSettleDay1 = new TableSchema.TableColumn(schema);
				colvarSettleDay1.ColumnName = "settle_day_1";
				colvarSettleDay1.DataType = DbType.Int32;
				colvarSettleDay1.MaxLength = 0;
				colvarSettleDay1.AutoIncrement = false;
				colvarSettleDay1.IsNullable = true;
				colvarSettleDay1.IsPrimaryKey = false;
				colvarSettleDay1.IsForeignKey = false;
				colvarSettleDay1.IsReadOnly = false;
				colvarSettleDay1.DefaultSetting = @"";
				colvarSettleDay1.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSettleDay1);
				
				TableSchema.TableColumn colvarSettleDay2 = new TableSchema.TableColumn(schema);
				colvarSettleDay2.ColumnName = "settle_day_2";
				colvarSettleDay2.DataType = DbType.Int32;
				colvarSettleDay2.MaxLength = 0;
				colvarSettleDay2.AutoIncrement = false;
				colvarSettleDay2.IsNullable = true;
				colvarSettleDay2.IsPrimaryKey = false;
				colvarSettleDay2.IsForeignKey = false;
				colvarSettleDay2.IsReadOnly = false;
				colvarSettleDay2.DefaultSetting = @"";
				colvarSettleDay2.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSettleDay2);
				
				TableSchema.TableColumn colvarSettleDay3 = new TableSchema.TableColumn(schema);
				colvarSettleDay3.ColumnName = "settle_day_3";
				colvarSettleDay3.DataType = DbType.Int32;
				colvarSettleDay3.MaxLength = 0;
				colvarSettleDay3.AutoIncrement = false;
				colvarSettleDay3.IsNullable = true;
				colvarSettleDay3.IsPrimaryKey = false;
				colvarSettleDay3.IsForeignKey = false;
				colvarSettleDay3.IsReadOnly = false;
				colvarSettleDay3.DefaultSetting = @"";
				colvarSettleDay3.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSettleDay3);
				
				TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
				colvarCreateId.ColumnName = "create_id";
				colvarCreateId.DataType = DbType.String;
				colvarCreateId.MaxLength = 100;
				colvarCreateId.AutoIncrement = false;
				colvarCreateId.IsNullable = true;
				colvarCreateId.IsPrimaryKey = false;
				colvarCreateId.IsForeignKey = false;
				colvarCreateId.IsReadOnly = false;
				colvarCreateId.DefaultSetting = @"";
				colvarCreateId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateId);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = true;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarModifyId = new TableSchema.TableColumn(schema);
				colvarModifyId.ColumnName = "modify_id";
				colvarModifyId.DataType = DbType.String;
				colvarModifyId.MaxLength = 100;
				colvarModifyId.AutoIncrement = false;
				colvarModifyId.IsNullable = true;
				colvarModifyId.IsPrimaryKey = false;
				colvarModifyId.IsForeignKey = false;
				colvarModifyId.IsReadOnly = false;
				colvarModifyId.DefaultSetting = @"";
				colvarModifyId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyId);
				
				TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
				colvarModifyTime.ColumnName = "modify_time";
				colvarModifyTime.DataType = DbType.DateTime;
				colvarModifyTime.MaxLength = 0;
				colvarModifyTime.AutoIncrement = false;
				colvarModifyTime.IsNullable = true;
				colvarModifyTime.IsPrimaryKey = false;
				colvarModifyTime.IsForeignKey = false;
				colvarModifyTime.IsReadOnly = false;
				colvarModifyTime.DefaultSetting = @"";
				colvarModifyTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyTime);
				
				TableSchema.TableColumn colvarSeq = new TableSchema.TableColumn(schema);
				colvarSeq.ColumnName = "seq";
				colvarSeq.DataType = DbType.Int32;
				colvarSeq.MaxLength = 0;
				colvarSeq.AutoIncrement = false;
				colvarSeq.IsNullable = true;
				colvarSeq.IsPrimaryKey = false;
				colvarSeq.IsForeignKey = false;
				colvarSeq.IsReadOnly = false;
				colvarSeq.DefaultSetting = @"";
				colvarSeq.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSeq);
				
				TableSchema.TableColumn colvarCities = new TableSchema.TableColumn(schema);
				colvarCities.ColumnName = "cities";
				colvarCities.DataType = DbType.String;
				colvarCities.MaxLength = 100;
				colvarCities.AutoIncrement = false;
				colvarCities.IsNullable = true;
				colvarCities.IsPrimaryKey = false;
				colvarCities.IsForeignKey = false;
				colvarCities.IsReadOnly = false;
				colvarCities.DefaultSetting = @"";
				colvarCities.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCities);
				
				TableSchema.TableColumn colvarIsVisa = new TableSchema.TableColumn(schema);
				colvarIsVisa.ColumnName = "is_visa";
				colvarIsVisa.DataType = DbType.Boolean;
				colvarIsVisa.MaxLength = 0;
				colvarIsVisa.AutoIncrement = false;
				colvarIsVisa.IsNullable = true;
				colvarIsVisa.IsPrimaryKey = false;
				colvarIsVisa.IsForeignKey = false;
				colvarIsVisa.IsReadOnly = false;
				
						colvarIsVisa.DefaultSetting = @"((0))";
				colvarIsVisa.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsVisa);
				
				TableSchema.TableColumn colvarEdmTitle = new TableSchema.TableColumn(schema);
				colvarEdmTitle.ColumnName = "edm_title";
				colvarEdmTitle.DataType = DbType.String;
				colvarEdmTitle.MaxLength = 40;
				colvarEdmTitle.AutoIncrement = false;
				colvarEdmTitle.IsNullable = true;
				colvarEdmTitle.IsPrimaryKey = false;
				colvarEdmTitle.IsForeignKey = false;
				colvarEdmTitle.IsReadOnly = false;
				colvarEdmTitle.DefaultSetting = @"";
				colvarEdmTitle.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEdmTitle);
				
				TableSchema.TableColumn colvarEdmSubject = new TableSchema.TableColumn(schema);
				colvarEdmSubject.ColumnName = "edm_subject";
				colvarEdmSubject.DataType = DbType.String;
				colvarEdmSubject.MaxLength = 40;
				colvarEdmSubject.AutoIncrement = false;
				colvarEdmSubject.IsNullable = true;
				colvarEdmSubject.IsPrimaryKey = false;
				colvarEdmSubject.IsForeignKey = false;
				colvarEdmSubject.IsReadOnly = false;
				colvarEdmSubject.DefaultSetting = @"";
				colvarEdmSubject.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEdmSubject);
				
				TableSchema.TableColumn colvarPrimaryBigPicture = new TableSchema.TableColumn(schema);
				colvarPrimaryBigPicture.ColumnName = "primary_big_picture";
				colvarPrimaryBigPicture.DataType = DbType.String;
				colvarPrimaryBigPicture.MaxLength = 200;
				colvarPrimaryBigPicture.AutoIncrement = false;
				colvarPrimaryBigPicture.IsNullable = true;
				colvarPrimaryBigPicture.IsPrimaryKey = false;
				colvarPrimaryBigPicture.IsForeignKey = false;
				colvarPrimaryBigPicture.IsReadOnly = false;
				colvarPrimaryBigPicture.DefaultSetting = @"";
				colvarPrimaryBigPicture.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPrimaryBigPicture);
				
				TableSchema.TableColumn colvarPrimarySmallPicture = new TableSchema.TableColumn(schema);
				colvarPrimarySmallPicture.ColumnName = "primary_small_picture";
				colvarPrimarySmallPicture.DataType = DbType.String;
				colvarPrimarySmallPicture.MaxLength = 200;
				colvarPrimarySmallPicture.AutoIncrement = false;
				colvarPrimarySmallPicture.IsNullable = true;
				colvarPrimarySmallPicture.IsPrimaryKey = false;
				colvarPrimarySmallPicture.IsForeignKey = false;
				colvarPrimarySmallPicture.IsReadOnly = false;
				colvarPrimarySmallPicture.DefaultSetting = @"";
				colvarPrimarySmallPicture.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPrimarySmallPicture);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("hi_deal_deal",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("HiDealGuid")]
		[Bindable(true)]
		public Guid HiDealGuid 
		{
			get { return GetColumnValue<Guid>(Columns.HiDealGuid); }
			set { SetColumnValue(Columns.HiDealGuid, value); }
		}
		  
		[XmlAttribute("SellerGuid")]
		[Bindable(true)]
		public Guid SellerGuid 
		{
			get { return GetColumnValue<Guid>(Columns.SellerGuid); }
			set { SetColumnValue(Columns.SellerGuid, value); }
		}
		  
		[XmlAttribute("DealType")]
		[Bindable(true)]
		public int? DealType 
		{
			get { return GetColumnValue<int?>(Columns.DealType); }
			set { SetColumnValue(Columns.DealType, value); }
		}
		  
		[XmlAttribute("Name")]
		[Bindable(true)]
		public string Name 
		{
			get { return GetColumnValue<string>(Columns.Name); }
			set { SetColumnValue(Columns.Name, value); }
		}
		  
		[XmlAttribute("PromoLongDesc")]
		[Bindable(true)]
		public string PromoLongDesc 
		{
			get { return GetColumnValue<string>(Columns.PromoLongDesc); }
			set { SetColumnValue(Columns.PromoLongDesc, value); }
		}
		  
		[XmlAttribute("PromoShortDesc")]
		[Bindable(true)]
		public string PromoShortDesc 
		{
			get { return GetColumnValue<string>(Columns.PromoShortDesc); }
			set { SetColumnValue(Columns.PromoShortDesc, value); }
		}
		  
		[XmlAttribute("Picture")]
		[Bindable(true)]
		public string Picture 
		{
			get { return GetColumnValue<string>(Columns.Picture); }
			set { SetColumnValue(Columns.Picture, value); }
		}
		  
		[XmlAttribute("DealStartTime")]
		[Bindable(true)]
		public DateTime? DealStartTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.DealStartTime); }
			set { SetColumnValue(Columns.DealStartTime, value); }
		}
		  
		[XmlAttribute("DealEndTime")]
		[Bindable(true)]
		public DateTime? DealEndTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.DealEndTime); }
			set { SetColumnValue(Columns.DealEndTime, value); }
		}
		  
		[XmlAttribute("IsOpen")]
		[Bindable(true)]
		public bool IsOpen 
		{
			get { return GetColumnValue<bool>(Columns.IsOpen); }
			set { SetColumnValue(Columns.IsOpen, value); }
		}
		  
		[XmlAttribute("IsShow")]
		[Bindable(true)]
		public bool IsShow 
		{
			get { return GetColumnValue<bool>(Columns.IsShow); }
			set { SetColumnValue(Columns.IsShow, value); }
		}
		  
		[XmlAttribute("IsSoldout")]
		[Bindable(true)]
		public bool? IsSoldout 
		{
			get { return GetColumnValue<bool?>(Columns.IsSoldout); }
			set { SetColumnValue(Columns.IsSoldout, value); }
		}
		  
		[XmlAttribute("IsAlwaysMain")]
		[Bindable(true)]
		public bool IsAlwaysMain 
		{
			get { return GetColumnValue<bool>(Columns.IsAlwaysMain); }
			set { SetColumnValue(Columns.IsAlwaysMain, value); }
		}
		  
		[XmlAttribute("IsVerifyByList")]
		[Bindable(true)]
		public bool IsVerifyByList 
		{
			get { return GetColumnValue<bool>(Columns.IsVerifyByList); }
			set { SetColumnValue(Columns.IsVerifyByList, value); }
		}
		  
		[XmlAttribute("IsVerifyByPad")]
		[Bindable(true)]
		public bool IsVerifyByPad 
		{
			get { return GetColumnValue<bool>(Columns.IsVerifyByPad); }
			set { SetColumnValue(Columns.IsVerifyByPad, value); }
		}
		  
		[XmlAttribute("Sales")]
		[Bindable(true)]
		public string Sales 
		{
			get { return GetColumnValue<string>(Columns.Sales); }
			set { SetColumnValue(Columns.Sales, value); }
		}
		  
		[XmlAttribute("SalesGroupId")]
		[Bindable(true)]
		public int? SalesGroupId 
		{
			get { return GetColumnValue<int?>(Columns.SalesGroupId); }
			set { SetColumnValue(Columns.SalesGroupId, value); }
		}
		  
		[XmlAttribute("SalesGroupName")]
		[Bindable(true)]
		public string SalesGroupName 
		{
			get { return GetColumnValue<string>(Columns.SalesGroupName); }
			set { SetColumnValue(Columns.SalesGroupName, value); }
		}
		  
		[XmlAttribute("SalesDeptId")]
		[Bindable(true)]
		public string SalesDeptId 
		{
			get { return GetColumnValue<string>(Columns.SalesDeptId); }
			set { SetColumnValue(Columns.SalesDeptId, value); }
		}
		  
		[XmlAttribute("SalesDept")]
		[Bindable(true)]
		public string SalesDept 
		{
			get { return GetColumnValue<string>(Columns.SalesDept); }
			set { SetColumnValue(Columns.SalesDept, value); }
		}
		  
		[XmlAttribute("SalesLocationId")]
		[Bindable(true)]
		public int? SalesLocationId 
		{
			get { return GetColumnValue<int?>(Columns.SalesLocationId); }
			set { SetColumnValue(Columns.SalesLocationId, value); }
		}
		  
		[XmlAttribute("SalesLocation")]
		[Bindable(true)]
		public string SalesLocation 
		{
			get { return GetColumnValue<string>(Columns.SalesLocation); }
			set { SetColumnValue(Columns.SalesLocation, value); }
		}
		  
		[XmlAttribute("SalesCatgCodeGroup")]
		[Bindable(true)]
		public string SalesCatgCodeGroup 
		{
			get { return GetColumnValue<string>(Columns.SalesCatgCodeGroup); }
			set { SetColumnValue(Columns.SalesCatgCodeGroup, value); }
		}
		  
		[XmlAttribute("SalesCatgCodeId")]
		[Bindable(true)]
		public int? SalesCatgCodeId 
		{
			get { return GetColumnValue<int?>(Columns.SalesCatgCodeId); }
			set { SetColumnValue(Columns.SalesCatgCodeId, value); }
		}
		  
		[XmlAttribute("SalesCatgCodeName")]
		[Bindable(true)]
		public string SalesCatgCodeName 
		{
			get { return GetColumnValue<string>(Columns.SalesCatgCodeName); }
			set { SetColumnValue(Columns.SalesCatgCodeName, value); }
		}
		  
		[XmlAttribute("SettleDay1")]
		[Bindable(true)]
		public int? SettleDay1 
		{
			get { return GetColumnValue<int?>(Columns.SettleDay1); }
			set { SetColumnValue(Columns.SettleDay1, value); }
		}
		  
		[XmlAttribute("SettleDay2")]
		[Bindable(true)]
		public int? SettleDay2 
		{
			get { return GetColumnValue<int?>(Columns.SettleDay2); }
			set { SetColumnValue(Columns.SettleDay2, value); }
		}
		  
		[XmlAttribute("SettleDay3")]
		[Bindable(true)]
		public int? SettleDay3 
		{
			get { return GetColumnValue<int?>(Columns.SettleDay3); }
			set { SetColumnValue(Columns.SettleDay3, value); }
		}
		  
		[XmlAttribute("CreateId")]
		[Bindable(true)]
		public string CreateId 
		{
			get { return GetColumnValue<string>(Columns.CreateId); }
			set { SetColumnValue(Columns.CreateId, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime? CreateTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		  
		[XmlAttribute("ModifyId")]
		[Bindable(true)]
		public string ModifyId 
		{
			get { return GetColumnValue<string>(Columns.ModifyId); }
			set { SetColumnValue(Columns.ModifyId, value); }
		}
		  
		[XmlAttribute("ModifyTime")]
		[Bindable(true)]
		public DateTime? ModifyTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.ModifyTime); }
			set { SetColumnValue(Columns.ModifyTime, value); }
		}
		  
		[XmlAttribute("Seq")]
		[Bindable(true)]
		public int? Seq 
		{
			get { return GetColumnValue<int?>(Columns.Seq); }
			set { SetColumnValue(Columns.Seq, value); }
		}
		  
		[XmlAttribute("Cities")]
		[Bindable(true)]
		public string Cities 
		{
			get { return GetColumnValue<string>(Columns.Cities); }
			set { SetColumnValue(Columns.Cities, value); }
		}
		  
		[XmlAttribute("IsVisa")]
		[Bindable(true)]
		public bool? IsVisa 
		{
			get { return GetColumnValue<bool?>(Columns.IsVisa); }
			set { SetColumnValue(Columns.IsVisa, value); }
		}
		  
		[XmlAttribute("EdmTitle")]
		[Bindable(true)]
		public string EdmTitle 
		{
			get { return GetColumnValue<string>(Columns.EdmTitle); }
			set { SetColumnValue(Columns.EdmTitle, value); }
		}
		  
		[XmlAttribute("EdmSubject")]
		[Bindable(true)]
		public string EdmSubject 
		{
			get { return GetColumnValue<string>(Columns.EdmSubject); }
			set { SetColumnValue(Columns.EdmSubject, value); }
		}
		  
		[XmlAttribute("PrimaryBigPicture")]
		[Bindable(true)]
		public string PrimaryBigPicture 
		{
			get { return GetColumnValue<string>(Columns.PrimaryBigPicture); }
			set { SetColumnValue(Columns.PrimaryBigPicture, value); }
		}
		  
		[XmlAttribute("PrimarySmallPicture")]
		[Bindable(true)]
		public string PrimarySmallPicture 
		{
			get { return GetColumnValue<string>(Columns.PrimarySmallPicture); }
			set { SetColumnValue(Columns.PrimarySmallPicture, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (5)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn HiDealGuidColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn SellerGuidColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn DealTypeColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn NameColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn PromoLongDescColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn PromoShortDescColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn PictureColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn DealStartTimeColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn DealEndTimeColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn IsOpenColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        public static TableSchema.TableColumn IsShowColumn
        {
            get { return Schema.Columns[11]; }
        }
        
        
        
        public static TableSchema.TableColumn IsSoldoutColumn
        {
            get { return Schema.Columns[12]; }
        }
        
        
        
        public static TableSchema.TableColumn IsAlwaysMainColumn
        {
            get { return Schema.Columns[13]; }
        }
        
        
        
        public static TableSchema.TableColumn IsVerifyByListColumn
        {
            get { return Schema.Columns[14]; }
        }
        
        
        
        public static TableSchema.TableColumn IsVerifyByPadColumn
        {
            get { return Schema.Columns[15]; }
        }
        
        
        
        public static TableSchema.TableColumn SalesColumn
        {
            get { return Schema.Columns[16]; }
        }
        
        
        
        public static TableSchema.TableColumn SalesGroupIdColumn
        {
            get { return Schema.Columns[17]; }
        }
        
        
        
        public static TableSchema.TableColumn SalesGroupNameColumn
        {
            get { return Schema.Columns[18]; }
        }
        
        
        
        public static TableSchema.TableColumn SalesDeptIdColumn
        {
            get { return Schema.Columns[19]; }
        }
        
        
        
        public static TableSchema.TableColumn SalesDeptColumn
        {
            get { return Schema.Columns[20]; }
        }
        
        
        
        public static TableSchema.TableColumn SalesLocationIdColumn
        {
            get { return Schema.Columns[21]; }
        }
        
        
        
        public static TableSchema.TableColumn SalesLocationColumn
        {
            get { return Schema.Columns[22]; }
        }
        
        
        
        public static TableSchema.TableColumn SalesCatgCodeGroupColumn
        {
            get { return Schema.Columns[23]; }
        }
        
        
        
        public static TableSchema.TableColumn SalesCatgCodeIdColumn
        {
            get { return Schema.Columns[24]; }
        }
        
        
        
        public static TableSchema.TableColumn SalesCatgCodeNameColumn
        {
            get { return Schema.Columns[25]; }
        }
        
        
        
        public static TableSchema.TableColumn SettleDay1Column
        {
            get { return Schema.Columns[26]; }
        }
        
        
        
        public static TableSchema.TableColumn SettleDay2Column
        {
            get { return Schema.Columns[27]; }
        }
        
        
        
        public static TableSchema.TableColumn SettleDay3Column
        {
            get { return Schema.Columns[28]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[29]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[30]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyIdColumn
        {
            get { return Schema.Columns[31]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[32]; }
        }
        
        
        
        public static TableSchema.TableColumn SeqColumn
        {
            get { return Schema.Columns[33]; }
        }
        
        
        
        public static TableSchema.TableColumn CitiesColumn
        {
            get { return Schema.Columns[34]; }
        }
        
        
        
        public static TableSchema.TableColumn IsVisaColumn
        {
            get { return Schema.Columns[35]; }
        }
        
        
        
        public static TableSchema.TableColumn EdmTitleColumn
        {
            get { return Schema.Columns[36]; }
        }
        
        
        
        public static TableSchema.TableColumn EdmSubjectColumn
        {
            get { return Schema.Columns[37]; }
        }
        
        
        
        public static TableSchema.TableColumn PrimaryBigPictureColumn
        {
            get { return Schema.Columns[38]; }
        }
        
        
        
        public static TableSchema.TableColumn PrimarySmallPictureColumn
        {
            get { return Schema.Columns[39]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string HiDealGuid = @"hi_deal_guid";
			 public static string SellerGuid = @"seller_guid";
			 public static string DealType = @"deal_type";
			 public static string Name = @"name";
			 public static string PromoLongDesc = @"promo_long_desc";
			 public static string PromoShortDesc = @"promo_short_desc";
			 public static string Picture = @"picture";
			 public static string DealStartTime = @"deal_start_time";
			 public static string DealEndTime = @"deal_end_time";
			 public static string IsOpen = @"IsOpen";
			 public static string IsShow = @"IsShow";
			 public static string IsSoldout = @"is_soldout";
			 public static string IsAlwaysMain = @"is_always_main";
			 public static string IsVerifyByList = @"is_verify_by_list";
			 public static string IsVerifyByPad = @"is_verify_by_pad";
			 public static string Sales = @"sales";
			 public static string SalesGroupId = @"sales_group_id";
			 public static string SalesGroupName = @"sales_group_name";
			 public static string SalesDeptId = @"sales_dept_id";
			 public static string SalesDept = @"sales_dept";
			 public static string SalesLocationId = @"sales_location_id";
			 public static string SalesLocation = @"sales_location";
			 public static string SalesCatgCodeGroup = @"sales_catg_code_group";
			 public static string SalesCatgCodeId = @"sales_catg_code_id";
			 public static string SalesCatgCodeName = @"sales_catg_code_name";
			 public static string SettleDay1 = @"settle_day_1";
			 public static string SettleDay2 = @"settle_day_2";
			 public static string SettleDay3 = @"settle_day_3";
			 public static string CreateId = @"create_id";
			 public static string CreateTime = @"create_time";
			 public static string ModifyId = @"modify_id";
			 public static string ModifyTime = @"modify_time";
			 public static string Seq = @"seq";
			 public static string Cities = @"cities";
			 public static string IsVisa = @"is_visa";
			 public static string EdmTitle = @"edm_title";
			 public static string EdmSubject = @"edm_subject";
			 public static string PrimaryBigPicture = @"primary_big_picture";
			 public static string PrimarySmallPicture = @"primary_small_picture";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the PcpOrderDetail class.
	/// </summary>
    [Serializable]
	public partial class PcpOrderDetailCollection : RepositoryList<PcpOrderDetail, PcpOrderDetailCollection>
	{	   
		public PcpOrderDetailCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>PcpOrderDetailCollection</returns>
		public PcpOrderDetailCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                PcpOrderDetail o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the pcp_order_detail table.
	/// </summary>
	[Serializable]
	public partial class PcpOrderDetail : RepositoryRecord<PcpOrderDetail>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public PcpOrderDetail()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public PcpOrderDetail(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("pcp_order_detail", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarPcpOrderGuid = new TableSchema.TableColumn(schema);
				colvarPcpOrderGuid.ColumnName = "pcp_order_guid";
				colvarPcpOrderGuid.DataType = DbType.Guid;
				colvarPcpOrderGuid.MaxLength = 0;
				colvarPcpOrderGuid.AutoIncrement = false;
				colvarPcpOrderGuid.IsNullable = false;
				colvarPcpOrderGuid.IsPrimaryKey = false;
				colvarPcpOrderGuid.IsForeignKey = false;
				colvarPcpOrderGuid.IsReadOnly = false;
				colvarPcpOrderGuid.DefaultSetting = @"";
				colvarPcpOrderGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPcpOrderGuid);
				
				TableSchema.TableColumn colvarPcpOrderPk = new TableSchema.TableColumn(schema);
				colvarPcpOrderPk.ColumnName = "pcp_order_pk";
				colvarPcpOrderPk.DataType = DbType.Int32;
				colvarPcpOrderPk.MaxLength = 0;
				colvarPcpOrderPk.AutoIncrement = false;
				colvarPcpOrderPk.IsNullable = false;
				colvarPcpOrderPk.IsPrimaryKey = false;
				colvarPcpOrderPk.IsForeignKey = false;
				colvarPcpOrderPk.IsReadOnly = false;
				colvarPcpOrderPk.DefaultSetting = @"";
				colvarPcpOrderPk.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPcpOrderPk);
				
				TableSchema.TableColumn colvarRelatedStoreCode = new TableSchema.TableColumn(schema);
				colvarRelatedStoreCode.ColumnName = "related_store_code";
				colvarRelatedStoreCode.DataType = DbType.String;
				colvarRelatedStoreCode.MaxLength = 10;
				colvarRelatedStoreCode.AutoIncrement = false;
				colvarRelatedStoreCode.IsNullable = false;
				colvarRelatedStoreCode.IsPrimaryKey = false;
				colvarRelatedStoreCode.IsForeignKey = false;
				colvarRelatedStoreCode.IsReadOnly = false;
				colvarRelatedStoreCode.DefaultSetting = @"";
				colvarRelatedStoreCode.ForeignKeyTableName = "";
				schema.Columns.Add(colvarRelatedStoreCode);
				
				TableSchema.TableColumn colvarRelatedStoreName = new TableSchema.TableColumn(schema);
				colvarRelatedStoreName.ColumnName = "related_store_name";
				colvarRelatedStoreName.DataType = DbType.String;
				colvarRelatedStoreName.MaxLength = 50;
				colvarRelatedStoreName.AutoIncrement = false;
				colvarRelatedStoreName.IsNullable = false;
				colvarRelatedStoreName.IsPrimaryKey = false;
				colvarRelatedStoreName.IsForeignKey = false;
				colvarRelatedStoreName.IsReadOnly = false;
				colvarRelatedStoreName.DefaultSetting = @"";
				colvarRelatedStoreName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarRelatedStoreName);
				
				TableSchema.TableColumn colvarRelatedOderId = new TableSchema.TableColumn(schema);
				colvarRelatedOderId.ColumnName = "related_oder_id";
				colvarRelatedOderId.DataType = DbType.String;
				colvarRelatedOderId.MaxLength = 50;
				colvarRelatedOderId.AutoIncrement = false;
				colvarRelatedOderId.IsNullable = false;
				colvarRelatedOderId.IsPrimaryKey = false;
				colvarRelatedOderId.IsForeignKey = false;
				colvarRelatedOderId.IsReadOnly = false;
				colvarRelatedOderId.DefaultSetting = @"";
				colvarRelatedOderId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarRelatedOderId);
				
				TableSchema.TableColumn colvarIdentityCode = new TableSchema.TableColumn(schema);
				colvarIdentityCode.ColumnName = "identity_code";
				colvarIdentityCode.DataType = DbType.String;
				colvarIdentityCode.MaxLength = 10;
				colvarIdentityCode.AutoIncrement = false;
				colvarIdentityCode.IsNullable = false;
				colvarIdentityCode.IsPrimaryKey = false;
				colvarIdentityCode.IsForeignKey = false;
				colvarIdentityCode.IsReadOnly = false;
				colvarIdentityCode.DefaultSetting = @"";
				colvarIdentityCode.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIdentityCode);
				
				TableSchema.TableColumn colvarTradeTime = new TableSchema.TableColumn(schema);
				colvarTradeTime.ColumnName = "trade_time";
				colvarTradeTime.DataType = DbType.DateTime;
				colvarTradeTime.MaxLength = 0;
				colvarTradeTime.AutoIncrement = false;
				colvarTradeTime.IsNullable = false;
				colvarTradeTime.IsPrimaryKey = false;
				colvarTradeTime.IsForeignKey = false;
				colvarTradeTime.IsReadOnly = false;
				colvarTradeTime.DefaultSetting = @"";
				colvarTradeTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTradeTime);
				
				TableSchema.TableColumn colvarItemId = new TableSchema.TableColumn(schema);
				colvarItemId.ColumnName = "item_id";
				colvarItemId.DataType = DbType.String;
				colvarItemId.MaxLength = 20;
				colvarItemId.AutoIncrement = false;
				colvarItemId.IsNullable = false;
				colvarItemId.IsPrimaryKey = false;
				colvarItemId.IsForeignKey = false;
				colvarItemId.IsReadOnly = false;
				colvarItemId.DefaultSetting = @"";
				colvarItemId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarItemId);
				
				TableSchema.TableColumn colvarItemName = new TableSchema.TableColumn(schema);
				colvarItemName.ColumnName = "item_name";
				colvarItemName.DataType = DbType.String;
				colvarItemName.MaxLength = 50;
				colvarItemName.AutoIncrement = false;
				colvarItemName.IsNullable = false;
				colvarItemName.IsPrimaryKey = false;
				colvarItemName.IsForeignKey = false;
				colvarItemName.IsReadOnly = false;
				colvarItemName.DefaultSetting = @"";
				colvarItemName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarItemName);
				
				TableSchema.TableColumn colvarItemSpec = new TableSchema.TableColumn(schema);
				colvarItemSpec.ColumnName = "item_spec";
				colvarItemSpec.DataType = DbType.String;
				colvarItemSpec.MaxLength = 20;
				colvarItemSpec.AutoIncrement = false;
				colvarItemSpec.IsNullable = false;
				colvarItemSpec.IsPrimaryKey = false;
				colvarItemSpec.IsForeignKey = false;
				colvarItemSpec.IsReadOnly = false;
				colvarItemSpec.DefaultSetting = @"";
				colvarItemSpec.ForeignKeyTableName = "";
				schema.Columns.Add(colvarItemSpec);
				
				TableSchema.TableColumn colvarItemNotes = new TableSchema.TableColumn(schema);
				colvarItemNotes.ColumnName = "item_notes";
				colvarItemNotes.DataType = DbType.String;
				colvarItemNotes.MaxLength = 50;
				colvarItemNotes.AutoIncrement = false;
				colvarItemNotes.IsNullable = false;
				colvarItemNotes.IsPrimaryKey = false;
				colvarItemNotes.IsForeignKey = false;
				colvarItemNotes.IsReadOnly = false;
				colvarItemNotes.DefaultSetting = @"";
				colvarItemNotes.ForeignKeyTableName = "";
				schema.Columns.Add(colvarItemNotes);
				
				TableSchema.TableColumn colvarPrice = new TableSchema.TableColumn(schema);
				colvarPrice.ColumnName = "price";
				colvarPrice.DataType = DbType.Currency;
				colvarPrice.MaxLength = 0;
				colvarPrice.AutoIncrement = false;
				colvarPrice.IsNullable = false;
				colvarPrice.IsPrimaryKey = false;
				colvarPrice.IsForeignKey = false;
				colvarPrice.IsReadOnly = false;
				colvarPrice.DefaultSetting = @"";
				colvarPrice.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPrice);
				
				TableSchema.TableColumn colvarQty = new TableSchema.TableColumn(schema);
				colvarQty.ColumnName = "qty";
				colvarQty.DataType = DbType.Int32;
				colvarQty.MaxLength = 0;
				colvarQty.AutoIncrement = false;
				colvarQty.IsNullable = false;
				colvarQty.IsPrimaryKey = false;
				colvarQty.IsForeignKey = false;
				colvarQty.IsReadOnly = false;
				colvarQty.DefaultSetting = @"";
				colvarQty.ForeignKeyTableName = "";
				schema.Columns.Add(colvarQty);
				
				TableSchema.TableColumn colvarUnit = new TableSchema.TableColumn(schema);
				colvarUnit.ColumnName = "unit";
				colvarUnit.DataType = DbType.String;
				colvarUnit.MaxLength = 10;
				colvarUnit.AutoIncrement = false;
				colvarUnit.IsNullable = false;
				colvarUnit.IsPrimaryKey = false;
				colvarUnit.IsForeignKey = false;
				colvarUnit.IsReadOnly = false;
				colvarUnit.DefaultSetting = @"";
				colvarUnit.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUnit);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				
						colvarCreateTime.DefaultSetting = @"(getdate())";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("pcp_order_detail",schema);
			}
		}
		#endregion
		
		#region Props
		
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		
		[XmlAttribute("PcpOrderGuid")]
		[Bindable(true)]
		public Guid PcpOrderGuid 
		{
			get { return GetColumnValue<Guid>(Columns.PcpOrderGuid); }
			set { SetColumnValue(Columns.PcpOrderGuid, value); }
		}
		
		[XmlAttribute("PcpOrderPk")]
		[Bindable(true)]
		public int PcpOrderPk 
		{
			get { return GetColumnValue<int>(Columns.PcpOrderPk); }
			set { SetColumnValue(Columns.PcpOrderPk, value); }
		}
		
		[XmlAttribute("RelatedStoreCode")]
		[Bindable(true)]
		public string RelatedStoreCode 
		{
			get { return GetColumnValue<string>(Columns.RelatedStoreCode); }
			set { SetColumnValue(Columns.RelatedStoreCode, value); }
		}
		
		[XmlAttribute("RelatedStoreName")]
		[Bindable(true)]
		public string RelatedStoreName 
		{
			get { return GetColumnValue<string>(Columns.RelatedStoreName); }
			set { SetColumnValue(Columns.RelatedStoreName, value); }
		}
		
		[XmlAttribute("RelatedOderId")]
		[Bindable(true)]
		public string RelatedOderId 
		{
			get { return GetColumnValue<string>(Columns.RelatedOderId); }
			set { SetColumnValue(Columns.RelatedOderId, value); }
		}
		
		[XmlAttribute("IdentityCode")]
		[Bindable(true)]
		public string IdentityCode 
		{
			get { return GetColumnValue<string>(Columns.IdentityCode); }
			set { SetColumnValue(Columns.IdentityCode, value); }
		}
		
		[XmlAttribute("TradeTime")]
		[Bindable(true)]
		public DateTime TradeTime 
		{
			get { return GetColumnValue<DateTime>(Columns.TradeTime); }
			set { SetColumnValue(Columns.TradeTime, value); }
		}
		
		[XmlAttribute("ItemId")]
		[Bindable(true)]
		public string ItemId 
		{
			get { return GetColumnValue<string>(Columns.ItemId); }
			set { SetColumnValue(Columns.ItemId, value); }
		}
		
		[XmlAttribute("ItemName")]
		[Bindable(true)]
		public string ItemName 
		{
			get { return GetColumnValue<string>(Columns.ItemName); }
			set { SetColumnValue(Columns.ItemName, value); }
		}
		
		[XmlAttribute("ItemSpec")]
		[Bindable(true)]
		public string ItemSpec 
		{
			get { return GetColumnValue<string>(Columns.ItemSpec); }
			set { SetColumnValue(Columns.ItemSpec, value); }
		}
		
		[XmlAttribute("ItemNotes")]
		[Bindable(true)]
		public string ItemNotes 
		{
			get { return GetColumnValue<string>(Columns.ItemNotes); }
			set { SetColumnValue(Columns.ItemNotes, value); }
		}
		
		[XmlAttribute("Price")]
		[Bindable(true)]
		public decimal Price 
		{
			get { return GetColumnValue<decimal>(Columns.Price); }
			set { SetColumnValue(Columns.Price, value); }
		}
		
		[XmlAttribute("Qty")]
		[Bindable(true)]
		public int Qty 
		{
			get { return GetColumnValue<int>(Columns.Qty); }
			set { SetColumnValue(Columns.Qty, value); }
		}
		
		[XmlAttribute("Unit")]
		[Bindable(true)]
		public string Unit 
		{
			get { return GetColumnValue<string>(Columns.Unit); }
			set { SetColumnValue(Columns.Unit, value); }
		}
		
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn PcpOrderGuidColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn PcpOrderPkColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn RelatedStoreCodeColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn RelatedStoreNameColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn RelatedOderIdColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn IdentityCodeColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn TradeTimeColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn ItemIdColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn ItemNameColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn ItemSpecColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        public static TableSchema.TableColumn ItemNotesColumn
        {
            get { return Schema.Columns[11]; }
        }
        
        
        
        public static TableSchema.TableColumn PriceColumn
        {
            get { return Schema.Columns[12]; }
        }
        
        
        
        public static TableSchema.TableColumn QtyColumn
        {
            get { return Schema.Columns[13]; }
        }
        
        
        
        public static TableSchema.TableColumn UnitColumn
        {
            get { return Schema.Columns[14]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[15]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string PcpOrderGuid = @"pcp_order_guid";
			 public static string PcpOrderPk = @"pcp_order_pk";
			 public static string RelatedStoreCode = @"related_store_code";
			 public static string RelatedStoreName = @"related_store_name";
			 public static string RelatedOderId = @"related_oder_id";
			 public static string IdentityCode = @"identity_code";
			 public static string TradeTime = @"trade_time";
			 public static string ItemId = @"item_id";
			 public static string ItemName = @"item_name";
			 public static string ItemSpec = @"item_spec";
			 public static string ItemNotes = @"item_notes";
			 public static string Price = @"price";
			 public static string Qty = @"qty";
			 public static string Unit = @"unit";
			 public static string CreateTime = @"create_time";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

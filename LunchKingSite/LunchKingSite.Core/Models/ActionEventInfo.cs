using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the ActionEventInfo class.
	/// </summary>
    [Serializable]
	public partial class ActionEventInfoCollection : RepositoryList<ActionEventInfo, ActionEventInfoCollection>
	{	   
		public ActionEventInfoCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>ActionEventInfoCollection</returns>
		public ActionEventInfoCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                ActionEventInfo o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the action_event_info table.
	/// </summary>
	[Serializable]
	public partial class ActionEventInfo : RepositoryRecord<ActionEventInfo>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public ActionEventInfo()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public ActionEventInfo(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("action_event_info", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarActionName = new TableSchema.TableColumn(schema);
				colvarActionName.ColumnName = "action_name";
				colvarActionName.DataType = DbType.AnsiString;
				colvarActionName.MaxLength = 50;
				colvarActionName.AutoIncrement = false;
				colvarActionName.IsNullable = false;
				colvarActionName.IsPrimaryKey = false;
				colvarActionName.IsForeignKey = false;
				colvarActionName.IsReadOnly = false;
				colvarActionName.DefaultSetting = @"";
				colvarActionName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarActionName);
				
				TableSchema.TableColumn colvarActionSubject = new TableSchema.TableColumn(schema);
				colvarActionSubject.ColumnName = "action_subject";
				colvarActionSubject.DataType = DbType.String;
				colvarActionSubject.MaxLength = 100;
				colvarActionSubject.AutoIncrement = false;
				colvarActionSubject.IsNullable = false;
				colvarActionSubject.IsPrimaryKey = false;
				colvarActionSubject.IsForeignKey = false;
				colvarActionSubject.IsReadOnly = false;
				colvarActionSubject.DefaultSetting = @"";
				colvarActionSubject.ForeignKeyTableName = "";
				schema.Columns.Add(colvarActionSubject);
				
				TableSchema.TableColumn colvarActionType = new TableSchema.TableColumn(schema);
				colvarActionType.ColumnName = "action_type";
				colvarActionType.DataType = DbType.Int32;
				colvarActionType.MaxLength = 0;
				colvarActionType.AutoIncrement = false;
				colvarActionType.IsNullable = false;
				colvarActionType.IsPrimaryKey = false;
				colvarActionType.IsForeignKey = false;
				colvarActionType.IsReadOnly = false;
				
						colvarActionType.DefaultSetting = @"((0))";
				colvarActionType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarActionType);
				
				TableSchema.TableColumn colvarActionStartTime = new TableSchema.TableColumn(schema);
				colvarActionStartTime.ColumnName = "action_start_time";
				colvarActionStartTime.DataType = DbType.DateTime;
				colvarActionStartTime.MaxLength = 0;
				colvarActionStartTime.AutoIncrement = false;
				colvarActionStartTime.IsNullable = false;
				colvarActionStartTime.IsPrimaryKey = false;
				colvarActionStartTime.IsForeignKey = false;
				colvarActionStartTime.IsReadOnly = false;
				colvarActionStartTime.DefaultSetting = @"";
				colvarActionStartTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarActionStartTime);
				
				TableSchema.TableColumn colvarActionEndTime = new TableSchema.TableColumn(schema);
				colvarActionEndTime.ColumnName = "action_end_time";
				colvarActionEndTime.DataType = DbType.DateTime;
				colvarActionEndTime.MaxLength = 0;
				colvarActionEndTime.AutoIncrement = false;
				colvarActionEndTime.IsNullable = false;
				colvarActionEndTime.IsPrimaryKey = false;
				colvarActionEndTime.IsForeignKey = false;
				colvarActionEndTime.IsReadOnly = false;
				colvarActionEndTime.DefaultSetting = @"";
				colvarActionEndTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarActionEndTime);
				
				TableSchema.TableColumn colvarSellerGuid = new TableSchema.TableColumn(schema);
				colvarSellerGuid.ColumnName = "seller_guid";
				colvarSellerGuid.DataType = DbType.Guid;
				colvarSellerGuid.MaxLength = 0;
				colvarSellerGuid.AutoIncrement = false;
				colvarSellerGuid.IsNullable = true;
				colvarSellerGuid.IsPrimaryKey = false;
				colvarSellerGuid.IsForeignKey = false;
				colvarSellerGuid.IsReadOnly = false;
				colvarSellerGuid.DefaultSetting = @"";
				colvarSellerGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSellerGuid);
				
				TableSchema.TableColumn colvarSellerUuid = new TableSchema.TableColumn(schema);
				colvarSellerUuid.ColumnName = "seller_uuid";
				colvarSellerUuid.DataType = DbType.Guid;
				colvarSellerUuid.MaxLength = 0;
				colvarSellerUuid.AutoIncrement = false;
				colvarSellerUuid.IsNullable = true;
				colvarSellerUuid.IsPrimaryKey = false;
				colvarSellerUuid.IsForeignKey = false;
				colvarSellerUuid.IsReadOnly = false;
				colvarSellerUuid.DefaultSetting = @"";
				colvarSellerUuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSellerUuid);
				
				TableSchema.TableColumn colvarElectricUuid = new TableSchema.TableColumn(schema);
				colvarElectricUuid.ColumnName = "electric_uuid";
				colvarElectricUuid.DataType = DbType.Guid;
				colvarElectricUuid.MaxLength = 0;
				colvarElectricUuid.AutoIncrement = false;
				colvarElectricUuid.IsNullable = true;
				colvarElectricUuid.IsPrimaryKey = false;
				colvarElectricUuid.IsForeignKey = false;
				colvarElectricUuid.IsReadOnly = false;
				colvarElectricUuid.DefaultSetting = @"";
				colvarElectricUuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarElectricUuid);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("action_event_info",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("ActionName")]
		[Bindable(true)]
		public string ActionName 
		{
			get { return GetColumnValue<string>(Columns.ActionName); }
			set { SetColumnValue(Columns.ActionName, value); }
		}
		  
		[XmlAttribute("ActionSubject")]
		[Bindable(true)]
		public string ActionSubject 
		{
			get { return GetColumnValue<string>(Columns.ActionSubject); }
			set { SetColumnValue(Columns.ActionSubject, value); }
		}
		  
		[XmlAttribute("ActionType")]
		[Bindable(true)]
		public int ActionType 
		{
			get { return GetColumnValue<int>(Columns.ActionType); }
			set { SetColumnValue(Columns.ActionType, value); }
		}
		  
		[XmlAttribute("ActionStartTime")]
		[Bindable(true)]
		public DateTime ActionStartTime 
		{
			get { return GetColumnValue<DateTime>(Columns.ActionStartTime); }
			set { SetColumnValue(Columns.ActionStartTime, value); }
		}
		  
		[XmlAttribute("ActionEndTime")]
		[Bindable(true)]
		public DateTime ActionEndTime 
		{
			get { return GetColumnValue<DateTime>(Columns.ActionEndTime); }
			set { SetColumnValue(Columns.ActionEndTime, value); }
		}
		  
		[XmlAttribute("SellerGuid")]
		[Bindable(true)]
		public Guid? SellerGuid 
		{
			get { return GetColumnValue<Guid?>(Columns.SellerGuid); }
			set { SetColumnValue(Columns.SellerGuid, value); }
		}
		  
		[XmlAttribute("SellerUuid")]
		[Bindable(true)]
		public Guid? SellerUuid 
		{
			get { return GetColumnValue<Guid?>(Columns.SellerUuid); }
			set { SetColumnValue(Columns.SellerUuid, value); }
		}
		  
		[XmlAttribute("ElectricUuid")]
		[Bindable(true)]
		public Guid? ElectricUuid 
		{
			get { return GetColumnValue<Guid?>(Columns.ElectricUuid); }
			set { SetColumnValue(Columns.ElectricUuid, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn ActionNameColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn ActionSubjectColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn ActionTypeColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn ActionStartTimeColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn ActionEndTimeColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn SellerGuidColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn SellerUuidColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn ElectricUuidColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string ActionName = @"action_name";
			 public static string ActionSubject = @"action_subject";
			 public static string ActionType = @"action_type";
			 public static string ActionStartTime = @"action_start_time";
			 public static string ActionEndTime = @"action_end_time";
			 public static string SellerGuid = @"seller_guid";
			 public static string SellerUuid = @"seller_uuid";
			 public static string ElectricUuid = @"electric_uuid";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

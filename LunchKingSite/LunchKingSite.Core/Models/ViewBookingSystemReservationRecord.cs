using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewBookingSystemReservationRecord class.
    /// </summary>
    [Serializable]
    public partial class ViewBookingSystemReservationRecordCollection : ReadOnlyList<ViewBookingSystemReservationRecord, ViewBookingSystemReservationRecordCollection>
    {        
        public ViewBookingSystemReservationRecordCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_booking_system_reservation_record view.
    /// </summary>
    [Serializable]
    public partial class ViewBookingSystemReservationRecord : ReadOnlyRecord<ViewBookingSystemReservationRecord>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_booking_system_reservation_record", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = false;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = false;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                
                schema.Columns.Add(colvarId);
                
                TableSchema.TableColumn colvarReservationDate = new TableSchema.TableColumn(schema);
                colvarReservationDate.ColumnName = "reservation_date";
                colvarReservationDate.DataType = DbType.AnsiString;
                colvarReservationDate.MaxLength = 30;
                colvarReservationDate.AutoIncrement = false;
                colvarReservationDate.IsNullable = true;
                colvarReservationDate.IsPrimaryKey = false;
                colvarReservationDate.IsForeignKey = false;
                colvarReservationDate.IsReadOnly = false;
                
                schema.Columns.Add(colvarReservationDate);
                
                TableSchema.TableColumn colvarTimeSlot = new TableSchema.TableColumn(schema);
                colvarTimeSlot.ColumnName = "time_slot";
                colvarTimeSlot.DataType = DbType.AnsiString;
                colvarTimeSlot.MaxLength = 5;
                colvarTimeSlot.AutoIncrement = false;
                colvarTimeSlot.IsNullable = false;
                colvarTimeSlot.IsPrimaryKey = false;
                colvarTimeSlot.IsForeignKey = false;
                colvarTimeSlot.IsReadOnly = false;
                
                schema.Columns.Add(colvarTimeSlot);
                
                TableSchema.TableColumn colvarReservationDateTime = new TableSchema.TableColumn(schema);
                colvarReservationDateTime.ColumnName = "reservation_date_time";
                colvarReservationDateTime.DataType = DbType.DateTime;
                colvarReservationDateTime.MaxLength = 0;
                colvarReservationDateTime.AutoIncrement = false;
                colvarReservationDateTime.IsNullable = true;
                colvarReservationDateTime.IsPrimaryKey = false;
                colvarReservationDateTime.IsForeignKey = false;
                colvarReservationDateTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarReservationDateTime);
                
                TableSchema.TableColumn colvarNumberOfPeople = new TableSchema.TableColumn(schema);
                colvarNumberOfPeople.ColumnName = "number_of_people";
                colvarNumberOfPeople.DataType = DbType.Int32;
                colvarNumberOfPeople.MaxLength = 0;
                colvarNumberOfPeople.AutoIncrement = false;
                colvarNumberOfPeople.IsNullable = false;
                colvarNumberOfPeople.IsPrimaryKey = false;
                colvarNumberOfPeople.IsForeignKey = false;
                colvarNumberOfPeople.IsReadOnly = false;
                
                schema.Columns.Add(colvarNumberOfPeople);
                
                TableSchema.TableColumn colvarRemark = new TableSchema.TableColumn(schema);
                colvarRemark.ColumnName = "remark";
                colvarRemark.DataType = DbType.String;
                colvarRemark.MaxLength = 256;
                colvarRemark.AutoIncrement = false;
                colvarRemark.IsNullable = false;
                colvarRemark.IsPrimaryKey = false;
                colvarRemark.IsForeignKey = false;
                colvarRemark.IsReadOnly = false;
                
                schema.Columns.Add(colvarRemark);
                
                TableSchema.TableColumn colvarIsCancel = new TableSchema.TableColumn(schema);
                colvarIsCancel.ColumnName = "is_cancel";
                colvarIsCancel.DataType = DbType.Boolean;
                colvarIsCancel.MaxLength = 0;
                colvarIsCancel.AutoIncrement = false;
                colvarIsCancel.IsNullable = false;
                colvarIsCancel.IsPrimaryKey = false;
                colvarIsCancel.IsForeignKey = false;
                colvarIsCancel.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsCancel);
                
                TableSchema.TableColumn colvarCancelDate = new TableSchema.TableColumn(schema);
                colvarCancelDate.ColumnName = "cancel_date";
                colvarCancelDate.DataType = DbType.DateTime;
                colvarCancelDate.MaxLength = 0;
                colvarCancelDate.AutoIncrement = false;
                colvarCancelDate.IsNullable = true;
                colvarCancelDate.IsPrimaryKey = false;
                colvarCancelDate.IsForeignKey = false;
                colvarCancelDate.IsReadOnly = false;
                
                schema.Columns.Add(colvarCancelDate);
                
                TableSchema.TableColumn colvarMemberKey = new TableSchema.TableColumn(schema);
                colvarMemberKey.ColumnName = "member_key";
                colvarMemberKey.DataType = DbType.AnsiString;
                colvarMemberKey.MaxLength = 50;
                colvarMemberKey.AutoIncrement = false;
                colvarMemberKey.IsNullable = true;
                colvarMemberKey.IsPrimaryKey = false;
                colvarMemberKey.IsForeignKey = false;
                colvarMemberKey.IsReadOnly = false;
                
                schema.Columns.Add(colvarMemberKey);
                
                TableSchema.TableColumn colvarCouponInfo = new TableSchema.TableColumn(schema);
                colvarCouponInfo.ColumnName = "coupon_info";
                colvarCouponInfo.DataType = DbType.String;
                colvarCouponInfo.MaxLength = 256;
                colvarCouponInfo.AutoIncrement = false;
                colvarCouponInfo.IsNullable = true;
                colvarCouponInfo.IsPrimaryKey = false;
                colvarCouponInfo.IsForeignKey = false;
                colvarCouponInfo.IsReadOnly = false;
                
                schema.Columns.Add(colvarCouponInfo);
                
                TableSchema.TableColumn colvarCouponLink = new TableSchema.TableColumn(schema);
                colvarCouponLink.ColumnName = "coupon_link";
                colvarCouponLink.DataType = DbType.AnsiString;
                colvarCouponLink.MaxLength = 128;
                colvarCouponLink.AutoIncrement = false;
                colvarCouponLink.IsNullable = true;
                colvarCouponLink.IsPrimaryKey = false;
                colvarCouponLink.IsForeignKey = false;
                colvarCouponLink.IsReadOnly = false;
                
                schema.Columns.Add(colvarCouponLink);
                
                TableSchema.TableColumn colvarApiId = new TableSchema.TableColumn(schema);
                colvarApiId.ColumnName = "api_id";
                colvarApiId.DataType = DbType.Int32;
                colvarApiId.MaxLength = 0;
                colvarApiId.AutoIncrement = false;
                colvarApiId.IsNullable = true;
                colvarApiId.IsPrimaryKey = false;
                colvarApiId.IsForeignKey = false;
                colvarApiId.IsReadOnly = false;
                
                schema.Columns.Add(colvarApiId);
                
                TableSchema.TableColumn colvarBookingType = new TableSchema.TableColumn(schema);
                colvarBookingType.ColumnName = "booking_type";
                colvarBookingType.DataType = DbType.Int32;
                colvarBookingType.MaxLength = 0;
                colvarBookingType.AutoIncrement = false;
                colvarBookingType.IsNullable = false;
                colvarBookingType.IsPrimaryKey = false;
                colvarBookingType.IsForeignKey = false;
                colvarBookingType.IsReadOnly = false;
                
                schema.Columns.Add(colvarBookingType);
                
                TableSchema.TableColumn colvarIsLock = new TableSchema.TableColumn(schema);
                colvarIsLock.ColumnName = "is_lock";
                colvarIsLock.DataType = DbType.Boolean;
                colvarIsLock.MaxLength = 0;
                colvarIsLock.AutoIncrement = false;
                colvarIsLock.IsNullable = false;
                colvarIsLock.IsPrimaryKey = false;
                colvarIsLock.IsForeignKey = false;
                colvarIsLock.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsLock);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_booking_system_reservation_record",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewBookingSystemReservationRecord()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewBookingSystemReservationRecord(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewBookingSystemReservationRecord(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewBookingSystemReservationRecord(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id 
	    {
		    get
		    {
			    return GetColumnValue<int>("id");
		    }
            set 
		    {
			    SetColumnValue("id", value);
            }
        }
	      
        [XmlAttribute("ReservationDate")]
        [Bindable(true)]
        public string ReservationDate 
	    {
		    get
		    {
			    return GetColumnValue<string>("reservation_date");
		    }
            set 
		    {
			    SetColumnValue("reservation_date", value);
            }
        }
	      
        [XmlAttribute("TimeSlot")]
        [Bindable(true)]
        public string TimeSlot 
	    {
		    get
		    {
			    return GetColumnValue<string>("time_slot");
		    }
            set 
		    {
			    SetColumnValue("time_slot", value);
            }
        }
	      
        [XmlAttribute("ReservationDateTime")]
        [Bindable(true)]
        public DateTime? ReservationDateTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("reservation_date_time");
		    }
            set 
		    {
			    SetColumnValue("reservation_date_time", value);
            }
        }
	      
        [XmlAttribute("NumberOfPeople")]
        [Bindable(true)]
        public int NumberOfPeople 
	    {
		    get
		    {
			    return GetColumnValue<int>("number_of_people");
		    }
            set 
		    {
			    SetColumnValue("number_of_people", value);
            }
        }
	      
        [XmlAttribute("Remark")]
        [Bindable(true)]
        public string Remark 
	    {
		    get
		    {
			    return GetColumnValue<string>("remark");
		    }
            set 
		    {
			    SetColumnValue("remark", value);
            }
        }
	      
        [XmlAttribute("IsCancel")]
        [Bindable(true)]
        public bool IsCancel 
	    {
		    get
		    {
			    return GetColumnValue<bool>("is_cancel");
		    }
            set 
		    {
			    SetColumnValue("is_cancel", value);
            }
        }
	      
        [XmlAttribute("CancelDate")]
        [Bindable(true)]
        public DateTime? CancelDate 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("cancel_date");
		    }
            set 
		    {
			    SetColumnValue("cancel_date", value);
            }
        }
	      
        [XmlAttribute("MemberKey")]
        [Bindable(true)]
        public string MemberKey 
	    {
		    get
		    {
			    return GetColumnValue<string>("member_key");
		    }
            set 
		    {
			    SetColumnValue("member_key", value);
            }
        }
	      
        [XmlAttribute("CouponInfo")]
        [Bindable(true)]
        public string CouponInfo 
	    {
		    get
		    {
			    return GetColumnValue<string>("coupon_info");
		    }
            set 
		    {
			    SetColumnValue("coupon_info", value);
            }
        }
	      
        [XmlAttribute("CouponLink")]
        [Bindable(true)]
        public string CouponLink 
	    {
		    get
		    {
			    return GetColumnValue<string>("coupon_link");
		    }
            set 
		    {
			    SetColumnValue("coupon_link", value);
            }
        }
	      
        [XmlAttribute("ApiId")]
        [Bindable(true)]
        public int? ApiId 
	    {
		    get
		    {
			    return GetColumnValue<int?>("api_id");
		    }
            set 
		    {
			    SetColumnValue("api_id", value);
            }
        }
	      
        [XmlAttribute("BookingType")]
        [Bindable(true)]
        public int BookingType 
	    {
		    get
		    {
			    return GetColumnValue<int>("booking_type");
		    }
            set 
		    {
			    SetColumnValue("booking_type", value);
            }
        }
	      
        [XmlAttribute("IsLock")]
        [Bindable(true)]
        public bool IsLock 
	    {
		    get
		    {
			    return GetColumnValue<bool>("is_lock");
		    }
            set 
		    {
			    SetColumnValue("is_lock", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string Id = @"id";
            
            public static string ReservationDate = @"reservation_date";
            
            public static string TimeSlot = @"time_slot";
            
            public static string ReservationDateTime = @"reservation_date_time";
            
            public static string NumberOfPeople = @"number_of_people";
            
            public static string Remark = @"remark";
            
            public static string IsCancel = @"is_cancel";
            
            public static string CancelDate = @"cancel_date";
            
            public static string MemberKey = @"member_key";
            
            public static string CouponInfo = @"coupon_info";
            
            public static string CouponLink = @"coupon_link";
            
            public static string ApiId = @"api_id";
            
            public static string BookingType = @"booking_type";
            
            public static string IsLock = @"is_lock";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

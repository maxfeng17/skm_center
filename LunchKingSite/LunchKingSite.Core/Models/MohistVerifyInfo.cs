using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the MohistVerifyInfo class.
    /// </summary>
    [Serializable]
    public partial class MohistVerifyInfoCollection : RepositoryList<MohistVerifyInfo, MohistVerifyInfoCollection>
    {
        public MohistVerifyInfoCollection() { }

        /// <summary>
        /// Filters an existing collection based on the set criteria. This is an in-memory filter
        /// Thanks to developingchris for this!
        /// </summary>
        /// <returns>MohistVerifyInfoCollection</returns>
        public MohistVerifyInfoCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                MohistVerifyInfo o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }


    }
    /// <summary>
    /// This is an ActiveRecord class which wraps the mohist_verify_info table.
    /// </summary>
    [Serializable]
    public partial class MohistVerifyInfo : RepositoryRecord<MohistVerifyInfo>, IRecordBase
    {
        #region .ctors and Default Settings

        public MohistVerifyInfo()
        {
            SetSQLProps();
            InitSetDefaults();
            MarkNew();
        }

        private void InitSetDefaults() { SetDefaults(); }

        public MohistVerifyInfo(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
                ForceDefaults();
            MarkNew();
        }

        protected static void SetSQLProps() { GetTableSchema(); }

        #endregion

        #region Schema and Query Accessor	
        public static Query CreateQuery() { return new Query(Schema); }
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                    SetSQLProps();
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("mohist_verify_info", TableType.Table, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = true;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = true;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                colvarId.DefaultSetting = @"";
                colvarId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarId);

                TableSchema.TableColumn colvarMohistDealsId = new TableSchema.TableColumn(schema);
                colvarMohistDealsId.ColumnName = "mohist_deals_id";
                colvarMohistDealsId.DataType = DbType.String;
                colvarMohistDealsId.MaxLength = 20;
                colvarMohistDealsId.AutoIncrement = false;
                colvarMohistDealsId.IsNullable = false;
                colvarMohistDealsId.IsPrimaryKey = false;
                colvarMohistDealsId.IsForeignKey = false;
                colvarMohistDealsId.IsReadOnly = false;
                colvarMohistDealsId.DefaultSetting = @"";
                colvarMohistDealsId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarMohistDealsId);

                TableSchema.TableColumn colvarOrderDate = new TableSchema.TableColumn(schema);
                colvarOrderDate.ColumnName = "order_date";
                colvarOrderDate.DataType = DbType.DateTime;
                colvarOrderDate.MaxLength = 0;
                colvarOrderDate.AutoIncrement = false;
                colvarOrderDate.IsNullable = false;
                colvarOrderDate.IsPrimaryKey = false;
                colvarOrderDate.IsForeignKey = false;
                colvarOrderDate.IsReadOnly = false;
                colvarOrderDate.DefaultSetting = @"";
                colvarOrderDate.ForeignKeyTableName = "";
                schema.Columns.Add(colvarOrderDate);

                TableSchema.TableColumn colvarMohistProductId = new TableSchema.TableColumn(schema);
                colvarMohistProductId.ColumnName = "mohist_product_id";
                colvarMohistProductId.DataType = DbType.String;
                colvarMohistProductId.MaxLength = 15;
                colvarMohistProductId.AutoIncrement = false;
                colvarMohistProductId.IsNullable = false;
                colvarMohistProductId.IsPrimaryKey = false;
                colvarMohistProductId.IsForeignKey = false;
                colvarMohistProductId.IsReadOnly = false;
                colvarMohistProductId.DefaultSetting = @"";
                colvarMohistProductId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarMohistProductId);

                TableSchema.TableColumn colvarItemPrice = new TableSchema.TableColumn(schema);
                colvarItemPrice.ColumnName = "item_price";
                colvarItemPrice.DataType = DbType.Currency;
                colvarItemPrice.MaxLength = 0;
                colvarItemPrice.AutoIncrement = false;
                colvarItemPrice.IsNullable = false;
                colvarItemPrice.IsPrimaryKey = false;
                colvarItemPrice.IsForeignKey = false;
                colvarItemPrice.IsReadOnly = false;
                colvarItemPrice.DefaultSetting = @"";
                colvarItemPrice.ForeignKeyTableName = "";
                schema.Columns.Add(colvarItemPrice);

                TableSchema.TableColumn colvarAmount = new TableSchema.TableColumn(schema);
                colvarAmount.ColumnName = "amount";
                colvarAmount.DataType = DbType.Currency;
                colvarAmount.MaxLength = 0;
                colvarAmount.AutoIncrement = false;
                colvarAmount.IsNullable = false;
                colvarAmount.IsPrimaryKey = false;
                colvarAmount.IsForeignKey = false;
                colvarAmount.IsReadOnly = false;
                colvarAmount.DefaultSetting = @"";
                colvarAmount.ForeignKeyTableName = "";
                schema.Columns.Add(colvarAmount);

                TableSchema.TableColumn colvarBonus = new TableSchema.TableColumn(schema);
                colvarBonus.ColumnName = "bonus";
                colvarBonus.DataType = DbType.Currency;
                colvarBonus.MaxLength = 0;
                colvarBonus.AutoIncrement = false;
                colvarBonus.IsNullable = false;
                colvarBonus.IsPrimaryKey = false;
                colvarBonus.IsForeignKey = false;
                colvarBonus.IsReadOnly = false;
                colvarBonus.DefaultSetting = @"";
                colvarBonus.ForeignKeyTableName = "";
                schema.Columns.Add(colvarBonus);

                TableSchema.TableColumn colvarEcash = new TableSchema.TableColumn(schema);
                colvarEcash.ColumnName = "ecash";
                colvarEcash.DataType = DbType.Currency;
                colvarEcash.MaxLength = 0;
                colvarEcash.AutoIncrement = false;
                colvarEcash.IsNullable = false;
                colvarEcash.IsPrimaryKey = false;
                colvarEcash.IsForeignKey = false;
                colvarEcash.IsReadOnly = false;
                colvarEcash.DefaultSetting = @"";
                colvarEcash.ForeignKeyTableName = "";
                schema.Columns.Add(colvarEcash);

                TableSchema.TableColumn colvarAuthCode = new TableSchema.TableColumn(schema);
                colvarAuthCode.ColumnName = "auth_code";
                colvarAuthCode.DataType = DbType.String;
                colvarAuthCode.MaxLength = 10;
                colvarAuthCode.AutoIncrement = false;
                colvarAuthCode.IsNullable = false;
                colvarAuthCode.IsPrimaryKey = false;
                colvarAuthCode.IsForeignKey = false;
                colvarAuthCode.IsReadOnly = false;
                colvarAuthCode.DefaultSetting = @"";
                colvarAuthCode.ForeignKeyTableName = "";
                schema.Columns.Add(colvarAuthCode);

                TableSchema.TableColumn colvarCouponId = new TableSchema.TableColumn(schema);
                colvarCouponId.ColumnName = "coupon_id";
                colvarCouponId.DataType = DbType.String;
                colvarCouponId.MaxLength = 11;
                colvarCouponId.AutoIncrement = false;
                colvarCouponId.IsNullable = false;
                colvarCouponId.IsPrimaryKey = false;
                colvarCouponId.IsForeignKey = false;
                colvarCouponId.IsReadOnly = false;
                colvarCouponId.DefaultSetting = @"";
                colvarCouponId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCouponId);

                TableSchema.TableColumn colvarSequenceNumber = new TableSchema.TableColumn(schema);
                colvarSequenceNumber.ColumnName = "sequence_number";
                colvarSequenceNumber.DataType = DbType.String;
                colvarSequenceNumber.MaxLength = 50;
                colvarSequenceNumber.AutoIncrement = false;
                colvarSequenceNumber.IsNullable = false;
                colvarSequenceNumber.IsPrimaryKey = false;
                colvarSequenceNumber.IsForeignKey = false;
                colvarSequenceNumber.IsReadOnly = false;
                colvarSequenceNumber.DefaultSetting = @"";
                colvarSequenceNumber.ForeignKeyTableName = "";
                schema.Columns.Add(colvarSequenceNumber);

                TableSchema.TableColumn colvarBarcode = new TableSchema.TableColumn(schema);
                colvarBarcode.ColumnName = "barcode";
                colvarBarcode.DataType = DbType.String;
                colvarBarcode.MaxLength = 20;
                colvarBarcode.AutoIncrement = false;
                colvarBarcode.IsNullable = false;
                colvarBarcode.IsPrimaryKey = false;
                colvarBarcode.IsForeignKey = false;
                colvarBarcode.IsReadOnly = false;
                colvarBarcode.DefaultSetting = @"";
                colvarBarcode.ForeignKeyTableName = "";
                schema.Columns.Add(colvarBarcode);

                TableSchema.TableColumn colvarCouponCode = new TableSchema.TableColumn(schema);
                colvarCouponCode.ColumnName = "coupon_code";
                colvarCouponCode.DataType = DbType.String;
                colvarCouponCode.MaxLength = 50;
                colvarCouponCode.AutoIncrement = false;
                colvarCouponCode.IsNullable = false;
                colvarCouponCode.IsPrimaryKey = false;
                colvarCouponCode.IsForeignKey = false;
                colvarCouponCode.IsReadOnly = false;
                colvarCouponCode.DefaultSetting = @"";
                colvarCouponCode.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCouponCode);

                TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
                colvarStatus.ColumnName = "status";
                colvarStatus.DataType = DbType.Int32;
                colvarStatus.MaxLength = 0;
                colvarStatus.AutoIncrement = false;
                colvarStatus.IsNullable = false;
                colvarStatus.IsPrimaryKey = false;
                colvarStatus.IsForeignKey = false;
                colvarStatus.IsReadOnly = false;
                colvarStatus.DefaultSetting = @"";
                colvarStatus.ForeignKeyTableName = "";
                schema.Columns.Add(colvarStatus);

                TableSchema.TableColumn colvarVerifyType = new TableSchema.TableColumn(schema);
                colvarVerifyType.ColumnName = "verify_type";
                colvarVerifyType.DataType = DbType.Int32;
                colvarVerifyType.MaxLength = 0;
                colvarVerifyType.AutoIncrement = false;
                colvarVerifyType.IsNullable = false;
                colvarVerifyType.IsPrimaryKey = false;
                colvarVerifyType.IsForeignKey = false;
                colvarVerifyType.IsReadOnly = false;

                colvarVerifyType.DefaultSetting = @"((0))";
                colvarVerifyType.ForeignKeyTableName = "";
                schema.Columns.Add(colvarVerifyType);

                TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
                colvarModifyTime.ColumnName = "modify_time";
                colvarModifyTime.DataType = DbType.DateTime;
                colvarModifyTime.MaxLength = 0;
                colvarModifyTime.AutoIncrement = false;
                colvarModifyTime.IsNullable = true;
                colvarModifyTime.IsPrimaryKey = false;
                colvarModifyTime.IsForeignKey = false;
                colvarModifyTime.IsReadOnly = false;
                colvarModifyTime.DefaultSetting = @"";
                colvarModifyTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarModifyTime);

                TableSchema.TableColumn colvarSpecialStatus = new TableSchema.TableColumn(schema);
                colvarSpecialStatus.ColumnName = "special_status";
                colvarSpecialStatus.DataType = DbType.Int32;
                colvarSpecialStatus.MaxLength = 0;
                colvarSpecialStatus.AutoIncrement = false;
                colvarSpecialStatus.IsNullable = true;
                colvarSpecialStatus.IsPrimaryKey = false;
                colvarSpecialStatus.IsForeignKey = false;
                colvarSpecialStatus.IsReadOnly = false;
                colvarSpecialStatus.DefaultSetting = @"";
                colvarSpecialStatus.ForeignKeyTableName = "";
                schema.Columns.Add(colvarSpecialStatus);

                TableSchema.TableColumn colvarSpecialTime = new TableSchema.TableColumn(schema);
                colvarSpecialTime.ColumnName = "special_time";
                colvarSpecialTime.DataType = DbType.DateTime;
                colvarSpecialTime.MaxLength = 0;
                colvarSpecialTime.AutoIncrement = false;
                colvarSpecialTime.IsNullable = true;
                colvarSpecialTime.IsPrimaryKey = false;
                colvarSpecialTime.IsForeignKey = false;
                colvarSpecialTime.IsReadOnly = false;
                colvarSpecialTime.DefaultSetting = @"";
                colvarSpecialTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarSpecialTime);

                TableSchema.TableColumn colvarIsWebserviceVerify = new TableSchema.TableColumn(schema);
                colvarIsWebserviceVerify.ColumnName = "is_webservice_verify";
                colvarIsWebserviceVerify.DataType = DbType.Boolean;
                colvarIsWebserviceVerify.MaxLength = 0;
                colvarIsWebserviceVerify.AutoIncrement = false;
                colvarIsWebserviceVerify.IsNullable = false;
                colvarIsWebserviceVerify.IsPrimaryKey = false;
                colvarIsWebserviceVerify.IsForeignKey = false;
                colvarIsWebserviceVerify.IsReadOnly = false;

                colvarIsWebserviceVerify.DefaultSetting = @"((0))";
                colvarIsWebserviceVerify.ForeignKeyTableName = "";
                schema.Columns.Add(colvarIsWebserviceVerify);

                TableSchema.TableColumn colvarMemo = new TableSchema.TableColumn(schema);
                colvarMemo.ColumnName = "memo";
                colvarMemo.DataType = DbType.String;
                colvarMemo.MaxLength = 500;
                colvarMemo.AutoIncrement = false;
                colvarMemo.IsNullable = true;
                colvarMemo.IsPrimaryKey = false;
                colvarMemo.IsForeignKey = false;
                colvarMemo.IsReadOnly = false;
                colvarMemo.DefaultSetting = @"";
                colvarMemo.ForeignKeyTableName = "";
                schema.Columns.Add(colvarMemo);

                TableSchema.TableColumn colvarOrderId = new TableSchema.TableColumn(schema);
                colvarOrderId.ColumnName = "order_id";
                colvarOrderId.DataType = DbType.AnsiString;
                colvarOrderId.MaxLength = 30;
                colvarOrderId.AutoIncrement = false;
                colvarOrderId.IsNullable = true;
                colvarOrderId.IsPrimaryKey = false;
                colvarOrderId.IsForeignKey = false;
                colvarOrderId.IsReadOnly = false;
                colvarOrderId.DefaultSetting = @"";
                colvarOrderId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarOrderId);

                TableSchema.TableColumn colvarRedoFlag = new TableSchema.TableColumn(schema);
                colvarRedoFlag.ColumnName = "redo_flag";
                colvarRedoFlag.DataType = DbType.Int32;
                colvarRedoFlag.MaxLength = 0;
                colvarRedoFlag.AutoIncrement = false;
                colvarRedoFlag.IsNullable = true;
                colvarRedoFlag.IsPrimaryKey = false;
                colvarRedoFlag.IsForeignKey = false;
                colvarRedoFlag.IsReadOnly = false;
                colvarRedoFlag.DefaultSetting = @"";
                colvarRedoFlag.ForeignKeyTableName = "";
                schema.Columns.Add(colvarRedoFlag);

                TableSchema.TableColumn colvarRedoTime = new TableSchema.TableColumn(schema);
                colvarRedoTime.ColumnName = "redo_time";
                colvarRedoTime.DataType = DbType.DateTime;
                colvarRedoTime.MaxLength = 0;
                colvarRedoTime.AutoIncrement = false;
                colvarRedoTime.IsNullable = true;
                colvarRedoTime.IsPrimaryKey = false;
                colvarRedoTime.IsForeignKey = false;
                colvarRedoTime.IsReadOnly = false;
                colvarRedoTime.DefaultSetting = @"";
                colvarRedoTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarRedoTime);

                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("mohist_verify_info", schema);
            }
        }
        #endregion

        #region Props

        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id
        {
            get { return GetColumnValue<int>(Columns.Id); }
            set { SetColumnValue(Columns.Id, value); }
        }

        [XmlAttribute("MohistDealsId")]
        [Bindable(true)]
        public string MohistDealsId
        {
            get { return GetColumnValue<string>(Columns.MohistDealsId); }
            set { SetColumnValue(Columns.MohistDealsId, value); }
        }

        [XmlAttribute("OrderDate")]
        [Bindable(true)]
        public DateTime OrderDate
        {
            get { return GetColumnValue<DateTime>(Columns.OrderDate); }
            set { SetColumnValue(Columns.OrderDate, value); }
        }

        [XmlAttribute("MohistProductId")]
        [Bindable(true)]
        public string MohistProductId
        {
            get { return GetColumnValue<string>(Columns.MohistProductId); }
            set { SetColumnValue(Columns.MohistProductId, value); }
        }

        [XmlAttribute("ItemPrice")]
        [Bindable(true)]
        public decimal ItemPrice
        {
            get { return GetColumnValue<decimal>(Columns.ItemPrice); }
            set { SetColumnValue(Columns.ItemPrice, value); }
        }

        [XmlAttribute("Amount")]
        [Bindable(true)]
        public decimal Amount
        {
            get { return GetColumnValue<decimal>(Columns.Amount); }
            set { SetColumnValue(Columns.Amount, value); }
        }

        [XmlAttribute("Bonus")]
        [Bindable(true)]
        public decimal Bonus
        {
            get { return GetColumnValue<decimal>(Columns.Bonus); }
            set { SetColumnValue(Columns.Bonus, value); }
        }

        [XmlAttribute("Ecash")]
        [Bindable(true)]
        public decimal Ecash
        {
            get { return GetColumnValue<decimal>(Columns.Ecash); }
            set { SetColumnValue(Columns.Ecash, value); }
        }

        [XmlAttribute("AuthCode")]
        [Bindable(true)]
        public string AuthCode
        {
            get { return GetColumnValue<string>(Columns.AuthCode); }
            set { SetColumnValue(Columns.AuthCode, value); }
        }

        [XmlAttribute("CouponId")]
        [Bindable(true)]
        public string CouponId
        {
            get { return GetColumnValue<string>(Columns.CouponId); }
            set { SetColumnValue(Columns.CouponId, value); }
        }

        [XmlAttribute("SequenceNumber")]
        [Bindable(true)]
        public string SequenceNumber
        {
            get { return GetColumnValue<string>(Columns.SequenceNumber); }
            set { SetColumnValue(Columns.SequenceNumber, value); }
        }

        [XmlAttribute("Barcode")]
        [Bindable(true)]
        public string Barcode
        {
            get { return GetColumnValue<string>(Columns.Barcode); }
            set { SetColumnValue(Columns.Barcode, value); }
        }

        [XmlAttribute("CouponCode")]
        [Bindable(true)]
        public string CouponCode
        {
            get { return GetColumnValue<string>(Columns.CouponCode); }
            set { SetColumnValue(Columns.CouponCode, value); }
        }

        [XmlAttribute("Status")]
        [Bindable(true)]
        public int Status
        {
            get { return GetColumnValue<int>(Columns.Status); }
            set { SetColumnValue(Columns.Status, value); }
        }

        [XmlAttribute("VerifyType")]
        [Bindable(true)]
        public int VerifyType
        {
            get { return GetColumnValue<int>(Columns.VerifyType); }
            set { SetColumnValue(Columns.VerifyType, value); }
        }

        [XmlAttribute("ModifyTime")]
        [Bindable(true)]
        public DateTime? ModifyTime
        {
            get { return GetColumnValue<DateTime?>(Columns.ModifyTime); }
            set { SetColumnValue(Columns.ModifyTime, value); }
        }

        [XmlAttribute("SpecialStatus")]
        [Bindable(true)]
        public int? SpecialStatus
        {
            get { return GetColumnValue<int?>(Columns.SpecialStatus); }
            set { SetColumnValue(Columns.SpecialStatus, value); }
        }

        [XmlAttribute("SpecialTime")]
        [Bindable(true)]
        public DateTime? SpecialTime
        {
            get { return GetColumnValue<DateTime?>(Columns.SpecialTime); }
            set { SetColumnValue(Columns.SpecialTime, value); }
        }

        [XmlAttribute("IsWebserviceVerify")]
        [Bindable(true)]
        public bool IsWebserviceVerify
        {
            get { return GetColumnValue<bool>(Columns.IsWebserviceVerify); }
            set { SetColumnValue(Columns.IsWebserviceVerify, value); }
        }

        [XmlAttribute("Memo")]
        [Bindable(true)]
        public string Memo
        {
            get { return GetColumnValue<string>(Columns.Memo); }
            set { SetColumnValue(Columns.Memo, value); }
        }

        [XmlAttribute("OrderId")]
        [Bindable(true)]
        public string OrderId
        {
            get { return GetColumnValue<string>(Columns.OrderId); }
            set { SetColumnValue(Columns.OrderId, value); }
        }

        [XmlAttribute("RedoFlag")]
        [Bindable(true)]
        public int? RedoFlag
        {
            get { return GetColumnValue<int?>(Columns.RedoFlag); }
            set { SetColumnValue(Columns.RedoFlag, value); }
        }

        [XmlAttribute("RedoTime")]
        [Bindable(true)]
        public DateTime? RedoTime
        {
            get { return GetColumnValue<DateTime?>(Columns.RedoTime); }
            set { SetColumnValue(Columns.RedoTime, value); }
        }

        #endregion




        //no foreign key tables defined (0)



        //no ManyToMany tables defined (0)





        #region Typed Columns


        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }



        public static TableSchema.TableColumn MohistDealsIdColumn
        {
            get { return Schema.Columns[1]; }
        }



        public static TableSchema.TableColumn OrderDateColumn
        {
            get { return Schema.Columns[2]; }
        }



        public static TableSchema.TableColumn MohistProductIdColumn
        {
            get { return Schema.Columns[3]; }
        }



        public static TableSchema.TableColumn ItemPriceColumn
        {
            get { return Schema.Columns[4]; }
        }



        public static TableSchema.TableColumn AmountColumn
        {
            get { return Schema.Columns[5]; }
        }



        public static TableSchema.TableColumn BonusColumn
        {
            get { return Schema.Columns[6]; }
        }



        public static TableSchema.TableColumn EcashColumn
        {
            get { return Schema.Columns[7]; }
        }



        public static TableSchema.TableColumn AuthCodeColumn
        {
            get { return Schema.Columns[8]; }
        }



        public static TableSchema.TableColumn CouponIdColumn
        {
            get { return Schema.Columns[9]; }
        }



        public static TableSchema.TableColumn SequenceNumberColumn
        {
            get { return Schema.Columns[10]; }
        }



        public static TableSchema.TableColumn BarcodeColumn
        {
            get { return Schema.Columns[11]; }
        }



        public static TableSchema.TableColumn CouponCodeColumn
        {
            get { return Schema.Columns[12]; }
        }



        public static TableSchema.TableColumn StatusColumn
        {
            get { return Schema.Columns[13]; }
        }



        public static TableSchema.TableColumn VerifyTypeColumn
        {
            get { return Schema.Columns[14]; }
        }



        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[15]; }
        }



        public static TableSchema.TableColumn SpecialStatusColumn
        {
            get { return Schema.Columns[16]; }
        }



        public static TableSchema.TableColumn SpecialTimeColumn
        {
            get { return Schema.Columns[17]; }
        }



        public static TableSchema.TableColumn IsWebserviceVerifyColumn
        {
            get { return Schema.Columns[18]; }
        }



        public static TableSchema.TableColumn MemoColumn
        {
            get { return Schema.Columns[19]; }
        }



        public static TableSchema.TableColumn OrderIdColumn
        {
            get { return Schema.Columns[20]; }
        }



        public static TableSchema.TableColumn RedoFlagColumn
        {
            get { return Schema.Columns[21]; }
        }



        public static TableSchema.TableColumn RedoTimeColumn
        {
            get { return Schema.Columns[22]; }
        }



        #endregion
        #region Columns Struct
        public struct Columns
        {
            public static string Id = @"id";
            public static string MohistDealsId = @"mohist_deals_id";
            public static string OrderDate = @"order_date";
            public static string MohistProductId = @"mohist_product_id";
            public static string ItemPrice = @"item_price";
            public static string Amount = @"amount";
            public static string Bonus = @"bonus";
            public static string Ecash = @"ecash";
            public static string AuthCode = @"auth_code";
            public static string CouponId = @"coupon_id";
            public static string SequenceNumber = @"sequence_number";
            public static string Barcode = @"barcode";
            public static string CouponCode = @"coupon_code";
            public static string Status = @"status";
            public static string VerifyType = @"verify_type";
            public static string ModifyTime = @"modify_time";
            public static string SpecialStatus = @"special_status";
            public static string SpecialTime = @"special_time";
            public static string IsWebserviceVerify = @"is_webservice_verify";
            public static string Memo = @"memo";
            public static string OrderId = @"order_id";
            public static string RedoFlag = @"redo_flag";
            public static string RedoTime = @"redo_time";

        }
        #endregion

        #region Update PK Collections

        #endregion

        #region Deep Save

        #endregion
    }
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewPponDealStoreChannel class.
    /// </summary>
    [Serializable]
    public partial class ViewPponDealStoreChannelCollection : ReadOnlyList<ViewPponDealStoreChannel, ViewPponDealStoreChannelCollection>
    {        
        public ViewPponDealStoreChannelCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_ppon_deal_store_channel view.
    /// </summary>
    [Serializable]
    public partial class ViewPponDealStoreChannel : ReadOnlyRecord<ViewPponDealStoreChannel>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_ppon_deal_store_channel", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarBid = new TableSchema.TableColumn(schema);
                colvarBid.ColumnName = "bid";
                colvarBid.DataType = DbType.Guid;
                colvarBid.MaxLength = 0;
                colvarBid.AutoIncrement = false;
                colvarBid.IsNullable = false;
                colvarBid.IsPrimaryKey = false;
                colvarBid.IsForeignKey = false;
                colvarBid.IsReadOnly = false;
                
                schema.Columns.Add(colvarBid);
                
                TableSchema.TableColumn colvarCid = new TableSchema.TableColumn(schema);
                colvarCid.ColumnName = "cid";
                colvarCid.DataType = DbType.Int32;
                colvarCid.MaxLength = 0;
                colvarCid.AutoIncrement = false;
                colvarCid.IsNullable = false;
                colvarCid.IsPrimaryKey = false;
                colvarCid.IsForeignKey = false;
                colvarCid.IsReadOnly = false;
                
                schema.Columns.Add(colvarCid);
                
                TableSchema.TableColumn colvarName = new TableSchema.TableColumn(schema);
                colvarName.ColumnName = "name";
                colvarName.DataType = DbType.String;
                colvarName.MaxLength = 50;
                colvarName.AutoIncrement = false;
                colvarName.IsNullable = true;
                colvarName.IsPrimaryKey = false;
                colvarName.IsForeignKey = false;
                colvarName.IsReadOnly = false;
                
                schema.Columns.Add(colvarName);
                
                TableSchema.TableColumn colvarRank = new TableSchema.TableColumn(schema);
                colvarRank.ColumnName = "rank";
                colvarRank.DataType = DbType.Int32;
                colvarRank.MaxLength = 0;
                colvarRank.AutoIncrement = false;
                colvarRank.IsNullable = false;
                colvarRank.IsPrimaryKey = false;
                colvarRank.IsForeignKey = false;
                colvarRank.IsReadOnly = false;
                
                schema.Columns.Add(colvarRank);
                
                TableSchema.TableColumn colvarType = new TableSchema.TableColumn(schema);
                colvarType.ColumnName = "type";
                colvarType.DataType = DbType.Int32;
                colvarType.MaxLength = 0;
                colvarType.AutoIncrement = false;
                colvarType.IsNullable = true;
                colvarType.IsPrimaryKey = false;
                colvarType.IsForeignKey = false;
                colvarType.IsReadOnly = false;
                
                schema.Columns.Add(colvarType);
                
                TableSchema.TableColumn colvarCityId = new TableSchema.TableColumn(schema);
                colvarCityId.ColumnName = "city_id";
                colvarCityId.DataType = DbType.Int32;
                colvarCityId.MaxLength = 0;
                colvarCityId.AutoIncrement = false;
                colvarCityId.IsNullable = false;
                colvarCityId.IsPrimaryKey = false;
                colvarCityId.IsForeignKey = false;
                colvarCityId.IsReadOnly = false;
                
                schema.Columns.Add(colvarCityId);
                
                TableSchema.TableColumn colvarSequence = new TableSchema.TableColumn(schema);
                colvarSequence.ColumnName = "sequence";
                colvarSequence.DataType = DbType.Int32;
                colvarSequence.MaxLength = 0;
                colvarSequence.AutoIncrement = false;
                colvarSequence.IsNullable = false;
                colvarSequence.IsPrimaryKey = false;
                colvarSequence.IsForeignKey = false;
                colvarSequence.IsReadOnly = false;
                
                schema.Columns.Add(colvarSequence);
                
                TableSchema.TableColumn colvarBusinessHourOrderTimeS = new TableSchema.TableColumn(schema);
                colvarBusinessHourOrderTimeS.ColumnName = "business_hour_order_time_s";
                colvarBusinessHourOrderTimeS.DataType = DbType.DateTime;
                colvarBusinessHourOrderTimeS.MaxLength = 0;
                colvarBusinessHourOrderTimeS.AutoIncrement = false;
                colvarBusinessHourOrderTimeS.IsNullable = false;
                colvarBusinessHourOrderTimeS.IsPrimaryKey = false;
                colvarBusinessHourOrderTimeS.IsForeignKey = false;
                colvarBusinessHourOrderTimeS.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourOrderTimeS);
                
                TableSchema.TableColumn colvarAppDealPic = new TableSchema.TableColumn(schema);
                colvarAppDealPic.ColumnName = "app_deal_pic";
                colvarAppDealPic.DataType = DbType.String;
                colvarAppDealPic.MaxLength = 200;
                colvarAppDealPic.AutoIncrement = false;
                colvarAppDealPic.IsNullable = true;
                colvarAppDealPic.IsPrimaryKey = false;
                colvarAppDealPic.IsForeignKey = false;
                colvarAppDealPic.IsReadOnly = false;
                
                schema.Columns.Add(colvarAppDealPic);
                
                TableSchema.TableColumn colvarAppDealStyle = new TableSchema.TableColumn(schema);
                colvarAppDealStyle.ColumnName = "app_deal_style";
                colvarAppDealStyle.DataType = DbType.Int32;
                colvarAppDealStyle.MaxLength = 0;
                colvarAppDealStyle.AutoIncrement = false;
                colvarAppDealStyle.IsNullable = true;
                colvarAppDealStyle.IsPrimaryKey = false;
                colvarAppDealStyle.IsForeignKey = false;
                colvarAppDealStyle.IsReadOnly = false;
                
                schema.Columns.Add(colvarAppDealStyle);
                
                TableSchema.TableColumn colvarSellerGuid = new TableSchema.TableColumn(schema);
                colvarSellerGuid.ColumnName = "seller_GUID";
                colvarSellerGuid.DataType = DbType.Guid;
                colvarSellerGuid.MaxLength = 0;
                colvarSellerGuid.AutoIncrement = false;
                colvarSellerGuid.IsNullable = false;
                colvarSellerGuid.IsPrimaryKey = false;
                colvarSellerGuid.IsForeignKey = false;
                colvarSellerGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerGuid);
                
                TableSchema.TableColumn colvarBuilding = new TableSchema.TableColumn(schema);
                colvarBuilding.ColumnName = "building";
                colvarBuilding.DataType = DbType.String;
                colvarBuilding.MaxLength = 50;
                colvarBuilding.AutoIncrement = false;
                colvarBuilding.IsNullable = true;
                colvarBuilding.IsPrimaryKey = false;
                colvarBuilding.IsForeignKey = false;
                colvarBuilding.IsReadOnly = false;
                
                schema.Columns.Add(colvarBuilding);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_ppon_deal_store_channel",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewPponDealStoreChannel()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewPponDealStoreChannel(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewPponDealStoreChannel(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewPponDealStoreChannel(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("Bid")]
        [Bindable(true)]
        public Guid Bid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("bid");
		    }
            set 
		    {
			    SetColumnValue("bid", value);
            }
        }
	      
        [XmlAttribute("Cid")]
        [Bindable(true)]
        public int Cid 
	    {
		    get
		    {
			    return GetColumnValue<int>("cid");
		    }
            set 
		    {
			    SetColumnValue("cid", value);
            }
        }
	      
        [XmlAttribute("Name")]
        [Bindable(true)]
        public string Name 
	    {
		    get
		    {
			    return GetColumnValue<string>("name");
		    }
            set 
		    {
			    SetColumnValue("name", value);
            }
        }
	      
        [XmlAttribute("Rank")]
        [Bindable(true)]
        public int Rank 
	    {
		    get
		    {
			    return GetColumnValue<int>("rank");
		    }
            set 
		    {
			    SetColumnValue("rank", value);
            }
        }
	      
        [XmlAttribute("Type")]
        [Bindable(true)]
        public int? Type 
	    {
		    get
		    {
			    return GetColumnValue<int?>("type");
		    }
            set 
		    {
			    SetColumnValue("type", value);
            }
        }
	      
        [XmlAttribute("CityId")]
        [Bindable(true)]
        public int CityId 
	    {
		    get
		    {
			    return GetColumnValue<int>("city_id");
		    }
            set 
		    {
			    SetColumnValue("city_id", value);
            }
        }
	      
        [XmlAttribute("Sequence")]
        [Bindable(true)]
        public int Sequence 
	    {
		    get
		    {
			    return GetColumnValue<int>("sequence");
		    }
            set 
		    {
			    SetColumnValue("sequence", value);
            }
        }
	      
        [XmlAttribute("BusinessHourOrderTimeS")]
        [Bindable(true)]
        public DateTime BusinessHourOrderTimeS 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("business_hour_order_time_s");
		    }
            set 
		    {
			    SetColumnValue("business_hour_order_time_s", value);
            }
        }
	      
        [XmlAttribute("AppDealPic")]
        [Bindable(true)]
        public string AppDealPic 
	    {
		    get
		    {
			    return GetColumnValue<string>("app_deal_pic");
		    }
            set 
		    {
			    SetColumnValue("app_deal_pic", value);
            }
        }
	      
        [XmlAttribute("AppDealStyle")]
        [Bindable(true)]
        public int? AppDealStyle 
	    {
		    get
		    {
			    return GetColumnValue<int?>("app_deal_style");
		    }
            set 
		    {
			    SetColumnValue("app_deal_style", value);
            }
        }
	      
        [XmlAttribute("SellerGuid")]
        [Bindable(true)]
        public Guid SellerGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("seller_GUID");
		    }
            set 
		    {
			    SetColumnValue("seller_GUID", value);
            }
        }
	      
        [XmlAttribute("Building")]
        [Bindable(true)]
        public string Building 
	    {
		    get
		    {
			    return GetColumnValue<string>("building");
		    }
            set 
		    {
			    SetColumnValue("building", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string Bid = @"bid";
            
            public static string Cid = @"cid";
            
            public static string Name = @"name";
            
            public static string Rank = @"rank";
            
            public static string Type = @"type";
            
            public static string CityId = @"city_id";
            
            public static string Sequence = @"sequence";
            
            public static string BusinessHourOrderTimeS = @"business_hour_order_time_s";
            
            public static string AppDealPic = @"app_deal_pic";
            
            public static string AppDealStyle = @"app_deal_style";
            
            public static string SellerGuid = @"seller_GUID";
            
            public static string Building = @"building";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewMemberCollectDealContent class.
    /// </summary>
    [Serializable]
    public partial class ViewMemberCollectDealContentCollection : ReadOnlyList<ViewMemberCollectDealContent, ViewMemberCollectDealContentCollection>
    {        
        public ViewMemberCollectDealContentCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_member_collect_deal_content view.
    /// </summary>
    [Serializable]
    public partial class ViewMemberCollectDealContent : ReadOnlyRecord<ViewMemberCollectDealContent>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_member_collect_deal_content", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarUserId = new TableSchema.TableColumn(schema);
                colvarUserId.ColumnName = "user_id";
                colvarUserId.DataType = DbType.Int32;
                colvarUserId.MaxLength = 0;
                colvarUserId.AutoIncrement = false;
                colvarUserId.IsNullable = false;
                colvarUserId.IsPrimaryKey = false;
                colvarUserId.IsForeignKey = false;
                colvarUserId.IsReadOnly = false;
                
                schema.Columns.Add(colvarUserId);
                
                TableSchema.TableColumn colvarBusinessHourGuid = new TableSchema.TableColumn(schema);
                colvarBusinessHourGuid.ColumnName = "business_hour_guid";
                colvarBusinessHourGuid.DataType = DbType.Guid;
                colvarBusinessHourGuid.MaxLength = 0;
                colvarBusinessHourGuid.AutoIncrement = false;
                colvarBusinessHourGuid.IsNullable = false;
                colvarBusinessHourGuid.IsPrimaryKey = false;
                colvarBusinessHourGuid.IsForeignKey = false;
                colvarBusinessHourGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourGuid);
                
                TableSchema.TableColumn colvarAppNotice = new TableSchema.TableColumn(schema);
                colvarAppNotice.ColumnName = "app_notice";
                colvarAppNotice.DataType = DbType.Boolean;
                colvarAppNotice.MaxLength = 0;
                colvarAppNotice.AutoIncrement = false;
                colvarAppNotice.IsNullable = false;
                colvarAppNotice.IsPrimaryKey = false;
                colvarAppNotice.IsForeignKey = false;
                colvarAppNotice.IsReadOnly = false;
                
                schema.Columns.Add(colvarAppNotice);
                
                TableSchema.TableColumn colvarExpr1 = new TableSchema.TableColumn(schema);
                colvarExpr1.ColumnName = "Expr1";
                colvarExpr1.DataType = DbType.Guid;
                colvarExpr1.MaxLength = 0;
                colvarExpr1.AutoIncrement = false;
                colvarExpr1.IsNullable = false;
                colvarExpr1.IsPrimaryKey = false;
                colvarExpr1.IsForeignKey = false;
                colvarExpr1.IsReadOnly = false;
                
                schema.Columns.Add(colvarExpr1);
                
                TableSchema.TableColumn colvarBusinessHourOrderTimeE = new TableSchema.TableColumn(schema);
                colvarBusinessHourOrderTimeE.ColumnName = "business_hour_order_time_e";
                colvarBusinessHourOrderTimeE.DataType = DbType.DateTime;
                colvarBusinessHourOrderTimeE.MaxLength = 0;
                colvarBusinessHourOrderTimeE.AutoIncrement = false;
                colvarBusinessHourOrderTimeE.IsNullable = false;
                colvarBusinessHourOrderTimeE.IsPrimaryKey = false;
                colvarBusinessHourOrderTimeE.IsForeignKey = false;
                colvarBusinessHourOrderTimeE.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourOrderTimeE);
                
                TableSchema.TableColumn colvarBusinessHourOrderTimeS = new TableSchema.TableColumn(schema);
                colvarBusinessHourOrderTimeS.ColumnName = "business_hour_order_time_s";
                colvarBusinessHourOrderTimeS.DataType = DbType.DateTime;
                colvarBusinessHourOrderTimeS.MaxLength = 0;
                colvarBusinessHourOrderTimeS.AutoIncrement = false;
                colvarBusinessHourOrderTimeS.IsNullable = false;
                colvarBusinessHourOrderTimeS.IsPrimaryKey = false;
                colvarBusinessHourOrderTimeS.IsForeignKey = false;
                colvarBusinessHourOrderTimeS.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourOrderTimeS);
                
                TableSchema.TableColumn colvarImagePath = new TableSchema.TableColumn(schema);
                colvarImagePath.ColumnName = "image_path";
                colvarImagePath.DataType = DbType.String;
                colvarImagePath.MaxLength = 1000;
                colvarImagePath.AutoIncrement = false;
                colvarImagePath.IsNullable = true;
                colvarImagePath.IsPrimaryKey = false;
                colvarImagePath.IsForeignKey = false;
                colvarImagePath.IsReadOnly = false;
                
                schema.Columns.Add(colvarImagePath);
                
                TableSchema.TableColumn colvarPrice = new TableSchema.TableColumn(schema);
                colvarPrice.ColumnName = "price";
                colvarPrice.DataType = DbType.Currency;
                colvarPrice.MaxLength = 0;
                colvarPrice.AutoIncrement = false;
                colvarPrice.IsNullable = false;
                colvarPrice.IsPrimaryKey = false;
                colvarPrice.IsForeignKey = false;
                colvarPrice.IsReadOnly = false;
                
                schema.Columns.Add(colvarPrice);
                
                TableSchema.TableColumn colvarOrigPrice = new TableSchema.TableColumn(schema);
                colvarOrigPrice.ColumnName = "orig_price";
                colvarOrigPrice.DataType = DbType.Currency;
                colvarOrigPrice.MaxLength = 0;
                colvarOrigPrice.AutoIncrement = false;
                colvarOrigPrice.IsNullable = false;
                colvarOrigPrice.IsPrimaryKey = false;
                colvarOrigPrice.IsForeignKey = false;
                colvarOrigPrice.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrigPrice);
                
                TableSchema.TableColumn colvarCollectTime = new TableSchema.TableColumn(schema);
                colvarCollectTime.ColumnName = "collect_time";
                colvarCollectTime.DataType = DbType.DateTime;
                colvarCollectTime.MaxLength = 0;
                colvarCollectTime.AutoIncrement = false;
                colvarCollectTime.IsNullable = false;
                colvarCollectTime.IsPrimaryKey = false;
                colvarCollectTime.IsForeignKey = false;
                colvarCollectTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarCollectTime);
                
                TableSchema.TableColumn colvarAppTitle = new TableSchema.TableColumn(schema);
                colvarAppTitle.ColumnName = "app_title";
                colvarAppTitle.DataType = DbType.String;
                colvarAppTitle.MaxLength = 40;
                colvarAppTitle.AutoIncrement = false;
                colvarAppTitle.IsNullable = true;
                colvarAppTitle.IsPrimaryKey = false;
                colvarAppTitle.IsForeignKey = false;
                colvarAppTitle.IsReadOnly = false;
                
                schema.Columns.Add(colvarAppTitle);
                
                TableSchema.TableColumn colvarCouponUsage = new TableSchema.TableColumn(schema);
                colvarCouponUsage.ColumnName = "coupon_usage";
                colvarCouponUsage.DataType = DbType.String;
                colvarCouponUsage.MaxLength = 500;
                colvarCouponUsage.AutoIncrement = false;
                colvarCouponUsage.IsNullable = true;
                colvarCouponUsage.IsPrimaryKey = false;
                colvarCouponUsage.IsForeignKey = false;
                colvarCouponUsage.IsReadOnly = false;
                
                schema.Columns.Add(colvarCouponUsage);
                
                TableSchema.TableColumn colvarCollectStatus = new TableSchema.TableColumn(schema);
                colvarCollectStatus.ColumnName = "collect_status";
                colvarCollectStatus.DataType = DbType.Byte;
                colvarCollectStatus.MaxLength = 0;
                colvarCollectStatus.AutoIncrement = false;
                colvarCollectStatus.IsNullable = false;
                colvarCollectStatus.IsPrimaryKey = false;
                colvarCollectStatus.IsForeignKey = false;
                colvarCollectStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarCollectStatus);
                
                TableSchema.TableColumn colvarGroupOrderStatus = new TableSchema.TableColumn(schema);
                colvarGroupOrderStatus.ColumnName = "group_order_status";
                colvarGroupOrderStatus.DataType = DbType.Int32;
                colvarGroupOrderStatus.MaxLength = 0;
                colvarGroupOrderStatus.AutoIncrement = false;
                colvarGroupOrderStatus.IsNullable = true;
                colvarGroupOrderStatus.IsPrimaryKey = false;
                colvarGroupOrderStatus.IsForeignKey = false;
                colvarGroupOrderStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarGroupOrderStatus);
                
                TableSchema.TableColumn colvarExchangePrice = new TableSchema.TableColumn(schema);
                colvarExchangePrice.ColumnName = "exchange_price";
                colvarExchangePrice.DataType = DbType.Decimal;
                colvarExchangePrice.MaxLength = 0;
                colvarExchangePrice.AutoIncrement = false;
                colvarExchangePrice.IsNullable = false;
                colvarExchangePrice.IsPrimaryKey = false;
                colvarExchangePrice.IsForeignKey = false;
                colvarExchangePrice.IsReadOnly = false;
                
                schema.Columns.Add(colvarExchangePrice);
                
                TableSchema.TableColumn colvarCollectType = new TableSchema.TableColumn(schema);
                colvarCollectType.ColumnName = "collect_type";
                colvarCollectType.DataType = DbType.Byte;
                colvarCollectType.MaxLength = 0;
                colvarCollectType.AutoIncrement = false;
                colvarCollectType.IsNullable = false;
                colvarCollectType.IsPrimaryKey = false;
                colvarCollectType.IsForeignKey = false;
                colvarCollectType.IsReadOnly = false;
                
                schema.Columns.Add(colvarCollectType);
                
                TableSchema.TableColumn colvarBusinessHourStatus = new TableSchema.TableColumn(schema);
                colvarBusinessHourStatus.ColumnName = "business_hour_status";
                colvarBusinessHourStatus.DataType = DbType.Int32;
                colvarBusinessHourStatus.MaxLength = 0;
                colvarBusinessHourStatus.AutoIncrement = false;
                colvarBusinessHourStatus.IsNullable = false;
                colvarBusinessHourStatus.IsPrimaryKey = false;
                colvarBusinessHourStatus.IsForeignKey = false;
                colvarBusinessHourStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourStatus);
                
                TableSchema.TableColumn colvarDiscountType = new TableSchema.TableColumn(schema);
                colvarDiscountType.ColumnName = "discount_type";
                colvarDiscountType.DataType = DbType.Int32;
                colvarDiscountType.MaxLength = 0;
                colvarDiscountType.AutoIncrement = false;
                colvarDiscountType.IsNullable = false;
                colvarDiscountType.IsPrimaryKey = false;
                colvarDiscountType.IsForeignKey = false;
                colvarDiscountType.IsReadOnly = false;
                
                schema.Columns.Add(colvarDiscountType);
                
                TableSchema.TableColumn colvarDiscount = new TableSchema.TableColumn(schema);
                colvarDiscount.ColumnName = "discount";
                colvarDiscount.DataType = DbType.Int32;
                colvarDiscount.MaxLength = 0;
                colvarDiscount.AutoIncrement = false;
                colvarDiscount.IsNullable = false;
                colvarDiscount.IsPrimaryKey = false;
                colvarDiscount.IsForeignKey = false;
                colvarDiscount.IsReadOnly = false;
                
                schema.Columns.Add(colvarDiscount);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_member_collect_deal_content",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewMemberCollectDealContent()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewMemberCollectDealContent(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewMemberCollectDealContent(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewMemberCollectDealContent(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("UserId")]
        [Bindable(true)]
        public int UserId 
	    {
		    get
		    {
			    return GetColumnValue<int>("user_id");
		    }
            set 
		    {
			    SetColumnValue("user_id", value);
            }
        }
	      
        [XmlAttribute("BusinessHourGuid")]
        [Bindable(true)]
        public Guid BusinessHourGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("business_hour_guid");
		    }
            set 
		    {
			    SetColumnValue("business_hour_guid", value);
            }
        }
	      
        [XmlAttribute("AppNotice")]
        [Bindable(true)]
        public bool AppNotice 
	    {
		    get
		    {
			    return GetColumnValue<bool>("app_notice");
		    }
            set 
		    {
			    SetColumnValue("app_notice", value);
            }
        }
	      
        [XmlAttribute("Expr1")]
        [Bindable(true)]
        public Guid Expr1 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("Expr1");
		    }
            set 
		    {
			    SetColumnValue("Expr1", value);
            }
        }
	      
        [XmlAttribute("BusinessHourOrderTimeE")]
        [Bindable(true)]
        public DateTime BusinessHourOrderTimeE 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("business_hour_order_time_e");
		    }
            set 
		    {
			    SetColumnValue("business_hour_order_time_e", value);
            }
        }
	      
        [XmlAttribute("BusinessHourOrderTimeS")]
        [Bindable(true)]
        public DateTime BusinessHourOrderTimeS 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("business_hour_order_time_s");
		    }
            set 
		    {
			    SetColumnValue("business_hour_order_time_s", value);
            }
        }
	      
        [XmlAttribute("ImagePath")]
        [Bindable(true)]
        public string ImagePath 
	    {
		    get
		    {
			    return GetColumnValue<string>("image_path");
		    }
            set 
		    {
			    SetColumnValue("image_path", value);
            }
        }
	      
        [XmlAttribute("Price")]
        [Bindable(true)]
        public decimal Price 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("price");
		    }
            set 
		    {
			    SetColumnValue("price", value);
            }
        }
	      
        [XmlAttribute("OrigPrice")]
        [Bindable(true)]
        public decimal OrigPrice 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("orig_price");
		    }
            set 
		    {
			    SetColumnValue("orig_price", value);
            }
        }
	      
        [XmlAttribute("CollectTime")]
        [Bindable(true)]
        public DateTime CollectTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("collect_time");
		    }
            set 
		    {
			    SetColumnValue("collect_time", value);
            }
        }
	      
        [XmlAttribute("AppTitle")]
        [Bindable(true)]
        public string AppTitle 
	    {
		    get
		    {
			    return GetColumnValue<string>("app_title");
		    }
            set 
		    {
			    SetColumnValue("app_title", value);
            }
        }
	      
        [XmlAttribute("CouponUsage")]
        [Bindable(true)]
        public string CouponUsage 
	    {
		    get
		    {
			    return GetColumnValue<string>("coupon_usage");
		    }
            set 
		    {
			    SetColumnValue("coupon_usage", value);
            }
        }
	      
        [XmlAttribute("CollectStatus")]
        [Bindable(true)]
        public byte CollectStatus 
	    {
		    get
		    {
			    return GetColumnValue<byte>("collect_status");
		    }
            set 
		    {
			    SetColumnValue("collect_status", value);
            }
        }
	      
        [XmlAttribute("GroupOrderStatus")]
        [Bindable(true)]
        public int? GroupOrderStatus 
	    {
		    get
		    {
			    return GetColumnValue<int?>("group_order_status");
		    }
            set 
		    {
			    SetColumnValue("group_order_status", value);
            }
        }
	      
        [XmlAttribute("ExchangePrice")]
        [Bindable(true)]
        public decimal ExchangePrice 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("exchange_price");
		    }
            set 
		    {
			    SetColumnValue("exchange_price", value);
            }
        }
	      
        [XmlAttribute("CollectType")]
        [Bindable(true)]
        public byte CollectType 
	    {
		    get
		    {
			    return GetColumnValue<byte>("collect_type");
		    }
            set 
		    {
			    SetColumnValue("collect_type", value);
            }
        }
	      
        [XmlAttribute("BusinessHourStatus")]
        [Bindable(true)]
        public int BusinessHourStatus 
	    {
		    get
		    {
			    return GetColumnValue<int>("business_hour_status");
		    }
            set 
		    {
			    SetColumnValue("business_hour_status", value);
            }
        }
	      
        [XmlAttribute("DiscountType")]
        [Bindable(true)]
        public int DiscountType 
	    {
		    get
		    {
			    return GetColumnValue<int>("discount_type");
		    }
            set 
		    {
			    SetColumnValue("discount_type", value);
            }
        }
	      
        [XmlAttribute("Discount")]
        [Bindable(true)]
        public int Discount 
	    {
		    get
		    {
			    return GetColumnValue<int>("discount");
		    }
            set 
		    {
			    SetColumnValue("discount", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string UserId = @"user_id";
            
            public static string BusinessHourGuid = @"business_hour_guid";
            
            public static string AppNotice = @"app_notice";
            
            public static string Expr1 = @"Expr1";
            
            public static string BusinessHourOrderTimeE = @"business_hour_order_time_e";
            
            public static string BusinessHourOrderTimeS = @"business_hour_order_time_s";
            
            public static string ImagePath = @"image_path";
            
            public static string Price = @"price";
            
            public static string OrigPrice = @"orig_price";
            
            public static string CollectTime = @"collect_time";
            
            public static string AppTitle = @"app_title";
            
            public static string CouponUsage = @"coupon_usage";
            
            public static string CollectStatus = @"collect_status";
            
            public static string GroupOrderStatus = @"group_order_status";
            
            public static string ExchangePrice = @"exchange_price";
            
            public static string CollectType = @"collect_type";
            
            public static string BusinessHourStatus = @"business_hour_status";
            
            public static string DiscountType = @"discount_type";
            
            public static string Discount = @"discount";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

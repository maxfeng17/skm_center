using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the HotDealConfig class.
	/// </summary>
    [Serializable]
	public partial class HotDealConfigCollection : RepositoryList<HotDealConfig, HotDealConfigCollection>
	{	   
		public HotDealConfigCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>HotDealConfigCollection</returns>
		public HotDealConfigCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                HotDealConfig o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the hot_deal_config table.
	/// </summary>
	[Serializable]
	public partial class HotDealConfig : RepositoryRecord<HotDealConfig>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public HotDealConfig()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public HotDealConfig(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("hot_deal_config", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarCityId = new TableSchema.TableColumn(schema);
				colvarCityId.ColumnName = "city_id";
				colvarCityId.DataType = DbType.Int32;
				colvarCityId.MaxLength = 0;
				colvarCityId.AutoIncrement = false;
				colvarCityId.IsNullable = false;
				colvarCityId.IsPrimaryKey = true;
				colvarCityId.IsForeignKey = false;
				colvarCityId.IsReadOnly = false;
				colvarCityId.DefaultSetting = @"";
				colvarCityId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCityId);
				
				TableSchema.TableColumn colvarTopN = new TableSchema.TableColumn(schema);
				colvarTopN.ColumnName = "top_n";
				colvarTopN.DataType = DbType.Int32;
				colvarTopN.MaxLength = 0;
				colvarTopN.AutoIncrement = false;
				colvarTopN.IsNullable = false;
				colvarTopN.IsPrimaryKey = false;
				colvarTopN.IsForeignKey = false;
				colvarTopN.IsReadOnly = false;
				colvarTopN.DefaultSetting = @"";
				colvarTopN.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTopN);
				
				TableSchema.TableColumn colvarNMultiple = new TableSchema.TableColumn(schema);
				colvarNMultiple.ColumnName = "n_multiple";
				colvarNMultiple.DataType = DbType.Int32;
				colvarNMultiple.MaxLength = 0;
				colvarNMultiple.AutoIncrement = false;
				colvarNMultiple.IsNullable = false;
				colvarNMultiple.IsPrimaryKey = false;
				colvarNMultiple.IsForeignKey = false;
				colvarNMultiple.IsReadOnly = false;
				colvarNMultiple.DefaultSetting = @"";
				colvarNMultiple.ForeignKeyTableName = "";
				schema.Columns.Add(colvarNMultiple);
				
				TableSchema.TableColumn colvarNDays = new TableSchema.TableColumn(schema);
				colvarNDays.ColumnName = "n_days";
				colvarNDays.DataType = DbType.Int32;
				colvarNDays.MaxLength = 0;
				colvarNDays.AutoIncrement = false;
				colvarNDays.IsNullable = false;
				colvarNDays.IsPrimaryKey = false;
				colvarNDays.IsForeignKey = false;
				colvarNDays.IsReadOnly = false;
				colvarNDays.DefaultSetting = @"";
				colvarNDays.ForeignKeyTableName = "";
				schema.Columns.Add(colvarNDays);
				
				TableSchema.TableColumn colvarEnable = new TableSchema.TableColumn(schema);
				colvarEnable.ColumnName = "enable";
				colvarEnable.DataType = DbType.Boolean;
				colvarEnable.MaxLength = 0;
				colvarEnable.AutoIncrement = false;
				colvarEnable.IsNullable = false;
				colvarEnable.IsPrimaryKey = false;
				colvarEnable.IsForeignKey = false;
				colvarEnable.IsReadOnly = false;
				colvarEnable.DefaultSetting = @"";
				colvarEnable.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEnable);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("hot_deal_config",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("CityId")]
		[Bindable(true)]
		public int CityId 
		{
			get { return GetColumnValue<int>(Columns.CityId); }
			set { SetColumnValue(Columns.CityId, value); }
		}
		  
		[XmlAttribute("TopN")]
		[Bindable(true)]
		public int TopN 
		{
			get { return GetColumnValue<int>(Columns.TopN); }
			set { SetColumnValue(Columns.TopN, value); }
		}
		  
		[XmlAttribute("NMultiple")]
		[Bindable(true)]
		public int NMultiple 
		{
			get { return GetColumnValue<int>(Columns.NMultiple); }
			set { SetColumnValue(Columns.NMultiple, value); }
		}
		  
		[XmlAttribute("NDays")]
		[Bindable(true)]
		public int NDays 
		{
			get { return GetColumnValue<int>(Columns.NDays); }
			set { SetColumnValue(Columns.NDays, value); }
		}
		  
		[XmlAttribute("Enable")]
		[Bindable(true)]
		public bool Enable 
		{
			get { return GetColumnValue<bool>(Columns.Enable); }
			set { SetColumnValue(Columns.Enable, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn CityIdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn TopNColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn NMultipleColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn NDaysColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn EnableColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string CityId = @"city_id";
			 public static string TopN = @"top_n";
			 public static string NMultiple = @"n_multiple";
			 public static string NDays = @"n_days";
			 public static string Enable = @"enable";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

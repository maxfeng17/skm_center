using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the HiDealSubscriptionPromoSlot class.
	/// </summary>
    [Serializable]
	public partial class HiDealSubscriptionPromoSlotCollection : RepositoryList<HiDealSubscriptionPromoSlot, HiDealSubscriptionPromoSlotCollection>
	{	   
		public HiDealSubscriptionPromoSlotCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>HiDealSubscriptionPromoSlotCollection</returns>
		public HiDealSubscriptionPromoSlotCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                HiDealSubscriptionPromoSlot o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the hi_deal_subscription_promo_slot table.
	/// </summary>
	[Serializable]
	public partial class HiDealSubscriptionPromoSlot : RepositoryRecord<HiDealSubscriptionPromoSlot>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public HiDealSubscriptionPromoSlot()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public HiDealSubscriptionPromoSlot(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("hi_deal_subscription_promo_slot", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarCodeGroup = new TableSchema.TableColumn(schema);
				colvarCodeGroup.ColumnName = "code_group";
				colvarCodeGroup.DataType = DbType.AnsiString;
				colvarCodeGroup.MaxLength = 50;
				colvarCodeGroup.AutoIncrement = false;
				colvarCodeGroup.IsNullable = false;
				colvarCodeGroup.IsPrimaryKey = false;
				colvarCodeGroup.IsForeignKey = false;
				colvarCodeGroup.IsReadOnly = false;
				colvarCodeGroup.DefaultSetting = @"";
				colvarCodeGroup.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCodeGroup);
				
				TableSchema.TableColumn colvarCodeId = new TableSchema.TableColumn(schema);
				colvarCodeId.ColumnName = "code_id";
				colvarCodeId.DataType = DbType.Int32;
				colvarCodeId.MaxLength = 0;
				colvarCodeId.AutoIncrement = false;
				colvarCodeId.IsNullable = false;
				colvarCodeId.IsPrimaryKey = false;
				colvarCodeId.IsForeignKey = false;
				colvarCodeId.IsReadOnly = false;
				colvarCodeId.DefaultSetting = @"";
				colvarCodeId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCodeId);
				
				TableSchema.TableColumn colvarPromoId = new TableSchema.TableColumn(schema);
				colvarPromoId.ColumnName = "promo_id";
				colvarPromoId.DataType = DbType.Int32;
				colvarPromoId.MaxLength = 0;
				colvarPromoId.AutoIncrement = false;
				colvarPromoId.IsNullable = false;
				colvarPromoId.IsPrimaryKey = false;
				colvarPromoId.IsForeignKey = false;
				colvarPromoId.IsReadOnly = false;
				colvarPromoId.DefaultSetting = @"";
				colvarPromoId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPromoId);
				
				TableSchema.TableColumn colvarPart = new TableSchema.TableColumn(schema);
				colvarPart.ColumnName = "part";
				colvarPart.DataType = DbType.Int32;
				colvarPart.MaxLength = 0;
				colvarPart.AutoIncrement = false;
				colvarPart.IsNullable = false;
				colvarPart.IsPrimaryKey = false;
				colvarPart.IsForeignKey = false;
				colvarPart.IsReadOnly = false;
				
						colvarPart.DefaultSetting = @"((0))";
				colvarPart.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPart);
				
				TableSchema.TableColumn colvarStartTime = new TableSchema.TableColumn(schema);
				colvarStartTime.ColumnName = "start_time";
				colvarStartTime.DataType = DbType.DateTime;
				colvarStartTime.MaxLength = 0;
				colvarStartTime.AutoIncrement = false;
				colvarStartTime.IsNullable = true;
				colvarStartTime.IsPrimaryKey = false;
				colvarStartTime.IsForeignKey = false;
				colvarStartTime.IsReadOnly = false;
				colvarStartTime.DefaultSetting = @"";
				colvarStartTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStartTime);
				
				TableSchema.TableColumn colvarEndTime = new TableSchema.TableColumn(schema);
				colvarEndTime.ColumnName = "end_time";
				colvarEndTime.DataType = DbType.DateTime;
				colvarEndTime.MaxLength = 0;
				colvarEndTime.AutoIncrement = false;
				colvarEndTime.IsNullable = true;
				colvarEndTime.IsPrimaryKey = false;
				colvarEndTime.IsForeignKey = false;
				colvarEndTime.IsReadOnly = false;
				colvarEndTime.DefaultSetting = @"";
				colvarEndTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEndTime);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("hi_deal_subscription_promo_slot",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("CodeGroup")]
		[Bindable(true)]
		public string CodeGroup 
		{
			get { return GetColumnValue<string>(Columns.CodeGroup); }
			set { SetColumnValue(Columns.CodeGroup, value); }
		}
		  
		[XmlAttribute("CodeId")]
		[Bindable(true)]
		public int CodeId 
		{
			get { return GetColumnValue<int>(Columns.CodeId); }
			set { SetColumnValue(Columns.CodeId, value); }
		}
		  
		[XmlAttribute("PromoId")]
		[Bindable(true)]
		public int PromoId 
		{
			get { return GetColumnValue<int>(Columns.PromoId); }
			set { SetColumnValue(Columns.PromoId, value); }
		}
		  
		[XmlAttribute("Part")]
		[Bindable(true)]
		public int Part 
		{
			get { return GetColumnValue<int>(Columns.Part); }
			set { SetColumnValue(Columns.Part, value); }
		}
		  
		[XmlAttribute("StartTime")]
		[Bindable(true)]
		public DateTime? StartTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.StartTime); }
			set { SetColumnValue(Columns.StartTime, value); }
		}
		  
		[XmlAttribute("EndTime")]
		[Bindable(true)]
		public DateTime? EndTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.EndTime); }
			set { SetColumnValue(Columns.EndTime, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn CodeGroupColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn CodeIdColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn PromoIdColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn PartColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn StartTimeColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn EndTimeColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string CodeGroup = @"code_group";
			 public static string CodeId = @"code_id";
			 public static string PromoId = @"promo_id";
			 public static string Part = @"part";
			 public static string StartTime = @"start_time";
			 public static string EndTime = @"end_time";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the CashTrustStatusLog class.
	/// </summary>
    [Serializable]
	public partial class CashTrustStatusLogCollection : RepositoryList<CashTrustStatusLog, CashTrustStatusLogCollection>
	{	   
		public CashTrustStatusLogCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>CashTrustStatusLogCollection</returns>
		public CashTrustStatusLogCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                CashTrustStatusLog o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the cash_trust_status_log table.
	/// </summary>
	[Serializable]
	public partial class CashTrustStatusLog : RepositoryRecord<CashTrustStatusLog>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public CashTrustStatusLog()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public CashTrustStatusLog(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("cash_trust_status_log", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarTrustId = new TableSchema.TableColumn(schema);
				colvarTrustId.ColumnName = "trust_id";
				colvarTrustId.DataType = DbType.Guid;
				colvarTrustId.MaxLength = 0;
				colvarTrustId.AutoIncrement = false;
				colvarTrustId.IsNullable = false;
				colvarTrustId.IsPrimaryKey = false;
				colvarTrustId.IsForeignKey = false;
				colvarTrustId.IsReadOnly = false;
				colvarTrustId.DefaultSetting = @"";
				colvarTrustId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTrustId);
				
				TableSchema.TableColumn colvarTrustProvider = new TableSchema.TableColumn(schema);
				colvarTrustProvider.ColumnName = "trust_provider";
				colvarTrustProvider.DataType = DbType.Int32;
				colvarTrustProvider.MaxLength = 0;
				colvarTrustProvider.AutoIncrement = false;
				colvarTrustProvider.IsNullable = false;
				colvarTrustProvider.IsPrimaryKey = false;
				colvarTrustProvider.IsForeignKey = false;
				colvarTrustProvider.IsReadOnly = false;
				colvarTrustProvider.DefaultSetting = @"";
				colvarTrustProvider.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTrustProvider);
				
				TableSchema.TableColumn colvarAmount = new TableSchema.TableColumn(schema);
				colvarAmount.ColumnName = "amount";
				colvarAmount.DataType = DbType.Int32;
				colvarAmount.MaxLength = 0;
				colvarAmount.AutoIncrement = false;
				colvarAmount.IsNullable = false;
				colvarAmount.IsPrimaryKey = false;
				colvarAmount.IsForeignKey = false;
				colvarAmount.IsReadOnly = false;
				colvarAmount.DefaultSetting = @"";
				colvarAmount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAmount);
				
				TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
				colvarStatus.ColumnName = "status";
				colvarStatus.DataType = DbType.Int32;
				colvarStatus.MaxLength = 0;
				colvarStatus.AutoIncrement = false;
				colvarStatus.IsNullable = false;
				colvarStatus.IsPrimaryKey = false;
				colvarStatus.IsForeignKey = false;
				colvarStatus.IsReadOnly = false;
				colvarStatus.DefaultSetting = @"";
				colvarStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStatus);
				
				TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
				colvarModifyTime.ColumnName = "modify_time";
				colvarModifyTime.DataType = DbType.DateTime;
				colvarModifyTime.MaxLength = 0;
				colvarModifyTime.AutoIncrement = false;
				colvarModifyTime.IsNullable = false;
				colvarModifyTime.IsPrimaryKey = false;
				colvarModifyTime.IsForeignKey = false;
				colvarModifyTime.IsReadOnly = false;
				colvarModifyTime.DefaultSetting = @"";
				colvarModifyTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyTime);
				
				TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
				colvarCreateId.ColumnName = "create_id";
				colvarCreateId.DataType = DbType.String;
				colvarCreateId.MaxLength = 256;
				colvarCreateId.AutoIncrement = false;
				colvarCreateId.IsNullable = true;
				colvarCreateId.IsPrimaryKey = false;
				colvarCreateId.IsForeignKey = false;
				colvarCreateId.IsReadOnly = false;
				colvarCreateId.DefaultSetting = @"";
				colvarCreateId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateId);
				
				TableSchema.TableColumn colvarSpecialStatus = new TableSchema.TableColumn(schema);
				colvarSpecialStatus.ColumnName = "special_status";
				colvarSpecialStatus.DataType = DbType.Int32;
				colvarSpecialStatus.MaxLength = 0;
				colvarSpecialStatus.AutoIncrement = false;
				colvarSpecialStatus.IsNullable = false;
				colvarSpecialStatus.IsPrimaryKey = false;
				colvarSpecialStatus.IsForeignKey = false;
				colvarSpecialStatus.IsReadOnly = false;
				
						colvarSpecialStatus.DefaultSetting = @"((0))";
				colvarSpecialStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSpecialStatus);
				
				TableSchema.TableColumn colvarSpecialOperatedTime = new TableSchema.TableColumn(schema);
				colvarSpecialOperatedTime.ColumnName = "special_operated_time";
				colvarSpecialOperatedTime.DataType = DbType.DateTime;
				colvarSpecialOperatedTime.MaxLength = 0;
				colvarSpecialOperatedTime.AutoIncrement = false;
				colvarSpecialOperatedTime.IsNullable = true;
				colvarSpecialOperatedTime.IsPrimaryKey = false;
				colvarSpecialOperatedTime.IsForeignKey = false;
				colvarSpecialOperatedTime.IsReadOnly = false;
				colvarSpecialOperatedTime.DefaultSetting = @"";
				colvarSpecialOperatedTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSpecialOperatedTime);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("cash_trust_status_log",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("TrustId")]
		[Bindable(true)]
		public Guid TrustId 
		{
			get { return GetColumnValue<Guid>(Columns.TrustId); }
			set { SetColumnValue(Columns.TrustId, value); }
		}
		  
		[XmlAttribute("TrustProvider")]
		[Bindable(true)]
		public int TrustProvider 
		{
			get { return GetColumnValue<int>(Columns.TrustProvider); }
			set { SetColumnValue(Columns.TrustProvider, value); }
		}
		  
		[XmlAttribute("Amount")]
		[Bindable(true)]
		public int Amount 
		{
			get { return GetColumnValue<int>(Columns.Amount); }
			set { SetColumnValue(Columns.Amount, value); }
		}
		  
		[XmlAttribute("Status")]
		[Bindable(true)]
		public int Status 
		{
			get { return GetColumnValue<int>(Columns.Status); }
			set { SetColumnValue(Columns.Status, value); }
		}
		  
		[XmlAttribute("ModifyTime")]
		[Bindable(true)]
		public DateTime ModifyTime 
		{
			get { return GetColumnValue<DateTime>(Columns.ModifyTime); }
			set { SetColumnValue(Columns.ModifyTime, value); }
		}
		  
		[XmlAttribute("CreateId")]
		[Bindable(true)]
		public string CreateId 
		{
			get { return GetColumnValue<string>(Columns.CreateId); }
			set { SetColumnValue(Columns.CreateId, value); }
		}
		  
		[XmlAttribute("SpecialStatus")]
		[Bindable(true)]
		public int SpecialStatus 
		{
			get { return GetColumnValue<int>(Columns.SpecialStatus); }
			set { SetColumnValue(Columns.SpecialStatus, value); }
		}
		  
		[XmlAttribute("SpecialOperatedTime")]
		[Bindable(true)]
		public DateTime? SpecialOperatedTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.SpecialOperatedTime); }
			set { SetColumnValue(Columns.SpecialOperatedTime, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn TrustIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn TrustProviderColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn AmountColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn StatusColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn SpecialStatusColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn SpecialOperatedTimeColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string TrustId = @"trust_id";
			 public static string TrustProvider = @"trust_provider";
			 public static string Amount = @"amount";
			 public static string Status = @"status";
			 public static string ModifyTime = @"modify_time";
			 public static string CreateId = @"create_id";
			 public static string SpecialStatus = @"special_status";
			 public static string SpecialOperatedTime = @"special_operated_time";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

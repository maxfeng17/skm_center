using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the FamiportPeztempLink class.
	/// </summary>
    [Serializable]
	public partial class FamiportPeztempLinkCollection : RepositoryList<FamiportPeztempLink, FamiportPeztempLinkCollection>
	{	   
		public FamiportPeztempLinkCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>FamiportPeztempLinkCollection</returns>
		public FamiportPeztempLinkCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                FamiportPeztempLink o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the famiport_peztemp_link table.
	/// </summary>
	[Serializable]
	public partial class FamiportPeztempLink : RepositoryRecord<FamiportPeztempLink>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public FamiportPeztempLink()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public FamiportPeztempLink(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("famiport_peztemp_link", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarPeztempId = new TableSchema.TableColumn(schema);
				colvarPeztempId.ColumnName = "peztemp_id";
				colvarPeztempId.DataType = DbType.Int32;
				colvarPeztempId.MaxLength = 0;
				colvarPeztempId.AutoIncrement = false;
				colvarPeztempId.IsNullable = false;
				colvarPeztempId.IsPrimaryKey = false;
				colvarPeztempId.IsForeignKey = false;
				colvarPeztempId.IsReadOnly = false;
				colvarPeztempId.DefaultSetting = @"";
				colvarPeztempId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPeztempId);
				
				TableSchema.TableColumn colvarTranNo = new TableSchema.TableColumn(schema);
				colvarTranNo.ColumnName = "tran_no";
				colvarTranNo.DataType = DbType.AnsiString;
				colvarTranNo.MaxLength = 20;
				colvarTranNo.AutoIncrement = false;
				colvarTranNo.IsNullable = false;
				colvarTranNo.IsPrimaryKey = false;
				colvarTranNo.IsForeignKey = false;
				colvarTranNo.IsReadOnly = false;
				colvarTranNo.DefaultSetting = @"";
				colvarTranNo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTranNo);
				
				TableSchema.TableColumn colvarMmkId = new TableSchema.TableColumn(schema);
				colvarMmkId.ColumnName = "mmk_id";
				colvarMmkId.DataType = DbType.AnsiString;
				colvarMmkId.MaxLength = 4;
				colvarMmkId.AutoIncrement = false;
				colvarMmkId.IsNullable = true;
				colvarMmkId.IsPrimaryKey = false;
				colvarMmkId.IsForeignKey = false;
				colvarMmkId.IsReadOnly = false;
				colvarMmkId.DefaultSetting = @"";
				colvarMmkId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMmkId);
				
				TableSchema.TableColumn colvarMmkIdName = new TableSchema.TableColumn(schema);
				colvarMmkIdName.ColumnName = "mmk_id_name";
				colvarMmkIdName.DataType = DbType.String;
				colvarMmkIdName.MaxLength = 30;
				colvarMmkIdName.AutoIncrement = false;
				colvarMmkIdName.IsNullable = true;
				colvarMmkIdName.IsPrimaryKey = false;
				colvarMmkIdName.IsForeignKey = false;
				colvarMmkIdName.IsReadOnly = false;
				colvarMmkIdName.DefaultSetting = @"";
				colvarMmkIdName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMmkIdName);
				
				TableSchema.TableColumn colvarTenCode = new TableSchema.TableColumn(schema);
				colvarTenCode.ColumnName = "ten_code";
				colvarTenCode.DataType = DbType.AnsiString;
				colvarTenCode.MaxLength = 6;
				colvarTenCode.AutoIncrement = false;
				colvarTenCode.IsNullable = true;
				colvarTenCode.IsPrimaryKey = false;
				colvarTenCode.IsForeignKey = false;
				colvarTenCode.IsReadOnly = false;
				colvarTenCode.DefaultSetting = @"";
				colvarTenCode.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTenCode);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				
						colvarCreateTime.DefaultSetting = @"(getdate())";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarSerialNo = new TableSchema.TableColumn(schema);
				colvarSerialNo.ColumnName = "serial_no";
				colvarSerialNo.DataType = DbType.AnsiString;
				colvarSerialNo.MaxLength = 2;
				colvarSerialNo.AutoIncrement = false;
				colvarSerialNo.IsNullable = true;
				colvarSerialNo.IsPrimaryKey = false;
				colvarSerialNo.IsForeignKey = false;
				colvarSerialNo.IsReadOnly = false;
				colvarSerialNo.DefaultSetting = @"";
				colvarSerialNo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSerialNo);
				
				TableSchema.TableColumn colvarCdFmcode = new TableSchema.TableColumn(schema);
				colvarCdFmcode.ColumnName = "cd_fmcode";
				colvarCdFmcode.DataType = DbType.AnsiString;
				colvarCdFmcode.MaxLength = 7;
				colvarCdFmcode.AutoIncrement = false;
				colvarCdFmcode.IsNullable = true;
				colvarCdFmcode.IsPrimaryKey = false;
				colvarCdFmcode.IsForeignKey = false;
				colvarCdFmcode.IsReadOnly = false;
				colvarCdFmcode.DefaultSetting = @"";
				colvarCdFmcode.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCdFmcode);
				
				TableSchema.TableColumn colvarFirmNo = new TableSchema.TableColumn(schema);
				colvarFirmNo.ColumnName = "firm_no";
				colvarFirmNo.DataType = DbType.AnsiString;
				colvarFirmNo.MaxLength = 10;
				colvarFirmNo.AutoIncrement = false;
				colvarFirmNo.IsNullable = true;
				colvarFirmNo.IsPrimaryKey = false;
				colvarFirmNo.IsForeignKey = false;
				colvarFirmNo.IsReadOnly = false;
				colvarFirmNo.DefaultSetting = @"";
				colvarFirmNo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFirmNo);
				
				TableSchema.TableColumn colvarEventNo = new TableSchema.TableColumn(schema);
				colvarEventNo.ColumnName = "event_no";
				colvarEventNo.DataType = DbType.AnsiString;
				colvarEventNo.MaxLength = 20;
				colvarEventNo.AutoIncrement = false;
				colvarEventNo.IsNullable = true;
				colvarEventNo.IsPrimaryKey = false;
				colvarEventNo.IsForeignKey = false;
				colvarEventNo.IsReadOnly = false;
				colvarEventNo.DefaultSetting = @"";
				colvarEventNo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEventNo);
				
				TableSchema.TableColumn colvarPayDate = new TableSchema.TableColumn(schema);
				colvarPayDate.ColumnName = "pay_date";
				colvarPayDate.DataType = DbType.AnsiString;
				colvarPayDate.MaxLength = 8;
				colvarPayDate.AutoIncrement = false;
				colvarPayDate.IsNullable = true;
				colvarPayDate.IsPrimaryKey = false;
				colvarPayDate.IsForeignKey = false;
				colvarPayDate.IsReadOnly = false;
				colvarPayDate.DefaultSetting = @"";
				colvarPayDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPayDate);
				
				TableSchema.TableColumn colvarPayTime = new TableSchema.TableColumn(schema);
				colvarPayTime.ColumnName = "pay_time";
				colvarPayTime.DataType = DbType.AnsiString;
				colvarPayTime.MaxLength = 6;
				colvarPayTime.AutoIncrement = false;
				colvarPayTime.IsNullable = true;
				colvarPayTime.IsPrimaryKey = false;
				colvarPayTime.IsForeignKey = false;
				colvarPayTime.IsReadOnly = false;
				colvarPayTime.DefaultSetting = @"";
				colvarPayTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPayTime);
				
				TableSchema.TableColumn colvarRtnUrl = new TableSchema.TableColumn(schema);
				colvarRtnUrl.ColumnName = "rtn_url";
				colvarRtnUrl.DataType = DbType.AnsiString;
				colvarRtnUrl.MaxLength = 120;
				colvarRtnUrl.AutoIncrement = false;
				colvarRtnUrl.IsNullable = true;
				colvarRtnUrl.IsPrimaryKey = false;
				colvarRtnUrl.IsForeignKey = false;
				colvarRtnUrl.IsReadOnly = false;
				colvarRtnUrl.DefaultSetting = @"";
				colvarRtnUrl.ForeignKeyTableName = "";
				schema.Columns.Add(colvarRtnUrl);
				
				TableSchema.TableColumn colvarTraceUrl = new TableSchema.TableColumn(schema);
				colvarTraceUrl.ColumnName = "trace_url";
				colvarTraceUrl.DataType = DbType.AnsiString;
				colvarTraceUrl.MaxLength = 255;
				colvarTraceUrl.AutoIncrement = false;
				colvarTraceUrl.IsNullable = true;
				colvarTraceUrl.IsPrimaryKey = false;
				colvarTraceUrl.IsForeignKey = false;
				colvarTraceUrl.IsReadOnly = false;
				colvarTraceUrl.DefaultSetting = @"";
				colvarTraceUrl.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTraceUrl);
				
				TableSchema.TableColumn colvarErrUrl = new TableSchema.TableColumn(schema);
				colvarErrUrl.ColumnName = "err_url";
				colvarErrUrl.DataType = DbType.AnsiString;
				colvarErrUrl.MaxLength = 255;
				colvarErrUrl.AutoIncrement = false;
				colvarErrUrl.IsNullable = true;
				colvarErrUrl.IsPrimaryKey = false;
				colvarErrUrl.IsForeignKey = false;
				colvarErrUrl.IsReadOnly = false;
				colvarErrUrl.DefaultSetting = @"";
				colvarErrUrl.ForeignKeyTableName = "";
				schema.Columns.Add(colvarErrUrl);
				
				TableSchema.TableColumn colvarData1 = new TableSchema.TableColumn(schema);
				colvarData1.ColumnName = "data_1";
				colvarData1.DataType = DbType.AnsiString;
				colvarData1.MaxLength = 255;
				colvarData1.AutoIncrement = false;
				colvarData1.IsNullable = true;
				colvarData1.IsPrimaryKey = false;
				colvarData1.IsForeignKey = false;
				colvarData1.IsReadOnly = false;
				colvarData1.DefaultSetting = @"";
				colvarData1.ForeignKeyTableName = "";
				schema.Columns.Add(colvarData1);
				
				TableSchema.TableColumn colvarData2 = new TableSchema.TableColumn(schema);
				colvarData2.ColumnName = "data_2";
				colvarData2.DataType = DbType.AnsiString;
				colvarData2.MaxLength = 255;
				colvarData2.AutoIncrement = false;
				colvarData2.IsNullable = true;
				colvarData2.IsPrimaryKey = false;
				colvarData2.IsForeignKey = false;
				colvarData2.IsReadOnly = false;
				colvarData2.DefaultSetting = @"";
				colvarData2.ForeignKeyTableName = "";
				schema.Columns.Add(colvarData2);
				
				TableSchema.TableColumn colvarData3 = new TableSchema.TableColumn(schema);
				colvarData3.ColumnName = "data_3";
				colvarData3.DataType = DbType.AnsiString;
				colvarData3.MaxLength = 255;
				colvarData3.AutoIncrement = false;
				colvarData3.IsNullable = true;
				colvarData3.IsPrimaryKey = false;
				colvarData3.IsForeignKey = false;
				colvarData3.IsReadOnly = false;
				colvarData3.DefaultSetting = @"";
				colvarData3.ForeignKeyTableName = "";
				schema.Columns.Add(colvarData3);
				
				TableSchema.TableColumn colvarPayDatetime = new TableSchema.TableColumn(schema);
				colvarPayDatetime.ColumnName = "pay_datetime";
				colvarPayDatetime.DataType = DbType.DateTime;
				colvarPayDatetime.MaxLength = 0;
				colvarPayDatetime.AutoIncrement = false;
				colvarPayDatetime.IsNullable = true;
				colvarPayDatetime.IsPrimaryKey = false;
				colvarPayDatetime.IsForeignKey = false;
				colvarPayDatetime.IsReadOnly = false;
				colvarPayDatetime.DefaultSetting = @"";
				colvarPayDatetime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPayDatetime);
				
				TableSchema.TableColumn colvarPayTenCode = new TableSchema.TableColumn(schema);
				colvarPayTenCode.ColumnName = "pay_ten_code";
				colvarPayTenCode.DataType = DbType.AnsiString;
				colvarPayTenCode.MaxLength = 6;
				colvarPayTenCode.AutoIncrement = false;
				colvarPayTenCode.IsNullable = true;
				colvarPayTenCode.IsPrimaryKey = false;
				colvarPayTenCode.IsForeignKey = false;
				colvarPayTenCode.IsReadOnly = false;
				colvarPayTenCode.DefaultSetting = @"";
				colvarPayTenCode.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPayTenCode);
				
				TableSchema.TableColumn colvarUpdatePayTime = new TableSchema.TableColumn(schema);
				colvarUpdatePayTime.ColumnName = "update_pay_time";
				colvarUpdatePayTime.DataType = DbType.DateTime;
				colvarUpdatePayTime.MaxLength = 0;
				colvarUpdatePayTime.AutoIncrement = false;
				colvarUpdatePayTime.IsNullable = true;
				colvarUpdatePayTime.IsPrimaryKey = false;
				colvarUpdatePayTime.IsForeignKey = false;
				colvarUpdatePayTime.IsReadOnly = false;
				colvarUpdatePayTime.DefaultSetting = @"";
				colvarUpdatePayTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUpdatePayTime);
				
				TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
				colvarModifyTime.ColumnName = "modify_time";
				colvarModifyTime.DataType = DbType.DateTime;
				colvarModifyTime.MaxLength = 0;
				colvarModifyTime.AutoIncrement = false;
				colvarModifyTime.IsNullable = true;
				colvarModifyTime.IsPrimaryKey = false;
				colvarModifyTime.IsForeignKey = false;
				colvarModifyTime.IsReadOnly = false;
				colvarModifyTime.DefaultSetting = @"";
				colvarModifyTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyTime);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("famiport_peztemp_link",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("PeztempId")]
		[Bindable(true)]
		public int PeztempId 
		{
			get { return GetColumnValue<int>(Columns.PeztempId); }
			set { SetColumnValue(Columns.PeztempId, value); }
		}
		  
		[XmlAttribute("TranNo")]
		[Bindable(true)]
		public string TranNo 
		{
			get { return GetColumnValue<string>(Columns.TranNo); }
			set { SetColumnValue(Columns.TranNo, value); }
		}
		  
		[XmlAttribute("MmkId")]
		[Bindable(true)]
		public string MmkId 
		{
			get { return GetColumnValue<string>(Columns.MmkId); }
			set { SetColumnValue(Columns.MmkId, value); }
		}
		  
		[XmlAttribute("MmkIdName")]
		[Bindable(true)]
		public string MmkIdName 
		{
			get { return GetColumnValue<string>(Columns.MmkIdName); }
			set { SetColumnValue(Columns.MmkIdName, value); }
		}
		  
		[XmlAttribute("TenCode")]
		[Bindable(true)]
		public string TenCode 
		{
			get { return GetColumnValue<string>(Columns.TenCode); }
			set { SetColumnValue(Columns.TenCode, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		  
		[XmlAttribute("SerialNo")]
		[Bindable(true)]
		public string SerialNo 
		{
			get { return GetColumnValue<string>(Columns.SerialNo); }
			set { SetColumnValue(Columns.SerialNo, value); }
		}
		  
		[XmlAttribute("CdFmcode")]
		[Bindable(true)]
		public string CdFmcode 
		{
			get { return GetColumnValue<string>(Columns.CdFmcode); }
			set { SetColumnValue(Columns.CdFmcode, value); }
		}
		  
		[XmlAttribute("FirmNo")]
		[Bindable(true)]
		public string FirmNo 
		{
			get { return GetColumnValue<string>(Columns.FirmNo); }
			set { SetColumnValue(Columns.FirmNo, value); }
		}
		  
		[XmlAttribute("EventNo")]
		[Bindable(true)]
		public string EventNo 
		{
			get { return GetColumnValue<string>(Columns.EventNo); }
			set { SetColumnValue(Columns.EventNo, value); }
		}
		  
		[XmlAttribute("PayDate")]
		[Bindable(true)]
		public string PayDate 
		{
			get { return GetColumnValue<string>(Columns.PayDate); }
			set { SetColumnValue(Columns.PayDate, value); }
		}
		  
		[XmlAttribute("PayTime")]
		[Bindable(true)]
		public string PayTime 
		{
			get { return GetColumnValue<string>(Columns.PayTime); }
			set { SetColumnValue(Columns.PayTime, value); }
		}
		  
		[XmlAttribute("RtnUrl")]
		[Bindable(true)]
		public string RtnUrl 
		{
			get { return GetColumnValue<string>(Columns.RtnUrl); }
			set { SetColumnValue(Columns.RtnUrl, value); }
		}
		  
		[XmlAttribute("TraceUrl")]
		[Bindable(true)]
		public string TraceUrl 
		{
			get { return GetColumnValue<string>(Columns.TraceUrl); }
			set { SetColumnValue(Columns.TraceUrl, value); }
		}
		  
		[XmlAttribute("ErrUrl")]
		[Bindable(true)]
		public string ErrUrl 
		{
			get { return GetColumnValue<string>(Columns.ErrUrl); }
			set { SetColumnValue(Columns.ErrUrl, value); }
		}
		  
		[XmlAttribute("Data1")]
		[Bindable(true)]
		public string Data1 
		{
			get { return GetColumnValue<string>(Columns.Data1); }
			set { SetColumnValue(Columns.Data1, value); }
		}
		  
		[XmlAttribute("Data2")]
		[Bindable(true)]
		public string Data2 
		{
			get { return GetColumnValue<string>(Columns.Data2); }
			set { SetColumnValue(Columns.Data2, value); }
		}
		  
		[XmlAttribute("Data3")]
		[Bindable(true)]
		public string Data3 
		{
			get { return GetColumnValue<string>(Columns.Data3); }
			set { SetColumnValue(Columns.Data3, value); }
		}
		  
		[XmlAttribute("PayDatetime")]
		[Bindable(true)]
		public DateTime? PayDatetime 
		{
			get { return GetColumnValue<DateTime?>(Columns.PayDatetime); }
			set { SetColumnValue(Columns.PayDatetime, value); }
		}
		  
		[XmlAttribute("PayTenCode")]
		[Bindable(true)]
		public string PayTenCode 
		{
			get { return GetColumnValue<string>(Columns.PayTenCode); }
			set { SetColumnValue(Columns.PayTenCode, value); }
		}
		  
		[XmlAttribute("UpdatePayTime")]
		[Bindable(true)]
		public DateTime? UpdatePayTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.UpdatePayTime); }
			set { SetColumnValue(Columns.UpdatePayTime, value); }
		}
		  
		[XmlAttribute("ModifyTime")]
		[Bindable(true)]
		public DateTime? ModifyTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.ModifyTime); }
			set { SetColumnValue(Columns.ModifyTime, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn PeztempIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn TranNoColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn MmkIdColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn MmkIdNameColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn TenCodeColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn SerialNoColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn CdFmcodeColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn FirmNoColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn EventNoColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        public static TableSchema.TableColumn PayDateColumn
        {
            get { return Schema.Columns[11]; }
        }
        
        
        
        public static TableSchema.TableColumn PayTimeColumn
        {
            get { return Schema.Columns[12]; }
        }
        
        
        
        public static TableSchema.TableColumn RtnUrlColumn
        {
            get { return Schema.Columns[13]; }
        }
        
        
        
        public static TableSchema.TableColumn TraceUrlColumn
        {
            get { return Schema.Columns[14]; }
        }
        
        
        
        public static TableSchema.TableColumn ErrUrlColumn
        {
            get { return Schema.Columns[15]; }
        }
        
        
        
        public static TableSchema.TableColumn Data1Column
        {
            get { return Schema.Columns[16]; }
        }
        
        
        
        public static TableSchema.TableColumn Data2Column
        {
            get { return Schema.Columns[17]; }
        }
        
        
        
        public static TableSchema.TableColumn Data3Column
        {
            get { return Schema.Columns[18]; }
        }
        
        
        
        public static TableSchema.TableColumn PayDatetimeColumn
        {
            get { return Schema.Columns[19]; }
        }
        
        
        
        public static TableSchema.TableColumn PayTenCodeColumn
        {
            get { return Schema.Columns[20]; }
        }
        
        
        
        public static TableSchema.TableColumn UpdatePayTimeColumn
        {
            get { return Schema.Columns[21]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[22]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string PeztempId = @"peztemp_id";
			 public static string TranNo = @"tran_no";
			 public static string MmkId = @"mmk_id";
			 public static string MmkIdName = @"mmk_id_name";
			 public static string TenCode = @"ten_code";
			 public static string CreateTime = @"create_time";
			 public static string SerialNo = @"serial_no";
			 public static string CdFmcode = @"cd_fmcode";
			 public static string FirmNo = @"firm_no";
			 public static string EventNo = @"event_no";
			 public static string PayDate = @"pay_date";
			 public static string PayTime = @"pay_time";
			 public static string RtnUrl = @"rtn_url";
			 public static string TraceUrl = @"trace_url";
			 public static string ErrUrl = @"err_url";
			 public static string Data1 = @"data_1";
			 public static string Data2 = @"data_2";
			 public static string Data3 = @"data_3";
			 public static string PayDatetime = @"pay_datetime";
			 public static string PayTenCode = @"pay_ten_code";
			 public static string UpdatePayTime = @"update_pay_time";
			 public static string ModifyTime = @"modify_time";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

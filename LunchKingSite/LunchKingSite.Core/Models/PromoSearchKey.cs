using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the PromoSearchKey class.
	/// </summary>
    [Serializable]
	public partial class PromoSearchKeyCollection : RepositoryList<PromoSearchKey, PromoSearchKeyCollection>
	{	   
		public PromoSearchKeyCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>PromoSearchKeyCollection</returns>
		public PromoSearchKeyCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                PromoSearchKey o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
	}
	
	
	/// <summary>
	/// This is an ActiveRecord class which wraps the promo_search_key table.
	/// </summary>
	
	[Serializable]
	public partial class PromoSearchKey : RepositoryRecord<PromoSearchKey>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public PromoSearchKey()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public PromoSearchKey(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("promo_search_key", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarKeyWord = new TableSchema.TableColumn(schema);
				colvarKeyWord.ColumnName = "key_word";
				colvarKeyWord.DataType = DbType.String;
				colvarKeyWord.MaxLength = 50;
				colvarKeyWord.AutoIncrement = false;
				colvarKeyWord.IsNullable = false;
				colvarKeyWord.IsPrimaryKey = false;
				colvarKeyWord.IsForeignKey = false;
				colvarKeyWord.IsReadOnly = false;
				colvarKeyWord.DefaultSetting = @"";
				colvarKeyWord.ForeignKeyTableName = "";
				schema.Columns.Add(colvarKeyWord);
				
				TableSchema.TableColumn colvarIsEnable = new TableSchema.TableColumn(schema);
				colvarIsEnable.ColumnName = "is_enable";
				colvarIsEnable.DataType = DbType.Boolean;
				colvarIsEnable.MaxLength = 0;
				colvarIsEnable.AutoIncrement = false;
				colvarIsEnable.IsNullable = false;
				colvarIsEnable.IsPrimaryKey = false;
				colvarIsEnable.IsForeignKey = false;
				colvarIsEnable.IsReadOnly = false;
				
						colvarIsEnable.DefaultSetting = @"((0))";
				colvarIsEnable.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsEnable);
				
				TableSchema.TableColumn colvarSeq = new TableSchema.TableColumn(schema);
				colvarSeq.ColumnName = "seq";
				colvarSeq.DataType = DbType.Int32;
				colvarSeq.MaxLength = 0;
				colvarSeq.AutoIncrement = false;
				colvarSeq.IsNullable = true;
				colvarSeq.IsPrimaryKey = false;
				colvarSeq.IsForeignKey = false;
				colvarSeq.IsReadOnly = false;
				colvarSeq.DefaultSetting = @"";
				colvarSeq.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSeq);
				
				TableSchema.TableColumn colvarUseMode = new TableSchema.TableColumn(schema);
				colvarUseMode.ColumnName = "use_mode";
				colvarUseMode.DataType = DbType.Int32;
				colvarUseMode.MaxLength = 0;
				colvarUseMode.AutoIncrement = false;
				colvarUseMode.IsNullable = false;
				colvarUseMode.IsPrimaryKey = false;
				colvarUseMode.IsForeignKey = false;
				colvarUseMode.IsReadOnly = false;
				colvarUseMode.DefaultSetting = @"";
				colvarUseMode.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUseMode);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarCreateUserId = new TableSchema.TableColumn(schema);
				colvarCreateUserId.ColumnName = "create_user_id";
				colvarCreateUserId.DataType = DbType.Int32;
				colvarCreateUserId.MaxLength = 0;
				colvarCreateUserId.AutoIncrement = false;
				colvarCreateUserId.IsNullable = false;
				colvarCreateUserId.IsPrimaryKey = false;
				colvarCreateUserId.IsForeignKey = false;
				colvarCreateUserId.IsReadOnly = false;
				colvarCreateUserId.DefaultSetting = @"";
				colvarCreateUserId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateUserId);
				
				TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
				colvarModifyTime.ColumnName = "modify_time";
				colvarModifyTime.DataType = DbType.DateTime;
				colvarModifyTime.MaxLength = 0;
				colvarModifyTime.AutoIncrement = false;
				colvarModifyTime.IsNullable = true;
				colvarModifyTime.IsPrimaryKey = false;
				colvarModifyTime.IsForeignKey = false;
				colvarModifyTime.IsReadOnly = false;
				colvarModifyTime.DefaultSetting = @"";
				colvarModifyTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyTime);
				
				TableSchema.TableColumn colvarModifyUserDi = new TableSchema.TableColumn(schema);
				colvarModifyUserDi.ColumnName = "modify_user_di";
				colvarModifyUserDi.DataType = DbType.Int32;
				colvarModifyUserDi.MaxLength = 0;
				colvarModifyUserDi.AutoIncrement = false;
				colvarModifyUserDi.IsNullable = true;
				colvarModifyUserDi.IsPrimaryKey = false;
				colvarModifyUserDi.IsForeignKey = false;
				colvarModifyUserDi.IsReadOnly = false;
				colvarModifyUserDi.DefaultSetting = @"";
				colvarModifyUserDi.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyUserDi);
				
				TableSchema.TableColumn colvarPicUrl = new TableSchema.TableColumn(schema);
				colvarPicUrl.ColumnName = "pic_url";
				colvarPicUrl.DataType = DbType.String;
				colvarPicUrl.MaxLength = 200;
				colvarPicUrl.AutoIncrement = false;
				colvarPicUrl.IsNullable = true;
				colvarPicUrl.IsPrimaryKey = false;
				colvarPicUrl.IsForeignKey = false;
				colvarPicUrl.IsReadOnly = false;
				colvarPicUrl.DefaultSetting = @"";
				colvarPicUrl.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPicUrl);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("promo_search_key",schema);
			}
		}
		#endregion
		
		#region Props
		
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		
		[XmlAttribute("KeyWord")]
		[Bindable(true)]
		public string KeyWord 
		{
			get { return GetColumnValue<string>(Columns.KeyWord); }
			set { SetColumnValue(Columns.KeyWord, value); }
		}
		
		[XmlAttribute("IsEnable")]
		[Bindable(true)]
		public bool IsEnable 
		{
			get { return GetColumnValue<bool>(Columns.IsEnable); }
			set { SetColumnValue(Columns.IsEnable, value); }
		}
		
		[XmlAttribute("Seq")]
		[Bindable(true)]
		public int? Seq 
		{
			get { return GetColumnValue<int?>(Columns.Seq); }
			set { SetColumnValue(Columns.Seq, value); }
		}
		
		[XmlAttribute("UseMode")]
		[Bindable(true)]
		public int UseMode 
		{
			get { return GetColumnValue<int>(Columns.UseMode); }
			set { SetColumnValue(Columns.UseMode, value); }
		}
		
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		
		[XmlAttribute("CreateUserId")]
		[Bindable(true)]
		public int CreateUserId 
		{
			get { return GetColumnValue<int>(Columns.CreateUserId); }
			set { SetColumnValue(Columns.CreateUserId, value); }
		}
		
		[XmlAttribute("ModifyTime")]
		[Bindable(true)]
		public DateTime? ModifyTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.ModifyTime); }
			set { SetColumnValue(Columns.ModifyTime, value); }
		}
		
		[XmlAttribute("ModifyUserDi")]
		[Bindable(true)]
		public int? ModifyUserDi 
		{
			get { return GetColumnValue<int?>(Columns.ModifyUserDi); }
			set { SetColumnValue(Columns.ModifyUserDi, value); }
		}
		
		[XmlAttribute("PicUrl")]
		[Bindable(true)]
		public string PicUrl 
		{
			get { return GetColumnValue<string>(Columns.PicUrl); }
			set { SetColumnValue(Columns.PicUrl, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn KeyWordColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn IsEnableColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn SeqColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn UseModeColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateUserIdColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyUserDiColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn PicUrlColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string KeyWord = @"key_word";
			 public static string IsEnable = @"is_enable";
			 public static string Seq = @"seq";
			 public static string UseMode = @"use_mode";
			 public static string CreateTime = @"create_time";
			 public static string CreateUserId = @"create_user_id";
			 public static string ModifyTime = @"modify_time";
			 public static string ModifyUserDi = @"modify_user_di";
			 public static string PicUrl = @"pic_url";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

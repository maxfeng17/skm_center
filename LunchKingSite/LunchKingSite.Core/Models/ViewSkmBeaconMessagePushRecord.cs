using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewSkmBeaconMessagePushRecord class.
    /// </summary>
    [Serializable]
    public partial class ViewSkmBeaconMessagePushRecordCollection : ReadOnlyList<ViewSkmBeaconMessagePushRecord, ViewSkmBeaconMessagePushRecordCollection>
    {        
        public ViewSkmBeaconMessagePushRecordCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_skm_beacon_message_push_record view.
    /// </summary>
    [Serializable]
    public partial class ViewSkmBeaconMessagePushRecord : ReadOnlyRecord<ViewSkmBeaconMessagePushRecord>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_skm_beacon_message_push_record", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = false;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = false;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                
                schema.Columns.Add(colvarId);
                
                TableSchema.TableColumn colvarIsRead = new TableSchema.TableColumn(schema);
                colvarIsRead.ColumnName = "is_read";
                colvarIsRead.DataType = DbType.Boolean;
                colvarIsRead.MaxLength = 0;
                colvarIsRead.AutoIncrement = false;
                colvarIsRead.IsNullable = false;
                colvarIsRead.IsPrimaryKey = false;
                colvarIsRead.IsForeignKey = false;
                colvarIsRead.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsRead);
                
                TableSchema.TableColumn colvarSubject = new TableSchema.TableColumn(schema);
                colvarSubject.ColumnName = "subject";
                colvarSubject.DataType = DbType.String;
                colvarSubject.MaxLength = 500;
                colvarSubject.AutoIncrement = false;
                colvarSubject.IsNullable = false;
                colvarSubject.IsPrimaryKey = false;
                colvarSubject.IsForeignKey = false;
                colvarSubject.IsReadOnly = false;
                
                schema.Columns.Add(colvarSubject);
                
                TableSchema.TableColumn colvarMemberId = new TableSchema.TableColumn(schema);
                colvarMemberId.ColumnName = "member_id";
                colvarMemberId.DataType = DbType.Int32;
                colvarMemberId.MaxLength = 0;
                colvarMemberId.AutoIncrement = false;
                colvarMemberId.IsNullable = true;
                colvarMemberId.IsPrimaryKey = false;
                colvarMemberId.IsForeignKey = false;
                colvarMemberId.IsReadOnly = false;
                
                schema.Columns.Add(colvarMemberId);
                
                TableSchema.TableColumn colvarIsRemove = new TableSchema.TableColumn(schema);
                colvarIsRemove.ColumnName = "is_remove";
                colvarIsRemove.DataType = DbType.Boolean;
                colvarIsRemove.MaxLength = 0;
                colvarIsRemove.AutoIncrement = false;
                colvarIsRemove.IsNullable = false;
                colvarIsRemove.IsPrimaryKey = false;
                colvarIsRemove.IsForeignKey = false;
                colvarIsRemove.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsRemove);
                
                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = false;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarCreateTime);
                
                TableSchema.TableColumn colvarBeaconType = new TableSchema.TableColumn(schema);
                colvarBeaconType.ColumnName = "beacon_type";
                colvarBeaconType.DataType = DbType.Int32;
                colvarBeaconType.MaxLength = 0;
                colvarBeaconType.AutoIncrement = false;
                colvarBeaconType.IsNullable = false;
                colvarBeaconType.IsPrimaryKey = false;
                colvarBeaconType.IsForeignKey = false;
                colvarBeaconType.IsReadOnly = false;
                
                schema.Columns.Add(colvarBeaconType);
                
                TableSchema.TableColumn colvarBusinessHourGuid = new TableSchema.TableColumn(schema);
                colvarBusinessHourGuid.ColumnName = "business_hour_guid";
                colvarBusinessHourGuid.DataType = DbType.Guid;
                colvarBusinessHourGuid.MaxLength = 0;
                colvarBusinessHourGuid.AutoIncrement = false;
                colvarBusinessHourGuid.IsNullable = false;
                colvarBusinessHourGuid.IsPrimaryKey = false;
                colvarBusinessHourGuid.IsForeignKey = false;
                colvarBusinessHourGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourGuid);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_skm_beacon_message_push_record",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewSkmBeaconMessagePushRecord()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewSkmBeaconMessagePushRecord(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewSkmBeaconMessagePushRecord(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewSkmBeaconMessagePushRecord(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id 
	    {
		    get
		    {
			    return GetColumnValue<int>("id");
		    }
            set 
		    {
			    SetColumnValue("id", value);
            }
        }
	      
        [XmlAttribute("IsRead")]
        [Bindable(true)]
        public bool IsRead 
	    {
		    get
		    {
			    return GetColumnValue<bool>("is_read");
		    }
            set 
		    {
			    SetColumnValue("is_read", value);
            }
        }
	      
        [XmlAttribute("Subject")]
        [Bindable(true)]
        public string Subject 
	    {
		    get
		    {
			    return GetColumnValue<string>("subject");
		    }
            set 
		    {
			    SetColumnValue("subject", value);
            }
        }
	      
        [XmlAttribute("MemberId")]
        [Bindable(true)]
        public int? MemberId 
	    {
		    get
		    {
			    return GetColumnValue<int?>("member_id");
		    }
            set 
		    {
			    SetColumnValue("member_id", value);
            }
        }
	      
        [XmlAttribute("IsRemove")]
        [Bindable(true)]
        public bool IsRemove 
	    {
		    get
		    {
			    return GetColumnValue<bool>("is_remove");
		    }
            set 
		    {
			    SetColumnValue("is_remove", value);
            }
        }
	      
        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime CreateTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("create_time");
		    }
            set 
		    {
			    SetColumnValue("create_time", value);
            }
        }
	      
        [XmlAttribute("BeaconType")]
        [Bindable(true)]
        public int BeaconType 
	    {
		    get
		    {
			    return GetColumnValue<int>("beacon_type");
		    }
            set 
		    {
			    SetColumnValue("beacon_type", value);
            }
        }
	      
        [XmlAttribute("BusinessHourGuid")]
        [Bindable(true)]
        public Guid BusinessHourGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("business_hour_guid");
		    }
            set 
		    {
			    SetColumnValue("business_hour_guid", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string Id = @"id";
            
            public static string IsRead = @"is_read";
            
            public static string Subject = @"subject";
            
            public static string MemberId = @"member_id";
            
            public static string IsRemove = @"is_remove";
            
            public static string CreateTime = @"create_time";
            
            public static string BeaconType = @"beacon_type";
            
            public static string BusinessHourGuid = @"business_hour_guid";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

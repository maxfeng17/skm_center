using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the BrandItemCategoryDependency class.
	/// </summary>
    [Serializable]
	public partial class BrandItemCategoryDependencyCollection : RepositoryList<BrandItemCategoryDependency, BrandItemCategoryDependencyCollection>
	{	   
		public BrandItemCategoryDependencyCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>BrandItemCategoryDependencyCollection</returns>
		public BrandItemCategoryDependencyCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                BrandItemCategoryDependency o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the brand_item_category_dependency table.
	/// </summary>
	[Serializable]
	public partial class BrandItemCategoryDependency : RepositoryRecord<BrandItemCategoryDependency>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public BrandItemCategoryDependency()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public BrandItemCategoryDependency(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("brand_item_category_dependency", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarBrandItemId = new TableSchema.TableColumn(schema);
				colvarBrandItemId.ColumnName = "brand_item_id";
				colvarBrandItemId.DataType = DbType.Int32;
				colvarBrandItemId.MaxLength = 0;
				colvarBrandItemId.AutoIncrement = false;
				colvarBrandItemId.IsNullable = false;
				colvarBrandItemId.IsPrimaryKey = false;
				colvarBrandItemId.IsForeignKey = false;
				colvarBrandItemId.IsReadOnly = false;
				colvarBrandItemId.DefaultSetting = @"";
				colvarBrandItemId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBrandItemId);
				
				TableSchema.TableColumn colvarBrandItemCategoryId = new TableSchema.TableColumn(schema);
				colvarBrandItemCategoryId.ColumnName = "brand_item_category_id";
				colvarBrandItemCategoryId.DataType = DbType.Int32;
				colvarBrandItemCategoryId.MaxLength = 0;
				colvarBrandItemCategoryId.AutoIncrement = false;
				colvarBrandItemCategoryId.IsNullable = false;
				colvarBrandItemCategoryId.IsPrimaryKey = false;
				colvarBrandItemCategoryId.IsForeignKey = false;
				colvarBrandItemCategoryId.IsReadOnly = false;
				colvarBrandItemCategoryId.DefaultSetting = @"";
				colvarBrandItemCategoryId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBrandItemCategoryId);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("brand_item_category_dependency",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("BrandItemId")]
		[Bindable(true)]
		public int BrandItemId 
		{
			get { return GetColumnValue<int>(Columns.BrandItemId); }
			set { SetColumnValue(Columns.BrandItemId, value); }
		}
		  
		[XmlAttribute("BrandItemCategoryId")]
		[Bindable(true)]
		public int BrandItemCategoryId 
		{
			get { return GetColumnValue<int>(Columns.BrandItemCategoryId); }
			set { SetColumnValue(Columns.BrandItemCategoryId, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn BrandItemIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn BrandItemCategoryIdColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string BrandItemId = @"brand_item_id";
			 public static string BrandItemCategoryId = @"brand_item_category_id";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

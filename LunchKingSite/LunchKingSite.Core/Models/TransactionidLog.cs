using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the TransactionidLog class.
	/// </summary>
    [Serializable]
	public partial class TransactionidLogCollection : RepositoryList<TransactionidLog, TransactionidLogCollection>
	{	   
		public TransactionidLogCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>TransactionidLogCollection</returns>
		public TransactionidLogCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                TransactionidLog o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the transactionid_log table.
	/// </summary>
	[Serializable]
	public partial class TransactionidLog : RepositoryRecord<TransactionidLog>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public TransactionidLog()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public TransactionidLog(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("transactionid_log", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarTransactionId = new TableSchema.TableColumn(schema);
				colvarTransactionId.ColumnName = "transaction_id";
				colvarTransactionId.DataType = DbType.String;
				colvarTransactionId.MaxLength = 40;
				colvarTransactionId.AutoIncrement = false;
				colvarTransactionId.IsNullable = false;
				colvarTransactionId.IsPrimaryKey = true;
				colvarTransactionId.IsForeignKey = false;
				colvarTransactionId.IsReadOnly = false;
				colvarTransactionId.DefaultSetting = @"";
				colvarTransactionId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTransactionId);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarServerName = new TableSchema.TableColumn(schema);
				colvarServerName.ColumnName = "server_name";
				colvarServerName.DataType = DbType.String;
				colvarServerName.MaxLength = 50;
				colvarServerName.AutoIncrement = false;
				colvarServerName.IsNullable = false;
				colvarServerName.IsPrimaryKey = false;
				colvarServerName.IsForeignKey = false;
				colvarServerName.IsReadOnly = false;
				colvarServerName.DefaultSetting = @"";
				colvarServerName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarServerName);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("transactionid_log",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("TransactionId")]
		[Bindable(true)]
		public string TransactionId 
		{
			get { return GetColumnValue<string>(Columns.TransactionId); }
			set { SetColumnValue(Columns.TransactionId, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		  
		[XmlAttribute("ServerName")]
		[Bindable(true)]
		public string ServerName 
		{
			get { return GetColumnValue<string>(Columns.ServerName); }
			set { SetColumnValue(Columns.ServerName, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn TransactionIdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn ServerNameColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string TransactionId = @"transaction_id";
			 public static string CreateTime = @"create_time";
			 public static string ServerName = @"server_name";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

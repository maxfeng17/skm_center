using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the ViewShipOrderStatus class.
    /// </summary>
    [Serializable]
    public partial class ViewShipOrderStatusCollection : ReadOnlyList<ViewShipOrderStatus, ViewShipOrderStatusCollection>
    {
        public ViewShipOrderStatusCollection() { }
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_ship_order_status view.
    /// </summary>
    [Serializable]
    public partial class ViewShipOrderStatus : ReadOnlyRecord<ViewShipOrderStatus>, IReadOnlyRecord
    {

        #region Default Settings
        protected static void SetSQLProps()
        {
            GetTableSchema();
        }
        #endregion
        #region Schema Accessor
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_ship_order_status", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarOrderId = new TableSchema.TableColumn(schema);
                colvarOrderId.ColumnName = "order_id";
                colvarOrderId.DataType = DbType.AnsiString;
                colvarOrderId.MaxLength = 30;
                colvarOrderId.AutoIncrement = false;
                colvarOrderId.IsNullable = true;
                colvarOrderId.IsPrimaryKey = false;
                colvarOrderId.IsForeignKey = false;
                colvarOrderId.IsReadOnly = false;

                schema.Columns.Add(colvarOrderId);

                TableSchema.TableColumn colvarUniqueId = new TableSchema.TableColumn(schema);
                colvarUniqueId.ColumnName = "unique_id";
                colvarUniqueId.DataType = DbType.Int32;
                colvarUniqueId.MaxLength = 0;
                colvarUniqueId.AutoIncrement = false;
                colvarUniqueId.IsNullable = true;
                colvarUniqueId.IsPrimaryKey = false;
                colvarUniqueId.IsForeignKey = false;
                colvarUniqueId.IsReadOnly = false;

                schema.Columns.Add(colvarUniqueId);

                TableSchema.TableColumn colvarMainUniqueId = new TableSchema.TableColumn(schema);
                colvarMainUniqueId.ColumnName = "main_unique_id";
                colvarMainUniqueId.DataType = DbType.Int32;
                colvarMainUniqueId.MaxLength = 0;
                colvarMainUniqueId.AutoIncrement = false;
                colvarMainUniqueId.IsNullable = true;
                colvarMainUniqueId.IsPrimaryKey = false;
                colvarMainUniqueId.IsForeignKey = false;
                colvarMainUniqueId.IsReadOnly = false;

                schema.Columns.Add(colvarMainUniqueId);

                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = false;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;

                schema.Columns.Add(colvarCreateTime);

                TableSchema.TableColumn colvarOrderSpec = new TableSchema.TableColumn(schema);
                colvarOrderSpec.ColumnName = "order_spec";
                colvarOrderSpec.DataType = DbType.String;
                colvarOrderSpec.MaxLength = -1;
                colvarOrderSpec.AutoIncrement = false;
                colvarOrderSpec.IsNullable = true;
                colvarOrderSpec.IsPrimaryKey = false;
                colvarOrderSpec.IsForeignKey = false;
                colvarOrderSpec.IsReadOnly = false;

                schema.Columns.Add(colvarOrderSpec);

                TableSchema.TableColumn colvarItemName = new TableSchema.TableColumn(schema);
                colvarItemName.ColumnName = "item_name";
                colvarItemName.DataType = DbType.String;
                colvarItemName.MaxLength = 750;
                colvarItemName.AutoIncrement = false;
                colvarItemName.IsNullable = false;
                colvarItemName.IsPrimaryKey = false;
                colvarItemName.IsForeignKey = false;
                colvarItemName.IsReadOnly = false;

                schema.Columns.Add(colvarItemName);

                TableSchema.TableColumn colvarComboPackCount = new TableSchema.TableColumn(schema);
                colvarComboPackCount.ColumnName = "combo_pack_count";
                colvarComboPackCount.DataType = DbType.Int32;
                colvarComboPackCount.MaxLength = 0;
                colvarComboPackCount.AutoIncrement = false;
                colvarComboPackCount.IsNullable = false;
                colvarComboPackCount.IsPrimaryKey = false;
                colvarComboPackCount.IsForeignKey = false;
                colvarComboPackCount.IsReadOnly = false;

                schema.Columns.Add(colvarComboPackCount);

                TableSchema.TableColumn colvarItemPrice = new TableSchema.TableColumn(schema);
                colvarItemPrice.ColumnName = "item_price";
                colvarItemPrice.DataType = DbType.Currency;
                colvarItemPrice.MaxLength = 0;
                colvarItemPrice.AutoIncrement = false;
                colvarItemPrice.IsNullable = false;
                colvarItemPrice.IsPrimaryKey = false;
                colvarItemPrice.IsForeignKey = false;
                colvarItemPrice.IsReadOnly = false;

                schema.Columns.Add(colvarItemPrice);

                TableSchema.TableColumn colvarMemberName = new TableSchema.TableColumn(schema);
                colvarMemberName.ColumnName = "member_name";
                colvarMemberName.DataType = DbType.String;
                colvarMemberName.MaxLength = 50;
                colvarMemberName.AutoIncrement = false;
                colvarMemberName.IsNullable = false;
                colvarMemberName.IsPrimaryKey = false;
                colvarMemberName.IsForeignKey = false;
                colvarMemberName.IsReadOnly = false;

                schema.Columns.Add(colvarMemberName);

                TableSchema.TableColumn colvarMobileNumber = new TableSchema.TableColumn(schema);
                colvarMobileNumber.ColumnName = "mobile_number";
                colvarMobileNumber.DataType = DbType.AnsiString;
                colvarMobileNumber.MaxLength = 50;
                colvarMobileNumber.AutoIncrement = false;
                colvarMobileNumber.IsNullable = true;
                colvarMobileNumber.IsPrimaryKey = false;
                colvarMobileNumber.IsForeignKey = false;
                colvarMobileNumber.IsReadOnly = false;

                schema.Columns.Add(colvarMobileNumber);

                TableSchema.TableColumn colvarDeliveryAddress = new TableSchema.TableColumn(schema);
                colvarDeliveryAddress.ColumnName = "delivery_address";
                colvarDeliveryAddress.DataType = DbType.String;
                colvarDeliveryAddress.MaxLength = 200;
                colvarDeliveryAddress.AutoIncrement = false;
                colvarDeliveryAddress.IsNullable = true;
                colvarDeliveryAddress.IsPrimaryKey = false;
                colvarDeliveryAddress.IsForeignKey = false;
                colvarDeliveryAddress.IsReadOnly = false;

                schema.Columns.Add(colvarDeliveryAddress);

                TableSchema.TableColumn colvarIsShipped = new TableSchema.TableColumn(schema);
                colvarIsShipped.ColumnName = "is_shipped";
                colvarIsShipped.DataType = DbType.Boolean;
                colvarIsShipped.MaxLength = 0;
                colvarIsShipped.AutoIncrement = false;
                colvarIsShipped.IsNullable = false;
                colvarIsShipped.IsPrimaryKey = false;
                colvarIsShipped.IsForeignKey = false;
                colvarIsShipped.IsReadOnly = false;

                schema.Columns.Add(colvarIsShipped);

                TableSchema.TableColumn colvarExchangeStatus = new TableSchema.TableColumn(schema);
                colvarExchangeStatus.ColumnName = "exchange_status";
                colvarExchangeStatus.DataType = DbType.Int32;
                colvarExchangeStatus.MaxLength = 0;
                colvarExchangeStatus.AutoIncrement = false;
                colvarExchangeStatus.IsNullable = true;
                colvarExchangeStatus.IsPrimaryKey = false;
                colvarExchangeStatus.IsForeignKey = false;
                colvarExchangeStatus.IsReadOnly = false;

                schema.Columns.Add(colvarExchangeStatus);

                TableSchema.TableColumn colvarVendorProgressStatus = new TableSchema.TableColumn(schema);
                colvarVendorProgressStatus.ColumnName = "vendor_progress_status";
                colvarVendorProgressStatus.DataType = DbType.Int32;
                colvarVendorProgressStatus.MaxLength = 0;
                colvarVendorProgressStatus.AutoIncrement = false;
                colvarVendorProgressStatus.IsNullable = true;
                colvarVendorProgressStatus.IsPrimaryKey = false;
                colvarVendorProgressStatus.IsForeignKey = false;
                colvarVendorProgressStatus.IsReadOnly = false;

                schema.Columns.Add(colvarVendorProgressStatus);

                TableSchema.TableColumn colvarSellerGuid = new TableSchema.TableColumn(schema);
                colvarSellerGuid.ColumnName = "seller_GUID";
                colvarSellerGuid.DataType = DbType.Guid;
                colvarSellerGuid.MaxLength = 0;
                colvarSellerGuid.AutoIncrement = false;
                colvarSellerGuid.IsNullable = false;
                colvarSellerGuid.IsPrimaryKey = false;
                colvarSellerGuid.IsForeignKey = false;
                colvarSellerGuid.IsReadOnly = false;

                schema.Columns.Add(colvarSellerGuid);

                TableSchema.TableColumn colvarOrderGuid = new TableSchema.TableColumn(schema);
                colvarOrderGuid.ColumnName = "order_guid";
                colvarOrderGuid.DataType = DbType.Guid;
                colvarOrderGuid.MaxLength = 0;
                colvarOrderGuid.AutoIncrement = false;
                colvarOrderGuid.IsNullable = false;
                colvarOrderGuid.IsPrimaryKey = false;
                colvarOrderGuid.IsForeignKey = false;
                colvarOrderGuid.IsReadOnly = false;

                schema.Columns.Add(colvarOrderGuid);

                TableSchema.TableColumn colvarParentOrderGuid = new TableSchema.TableColumn(schema);
                colvarParentOrderGuid.ColumnName = "parent_order_guid";
                colvarParentOrderGuid.DataType = DbType.Guid;
                colvarParentOrderGuid.MaxLength = 0;
                colvarParentOrderGuid.AutoIncrement = false;
                colvarParentOrderGuid.IsNullable = true;
                colvarParentOrderGuid.IsPrimaryKey = false;
                colvarParentOrderGuid.IsForeignKey = false;
                colvarParentOrderGuid.IsReadOnly = false;

                schema.Columns.Add(colvarParentOrderGuid);

                TableSchema.TableColumn colvarBusinessHourGuid = new TableSchema.TableColumn(schema);
                colvarBusinessHourGuid.ColumnName = "business_hour_guid";
                colvarBusinessHourGuid.DataType = DbType.Guid;
                colvarBusinessHourGuid.MaxLength = 0;
                colvarBusinessHourGuid.AutoIncrement = false;
                colvarBusinessHourGuid.IsNullable = false;
                colvarBusinessHourGuid.IsPrimaryKey = false;
                colvarBusinessHourGuid.IsForeignKey = false;
                colvarBusinessHourGuid.IsReadOnly = false;

                schema.Columns.Add(colvarBusinessHourGuid);

                TableSchema.TableColumn colvarMainBusinessHourGuid = new TableSchema.TableColumn(schema);
                colvarMainBusinessHourGuid.ColumnName = "main_business_hour_guid";
                colvarMainBusinessHourGuid.DataType = DbType.Guid;
                colvarMainBusinessHourGuid.MaxLength = 0;
                colvarMainBusinessHourGuid.AutoIncrement = false;
                colvarMainBusinessHourGuid.IsNullable = true;
                colvarMainBusinessHourGuid.IsPrimaryKey = false;
                colvarMainBusinessHourGuid.IsForeignKey = false;
                colvarMainBusinessHourGuid.IsReadOnly = false;

                schema.Columns.Add(colvarMainBusinessHourGuid);

                TableSchema.TableColumn colvarIsComboDeal = new TableSchema.TableColumn(schema);
                colvarIsComboDeal.ColumnName = "is_combo_deal";
                colvarIsComboDeal.DataType = DbType.Int32;
                colvarIsComboDeal.MaxLength = 0;
                colvarIsComboDeal.AutoIncrement = false;
                colvarIsComboDeal.IsNullable = false;
                colvarIsComboDeal.IsPrimaryKey = false;
                colvarIsComboDeal.IsForeignKey = false;
                colvarIsComboDeal.IsReadOnly = false;

                schema.Columns.Add(colvarIsComboDeal);

                TableSchema.TableColumn colvarCsmOrderId = new TableSchema.TableColumn(schema);
                colvarCsmOrderId.ColumnName = "csm_order_id";
                colvarCsmOrderId.DataType = DbType.AnsiString;
                colvarCsmOrderId.MaxLength = 30;
                colvarCsmOrderId.AutoIncrement = false;
                colvarCsmOrderId.IsNullable = true;
                colvarCsmOrderId.IsPrimaryKey = false;
                colvarCsmOrderId.IsForeignKey = false;
                colvarCsmOrderId.IsReadOnly = false;

                schema.Columns.Add(colvarCsmOrderId);

                TableSchema.TableColumn colvarAccountId = new TableSchema.TableColumn(schema);
                colvarAccountId.ColumnName = "account_id";
                colvarAccountId.DataType = DbType.String;
                colvarAccountId.MaxLength = 256;
                colvarAccountId.AutoIncrement = false;
                colvarAccountId.IsNullable = false;
                colvarAccountId.IsPrimaryKey = false;
                colvarAccountId.IsForeignKey = false;
                colvarAccountId.IsReadOnly = false;

                schema.Columns.Add(colvarAccountId);


                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_ship_order_status", schema);
            }
        }
        #endregion

        #region Query Accessor
        public static Query CreateQuery()
        {
            return new Query(Schema);
        }
        #endregion

        #region .ctors
        public ViewShipOrderStatus()
        {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewShipOrderStatus(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
            {
                ForceDefaults();
            }
            MarkNew();
        }

        public ViewShipOrderStatus(object keyID)
        {
            SetSQLProps();
            LoadByKey(keyID);
        }

        public ViewShipOrderStatus(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName, columnValue);
        }

        #endregion

        #region Props


        [XmlAttribute("OrderId")]
        [Bindable(true)]
        public string OrderId
        {
            get
            {
                return GetColumnValue<string>("order_id");
            }
            set
            {
                SetColumnValue("order_id", value);
            }
        }

        [XmlAttribute("UniqueId")]
        [Bindable(true)]
        public int? UniqueId
        {
            get
            {
                return GetColumnValue<int?>("unique_id");
            }
            set
            {
                SetColumnValue("unique_id", value);
            }
        }

        [XmlAttribute("MainUniqueId")]
        [Bindable(true)]
        public int? MainUniqueId
        {
            get
            {
                return GetColumnValue<int?>("main_unique_id");
            }
            set
            {
                SetColumnValue("main_unique_id", value);
            }
        }

        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime CreateTime
        {
            get
            {
                return GetColumnValue<DateTime>("create_time");
            }
            set
            {
                SetColumnValue("create_time", value);
            }
        }

        [XmlAttribute("OrderSpec")]
        [Bindable(true)]
        public string OrderSpec
        {
            get
            {
                return GetColumnValue<string>("order_spec");
            }
            set
            {
                SetColumnValue("order_spec", value);
            }
        }

        [XmlAttribute("ItemName")]
        [Bindable(true)]
        public string ItemName
        {
            get
            {
                return GetColumnValue<string>("item_name");
            }
            set
            {
                SetColumnValue("item_name", value);
            }
        }

        [XmlAttribute("ComboPackCount")]
        [Bindable(true)]
        public int ComboPackCount
        {
            get
            {
                return GetColumnValue<int>("combo_pack_count");
            }
            set
            {
                SetColumnValue("combo_pack_count", value);
            }
        }

        [XmlAttribute("ItemPrice")]
        [Bindable(true)]
        public decimal ItemPrice
        {
            get
            {
                return GetColumnValue<decimal>("item_price");
            }
            set
            {
                SetColumnValue("item_price", value);
            }
        }

        [XmlAttribute("MemberName")]
        [Bindable(true)]
        public string MemberName
        {
            get
            {
                return GetColumnValue<string>("member_name");
            }
            set
            {
                SetColumnValue("member_name", value);
            }
        }

        [XmlAttribute("MobileNumber")]
        [Bindable(true)]
        public string MobileNumber
        {
            get
            {
                return GetColumnValue<string>("mobile_number");
            }
            set
            {
                SetColumnValue("mobile_number", value);
            }
        }

        [XmlAttribute("DeliveryAddress")]
        [Bindable(true)]
        public string DeliveryAddress
        {
            get
            {
                return GetColumnValue<string>("delivery_address");
            }
            set
            {
                SetColumnValue("delivery_address", value);
            }
        }

        [XmlAttribute("IsShipped")]
        [Bindable(true)]
        public bool IsShipped
        {
            get
            {
                return GetColumnValue<bool>("is_shipped");
            }
            set
            {
                SetColumnValue("is_shipped", value);
            }
        }

        [XmlAttribute("ExchangeStatus")]
        [Bindable(true)]
        public int? ExchangeStatus
        {
            get
            {
                return GetColumnValue<int?>("exchange_status");
            }
            set
            {
                SetColumnValue("exchange_status", value);
            }
        }

        [XmlAttribute("VendorProgressStatus")]
        [Bindable(true)]
        public int? VendorProgressStatus
        {
            get
            {
                return GetColumnValue<int?>("vendor_progress_status");
            }
            set
            {
                SetColumnValue("vendor_progress_status", value);
            }
        }

        [XmlAttribute("SellerGuid")]
        [Bindable(true)]
        public Guid SellerGuid
        {
            get
            {
                return GetColumnValue<Guid>("seller_GUID");
            }
            set
            {
                SetColumnValue("seller_GUID", value);
            }
        }

        [XmlAttribute("OrderGuid")]
        [Bindable(true)]
        public Guid OrderGuid
        {
            get
            {
                return GetColumnValue<Guid>("order_guid");
            }
            set
            {
                SetColumnValue("order_guid", value);
            }
        }

        [XmlAttribute("ParentOrderGuid")]
        [Bindable(true)]
        public Guid? ParentOrderGuid
        {
            get
            {
                return GetColumnValue<Guid?>("parent_order_guid");
            }
            set
            {
                SetColumnValue("parent_order_guid", value);
            }
        }

        [XmlAttribute("BusinessHourGuid")]
        [Bindable(true)]
        public Guid BusinessHourGuid
        {
            get
            {
                return GetColumnValue<Guid>("business_hour_guid");
            }
            set
            {
                SetColumnValue("business_hour_guid", value);
            }
        }

        [XmlAttribute("MainBusinessHourGuid")]
        [Bindable(true)]
        public Guid? MainBusinessHourGuid
        {
            get
            {
                return GetColumnValue<Guid?>("main_business_hour_guid");
            }
            set
            {
                SetColumnValue("main_business_hour_guid", value);
            }
        }

        [XmlAttribute("IsComboDeal")]
        [Bindable(true)]
        public int IsComboDeal
        {
            get
            {
                return GetColumnValue<int>("is_combo_deal");
            }
            set
            {
                SetColumnValue("is_combo_deal", value);
            }
        }

        [XmlAttribute("CsmOrderId")]
        [Bindable(true)]
        public string CsmOrderId
        {
            get
            {
                return GetColumnValue<string>("csm_order_id");
            }
            set
            {
                SetColumnValue("csm_order_id", value);
            }
        }

        [XmlAttribute("AccountId")]
        [Bindable(true)]
        public string AccountId
        {
            get
            {
                return GetColumnValue<string>("account_id");
            }
            set
            {
                SetColumnValue("account_id", value);
            }
        }

        #endregion

        #region Columns Struct
        public struct Columns
        {


            public static string OrderId = @"order_id";

            public static string UniqueId = @"unique_id";

            public static string MainUniqueId = @"main_unique_id";

            public static string CreateTime = @"create_time";

            public static string OrderSpec = @"order_spec";

            public static string ItemName = @"item_name";

            public static string ComboPackCount = @"combo_pack_count";

            public static string ItemPrice = @"item_price";

            public static string MemberName = @"member_name";

            public static string MobileNumber = @"mobile_number";

            public static string DeliveryAddress = @"delivery_address";

            public static string IsShipped = @"is_shipped";

            public static string ExchangeStatus = @"exchange_status";

            public static string VendorProgressStatus = @"vendor_progress_status";

            public static string SellerGuid = @"seller_GUID";

            public static string OrderGuid = @"order_guid";

            public static string ParentOrderGuid = @"parent_order_guid";

            public static string BusinessHourGuid = @"business_hour_guid";

            public static string MainBusinessHourGuid = @"main_business_hour_guid";

            public static string IsComboDeal = @"is_combo_deal";

            public static string CsmOrderId = @"csm_order_id";

            public static string AccountId = @"account_id";

        }
        #endregion


        #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName)
        {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName)
        {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion

    }
}

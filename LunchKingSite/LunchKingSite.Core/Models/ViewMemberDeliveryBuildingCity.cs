using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewMemberDeliveryBuildingCity class.
    /// </summary>
    [Serializable]
    public partial class ViewMemberDeliveryBuildingCityCollection : ReadOnlyList<ViewMemberDeliveryBuildingCity, ViewMemberDeliveryBuildingCityCollection>
    {        
        public ViewMemberDeliveryBuildingCityCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_member_delivery_building_city view.
    /// </summary>
    [Serializable]
    public partial class ViewMemberDeliveryBuildingCity : ReadOnlyRecord<ViewMemberDeliveryBuildingCity>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_member_delivery_building_city", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = false;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = false;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                
                schema.Columns.Add(colvarId);
                
                TableSchema.TableColumn colvarUserName = new TableSchema.TableColumn(schema);
                colvarUserName.ColumnName = "user_name";
                colvarUserName.DataType = DbType.String;
                colvarUserName.MaxLength = 256;
                colvarUserName.AutoIncrement = false;
                colvarUserName.IsNullable = false;
                colvarUserName.IsPrimaryKey = false;
                colvarUserName.IsForeignKey = false;
                colvarUserName.IsReadOnly = false;
                
                schema.Columns.Add(colvarUserName);
                
                TableSchema.TableColumn colvarAddressAlias = new TableSchema.TableColumn(schema);
                colvarAddressAlias.ColumnName = "address_Alias";
                colvarAddressAlias.DataType = DbType.String;
                colvarAddressAlias.MaxLength = 100;
                colvarAddressAlias.AutoIncrement = false;
                colvarAddressAlias.IsNullable = true;
                colvarAddressAlias.IsPrimaryKey = false;
                colvarAddressAlias.IsForeignKey = false;
                colvarAddressAlias.IsReadOnly = false;
                
                schema.Columns.Add(colvarAddressAlias);
                
                TableSchema.TableColumn colvarBuildingGuid = new TableSchema.TableColumn(schema);
                colvarBuildingGuid.ColumnName = "building_GUID";
                colvarBuildingGuid.DataType = DbType.Guid;
                colvarBuildingGuid.MaxLength = 0;
                colvarBuildingGuid.AutoIncrement = false;
                colvarBuildingGuid.IsNullable = false;
                colvarBuildingGuid.IsPrimaryKey = false;
                colvarBuildingGuid.IsForeignKey = false;
                colvarBuildingGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarBuildingGuid);
                
                TableSchema.TableColumn colvarTel = new TableSchema.TableColumn(schema);
                colvarTel.ColumnName = "Tel";
                colvarTel.DataType = DbType.AnsiString;
                colvarTel.MaxLength = 20;
                colvarTel.AutoIncrement = false;
                colvarTel.IsNullable = true;
                colvarTel.IsPrimaryKey = false;
                colvarTel.IsForeignKey = false;
                colvarTel.IsReadOnly = false;
                
                schema.Columns.Add(colvarTel);
                
                TableSchema.TableColumn colvarExt = new TableSchema.TableColumn(schema);
                colvarExt.ColumnName = "ext";
                colvarExt.DataType = DbType.AnsiString;
                colvarExt.MaxLength = 50;
                colvarExt.AutoIncrement = false;
                colvarExt.IsNullable = true;
                colvarExt.IsPrimaryKey = false;
                colvarExt.IsForeignKey = false;
                colvarExt.IsReadOnly = false;
                
                schema.Columns.Add(colvarExt);
                
                TableSchema.TableColumn colvarMobile = new TableSchema.TableColumn(schema);
                colvarMobile.ColumnName = "mobile";
                colvarMobile.DataType = DbType.AnsiString;
                colvarMobile.MaxLength = 50;
                colvarMobile.AutoIncrement = false;
                colvarMobile.IsNullable = true;
                colvarMobile.IsPrimaryKey = false;
                colvarMobile.IsForeignKey = false;
                colvarMobile.IsReadOnly = false;
                
                schema.Columns.Add(colvarMobile);
                
                TableSchema.TableColumn colvarAddress = new TableSchema.TableColumn(schema);
                colvarAddress.ColumnName = "address";
                colvarAddress.DataType = DbType.String;
                colvarAddress.MaxLength = 100;
                colvarAddress.AutoIncrement = false;
                colvarAddress.IsNullable = true;
                colvarAddress.IsPrimaryKey = false;
                colvarAddress.IsForeignKey = false;
                colvarAddress.IsReadOnly = false;
                
                schema.Columns.Add(colvarAddress);
                
                TableSchema.TableColumn colvarMemo = new TableSchema.TableColumn(schema);
                colvarMemo.ColumnName = "memo";
                colvarMemo.DataType = DbType.String;
                colvarMemo.MaxLength = 200;
                colvarMemo.AutoIncrement = false;
                colvarMemo.IsNullable = true;
                colvarMemo.IsPrimaryKey = false;
                colvarMemo.IsForeignKey = false;
                colvarMemo.IsReadOnly = false;
                
                schema.Columns.Add(colvarMemo);
                
                TableSchema.TableColumn colvarBuildingName = new TableSchema.TableColumn(schema);
                colvarBuildingName.ColumnName = "building_Name";
                colvarBuildingName.DataType = DbType.String;
                colvarBuildingName.MaxLength = 50;
                colvarBuildingName.AutoIncrement = false;
                colvarBuildingName.IsNullable = true;
                colvarBuildingName.IsPrimaryKey = false;
                colvarBuildingName.IsForeignKey = false;
                colvarBuildingName.IsReadOnly = false;
                
                schema.Columns.Add(colvarBuildingName);
                
                TableSchema.TableColumn colvarSection = new TableSchema.TableColumn(schema);
                colvarSection.ColumnName = "section";
                colvarSection.DataType = DbType.String;
                colvarSection.MaxLength = 50;
                colvarSection.AutoIncrement = false;
                colvarSection.IsNullable = true;
                colvarSection.IsPrimaryKey = false;
                colvarSection.IsForeignKey = false;
                colvarSection.IsReadOnly = false;
                
                schema.Columns.Add(colvarSection);
                
                TableSchema.TableColumn colvarIsDefault = new TableSchema.TableColumn(schema);
                colvarIsDefault.ColumnName = "IsDefault";
                colvarIsDefault.DataType = DbType.Boolean;
                colvarIsDefault.MaxLength = 0;
                colvarIsDefault.AutoIncrement = false;
                colvarIsDefault.IsNullable = true;
                colvarIsDefault.IsPrimaryKey = false;
                colvarIsDefault.IsForeignKey = false;
                colvarIsDefault.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsDefault);
                
                TableSchema.TableColumn colvarCityId = new TableSchema.TableColumn(schema);
                colvarCityId.ColumnName = "city_id";
                colvarCityId.DataType = DbType.Int32;
                colvarCityId.MaxLength = 0;
                colvarCityId.AutoIncrement = false;
                colvarCityId.IsNullable = false;
                colvarCityId.IsPrimaryKey = false;
                colvarCityId.IsForeignKey = false;
                colvarCityId.IsReadOnly = false;
                
                schema.Columns.Add(colvarCityId);
                
                TableSchema.TableColumn colvarParentId = new TableSchema.TableColumn(schema);
                colvarParentId.ColumnName = "parent_id";
                colvarParentId.DataType = DbType.Int32;
                colvarParentId.MaxLength = 0;
                colvarParentId.AutoIncrement = false;
                colvarParentId.IsNullable = true;
                colvarParentId.IsPrimaryKey = false;
                colvarParentId.IsForeignKey = false;
                colvarParentId.IsReadOnly = false;
                
                schema.Columns.Add(colvarParentId);
                
                TableSchema.TableColumn colvarContactName = new TableSchema.TableColumn(schema);
                colvarContactName.ColumnName = "contact_name";
                colvarContactName.DataType = DbType.String;
                colvarContactName.MaxLength = 50;
                colvarContactName.AutoIncrement = false;
                colvarContactName.IsNullable = true;
                colvarContactName.IsPrimaryKey = false;
                colvarContactName.IsForeignKey = false;
                colvarContactName.IsReadOnly = false;
                
                schema.Columns.Add(colvarContactName);
                
                TableSchema.TableColumn colvarCityName = new TableSchema.TableColumn(schema);
                colvarCityName.ColumnName = "city_name";
                colvarCityName.DataType = DbType.String;
                colvarCityName.MaxLength = 20;
                colvarCityName.AutoIncrement = false;
                colvarCityName.IsNullable = false;
                colvarCityName.IsPrimaryKey = false;
                colvarCityName.IsForeignKey = false;
                colvarCityName.IsReadOnly = false;
                
                schema.Columns.Add(colvarCityName);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_member_delivery_building_city",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewMemberDeliveryBuildingCity()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewMemberDeliveryBuildingCity(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewMemberDeliveryBuildingCity(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewMemberDeliveryBuildingCity(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id 
	    {
		    get
		    {
			    return GetColumnValue<int>("id");
		    }
            set 
		    {
			    SetColumnValue("id", value);
            }
        }
	      
        [XmlAttribute("UserName")]
        [Bindable(true)]
        public string UserName 
	    {
		    get
		    {
			    return GetColumnValue<string>("user_name");
		    }
            set 
		    {
			    SetColumnValue("user_name", value);
            }
        }
	      
        [XmlAttribute("AddressAlias")]
        [Bindable(true)]
        public string AddressAlias 
	    {
		    get
		    {
			    return GetColumnValue<string>("address_Alias");
		    }
            set 
		    {
			    SetColumnValue("address_Alias", value);
            }
        }
	      
        [XmlAttribute("BuildingGuid")]
        [Bindable(true)]
        public Guid BuildingGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("building_GUID");
		    }
            set 
		    {
			    SetColumnValue("building_GUID", value);
            }
        }
	      
        [XmlAttribute("Tel")]
        [Bindable(true)]
        public string Tel 
	    {
		    get
		    {
			    return GetColumnValue<string>("Tel");
		    }
            set 
		    {
			    SetColumnValue("Tel", value);
            }
        }
	      
        [XmlAttribute("Ext")]
        [Bindable(true)]
        public string Ext 
	    {
		    get
		    {
			    return GetColumnValue<string>("ext");
		    }
            set 
		    {
			    SetColumnValue("ext", value);
            }
        }
	      
        [XmlAttribute("Mobile")]
        [Bindable(true)]
        public string Mobile 
	    {
		    get
		    {
			    return GetColumnValue<string>("mobile");
		    }
            set 
		    {
			    SetColumnValue("mobile", value);
            }
        }
	      
        [XmlAttribute("Address")]
        [Bindable(true)]
        public string Address 
	    {
		    get
		    {
			    return GetColumnValue<string>("address");
		    }
            set 
		    {
			    SetColumnValue("address", value);
            }
        }
	      
        [XmlAttribute("Memo")]
        [Bindable(true)]
        public string Memo 
	    {
		    get
		    {
			    return GetColumnValue<string>("memo");
		    }
            set 
		    {
			    SetColumnValue("memo", value);
            }
        }
	      
        [XmlAttribute("BuildingName")]
        [Bindable(true)]
        public string BuildingName 
	    {
		    get
		    {
			    return GetColumnValue<string>("building_Name");
		    }
            set 
		    {
			    SetColumnValue("building_Name", value);
            }
        }
	      
        [XmlAttribute("Section")]
        [Bindable(true)]
        public string Section 
	    {
		    get
		    {
			    return GetColumnValue<string>("section");
		    }
            set 
		    {
			    SetColumnValue("section", value);
            }
        }
	      
        [XmlAttribute("IsDefault")]
        [Bindable(true)]
        public bool? IsDefault 
	    {
		    get
		    {
			    return GetColumnValue<bool?>("IsDefault");
		    }
            set 
		    {
			    SetColumnValue("IsDefault", value);
            }
        }
	      
        [XmlAttribute("CityId")]
        [Bindable(true)]
        public int CityId 
	    {
		    get
		    {
			    return GetColumnValue<int>("city_id");
		    }
            set 
		    {
			    SetColumnValue("city_id", value);
            }
        }
	      
        [XmlAttribute("ParentId")]
        [Bindable(true)]
        public int? ParentId 
	    {
		    get
		    {
			    return GetColumnValue<int?>("parent_id");
		    }
            set 
		    {
			    SetColumnValue("parent_id", value);
            }
        }
	      
        [XmlAttribute("ContactName")]
        [Bindable(true)]
        public string ContactName 
	    {
		    get
		    {
			    return GetColumnValue<string>("contact_name");
		    }
            set 
		    {
			    SetColumnValue("contact_name", value);
            }
        }
	      
        [XmlAttribute("CityName")]
        [Bindable(true)]
        public string CityName 
	    {
		    get
		    {
			    return GetColumnValue<string>("city_name");
		    }
            set 
		    {
			    SetColumnValue("city_name", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string Id = @"id";
            
            public static string UserName = @"user_name";
            
            public static string AddressAlias = @"address_Alias";
            
            public static string BuildingGuid = @"building_GUID";
            
            public static string Tel = @"Tel";
            
            public static string Ext = @"ext";
            
            public static string Mobile = @"mobile";
            
            public static string Address = @"address";
            
            public static string Memo = @"memo";
            
            public static string BuildingName = @"building_Name";
            
            public static string Section = @"section";
            
            public static string IsDefault = @"IsDefault";
            
            public static string CityId = @"city_id";
            
            public static string ParentId = @"parent_id";
            
            public static string ContactName = @"contact_name";
            
            public static string CityName = @"city_name";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the LinePayRefundLog class.
	/// </summary>
    [Serializable]
	public partial class LinePayRefundLogCollection : RepositoryList<LinePayRefundLog, LinePayRefundLogCollection>
	{	   
		public LinePayRefundLogCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>LinePayRefundLogCollection</returns>
		public LinePayRefundLogCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                LinePayRefundLog o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the line_pay_refund_log table.
	/// </summary>
	[Serializable]
	public partial class LinePayRefundLog : RepositoryRecord<LinePayRefundLog>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public LinePayRefundLog()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public LinePayRefundLog(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("line_pay_refund_log", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarLinePayTransLogId = new TableSchema.TableColumn(schema);
				colvarLinePayTransLogId.ColumnName = "line_pay_trans_log_id";
				colvarLinePayTransLogId.DataType = DbType.Int32;
				colvarLinePayTransLogId.MaxLength = 0;
				colvarLinePayTransLogId.AutoIncrement = false;
				colvarLinePayTransLogId.IsNullable = false;
				colvarLinePayTransLogId.IsPrimaryKey = true;
				colvarLinePayTransLogId.IsForeignKey = false;
				colvarLinePayTransLogId.IsReadOnly = false;
				colvarLinePayTransLogId.DefaultSetting = @"";
				colvarLinePayTransLogId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarLinePayTransLogId);
				
				TableSchema.TableColumn colvarPaymentTransactionId = new TableSchema.TableColumn(schema);
				colvarPaymentTransactionId.ColumnName = "payment_transaction_id";
				colvarPaymentTransactionId.DataType = DbType.Int32;
				colvarPaymentTransactionId.MaxLength = 0;
				colvarPaymentTransactionId.AutoIncrement = false;
				colvarPaymentTransactionId.IsNullable = false;
				colvarPaymentTransactionId.IsPrimaryKey = true;
				colvarPaymentTransactionId.IsForeignKey = false;
				colvarPaymentTransactionId.IsReadOnly = false;
				colvarPaymentTransactionId.DefaultSetting = @"";
				colvarPaymentTransactionId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPaymentTransactionId);
				
				TableSchema.TableColumn colvarRefundStatus = new TableSchema.TableColumn(schema);
				colvarRefundStatus.ColumnName = "refund_status";
				colvarRefundStatus.DataType = DbType.Byte;
				colvarRefundStatus.MaxLength = 0;
				colvarRefundStatus.AutoIncrement = false;
				colvarRefundStatus.IsNullable = false;
				colvarRefundStatus.IsPrimaryKey = false;
				colvarRefundStatus.IsForeignKey = false;
				colvarRefundStatus.IsReadOnly = false;
				
						colvarRefundStatus.DefaultSetting = @"((0))";
				colvarRefundStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarRefundStatus);
				
				TableSchema.TableColumn colvarRefundMsg = new TableSchema.TableColumn(schema);
				colvarRefundMsg.ColumnName = "refund_msg";
				colvarRefundMsg.DataType = DbType.String;
				colvarRefundMsg.MaxLength = 100;
				colvarRefundMsg.AutoIncrement = false;
				colvarRefundMsg.IsNullable = false;
				colvarRefundMsg.IsPrimaryKey = false;
				colvarRefundMsg.IsForeignKey = false;
				colvarRefundMsg.IsReadOnly = false;
				
						colvarRefundMsg.DefaultSetting = @"('')";
				colvarRefundMsg.ForeignKeyTableName = "";
				schema.Columns.Add(colvarRefundMsg);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				
						colvarCreateTime.DefaultSetting = @"(getdate())";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
				colvarModifyTime.ColumnName = "modify_time";
				colvarModifyTime.DataType = DbType.DateTime;
				colvarModifyTime.MaxLength = 0;
				colvarModifyTime.AutoIncrement = false;
				colvarModifyTime.IsNullable = false;
				colvarModifyTime.IsPrimaryKey = false;
				colvarModifyTime.IsForeignKey = false;
				colvarModifyTime.IsReadOnly = false;
				
						colvarModifyTime.DefaultSetting = @"(getdate())";
				colvarModifyTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyTime);
				
				TableSchema.TableColumn colvarRetryTime = new TableSchema.TableColumn(schema);
				colvarRetryTime.ColumnName = "retry_time";
				colvarRetryTime.DataType = DbType.Int32;
				colvarRetryTime.MaxLength = 0;
				colvarRetryTime.AutoIncrement = false;
				colvarRetryTime.IsNullable = false;
				colvarRetryTime.IsPrimaryKey = false;
				colvarRetryTime.IsForeignKey = false;
				colvarRetryTime.IsReadOnly = false;
				
						colvarRetryTime.DefaultSetting = @"((0))";
				colvarRetryTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarRetryTime);
				
				TableSchema.TableColumn colvarModifyUserId = new TableSchema.TableColumn(schema);
				colvarModifyUserId.ColumnName = "modify_user_id";
				colvarModifyUserId.DataType = DbType.AnsiString;
				colvarModifyUserId.MaxLength = 100;
				colvarModifyUserId.AutoIncrement = false;
				colvarModifyUserId.IsNullable = false;
				colvarModifyUserId.IsPrimaryKey = false;
				colvarModifyUserId.IsForeignKey = false;
				colvarModifyUserId.IsReadOnly = false;
				
						colvarModifyUserId.DefaultSetting = @"('')";
				colvarModifyUserId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyUserId);
				
				TableSchema.TableColumn colvarReturnFormId = new TableSchema.TableColumn(schema);
				colvarReturnFormId.ColumnName = "return_form_id";
				colvarReturnFormId.DataType = DbType.Int32;
				colvarReturnFormId.MaxLength = 0;
				colvarReturnFormId.AutoIncrement = false;
				colvarReturnFormId.IsNullable = false;
				colvarReturnFormId.IsPrimaryKey = false;
				colvarReturnFormId.IsForeignKey = false;
				colvarReturnFormId.IsReadOnly = false;
				
						colvarReturnFormId.DefaultSetting = @"((0))";
				colvarReturnFormId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReturnFormId);
				
				TableSchema.TableColumn colvarTrustId = new TableSchema.TableColumn(schema);
				colvarTrustId.ColumnName = "trust_id";
				colvarTrustId.DataType = DbType.Guid;
				colvarTrustId.MaxLength = 0;
				colvarTrustId.AutoIncrement = false;
				colvarTrustId.IsNullable = false;
				colvarTrustId.IsPrimaryKey = false;
				colvarTrustId.IsForeignKey = false;
				colvarTrustId.IsReadOnly = false;
				colvarTrustId.DefaultSetting = @"";
				colvarTrustId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTrustId);
				
				TableSchema.TableColumn colvarCanRetry = new TableSchema.TableColumn(schema);
				colvarCanRetry.ColumnName = "can_retry";
				colvarCanRetry.DataType = DbType.Boolean;
				colvarCanRetry.MaxLength = 0;
				colvarCanRetry.AutoIncrement = false;
				colvarCanRetry.IsNullable = false;
				colvarCanRetry.IsPrimaryKey = false;
				colvarCanRetry.IsForeignKey = false;
				colvarCanRetry.IsReadOnly = false;
				
						colvarCanRetry.DefaultSetting = @"((0))";
				colvarCanRetry.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCanRetry);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("line_pay_refund_log",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("LinePayTransLogId")]
		[Bindable(true)]
		public int LinePayTransLogId 
		{
			get { return GetColumnValue<int>(Columns.LinePayTransLogId); }
			set { SetColumnValue(Columns.LinePayTransLogId, value); }
		}
		  
		[XmlAttribute("PaymentTransactionId")]
		[Bindable(true)]
		public int PaymentTransactionId 
		{
			get { return GetColumnValue<int>(Columns.PaymentTransactionId); }
			set { SetColumnValue(Columns.PaymentTransactionId, value); }
		}
		  
		[XmlAttribute("RefundStatus")]
		[Bindable(true)]
		public byte RefundStatus 
		{
			get { return GetColumnValue<byte>(Columns.RefundStatus); }
			set { SetColumnValue(Columns.RefundStatus, value); }
		}
		  
		[XmlAttribute("RefundMsg")]
		[Bindable(true)]
		public string RefundMsg 
		{
			get { return GetColumnValue<string>(Columns.RefundMsg); }
			set { SetColumnValue(Columns.RefundMsg, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		  
		[XmlAttribute("ModifyTime")]
		[Bindable(true)]
		public DateTime ModifyTime 
		{
			get { return GetColumnValue<DateTime>(Columns.ModifyTime); }
			set { SetColumnValue(Columns.ModifyTime, value); }
		}
		  
		[XmlAttribute("RetryTime")]
		[Bindable(true)]
		public int RetryTime 
		{
			get { return GetColumnValue<int>(Columns.RetryTime); }
			set { SetColumnValue(Columns.RetryTime, value); }
		}
		  
		[XmlAttribute("ModifyUserId")]
		[Bindable(true)]
		public string ModifyUserId 
		{
			get { return GetColumnValue<string>(Columns.ModifyUserId); }
			set { SetColumnValue(Columns.ModifyUserId, value); }
		}
		  
		[XmlAttribute("ReturnFormId")]
		[Bindable(true)]
		public int ReturnFormId 
		{
			get { return GetColumnValue<int>(Columns.ReturnFormId); }
			set { SetColumnValue(Columns.ReturnFormId, value); }
		}
		  
		[XmlAttribute("TrustId")]
		[Bindable(true)]
		public Guid TrustId 
		{
			get { return GetColumnValue<Guid>(Columns.TrustId); }
			set { SetColumnValue(Columns.TrustId, value); }
		}
		  
		[XmlAttribute("CanRetry")]
		[Bindable(true)]
		public bool CanRetry 
		{
			get { return GetColumnValue<bool>(Columns.CanRetry); }
			set { SetColumnValue(Columns.CanRetry, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn LinePayTransLogIdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn PaymentTransactionIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn RefundStatusColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn RefundMsgColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn RetryTimeColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyUserIdColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn ReturnFormIdColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn TrustIdColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn CanRetryColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string LinePayTransLogId = @"line_pay_trans_log_id";
			 public static string PaymentTransactionId = @"payment_transaction_id";
			 public static string RefundStatus = @"refund_status";
			 public static string RefundMsg = @"refund_msg";
			 public static string CreateTime = @"create_time";
			 public static string ModifyTime = @"modify_time";
			 public static string RetryTime = @"retry_time";
			 public static string ModifyUserId = @"modify_user_id";
			 public static string ReturnFormId = @"return_form_id";
			 public static string TrustId = @"trust_id";
			 public static string CanRetry = @"can_retry";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the BiTraffic class.
	/// </summary>
    [Serializable]
	public partial class BiTrafficCollection : RepositoryList<BiTraffic, BiTrafficCollection>
	{	   
		public BiTrafficCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>BiTrafficCollection</returns>
		public BiTrafficCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                BiTraffic o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the bi_traffics table.
	/// </summary>
	[Serializable]
	public partial class BiTraffic : RepositoryRecord<BiTraffic>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public BiTraffic()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public BiTraffic(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("bi_traffics", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarTrafficesDate = new TableSchema.TableColumn(schema);
				colvarTrafficesDate.ColumnName = "traffices_date";
				colvarTrafficesDate.DataType = DbType.DateTime;
				colvarTrafficesDate.MaxLength = 0;
				colvarTrafficesDate.AutoIncrement = false;
				colvarTrafficesDate.IsNullable = false;
				colvarTrafficesDate.IsPrimaryKey = false;
				colvarTrafficesDate.IsForeignKey = false;
				colvarTrafficesDate.IsReadOnly = false;
				colvarTrafficesDate.DefaultSetting = @"";
				colvarTrafficesDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTrafficesDate);
				
				TableSchema.TableColumn colvarBusinessHourGuid = new TableSchema.TableColumn(schema);
				colvarBusinessHourGuid.ColumnName = "business_hour_guid";
				colvarBusinessHourGuid.DataType = DbType.Guid;
				colvarBusinessHourGuid.MaxLength = 0;
				colvarBusinessHourGuid.AutoIncrement = false;
				colvarBusinessHourGuid.IsNullable = false;
				colvarBusinessHourGuid.IsPrimaryKey = false;
				colvarBusinessHourGuid.IsForeignKey = false;
				colvarBusinessHourGuid.IsReadOnly = false;
				colvarBusinessHourGuid.DefaultSetting = @"";
				colvarBusinessHourGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBusinessHourGuid);
				
				TableSchema.TableColumn colvarViews = new TableSchema.TableColumn(schema);
				colvarViews.ColumnName = "views";
				colvarViews.DataType = DbType.Int32;
				colvarViews.MaxLength = 0;
				colvarViews.AutoIncrement = false;
				colvarViews.IsNullable = true;
				colvarViews.IsPrimaryKey = false;
				colvarViews.IsForeignKey = false;
				colvarViews.IsReadOnly = false;
				colvarViews.DefaultSetting = @"";
				colvarViews.ForeignKeyTableName = "";
				schema.Columns.Add(colvarViews);
				
				TableSchema.TableColumn colvarViewercount = new TableSchema.TableColumn(schema);
				colvarViewercount.ColumnName = "viewercount";
				colvarViewercount.DataType = DbType.Int32;
				colvarViewercount.MaxLength = 0;
				colvarViewercount.AutoIncrement = false;
				colvarViewercount.IsNullable = true;
				colvarViewercount.IsPrimaryKey = false;
				colvarViewercount.IsForeignKey = false;
				colvarViewercount.IsReadOnly = false;
				colvarViewercount.DefaultSetting = @"";
				colvarViewercount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarViewercount);
				
				TableSchema.TableColumn colvarBuyercount = new TableSchema.TableColumn(schema);
				colvarBuyercount.ColumnName = "buyercount";
				colvarBuyercount.DataType = DbType.Int32;
				colvarBuyercount.MaxLength = 0;
				colvarBuyercount.AutoIncrement = false;
				colvarBuyercount.IsNullable = true;
				colvarBuyercount.IsPrimaryKey = false;
				colvarBuyercount.IsForeignKey = false;
				colvarBuyercount.IsReadOnly = false;
				colvarBuyercount.DefaultSetting = @"";
				colvarBuyercount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBuyercount);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("bi_traffics",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("TrafficesDate")]
		[Bindable(true)]
		public DateTime TrafficesDate 
		{
			get { return GetColumnValue<DateTime>(Columns.TrafficesDate); }
			set { SetColumnValue(Columns.TrafficesDate, value); }
		}
		  
		[XmlAttribute("BusinessHourGuid")]
		[Bindable(true)]
		public Guid BusinessHourGuid 
		{
			get { return GetColumnValue<Guid>(Columns.BusinessHourGuid); }
			set { SetColumnValue(Columns.BusinessHourGuid, value); }
		}
		  
		[XmlAttribute("Views")]
		[Bindable(true)]
		public int? Views 
		{
			get { return GetColumnValue<int?>(Columns.Views); }
			set { SetColumnValue(Columns.Views, value); }
		}
		  
		[XmlAttribute("Viewercount")]
		[Bindable(true)]
		public int? Viewercount 
		{
			get { return GetColumnValue<int?>(Columns.Viewercount); }
			set { SetColumnValue(Columns.Viewercount, value); }
		}
		  
		[XmlAttribute("Buyercount")]
		[Bindable(true)]
		public int? Buyercount 
		{
			get { return GetColumnValue<int?>(Columns.Buyercount); }
			set { SetColumnValue(Columns.Buyercount, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn TrafficesDateColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn BusinessHourGuidColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn ViewsColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn ViewercountColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn BuyercountColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string TrafficesDate = @"traffices_date";
			 public static string BusinessHourGuid = @"business_hour_guid";
			 public static string Views = @"views";
			 public static string Viewercount = @"viewercount";
			 public static string Buyercount = @"buyercount";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

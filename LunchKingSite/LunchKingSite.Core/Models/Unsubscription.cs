using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the Unsubscription class.
    /// </summary>
    [Serializable]
    public partial class UnsubscriptionCollection : RepositoryList<Unsubscription, UnsubscriptionCollection>
    {
        public UnsubscriptionCollection() { }

        /// <summary>
        /// Filters an existing collection based on the set criteria. This is an in-memory filter
        /// Thanks to developingchris for this!
        /// </summary>
        /// <returns>UnsubscriptionCollection</returns>
        public UnsubscriptionCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                Unsubscription o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }


    }
    /// <summary>
    /// This is an ActiveRecord class which wraps the unsubscription table.
    /// </summary>
    [Serializable]
    public partial class Unsubscription : RepositoryRecord<Unsubscription>, IRecordBase
    {
        #region .ctors and Default Settings

        public Unsubscription()
        {
            SetSQLProps();
            InitSetDefaults();
            MarkNew();
        }

        private void InitSetDefaults() { SetDefaults(); }

        public Unsubscription(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
                ForceDefaults();
            MarkNew();
        }

        protected static void SetSQLProps() { GetTableSchema(); }

        #endregion

        #region Schema and Query Accessor
        public static Query CreateQuery() { return new Query(Schema); }
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                    SetSQLProps();
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("unsubscription", TableType.Table, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarUserName = new TableSchema.TableColumn(schema);
                colvarUserName.ColumnName = "user_name";
                colvarUserName.DataType = DbType.String;
                colvarUserName.MaxLength = 250;
                colvarUserName.AutoIncrement = false;
                colvarUserName.IsNullable = false;
                colvarUserName.IsPrimaryKey = true;
                colvarUserName.IsForeignKey = false;
                colvarUserName.IsReadOnly = false;
                colvarUserName.DefaultSetting = @"";
                colvarUserName.ForeignKeyTableName = "";
                schema.Columns.Add(colvarUserName);

                TableSchema.TableColumn colvarPiinlifeEdm = new TableSchema.TableColumn(schema);
                colvarPiinlifeEdm.ColumnName = "piinlife_edm";
                colvarPiinlifeEdm.DataType = DbType.Boolean;
                colvarPiinlifeEdm.MaxLength = 0;
                colvarPiinlifeEdm.AutoIncrement = false;
                colvarPiinlifeEdm.IsNullable = false;
                colvarPiinlifeEdm.IsPrimaryKey = false;
                colvarPiinlifeEdm.IsForeignKey = false;
                colvarPiinlifeEdm.IsReadOnly = false;

                colvarPiinlifeEdm.DefaultSetting = @"((1))";
                colvarPiinlifeEdm.ForeignKeyTableName = "";
                schema.Columns.Add(colvarPiinlifeEdm);

                TableSchema.TableColumn colvarPponEdm = new TableSchema.TableColumn(schema);
                colvarPponEdm.ColumnName = "ppon_edm";
                colvarPponEdm.DataType = DbType.Boolean;
                colvarPponEdm.MaxLength = 0;
                colvarPponEdm.AutoIncrement = false;
                colvarPponEdm.IsNullable = false;
                colvarPponEdm.IsPrimaryKey = false;
                colvarPponEdm.IsForeignKey = false;
                colvarPponEdm.IsReadOnly = false;

                colvarPponEdm.DefaultSetting = @"((1))";
                colvarPponEdm.ForeignKeyTableName = "";
                schema.Columns.Add(colvarPponEdm);

                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = false;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;

                colvarCreateTime.DefaultSetting = @"(getdate())";
                colvarCreateTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateTime);

                TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
                colvarCreateId.ColumnName = "create_id";
                colvarCreateId.DataType = DbType.String;
                colvarCreateId.MaxLength = 250;
                colvarCreateId.AutoIncrement = false;
                colvarCreateId.IsNullable = false;
                colvarCreateId.IsPrimaryKey = false;
                colvarCreateId.IsForeignKey = false;
                colvarCreateId.IsReadOnly = false;
                colvarCreateId.DefaultSetting = @"";
                colvarCreateId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateId);

                TableSchema.TableColumn colvarModifyId = new TableSchema.TableColumn(schema);
                colvarModifyId.ColumnName = "modify_id";
                colvarModifyId.DataType = DbType.String;
                colvarModifyId.MaxLength = 250;
                colvarModifyId.AutoIncrement = false;
                colvarModifyId.IsNullable = true;
                colvarModifyId.IsPrimaryKey = false;
                colvarModifyId.IsForeignKey = false;
                colvarModifyId.IsReadOnly = false;
                colvarModifyId.DefaultSetting = @"";
                colvarModifyId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarModifyId);

                TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
                colvarModifyTime.ColumnName = "modify_time";
                colvarModifyTime.DataType = DbType.DateTime;
                colvarModifyTime.MaxLength = 0;
                colvarModifyTime.AutoIncrement = false;
                colvarModifyTime.IsNullable = true;
                colvarModifyTime.IsPrimaryKey = false;
                colvarModifyTime.IsForeignKey = false;
                colvarModifyTime.IsReadOnly = false;
                colvarModifyTime.DefaultSetting = @"";
                colvarModifyTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarModifyTime);

                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("unsubscription", schema);
            }
        }
        #endregion

        #region Props

        [XmlAttribute("UserName")]
        [Bindable(true)]
        public string UserName
        {
            get { return GetColumnValue<string>(Columns.UserName); }
            set { SetColumnValue(Columns.UserName, value); }
        }

        [XmlAttribute("PiinlifeEdm")]
        [Bindable(true)]
        public bool PiinlifeEdm
        {
            get { return GetColumnValue<bool>(Columns.PiinlifeEdm); }
            set { SetColumnValue(Columns.PiinlifeEdm, value); }
        }

        [XmlAttribute("PponEdm")]
        [Bindable(true)]
        public bool PponEdm
        {
            get { return GetColumnValue<bool>(Columns.PponEdm); }
            set { SetColumnValue(Columns.PponEdm, value); }
        }

        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime CreateTime
        {
            get { return GetColumnValue<DateTime>(Columns.CreateTime); }
            set { SetColumnValue(Columns.CreateTime, value); }
        }

        [XmlAttribute("CreateId")]
        [Bindable(true)]
        public string CreateId
        {
            get { return GetColumnValue<string>(Columns.CreateId); }
            set { SetColumnValue(Columns.CreateId, value); }
        }

        [XmlAttribute("ModifyId")]
        [Bindable(true)]
        public string ModifyId
        {
            get { return GetColumnValue<string>(Columns.ModifyId); }
            set { SetColumnValue(Columns.ModifyId, value); }
        }

        [XmlAttribute("ModifyTime")]
        [Bindable(true)]
        public DateTime? ModifyTime
        {
            get { return GetColumnValue<DateTime?>(Columns.ModifyTime); }
            set { SetColumnValue(Columns.ModifyTime, value); }
        }

        #endregion




        //no foreign key tables defined (0)



        //no ManyToMany tables defined (0)





        #region Typed Columns


        public static TableSchema.TableColumn UserNameColumn
        {
            get { return Schema.Columns[0]; }
        }



        public static TableSchema.TableColumn PiinlifeEdmColumn
        {
            get { return Schema.Columns[1]; }
        }



        public static TableSchema.TableColumn PponEdmColumn
        {
            get { return Schema.Columns[2]; }
        }



        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[3]; }
        }



        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[4]; }
        }



        public static TableSchema.TableColumn ModifyIdColumn
        {
            get { return Schema.Columns[5]; }
        }



        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[6]; }
        }



        #endregion
        #region Columns Struct
        public struct Columns
        {
            public static string UserName = @"user_name";
            public static string PiinlifeEdm = @"piinlife_edm";
            public static string PponEdm = @"ppon_edm";
            public static string CreateTime = @"create_time";
            public static string CreateId = @"create_id";
            public static string ModifyId = @"modify_id";
            public static string ModifyTime = @"modify_time";

        }
        #endregion

        #region Update PK Collections

        #endregion

        #region Deep Save

        #endregion
    }
}

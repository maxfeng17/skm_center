using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewSellerHiDeal class.
    /// </summary>
    [Serializable]
    public partial class ViewSellerHiDealCollection : ReadOnlyList<ViewSellerHiDeal, ViewSellerHiDealCollection>
    {        
        public ViewSellerHiDealCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_seller_hi_deal view.
    /// </summary>
    [Serializable]
    public partial class ViewSellerHiDeal : ReadOnlyRecord<ViewSellerHiDeal>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_seller_hi_deal", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarDealId = new TableSchema.TableColumn(schema);
                colvarDealId.ColumnName = "deal_id";
                colvarDealId.DataType = DbType.Int32;
                colvarDealId.MaxLength = 0;
                colvarDealId.AutoIncrement = false;
                colvarDealId.IsNullable = false;
                colvarDealId.IsPrimaryKey = false;
                colvarDealId.IsForeignKey = false;
                colvarDealId.IsReadOnly = false;
                
                schema.Columns.Add(colvarDealId);
                
                TableSchema.TableColumn colvarSellerGuid = new TableSchema.TableColumn(schema);
                colvarSellerGuid.ColumnName = "seller_guid";
                colvarSellerGuid.DataType = DbType.Guid;
                colvarSellerGuid.MaxLength = 0;
                colvarSellerGuid.AutoIncrement = false;
                colvarSellerGuid.IsNullable = false;
                colvarSellerGuid.IsPrimaryKey = false;
                colvarSellerGuid.IsForeignKey = false;
                colvarSellerGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerGuid);
                
                TableSchema.TableColumn colvarSellerName = new TableSchema.TableColumn(schema);
                colvarSellerName.ColumnName = "seller_name";
                colvarSellerName.DataType = DbType.String;
                colvarSellerName.MaxLength = 100;
                colvarSellerName.AutoIncrement = false;
                colvarSellerName.IsNullable = true;
                colvarSellerName.IsPrimaryKey = false;
                colvarSellerName.IsForeignKey = false;
                colvarSellerName.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerName);
                
                TableSchema.TableColumn colvarDealName = new TableSchema.TableColumn(schema);
                colvarDealName.ColumnName = "deal_name";
                colvarDealName.DataType = DbType.String;
                colvarDealName.MaxLength = 50;
                colvarDealName.AutoIncrement = false;
                colvarDealName.IsNullable = true;
                colvarDealName.IsPrimaryKey = false;
                colvarDealName.IsForeignKey = false;
                colvarDealName.IsReadOnly = false;
                
                schema.Columns.Add(colvarDealName);
                
                TableSchema.TableColumn colvarIsOpen = new TableSchema.TableColumn(schema);
                colvarIsOpen.ColumnName = "IsOpen";
                colvarIsOpen.DataType = DbType.Boolean;
                colvarIsOpen.MaxLength = 0;
                colvarIsOpen.AutoIncrement = false;
                colvarIsOpen.IsNullable = false;
                colvarIsOpen.IsPrimaryKey = false;
                colvarIsOpen.IsForeignKey = false;
                colvarIsOpen.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsOpen);
                
                TableSchema.TableColumn colvarCurrentSoldItems = new TableSchema.TableColumn(schema);
                colvarCurrentSoldItems.ColumnName = "current_sold_items";
                colvarCurrentSoldItems.DataType = DbType.Int32;
                colvarCurrentSoldItems.MaxLength = 0;
                colvarCurrentSoldItems.AutoIncrement = false;
                colvarCurrentSoldItems.IsNullable = true;
                colvarCurrentSoldItems.IsPrimaryKey = false;
                colvarCurrentSoldItems.IsForeignKey = false;
                colvarCurrentSoldItems.IsReadOnly = false;
                
                schema.Columns.Add(colvarCurrentSoldItems);
                
                TableSchema.TableColumn colvarCurrentSoldAmount = new TableSchema.TableColumn(schema);
                colvarCurrentSoldAmount.ColumnName = "current_sold_amount";
                colvarCurrentSoldAmount.DataType = DbType.Currency;
                colvarCurrentSoldAmount.MaxLength = 0;
                colvarCurrentSoldAmount.AutoIncrement = false;
                colvarCurrentSoldAmount.IsNullable = true;
                colvarCurrentSoldAmount.IsPrimaryKey = false;
                colvarCurrentSoldAmount.IsForeignKey = false;
                colvarCurrentSoldAmount.IsReadOnly = false;
                
                schema.Columns.Add(colvarCurrentSoldAmount);
                
                TableSchema.TableColumn colvarDealStartTime = new TableSchema.TableColumn(schema);
                colvarDealStartTime.ColumnName = "deal_start_time";
                colvarDealStartTime.DataType = DbType.DateTime;
                colvarDealStartTime.MaxLength = 0;
                colvarDealStartTime.AutoIncrement = false;
                colvarDealStartTime.IsNullable = true;
                colvarDealStartTime.IsPrimaryKey = false;
                colvarDealStartTime.IsForeignKey = false;
                colvarDealStartTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarDealStartTime);
                
                TableSchema.TableColumn colvarDealEndTime = new TableSchema.TableColumn(schema);
                colvarDealEndTime.ColumnName = "deal_end_time";
                colvarDealEndTime.DataType = DbType.DateTime;
                colvarDealEndTime.MaxLength = 0;
                colvarDealEndTime.AutoIncrement = false;
                colvarDealEndTime.IsNullable = true;
                colvarDealEndTime.IsPrimaryKey = false;
                colvarDealEndTime.IsForeignKey = false;
                colvarDealEndTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarDealEndTime);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_seller_hi_deal",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewSellerHiDeal()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewSellerHiDeal(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewSellerHiDeal(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewSellerHiDeal(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("DealId")]
        [Bindable(true)]
        public int DealId 
	    {
		    get
		    {
			    return GetColumnValue<int>("deal_id");
		    }
            set 
		    {
			    SetColumnValue("deal_id", value);
            }
        }
	      
        [XmlAttribute("SellerGuid")]
        [Bindable(true)]
        public Guid SellerGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("seller_guid");
		    }
            set 
		    {
			    SetColumnValue("seller_guid", value);
            }
        }
	      
        [XmlAttribute("SellerName")]
        [Bindable(true)]
        public string SellerName 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_name");
		    }
            set 
		    {
			    SetColumnValue("seller_name", value);
            }
        }
	      
        [XmlAttribute("DealName")]
        [Bindable(true)]
        public string DealName 
	    {
		    get
		    {
			    return GetColumnValue<string>("deal_name");
		    }
            set 
		    {
			    SetColumnValue("deal_name", value);
            }
        }
	      
        [XmlAttribute("IsOpen")]
        [Bindable(true)]
        public bool IsOpen 
	    {
		    get
		    {
			    return GetColumnValue<bool>("IsOpen");
		    }
            set 
		    {
			    SetColumnValue("IsOpen", value);
            }
        }
	      
        [XmlAttribute("CurrentSoldItems")]
        [Bindable(true)]
        public int? CurrentSoldItems 
	    {
		    get
		    {
			    return GetColumnValue<int?>("current_sold_items");
		    }
            set 
		    {
			    SetColumnValue("current_sold_items", value);
            }
        }
	      
        [XmlAttribute("CurrentSoldAmount")]
        [Bindable(true)]
        public decimal? CurrentSoldAmount 
	    {
		    get
		    {
			    return GetColumnValue<decimal?>("current_sold_amount");
		    }
            set 
		    {
			    SetColumnValue("current_sold_amount", value);
            }
        }
	      
        [XmlAttribute("DealStartTime")]
        [Bindable(true)]
        public DateTime? DealStartTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("deal_start_time");
		    }
            set 
		    {
			    SetColumnValue("deal_start_time", value);
            }
        }
	      
        [XmlAttribute("DealEndTime")]
        [Bindable(true)]
        public DateTime? DealEndTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("deal_end_time");
		    }
            set 
		    {
			    SetColumnValue("deal_end_time", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string DealId = @"deal_id";
            
            public static string SellerGuid = @"seller_guid";
            
            public static string SellerName = @"seller_name";
            
            public static string DealName = @"deal_name";
            
            public static string IsOpen = @"IsOpen";
            
            public static string CurrentSoldItems = @"current_sold_items";
            
            public static string CurrentSoldAmount = @"current_sold_amount";
            
            public static string DealStartTime = @"deal_start_time";
            
            public static string DealEndTime = @"deal_end_time";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

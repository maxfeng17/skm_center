using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewPcpUserDepositCoupon class.
    /// </summary>
    [Serializable]
    public partial class ViewPcpUserDepositCouponCollection : ReadOnlyList<ViewPcpUserDepositCoupon, ViewPcpUserDepositCouponCollection>
    {        
        public ViewPcpUserDepositCouponCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_pcp_user_deposit_coupon view.
    /// </summary>
    [Serializable]
    public partial class ViewPcpUserDepositCoupon : ReadOnlyRecord<ViewPcpUserDepositCoupon>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_pcp_user_deposit_coupon", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarDepositId = new TableSchema.TableColumn(schema);
                colvarDepositId.ColumnName = "deposit_id";
                colvarDepositId.DataType = DbType.Int32;
                colvarDepositId.MaxLength = 0;
                colvarDepositId.AutoIncrement = false;
                colvarDepositId.IsNullable = false;
                colvarDepositId.IsPrimaryKey = false;
                colvarDepositId.IsForeignKey = false;
                colvarDepositId.IsReadOnly = false;
                
                schema.Columns.Add(colvarDepositId);
                
                TableSchema.TableColumn colvarUserId = new TableSchema.TableColumn(schema);
                colvarUserId.ColumnName = "user_id";
                colvarUserId.DataType = DbType.Int32;
                colvarUserId.MaxLength = 0;
                colvarUserId.AutoIncrement = false;
                colvarUserId.IsNullable = false;
                colvarUserId.IsPrimaryKey = false;
                colvarUserId.IsForeignKey = false;
                colvarUserId.IsReadOnly = false;
                
                schema.Columns.Add(colvarUserId);
                
                TableSchema.TableColumn colvarGroupId = new TableSchema.TableColumn(schema);
                colvarGroupId.ColumnName = "group_id";
                colvarGroupId.DataType = DbType.Int32;
                colvarGroupId.MaxLength = 0;
                colvarGroupId.AutoIncrement = false;
                colvarGroupId.IsNullable = true;
                colvarGroupId.IsPrimaryKey = false;
                colvarGroupId.IsForeignKey = false;
                colvarGroupId.IsReadOnly = false;
                
                schema.Columns.Add(colvarGroupId);
                
                TableSchema.TableColumn colvarCouponId = new TableSchema.TableColumn(schema);
                colvarCouponId.ColumnName = "coupon_id";
                colvarCouponId.DataType = DbType.Int32;
                colvarCouponId.MaxLength = 0;
                colvarCouponId.AutoIncrement = false;
                colvarCouponId.IsNullable = true;
                colvarCouponId.IsPrimaryKey = false;
                colvarCouponId.IsForeignKey = false;
                colvarCouponId.IsReadOnly = false;
                
                schema.Columns.Add(colvarCouponId);
                
                TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
                colvarStatus.ColumnName = "status";
                colvarStatus.DataType = DbType.Int32;
                colvarStatus.MaxLength = 0;
                colvarStatus.AutoIncrement = false;
                colvarStatus.IsNullable = true;
                colvarStatus.IsPrimaryKey = false;
                colvarStatus.IsForeignKey = false;
                colvarStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarStatus);
                
                TableSchema.TableColumn colvarExchangeTime = new TableSchema.TableColumn(schema);
                colvarExchangeTime.ColumnName = "exchange_time";
                colvarExchangeTime.DataType = DbType.DateTime;
                colvarExchangeTime.MaxLength = 0;
                colvarExchangeTime.AutoIncrement = false;
                colvarExchangeTime.IsNullable = true;
                colvarExchangeTime.IsPrimaryKey = false;
                colvarExchangeTime.IsForeignKey = false;
                colvarExchangeTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarExchangeTime);
                
                TableSchema.TableColumn colvarCreatedTime = new TableSchema.TableColumn(schema);
                colvarCreatedTime.ColumnName = "created_time";
                colvarCreatedTime.DataType = DbType.DateTime;
                colvarCreatedTime.MaxLength = 0;
                colvarCreatedTime.AutoIncrement = false;
                colvarCreatedTime.IsNullable = true;
                colvarCreatedTime.IsPrimaryKey = false;
                colvarCreatedTime.IsForeignKey = false;
                colvarCreatedTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarCreatedTime);
                
                TableSchema.TableColumn colvarMenuId = new TableSchema.TableColumn(schema);
                colvarMenuId.ColumnName = "menu_id";
                colvarMenuId.DataType = DbType.Int32;
                colvarMenuId.MaxLength = 0;
                colvarMenuId.AutoIncrement = false;
                colvarMenuId.IsNullable = true;
                colvarMenuId.IsPrimaryKey = false;
                colvarMenuId.IsForeignKey = false;
                colvarMenuId.IsReadOnly = false;
                
                schema.Columns.Add(colvarMenuId);
                
                TableSchema.TableColumn colvarTitle = new TableSchema.TableColumn(schema);
                colvarTitle.ColumnName = "title";
                colvarTitle.DataType = DbType.String;
                colvarTitle.MaxLength = 100;
                colvarTitle.AutoIncrement = false;
                colvarTitle.IsNullable = true;
                colvarTitle.IsPrimaryKey = false;
                colvarTitle.IsForeignKey = false;
                colvarTitle.IsReadOnly = false;
                
                schema.Columns.Add(colvarTitle);
                
                TableSchema.TableColumn colvarItemId = new TableSchema.TableColumn(schema);
                colvarItemId.ColumnName = "item_id";
                colvarItemId.DataType = DbType.Int32;
                colvarItemId.MaxLength = 0;
                colvarItemId.AutoIncrement = false;
                colvarItemId.IsNullable = false;
                colvarItemId.IsPrimaryKey = false;
                colvarItemId.IsForeignKey = false;
                colvarItemId.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemId);
                
                TableSchema.TableColumn colvarItemName = new TableSchema.TableColumn(schema);
                colvarItemName.ColumnName = "item_name";
                colvarItemName.DataType = DbType.String;
                colvarItemName.MaxLength = 100;
                colvarItemName.AutoIncrement = false;
                colvarItemName.IsNullable = true;
                colvarItemName.IsPrimaryKey = false;
                colvarItemName.IsForeignKey = false;
                colvarItemName.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemName);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_pcp_user_deposit_coupon",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewPcpUserDepositCoupon()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewPcpUserDepositCoupon(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewPcpUserDepositCoupon(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewPcpUserDepositCoupon(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("DepositId")]
        [Bindable(true)]
        public int DepositId 
	    {
		    get
		    {
			    return GetColumnValue<int>("deposit_id");
		    }
            set 
		    {
			    SetColumnValue("deposit_id", value);
            }
        }
	      
        [XmlAttribute("UserId")]
        [Bindable(true)]
        public int UserId 
	    {
		    get
		    {
			    return GetColumnValue<int>("user_id");
		    }
            set 
		    {
			    SetColumnValue("user_id", value);
            }
        }
	      
        [XmlAttribute("GroupId")]
        [Bindable(true)]
        public int? GroupId 
	    {
		    get
		    {
			    return GetColumnValue<int?>("group_id");
		    }
            set 
		    {
			    SetColumnValue("group_id", value);
            }
        }
	      
        [XmlAttribute("CouponId")]
        [Bindable(true)]
        public int? CouponId 
	    {
		    get
		    {
			    return GetColumnValue<int?>("coupon_id");
		    }
            set 
		    {
			    SetColumnValue("coupon_id", value);
            }
        }
	      
        [XmlAttribute("Status")]
        [Bindable(true)]
        public int? Status 
	    {
		    get
		    {
			    return GetColumnValue<int?>("status");
		    }
            set 
		    {
			    SetColumnValue("status", value);
            }
        }
	      
        [XmlAttribute("ExchangeTime")]
        [Bindable(true)]
        public DateTime? ExchangeTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("exchange_time");
		    }
            set 
		    {
			    SetColumnValue("exchange_time", value);
            }
        }
	      
        [XmlAttribute("CreatedTime")]
        [Bindable(true)]
        public DateTime? CreatedTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("created_time");
		    }
            set 
		    {
			    SetColumnValue("created_time", value);
            }
        }
	      
        [XmlAttribute("MenuId")]
        [Bindable(true)]
        public int? MenuId 
	    {
		    get
		    {
			    return GetColumnValue<int?>("menu_id");
		    }
            set 
		    {
			    SetColumnValue("menu_id", value);
            }
        }
	      
        [XmlAttribute("Title")]
        [Bindable(true)]
        public string Title 
	    {
		    get
		    {
			    return GetColumnValue<string>("title");
		    }
            set 
		    {
			    SetColumnValue("title", value);
            }
        }
	      
        [XmlAttribute("ItemId")]
        [Bindable(true)]
        public int ItemId 
	    {
		    get
		    {
			    return GetColumnValue<int>("item_id");
		    }
            set 
		    {
			    SetColumnValue("item_id", value);
            }
        }
	      
        [XmlAttribute("ItemName")]
        [Bindable(true)]
        public string ItemName 
	    {
		    get
		    {
			    return GetColumnValue<string>("item_name");
		    }
            set 
		    {
			    SetColumnValue("item_name", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string DepositId = @"deposit_id";
            
            public static string UserId = @"user_id";
            
            public static string GroupId = @"group_id";
            
            public static string CouponId = @"coupon_id";
            
            public static string Status = @"status";
            
            public static string ExchangeTime = @"exchange_time";
            
            public static string CreatedTime = @"created_time";
            
            public static string MenuId = @"menu_id";
            
            public static string Title = @"title";
            
            public static string ItemId = @"item_id";
            
            public static string ItemName = @"item_name";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

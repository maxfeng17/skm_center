using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the ReferralAction class.
    /// </summary>
    [Serializable]
    public partial class ReferralActionCollection : RepositoryList<ReferralAction, ReferralActionCollection>
    {
        public ReferralActionCollection() { }

        /// <summary>
        /// Filters an existing collection based on the set criteria. This is an in-memory filter
        /// Thanks to developingchris for this!
        /// </summary>
        /// <returns>ReferralActionCollection</returns>
        public ReferralActionCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                ReferralAction o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }


    }
    /// <summary>
    /// This is an ActiveRecord class which wraps the referral_action table.
    /// </summary>
    [Serializable]
    public partial class ReferralAction : RepositoryRecord<ReferralAction>, IRecordBase
    {
        #region .ctors and Default Settings

        public ReferralAction()
        {
            SetSQLProps();
            InitSetDefaults();
            MarkNew();
        }

        private void InitSetDefaults() { SetDefaults(); }

        public ReferralAction(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
                ForceDefaults();
            MarkNew();
        }

        protected static void SetSQLProps() { GetTableSchema(); }

        #endregion

        #region Schema and Query Accessor
        public static Query CreateQuery() { return new Query(Schema); }
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                    SetSQLProps();
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("referral_action", TableType.Table, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = true;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = true;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                colvarId.DefaultSetting = @"";
                colvarId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarId);

                TableSchema.TableColumn colvarGuid = new TableSchema.TableColumn(schema);
                colvarGuid.ColumnName = "guid";
                colvarGuid.DataType = DbType.String;
                colvarGuid.MaxLength = 100;
                colvarGuid.AutoIncrement = false;
                colvarGuid.IsNullable = false;
                colvarGuid.IsPrimaryKey = false;
                colvarGuid.IsForeignKey = false;
                colvarGuid.IsReadOnly = false;
                colvarGuid.DefaultSetting = @"";
                colvarGuid.ForeignKeyTableName = "";
                schema.Columns.Add(colvarGuid);

                TableSchema.TableColumn colvarReferrerGuid = new TableSchema.TableColumn(schema);
                colvarReferrerGuid.ColumnName = "referrer_guid";
                colvarReferrerGuid.DataType = DbType.Guid;
                colvarReferrerGuid.MaxLength = 0;
                colvarReferrerGuid.AutoIncrement = false;
                colvarReferrerGuid.IsNullable = false;
                colvarReferrerGuid.IsPrimaryKey = false;
                colvarReferrerGuid.IsForeignKey = false;
                colvarReferrerGuid.IsReadOnly = false;
                colvarReferrerGuid.DefaultSetting = @"";
                colvarReferrerGuid.ForeignKeyTableName = "";
                schema.Columns.Add(colvarReferrerGuid);

                TableSchema.TableColumn colvarReferrerSourceId = new TableSchema.TableColumn(schema);
                colvarReferrerSourceId.ColumnName = "referrer_source_id";
                colvarReferrerSourceId.DataType = DbType.String;
                colvarReferrerSourceId.MaxLength = 256;
                colvarReferrerSourceId.AutoIncrement = false;
                colvarReferrerSourceId.IsNullable = false;
                colvarReferrerSourceId.IsPrimaryKey = false;
                colvarReferrerSourceId.IsForeignKey = false;
                colvarReferrerSourceId.IsReadOnly = false;
                colvarReferrerSourceId.DefaultSetting = @"";
                colvarReferrerSourceId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarReferrerSourceId);

                TableSchema.TableColumn colvarActionType = new TableSchema.TableColumn(schema);
                colvarActionType.ColumnName = "action_type";
                colvarActionType.DataType = DbType.Int32;
                colvarActionType.MaxLength = 0;
                colvarActionType.AutoIncrement = false;
                colvarActionType.IsNullable = false;
                colvarActionType.IsPrimaryKey = false;
                colvarActionType.IsForeignKey = false;
                colvarActionType.IsReadOnly = false;
                colvarActionType.DefaultSetting = @"";
                colvarActionType.ForeignKeyTableName = "";
                schema.Columns.Add(colvarActionType);

                TableSchema.TableColumn colvarReferenceId = new TableSchema.TableColumn(schema);
                colvarReferenceId.ColumnName = "reference_id";
                colvarReferenceId.DataType = DbType.String;
                colvarReferenceId.MaxLength = 256;
                colvarReferenceId.AutoIncrement = false;
                colvarReferenceId.IsNullable = false;
                colvarReferenceId.IsPrimaryKey = false;
                colvarReferenceId.IsForeignKey = false;
                colvarReferenceId.IsReadOnly = false;
                colvarReferenceId.DefaultSetting = @"";
                colvarReferenceId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarReferenceId);

                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = false;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                colvarCreateTime.DefaultSetting = @"";
                colvarCreateTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateTime);

                TableSchema.TableColumn colvarDepartment = new TableSchema.TableColumn(schema);
                colvarDepartment.ColumnName = "department";
                colvarDepartment.DataType = DbType.String;
                colvarDepartment.MaxLength = 256;
                colvarDepartment.AutoIncrement = false;
                colvarDepartment.IsNullable = true;
                colvarDepartment.IsPrimaryKey = false;
                colvarDepartment.IsForeignKey = false;
                colvarDepartment.IsReadOnly = false;
                colvarDepartment.DefaultSetting = @"";
                colvarDepartment.ForeignKeyTableName = "";
                schema.Columns.Add(colvarDepartment);

                TableSchema.TableColumn colvarCity = new TableSchema.TableColumn(schema);
                colvarCity.ColumnName = "city";
                colvarCity.DataType = DbType.String;
                colvarCity.MaxLength = 50;
                colvarCity.AutoIncrement = false;
                colvarCity.IsNullable = true;
                colvarCity.IsPrimaryKey = false;
                colvarCity.IsForeignKey = false;
                colvarCity.IsReadOnly = false;
                colvarCity.DefaultSetting = @"";
                colvarCity.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCity);

                TableSchema.TableColumn colvarItemName = new TableSchema.TableColumn(schema);
                colvarItemName.ColumnName = "item_name";
                colvarItemName.DataType = DbType.String;
                colvarItemName.MaxLength = 500;
                colvarItemName.AutoIncrement = false;
                colvarItemName.IsNullable = true;
                colvarItemName.IsPrimaryKey = false;
                colvarItemName.IsForeignKey = false;
                colvarItemName.IsReadOnly = false;
                colvarItemName.DefaultSetting = @"";
                colvarItemName.ForeignKeyTableName = "";
                schema.Columns.Add(colvarItemName);

                TableSchema.TableColumn colvarPrice = new TableSchema.TableColumn(schema);
                colvarPrice.ColumnName = "price";
                colvarPrice.DataType = DbType.String;
                colvarPrice.MaxLength = 50;
                colvarPrice.AutoIncrement = false;
                colvarPrice.IsNullable = true;
                colvarPrice.IsPrimaryKey = false;
                colvarPrice.IsForeignKey = false;
                colvarPrice.IsReadOnly = false;
                colvarPrice.DefaultSetting = @"";
                colvarPrice.ForeignKeyTableName = "";
                schema.Columns.Add(colvarPrice);

                TableSchema.TableColumn colvarTotal = new TableSchema.TableColumn(schema);
                colvarTotal.ColumnName = "total";
                colvarTotal.DataType = DbType.String;
                colvarTotal.MaxLength = 50;
                colvarTotal.AutoIncrement = false;
                colvarTotal.IsNullable = true;
                colvarTotal.IsPrimaryKey = false;
                colvarTotal.IsForeignKey = false;
                colvarTotal.IsReadOnly = false;
                colvarTotal.DefaultSetting = @"";
                colvarTotal.ForeignKeyTableName = "";
                schema.Columns.Add(colvarTotal);

                TableSchema.TableColumn colvarSubTotal = new TableSchema.TableColumn(schema);
                colvarSubTotal.ColumnName = "sub_total";
                colvarSubTotal.DataType = DbType.String;
                colvarSubTotal.MaxLength = 50;
                colvarSubTotal.AutoIncrement = false;
                colvarSubTotal.IsNullable = true;
                colvarSubTotal.IsPrimaryKey = false;
                colvarSubTotal.IsForeignKey = false;
                colvarSubTotal.IsReadOnly = false;
                colvarSubTotal.DefaultSetting = @"";
                colvarSubTotal.ForeignKeyTableName = "";
                schema.Columns.Add(colvarSubTotal);

                TableSchema.TableColumn colvarRemark = new TableSchema.TableColumn(schema);
                colvarRemark.ColumnName = "remark";
                colvarRemark.DataType = DbType.String;
                colvarRemark.MaxLength = 256;
                colvarRemark.AutoIncrement = false;
                colvarRemark.IsNullable = true;
                colvarRemark.IsPrimaryKey = false;
                colvarRemark.IsForeignKey = false;
                colvarRemark.IsReadOnly = false;
                colvarRemark.DefaultSetting = @"";
                colvarRemark.ForeignKeyTableName = "";
                schema.Columns.Add(colvarRemark);

                TableSchema.TableColumn colvarDealType = new TableSchema.TableColumn(schema);
                colvarDealType.ColumnName = "deal_type";
                colvarDealType.DataType = DbType.Int32;
                colvarDealType.MaxLength = 0;
                colvarDealType.AutoIncrement = false;
                colvarDealType.IsNullable = false;
                colvarDealType.IsPrimaryKey = false;
                colvarDealType.IsForeignKey = false;
                colvarDealType.IsReadOnly = false;
                colvarDealType.DefaultSetting = @"";
                colvarDealType.ForeignKeyTableName = "";
                schema.Columns.Add(colvarDealType);

                TableSchema.TableColumn colvarAccBusinessGroupId = new TableSchema.TableColumn(schema);
                colvarAccBusinessGroupId.ColumnName = "acc_business_group_id";
                colvarAccBusinessGroupId.DataType = DbType.Int32;
                colvarAccBusinessGroupId.MaxLength = 0;
                colvarAccBusinessGroupId.AutoIncrement = false;
                colvarAccBusinessGroupId.IsNullable = false;
                colvarAccBusinessGroupId.IsPrimaryKey = false;
                colvarAccBusinessGroupId.IsForeignKey = false;
                colvarAccBusinessGroupId.IsReadOnly = false;
                colvarAccBusinessGroupId.DefaultSetting = @"";
                colvarAccBusinessGroupId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarAccBusinessGroupId);

                TableSchema.TableColumn colvarBusinessModel = new TableSchema.TableColumn(schema);
                colvarBusinessModel.ColumnName = "business_model";
                colvarBusinessModel.DataType = DbType.Int32;
                colvarBusinessModel.MaxLength = 0;
                colvarBusinessModel.AutoIncrement = false;
                colvarBusinessModel.IsNullable = false;
                colvarBusinessModel.IsPrimaryKey = false;
                colvarBusinessModel.IsForeignKey = false;
                colvarBusinessModel.IsReadOnly = false;

                colvarBusinessModel.DefaultSetting = @"((0))";
                colvarBusinessModel.ForeignKeyTableName = "";
                schema.Columns.Add(colvarBusinessModel);

                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("referral_action", schema);
            }
        }
        #endregion

        #region Props

        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id
        {
            get { return GetColumnValue<int>(Columns.Id); }
            set { SetColumnValue(Columns.Id, value); }
        }

        [XmlAttribute("Guid")]
        [Bindable(true)]
        public string Guid
        {
            get { return GetColumnValue<string>(Columns.Guid); }
            set { SetColumnValue(Columns.Guid, value); }
        }

        [XmlAttribute("ReferrerGuid")]
        [Bindable(true)]
        public Guid ReferrerGuid
        {
            get { return GetColumnValue<Guid>(Columns.ReferrerGuid); }
            set { SetColumnValue(Columns.ReferrerGuid, value); }
        }

        [XmlAttribute("ReferrerSourceId")]
        [Bindable(true)]
        public string ReferrerSourceId
        {
            get { return GetColumnValue<string>(Columns.ReferrerSourceId); }
            set { SetColumnValue(Columns.ReferrerSourceId, value); }
        }

        [XmlAttribute("ActionType")]
        [Bindable(true)]
        public int ActionType
        {
            get { return GetColumnValue<int>(Columns.ActionType); }
            set { SetColumnValue(Columns.ActionType, value); }
        }

        [XmlAttribute("ReferenceId")]
        [Bindable(true)]
        public string ReferenceId
        {
            get { return GetColumnValue<string>(Columns.ReferenceId); }
            set { SetColumnValue(Columns.ReferenceId, value); }
        }

        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime CreateTime
        {
            get { return GetColumnValue<DateTime>(Columns.CreateTime); }
            set { SetColumnValue(Columns.CreateTime, value); }
        }

        [XmlAttribute("Department")]
        [Bindable(true)]
        public string Department
        {
            get { return GetColumnValue<string>(Columns.Department); }
            set { SetColumnValue(Columns.Department, value); }
        }

        [XmlAttribute("City")]
        [Bindable(true)]
        public string City
        {
            get { return GetColumnValue<string>(Columns.City); }
            set { SetColumnValue(Columns.City, value); }
        }

        [XmlAttribute("ItemName")]
        [Bindable(true)]
        public string ItemName
        {
            get { return GetColumnValue<string>(Columns.ItemName); }
            set { SetColumnValue(Columns.ItemName, value); }
        }

        [XmlAttribute("Price")]
        [Bindable(true)]
        public string Price
        {
            get { return GetColumnValue<string>(Columns.Price); }
            set { SetColumnValue(Columns.Price, value); }
        }

        [XmlAttribute("Total")]
        [Bindable(true)]
        public string Total
        {
            get { return GetColumnValue<string>(Columns.Total); }
            set { SetColumnValue(Columns.Total, value); }
        }

        [XmlAttribute("SubTotal")]
        [Bindable(true)]
        public string SubTotal
        {
            get { return GetColumnValue<string>(Columns.SubTotal); }
            set { SetColumnValue(Columns.SubTotal, value); }
        }

        [XmlAttribute("Remark")]
        [Bindable(true)]
        public string Remark
        {
            get { return GetColumnValue<string>(Columns.Remark); }
            set { SetColumnValue(Columns.Remark, value); }
        }

        [XmlAttribute("DealType")]
        [Bindable(true)]
        public int DealType
        {
            get { return GetColumnValue<int>(Columns.DealType); }
            set { SetColumnValue(Columns.DealType, value); }
        }

        [XmlAttribute("AccBusinessGroupId")]
        [Bindable(true)]
        public int AccBusinessGroupId
        {
            get { return GetColumnValue<int>(Columns.AccBusinessGroupId); }
            set { SetColumnValue(Columns.AccBusinessGroupId, value); }
        }

        [XmlAttribute("BusinessModel")]
        [Bindable(true)]
        public int BusinessModel
        {
            get { return GetColumnValue<int>(Columns.BusinessModel); }
            set { SetColumnValue(Columns.BusinessModel, value); }
        }

        #endregion




        //no foreign key tables defined (0)



        //no ManyToMany tables defined (0)





        #region Typed Columns


        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }



        public static TableSchema.TableColumn GuidColumn
        {
            get { return Schema.Columns[1]; }
        }



        public static TableSchema.TableColumn ReferrerGuidColumn
        {
            get { return Schema.Columns[2]; }
        }



        public static TableSchema.TableColumn ReferrerSourceIdColumn
        {
            get { return Schema.Columns[3]; }
        }



        public static TableSchema.TableColumn ActionTypeColumn
        {
            get { return Schema.Columns[4]; }
        }



        public static TableSchema.TableColumn ReferenceIdColumn
        {
            get { return Schema.Columns[5]; }
        }



        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[6]; }
        }



        public static TableSchema.TableColumn DepartmentColumn
        {
            get { return Schema.Columns[7]; }
        }



        public static TableSchema.TableColumn CityColumn
        {
            get { return Schema.Columns[8]; }
        }



        public static TableSchema.TableColumn ItemNameColumn
        {
            get { return Schema.Columns[9]; }
        }



        public static TableSchema.TableColumn PriceColumn
        {
            get { return Schema.Columns[10]; }
        }



        public static TableSchema.TableColumn TotalColumn
        {
            get { return Schema.Columns[11]; }
        }



        public static TableSchema.TableColumn SubTotalColumn
        {
            get { return Schema.Columns[12]; }
        }



        public static TableSchema.TableColumn RemarkColumn
        {
            get { return Schema.Columns[13]; }
        }



        public static TableSchema.TableColumn DealTypeColumn
        {
            get { return Schema.Columns[14]; }
        }



        public static TableSchema.TableColumn AccBusinessGroupIdColumn
        {
            get { return Schema.Columns[15]; }
        }



        public static TableSchema.TableColumn BusinessModelColumn
        {
            get { return Schema.Columns[16]; }
        }



        #endregion
        #region Columns Struct
        public struct Columns
        {
            public static string Id = @"id";
            public static string Guid = @"guid";
            public static string ReferrerGuid = @"referrer_guid";
            public static string ReferrerSourceId = @"referrer_source_id";
            public static string ActionType = @"action_type";
            public static string ReferenceId = @"reference_id";
            public static string CreateTime = @"create_time";
            public static string Department = @"department";
            public static string City = @"city";
            public static string ItemName = @"item_name";
            public static string Price = @"price";
            public static string Total = @"total";
            public static string SubTotal = @"sub_total";
            public static string Remark = @"remark";
            public static string DealType = @"deal_type";
            public static string AccBusinessGroupId = @"acc_business_group_id";
            public static string BusinessModel = @"business_model";

        }
        #endregion

        #region Update PK Collections

        #endregion

        #region Deep Save

        #endregion
    }
}

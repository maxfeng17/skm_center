using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewHiDealVerificationAcct class.
    /// </summary>
    [Serializable]
    public partial class ViewHiDealVerificationAcctCollection : ReadOnlyList<ViewHiDealVerificationAcct, ViewHiDealVerificationAcctCollection>
    {        
        public ViewHiDealVerificationAcctCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_hi_deal_verification_acct view.
    /// </summary>
    [Serializable]
    public partial class ViewHiDealVerificationAcct : ReadOnlyRecord<ViewHiDealVerificationAcct>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_hi_deal_verification_acct", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = false;
                colvarId.IsNullable = true;
                colvarId.IsPrimaryKey = false;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                
                schema.Columns.Add(colvarId);
                
                TableSchema.TableColumn colvarOrderDetailGuid = new TableSchema.TableColumn(schema);
                colvarOrderDetailGuid.ColumnName = "order_detail_guid";
                colvarOrderDetailGuid.DataType = DbType.Guid;
                colvarOrderDetailGuid.MaxLength = 0;
                colvarOrderDetailGuid.AutoIncrement = false;
                colvarOrderDetailGuid.IsNullable = false;
                colvarOrderDetailGuid.IsPrimaryKey = false;
                colvarOrderDetailGuid.IsForeignKey = false;
                colvarOrderDetailGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderDetailGuid);
                
                TableSchema.TableColumn colvarSellerGuid = new TableSchema.TableColumn(schema);
                colvarSellerGuid.ColumnName = "seller_guid";
                colvarSellerGuid.DataType = DbType.Guid;
                colvarSellerGuid.MaxLength = 0;
                colvarSellerGuid.AutoIncrement = false;
                colvarSellerGuid.IsNullable = false;
                colvarSellerGuid.IsPrimaryKey = false;
                colvarSellerGuid.IsForeignKey = false;
                colvarSellerGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerGuid);
                
                TableSchema.TableColumn colvarStoreGuid = new TableSchema.TableColumn(schema);
                colvarStoreGuid.ColumnName = "store_guid";
                colvarStoreGuid.DataType = DbType.Guid;
                colvarStoreGuid.MaxLength = 0;
                colvarStoreGuid.AutoIncrement = false;
                colvarStoreGuid.IsNullable = true;
                colvarStoreGuid.IsPrimaryKey = false;
                colvarStoreGuid.IsForeignKey = false;
                colvarStoreGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarStoreGuid);
                
                TableSchema.TableColumn colvarUseStartTime = new TableSchema.TableColumn(schema);
                colvarUseStartTime.ColumnName = "use_start_time";
                colvarUseStartTime.DataType = DbType.DateTime;
                colvarUseStartTime.MaxLength = 0;
                colvarUseStartTime.AutoIncrement = false;
                colvarUseStartTime.IsNullable = true;
                colvarUseStartTime.IsPrimaryKey = false;
                colvarUseStartTime.IsForeignKey = false;
                colvarUseStartTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarUseStartTime);
                
                TableSchema.TableColumn colvarUseEndTime = new TableSchema.TableColumn(schema);
                colvarUseEndTime.ColumnName = "use_end_time";
                colvarUseEndTime.DataType = DbType.DateTime;
                colvarUseEndTime.MaxLength = 0;
                colvarUseEndTime.AutoIncrement = false;
                colvarUseEndTime.IsNullable = true;
                colvarUseEndTime.IsPrimaryKey = false;
                colvarUseEndTime.IsForeignKey = false;
                colvarUseEndTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarUseEndTime);
                
                TableSchema.TableColumn colvarAcctCode = new TableSchema.TableColumn(schema);
                colvarAcctCode.ColumnName = "acct_code";
                colvarAcctCode.DataType = DbType.AnsiString;
                colvarAcctCode.MaxLength = 10;
                colvarAcctCode.AutoIncrement = false;
                colvarAcctCode.IsNullable = true;
                colvarAcctCode.IsPrimaryKey = false;
                colvarAcctCode.IsForeignKey = false;
                colvarAcctCode.IsReadOnly = false;
                
                schema.Columns.Add(colvarAcctCode);
                
                TableSchema.TableColumn colvarAcctNo = new TableSchema.TableColumn(schema);
                colvarAcctNo.ColumnName = "acct_no";
                colvarAcctNo.DataType = DbType.AnsiString;
                colvarAcctNo.MaxLength = 10;
                colvarAcctNo.AutoIncrement = false;
                colvarAcctNo.IsNullable = true;
                colvarAcctNo.IsPrimaryKey = false;
                colvarAcctNo.IsForeignKey = false;
                colvarAcctNo.IsReadOnly = false;
                
                schema.Columns.Add(colvarAcctNo);
                
                TableSchema.TableColumn colvarAcctPw = new TableSchema.TableColumn(schema);
                colvarAcctPw.ColumnName = "acct_pw";
                colvarAcctPw.DataType = DbType.AnsiString;
                colvarAcctPw.MaxLength = 10;
                colvarAcctPw.AutoIncrement = false;
                colvarAcctPw.IsNullable = true;
                colvarAcctPw.IsPrimaryKey = false;
                colvarAcctPw.IsForeignKey = false;
                colvarAcctPw.IsReadOnly = false;
                
                schema.Columns.Add(colvarAcctPw);
                
                TableSchema.TableColumn colvarQuantity = new TableSchema.TableColumn(schema);
                colvarQuantity.ColumnName = "quantity";
                colvarQuantity.DataType = DbType.Int32;
                colvarQuantity.MaxLength = 0;
                colvarQuantity.AutoIncrement = false;
                colvarQuantity.IsNullable = true;
                colvarQuantity.IsPrimaryKey = false;
                colvarQuantity.IsForeignKey = false;
                colvarQuantity.IsReadOnly = false;
                
                schema.Columns.Add(colvarQuantity);
                
                TableSchema.TableColumn colvarChangedExpireTime = new TableSchema.TableColumn(schema);
                colvarChangedExpireTime.ColumnName = "changed_expire_time";
                colvarChangedExpireTime.DataType = DbType.DateTime;
                colvarChangedExpireTime.MaxLength = 0;
                colvarChangedExpireTime.AutoIncrement = false;
                colvarChangedExpireTime.IsNullable = true;
                colvarChangedExpireTime.IsPrimaryKey = false;
                colvarChangedExpireTime.IsForeignKey = false;
                colvarChangedExpireTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarChangedExpireTime);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_hi_deal_verification_acct",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewHiDealVerificationAcct()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewHiDealVerificationAcct(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewHiDealVerificationAcct(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewHiDealVerificationAcct(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("Id")]
        [Bindable(true)]
        public int? Id 
	    {
		    get
		    {
			    return GetColumnValue<int?>("id");
		    }
            set 
		    {
			    SetColumnValue("id", value);
            }
        }
	      
        [XmlAttribute("OrderDetailGuid")]
        [Bindable(true)]
        public Guid OrderDetailGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("order_detail_guid");
		    }
            set 
		    {
			    SetColumnValue("order_detail_guid", value);
            }
        }
	      
        [XmlAttribute("SellerGuid")]
        [Bindable(true)]
        public Guid SellerGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("seller_guid");
		    }
            set 
		    {
			    SetColumnValue("seller_guid", value);
            }
        }
	      
        [XmlAttribute("StoreGuid")]
        [Bindable(true)]
        public Guid? StoreGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid?>("store_guid");
		    }
            set 
		    {
			    SetColumnValue("store_guid", value);
            }
        }
	      
        [XmlAttribute("UseStartTime")]
        [Bindable(true)]
        public DateTime? UseStartTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("use_start_time");
		    }
            set 
		    {
			    SetColumnValue("use_start_time", value);
            }
        }
	      
        [XmlAttribute("UseEndTime")]
        [Bindable(true)]
        public DateTime? UseEndTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("use_end_time");
		    }
            set 
		    {
			    SetColumnValue("use_end_time", value);
            }
        }
	      
        [XmlAttribute("AcctCode")]
        [Bindable(true)]
        public string AcctCode 
	    {
		    get
		    {
			    return GetColumnValue<string>("acct_code");
		    }
            set 
		    {
			    SetColumnValue("acct_code", value);
            }
        }
	      
        [XmlAttribute("AcctNo")]
        [Bindable(true)]
        public string AcctNo 
	    {
		    get
		    {
			    return GetColumnValue<string>("acct_no");
		    }
            set 
		    {
			    SetColumnValue("acct_no", value);
            }
        }
	      
        [XmlAttribute("AcctPw")]
        [Bindable(true)]
        public string AcctPw 
	    {
		    get
		    {
			    return GetColumnValue<string>("acct_pw");
		    }
            set 
		    {
			    SetColumnValue("acct_pw", value);
            }
        }
	      
        [XmlAttribute("Quantity")]
        [Bindable(true)]
        public int? Quantity 
	    {
		    get
		    {
			    return GetColumnValue<int?>("quantity");
		    }
            set 
		    {
			    SetColumnValue("quantity", value);
            }
        }
	      
        [XmlAttribute("ChangedExpireTime")]
        [Bindable(true)]
        public DateTime? ChangedExpireTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("changed_expire_time");
		    }
            set 
		    {
			    SetColumnValue("changed_expire_time", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string Id = @"id";
            
            public static string OrderDetailGuid = @"order_detail_guid";
            
            public static string SellerGuid = @"seller_guid";
            
            public static string StoreGuid = @"store_guid";
            
            public static string UseStartTime = @"use_start_time";
            
            public static string UseEndTime = @"use_end_time";
            
            public static string AcctCode = @"acct_code";
            
            public static string AcctNo = @"acct_no";
            
            public static string AcctPw = @"acct_pw";
            
            public static string Quantity = @"quantity";
            
            public static string ChangedExpireTime = @"changed_expire_time";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

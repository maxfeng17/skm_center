using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewPponCoupon class.
    /// </summary>
    [Serializable]
    public partial class ViewPponCouponCollection : ReadOnlyList<ViewPponCoupon, ViewPponCouponCollection>
    {        
        public ViewPponCouponCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_ppon_coupon view.
    /// </summary>
    [Serializable]
    public partial class ViewPponCoupon : ReadOnlyRecord<ViewPponCoupon>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_ppon_coupon", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarCouponId = new TableSchema.TableColumn(schema);
                colvarCouponId.ColumnName = "coupon_id";
                colvarCouponId.DataType = DbType.Int32;
                colvarCouponId.MaxLength = 0;
                colvarCouponId.AutoIncrement = false;
                colvarCouponId.IsNullable = true;
                colvarCouponId.IsPrimaryKey = false;
                colvarCouponId.IsForeignKey = false;
                colvarCouponId.IsReadOnly = false;
                
                schema.Columns.Add(colvarCouponId);
                
                TableSchema.TableColumn colvarOrderDetailId = new TableSchema.TableColumn(schema);
                colvarOrderDetailId.ColumnName = "order_detail_id";
                colvarOrderDetailId.DataType = DbType.Guid;
                colvarOrderDetailId.MaxLength = 0;
                colvarOrderDetailId.AutoIncrement = false;
                colvarOrderDetailId.IsNullable = true;
                colvarOrderDetailId.IsPrimaryKey = false;
                colvarOrderDetailId.IsForeignKey = false;
                colvarOrderDetailId.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderDetailId);
                
                TableSchema.TableColumn colvarSequenceNumber = new TableSchema.TableColumn(schema);
                colvarSequenceNumber.ColumnName = "sequence_number";
                colvarSequenceNumber.DataType = DbType.AnsiString;
                colvarSequenceNumber.MaxLength = 50;
                colvarSequenceNumber.AutoIncrement = false;
                colvarSequenceNumber.IsNullable = true;
                colvarSequenceNumber.IsPrimaryKey = false;
                colvarSequenceNumber.IsForeignKey = false;
                colvarSequenceNumber.IsReadOnly = false;
                
                schema.Columns.Add(colvarSequenceNumber);
                
                TableSchema.TableColumn colvarCouponCode = new TableSchema.TableColumn(schema);
                colvarCouponCode.ColumnName = "coupon_code";
                colvarCouponCode.DataType = DbType.AnsiString;
                colvarCouponCode.MaxLength = 50;
                colvarCouponCode.AutoIncrement = false;
                colvarCouponCode.IsNullable = true;
                colvarCouponCode.IsPrimaryKey = false;
                colvarCouponCode.IsForeignKey = false;
                colvarCouponCode.IsReadOnly = false;
                
                schema.Columns.Add(colvarCouponCode);
                
                TableSchema.TableColumn colvarCouponDescription = new TableSchema.TableColumn(schema);
                colvarCouponDescription.ColumnName = "coupon_description";
                colvarCouponDescription.DataType = DbType.String;
                colvarCouponDescription.MaxLength = 50;
                colvarCouponDescription.AutoIncrement = false;
                colvarCouponDescription.IsNullable = true;
                colvarCouponDescription.IsPrimaryKey = false;
                colvarCouponDescription.IsForeignKey = false;
                colvarCouponDescription.IsReadOnly = false;
                
                schema.Columns.Add(colvarCouponDescription);
                
                TableSchema.TableColumn colvarCouponStatus = new TableSchema.TableColumn(schema);
                colvarCouponStatus.ColumnName = "coupon_status";
                colvarCouponStatus.DataType = DbType.Int32;
                colvarCouponStatus.MaxLength = 0;
                colvarCouponStatus.AutoIncrement = false;
                colvarCouponStatus.IsNullable = true;
                colvarCouponStatus.IsPrimaryKey = false;
                colvarCouponStatus.IsForeignKey = false;
                colvarCouponStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarCouponStatus);
                
                TableSchema.TableColumn colvarCouponAvailable = new TableSchema.TableColumn(schema);
                colvarCouponAvailable.ColumnName = "coupon_available";
                colvarCouponAvailable.DataType = DbType.Boolean;
                colvarCouponAvailable.MaxLength = 0;
                colvarCouponAvailable.AutoIncrement = false;
                colvarCouponAvailable.IsNullable = true;
                colvarCouponAvailable.IsPrimaryKey = false;
                colvarCouponAvailable.IsForeignKey = false;
                colvarCouponAvailable.IsReadOnly = false;
                
                schema.Columns.Add(colvarCouponAvailable);
                
                TableSchema.TableColumn colvarCouponStoreSequence = new TableSchema.TableColumn(schema);
                colvarCouponStoreSequence.ColumnName = "coupon_store_sequence";
                colvarCouponStoreSequence.DataType = DbType.Int32;
                colvarCouponStoreSequence.MaxLength = 0;
                colvarCouponStoreSequence.AutoIncrement = false;
                colvarCouponStoreSequence.IsNullable = true;
                colvarCouponStoreSequence.IsPrimaryKey = false;
                colvarCouponStoreSequence.IsForeignKey = false;
                colvarCouponStoreSequence.IsReadOnly = false;
                
                schema.Columns.Add(colvarCouponStoreSequence);
                
                TableSchema.TableColumn colvarIsReservationLock = new TableSchema.TableColumn(schema);
                colvarIsReservationLock.ColumnName = "is_reservation_lock";
                colvarIsReservationLock.DataType = DbType.Boolean;
                colvarIsReservationLock.MaxLength = 0;
                colvarIsReservationLock.AutoIncrement = false;
                colvarIsReservationLock.IsNullable = true;
                colvarIsReservationLock.IsPrimaryKey = false;
                colvarIsReservationLock.IsForeignKey = false;
                colvarIsReservationLock.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsReservationLock);
                
                TableSchema.TableColumn colvarOrderDetailGuid = new TableSchema.TableColumn(schema);
                colvarOrderDetailGuid.ColumnName = "order_detail_guid";
                colvarOrderDetailGuid.DataType = DbType.Guid;
                colvarOrderDetailGuid.MaxLength = 0;
                colvarOrderDetailGuid.AutoIncrement = false;
                colvarOrderDetailGuid.IsNullable = false;
                colvarOrderDetailGuid.IsPrimaryKey = false;
                colvarOrderDetailGuid.IsForeignKey = false;
                colvarOrderDetailGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderDetailGuid);
                
                TableSchema.TableColumn colvarOrderGuid = new TableSchema.TableColumn(schema);
                colvarOrderGuid.ColumnName = "order_GUID";
                colvarOrderGuid.DataType = DbType.Guid;
                colvarOrderGuid.MaxLength = 0;
                colvarOrderGuid.AutoIncrement = false;
                colvarOrderGuid.IsNullable = false;
                colvarOrderGuid.IsPrimaryKey = false;
                colvarOrderGuid.IsForeignKey = false;
                colvarOrderGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderGuid);
                
                TableSchema.TableColumn colvarItemName = new TableSchema.TableColumn(schema);
                colvarItemName.ColumnName = "item_name";
                colvarItemName.DataType = DbType.String;
                colvarItemName.MaxLength = 4000;
                colvarItemName.AutoIncrement = false;
                colvarItemName.IsNullable = true;
                colvarItemName.IsPrimaryKey = false;
                colvarItemName.IsForeignKey = false;
                colvarItemName.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemName);
                
                TableSchema.TableColumn colvarItemGuid = new TableSchema.TableColumn(schema);
                colvarItemGuid.ColumnName = "item_GUID";
                colvarItemGuid.DataType = DbType.Guid;
                colvarItemGuid.MaxLength = 0;
                colvarItemGuid.AutoIncrement = false;
                colvarItemGuid.IsNullable = true;
                colvarItemGuid.IsPrimaryKey = false;
                colvarItemGuid.IsForeignKey = false;
                colvarItemGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemGuid);
                
                TableSchema.TableColumn colvarItemUnitPrice = new TableSchema.TableColumn(schema);
                colvarItemUnitPrice.ColumnName = "item_unit_price";
                colvarItemUnitPrice.DataType = DbType.Currency;
                colvarItemUnitPrice.MaxLength = 0;
                colvarItemUnitPrice.AutoIncrement = false;
                colvarItemUnitPrice.IsNullable = false;
                colvarItemUnitPrice.IsPrimaryKey = false;
                colvarItemUnitPrice.IsForeignKey = false;
                colvarItemUnitPrice.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemUnitPrice);
                
                TableSchema.TableColumn colvarItemQuantity = new TableSchema.TableColumn(schema);
                colvarItemQuantity.ColumnName = "item_quantity";
                colvarItemQuantity.DataType = DbType.Int32;
                colvarItemQuantity.MaxLength = 0;
                colvarItemQuantity.AutoIncrement = false;
                colvarItemQuantity.IsNullable = false;
                colvarItemQuantity.IsPrimaryKey = false;
                colvarItemQuantity.IsForeignKey = false;
                colvarItemQuantity.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemQuantity);
                
                TableSchema.TableColumn colvarOrderDetailTotal = new TableSchema.TableColumn(schema);
                colvarOrderDetailTotal.ColumnName = "order_detail_total";
                colvarOrderDetailTotal.DataType = DbType.Currency;
                colvarOrderDetailTotal.MaxLength = 0;
                colvarOrderDetailTotal.AutoIncrement = false;
                colvarOrderDetailTotal.IsNullable = true;
                colvarOrderDetailTotal.IsPrimaryKey = false;
                colvarOrderDetailTotal.IsForeignKey = false;
                colvarOrderDetailTotal.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderDetailTotal);
                
                TableSchema.TableColumn colvarConsumerName = new TableSchema.TableColumn(schema);
                colvarConsumerName.ColumnName = "consumer_name";
                colvarConsumerName.DataType = DbType.String;
                colvarConsumerName.MaxLength = 100;
                colvarConsumerName.AutoIncrement = false;
                colvarConsumerName.IsNullable = true;
                colvarConsumerName.IsPrimaryKey = false;
                colvarConsumerName.IsForeignKey = false;
                colvarConsumerName.IsReadOnly = false;
                
                schema.Columns.Add(colvarConsumerName);
                
                TableSchema.TableColumn colvarConsumerTeleExt = new TableSchema.TableColumn(schema);
                colvarConsumerTeleExt.ColumnName = "consumer_tele_ext";
                colvarConsumerTeleExt.DataType = DbType.AnsiString;
                colvarConsumerTeleExt.MaxLength = 10;
                colvarConsumerTeleExt.AutoIncrement = false;
                colvarConsumerTeleExt.IsNullable = true;
                colvarConsumerTeleExt.IsPrimaryKey = false;
                colvarConsumerTeleExt.IsForeignKey = false;
                colvarConsumerTeleExt.IsReadOnly = false;
                
                schema.Columns.Add(colvarConsumerTeleExt);
                
                TableSchema.TableColumn colvarConsumerGroup = new TableSchema.TableColumn(schema);
                colvarConsumerGroup.ColumnName = "consumer_group";
                colvarConsumerGroup.DataType = DbType.String;
                colvarConsumerGroup.MaxLength = 50;
                colvarConsumerGroup.AutoIncrement = false;
                colvarConsumerGroup.IsNullable = true;
                colvarConsumerGroup.IsPrimaryKey = false;
                colvarConsumerGroup.IsForeignKey = false;
                colvarConsumerGroup.IsReadOnly = false;
                
                schema.Columns.Add(colvarConsumerGroup);
                
                TableSchema.TableColumn colvarOrderDetailStatus = new TableSchema.TableColumn(schema);
                colvarOrderDetailStatus.ColumnName = "order_detail_status";
                colvarOrderDetailStatus.DataType = DbType.Int32;
                colvarOrderDetailStatus.MaxLength = 0;
                colvarOrderDetailStatus.AutoIncrement = false;
                colvarOrderDetailStatus.IsNullable = false;
                colvarOrderDetailStatus.IsPrimaryKey = false;
                colvarOrderDetailStatus.IsForeignKey = false;
                colvarOrderDetailStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderDetailStatus);
                
                TableSchema.TableColumn colvarOrderDetailCreateId = new TableSchema.TableColumn(schema);
                colvarOrderDetailCreateId.ColumnName = "order_detail_create_id";
                colvarOrderDetailCreateId.DataType = DbType.String;
                colvarOrderDetailCreateId.MaxLength = 30;
                colvarOrderDetailCreateId.AutoIncrement = false;
                colvarOrderDetailCreateId.IsNullable = false;
                colvarOrderDetailCreateId.IsPrimaryKey = false;
                colvarOrderDetailCreateId.IsForeignKey = false;
                colvarOrderDetailCreateId.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderDetailCreateId);
                
                TableSchema.TableColumn colvarOrderDetailCreateTime = new TableSchema.TableColumn(schema);
                colvarOrderDetailCreateTime.ColumnName = "order_detail_create_time";
                colvarOrderDetailCreateTime.DataType = DbType.DateTime;
                colvarOrderDetailCreateTime.MaxLength = 0;
                colvarOrderDetailCreateTime.AutoIncrement = false;
                colvarOrderDetailCreateTime.IsNullable = false;
                colvarOrderDetailCreateTime.IsPrimaryKey = false;
                colvarOrderDetailCreateTime.IsForeignKey = false;
                colvarOrderDetailCreateTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderDetailCreateTime);
                
                TableSchema.TableColumn colvarOrderDetailModifyId = new TableSchema.TableColumn(schema);
                colvarOrderDetailModifyId.ColumnName = "order_detail_modify_id";
                colvarOrderDetailModifyId.DataType = DbType.String;
                colvarOrderDetailModifyId.MaxLength = 30;
                colvarOrderDetailModifyId.AutoIncrement = false;
                colvarOrderDetailModifyId.IsNullable = true;
                colvarOrderDetailModifyId.IsPrimaryKey = false;
                colvarOrderDetailModifyId.IsForeignKey = false;
                colvarOrderDetailModifyId.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderDetailModifyId);
                
                TableSchema.TableColumn colvarOrderDetailModifyTime = new TableSchema.TableColumn(schema);
                colvarOrderDetailModifyTime.ColumnName = "order_detail_modify_time";
                colvarOrderDetailModifyTime.DataType = DbType.DateTime;
                colvarOrderDetailModifyTime.MaxLength = 0;
                colvarOrderDetailModifyTime.AutoIncrement = false;
                colvarOrderDetailModifyTime.IsNullable = true;
                colvarOrderDetailModifyTime.IsPrimaryKey = false;
                colvarOrderDetailModifyTime.IsForeignKey = false;
                colvarOrderDetailModifyTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderDetailModifyTime);
                
                TableSchema.TableColumn colvarItemId = new TableSchema.TableColumn(schema);
                colvarItemId.ColumnName = "item_id";
                colvarItemId.DataType = DbType.String;
                colvarItemId.MaxLength = 100;
                colvarItemId.AutoIncrement = false;
                colvarItemId.IsNullable = true;
                colvarItemId.IsPrimaryKey = false;
                colvarItemId.IsForeignKey = false;
                colvarItemId.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemId);
                
                TableSchema.TableColumn colvarCityId = new TableSchema.TableColumn(schema);
                colvarCityId.ColumnName = "city_id";
                colvarCityId.DataType = DbType.Int32;
                colvarCityId.MaxLength = 0;
                colvarCityId.AutoIncrement = false;
                colvarCityId.IsNullable = true;
                colvarCityId.IsPrimaryKey = false;
                colvarCityId.IsForeignKey = false;
                colvarCityId.IsReadOnly = false;
                
                schema.Columns.Add(colvarCityId);
                
                TableSchema.TableColumn colvarParentOrderId = new TableSchema.TableColumn(schema);
                colvarParentOrderId.ColumnName = "parent_order_id";
                colvarParentOrderId.DataType = DbType.Guid;
                colvarParentOrderId.MaxLength = 0;
                colvarParentOrderId.AutoIncrement = false;
                colvarParentOrderId.IsNullable = true;
                colvarParentOrderId.IsPrimaryKey = false;
                colvarParentOrderId.IsForeignKey = false;
                colvarParentOrderId.IsReadOnly = false;
                
                schema.Columns.Add(colvarParentOrderId);
                
                TableSchema.TableColumn colvarGroupOrderStatus = new TableSchema.TableColumn(schema);
                colvarGroupOrderStatus.ColumnName = "group_order_status";
                colvarGroupOrderStatus.DataType = DbType.Int32;
                colvarGroupOrderStatus.MaxLength = 0;
                colvarGroupOrderStatus.AutoIncrement = false;
                colvarGroupOrderStatus.IsNullable = true;
                colvarGroupOrderStatus.IsPrimaryKey = false;
                colvarGroupOrderStatus.IsForeignKey = false;
                colvarGroupOrderStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarGroupOrderStatus);
                
                TableSchema.TableColumn colvarBusinessHourId = new TableSchema.TableColumn(schema);
                colvarBusinessHourId.ColumnName = "business_hour_id";
                colvarBusinessHourId.DataType = DbType.AnsiString;
                colvarBusinessHourId.MaxLength = 20;
                colvarBusinessHourId.AutoIncrement = false;
                colvarBusinessHourId.IsNullable = true;
                colvarBusinessHourId.IsPrimaryKey = false;
                colvarBusinessHourId.IsForeignKey = false;
                colvarBusinessHourId.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourId);
                
                TableSchema.TableColumn colvarBusinessHourTypeId = new TableSchema.TableColumn(schema);
                colvarBusinessHourTypeId.ColumnName = "business_hour_type_id";
                colvarBusinessHourTypeId.DataType = DbType.Int32;
                colvarBusinessHourTypeId.MaxLength = 0;
                colvarBusinessHourTypeId.AutoIncrement = false;
                colvarBusinessHourTypeId.IsNullable = false;
                colvarBusinessHourTypeId.IsPrimaryKey = false;
                colvarBusinessHourTypeId.IsForeignKey = false;
                colvarBusinessHourTypeId.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourTypeId);
                
                TableSchema.TableColumn colvarBusinessHourOrderTimeS = new TableSchema.TableColumn(schema);
                colvarBusinessHourOrderTimeS.ColumnName = "business_hour_order_time_s";
                colvarBusinessHourOrderTimeS.DataType = DbType.DateTime;
                colvarBusinessHourOrderTimeS.MaxLength = 0;
                colvarBusinessHourOrderTimeS.AutoIncrement = false;
                colvarBusinessHourOrderTimeS.IsNullable = false;
                colvarBusinessHourOrderTimeS.IsPrimaryKey = false;
                colvarBusinessHourOrderTimeS.IsForeignKey = false;
                colvarBusinessHourOrderTimeS.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourOrderTimeS);
                
                TableSchema.TableColumn colvarBusinessHourOrderTimeE = new TableSchema.TableColumn(schema);
                colvarBusinessHourOrderTimeE.ColumnName = "business_hour_order_time_e";
                colvarBusinessHourOrderTimeE.DataType = DbType.DateTime;
                colvarBusinessHourOrderTimeE.MaxLength = 0;
                colvarBusinessHourOrderTimeE.AutoIncrement = false;
                colvarBusinessHourOrderTimeE.IsNullable = false;
                colvarBusinessHourOrderTimeE.IsPrimaryKey = false;
                colvarBusinessHourOrderTimeE.IsForeignKey = false;
                colvarBusinessHourOrderTimeE.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourOrderTimeE);
                
                TableSchema.TableColumn colvarBusinessHourDeliverTimeS = new TableSchema.TableColumn(schema);
                colvarBusinessHourDeliverTimeS.ColumnName = "business_hour_deliver_time_s";
                colvarBusinessHourDeliverTimeS.DataType = DbType.DateTime;
                colvarBusinessHourDeliverTimeS.MaxLength = 0;
                colvarBusinessHourDeliverTimeS.AutoIncrement = false;
                colvarBusinessHourDeliverTimeS.IsNullable = true;
                colvarBusinessHourDeliverTimeS.IsPrimaryKey = false;
                colvarBusinessHourDeliverTimeS.IsForeignKey = false;
                colvarBusinessHourDeliverTimeS.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourDeliverTimeS);
                
                TableSchema.TableColumn colvarBusinessHourDeliverTimeE = new TableSchema.TableColumn(schema);
                colvarBusinessHourDeliverTimeE.ColumnName = "business_hour_deliver_time_e";
                colvarBusinessHourDeliverTimeE.DataType = DbType.DateTime;
                colvarBusinessHourDeliverTimeE.MaxLength = 0;
                colvarBusinessHourDeliverTimeE.AutoIncrement = false;
                colvarBusinessHourDeliverTimeE.IsNullable = true;
                colvarBusinessHourDeliverTimeE.IsPrimaryKey = false;
                colvarBusinessHourDeliverTimeE.IsForeignKey = false;
                colvarBusinessHourDeliverTimeE.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourDeliverTimeE);
                
                TableSchema.TableColumn colvarBusinessHourDeliveryCharge = new TableSchema.TableColumn(schema);
                colvarBusinessHourDeliveryCharge.ColumnName = "business_hour_delivery_charge";
                colvarBusinessHourDeliveryCharge.DataType = DbType.Currency;
                colvarBusinessHourDeliveryCharge.MaxLength = 0;
                colvarBusinessHourDeliveryCharge.AutoIncrement = false;
                colvarBusinessHourDeliveryCharge.IsNullable = false;
                colvarBusinessHourDeliveryCharge.IsPrimaryKey = false;
                colvarBusinessHourDeliveryCharge.IsForeignKey = false;
                colvarBusinessHourDeliveryCharge.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourDeliveryCharge);
                
                TableSchema.TableColumn colvarBusinessHourOrderMinimum = new TableSchema.TableColumn(schema);
                colvarBusinessHourOrderMinimum.ColumnName = "business_hour_order_minimum";
                colvarBusinessHourOrderMinimum.DataType = DbType.Currency;
                colvarBusinessHourOrderMinimum.MaxLength = 0;
                colvarBusinessHourOrderMinimum.AutoIncrement = false;
                colvarBusinessHourOrderMinimum.IsNullable = false;
                colvarBusinessHourOrderMinimum.IsPrimaryKey = false;
                colvarBusinessHourOrderMinimum.IsForeignKey = false;
                colvarBusinessHourOrderMinimum.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourOrderMinimum);
                
                TableSchema.TableColumn colvarBusinessHourPreparationTime = new TableSchema.TableColumn(schema);
                colvarBusinessHourPreparationTime.ColumnName = "business_hour_preparation_time";
                colvarBusinessHourPreparationTime.DataType = DbType.Int32;
                colvarBusinessHourPreparationTime.MaxLength = 0;
                colvarBusinessHourPreparationTime.AutoIncrement = false;
                colvarBusinessHourPreparationTime.IsNullable = false;
                colvarBusinessHourPreparationTime.IsPrimaryKey = false;
                colvarBusinessHourPreparationTime.IsForeignKey = false;
                colvarBusinessHourPreparationTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourPreparationTime);
                
                TableSchema.TableColumn colvarBusinessHourOnline = new TableSchema.TableColumn(schema);
                colvarBusinessHourOnline.ColumnName = "business_hour_online";
                colvarBusinessHourOnline.DataType = DbType.Boolean;
                colvarBusinessHourOnline.MaxLength = 0;
                colvarBusinessHourOnline.AutoIncrement = false;
                colvarBusinessHourOnline.IsNullable = false;
                colvarBusinessHourOnline.IsPrimaryKey = false;
                colvarBusinessHourOnline.IsForeignKey = false;
                colvarBusinessHourOnline.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourOnline);
                
                TableSchema.TableColumn colvarBusinessHourStatus = new TableSchema.TableColumn(schema);
                colvarBusinessHourStatus.ColumnName = "business_hour_status";
                colvarBusinessHourStatus.DataType = DbType.Int32;
                colvarBusinessHourStatus.MaxLength = 0;
                colvarBusinessHourStatus.AutoIncrement = false;
                colvarBusinessHourStatus.IsNullable = false;
                colvarBusinessHourStatus.IsPrimaryKey = false;
                colvarBusinessHourStatus.IsForeignKey = false;
                colvarBusinessHourStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourStatus);
                
                TableSchema.TableColumn colvarOrderTotalLimit = new TableSchema.TableColumn(schema);
                colvarOrderTotalLimit.ColumnName = "order_total_limit";
                colvarOrderTotalLimit.DataType = DbType.Currency;
                colvarOrderTotalLimit.MaxLength = 0;
                colvarOrderTotalLimit.AutoIncrement = false;
                colvarOrderTotalLimit.IsNullable = true;
                colvarOrderTotalLimit.IsPrimaryKey = false;
                colvarOrderTotalLimit.IsForeignKey = false;
                colvarOrderTotalLimit.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderTotalLimit);
                
                TableSchema.TableColumn colvarDeliveryLimit = new TableSchema.TableColumn(schema);
                colvarDeliveryLimit.ColumnName = "delivery_limit";
                colvarDeliveryLimit.DataType = DbType.Int32;
                colvarDeliveryLimit.MaxLength = 0;
                colvarDeliveryLimit.AutoIncrement = false;
                colvarDeliveryLimit.IsNullable = true;
                colvarDeliveryLimit.IsPrimaryKey = false;
                colvarDeliveryLimit.IsForeignKey = false;
                colvarDeliveryLimit.IsReadOnly = false;
                
                schema.Columns.Add(colvarDeliveryLimit);
                
                TableSchema.TableColumn colvarHoliday = new TableSchema.TableColumn(schema);
                colvarHoliday.ColumnName = "holiday";
                colvarHoliday.DataType = DbType.Int32;
                colvarHoliday.MaxLength = 0;
                colvarHoliday.AutoIncrement = false;
                colvarHoliday.IsNullable = false;
                colvarHoliday.IsPrimaryKey = false;
                colvarHoliday.IsForeignKey = false;
                colvarHoliday.IsReadOnly = false;
                
                schema.Columns.Add(colvarHoliday);
                
                TableSchema.TableColumn colvarCouponEventId = new TableSchema.TableColumn(schema);
                colvarCouponEventId.ColumnName = "coupon_event_id";
                colvarCouponEventId.DataType = DbType.Int32;
                colvarCouponEventId.MaxLength = 0;
                colvarCouponEventId.AutoIncrement = false;
                colvarCouponEventId.IsNullable = false;
                colvarCouponEventId.IsPrimaryKey = false;
                colvarCouponEventId.IsForeignKey = false;
                colvarCouponEventId.IsReadOnly = false;
                
                schema.Columns.Add(colvarCouponEventId);
                
                TableSchema.TableColumn colvarBusinessHourGuid = new TableSchema.TableColumn(schema);
                colvarBusinessHourGuid.ColumnName = "business_hour_guid";
                colvarBusinessHourGuid.DataType = DbType.Guid;
                colvarBusinessHourGuid.MaxLength = 0;
                colvarBusinessHourGuid.AutoIncrement = false;
                colvarBusinessHourGuid.IsNullable = false;
                colvarBusinessHourGuid.IsPrimaryKey = false;
                colvarBusinessHourGuid.IsForeignKey = false;
                colvarBusinessHourGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourGuid);
                
                TableSchema.TableColumn colvarEventName = new TableSchema.TableColumn(schema);
                colvarEventName.ColumnName = "event_name";
                colvarEventName.DataType = DbType.String;
                colvarEventName.MaxLength = 400;
                colvarEventName.AutoIncrement = false;
                colvarEventName.IsNullable = true;
                colvarEventName.IsPrimaryKey = false;
                colvarEventName.IsForeignKey = false;
                colvarEventName.IsReadOnly = false;
                
                schema.Columns.Add(colvarEventName);
                
                TableSchema.TableColumn colvarEventImagePath = new TableSchema.TableColumn(schema);
                colvarEventImagePath.ColumnName = "event_image_path";
                colvarEventImagePath.DataType = DbType.String;
                colvarEventImagePath.MaxLength = 1000;
                colvarEventImagePath.AutoIncrement = false;
                colvarEventImagePath.IsNullable = true;
                colvarEventImagePath.IsPrimaryKey = false;
                colvarEventImagePath.IsForeignKey = false;
                colvarEventImagePath.IsReadOnly = false;
                
                schema.Columns.Add(colvarEventImagePath);
                
                TableSchema.TableColumn colvarIntroduction = new TableSchema.TableColumn(schema);
                colvarIntroduction.ColumnName = "introduction";
                colvarIntroduction.DataType = DbType.String;
                colvarIntroduction.MaxLength = 1073741823;
                colvarIntroduction.AutoIncrement = false;
                colvarIntroduction.IsNullable = true;
                colvarIntroduction.IsPrimaryKey = false;
                colvarIntroduction.IsForeignKey = false;
                colvarIntroduction.IsReadOnly = false;
                
                schema.Columns.Add(colvarIntroduction);
                
                TableSchema.TableColumn colvarRestrictions = new TableSchema.TableColumn(schema);
                colvarRestrictions.ColumnName = "restrictions";
                colvarRestrictions.DataType = DbType.String;
                colvarRestrictions.MaxLength = 1073741823;
                colvarRestrictions.AutoIncrement = false;
                colvarRestrictions.IsNullable = true;
                colvarRestrictions.IsPrimaryKey = false;
                colvarRestrictions.IsForeignKey = false;
                colvarRestrictions.IsReadOnly = false;
                
                schema.Columns.Add(colvarRestrictions);
                
                TableSchema.TableColumn colvarCouponEventDescription = new TableSchema.TableColumn(schema);
                colvarCouponEventDescription.ColumnName = "coupon_event_description";
                colvarCouponEventDescription.DataType = DbType.String;
                colvarCouponEventDescription.MaxLength = 1073741823;
                colvarCouponEventDescription.AutoIncrement = false;
                colvarCouponEventDescription.IsNullable = true;
                colvarCouponEventDescription.IsPrimaryKey = false;
                colvarCouponEventDescription.IsForeignKey = false;
                colvarCouponEventDescription.IsReadOnly = false;
                
                schema.Columns.Add(colvarCouponEventDescription);
                
                TableSchema.TableColumn colvarReferenceText = new TableSchema.TableColumn(schema);
                colvarReferenceText.ColumnName = "reference_text";
                colvarReferenceText.DataType = DbType.String;
                colvarReferenceText.MaxLength = 1073741823;
                colvarReferenceText.AutoIncrement = false;
                colvarReferenceText.IsNullable = true;
                colvarReferenceText.IsPrimaryKey = false;
                colvarReferenceText.IsForeignKey = false;
                colvarReferenceText.IsReadOnly = false;
                
                schema.Columns.Add(colvarReferenceText);
                
                TableSchema.TableColumn colvarCouponUsage = new TableSchema.TableColumn(schema);
                colvarCouponUsage.ColumnName = "coupon_usage";
                colvarCouponUsage.DataType = DbType.String;
                colvarCouponUsage.MaxLength = 500;
                colvarCouponUsage.AutoIncrement = false;
                colvarCouponUsage.IsNullable = true;
                colvarCouponUsage.IsPrimaryKey = false;
                colvarCouponUsage.IsForeignKey = false;
                colvarCouponUsage.IsReadOnly = false;
                
                schema.Columns.Add(colvarCouponUsage);
                
                TableSchema.TableColumn colvarRemark = new TableSchema.TableColumn(schema);
                colvarRemark.ColumnName = "remark";
                colvarRemark.DataType = DbType.String;
                colvarRemark.MaxLength = 1073741823;
                colvarRemark.AutoIncrement = false;
                colvarRemark.IsNullable = true;
                colvarRemark.IsPrimaryKey = false;
                colvarRemark.IsForeignKey = false;
                colvarRemark.IsReadOnly = false;
                
                schema.Columns.Add(colvarRemark);
                
                TableSchema.TableColumn colvarSellerGuid = new TableSchema.TableColumn(schema);
                colvarSellerGuid.ColumnName = "seller_guid";
                colvarSellerGuid.DataType = DbType.Guid;
                colvarSellerGuid.MaxLength = 0;
                colvarSellerGuid.AutoIncrement = false;
                colvarSellerGuid.IsNullable = false;
                colvarSellerGuid.IsPrimaryKey = false;
                colvarSellerGuid.IsForeignKey = false;
                colvarSellerGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerGuid);
                
                TableSchema.TableColumn colvarSellerId = new TableSchema.TableColumn(schema);
                colvarSellerId.ColumnName = "seller_id";
                colvarSellerId.DataType = DbType.AnsiString;
                colvarSellerId.MaxLength = 20;
                colvarSellerId.AutoIncrement = false;
                colvarSellerId.IsNullable = true;
                colvarSellerId.IsPrimaryKey = false;
                colvarSellerId.IsForeignKey = false;
                colvarSellerId.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerId);
                
                TableSchema.TableColumn colvarSellerName = new TableSchema.TableColumn(schema);
                colvarSellerName.ColumnName = "seller_name";
                colvarSellerName.DataType = DbType.String;
                colvarSellerName.MaxLength = 100;
                colvarSellerName.AutoIncrement = false;
                colvarSellerName.IsNullable = false;
                colvarSellerName.IsPrimaryKey = false;
                colvarSellerName.IsForeignKey = false;
                colvarSellerName.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerName);
                
                TableSchema.TableColumn colvarSellerBossName = new TableSchema.TableColumn(schema);
                colvarSellerBossName.ColumnName = "seller_boss_name";
                colvarSellerBossName.DataType = DbType.String;
                colvarSellerBossName.MaxLength = 30;
                colvarSellerBossName.AutoIncrement = false;
                colvarSellerBossName.IsNullable = true;
                colvarSellerBossName.IsPrimaryKey = false;
                colvarSellerBossName.IsForeignKey = false;
                colvarSellerBossName.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerBossName);
                
                TableSchema.TableColumn colvarSellerTel = new TableSchema.TableColumn(schema);
                colvarSellerTel.ColumnName = "seller_tel";
                colvarSellerTel.DataType = DbType.AnsiString;
                colvarSellerTel.MaxLength = 100;
                colvarSellerTel.AutoIncrement = false;
                colvarSellerTel.IsNullable = true;
                colvarSellerTel.IsPrimaryKey = false;
                colvarSellerTel.IsForeignKey = false;
                colvarSellerTel.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerTel);
                
                TableSchema.TableColumn colvarSellerTel2 = new TableSchema.TableColumn(schema);
                colvarSellerTel2.ColumnName = "seller_tel2";
                colvarSellerTel2.DataType = DbType.AnsiString;
                colvarSellerTel2.MaxLength = 100;
                colvarSellerTel2.AutoIncrement = false;
                colvarSellerTel2.IsNullable = true;
                colvarSellerTel2.IsPrimaryKey = false;
                colvarSellerTel2.IsForeignKey = false;
                colvarSellerTel2.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerTel2);
                
                TableSchema.TableColumn colvarSellerFax = new TableSchema.TableColumn(schema);
                colvarSellerFax.ColumnName = "seller_fax";
                colvarSellerFax.DataType = DbType.AnsiString;
                colvarSellerFax.MaxLength = 20;
                colvarSellerFax.AutoIncrement = false;
                colvarSellerFax.IsNullable = true;
                colvarSellerFax.IsPrimaryKey = false;
                colvarSellerFax.IsForeignKey = false;
                colvarSellerFax.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerFax);
                
                TableSchema.TableColumn colvarSellerMobile = new TableSchema.TableColumn(schema);
                colvarSellerMobile.ColumnName = "seller_mobile";
                colvarSellerMobile.DataType = DbType.AnsiString;
                colvarSellerMobile.MaxLength = 100;
                colvarSellerMobile.AutoIncrement = false;
                colvarSellerMobile.IsNullable = true;
                colvarSellerMobile.IsPrimaryKey = false;
                colvarSellerMobile.IsForeignKey = false;
                colvarSellerMobile.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerMobile);
                
                TableSchema.TableColumn colvarSellerAddress = new TableSchema.TableColumn(schema);
                colvarSellerAddress.ColumnName = "seller_address";
                colvarSellerAddress.DataType = DbType.String;
                colvarSellerAddress.MaxLength = 100;
                colvarSellerAddress.AutoIncrement = false;
                colvarSellerAddress.IsNullable = true;
                colvarSellerAddress.IsPrimaryKey = false;
                colvarSellerAddress.IsForeignKey = false;
                colvarSellerAddress.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerAddress);
                
                TableSchema.TableColumn colvarSellerEmail = new TableSchema.TableColumn(schema);
                colvarSellerEmail.ColumnName = "seller_email";
                colvarSellerEmail.DataType = DbType.AnsiString;
                colvarSellerEmail.MaxLength = 200;
                colvarSellerEmail.AutoIncrement = false;
                colvarSellerEmail.IsNullable = true;
                colvarSellerEmail.IsPrimaryKey = false;
                colvarSellerEmail.IsForeignKey = false;
                colvarSellerEmail.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerEmail);
                
                TableSchema.TableColumn colvarSellerBlog = new TableSchema.TableColumn(schema);
                colvarSellerBlog.ColumnName = "seller_blog";
                colvarSellerBlog.DataType = DbType.AnsiString;
                colvarSellerBlog.MaxLength = 100;
                colvarSellerBlog.AutoIncrement = false;
                colvarSellerBlog.IsNullable = true;
                colvarSellerBlog.IsPrimaryKey = false;
                colvarSellerBlog.IsForeignKey = false;
                colvarSellerBlog.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerBlog);
                
                TableSchema.TableColumn colvarSellerInvoice = new TableSchema.TableColumn(schema);
                colvarSellerInvoice.ColumnName = "seller_invoice";
                colvarSellerInvoice.DataType = DbType.String;
                colvarSellerInvoice.MaxLength = 50;
                colvarSellerInvoice.AutoIncrement = false;
                colvarSellerInvoice.IsNullable = true;
                colvarSellerInvoice.IsPrimaryKey = false;
                colvarSellerInvoice.IsForeignKey = false;
                colvarSellerInvoice.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerInvoice);
                
                TableSchema.TableColumn colvarSellerDescription = new TableSchema.TableColumn(schema);
                colvarSellerDescription.ColumnName = "seller_description";
                colvarSellerDescription.DataType = DbType.String;
                colvarSellerDescription.MaxLength = 1073741823;
                colvarSellerDescription.AutoIncrement = false;
                colvarSellerDescription.IsNullable = true;
                colvarSellerDescription.IsPrimaryKey = false;
                colvarSellerDescription.IsForeignKey = false;
                colvarSellerDescription.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerDescription);
                
                TableSchema.TableColumn colvarSellerStatus = new TableSchema.TableColumn(schema);
                colvarSellerStatus.ColumnName = "seller_status";
                colvarSellerStatus.DataType = DbType.Int32;
                colvarSellerStatus.MaxLength = 0;
                colvarSellerStatus.AutoIncrement = false;
                colvarSellerStatus.IsNullable = false;
                colvarSellerStatus.IsPrimaryKey = false;
                colvarSellerStatus.IsForeignKey = false;
                colvarSellerStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerStatus);
                
                TableSchema.TableColumn colvarDepartment = new TableSchema.TableColumn(schema);
                colvarDepartment.ColumnName = "department";
                colvarDepartment.DataType = DbType.Int32;
                colvarDepartment.MaxLength = 0;
                colvarDepartment.AutoIncrement = false;
                colvarDepartment.IsNullable = false;
                colvarDepartment.IsPrimaryKey = false;
                colvarDepartment.IsForeignKey = false;
                colvarDepartment.IsReadOnly = false;
                
                schema.Columns.Add(colvarDepartment);
                
                TableSchema.TableColumn colvarSellerRemark = new TableSchema.TableColumn(schema);
                colvarSellerRemark.ColumnName = "seller_remark";
                colvarSellerRemark.DataType = DbType.String;
                colvarSellerRemark.MaxLength = 200;
                colvarSellerRemark.AutoIncrement = false;
                colvarSellerRemark.IsNullable = true;
                colvarSellerRemark.IsPrimaryKey = false;
                colvarSellerRemark.IsForeignKey = false;
                colvarSellerRemark.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerRemark);
                
                TableSchema.TableColumn colvarSellerSales = new TableSchema.TableColumn(schema);
                colvarSellerSales.ColumnName = "seller_sales";
                colvarSellerSales.DataType = DbType.String;
                colvarSellerSales.MaxLength = 50;
                colvarSellerSales.AutoIncrement = false;
                colvarSellerSales.IsNullable = true;
                colvarSellerSales.IsPrimaryKey = false;
                colvarSellerSales.IsForeignKey = false;
                colvarSellerSales.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerSales);
                
                TableSchema.TableColumn colvarSellerLogoimgPath = new TableSchema.TableColumn(schema);
                colvarSellerLogoimgPath.ColumnName = "seller_logoimg_path";
                colvarSellerLogoimgPath.DataType = DbType.AnsiString;
                colvarSellerLogoimgPath.MaxLength = 500;
                colvarSellerLogoimgPath.AutoIncrement = false;
                colvarSellerLogoimgPath.IsNullable = true;
                colvarSellerLogoimgPath.IsPrimaryKey = false;
                colvarSellerLogoimgPath.IsForeignKey = false;
                colvarSellerLogoimgPath.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerLogoimgPath);
                
                TableSchema.TableColumn colvarSellerVideoPath = new TableSchema.TableColumn(schema);
                colvarSellerVideoPath.ColumnName = "seller_video_path";
                colvarSellerVideoPath.DataType = DbType.AnsiString;
                colvarSellerVideoPath.MaxLength = 100;
                colvarSellerVideoPath.AutoIncrement = false;
                colvarSellerVideoPath.IsNullable = true;
                colvarSellerVideoPath.IsPrimaryKey = false;
                colvarSellerVideoPath.IsForeignKey = false;
                colvarSellerVideoPath.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerVideoPath);
                
                TableSchema.TableColumn colvarSellerCreateId = new TableSchema.TableColumn(schema);
                colvarSellerCreateId.ColumnName = "seller_create_id";
                colvarSellerCreateId.DataType = DbType.String;
                colvarSellerCreateId.MaxLength = 30;
                colvarSellerCreateId.AutoIncrement = false;
                colvarSellerCreateId.IsNullable = false;
                colvarSellerCreateId.IsPrimaryKey = false;
                colvarSellerCreateId.IsForeignKey = false;
                colvarSellerCreateId.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerCreateId);
                
                TableSchema.TableColumn colvarSellerCreateTime = new TableSchema.TableColumn(schema);
                colvarSellerCreateTime.ColumnName = "seller_create_time";
                colvarSellerCreateTime.DataType = DbType.DateTime;
                colvarSellerCreateTime.MaxLength = 0;
                colvarSellerCreateTime.AutoIncrement = false;
                colvarSellerCreateTime.IsNullable = false;
                colvarSellerCreateTime.IsPrimaryKey = false;
                colvarSellerCreateTime.IsForeignKey = false;
                colvarSellerCreateTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerCreateTime);
                
                TableSchema.TableColumn colvarSellerModifyId = new TableSchema.TableColumn(schema);
                colvarSellerModifyId.ColumnName = "seller_modify_id";
                colvarSellerModifyId.DataType = DbType.String;
                colvarSellerModifyId.MaxLength = 30;
                colvarSellerModifyId.AutoIncrement = false;
                colvarSellerModifyId.IsNullable = true;
                colvarSellerModifyId.IsPrimaryKey = false;
                colvarSellerModifyId.IsForeignKey = false;
                colvarSellerModifyId.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerModifyId);
                
                TableSchema.TableColumn colvarSellerModifyTime = new TableSchema.TableColumn(schema);
                colvarSellerModifyTime.ColumnName = "seller_modify_time";
                colvarSellerModifyTime.DataType = DbType.DateTime;
                colvarSellerModifyTime.MaxLength = 0;
                colvarSellerModifyTime.AutoIncrement = false;
                colvarSellerModifyTime.IsNullable = true;
                colvarSellerModifyTime.IsPrimaryKey = false;
                colvarSellerModifyTime.IsForeignKey = false;
                colvarSellerModifyTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerModifyTime);
                
                TableSchema.TableColumn colvarDefaultCommissionRate = new TableSchema.TableColumn(schema);
                colvarDefaultCommissionRate.ColumnName = "default_commission_rate";
                colvarDefaultCommissionRate.DataType = DbType.Double;
                colvarDefaultCommissionRate.MaxLength = 0;
                colvarDefaultCommissionRate.AutoIncrement = false;
                colvarDefaultCommissionRate.IsNullable = true;
                colvarDefaultCommissionRate.IsPrimaryKey = false;
                colvarDefaultCommissionRate.IsForeignKey = false;
                colvarDefaultCommissionRate.IsReadOnly = false;
                
                schema.Columns.Add(colvarDefaultCommissionRate);
                
                TableSchema.TableColumn colvarBillingCode = new TableSchema.TableColumn(schema);
                colvarBillingCode.ColumnName = "billing_code";
                colvarBillingCode.DataType = DbType.String;
                colvarBillingCode.MaxLength = 50;
                colvarBillingCode.AutoIncrement = false;
                colvarBillingCode.IsNullable = true;
                colvarBillingCode.IsPrimaryKey = false;
                colvarBillingCode.IsForeignKey = false;
                colvarBillingCode.IsReadOnly = false;
                
                schema.Columns.Add(colvarBillingCode);
                
                TableSchema.TableColumn colvarWeight = new TableSchema.TableColumn(schema);
                colvarWeight.ColumnName = "weight";
                colvarWeight.DataType = DbType.Int32;
                colvarWeight.MaxLength = 0;
                colvarWeight.AutoIncrement = false;
                colvarWeight.IsNullable = true;
                colvarWeight.IsPrimaryKey = false;
                colvarWeight.IsForeignKey = false;
                colvarWeight.IsReadOnly = false;
                
                schema.Columns.Add(colvarWeight);
                
                TableSchema.TableColumn colvarPostCkoutAction = new TableSchema.TableColumn(schema);
                colvarPostCkoutAction.ColumnName = "post_ckout_action";
                colvarPostCkoutAction.DataType = DbType.Int32;
                colvarPostCkoutAction.MaxLength = 0;
                colvarPostCkoutAction.AutoIncrement = false;
                colvarPostCkoutAction.IsNullable = false;
                colvarPostCkoutAction.IsPrimaryKey = false;
                colvarPostCkoutAction.IsForeignKey = false;
                colvarPostCkoutAction.IsReadOnly = false;
                
                schema.Columns.Add(colvarPostCkoutAction);
                
                TableSchema.TableColumn colvarPostCkoutArgs = new TableSchema.TableColumn(schema);
                colvarPostCkoutArgs.ColumnName = "post_ckout_args";
                colvarPostCkoutArgs.DataType = DbType.String;
                colvarPostCkoutArgs.MaxLength = 150;
                colvarPostCkoutArgs.AutoIncrement = false;
                colvarPostCkoutArgs.IsNullable = true;
                colvarPostCkoutArgs.IsPrimaryKey = false;
                colvarPostCkoutArgs.IsForeignKey = false;
                colvarPostCkoutArgs.IsReadOnly = false;
                
                schema.Columns.Add(colvarPostCkoutArgs);
                
                TableSchema.TableColumn colvarCoordinate = new TableSchema.TableColumn(schema);
                colvarCoordinate.ColumnName = "coordinate";
                colvarCoordinate.DataType = DbType.AnsiString;
                colvarCoordinate.MaxLength = -1;
                colvarCoordinate.AutoIncrement = false;
                colvarCoordinate.IsNullable = true;
                colvarCoordinate.IsPrimaryKey = false;
                colvarCoordinate.IsForeignKey = false;
                colvarCoordinate.IsReadOnly = false;
                
                schema.Columns.Add(colvarCoordinate);
                
                TableSchema.TableColumn colvarDeliveryMinuteGap = new TableSchema.TableColumn(schema);
                colvarDeliveryMinuteGap.ColumnName = "delivery_minute_gap";
                colvarDeliveryMinuteGap.DataType = DbType.Int32;
                colvarDeliveryMinuteGap.MaxLength = 0;
                colvarDeliveryMinuteGap.AutoIncrement = false;
                colvarDeliveryMinuteGap.IsNullable = true;
                colvarDeliveryMinuteGap.IsPrimaryKey = false;
                colvarDeliveryMinuteGap.IsForeignKey = false;
                colvarDeliveryMinuteGap.IsReadOnly = false;
                
                schema.Columns.Add(colvarDeliveryMinuteGap);
                
                TableSchema.TableColumn colvarSellerCityId = new TableSchema.TableColumn(schema);
                colvarSellerCityId.ColumnName = "seller_city_id";
                colvarSellerCityId.DataType = DbType.Int32;
                colvarSellerCityId.MaxLength = 0;
                colvarSellerCityId.AutoIncrement = false;
                colvarSellerCityId.IsNullable = false;
                colvarSellerCityId.IsPrimaryKey = false;
                colvarSellerCityId.IsForeignKey = false;
                colvarSellerCityId.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerCityId);
                
                TableSchema.TableColumn colvarDeliveryAddress = new TableSchema.TableColumn(schema);
                colvarDeliveryAddress.ColumnName = "delivery_address";
                colvarDeliveryAddress.DataType = DbType.String;
                colvarDeliveryAddress.MaxLength = 200;
                colvarDeliveryAddress.AutoIncrement = false;
                colvarDeliveryAddress.IsNullable = true;
                colvarDeliveryAddress.IsPrimaryKey = false;
                colvarDeliveryAddress.IsForeignKey = false;
                colvarDeliveryAddress.IsReadOnly = false;
                
                schema.Columns.Add(colvarDeliveryAddress);
                
                TableSchema.TableColumn colvarDeliveryTime = new TableSchema.TableColumn(schema);
                colvarDeliveryTime.ColumnName = "delivery_time";
                colvarDeliveryTime.DataType = DbType.DateTime;
                colvarDeliveryTime.MaxLength = 0;
                colvarDeliveryTime.AutoIncrement = false;
                colvarDeliveryTime.IsNullable = true;
                colvarDeliveryTime.IsPrimaryKey = false;
                colvarDeliveryTime.IsForeignKey = false;
                colvarDeliveryTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarDeliveryTime);
                
                TableSchema.TableColumn colvarMemberEmail = new TableSchema.TableColumn(schema);
                colvarMemberEmail.ColumnName = "member_email";
                colvarMemberEmail.DataType = DbType.String;
                colvarMemberEmail.MaxLength = 256;
                colvarMemberEmail.AutoIncrement = false;
                colvarMemberEmail.IsNullable = true;
                colvarMemberEmail.IsPrimaryKey = false;
                colvarMemberEmail.IsForeignKey = false;
                colvarMemberEmail.IsReadOnly = false;
                
                schema.Columns.Add(colvarMemberEmail);
                
                TableSchema.TableColumn colvarGroupOrderGuid = new TableSchema.TableColumn(schema);
                colvarGroupOrderGuid.ColumnName = "group_order_guid";
                colvarGroupOrderGuid.DataType = DbType.Guid;
                colvarGroupOrderGuid.MaxLength = 0;
                colvarGroupOrderGuid.AutoIncrement = false;
                colvarGroupOrderGuid.IsNullable = false;
                colvarGroupOrderGuid.IsPrimaryKey = false;
                colvarGroupOrderGuid.IsForeignKey = false;
                colvarGroupOrderGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarGroupOrderGuid);
                
                TableSchema.TableColumn colvarMemberName = new TableSchema.TableColumn(schema);
                colvarMemberName.ColumnName = "member_name";
                colvarMemberName.DataType = DbType.String;
                colvarMemberName.MaxLength = 50;
                colvarMemberName.AutoIncrement = false;
                colvarMemberName.IsNullable = false;
                colvarMemberName.IsPrimaryKey = false;
                colvarMemberName.IsForeignKey = false;
                colvarMemberName.IsReadOnly = false;
                
                schema.Columns.Add(colvarMemberName);
                
                TableSchema.TableColumn colvarAvailability = new TableSchema.TableColumn(schema);
                colvarAvailability.ColumnName = "availability";
                colvarAvailability.DataType = DbType.String;
                colvarAvailability.MaxLength = 1073741823;
                colvarAvailability.AutoIncrement = false;
                colvarAvailability.IsNullable = true;
                colvarAvailability.IsPrimaryKey = false;
                colvarAvailability.IsForeignKey = false;
                colvarAvailability.IsReadOnly = false;
                
                schema.Columns.Add(colvarAvailability);
                
                TableSchema.TableColumn colvarOrderStatus = new TableSchema.TableColumn(schema);
                colvarOrderStatus.ColumnName = "order_status";
                colvarOrderStatus.DataType = DbType.Int32;
                colvarOrderStatus.MaxLength = 0;
                colvarOrderStatus.AutoIncrement = false;
                colvarOrderStatus.IsNullable = false;
                colvarOrderStatus.IsPrimaryKey = false;
                colvarOrderStatus.IsForeignKey = false;
                colvarOrderStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderStatus);
                
                TableSchema.TableColumn colvarOrderId = new TableSchema.TableColumn(schema);
                colvarOrderId.ColumnName = "order_id";
                colvarOrderId.DataType = DbType.AnsiString;
                colvarOrderId.MaxLength = 30;
                colvarOrderId.AutoIncrement = false;
                colvarOrderId.IsNullable = true;
                colvarOrderId.IsPrimaryKey = false;
                colvarOrderId.IsForeignKey = false;
                colvarOrderId.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderId);
                
                TableSchema.TableColumn colvarStoreGuid = new TableSchema.TableColumn(schema);
                colvarStoreGuid.ColumnName = "store_guid";
                colvarStoreGuid.DataType = DbType.Guid;
                colvarStoreGuid.MaxLength = 0;
                colvarStoreGuid.AutoIncrement = false;
                colvarStoreGuid.IsNullable = true;
                colvarStoreGuid.IsPrimaryKey = false;
                colvarStoreGuid.IsForeignKey = false;
                colvarStoreGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarStoreGuid);
                
                TableSchema.TableColumn colvarSellerIsCloseDown = new TableSchema.TableColumn(schema);
                colvarSellerIsCloseDown.ColumnName = "seller_is_close_down";
                colvarSellerIsCloseDown.DataType = DbType.Boolean;
                colvarSellerIsCloseDown.MaxLength = 0;
                colvarSellerIsCloseDown.AutoIncrement = false;
                colvarSellerIsCloseDown.IsNullable = false;
                colvarSellerIsCloseDown.IsPrimaryKey = false;
                colvarSellerIsCloseDown.IsForeignKey = false;
                colvarSellerIsCloseDown.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerIsCloseDown);
                
                TableSchema.TableColumn colvarSellerCloseDownDate = new TableSchema.TableColumn(schema);
                colvarSellerCloseDownDate.ColumnName = "seller_close_down_date";
                colvarSellerCloseDownDate.DataType = DbType.DateTime;
                colvarSellerCloseDownDate.MaxLength = 0;
                colvarSellerCloseDownDate.AutoIncrement = false;
                colvarSellerCloseDownDate.IsNullable = true;
                colvarSellerCloseDownDate.IsPrimaryKey = false;
                colvarSellerCloseDownDate.IsForeignKey = false;
                colvarSellerCloseDownDate.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerCloseDownDate);
                
                TableSchema.TableColumn colvarStoreName = new TableSchema.TableColumn(schema);
                colvarStoreName.ColumnName = "store_name";
                colvarStoreName.DataType = DbType.String;
                colvarStoreName.MaxLength = 100;
                colvarStoreName.AutoIncrement = false;
                colvarStoreName.IsNullable = true;
                colvarStoreName.IsPrimaryKey = false;
                colvarStoreName.IsForeignKey = false;
                colvarStoreName.IsReadOnly = false;
                
                schema.Columns.Add(colvarStoreName);
                
                TableSchema.TableColumn colvarStoreIsCloseDown = new TableSchema.TableColumn(schema);
                colvarStoreIsCloseDown.ColumnName = "store_is_close_down";
                colvarStoreIsCloseDown.DataType = DbType.Boolean;
                colvarStoreIsCloseDown.MaxLength = 0;
                colvarStoreIsCloseDown.AutoIncrement = false;
                colvarStoreIsCloseDown.IsNullable = true;
                colvarStoreIsCloseDown.IsPrimaryKey = false;
                colvarStoreIsCloseDown.IsForeignKey = false;
                colvarStoreIsCloseDown.IsReadOnly = false;
                
                schema.Columns.Add(colvarStoreIsCloseDown);
                
                TableSchema.TableColumn colvarStoreCloseDownDate = new TableSchema.TableColumn(schema);
                colvarStoreCloseDownDate.ColumnName = "store_close_down_date";
                colvarStoreCloseDownDate.DataType = DbType.DateTime;
                colvarStoreCloseDownDate.MaxLength = 0;
                colvarStoreCloseDownDate.AutoIncrement = false;
                colvarStoreCloseDownDate.IsNullable = true;
                colvarStoreCloseDownDate.IsPrimaryKey = false;
                colvarStoreCloseDownDate.IsForeignKey = false;
                colvarStoreCloseDownDate.IsReadOnly = false;
                
                schema.Columns.Add(colvarStoreCloseDownDate);
                
                TableSchema.TableColumn colvarBhChangedExpireDate = new TableSchema.TableColumn(schema);
                colvarBhChangedExpireDate.ColumnName = "bh_changed_expire_date";
                colvarBhChangedExpireDate.DataType = DbType.DateTime;
                colvarBhChangedExpireDate.MaxLength = 0;
                colvarBhChangedExpireDate.AutoIncrement = false;
                colvarBhChangedExpireDate.IsNullable = true;
                colvarBhChangedExpireDate.IsPrimaryKey = false;
                colvarBhChangedExpireDate.IsForeignKey = false;
                colvarBhChangedExpireDate.IsReadOnly = false;
                
                schema.Columns.Add(colvarBhChangedExpireDate);
                
                TableSchema.TableColumn colvarStoreChangedExpireDate = new TableSchema.TableColumn(schema);
                colvarStoreChangedExpireDate.ColumnName = "store_changed_expire_date";
                colvarStoreChangedExpireDate.DataType = DbType.DateTime;
                colvarStoreChangedExpireDate.MaxLength = 0;
                colvarStoreChangedExpireDate.AutoIncrement = false;
                colvarStoreChangedExpireDate.IsNullable = true;
                colvarStoreChangedExpireDate.IsPrimaryKey = false;
                colvarStoreChangedExpireDate.IsForeignKey = false;
                colvarStoreChangedExpireDate.IsReadOnly = false;
                
                schema.Columns.Add(colvarStoreChangedExpireDate);
                
                TableSchema.TableColumn colvarMobile = new TableSchema.TableColumn(schema);
                colvarMobile.ColumnName = "mobile";
                colvarMobile.DataType = DbType.AnsiString;
                colvarMobile.MaxLength = 50;
                colvarMobile.AutoIncrement = false;
                colvarMobile.IsNullable = true;
                colvarMobile.IsPrimaryKey = false;
                colvarMobile.IsForeignKey = false;
                colvarMobile.IsReadOnly = false;
                
                schema.Columns.Add(colvarMobile);
                
                TableSchema.TableColumn colvarInvoiceNumber = new TableSchema.TableColumn(schema);
                colvarInvoiceNumber.ColumnName = "invoice_number";
                colvarInvoiceNumber.DataType = DbType.String;
                colvarInvoiceNumber.MaxLength = 10;
                colvarInvoiceNumber.AutoIncrement = false;
                colvarInvoiceNumber.IsNullable = true;
                colvarInvoiceNumber.IsPrimaryKey = false;
                colvarInvoiceNumber.IsForeignKey = false;
                colvarInvoiceNumber.IsReadOnly = false;
                
                schema.Columns.Add(colvarInvoiceNumber);
                
                TableSchema.TableColumn colvarInvoiceMessage = new TableSchema.TableColumn(schema);
                colvarInvoiceMessage.ColumnName = "invoice_message";
                colvarInvoiceMessage.DataType = DbType.String;
                colvarInvoiceMessage.MaxLength = 250;
                colvarInvoiceMessage.AutoIncrement = false;
                colvarInvoiceMessage.IsNullable = true;
                colvarInvoiceMessage.IsPrimaryKey = false;
                colvarInvoiceMessage.IsForeignKey = false;
                colvarInvoiceMessage.IsReadOnly = false;
                
                schema.Columns.Add(colvarInvoiceMessage);
                
                TableSchema.TableColumn colvarStoreSellerId = new TableSchema.TableColumn(schema);
                colvarStoreSellerId.ColumnName = "store_seller_id";
                colvarStoreSellerId.DataType = DbType.AnsiString;
                colvarStoreSellerId.MaxLength = 20;
                colvarStoreSellerId.AutoIncrement = false;
                colvarStoreSellerId.IsNullable = true;
                colvarStoreSellerId.IsPrimaryKey = false;
                colvarStoreSellerId.IsForeignKey = false;
                colvarStoreSellerId.IsReadOnly = false;
                
                schema.Columns.Add(colvarStoreSellerId);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_ppon_coupon",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewPponCoupon()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewPponCoupon(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewPponCoupon(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewPponCoupon(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("CouponId")]
        [Bindable(true)]
        public int? CouponId 
	    {
		    get
		    {
			    return GetColumnValue<int?>("coupon_id");
		    }
            set 
		    {
			    SetColumnValue("coupon_id", value);
            }
        }
	      
        [XmlAttribute("OrderDetailId")]
        [Bindable(true)]
        public Guid? OrderDetailId 
	    {
		    get
		    {
			    return GetColumnValue<Guid?>("order_detail_id");
		    }
            set 
		    {
			    SetColumnValue("order_detail_id", value);
            }
        }
	      
        [XmlAttribute("SequenceNumber")]
        [Bindable(true)]
        public string SequenceNumber 
	    {
		    get
		    {
			    return GetColumnValue<string>("sequence_number");
		    }
            set 
		    {
			    SetColumnValue("sequence_number", value);
            }
        }
	      
        [XmlAttribute("CouponCode")]
        [Bindable(true)]
        public string CouponCode 
	    {
		    get
		    {
			    return GetColumnValue<string>("coupon_code");
		    }
            set 
		    {
			    SetColumnValue("coupon_code", value);
            }
        }
	      
        [XmlAttribute("CouponDescription")]
        [Bindable(true)]
        public string CouponDescription 
	    {
		    get
		    {
			    return GetColumnValue<string>("coupon_description");
		    }
            set 
		    {
			    SetColumnValue("coupon_description", value);
            }
        }
	      
        [XmlAttribute("CouponStatus")]
        [Bindable(true)]
        public int? CouponStatus 
	    {
		    get
		    {
			    return GetColumnValue<int?>("coupon_status");
		    }
            set 
		    {
			    SetColumnValue("coupon_status", value);
            }
        }
	      
        [XmlAttribute("CouponAvailable")]
        [Bindable(true)]
        public bool? CouponAvailable 
	    {
		    get
		    {
			    return GetColumnValue<bool?>("coupon_available");
		    }
            set 
		    {
			    SetColumnValue("coupon_available", value);
            }
        }
	      
        [XmlAttribute("CouponStoreSequence")]
        [Bindable(true)]
        public int? CouponStoreSequence 
	    {
		    get
		    {
			    return GetColumnValue<int?>("coupon_store_sequence");
		    }
            set 
		    {
			    SetColumnValue("coupon_store_sequence", value);
            }
        }
	      
        [XmlAttribute("IsReservationLock")]
        [Bindable(true)]
        public bool? IsReservationLock 
	    {
		    get
		    {
			    return GetColumnValue<bool?>("is_reservation_lock");
		    }
            set 
		    {
			    SetColumnValue("is_reservation_lock", value);
            }
        }
	      
        [XmlAttribute("OrderDetailGuid")]
        [Bindable(true)]
        public Guid OrderDetailGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("order_detail_guid");
		    }
            set 
		    {
			    SetColumnValue("order_detail_guid", value);
            }
        }
	      
        [XmlAttribute("OrderGuid")]
        [Bindable(true)]
        public Guid OrderGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("order_GUID");
		    }
            set 
		    {
			    SetColumnValue("order_GUID", value);
            }
        }
	      
        [XmlAttribute("ItemName")]
        [Bindable(true)]
        public string ItemName 
	    {
		    get
		    {
			    return GetColumnValue<string>("item_name");
		    }
            set 
		    {
			    SetColumnValue("item_name", value);
            }
        }
	      
        [XmlAttribute("ItemGuid")]
        [Bindable(true)]
        public Guid? ItemGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid?>("item_GUID");
		    }
            set 
		    {
			    SetColumnValue("item_GUID", value);
            }
        }
	      
        [XmlAttribute("ItemUnitPrice")]
        [Bindable(true)]
        public decimal ItemUnitPrice 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("item_unit_price");
		    }
            set 
		    {
			    SetColumnValue("item_unit_price", value);
            }
        }
	      
        [XmlAttribute("ItemQuantity")]
        [Bindable(true)]
        public int ItemQuantity 
	    {
		    get
		    {
			    return GetColumnValue<int>("item_quantity");
		    }
            set 
		    {
			    SetColumnValue("item_quantity", value);
            }
        }
	      
        [XmlAttribute("OrderDetailTotal")]
        [Bindable(true)]
        public decimal? OrderDetailTotal 
	    {
		    get
		    {
			    return GetColumnValue<decimal?>("order_detail_total");
		    }
            set 
		    {
			    SetColumnValue("order_detail_total", value);
            }
        }
	      
        [XmlAttribute("ConsumerName")]
        [Bindable(true)]
        public string ConsumerName 
	    {
		    get
		    {
			    return GetColumnValue<string>("consumer_name");
		    }
            set 
		    {
			    SetColumnValue("consumer_name", value);
            }
        }
	      
        [XmlAttribute("ConsumerTeleExt")]
        [Bindable(true)]
        public string ConsumerTeleExt 
	    {
		    get
		    {
			    return GetColumnValue<string>("consumer_tele_ext");
		    }
            set 
		    {
			    SetColumnValue("consumer_tele_ext", value);
            }
        }
	      
        [XmlAttribute("ConsumerGroup")]
        [Bindable(true)]
        public string ConsumerGroup 
	    {
		    get
		    {
			    return GetColumnValue<string>("consumer_group");
		    }
            set 
		    {
			    SetColumnValue("consumer_group", value);
            }
        }
	      
        [XmlAttribute("OrderDetailStatus")]
        [Bindable(true)]
        public int OrderDetailStatus 
	    {
		    get
		    {
			    return GetColumnValue<int>("order_detail_status");
		    }
            set 
		    {
			    SetColumnValue("order_detail_status", value);
            }
        }
	      
        [XmlAttribute("OrderDetailCreateId")]
        [Bindable(true)]
        public string OrderDetailCreateId 
	    {
		    get
		    {
			    return GetColumnValue<string>("order_detail_create_id");
		    }
            set 
		    {
			    SetColumnValue("order_detail_create_id", value);
            }
        }
	      
        [XmlAttribute("OrderDetailCreateTime")]
        [Bindable(true)]
        public DateTime OrderDetailCreateTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("order_detail_create_time");
		    }
            set 
		    {
			    SetColumnValue("order_detail_create_time", value);
            }
        }
	      
        [XmlAttribute("OrderDetailModifyId")]
        [Bindable(true)]
        public string OrderDetailModifyId 
	    {
		    get
		    {
			    return GetColumnValue<string>("order_detail_modify_id");
		    }
            set 
		    {
			    SetColumnValue("order_detail_modify_id", value);
            }
        }
	      
        [XmlAttribute("OrderDetailModifyTime")]
        [Bindable(true)]
        public DateTime? OrderDetailModifyTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("order_detail_modify_time");
		    }
            set 
		    {
			    SetColumnValue("order_detail_modify_time", value);
            }
        }
	      
        [XmlAttribute("ItemId")]
        [Bindable(true)]
        public string ItemId 
	    {
		    get
		    {
			    return GetColumnValue<string>("item_id");
		    }
            set 
		    {
			    SetColumnValue("item_id", value);
            }
        }
	      
        [XmlAttribute("CityId")]
        [Bindable(true)]
        public int? CityId 
	    {
		    get
		    {
			    return GetColumnValue<int?>("city_id");
		    }
            set 
		    {
			    SetColumnValue("city_id", value);
            }
        }
	      
        [XmlAttribute("ParentOrderId")]
        [Bindable(true)]
        public Guid? ParentOrderId 
	    {
		    get
		    {
			    return GetColumnValue<Guid?>("parent_order_id");
		    }
            set 
		    {
			    SetColumnValue("parent_order_id", value);
            }
        }
	      
        [XmlAttribute("GroupOrderStatus")]
        [Bindable(true)]
        public int? GroupOrderStatus 
	    {
		    get
		    {
			    return GetColumnValue<int?>("group_order_status");
		    }
            set 
		    {
			    SetColumnValue("group_order_status", value);
            }
        }
	      
        [XmlAttribute("BusinessHourId")]
        [Bindable(true)]
        public string BusinessHourId 
	    {
		    get
		    {
			    return GetColumnValue<string>("business_hour_id");
		    }
            set 
		    {
			    SetColumnValue("business_hour_id", value);
            }
        }
	      
        [XmlAttribute("BusinessHourTypeId")]
        [Bindable(true)]
        public int BusinessHourTypeId 
	    {
		    get
		    {
			    return GetColumnValue<int>("business_hour_type_id");
		    }
            set 
		    {
			    SetColumnValue("business_hour_type_id", value);
            }
        }
	      
        [XmlAttribute("BusinessHourOrderTimeS")]
        [Bindable(true)]
        public DateTime BusinessHourOrderTimeS 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("business_hour_order_time_s");
		    }
            set 
		    {
			    SetColumnValue("business_hour_order_time_s", value);
            }
        }
	      
        [XmlAttribute("BusinessHourOrderTimeE")]
        [Bindable(true)]
        public DateTime BusinessHourOrderTimeE 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("business_hour_order_time_e");
		    }
            set 
		    {
			    SetColumnValue("business_hour_order_time_e", value);
            }
        }
	      
        [XmlAttribute("BusinessHourDeliverTimeS")]
        [Bindable(true)]
        public DateTime? BusinessHourDeliverTimeS 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("business_hour_deliver_time_s");
		    }
            set 
		    {
			    SetColumnValue("business_hour_deliver_time_s", value);
            }
        }
	      
        [XmlAttribute("BusinessHourDeliverTimeE")]
        [Bindable(true)]
        public DateTime? BusinessHourDeliverTimeE 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("business_hour_deliver_time_e");
		    }
            set 
		    {
			    SetColumnValue("business_hour_deliver_time_e", value);
            }
        }
	      
        [XmlAttribute("BusinessHourDeliveryCharge")]
        [Bindable(true)]
        public decimal BusinessHourDeliveryCharge 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("business_hour_delivery_charge");
		    }
            set 
		    {
			    SetColumnValue("business_hour_delivery_charge", value);
            }
        }
	      
        [XmlAttribute("BusinessHourOrderMinimum")]
        [Bindable(true)]
        public decimal BusinessHourOrderMinimum 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("business_hour_order_minimum");
		    }
            set 
		    {
			    SetColumnValue("business_hour_order_minimum", value);
            }
        }
	      
        [XmlAttribute("BusinessHourPreparationTime")]
        [Bindable(true)]
        public int BusinessHourPreparationTime 
	    {
		    get
		    {
			    return GetColumnValue<int>("business_hour_preparation_time");
		    }
            set 
		    {
			    SetColumnValue("business_hour_preparation_time", value);
            }
        }
	      
        [XmlAttribute("BusinessHourOnline")]
        [Bindable(true)]
        public bool BusinessHourOnline 
	    {
		    get
		    {
			    return GetColumnValue<bool>("business_hour_online");
		    }
            set 
		    {
			    SetColumnValue("business_hour_online", value);
            }
        }
	      
        [XmlAttribute("BusinessHourStatus")]
        [Bindable(true)]
        public int BusinessHourStatus 
	    {
		    get
		    {
			    return GetColumnValue<int>("business_hour_status");
		    }
            set 
		    {
			    SetColumnValue("business_hour_status", value);
            }
        }
	      
        [XmlAttribute("OrderTotalLimit")]
        [Bindable(true)]
        public decimal? OrderTotalLimit 
	    {
		    get
		    {
			    return GetColumnValue<decimal?>("order_total_limit");
		    }
            set 
		    {
			    SetColumnValue("order_total_limit", value);
            }
        }
	      
        [XmlAttribute("DeliveryLimit")]
        [Bindable(true)]
        public int? DeliveryLimit 
	    {
		    get
		    {
			    return GetColumnValue<int?>("delivery_limit");
		    }
            set 
		    {
			    SetColumnValue("delivery_limit", value);
            }
        }
	      
        [XmlAttribute("Holiday")]
        [Bindable(true)]
        public int Holiday 
	    {
		    get
		    {
			    return GetColumnValue<int>("holiday");
		    }
            set 
		    {
			    SetColumnValue("holiday", value);
            }
        }
	      
        [XmlAttribute("CouponEventId")]
        [Bindable(true)]
        public int CouponEventId 
	    {
		    get
		    {
			    return GetColumnValue<int>("coupon_event_id");
		    }
            set 
		    {
			    SetColumnValue("coupon_event_id", value);
            }
        }
	      
        [XmlAttribute("BusinessHourGuid")]
        [Bindable(true)]
        public Guid BusinessHourGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("business_hour_guid");
		    }
            set 
		    {
			    SetColumnValue("business_hour_guid", value);
            }
        }
	      
        [XmlAttribute("EventName")]
        [Bindable(true)]
        public string EventName 
	    {
		    get
		    {
			    return GetColumnValue<string>("event_name");
		    }
            set 
		    {
			    SetColumnValue("event_name", value);
            }
        }
	      
        [XmlAttribute("EventImagePath")]
        [Bindable(true)]
        public string EventImagePath 
	    {
		    get
		    {
			    return GetColumnValue<string>("event_image_path");
		    }
            set 
		    {
			    SetColumnValue("event_image_path", value);
            }
        }
	      
        [XmlAttribute("Introduction")]
        [Bindable(true)]
        public string Introduction 
	    {
		    get
		    {
			    return GetColumnValue<string>("introduction");
		    }
            set 
		    {
			    SetColumnValue("introduction", value);
            }
        }
	      
        [XmlAttribute("Restrictions")]
        [Bindable(true)]
        public string Restrictions 
	    {
		    get
		    {
			    return GetColumnValue<string>("restrictions");
		    }
            set 
		    {
			    SetColumnValue("restrictions", value);
            }
        }
	      
        [XmlAttribute("CouponEventDescription")]
        [Bindable(true)]
        public string CouponEventDescription 
	    {
		    get
		    {
			    return GetColumnValue<string>("coupon_event_description");
		    }
            set 
		    {
			    SetColumnValue("coupon_event_description", value);
            }
        }
	      
        [XmlAttribute("ReferenceText")]
        [Bindable(true)]
        public string ReferenceText 
	    {
		    get
		    {
			    return GetColumnValue<string>("reference_text");
		    }
            set 
		    {
			    SetColumnValue("reference_text", value);
            }
        }
	      
        [XmlAttribute("CouponUsage")]
        [Bindable(true)]
        public string CouponUsage 
	    {
		    get
		    {
			    return GetColumnValue<string>("coupon_usage");
		    }
            set 
		    {
			    SetColumnValue("coupon_usage", value);
            }
        }
	      
        [XmlAttribute("Remark")]
        [Bindable(true)]
        public string Remark 
	    {
		    get
		    {
			    return GetColumnValue<string>("remark");
		    }
            set 
		    {
			    SetColumnValue("remark", value);
            }
        }
	      
        [XmlAttribute("SellerGuid")]
        [Bindable(true)]
        public Guid SellerGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("seller_guid");
		    }
            set 
		    {
			    SetColumnValue("seller_guid", value);
            }
        }
	      
        [XmlAttribute("SellerId")]
        [Bindable(true)]
        public string SellerId 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_id");
		    }
            set 
		    {
			    SetColumnValue("seller_id", value);
            }
        }
	      
        [XmlAttribute("SellerName")]
        [Bindable(true)]
        public string SellerName 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_name");
		    }
            set 
		    {
			    SetColumnValue("seller_name", value);
            }
        }
	      
        [XmlAttribute("SellerBossName")]
        [Bindable(true)]
        public string SellerBossName 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_boss_name");
		    }
            set 
		    {
			    SetColumnValue("seller_boss_name", value);
            }
        }
	      
        [XmlAttribute("SellerTel")]
        [Bindable(true)]
        public string SellerTel 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_tel");
		    }
            set 
		    {
			    SetColumnValue("seller_tel", value);
            }
        }
	      
        [XmlAttribute("SellerTel2")]
        [Bindable(true)]
        public string SellerTel2 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_tel2");
		    }
            set 
		    {
			    SetColumnValue("seller_tel2", value);
            }
        }
	      
        [XmlAttribute("SellerFax")]
        [Bindable(true)]
        public string SellerFax 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_fax");
		    }
            set 
		    {
			    SetColumnValue("seller_fax", value);
            }
        }
	      
        [XmlAttribute("SellerMobile")]
        [Bindable(true)]
        public string SellerMobile 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_mobile");
		    }
            set 
		    {
			    SetColumnValue("seller_mobile", value);
            }
        }
	      
        [XmlAttribute("SellerAddress")]
        [Bindable(true)]
        public string SellerAddress 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_address");
		    }
            set 
		    {
			    SetColumnValue("seller_address", value);
            }
        }
	      
        [XmlAttribute("SellerEmail")]
        [Bindable(true)]
        public string SellerEmail 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_email");
		    }
            set 
		    {
			    SetColumnValue("seller_email", value);
            }
        }
	      
        [XmlAttribute("SellerBlog")]
        [Bindable(true)]
        public string SellerBlog 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_blog");
		    }
            set 
		    {
			    SetColumnValue("seller_blog", value);
            }
        }
	      
        [XmlAttribute("SellerInvoice")]
        [Bindable(true)]
        public string SellerInvoice 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_invoice");
		    }
            set 
		    {
			    SetColumnValue("seller_invoice", value);
            }
        }
	      
        [XmlAttribute("SellerDescription")]
        [Bindable(true)]
        public string SellerDescription 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_description");
		    }
            set 
		    {
			    SetColumnValue("seller_description", value);
            }
        }
	      
        [XmlAttribute("SellerStatus")]
        [Bindable(true)]
        public int SellerStatus 
	    {
		    get
		    {
			    return GetColumnValue<int>("seller_status");
		    }
            set 
		    {
			    SetColumnValue("seller_status", value);
            }
        }
	      
        [XmlAttribute("Department")]
        [Bindable(true)]
        public int Department 
	    {
		    get
		    {
			    return GetColumnValue<int>("department");
		    }
            set 
		    {
			    SetColumnValue("department", value);
            }
        }
	      
        [XmlAttribute("SellerRemark")]
        [Bindable(true)]
        public string SellerRemark 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_remark");
		    }
            set 
		    {
			    SetColumnValue("seller_remark", value);
            }
        }
	      
        [XmlAttribute("SellerSales")]
        [Bindable(true)]
        public string SellerSales 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_sales");
		    }
            set 
		    {
			    SetColumnValue("seller_sales", value);
            }
        }
	      
        [XmlAttribute("SellerLogoimgPath")]
        [Bindable(true)]
        public string SellerLogoimgPath 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_logoimg_path");
		    }
            set 
		    {
			    SetColumnValue("seller_logoimg_path", value);
            }
        }
	      
        [XmlAttribute("SellerVideoPath")]
        [Bindable(true)]
        public string SellerVideoPath 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_video_path");
		    }
            set 
		    {
			    SetColumnValue("seller_video_path", value);
            }
        }
	      
        [XmlAttribute("SellerCreateId")]
        [Bindable(true)]
        public string SellerCreateId 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_create_id");
		    }
            set 
		    {
			    SetColumnValue("seller_create_id", value);
            }
        }
	      
        [XmlAttribute("SellerCreateTime")]
        [Bindable(true)]
        public DateTime SellerCreateTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("seller_create_time");
		    }
            set 
		    {
			    SetColumnValue("seller_create_time", value);
            }
        }
	      
        [XmlAttribute("SellerModifyId")]
        [Bindable(true)]
        public string SellerModifyId 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_modify_id");
		    }
            set 
		    {
			    SetColumnValue("seller_modify_id", value);
            }
        }
	      
        [XmlAttribute("SellerModifyTime")]
        [Bindable(true)]
        public DateTime? SellerModifyTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("seller_modify_time");
		    }
            set 
		    {
			    SetColumnValue("seller_modify_time", value);
            }
        }
	      
        [XmlAttribute("DefaultCommissionRate")]
        [Bindable(true)]
        public double? DefaultCommissionRate 
	    {
		    get
		    {
			    return GetColumnValue<double?>("default_commission_rate");
		    }
            set 
		    {
			    SetColumnValue("default_commission_rate", value);
            }
        }
	      
        [XmlAttribute("BillingCode")]
        [Bindable(true)]
        public string BillingCode 
	    {
		    get
		    {
			    return GetColumnValue<string>("billing_code");
		    }
            set 
		    {
			    SetColumnValue("billing_code", value);
            }
        }
	      
        [XmlAttribute("Weight")]
        [Bindable(true)]
        public int? Weight 
	    {
		    get
		    {
			    return GetColumnValue<int?>("weight");
		    }
            set 
		    {
			    SetColumnValue("weight", value);
            }
        }
	      
        [XmlAttribute("PostCkoutAction")]
        [Bindable(true)]
        public int PostCkoutAction 
	    {
		    get
		    {
			    return GetColumnValue<int>("post_ckout_action");
		    }
            set 
		    {
			    SetColumnValue("post_ckout_action", value);
            }
        }
	      
        [XmlAttribute("PostCkoutArgs")]
        [Bindable(true)]
        public string PostCkoutArgs 
	    {
		    get
		    {
			    return GetColumnValue<string>("post_ckout_args");
		    }
            set 
		    {
			    SetColumnValue("post_ckout_args", value);
            }
        }
	      
        [XmlAttribute("Coordinate")]
        [Bindable(true)]
        public string Coordinate 
	    {
		    get
		    {
			    return GetColumnValue<string>("coordinate");
		    }
            set 
		    {
			    SetColumnValue("coordinate", value);
            }
        }
	      
        [XmlAttribute("DeliveryMinuteGap")]
        [Bindable(true)]
        public int? DeliveryMinuteGap 
	    {
		    get
		    {
			    return GetColumnValue<int?>("delivery_minute_gap");
		    }
            set 
		    {
			    SetColumnValue("delivery_minute_gap", value);
            }
        }
	      
        [XmlAttribute("SellerCityId")]
        [Bindable(true)]
        public int SellerCityId 
	    {
		    get
		    {
			    return GetColumnValue<int>("seller_city_id");
		    }
            set 
		    {
			    SetColumnValue("seller_city_id", value);
            }
        }
	      
        [XmlAttribute("DeliveryAddress")]
        [Bindable(true)]
        public string DeliveryAddress 
	    {
		    get
		    {
			    return GetColumnValue<string>("delivery_address");
		    }
            set 
		    {
			    SetColumnValue("delivery_address", value);
            }
        }
	      
        [XmlAttribute("DeliveryTime")]
        [Bindable(true)]
        public DateTime? DeliveryTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("delivery_time");
		    }
            set 
		    {
			    SetColumnValue("delivery_time", value);
            }
        }
	      
        [XmlAttribute("MemberEmail")]
        [Bindable(true)]
        public string MemberEmail 
	    {
		    get
		    {
			    return GetColumnValue<string>("member_email");
		    }
            set 
		    {
			    SetColumnValue("member_email", value);
            }
        }
	      
        [XmlAttribute("GroupOrderGuid")]
        [Bindable(true)]
        public Guid GroupOrderGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("group_order_guid");
		    }
            set 
		    {
			    SetColumnValue("group_order_guid", value);
            }
        }
	      
        [XmlAttribute("MemberName")]
        [Bindable(true)]
        public string MemberName 
	    {
		    get
		    {
			    return GetColumnValue<string>("member_name");
		    }
            set 
		    {
			    SetColumnValue("member_name", value);
            }
        }
	      
        [XmlAttribute("Availability")]
        [Bindable(true)]
        public string Availability 
	    {
		    get
		    {
			    return GetColumnValue<string>("availability");
		    }
            set 
		    {
			    SetColumnValue("availability", value);
            }
        }
	      
        [XmlAttribute("OrderStatus")]
        [Bindable(true)]
        public int OrderStatus 
	    {
		    get
		    {
			    return GetColumnValue<int>("order_status");
		    }
            set 
		    {
			    SetColumnValue("order_status", value);
            }
        }
	      
        [XmlAttribute("OrderId")]
        [Bindable(true)]
        public string OrderId 
	    {
		    get
		    {
			    return GetColumnValue<string>("order_id");
		    }
            set 
		    {
			    SetColumnValue("order_id", value);
            }
        }
	      
        [XmlAttribute("StoreGuid")]
        [Bindable(true)]
        public Guid? StoreGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid?>("store_guid");
		    }
            set 
		    {
			    SetColumnValue("store_guid", value);
            }
        }
	      
        [XmlAttribute("SellerIsCloseDown")]
        [Bindable(true)]
        public bool SellerIsCloseDown 
	    {
		    get
		    {
			    return GetColumnValue<bool>("seller_is_close_down");
		    }
            set 
		    {
			    SetColumnValue("seller_is_close_down", value);
            }
        }
	      
        [XmlAttribute("SellerCloseDownDate")]
        [Bindable(true)]
        public DateTime? SellerCloseDownDate 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("seller_close_down_date");
		    }
            set 
		    {
			    SetColumnValue("seller_close_down_date", value);
            }
        }
	      
        [XmlAttribute("StoreName")]
        [Bindable(true)]
        public string StoreName 
	    {
		    get
		    {
			    return GetColumnValue<string>("store_name");
		    }
            set 
		    {
			    SetColumnValue("store_name", value);
            }
        }
	      
        [XmlAttribute("StoreIsCloseDown")]
        [Bindable(true)]
        public bool? StoreIsCloseDown 
	    {
		    get
		    {
			    return GetColumnValue<bool?>("store_is_close_down");
		    }
            set 
		    {
			    SetColumnValue("store_is_close_down", value);
            }
        }
	      
        [XmlAttribute("StoreCloseDownDate")]
        [Bindable(true)]
        public DateTime? StoreCloseDownDate 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("store_close_down_date");
		    }
            set 
		    {
			    SetColumnValue("store_close_down_date", value);
            }
        }
	      
        [XmlAttribute("BhChangedExpireDate")]
        [Bindable(true)]
        public DateTime? BhChangedExpireDate 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("bh_changed_expire_date");
		    }
            set 
		    {
			    SetColumnValue("bh_changed_expire_date", value);
            }
        }
	      
        [XmlAttribute("StoreChangedExpireDate")]
        [Bindable(true)]
        public DateTime? StoreChangedExpireDate 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("store_changed_expire_date");
		    }
            set 
		    {
			    SetColumnValue("store_changed_expire_date", value);
            }
        }
	      
        [XmlAttribute("Mobile")]
        [Bindable(true)]
        public string Mobile 
	    {
		    get
		    {
			    return GetColumnValue<string>("mobile");
		    }
            set 
		    {
			    SetColumnValue("mobile", value);
            }
        }
	      
        [XmlAttribute("InvoiceNumber")]
        [Bindable(true)]
        public string InvoiceNumber 
	    {
		    get
		    {
			    return GetColumnValue<string>("invoice_number");
		    }
            set 
		    {
			    SetColumnValue("invoice_number", value);
            }
        }
	      
        [XmlAttribute("InvoiceMessage")]
        [Bindable(true)]
        public string InvoiceMessage 
	    {
		    get
		    {
			    return GetColumnValue<string>("invoice_message");
		    }
            set 
		    {
			    SetColumnValue("invoice_message", value);
            }
        }
	      
        [XmlAttribute("StoreSellerId")]
        [Bindable(true)]
        public string StoreSellerId 
	    {
		    get
		    {
			    return GetColumnValue<string>("store_seller_id");
		    }
            set 
		    {
			    SetColumnValue("store_seller_id", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string CouponId = @"coupon_id";
            
            public static string OrderDetailId = @"order_detail_id";
            
            public static string SequenceNumber = @"sequence_number";
            
            public static string CouponCode = @"coupon_code";
            
            public static string CouponDescription = @"coupon_description";
            
            public static string CouponStatus = @"coupon_status";
            
            public static string CouponAvailable = @"coupon_available";
            
            public static string CouponStoreSequence = @"coupon_store_sequence";
            
            public static string IsReservationLock = @"is_reservation_lock";
            
            public static string OrderDetailGuid = @"order_detail_guid";
            
            public static string OrderGuid = @"order_GUID";
            
            public static string ItemName = @"item_name";
            
            public static string ItemGuid = @"item_GUID";
            
            public static string ItemUnitPrice = @"item_unit_price";
            
            public static string ItemQuantity = @"item_quantity";
            
            public static string OrderDetailTotal = @"order_detail_total";
            
            public static string ConsumerName = @"consumer_name";
            
            public static string ConsumerTeleExt = @"consumer_tele_ext";
            
            public static string ConsumerGroup = @"consumer_group";
            
            public static string OrderDetailStatus = @"order_detail_status";
            
            public static string OrderDetailCreateId = @"order_detail_create_id";
            
            public static string OrderDetailCreateTime = @"order_detail_create_time";
            
            public static string OrderDetailModifyId = @"order_detail_modify_id";
            
            public static string OrderDetailModifyTime = @"order_detail_modify_time";
            
            public static string ItemId = @"item_id";
            
            public static string CityId = @"city_id";
            
            public static string ParentOrderId = @"parent_order_id";
            
            public static string GroupOrderStatus = @"group_order_status";
            
            public static string BusinessHourId = @"business_hour_id";
            
            public static string BusinessHourTypeId = @"business_hour_type_id";
            
            public static string BusinessHourOrderTimeS = @"business_hour_order_time_s";
            
            public static string BusinessHourOrderTimeE = @"business_hour_order_time_e";
            
            public static string BusinessHourDeliverTimeS = @"business_hour_deliver_time_s";
            
            public static string BusinessHourDeliverTimeE = @"business_hour_deliver_time_e";
            
            public static string BusinessHourDeliveryCharge = @"business_hour_delivery_charge";
            
            public static string BusinessHourOrderMinimum = @"business_hour_order_minimum";
            
            public static string BusinessHourPreparationTime = @"business_hour_preparation_time";
            
            public static string BusinessHourOnline = @"business_hour_online";
            
            public static string BusinessHourStatus = @"business_hour_status";
            
            public static string OrderTotalLimit = @"order_total_limit";
            
            public static string DeliveryLimit = @"delivery_limit";
            
            public static string Holiday = @"holiday";
            
            public static string CouponEventId = @"coupon_event_id";
            
            public static string BusinessHourGuid = @"business_hour_guid";
            
            public static string EventName = @"event_name";
            
            public static string EventImagePath = @"event_image_path";
            
            public static string Introduction = @"introduction";
            
            public static string Restrictions = @"restrictions";
            
            public static string CouponEventDescription = @"coupon_event_description";
            
            public static string ReferenceText = @"reference_text";
            
            public static string CouponUsage = @"coupon_usage";
            
            public static string Remark = @"remark";
            
            public static string SellerGuid = @"seller_guid";
            
            public static string SellerId = @"seller_id";
            
            public static string SellerName = @"seller_name";
            
            public static string SellerBossName = @"seller_boss_name";
            
            public static string SellerTel = @"seller_tel";
            
            public static string SellerTel2 = @"seller_tel2";
            
            public static string SellerFax = @"seller_fax";
            
            public static string SellerMobile = @"seller_mobile";
            
            public static string SellerAddress = @"seller_address";
            
            public static string SellerEmail = @"seller_email";
            
            public static string SellerBlog = @"seller_blog";
            
            public static string SellerInvoice = @"seller_invoice";
            
            public static string SellerDescription = @"seller_description";
            
            public static string SellerStatus = @"seller_status";
            
            public static string Department = @"department";
            
            public static string SellerRemark = @"seller_remark";
            
            public static string SellerSales = @"seller_sales";
            
            public static string SellerLogoimgPath = @"seller_logoimg_path";
            
            public static string SellerVideoPath = @"seller_video_path";
            
            public static string SellerCreateId = @"seller_create_id";
            
            public static string SellerCreateTime = @"seller_create_time";
            
            public static string SellerModifyId = @"seller_modify_id";
            
            public static string SellerModifyTime = @"seller_modify_time";
            
            public static string DefaultCommissionRate = @"default_commission_rate";
            
            public static string BillingCode = @"billing_code";
            
            public static string Weight = @"weight";
            
            public static string PostCkoutAction = @"post_ckout_action";
            
            public static string PostCkoutArgs = @"post_ckout_args";
            
            public static string Coordinate = @"coordinate";
            
            public static string DeliveryMinuteGap = @"delivery_minute_gap";
            
            public static string SellerCityId = @"seller_city_id";
            
            public static string DeliveryAddress = @"delivery_address";
            
            public static string DeliveryTime = @"delivery_time";
            
            public static string MemberEmail = @"member_email";
            
            public static string GroupOrderGuid = @"group_order_guid";
            
            public static string MemberName = @"member_name";
            
            public static string Availability = @"availability";
            
            public static string OrderStatus = @"order_status";
            
            public static string OrderId = @"order_id";
            
            public static string StoreGuid = @"store_guid";
            
            public static string SellerIsCloseDown = @"seller_is_close_down";
            
            public static string SellerCloseDownDate = @"seller_close_down_date";
            
            public static string StoreName = @"store_name";
            
            public static string StoreIsCloseDown = @"store_is_close_down";
            
            public static string StoreCloseDownDate = @"store_close_down_date";
            
            public static string BhChangedExpireDate = @"bh_changed_expire_date";
            
            public static string StoreChangedExpireDate = @"store_changed_expire_date";
            
            public static string Mobile = @"mobile";
            
            public static string InvoiceNumber = @"invoice_number";
            
            public static string InvoiceMessage = @"invoice_message";
            
            public static string StoreSellerId = @"store_seller_id";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

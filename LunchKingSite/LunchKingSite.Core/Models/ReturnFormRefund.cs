using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the ReturnFormRefund class.
	/// </summary>
    [Serializable]
	public partial class ReturnFormRefundCollection : RepositoryList<ReturnFormRefund, ReturnFormRefundCollection>
	{	   
		public ReturnFormRefundCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>ReturnFormRefundCollection</returns>
		public ReturnFormRefundCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                ReturnFormRefund o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the return_form_refund table.
	/// </summary>
	[Serializable]
	public partial class ReturnFormRefund : RepositoryRecord<ReturnFormRefund>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public ReturnFormRefund()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public ReturnFormRefund(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("return_form_refund", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarReturnFormId = new TableSchema.TableColumn(schema);
				colvarReturnFormId.ColumnName = "return_form_id";
				colvarReturnFormId.DataType = DbType.Int32;
				colvarReturnFormId.MaxLength = 0;
				colvarReturnFormId.AutoIncrement = false;
				colvarReturnFormId.IsNullable = false;
				colvarReturnFormId.IsPrimaryKey = true;
				colvarReturnFormId.IsForeignKey = true;
				colvarReturnFormId.IsReadOnly = false;
				colvarReturnFormId.DefaultSetting = @"";
				
					colvarReturnFormId.ForeignKeyTableName = "return_form";
				schema.Columns.Add(colvarReturnFormId);
				
				TableSchema.TableColumn colvarTrustId = new TableSchema.TableColumn(schema);
				colvarTrustId.ColumnName = "trust_id";
				colvarTrustId.DataType = DbType.Guid;
				colvarTrustId.MaxLength = 0;
				colvarTrustId.AutoIncrement = false;
				colvarTrustId.IsNullable = false;
				colvarTrustId.IsPrimaryKey = true;
				colvarTrustId.IsForeignKey = false;
				colvarTrustId.IsReadOnly = false;
				colvarTrustId.DefaultSetting = @"";
				colvarTrustId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTrustId);
				
				TableSchema.TableColumn colvarVendorNoRefund = new TableSchema.TableColumn(schema);
				colvarVendorNoRefund.ColumnName = "vendor_no_refund";
				colvarVendorNoRefund.DataType = DbType.Boolean;
				colvarVendorNoRefund.MaxLength = 0;
				colvarVendorNoRefund.AutoIncrement = false;
				colvarVendorNoRefund.IsNullable = false;
				colvarVendorNoRefund.IsPrimaryKey = false;
				colvarVendorNoRefund.IsForeignKey = false;
				colvarVendorNoRefund.IsReadOnly = false;
				colvarVendorNoRefund.DefaultSetting = @"";
				colvarVendorNoRefund.ForeignKeyTableName = "";
				schema.Columns.Add(colvarVendorNoRefund);
				
				TableSchema.TableColumn colvarIsRefunded = new TableSchema.TableColumn(schema);
				colvarIsRefunded.ColumnName = "is_refunded";
				colvarIsRefunded.DataType = DbType.Boolean;
				colvarIsRefunded.MaxLength = 0;
				colvarIsRefunded.AutoIncrement = false;
				colvarIsRefunded.IsNullable = true;
				colvarIsRefunded.IsPrimaryKey = false;
				colvarIsRefunded.IsForeignKey = false;
				colvarIsRefunded.IsReadOnly = false;
				colvarIsRefunded.DefaultSetting = @"";
				colvarIsRefunded.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsRefunded);
				
				TableSchema.TableColumn colvarFailReason = new TableSchema.TableColumn(schema);
				colvarFailReason.ColumnName = "fail_reason";
				colvarFailReason.DataType = DbType.String;
				colvarFailReason.MaxLength = 200;
				colvarFailReason.AutoIncrement = false;
				colvarFailReason.IsNullable = true;
				colvarFailReason.IsPrimaryKey = false;
				colvarFailReason.IsForeignKey = false;
				colvarFailReason.IsReadOnly = false;
				colvarFailReason.DefaultSetting = @"";
				colvarFailReason.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFailReason);
				
				TableSchema.TableColumn colvarIsFreight = new TableSchema.TableColumn(schema);
				colvarIsFreight.ColumnName = "is_freight";
				colvarIsFreight.DataType = DbType.Boolean;
				colvarIsFreight.MaxLength = 0;
				colvarIsFreight.AutoIncrement = false;
				colvarIsFreight.IsNullable = false;
				colvarIsFreight.IsPrimaryKey = false;
				colvarIsFreight.IsForeignKey = false;
				colvarIsFreight.IsReadOnly = false;
				colvarIsFreight.DefaultSetting = @"";
				colvarIsFreight.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsFreight);
				
				TableSchema.TableColumn colvarIsInRetry = new TableSchema.TableColumn(schema);
				colvarIsInRetry.ColumnName = "is_in_retry";
				colvarIsInRetry.DataType = DbType.Boolean;
				colvarIsInRetry.MaxLength = 0;
				colvarIsInRetry.AutoIncrement = false;
				colvarIsInRetry.IsNullable = false;
				colvarIsInRetry.IsPrimaryKey = false;
				colvarIsInRetry.IsForeignKey = false;
				colvarIsInRetry.IsReadOnly = false;
				
						colvarIsInRetry.DefaultSetting = @"((0))";
				colvarIsInRetry.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsInRetry);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("return_form_refund",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("ReturnFormId")]
		[Bindable(true)]
		public int ReturnFormId 
		{
			get { return GetColumnValue<int>(Columns.ReturnFormId); }
			set { SetColumnValue(Columns.ReturnFormId, value); }
		}
		  
		[XmlAttribute("TrustId")]
		[Bindable(true)]
		public Guid TrustId 
		{
			get { return GetColumnValue<Guid>(Columns.TrustId); }
			set { SetColumnValue(Columns.TrustId, value); }
		}
		  
		[XmlAttribute("VendorNoRefund")]
		[Bindable(true)]
		public bool VendorNoRefund 
		{
			get { return GetColumnValue<bool>(Columns.VendorNoRefund); }
			set { SetColumnValue(Columns.VendorNoRefund, value); }
		}
		  
		[XmlAttribute("IsRefunded")]
		[Bindable(true)]
		public bool? IsRefunded 
		{
			get { return GetColumnValue<bool?>(Columns.IsRefunded); }
			set { SetColumnValue(Columns.IsRefunded, value); }
		}
		  
		[XmlAttribute("FailReason")]
		[Bindable(true)]
		public string FailReason 
		{
			get { return GetColumnValue<string>(Columns.FailReason); }
			set { SetColumnValue(Columns.FailReason, value); }
		}
		  
		[XmlAttribute("IsFreight")]
		[Bindable(true)]
		public bool IsFreight 
		{
			get { return GetColumnValue<bool>(Columns.IsFreight); }
			set { SetColumnValue(Columns.IsFreight, value); }
		}
		  
		[XmlAttribute("IsInRetry")]
		[Bindable(true)]
		public bool IsInRetry 
		{
			get { return GetColumnValue<bool>(Columns.IsInRetry); }
			set { SetColumnValue(Columns.IsInRetry, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (1)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn ReturnFormIdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn TrustIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn VendorNoRefundColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn IsRefundedColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn FailReasonColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn IsFreightColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn IsInRetryColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string ReturnFormId = @"return_form_id";
			 public static string TrustId = @"trust_id";
			 public static string VendorNoRefund = @"vendor_no_refund";
			 public static string IsRefunded = @"is_refunded";
			 public static string FailReason = @"fail_reason";
			 public static string IsFreight = @"is_freight";
			 public static string IsInRetry = @"is_in_retry";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewVbsFlattenedAcl class.
    /// </summary>
    [Serializable]
    public partial class ViewVbsFlattenedAclCollection : ReadOnlyList<ViewVbsFlattenedAcl, ViewVbsFlattenedAclCollection>
    {        
        public ViewVbsFlattenedAclCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_vbs_flattened_acl view.
    /// </summary>
    [Serializable]
    public partial class ViewVbsFlattenedAcl : ReadOnlyRecord<ViewVbsFlattenedAcl>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_vbs_flattened_acl", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarAccountId = new TableSchema.TableColumn(schema);
                colvarAccountId.ColumnName = "account_id";
                colvarAccountId.DataType = DbType.String;
                colvarAccountId.MaxLength = 256;
                colvarAccountId.AutoIncrement = false;
                colvarAccountId.IsNullable = false;
                colvarAccountId.IsPrimaryKey = false;
                colvarAccountId.IsForeignKey = false;
                colvarAccountId.IsReadOnly = false;
                
                schema.Columns.Add(colvarAccountId);
                
                TableSchema.TableColumn colvarSellerGuid = new TableSchema.TableColumn(schema);
                colvarSellerGuid.ColumnName = "seller_guid";
                colvarSellerGuid.DataType = DbType.Guid;
                colvarSellerGuid.MaxLength = 0;
                colvarSellerGuid.AutoIncrement = false;
                colvarSellerGuid.IsNullable = false;
                colvarSellerGuid.IsPrimaryKey = false;
                colvarSellerGuid.IsForeignKey = false;
                colvarSellerGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerGuid);
                
                TableSchema.TableColumn colvarSellerName = new TableSchema.TableColumn(schema);
                colvarSellerName.ColumnName = "seller_name";
                colvarSellerName.DataType = DbType.String;
                colvarSellerName.MaxLength = 121;
                colvarSellerName.AutoIncrement = false;
                colvarSellerName.IsNullable = true;
                colvarSellerName.IsPrimaryKey = false;
                colvarSellerName.IsForeignKey = false;
                colvarSellerName.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerName);
                
                TableSchema.TableColumn colvarStoreGuid = new TableSchema.TableColumn(schema);
                colvarStoreGuid.ColumnName = "store_guid";
                colvarStoreGuid.DataType = DbType.Guid;
                colvarStoreGuid.MaxLength = 0;
                colvarStoreGuid.AutoIncrement = false;
                colvarStoreGuid.IsNullable = true;
                colvarStoreGuid.IsPrimaryKey = false;
                colvarStoreGuid.IsForeignKey = false;
                colvarStoreGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarStoreGuid);
                
                TableSchema.TableColumn colvarStoreName = new TableSchema.TableColumn(schema);
                colvarStoreName.ColumnName = "store_name";
                colvarStoreName.DataType = DbType.String;
                colvarStoreName.MaxLength = 256;
                colvarStoreName.AutoIncrement = false;
                colvarStoreName.IsNullable = true;
                colvarStoreName.IsPrimaryKey = false;
                colvarStoreName.IsForeignKey = false;
                colvarStoreName.IsReadOnly = false;
                
                schema.Columns.Add(colvarStoreName);
                
                TableSchema.TableColumn colvarStoreStatus = new TableSchema.TableColumn(schema);
                colvarStoreStatus.ColumnName = "store_status";
                colvarStoreStatus.DataType = DbType.Int32;
                colvarStoreStatus.MaxLength = 0;
                colvarStoreStatus.AutoIncrement = false;
                colvarStoreStatus.IsNullable = true;
                colvarStoreStatus.IsPrimaryKey = false;
                colvarStoreStatus.IsForeignKey = false;
                colvarStoreStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarStoreStatus);
                
                TableSchema.TableColumn colvarMerchandiseGuid = new TableSchema.TableColumn(schema);
                colvarMerchandiseGuid.ColumnName = "merchandise_guid";
                colvarMerchandiseGuid.DataType = DbType.Guid;
                colvarMerchandiseGuid.MaxLength = 0;
                colvarMerchandiseGuid.AutoIncrement = false;
                colvarMerchandiseGuid.IsNullable = true;
                colvarMerchandiseGuid.IsPrimaryKey = false;
                colvarMerchandiseGuid.IsForeignKey = false;
                colvarMerchandiseGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarMerchandiseGuid);
                
                TableSchema.TableColumn colvarIsPiinDeal = new TableSchema.TableColumn(schema);
                colvarIsPiinDeal.ColumnName = "is_piin_deal";
                colvarIsPiinDeal.DataType = DbType.Boolean;
                colvarIsPiinDeal.MaxLength = 0;
                colvarIsPiinDeal.AutoIncrement = false;
                colvarIsPiinDeal.IsNullable = true;
                colvarIsPiinDeal.IsPrimaryKey = false;
                colvarIsPiinDeal.IsForeignKey = false;
                colvarIsPiinDeal.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsPiinDeal);
                
                TableSchema.TableColumn colvarIsPponDeal = new TableSchema.TableColumn(schema);
                colvarIsPponDeal.ColumnName = "is_ppon_deal";
                colvarIsPponDeal.DataType = DbType.Boolean;
                colvarIsPponDeal.MaxLength = 0;
                colvarIsPponDeal.AutoIncrement = false;
                colvarIsPponDeal.IsNullable = true;
                colvarIsPponDeal.IsPrimaryKey = false;
                colvarIsPponDeal.IsForeignKey = false;
                colvarIsPponDeal.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsPponDeal);
                
                TableSchema.TableColumn colvarMerchandiseId = new TableSchema.TableColumn(schema);
                colvarMerchandiseId.ColumnName = "merchandise_id";
                colvarMerchandiseId.DataType = DbType.Int32;
                colvarMerchandiseId.MaxLength = 0;
                colvarMerchandiseId.AutoIncrement = false;
                colvarMerchandiseId.IsNullable = true;
                colvarMerchandiseId.IsPrimaryKey = false;
                colvarMerchandiseId.IsForeignKey = false;
                colvarMerchandiseId.IsReadOnly = false;
                
                schema.Columns.Add(colvarMerchandiseId);
                
                TableSchema.TableColumn colvarMerchandiseParentId = new TableSchema.TableColumn(schema);
                colvarMerchandiseParentId.ColumnName = "merchandise_parent_id";
                colvarMerchandiseParentId.DataType = DbType.Int32;
                colvarMerchandiseParentId.MaxLength = 0;
                colvarMerchandiseParentId.AutoIncrement = false;
                colvarMerchandiseParentId.IsNullable = true;
                colvarMerchandiseParentId.IsPrimaryKey = false;
                colvarMerchandiseParentId.IsForeignKey = false;
                colvarMerchandiseParentId.IsReadOnly = false;
                
                schema.Columns.Add(colvarMerchandiseParentId);
                
                TableSchema.TableColumn colvarMerchandiseStoreGuid = new TableSchema.TableColumn(schema);
                colvarMerchandiseStoreGuid.ColumnName = "merchandise_store_guid";
                colvarMerchandiseStoreGuid.DataType = DbType.Guid;
                colvarMerchandiseStoreGuid.MaxLength = 0;
                colvarMerchandiseStoreGuid.AutoIncrement = false;
                colvarMerchandiseStoreGuid.IsNullable = true;
                colvarMerchandiseStoreGuid.IsPrimaryKey = false;
                colvarMerchandiseStoreGuid.IsForeignKey = false;
                colvarMerchandiseStoreGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarMerchandiseStoreGuid);
                
                TableSchema.TableColumn colvarMerchandiseStoreResourceGuid = new TableSchema.TableColumn(schema);
                colvarMerchandiseStoreResourceGuid.ColumnName = "merchandise_store_resource_guid";
                colvarMerchandiseStoreResourceGuid.DataType = DbType.Guid;
                colvarMerchandiseStoreResourceGuid.MaxLength = 0;
                colvarMerchandiseStoreResourceGuid.AutoIncrement = false;
                colvarMerchandiseStoreResourceGuid.IsNullable = true;
                colvarMerchandiseStoreResourceGuid.IsPrimaryKey = false;
                colvarMerchandiseStoreResourceGuid.IsForeignKey = false;
                colvarMerchandiseStoreResourceGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarMerchandiseStoreResourceGuid);
                
                TableSchema.TableColumn colvarMerchandiseName = new TableSchema.TableColumn(schema);
                colvarMerchandiseName.ColumnName = "merchandise_name";
                colvarMerchandiseName.DataType = DbType.String;
                colvarMerchandiseName.MaxLength = 500;
                colvarMerchandiseName.AutoIncrement = false;
                colvarMerchandiseName.IsNullable = true;
                colvarMerchandiseName.IsPrimaryKey = false;
                colvarMerchandiseName.IsForeignKey = false;
                colvarMerchandiseName.IsReadOnly = false;
                
                schema.Columns.Add(colvarMerchandiseName);
                
                TableSchema.TableColumn colvarAppTitle = new TableSchema.TableColumn(schema);
                colvarAppTitle.ColumnName = "app_title";
                colvarAppTitle.DataType = DbType.String;
                colvarAppTitle.MaxLength = 40;
                colvarAppTitle.AutoIncrement = false;
                colvarAppTitle.IsNullable = true;
                colvarAppTitle.IsPrimaryKey = false;
                colvarAppTitle.IsForeignKey = false;
                colvarAppTitle.IsReadOnly = false;
                
                schema.Columns.Add(colvarAppTitle);
                
                TableSchema.TableColumn colvarDealStartTime = new TableSchema.TableColumn(schema);
                colvarDealStartTime.ColumnName = "deal_start_time";
                colvarDealStartTime.DataType = DbType.DateTime;
                colvarDealStartTime.MaxLength = 0;
                colvarDealStartTime.AutoIncrement = false;
                colvarDealStartTime.IsNullable = true;
                colvarDealStartTime.IsPrimaryKey = false;
                colvarDealStartTime.IsForeignKey = false;
                colvarDealStartTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarDealStartTime);
                
                TableSchema.TableColumn colvarDealEndTime = new TableSchema.TableColumn(schema);
                colvarDealEndTime.ColumnName = "deal_end_time";
                colvarDealEndTime.DataType = DbType.DateTime;
                colvarDealEndTime.MaxLength = 0;
                colvarDealEndTime.AutoIncrement = false;
                colvarDealEndTime.IsNullable = true;
                colvarDealEndTime.IsPrimaryKey = false;
                colvarDealEndTime.IsForeignKey = false;
                colvarDealEndTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarDealEndTime);
                
                TableSchema.TableColumn colvarDealUseStartTime = new TableSchema.TableColumn(schema);
                colvarDealUseStartTime.ColumnName = "deal_use_start_time";
                colvarDealUseStartTime.DataType = DbType.DateTime;
                colvarDealUseStartTime.MaxLength = 0;
                colvarDealUseStartTime.AutoIncrement = false;
                colvarDealUseStartTime.IsNullable = true;
                colvarDealUseStartTime.IsPrimaryKey = false;
                colvarDealUseStartTime.IsForeignKey = false;
                colvarDealUseStartTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarDealUseStartTime);
                
                TableSchema.TableColumn colvarDealUseEndTime = new TableSchema.TableColumn(schema);
                colvarDealUseEndTime.ColumnName = "deal_use_end_time";
                colvarDealUseEndTime.DataType = DbType.DateTime;
                colvarDealUseEndTime.MaxLength = 0;
                colvarDealUseEndTime.AutoIncrement = false;
                colvarDealUseEndTime.IsNullable = true;
                colvarDealUseEndTime.IsPrimaryKey = false;
                colvarDealUseEndTime.IsForeignKey = false;
                colvarDealUseEndTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarDealUseEndTime);
                
                TableSchema.TableColumn colvarDealEstablished = new TableSchema.TableColumn(schema);
                colvarDealEstablished.ColumnName = "deal_established";
                colvarDealEstablished.DataType = DbType.Int32;
                colvarDealEstablished.MaxLength = 0;
                colvarDealEstablished.AutoIncrement = false;
                colvarDealEstablished.IsNullable = true;
                colvarDealEstablished.IsPrimaryKey = false;
                colvarDealEstablished.IsForeignKey = false;
                colvarDealEstablished.IsReadOnly = false;
                
                schema.Columns.Add(colvarDealEstablished);
                
                TableSchema.TableColumn colvarVendorBillingModel = new TableSchema.TableColumn(schema);
                colvarVendorBillingModel.ColumnName = "vendor_billing_model";
                colvarVendorBillingModel.DataType = DbType.Int32;
                colvarVendorBillingModel.MaxLength = 0;
                colvarVendorBillingModel.AutoIncrement = false;
                colvarVendorBillingModel.IsNullable = true;
                colvarVendorBillingModel.IsPrimaryKey = false;
                colvarVendorBillingModel.IsForeignKey = false;
                colvarVendorBillingModel.IsReadOnly = false;
                
                schema.Columns.Add(colvarVendorBillingModel);
                
                TableSchema.TableColumn colvarRemittanceType = new TableSchema.TableColumn(schema);
                colvarRemittanceType.ColumnName = "remittance_type";
                colvarRemittanceType.DataType = DbType.Int32;
                colvarRemittanceType.MaxLength = 0;
                colvarRemittanceType.AutoIncrement = false;
                colvarRemittanceType.IsNullable = true;
                colvarRemittanceType.IsPrimaryKey = false;
                colvarRemittanceType.IsForeignKey = false;
                colvarRemittanceType.IsReadOnly = false;
                
                schema.Columns.Add(colvarRemittanceType);
                
                TableSchema.TableColumn colvarIsInStore = new TableSchema.TableColumn(schema);
                colvarIsInStore.ColumnName = "is_in_store";
                colvarIsInStore.DataType = DbType.Boolean;
                colvarIsInStore.MaxLength = 0;
                colvarIsInStore.AutoIncrement = false;
                colvarIsInStore.IsNullable = true;
                colvarIsInStore.IsPrimaryKey = false;
                colvarIsInStore.IsForeignKey = false;
                colvarIsInStore.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsInStore);
                
                TableSchema.TableColumn colvarIsHomeDelivery = new TableSchema.TableColumn(schema);
                colvarIsHomeDelivery.ColumnName = "is_home_delivery";
                colvarIsHomeDelivery.DataType = DbType.Boolean;
                colvarIsHomeDelivery.MaxLength = 0;
                colvarIsHomeDelivery.AutoIncrement = false;
                colvarIsHomeDelivery.IsNullable = true;
                colvarIsHomeDelivery.IsPrimaryKey = false;
                colvarIsHomeDelivery.IsForeignKey = false;
                colvarIsHomeDelivery.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsHomeDelivery);
                
                TableSchema.TableColumn colvarLabelIconList = new TableSchema.TableColumn(schema);
                colvarLabelIconList.ColumnName = "label_icon_list";
                colvarLabelIconList.DataType = DbType.AnsiString;
                colvarLabelIconList.MaxLength = 255;
                colvarLabelIconList.AutoIncrement = false;
                colvarLabelIconList.IsNullable = true;
                colvarLabelIconList.IsPrimaryKey = false;
                colvarLabelIconList.IsForeignKey = false;
                colvarLabelIconList.IsReadOnly = false;
                
                schema.Columns.Add(colvarLabelIconList);
                
                TableSchema.TableColumn colvarVbsRight = new TableSchema.TableColumn(schema);
                colvarVbsRight.ColumnName = "vbs_right";
                colvarVbsRight.DataType = DbType.Int32;
                colvarVbsRight.MaxLength = 0;
                colvarVbsRight.AutoIncrement = false;
                colvarVbsRight.IsNullable = true;
                colvarVbsRight.IsPrimaryKey = false;
                colvarVbsRight.IsForeignKey = false;
                colvarVbsRight.IsReadOnly = false;
                
                schema.Columns.Add(colvarVbsRight);
                
                TableSchema.TableColumn colvarIsRestrictedStore = new TableSchema.TableColumn(schema);
                colvarIsRestrictedStore.ColumnName = "is_restricted_store";
                colvarIsRestrictedStore.DataType = DbType.Int32;
                colvarIsRestrictedStore.MaxLength = 0;
                colvarIsRestrictedStore.AutoIncrement = false;
                colvarIsRestrictedStore.IsNullable = true;
                colvarIsRestrictedStore.IsPrimaryKey = false;
                colvarIsRestrictedStore.IsForeignKey = false;
                colvarIsRestrictedStore.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsRestrictedStore);
                
                TableSchema.TableColumn colvarSellerPermission = new TableSchema.TableColumn(schema);
                colvarSellerPermission.ColumnName = "seller_permission";
                colvarSellerPermission.DataType = DbType.Int32;
                colvarSellerPermission.MaxLength = 0;
                colvarSellerPermission.AutoIncrement = false;
                colvarSellerPermission.IsNullable = true;
                colvarSellerPermission.IsPrimaryKey = false;
                colvarSellerPermission.IsForeignKey = false;
                colvarSellerPermission.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerPermission);
                
                TableSchema.TableColumn colvarStorePermission = new TableSchema.TableColumn(schema);
                colvarStorePermission.ColumnName = "store_permission";
                colvarStorePermission.DataType = DbType.Int32;
                colvarStorePermission.MaxLength = 0;
                colvarStorePermission.AutoIncrement = false;
                colvarStorePermission.IsNullable = true;
                colvarStorePermission.IsPrimaryKey = false;
                colvarStorePermission.IsForeignKey = false;
                colvarStorePermission.IsReadOnly = false;
                
                schema.Columns.Add(colvarStorePermission);
                
                TableSchema.TableColumn colvarDealStorePermission = new TableSchema.TableColumn(schema);
                colvarDealStorePermission.ColumnName = "deal_store_permission";
                colvarDealStorePermission.DataType = DbType.Int32;
                colvarDealStorePermission.MaxLength = 0;
                colvarDealStorePermission.AutoIncrement = false;
                colvarDealStorePermission.IsNullable = true;
                colvarDealStorePermission.IsPrimaryKey = false;
                colvarDealStorePermission.IsForeignKey = false;
                colvarDealStorePermission.IsReadOnly = false;
                
                schema.Columns.Add(colvarDealStorePermission);
                
                TableSchema.TableColumn colvarPponPermission = new TableSchema.TableColumn(schema);
                colvarPponPermission.ColumnName = "ppon_permission";
                colvarPponPermission.DataType = DbType.Int32;
                colvarPponPermission.MaxLength = 0;
                colvarPponPermission.AutoIncrement = false;
                colvarPponPermission.IsNullable = true;
                colvarPponPermission.IsPrimaryKey = false;
                colvarPponPermission.IsForeignKey = false;
                colvarPponPermission.IsReadOnly = false;
                
                schema.Columns.Add(colvarPponPermission);
                
                TableSchema.TableColumn colvarPiinPermission = new TableSchema.TableColumn(schema);
                colvarPiinPermission.ColumnName = "piin_permission";
                colvarPiinPermission.DataType = DbType.Int32;
                colvarPiinPermission.MaxLength = 0;
                colvarPiinPermission.AutoIncrement = false;
                colvarPiinPermission.IsNullable = true;
                colvarPiinPermission.IsPrimaryKey = false;
                colvarPiinPermission.IsForeignKey = false;
                colvarPiinPermission.IsReadOnly = false;
                
                schema.Columns.Add(colvarPiinPermission);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_vbs_flattened_acl",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewVbsFlattenedAcl()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewVbsFlattenedAcl(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewVbsFlattenedAcl(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewVbsFlattenedAcl(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("AccountId")]
        [Bindable(true)]
        public string AccountId 
	    {
		    get
		    {
			    return GetColumnValue<string>("account_id");
		    }
            set 
		    {
			    SetColumnValue("account_id", value);
            }
        }
	      
        [XmlAttribute("SellerGuid")]
        [Bindable(true)]
        public Guid SellerGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("seller_guid");
		    }
            set 
		    {
			    SetColumnValue("seller_guid", value);
            }
        }
	      
        [XmlAttribute("SellerName")]
        [Bindable(true)]
        public string SellerName 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_name");
		    }
            set 
		    {
			    SetColumnValue("seller_name", value);
            }
        }
	      
        [XmlAttribute("StoreGuid")]
        [Bindable(true)]
        public Guid? StoreGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid?>("store_guid");
		    }
            set 
		    {
			    SetColumnValue("store_guid", value);
            }
        }
	      
        [XmlAttribute("StoreName")]
        [Bindable(true)]
        public string StoreName 
	    {
		    get
		    {
			    return GetColumnValue<string>("store_name");
		    }
            set 
		    {
			    SetColumnValue("store_name", value);
            }
        }
	      
        [XmlAttribute("StoreStatus")]
        [Bindable(true)]
        public int? StoreStatus 
	    {
		    get
		    {
			    return GetColumnValue<int?>("store_status");
		    }
            set 
		    {
			    SetColumnValue("store_status", value);
            }
        }
	      
        [XmlAttribute("MerchandiseGuid")]
        [Bindable(true)]
        public Guid? MerchandiseGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid?>("merchandise_guid");
		    }
            set 
		    {
			    SetColumnValue("merchandise_guid", value);
            }
        }
	      
        [XmlAttribute("IsPiinDeal")]
        [Bindable(true)]
        public bool? IsPiinDeal 
	    {
		    get
		    {
			    return GetColumnValue<bool?>("is_piin_deal");
		    }
            set 
		    {
			    SetColumnValue("is_piin_deal", value);
            }
        }
	      
        [XmlAttribute("IsPponDeal")]
        [Bindable(true)]
        public bool? IsPponDeal 
	    {
		    get
		    {
			    return GetColumnValue<bool?>("is_ppon_deal");
		    }
            set 
		    {
			    SetColumnValue("is_ppon_deal", value);
            }
        }
	      
        [XmlAttribute("MerchandiseId")]
        [Bindable(true)]
        public int? MerchandiseId 
	    {
		    get
		    {
			    return GetColumnValue<int?>("merchandise_id");
		    }
            set 
		    {
			    SetColumnValue("merchandise_id", value);
            }
        }
	      
        [XmlAttribute("MerchandiseParentId")]
        [Bindable(true)]
        public int? MerchandiseParentId 
	    {
		    get
		    {
			    return GetColumnValue<int?>("merchandise_parent_id");
		    }
            set 
		    {
			    SetColumnValue("merchandise_parent_id", value);
            }
        }
	      
        [XmlAttribute("MerchandiseStoreGuid")]
        [Bindable(true)]
        public Guid? MerchandiseStoreGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid?>("merchandise_store_guid");
		    }
            set 
		    {
			    SetColumnValue("merchandise_store_guid", value);
            }
        }
	      
        [XmlAttribute("MerchandiseStoreResourceGuid")]
        [Bindable(true)]
        public Guid? MerchandiseStoreResourceGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid?>("merchandise_store_resource_guid");
		    }
            set 
		    {
			    SetColumnValue("merchandise_store_resource_guid", value);
            }
        }
	      
        [XmlAttribute("MerchandiseName")]
        [Bindable(true)]
        public string MerchandiseName 
	    {
		    get
		    {
			    return GetColumnValue<string>("merchandise_name");
		    }
            set 
		    {
			    SetColumnValue("merchandise_name", value);
            }
        }
	      
        [XmlAttribute("AppTitle")]
        [Bindable(true)]
        public string AppTitle 
	    {
		    get
		    {
			    return GetColumnValue<string>("app_title");
		    }
            set 
		    {
			    SetColumnValue("app_title", value);
            }
        }
	      
        [XmlAttribute("DealStartTime")]
        [Bindable(true)]
        public DateTime? DealStartTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("deal_start_time");
		    }
            set 
		    {
			    SetColumnValue("deal_start_time", value);
            }
        }
	      
        [XmlAttribute("DealEndTime")]
        [Bindable(true)]
        public DateTime? DealEndTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("deal_end_time");
		    }
            set 
		    {
			    SetColumnValue("deal_end_time", value);
            }
        }
	      
        [XmlAttribute("DealUseStartTime")]
        [Bindable(true)]
        public DateTime? DealUseStartTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("deal_use_start_time");
		    }
            set 
		    {
			    SetColumnValue("deal_use_start_time", value);
            }
        }
	      
        [XmlAttribute("DealUseEndTime")]
        [Bindable(true)]
        public DateTime? DealUseEndTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("deal_use_end_time");
		    }
            set 
		    {
			    SetColumnValue("deal_use_end_time", value);
            }
        }
	      
        [XmlAttribute("DealEstablished")]
        [Bindable(true)]
        public int? DealEstablished 
	    {
		    get
		    {
			    return GetColumnValue<int?>("deal_established");
		    }
            set 
		    {
			    SetColumnValue("deal_established", value);
            }
        }
	      
        [XmlAttribute("VendorBillingModel")]
        [Bindable(true)]
        public int? VendorBillingModel 
	    {
		    get
		    {
			    return GetColumnValue<int?>("vendor_billing_model");
		    }
            set 
		    {
			    SetColumnValue("vendor_billing_model", value);
            }
        }
	      
        [XmlAttribute("RemittanceType")]
        [Bindable(true)]
        public int? RemittanceType 
	    {
		    get
		    {
			    return GetColumnValue<int?>("remittance_type");
		    }
            set 
		    {
			    SetColumnValue("remittance_type", value);
            }
        }
	      
        [XmlAttribute("IsInStore")]
        [Bindable(true)]
        public bool? IsInStore 
	    {
		    get
		    {
			    return GetColumnValue<bool?>("is_in_store");
		    }
            set 
		    {
			    SetColumnValue("is_in_store", value);
            }
        }
	      
        [XmlAttribute("IsHomeDelivery")]
        [Bindable(true)]
        public bool? IsHomeDelivery 
	    {
		    get
		    {
			    return GetColumnValue<bool?>("is_home_delivery");
		    }
            set 
		    {
			    SetColumnValue("is_home_delivery", value);
            }
        }
	      
        [XmlAttribute("LabelIconList")]
        [Bindable(true)]
        public string LabelIconList 
	    {
		    get
		    {
			    return GetColumnValue<string>("label_icon_list");
		    }
            set 
		    {
			    SetColumnValue("label_icon_list", value);
            }
        }
	      
        [XmlAttribute("VbsRight")]
        [Bindable(true)]
        public int? VbsRight 
	    {
		    get
		    {
			    return GetColumnValue<int?>("vbs_right");
		    }
            set 
		    {
			    SetColumnValue("vbs_right", value);
            }
        }
	      
        [XmlAttribute("IsRestrictedStore")]
        [Bindable(true)]
        public int? IsRestrictedStore 
	    {
		    get
		    {
			    return GetColumnValue<int?>("is_restricted_store");
		    }
            set 
		    {
			    SetColumnValue("is_restricted_store", value);
            }
        }
	      
        [XmlAttribute("SellerPermission")]
        [Bindable(true)]
        public int? SellerPermission 
	    {
		    get
		    {
			    return GetColumnValue<int?>("seller_permission");
		    }
            set 
		    {
			    SetColumnValue("seller_permission", value);
            }
        }
	      
        [XmlAttribute("StorePermission")]
        [Bindable(true)]
        public int? StorePermission 
	    {
		    get
		    {
			    return GetColumnValue<int?>("store_permission");
		    }
            set 
		    {
			    SetColumnValue("store_permission", value);
            }
        }
	      
        [XmlAttribute("DealStorePermission")]
        [Bindable(true)]
        public int? DealStorePermission 
	    {
		    get
		    {
			    return GetColumnValue<int?>("deal_store_permission");
		    }
            set 
		    {
			    SetColumnValue("deal_store_permission", value);
            }
        }
	      
        [XmlAttribute("PponPermission")]
        [Bindable(true)]
        public int? PponPermission 
	    {
		    get
		    {
			    return GetColumnValue<int?>("ppon_permission");
		    }
            set 
		    {
			    SetColumnValue("ppon_permission", value);
            }
        }
	      
        [XmlAttribute("PiinPermission")]
        [Bindable(true)]
        public int? PiinPermission 
	    {
		    get
		    {
			    return GetColumnValue<int?>("piin_permission");
		    }
            set 
		    {
			    SetColumnValue("piin_permission", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string AccountId = @"account_id";
            
            public static string SellerGuid = @"seller_guid";
            
            public static string SellerName = @"seller_name";
            
            public static string StoreGuid = @"store_guid";
            
            public static string StoreName = @"store_name";
            
            public static string StoreStatus = @"store_status";
            
            public static string MerchandiseGuid = @"merchandise_guid";
            
            public static string IsPiinDeal = @"is_piin_deal";
            
            public static string IsPponDeal = @"is_ppon_deal";
            
            public static string MerchandiseId = @"merchandise_id";
            
            public static string MerchandiseParentId = @"merchandise_parent_id";
            
            public static string MerchandiseStoreGuid = @"merchandise_store_guid";
            
            public static string MerchandiseStoreResourceGuid = @"merchandise_store_resource_guid";
            
            public static string MerchandiseName = @"merchandise_name";
            
            public static string AppTitle = @"app_title";
            
            public static string DealStartTime = @"deal_start_time";
            
            public static string DealEndTime = @"deal_end_time";
            
            public static string DealUseStartTime = @"deal_use_start_time";
            
            public static string DealUseEndTime = @"deal_use_end_time";
            
            public static string DealEstablished = @"deal_established";
            
            public static string VendorBillingModel = @"vendor_billing_model";
            
            public static string RemittanceType = @"remittance_type";
            
            public static string IsInStore = @"is_in_store";
            
            public static string IsHomeDelivery = @"is_home_delivery";
            
            public static string LabelIconList = @"label_icon_list";
            
            public static string VbsRight = @"vbs_right";
            
            public static string IsRestrictedStore = @"is_restricted_store";
            
            public static string SellerPermission = @"seller_permission";
            
            public static string StorePermission = @"store_permission";
            
            public static string DealStorePermission = @"deal_store_permission";
            
            public static string PponPermission = @"ppon_permission";
            
            public static string PiinPermission = @"piin_permission";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

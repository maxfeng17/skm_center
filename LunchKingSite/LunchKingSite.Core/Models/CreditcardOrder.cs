using System;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using SubSonic;

namespace LunchKingSite.DataOrm
{
	[Serializable]
	public partial class CreditcardOrderCollection : RepositoryList<CreditcardOrder, CreditcardOrderCollection>
	{
			public CreditcardOrderCollection() {}

			public CreditcardOrderCollection Filter()
			{
					for (int i = this.Count - 1; i > -1; i--)
					{
							CreditcardOrder o = this[i];
							foreach (SubSonic.Where w in this.wheres)
							{
									bool remove = false;
									System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
									if(pi.CanRead)
									{
											object val = pi.GetValue(o, null);
											switch(w.Comparison)
											{
													case SubSonic.Comparison.Equals:
															if(!val.Equals(w.ParameterValue))
															{
																	remove = true;
															}
															break;
											}
									}
									if(remove)
									{
											this.Remove(o);
											break;
									}
							}
					}
					return this;
			}
	}

	[Serializable]
	public partial class CreditcardOrder : RepositoryRecord<CreditcardOrder>, IRecordBase
	{
		#region .ctors and Default Settings
		public CreditcardOrder()
		{
			SetSQLProps();
			InitSetDefaults();
			MarkNew();
		}

		private void InitSetDefaults() { SetDefaults(); }

		public CreditcardOrder(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}

		protected static void SetSQLProps() { GetTableSchema(); }

		#endregion

		#region Schema and Query Accessor
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}

		private static void GetTableSchema()
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("creditcard_order", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);

				TableSchema.TableColumn colvarMemberId = new TableSchema.TableColumn(schema);
				colvarMemberId.ColumnName = "member_id";
				colvarMemberId.DataType = DbType.Int32;
				colvarMemberId.MaxLength = 0;
				colvarMemberId.AutoIncrement = false;
				colvarMemberId.IsNullable = false;
				colvarMemberId.IsPrimaryKey = false;
				colvarMemberId.IsForeignKey = false;
				colvarMemberId.IsReadOnly = false;
				colvarMemberId.DefaultSetting = @"";
				colvarMemberId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMemberId);

				TableSchema.TableColumn colvarOrderGuid = new TableSchema.TableColumn(schema);
				colvarOrderGuid.ColumnName = "order_guid";
				colvarOrderGuid.DataType = DbType.Guid;
				colvarOrderGuid.MaxLength = 0;
				colvarOrderGuid.AutoIncrement = false;
				colvarOrderGuid.IsNullable = false;
				colvarOrderGuid.IsPrimaryKey = false;
				colvarOrderGuid.IsForeignKey = false;
				colvarOrderGuid.IsReadOnly = false;
				colvarOrderGuid.DefaultSetting = @"";
				colvarOrderGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderGuid);

				TableSchema.TableColumn colvarOrderClassification = new TableSchema.TableColumn(schema);
				colvarOrderClassification.ColumnName = "order_classification";
				colvarOrderClassification.DataType = DbType.Int32;
				colvarOrderClassification.MaxLength = 0;
				colvarOrderClassification.AutoIncrement = false;
				colvarOrderClassification.IsNullable = false;
				colvarOrderClassification.IsPrimaryKey = false;
				colvarOrderClassification.IsForeignKey = false;
				colvarOrderClassification.IsReadOnly = false;
				colvarOrderClassification.DefaultSetting = @"";
				colvarOrderClassification.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderClassification);

				TableSchema.TableColumn colvarCreditcardAmount = new TableSchema.TableColumn(schema);
				colvarCreditcardAmount.ColumnName = "creditcard_amount";
				colvarCreditcardAmount.DataType = DbType.Int32;
				colvarCreditcardAmount.MaxLength = 0;
				colvarCreditcardAmount.AutoIncrement = false;
				colvarCreditcardAmount.IsNullable = false;
				colvarCreditcardAmount.IsPrimaryKey = false;
				colvarCreditcardAmount.IsForeignKey = false;
				colvarCreditcardAmount.IsReadOnly = false;
				colvarCreditcardAmount.DefaultSetting = @"";
				colvarCreditcardAmount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreditcardAmount);

				TableSchema.TableColumn colvarExpDate = new TableSchema.TableColumn(schema);
				colvarExpDate.ColumnName = "exp_date";
				colvarExpDate.DataType = DbType.String;
				colvarExpDate.MaxLength = 10;
				colvarExpDate.AutoIncrement = false;
				colvarExpDate.IsNullable = true;
				colvarExpDate.IsPrimaryKey = false;
				colvarExpDate.IsForeignKey = false;
				colvarExpDate.IsReadOnly = false;
				colvarExpDate.DefaultSetting = @"";
				colvarExpDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarExpDate);

				TableSchema.TableColumn colvarResult = new TableSchema.TableColumn(schema);
				colvarResult.ColumnName = "result";
				colvarResult.DataType = DbType.Int32;
				colvarResult.MaxLength = 0;
				colvarResult.AutoIncrement = false;
				colvarResult.IsNullable = false;
				colvarResult.IsPrimaryKey = false;
				colvarResult.IsForeignKey = false;
				colvarResult.IsReadOnly = false;
				colvarResult.DefaultSetting = @"((0))";
				colvarResult.ForeignKeyTableName = "";
				schema.Columns.Add(colvarResult);

				TableSchema.TableColumn colvarConsumerIp = new TableSchema.TableColumn(schema);
				colvarConsumerIp.ColumnName = "consumer_ip";
				colvarConsumerIp.DataType = DbType.String;
				colvarConsumerIp.MaxLength = 50;
				colvarConsumerIp.AutoIncrement = false;
				colvarConsumerIp.IsNullable = true;
				colvarConsumerIp.IsPrimaryKey = false;
				colvarConsumerIp.IsForeignKey = false;
				colvarConsumerIp.IsReadOnly = false;
				colvarConsumerIp.DefaultSetting = @"";
				colvarConsumerIp.ForeignKeyTableName = "";
				schema.Columns.Add(colvarConsumerIp);

				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);

				TableSchema.TableColumn colvarIsLocked = new TableSchema.TableColumn(schema);
				colvarIsLocked.ColumnName = "is_locked";
				colvarIsLocked.DataType = DbType.Boolean;
				colvarIsLocked.MaxLength = 0;
				colvarIsLocked.AutoIncrement = false;
				colvarIsLocked.IsNullable = false;
				colvarIsLocked.IsPrimaryKey = false;
				colvarIsLocked.IsForeignKey = false;
				colvarIsLocked.IsReadOnly = false;
				colvarIsLocked.DefaultSetting = @"((0))";
				colvarIsLocked.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsLocked);

				TableSchema.TableColumn colvarLockerId = new TableSchema.TableColumn(schema);
				colvarLockerId.ColumnName = "locker_id";
				colvarLockerId.DataType = DbType.String;
				colvarLockerId.MaxLength = 256;
				colvarLockerId.AutoIncrement = false;
				colvarLockerId.IsNullable = true;
				colvarLockerId.IsPrimaryKey = false;
				colvarLockerId.IsForeignKey = false;
				colvarLockerId.IsReadOnly = false;
				colvarLockerId.DefaultSetting = @"";
				colvarLockerId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarLockerId);

				TableSchema.TableColumn colvarLockedTime = new TableSchema.TableColumn(schema);
				colvarLockedTime.ColumnName = "locked_time";
				colvarLockedTime.DataType = DbType.DateTime;
				colvarLockedTime.MaxLength = 0;
				colvarLockedTime.AutoIncrement = false;
				colvarLockedTime.IsNullable = true;
				colvarLockedTime.IsPrimaryKey = false;
				colvarLockedTime.IsForeignKey = false;
				colvarLockedTime.IsReadOnly = false;
				colvarLockedTime.DefaultSetting = @"";
				colvarLockedTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarLockedTime);

				TableSchema.TableColumn colvarUnlockerId = new TableSchema.TableColumn(schema);
				colvarUnlockerId.ColumnName = "unlocker_id";
				colvarUnlockerId.DataType = DbType.String;
				colvarUnlockerId.MaxLength = 256;
				colvarUnlockerId.AutoIncrement = false;
				colvarUnlockerId.IsNullable = true;
				colvarUnlockerId.IsPrimaryKey = false;
				colvarUnlockerId.IsForeignKey = false;
				colvarUnlockerId.IsReadOnly = false;
				colvarUnlockerId.DefaultSetting = @"";
				colvarUnlockerId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUnlockerId);

				TableSchema.TableColumn colvarUnlockedTime = new TableSchema.TableColumn(schema);
				colvarUnlockedTime.ColumnName = "unlocked_time";
				colvarUnlockedTime.DataType = DbType.DateTime;
				colvarUnlockedTime.MaxLength = 0;
				colvarUnlockedTime.AutoIncrement = false;
				colvarUnlockedTime.IsNullable = true;
				colvarUnlockedTime.IsPrimaryKey = false;
				colvarUnlockedTime.IsForeignKey = false;
				colvarUnlockedTime.IsReadOnly = false;
				colvarUnlockedTime.DefaultSetting = @"";
				colvarUnlockedTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUnlockedTime);

				TableSchema.TableColumn colvarIsReference = new TableSchema.TableColumn(schema);
				colvarIsReference.ColumnName = "is_reference";
				colvarIsReference.DataType = DbType.Boolean;
				colvarIsReference.MaxLength = 0;
				colvarIsReference.AutoIncrement = false;
				colvarIsReference.IsNullable = false;
				colvarIsReference.IsPrimaryKey = false;
				colvarIsReference.IsForeignKey = false;
				colvarIsReference.IsReadOnly = false;
				colvarIsReference.DefaultSetting = @"((0))";
				colvarIsReference.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsReference);

				TableSchema.TableColumn colvarReferenceId = new TableSchema.TableColumn(schema);
				colvarReferenceId.ColumnName = "reference_id";
				colvarReferenceId.DataType = DbType.String;
				colvarReferenceId.MaxLength = 256;
				colvarReferenceId.AutoIncrement = false;
				colvarReferenceId.IsNullable = true;
				colvarReferenceId.IsPrimaryKey = false;
				colvarReferenceId.IsForeignKey = false;
				colvarReferenceId.IsReadOnly = false;
				colvarReferenceId.DefaultSetting = @"";
				colvarReferenceId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReferenceId);

				TableSchema.TableColumn colvarReferenceTime = new TableSchema.TableColumn(schema);
				colvarReferenceTime.ColumnName = "reference_time";
				colvarReferenceTime.DataType = DbType.DateTime;
				colvarReferenceTime.MaxLength = 0;
				colvarReferenceTime.AutoIncrement = false;
				colvarReferenceTime.IsNullable = true;
				colvarReferenceTime.IsPrimaryKey = false;
				colvarReferenceTime.IsForeignKey = false;
				colvarReferenceTime.IsReadOnly = false;
				colvarReferenceTime.DefaultSetting = @"";
				colvarReferenceTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReferenceTime);

				TableSchema.TableColumn colvarApiProvider = new TableSchema.TableColumn(schema);
				colvarApiProvider.ColumnName = "api_provider";
				colvarApiProvider.DataType = DbType.Int32;
				colvarApiProvider.MaxLength = 0;
				colvarApiProvider.AutoIncrement = false;
				colvarApiProvider.IsNullable = true;
				colvarApiProvider.IsPrimaryKey = false;
				colvarApiProvider.IsForeignKey = false;
				colvarApiProvider.IsReadOnly = false;
				colvarApiProvider.DefaultSetting = @"";
				colvarApiProvider.ForeignKeyTableName = "";
				schema.Columns.Add(colvarApiProvider);

				TableSchema.TableColumn colvarCreditcardInfo = new TableSchema.TableColumn(schema);
				colvarCreditcardInfo.ColumnName = "creditcard_info";
				colvarCreditcardInfo.DataType = DbType.String;
				colvarCreditcardInfo.MaxLength = 200;
				colvarCreditcardInfo.AutoIncrement = false;
				colvarCreditcardInfo.IsNullable = true;
				colvarCreditcardInfo.IsPrimaryKey = false;
				colvarCreditcardInfo.IsForeignKey = false;
				colvarCreditcardInfo.IsReadOnly = false;
				colvarCreditcardInfo.DefaultSetting = @"";
				colvarCreditcardInfo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreditcardInfo);

				TableSchema.TableColumn colvarBlackListType = new TableSchema.TableColumn(schema);
				colvarBlackListType.ColumnName = "black_list_type";
				colvarBlackListType.DataType = DbType.Int32;
				colvarBlackListType.MaxLength = 0;
				colvarBlackListType.AutoIncrement = false;
				colvarBlackListType.IsNullable = true;
				colvarBlackListType.IsPrimaryKey = false;
				colvarBlackListType.IsForeignKey = false;
				colvarBlackListType.IsReadOnly = false;
				colvarBlackListType.DefaultSetting = @"";
				colvarBlackListType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBlackListType);

				TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
				colvarCreateId.ColumnName = "create_id";
				colvarCreateId.DataType = DbType.String;
				colvarCreateId.MaxLength = 512;
				colvarCreateId.AutoIncrement = false;
				colvarCreateId.IsNullable = true;
				colvarCreateId.IsPrimaryKey = false;
				colvarCreateId.IsForeignKey = false;
				colvarCreateId.IsReadOnly = false;
				colvarCreateId.DefaultSetting = @"";
				colvarCreateId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateId);

				TableSchema.TableColumn colvarCreditcardInvalidTimes = new TableSchema.TableColumn(schema);
				colvarCreditcardInvalidTimes.ColumnName = "creditcard_invalid_times";
				colvarCreditcardInvalidTimes.DataType = DbType.Int32;
				colvarCreditcardInvalidTimes.MaxLength = 0;
				colvarCreditcardInvalidTimes.AutoIncrement = false;
				colvarCreditcardInvalidTimes.IsNullable = true;
				colvarCreditcardInvalidTimes.IsPrimaryKey = false;
				colvarCreditcardInvalidTimes.IsForeignKey = false;
				colvarCreditcardInvalidTimes.IsReadOnly = false;
				colvarCreditcardInvalidTimes.DefaultSetting = @"";
				colvarCreditcardInvalidTimes.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreditcardInvalidTimes);

				TableSchema.TableColumn colvarMaskedCardNumber = new TableSchema.TableColumn(schema);
				colvarMaskedCardNumber.ColumnName = "masked_card_number";
				colvarMaskedCardNumber.DataType = DbType.String;
				colvarMaskedCardNumber.MaxLength = 20;
				colvarMaskedCardNumber.AutoIncrement = false;
				colvarMaskedCardNumber.IsNullable = true;
				colvarMaskedCardNumber.IsPrimaryKey = false;
				colvarMaskedCardNumber.IsForeignKey = false;
				colvarMaskedCardNumber.IsReadOnly = false;
				colvarMaskedCardNumber.DefaultSetting = @"";
				colvarMaskedCardNumber.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMaskedCardNumber);

				TableSchema.TableColumn colvarIsOtp = new TableSchema.TableColumn(schema);
				colvarIsOtp.ColumnName = "is_otp";
				colvarIsOtp.DataType = DbType.Boolean;
				colvarIsOtp.MaxLength = 0;
				colvarIsOtp.AutoIncrement = false;
				colvarIsOtp.IsNullable = true;
				colvarIsOtp.IsPrimaryKey = false;
				colvarIsOtp.IsForeignKey = false;
				colvarIsOtp.IsReadOnly = false;
				colvarIsOtp.DefaultSetting = @"";
				colvarIsOtp.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsOtp);

				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("creditcard_order",schema);
			}
		}
		#endregion

		#region Props

		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}

		[XmlAttribute("MemberId")]
		[Bindable(true)]
		public int MemberId
		{
			get { return GetColumnValue<int>(Columns.MemberId); }
			set { SetColumnValue(Columns.MemberId, value); }
		}

		[XmlAttribute("OrderGuid")]
		[Bindable(true)]
		public Guid OrderGuid
		{
			get { return GetColumnValue<Guid>(Columns.OrderGuid); }
			set { SetColumnValue(Columns.OrderGuid, value); }
		}

		[XmlAttribute("OrderClassification")]
		[Bindable(true)]
		public int OrderClassification
		{
			get { return GetColumnValue<int>(Columns.OrderClassification); }
			set { SetColumnValue(Columns.OrderClassification, value); }
		}

		[XmlAttribute("CreditcardAmount")]
		[Bindable(true)]
		public int CreditcardAmount
		{
			get { return GetColumnValue<int>(Columns.CreditcardAmount); }
			set { SetColumnValue(Columns.CreditcardAmount, value); }
		}

		[XmlAttribute("ExpDate")]
		[Bindable(true)]
		public string ExpDate
		{
			get { return GetColumnValue<string>(Columns.ExpDate); }
			set { SetColumnValue(Columns.ExpDate, value); }
		}

		[XmlAttribute("Result")]
		[Bindable(true)]
		public int Result
		{
			get { return GetColumnValue<int>(Columns.Result); }
			set { SetColumnValue(Columns.Result, value); }
		}

		[XmlAttribute("ConsumerIp")]
		[Bindable(true)]
		public string ConsumerIp
		{
			get { return GetColumnValue<string>(Columns.ConsumerIp); }
			set { SetColumnValue(Columns.ConsumerIp, value); }
		}

		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}

		[XmlAttribute("IsLocked")]
		[Bindable(true)]
		public bool IsLocked
		{
			get { return GetColumnValue<bool>(Columns.IsLocked); }
			set { SetColumnValue(Columns.IsLocked, value); }
		}

		[XmlAttribute("LockerId")]
		[Bindable(true)]
		public string LockerId
		{
			get { return GetColumnValue<string>(Columns.LockerId); }
			set { SetColumnValue(Columns.LockerId, value); }
		}

		[XmlAttribute("LockedTime")]
		[Bindable(true)]
		public DateTime? LockedTime
		{
			get { return GetColumnValue<DateTime?>(Columns.LockedTime); }
			set { SetColumnValue(Columns.LockedTime, value); }
		}

		[XmlAttribute("UnlockerId")]
		[Bindable(true)]
		public string UnlockerId
		{
			get { return GetColumnValue<string>(Columns.UnlockerId); }
			set { SetColumnValue(Columns.UnlockerId, value); }
		}

		[XmlAttribute("UnlockedTime")]
		[Bindable(true)]
		public DateTime? UnlockedTime
		{
			get { return GetColumnValue<DateTime?>(Columns.UnlockedTime); }
			set { SetColumnValue(Columns.UnlockedTime, value); }
		}

		[XmlAttribute("IsReference")]
		[Bindable(true)]
		public bool IsReference
		{
			get { return GetColumnValue<bool>(Columns.IsReference); }
			set { SetColumnValue(Columns.IsReference, value); }
		}

		[XmlAttribute("ReferenceId")]
		[Bindable(true)]
		public string ReferenceId
		{
			get { return GetColumnValue<string>(Columns.ReferenceId); }
			set { SetColumnValue(Columns.ReferenceId, value); }
		}

		[XmlAttribute("ReferenceTime")]
		[Bindable(true)]
		public DateTime? ReferenceTime
		{
			get { return GetColumnValue<DateTime?>(Columns.ReferenceTime); }
			set { SetColumnValue(Columns.ReferenceTime, value); }
		}

		[XmlAttribute("ApiProvider")]
		[Bindable(true)]
		public int? ApiProvider
		{
			get { return GetColumnValue<int?>(Columns.ApiProvider); }
			set { SetColumnValue(Columns.ApiProvider, value); }
		}

		[XmlAttribute("CreditcardInfo")]
		[Bindable(true)]
		public string CreditcardInfo
		{
			get { return GetColumnValue<string>(Columns.CreditcardInfo); }
			set { SetColumnValue(Columns.CreditcardInfo, value); }
		}

		[XmlAttribute("BlackListType")]
		[Bindable(true)]
		public int? BlackListType
		{
			get { return GetColumnValue<int?>(Columns.BlackListType); }
			set { SetColumnValue(Columns.BlackListType, value); }
		}

		[XmlAttribute("CreateId")]
		[Bindable(true)]
		public string CreateId
		{
			get { return GetColumnValue<string>(Columns.CreateId); }
			set { SetColumnValue(Columns.CreateId, value); }
		}

		[XmlAttribute("CreditcardInvalidTimes")]
		[Bindable(true)]
		public int? CreditcardInvalidTimes
		{
			get { return GetColumnValue<int?>(Columns.CreditcardInvalidTimes); }
			set { SetColumnValue(Columns.CreditcardInvalidTimes, value); }
		}

		[XmlAttribute("MaskedCardNumber")]
		[Bindable(true)]
		public string MaskedCardNumber
		{
			get { return GetColumnValue<string>(Columns.MaskedCardNumber); }
			set { SetColumnValue(Columns.MaskedCardNumber, value); }
		}

		[XmlAttribute("IsOtp")]
		[Bindable(true)]
		public bool? IsOtp
		{
			get { return GetColumnValue<bool?>(Columns.IsOtp); }
			set { SetColumnValue(Columns.IsOtp, value); }
		}

		#endregion

		#region Typed Columns

		public static TableSchema.TableColumn IdColumn
		{
			get { return Schema.Columns[0]; }
		}

		public static TableSchema.TableColumn MemberIdColumn
		{
			get { return Schema.Columns[1]; }
		}

		public static TableSchema.TableColumn OrderGuidColumn
		{
			get { return Schema.Columns[2]; }
		}

		public static TableSchema.TableColumn OrderClassificationColumn
		{
			get { return Schema.Columns[3]; }
		}

		public static TableSchema.TableColumn CreditcardAmountColumn
		{
			get { return Schema.Columns[4]; }
		}

		public static TableSchema.TableColumn ExpDateColumn
		{
			get { return Schema.Columns[5]; }
		}

		public static TableSchema.TableColumn ResultColumn
		{
			get { return Schema.Columns[6]; }
		}

		public static TableSchema.TableColumn ConsumerIpColumn
		{
			get { return Schema.Columns[7]; }
		}

		public static TableSchema.TableColumn CreateTimeColumn
		{
			get { return Schema.Columns[8]; }
		}

		public static TableSchema.TableColumn IsLockedColumn
		{
			get { return Schema.Columns[9]; }
		}

		public static TableSchema.TableColumn LockerIdColumn
		{
			get { return Schema.Columns[10]; }
		}

		public static TableSchema.TableColumn LockedTimeColumn
		{
			get { return Schema.Columns[11]; }
		}

		public static TableSchema.TableColumn UnlockerIdColumn
		{
			get { return Schema.Columns[12]; }
		}

		public static TableSchema.TableColumn UnlockedTimeColumn
		{
			get { return Schema.Columns[13]; }
		}

		public static TableSchema.TableColumn IsReferenceColumn
		{
			get { return Schema.Columns[14]; }
		}

		public static TableSchema.TableColumn ReferenceIdColumn
		{
			get { return Schema.Columns[15]; }
		}

		public static TableSchema.TableColumn ReferenceTimeColumn
		{
			get { return Schema.Columns[16]; }
		}

		public static TableSchema.TableColumn ApiProviderColumn
		{
			get { return Schema.Columns[17]; }
		}

		public static TableSchema.TableColumn CreditcardInfoColumn
		{
			get { return Schema.Columns[18]; }
		}

		public static TableSchema.TableColumn BlackListTypeColumn
		{
			get { return Schema.Columns[19]; }
		}

		public static TableSchema.TableColumn CreateIdColumn
		{
			get { return Schema.Columns[20]; }
		}

		public static TableSchema.TableColumn CreditcardInvalidTimesColumn
		{
			get { return Schema.Columns[21]; }
		}

		public static TableSchema.TableColumn MaskedCardNumberColumn
		{
			get { return Schema.Columns[22]; }
		}

		public static TableSchema.TableColumn IsOtpColumn
		{
			get { return Schema.Columns[23]; }
		}

		#endregion

		#region Columns Struct

		public struct Columns
		{
			public static string Id = @"id";
			public static string MemberId = @"member_id";
			public static string OrderGuid = @"order_guid";
			public static string OrderClassification = @"order_classification";
			public static string CreditcardAmount = @"creditcard_amount";
			public static string ExpDate = @"exp_date";
			public static string Result = @"result";
			public static string ConsumerIp = @"consumer_ip";
			public static string CreateTime = @"create_time";
			public static string IsLocked = @"is_locked";
			public static string LockerId = @"locker_id";
			public static string LockedTime = @"locked_time";
			public static string UnlockerId = @"unlocker_id";
			public static string UnlockedTime = @"unlocked_time";
			public static string IsReference = @"is_reference";
			public static string ReferenceId = @"reference_id";
			public static string ReferenceTime = @"reference_time";
			public static string ApiProvider = @"api_provider";
			public static string CreditcardInfo = @"creditcard_info";
			public static string BlackListType = @"black_list_type";
			public static string CreateId = @"create_id";
			public static string CreditcardInvalidTimes = @"creditcard_invalid_times";
			public static string MaskedCardNumber = @"masked_card_number";
			public static string IsOtp = @"is_otp";
		}

		#endregion

	}
}

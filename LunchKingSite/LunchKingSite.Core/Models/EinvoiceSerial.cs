using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the EinvoiceSerial class.
    /// </summary>
    [Serializable]
    public partial class EinvoiceSerialCollection : RepositoryList<EinvoiceSerial, EinvoiceSerialCollection>
    {
        public EinvoiceSerialCollection() { }

        /// <summary>
        /// Filters an existing collection based on the set criteria. This is an in-memory filter
        /// Thanks to developingchris for this!
        /// </summary>
        /// <returns>EinvoiceSerialCollection</returns>
        public EinvoiceSerialCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                EinvoiceSerial o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }


    }
    /// <summary>
    /// This is an ActiveRecord class which wraps the einvoice_serial table.
    /// </summary>
    [Serializable]
    public partial class EinvoiceSerial : RepositoryRecord<EinvoiceSerial>, IRecordBase
    {
        #region .ctors and Default Settings

        public EinvoiceSerial()
        {
            SetSQLProps();
            InitSetDefaults();
            MarkNew();
        }

        private void InitSetDefaults() { SetDefaults(); }

        public EinvoiceSerial(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
                ForceDefaults();
            MarkNew();
        }

        protected static void SetSQLProps() { GetTableSchema(); }

        #endregion

        #region Schema and Query Accessor	
        public static Query CreateQuery() { return new Query(Schema); }
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                    SetSQLProps();
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("einvoice_serial", TableType.Table, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = true;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = true;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                colvarId.DefaultSetting = @"";
                colvarId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarId);

                TableSchema.TableColumn colvarHeadCode = new TableSchema.TableColumn(schema);
                colvarHeadCode.ColumnName = "head_code";
                colvarHeadCode.DataType = DbType.String;
                colvarHeadCode.MaxLength = 2;
                colvarHeadCode.AutoIncrement = false;
                colvarHeadCode.IsNullable = false;
                colvarHeadCode.IsPrimaryKey = false;
                colvarHeadCode.IsForeignKey = false;
                colvarHeadCode.IsReadOnly = false;
                colvarHeadCode.DefaultSetting = @"";
                colvarHeadCode.ForeignKeyTableName = "";
                schema.Columns.Add(colvarHeadCode);

                TableSchema.TableColumn colvarMinSerial = new TableSchema.TableColumn(schema);
                colvarMinSerial.ColumnName = "min_serial";
                colvarMinSerial.DataType = DbType.Int32;
                colvarMinSerial.MaxLength = 0;
                colvarMinSerial.AutoIncrement = false;
                colvarMinSerial.IsNullable = false;
                colvarMinSerial.IsPrimaryKey = false;
                colvarMinSerial.IsForeignKey = false;
                colvarMinSerial.IsReadOnly = false;
                colvarMinSerial.DefaultSetting = @"";
                colvarMinSerial.ForeignKeyTableName = "";
                schema.Columns.Add(colvarMinSerial);

                TableSchema.TableColumn colvarMaxSerial = new TableSchema.TableColumn(schema);
                colvarMaxSerial.ColumnName = "max_serial";
                colvarMaxSerial.DataType = DbType.Int32;
                colvarMaxSerial.MaxLength = 0;
                colvarMaxSerial.AutoIncrement = false;
                colvarMaxSerial.IsNullable = false;
                colvarMaxSerial.IsPrimaryKey = false;
                colvarMaxSerial.IsForeignKey = false;
                colvarMaxSerial.IsReadOnly = false;
                colvarMaxSerial.DefaultSetting = @"";
                colvarMaxSerial.ForeignKeyTableName = "";
                schema.Columns.Add(colvarMaxSerial);

                TableSchema.TableColumn colvarNextSerial = new TableSchema.TableColumn(schema);
                colvarNextSerial.ColumnName = "next_serial";
                colvarNextSerial.DataType = DbType.Int32;
                colvarNextSerial.MaxLength = 0;
                colvarNextSerial.AutoIncrement = false;
                colvarNextSerial.IsNullable = false;
                colvarNextSerial.IsPrimaryKey = false;
                colvarNextSerial.IsForeignKey = false;
                colvarNextSerial.IsReadOnly = false;
                colvarNextSerial.DefaultSetting = @"";
                colvarNextSerial.ForeignKeyTableName = "";
                schema.Columns.Add(colvarNextSerial);

                TableSchema.TableColumn colvarStartDate = new TableSchema.TableColumn(schema);
                colvarStartDate.ColumnName = "start_date";
                colvarStartDate.DataType = DbType.DateTime;
                colvarStartDate.MaxLength = 0;
                colvarStartDate.AutoIncrement = false;
                colvarStartDate.IsNullable = false;
                colvarStartDate.IsPrimaryKey = false;
                colvarStartDate.IsForeignKey = false;
                colvarStartDate.IsReadOnly = false;
                colvarStartDate.DefaultSetting = @"";
                colvarStartDate.ForeignKeyTableName = "";
                schema.Columns.Add(colvarStartDate);

                TableSchema.TableColumn colvarEndDate = new TableSchema.TableColumn(schema);
                colvarEndDate.ColumnName = "end_date";
                colvarEndDate.DataType = DbType.DateTime;
                colvarEndDate.MaxLength = 0;
                colvarEndDate.AutoIncrement = false;
                colvarEndDate.IsNullable = false;
                colvarEndDate.IsPrimaryKey = false;
                colvarEndDate.IsForeignKey = false;
                colvarEndDate.IsReadOnly = false;
                colvarEndDate.DefaultSetting = @"";
                colvarEndDate.ForeignKeyTableName = "";
                schema.Columns.Add(colvarEndDate);

                TableSchema.TableColumn colvarDateCode = new TableSchema.TableColumn(schema);
                colvarDateCode.ColumnName = "date_code";
                colvarDateCode.DataType = DbType.AnsiString;
                colvarDateCode.MaxLength = 6;
                colvarDateCode.AutoIncrement = false;
                colvarDateCode.IsNullable = false;
                colvarDateCode.IsPrimaryKey = false;
                colvarDateCode.IsForeignKey = false;
                colvarDateCode.IsReadOnly = false;
                colvarDateCode.DefaultSetting = @"";
                colvarDateCode.ForeignKeyTableName = "";
                schema.Columns.Add(colvarDateCode);

                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = false;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                colvarCreateTime.DefaultSetting = @"";
                colvarCreateTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateTime);

                TableSchema.TableColumn colvarCreator = new TableSchema.TableColumn(schema);
                colvarCreator.ColumnName = "creator";
                colvarCreator.DataType = DbType.String;
                colvarCreator.MaxLength = 50;
                colvarCreator.AutoIncrement = false;
                colvarCreator.IsNullable = false;
                colvarCreator.IsPrimaryKey = false;
                colvarCreator.IsForeignKey = false;
                colvarCreator.IsReadOnly = false;
                colvarCreator.DefaultSetting = @"";
                colvarCreator.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreator);

                TableSchema.TableColumn colvarMessage = new TableSchema.TableColumn(schema);
                colvarMessage.ColumnName = "message";
                colvarMessage.DataType = DbType.String;
                colvarMessage.MaxLength = 250;
                colvarMessage.AutoIncrement = false;
                colvarMessage.IsNullable = true;
                colvarMessage.IsPrimaryKey = false;
                colvarMessage.IsForeignKey = false;
                colvarMessage.IsReadOnly = false;
                colvarMessage.DefaultSetting = @"";
                colvarMessage.ForeignKeyTableName = "";
                schema.Columns.Add(colvarMessage);

                TableSchema.TableColumn colvarVendorNextSerial = new TableSchema.TableColumn(schema);
                colvarVendorNextSerial.ColumnName = "vendor_next_serial";
                colvarVendorNextSerial.DataType = DbType.Int32;
                colvarVendorNextSerial.MaxLength = 0;
                colvarVendorNextSerial.AutoIncrement = false;
                colvarVendorNextSerial.IsNullable = true;
                colvarVendorNextSerial.IsPrimaryKey = false;
                colvarVendorNextSerial.IsForeignKey = false;
                colvarVendorNextSerial.IsReadOnly = false;
                colvarVendorNextSerial.DefaultSetting = @"";
                colvarVendorNextSerial.ForeignKeyTableName = "";
                schema.Columns.Add(colvarVendorNextSerial);

                TableSchema.TableColumn colvarVendorMinSerial = new TableSchema.TableColumn(schema);
                colvarVendorMinSerial.ColumnName = "vendor_min_serial";
                colvarVendorMinSerial.DataType = DbType.Int32;
                colvarVendorMinSerial.MaxLength = 0;
                colvarVendorMinSerial.AutoIncrement = false;
                colvarVendorMinSerial.IsNullable = true;
                colvarVendorMinSerial.IsPrimaryKey = false;
                colvarVendorMinSerial.IsForeignKey = false;
                colvarVendorMinSerial.IsReadOnly = false;
                colvarVendorMinSerial.DefaultSetting = @"";
                colvarVendorMinSerial.ForeignKeyTableName = "";
                schema.Columns.Add(colvarVendorMinSerial);

                TableSchema.TableColumn colvarWinnerMailSendStatus = new TableSchema.TableColumn(schema);
                colvarWinnerMailSendStatus.ColumnName = "winner_mail_send_status";
                colvarWinnerMailSendStatus.DataType = DbType.Int32;
                colvarWinnerMailSendStatus.MaxLength = 0;
                colvarWinnerMailSendStatus.AutoIncrement = false;
                colvarWinnerMailSendStatus.IsNullable = false;
                colvarWinnerMailSendStatus.IsPrimaryKey = false;
                colvarWinnerMailSendStatus.IsForeignKey = false;
                colvarWinnerMailSendStatus.IsReadOnly = false;

                colvarWinnerMailSendStatus.DefaultSetting = @"((2))";
                colvarWinnerMailSendStatus.ForeignKeyTableName = "";
                schema.Columns.Add(colvarWinnerMailSendStatus);

                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("einvoice_serial", schema);
            }
        }
        #endregion

        #region Props

        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id
        {
            get { return GetColumnValue<int>(Columns.Id); }
            set { SetColumnValue(Columns.Id, value); }
        }

        [XmlAttribute("HeadCode")]
        [Bindable(true)]
        public string HeadCode
        {
            get { return GetColumnValue<string>(Columns.HeadCode); }
            set { SetColumnValue(Columns.HeadCode, value); }
        }

        [XmlAttribute("MinSerial")]
        [Bindable(true)]
        public int MinSerial
        {
            get { return GetColumnValue<int>(Columns.MinSerial); }
            set { SetColumnValue(Columns.MinSerial, value); }
        }

        [XmlAttribute("MaxSerial")]
        [Bindable(true)]
        public int MaxSerial
        {
            get { return GetColumnValue<int>(Columns.MaxSerial); }
            set { SetColumnValue(Columns.MaxSerial, value); }
        }

        [XmlAttribute("NextSerial")]
        [Bindable(true)]
        public int NextSerial
        {
            get { return GetColumnValue<int>(Columns.NextSerial); }
            set { SetColumnValue(Columns.NextSerial, value); }
        }

        [XmlAttribute("StartDate")]
        [Bindable(true)]
        public DateTime StartDate
        {
            get { return GetColumnValue<DateTime>(Columns.StartDate); }
            set { SetColumnValue(Columns.StartDate, value); }
        }

        [XmlAttribute("EndDate")]
        [Bindable(true)]
        public DateTime EndDate
        {
            get { return GetColumnValue<DateTime>(Columns.EndDate); }
            set { SetColumnValue(Columns.EndDate, value); }
        }

        [XmlAttribute("DateCode")]
        [Bindable(true)]
        public string DateCode
        {
            get { return GetColumnValue<string>(Columns.DateCode); }
            set { SetColumnValue(Columns.DateCode, value); }
        }

        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime CreateTime
        {
            get { return GetColumnValue<DateTime>(Columns.CreateTime); }
            set { SetColumnValue(Columns.CreateTime, value); }
        }

        [XmlAttribute("Creator")]
        [Bindable(true)]
        public string Creator
        {
            get { return GetColumnValue<string>(Columns.Creator); }
            set { SetColumnValue(Columns.Creator, value); }
        }

        [XmlAttribute("Message")]
        [Bindable(true)]
        public string Message
        {
            get { return GetColumnValue<string>(Columns.Message); }
            set { SetColumnValue(Columns.Message, value); }
        }

        [XmlAttribute("VendorNextSerial")]
        [Bindable(true)]
        public int? VendorNextSerial
        {
            get { return GetColumnValue<int?>(Columns.VendorNextSerial); }
            set { SetColumnValue(Columns.VendorNextSerial, value); }
        }

        [XmlAttribute("VendorMinSerial")]
        [Bindable(true)]
        public int? VendorMinSerial
        {
            get { return GetColumnValue<int?>(Columns.VendorMinSerial); }
            set { SetColumnValue(Columns.VendorMinSerial, value); }
        }

        [XmlAttribute("WinnerMailSendStatus")]
        [Bindable(true)]
        public int WinnerMailSendStatus
        {
            get { return GetColumnValue<int>(Columns.WinnerMailSendStatus); }
            set { SetColumnValue(Columns.WinnerMailSendStatus, value); }
        }

        #endregion




        //no foreign key tables defined (0)



        //no ManyToMany tables defined (0)





        #region Typed Columns


        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }



        public static TableSchema.TableColumn HeadCodeColumn
        {
            get { return Schema.Columns[1]; }
        }



        public static TableSchema.TableColumn MinSerialColumn
        {
            get { return Schema.Columns[2]; }
        }



        public static TableSchema.TableColumn MaxSerialColumn
        {
            get { return Schema.Columns[3]; }
        }



        public static TableSchema.TableColumn NextSerialColumn
        {
            get { return Schema.Columns[4]; }
        }



        public static TableSchema.TableColumn StartDateColumn
        {
            get { return Schema.Columns[5]; }
        }



        public static TableSchema.TableColumn EndDateColumn
        {
            get { return Schema.Columns[6]; }
        }



        public static TableSchema.TableColumn DateCodeColumn
        {
            get { return Schema.Columns[7]; }
        }



        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[8]; }
        }



        public static TableSchema.TableColumn CreatorColumn
        {
            get { return Schema.Columns[9]; }
        }



        public static TableSchema.TableColumn MessageColumn
        {
            get { return Schema.Columns[10]; }
        }



        public static TableSchema.TableColumn VendorNextSerialColumn
        {
            get { return Schema.Columns[11]; }
        }



        public static TableSchema.TableColumn VendorMinSerialColumn
        {
            get { return Schema.Columns[12]; }
        }



        public static TableSchema.TableColumn WinnerMailSendStatusColumn
        {
            get { return Schema.Columns[13]; }
        }



        #endregion
        #region Columns Struct
        public struct Columns
        {
            public static string Id = @"id";
            public static string HeadCode = @"head_code";
            public static string MinSerial = @"min_serial";
            public static string MaxSerial = @"max_serial";
            public static string NextSerial = @"next_serial";
            public static string StartDate = @"start_date";
            public static string EndDate = @"end_date";
            public static string DateCode = @"date_code";
            public static string CreateTime = @"create_time";
            public static string Creator = @"creator";
            public static string Message = @"message";
            public static string VendorNextSerial = @"vendor_next_serial";
            public static string VendorMinSerial = @"vendor_min_serial";
            public static string WinnerMailSendStatus = @"winner_mail_send_status";

        }
        #endregion

        #region Update PK Collections

        #endregion

        #region Deep Save

        #endregion
    }
}

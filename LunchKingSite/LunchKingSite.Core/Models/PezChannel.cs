using System;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using SubSonic;

namespace LunchKingSite.DataOrm
{
	[Serializable]
	public partial class PezChannelCollection : RepositoryList<PezChannel, PezChannelCollection>
	{
			public PezChannelCollection() {}

			public PezChannelCollection Filter()
			{
					for (int i = this.Count - 1; i > -1; i--)
					{
							PezChannel o = this[i];
							foreach (SubSonic.Where w in this.wheres)
							{
									bool remove = false;
									System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
									if(pi.CanRead)
									{
											object val = pi.GetValue(o, null);
											switch(w.Comparison)
											{
													case SubSonic.Comparison.Equals:
															if(!val.Equals(w.ParameterValue))
															{
																	remove = true;
															}
															break;
											}
									}
									if(remove)
									{
											this.Remove(o);
											break;
									}
							}
					}
					return this;
			}
	}

	[Serializable]
	public partial class PezChannel : RepositoryRecord<PezChannel>, IRecordBase
	{
		#region .ctors and Default Settings
		public PezChannel()
		{
			SetSQLProps();
			InitSetDefaults();
			MarkNew();
		}

		private void InitSetDefaults() { SetDefaults(); }

		public PezChannel(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}

		protected static void SetSQLProps() { GetTableSchema(); }

		#endregion

		#region Schema and Query Accessor
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}

		private static void GetTableSchema()
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("pez_channel", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);

				TableSchema.TableColumn colvarTrustId = new TableSchema.TableColumn(schema);
				colvarTrustId.ColumnName = "trust_id";
				colvarTrustId.DataType = DbType.Guid;
				colvarTrustId.MaxLength = 0;
				colvarTrustId.AutoIncrement = false;
				colvarTrustId.IsNullable = false;
				colvarTrustId.IsPrimaryKey = false;
				colvarTrustId.IsForeignKey = false;
				colvarTrustId.IsReadOnly = false;
				colvarTrustId.DefaultSetting = @"";
				colvarTrustId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTrustId);

				TableSchema.TableColumn colvarOrderGuid = new TableSchema.TableColumn(schema);
				colvarOrderGuid.ColumnName = "order_guid";
				colvarOrderGuid.DataType = DbType.Guid;
				colvarOrderGuid.MaxLength = 0;
				colvarOrderGuid.AutoIncrement = false;
				colvarOrderGuid.IsNullable = false;
				colvarOrderGuid.IsPrimaryKey = false;
				colvarOrderGuid.IsForeignKey = false;
				colvarOrderGuid.IsReadOnly = false;
				colvarOrderGuid.DefaultSetting = @"";
				colvarOrderGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderGuid);

				TableSchema.TableColumn colvarBid = new TableSchema.TableColumn(schema);
				colvarBid.ColumnName = "bid";
				colvarBid.DataType = DbType.Guid;
				colvarBid.MaxLength = 0;
				colvarBid.AutoIncrement = false;
				colvarBid.IsNullable = false;
				colvarBid.IsPrimaryKey = false;
				colvarBid.IsForeignKey = false;
				colvarBid.IsReadOnly = false;
				colvarBid.DefaultSetting = @"";
				colvarBid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBid);

				TableSchema.TableColumn colvarDeliveryType = new TableSchema.TableColumn(schema);
				colvarDeliveryType.ColumnName = "delivery_type";
				colvarDeliveryType.DataType = DbType.Int32;
				colvarDeliveryType.MaxLength = 0;
				colvarDeliveryType.AutoIncrement = false;
				colvarDeliveryType.IsNullable = false;
				colvarDeliveryType.IsPrimaryKey = false;
				colvarDeliveryType.IsForeignKey = false;
				colvarDeliveryType.IsReadOnly = false;
				colvarDeliveryType.DefaultSetting = @"";
				colvarDeliveryType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDeliveryType);

				TableSchema.TableColumn colvarGid = new TableSchema.TableColumn(schema);
				colvarGid.ColumnName = "gid";
				colvarGid.DataType = DbType.AnsiString;
				colvarGid.MaxLength = 100;
				colvarGid.AutoIncrement = false;
				colvarGid.IsNullable = false;
				colvarGid.IsPrimaryKey = false;
				colvarGid.IsForeignKey = false;
				colvarGid.IsReadOnly = false;
				colvarGid.DefaultSetting = @"('')";
				colvarGid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGid);

				TableSchema.TableColumn colvarAmount = new TableSchema.TableColumn(schema);
				colvarAmount.ColumnName = "amount";
				colvarAmount.DataType = DbType.Currency;
				colvarAmount.MaxLength = 0;
				colvarAmount.AutoIncrement = false;
				colvarAmount.IsNullable = false;
				colvarAmount.IsPrimaryKey = false;
				colvarAmount.IsForeignKey = false;
				colvarAmount.IsReadOnly = false;
				colvarAmount.DefaultSetting = @"";
				colvarAmount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAmount);

				TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
				colvarModifyTime.ColumnName = "modify_time";
				colvarModifyTime.DataType = DbType.DateTime;
				colvarModifyTime.MaxLength = 0;
				colvarModifyTime.AutoIncrement = false;
				colvarModifyTime.IsNullable = true;
				colvarModifyTime.IsPrimaryKey = false;
				colvarModifyTime.IsForeignKey = false;
				colvarModifyTime.IsReadOnly = false;
				colvarModifyTime.DefaultSetting = @"";
				colvarModifyTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyTime);

				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"(getdate())";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);

				TableSchema.TableColumn colvarSendCount = new TableSchema.TableColumn(schema);
				colvarSendCount.ColumnName = "send_count";
				colvarSendCount.DataType = DbType.Int32;
				colvarSendCount.MaxLength = 0;
				colvarSendCount.AutoIncrement = false;
				colvarSendCount.IsNullable = false;
				colvarSendCount.IsPrimaryKey = false;
				colvarSendCount.IsForeignKey = false;
				colvarSendCount.IsReadOnly = false;
				colvarSendCount.DefaultSetting = @"((0))";
				colvarSendCount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSendCount);

				TableSchema.TableColumn colvarSendType = new TableSchema.TableColumn(schema);
				colvarSendType.ColumnName = "send_type";
				colvarSendType.DataType = DbType.Int32;
				colvarSendType.MaxLength = 0;
				colvarSendType.AutoIncrement = false;
				colvarSendType.IsNullable = false;
				colvarSendType.IsPrimaryKey = false;
				colvarSendType.IsForeignKey = false;
				colvarSendType.IsReadOnly = false;
				colvarSendType.DefaultSetting = @"((0))";
				colvarSendType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSendType);

				TableSchema.TableColumn colvarReturnCode = new TableSchema.TableColumn(schema);
				colvarReturnCode.ColumnName = "return_code";
				colvarReturnCode.DataType = DbType.AnsiString;
				colvarReturnCode.MaxLength = 20;
				colvarReturnCode.AutoIncrement = false;
				colvarReturnCode.IsNullable = true;
				colvarReturnCode.IsPrimaryKey = false;
				colvarReturnCode.IsForeignKey = false;
				colvarReturnCode.IsReadOnly = false;
				colvarReturnCode.DefaultSetting = @"";
				colvarReturnCode.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReturnCode);

				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("pez_channel",schema);
			}
		}
		#endregion

		#region Props

		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}

		[XmlAttribute("TrustId")]
		[Bindable(true)]
		public Guid TrustId
		{
			get { return GetColumnValue<Guid>(Columns.TrustId); }
			set { SetColumnValue(Columns.TrustId, value); }
		}

		[XmlAttribute("OrderGuid")]
		[Bindable(true)]
		public Guid OrderGuid
		{
			get { return GetColumnValue<Guid>(Columns.OrderGuid); }
			set { SetColumnValue(Columns.OrderGuid, value); }
		}

		[XmlAttribute("Bid")]
		[Bindable(true)]
		public Guid Bid
		{
			get { return GetColumnValue<Guid>(Columns.Bid); }
			set { SetColumnValue(Columns.Bid, value); }
		}

		[XmlAttribute("DeliveryType")]
		[Bindable(true)]
		public int DeliveryType
		{
			get { return GetColumnValue<int>(Columns.DeliveryType); }
			set { SetColumnValue(Columns.DeliveryType, value); }
		}

		[XmlAttribute("Gid")]
		[Bindable(true)]
		public string Gid
		{
			get { return GetColumnValue<string>(Columns.Gid); }
			set { SetColumnValue(Columns.Gid, value); }
		}

		[XmlAttribute("Amount")]
		[Bindable(true)]
		public decimal Amount
		{
			get { return GetColumnValue<decimal>(Columns.Amount); }
			set { SetColumnValue(Columns.Amount, value); }
		}

		[XmlAttribute("ModifyTime")]
		[Bindable(true)]
		public DateTime? ModifyTime
		{
			get { return GetColumnValue<DateTime?>(Columns.ModifyTime); }
			set { SetColumnValue(Columns.ModifyTime, value); }
		}

		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}

		[XmlAttribute("SendCount")]
		[Bindable(true)]
		public int SendCount
		{
			get { return GetColumnValue<int>(Columns.SendCount); }
			set { SetColumnValue(Columns.SendCount, value); }
		}

		[XmlAttribute("SendType")]
		[Bindable(true)]
		public int SendType
		{
			get { return GetColumnValue<int>(Columns.SendType); }
			set { SetColumnValue(Columns.SendType, value); }
		}

		[XmlAttribute("ReturnCode")]
		[Bindable(true)]
		public string ReturnCode
		{
			get { return GetColumnValue<string>(Columns.ReturnCode); }
			set { SetColumnValue(Columns.ReturnCode, value); }
		}

		#endregion

		#region Typed Columns

		public static TableSchema.TableColumn IdColumn
		{
			get { return Schema.Columns[0]; }
		}

		public static TableSchema.TableColumn TrustIdColumn
		{
			get { return Schema.Columns[1]; }
		}

		public static TableSchema.TableColumn OrderGuidColumn
		{
			get { return Schema.Columns[2]; }
		}

		public static TableSchema.TableColumn BidColumn
		{
			get { return Schema.Columns[3]; }
		}

		public static TableSchema.TableColumn DeliveryTypeColumn
		{
			get { return Schema.Columns[4]; }
		}

		public static TableSchema.TableColumn GidColumn
		{
			get { return Schema.Columns[5]; }
		}

		public static TableSchema.TableColumn AmountColumn
		{
			get { return Schema.Columns[6]; }
		}

		public static TableSchema.TableColumn ModifyTimeColumn
		{
			get { return Schema.Columns[7]; }
		}

		public static TableSchema.TableColumn CreateTimeColumn
		{
			get { return Schema.Columns[8]; }
		}

		public static TableSchema.TableColumn SendCountColumn
		{
			get { return Schema.Columns[9]; }
		}

		public static TableSchema.TableColumn SendTypeColumn
		{
			get { return Schema.Columns[10]; }
		}

		public static TableSchema.TableColumn ReturnCodeColumn
		{
			get { return Schema.Columns[11]; }
		}

		#endregion

		#region Columns Struct

		public struct Columns
		{
			public static string Id = @"id";
			public static string TrustId = @"trust_id";
			public static string OrderGuid = @"order_guid";
			public static string Bid = @"bid";
			public static string DeliveryType = @"delivery_type";
			public static string Gid = @"gid";
			public static string Amount = @"amount";
			public static string ModifyTime = @"modify_time";
			public static string CreateTime = @"create_time";
			public static string SendCount = @"send_count";
			public static string SendType = @"send_type";
			public static string ReturnCode = @"return_code";
		}

		#endregion

	}
}

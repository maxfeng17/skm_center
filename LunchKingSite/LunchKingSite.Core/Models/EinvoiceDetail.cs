using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the EinvoiceDetail class.
    /// </summary>
    [Serializable]
    public partial class EinvoiceDetailCollection : RepositoryList<EinvoiceDetail, EinvoiceDetailCollection>
    {
        public EinvoiceDetailCollection() {}

        /// <summary>
        /// Filters an existing collection based on the set criteria. This is an in-memory filter
        /// Thanks to developingchris for this!
        /// </summary>
        /// <returns>EinvoiceDetailCollection</returns>
        public EinvoiceDetailCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                EinvoiceDetail o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }


    }
    /// <summary>
    /// This is an ActiveRecord class which wraps the einvoice_detail table.
    /// </summary>
    [Serializable]
    public partial class EinvoiceDetail : RepositoryRecord<EinvoiceDetail>, IRecordBase
    {
        #region .ctors and Default Settings

        public EinvoiceDetail()
        {
            SetSQLProps();
            InitSetDefaults();
            MarkNew();
        }

        private void InitSetDefaults() { SetDefaults(); }

        public EinvoiceDetail(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if(useDatabaseDefaults)
                ForceDefaults();
            MarkNew();
        }

        protected static void SetSQLProps() { GetTableSchema(); }

        #endregion

        #region Schema and Query Accessor	
        public static Query CreateQuery() { return new Query(Schema); }
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                    SetSQLProps();
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("einvoice_detail", TableType.Table, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = true;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = true;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                colvarId.DefaultSetting = @"";
                colvarId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarId);

                TableSchema.TableColumn colvarMainId = new TableSchema.TableColumn(schema);
                colvarMainId.ColumnName = "main_id";
                colvarMainId.DataType = DbType.Int32;
                colvarMainId.MaxLength = 0;
                colvarMainId.AutoIncrement = false;
                colvarMainId.IsNullable = false;
                colvarMainId.IsPrimaryKey = false;
                colvarMainId.IsForeignKey = false;
                colvarMainId.IsReadOnly = false;
                colvarMainId.DefaultSetting = @"";
                colvarMainId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarMainId);

                TableSchema.TableColumn colvarInvoiceAmount = new TableSchema.TableColumn(schema);
                colvarInvoiceAmount.ColumnName = "invoice_amount";
                colvarInvoiceAmount.DataType = DbType.Currency;
                colvarInvoiceAmount.MaxLength = 0;
                colvarInvoiceAmount.AutoIncrement = false;
                colvarInvoiceAmount.IsNullable = false;
                colvarInvoiceAmount.IsPrimaryKey = false;
                colvarInvoiceAmount.IsForeignKey = false;
                colvarInvoiceAmount.IsReadOnly = false;
                colvarInvoiceAmount.DefaultSetting = @"";
                colvarInvoiceAmount.ForeignKeyTableName = "";
                schema.Columns.Add(colvarInvoiceAmount);

                TableSchema.TableColumn colvarInvoiceType = new TableSchema.TableColumn(schema);
                colvarInvoiceType.ColumnName = "invoice_type";
                colvarInvoiceType.DataType = DbType.Int32;
                colvarInvoiceType.MaxLength = 0;
                colvarInvoiceType.AutoIncrement = false;
                colvarInvoiceType.IsNullable = false;
                colvarInvoiceType.IsPrimaryKey = false;
                colvarInvoiceType.IsForeignKey = false;
                colvarInvoiceType.IsReadOnly = false;
                colvarInvoiceType.DefaultSetting = @"";
                colvarInvoiceType.ForeignKeyTableName = "";
                schema.Columns.Add(colvarInvoiceType);

                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = false;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                colvarCreateTime.DefaultSetting = @"";
                colvarCreateTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateTime);

                TableSchema.TableColumn colvarCreator = new TableSchema.TableColumn(schema);
                colvarCreator.ColumnName = "creator";
                colvarCreator.DataType = DbType.String;
                colvarCreator.MaxLength = 50;
                colvarCreator.AutoIncrement = false;
                colvarCreator.IsNullable = false;
                colvarCreator.IsPrimaryKey = false;
                colvarCreator.IsForeignKey = false;
                colvarCreator.IsReadOnly = false;
                colvarCreator.DefaultSetting = @"";
                colvarCreator.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreator);

                TableSchema.TableColumn colvarMessage = new TableSchema.TableColumn(schema);
                colvarMessage.ColumnName = "message";
                colvarMessage.DataType = DbType.String;
                colvarMessage.MaxLength = 250;
                colvarMessage.AutoIncrement = false;
                colvarMessage.IsNullable = true;
                colvarMessage.IsPrimaryKey = false;
                colvarMessage.IsForeignKey = false;
                colvarMessage.IsReadOnly = false;
                colvarMessage.DefaultSetting = @"";
                colvarMessage.ForeignKeyTableName = "";
                schema.Columns.Add(colvarMessage);

                TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
                colvarStatus.ColumnName = "status";
                colvarStatus.DataType = DbType.Boolean;
                colvarStatus.MaxLength = 0;
                colvarStatus.AutoIncrement = false;
                colvarStatus.IsNullable = false;
                colvarStatus.IsPrimaryKey = false;
                colvarStatus.IsForeignKey = false;
                colvarStatus.IsReadOnly = false;

                colvarStatus.DefaultSetting = @"((0))";
                colvarStatus.ForeignKeyTableName = "";
                schema.Columns.Add(colvarStatus);

                TableSchema.TableColumn colvarInvoicedTime = new TableSchema.TableColumn(schema);
                colvarInvoicedTime.ColumnName = "invoiced_time";
                colvarInvoicedTime.DataType = DbType.DateTime;
                colvarInvoicedTime.MaxLength = 0;
                colvarInvoicedTime.AutoIncrement = false;
                colvarInvoicedTime.IsNullable = true;
                colvarInvoicedTime.IsPrimaryKey = false;
                colvarInvoicedTime.IsForeignKey = false;
                colvarInvoicedTime.IsReadOnly = false;
                colvarInvoicedTime.DefaultSetting = @"";
                colvarInvoicedTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarInvoicedTime);

                TableSchema.TableColumn colvarPaymentTransId = new TableSchema.TableColumn(schema);
                colvarPaymentTransId.ColumnName = "payment_trans_id";
                colvarPaymentTransId.DataType = DbType.Int32;
                colvarPaymentTransId.MaxLength = 0;
                colvarPaymentTransId.AutoIncrement = false;
                colvarPaymentTransId.IsNullable = true;
                colvarPaymentTransId.IsPrimaryKey = false;
                colvarPaymentTransId.IsForeignKey = false;
                colvarPaymentTransId.IsReadOnly = false;
                colvarPaymentTransId.DefaultSetting = @"";
                colvarPaymentTransId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarPaymentTransId);

                TableSchema.TableColumn colvarTransTime = new TableSchema.TableColumn(schema);
                colvarTransTime.ColumnName = "trans_time";
                colvarTransTime.DataType = DbType.DateTime;
                colvarTransTime.MaxLength = 0;
                colvarTransTime.AutoIncrement = false;
                colvarTransTime.IsNullable = true;
                colvarTransTime.IsPrimaryKey = false;
                colvarTransTime.IsForeignKey = false;
                colvarTransTime.IsReadOnly = false;
                colvarTransTime.DefaultSetting = @"";
                colvarTransTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarTransTime);

                TableSchema.TableColumn colvarInvoiceFileSerial = new TableSchema.TableColumn(schema);
                colvarInvoiceFileSerial.ColumnName = "invoice_file_serial";
                colvarInvoiceFileSerial.DataType = DbType.Int32;
                colvarInvoiceFileSerial.MaxLength = 0;
                colvarInvoiceFileSerial.AutoIncrement = false;
                colvarInvoiceFileSerial.IsNullable = true;
                colvarInvoiceFileSerial.IsPrimaryKey = false;
                colvarInvoiceFileSerial.IsForeignKey = false;
                colvarInvoiceFileSerial.IsReadOnly = false;
                colvarInvoiceFileSerial.DefaultSetting = @"";
                colvarInvoiceFileSerial.ForeignKeyTableName = "";
                schema.Columns.Add(colvarInvoiceFileSerial);

                TableSchema.TableColumn colvarTrustId = new TableSchema.TableColumn(schema);
                colvarTrustId.ColumnName = "trust_id";
                colvarTrustId.DataType = DbType.Guid;
                colvarTrustId.MaxLength = 0;
                colvarTrustId.AutoIncrement = false;
                colvarTrustId.IsNullable = true;
                colvarTrustId.IsPrimaryKey = false;
                colvarTrustId.IsForeignKey = false;
                colvarTrustId.IsReadOnly = false;
                colvarTrustId.DefaultSetting = @"";
                colvarTrustId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarTrustId);

                TableSchema.TableColumn colvarReturnFormId = new TableSchema.TableColumn(schema);
                colvarReturnFormId.ColumnName = "return_form_id";
                colvarReturnFormId.DataType = DbType.Int32;
                colvarReturnFormId.MaxLength = 0;
                colvarReturnFormId.AutoIncrement = false;
                colvarReturnFormId.IsNullable = true;
                colvarReturnFormId.IsPrimaryKey = false;
                colvarReturnFormId.IsForeignKey = false;
                colvarReturnFormId.IsReadOnly = false;
                colvarReturnFormId.DefaultSetting = @"";
                colvarReturnFormId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarReturnFormId);

                TableSchema.TableColumn colvarAllowanceNumber = new TableSchema.TableColumn(schema);
                colvarAllowanceNumber.ColumnName = "allowance_number";
                colvarAllowanceNumber.DataType = DbType.String;
                colvarAllowanceNumber.MaxLength = 16;
                colvarAllowanceNumber.AutoIncrement = false;
                colvarAllowanceNumber.IsNullable = true;
                colvarAllowanceNumber.IsPrimaryKey = false;
                colvarAllowanceNumber.IsForeignKey = false;
                colvarAllowanceNumber.IsReadOnly = false;
                colvarAllowanceNumber.DefaultSetting = @"";
                colvarAllowanceNumber.ForeignKeyTableName = "";
                schema.Columns.Add(colvarAllowanceNumber);

                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("einvoice_detail",schema);
            }
        }
        #endregion

        #region Props

        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id
        {
            get { return GetColumnValue<int>(Columns.Id); }
            set { SetColumnValue(Columns.Id, value); }
        }

        [XmlAttribute("MainId")]
        [Bindable(true)]
        public int MainId
        {
            get { return GetColumnValue<int>(Columns.MainId); }
            set { SetColumnValue(Columns.MainId, value); }
        }

        [XmlAttribute("InvoiceAmount")]
        [Bindable(true)]
        public decimal InvoiceAmount
        {
            get { return GetColumnValue<decimal>(Columns.InvoiceAmount); }
            set { SetColumnValue(Columns.InvoiceAmount, value); }
        }

        [XmlAttribute("InvoiceType")]
        [Bindable(true)]
        public int InvoiceType
        {
            get { return GetColumnValue<int>(Columns.InvoiceType); }
            set { SetColumnValue(Columns.InvoiceType, value); }
        }

        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime CreateTime
        {
            get { return GetColumnValue<DateTime>(Columns.CreateTime); }
            set { SetColumnValue(Columns.CreateTime, value); }
        }

        [XmlAttribute("Creator")]
        [Bindable(true)]
        public string Creator
        {
            get { return GetColumnValue<string>(Columns.Creator); }
            set { SetColumnValue(Columns.Creator, value); }
        }

        [XmlAttribute("Message")]
        [Bindable(true)]
        public string Message
        {
            get { return GetColumnValue<string>(Columns.Message); }
            set { SetColumnValue(Columns.Message, value); }
        }

        [XmlAttribute("Status")]
        [Bindable(true)]
        public bool Status
        {
            get { return GetColumnValue<bool>(Columns.Status); }
            set { SetColumnValue(Columns.Status, value); }
        }

        [XmlAttribute("InvoicedTime")]
        [Bindable(true)]
        public DateTime? InvoicedTime
        {
            get { return GetColumnValue<DateTime?>(Columns.InvoicedTime); }
            set { SetColumnValue(Columns.InvoicedTime, value); }
        }

        [XmlAttribute("PaymentTransId")]
        [Bindable(true)]
        public int? PaymentTransId
        {
            get { return GetColumnValue<int?>(Columns.PaymentTransId); }
            set { SetColumnValue(Columns.PaymentTransId, value); }
        }

        [XmlAttribute("TransTime")]
        [Bindable(true)]
        public DateTime? TransTime
        {
            get { return GetColumnValue<DateTime?>(Columns.TransTime); }
            set { SetColumnValue(Columns.TransTime, value); }
        }

        [XmlAttribute("InvoiceFileSerial")]
        [Bindable(true)]
        public int? InvoiceFileSerial
        {
            get { return GetColumnValue<int?>(Columns.InvoiceFileSerial); }
            set { SetColumnValue(Columns.InvoiceFileSerial, value); }
        }

        [XmlAttribute("TrustId")]
        [Bindable(true)]
        public Guid? TrustId
        {
            get { return GetColumnValue<Guid?>(Columns.TrustId); }
            set { SetColumnValue(Columns.TrustId, value); }
        }

        [XmlAttribute("ReturnFormId")]
        [Bindable(true)]
        public int? ReturnFormId
        {
            get { return GetColumnValue<int?>(Columns.ReturnFormId); }
            set { SetColumnValue(Columns.ReturnFormId, value); }
        }

        [XmlAttribute("AllowanceNumber")]
        [Bindable(true)]
        public string AllowanceNumber
        {
            get { return GetColumnValue<string>(Columns.AllowanceNumber); }
            set { SetColumnValue(Columns.AllowanceNumber, value); }
        }

        #endregion




        //no foreign key tables defined (0)



        //no ManyToMany tables defined (0)





        #region Typed Columns


        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }



        public static TableSchema.TableColumn MainIdColumn
        {
            get { return Schema.Columns[1]; }
        }



        public static TableSchema.TableColumn InvoiceAmountColumn
        {
            get { return Schema.Columns[2]; }
        }



        public static TableSchema.TableColumn InvoiceTypeColumn
        {
            get { return Schema.Columns[3]; }
        }



        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[4]; }
        }



        public static TableSchema.TableColumn CreatorColumn
        {
            get { return Schema.Columns[5]; }
        }



        public static TableSchema.TableColumn MessageColumn
        {
            get { return Schema.Columns[6]; }
        }



        public static TableSchema.TableColumn StatusColumn
        {
            get { return Schema.Columns[7]; }
        }



        public static TableSchema.TableColumn InvoicedTimeColumn
        {
            get { return Schema.Columns[8]; }
        }



        public static TableSchema.TableColumn PaymentTransIdColumn
        {
            get { return Schema.Columns[9]; }
        }



        public static TableSchema.TableColumn TransTimeColumn
        {
            get { return Schema.Columns[10]; }
        }



        public static TableSchema.TableColumn InvoiceFileSerialColumn
        {
            get { return Schema.Columns[11]; }
        }



        public static TableSchema.TableColumn TrustIdColumn
        {
            get { return Schema.Columns[12]; }
        }



        public static TableSchema.TableColumn ReturnFormIdColumn
        {
            get { return Schema.Columns[13]; }
        }



        public static TableSchema.TableColumn AllowanceNumberColumn
        {
            get { return Schema.Columns[14]; }
        }



        #endregion
        #region Columns Struct
        public struct Columns
        {
            public static string Id = @"id";
            public static string MainId = @"main_id";
            public static string InvoiceAmount = @"invoice_amount";
            public static string InvoiceType = @"invoice_type";
            public static string CreateTime = @"create_time";
            public static string Creator = @"creator";
            public static string Message = @"message";
            public static string Status = @"status";
            public static string InvoicedTime = @"invoiced_time";
            public static string PaymentTransId = @"payment_trans_id";
            public static string TransTime = @"trans_time";
            public static string InvoiceFileSerial = @"invoice_file_serial";
            public static string TrustId = @"trust_id";
            public static string ReturnFormId = @"return_form_id";
            public static string AllowanceNumber = @"allowance_number";

        }
        #endregion

        #region Update PK Collections

        #endregion

        #region Deep Save

        #endregion
    }
}

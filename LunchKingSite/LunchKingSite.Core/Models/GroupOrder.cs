using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the GroupOrder class.
	/// </summary>
    [Serializable]
	public partial class GroupOrderCollection : RepositoryList<GroupOrder, GroupOrderCollection>
	{	   
		public GroupOrderCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>GroupOrderCollection</returns>
		public GroupOrderCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                GroupOrder o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the group_order table.
	/// </summary>
	[Serializable]
	public partial class GroupOrder : RepositoryRecord<GroupOrder>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public GroupOrder()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public GroupOrder(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("group_order", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarGuid = new TableSchema.TableColumn(schema);
				colvarGuid.ColumnName = "GUID";
				colvarGuid.DataType = DbType.Guid;
				colvarGuid.MaxLength = 0;
				colvarGuid.AutoIncrement = false;
				colvarGuid.IsNullable = false;
				colvarGuid.IsPrimaryKey = true;
				colvarGuid.IsForeignKey = false;
				colvarGuid.IsReadOnly = false;
				colvarGuid.DefaultSetting = @"";
				colvarGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGuid);
				
				TableSchema.TableColumn colvarMemberUserName = new TableSchema.TableColumn(schema);
				colvarMemberUserName.ColumnName = "member_user_name";
				colvarMemberUserName.DataType = DbType.String;
				colvarMemberUserName.MaxLength = 256;
				colvarMemberUserName.AutoIncrement = false;
				colvarMemberUserName.IsNullable = false;
				colvarMemberUserName.IsPrimaryKey = false;
				colvarMemberUserName.IsForeignKey = true;
				colvarMemberUserName.IsReadOnly = false;
				colvarMemberUserName.DefaultSetting = @"";
				
					colvarMemberUserName.ForeignKeyTableName = "member";
				schema.Columns.Add(colvarMemberUserName);
				
				TableSchema.TableColumn colvarBusinessHourGuid = new TableSchema.TableColumn(schema);
				colvarBusinessHourGuid.ColumnName = "business_hour_guid";
				colvarBusinessHourGuid.DataType = DbType.Guid;
				colvarBusinessHourGuid.MaxLength = 0;
				colvarBusinessHourGuid.AutoIncrement = false;
				colvarBusinessHourGuid.IsNullable = false;
				colvarBusinessHourGuid.IsPrimaryKey = false;
				colvarBusinessHourGuid.IsForeignKey = true;
				colvarBusinessHourGuid.IsReadOnly = false;
				colvarBusinessHourGuid.DefaultSetting = @"";
				
					colvarBusinessHourGuid.ForeignKeyTableName = "business_hour";
				schema.Columns.Add(colvarBusinessHourGuid);
				
				TableSchema.TableColumn colvarOrderGuid = new TableSchema.TableColumn(schema);
				colvarOrderGuid.ColumnName = "order_guid";
				colvarOrderGuid.DataType = DbType.Guid;
				colvarOrderGuid.MaxLength = 0;
				colvarOrderGuid.AutoIncrement = false;
				colvarOrderGuid.IsNullable = true;
				colvarOrderGuid.IsPrimaryKey = false;
				colvarOrderGuid.IsForeignKey = true;
				colvarOrderGuid.IsReadOnly = false;
				colvarOrderGuid.DefaultSetting = @"";
				
					colvarOrderGuid.ForeignKeyTableName = "order";
				schema.Columns.Add(colvarOrderGuid);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarCloseTime = new TableSchema.TableColumn(schema);
				colvarCloseTime.ColumnName = "close_time";
				colvarCloseTime.DataType = DbType.DateTime;
				colvarCloseTime.MaxLength = 0;
				colvarCloseTime.AutoIncrement = false;
				colvarCloseTime.IsNullable = false;
				colvarCloseTime.IsPrimaryKey = false;
				colvarCloseTime.IsForeignKey = false;
				colvarCloseTime.IsReadOnly = false;
				colvarCloseTime.DefaultSetting = @"";
				colvarCloseTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCloseTime);
				
				TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
				colvarStatus.ColumnName = "status";
				colvarStatus.DataType = DbType.Int32;
				colvarStatus.MaxLength = 0;
				colvarStatus.AutoIncrement = false;
				colvarStatus.IsNullable = true;
				colvarStatus.IsPrimaryKey = false;
				colvarStatus.IsForeignKey = false;
				colvarStatus.IsReadOnly = false;
				colvarStatus.DefaultSetting = @"";
				colvarStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStatus);
				
				TableSchema.TableColumn colvarSlug = new TableSchema.TableColumn(schema);
				colvarSlug.ColumnName = "slug";
				colvarSlug.DataType = DbType.String;
				colvarSlug.MaxLength = 100;
				colvarSlug.AutoIncrement = false;
				colvarSlug.IsNullable = true;
				colvarSlug.IsPrimaryKey = false;
				colvarSlug.IsForeignKey = false;
				colvarSlug.IsReadOnly = false;
				colvarSlug.DefaultSetting = @"";
				colvarSlug.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSlug);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("group_order",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Guid")]
		[Bindable(true)]
		public Guid Guid 
		{
			get { return GetColumnValue<Guid>(Columns.Guid); }
			set { SetColumnValue(Columns.Guid, value); }
		}
		  
		[XmlAttribute("MemberUserName")]
		[Bindable(true)]
		public string MemberUserName 
		{
			get { return GetColumnValue<string>(Columns.MemberUserName); }
			set { SetColumnValue(Columns.MemberUserName, value); }
		}
		  
		[XmlAttribute("BusinessHourGuid")]
		[Bindable(true)]
		public Guid BusinessHourGuid 
		{
			get { return GetColumnValue<Guid>(Columns.BusinessHourGuid); }
			set { SetColumnValue(Columns.BusinessHourGuid, value); }
		}
		  
		[XmlAttribute("OrderGuid")]
		[Bindable(true)]
		public Guid? OrderGuid 
		{
			get { return GetColumnValue<Guid?>(Columns.OrderGuid); }
			set { SetColumnValue(Columns.OrderGuid, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		  
		[XmlAttribute("CloseTime")]
		[Bindable(true)]
		public DateTime CloseTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CloseTime); }
			set { SetColumnValue(Columns.CloseTime, value); }
		}
		  
		[XmlAttribute("Status")]
		[Bindable(true)]
		public int? Status 
		{
			get { return GetColumnValue<int?>(Columns.Status); }
			set { SetColumnValue(Columns.Status, value); }
		}
		  
		[XmlAttribute("Slug")]
		[Bindable(true)]
		public string Slug 
		{
			get { return GetColumnValue<string>(Columns.Slug); }
			set { SetColumnValue(Columns.Slug, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (3)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn GuidColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn MemberUserNameColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn BusinessHourGuidColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn OrderGuidColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn CloseTimeColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn StatusColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn SlugColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Guid = @"GUID";
			 public static string MemberUserName = @"member_user_name";
			 public static string BusinessHourGuid = @"business_hour_guid";
			 public static string OrderGuid = @"order_guid";
			 public static string CreateTime = @"create_time";
			 public static string CloseTime = @"close_time";
			 public static string Status = @"status";
			 public static string Slug = @"slug";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

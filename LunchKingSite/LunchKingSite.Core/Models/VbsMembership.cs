using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the VbsMembership class.
	/// </summary>
    [Serializable]
	public partial class VbsMembershipCollection : RepositoryList<VbsMembership, VbsMembershipCollection>
	{	   
		public VbsMembershipCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>VbsMembershipCollection</returns>
		public VbsMembershipCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                VbsMembership o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the vbs_membership table.
	/// </summary>
	[Serializable]
	public partial class VbsMembership : RepositoryRecord<VbsMembership>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public VbsMembership()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public VbsMembership(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("vbs_membership", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarAccountId = new TableSchema.TableColumn(schema);
				colvarAccountId.ColumnName = "account_id";
				colvarAccountId.DataType = DbType.String;
				colvarAccountId.MaxLength = 256;
				colvarAccountId.AutoIncrement = false;
				colvarAccountId.IsNullable = false;
				colvarAccountId.IsPrimaryKey = false;
				colvarAccountId.IsForeignKey = false;
				colvarAccountId.IsReadOnly = false;
				colvarAccountId.DefaultSetting = @"";
				colvarAccountId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAccountId);
				
				TableSchema.TableColumn colvarAccountType = new TableSchema.TableColumn(schema);
				colvarAccountType.ColumnName = "account_type";
				colvarAccountType.DataType = DbType.Int32;
				colvarAccountType.MaxLength = 0;
				colvarAccountType.AutoIncrement = false;
				colvarAccountType.IsNullable = false;
				colvarAccountType.IsPrimaryKey = false;
				colvarAccountType.IsForeignKey = false;
				colvarAccountType.IsReadOnly = false;
				colvarAccountType.DefaultSetting = @"";
				colvarAccountType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAccountType);
				
				TableSchema.TableColumn colvarName = new TableSchema.TableColumn(schema);
				colvarName.ColumnName = "name";
				colvarName.DataType = DbType.String;
				colvarName.MaxLength = 100;
				colvarName.AutoIncrement = false;
				colvarName.IsNullable = false;
				colvarName.IsPrimaryKey = false;
				colvarName.IsForeignKey = false;
				colvarName.IsReadOnly = false;
				colvarName.DefaultSetting = @"";
				colvarName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarName);
				
				TableSchema.TableColumn colvarEmail = new TableSchema.TableColumn(schema);
				colvarEmail.ColumnName = "email";
				colvarEmail.DataType = DbType.String;
				colvarEmail.MaxLength = 256;
				colvarEmail.AutoIncrement = false;
				colvarEmail.IsNullable = true;
				colvarEmail.IsPrimaryKey = false;
				colvarEmail.IsForeignKey = false;
				colvarEmail.IsReadOnly = false;
				colvarEmail.DefaultSetting = @"";
				colvarEmail.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEmail);
				
				TableSchema.TableColumn colvarPassword = new TableSchema.TableColumn(schema);
				colvarPassword.ColumnName = "password";
				colvarPassword.DataType = DbType.String;
				colvarPassword.MaxLength = 128;
				colvarPassword.AutoIncrement = false;
				colvarPassword.IsNullable = false;
				colvarPassword.IsPrimaryKey = false;
				colvarPassword.IsForeignKey = false;
				colvarPassword.IsReadOnly = false;
				colvarPassword.DefaultSetting = @"";
				colvarPassword.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPassword);
				
				TableSchema.TableColumn colvarPasswordFormat = new TableSchema.TableColumn(schema);
				colvarPasswordFormat.ColumnName = "password_format";
				colvarPasswordFormat.DataType = DbType.Int32;
				colvarPasswordFormat.MaxLength = 0;
				colvarPasswordFormat.AutoIncrement = false;
				colvarPasswordFormat.IsNullable = false;
				colvarPasswordFormat.IsPrimaryKey = false;
				colvarPasswordFormat.IsForeignKey = false;
				colvarPasswordFormat.IsReadOnly = false;
				colvarPasswordFormat.DefaultSetting = @"";
				colvarPasswordFormat.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPasswordFormat);
				
				TableSchema.TableColumn colvarHasInspectionCode = new TableSchema.TableColumn(schema);
				colvarHasInspectionCode.ColumnName = "has_inspection_code";
				colvarHasInspectionCode.DataType = DbType.Boolean;
				colvarHasInspectionCode.MaxLength = 0;
				colvarHasInspectionCode.AutoIncrement = false;
				colvarHasInspectionCode.IsNullable = false;
				colvarHasInspectionCode.IsPrimaryKey = false;
				colvarHasInspectionCode.IsForeignKey = false;
				colvarHasInspectionCode.IsReadOnly = false;
				colvarHasInspectionCode.DefaultSetting = @"";
				colvarHasInspectionCode.ForeignKeyTableName = "";
				schema.Columns.Add(colvarHasInspectionCode);
				
				TableSchema.TableColumn colvarInspectionCode = new TableSchema.TableColumn(schema);
				colvarInspectionCode.ColumnName = "inspection_code";
				colvarInspectionCode.DataType = DbType.String;
				colvarInspectionCode.MaxLength = 128;
				colvarInspectionCode.AutoIncrement = false;
				colvarInspectionCode.IsNullable = true;
				colvarInspectionCode.IsPrimaryKey = false;
				colvarInspectionCode.IsForeignKey = false;
				colvarInspectionCode.IsReadOnly = false;
				colvarInspectionCode.DefaultSetting = @"";
				colvarInspectionCode.ForeignKeyTableName = "";
				schema.Columns.Add(colvarInspectionCode);
				
				TableSchema.TableColumn colvarIsLockedOut = new TableSchema.TableColumn(schema);
				colvarIsLockedOut.ColumnName = "is_locked_out";
				colvarIsLockedOut.DataType = DbType.Boolean;
				colvarIsLockedOut.MaxLength = 0;
				colvarIsLockedOut.AutoIncrement = false;
				colvarIsLockedOut.IsNullable = false;
				colvarIsLockedOut.IsPrimaryKey = false;
				colvarIsLockedOut.IsForeignKey = false;
				colvarIsLockedOut.IsReadOnly = false;
				colvarIsLockedOut.DefaultSetting = @"";
				colvarIsLockedOut.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsLockedOut);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarLastLoginDate = new TableSchema.TableColumn(schema);
				colvarLastLoginDate.ColumnName = "last_login_date";
				colvarLastLoginDate.DataType = DbType.DateTime;
				colvarLastLoginDate.MaxLength = 0;
				colvarLastLoginDate.AutoIncrement = false;
				colvarLastLoginDate.IsNullable = true;
				colvarLastLoginDate.IsPrimaryKey = false;
				colvarLastLoginDate.IsForeignKey = false;
				colvarLastLoginDate.IsReadOnly = false;
				colvarLastLoginDate.DefaultSetting = @"";
				colvarLastLoginDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarLastLoginDate);
				
				TableSchema.TableColumn colvarLastPasswordChangedDate = new TableSchema.TableColumn(schema);
				colvarLastPasswordChangedDate.ColumnName = "last_password_changed_date";
				colvarLastPasswordChangedDate.DataType = DbType.DateTime;
				colvarLastPasswordChangedDate.MaxLength = 0;
				colvarLastPasswordChangedDate.AutoIncrement = false;
				colvarLastPasswordChangedDate.IsNullable = true;
				colvarLastPasswordChangedDate.IsPrimaryKey = false;
				colvarLastPasswordChangedDate.IsForeignKey = false;
				colvarLastPasswordChangedDate.IsReadOnly = false;
				colvarLastPasswordChangedDate.DefaultSetting = @"";
				colvarLastPasswordChangedDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarLastPasswordChangedDate);
				
				TableSchema.TableColumn colvarLastInspectionDate = new TableSchema.TableColumn(schema);
				colvarLastInspectionDate.ColumnName = "last_inspection_date";
				colvarLastInspectionDate.DataType = DbType.DateTime;
				colvarLastInspectionDate.MaxLength = 0;
				colvarLastInspectionDate.AutoIncrement = false;
				colvarLastInspectionDate.IsNullable = true;
				colvarLastInspectionDate.IsPrimaryKey = false;
				colvarLastInspectionDate.IsForeignKey = false;
				colvarLastInspectionDate.IsReadOnly = false;
				colvarLastInspectionDate.DefaultSetting = @"";
				colvarLastInspectionDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarLastInspectionDate);
				
				TableSchema.TableColumn colvarLastInspectionCodeChangedDate = new TableSchema.TableColumn(schema);
				colvarLastInspectionCodeChangedDate.ColumnName = "last_inspection_code_changed_date";
				colvarLastInspectionCodeChangedDate.DataType = DbType.DateTime;
				colvarLastInspectionCodeChangedDate.MaxLength = 0;
				colvarLastInspectionCodeChangedDate.AutoIncrement = false;
				colvarLastInspectionCodeChangedDate.IsNullable = true;
				colvarLastInspectionCodeChangedDate.IsPrimaryKey = false;
				colvarLastInspectionCodeChangedDate.IsForeignKey = false;
				colvarLastInspectionCodeChangedDate.IsReadOnly = false;
				colvarLastInspectionCodeChangedDate.DefaultSetting = @"";
				colvarLastInspectionCodeChangedDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarLastInspectionCodeChangedDate);
				
				TableSchema.TableColumn colvarLastLockoutDate = new TableSchema.TableColumn(schema);
				colvarLastLockoutDate.ColumnName = "last_lockout_date";
				colvarLastLockoutDate.DataType = DbType.DateTime;
				colvarLastLockoutDate.MaxLength = 0;
				colvarLastLockoutDate.AutoIncrement = false;
				colvarLastLockoutDate.IsNullable = true;
				colvarLastLockoutDate.IsPrimaryKey = false;
				colvarLastLockoutDate.IsForeignKey = false;
				colvarLastLockoutDate.IsReadOnly = false;
				colvarLastLockoutDate.DefaultSetting = @"";
				colvarLastLockoutDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarLastLockoutDate);
				
				TableSchema.TableColumn colvarFailedPasswordAttemptCount = new TableSchema.TableColumn(schema);
				colvarFailedPasswordAttemptCount.ColumnName = "failed_password_attempt_count";
				colvarFailedPasswordAttemptCount.DataType = DbType.Int32;
				colvarFailedPasswordAttemptCount.MaxLength = 0;
				colvarFailedPasswordAttemptCount.AutoIncrement = false;
				colvarFailedPasswordAttemptCount.IsNullable = false;
				colvarFailedPasswordAttemptCount.IsPrimaryKey = false;
				colvarFailedPasswordAttemptCount.IsForeignKey = false;
				colvarFailedPasswordAttemptCount.IsReadOnly = false;
				colvarFailedPasswordAttemptCount.DefaultSetting = @"";
				colvarFailedPasswordAttemptCount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFailedPasswordAttemptCount);
				
				TableSchema.TableColumn colvarFailedPasswordAttemptWindowStart = new TableSchema.TableColumn(schema);
				colvarFailedPasswordAttemptWindowStart.ColumnName = "failed_password_attempt_window_start";
				colvarFailedPasswordAttemptWindowStart.DataType = DbType.DateTime;
				colvarFailedPasswordAttemptWindowStart.MaxLength = 0;
				colvarFailedPasswordAttemptWindowStart.AutoIncrement = false;
				colvarFailedPasswordAttemptWindowStart.IsNullable = true;
				colvarFailedPasswordAttemptWindowStart.IsPrimaryKey = false;
				colvarFailedPasswordAttemptWindowStart.IsForeignKey = false;
				colvarFailedPasswordAttemptWindowStart.IsReadOnly = false;
				colvarFailedPasswordAttemptWindowStart.DefaultSetting = @"";
				colvarFailedPasswordAttemptWindowStart.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFailedPasswordAttemptWindowStart);
				
				TableSchema.TableColumn colvarFailedInspectionCodeAttemptCount = new TableSchema.TableColumn(schema);
				colvarFailedInspectionCodeAttemptCount.ColumnName = "failed_inspection_code_attempt_count";
				colvarFailedInspectionCodeAttemptCount.DataType = DbType.Int32;
				colvarFailedInspectionCodeAttemptCount.MaxLength = 0;
				colvarFailedInspectionCodeAttemptCount.AutoIncrement = false;
				colvarFailedInspectionCodeAttemptCount.IsNullable = false;
				colvarFailedInspectionCodeAttemptCount.IsPrimaryKey = false;
				colvarFailedInspectionCodeAttemptCount.IsForeignKey = false;
				colvarFailedInspectionCodeAttemptCount.IsReadOnly = false;
				colvarFailedInspectionCodeAttemptCount.DefaultSetting = @"";
				colvarFailedInspectionCodeAttemptCount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFailedInspectionCodeAttemptCount);
				
				TableSchema.TableColumn colvarFailedInspectionCodeAttemptWindowStart = new TableSchema.TableColumn(schema);
				colvarFailedInspectionCodeAttemptWindowStart.ColumnName = "failed_inspection_code_attempt_window_start";
				colvarFailedInspectionCodeAttemptWindowStart.DataType = DbType.DateTime;
				colvarFailedInspectionCodeAttemptWindowStart.MaxLength = 0;
				colvarFailedInspectionCodeAttemptWindowStart.AutoIncrement = false;
				colvarFailedInspectionCodeAttemptWindowStart.IsNullable = true;
				colvarFailedInspectionCodeAttemptWindowStart.IsPrimaryKey = false;
				colvarFailedInspectionCodeAttemptWindowStart.IsForeignKey = false;
				colvarFailedInspectionCodeAttemptWindowStart.IsReadOnly = false;
				colvarFailedInspectionCodeAttemptWindowStart.DefaultSetting = @"";
				colvarFailedInspectionCodeAttemptWindowStart.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFailedInspectionCodeAttemptWindowStart);
				
				TableSchema.TableColumn colvarIsApproved = new TableSchema.TableColumn(schema);
				colvarIsApproved.ColumnName = "is_approved";
				colvarIsApproved.DataType = DbType.Boolean;
				colvarIsApproved.MaxLength = 0;
				colvarIsApproved.AutoIncrement = false;
				colvarIsApproved.IsNullable = false;
				colvarIsApproved.IsPrimaryKey = false;
				colvarIsApproved.IsForeignKey = false;
				colvarIsApproved.IsReadOnly = false;
				colvarIsApproved.DefaultSetting = @"";
				colvarIsApproved.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsApproved);
				
				TableSchema.TableColumn colvarViewBsRight = new TableSchema.TableColumn(schema);
				colvarViewBsRight.ColumnName = "view_bs_right";
				colvarViewBsRight.DataType = DbType.Boolean;
				colvarViewBsRight.MaxLength = 0;
				colvarViewBsRight.AutoIncrement = false;
				colvarViewBsRight.IsNullable = false;
				colvarViewBsRight.IsPrimaryKey = false;
				colvarViewBsRight.IsForeignKey = false;
				colvarViewBsRight.IsReadOnly = false;
				
						colvarViewBsRight.DefaultSetting = @"((0))";
				colvarViewBsRight.ForeignKeyTableName = "";
				schema.Columns.Add(colvarViewBsRight);
				
				TableSchema.TableColumn colvarViewVerifyRight = new TableSchema.TableColumn(schema);
				colvarViewVerifyRight.ColumnName = "view_verify_right";
				colvarViewVerifyRight.DataType = DbType.Boolean;
				colvarViewVerifyRight.MaxLength = 0;
				colvarViewVerifyRight.AutoIncrement = false;
				colvarViewVerifyRight.IsNullable = false;
				colvarViewVerifyRight.IsPrimaryKey = false;
				colvarViewVerifyRight.IsForeignKey = false;
				colvarViewVerifyRight.IsReadOnly = false;
				
						colvarViewVerifyRight.DefaultSetting = @"((0))";
				colvarViewVerifyRight.ForeignKeyTableName = "";
				schema.Columns.Add(colvarViewVerifyRight);
				
				TableSchema.TableColumn colvarViewShipRight = new TableSchema.TableColumn(schema);
				colvarViewShipRight.ColumnName = "view_ship_right";
				colvarViewShipRight.DataType = DbType.Boolean;
				colvarViewShipRight.MaxLength = 0;
				colvarViewShipRight.AutoIncrement = false;
				colvarViewShipRight.IsNullable = false;
				colvarViewShipRight.IsPrimaryKey = false;
				colvarViewShipRight.IsForeignKey = false;
				colvarViewShipRight.IsReadOnly = false;
				
						colvarViewShipRight.DefaultSetting = @"((0))";
				colvarViewShipRight.ForeignKeyTableName = "";
				schema.Columns.Add(colvarViewShipRight);
				
				TableSchema.TableColumn colvarViewBookingRight = new TableSchema.TableColumn(schema);
				colvarViewBookingRight.ColumnName = "view_booking_right";
				colvarViewBookingRight.DataType = DbType.Boolean;
				colvarViewBookingRight.MaxLength = 0;
				colvarViewBookingRight.AutoIncrement = false;
				colvarViewBookingRight.IsNullable = false;
				colvarViewBookingRight.IsPrimaryKey = false;
				colvarViewBookingRight.IsForeignKey = false;
				colvarViewBookingRight.IsReadOnly = false;
				
						colvarViewBookingRight.DefaultSetting = @"((0))";
				colvarViewBookingRight.ForeignKeyTableName = "";
				schema.Columns.Add(colvarViewBookingRight);
				
				TableSchema.TableColumn colvarViewReserveLockRight = new TableSchema.TableColumn(schema);
				colvarViewReserveLockRight.ColumnName = "view_reserve_lock_right";
				colvarViewReserveLockRight.DataType = DbType.Boolean;
				colvarViewReserveLockRight.MaxLength = 0;
				colvarViewReserveLockRight.AutoIncrement = false;
				colvarViewReserveLockRight.IsNullable = false;
				colvarViewReserveLockRight.IsPrimaryKey = false;
				colvarViewReserveLockRight.IsForeignKey = false;
				colvarViewReserveLockRight.IsReadOnly = false;
				
						colvarViewReserveLockRight.DefaultSetting = @"((0))";
				colvarViewReserveLockRight.ForeignKeyTableName = "";
				schema.Columns.Add(colvarViewReserveLockRight);
				
				TableSchema.TableColumn colvarLastInfoProtectReadDate = new TableSchema.TableColumn(schema);
				colvarLastInfoProtectReadDate.ColumnName = "last_info_protect_read_date";
				colvarLastInfoProtectReadDate.DataType = DbType.DateTime;
				colvarLastInfoProtectReadDate.MaxLength = 0;
				colvarLastInfoProtectReadDate.AutoIncrement = false;
				colvarLastInfoProtectReadDate.IsNullable = true;
				colvarLastInfoProtectReadDate.IsPrimaryKey = false;
				colvarLastInfoProtectReadDate.IsForeignKey = false;
				colvarLastInfoProtectReadDate.IsReadOnly = false;
				colvarLastInfoProtectReadDate.DefaultSetting = @"";
				colvarLastInfoProtectReadDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarLastInfoProtectReadDate);
				
				TableSchema.TableColumn colvarViewUserEvaluateRight = new TableSchema.TableColumn(schema);
				colvarViewUserEvaluateRight.ColumnName = "view_user_evaluate_right";
				colvarViewUserEvaluateRight.DataType = DbType.Boolean;
				colvarViewUserEvaluateRight.MaxLength = 0;
				colvarViewUserEvaluateRight.AutoIncrement = false;
				colvarViewUserEvaluateRight.IsNullable = false;
				colvarViewUserEvaluateRight.IsPrimaryKey = false;
				colvarViewUserEvaluateRight.IsForeignKey = false;
				colvarViewUserEvaluateRight.IsReadOnly = false;
				
						colvarViewUserEvaluateRight.DefaultSetting = @"((0))";
				colvarViewUserEvaluateRight.ForeignKeyTableName = "";
				schema.Columns.Add(colvarViewUserEvaluateRight);
				
				TableSchema.TableColumn colvarPersonalInfoClauseAgreeDate = new TableSchema.TableColumn(schema);
				colvarPersonalInfoClauseAgreeDate.ColumnName = "personal_info_clause_agree_date";
				colvarPersonalInfoClauseAgreeDate.DataType = DbType.DateTime;
				colvarPersonalInfoClauseAgreeDate.MaxLength = 0;
				colvarPersonalInfoClauseAgreeDate.AutoIncrement = false;
				colvarPersonalInfoClauseAgreeDate.IsNullable = true;
				colvarPersonalInfoClauseAgreeDate.IsPrimaryKey = false;
				colvarPersonalInfoClauseAgreeDate.IsForeignKey = false;
				colvarPersonalInfoClauseAgreeDate.IsReadOnly = false;
				colvarPersonalInfoClauseAgreeDate.DefaultSetting = @"";
				colvarPersonalInfoClauseAgreeDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPersonalInfoClauseAgreeDate);
				
				TableSchema.TableColumn colvarCompanyDetailId = new TableSchema.TableColumn(schema);
				colvarCompanyDetailId.ColumnName = "company_detail_id";
				colvarCompanyDetailId.DataType = DbType.Int32;
				colvarCompanyDetailId.MaxLength = 0;
				colvarCompanyDetailId.AutoIncrement = false;
				colvarCompanyDetailId.IsNullable = true;
				colvarCompanyDetailId.IsPrimaryKey = false;
				colvarCompanyDetailId.IsForeignKey = false;
				colvarCompanyDetailId.IsReadOnly = false;
				colvarCompanyDetailId.DefaultSetting = @"";
				colvarCompanyDetailId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCompanyDetailId);
				
				TableSchema.TableColumn colvarUserId = new TableSchema.TableColumn(schema);
				colvarUserId.ColumnName = "user_id";
				colvarUserId.DataType = DbType.Int32;
				colvarUserId.MaxLength = 0;
				colvarUserId.AutoIncrement = false;
				colvarUserId.IsNullable = true;
				colvarUserId.IsPrimaryKey = false;
				colvarUserId.IsForeignKey = false;
				colvarUserId.IsReadOnly = false;
				colvarUserId.DefaultSetting = @"";
				colvarUserId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUserId);
				
				TableSchema.TableColumn colvarViewDealScheduleRight = new TableSchema.TableColumn(schema);
				colvarViewDealScheduleRight.ColumnName = "view_deal_schedule_right";
				colvarViewDealScheduleRight.DataType = DbType.Boolean;
				colvarViewDealScheduleRight.MaxLength = 0;
				colvarViewDealScheduleRight.AutoIncrement = false;
				colvarViewDealScheduleRight.IsNullable = false;
				colvarViewDealScheduleRight.IsPrimaryKey = false;
				colvarViewDealScheduleRight.IsForeignKey = false;
				colvarViewDealScheduleRight.IsReadOnly = false;
				
						colvarViewDealScheduleRight.DefaultSetting = @"((0))";
				colvarViewDealScheduleRight.ForeignKeyTableName = "";
				schema.Columns.Add(colvarViewDealScheduleRight);
				
				TableSchema.TableColumn colvarViewProposalRight = new TableSchema.TableColumn(schema);
				colvarViewProposalRight.ColumnName = "view_proposal_right";
				colvarViewProposalRight.DataType = DbType.Boolean;
				colvarViewProposalRight.MaxLength = 0;
				colvarViewProposalRight.AutoIncrement = false;
				colvarViewProposalRight.IsNullable = false;
				colvarViewProposalRight.IsPrimaryKey = false;
				colvarViewProposalRight.IsForeignKey = false;
				colvarViewProposalRight.IsReadOnly = false;
				
						colvarViewProposalRight.DefaultSetting = @"((0))";
				colvarViewProposalRight.ForeignKeyTableName = "";
				schema.Columns.Add(colvarViewProposalRight);
				
				TableSchema.TableColumn colvarProposalConsent = new TableSchema.TableColumn(schema);
				colvarProposalConsent.ColumnName = "proposal_consent";
				colvarProposalConsent.DataType = DbType.Boolean;
				colvarProposalConsent.MaxLength = 0;
				colvarProposalConsent.AutoIncrement = false;
				colvarProposalConsent.IsNullable = false;
				colvarProposalConsent.IsPrimaryKey = false;
				colvarProposalConsent.IsForeignKey = false;
				colvarProposalConsent.IsReadOnly = false;
				
						colvarProposalConsent.DefaultSetting = @"((0))";
				colvarProposalConsent.ForeignKeyTableName = "";
				schema.Columns.Add(colvarProposalConsent);
				
				TableSchema.TableColumn colvarProposalConsentDate = new TableSchema.TableColumn(schema);
				colvarProposalConsentDate.ColumnName = "proposal_consent_date";
				colvarProposalConsentDate.DataType = DbType.DateTime;
				colvarProposalConsentDate.MaxLength = 0;
				colvarProposalConsentDate.AutoIncrement = false;
				colvarProposalConsentDate.IsNullable = true;
				colvarProposalConsentDate.IsPrimaryKey = false;
				colvarProposalConsentDate.IsForeignKey = false;
				colvarProposalConsentDate.IsReadOnly = false;
				colvarProposalConsentDate.DefaultSetting = @"";
				colvarProposalConsentDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarProposalConsentDate);
				
				TableSchema.TableColumn colvarViewShoppingCartRight = new TableSchema.TableColumn(schema);
				colvarViewShoppingCartRight.ColumnName = "view_shopping_cart_right";
				colvarViewShoppingCartRight.DataType = DbType.Boolean;
				colvarViewShoppingCartRight.MaxLength = 0;
				colvarViewShoppingCartRight.AutoIncrement = false;
				colvarViewShoppingCartRight.IsNullable = false;
				colvarViewShoppingCartRight.IsPrimaryKey = false;
				colvarViewShoppingCartRight.IsForeignKey = false;
				colvarViewShoppingCartRight.IsReadOnly = false;
				
						colvarViewShoppingCartRight.DefaultSetting = @"((0))";
				colvarViewShoppingCartRight.ForeignKeyTableName = "";
				schema.Columns.Add(colvarViewShoppingCartRight);
				
				TableSchema.TableColumn colvarTemporaryPassword = new TableSchema.TableColumn(schema);
				colvarTemporaryPassword.ColumnName = "temporary_password";
				colvarTemporaryPassword.DataType = DbType.AnsiString;
				colvarTemporaryPassword.MaxLength = 100;
				colvarTemporaryPassword.AutoIncrement = false;
				colvarTemporaryPassword.IsNullable = true;
				colvarTemporaryPassword.IsPrimaryKey = false;
				colvarTemporaryPassword.IsForeignKey = false;
				colvarTemporaryPassword.IsReadOnly = false;
				colvarTemporaryPassword.DefaultSetting = @"";
				colvarTemporaryPassword.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTemporaryPassword);
				
				TableSchema.TableColumn colvarTemporaryPasswordDate = new TableSchema.TableColumn(schema);
				colvarTemporaryPasswordDate.ColumnName = "temporary_password_date";
				colvarTemporaryPasswordDate.DataType = DbType.DateTime;
				colvarTemporaryPasswordDate.MaxLength = 0;
				colvarTemporaryPasswordDate.AutoIncrement = false;
				colvarTemporaryPasswordDate.IsNullable = true;
				colvarTemporaryPasswordDate.IsPrimaryKey = false;
				colvarTemporaryPasswordDate.IsForeignKey = false;
				colvarTemporaryPasswordDate.IsReadOnly = false;
				colvarTemporaryPasswordDate.DefaultSetting = @"";
				colvarTemporaryPasswordDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTemporaryPasswordDate);
				
				TableSchema.TableColumn colvarIsTemporaryLogin = new TableSchema.TableColumn(schema);
				colvarIsTemporaryLogin.ColumnName = "is_temporary_login";
				colvarIsTemporaryLogin.DataType = DbType.Boolean;
				colvarIsTemporaryLogin.MaxLength = 0;
				colvarIsTemporaryLogin.AutoIncrement = false;
				colvarIsTemporaryLogin.IsNullable = false;
				colvarIsTemporaryLogin.IsPrimaryKey = false;
				colvarIsTemporaryLogin.IsForeignKey = false;
				colvarIsTemporaryLogin.IsReadOnly = false;
				
						colvarIsTemporaryLogin.DefaultSetting = @"((0))";
				colvarIsTemporaryLogin.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsTemporaryLogin);
				
				TableSchema.TableColumn colvarViewToHouse = new TableSchema.TableColumn(schema);
				colvarViewToHouse.ColumnName = "view_to_house";
				colvarViewToHouse.DataType = DbType.Boolean;
				colvarViewToHouse.MaxLength = 0;
				colvarViewToHouse.AutoIncrement = false;
				colvarViewToHouse.IsNullable = false;
				colvarViewToHouse.IsPrimaryKey = false;
				colvarViewToHouse.IsForeignKey = false;
				colvarViewToHouse.IsReadOnly = false;
				
						colvarViewToHouse.DefaultSetting = @"((0))";
				colvarViewToHouse.ForeignKeyTableName = "";
				schema.Columns.Add(colvarViewToHouse);
				
				TableSchema.TableColumn colvarViewToShop = new TableSchema.TableColumn(schema);
				colvarViewToShop.ColumnName = "view_to_shop";
				colvarViewToShop.DataType = DbType.Boolean;
				colvarViewToShop.MaxLength = 0;
				colvarViewToShop.AutoIncrement = false;
				colvarViewToShop.IsNullable = false;
				colvarViewToShop.IsPrimaryKey = false;
				colvarViewToShop.IsForeignKey = false;
				colvarViewToShop.IsReadOnly = false;
				
						colvarViewToShop.DefaultSetting = @"((0))";
				colvarViewToShop.ForeignKeyTableName = "";
				schema.Columns.Add(colvarViewToShop);
				
				TableSchema.TableColumn colvarViewProposalProductRight = new TableSchema.TableColumn(schema);
				colvarViewProposalProductRight.ColumnName = "view_proposal_product_right";
				colvarViewProposalProductRight.DataType = DbType.Boolean;
				colvarViewProposalProductRight.MaxLength = 0;
				colvarViewProposalProductRight.AutoIncrement = false;
				colvarViewProposalProductRight.IsNullable = false;
				colvarViewProposalProductRight.IsPrimaryKey = false;
				colvarViewProposalProductRight.IsForeignKey = false;
				colvarViewProposalProductRight.IsReadOnly = false;
				
						colvarViewProposalProductRight.DefaultSetting = @"((0))";
				colvarViewProposalProductRight.ForeignKeyTableName = "";
				schema.Columns.Add(colvarViewProposalProductRight);
				
				TableSchema.TableColumn colvarViewAccountManageRight = new TableSchema.TableColumn(schema);
				colvarViewAccountManageRight.ColumnName = "view_account_manage_right";
				colvarViewAccountManageRight.DataType = DbType.Boolean;
				colvarViewAccountManageRight.MaxLength = 0;
				colvarViewAccountManageRight.AutoIncrement = false;
				colvarViewAccountManageRight.IsNullable = false;
				colvarViewAccountManageRight.IsPrimaryKey = false;
				colvarViewAccountManageRight.IsForeignKey = false;
				colvarViewAccountManageRight.IsReadOnly = false;
				
						colvarViewAccountManageRight.DefaultSetting = @"((0))";
				colvarViewAccountManageRight.ForeignKeyTableName = "";
				schema.Columns.Add(colvarViewAccountManageRight);
				
				TableSchema.TableColumn colvarViewCustomerServiceRight = new TableSchema.TableColumn(schema);
				colvarViewCustomerServiceRight.ColumnName = "view_customer_service_right";
				colvarViewCustomerServiceRight.DataType = DbType.Boolean;
				colvarViewCustomerServiceRight.MaxLength = 0;
				colvarViewCustomerServiceRight.AutoIncrement = false;
				colvarViewCustomerServiceRight.IsNullable = false;
				colvarViewCustomerServiceRight.IsPrimaryKey = false;
				colvarViewCustomerServiceRight.IsForeignKey = false;
				colvarViewCustomerServiceRight.IsReadOnly = false;
				
						colvarViewCustomerServiceRight.DefaultSetting = @"((1))";
				colvarViewCustomerServiceRight.ForeignKeyTableName = "";
				schema.Columns.Add(colvarViewCustomerServiceRight);
				
				TableSchema.TableColumn colvarViewWmsRight = new TableSchema.TableColumn(schema);
				colvarViewWmsRight.ColumnName = "view_wms_right";
				colvarViewWmsRight.DataType = DbType.Boolean;
				colvarViewWmsRight.MaxLength = 0;
				colvarViewWmsRight.AutoIncrement = false;
				colvarViewWmsRight.IsNullable = false;
				colvarViewWmsRight.IsPrimaryKey = false;
				colvarViewWmsRight.IsForeignKey = false;
				colvarViewWmsRight.IsReadOnly = false;
				
						colvarViewWmsRight.DefaultSetting = @"((0))";
				colvarViewWmsRight.ForeignKeyTableName = "";
				schema.Columns.Add(colvarViewWmsRight);
				
				TableSchema.TableColumn colvarWmsContract = new TableSchema.TableColumn(schema);
				colvarWmsContract.ColumnName = "wms_contract";
				colvarWmsContract.DataType = DbType.AnsiString;
				colvarWmsContract.MaxLength = 10;
				colvarWmsContract.AutoIncrement = false;
				colvarWmsContract.IsNullable = true;
				colvarWmsContract.IsPrimaryKey = false;
				colvarWmsContract.IsForeignKey = false;
				colvarWmsContract.IsReadOnly = false;
				colvarWmsContract.DefaultSetting = @"";
				colvarWmsContract.ForeignKeyTableName = "";
				schema.Columns.Add(colvarWmsContract);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("vbs_membership",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("AccountId")]
		[Bindable(true)]
		public string AccountId 
		{
			get { return GetColumnValue<string>(Columns.AccountId); }
			set { SetColumnValue(Columns.AccountId, value); }
		}
		  
		[XmlAttribute("AccountType")]
		[Bindable(true)]
		public int AccountType 
		{
			get { return GetColumnValue<int>(Columns.AccountType); }
			set { SetColumnValue(Columns.AccountType, value); }
		}
		  
		[XmlAttribute("Name")]
		[Bindable(true)]
		public string Name 
		{
			get { return GetColumnValue<string>(Columns.Name); }
			set { SetColumnValue(Columns.Name, value); }
		}
		  
		[XmlAttribute("Email")]
		[Bindable(true)]
		public string Email 
		{
			get { return GetColumnValue<string>(Columns.Email); }
			set { SetColumnValue(Columns.Email, value); }
		}
		  
		[XmlAttribute("Password")]
		[Bindable(true)]
		public string Password 
		{
			get { return GetColumnValue<string>(Columns.Password); }
			set { SetColumnValue(Columns.Password, value); }
		}
		  
		[XmlAttribute("PasswordFormat")]
		[Bindable(true)]
		public int PasswordFormat 
		{
			get { return GetColumnValue<int>(Columns.PasswordFormat); }
			set { SetColumnValue(Columns.PasswordFormat, value); }
		}
		  
		[XmlAttribute("HasInspectionCode")]
		[Bindable(true)]
		public bool HasInspectionCode 
		{
			get { return GetColumnValue<bool>(Columns.HasInspectionCode); }
			set { SetColumnValue(Columns.HasInspectionCode, value); }
		}
		  
		[XmlAttribute("InspectionCode")]
		[Bindable(true)]
		public string InspectionCode 
		{
			get { return GetColumnValue<string>(Columns.InspectionCode); }
			set { SetColumnValue(Columns.InspectionCode, value); }
		}
		  
		[XmlAttribute("IsLockedOut")]
		[Bindable(true)]
		public bool IsLockedOut 
		{
			get { return GetColumnValue<bool>(Columns.IsLockedOut); }
			set { SetColumnValue(Columns.IsLockedOut, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		  
		[XmlAttribute("LastLoginDate")]
		[Bindable(true)]
		public DateTime? LastLoginDate 
		{
			get { return GetColumnValue<DateTime?>(Columns.LastLoginDate); }
			set { SetColumnValue(Columns.LastLoginDate, value); }
		}
		  
		[XmlAttribute("LastPasswordChangedDate")]
		[Bindable(true)]
		public DateTime? LastPasswordChangedDate 
		{
			get { return GetColumnValue<DateTime?>(Columns.LastPasswordChangedDate); }
			set { SetColumnValue(Columns.LastPasswordChangedDate, value); }
		}
		  
		[XmlAttribute("LastInspectionDate")]
		[Bindable(true)]
		public DateTime? LastInspectionDate 
		{
			get { return GetColumnValue<DateTime?>(Columns.LastInspectionDate); }
			set { SetColumnValue(Columns.LastInspectionDate, value); }
		}
		  
		[XmlAttribute("LastInspectionCodeChangedDate")]
		[Bindable(true)]
		public DateTime? LastInspectionCodeChangedDate 
		{
			get { return GetColumnValue<DateTime?>(Columns.LastInspectionCodeChangedDate); }
			set { SetColumnValue(Columns.LastInspectionCodeChangedDate, value); }
		}
		  
		[XmlAttribute("LastLockoutDate")]
		[Bindable(true)]
		public DateTime? LastLockoutDate 
		{
			get { return GetColumnValue<DateTime?>(Columns.LastLockoutDate); }
			set { SetColumnValue(Columns.LastLockoutDate, value); }
		}
		  
		[XmlAttribute("FailedPasswordAttemptCount")]
		[Bindable(true)]
		public int FailedPasswordAttemptCount 
		{
			get { return GetColumnValue<int>(Columns.FailedPasswordAttemptCount); }
			set { SetColumnValue(Columns.FailedPasswordAttemptCount, value); }
		}
		  
		[XmlAttribute("FailedPasswordAttemptWindowStart")]
		[Bindable(true)]
		public DateTime? FailedPasswordAttemptWindowStart 
		{
			get { return GetColumnValue<DateTime?>(Columns.FailedPasswordAttemptWindowStart); }
			set { SetColumnValue(Columns.FailedPasswordAttemptWindowStart, value); }
		}
		  
		[XmlAttribute("FailedInspectionCodeAttemptCount")]
		[Bindable(true)]
		public int FailedInspectionCodeAttemptCount 
		{
			get { return GetColumnValue<int>(Columns.FailedInspectionCodeAttemptCount); }
			set { SetColumnValue(Columns.FailedInspectionCodeAttemptCount, value); }
		}
		  
		[XmlAttribute("FailedInspectionCodeAttemptWindowStart")]
		[Bindable(true)]
		public DateTime? FailedInspectionCodeAttemptWindowStart 
		{
			get { return GetColumnValue<DateTime?>(Columns.FailedInspectionCodeAttemptWindowStart); }
			set { SetColumnValue(Columns.FailedInspectionCodeAttemptWindowStart, value); }
		}
		  
		[XmlAttribute("IsApproved")]
		[Bindable(true)]
		public bool IsApproved 
		{
			get { return GetColumnValue<bool>(Columns.IsApproved); }
			set { SetColumnValue(Columns.IsApproved, value); }
		}
		  
		[XmlAttribute("ViewBsRight")]
		[Bindable(true)]
		public bool ViewBsRight 
		{
			get { return GetColumnValue<bool>(Columns.ViewBsRight); }
			set { SetColumnValue(Columns.ViewBsRight, value); }
		}
		  
		[XmlAttribute("ViewVerifyRight")]
		[Bindable(true)]
		public bool ViewVerifyRight 
		{
			get { return GetColumnValue<bool>(Columns.ViewVerifyRight); }
			set { SetColumnValue(Columns.ViewVerifyRight, value); }
		}
		  
		[XmlAttribute("ViewShipRight")]
		[Bindable(true)]
		public bool ViewShipRight 
		{
			get { return GetColumnValue<bool>(Columns.ViewShipRight); }
			set { SetColumnValue(Columns.ViewShipRight, value); }
		}
		  
		[XmlAttribute("ViewBookingRight")]
		[Bindable(true)]
		public bool ViewBookingRight 
		{
			get { return GetColumnValue<bool>(Columns.ViewBookingRight); }
			set { SetColumnValue(Columns.ViewBookingRight, value); }
		}
		  
		[XmlAttribute("ViewReserveLockRight")]
		[Bindable(true)]
		public bool ViewReserveLockRight 
		{
			get { return GetColumnValue<bool>(Columns.ViewReserveLockRight); }
			set { SetColumnValue(Columns.ViewReserveLockRight, value); }
		}
		  
		[XmlAttribute("LastInfoProtectReadDate")]
		[Bindable(true)]
		public DateTime? LastInfoProtectReadDate 
		{
			get { return GetColumnValue<DateTime?>(Columns.LastInfoProtectReadDate); }
			set { SetColumnValue(Columns.LastInfoProtectReadDate, value); }
		}
		  
		[XmlAttribute("ViewUserEvaluateRight")]
		[Bindable(true)]
		public bool ViewUserEvaluateRight 
		{
			get { return GetColumnValue<bool>(Columns.ViewUserEvaluateRight); }
			set { SetColumnValue(Columns.ViewUserEvaluateRight, value); }
		}
		  
		[XmlAttribute("PersonalInfoClauseAgreeDate")]
		[Bindable(true)]
		public DateTime? PersonalInfoClauseAgreeDate 
		{
			get { return GetColumnValue<DateTime?>(Columns.PersonalInfoClauseAgreeDate); }
			set { SetColumnValue(Columns.PersonalInfoClauseAgreeDate, value); }
		}
		  
		[XmlAttribute("CompanyDetailId")]
		[Bindable(true)]
		public int? CompanyDetailId 
		{
			get { return GetColumnValue<int?>(Columns.CompanyDetailId); }
			set { SetColumnValue(Columns.CompanyDetailId, value); }
		}
		  
		[XmlAttribute("UserId")]
		[Bindable(true)]
		public int? UserId 
		{
			get { return GetColumnValue<int?>(Columns.UserId); }
			set { SetColumnValue(Columns.UserId, value); }
		}
		  
		[XmlAttribute("ViewDealScheduleRight")]
		[Bindable(true)]
		public bool ViewDealScheduleRight 
		{
			get { return GetColumnValue<bool>(Columns.ViewDealScheduleRight); }
			set { SetColumnValue(Columns.ViewDealScheduleRight, value); }
		}
		  
		[XmlAttribute("ViewProposalRight")]
		[Bindable(true)]
		public bool ViewProposalRight 
		{
			get { return GetColumnValue<bool>(Columns.ViewProposalRight); }
			set { SetColumnValue(Columns.ViewProposalRight, value); }
		}
		  
		[XmlAttribute("ProposalConsent")]
		[Bindable(true)]
		public bool ProposalConsent 
		{
			get { return GetColumnValue<bool>(Columns.ProposalConsent); }
			set { SetColumnValue(Columns.ProposalConsent, value); }
		}
		  
		[XmlAttribute("ProposalConsentDate")]
		[Bindable(true)]
		public DateTime? ProposalConsentDate 
		{
			get { return GetColumnValue<DateTime?>(Columns.ProposalConsentDate); }
			set { SetColumnValue(Columns.ProposalConsentDate, value); }
		}
		  
		[XmlAttribute("ViewShoppingCartRight")]
		[Bindable(true)]
		public bool ViewShoppingCartRight 
		{
			get { return GetColumnValue<bool>(Columns.ViewShoppingCartRight); }
			set { SetColumnValue(Columns.ViewShoppingCartRight, value); }
		}
		  
		[XmlAttribute("TemporaryPassword")]
		[Bindable(true)]
		public string TemporaryPassword 
		{
			get { return GetColumnValue<string>(Columns.TemporaryPassword); }
			set { SetColumnValue(Columns.TemporaryPassword, value); }
		}
		  
		[XmlAttribute("TemporaryPasswordDate")]
		[Bindable(true)]
		public DateTime? TemporaryPasswordDate 
		{
			get { return GetColumnValue<DateTime?>(Columns.TemporaryPasswordDate); }
			set { SetColumnValue(Columns.TemporaryPasswordDate, value); }
		}
		  
		[XmlAttribute("IsTemporaryLogin")]
		[Bindable(true)]
		public bool IsTemporaryLogin 
		{
			get { return GetColumnValue<bool>(Columns.IsTemporaryLogin); }
			set { SetColumnValue(Columns.IsTemporaryLogin, value); }
		}
		  
		[XmlAttribute("ViewToHouse")]
		[Bindable(true)]
		public bool ViewToHouse 
		{
			get { return GetColumnValue<bool>(Columns.ViewToHouse); }
			set { SetColumnValue(Columns.ViewToHouse, value); }
		}
		  
		[XmlAttribute("ViewToShop")]
		[Bindable(true)]
		public bool ViewToShop 
		{
			get { return GetColumnValue<bool>(Columns.ViewToShop); }
			set { SetColumnValue(Columns.ViewToShop, value); }
		}
		  
		[XmlAttribute("ViewProposalProductRight")]
		[Bindable(true)]
		public bool ViewProposalProductRight 
		{
			get { return GetColumnValue<bool>(Columns.ViewProposalProductRight); }
			set { SetColumnValue(Columns.ViewProposalProductRight, value); }
		}
		  
		[XmlAttribute("ViewAccountManageRight")]
		[Bindable(true)]
		public bool ViewAccountManageRight 
		{
			get { return GetColumnValue<bool>(Columns.ViewAccountManageRight); }
			set { SetColumnValue(Columns.ViewAccountManageRight, value); }
		}
		  
		[XmlAttribute("ViewCustomerServiceRight")]
		[Bindable(true)]
		public bool ViewCustomerServiceRight 
		{
			get { return GetColumnValue<bool>(Columns.ViewCustomerServiceRight); }
			set { SetColumnValue(Columns.ViewCustomerServiceRight, value); }
		}
		  
		[XmlAttribute("ViewWmsRight")]
		[Bindable(true)]
		public bool ViewWmsRight 
		{
			get { return GetColumnValue<bool>(Columns.ViewWmsRight); }
			set { SetColumnValue(Columns.ViewWmsRight, value); }
		}
		  
		[XmlAttribute("WmsContract")]
		[Bindable(true)]
		public string WmsContract 
		{
			get { return GetColumnValue<string>(Columns.WmsContract); }
			set { SetColumnValue(Columns.WmsContract, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn AccountIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn AccountTypeColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn NameColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn EmailColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn PasswordColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn PasswordFormatColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn HasInspectionCodeColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn InspectionCodeColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn IsLockedOutColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        public static TableSchema.TableColumn LastLoginDateColumn
        {
            get { return Schema.Columns[11]; }
        }
        
        
        
        public static TableSchema.TableColumn LastPasswordChangedDateColumn
        {
            get { return Schema.Columns[12]; }
        }
        
        
        
        public static TableSchema.TableColumn LastInspectionDateColumn
        {
            get { return Schema.Columns[13]; }
        }
        
        
        
        public static TableSchema.TableColumn LastInspectionCodeChangedDateColumn
        {
            get { return Schema.Columns[14]; }
        }
        
        
        
        public static TableSchema.TableColumn LastLockoutDateColumn
        {
            get { return Schema.Columns[15]; }
        }
        
        
        
        public static TableSchema.TableColumn FailedPasswordAttemptCountColumn
        {
            get { return Schema.Columns[16]; }
        }
        
        
        
        public static TableSchema.TableColumn FailedPasswordAttemptWindowStartColumn
        {
            get { return Schema.Columns[17]; }
        }
        
        
        
        public static TableSchema.TableColumn FailedInspectionCodeAttemptCountColumn
        {
            get { return Schema.Columns[18]; }
        }
        
        
        
        public static TableSchema.TableColumn FailedInspectionCodeAttemptWindowStartColumn
        {
            get { return Schema.Columns[19]; }
        }
        
        
        
        public static TableSchema.TableColumn IsApprovedColumn
        {
            get { return Schema.Columns[20]; }
        }
        
        
        
        public static TableSchema.TableColumn ViewBsRightColumn
        {
            get { return Schema.Columns[21]; }
        }
        
        
        
        public static TableSchema.TableColumn ViewVerifyRightColumn
        {
            get { return Schema.Columns[22]; }
        }
        
        
        
        public static TableSchema.TableColumn ViewShipRightColumn
        {
            get { return Schema.Columns[23]; }
        }
        
        
        
        public static TableSchema.TableColumn ViewBookingRightColumn
        {
            get { return Schema.Columns[24]; }
        }
        
        
        
        public static TableSchema.TableColumn ViewReserveLockRightColumn
        {
            get { return Schema.Columns[25]; }
        }
        
        
        
        public static TableSchema.TableColumn LastInfoProtectReadDateColumn
        {
            get { return Schema.Columns[26]; }
        }
        
        
        
        public static TableSchema.TableColumn ViewUserEvaluateRightColumn
        {
            get { return Schema.Columns[27]; }
        }
        
        
        
        public static TableSchema.TableColumn PersonalInfoClauseAgreeDateColumn
        {
            get { return Schema.Columns[28]; }
        }
        
        
        
        public static TableSchema.TableColumn CompanyDetailIdColumn
        {
            get { return Schema.Columns[29]; }
        }
        
        
        
        public static TableSchema.TableColumn UserIdColumn
        {
            get { return Schema.Columns[30]; }
        }
        
        
        
        public static TableSchema.TableColumn ViewDealScheduleRightColumn
        {
            get { return Schema.Columns[31]; }
        }
        
        
        
        public static TableSchema.TableColumn ViewProposalRightColumn
        {
            get { return Schema.Columns[32]; }
        }
        
        
        
        public static TableSchema.TableColumn ProposalConsentColumn
        {
            get { return Schema.Columns[33]; }
        }
        
        
        
        public static TableSchema.TableColumn ProposalConsentDateColumn
        {
            get { return Schema.Columns[34]; }
        }
        
        
        
        public static TableSchema.TableColumn ViewShoppingCartRightColumn
        {
            get { return Schema.Columns[35]; }
        }
        
        
        
        public static TableSchema.TableColumn TemporaryPasswordColumn
        {
            get { return Schema.Columns[36]; }
        }
        
        
        
        public static TableSchema.TableColumn TemporaryPasswordDateColumn
        {
            get { return Schema.Columns[37]; }
        }
        
        
        
        public static TableSchema.TableColumn IsTemporaryLoginColumn
        {
            get { return Schema.Columns[38]; }
        }
        
        
        
        public static TableSchema.TableColumn ViewToHouseColumn
        {
            get { return Schema.Columns[39]; }
        }
        
        
        
        public static TableSchema.TableColumn ViewToShopColumn
        {
            get { return Schema.Columns[40]; }
        }
        
        
        
        public static TableSchema.TableColumn ViewProposalProductRightColumn
        {
            get { return Schema.Columns[41]; }
        }
        
        
        
        public static TableSchema.TableColumn ViewAccountManageRightColumn
        {
            get { return Schema.Columns[42]; }
        }
        
        
        
        public static TableSchema.TableColumn ViewCustomerServiceRightColumn
        {
            get { return Schema.Columns[43]; }
        }
        
        
        
        public static TableSchema.TableColumn ViewWmsRightColumn
        {
            get { return Schema.Columns[44]; }
        }
        
        
        
        public static TableSchema.TableColumn WmsContractColumn
        {
            get { return Schema.Columns[45]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string AccountId = @"account_id";
			 public static string AccountType = @"account_type";
			 public static string Name = @"name";
			 public static string Email = @"email";
			 public static string Password = @"password";
			 public static string PasswordFormat = @"password_format";
			 public static string HasInspectionCode = @"has_inspection_code";
			 public static string InspectionCode = @"inspection_code";
			 public static string IsLockedOut = @"is_locked_out";
			 public static string CreateTime = @"create_time";
			 public static string LastLoginDate = @"last_login_date";
			 public static string LastPasswordChangedDate = @"last_password_changed_date";
			 public static string LastInspectionDate = @"last_inspection_date";
			 public static string LastInspectionCodeChangedDate = @"last_inspection_code_changed_date";
			 public static string LastLockoutDate = @"last_lockout_date";
			 public static string FailedPasswordAttemptCount = @"failed_password_attempt_count";
			 public static string FailedPasswordAttemptWindowStart = @"failed_password_attempt_window_start";
			 public static string FailedInspectionCodeAttemptCount = @"failed_inspection_code_attempt_count";
			 public static string FailedInspectionCodeAttemptWindowStart = @"failed_inspection_code_attempt_window_start";
			 public static string IsApproved = @"is_approved";
			 public static string ViewBsRight = @"view_bs_right";
			 public static string ViewVerifyRight = @"view_verify_right";
			 public static string ViewShipRight = @"view_ship_right";
			 public static string ViewBookingRight = @"view_booking_right";
			 public static string ViewReserveLockRight = @"view_reserve_lock_right";
			 public static string LastInfoProtectReadDate = @"last_info_protect_read_date";
			 public static string ViewUserEvaluateRight = @"view_user_evaluate_right";
			 public static string PersonalInfoClauseAgreeDate = @"personal_info_clause_agree_date";
			 public static string CompanyDetailId = @"company_detail_id";
			 public static string UserId = @"user_id";
			 public static string ViewDealScheduleRight = @"view_deal_schedule_right";
			 public static string ViewProposalRight = @"view_proposal_right";
			 public static string ProposalConsent = @"proposal_consent";
			 public static string ProposalConsentDate = @"proposal_consent_date";
			 public static string ViewShoppingCartRight = @"view_shopping_cart_right";
			 public static string TemporaryPassword = @"temporary_password";
			 public static string TemporaryPasswordDate = @"temporary_password_date";
			 public static string IsTemporaryLogin = @"is_temporary_login";
			 public static string ViewToHouse = @"view_to_house";
			 public static string ViewToShop = @"view_to_shop";
			 public static string ViewProposalProductRight = @"view_proposal_product_right";
			 public static string ViewAccountManageRight = @"view_account_manage_right";
			 public static string ViewCustomerServiceRight = @"view_customer_service_right";
			 public static string ViewWmsRight = @"view_wms_right";
			 public static string WmsContract = @"wms_contract";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

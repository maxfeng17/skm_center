using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewPrivilege class.
    /// </summary>
    [Serializable]
    public partial class ViewPrivilegeCollection : ReadOnlyList<ViewPrivilege, ViewPrivilegeCollection>
    {        
        public ViewPrivilegeCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_privilege view.
    /// </summary>
    [Serializable]
    public partial class ViewPrivilege : ReadOnlyRecord<ViewPrivilege>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_privilege", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarPrivilegeId = new TableSchema.TableColumn(schema);
                colvarPrivilegeId.ColumnName = "privilege_id";
                colvarPrivilegeId.DataType = DbType.Int32;
                colvarPrivilegeId.MaxLength = 0;
                colvarPrivilegeId.AutoIncrement = false;
                colvarPrivilegeId.IsNullable = false;
                colvarPrivilegeId.IsPrimaryKey = false;
                colvarPrivilegeId.IsForeignKey = false;
                colvarPrivilegeId.IsReadOnly = false;
                
                schema.Columns.Add(colvarPrivilegeId);
                
                TableSchema.TableColumn colvarFuncId = new TableSchema.TableColumn(schema);
                colvarFuncId.ColumnName = "func_id";
                colvarFuncId.DataType = DbType.Int32;
                colvarFuncId.MaxLength = 0;
                colvarFuncId.AutoIncrement = false;
                colvarFuncId.IsNullable = false;
                colvarFuncId.IsPrimaryKey = false;
                colvarFuncId.IsForeignKey = false;
                colvarFuncId.IsReadOnly = false;
                
                schema.Columns.Add(colvarFuncId);
                
                TableSchema.TableColumn colvarDisplayName = new TableSchema.TableColumn(schema);
                colvarDisplayName.ColumnName = "display_name";
                colvarDisplayName.DataType = DbType.String;
                colvarDisplayName.MaxLength = 256;
                colvarDisplayName.AutoIncrement = false;
                colvarDisplayName.IsNullable = false;
                colvarDisplayName.IsPrimaryKey = false;
                colvarDisplayName.IsForeignKey = false;
                colvarDisplayName.IsReadOnly = false;
                
                schema.Columns.Add(colvarDisplayName);
                
                TableSchema.TableColumn colvarFuncType = new TableSchema.TableColumn(schema);
                colvarFuncType.ColumnName = "func_type";
                colvarFuncType.DataType = DbType.String;
                colvarFuncType.MaxLength = 256;
                colvarFuncType.AutoIncrement = false;
                colvarFuncType.IsNullable = true;
                colvarFuncType.IsPrimaryKey = false;
                colvarFuncType.IsForeignKey = false;
                colvarFuncType.IsReadOnly = false;
                
                schema.Columns.Add(colvarFuncType);
                
                TableSchema.TableColumn colvarLink = new TableSchema.TableColumn(schema);
                colvarLink.ColumnName = "link";
                colvarLink.DataType = DbType.String;
                colvarLink.MaxLength = 256;
                colvarLink.AutoIncrement = false;
                colvarLink.IsNullable = false;
                colvarLink.IsPrimaryKey = false;
                colvarLink.IsForeignKey = false;
                colvarLink.IsReadOnly = false;
                
                schema.Columns.Add(colvarLink);
                
                TableSchema.TableColumn colvarParentFunc = new TableSchema.TableColumn(schema);
                colvarParentFunc.ColumnName = "parent_func";
                colvarParentFunc.DataType = DbType.String;
                colvarParentFunc.MaxLength = 256;
                colvarParentFunc.AutoIncrement = false;
                colvarParentFunc.IsNullable = true;
                colvarParentFunc.IsPrimaryKey = false;
                colvarParentFunc.IsForeignKey = false;
                colvarParentFunc.IsReadOnly = false;
                
                schema.Columns.Add(colvarParentFunc);
                
                TableSchema.TableColumn colvarUserName = new TableSchema.TableColumn(schema);
                colvarUserName.ColumnName = "user_name";
                colvarUserName.DataType = DbType.String;
                colvarUserName.MaxLength = 256;
                colvarUserName.AutoIncrement = false;
                colvarUserName.IsNullable = false;
                colvarUserName.IsPrimaryKey = false;
                colvarUserName.IsForeignKey = false;
                colvarUserName.IsReadOnly = false;
                
                schema.Columns.Add(colvarUserName);
                
                TableSchema.TableColumn colvarSortOrder = new TableSchema.TableColumn(schema);
                colvarSortOrder.ColumnName = "sort_order";
                colvarSortOrder.DataType = DbType.Int32;
                colvarSortOrder.MaxLength = 0;
                colvarSortOrder.AutoIncrement = false;
                colvarSortOrder.IsNullable = false;
                colvarSortOrder.IsPrimaryKey = false;
                colvarSortOrder.IsForeignKey = false;
                colvarSortOrder.IsReadOnly = false;
                
                schema.Columns.Add(colvarSortOrder);
                
                TableSchema.TableColumn colvarParentSortOrder = new TableSchema.TableColumn(schema);
                colvarParentSortOrder.ColumnName = "parent_sort_order";
                colvarParentSortOrder.DataType = DbType.Int32;
                colvarParentSortOrder.MaxLength = 0;
                colvarParentSortOrder.AutoIncrement = false;
                colvarParentSortOrder.IsNullable = false;
                colvarParentSortOrder.IsPrimaryKey = false;
                colvarParentSortOrder.IsForeignKey = false;
                colvarParentSortOrder.IsReadOnly = false;
                
                schema.Columns.Add(colvarParentSortOrder);
                
                TableSchema.TableColumn colvarVisible = new TableSchema.TableColumn(schema);
                colvarVisible.ColumnName = "visible";
                colvarVisible.DataType = DbType.Boolean;
                colvarVisible.MaxLength = 0;
                colvarVisible.AutoIncrement = false;
                colvarVisible.IsNullable = false;
                colvarVisible.IsPrimaryKey = false;
                colvarVisible.IsForeignKey = false;
                colvarVisible.IsReadOnly = false;
                
                schema.Columns.Add(colvarVisible);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_privilege",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewPrivilege()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewPrivilege(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewPrivilege(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewPrivilege(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("PrivilegeId")]
        [Bindable(true)]
        public int PrivilegeId 
	    {
		    get
		    {
			    return GetColumnValue<int>("privilege_id");
		    }
            set 
		    {
			    SetColumnValue("privilege_id", value);
            }
        }
	      
        [XmlAttribute("FuncId")]
        [Bindable(true)]
        public int FuncId 
	    {
		    get
		    {
			    return GetColumnValue<int>("func_id");
		    }
            set 
		    {
			    SetColumnValue("func_id", value);
            }
        }
	      
        [XmlAttribute("DisplayName")]
        [Bindable(true)]
        public string DisplayName 
	    {
		    get
		    {
			    return GetColumnValue<string>("display_name");
		    }
            set 
		    {
			    SetColumnValue("display_name", value);
            }
        }
	      
        [XmlAttribute("FuncType")]
        [Bindable(true)]
        public string FuncType 
	    {
		    get
		    {
			    return GetColumnValue<string>("func_type");
		    }
            set 
		    {
			    SetColumnValue("func_type", value);
            }
        }
	      
        [XmlAttribute("Link")]
        [Bindable(true)]
        public string Link 
	    {
		    get
		    {
			    return GetColumnValue<string>("link");
		    }
            set 
		    {
			    SetColumnValue("link", value);
            }
        }
	      
        [XmlAttribute("ParentFunc")]
        [Bindable(true)]
        public string ParentFunc 
	    {
		    get
		    {
			    return GetColumnValue<string>("parent_func");
		    }
            set 
		    {
			    SetColumnValue("parent_func", value);
            }
        }
	      
        [XmlAttribute("UserName")]
        [Bindable(true)]
        public string UserName 
	    {
		    get
		    {
			    return GetColumnValue<string>("user_name");
		    }
            set 
		    {
			    SetColumnValue("user_name", value);
            }
        }
	      
        [XmlAttribute("SortOrder")]
        [Bindable(true)]
        public int SortOrder 
	    {
		    get
		    {
			    return GetColumnValue<int>("sort_order");
		    }
            set 
		    {
			    SetColumnValue("sort_order", value);
            }
        }
	      
        [XmlAttribute("ParentSortOrder")]
        [Bindable(true)]
        public int ParentSortOrder 
	    {
		    get
		    {
			    return GetColumnValue<int>("parent_sort_order");
		    }
            set 
		    {
			    SetColumnValue("parent_sort_order", value);
            }
        }
	      
        [XmlAttribute("Visible")]
        [Bindable(true)]
        public bool Visible 
	    {
		    get
		    {
			    return GetColumnValue<bool>("visible");
		    }
            set 
		    {
			    SetColumnValue("visible", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string PrivilegeId = @"privilege_id";
            
            public static string FuncId = @"func_id";
            
            public static string DisplayName = @"display_name";
            
            public static string FuncType = @"func_type";
            
            public static string Link = @"link";
            
            public static string ParentFunc = @"parent_func";
            
            public static string UserName = @"user_name";
            
            public static string SortOrder = @"sort_order";
            
            public static string ParentSortOrder = @"parent_sort_order";
            
            public static string Visible = @"visible";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the AppDefaultPageDealsSetting class.
	/// </summary>
    [Serializable]
	public partial class AppDefaultPageDealsSettingCollection : RepositoryList<AppDefaultPageDealsSetting, AppDefaultPageDealsSettingCollection>
	{	   
		public AppDefaultPageDealsSettingCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>AppDefaultPageDealsSettingCollection</returns>
		public AppDefaultPageDealsSettingCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                AppDefaultPageDealsSetting o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the app_default_page_deals_setting table.
	/// </summary>
	[Serializable]
	public partial class AppDefaultPageDealsSetting : RepositoryRecord<AppDefaultPageDealsSetting>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public AppDefaultPageDealsSetting()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public AppDefaultPageDealsSetting(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("app_default_page_deals_setting", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarCategoryId = new TableSchema.TableColumn(schema);
				colvarCategoryId.ColumnName = "category_id";
				colvarCategoryId.DataType = DbType.Int32;
				colvarCategoryId.MaxLength = 0;
				colvarCategoryId.AutoIncrement = false;
				colvarCategoryId.IsNullable = false;
				colvarCategoryId.IsPrimaryKey = false;
				colvarCategoryId.IsForeignKey = false;
				colvarCategoryId.IsReadOnly = false;
				colvarCategoryId.DefaultSetting = @"";
				colvarCategoryId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCategoryId);
				
				TableSchema.TableColumn colvarTakeDealCount = new TableSchema.TableColumn(schema);
				colvarTakeDealCount.ColumnName = "take_deal_count";
				colvarTakeDealCount.DataType = DbType.Int32;
				colvarTakeDealCount.MaxLength = 0;
				colvarTakeDealCount.AutoIncrement = false;
				colvarTakeDealCount.IsNullable = false;
				colvarTakeDealCount.IsPrimaryKey = false;
				colvarTakeDealCount.IsForeignKey = false;
				colvarTakeDealCount.IsReadOnly = false;
				colvarTakeDealCount.DefaultSetting = @"";
				colvarTakeDealCount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTakeDealCount);
				
				TableSchema.TableColumn colvarModifyUser = new TableSchema.TableColumn(schema);
				colvarModifyUser.ColumnName = "modify_user";
				colvarModifyUser.DataType = DbType.String;
				colvarModifyUser.MaxLength = 50;
				colvarModifyUser.AutoIncrement = false;
				colvarModifyUser.IsNullable = false;
				colvarModifyUser.IsPrimaryKey = false;
				colvarModifyUser.IsForeignKey = false;
				colvarModifyUser.IsReadOnly = false;
				colvarModifyUser.DefaultSetting = @"";
				colvarModifyUser.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyUser);
				
				TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
				colvarModifyTime.ColumnName = "modify_time";
				colvarModifyTime.DataType = DbType.DateTime;
				colvarModifyTime.MaxLength = 0;
				colvarModifyTime.AutoIncrement = false;
				colvarModifyTime.IsNullable = false;
				colvarModifyTime.IsPrimaryKey = false;
				colvarModifyTime.IsForeignKey = false;
				colvarModifyTime.IsReadOnly = false;
				colvarModifyTime.DefaultSetting = @"";
				colvarModifyTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyTime);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("app_default_page_deals_setting",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("CategoryId")]
		[Bindable(true)]
		public int CategoryId 
		{
			get { return GetColumnValue<int>(Columns.CategoryId); }
			set { SetColumnValue(Columns.CategoryId, value); }
		}
		  
		[XmlAttribute("TakeDealCount")]
		[Bindable(true)]
		public int TakeDealCount 
		{
			get { return GetColumnValue<int>(Columns.TakeDealCount); }
			set { SetColumnValue(Columns.TakeDealCount, value); }
		}
		  
		[XmlAttribute("ModifyUser")]
		[Bindable(true)]
		public string ModifyUser 
		{
			get { return GetColumnValue<string>(Columns.ModifyUser); }
			set { SetColumnValue(Columns.ModifyUser, value); }
		}
		  
		[XmlAttribute("ModifyTime")]
		[Bindable(true)]
		public DateTime ModifyTime 
		{
			get { return GetColumnValue<DateTime>(Columns.ModifyTime); }
			set { SetColumnValue(Columns.ModifyTime, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn CategoryIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn TakeDealCountColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyUserColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string CategoryId = @"category_id";
			 public static string TakeDealCount = @"take_deal_count";
			 public static string ModifyUser = @"modify_user";
			 public static string ModifyTime = @"modify_time";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the MgmGiftHeader class.
	/// </summary>
    [Serializable]
	public partial class MgmGiftHeaderCollection : RepositoryList<MgmGiftHeader, MgmGiftHeaderCollection>
	{	   
		public MgmGiftHeaderCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>MgmGiftHeaderCollection</returns>
		public MgmGiftHeaderCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                MgmGiftHeader o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the mgm_gift_header table.
	/// </summary>
	[Serializable]
	public partial class MgmGiftHeader : RepositoryRecord<MgmGiftHeader>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public MgmGiftHeader()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public MgmGiftHeader(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("mgm_gift_header", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarTitle = new TableSchema.TableColumn(schema);
				colvarTitle.ColumnName = "title";
				colvarTitle.DataType = DbType.String;
				colvarTitle.MaxLength = 100;
				colvarTitle.AutoIncrement = false;
				colvarTitle.IsNullable = false;
				colvarTitle.IsPrimaryKey = false;
				colvarTitle.IsForeignKey = false;
				colvarTitle.IsReadOnly = false;
				colvarTitle.DefaultSetting = @"";
				colvarTitle.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTitle);
				
				TableSchema.TableColumn colvarContent = new TableSchema.TableColumn(schema);
				colvarContent.ColumnName = "content";
				colvarContent.DataType = DbType.String;
				colvarContent.MaxLength = -1;
				colvarContent.AutoIncrement = false;
				colvarContent.IsNullable = true;
				colvarContent.IsPrimaryKey = false;
				colvarContent.IsForeignKey = false;
				colvarContent.IsReadOnly = false;
				colvarContent.DefaultSetting = @"";
				colvarContent.ForeignKeyTableName = "";
				schema.Columns.Add(colvarContent);
				
				TableSchema.TableColumn colvarImageUrl = new TableSchema.TableColumn(schema);
				colvarImageUrl.ColumnName = "image_url";
				colvarImageUrl.DataType = DbType.AnsiString;
				colvarImageUrl.MaxLength = -1;
				colvarImageUrl.AutoIncrement = false;
				colvarImageUrl.IsNullable = true;
				colvarImageUrl.IsPrimaryKey = false;
				colvarImageUrl.IsForeignKey = false;
				colvarImageUrl.IsReadOnly = false;
				colvarImageUrl.DefaultSetting = @"";
				colvarImageUrl.ForeignKeyTableName = "";
				schema.Columns.Add(colvarImageUrl);
				
				TableSchema.TableColumn colvarTextColor = new TableSchema.TableColumn(schema);
				colvarTextColor.ColumnName = "text_color";
				colvarTextColor.DataType = DbType.AnsiString;
				colvarTextColor.MaxLength = 20;
				colvarTextColor.AutoIncrement = false;
				colvarTextColor.IsNullable = false;
				colvarTextColor.IsPrimaryKey = false;
				colvarTextColor.IsForeignKey = false;
				colvarTextColor.IsReadOnly = false;
				colvarTextColor.DefaultSetting = @"";
				colvarTextColor.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTextColor);
				
				TableSchema.TableColumn colvarShowType = new TableSchema.TableColumn(schema);
				colvarShowType.ColumnName = "show_type";
				colvarShowType.DataType = DbType.Int32;
				colvarShowType.MaxLength = 0;
				colvarShowType.AutoIncrement = false;
				colvarShowType.IsNullable = false;
				colvarShowType.IsPrimaryKey = false;
				colvarShowType.IsForeignKey = false;
				colvarShowType.IsReadOnly = false;
				colvarShowType.DefaultSetting = @"";
				colvarShowType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarShowType);
				
				TableSchema.TableColumn colvarOnLine = new TableSchema.TableColumn(schema);
				colvarOnLine.ColumnName = "on_line";
				colvarOnLine.DataType = DbType.Boolean;
				colvarOnLine.MaxLength = 0;
				colvarOnLine.AutoIncrement = false;
				colvarOnLine.IsNullable = false;
				colvarOnLine.IsPrimaryKey = false;
				colvarOnLine.IsForeignKey = false;
				colvarOnLine.IsReadOnly = false;
				colvarOnLine.DefaultSetting = @"";
				colvarOnLine.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOnLine);
				
				TableSchema.TableColumn colvarCreatedTime = new TableSchema.TableColumn(schema);
				colvarCreatedTime.ColumnName = "created_time";
				colvarCreatedTime.DataType = DbType.DateTime;
				colvarCreatedTime.MaxLength = 0;
				colvarCreatedTime.AutoIncrement = false;
				colvarCreatedTime.IsNullable = false;
				colvarCreatedTime.IsPrimaryKey = false;
				colvarCreatedTime.IsForeignKey = false;
				colvarCreatedTime.IsReadOnly = false;
				colvarCreatedTime.DefaultSetting = @"";
				colvarCreatedTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreatedTime);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("mgm_gift_header",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("Title")]
		[Bindable(true)]
		public string Title 
		{
			get { return GetColumnValue<string>(Columns.Title); }
			set { SetColumnValue(Columns.Title, value); }
		}
		  
		[XmlAttribute("Content")]
		[Bindable(true)]
		public string Content 
		{
			get { return GetColumnValue<string>(Columns.Content); }
			set { SetColumnValue(Columns.Content, value); }
		}
		  
		[XmlAttribute("ImageUrl")]
		[Bindable(true)]
		public string ImageUrl 
		{
			get { return GetColumnValue<string>(Columns.ImageUrl); }
			set { SetColumnValue(Columns.ImageUrl, value); }
		}
		  
		[XmlAttribute("TextColor")]
		[Bindable(true)]
		public string TextColor 
		{
			get { return GetColumnValue<string>(Columns.TextColor); }
			set { SetColumnValue(Columns.TextColor, value); }
		}
		  
		[XmlAttribute("ShowType")]
		[Bindable(true)]
		public int ShowType 
		{
			get { return GetColumnValue<int>(Columns.ShowType); }
			set { SetColumnValue(Columns.ShowType, value); }
		}
		  
		[XmlAttribute("OnLine")]
		[Bindable(true)]
		public bool OnLine 
		{
			get { return GetColumnValue<bool>(Columns.OnLine); }
			set { SetColumnValue(Columns.OnLine, value); }
		}
		  
		[XmlAttribute("CreatedTime")]
		[Bindable(true)]
		public DateTime CreatedTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreatedTime); }
			set { SetColumnValue(Columns.CreatedTime, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn TitleColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn ContentColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn ImageUrlColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn TextColorColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn ShowTypeColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn OnLineColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn CreatedTimeColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string Title = @"title";
			 public static string Content = @"content";
			 public static string ImageUrl = @"image_url";
			 public static string TextColor = @"text_color";
			 public static string ShowType = @"show_type";
			 public static string OnLine = @"on_line";
			 public static string CreatedTime = @"created_time";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

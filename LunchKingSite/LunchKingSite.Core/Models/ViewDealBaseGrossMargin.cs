using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewDealBaseGrossMargin class.
    /// </summary>
    [Serializable]
    public partial class ViewDealBaseGrossMarginCollection : ReadOnlyList<ViewDealBaseGrossMargin, ViewDealBaseGrossMarginCollection>
    {        
        public ViewDealBaseGrossMarginCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_deal_base_gross_margin view.
    /// </summary>
    [Serializable]
    public partial class ViewDealBaseGrossMargin : ReadOnlyRecord<ViewDealBaseGrossMargin>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_deal_base_gross_margin", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarMainBusinessHourGuid = new TableSchema.TableColumn(schema);
                colvarMainBusinessHourGuid.ColumnName = "MainBusinessHourGuid";
                colvarMainBusinessHourGuid.DataType = DbType.Guid;
                colvarMainBusinessHourGuid.MaxLength = 0;
                colvarMainBusinessHourGuid.AutoIncrement = false;
                colvarMainBusinessHourGuid.IsNullable = true;
                colvarMainBusinessHourGuid.IsPrimaryKey = false;
                colvarMainBusinessHourGuid.IsForeignKey = false;
                colvarMainBusinessHourGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarMainBusinessHourGuid);
                
                TableSchema.TableColumn colvarBusinessHourGuid = new TableSchema.TableColumn(schema);
                colvarBusinessHourGuid.ColumnName = "business_hour_guid";
                colvarBusinessHourGuid.DataType = DbType.Guid;
                colvarBusinessHourGuid.MaxLength = 0;
                colvarBusinessHourGuid.AutoIncrement = false;
                colvarBusinessHourGuid.IsNullable = false;
                colvarBusinessHourGuid.IsPrimaryKey = false;
                colvarBusinessHourGuid.IsForeignKey = false;
                colvarBusinessHourGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourGuid);
                
                TableSchema.TableColumn colvarGrossMargin = new TableSchema.TableColumn(schema);
                colvarGrossMargin.ColumnName = "gross_margin";
                colvarGrossMargin.DataType = DbType.Currency;
                colvarGrossMargin.MaxLength = 0;
                colvarGrossMargin.AutoIncrement = false;
                colvarGrossMargin.IsNullable = true;
                colvarGrossMargin.IsPrimaryKey = false;
                colvarGrossMargin.IsForeignKey = false;
                colvarGrossMargin.IsReadOnly = false;
                
                schema.Columns.Add(colvarGrossMargin);
                
                TableSchema.TableColumn colvarTaxGrossMargin = new TableSchema.TableColumn(schema);
                colvarTaxGrossMargin.ColumnName = "tax_gross_margin";
                colvarTaxGrossMargin.DataType = DbType.Decimal;
                colvarTaxGrossMargin.MaxLength = 0;
                colvarTaxGrossMargin.AutoIncrement = false;
                colvarTaxGrossMargin.IsNullable = true;
                colvarTaxGrossMargin.IsPrimaryKey = false;
                colvarTaxGrossMargin.IsForeignKey = false;
                colvarTaxGrossMargin.IsReadOnly = false;
                
                schema.Columns.Add(colvarTaxGrossMargin);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_deal_base_gross_margin",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewDealBaseGrossMargin()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewDealBaseGrossMargin(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewDealBaseGrossMargin(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewDealBaseGrossMargin(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("MainBusinessHourGuid")]
        [Bindable(true)]
        public Guid? MainBusinessHourGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid?>("MainBusinessHourGuid");
		    }
            set 
		    {
			    SetColumnValue("MainBusinessHourGuid", value);
            }
        }
	      
        [XmlAttribute("BusinessHourGuid")]
        [Bindable(true)]
        public Guid BusinessHourGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("business_hour_guid");
		    }
            set 
		    {
			    SetColumnValue("business_hour_guid", value);
            }
        }
	      
        [XmlAttribute("GrossMargin")]
        [Bindable(true)]
        public decimal? GrossMargin 
	    {
		    get
		    {
			    return GetColumnValue<decimal?>("gross_margin");
		    }
            set 
		    {
			    SetColumnValue("gross_margin", value);
            }
        }
	      
        [XmlAttribute("TaxGrossMargin")]
        [Bindable(true)]
        public decimal? TaxGrossMargin 
	    {
		    get
		    {
			    return GetColumnValue<decimal?>("tax_gross_margin");
		    }
            set 
		    {
			    SetColumnValue("tax_gross_margin", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string MainBusinessHourGuid = @"MainBusinessHourGuid";
            
            public static string BusinessHourGuid = @"business_hour_guid";
            
            public static string GrossMargin = @"gross_margin";
            
            public static string TaxGrossMargin = @"tax_gross_margin";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the LoginTicket class.
	/// </summary>
    [Serializable]
	public partial class LoginTicketCollection : RepositoryList<LoginTicket, LoginTicketCollection>
	{	   
		public LoginTicketCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>LoginTicketCollection</returns>
		public LoginTicketCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                LoginTicket o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the login_ticket table.
	/// </summary>
	[Serializable]
	public partial class LoginTicket : RepositoryRecord<LoginTicket>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public LoginTicket()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public LoginTicket(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("login_ticket", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarTicketId = new TableSchema.TableColumn(schema);
				colvarTicketId.ColumnName = "ticket_id";
				colvarTicketId.DataType = DbType.AnsiString;
				colvarTicketId.MaxLength = 256;
				colvarTicketId.AutoIncrement = false;
				colvarTicketId.IsNullable = false;
				colvarTicketId.IsPrimaryKey = false;
				colvarTicketId.IsForeignKey = false;
				colvarTicketId.IsReadOnly = false;
				colvarTicketId.DefaultSetting = @"";
				colvarTicketId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTicketId);
				
				TableSchema.TableColumn colvarUserId = new TableSchema.TableColumn(schema);
				colvarUserId.ColumnName = "user_id";
				colvarUserId.DataType = DbType.Int32;
				colvarUserId.MaxLength = 0;
				colvarUserId.AutoIncrement = false;
				colvarUserId.IsNullable = false;
				colvarUserId.IsPrimaryKey = false;
				colvarUserId.IsForeignKey = false;
				colvarUserId.IsReadOnly = false;
				colvarUserId.DefaultSetting = @"";
				colvarUserId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUserId);
				
				TableSchema.TableColumn colvarDriverId = new TableSchema.TableColumn(schema);
				colvarDriverId.ColumnName = "driver_id";
				colvarDriverId.DataType = DbType.AnsiString;
				colvarDriverId.MaxLength = 50;
				colvarDriverId.AutoIncrement = false;
				colvarDriverId.IsNullable = false;
				colvarDriverId.IsPrimaryKey = false;
				colvarDriverId.IsForeignKey = false;
				colvarDriverId.IsReadOnly = false;
				colvarDriverId.DefaultSetting = @"";
				colvarDriverId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDriverId);
				
				TableSchema.TableColumn colvarUserName = new TableSchema.TableColumn(schema);
				colvarUserName.ColumnName = "user_name";
				colvarUserName.DataType = DbType.String;
				colvarUserName.MaxLength = 256;
				colvarUserName.AutoIncrement = false;
				colvarUserName.IsNullable = false;
				colvarUserName.IsPrimaryKey = false;
				colvarUserName.IsForeignKey = false;
				colvarUserName.IsReadOnly = false;
				colvarUserName.DefaultSetting = @"";
				colvarUserName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUserName);
				
				TableSchema.TableColumn colvarClientIp = new TableSchema.TableColumn(schema);
				colvarClientIp.ColumnName = "client_ip";
				colvarClientIp.DataType = DbType.AnsiString;
				colvarClientIp.MaxLength = 34;
				colvarClientIp.AutoIncrement = false;
				colvarClientIp.IsNullable = false;
				colvarClientIp.IsPrimaryKey = false;
				colvarClientIp.IsForeignKey = false;
				colvarClientIp.IsReadOnly = false;
				colvarClientIp.DefaultSetting = @"";
				colvarClientIp.ForeignKeyTableName = "";
				schema.Columns.Add(colvarClientIp);
				
				TableSchema.TableColumn colvarExternalOrg = new TableSchema.TableColumn(schema);
				colvarExternalOrg.ColumnName = "external_org";
				colvarExternalOrg.DataType = DbType.Int32;
				colvarExternalOrg.MaxLength = 0;
				colvarExternalOrg.AutoIncrement = false;
				colvarExternalOrg.IsNullable = false;
				colvarExternalOrg.IsPrimaryKey = false;
				colvarExternalOrg.IsForeignKey = false;
				colvarExternalOrg.IsReadOnly = false;
				colvarExternalOrg.DefaultSetting = @"";
				colvarExternalOrg.ForeignKeyTableName = "";
				schema.Columns.Add(colvarExternalOrg);
				
				TableSchema.TableColumn colvarLoginTime = new TableSchema.TableColumn(schema);
				colvarLoginTime.ColumnName = "login_time";
				colvarLoginTime.DataType = DbType.DateTime;
				colvarLoginTime.MaxLength = 0;
				colvarLoginTime.AutoIncrement = false;
				colvarLoginTime.IsNullable = false;
				colvarLoginTime.IsPrimaryKey = false;
				colvarLoginTime.IsForeignKey = false;
				colvarLoginTime.IsReadOnly = false;
				colvarLoginTime.DefaultSetting = @"";
				colvarLoginTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarLoginTime);
				
				TableSchema.TableColumn colvarExpirationTime = new TableSchema.TableColumn(schema);
				colvarExpirationTime.ColumnName = "expiration_time";
				colvarExpirationTime.DataType = DbType.DateTime;
				colvarExpirationTime.MaxLength = 0;
				colvarExpirationTime.AutoIncrement = false;
				colvarExpirationTime.IsNullable = false;
				colvarExpirationTime.IsPrimaryKey = false;
				colvarExpirationTime.IsForeignKey = false;
				colvarExpirationTime.IsReadOnly = false;
				colvarExpirationTime.DefaultSetting = @"";
				colvarExpirationTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarExpirationTime);
				
				TableSchema.TableColumn colvarLoginTicketType = new TableSchema.TableColumn(schema);
				colvarLoginTicketType.ColumnName = "login_ticket_type";
				colvarLoginTicketType.DataType = DbType.Int32;
				colvarLoginTicketType.MaxLength = 0;
				colvarLoginTicketType.AutoIncrement = false;
				colvarLoginTicketType.IsNullable = false;
				colvarLoginTicketType.IsPrimaryKey = false;
				colvarLoginTicketType.IsForeignKey = false;
				colvarLoginTicketType.IsReadOnly = false;
				
						colvarLoginTicketType.DefaultSetting = @"((0))";
				colvarLoginTicketType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarLoginTicketType);
				
				TableSchema.TableColumn colvarPrevTicketId = new TableSchema.TableColumn(schema);
				colvarPrevTicketId.ColumnName = "prev_ticket_id";
				colvarPrevTicketId.DataType = DbType.String;
				colvarPrevTicketId.MaxLength = 256;
				colvarPrevTicketId.AutoIncrement = false;
				colvarPrevTicketId.IsNullable = true;
				colvarPrevTicketId.IsPrimaryKey = false;
				colvarPrevTicketId.IsForeignKey = false;
				colvarPrevTicketId.IsReadOnly = false;
				colvarPrevTicketId.DefaultSetting = @"";
				colvarPrevTicketId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPrevTicketId);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("login_ticket",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("TicketId")]
		[Bindable(true)]
		public string TicketId 
		{
			get { return GetColumnValue<string>(Columns.TicketId); }
			set { SetColumnValue(Columns.TicketId, value); }
		}
		  
		[XmlAttribute("UserId")]
		[Bindable(true)]
		public int UserId 
		{
			get { return GetColumnValue<int>(Columns.UserId); }
			set { SetColumnValue(Columns.UserId, value); }
		}
		  
		[XmlAttribute("DriverId")]
		[Bindable(true)]
		public string DriverId 
		{
			get { return GetColumnValue<string>(Columns.DriverId); }
			set { SetColumnValue(Columns.DriverId, value); }
		}
		  
		[XmlAttribute("UserName")]
		[Bindable(true)]
		public string UserName 
		{
			get { return GetColumnValue<string>(Columns.UserName); }
			set { SetColumnValue(Columns.UserName, value); }
		}
		  
		[XmlAttribute("ClientIp")]
		[Bindable(true)]
		public string ClientIp 
		{
			get { return GetColumnValue<string>(Columns.ClientIp); }
			set { SetColumnValue(Columns.ClientIp, value); }
		}
		  
		[XmlAttribute("ExternalOrg")]
		[Bindable(true)]
		public int ExternalOrg 
		{
			get { return GetColumnValue<int>(Columns.ExternalOrg); }
			set { SetColumnValue(Columns.ExternalOrg, value); }
		}
		  
		[XmlAttribute("LoginTime")]
		[Bindable(true)]
		public DateTime LoginTime 
		{
			get { return GetColumnValue<DateTime>(Columns.LoginTime); }
			set { SetColumnValue(Columns.LoginTime, value); }
		}
		  
		[XmlAttribute("ExpirationTime")]
		[Bindable(true)]
		public DateTime ExpirationTime 
		{
			get { return GetColumnValue<DateTime>(Columns.ExpirationTime); }
			set { SetColumnValue(Columns.ExpirationTime, value); }
		}
		  
		[XmlAttribute("LoginTicketType")]
		[Bindable(true)]
		public int LoginTicketType 
		{
			get { return GetColumnValue<int>(Columns.LoginTicketType); }
			set { SetColumnValue(Columns.LoginTicketType, value); }
		}
		  
		[XmlAttribute("PrevTicketId")]
		[Bindable(true)]
		public string PrevTicketId 
		{
			get { return GetColumnValue<string>(Columns.PrevTicketId); }
			set { SetColumnValue(Columns.PrevTicketId, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn TicketIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn UserIdColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn DriverIdColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn UserNameColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn ClientIpColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn ExternalOrgColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn LoginTimeColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn ExpirationTimeColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn LoginTicketTypeColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn PrevTicketIdColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string TicketId = @"ticket_id";
			 public static string UserId = @"user_id";
			 public static string DriverId = @"driver_id";
			 public static string UserName = @"user_name";
			 public static string ClientIp = @"client_ip";
			 public static string ExternalOrg = @"external_org";
			 public static string LoginTime = @"login_time";
			 public static string ExpirationTime = @"expiration_time";
			 public static string LoginTicketType = @"login_ticket_type";
			 public static string PrevTicketId = @"prev_ticket_id";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

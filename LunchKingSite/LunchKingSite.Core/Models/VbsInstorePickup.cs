using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the VbsInstorePickup class.
	/// </summary>
    [Serializable]
	public partial class VbsInstorePickupCollection : RepositoryList<VbsInstorePickup, VbsInstorePickupCollection>
	{	   
		public VbsInstorePickupCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>VbsInstorePickupCollection</returns>
		public VbsInstorePickupCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                VbsInstorePickup o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the vbs_instore_pickup table.
	/// </summary>
	[Serializable]
	public partial class VbsInstorePickup : RepositoryRecord<VbsInstorePickup>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public VbsInstorePickup()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public VbsInstorePickup(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("vbs_instore_pickup", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarSellerGuid = new TableSchema.TableColumn(schema);
				colvarSellerGuid.ColumnName = "seller_guid";
				colvarSellerGuid.DataType = DbType.Guid;
				colvarSellerGuid.MaxLength = 0;
				colvarSellerGuid.AutoIncrement = false;
				colvarSellerGuid.IsNullable = false;
				colvarSellerGuid.IsPrimaryKey = false;
				colvarSellerGuid.IsForeignKey = false;
				colvarSellerGuid.IsReadOnly = false;
				colvarSellerGuid.DefaultSetting = @"";
				colvarSellerGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSellerGuid);
				
				TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
				colvarStatus.ColumnName = "status";
				colvarStatus.DataType = DbType.Int32;
				colvarStatus.MaxLength = 0;
				colvarStatus.AutoIncrement = false;
				colvarStatus.IsNullable = false;
				colvarStatus.IsPrimaryKey = false;
				colvarStatus.IsForeignKey = false;
				colvarStatus.IsReadOnly = false;
				colvarStatus.DefaultSetting = @"";
				colvarStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStatus);
				
				TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
				colvarCreateId.ColumnName = "create_id";
				colvarCreateId.DataType = DbType.String;
				colvarCreateId.MaxLength = 50;
				colvarCreateId.AutoIncrement = false;
				colvarCreateId.IsNullable = false;
				colvarCreateId.IsPrimaryKey = false;
				colvarCreateId.IsForeignKey = false;
				colvarCreateId.IsReadOnly = false;
				colvarCreateId.DefaultSetting = @"";
				colvarCreateId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateId);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarReturnCycle = new TableSchema.TableColumn(schema);
				colvarReturnCycle.ColumnName = "return_cycle";
				colvarReturnCycle.DataType = DbType.Int32;
				colvarReturnCycle.MaxLength = 0;
				colvarReturnCycle.AutoIncrement = false;
				colvarReturnCycle.IsNullable = true;
				colvarReturnCycle.IsPrimaryKey = false;
				colvarReturnCycle.IsForeignKey = false;
				colvarReturnCycle.IsReadOnly = false;
				colvarReturnCycle.DefaultSetting = @"";
				colvarReturnCycle.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReturnCycle);
				
				TableSchema.TableColumn colvarReturnType = new TableSchema.TableColumn(schema);
				colvarReturnType.ColumnName = "return_type";
				colvarReturnType.DataType = DbType.Int32;
				colvarReturnType.MaxLength = 0;
				colvarReturnType.AutoIncrement = false;
				colvarReturnType.IsNullable = true;
				colvarReturnType.IsPrimaryKey = false;
				colvarReturnType.IsForeignKey = false;
				colvarReturnType.IsReadOnly = false;
				colvarReturnType.DefaultSetting = @"";
				colvarReturnType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReturnType);
				
				TableSchema.TableColumn colvarReturnOther = new TableSchema.TableColumn(schema);
				colvarReturnOther.ColumnName = "return_other";
				colvarReturnOther.DataType = DbType.String;
				colvarReturnOther.MaxLength = -1;
				colvarReturnOther.AutoIncrement = false;
				colvarReturnOther.IsNullable = true;
				colvarReturnOther.IsPrimaryKey = false;
				colvarReturnOther.IsForeignKey = false;
				colvarReturnOther.IsReadOnly = false;
				colvarReturnOther.DefaultSetting = @"";
				colvarReturnOther.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReturnOther);
				
				TableSchema.TableColumn colvarServiceChannel = new TableSchema.TableColumn(schema);
				colvarServiceChannel.ColumnName = "service_channel";
				colvarServiceChannel.DataType = DbType.Int32;
				colvarServiceChannel.MaxLength = 0;
				colvarServiceChannel.AutoIncrement = false;
				colvarServiceChannel.IsNullable = false;
				colvarServiceChannel.IsPrimaryKey = false;
				colvarServiceChannel.IsForeignKey = false;
				colvarServiceChannel.IsReadOnly = false;
				colvarServiceChannel.DefaultSetting = @"";
				colvarServiceChannel.ForeignKeyTableName = "";
				schema.Columns.Add(colvarServiceChannel);
				
				TableSchema.TableColumn colvarVerifyTime = new TableSchema.TableColumn(schema);
				colvarVerifyTime.ColumnName = "verify_time";
				colvarVerifyTime.DataType = DbType.DateTime;
				colvarVerifyTime.MaxLength = 0;
				colvarVerifyTime.AutoIncrement = false;
				colvarVerifyTime.IsNullable = true;
				colvarVerifyTime.IsPrimaryKey = false;
				colvarVerifyTime.IsForeignKey = false;
				colvarVerifyTime.IsReadOnly = false;
				colvarVerifyTime.DefaultSetting = @"";
				colvarVerifyTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarVerifyTime);
				
				TableSchema.TableColumn colvarVerifyMemo = new TableSchema.TableColumn(schema);
				colvarVerifyMemo.ColumnName = "verify_memo";
				colvarVerifyMemo.DataType = DbType.String;
				colvarVerifyMemo.MaxLength = -1;
				colvarVerifyMemo.AutoIncrement = false;
				colvarVerifyMemo.IsNullable = true;
				colvarVerifyMemo.IsPrimaryKey = false;
				colvarVerifyMemo.IsForeignKey = false;
				colvarVerifyMemo.IsReadOnly = false;
				colvarVerifyMemo.DefaultSetting = @"";
				colvarVerifyMemo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarVerifyMemo);
				
				TableSchema.TableColumn colvarStoreCreateTime = new TableSchema.TableColumn(schema);
				colvarStoreCreateTime.ColumnName = "store_create_time";
				colvarStoreCreateTime.DataType = DbType.DateTime;
				colvarStoreCreateTime.MaxLength = 0;
				colvarStoreCreateTime.AutoIncrement = false;
				colvarStoreCreateTime.IsNullable = true;
				colvarStoreCreateTime.IsPrimaryKey = false;
				colvarStoreCreateTime.IsForeignKey = false;
				colvarStoreCreateTime.IsReadOnly = false;
				colvarStoreCreateTime.DefaultSetting = @"";
				colvarStoreCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStoreCreateTime);
				
				TableSchema.TableColumn colvarStoreSubCode = new TableSchema.TableColumn(schema);
				colvarStoreSubCode.ColumnName = "store_sub_code";
				colvarStoreSubCode.DataType = DbType.String;
				colvarStoreSubCode.MaxLength = 20;
				colvarStoreSubCode.AutoIncrement = false;
				colvarStoreSubCode.IsNullable = true;
				colvarStoreSubCode.IsPrimaryKey = false;
				colvarStoreSubCode.IsForeignKey = false;
				colvarStoreSubCode.IsReadOnly = false;
				colvarStoreSubCode.DefaultSetting = @"";
				colvarStoreSubCode.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStoreSubCode);
				
				TableSchema.TableColumn colvarCheckStatus = new TableSchema.TableColumn(schema);
				colvarCheckStatus.ColumnName = "check_status";
				colvarCheckStatus.DataType = DbType.Int32;
				colvarCheckStatus.MaxLength = 0;
				colvarCheckStatus.AutoIncrement = false;
				colvarCheckStatus.IsNullable = true;
				colvarCheckStatus.IsPrimaryKey = false;
				colvarCheckStatus.IsForeignKey = false;
				colvarCheckStatus.IsReadOnly = false;
				colvarCheckStatus.DefaultSetting = @"";
				colvarCheckStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCheckStatus);
				
				TableSchema.TableColumn colvarCheckFinishTime = new TableSchema.TableColumn(schema);
				colvarCheckFinishTime.ColumnName = "check_finish_time";
				colvarCheckFinishTime.DataType = DbType.DateTime;
				colvarCheckFinishTime.MaxLength = 0;
				colvarCheckFinishTime.AutoIncrement = false;
				colvarCheckFinishTime.IsNullable = true;
				colvarCheckFinishTime.IsPrimaryKey = false;
				colvarCheckFinishTime.IsForeignKey = false;
				colvarCheckFinishTime.IsReadOnly = false;
				colvarCheckFinishTime.DefaultSetting = @"";
				colvarCheckFinishTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCheckFinishTime);
				
				TableSchema.TableColumn colvarFinishTime = new TableSchema.TableColumn(schema);
				colvarFinishTime.ColumnName = "finish_time";
				colvarFinishTime.DataType = DbType.DateTime;
				colvarFinishTime.MaxLength = 0;
				colvarFinishTime.AutoIncrement = false;
				colvarFinishTime.IsNullable = true;
				colvarFinishTime.IsPrimaryKey = false;
				colvarFinishTime.IsForeignKey = false;
				colvarFinishTime.IsReadOnly = false;
				colvarFinishTime.DefaultSetting = @"";
				colvarFinishTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFinishTime);
				
				TableSchema.TableColumn colvarIsEnabled = new TableSchema.TableColumn(schema);
				colvarIsEnabled.ColumnName = "is_enabled";
				colvarIsEnabled.DataType = DbType.Boolean;
				colvarIsEnabled.MaxLength = 0;
				colvarIsEnabled.AutoIncrement = false;
				colvarIsEnabled.IsNullable = true;
				colvarIsEnabled.IsPrimaryKey = false;
				colvarIsEnabled.IsForeignKey = false;
				colvarIsEnabled.IsReadOnly = false;
				colvarIsEnabled.DefaultSetting = @"";
				colvarIsEnabled.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsEnabled);
				
				TableSchema.TableColumn colvarModifyId = new TableSchema.TableColumn(schema);
				colvarModifyId.ColumnName = "modify_id";
				colvarModifyId.DataType = DbType.String;
				colvarModifyId.MaxLength = 50;
				colvarModifyId.AutoIncrement = false;
				colvarModifyId.IsNullable = true;
				colvarModifyId.IsPrimaryKey = false;
				colvarModifyId.IsForeignKey = false;
				colvarModifyId.IsReadOnly = false;
				colvarModifyId.DefaultSetting = @"";
				colvarModifyId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyId);
				
				TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
				colvarModifyTime.ColumnName = "modify_time";
				colvarModifyTime.DataType = DbType.DateTime;
				colvarModifyTime.MaxLength = 0;
				colvarModifyTime.AutoIncrement = false;
				colvarModifyTime.IsNullable = true;
				colvarModifyTime.IsPrimaryKey = false;
				colvarModifyTime.IsForeignKey = false;
				colvarModifyTime.IsReadOnly = false;
				colvarModifyTime.DefaultSetting = @"";
				colvarModifyTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyTime);
				
				TableSchema.TableColumn colvarContacts = new TableSchema.TableColumn(schema);
				colvarContacts.ColumnName = "contacts";
				colvarContacts.DataType = DbType.String;
				colvarContacts.MaxLength = 500;
				colvarContacts.AutoIncrement = false;
				colvarContacts.IsNullable = true;
				colvarContacts.IsPrimaryKey = false;
				colvarContacts.IsForeignKey = false;
				colvarContacts.IsReadOnly = false;
				colvarContacts.DefaultSetting = @"";
				colvarContacts.ForeignKeyTableName = "";
				schema.Columns.Add(colvarContacts);
				
				TableSchema.TableColumn colvarSellerAddress = new TableSchema.TableColumn(schema);
				colvarSellerAddress.ColumnName = "seller_address";
				colvarSellerAddress.DataType = DbType.String;
				colvarSellerAddress.MaxLength = 500;
				colvarSellerAddress.AutoIncrement = false;
				colvarSellerAddress.IsNullable = true;
				colvarSellerAddress.IsPrimaryKey = false;
				colvarSellerAddress.IsForeignKey = false;
				colvarSellerAddress.IsReadOnly = false;
				colvarSellerAddress.DefaultSetting = @"";
				colvarSellerAddress.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSellerAddress);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("vbs_instore_pickup",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("SellerGuid")]
		[Bindable(true)]
		public Guid SellerGuid 
		{
			get { return GetColumnValue<Guid>(Columns.SellerGuid); }
			set { SetColumnValue(Columns.SellerGuid, value); }
		}
		  
		[XmlAttribute("Status")]
		[Bindable(true)]
		public int Status 
		{
			get { return GetColumnValue<int>(Columns.Status); }
			set { SetColumnValue(Columns.Status, value); }
		}
		  
		[XmlAttribute("CreateId")]
		[Bindable(true)]
		public string CreateId 
		{
			get { return GetColumnValue<string>(Columns.CreateId); }
			set { SetColumnValue(Columns.CreateId, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		  
		[XmlAttribute("ReturnCycle")]
		[Bindable(true)]
		public int? ReturnCycle 
		{
			get { return GetColumnValue<int?>(Columns.ReturnCycle); }
			set { SetColumnValue(Columns.ReturnCycle, value); }
		}
		  
		[XmlAttribute("ReturnType")]
		[Bindable(true)]
		public int? ReturnType 
		{
			get { return GetColumnValue<int?>(Columns.ReturnType); }
			set { SetColumnValue(Columns.ReturnType, value); }
		}
		  
		[XmlAttribute("ReturnOther")]
		[Bindable(true)]
		public string ReturnOther 
		{
			get { return GetColumnValue<string>(Columns.ReturnOther); }
			set { SetColumnValue(Columns.ReturnOther, value); }
		}
		  
		[XmlAttribute("ServiceChannel")]
		[Bindable(true)]
		public int ServiceChannel 
		{
			get { return GetColumnValue<int>(Columns.ServiceChannel); }
			set { SetColumnValue(Columns.ServiceChannel, value); }
		}
		  
		[XmlAttribute("VerifyTime")]
		[Bindable(true)]
		public DateTime? VerifyTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.VerifyTime); }
			set { SetColumnValue(Columns.VerifyTime, value); }
		}
		  
		[XmlAttribute("VerifyMemo")]
		[Bindable(true)]
		public string VerifyMemo 
		{
			get { return GetColumnValue<string>(Columns.VerifyMemo); }
			set { SetColumnValue(Columns.VerifyMemo, value); }
		}
		  
		[XmlAttribute("StoreCreateTime")]
		[Bindable(true)]
		public DateTime? StoreCreateTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.StoreCreateTime); }
			set { SetColumnValue(Columns.StoreCreateTime, value); }
		}
		  
		[XmlAttribute("StoreSubCode")]
		[Bindable(true)]
		public string StoreSubCode 
		{
			get { return GetColumnValue<string>(Columns.StoreSubCode); }
			set { SetColumnValue(Columns.StoreSubCode, value); }
		}
		  
		[XmlAttribute("CheckStatus")]
		[Bindable(true)]
		public int? CheckStatus 
		{
			get { return GetColumnValue<int?>(Columns.CheckStatus); }
			set { SetColumnValue(Columns.CheckStatus, value); }
		}
		  
		[XmlAttribute("CheckFinishTime")]
		[Bindable(true)]
		public DateTime? CheckFinishTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.CheckFinishTime); }
			set { SetColumnValue(Columns.CheckFinishTime, value); }
		}
		  
		[XmlAttribute("FinishTime")]
		[Bindable(true)]
		public DateTime? FinishTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.FinishTime); }
			set { SetColumnValue(Columns.FinishTime, value); }
		}
		  
		[XmlAttribute("IsEnabled")]
		[Bindable(true)]
		public bool? IsEnabled 
		{
			get { return GetColumnValue<bool?>(Columns.IsEnabled); }
			set { SetColumnValue(Columns.IsEnabled, value); }
		}
		  
		[XmlAttribute("ModifyId")]
		[Bindable(true)]
		public string ModifyId 
		{
			get { return GetColumnValue<string>(Columns.ModifyId); }
			set { SetColumnValue(Columns.ModifyId, value); }
		}
		  
		[XmlAttribute("ModifyTime")]
		[Bindable(true)]
		public DateTime? ModifyTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.ModifyTime); }
			set { SetColumnValue(Columns.ModifyTime, value); }
		}
		  
		[XmlAttribute("Contacts")]
		[Bindable(true)]
		public string Contacts 
		{
			get { return GetColumnValue<string>(Columns.Contacts); }
			set { SetColumnValue(Columns.Contacts, value); }
		}
		  
		[XmlAttribute("SellerAddress")]
		[Bindable(true)]
		public string SellerAddress 
		{
			get { return GetColumnValue<string>(Columns.SellerAddress); }
			set { SetColumnValue(Columns.SellerAddress, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn SellerGuidColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn StatusColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn ReturnCycleColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn ReturnTypeColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn ReturnOtherColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn ServiceChannelColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn VerifyTimeColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn VerifyMemoColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        public static TableSchema.TableColumn StoreCreateTimeColumn
        {
            get { return Schema.Columns[11]; }
        }
        
        
        
        public static TableSchema.TableColumn StoreSubCodeColumn
        {
            get { return Schema.Columns[12]; }
        }
        
        
        
        public static TableSchema.TableColumn CheckStatusColumn
        {
            get { return Schema.Columns[13]; }
        }
        
        
        
        public static TableSchema.TableColumn CheckFinishTimeColumn
        {
            get { return Schema.Columns[14]; }
        }
        
        
        
        public static TableSchema.TableColumn FinishTimeColumn
        {
            get { return Schema.Columns[15]; }
        }
        
        
        
        public static TableSchema.TableColumn IsEnabledColumn
        {
            get { return Schema.Columns[16]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyIdColumn
        {
            get { return Schema.Columns[17]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[18]; }
        }
        
        
        
        public static TableSchema.TableColumn ContactsColumn
        {
            get { return Schema.Columns[19]; }
        }
        
        
        
        public static TableSchema.TableColumn SellerAddressColumn
        {
            get { return Schema.Columns[20]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string SellerGuid = @"seller_guid";
			 public static string Status = @"status";
			 public static string CreateId = @"create_id";
			 public static string CreateTime = @"create_time";
			 public static string ReturnCycle = @"return_cycle";
			 public static string ReturnType = @"return_type";
			 public static string ReturnOther = @"return_other";
			 public static string ServiceChannel = @"service_channel";
			 public static string VerifyTime = @"verify_time";
			 public static string VerifyMemo = @"verify_memo";
			 public static string StoreCreateTime = @"store_create_time";
			 public static string StoreSubCode = @"store_sub_code";
			 public static string CheckStatus = @"check_status";
			 public static string CheckFinishTime = @"check_finish_time";
			 public static string FinishTime = @"finish_time";
			 public static string IsEnabled = @"is_enabled";
			 public static string ModifyId = @"modify_id";
			 public static string ModifyTime = @"modify_time";
			 public static string Contacts = @"contacts";
			 public static string SellerAddress = @"seller_address";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewThemeCuration class.
    /// </summary>
    [Serializable]
    public partial class ViewThemeCurationCollection : ReadOnlyList<ViewThemeCuration, ViewThemeCurationCollection>
    {        
        public ViewThemeCurationCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_theme_curation view.
    /// </summary>
    [Serializable]
    public partial class ViewThemeCuration : ReadOnlyRecord<ViewThemeCuration>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_theme_curation", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarMainId = new TableSchema.TableColumn(schema);
                colvarMainId.ColumnName = "main_id";
                colvarMainId.DataType = DbType.Int32;
                colvarMainId.MaxLength = 0;
                colvarMainId.AutoIncrement = false;
                colvarMainId.IsNullable = false;
                colvarMainId.IsPrimaryKey = false;
                colvarMainId.IsForeignKey = false;
                colvarMainId.IsReadOnly = false;
                
                schema.Columns.Add(colvarMainId);
                
                TableSchema.TableColumn colvarMainStart = new TableSchema.TableColumn(schema);
                colvarMainStart.ColumnName = "main_start";
                colvarMainStart.DataType = DbType.DateTime;
                colvarMainStart.MaxLength = 0;
                colvarMainStart.AutoIncrement = false;
                colvarMainStart.IsNullable = false;
                colvarMainStart.IsPrimaryKey = false;
                colvarMainStart.IsForeignKey = false;
                colvarMainStart.IsReadOnly = false;
                
                schema.Columns.Add(colvarMainStart);
                
                TableSchema.TableColumn colvarMainEnd = new TableSchema.TableColumn(schema);
                colvarMainEnd.ColumnName = "main_end";
                colvarMainEnd.DataType = DbType.DateTime;
                colvarMainEnd.MaxLength = 0;
                colvarMainEnd.AutoIncrement = false;
                colvarMainEnd.IsNullable = false;
                colvarMainEnd.IsPrimaryKey = false;
                colvarMainEnd.IsForeignKey = false;
                colvarMainEnd.IsReadOnly = false;
                
                schema.Columns.Add(colvarMainEnd);
                
                TableSchema.TableColumn colvarMainTitle = new TableSchema.TableColumn(schema);
                colvarMainTitle.ColumnName = "main_title";
                colvarMainTitle.DataType = DbType.String;
                colvarMainTitle.MaxLength = 100;
                colvarMainTitle.AutoIncrement = false;
                colvarMainTitle.IsNullable = false;
                colvarMainTitle.IsPrimaryKey = false;
                colvarMainTitle.IsForeignKey = false;
                colvarMainTitle.IsReadOnly = false;
                
                schema.Columns.Add(colvarMainTitle);
                
                TableSchema.TableColumn colvarMainImage = new TableSchema.TableColumn(schema);
                colvarMainImage.ColumnName = "main_image";
                colvarMainImage.DataType = DbType.String;
                colvarMainImage.MaxLength = 255;
                colvarMainImage.AutoIncrement = false;
                colvarMainImage.IsNullable = true;
                colvarMainImage.IsPrimaryKey = false;
                colvarMainImage.IsForeignKey = false;
                colvarMainImage.IsReadOnly = false;
                
                schema.Columns.Add(colvarMainImage);
                
                TableSchema.TableColumn colvarMainBackgroundImage = new TableSchema.TableColumn(schema);
                colvarMainBackgroundImage.ColumnName = "main_background_image";
                colvarMainBackgroundImage.DataType = DbType.String;
                colvarMainBackgroundImage.MaxLength = 255;
                colvarMainBackgroundImage.AutoIncrement = false;
                colvarMainBackgroundImage.IsNullable = true;
                colvarMainBackgroundImage.IsPrimaryKey = false;
                colvarMainBackgroundImage.IsForeignKey = false;
                colvarMainBackgroundImage.IsReadOnly = false;
                
                schema.Columns.Add(colvarMainBackgroundImage);
                
                TableSchema.TableColumn colvarMainSpacerImage = new TableSchema.TableColumn(schema);
                colvarMainSpacerImage.ColumnName = "main_spacer_image";
                colvarMainSpacerImage.DataType = DbType.String;
                colvarMainSpacerImage.MaxLength = 255;
                colvarMainSpacerImage.AutoIncrement = false;
                colvarMainSpacerImage.IsNullable = true;
                colvarMainSpacerImage.IsPrimaryKey = false;
                colvarMainSpacerImage.IsForeignKey = false;
                colvarMainSpacerImage.IsReadOnly = false;
                
                schema.Columns.Add(colvarMainSpacerImage);
                
                TableSchema.TableColumn colvarMobileMainImage = new TableSchema.TableColumn(schema);
                colvarMobileMainImage.ColumnName = "mobile_main_image";
                colvarMobileMainImage.DataType = DbType.String;
                colvarMobileMainImage.MaxLength = 255;
                colvarMobileMainImage.AutoIncrement = false;
                colvarMobileMainImage.IsNullable = true;
                colvarMobileMainImage.IsPrimaryKey = false;
                colvarMobileMainImage.IsForeignKey = false;
                colvarMobileMainImage.IsReadOnly = false;
                
                schema.Columns.Add(colvarMobileMainImage);
                
                TableSchema.TableColumn colvarGroupSeq = new TableSchema.TableColumn(schema);
                colvarGroupSeq.ColumnName = "group_seq";
                colvarGroupSeq.DataType = DbType.Int32;
                colvarGroupSeq.MaxLength = 0;
                colvarGroupSeq.AutoIncrement = false;
                colvarGroupSeq.IsNullable = false;
                colvarGroupSeq.IsPrimaryKey = false;
                colvarGroupSeq.IsForeignKey = false;
                colvarGroupSeq.IsReadOnly = false;
                
                schema.Columns.Add(colvarGroupSeq);
                
                TableSchema.TableColumn colvarGroupId = new TableSchema.TableColumn(schema);
                colvarGroupId.ColumnName = "group_id";
                colvarGroupId.DataType = DbType.Int32;
                colvarGroupId.MaxLength = 0;
                colvarGroupId.AutoIncrement = false;
                colvarGroupId.IsNullable = false;
                colvarGroupId.IsPrimaryKey = false;
                colvarGroupId.IsForeignKey = false;
                colvarGroupId.IsReadOnly = false;
                
                schema.Columns.Add(colvarGroupId);
                
                TableSchema.TableColumn colvarGroupName = new TableSchema.TableColumn(schema);
                colvarGroupName.ColumnName = "group_name";
                colvarGroupName.DataType = DbType.String;
                colvarGroupName.MaxLength = 255;
                colvarGroupName.AutoIncrement = false;
                colvarGroupName.IsNullable = false;
                colvarGroupName.IsPrimaryKey = false;
                colvarGroupName.IsForeignKey = false;
                colvarGroupName.IsReadOnly = false;
                
                schema.Columns.Add(colvarGroupName);
                
                TableSchema.TableColumn colvarGroupEnabled = new TableSchema.TableColumn(schema);
                colvarGroupEnabled.ColumnName = "group_enabled";
                colvarGroupEnabled.DataType = DbType.Boolean;
                colvarGroupEnabled.MaxLength = 0;
                colvarGroupEnabled.AutoIncrement = false;
                colvarGroupEnabled.IsNullable = false;
                colvarGroupEnabled.IsPrimaryKey = false;
                colvarGroupEnabled.IsForeignKey = false;
                colvarGroupEnabled.IsReadOnly = false;
                
                schema.Columns.Add(colvarGroupEnabled);
                
                TableSchema.TableColumn colvarGroupStart = new TableSchema.TableColumn(schema);
                colvarGroupStart.ColumnName = "group_start";
                colvarGroupStart.DataType = DbType.DateTime;
                colvarGroupStart.MaxLength = 0;
                colvarGroupStart.AutoIncrement = false;
                colvarGroupStart.IsNullable = false;
                colvarGroupStart.IsPrimaryKey = false;
                colvarGroupStart.IsForeignKey = false;
                colvarGroupStart.IsReadOnly = false;
                
                schema.Columns.Add(colvarGroupStart);
                
                TableSchema.TableColumn colvarGroupEnd = new TableSchema.TableColumn(schema);
                colvarGroupEnd.ColumnName = "group_end";
                colvarGroupEnd.DataType = DbType.DateTime;
                colvarGroupEnd.MaxLength = 0;
                colvarGroupEnd.AutoIncrement = false;
                colvarGroupEnd.IsNullable = false;
                colvarGroupEnd.IsPrimaryKey = false;
                colvarGroupEnd.IsForeignKey = false;
                colvarGroupEnd.IsReadOnly = false;
                
                schema.Columns.Add(colvarGroupEnd);
                
                TableSchema.TableColumn colvarIsTopGroup = new TableSchema.TableColumn(schema);
                colvarIsTopGroup.ColumnName = "is_top_group";
                colvarIsTopGroup.DataType = DbType.Boolean;
                colvarIsTopGroup.MaxLength = 0;
                colvarIsTopGroup.AutoIncrement = false;
                colvarIsTopGroup.IsNullable = false;
                colvarIsTopGroup.IsPrimaryKey = false;
                colvarIsTopGroup.IsForeignKey = false;
                colvarIsTopGroup.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsTopGroup);
                
                TableSchema.TableColumn colvarListSeq = new TableSchema.TableColumn(schema);
                colvarListSeq.ColumnName = "list_seq";
                colvarListSeq.DataType = DbType.Int32;
                colvarListSeq.MaxLength = 0;
                colvarListSeq.AutoIncrement = false;
                colvarListSeq.IsNullable = true;
                colvarListSeq.IsPrimaryKey = false;
                colvarListSeq.IsForeignKey = false;
                colvarListSeq.IsReadOnly = false;
                
                schema.Columns.Add(colvarListSeq);
                
                TableSchema.TableColumn colvarListId = new TableSchema.TableColumn(schema);
                colvarListId.ColumnName = "list_id";
                colvarListId.DataType = DbType.Int32;
                colvarListId.MaxLength = 0;
                colvarListId.AutoIncrement = false;
                colvarListId.IsNullable = false;
                colvarListId.IsPrimaryKey = false;
                colvarListId.IsForeignKey = false;
                colvarListId.IsReadOnly = false;
                
                schema.Columns.Add(colvarListId);
                
                TableSchema.TableColumn colvarCurationType = new TableSchema.TableColumn(schema);
                colvarCurationType.ColumnName = "curation_type";
                colvarCurationType.DataType = DbType.Int32;
                colvarCurationType.MaxLength = 0;
                colvarCurationType.AutoIncrement = false;
                colvarCurationType.IsNullable = false;
                colvarCurationType.IsPrimaryKey = false;
                colvarCurationType.IsForeignKey = false;
                colvarCurationType.IsReadOnly = false;
                
                schema.Columns.Add(colvarCurationType);
                
                TableSchema.TableColumn colvarCurationId = new TableSchema.TableColumn(schema);
                colvarCurationId.ColumnName = "curation_id";
                colvarCurationId.DataType = DbType.Int32;
                colvarCurationId.MaxLength = 0;
                colvarCurationId.AutoIncrement = false;
                colvarCurationId.IsNullable = false;
                colvarCurationId.IsPrimaryKey = false;
                colvarCurationId.IsForeignKey = false;
                colvarCurationId.IsReadOnly = false;
                
                schema.Columns.Add(colvarCurationId);
                
                TableSchema.TableColumn colvarCurationTitle = new TableSchema.TableColumn(schema);
                colvarCurationTitle.ColumnName = "curation_title";
                colvarCurationTitle.DataType = DbType.String;
                colvarCurationTitle.MaxLength = 200;
                colvarCurationTitle.AutoIncrement = false;
                colvarCurationTitle.IsNullable = false;
                colvarCurationTitle.IsPrimaryKey = false;
                colvarCurationTitle.IsForeignKey = false;
                colvarCurationTitle.IsReadOnly = false;
                
                schema.Columns.Add(colvarCurationTitle);
                
                TableSchema.TableColumn colvarBrandIsActive = new TableSchema.TableColumn(schema);
                colvarBrandIsActive.ColumnName = "brand_is_active";
                colvarBrandIsActive.DataType = DbType.Boolean;
                colvarBrandIsActive.MaxLength = 0;
                colvarBrandIsActive.AutoIncrement = false;
                colvarBrandIsActive.IsNullable = false;
                colvarBrandIsActive.IsPrimaryKey = false;
                colvarBrandIsActive.IsForeignKey = false;
                colvarBrandIsActive.IsReadOnly = false;
                
                schema.Columns.Add(colvarBrandIsActive);
                
                TableSchema.TableColumn colvarShowInWeb = new TableSchema.TableColumn(schema);
                colvarShowInWeb.ColumnName = "show_in_web";
                colvarShowInWeb.DataType = DbType.Boolean;
                colvarShowInWeb.MaxLength = 0;
                colvarShowInWeb.AutoIncrement = false;
                colvarShowInWeb.IsNullable = false;
                colvarShowInWeb.IsPrimaryKey = false;
                colvarShowInWeb.IsForeignKey = false;
                colvarShowInWeb.IsReadOnly = false;
                
                schema.Columns.Add(colvarShowInWeb);
                
                TableSchema.TableColumn colvarShowInApp = new TableSchema.TableColumn(schema);
                colvarShowInApp.ColumnName = "show_in_app";
                colvarShowInApp.DataType = DbType.Boolean;
                colvarShowInApp.MaxLength = 0;
                colvarShowInApp.AutoIncrement = false;
                colvarShowInApp.IsNullable = false;
                colvarShowInApp.IsPrimaryKey = false;
                colvarShowInApp.IsForeignKey = false;
                colvarShowInApp.IsReadOnly = false;
                
                schema.Columns.Add(colvarShowInApp);
                
                TableSchema.TableColumn colvarCurationImage = new TableSchema.TableColumn(schema);
                colvarCurationImage.ColumnName = "curation_image";
                colvarCurationImage.DataType = DbType.String;
                colvarCurationImage.MaxLength = 200;
                colvarCurationImage.AutoIncrement = false;
                colvarCurationImage.IsNullable = true;
                colvarCurationImage.IsPrimaryKey = false;
                colvarCurationImage.IsForeignKey = false;
                colvarCurationImage.IsReadOnly = false;
                
                schema.Columns.Add(colvarCurationImage);
                
                TableSchema.TableColumn colvarCurationChannelImage = new TableSchema.TableColumn(schema);
                colvarCurationChannelImage.ColumnName = "curation_channel_image";
                colvarCurationChannelImage.DataType = DbType.String;
                colvarCurationChannelImage.MaxLength = 200;
                colvarCurationChannelImage.AutoIncrement = false;
                colvarCurationChannelImage.IsNullable = true;
                colvarCurationChannelImage.IsPrimaryKey = false;
                colvarCurationChannelImage.IsForeignKey = false;
                colvarCurationChannelImage.IsReadOnly = false;
                
                schema.Columns.Add(colvarCurationChannelImage);
                
                TableSchema.TableColumn colvarCurationStart = new TableSchema.TableColumn(schema);
                colvarCurationStart.ColumnName = "curation_start";
                colvarCurationStart.DataType = DbType.DateTime;
                colvarCurationStart.MaxLength = 0;
                colvarCurationStart.AutoIncrement = false;
                colvarCurationStart.IsNullable = false;
                colvarCurationStart.IsPrimaryKey = false;
                colvarCurationStart.IsForeignKey = false;
                colvarCurationStart.IsReadOnly = false;
                
                schema.Columns.Add(colvarCurationStart);
                
                TableSchema.TableColumn colvarCurationEnd = new TableSchema.TableColumn(schema);
                colvarCurationEnd.ColumnName = "curation_end";
                colvarCurationEnd.DataType = DbType.DateTime;
                colvarCurationEnd.MaxLength = 0;
                colvarCurationEnd.AutoIncrement = false;
                colvarCurationEnd.IsNullable = false;
                colvarCurationEnd.IsPrimaryKey = false;
                colvarCurationEnd.IsForeignKey = false;
                colvarCurationEnd.IsReadOnly = false;
                
                schema.Columns.Add(colvarCurationEnd);
                
                TableSchema.TableColumn colvarCpa = new TableSchema.TableColumn(schema);
                colvarCpa.ColumnName = "cpa";
                colvarCpa.DataType = DbType.String;
                colvarCpa.MaxLength = 100;
                colvarCpa.AutoIncrement = false;
                colvarCpa.IsNullable = true;
                colvarCpa.IsPrimaryKey = false;
                colvarCpa.IsForeignKey = false;
                colvarCpa.IsReadOnly = false;
                
                schema.Columns.Add(colvarCpa);
                
                TableSchema.TableColumn colvarUrl = new TableSchema.TableColumn(schema);
                colvarUrl.ColumnName = "url";
                colvarUrl.DataType = DbType.String;
                colvarUrl.MaxLength = 100;
                colvarUrl.AutoIncrement = false;
                colvarUrl.IsNullable = false;
                colvarUrl.IsPrimaryKey = false;
                colvarUrl.IsForeignKey = false;
                colvarUrl.IsReadOnly = false;
                
                schema.Columns.Add(colvarUrl);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_theme_curation",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewThemeCuration()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewThemeCuration(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewThemeCuration(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewThemeCuration(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("MainId")]
        [Bindable(true)]
        public int MainId 
	    {
		    get
		    {
			    return GetColumnValue<int>("main_id");
		    }
            set 
		    {
			    SetColumnValue("main_id", value);
            }
        }
	      
        [XmlAttribute("MainStart")]
        [Bindable(true)]
        public DateTime MainStart 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("main_start");
		    }
            set 
		    {
			    SetColumnValue("main_start", value);
            }
        }
	      
        [XmlAttribute("MainEnd")]
        [Bindable(true)]
        public DateTime MainEnd 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("main_end");
		    }
            set 
		    {
			    SetColumnValue("main_end", value);
            }
        }
	      
        [XmlAttribute("MainTitle")]
        [Bindable(true)]
        public string MainTitle 
	    {
		    get
		    {
			    return GetColumnValue<string>("main_title");
		    }
            set 
		    {
			    SetColumnValue("main_title", value);
            }
        }
	      
        [XmlAttribute("MainImage")]
        [Bindable(true)]
        public string MainImage 
	    {
		    get
		    {
			    return GetColumnValue<string>("main_image");
		    }
            set 
		    {
			    SetColumnValue("main_image", value);
            }
        }
	      
        [XmlAttribute("MainBackgroundImage")]
        [Bindable(true)]
        public string MainBackgroundImage 
	    {
		    get
		    {
			    return GetColumnValue<string>("main_background_image");
		    }
            set 
		    {
			    SetColumnValue("main_background_image", value);
            }
        }
	      
        [XmlAttribute("MainSpacerImage")]
        [Bindable(true)]
        public string MainSpacerImage 
	    {
		    get
		    {
			    return GetColumnValue<string>("main_spacer_image");
		    }
            set 
		    {
			    SetColumnValue("main_spacer_image", value);
            }
        }
	      
        [XmlAttribute("MobileMainImage")]
        [Bindable(true)]
        public string MobileMainImage 
	    {
		    get
		    {
			    return GetColumnValue<string>("mobile_main_image");
		    }
            set 
		    {
			    SetColumnValue("mobile_main_image", value);
            }
        }
	      
        [XmlAttribute("GroupSeq")]
        [Bindable(true)]
        public int GroupSeq 
	    {
		    get
		    {
			    return GetColumnValue<int>("group_seq");
		    }
            set 
		    {
			    SetColumnValue("group_seq", value);
            }
        }
	      
        [XmlAttribute("GroupId")]
        [Bindable(true)]
        public int GroupId 
	    {
		    get
		    {
			    return GetColumnValue<int>("group_id");
		    }
            set 
		    {
			    SetColumnValue("group_id", value);
            }
        }
	      
        [XmlAttribute("GroupName")]
        [Bindable(true)]
        public string GroupName 
	    {
		    get
		    {
			    return GetColumnValue<string>("group_name");
		    }
            set 
		    {
			    SetColumnValue("group_name", value);
            }
        }
	      
        [XmlAttribute("GroupEnabled")]
        [Bindable(true)]
        public bool GroupEnabled 
	    {
		    get
		    {
			    return GetColumnValue<bool>("group_enabled");
		    }
            set 
		    {
			    SetColumnValue("group_enabled", value);
            }
        }
	      
        [XmlAttribute("GroupStart")]
        [Bindable(true)]
        public DateTime GroupStart 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("group_start");
		    }
            set 
		    {
			    SetColumnValue("group_start", value);
            }
        }
	      
        [XmlAttribute("GroupEnd")]
        [Bindable(true)]
        public DateTime GroupEnd 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("group_end");
		    }
            set 
		    {
			    SetColumnValue("group_end", value);
            }
        }
	      
        [XmlAttribute("IsTopGroup")]
        [Bindable(true)]
        public bool IsTopGroup 
	    {
		    get
		    {
			    return GetColumnValue<bool>("is_top_group");
		    }
            set 
		    {
			    SetColumnValue("is_top_group", value);
            }
        }
	      
        [XmlAttribute("ListSeq")]
        [Bindable(true)]
        public int? ListSeq 
	    {
		    get
		    {
			    return GetColumnValue<int?>("list_seq");
		    }
            set 
		    {
			    SetColumnValue("list_seq", value);
            }
        }
	      
        [XmlAttribute("ListId")]
        [Bindable(true)]
        public int ListId 
	    {
		    get
		    {
			    return GetColumnValue<int>("list_id");
		    }
            set 
		    {
			    SetColumnValue("list_id", value);
            }
        }
	      
        [XmlAttribute("CurationType")]
        [Bindable(true)]
        public int CurationType 
	    {
		    get
		    {
			    return GetColumnValue<int>("curation_type");
		    }
            set 
		    {
			    SetColumnValue("curation_type", value);
            }
        }
	      
        [XmlAttribute("CurationId")]
        [Bindable(true)]
        public int CurationId 
	    {
		    get
		    {
			    return GetColumnValue<int>("curation_id");
		    }
            set 
		    {
			    SetColumnValue("curation_id", value);
            }
        }
	      
        [XmlAttribute("CurationTitle")]
        [Bindable(true)]
        public string CurationTitle 
	    {
		    get
		    {
			    return GetColumnValue<string>("curation_title");
		    }
            set 
		    {
			    SetColumnValue("curation_title", value);
            }
        }
	      
        [XmlAttribute("BrandIsActive")]
        [Bindable(true)]
        public bool BrandIsActive 
	    {
		    get
		    {
			    return GetColumnValue<bool>("brand_is_active");
		    }
            set 
		    {
			    SetColumnValue("brand_is_active", value);
            }
        }
	      
        [XmlAttribute("ShowInWeb")]
        [Bindable(true)]
        public bool ShowInWeb 
	    {
		    get
		    {
			    return GetColumnValue<bool>("show_in_web");
		    }
            set 
		    {
			    SetColumnValue("show_in_web", value);
            }
        }
	      
        [XmlAttribute("ShowInApp")]
        [Bindable(true)]
        public bool ShowInApp 
	    {
		    get
		    {
			    return GetColumnValue<bool>("show_in_app");
		    }
            set 
		    {
			    SetColumnValue("show_in_app", value);
            }
        }
	      
        [XmlAttribute("CurationImage")]
        [Bindable(true)]
        public string CurationImage 
	    {
		    get
		    {
			    return GetColumnValue<string>("curation_image");
		    }
            set 
		    {
			    SetColumnValue("curation_image", value);
            }
        }
	      
        [XmlAttribute("CurationChannelImage")]
        [Bindable(true)]
        public string CurationChannelImage 
	    {
		    get
		    {
			    return GetColumnValue<string>("curation_channel_image");
		    }
            set 
		    {
			    SetColumnValue("curation_channel_image", value);
            }
        }
	      
        [XmlAttribute("CurationStart")]
        [Bindable(true)]
        public DateTime CurationStart 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("curation_start");
		    }
            set 
		    {
			    SetColumnValue("curation_start", value);
            }
        }
	      
        [XmlAttribute("CurationEnd")]
        [Bindable(true)]
        public DateTime CurationEnd 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("curation_end");
		    }
            set 
		    {
			    SetColumnValue("curation_end", value);
            }
        }
	      
        [XmlAttribute("Cpa")]
        [Bindable(true)]
        public string Cpa 
	    {
		    get
		    {
			    return GetColumnValue<string>("cpa");
		    }
            set 
		    {
			    SetColumnValue("cpa", value);
            }
        }
	      
        [XmlAttribute("Url")]
        [Bindable(true)]
        public string Url 
	    {
		    get
		    {
			    return GetColumnValue<string>("url");
		    }
            set 
		    {
			    SetColumnValue("url", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string MainId = @"main_id";
            
            public static string MainStart = @"main_start";
            
            public static string MainEnd = @"main_end";
            
            public static string MainTitle = @"main_title";
            
            public static string MainImage = @"main_image";
            
            public static string MainBackgroundImage = @"main_background_image";
            
            public static string MainSpacerImage = @"main_spacer_image";
            
            public static string MobileMainImage = @"mobile_main_image";
            
            public static string GroupSeq = @"group_seq";
            
            public static string GroupId = @"group_id";
            
            public static string GroupName = @"group_name";
            
            public static string GroupEnabled = @"group_enabled";
            
            public static string GroupStart = @"group_start";
            
            public static string GroupEnd = @"group_end";
            
            public static string IsTopGroup = @"is_top_group";
            
            public static string ListSeq = @"list_seq";
            
            public static string ListId = @"list_id";
            
            public static string CurationType = @"curation_type";
            
            public static string CurationId = @"curation_id";
            
            public static string CurationTitle = @"curation_title";
            
            public static string BrandIsActive = @"brand_is_active";
            
            public static string ShowInWeb = @"show_in_web";
            
            public static string ShowInApp = @"show_in_app";
            
            public static string CurationImage = @"curation_image";
            
            public static string CurationChannelImage = @"curation_channel_image";
            
            public static string CurationStart = @"curation_start";
            
            public static string CurationEnd = @"curation_end";
            
            public static string Cpa = @"cpa";
            
            public static string Url = @"url";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the CtAtmMessageInform class.
	/// </summary>
    [Serializable]
	public partial class CtAtmMessageInformCollection : RepositoryList<CtAtmMessageInform, CtAtmMessageInformCollection>
	{	   
		public CtAtmMessageInformCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>CtAtmMessageInformCollection</returns>
		public CtAtmMessageInformCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                CtAtmMessageInform o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the CT_ATM_MessageInform table.
	/// </summary>
	[Serializable]
	public partial class CtAtmMessageInform : RepositoryRecord<CtAtmMessageInform>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public CtAtmMessageInform()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public CtAtmMessageInform(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("CT_ATM_MessageInform", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarSEQNo = new TableSchema.TableColumn(schema);
				colvarSEQNo.ColumnName = "SEQNo";
				colvarSEQNo.DataType = DbType.AnsiString;
				colvarSEQNo.MaxLength = 16;
				colvarSEQNo.AutoIncrement = false;
				colvarSEQNo.IsNullable = false;
				colvarSEQNo.IsPrimaryKey = true;
				colvarSEQNo.IsForeignKey = false;
				colvarSEQNo.IsReadOnly = false;
				colvarSEQNo.DefaultSetting = @"";
				colvarSEQNo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSEQNo);
				
				TableSchema.TableColumn colvarInfoType = new TableSchema.TableColumn(schema);
				colvarInfoType.ColumnName = "InfoType";
				colvarInfoType.DataType = DbType.AnsiString;
				colvarInfoType.MaxLength = 2;
				colvarInfoType.AutoIncrement = false;
				colvarInfoType.IsNullable = true;
				colvarInfoType.IsPrimaryKey = false;
				colvarInfoType.IsForeignKey = false;
				colvarInfoType.IsReadOnly = false;
				colvarInfoType.DefaultSetting = @"";
				colvarInfoType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarInfoType);
				
				TableSchema.TableColumn colvarResponseStats = new TableSchema.TableColumn(schema);
				colvarResponseStats.ColumnName = "ResponseStats";
				colvarResponseStats.DataType = DbType.AnsiString;
				colvarResponseStats.MaxLength = 5;
				colvarResponseStats.AutoIncrement = false;
				colvarResponseStats.IsNullable = true;
				colvarResponseStats.IsPrimaryKey = false;
				colvarResponseStats.IsForeignKey = false;
				colvarResponseStats.IsReadOnly = false;
				colvarResponseStats.DefaultSetting = @"";
				colvarResponseStats.ForeignKeyTableName = "";
				schema.Columns.Add(colvarResponseStats);
				
				TableSchema.TableColumn colvarReceiveDate = new TableSchema.TableColumn(schema);
				colvarReceiveDate.ColumnName = "ReceiveDate";
				colvarReceiveDate.DataType = DbType.DateTime;
				colvarReceiveDate.MaxLength = 0;
				colvarReceiveDate.AutoIncrement = false;
				colvarReceiveDate.IsNullable = true;
				colvarReceiveDate.IsPrimaryKey = false;
				colvarReceiveDate.IsForeignKey = false;
				colvarReceiveDate.IsReadOnly = false;
				colvarReceiveDate.DefaultSetting = @"";
				colvarReceiveDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReceiveDate);
				
				TableSchema.TableColumn colvarAccountNoReceive = new TableSchema.TableColumn(schema);
				colvarAccountNoReceive.ColumnName = "AccountNoReceive";
				colvarAccountNoReceive.DataType = DbType.AnsiString;
				colvarAccountNoReceive.MaxLength = 12;
				colvarAccountNoReceive.AutoIncrement = false;
				colvarAccountNoReceive.IsNullable = true;
				colvarAccountNoReceive.IsPrimaryKey = false;
				colvarAccountNoReceive.IsForeignKey = false;
				colvarAccountNoReceive.IsReadOnly = false;
				colvarAccountNoReceive.DefaultSetting = @"";
				colvarAccountNoReceive.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAccountNoReceive);
				
				TableSchema.TableColumn colvarAccountType = new TableSchema.TableColumn(schema);
				colvarAccountType.ColumnName = "AccountType";
				colvarAccountType.DataType = DbType.AnsiString;
				colvarAccountType.MaxLength = 1;
				colvarAccountType.AutoIncrement = false;
				colvarAccountType.IsNullable = true;
				colvarAccountType.IsPrimaryKey = false;
				colvarAccountType.IsForeignKey = false;
				colvarAccountType.IsReadOnly = false;
				colvarAccountType.DefaultSetting = @"";
				colvarAccountType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAccountType);
				
				TableSchema.TableColumn colvarAccountDate = new TableSchema.TableColumn(schema);
				colvarAccountDate.ColumnName = "AccountDate";
				colvarAccountDate.DataType = DbType.AnsiString;
				colvarAccountDate.MaxLength = 7;
				colvarAccountDate.AutoIncrement = false;
				colvarAccountDate.IsNullable = true;
				colvarAccountDate.IsPrimaryKey = false;
				colvarAccountDate.IsForeignKey = false;
				colvarAccountDate.IsReadOnly = false;
				colvarAccountDate.DefaultSetting = @"";
				colvarAccountDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAccountDate);
				
				TableSchema.TableColumn colvarMemo1 = new TableSchema.TableColumn(schema);
				colvarMemo1.ColumnName = "Memo1";
				colvarMemo1.DataType = DbType.AnsiString;
				colvarMemo1.MaxLength = 14;
				colvarMemo1.AutoIncrement = false;
				colvarMemo1.IsNullable = false;
				colvarMemo1.IsPrimaryKey = true;
				colvarMemo1.IsForeignKey = false;
				colvarMemo1.IsReadOnly = false;
				colvarMemo1.DefaultSetting = @"";
				colvarMemo1.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMemo1);
				
				TableSchema.TableColumn colvarTransactionAmount = new TableSchema.TableColumn(schema);
				colvarTransactionAmount.ColumnName = "TransactionAmount";
				colvarTransactionAmount.DataType = DbType.Currency;
				colvarTransactionAmount.MaxLength = 0;
				colvarTransactionAmount.AutoIncrement = false;
				colvarTransactionAmount.IsNullable = true;
				colvarTransactionAmount.IsPrimaryKey = false;
				colvarTransactionAmount.IsForeignKey = false;
				colvarTransactionAmount.IsReadOnly = false;
				colvarTransactionAmount.DefaultSetting = @"";
				colvarTransactionAmount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTransactionAmount);
				
				TableSchema.TableColumn colvarTransactionDate = new TableSchema.TableColumn(schema);
				colvarTransactionDate.ColumnName = "TransactionDate";
				colvarTransactionDate.DataType = DbType.AnsiString;
				colvarTransactionDate.MaxLength = 7;
				colvarTransactionDate.AutoIncrement = false;
				colvarTransactionDate.IsNullable = true;
				colvarTransactionDate.IsPrimaryKey = false;
				colvarTransactionDate.IsForeignKey = false;
				colvarTransactionDate.IsReadOnly = false;
				colvarTransactionDate.DefaultSetting = @"";
				colvarTransactionDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTransactionDate);
				
				TableSchema.TableColumn colvarTransactionTime = new TableSchema.TableColumn(schema);
				colvarTransactionTime.ColumnName = "TransactionTime";
				colvarTransactionTime.DataType = DbType.AnsiString;
				colvarTransactionTime.MaxLength = 6;
				colvarTransactionTime.AutoIncrement = false;
				colvarTransactionTime.IsNullable = true;
				colvarTransactionTime.IsPrimaryKey = false;
				colvarTransactionTime.IsForeignKey = false;
				colvarTransactionTime.IsReadOnly = false;
				colvarTransactionTime.DefaultSetting = @"";
				colvarTransactionTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTransactionTime);
				
				TableSchema.TableColumn colvarCrossBankNo = new TableSchema.TableColumn(schema);
				colvarCrossBankNo.ColumnName = "CrossBankNo";
				colvarCrossBankNo.DataType = DbType.AnsiString;
				colvarCrossBankNo.MaxLength = 10;
				colvarCrossBankNo.AutoIncrement = false;
				colvarCrossBankNo.IsNullable = true;
				colvarCrossBankNo.IsPrimaryKey = false;
				colvarCrossBankNo.IsForeignKey = false;
				colvarCrossBankNo.IsReadOnly = false;
				colvarCrossBankNo.DefaultSetting = @"";
				colvarCrossBankNo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCrossBankNo);
				
				TableSchema.TableColumn colvarDebitCredit = new TableSchema.TableColumn(schema);
				colvarDebitCredit.ColumnName = "DebitCredit";
				colvarDebitCredit.DataType = DbType.AnsiString;
				colvarDebitCredit.MaxLength = 1;
				colvarDebitCredit.AutoIncrement = false;
				colvarDebitCredit.IsNullable = true;
				colvarDebitCredit.IsPrimaryKey = false;
				colvarDebitCredit.IsForeignKey = false;
				colvarDebitCredit.IsReadOnly = false;
				colvarDebitCredit.DefaultSetting = @"";
				colvarDebitCredit.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDebitCredit);
				
				TableSchema.TableColumn colvarAmountType = new TableSchema.TableColumn(schema);
				colvarAmountType.ColumnName = "AmountType";
				colvarAmountType.DataType = DbType.AnsiString;
				colvarAmountType.MaxLength = 1;
				colvarAmountType.AutoIncrement = false;
				colvarAmountType.IsNullable = true;
				colvarAmountType.IsPrimaryKey = false;
				colvarAmountType.IsForeignKey = false;
				colvarAmountType.IsReadOnly = false;
				colvarAmountType.DefaultSetting = @"";
				colvarAmountType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAmountType);
				
				TableSchema.TableColumn colvarServiceManNo = new TableSchema.TableColumn(schema);
				colvarServiceManNo.ColumnName = "ServiceManNo";
				colvarServiceManNo.DataType = DbType.AnsiString;
				colvarServiceManNo.MaxLength = 5;
				colvarServiceManNo.AutoIncrement = false;
				colvarServiceManNo.IsNullable = true;
				colvarServiceManNo.IsPrimaryKey = false;
				colvarServiceManNo.IsForeignKey = false;
				colvarServiceManNo.IsReadOnly = false;
				colvarServiceManNo.DefaultSetting = @"";
				colvarServiceManNo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarServiceManNo);
				
				TableSchema.TableColumn colvarTransactionID = new TableSchema.TableColumn(schema);
				colvarTransactionID.ColumnName = "TransactionID";
				colvarTransactionID.DataType = DbType.AnsiString;
				colvarTransactionID.MaxLength = 5;
				colvarTransactionID.AutoIncrement = false;
				colvarTransactionID.IsNullable = true;
				colvarTransactionID.IsPrimaryKey = false;
				colvarTransactionID.IsForeignKey = false;
				colvarTransactionID.IsReadOnly = false;
				colvarTransactionID.DefaultSetting = @"";
				colvarTransactionID.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTransactionID);
				
				TableSchema.TableColumn colvarTransactionType = new TableSchema.TableColumn(schema);
				colvarTransactionType.ColumnName = "TransactionType";
				colvarTransactionType.DataType = DbType.AnsiString;
				colvarTransactionType.MaxLength = 1;
				colvarTransactionType.AutoIncrement = false;
				colvarTransactionType.IsNullable = true;
				colvarTransactionType.IsPrimaryKey = false;
				colvarTransactionType.IsForeignKey = false;
				colvarTransactionType.IsReadOnly = false;
				colvarTransactionType.DefaultSetting = @"";
				colvarTransactionType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTransactionType);
				
				TableSchema.TableColumn colvarAccountNoSend = new TableSchema.TableColumn(schema);
				colvarAccountNoSend.ColumnName = "AccountNoSend";
				colvarAccountNoSend.DataType = DbType.AnsiString;
				colvarAccountNoSend.MaxLength = 19;
				colvarAccountNoSend.AutoIncrement = false;
				colvarAccountNoSend.IsNullable = true;
				colvarAccountNoSend.IsPrimaryKey = false;
				colvarAccountNoSend.IsForeignKey = false;
				colvarAccountNoSend.IsReadOnly = false;
				colvarAccountNoSend.DefaultSetting = @"";
				colvarAccountNoSend.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAccountNoSend);
				
				TableSchema.TableColumn colvarMemo2 = new TableSchema.TableColumn(schema);
				colvarMemo2.ColumnName = "Memo2";
				colvarMemo2.DataType = DbType.AnsiString;
				colvarMemo2.MaxLength = 11;
				colvarMemo2.AutoIncrement = false;
				colvarMemo2.IsNullable = true;
				colvarMemo2.IsPrimaryKey = false;
				colvarMemo2.IsForeignKey = false;
				colvarMemo2.IsReadOnly = false;
				colvarMemo2.DefaultSetting = @"";
				colvarMemo2.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMemo2);
				
				TableSchema.TableColumn colvarCashIncrease = new TableSchema.TableColumn(schema);
				colvarCashIncrease.ColumnName = "CashIncrease";
				colvarCashIncrease.DataType = DbType.AnsiString;
				colvarCashIncrease.MaxLength = 35;
				colvarCashIncrease.AutoIncrement = false;
				colvarCashIncrease.IsNullable = true;
				colvarCashIncrease.IsPrimaryKey = false;
				colvarCashIncrease.IsForeignKey = false;
				colvarCashIncrease.IsReadOnly = false;
				colvarCashIncrease.DefaultSetting = @"";
				colvarCashIncrease.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCashIncrease);
				
				TableSchema.TableColumn colvarRemitter = new TableSchema.TableColumn(schema);
				colvarRemitter.ColumnName = "Remitter";
				colvarRemitter.DataType = DbType.AnsiString;
				colvarRemitter.MaxLength = 80;
				colvarRemitter.AutoIncrement = false;
				colvarRemitter.IsNullable = true;
				colvarRemitter.IsPrimaryKey = false;
				colvarRemitter.IsForeignKey = false;
				colvarRemitter.IsReadOnly = false;
				colvarRemitter.DefaultSetting = @"";
				colvarRemitter.ForeignKeyTableName = "";
				schema.Columns.Add(colvarRemitter);
				
				TableSchema.TableColumn colvarRemittanceMemo = new TableSchema.TableColumn(schema);
				colvarRemittanceMemo.ColumnName = "RemittanceMemo";
				colvarRemittanceMemo.DataType = DbType.AnsiString;
				colvarRemittanceMemo.MaxLength = 80;
				colvarRemittanceMemo.AutoIncrement = false;
				colvarRemittanceMemo.IsNullable = true;
				colvarRemittanceMemo.IsPrimaryKey = false;
				colvarRemittanceMemo.IsForeignKey = false;
				colvarRemittanceMemo.IsReadOnly = false;
				colvarRemittanceMemo.DefaultSetting = @"";
				colvarRemittanceMemo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarRemittanceMemo);
				
				TableSchema.TableColumn colvarMessageContent = new TableSchema.TableColumn(schema);
				colvarMessageContent.ColumnName = "MessageContent";
				colvarMessageContent.DataType = DbType.AnsiString;
				colvarMessageContent.MaxLength = 350;
				colvarMessageContent.AutoIncrement = false;
				colvarMessageContent.IsNullable = true;
				colvarMessageContent.IsPrimaryKey = false;
				colvarMessageContent.IsForeignKey = false;
				colvarMessageContent.IsReadOnly = false;
				colvarMessageContent.DefaultSetting = @"";
				colvarMessageContent.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMessageContent);
				
				TableSchema.TableColumn colvarProcessStatus = new TableSchema.TableColumn(schema);
				colvarProcessStatus.ColumnName = "ProcessStatus";
				colvarProcessStatus.DataType = DbType.Int16;
				colvarProcessStatus.MaxLength = 0;
				colvarProcessStatus.AutoIncrement = false;
				colvarProcessStatus.IsNullable = true;
				colvarProcessStatus.IsPrimaryKey = false;
				colvarProcessStatus.IsForeignKey = false;
				colvarProcessStatus.IsReadOnly = false;
				
						colvarProcessStatus.DefaultSetting = @"((0))";
				colvarProcessStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarProcessStatus);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("CT_ATM_MessageInform",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("SEQNo")]
		[Bindable(true)]
		public string SEQNo 
		{
			get { return GetColumnValue<string>(Columns.SEQNo); }
			set { SetColumnValue(Columns.SEQNo, value); }
		}
		  
		[XmlAttribute("InfoType")]
		[Bindable(true)]
		public string InfoType 
		{
			get { return GetColumnValue<string>(Columns.InfoType); }
			set { SetColumnValue(Columns.InfoType, value); }
		}
		  
		[XmlAttribute("ResponseStats")]
		[Bindable(true)]
		public string ResponseStats 
		{
			get { return GetColumnValue<string>(Columns.ResponseStats); }
			set { SetColumnValue(Columns.ResponseStats, value); }
		}
		  
		[XmlAttribute("ReceiveDate")]
		[Bindable(true)]
		public DateTime? ReceiveDate 
		{
			get { return GetColumnValue<DateTime?>(Columns.ReceiveDate); }
			set { SetColumnValue(Columns.ReceiveDate, value); }
		}
		  
		[XmlAttribute("AccountNoReceive")]
		[Bindable(true)]
		public string AccountNoReceive 
		{
			get { return GetColumnValue<string>(Columns.AccountNoReceive); }
			set { SetColumnValue(Columns.AccountNoReceive, value); }
		}
		  
		[XmlAttribute("AccountType")]
		[Bindable(true)]
		public string AccountType 
		{
			get { return GetColumnValue<string>(Columns.AccountType); }
			set { SetColumnValue(Columns.AccountType, value); }
		}
		  
		[XmlAttribute("AccountDate")]
		[Bindable(true)]
		public string AccountDate 
		{
			get { return GetColumnValue<string>(Columns.AccountDate); }
			set { SetColumnValue(Columns.AccountDate, value); }
		}
		  
		[XmlAttribute("Memo1")]
		[Bindable(true)]
		public string Memo1 
		{
			get { return GetColumnValue<string>(Columns.Memo1); }
			set { SetColumnValue(Columns.Memo1, value); }
		}
		  
		[XmlAttribute("TransactionAmount")]
		[Bindable(true)]
		public decimal? TransactionAmount 
		{
			get { return GetColumnValue<decimal?>(Columns.TransactionAmount); }
			set { SetColumnValue(Columns.TransactionAmount, value); }
		}
		  
		[XmlAttribute("TransactionDate")]
		[Bindable(true)]
		public string TransactionDate 
		{
			get { return GetColumnValue<string>(Columns.TransactionDate); }
			set { SetColumnValue(Columns.TransactionDate, value); }
		}
		  
		[XmlAttribute("TransactionTime")]
		[Bindable(true)]
		public string TransactionTime 
		{
			get { return GetColumnValue<string>(Columns.TransactionTime); }
			set { SetColumnValue(Columns.TransactionTime, value); }
		}
		  
		[XmlAttribute("CrossBankNo")]
		[Bindable(true)]
		public string CrossBankNo 
		{
			get { return GetColumnValue<string>(Columns.CrossBankNo); }
			set { SetColumnValue(Columns.CrossBankNo, value); }
		}
		  
		[XmlAttribute("DebitCredit")]
		[Bindable(true)]
		public string DebitCredit 
		{
			get { return GetColumnValue<string>(Columns.DebitCredit); }
			set { SetColumnValue(Columns.DebitCredit, value); }
		}
		  
		[XmlAttribute("AmountType")]
		[Bindable(true)]
		public string AmountType 
		{
			get { return GetColumnValue<string>(Columns.AmountType); }
			set { SetColumnValue(Columns.AmountType, value); }
		}
		  
		[XmlAttribute("ServiceManNo")]
		[Bindable(true)]
		public string ServiceManNo 
		{
			get { return GetColumnValue<string>(Columns.ServiceManNo); }
			set { SetColumnValue(Columns.ServiceManNo, value); }
		}
		  
		[XmlAttribute("TransactionID")]
		[Bindable(true)]
		public string TransactionID 
		{
			get { return GetColumnValue<string>(Columns.TransactionID); }
			set { SetColumnValue(Columns.TransactionID, value); }
		}
		  
		[XmlAttribute("TransactionType")]
		[Bindable(true)]
		public string TransactionType 
		{
			get { return GetColumnValue<string>(Columns.TransactionType); }
			set { SetColumnValue(Columns.TransactionType, value); }
		}
		  
		[XmlAttribute("AccountNoSend")]
		[Bindable(true)]
		public string AccountNoSend 
		{
			get { return GetColumnValue<string>(Columns.AccountNoSend); }
			set { SetColumnValue(Columns.AccountNoSend, value); }
		}
		  
		[XmlAttribute("Memo2")]
		[Bindable(true)]
		public string Memo2 
		{
			get { return GetColumnValue<string>(Columns.Memo2); }
			set { SetColumnValue(Columns.Memo2, value); }
		}
		  
		[XmlAttribute("CashIncrease")]
		[Bindable(true)]
		public string CashIncrease 
		{
			get { return GetColumnValue<string>(Columns.CashIncrease); }
			set { SetColumnValue(Columns.CashIncrease, value); }
		}
		  
		[XmlAttribute("Remitter")]
		[Bindable(true)]
		public string Remitter 
		{
			get { return GetColumnValue<string>(Columns.Remitter); }
			set { SetColumnValue(Columns.Remitter, value); }
		}
		  
		[XmlAttribute("RemittanceMemo")]
		[Bindable(true)]
		public string RemittanceMemo 
		{
			get { return GetColumnValue<string>(Columns.RemittanceMemo); }
			set { SetColumnValue(Columns.RemittanceMemo, value); }
		}
		  
		[XmlAttribute("MessageContent")]
		[Bindable(true)]
		public string MessageContent 
		{
			get { return GetColumnValue<string>(Columns.MessageContent); }
			set { SetColumnValue(Columns.MessageContent, value); }
		}
		  
		[XmlAttribute("ProcessStatus")]
		[Bindable(true)]
		public short? ProcessStatus 
		{
			get { return GetColumnValue<short?>(Columns.ProcessStatus); }
			set { SetColumnValue(Columns.ProcessStatus, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn SEQNoColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn InfoTypeColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn ResponseStatsColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn ReceiveDateColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn AccountNoReceiveColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn AccountTypeColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn AccountDateColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn Memo1Column
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn TransactionAmountColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn TransactionDateColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn TransactionTimeColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        public static TableSchema.TableColumn CrossBankNoColumn
        {
            get { return Schema.Columns[11]; }
        }
        
        
        
        public static TableSchema.TableColumn DebitCreditColumn
        {
            get { return Schema.Columns[12]; }
        }
        
        
        
        public static TableSchema.TableColumn AmountTypeColumn
        {
            get { return Schema.Columns[13]; }
        }
        
        
        
        public static TableSchema.TableColumn ServiceManNoColumn
        {
            get { return Schema.Columns[14]; }
        }
        
        
        
        public static TableSchema.TableColumn TransactionIDColumn
        {
            get { return Schema.Columns[15]; }
        }
        
        
        
        public static TableSchema.TableColumn TransactionTypeColumn
        {
            get { return Schema.Columns[16]; }
        }
        
        
        
        public static TableSchema.TableColumn AccountNoSendColumn
        {
            get { return Schema.Columns[17]; }
        }
        
        
        
        public static TableSchema.TableColumn Memo2Column
        {
            get { return Schema.Columns[18]; }
        }
        
        
        
        public static TableSchema.TableColumn CashIncreaseColumn
        {
            get { return Schema.Columns[19]; }
        }
        
        
        
        public static TableSchema.TableColumn RemitterColumn
        {
            get { return Schema.Columns[20]; }
        }
        
        
        
        public static TableSchema.TableColumn RemittanceMemoColumn
        {
            get { return Schema.Columns[21]; }
        }
        
        
        
        public static TableSchema.TableColumn MessageContentColumn
        {
            get { return Schema.Columns[22]; }
        }
        
        
        
        public static TableSchema.TableColumn ProcessStatusColumn
        {
            get { return Schema.Columns[23]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string SEQNo = @"SEQNo";
			 public static string InfoType = @"InfoType";
			 public static string ResponseStats = @"ResponseStats";
			 public static string ReceiveDate = @"ReceiveDate";
			 public static string AccountNoReceive = @"AccountNoReceive";
			 public static string AccountType = @"AccountType";
			 public static string AccountDate = @"AccountDate";
			 public static string Memo1 = @"Memo1";
			 public static string TransactionAmount = @"TransactionAmount";
			 public static string TransactionDate = @"TransactionDate";
			 public static string TransactionTime = @"TransactionTime";
			 public static string CrossBankNo = @"CrossBankNo";
			 public static string DebitCredit = @"DebitCredit";
			 public static string AmountType = @"AmountType";
			 public static string ServiceManNo = @"ServiceManNo";
			 public static string TransactionID = @"TransactionID";
			 public static string TransactionType = @"TransactionType";
			 public static string AccountNoSend = @"AccountNoSend";
			 public static string Memo2 = @"Memo2";
			 public static string CashIncrease = @"CashIncrease";
			 public static string Remitter = @"Remitter";
			 public static string RemittanceMemo = @"RemittanceMemo";
			 public static string MessageContent = @"MessageContent";
			 public static string ProcessStatus = @"ProcessStatus";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

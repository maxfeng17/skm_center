﻿using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the ProductLog class.
    /// </summary>
    [Serializable]
    public partial class ProductLogCollection : RepositoryList<ProductLog, ProductLogCollection>
    {
        public ProductLogCollection() { }

        /// <summary>
        /// Filters an existing collection based on the set criteria. This is an in-memory filter
        /// Thanks to developingchris for this!
        /// </summary>
        /// <returns>ProductLogCollection</returns>
        public ProductLogCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                ProductLog o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }


    }
    /// <summary>
    /// This is an ActiveRecord class which wraps the product_log table.
    /// </summary>
    [Serializable]
    public partial class ProductLog : RepositoryRecord<ProductLog>, IRecordBase
    {
        #region .ctors and Default Settings

        public ProductLog()
        {
            SetSQLProps();
            InitSetDefaults();
            MarkNew();
        }

        private void InitSetDefaults() { SetDefaults(); }

        public ProductLog(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
                ForceDefaults();
            MarkNew();
        }

        protected static void SetSQLProps() { GetTableSchema(); }

        #endregion

        #region Schema and Query Accessor	
        public static Query CreateQuery() { return new Query(Schema); }
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                    SetSQLProps();
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("product_log", TableType.Table, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = true;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = true;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                colvarId.DefaultSetting = @"";
                colvarId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarId);

                TableSchema.TableColumn colvarContentLog = new TableSchema.TableColumn(schema);
                colvarContentLog.ColumnName = "content_log";
                colvarContentLog.DataType = DbType.String;
                colvarContentLog.MaxLength = -1;
                colvarContentLog.AutoIncrement = false;
                colvarContentLog.IsNullable = true;
                colvarContentLog.IsPrimaryKey = false;
                colvarContentLog.IsForeignKey = false;
                colvarContentLog.IsReadOnly = false;
                colvarContentLog.DefaultSetting = @"";
                colvarContentLog.ForeignKeyTableName = "";
                schema.Columns.Add(colvarContentLog);

                TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
                colvarCreateId.ColumnName = "create_id";
                colvarCreateId.DataType = DbType.AnsiString;
                colvarCreateId.MaxLength = 100;
                colvarCreateId.AutoIncrement = false;
                colvarCreateId.IsNullable = true;
                colvarCreateId.IsPrimaryKey = false;
                colvarCreateId.IsForeignKey = false;
                colvarCreateId.IsReadOnly = false;
                colvarCreateId.DefaultSetting = @"";
                colvarCreateId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateId);

                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = true;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                colvarCreateTime.DefaultSetting = @"";
                colvarCreateTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateTime);

                TableSchema.TableColumn colvarProductGuid = new TableSchema.TableColumn(schema);
                colvarProductGuid.ColumnName = "product_guid";
                colvarProductGuid.DataType = DbType.Guid;
                colvarProductGuid.MaxLength = 0;
                colvarProductGuid.AutoIncrement = false;
                colvarProductGuid.IsNullable = true;
                colvarProductGuid.IsPrimaryKey = false;
                colvarProductGuid.IsForeignKey = false;
                colvarProductGuid.IsReadOnly = false;
                colvarProductGuid.DefaultSetting = @"";
                colvarProductGuid.ForeignKeyTableName = "";
                schema.Columns.Add(colvarProductGuid);

                TableSchema.TableColumn colvarChangeLog = new TableSchema.TableColumn(schema);
                colvarChangeLog.ColumnName = "change_log";
                colvarChangeLog.DataType = DbType.String;
                colvarChangeLog.MaxLength = -1;
                colvarChangeLog.AutoIncrement = false;
                colvarChangeLog.IsNullable = true;
                colvarChangeLog.IsPrimaryKey = false;
                colvarChangeLog.IsForeignKey = false;
                colvarChangeLog.IsReadOnly = false;
                colvarChangeLog.DefaultSetting = @"";
                colvarChangeLog.ForeignKeyTableName = "";
                schema.Columns.Add(colvarChangeLog);

                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("product_log", schema);
            }
        }
        #endregion

        #region Props

        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id
        {
            get { return GetColumnValue<int>(Columns.Id); }
            set { SetColumnValue(Columns.Id, value); }
        }

        [XmlAttribute("ContentLog")]
        [Bindable(true)]
        public string ContentLog
        {
            get { return GetColumnValue<string>(Columns.ContentLog); }
            set { SetColumnValue(Columns.ContentLog, value); }
        }

        [XmlAttribute("CreateId")]
        [Bindable(true)]
        public string CreateId
        {
            get { return GetColumnValue<string>(Columns.CreateId); }
            set { SetColumnValue(Columns.CreateId, value); }
        }

        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime? CreateTime
        {
            get { return GetColumnValue<DateTime?>(Columns.CreateTime); }
            set { SetColumnValue(Columns.CreateTime, value); }
        }

        [XmlAttribute("ProductGuid")]
        [Bindable(true)]
        public Guid? ProductGuid
        {
            get { return GetColumnValue<Guid?>(Columns.ProductGuid); }
            set { SetColumnValue(Columns.ProductGuid, value); }
        }

        [XmlAttribute("ChangeLog")]
        [Bindable(true)]
        public string ChangeLog
        {
            get { return GetColumnValue<string>(Columns.ChangeLog); }
            set { SetColumnValue(Columns.ChangeLog, value); }
        }

        #endregion




        //no foreign key tables defined (0)



        //no ManyToMany tables defined (0)





        #region Typed Columns


        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }



        public static TableSchema.TableColumn ContentLogColumn
        {
            get { return Schema.Columns[1]; }
        }



        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[2]; }
        }



        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[3]; }
        }



        public static TableSchema.TableColumn ProductGuidColumn
        {
            get { return Schema.Columns[4]; }
        }



        public static TableSchema.TableColumn ChangeLogColumn
        {
            get { return Schema.Columns[5]; }
        }



        #endregion
        #region Columns Struct
        public struct Columns
        {
            public static string Id = @"id";
            public static string ContentLog = @"content_log";
            public static string CreateId = @"create_id";
            public static string CreateTime = @"create_time";
            public static string ProductGuid = @"product_guid";
            public static string ChangeLog = @"change_log";

        }
        #endregion

        #region Update PK Collections

        #endregion

        #region Deep Save

        #endregion
    }
}

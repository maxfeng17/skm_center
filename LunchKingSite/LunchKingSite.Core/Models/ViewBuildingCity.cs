using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
using Microsoft.SqlServer.Types;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewBuildingCity class.
    /// </summary>
    [Serializable]
    public partial class ViewBuildingCityCollection : ReadOnlyList<ViewBuildingCity, ViewBuildingCityCollection>
    {        
        public ViewBuildingCityCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_building_city view.
    /// </summary>
    [Serializable]
    public partial class ViewBuildingCity : ReadOnlyRecord<ViewBuildingCity>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_building_city", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarGuid = new TableSchema.TableColumn(schema);
                colvarGuid.ColumnName = "GUID";
                colvarGuid.DataType = DbType.Guid;
                colvarGuid.MaxLength = 0;
                colvarGuid.AutoIncrement = false;
                colvarGuid.IsNullable = false;
                colvarGuid.IsPrimaryKey = false;
                colvarGuid.IsForeignKey = false;
                colvarGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarGuid);
                
                TableSchema.TableColumn colvarBuildingId = new TableSchema.TableColumn(schema);
                colvarBuildingId.ColumnName = "building_id";
                colvarBuildingId.DataType = DbType.AnsiString;
                colvarBuildingId.MaxLength = 20;
                colvarBuildingId.AutoIncrement = false;
                colvarBuildingId.IsNullable = true;
                colvarBuildingId.IsPrimaryKey = false;
                colvarBuildingId.IsForeignKey = false;
                colvarBuildingId.IsReadOnly = false;
                
                schema.Columns.Add(colvarBuildingId);
                
                TableSchema.TableColumn colvarBuildingName = new TableSchema.TableColumn(schema);
                colvarBuildingName.ColumnName = "building_name";
                colvarBuildingName.DataType = DbType.String;
                colvarBuildingName.MaxLength = 50;
                colvarBuildingName.AutoIncrement = false;
                colvarBuildingName.IsNullable = false;
                colvarBuildingName.IsPrimaryKey = false;
                colvarBuildingName.IsForeignKey = false;
                colvarBuildingName.IsReadOnly = false;
                
                schema.Columns.Add(colvarBuildingName);
                
                TableSchema.TableColumn colvarBuildingStreetName = new TableSchema.TableColumn(schema);
                colvarBuildingStreetName.ColumnName = "building_street_name";
                colvarBuildingStreetName.DataType = DbType.String;
                colvarBuildingStreetName.MaxLength = 100;
                colvarBuildingStreetName.AutoIncrement = false;
                colvarBuildingStreetName.IsNullable = false;
                colvarBuildingStreetName.IsPrimaryKey = false;
                colvarBuildingStreetName.IsForeignKey = false;
                colvarBuildingStreetName.IsReadOnly = false;
                
                schema.Columns.Add(colvarBuildingStreetName);
                
                TableSchema.TableColumn colvarBuildingAddressNumber = new TableSchema.TableColumn(schema);
                colvarBuildingAddressNumber.ColumnName = "building_address_number";
                colvarBuildingAddressNumber.DataType = DbType.String;
                colvarBuildingAddressNumber.MaxLength = 100;
                colvarBuildingAddressNumber.AutoIncrement = false;
                colvarBuildingAddressNumber.IsNullable = true;
                colvarBuildingAddressNumber.IsPrimaryKey = false;
                colvarBuildingAddressNumber.IsForeignKey = false;
                colvarBuildingAddressNumber.IsReadOnly = false;
                
                schema.Columns.Add(colvarBuildingAddressNumber);
                
                TableSchema.TableColumn colvarBuildingAddress = new TableSchema.TableColumn(schema);
                colvarBuildingAddress.ColumnName = "building_address";
                colvarBuildingAddress.DataType = DbType.String;
                colvarBuildingAddress.MaxLength = 200;
                colvarBuildingAddress.AutoIncrement = false;
                colvarBuildingAddress.IsNullable = true;
                colvarBuildingAddress.IsPrimaryKey = false;
                colvarBuildingAddress.IsForeignKey = false;
                colvarBuildingAddress.IsReadOnly = false;
                
                schema.Columns.Add(colvarBuildingAddress);
                
                TableSchema.TableColumn colvarBuildingOnline = new TableSchema.TableColumn(schema);
                colvarBuildingOnline.ColumnName = "building_online";
                colvarBuildingOnline.DataType = DbType.Boolean;
                colvarBuildingOnline.MaxLength = 0;
                colvarBuildingOnline.AutoIncrement = false;
                colvarBuildingOnline.IsNullable = false;
                colvarBuildingOnline.IsPrimaryKey = false;
                colvarBuildingOnline.IsForeignKey = false;
                colvarBuildingOnline.IsReadOnly = false;
                
                schema.Columns.Add(colvarBuildingOnline);
                
                TableSchema.TableColumn colvarBuildingRank = new TableSchema.TableColumn(schema);
                colvarBuildingRank.ColumnName = "building_rank";
                colvarBuildingRank.DataType = DbType.Int32;
                colvarBuildingRank.MaxLength = 0;
                colvarBuildingRank.AutoIncrement = false;
                colvarBuildingRank.IsNullable = false;
                colvarBuildingRank.IsPrimaryKey = false;
                colvarBuildingRank.IsForeignKey = false;
                colvarBuildingRank.IsReadOnly = false;
                
                schema.Columns.Add(colvarBuildingRank);
                
                TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
                colvarCreateId.ColumnName = "create_id";
                colvarCreateId.DataType = DbType.String;
                colvarCreateId.MaxLength = 30;
                colvarCreateId.AutoIncrement = false;
                colvarCreateId.IsNullable = false;
                colvarCreateId.IsPrimaryKey = false;
                colvarCreateId.IsForeignKey = false;
                colvarCreateId.IsReadOnly = false;
                
                schema.Columns.Add(colvarCreateId);
                
                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = false;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarCreateTime);
                
                TableSchema.TableColumn colvarModifyId = new TableSchema.TableColumn(schema);
                colvarModifyId.ColumnName = "modify_id";
                colvarModifyId.DataType = DbType.String;
                colvarModifyId.MaxLength = 30;
                colvarModifyId.AutoIncrement = false;
                colvarModifyId.IsNullable = true;
                colvarModifyId.IsPrimaryKey = false;
                colvarModifyId.IsForeignKey = false;
                colvarModifyId.IsReadOnly = false;
                
                schema.Columns.Add(colvarModifyId);
                
                TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
                colvarModifyTime.ColumnName = "modify_time";
                colvarModifyTime.DataType = DbType.DateTime;
                colvarModifyTime.MaxLength = 0;
                colvarModifyTime.AutoIncrement = false;
                colvarModifyTime.IsNullable = true;
                colvarModifyTime.IsPrimaryKey = false;
                colvarModifyTime.IsForeignKey = false;
                colvarModifyTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarModifyTime);
                
                TableSchema.TableColumn colvarCityName = new TableSchema.TableColumn(schema);
                colvarCityName.ColumnName = "city_name";
                colvarCityName.DataType = DbType.String;
                colvarCityName.MaxLength = 20;
                colvarCityName.AutoIncrement = false;
                colvarCityName.IsNullable = false;
                colvarCityName.IsPrimaryKey = false;
                colvarCityName.IsForeignKey = false;
                colvarCityName.IsReadOnly = false;
                
                schema.Columns.Add(colvarCityName);
                
                TableSchema.TableColumn colvarCode = new TableSchema.TableColumn(schema);
                colvarCode.ColumnName = "code";
                colvarCode.DataType = DbType.AnsiString;
                colvarCode.MaxLength = 10;
                colvarCode.AutoIncrement = false;
                colvarCode.IsNullable = true;
                colvarCode.IsPrimaryKey = false;
                colvarCode.IsForeignKey = false;
                colvarCode.IsReadOnly = false;
                
                schema.Columns.Add(colvarCode);
                
                TableSchema.TableColumn colvarCoordinate = new TableSchema.TableColumn(schema);
                colvarCoordinate.ColumnName = "coordinate";
                colvarCoordinate.DataType = DbType.AnsiString;
                colvarCoordinate.MaxLength = -1;
                colvarCoordinate.AutoIncrement = false;
                colvarCoordinate.IsNullable = true;
                colvarCoordinate.IsPrimaryKey = false;
                colvarCoordinate.IsForeignKey = false;
                colvarCoordinate.IsReadOnly = false;
                
                schema.Columns.Add(colvarCoordinate);
                
                TableSchema.TableColumn colvarCityId = new TableSchema.TableColumn(schema);
                colvarCityId.ColumnName = "city_id";
                colvarCityId.DataType = DbType.Int32;
                colvarCityId.MaxLength = 0;
                colvarCityId.AutoIncrement = false;
                colvarCityId.IsNullable = false;
                colvarCityId.IsPrimaryKey = false;
                colvarCityId.IsForeignKey = false;
                colvarCityId.IsReadOnly = false;
                
                schema.Columns.Add(colvarCityId);
                
                TableSchema.TableColumn colvarParentId = new TableSchema.TableColumn(schema);
                colvarParentId.ColumnName = "parent_id";
                colvarParentId.DataType = DbType.Int32;
                colvarParentId.MaxLength = 0;
                colvarParentId.AutoIncrement = false;
                colvarParentId.IsNullable = true;
                colvarParentId.IsPrimaryKey = false;
                colvarParentId.IsForeignKey = false;
                colvarParentId.IsReadOnly = false;
                
                schema.Columns.Add(colvarParentId);
                
                TableSchema.TableColumn colvarCityStatus = new TableSchema.TableColumn(schema);
                colvarCityStatus.ColumnName = "city_status";
                colvarCityStatus.DataType = DbType.Int32;
                colvarCityStatus.MaxLength = 0;
                colvarCityStatus.AutoIncrement = false;
                colvarCityStatus.IsNullable = true;
                colvarCityStatus.IsPrimaryKey = false;
                colvarCityStatus.IsForeignKey = false;
                colvarCityStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarCityStatus);
                
                TableSchema.TableColumn colvarCityRank = new TableSchema.TableColumn(schema);
                colvarCityRank.ColumnName = "city_rank";
                colvarCityRank.DataType = DbType.Int32;
                colvarCityRank.MaxLength = 0;
                colvarCityRank.AutoIncrement = false;
                colvarCityRank.IsNullable = true;
                colvarCityRank.IsPrimaryKey = false;
                colvarCityRank.IsForeignKey = false;
                colvarCityRank.IsReadOnly = false;
                
                schema.Columns.Add(colvarCityRank);
                
                TableSchema.TableColumn colvarZipCode = new TableSchema.TableColumn(schema);
                colvarZipCode.ColumnName = "zip_code";
                colvarZipCode.DataType = DbType.AnsiString;
                colvarZipCode.MaxLength = 5;
                colvarZipCode.AutoIncrement = false;
                colvarZipCode.IsNullable = true;
                colvarZipCode.IsPrimaryKey = false;
                colvarZipCode.IsForeignKey = false;
                colvarZipCode.IsReadOnly = false;
                
                schema.Columns.Add(colvarZipCode);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_building_city",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewBuildingCity()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewBuildingCity(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewBuildingCity(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewBuildingCity(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("Guid")]
        [Bindable(true)]
        public Guid Guid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("GUID");
		    }
            set 
		    {
			    SetColumnValue("GUID", value);
            }
        }
	      
        [XmlAttribute("BuildingId")]
        [Bindable(true)]
        public string BuildingId 
	    {
		    get
		    {
			    return GetColumnValue<string>("building_id");
		    }
            set 
		    {
			    SetColumnValue("building_id", value);
            }
        }
	      
        [XmlAttribute("BuildingName")]
        [Bindable(true)]
        public string BuildingName 
	    {
		    get
		    {
			    return GetColumnValue<string>("building_name");
		    }
            set 
		    {
			    SetColumnValue("building_name", value);
            }
        }
	      
        [XmlAttribute("BuildingStreetName")]
        [Bindable(true)]
        public string BuildingStreetName 
	    {
		    get
		    {
			    return GetColumnValue<string>("building_street_name");
		    }
            set 
		    {
			    SetColumnValue("building_street_name", value);
            }
        }
	      
        [XmlAttribute("BuildingAddressNumber")]
        [Bindable(true)]
        public string BuildingAddressNumber 
	    {
		    get
		    {
			    return GetColumnValue<string>("building_address_number");
		    }
            set 
		    {
			    SetColumnValue("building_address_number", value);
            }
        }
	      
        [XmlAttribute("BuildingAddress")]
        [Bindable(true)]
        public string BuildingAddress 
	    {
		    get
		    {
			    return GetColumnValue<string>("building_address");
		    }
            set 
		    {
			    SetColumnValue("building_address", value);
            }
        }
	      
        [XmlAttribute("BuildingOnline")]
        [Bindable(true)]
        public bool BuildingOnline 
	    {
		    get
		    {
			    return GetColumnValue<bool>("building_online");
		    }
            set 
		    {
			    SetColumnValue("building_online", value);
            }
        }
	      
        [XmlAttribute("BuildingRank")]
        [Bindable(true)]
        public int BuildingRank 
	    {
		    get
		    {
			    return GetColumnValue<int>("building_rank");
		    }
            set 
		    {
			    SetColumnValue("building_rank", value);
            }
        }
	      
        [XmlAttribute("CreateId")]
        [Bindable(true)]
        public string CreateId 
	    {
		    get
		    {
			    return GetColumnValue<string>("create_id");
		    }
            set 
		    {
			    SetColumnValue("create_id", value);
            }
        }
	      
        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime CreateTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("create_time");
		    }
            set 
		    {
			    SetColumnValue("create_time", value);
            }
        }
	      
        [XmlAttribute("ModifyId")]
        [Bindable(true)]
        public string ModifyId 
	    {
		    get
		    {
			    return GetColumnValue<string>("modify_id");
		    }
            set 
		    {
			    SetColumnValue("modify_id", value);
            }
        }
	      
        [XmlAttribute("ModifyTime")]
        [Bindable(true)]
        public DateTime? ModifyTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("modify_time");
		    }
            set 
		    {
			    SetColumnValue("modify_time", value);
            }
        }
	      
        [XmlAttribute("CityName")]
        [Bindable(true)]
        public string CityName 
	    {
		    get
		    {
			    return GetColumnValue<string>("city_name");
		    }
            set 
		    {
			    SetColumnValue("city_name", value);
            }
        }
	      
        [XmlAttribute("Code")]
        [Bindable(true)]
        public string Code 
	    {
		    get
		    {
			    return GetColumnValue<string>("code");
		    }
            set 
		    {
			    SetColumnValue("code", value);
            }
        }
	      
        [XmlAttribute("Coordinate")]
        [Bindable(true)]
        public string Coordinate 
	    {
		    get
		    {
                return GetColumnValue<object>("coordinate") != null ? GetColumnValue<object>("coordinate").ToString() : string.Empty;
		    }
            set 
		    {
			    SetColumnValue("coordinate", value);
            }
        }
	      
        [XmlAttribute("CityId")]
        [Bindable(true)]
        public int CityId 
	    {
		    get
		    {
			    return GetColumnValue<int>("city_id");
		    }
            set 
		    {
			    SetColumnValue("city_id", value);
            }
        }
	      
        [XmlAttribute("ParentId")]
        [Bindable(true)]
        public int? ParentId 
	    {
		    get
		    {
			    return GetColumnValue<int?>("parent_id");
		    }
            set 
		    {
			    SetColumnValue("parent_id", value);
            }
        }
	      
        [XmlAttribute("CityStatus")]
        [Bindable(true)]
        public int? CityStatus 
	    {
		    get
		    {
			    return GetColumnValue<int?>("city_status");
		    }
            set 
		    {
			    SetColumnValue("city_status", value);
            }
        }
	      
        [XmlAttribute("CityRank")]
        [Bindable(true)]
        public int? CityRank 
	    {
		    get
		    {
			    return GetColumnValue<int?>("city_rank");
		    }
            set 
		    {
			    SetColumnValue("city_rank", value);
            }
        }
	      
        [XmlAttribute("ZipCode")]
        [Bindable(true)]
        public string ZipCode 
	    {
		    get
		    {
			    return GetColumnValue<string>("zip_code");
		    }
            set 
		    {
			    SetColumnValue("zip_code", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string Guid = @"GUID";
            
            public static string BuildingId = @"building_id";
            
            public static string BuildingName = @"building_name";
            
            public static string BuildingStreetName = @"building_street_name";
            
            public static string BuildingAddressNumber = @"building_address_number";
            
            public static string BuildingAddress = @"building_address";
            
            public static string BuildingOnline = @"building_online";
            
            public static string BuildingRank = @"building_rank";
            
            public static string CreateId = @"create_id";
            
            public static string CreateTime = @"create_time";
            
            public static string ModifyId = @"modify_id";
            
            public static string ModifyTime = @"modify_time";
            
            public static string CityName = @"city_name";
            
            public static string Code = @"code";
            
            public static string Coordinate = @"coordinate";
            
            public static string CityId = @"city_id";
            
            public static string ParentId = @"parent_id";
            
            public static string CityStatus = @"city_status";
            
            public static string CityRank = @"city_rank";
            
            public static string ZipCode = @"zip_code";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

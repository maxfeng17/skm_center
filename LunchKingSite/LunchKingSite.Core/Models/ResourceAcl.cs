using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the ResourceAcl class.
	/// </summary>
    [Serializable]
	public partial class ResourceAclCollection : RepositoryList<ResourceAcl, ResourceAclCollection>
	{	   
		public ResourceAclCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>ResourceAclCollection</returns>
		public ResourceAclCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                ResourceAcl o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the resource_acl table.
	/// </summary>
	[Serializable]
	public partial class ResourceAcl : RepositoryRecord<ResourceAcl>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public ResourceAcl()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public ResourceAcl(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("resource_acl", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarAccountId = new TableSchema.TableColumn(schema);
				colvarAccountId.ColumnName = "account_id";
				colvarAccountId.DataType = DbType.String;
				colvarAccountId.MaxLength = 256;
				colvarAccountId.AutoIncrement = false;
				colvarAccountId.IsNullable = false;
				colvarAccountId.IsPrimaryKey = false;
				colvarAccountId.IsForeignKey = false;
				colvarAccountId.IsReadOnly = false;
				colvarAccountId.DefaultSetting = @"";
				colvarAccountId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAccountId);
				
				TableSchema.TableColumn colvarAccountType = new TableSchema.TableColumn(schema);
				colvarAccountType.ColumnName = "account_type";
				colvarAccountType.DataType = DbType.Int32;
				colvarAccountType.MaxLength = 0;
				colvarAccountType.AutoIncrement = false;
				colvarAccountType.IsNullable = false;
				colvarAccountType.IsPrimaryKey = false;
				colvarAccountType.IsForeignKey = false;
				colvarAccountType.IsReadOnly = false;
				colvarAccountType.DefaultSetting = @"";
				colvarAccountType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAccountType);
				
				TableSchema.TableColumn colvarResourceGuid = new TableSchema.TableColumn(schema);
				colvarResourceGuid.ColumnName = "resource_guid";
				colvarResourceGuid.DataType = DbType.Guid;
				colvarResourceGuid.MaxLength = 0;
				colvarResourceGuid.AutoIncrement = false;
				colvarResourceGuid.IsNullable = false;
				colvarResourceGuid.IsPrimaryKey = false;
				colvarResourceGuid.IsForeignKey = false;
				colvarResourceGuid.IsReadOnly = false;
				colvarResourceGuid.DefaultSetting = @"";
				colvarResourceGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarResourceGuid);
				
				TableSchema.TableColumn colvarResourceType = new TableSchema.TableColumn(schema);
				colvarResourceType.ColumnName = "resource_type";
				colvarResourceType.DataType = DbType.Int32;
				colvarResourceType.MaxLength = 0;
				colvarResourceType.AutoIncrement = false;
				colvarResourceType.IsNullable = false;
				colvarResourceType.IsPrimaryKey = false;
				colvarResourceType.IsForeignKey = false;
				colvarResourceType.IsReadOnly = false;
				colvarResourceType.DefaultSetting = @"";
				colvarResourceType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarResourceType);
				
				TableSchema.TableColumn colvarPermissionType = new TableSchema.TableColumn(schema);
				colvarPermissionType.ColumnName = "permission_type";
				colvarPermissionType.DataType = DbType.Int32;
				colvarPermissionType.MaxLength = 0;
				colvarPermissionType.AutoIncrement = false;
				colvarPermissionType.IsNullable = false;
				colvarPermissionType.IsPrimaryKey = false;
				colvarPermissionType.IsForeignKey = false;
				colvarPermissionType.IsReadOnly = false;
				colvarPermissionType.DefaultSetting = @"";
				colvarPermissionType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPermissionType);
				
				TableSchema.TableColumn colvarPermissionSetting = new TableSchema.TableColumn(schema);
				colvarPermissionSetting.ColumnName = "permission_setting";
				colvarPermissionSetting.DataType = DbType.Int32;
				colvarPermissionSetting.MaxLength = 0;
				colvarPermissionSetting.AutoIncrement = false;
				colvarPermissionSetting.IsNullable = false;
				colvarPermissionSetting.IsPrimaryKey = false;
				colvarPermissionSetting.IsForeignKey = false;
				colvarPermissionSetting.IsReadOnly = false;
				colvarPermissionSetting.DefaultSetting = @"";
				colvarPermissionSetting.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPermissionSetting);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("resource_acl",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("AccountId")]
		[Bindable(true)]
		public string AccountId 
		{
			get { return GetColumnValue<string>(Columns.AccountId); }
			set { SetColumnValue(Columns.AccountId, value); }
		}
		  
		[XmlAttribute("AccountType")]
		[Bindable(true)]
		public int AccountType 
		{
			get { return GetColumnValue<int>(Columns.AccountType); }
			set { SetColumnValue(Columns.AccountType, value); }
		}
		  
		[XmlAttribute("ResourceGuid")]
		[Bindable(true)]
		public Guid ResourceGuid 
		{
			get { return GetColumnValue<Guid>(Columns.ResourceGuid); }
			set { SetColumnValue(Columns.ResourceGuid, value); }
		}
		  
		[XmlAttribute("ResourceType")]
		[Bindable(true)]
		public int ResourceType 
		{
			get { return GetColumnValue<int>(Columns.ResourceType); }
			set { SetColumnValue(Columns.ResourceType, value); }
		}
		  
		[XmlAttribute("PermissionType")]
		[Bindable(true)]
		public int PermissionType 
		{
			get { return GetColumnValue<int>(Columns.PermissionType); }
			set { SetColumnValue(Columns.PermissionType, value); }
		}
		  
		[XmlAttribute("PermissionSetting")]
		[Bindable(true)]
		public int PermissionSetting 
		{
			get { return GetColumnValue<int>(Columns.PermissionSetting); }
			set { SetColumnValue(Columns.PermissionSetting, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn AccountIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn AccountTypeColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn ResourceGuidColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn ResourceTypeColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn PermissionTypeColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn PermissionSettingColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string AccountId = @"account_id";
			 public static string AccountType = @"account_type";
			 public static string ResourceGuid = @"resource_guid";
			 public static string ResourceType = @"resource_type";
			 public static string PermissionType = @"permission_type";
			 public static string PermissionSetting = @"permission_setting";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

using System;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using SubSonic;

namespace LunchKingSite.DataOrm
{
	[Serializable]
	public partial class ViewPponOrderCollection : ReadOnlyList<ViewPponOrder, ViewPponOrderCollection>
	{
			public ViewPponOrderCollection() {}

	}

	[Serializable]
	public partial class ViewPponOrder : ReadOnlyRecord<ViewPponOrder>, IReadOnlyRecord
	{
		#region .ctors and Default Settings
		public ViewPponOrder()
		{
			SetSQLProps();
			InitSetDefaults();
			MarkNew();
		}

		private void InitSetDefaults() { SetDefaults(); }

		public ViewPponOrder(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}

		protected static void SetSQLProps() { GetTableSchema(); }

		public ViewPponOrder(object keyID)
		{
				SetSQLProps();
				LoadByKey(keyID);
		}

		public ViewPponOrder(string columnName, object columnValue)
		{
				SetSQLProps();
				LoadByParam(columnName,columnValue);
		}
		#endregion

		#region Schema and Query Accessor
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}

		private static void GetTableSchema()
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("view_ppon_order", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				TableSchema.TableColumn colvarGuid = new TableSchema.TableColumn(schema);
				colvarGuid.ColumnName = "GUID";
				colvarGuid.DataType = DbType.Guid;
				colvarGuid.MaxLength = 0;
				colvarGuid.AutoIncrement = false;
				colvarGuid.IsNullable = false;
				colvarGuid.IsPrimaryKey = false;
				colvarGuid.IsForeignKey = false;
				colvarGuid.IsReadOnly = false;
				colvarGuid.DefaultSetting = @"";
				colvarGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGuid);

				TableSchema.TableColumn colvarOrderId = new TableSchema.TableColumn(schema);
				colvarOrderId.ColumnName = "order_id";
				colvarOrderId.DataType = DbType.AnsiString;
				colvarOrderId.MaxLength = 30;
				colvarOrderId.AutoIncrement = false;
				colvarOrderId.IsNullable = true;
				colvarOrderId.IsPrimaryKey = false;
				colvarOrderId.IsForeignKey = false;
				colvarOrderId.IsReadOnly = false;
				colvarOrderId.DefaultSetting = @"";
				colvarOrderId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderId);

				TableSchema.TableColumn colvarSellerGuid = new TableSchema.TableColumn(schema);
				colvarSellerGuid.ColumnName = "seller_GUID";
				colvarSellerGuid.DataType = DbType.Guid;
				colvarSellerGuid.MaxLength = 0;
				colvarSellerGuid.AutoIncrement = false;
				colvarSellerGuid.IsNullable = false;
				colvarSellerGuid.IsPrimaryKey = false;
				colvarSellerGuid.IsForeignKey = false;
				colvarSellerGuid.IsReadOnly = false;
				colvarSellerGuid.DefaultSetting = @"";
				colvarSellerGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSellerGuid);

				TableSchema.TableColumn colvarSellerName = new TableSchema.TableColumn(schema);
				colvarSellerName.ColumnName = "seller_name";
				colvarSellerName.DataType = DbType.String;
				colvarSellerName.MaxLength = 50;
				colvarSellerName.AutoIncrement = false;
				colvarSellerName.IsNullable = false;
				colvarSellerName.IsPrimaryKey = false;
				colvarSellerName.IsForeignKey = false;
				colvarSellerName.IsReadOnly = false;
				colvarSellerName.DefaultSetting = @"";
				colvarSellerName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSellerName);

				TableSchema.TableColumn colvarMemberEmail = new TableSchema.TableColumn(schema);
				colvarMemberEmail.ColumnName = "member_email";
				colvarMemberEmail.DataType = DbType.String;
				colvarMemberEmail.MaxLength = 256;
				colvarMemberEmail.AutoIncrement = false;
				colvarMemberEmail.IsNullable = false;
				colvarMemberEmail.IsPrimaryKey = false;
				colvarMemberEmail.IsForeignKey = false;
				colvarMemberEmail.IsReadOnly = false;
				colvarMemberEmail.DefaultSetting = @"";
				colvarMemberEmail.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMemberEmail);

				TableSchema.TableColumn colvarMemberName = new TableSchema.TableColumn(schema);
				colvarMemberName.ColumnName = "member_name";
				colvarMemberName.DataType = DbType.String;
				colvarMemberName.MaxLength = 50;
				colvarMemberName.AutoIncrement = false;
				colvarMemberName.IsNullable = false;
				colvarMemberName.IsPrimaryKey = false;
				colvarMemberName.IsForeignKey = false;
				colvarMemberName.IsReadOnly = false;
				colvarMemberName.DefaultSetting = @"";
				colvarMemberName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMemberName);

				TableSchema.TableColumn colvarPhoneNumber = new TableSchema.TableColumn(schema);
				colvarPhoneNumber.ColumnName = "phone_number";
				colvarPhoneNumber.DataType = DbType.AnsiString;
				colvarPhoneNumber.MaxLength = 50;
				colvarPhoneNumber.AutoIncrement = false;
				colvarPhoneNumber.IsNullable = true;
				colvarPhoneNumber.IsPrimaryKey = false;
				colvarPhoneNumber.IsForeignKey = false;
				colvarPhoneNumber.IsReadOnly = false;
				colvarPhoneNumber.DefaultSetting = @"";
				colvarPhoneNumber.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPhoneNumber);

				TableSchema.TableColumn colvarMobileNumber = new TableSchema.TableColumn(schema);
				colvarMobileNumber.ColumnName = "mobile_number";
				colvarMobileNumber.DataType = DbType.AnsiString;
				colvarMobileNumber.MaxLength = 50;
				colvarMobileNumber.AutoIncrement = false;
				colvarMobileNumber.IsNullable = true;
				colvarMobileNumber.IsPrimaryKey = false;
				colvarMobileNumber.IsForeignKey = false;
				colvarMobileNumber.IsReadOnly = false;
				colvarMobileNumber.DefaultSetting = @"";
				colvarMobileNumber.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMobileNumber);

				TableSchema.TableColumn colvarDeliveryAddress = new TableSchema.TableColumn(schema);
				colvarDeliveryAddress.ColumnName = "delivery_address";
				colvarDeliveryAddress.DataType = DbType.String;
				colvarDeliveryAddress.MaxLength = 200;
				colvarDeliveryAddress.AutoIncrement = false;
				colvarDeliveryAddress.IsNullable = true;
				colvarDeliveryAddress.IsPrimaryKey = false;
				colvarDeliveryAddress.IsForeignKey = false;
				colvarDeliveryAddress.IsReadOnly = false;
				colvarDeliveryAddress.DefaultSetting = @"";
				colvarDeliveryAddress.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDeliveryAddress);

				TableSchema.TableColumn colvarDeliveryTime = new TableSchema.TableColumn(schema);
				colvarDeliveryTime.ColumnName = "delivery_time";
				colvarDeliveryTime.DataType = DbType.DateTime;
				colvarDeliveryTime.MaxLength = 0;
				colvarDeliveryTime.AutoIncrement = false;
				colvarDeliveryTime.IsNullable = true;
				colvarDeliveryTime.IsPrimaryKey = false;
				colvarDeliveryTime.IsForeignKey = false;
				colvarDeliveryTime.IsReadOnly = false;
				colvarDeliveryTime.DefaultSetting = @"";
				colvarDeliveryTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDeliveryTime);

				TableSchema.TableColumn colvarSubtotal = new TableSchema.TableColumn(schema);
				colvarSubtotal.ColumnName = "subtotal";
				colvarSubtotal.DataType = DbType.Currency;
				colvarSubtotal.MaxLength = 0;
				colvarSubtotal.AutoIncrement = false;
				colvarSubtotal.IsNullable = false;
				colvarSubtotal.IsPrimaryKey = false;
				colvarSubtotal.IsForeignKey = false;
				colvarSubtotal.IsReadOnly = false;
				colvarSubtotal.DefaultSetting = @"";
				colvarSubtotal.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSubtotal);

				TableSchema.TableColumn colvarTotal = new TableSchema.TableColumn(schema);
				colvarTotal.ColumnName = "total";
				colvarTotal.DataType = DbType.Currency;
				colvarTotal.MaxLength = 0;
				colvarTotal.AutoIncrement = false;
				colvarTotal.IsNullable = false;
				colvarTotal.IsPrimaryKey = false;
				colvarTotal.IsForeignKey = false;
				colvarTotal.IsReadOnly = false;
				colvarTotal.DefaultSetting = @"";
				colvarTotal.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTotal);

				TableSchema.TableColumn colvarUserMemo = new TableSchema.TableColumn(schema);
				colvarUserMemo.ColumnName = "user_memo";
				colvarUserMemo.DataType = DbType.String;
				colvarUserMemo.MaxLength = 1073741823;
				colvarUserMemo.AutoIncrement = false;
				colvarUserMemo.IsNullable = true;
				colvarUserMemo.IsPrimaryKey = false;
				colvarUserMemo.IsForeignKey = false;
				colvarUserMemo.IsReadOnly = false;
				colvarUserMemo.DefaultSetting = @"";
				colvarUserMemo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUserMemo);

				TableSchema.TableColumn colvarOrderMemo = new TableSchema.TableColumn(schema);
				colvarOrderMemo.ColumnName = "order_memo";
				colvarOrderMemo.DataType = DbType.String;
				colvarOrderMemo.MaxLength = 1073741823;
				colvarOrderMemo.AutoIncrement = false;
				colvarOrderMemo.IsNullable = true;
				colvarOrderMemo.IsPrimaryKey = false;
				colvarOrderMemo.IsForeignKey = false;
				colvarOrderMemo.IsReadOnly = false;
				colvarOrderMemo.DefaultSetting = @"";
				colvarOrderMemo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderMemo);

				TableSchema.TableColumn colvarOrderStatus = new TableSchema.TableColumn(schema);
				colvarOrderStatus.ColumnName = "order_status";
				colvarOrderStatus.DataType = DbType.Int32;
				colvarOrderStatus.MaxLength = 0;
				colvarOrderStatus.AutoIncrement = false;
				colvarOrderStatus.IsNullable = false;
				colvarOrderStatus.IsPrimaryKey = false;
				colvarOrderStatus.IsForeignKey = false;
				colvarOrderStatus.IsReadOnly = false;
				colvarOrderStatus.DefaultSetting = @"";
				colvarOrderStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderStatus);

				TableSchema.TableColumn colvarOrderStage = new TableSchema.TableColumn(schema);
				colvarOrderStage.ColumnName = "order_stage";
				colvarOrderStage.DataType = DbType.Int32;
				colvarOrderStage.MaxLength = 0;
				colvarOrderStage.AutoIncrement = false;
				colvarOrderStage.IsNullable = false;
				colvarOrderStage.IsPrimaryKey = false;
				colvarOrderStage.IsForeignKey = false;
				colvarOrderStage.IsReadOnly = false;
				colvarOrderStage.DefaultSetting = @"";
				colvarOrderStage.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderStage);

				TableSchema.TableColumn colvarBonusPoint = new TableSchema.TableColumn(schema);
				colvarBonusPoint.ColumnName = "bonus_point";
				colvarBonusPoint.DataType = DbType.AnsiString;
				colvarBonusPoint.MaxLength = 10;
				colvarBonusPoint.AutoIncrement = false;
				colvarBonusPoint.IsNullable = true;
				colvarBonusPoint.IsPrimaryKey = false;
				colvarBonusPoint.IsForeignKey = false;
				colvarBonusPoint.IsReadOnly = false;
				colvarBonusPoint.DefaultSetting = @"";
				colvarBonusPoint.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBonusPoint);

				TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
				colvarCreateId.ColumnName = "create_id";
				colvarCreateId.DataType = DbType.String;
				colvarCreateId.MaxLength = 30;
				colvarCreateId.AutoIncrement = false;
				colvarCreateId.IsNullable = false;
				colvarCreateId.IsPrimaryKey = false;
				colvarCreateId.IsForeignKey = false;
				colvarCreateId.IsReadOnly = false;
				colvarCreateId.DefaultSetting = @"";
				colvarCreateId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateId);

				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);

				TableSchema.TableColumn colvarModifyId = new TableSchema.TableColumn(schema);
				colvarModifyId.ColumnName = "modify_id";
				colvarModifyId.DataType = DbType.String;
				colvarModifyId.MaxLength = 30;
				colvarModifyId.AutoIncrement = false;
				colvarModifyId.IsNullable = true;
				colvarModifyId.IsPrimaryKey = false;
				colvarModifyId.IsForeignKey = false;
				colvarModifyId.IsReadOnly = false;
				colvarModifyId.DefaultSetting = @"";
				colvarModifyId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyId);

				TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
				colvarModifyTime.ColumnName = "modify_time";
				colvarModifyTime.DataType = DbType.DateTime;
				colvarModifyTime.MaxLength = 0;
				colvarModifyTime.AutoIncrement = false;
				colvarModifyTime.IsNullable = true;
				colvarModifyTime.IsPrimaryKey = false;
				colvarModifyTime.IsForeignKey = false;
				colvarModifyTime.IsReadOnly = false;
				colvarModifyTime.DefaultSetting = @"";
				colvarModifyTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyTime);

				TableSchema.TableColumn colvarAccessLock = new TableSchema.TableColumn(schema);
				colvarAccessLock.ColumnName = "access_lock";
				colvarAccessLock.DataType = DbType.String;
				colvarAccessLock.MaxLength = 50;
				colvarAccessLock.AutoIncrement = false;
				colvarAccessLock.IsNullable = true;
				colvarAccessLock.IsPrimaryKey = false;
				colvarAccessLock.IsForeignKey = false;
				colvarAccessLock.IsReadOnly = false;
				colvarAccessLock.DefaultSetting = @"";
				colvarAccessLock.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAccessLock);

				TableSchema.TableColumn colvarCityId = new TableSchema.TableColumn(schema);
				colvarCityId.ColumnName = "city_id";
				colvarCityId.DataType = DbType.Int32;
				colvarCityId.MaxLength = 0;
				colvarCityId.AutoIncrement = false;
				colvarCityId.IsNullable = true;
				colvarCityId.IsPrimaryKey = false;
				colvarCityId.IsForeignKey = false;
				colvarCityId.IsReadOnly = false;
				colvarCityId.DefaultSetting = @"";
				colvarCityId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCityId);

				TableSchema.TableColumn colvarParentOrderId = new TableSchema.TableColumn(schema);
				colvarParentOrderId.ColumnName = "parent_order_id";
				colvarParentOrderId.DataType = DbType.Guid;
				colvarParentOrderId.MaxLength = 0;
				colvarParentOrderId.AutoIncrement = false;
				colvarParentOrderId.IsNullable = true;
				colvarParentOrderId.IsPrimaryKey = false;
				colvarParentOrderId.IsForeignKey = false;
				colvarParentOrderId.IsReadOnly = false;
				colvarParentOrderId.DefaultSetting = @"";
				colvarParentOrderId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarParentOrderId);

				TableSchema.TableColumn colvarGroupOrderGuid = new TableSchema.TableColumn(schema);
				colvarGroupOrderGuid.ColumnName = "group_order_guid";
				colvarGroupOrderGuid.DataType = DbType.Guid;
				colvarGroupOrderGuid.MaxLength = 0;
				colvarGroupOrderGuid.AutoIncrement = false;
				colvarGroupOrderGuid.IsNullable = false;
				colvarGroupOrderGuid.IsPrimaryKey = false;
				colvarGroupOrderGuid.IsForeignKey = false;
				colvarGroupOrderGuid.IsReadOnly = false;
				colvarGroupOrderGuid.DefaultSetting = @"";
				colvarGroupOrderGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGroupOrderGuid);

				TableSchema.TableColumn colvarMemberUserName = new TableSchema.TableColumn(schema);
				colvarMemberUserName.ColumnName = "member_user_name";
				colvarMemberUserName.DataType = DbType.String;
				colvarMemberUserName.MaxLength = 256;
				colvarMemberUserName.AutoIncrement = false;
				colvarMemberUserName.IsNullable = false;
				colvarMemberUserName.IsPrimaryKey = false;
				colvarMemberUserName.IsForeignKey = false;
				colvarMemberUserName.IsReadOnly = false;
				colvarMemberUserName.DefaultSetting = @"";
				colvarMemberUserName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMemberUserName);

				TableSchema.TableColumn colvarBusinessHourGuid = new TableSchema.TableColumn(schema);
				colvarBusinessHourGuid.ColumnName = "business_hour_guid";
				colvarBusinessHourGuid.DataType = DbType.Guid;
				colvarBusinessHourGuid.MaxLength = 0;
				colvarBusinessHourGuid.AutoIncrement = false;
				colvarBusinessHourGuid.IsNullable = false;
				colvarBusinessHourGuid.IsPrimaryKey = false;
				colvarBusinessHourGuid.IsForeignKey = false;
				colvarBusinessHourGuid.IsReadOnly = false;
				colvarBusinessHourGuid.DefaultSetting = @"";
				colvarBusinessHourGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBusinessHourGuid);

				TableSchema.TableColumn colvarOrderGuid = new TableSchema.TableColumn(schema);
				colvarOrderGuid.ColumnName = "order_guid";
				colvarOrderGuid.DataType = DbType.Guid;
				colvarOrderGuid.MaxLength = 0;
				colvarOrderGuid.AutoIncrement = false;
				colvarOrderGuid.IsNullable = true;
				colvarOrderGuid.IsPrimaryKey = false;
				colvarOrderGuid.IsForeignKey = false;
				colvarOrderGuid.IsReadOnly = false;
				colvarOrderGuid.DefaultSetting = @"";
				colvarOrderGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderGuid);

				TableSchema.TableColumn colvarCloseTime = new TableSchema.TableColumn(schema);
				colvarCloseTime.ColumnName = "close_time";
				colvarCloseTime.DataType = DbType.DateTime;
				colvarCloseTime.MaxLength = 0;
				colvarCloseTime.AutoIncrement = false;
				colvarCloseTime.IsNullable = false;
				colvarCloseTime.IsPrimaryKey = false;
				colvarCloseTime.IsForeignKey = false;
				colvarCloseTime.IsReadOnly = false;
				colvarCloseTime.DefaultSetting = @"";
				colvarCloseTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCloseTime);

				TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
				colvarStatus.ColumnName = "status";
				colvarStatus.DataType = DbType.Int32;
				colvarStatus.MaxLength = 0;
				colvarStatus.AutoIncrement = false;
				colvarStatus.IsNullable = true;
				colvarStatus.IsPrimaryKey = false;
				colvarStatus.IsForeignKey = false;
				colvarStatus.IsReadOnly = false;
				colvarStatus.DefaultSetting = @"";
				colvarStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStatus);

				TableSchema.TableColumn colvarSlug = new TableSchema.TableColumn(schema);
				colvarSlug.ColumnName = "slug";
				colvarSlug.DataType = DbType.String;
				colvarSlug.MaxLength = 100;
				colvarSlug.AutoIncrement = false;
				colvarSlug.IsNullable = true;
				colvarSlug.IsPrimaryKey = false;
				colvarSlug.IsForeignKey = false;
				colvarSlug.IsReadOnly = false;
				colvarSlug.DefaultSetting = @"";
				colvarSlug.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSlug);

				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = false;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = false;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);

				TableSchema.TableColumn colvarName = new TableSchema.TableColumn(schema);
				colvarName.ColumnName = "name";
				colvarName.DataType = DbType.String;
				colvarName.MaxLength = 400;
				colvarName.AutoIncrement = false;
				colvarName.IsNullable = true;
				colvarName.IsPrimaryKey = false;
				colvarName.IsForeignKey = false;
				colvarName.IsReadOnly = false;
				colvarName.DefaultSetting = @"";
				colvarName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarName);

				TableSchema.TableColumn colvarImagePath = new TableSchema.TableColumn(schema);
				colvarImagePath.ColumnName = "image_path";
				colvarImagePath.DataType = DbType.String;
				colvarImagePath.MaxLength = 1000;
				colvarImagePath.AutoIncrement = false;
				colvarImagePath.IsNullable = true;
				colvarImagePath.IsPrimaryKey = false;
				colvarImagePath.IsForeignKey = false;
				colvarImagePath.IsReadOnly = false;
				colvarImagePath.DefaultSetting = @"";
				colvarImagePath.ForeignKeyTableName = "";
				schema.Columns.Add(colvarImagePath);

				TableSchema.TableColumn colvarIntroduction = new TableSchema.TableColumn(schema);
				colvarIntroduction.ColumnName = "introduction";
				colvarIntroduction.DataType = DbType.String;
				colvarIntroduction.MaxLength = 1073741823;
				colvarIntroduction.AutoIncrement = false;
				colvarIntroduction.IsNullable = true;
				colvarIntroduction.IsPrimaryKey = false;
				colvarIntroduction.IsForeignKey = false;
				colvarIntroduction.IsReadOnly = false;
				colvarIntroduction.DefaultSetting = @"";
				colvarIntroduction.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIntroduction);

				TableSchema.TableColumn colvarRestrictions = new TableSchema.TableColumn(schema);
				colvarRestrictions.ColumnName = "restrictions";
				colvarRestrictions.DataType = DbType.String;
				colvarRestrictions.MaxLength = 1073741823;
				colvarRestrictions.AutoIncrement = false;
				colvarRestrictions.IsNullable = true;
				colvarRestrictions.IsPrimaryKey = false;
				colvarRestrictions.IsForeignKey = false;
				colvarRestrictions.IsReadOnly = false;
				colvarRestrictions.DefaultSetting = @"";
				colvarRestrictions.ForeignKeyTableName = "";
				schema.Columns.Add(colvarRestrictions);

				TableSchema.TableColumn colvarDescription = new TableSchema.TableColumn(schema);
				colvarDescription.ColumnName = "description";
				colvarDescription.DataType = DbType.String;
				colvarDescription.MaxLength = 1073741823;
				colvarDescription.AutoIncrement = false;
				colvarDescription.IsNullable = true;
				colvarDescription.IsPrimaryKey = false;
				colvarDescription.IsForeignKey = false;
				colvarDescription.IsReadOnly = false;
				colvarDescription.DefaultSetting = @"";
				colvarDescription.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDescription);

				TableSchema.TableColumn colvarReferenceText = new TableSchema.TableColumn(schema);
				colvarReferenceText.ColumnName = "reference_text";
				colvarReferenceText.DataType = DbType.String;
				colvarReferenceText.MaxLength = 1073741823;
				colvarReferenceText.AutoIncrement = false;
				colvarReferenceText.IsNullable = true;
				colvarReferenceText.IsPrimaryKey = false;
				colvarReferenceText.IsForeignKey = false;
				colvarReferenceText.IsReadOnly = false;
				colvarReferenceText.DefaultSetting = @"";
				colvarReferenceText.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReferenceText);

				TableSchema.TableColumn colvarCouponUsage = new TableSchema.TableColumn(schema);
				colvarCouponUsage.ColumnName = "coupon_usage";
				colvarCouponUsage.DataType = DbType.String;
				colvarCouponUsage.MaxLength = 500;
				colvarCouponUsage.AutoIncrement = false;
				colvarCouponUsage.IsNullable = true;
				colvarCouponUsage.IsPrimaryKey = false;
				colvarCouponUsage.IsForeignKey = false;
				colvarCouponUsage.IsReadOnly = false;
				colvarCouponUsage.DefaultSetting = @"";
				colvarCouponUsage.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCouponUsage);

				TableSchema.TableColumn colvarRemark = new TableSchema.TableColumn(schema);
				colvarRemark.ColumnName = "remark";
				colvarRemark.DataType = DbType.String;
				colvarRemark.MaxLength = 1073741823;
				colvarRemark.AutoIncrement = false;
				colvarRemark.IsNullable = true;
				colvarRemark.IsPrimaryKey = false;
				colvarRemark.IsForeignKey = false;
				colvarRemark.IsReadOnly = false;
				colvarRemark.DefaultSetting = @"";
				colvarRemark.ForeignKeyTableName = "";
				schema.Columns.Add(colvarRemark);

				TableSchema.TableColumn colvarAvailability = new TableSchema.TableColumn(schema);
				colvarAvailability.ColumnName = "availability";
				colvarAvailability.DataType = DbType.String;
				colvarAvailability.MaxLength = 1073741823;
				colvarAvailability.AutoIncrement = false;
				colvarAvailability.IsNullable = true;
				colvarAvailability.IsPrimaryKey = false;
				colvarAvailability.IsForeignKey = false;
				colvarAvailability.IsReadOnly = false;
				colvarAvailability.DefaultSetting = @"";
				colvarAvailability.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAvailability);

				TableSchema.TableColumn colvarDepartment = new TableSchema.TableColumn(schema);
				colvarDepartment.ColumnName = "department";
				colvarDepartment.DataType = DbType.Int32;
				colvarDepartment.MaxLength = 0;
				colvarDepartment.AutoIncrement = false;
				colvarDepartment.IsNullable = false;
				colvarDepartment.IsPrimaryKey = false;
				colvarDepartment.IsForeignKey = false;
				colvarDepartment.IsReadOnly = false;
				colvarDepartment.DefaultSetting = @"";
				colvarDepartment.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDepartment);

				TableSchema.TableColumn colvarUserId = new TableSchema.TableColumn(schema);
				colvarUserId.ColumnName = "user_id";
				colvarUserId.DataType = DbType.Int32;
				colvarUserId.MaxLength = 0;
				colvarUserId.AutoIncrement = false;
				colvarUserId.IsNullable = false;
				colvarUserId.IsPrimaryKey = false;
				colvarUserId.IsForeignKey = false;
				colvarUserId.IsReadOnly = false;
				colvarUserId.DefaultSetting = @"";
				colvarUserId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUserId);

				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("view_ppon_order",schema);
			}
		}
		#endregion

		#region Props

		[XmlAttribute("Guid")]
		[Bindable(true)]
		public Guid Guid
		{
			get { return GetColumnValue<Guid>(Columns.Guid); }
			set { SetColumnValue(Columns.Guid, value); }
		}

		[XmlAttribute("OrderId")]
		[Bindable(true)]
		public string OrderId
		{
			get { return GetColumnValue<string>(Columns.OrderId); }
			set { SetColumnValue(Columns.OrderId, value); }
		}

		[XmlAttribute("SellerGuid")]
		[Bindable(true)]
		public Guid SellerGuid
		{
			get { return GetColumnValue<Guid>(Columns.SellerGuid); }
			set { SetColumnValue(Columns.SellerGuid, value); }
		}

		[XmlAttribute("SellerName")]
		[Bindable(true)]
		public string SellerName
		{
			get { return GetColumnValue<string>(Columns.SellerName); }
			set { SetColumnValue(Columns.SellerName, value); }
		}

		[XmlAttribute("MemberEmail")]
		[Bindable(true)]
		public string MemberEmail
		{
			get { return GetColumnValue<string>(Columns.MemberEmail); }
			set { SetColumnValue(Columns.MemberEmail, value); }
		}

		[XmlAttribute("MemberName")]
		[Bindable(true)]
		public string MemberName
		{
			get { return GetColumnValue<string>(Columns.MemberName); }
			set { SetColumnValue(Columns.MemberName, value); }
		}

		[XmlAttribute("PhoneNumber")]
		[Bindable(true)]
		public string PhoneNumber
		{
			get { return GetColumnValue<string>(Columns.PhoneNumber); }
			set { SetColumnValue(Columns.PhoneNumber, value); }
		}

		[XmlAttribute("MobileNumber")]
		[Bindable(true)]
		public string MobileNumber
		{
			get { return GetColumnValue<string>(Columns.MobileNumber); }
			set { SetColumnValue(Columns.MobileNumber, value); }
		}

		[XmlAttribute("DeliveryAddress")]
		[Bindable(true)]
		public string DeliveryAddress
		{
			get { return GetColumnValue<string>(Columns.DeliveryAddress); }
			set { SetColumnValue(Columns.DeliveryAddress, value); }
		}

		[XmlAttribute("DeliveryTime")]
		[Bindable(true)]
		public DateTime? DeliveryTime
		{
			get { return GetColumnValue<DateTime?>(Columns.DeliveryTime); }
			set { SetColumnValue(Columns.DeliveryTime, value); }
		}

		[XmlAttribute("Subtotal")]
		[Bindable(true)]
		public decimal Subtotal
		{
			get { return GetColumnValue<decimal>(Columns.Subtotal); }
			set { SetColumnValue(Columns.Subtotal, value); }
		}

		[XmlAttribute("Total")]
		[Bindable(true)]
		public decimal Total
		{
			get { return GetColumnValue<decimal>(Columns.Total); }
			set { SetColumnValue(Columns.Total, value); }
		}

		[XmlAttribute("UserMemo")]
		[Bindable(true)]
		public string UserMemo
		{
			get { return GetColumnValue<string>(Columns.UserMemo); }
			set { SetColumnValue(Columns.UserMemo, value); }
		}

		[XmlAttribute("OrderMemo")]
		[Bindable(true)]
		public string OrderMemo
		{
			get { return GetColumnValue<string>(Columns.OrderMemo); }
			set { SetColumnValue(Columns.OrderMemo, value); }
		}

		[XmlAttribute("OrderStatus")]
		[Bindable(true)]
		public int OrderStatus
		{
			get { return GetColumnValue<int>(Columns.OrderStatus); }
			set { SetColumnValue(Columns.OrderStatus, value); }
		}

		[XmlAttribute("OrderStage")]
		[Bindable(true)]
		public int OrderStage
		{
			get { return GetColumnValue<int>(Columns.OrderStage); }
			set { SetColumnValue(Columns.OrderStage, value); }
		}

		[XmlAttribute("BonusPoint")]
		[Bindable(true)]
		public string BonusPoint
		{
			get { return GetColumnValue<string>(Columns.BonusPoint); }
			set { SetColumnValue(Columns.BonusPoint, value); }
		}

		[XmlAttribute("CreateId")]
		[Bindable(true)]
		public string CreateId
		{
			get { return GetColumnValue<string>(Columns.CreateId); }
			set { SetColumnValue(Columns.CreateId, value); }
		}

		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}

		[XmlAttribute("ModifyId")]
		[Bindable(true)]
		public string ModifyId
		{
			get { return GetColumnValue<string>(Columns.ModifyId); }
			set { SetColumnValue(Columns.ModifyId, value); }
		}

		[XmlAttribute("ModifyTime")]
		[Bindable(true)]
		public DateTime? ModifyTime
		{
			get { return GetColumnValue<DateTime?>(Columns.ModifyTime); }
			set { SetColumnValue(Columns.ModifyTime, value); }
		}

		[XmlAttribute("AccessLock")]
		[Bindable(true)]
		public string AccessLock
		{
			get { return GetColumnValue<string>(Columns.AccessLock); }
			set { SetColumnValue(Columns.AccessLock, value); }
		}

		[XmlAttribute("CityId")]
		[Bindable(true)]
		public int? CityId
		{
			get { return GetColumnValue<int?>(Columns.CityId); }
			set { SetColumnValue(Columns.CityId, value); }
		}

		[XmlAttribute("ParentOrderId")]
		[Bindable(true)]
		public Guid? ParentOrderId
		{
			get { return GetColumnValue<Guid?>(Columns.ParentOrderId); }
			set { SetColumnValue(Columns.ParentOrderId, value); }
		}

		[XmlAttribute("GroupOrderGuid")]
		[Bindable(true)]
		public Guid GroupOrderGuid
		{
			get { return GetColumnValue<Guid>(Columns.GroupOrderGuid); }
			set { SetColumnValue(Columns.GroupOrderGuid, value); }
		}

		[XmlAttribute("MemberUserName")]
		[Bindable(true)]
		public string MemberUserName
		{
			get { return GetColumnValue<string>(Columns.MemberUserName); }
			set { SetColumnValue(Columns.MemberUserName, value); }
		}

		[XmlAttribute("BusinessHourGuid")]
		[Bindable(true)]
		public Guid BusinessHourGuid
		{
			get { return GetColumnValue<Guid>(Columns.BusinessHourGuid); }
			set { SetColumnValue(Columns.BusinessHourGuid, value); }
		}

		[XmlAttribute("OrderGuid")]
		[Bindable(true)]
		public Guid? OrderGuid
		{
			get { return GetColumnValue<Guid?>(Columns.OrderGuid); }
			set { SetColumnValue(Columns.OrderGuid, value); }
		}

		[XmlAttribute("CloseTime")]
		[Bindable(true)]
		public DateTime CloseTime
		{
			get { return GetColumnValue<DateTime>(Columns.CloseTime); }
			set { SetColumnValue(Columns.CloseTime, value); }
		}

		[XmlAttribute("Status")]
		[Bindable(true)]
		public int? Status
		{
			get { return GetColumnValue<int?>(Columns.Status); }
			set { SetColumnValue(Columns.Status, value); }
		}

		[XmlAttribute("Slug")]
		[Bindable(true)]
		public string Slug
		{
			get { return GetColumnValue<string>(Columns.Slug); }
			set { SetColumnValue(Columns.Slug, value); }
		}

		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}

		[XmlAttribute("Name")]
		[Bindable(true)]
		public string Name
		{
			get { return GetColumnValue<string>(Columns.Name); }
			set { SetColumnValue(Columns.Name, value); }
		}

		[XmlAttribute("ImagePath")]
		[Bindable(true)]
		public string ImagePath
		{
			get { return GetColumnValue<string>(Columns.ImagePath); }
			set { SetColumnValue(Columns.ImagePath, value); }
		}

		[XmlAttribute("Introduction")]
		[Bindable(true)]
		public string Introduction
		{
			get { return GetColumnValue<string>(Columns.Introduction); }
			set { SetColumnValue(Columns.Introduction, value); }
		}

		[XmlAttribute("Restrictions")]
		[Bindable(true)]
		public string Restrictions
		{
			get { return GetColumnValue<string>(Columns.Restrictions); }
			set { SetColumnValue(Columns.Restrictions, value); }
		}

		[XmlAttribute("Description")]
		[Bindable(true)]
		public string Description
		{
			get { return GetColumnValue<string>(Columns.Description); }
			set { SetColumnValue(Columns.Description, value); }
		}

		[XmlAttribute("ReferenceText")]
		[Bindable(true)]
		public string ReferenceText
		{
			get { return GetColumnValue<string>(Columns.ReferenceText); }
			set { SetColumnValue(Columns.ReferenceText, value); }
		}

		[XmlAttribute("CouponUsage")]
		[Bindable(true)]
		public string CouponUsage
		{
			get { return GetColumnValue<string>(Columns.CouponUsage); }
			set { SetColumnValue(Columns.CouponUsage, value); }
		}

		[XmlAttribute("Remark")]
		[Bindable(true)]
		public string Remark
		{
			get { return GetColumnValue<string>(Columns.Remark); }
			set { SetColumnValue(Columns.Remark, value); }
		}

		[XmlAttribute("Availability")]
		[Bindable(true)]
		public string Availability
		{
			get { return GetColumnValue<string>(Columns.Availability); }
			set { SetColumnValue(Columns.Availability, value); }
		}

		[XmlAttribute("Department")]
		[Bindable(true)]
		public int Department
		{
			get { return GetColumnValue<int>(Columns.Department); }
			set { SetColumnValue(Columns.Department, value); }
		}

		[XmlAttribute("UserId")]
		[Bindable(true)]
		public int UserId
		{
			get { return GetColumnValue<int>(Columns.UserId); }
			set { SetColumnValue(Columns.UserId, value); }
		}

		#endregion

		#region Typed Columns

		public static TableSchema.TableColumn GuidColumn
		{
			get { return Schema.Columns[0]; }
		}

		public static TableSchema.TableColumn OrderIdColumn
		{
			get { return Schema.Columns[1]; }
		}

		public static TableSchema.TableColumn SellerGuidColumn
		{
			get { return Schema.Columns[2]; }
		}

		public static TableSchema.TableColumn SellerNameColumn
		{
			get { return Schema.Columns[3]; }
		}

		public static TableSchema.TableColumn MemberEmailColumn
		{
			get { return Schema.Columns[4]; }
		}

		public static TableSchema.TableColumn MemberNameColumn
		{
			get { return Schema.Columns[5]; }
		}

		public static TableSchema.TableColumn PhoneNumberColumn
		{
			get { return Schema.Columns[6]; }
		}

		public static TableSchema.TableColumn MobileNumberColumn
		{
			get { return Schema.Columns[7]; }
		}

		public static TableSchema.TableColumn DeliveryAddressColumn
		{
			get { return Schema.Columns[8]; }
		}

		public static TableSchema.TableColumn DeliveryTimeColumn
		{
			get { return Schema.Columns[9]; }
		}

		public static TableSchema.TableColumn SubtotalColumn
		{
			get { return Schema.Columns[10]; }
		}

		public static TableSchema.TableColumn TotalColumn
		{
			get { return Schema.Columns[11]; }
		}

		public static TableSchema.TableColumn UserMemoColumn
		{
			get { return Schema.Columns[12]; }
		}

		public static TableSchema.TableColumn OrderMemoColumn
		{
			get { return Schema.Columns[13]; }
		}

		public static TableSchema.TableColumn OrderStatusColumn
		{
			get { return Schema.Columns[14]; }
		}

		public static TableSchema.TableColumn OrderStageColumn
		{
			get { return Schema.Columns[15]; }
		}

		public static TableSchema.TableColumn BonusPointColumn
		{
			get { return Schema.Columns[16]; }
		}

		public static TableSchema.TableColumn CreateIdColumn
		{
			get { return Schema.Columns[17]; }
		}

		public static TableSchema.TableColumn CreateTimeColumn
		{
			get { return Schema.Columns[18]; }
		}

		public static TableSchema.TableColumn ModifyIdColumn
		{
			get { return Schema.Columns[19]; }
		}

		public static TableSchema.TableColumn ModifyTimeColumn
		{
			get { return Schema.Columns[20]; }
		}

		public static TableSchema.TableColumn AccessLockColumn
		{
			get { return Schema.Columns[21]; }
		}

		public static TableSchema.TableColumn CityIdColumn
		{
			get { return Schema.Columns[22]; }
		}

		public static TableSchema.TableColumn ParentOrderIdColumn
		{
			get { return Schema.Columns[23]; }
		}

		public static TableSchema.TableColumn GroupOrderGuidColumn
		{
			get { return Schema.Columns[24]; }
		}

		public static TableSchema.TableColumn MemberUserNameColumn
		{
			get { return Schema.Columns[25]; }
		}

		public static TableSchema.TableColumn BusinessHourGuidColumn
		{
			get { return Schema.Columns[26]; }
		}

		public static TableSchema.TableColumn OrderGuidColumn
		{
			get { return Schema.Columns[27]; }
		}

		public static TableSchema.TableColumn CloseTimeColumn
		{
			get { return Schema.Columns[28]; }
		}

		public static TableSchema.TableColumn StatusColumn
		{
			get { return Schema.Columns[29]; }
		}

		public static TableSchema.TableColumn SlugColumn
		{
			get { return Schema.Columns[30]; }
		}

		public static TableSchema.TableColumn IdColumn
		{
			get { return Schema.Columns[31]; }
		}

		public static TableSchema.TableColumn NameColumn
		{
			get { return Schema.Columns[32]; }
		}

		public static TableSchema.TableColumn ImagePathColumn
		{
			get { return Schema.Columns[33]; }
		}

		public static TableSchema.TableColumn IntroductionColumn
		{
			get { return Schema.Columns[34]; }
		}

		public static TableSchema.TableColumn RestrictionsColumn
		{
			get { return Schema.Columns[35]; }
		}

		public static TableSchema.TableColumn DescriptionColumn
		{
			get { return Schema.Columns[36]; }
		}

		public static TableSchema.TableColumn ReferenceTextColumn
		{
			get { return Schema.Columns[37]; }
		}

		public static TableSchema.TableColumn CouponUsageColumn
		{
			get { return Schema.Columns[38]; }
		}

		public static TableSchema.TableColumn RemarkColumn
		{
			get { return Schema.Columns[39]; }
		}

		public static TableSchema.TableColumn AvailabilityColumn
		{
			get { return Schema.Columns[40]; }
		}

		public static TableSchema.TableColumn DepartmentColumn
		{
			get { return Schema.Columns[41]; }
		}

		public static TableSchema.TableColumn UserIdColumn
		{
			get { return Schema.Columns[42]; }
		}

		#endregion

		#region Columns Struct

		public struct Columns
		{
			public static string Guid = @"GUID";
			public static string OrderId = @"order_id";
			public static string SellerGuid = @"seller_GUID";
			public static string SellerName = @"seller_name";
			public static string MemberEmail = @"member_email";
			public static string MemberName = @"member_name";
			public static string PhoneNumber = @"phone_number";
			public static string MobileNumber = @"mobile_number";
			public static string DeliveryAddress = @"delivery_address";
			public static string DeliveryTime = @"delivery_time";
			public static string Subtotal = @"subtotal";
			public static string Total = @"total";
			public static string UserMemo = @"user_memo";
			public static string OrderMemo = @"order_memo";
			public static string OrderStatus = @"order_status";
			public static string OrderStage = @"order_stage";
			public static string BonusPoint = @"bonus_point";
			public static string CreateId = @"create_id";
			public static string CreateTime = @"create_time";
			public static string ModifyId = @"modify_id";
			public static string ModifyTime = @"modify_time";
			public static string AccessLock = @"access_lock";
			public static string CityId = @"city_id";
			public static string ParentOrderId = @"parent_order_id";
			public static string GroupOrderGuid = @"group_order_guid";
			public static string MemberUserName = @"member_user_name";
			public static string BusinessHourGuid = @"business_hour_guid";
			public static string OrderGuid = @"order_guid";
			public static string CloseTime = @"close_time";
			public static string Status = @"status";
			public static string Slug = @"slug";
			public static string Id = @"id";
			public static string Name = @"name";
			public static string ImagePath = @"image_path";
			public static string Introduction = @"introduction";
			public static string Restrictions = @"restrictions";
			public static string Description = @"description";
			public static string ReferenceText = @"reference_text";
			public static string CouponUsage = @"coupon_usage";
			public static string Remark = @"remark";
			public static string Availability = @"availability";
			public static string Department = @"department";
			public static string UserId = @"user_id";
		}

		#endregion

		#region IAbstractRecord Members
		public new CT GetColumnValue<CT>(string columnName)
		{
			return base.GetColumnValue<CT>(columnName);
		}
		public object GetColumnValue(string columnName)
		{
			return base.GetColumnValue<object>(columnName);
		}
		#endregion

	}
}

﻿using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the ProposalPerformanceLog class.
    /// </summary>
    [Serializable]
    public partial class ProposalPerformanceLogCollection : RepositoryList<ProposalPerformanceLog, ProposalPerformanceLogCollection>
    {
        public ProposalPerformanceLogCollection() { }

        /// <summary>
        /// Filters an existing collection based on the set criteria. This is an in-memory filter
        /// Thanks to developingchris for this!
        /// </summary>
        /// <returns>ProposalPerformanceLogCollection</returns>
        public ProposalPerformanceLogCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                ProposalPerformanceLog o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }


    }
    /// <summary>
    /// This is an ActiveRecord class which wraps the proposal_performance_log table.
    /// </summary>
    [Serializable]
    public partial class ProposalPerformanceLog : RepositoryRecord<ProposalPerformanceLog>, IRecordBase
    {
        #region .ctors and Default Settings

        public ProposalPerformanceLog()
        {
            SetSQLProps();
            InitSetDefaults();
            MarkNew();
        }

        private void InitSetDefaults() { SetDefaults(); }

        public ProposalPerformanceLog(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
                ForceDefaults();
            MarkNew();
        }

        protected static void SetSQLProps() { GetTableSchema(); }

        #endregion

        #region Schema and Query Accessor	
        public static Query CreateQuery() { return new Query(Schema); }
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                    SetSQLProps();
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("proposal_performance_log", TableType.Table, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = true;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = true;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                colvarId.DefaultSetting = @"";
                colvarId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarId);

                TableSchema.TableColumn colvarLink = new TableSchema.TableColumn(schema);
                colvarLink.ColumnName = "link";
                colvarLink.DataType = DbType.AnsiString;
                colvarLink.MaxLength = 200;
                colvarLink.AutoIncrement = false;
                colvarLink.IsNullable = true;
                colvarLink.IsPrimaryKey = false;
                colvarLink.IsForeignKey = false;
                colvarLink.IsReadOnly = false;
                colvarLink.DefaultSetting = @"";
                colvarLink.ForeignKeyTableName = "";
                schema.Columns.Add(colvarLink);

                TableSchema.TableColumn colvarContentLog = new TableSchema.TableColumn(schema);
                colvarContentLog.ColumnName = "content_log";
                colvarContentLog.DataType = DbType.String;
                colvarContentLog.MaxLength = -1;
                colvarContentLog.AutoIncrement = false;
                colvarContentLog.IsNullable = true;
                colvarContentLog.IsPrimaryKey = false;
                colvarContentLog.IsForeignKey = false;
                colvarContentLog.IsReadOnly = false;
                colvarContentLog.DefaultSetting = @"";
                colvarContentLog.ForeignKeyTableName = "";
                schema.Columns.Add(colvarContentLog);

                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = true;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;

                colvarCreateTime.DefaultSetting = @"(getdate())";
                colvarCreateTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateTime);

                TableSchema.TableColumn colvarCreateUser = new TableSchema.TableColumn(schema);
                colvarCreateUser.ColumnName = "create_user";
                colvarCreateUser.DataType = DbType.AnsiString;
                colvarCreateUser.MaxLength = 100;
                colvarCreateUser.AutoIncrement = false;
                colvarCreateUser.IsNullable = true;
                colvarCreateUser.IsPrimaryKey = false;
                colvarCreateUser.IsForeignKey = false;
                colvarCreateUser.IsReadOnly = false;
                colvarCreateUser.DefaultSetting = @"";
                colvarCreateUser.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateUser);

                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("proposal_performance_log", schema);
            }
        }
        #endregion

        #region Props

        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id
        {
            get { return GetColumnValue<int>(Columns.Id); }
            set { SetColumnValue(Columns.Id, value); }
        }

        [XmlAttribute("Link")]
        [Bindable(true)]
        public string Link
        {
            get { return GetColumnValue<string>(Columns.Link); }
            set { SetColumnValue(Columns.Link, value); }
        }

        [XmlAttribute("ContentLog")]
        [Bindable(true)]
        public string ContentLog
        {
            get { return GetColumnValue<string>(Columns.ContentLog); }
            set { SetColumnValue(Columns.ContentLog, value); }
        }

        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime? CreateTime
        {
            get { return GetColumnValue<DateTime?>(Columns.CreateTime); }
            set { SetColumnValue(Columns.CreateTime, value); }
        }

        [XmlAttribute("CreateUser")]
        [Bindable(true)]
        public string CreateUser
        {
            get { return GetColumnValue<string>(Columns.CreateUser); }
            set { SetColumnValue(Columns.CreateUser, value); }
        }

        #endregion




        //no foreign key tables defined (0)



        //no ManyToMany tables defined (0)





        #region Typed Columns


        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }



        public static TableSchema.TableColumn LinkColumn
        {
            get { return Schema.Columns[1]; }
        }



        public static TableSchema.TableColumn ContentLogColumn
        {
            get { return Schema.Columns[2]; }
        }



        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[3]; }
        }



        public static TableSchema.TableColumn CreateUserColumn
        {
            get { return Schema.Columns[4]; }
        }



        #endregion
        #region Columns Struct
        public struct Columns
        {
            public static string Id = @"id";
            public static string Link = @"link";
            public static string ContentLog = @"content_log";
            public static string CreateTime = @"create_time";
            public static string CreateUser = @"create_user";

        }
        #endregion

        #region Update PK Collections

        #endregion

        #region Deep Save

        #endregion
    }
}

using System;
using System.ComponentModel;
using System.Data;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the ViewOrderDetailItem class.
    /// </summary>
    [Serializable]
    public partial class ViewOrderDetailItemCollection : ReadOnlyList<ViewOrderDetailItem, ViewOrderDetailItemCollection>
    {        
        public ViewOrderDetailItemCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_order_detail_item view.
    /// </summary>
    [Serializable]
    public partial class ViewOrderDetailItem : ReadOnlyRecord<ViewOrderDetailItem>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_order_detail_item", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarItemGuid = new TableSchema.TableColumn(schema);
                colvarItemGuid.ColumnName = "item_guid";
                colvarItemGuid.DataType = DbType.Guid;
                colvarItemGuid.MaxLength = 0;
                colvarItemGuid.AutoIncrement = false;
                colvarItemGuid.IsNullable = true;
                colvarItemGuid.IsPrimaryKey = false;
                colvarItemGuid.IsForeignKey = false;
                colvarItemGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemGuid);
                
                TableSchema.TableColumn colvarProductId = new TableSchema.TableColumn(schema);
                colvarProductId.ColumnName = "product_id";
                colvarProductId.DataType = DbType.String;
                colvarProductId.MaxLength = 50;
                colvarProductId.AutoIncrement = false;
                colvarProductId.IsNullable = true;
                colvarProductId.IsPrimaryKey = false;
                colvarProductId.IsForeignKey = false;
                colvarProductId.IsReadOnly = false;
                
                schema.Columns.Add(colvarProductId);
                
                TableSchema.TableColumn colvarMenuGuid = new TableSchema.TableColumn(schema);
                colvarMenuGuid.ColumnName = "menu_GUID";
                colvarMenuGuid.DataType = DbType.Guid;
                colvarMenuGuid.MaxLength = 0;
                colvarMenuGuid.AutoIncrement = false;
                colvarMenuGuid.IsNullable = true;
                colvarMenuGuid.IsPrimaryKey = false;
                colvarMenuGuid.IsForeignKey = false;
                colvarMenuGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarMenuGuid);
                
                TableSchema.TableColumn colvarItemId = new TableSchema.TableColumn(schema);
                colvarItemId.ColumnName = "item_id";
                colvarItemId.DataType = DbType.AnsiString;
                colvarItemId.MaxLength = 20;
                colvarItemId.AutoIncrement = false;
                colvarItemId.IsNullable = true;
                colvarItemId.IsPrimaryKey = false;
                colvarItemId.IsForeignKey = false;
                colvarItemId.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemId);
                
                TableSchema.TableColumn colvarItemName = new TableSchema.TableColumn(schema);
                colvarItemName.ColumnName = "item_name";
                colvarItemName.DataType = DbType.String;
                colvarItemName.MaxLength = 100;
                colvarItemName.AutoIncrement = false;
                colvarItemName.IsNullable = true;
                colvarItemName.IsPrimaryKey = false;
                colvarItemName.IsForeignKey = false;
                colvarItemName.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemName);
                
                TableSchema.TableColumn colvarItemOrigPrice = new TableSchema.TableColumn(schema);
                colvarItemOrigPrice.ColumnName = "item_orig_price";
                colvarItemOrigPrice.DataType = DbType.Currency;
                colvarItemOrigPrice.MaxLength = 0;
                colvarItemOrigPrice.AutoIncrement = false;
                colvarItemOrigPrice.IsNullable = true;
                colvarItemOrigPrice.IsPrimaryKey = false;
                colvarItemOrigPrice.IsForeignKey = false;
                colvarItemOrigPrice.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemOrigPrice);
                
                TableSchema.TableColumn colvarItemPrice = new TableSchema.TableColumn(schema);
                colvarItemPrice.ColumnName = "item_price";
                colvarItemPrice.DataType = DbType.Currency;
                colvarItemPrice.MaxLength = 0;
                colvarItemPrice.AutoIncrement = false;
                colvarItemPrice.IsNullable = true;
                colvarItemPrice.IsPrimaryKey = false;
                colvarItemPrice.IsForeignKey = false;
                colvarItemPrice.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemPrice);
                
                TableSchema.TableColumn colvarItemDescription = new TableSchema.TableColumn(schema);
                colvarItemDescription.ColumnName = "item_description";
                colvarItemDescription.DataType = DbType.String;
                colvarItemDescription.MaxLength = 1073741823;
                colvarItemDescription.AutoIncrement = false;
                colvarItemDescription.IsNullable = true;
                colvarItemDescription.IsPrimaryKey = false;
                colvarItemDescription.IsForeignKey = false;
                colvarItemDescription.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemDescription);
                
                TableSchema.TableColumn colvarItemDefaultDailyAmount = new TableSchema.TableColumn(schema);
                colvarItemDefaultDailyAmount.ColumnName = "item_default_daily_amount";
                colvarItemDefaultDailyAmount.DataType = DbType.Int32;
                colvarItemDefaultDailyAmount.MaxLength = 0;
                colvarItemDefaultDailyAmount.AutoIncrement = false;
                colvarItemDefaultDailyAmount.IsNullable = true;
                colvarItemDefaultDailyAmount.IsPrimaryKey = false;
                colvarItemDefaultDailyAmount.IsForeignKey = false;
                colvarItemDefaultDailyAmount.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemDefaultDailyAmount);
                
                TableSchema.TableColumn colvarItemImgPath = new TableSchema.TableColumn(schema);
                colvarItemImgPath.ColumnName = "item_img_path";
                colvarItemImgPath.DataType = DbType.String;
                colvarItemImgPath.MaxLength = 500;
                colvarItemImgPath.AutoIncrement = false;
                colvarItemImgPath.IsNullable = true;
                colvarItemImgPath.IsPrimaryKey = false;
                colvarItemImgPath.IsForeignKey = false;
                colvarItemImgPath.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemImgPath);
                
                TableSchema.TableColumn colvarItemUrl = new TableSchema.TableColumn(schema);
                colvarItemUrl.ColumnName = "item_url";
                colvarItemUrl.DataType = DbType.String;
                colvarItemUrl.MaxLength = 500;
                colvarItemUrl.AutoIncrement = false;
                colvarItemUrl.IsNullable = true;
                colvarItemUrl.IsPrimaryKey = false;
                colvarItemUrl.IsForeignKey = false;
                colvarItemUrl.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemUrl);
                
                TableSchema.TableColumn colvarItemSequence = new TableSchema.TableColumn(schema);
                colvarItemSequence.ColumnName = "item_sequence";
                colvarItemSequence.DataType = DbType.Int32;
                colvarItemSequence.MaxLength = 0;
                colvarItemSequence.AutoIncrement = false;
                colvarItemSequence.IsNullable = true;
                colvarItemSequence.IsPrimaryKey = false;
                colvarItemSequence.IsForeignKey = false;
                colvarItemSequence.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemSequence);
                
                TableSchema.TableColumn colvarItemStatus = new TableSchema.TableColumn(schema);
                colvarItemStatus.ColumnName = "item_status";
                colvarItemStatus.DataType = DbType.Int32;
                colvarItemStatus.MaxLength = 0;
                colvarItemStatus.AutoIncrement = false;
                colvarItemStatus.IsNullable = true;
                colvarItemStatus.IsPrimaryKey = false;
                colvarItemStatus.IsForeignKey = false;
                colvarItemStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemStatus);
                
                TableSchema.TableColumn colvarOrderDetailGuid = new TableSchema.TableColumn(schema);
                colvarOrderDetailGuid.ColumnName = "order_detail_guid";
                colvarOrderDetailGuid.DataType = DbType.Guid;
                colvarOrderDetailGuid.MaxLength = 0;
                colvarOrderDetailGuid.AutoIncrement = false;
                colvarOrderDetailGuid.IsNullable = false;
                colvarOrderDetailGuid.IsPrimaryKey = false;
                colvarOrderDetailGuid.IsForeignKey = false;
                colvarOrderDetailGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderDetailGuid);
                
                TableSchema.TableColumn colvarOrderGuid = new TableSchema.TableColumn(schema);
                colvarOrderGuid.ColumnName = "order_GUID";
                colvarOrderGuid.DataType = DbType.Guid;
                colvarOrderGuid.MaxLength = 0;
                colvarOrderGuid.AutoIncrement = false;
                colvarOrderGuid.IsNullable = false;
                colvarOrderGuid.IsPrimaryKey = false;
                colvarOrderGuid.IsForeignKey = false;
                colvarOrderGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderGuid);
                
                TableSchema.TableColumn colvarOrderDetailItemName = new TableSchema.TableColumn(schema);
                colvarOrderDetailItemName.ColumnName = "order_detail_item_name";
                colvarOrderDetailItemName.DataType = DbType.String;
                colvarOrderDetailItemName.MaxLength = 300;
                colvarOrderDetailItemName.AutoIncrement = false;
                colvarOrderDetailItemName.IsNullable = false;
                colvarOrderDetailItemName.IsPrimaryKey = false;
                colvarOrderDetailItemName.IsForeignKey = false;
                colvarOrderDetailItemName.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderDetailItemName);
                
                TableSchema.TableColumn colvarOrderDetailUnitPrice = new TableSchema.TableColumn(schema);
                colvarOrderDetailUnitPrice.ColumnName = "order_detail_unit_price";
                colvarOrderDetailUnitPrice.DataType = DbType.Currency;
                colvarOrderDetailUnitPrice.MaxLength = 0;
                colvarOrderDetailUnitPrice.AutoIncrement = false;
                colvarOrderDetailUnitPrice.IsNullable = false;
                colvarOrderDetailUnitPrice.IsPrimaryKey = false;
                colvarOrderDetailUnitPrice.IsForeignKey = false;
                colvarOrderDetailUnitPrice.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderDetailUnitPrice);
                
                TableSchema.TableColumn colvarOrderDetailQuantity = new TableSchema.TableColumn(schema);
                colvarOrderDetailQuantity.ColumnName = "order_detail_quantity";
                colvarOrderDetailQuantity.DataType = DbType.Int32;
                colvarOrderDetailQuantity.MaxLength = 0;
                colvarOrderDetailQuantity.AutoIncrement = false;
                colvarOrderDetailQuantity.IsNullable = false;
                colvarOrderDetailQuantity.IsPrimaryKey = false;
                colvarOrderDetailQuantity.IsForeignKey = false;
                colvarOrderDetailQuantity.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderDetailQuantity);
                
                TableSchema.TableColumn colvarOrderDetailTotal = new TableSchema.TableColumn(schema);
                colvarOrderDetailTotal.ColumnName = "order_detail_total";
                colvarOrderDetailTotal.DataType = DbType.Currency;
                colvarOrderDetailTotal.MaxLength = 0;
                colvarOrderDetailTotal.AutoIncrement = false;
                colvarOrderDetailTotal.IsNullable = true;
                colvarOrderDetailTotal.IsPrimaryKey = false;
                colvarOrderDetailTotal.IsForeignKey = false;
                colvarOrderDetailTotal.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderDetailTotal);
                
                TableSchema.TableColumn colvarConsumerName = new TableSchema.TableColumn(schema);
                colvarConsumerName.ColumnName = "consumer_name";
                colvarConsumerName.DataType = DbType.String;
                colvarConsumerName.MaxLength = 100;
                colvarConsumerName.AutoIncrement = false;
                colvarConsumerName.IsNullable = true;
                colvarConsumerName.IsPrimaryKey = false;
                colvarConsumerName.IsForeignKey = false;
                colvarConsumerName.IsReadOnly = false;
                
                schema.Columns.Add(colvarConsumerName);
                
                TableSchema.TableColumn colvarConsumerTeleExt = new TableSchema.TableColumn(schema);
                colvarConsumerTeleExt.ColumnName = "consumer_tele_ext";
                colvarConsumerTeleExt.DataType = DbType.AnsiString;
                colvarConsumerTeleExt.MaxLength = 10;
                colvarConsumerTeleExt.AutoIncrement = false;
                colvarConsumerTeleExt.IsNullable = true;
                colvarConsumerTeleExt.IsPrimaryKey = false;
                colvarConsumerTeleExt.IsForeignKey = false;
                colvarConsumerTeleExt.IsReadOnly = false;
                
                schema.Columns.Add(colvarConsumerTeleExt);
                
                TableSchema.TableColumn colvarOrderDetailStatus = new TableSchema.TableColumn(schema);
                colvarOrderDetailStatus.ColumnName = "order_detail_status";
                colvarOrderDetailStatus.DataType = DbType.Int32;
                colvarOrderDetailStatus.MaxLength = 0;
                colvarOrderDetailStatus.AutoIncrement = false;
                colvarOrderDetailStatus.IsNullable = false;
                colvarOrderDetailStatus.IsPrimaryKey = false;
                colvarOrderDetailStatus.IsForeignKey = false;
                colvarOrderDetailStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderDetailStatus);
                
                TableSchema.TableColumn colvarConsumerGroup = new TableSchema.TableColumn(schema);
                colvarConsumerGroup.ColumnName = "consumer_group";
                colvarConsumerGroup.DataType = DbType.String;
                colvarConsumerGroup.MaxLength = 50;
                colvarConsumerGroup.AutoIncrement = false;
                colvarConsumerGroup.IsNullable = true;
                colvarConsumerGroup.IsPrimaryKey = false;
                colvarConsumerGroup.IsForeignKey = false;
                colvarConsumerGroup.IsReadOnly = false;
                
                schema.Columns.Add(colvarConsumerGroup);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_order_detail_item",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewOrderDetailItem()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewOrderDetailItem(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewOrderDetailItem(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewOrderDetailItem(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("ItemGuid")]
        [Bindable(true)]
        public Guid? ItemGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid?>("item_guid");
		    }
            set 
		    {
			    SetColumnValue("item_guid", value);
            }
        }
	      
        [XmlAttribute("ProductId")]
        [Bindable(true)]
        public string ProductId 
	    {
		    get
		    {
			    return GetColumnValue<string>("product_id");
		    }
            set 
		    {
			    SetColumnValue("product_id", value);
            }
        }
	      
        [XmlAttribute("MenuGuid")]
        [Bindable(true)]
        public Guid? MenuGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid?>("menu_GUID");
		    }
            set 
		    {
			    SetColumnValue("menu_GUID", value);
            }
        }
	      
        [XmlAttribute("ItemId")]
        [Bindable(true)]
        public string ItemId 
	    {
		    get
		    {
			    return GetColumnValue<string>("item_id");
		    }
            set 
		    {
			    SetColumnValue("item_id", value);
            }
        }
	      
        [XmlAttribute("ItemName")]
        [Bindable(true)]
        public string ItemName 
	    {
		    get
		    {
			    return GetColumnValue<string>("item_name");
		    }
            set 
		    {
			    SetColumnValue("item_name", value);
            }
        }
	      
        [XmlAttribute("ItemOrigPrice")]
        [Bindable(true)]
        public decimal? ItemOrigPrice 
	    {
		    get
		    {
			    return GetColumnValue<decimal?>("item_orig_price");
		    }
            set 
		    {
			    SetColumnValue("item_orig_price", value);
            }
        }
	      
        [XmlAttribute("ItemPrice")]
        [Bindable(true)]
        public decimal? ItemPrice 
	    {
		    get
		    {
			    return GetColumnValue<decimal?>("item_price");
		    }
            set 
		    {
			    SetColumnValue("item_price", value);
            }
        }
	      
        [XmlAttribute("ItemDescription")]
        [Bindable(true)]
        public string ItemDescription 
	    {
		    get
		    {
			    return GetColumnValue<string>("item_description");
		    }
            set 
		    {
			    SetColumnValue("item_description", value);
            }
        }
	      
        [XmlAttribute("ItemDefaultDailyAmount")]
        [Bindable(true)]
        public int? ItemDefaultDailyAmount 
	    {
		    get
		    {
			    return GetColumnValue<int?>("item_default_daily_amount");
		    }
            set 
		    {
			    SetColumnValue("item_default_daily_amount", value);
            }
        }
	      
        [XmlAttribute("ItemImgPath")]
        [Bindable(true)]
        public string ItemImgPath 
	    {
		    get
		    {
			    return GetColumnValue<string>("item_img_path");
		    }
            set 
		    {
			    SetColumnValue("item_img_path", value);
            }
        }
	      
        [XmlAttribute("ItemUrl")]
        [Bindable(true)]
        public string ItemUrl 
	    {
		    get
		    {
			    return GetColumnValue<string>("item_url");
		    }
            set 
		    {
			    SetColumnValue("item_url", value);
            }
        }
	      
        [XmlAttribute("ItemSequence")]
        [Bindable(true)]
        public int? ItemSequence 
	    {
		    get
		    {
			    return GetColumnValue<int?>("item_sequence");
		    }
            set 
		    {
			    SetColumnValue("item_sequence", value);
            }
        }
	      
        [XmlAttribute("ItemStatus")]
        [Bindable(true)]
        public int? ItemStatus 
	    {
		    get
		    {
			    return GetColumnValue<int?>("item_status");
		    }
            set 
		    {
			    SetColumnValue("item_status", value);
            }
        }
	      
        [XmlAttribute("OrderDetailGuid")]
        [Bindable(true)]
        public Guid OrderDetailGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("order_detail_guid");
		    }
            set 
		    {
			    SetColumnValue("order_detail_guid", value);
            }
        }
	      
        [XmlAttribute("OrderGuid")]
        [Bindable(true)]
        public Guid OrderGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("order_GUID");
		    }
            set 
		    {
			    SetColumnValue("order_GUID", value);
            }
        }
	      
        [XmlAttribute("OrderDetailItemName")]
        [Bindable(true)]
        public string OrderDetailItemName 
	    {
		    get
		    {
			    return GetColumnValue<string>("order_detail_item_name");
		    }
            set 
		    {
			    SetColumnValue("order_detail_item_name", value);
            }
        }
	      
        [XmlAttribute("OrderDetailUnitPrice")]
        [Bindable(true)]
        public decimal OrderDetailUnitPrice 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("order_detail_unit_price");
		    }
            set 
		    {
			    SetColumnValue("order_detail_unit_price", value);
            }
        }
	      
        [XmlAttribute("OrderDetailQuantity")]
        [Bindable(true)]
        public int OrderDetailQuantity 
	    {
		    get
		    {
			    return GetColumnValue<int>("order_detail_quantity");
		    }
            set 
		    {
			    SetColumnValue("order_detail_quantity", value);
            }
        }
	      
        [XmlAttribute("OrderDetailTotal")]
        [Bindable(true)]
        public decimal? OrderDetailTotal 
	    {
		    get
		    {
			    return GetColumnValue<decimal?>("order_detail_total");
		    }
            set 
		    {
			    SetColumnValue("order_detail_total", value);
            }
        }
	      
        [XmlAttribute("ConsumerName")]
        [Bindable(true)]
        public string ConsumerName 
	    {
		    get
		    {
			    return GetColumnValue<string>("consumer_name");
		    }
            set 
		    {
			    SetColumnValue("consumer_name", value);
            }
        }
	      
        [XmlAttribute("ConsumerTeleExt")]
        [Bindable(true)]
        public string ConsumerTeleExt 
	    {
		    get
		    {
			    return GetColumnValue<string>("consumer_tele_ext");
		    }
            set 
		    {
			    SetColumnValue("consumer_tele_ext", value);
            }
        }
	      
        [XmlAttribute("OrderDetailStatus")]
        [Bindable(true)]
        public int OrderDetailStatus 
	    {
		    get
		    {
			    return GetColumnValue<int>("order_detail_status");
		    }
            set 
		    {
			    SetColumnValue("order_detail_status", value);
            }
        }
	      
        [XmlAttribute("ConsumerGroup")]
        [Bindable(true)]
        public string ConsumerGroup 
	    {
		    get
		    {
			    return GetColumnValue<string>("consumer_group");
		    }
            set 
		    {
			    SetColumnValue("consumer_group", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string ItemGuid = @"item_guid";
            
            public static string ProductId = @"product_id";
            
            public static string MenuGuid = @"menu_GUID";
            
            public static string ItemId = @"item_id";
            
            public static string ItemName = @"item_name";
            
            public static string ItemOrigPrice = @"item_orig_price";
            
            public static string ItemPrice = @"item_price";
            
            public static string ItemDescription = @"item_description";
            
            public static string ItemDefaultDailyAmount = @"item_default_daily_amount";
            
            public static string ItemImgPath = @"item_img_path";
            
            public static string ItemUrl = @"item_url";
            
            public static string ItemSequence = @"item_sequence";
            
            public static string ItemStatus = @"item_status";
            
            public static string OrderDetailGuid = @"order_detail_guid";
            
            public static string OrderGuid = @"order_GUID";
            
            public static string OrderDetailItemName = @"order_detail_item_name";
            
            public static string OrderDetailUnitPrice = @"order_detail_unit_price";
            
            public static string OrderDetailQuantity = @"order_detail_quantity";
            
            public static string OrderDetailTotal = @"order_detail_total";
            
            public static string ConsumerName = @"consumer_name";
            
            public static string ConsumerTeleExt = @"consumer_tele_ext";
            
            public static string OrderDetailStatus = @"order_detail_status";
            
            public static string ConsumerGroup = @"consumer_group";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewCouponOnlyListMain class.
    /// </summary>
    [Serializable]
    public partial class ViewCouponOnlyListMainCollection : ReadOnlyList<ViewCouponOnlyListMain, ViewCouponOnlyListMainCollection>
    {        
        public ViewCouponOnlyListMainCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_coupon_only_list_main view.
    /// </summary>
    [Serializable]
    public partial class ViewCouponOnlyListMain : ReadOnlyRecord<ViewCouponOnlyListMain>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_coupon_only_list_main", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = false;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarCreateTime);
                
                TableSchema.TableColumn colvarOrderId = new TableSchema.TableColumn(schema);
                colvarOrderId.ColumnName = "order_id";
                colvarOrderId.DataType = DbType.AnsiString;
                colvarOrderId.MaxLength = 30;
                colvarOrderId.AutoIncrement = false;
                colvarOrderId.IsNullable = true;
                colvarOrderId.IsPrimaryKey = false;
                colvarOrderId.IsForeignKey = false;
                colvarOrderId.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderId);
                
                TableSchema.TableColumn colvarGuid = new TableSchema.TableColumn(schema);
                colvarGuid.ColumnName = "GUID";
                colvarGuid.DataType = DbType.Guid;
                colvarGuid.MaxLength = 0;
                colvarGuid.AutoIncrement = false;
                colvarGuid.IsNullable = false;
                colvarGuid.IsPrimaryKey = false;
                colvarGuid.IsForeignKey = false;
                colvarGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarGuid);
                
                TableSchema.TableColumn colvarOrderStatus = new TableSchema.TableColumn(schema);
                colvarOrderStatus.ColumnName = "order_status";
                colvarOrderStatus.DataType = DbType.Int32;
                colvarOrderStatus.MaxLength = 0;
                colvarOrderStatus.AutoIncrement = false;
                colvarOrderStatus.IsNullable = false;
                colvarOrderStatus.IsPrimaryKey = false;
                colvarOrderStatus.IsForeignKey = false;
                colvarOrderStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderStatus);
                
                TableSchema.TableColumn colvarMemberEmail = new TableSchema.TableColumn(schema);
                colvarMemberEmail.ColumnName = "member_email";
                colvarMemberEmail.DataType = DbType.String;
                colvarMemberEmail.MaxLength = 256;
                colvarMemberEmail.AutoIncrement = false;
                colvarMemberEmail.IsNullable = false;
                colvarMemberEmail.IsPrimaryKey = false;
                colvarMemberEmail.IsForeignKey = false;
                colvarMemberEmail.IsReadOnly = false;
                
                schema.Columns.Add(colvarMemberEmail);
                
                TableSchema.TableColumn colvarUniqueId = new TableSchema.TableColumn(schema);
                colvarUniqueId.ColumnName = "unique_id";
                colvarUniqueId.DataType = DbType.Int32;
                colvarUniqueId.MaxLength = 0;
                colvarUniqueId.AutoIncrement = false;
                colvarUniqueId.IsNullable = false;
                colvarUniqueId.IsPrimaryKey = false;
                colvarUniqueId.IsForeignKey = false;
                colvarUniqueId.IsReadOnly = false;
                
                schema.Columns.Add(colvarUniqueId);
                
                TableSchema.TableColumn colvarSubtotal = new TableSchema.TableColumn(schema);
                colvarSubtotal.ColumnName = "subtotal";
                colvarSubtotal.DataType = DbType.Currency;
                colvarSubtotal.MaxLength = 0;
                colvarSubtotal.AutoIncrement = false;
                colvarSubtotal.IsNullable = false;
                colvarSubtotal.IsPrimaryKey = false;
                colvarSubtotal.IsForeignKey = false;
                colvarSubtotal.IsReadOnly = false;
                
                schema.Columns.Add(colvarSubtotal);
                
                TableSchema.TableColumn colvarTotal = new TableSchema.TableColumn(schema);
                colvarTotal.ColumnName = "total";
                colvarTotal.DataType = DbType.Currency;
                colvarTotal.MaxLength = 0;
                colvarTotal.AutoIncrement = false;
                colvarTotal.IsNullable = false;
                colvarTotal.IsPrimaryKey = false;
                colvarTotal.IsForeignKey = false;
                colvarTotal.IsReadOnly = false;
                
                schema.Columns.Add(colvarTotal);
                
                TableSchema.TableColumn colvarBusinessHourGuid = new TableSchema.TableColumn(schema);
                colvarBusinessHourGuid.ColumnName = "business_hour_guid";
                colvarBusinessHourGuid.DataType = DbType.Guid;
                colvarBusinessHourGuid.MaxLength = 0;
                colvarBusinessHourGuid.AutoIncrement = false;
                colvarBusinessHourGuid.IsNullable = false;
                colvarBusinessHourGuid.IsPrimaryKey = false;
                colvarBusinessHourGuid.IsForeignKey = false;
                colvarBusinessHourGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourGuid);
                
                TableSchema.TableColumn colvarSlug = new TableSchema.TableColumn(schema);
                colvarSlug.ColumnName = "slug";
                colvarSlug.DataType = DbType.String;
                colvarSlug.MaxLength = 100;
                colvarSlug.AutoIncrement = false;
                colvarSlug.IsNullable = false;
                colvarSlug.IsPrimaryKey = false;
                colvarSlug.IsForeignKey = false;
                colvarSlug.IsReadOnly = false;
                
                schema.Columns.Add(colvarSlug);
                
                TableSchema.TableColumn colvarBusinessHourOrderTimeS = new TableSchema.TableColumn(schema);
                colvarBusinessHourOrderTimeS.ColumnName = "business_hour_order_time_s";
                colvarBusinessHourOrderTimeS.DataType = DbType.DateTime;
                colvarBusinessHourOrderTimeS.MaxLength = 0;
                colvarBusinessHourOrderTimeS.AutoIncrement = false;
                colvarBusinessHourOrderTimeS.IsNullable = false;
                colvarBusinessHourOrderTimeS.IsPrimaryKey = false;
                colvarBusinessHourOrderTimeS.IsForeignKey = false;
                colvarBusinessHourOrderTimeS.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourOrderTimeS);
                
                TableSchema.TableColumn colvarBusinessHourOrderTimeE = new TableSchema.TableColumn(schema);
                colvarBusinessHourOrderTimeE.ColumnName = "business_hour_order_time_e";
                colvarBusinessHourOrderTimeE.DataType = DbType.DateTime;
                colvarBusinessHourOrderTimeE.MaxLength = 0;
                colvarBusinessHourOrderTimeE.AutoIncrement = false;
                colvarBusinessHourOrderTimeE.IsNullable = false;
                colvarBusinessHourOrderTimeE.IsPrimaryKey = false;
                colvarBusinessHourOrderTimeE.IsForeignKey = false;
                colvarBusinessHourOrderTimeE.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourOrderTimeE);
                
                TableSchema.TableColumn colvarBusinessHourDeliverTimeE = new TableSchema.TableColumn(schema);
                colvarBusinessHourDeliverTimeE.ColumnName = "business_hour_deliver_time_e";
                colvarBusinessHourDeliverTimeE.DataType = DbType.DateTime;
                colvarBusinessHourDeliverTimeE.MaxLength = 0;
                colvarBusinessHourDeliverTimeE.AutoIncrement = false;
                colvarBusinessHourDeliverTimeE.IsNullable = true;
                colvarBusinessHourDeliverTimeE.IsPrimaryKey = false;
                colvarBusinessHourDeliverTimeE.IsForeignKey = false;
                colvarBusinessHourDeliverTimeE.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourDeliverTimeE);
                
                TableSchema.TableColumn colvarBusinessHourOrderMinimum = new TableSchema.TableColumn(schema);
                colvarBusinessHourOrderMinimum.ColumnName = "business_hour_order_minimum";
                colvarBusinessHourOrderMinimum.DataType = DbType.Currency;
                colvarBusinessHourOrderMinimum.MaxLength = 0;
                colvarBusinessHourOrderMinimum.AutoIncrement = false;
                colvarBusinessHourOrderMinimum.IsNullable = false;
                colvarBusinessHourOrderMinimum.IsPrimaryKey = false;
                colvarBusinessHourOrderMinimum.IsForeignKey = false;
                colvarBusinessHourOrderMinimum.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourOrderMinimum);
                
                TableSchema.TableColumn colvarItemOrigPrice = new TableSchema.TableColumn(schema);
                colvarItemOrigPrice.ColumnName = "item_orig_price";
                colvarItemOrigPrice.DataType = DbType.Currency;
                colvarItemOrigPrice.MaxLength = 0;
                colvarItemOrigPrice.AutoIncrement = false;
                colvarItemOrigPrice.IsNullable = false;
                colvarItemOrigPrice.IsPrimaryKey = false;
                colvarItemOrigPrice.IsForeignKey = false;
                colvarItemOrigPrice.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemOrigPrice);
                
                TableSchema.TableColumn colvarItemPrice = new TableSchema.TableColumn(schema);
                colvarItemPrice.ColumnName = "item_price";
                colvarItemPrice.DataType = DbType.Currency;
                colvarItemPrice.MaxLength = 0;
                colvarItemPrice.AutoIncrement = false;
                colvarItemPrice.IsNullable = false;
                colvarItemPrice.IsPrimaryKey = false;
                colvarItemPrice.IsForeignKey = false;
                colvarItemPrice.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemPrice);
                
                TableSchema.TableColumn colvarItemName = new TableSchema.TableColumn(schema);
                colvarItemName.ColumnName = "item_name";
                colvarItemName.DataType = DbType.String;
                colvarItemName.MaxLength = 750;
                colvarItemName.AutoIncrement = false;
                colvarItemName.IsNullable = false;
                colvarItemName.IsPrimaryKey = false;
                colvarItemName.IsForeignKey = false;
                colvarItemName.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemName);
                
                TableSchema.TableColumn colvarDepartment = new TableSchema.TableColumn(schema);
                colvarDepartment.ColumnName = "department";
                colvarDepartment.DataType = DbType.Int32;
                colvarDepartment.MaxLength = 0;
                colvarDepartment.AutoIncrement = false;
                colvarDepartment.IsNullable = false;
                colvarDepartment.IsPrimaryKey = false;
                colvarDepartment.IsForeignKey = false;
                colvarDepartment.IsReadOnly = false;
                
                schema.Columns.Add(colvarDepartment);
                
                TableSchema.TableColumn colvarCancelStatus = new TableSchema.TableColumn(schema);
                colvarCancelStatus.ColumnName = "cancel_status";
                colvarCancelStatus.DataType = DbType.Int32;
                colvarCancelStatus.MaxLength = 0;
                colvarCancelStatus.AutoIncrement = false;
                colvarCancelStatus.IsNullable = false;
                colvarCancelStatus.IsPrimaryKey = false;
                colvarCancelStatus.IsForeignKey = false;
                colvarCancelStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarCancelStatus);
                
                TableSchema.TableColumn colvarRemainCount = new TableSchema.TableColumn(schema);
                colvarRemainCount.ColumnName = "remain_count";
                colvarRemainCount.DataType = DbType.Int32;
                colvarRemainCount.MaxLength = 0;
                colvarRemainCount.AutoIncrement = false;
                colvarRemainCount.IsNullable = true;
                colvarRemainCount.IsPrimaryKey = false;
                colvarRemainCount.IsForeignKey = false;
                colvarRemainCount.IsReadOnly = false;
                
                schema.Columns.Add(colvarRemainCount);
                
                TableSchema.TableColumn colvarTotalCount = new TableSchema.TableColumn(schema);
                colvarTotalCount.ColumnName = "total_count";
                colvarTotalCount.DataType = DbType.Int32;
                colvarTotalCount.MaxLength = 0;
                colvarTotalCount.AutoIncrement = false;
                colvarTotalCount.IsNullable = true;
                colvarTotalCount.IsPrimaryKey = false;
                colvarTotalCount.IsForeignKey = false;
                colvarTotalCount.IsReadOnly = false;
                
                schema.Columns.Add(colvarTotalCount);
                
                TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
                colvarStatus.ColumnName = "status";
                colvarStatus.DataType = DbType.Int32;
                colvarStatus.MaxLength = 0;
                colvarStatus.AutoIncrement = false;
                colvarStatus.IsNullable = true;
                colvarStatus.IsPrimaryKey = false;
                colvarStatus.IsForeignKey = false;
                colvarStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarStatus);
                
                TableSchema.TableColumn colvarBusinessHourDeliverTimeS = new TableSchema.TableColumn(schema);
                colvarBusinessHourDeliverTimeS.ColumnName = "business_hour_deliver_time_s";
                colvarBusinessHourDeliverTimeS.DataType = DbType.DateTime;
                colvarBusinessHourDeliverTimeS.MaxLength = 0;
                colvarBusinessHourDeliverTimeS.AutoIncrement = false;
                colvarBusinessHourDeliverTimeS.IsNullable = true;
                colvarBusinessHourDeliverTimeS.IsPrimaryKey = false;
                colvarBusinessHourDeliverTimeS.IsForeignKey = false;
                colvarBusinessHourDeliverTimeS.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourDeliverTimeS);
                
                TableSchema.TableColumn colvarBusinessHourStatus = new TableSchema.TableColumn(schema);
                colvarBusinessHourStatus.ColumnName = "business_hour_status";
                colvarBusinessHourStatus.DataType = DbType.Int32;
                colvarBusinessHourStatus.MaxLength = 0;
                colvarBusinessHourStatus.AutoIncrement = false;
                colvarBusinessHourStatus.IsNullable = false;
                colvarBusinessHourStatus.IsPrimaryKey = false;
                colvarBusinessHourStatus.IsForeignKey = false;
                colvarBusinessHourStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourStatus);
                
                TableSchema.TableColumn colvarDeliveryType = new TableSchema.TableColumn(schema);
                colvarDeliveryType.ColumnName = "delivery_type";
                colvarDeliveryType.DataType = DbType.Int32;
                colvarDeliveryType.MaxLength = 0;
                colvarDeliveryType.AutoIncrement = false;
                colvarDeliveryType.IsNullable = true;
                colvarDeliveryType.IsPrimaryKey = false;
                colvarDeliveryType.IsForeignKey = false;
                colvarDeliveryType.IsReadOnly = false;
                
                schema.Columns.Add(colvarDeliveryType);
                
                TableSchema.TableColumn colvarShoppingCart = new TableSchema.TableColumn(schema);
                colvarShoppingCart.ColumnName = "shopping_cart";
                colvarShoppingCart.DataType = DbType.Boolean;
                colvarShoppingCart.MaxLength = 0;
                colvarShoppingCart.AutoIncrement = false;
                colvarShoppingCart.IsNullable = false;
                colvarShoppingCart.IsPrimaryKey = false;
                colvarShoppingCart.IsForeignKey = false;
                colvarShoppingCart.IsReadOnly = false;
                
                schema.Columns.Add(colvarShoppingCart);
                
                TableSchema.TableColumn colvarComboPackCount = new TableSchema.TableColumn(schema);
                colvarComboPackCount.ColumnName = "combo_pack_count";
                colvarComboPackCount.DataType = DbType.Int32;
                colvarComboPackCount.MaxLength = 0;
                colvarComboPackCount.AutoIncrement = false;
                colvarComboPackCount.IsNullable = false;
                colvarComboPackCount.IsPrimaryKey = false;
                colvarComboPackCount.IsForeignKey = false;
                colvarComboPackCount.IsReadOnly = false;
                
                schema.Columns.Add(colvarComboPackCount);
                
                TableSchema.TableColumn colvarPrintCount = new TableSchema.TableColumn(schema);
                colvarPrintCount.ColumnName = "print_count";
                colvarPrintCount.DataType = DbType.Int32;
                colvarPrintCount.MaxLength = 0;
                colvarPrintCount.AutoIncrement = false;
                colvarPrintCount.IsNullable = true;
                colvarPrintCount.IsPrimaryKey = false;
                colvarPrintCount.IsForeignKey = false;
                colvarPrintCount.IsReadOnly = false;
                
                schema.Columns.Add(colvarPrintCount);
                
                TableSchema.TableColumn colvarCouponUsage = new TableSchema.TableColumn(schema);
                colvarCouponUsage.ColumnName = "coupon_usage";
                colvarCouponUsage.DataType = DbType.String;
                colvarCouponUsage.MaxLength = 500;
                colvarCouponUsage.AutoIncrement = false;
                colvarCouponUsage.IsNullable = true;
                colvarCouponUsage.IsPrimaryKey = false;
                colvarCouponUsage.IsForeignKey = false;
                colvarCouponUsage.IsReadOnly = false;
                
                schema.Columns.Add(colvarCouponUsage);
                
                TableSchema.TableColumn colvarImagePath = new TableSchema.TableColumn(schema);
                colvarImagePath.ColumnName = "image_path";
                colvarImagePath.DataType = DbType.String;
                colvarImagePath.MaxLength = 1000;
                colvarImagePath.AutoIncrement = false;
                colvarImagePath.IsNullable = true;
                colvarImagePath.IsPrimaryKey = false;
                colvarImagePath.IsForeignKey = false;
                colvarImagePath.IsReadOnly = false;
                
                schema.Columns.Add(colvarImagePath);
                
                TableSchema.TableColumn colvarChangedExpireDate = new TableSchema.TableColumn(schema);
                colvarChangedExpireDate.ColumnName = "changed_expire_date";
                colvarChangedExpireDate.DataType = DbType.DateTime;
                colvarChangedExpireDate.MaxLength = 0;
                colvarChangedExpireDate.AutoIncrement = false;
                colvarChangedExpireDate.IsNullable = true;
                colvarChangedExpireDate.IsPrimaryKey = false;
                colvarChangedExpireDate.IsForeignKey = false;
                colvarChangedExpireDate.IsReadOnly = false;
                
                schema.Columns.Add(colvarChangedExpireDate);
                
                TableSchema.TableColumn colvarCouponCodeType = new TableSchema.TableColumn(schema);
                colvarCouponCodeType.ColumnName = "coupon_code_type";
                colvarCouponCodeType.DataType = DbType.Int32;
                colvarCouponCodeType.MaxLength = 0;
                colvarCouponCodeType.AutoIncrement = false;
                colvarCouponCodeType.IsNullable = false;
                colvarCouponCodeType.IsPrimaryKey = false;
                colvarCouponCodeType.IsForeignKey = false;
                colvarCouponCodeType.IsReadOnly = false;
                
                schema.Columns.Add(colvarCouponCodeType);
                
                TableSchema.TableColumn colvarPinType = new TableSchema.TableColumn(schema);
                colvarPinType.ColumnName = "pin_type";
                colvarPinType.DataType = DbType.Int32;
                colvarPinType.MaxLength = 0;
                colvarPinType.AutoIncrement = false;
                colvarPinType.IsNullable = false;
                colvarPinType.IsPrimaryKey = false;
                colvarPinType.IsForeignKey = false;
                colvarPinType.IsReadOnly = false;
                
                schema.Columns.Add(colvarPinType);
                
                TableSchema.TableColumn colvarBookingSystemType = new TableSchema.TableColumn(schema);
                colvarBookingSystemType.ColumnName = "booking_system_type";
                colvarBookingSystemType.DataType = DbType.Int32;
                colvarBookingSystemType.MaxLength = 0;
                colvarBookingSystemType.AutoIncrement = false;
                colvarBookingSystemType.IsNullable = false;
                colvarBookingSystemType.IsPrimaryKey = false;
                colvarBookingSystemType.IsForeignKey = false;
                colvarBookingSystemType.IsReadOnly = false;
                
                schema.Columns.Add(colvarBookingSystemType);
                
                TableSchema.TableColumn colvarReturnStatus = new TableSchema.TableColumn(schema);
                colvarReturnStatus.ColumnName = "return_status";
                colvarReturnStatus.DataType = DbType.Int32;
                colvarReturnStatus.MaxLength = 0;
                colvarReturnStatus.AutoIncrement = false;
                colvarReturnStatus.IsNullable = false;
                colvarReturnStatus.IsPrimaryKey = false;
                colvarReturnStatus.IsForeignKey = false;
                colvarReturnStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarReturnStatus);
                
                TableSchema.TableColumn colvarAppTitle = new TableSchema.TableColumn(schema);
                colvarAppTitle.ColumnName = "app_title";
                colvarAppTitle.DataType = DbType.String;
                colvarAppTitle.MaxLength = 40;
                colvarAppTitle.AutoIncrement = false;
                colvarAppTitle.IsNullable = true;
                colvarAppTitle.IsPrimaryKey = false;
                colvarAppTitle.IsForeignKey = false;
                colvarAppTitle.IsReadOnly = false;
                
                schema.Columns.Add(colvarAppTitle);
                
                TableSchema.TableColumn colvarShipTime = new TableSchema.TableColumn(schema);
                colvarShipTime.ColumnName = "ship_time";
                colvarShipTime.DataType = DbType.DateTime;
                colvarShipTime.MaxLength = 0;
                colvarShipTime.AutoIncrement = false;
                colvarShipTime.IsNullable = true;
                colvarShipTime.IsPrimaryKey = false;
                colvarShipTime.IsForeignKey = false;
                colvarShipTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarShipTime);
                
                TableSchema.TableColumn colvarShipType = new TableSchema.TableColumn(schema);
                colvarShipType.ColumnName = "ship_type";
                colvarShipType.DataType = DbType.Int32;
                colvarShipType.MaxLength = 0;
                colvarShipType.AutoIncrement = false;
                colvarShipType.IsNullable = true;
                colvarShipType.IsPrimaryKey = false;
                colvarShipType.IsForeignKey = false;
                colvarShipType.IsReadOnly = false;
                
                schema.Columns.Add(colvarShipType);
                
                TableSchema.TableColumn colvarShippingdateType = new TableSchema.TableColumn(schema);
                colvarShippingdateType.ColumnName = "shippingdate_type";
                colvarShippingdateType.DataType = DbType.Int32;
                colvarShippingdateType.MaxLength = 0;
                colvarShippingdateType.AutoIncrement = false;
                colvarShippingdateType.IsNullable = true;
                colvarShippingdateType.IsPrimaryKey = false;
                colvarShippingdateType.IsForeignKey = false;
                colvarShippingdateType.IsReadOnly = false;
                
                schema.Columns.Add(colvarShippingdateType);
                
                TableSchema.TableColumn colvarShippingdate = new TableSchema.TableColumn(schema);
                colvarShippingdate.ColumnName = "shippingdate";
                colvarShippingdate.DataType = DbType.Int32;
                colvarShippingdate.MaxLength = 0;
                colvarShippingdate.AutoIncrement = false;
                colvarShippingdate.IsNullable = true;
                colvarShippingdate.IsPrimaryKey = false;
                colvarShippingdate.IsForeignKey = false;
                colvarShippingdate.IsReadOnly = false;
                
                schema.Columns.Add(colvarShippingdate);
                
                TableSchema.TableColumn colvarProductUseDateEndSet = new TableSchema.TableColumn(schema);
                colvarProductUseDateEndSet.ColumnName = "product_use_date_end_set";
                colvarProductUseDateEndSet.DataType = DbType.Int32;
                colvarProductUseDateEndSet.MaxLength = 0;
                colvarProductUseDateEndSet.AutoIncrement = false;
                colvarProductUseDateEndSet.IsNullable = true;
                colvarProductUseDateEndSet.IsPrimaryKey = false;
                colvarProductUseDateEndSet.IsForeignKey = false;
                colvarProductUseDateEndSet.IsReadOnly = false;
                
                schema.Columns.Add(colvarProductUseDateEndSet);
                
                TableSchema.TableColumn colvarGroupProductUnitPrice = new TableSchema.TableColumn(schema);
                colvarGroupProductUnitPrice.ColumnName = "group_product_unit_price";
                colvarGroupProductUnitPrice.DataType = DbType.Int32;
                colvarGroupProductUnitPrice.MaxLength = 0;
                colvarGroupProductUnitPrice.AutoIncrement = false;
                colvarGroupProductUnitPrice.IsNullable = true;
                colvarGroupProductUnitPrice.IsPrimaryKey = false;
                colvarGroupProductUnitPrice.IsForeignKey = false;
                colvarGroupProductUnitPrice.IsReadOnly = false;
                
                schema.Columns.Add(colvarGroupProductUnitPrice);
                
                TableSchema.TableColumn colvarGroupCouponAppStyle = new TableSchema.TableColumn(schema);
                colvarGroupCouponAppStyle.ColumnName = "group_coupon_app_style";
                colvarGroupCouponAppStyle.DataType = DbType.Int32;
                colvarGroupCouponAppStyle.MaxLength = 0;
                colvarGroupCouponAppStyle.AutoIncrement = false;
                colvarGroupCouponAppStyle.IsNullable = false;
                colvarGroupCouponAppStyle.IsPrimaryKey = false;
                colvarGroupCouponAppStyle.IsForeignKey = false;
                colvarGroupCouponAppStyle.IsReadOnly = false;
                
                schema.Columns.Add(colvarGroupCouponAppStyle);
                
                TableSchema.TableColumn colvarName = new TableSchema.TableColumn(schema);
                colvarName.ColumnName = "name";
                colvarName.DataType = DbType.String;
                colvarName.MaxLength = 400;
                colvarName.AutoIncrement = false;
                colvarName.IsNullable = true;
                colvarName.IsPrimaryKey = false;
                colvarName.IsForeignKey = false;
                colvarName.IsReadOnly = false;
                
                schema.Columns.Add(colvarName);
                
                TableSchema.TableColumn colvarConsignment = new TableSchema.TableColumn(schema);
                colvarConsignment.ColumnName = "consignment";
                colvarConsignment.DataType = DbType.Boolean;
                colvarConsignment.MaxLength = 0;
                colvarConsignment.AutoIncrement = false;
                colvarConsignment.IsNullable = false;
                colvarConsignment.IsPrimaryKey = false;
                colvarConsignment.IsForeignKey = false;
                colvarConsignment.IsReadOnly = false;
                
                schema.Columns.Add(colvarConsignment);
                
                TableSchema.TableColumn colvarExpirationDate = new TableSchema.TableColumn(schema);
                colvarExpirationDate.ColumnName = "expiration_date";
                colvarExpirationDate.DataType = DbType.DateTime;
                colvarExpirationDate.MaxLength = 0;
                colvarExpirationDate.AutoIncrement = false;
                colvarExpirationDate.IsNullable = true;
                colvarExpirationDate.IsPrimaryKey = false;
                colvarExpirationDate.IsForeignKey = false;
                colvarExpirationDate.IsReadOnly = false;
                
                schema.Columns.Add(colvarExpirationDate);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_coupon_only_list_main",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewCouponOnlyListMain()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewCouponOnlyListMain(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewCouponOnlyListMain(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewCouponOnlyListMain(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime CreateTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("create_time");
		    }
            set 
		    {
			    SetColumnValue("create_time", value);
            }
        }
	      
        [XmlAttribute("OrderId")]
        [Bindable(true)]
        public string OrderId 
	    {
		    get
		    {
			    return GetColumnValue<string>("order_id");
		    }
            set 
		    {
			    SetColumnValue("order_id", value);
            }
        }
	      
        [XmlAttribute("Guid")]
        [Bindable(true)]
        public Guid Guid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("GUID");
		    }
            set 
		    {
			    SetColumnValue("GUID", value);
            }
        }
	      
        [XmlAttribute("OrderStatus")]
        [Bindable(true)]
        public int OrderStatus 
	    {
		    get
		    {
			    return GetColumnValue<int>("order_status");
		    }
            set 
		    {
			    SetColumnValue("order_status", value);
            }
        }
	      
        [XmlAttribute("MemberEmail")]
        [Bindable(true)]
        public string MemberEmail 
	    {
		    get
		    {
			    return GetColumnValue<string>("member_email");
		    }
            set 
		    {
			    SetColumnValue("member_email", value);
            }
        }
	      
        [XmlAttribute("UniqueId")]
        [Bindable(true)]
        public int UniqueId 
	    {
		    get
		    {
			    return GetColumnValue<int>("unique_id");
		    }
            set 
		    {
			    SetColumnValue("unique_id", value);
            }
        }
	      
        [XmlAttribute("Subtotal")]
        [Bindable(true)]
        public decimal Subtotal 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("subtotal");
		    }
            set 
		    {
			    SetColumnValue("subtotal", value);
            }
        }
	      
        [XmlAttribute("Total")]
        [Bindable(true)]
        public decimal Total 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("total");
		    }
            set 
		    {
			    SetColumnValue("total", value);
            }
        }
	      
        [XmlAttribute("BusinessHourGuid")]
        [Bindable(true)]
        public Guid BusinessHourGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("business_hour_guid");
		    }
            set 
		    {
			    SetColumnValue("business_hour_guid", value);
            }
        }
	      
        [XmlAttribute("Slug")]
        [Bindable(true)]
        public string Slug 
	    {
		    get
		    {
			    return GetColumnValue<string>("slug");
		    }
            set 
		    {
			    SetColumnValue("slug", value);
            }
        }
	      
        [XmlAttribute("BusinessHourOrderTimeS")]
        [Bindable(true)]
        public DateTime BusinessHourOrderTimeS 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("business_hour_order_time_s");
		    }
            set 
		    {
			    SetColumnValue("business_hour_order_time_s", value);
            }
        }
	      
        [XmlAttribute("BusinessHourOrderTimeE")]
        [Bindable(true)]
        public DateTime BusinessHourOrderTimeE 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("business_hour_order_time_e");
		    }
            set 
		    {
			    SetColumnValue("business_hour_order_time_e", value);
            }
        }
	      
        [XmlAttribute("BusinessHourDeliverTimeE")]
        [Bindable(true)]
        public DateTime? BusinessHourDeliverTimeE 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("business_hour_deliver_time_e");
		    }
            set 
		    {
			    SetColumnValue("business_hour_deliver_time_e", value);
            }
        }
	      
        [XmlAttribute("BusinessHourOrderMinimum")]
        [Bindable(true)]
        public decimal BusinessHourOrderMinimum 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("business_hour_order_minimum");
		    }
            set 
		    {
			    SetColumnValue("business_hour_order_minimum", value);
            }
        }
	      
        [XmlAttribute("ItemOrigPrice")]
        [Bindable(true)]
        public decimal ItemOrigPrice 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("item_orig_price");
		    }
            set 
		    {
			    SetColumnValue("item_orig_price", value);
            }
        }
	      
        [XmlAttribute("ItemPrice")]
        [Bindable(true)]
        public decimal ItemPrice 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("item_price");
		    }
            set 
		    {
			    SetColumnValue("item_price", value);
            }
        }
	      
        [XmlAttribute("ItemName")]
        [Bindable(true)]
        public string ItemName 
	    {
		    get
		    {
			    return GetColumnValue<string>("item_name");
		    }
            set 
		    {
			    SetColumnValue("item_name", value);
            }
        }
	      
        [XmlAttribute("Department")]
        [Bindable(true)]
        public int Department 
	    {
		    get
		    {
			    return GetColumnValue<int>("department");
		    }
            set 
		    {
			    SetColumnValue("department", value);
            }
        }
	      
        [XmlAttribute("CancelStatus")]
        [Bindable(true)]
        public int CancelStatus 
	    {
		    get
		    {
			    return GetColumnValue<int>("cancel_status");
		    }
            set 
		    {
			    SetColumnValue("cancel_status", value);
            }
        }
	      
        [XmlAttribute("RemainCount")]
        [Bindable(true)]
        public int? RemainCount 
	    {
		    get
		    {
			    return GetColumnValue<int?>("remain_count");
		    }
            set 
		    {
			    SetColumnValue("remain_count", value);
            }
        }
	      
        [XmlAttribute("TotalCount")]
        [Bindable(true)]
        public int? TotalCount 
	    {
		    get
		    {
			    return GetColumnValue<int?>("total_count");
		    }
            set 
		    {
			    SetColumnValue("total_count", value);
            }
        }
	      
        [XmlAttribute("Status")]
        [Bindable(true)]
        public int? Status 
	    {
		    get
		    {
			    return GetColumnValue<int?>("status");
		    }
            set 
		    {
			    SetColumnValue("status", value);
            }
        }
	      
        [XmlAttribute("BusinessHourDeliverTimeS")]
        [Bindable(true)]
        public DateTime? BusinessHourDeliverTimeS 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("business_hour_deliver_time_s");
		    }
            set 
		    {
			    SetColumnValue("business_hour_deliver_time_s", value);
            }
        }
	      
        [XmlAttribute("BusinessHourStatus")]
        [Bindable(true)]
        public int BusinessHourStatus 
	    {
		    get
		    {
			    return GetColumnValue<int>("business_hour_status");
		    }
            set 
		    {
			    SetColumnValue("business_hour_status", value);
            }
        }
	      
        [XmlAttribute("DeliveryType")]
        [Bindable(true)]
        public int? DeliveryType 
	    {
		    get
		    {
			    return GetColumnValue<int?>("delivery_type");
		    }
            set 
		    {
			    SetColumnValue("delivery_type", value);
            }
        }
	      
        [XmlAttribute("ShoppingCart")]
        [Bindable(true)]
        public bool ShoppingCart 
	    {
		    get
		    {
			    return GetColumnValue<bool>("shopping_cart");
		    }
            set 
		    {
			    SetColumnValue("shopping_cart", value);
            }
        }
	      
        [XmlAttribute("ComboPackCount")]
        [Bindable(true)]
        public int ComboPackCount 
	    {
		    get
		    {
			    return GetColumnValue<int>("combo_pack_count");
		    }
            set 
		    {
			    SetColumnValue("combo_pack_count", value);
            }
        }
	      
        [XmlAttribute("PrintCount")]
        [Bindable(true)]
        public int? PrintCount 
	    {
		    get
		    {
			    return GetColumnValue<int?>("print_count");
		    }
            set 
		    {
			    SetColumnValue("print_count", value);
            }
        }
	      
        [XmlAttribute("CouponUsage")]
        [Bindable(true)]
        public string CouponUsage 
	    {
		    get
		    {
			    return GetColumnValue<string>("coupon_usage");
		    }
            set 
		    {
			    SetColumnValue("coupon_usage", value);
            }
        }
	      
        [XmlAttribute("ImagePath")]
        [Bindable(true)]
        public string ImagePath 
	    {
		    get
		    {
			    return GetColumnValue<string>("image_path");
		    }
            set 
		    {
			    SetColumnValue("image_path", value);
            }
        }
	      
        [XmlAttribute("ChangedExpireDate")]
        [Bindable(true)]
        public DateTime? ChangedExpireDate 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("changed_expire_date");
		    }
            set 
		    {
			    SetColumnValue("changed_expire_date", value);
            }
        }
	      
        [XmlAttribute("CouponCodeType")]
        [Bindable(true)]
        public int CouponCodeType 
	    {
		    get
		    {
			    return GetColumnValue<int>("coupon_code_type");
		    }
            set 
		    {
			    SetColumnValue("coupon_code_type", value);
            }
        }
	      
        [XmlAttribute("PinType")]
        [Bindable(true)]
        public int PinType 
	    {
		    get
		    {
			    return GetColumnValue<int>("pin_type");
		    }
            set 
		    {
			    SetColumnValue("pin_type", value);
            }
        }
	      
        [XmlAttribute("BookingSystemType")]
        [Bindable(true)]
        public int BookingSystemType 
	    {
		    get
		    {
			    return GetColumnValue<int>("booking_system_type");
		    }
            set 
		    {
			    SetColumnValue("booking_system_type", value);
            }
        }
	      
        [XmlAttribute("ReturnStatus")]
        [Bindable(true)]
        public int ReturnStatus 
	    {
		    get
		    {
			    return GetColumnValue<int>("return_status");
		    }
            set 
		    {
			    SetColumnValue("return_status", value);
            }
        }
	      
        [XmlAttribute("AppTitle")]
        [Bindable(true)]
        public string AppTitle 
	    {
		    get
		    {
			    return GetColumnValue<string>("app_title");
		    }
            set 
		    {
			    SetColumnValue("app_title", value);
            }
        }
	      
        [XmlAttribute("ShipTime")]
        [Bindable(true)]
        public DateTime? ShipTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("ship_time");
		    }
            set 
		    {
			    SetColumnValue("ship_time", value);
            }
        }
	      
        [XmlAttribute("ShipType")]
        [Bindable(true)]
        public int? ShipType 
	    {
		    get
		    {
			    return GetColumnValue<int?>("ship_type");
		    }
            set 
		    {
			    SetColumnValue("ship_type", value);
            }
        }
	      
        [XmlAttribute("ShippingdateType")]
        [Bindable(true)]
        public int? ShippingdateType 
	    {
		    get
		    {
			    return GetColumnValue<int?>("shippingdate_type");
		    }
            set 
		    {
			    SetColumnValue("shippingdate_type", value);
            }
        }
	      
        [XmlAttribute("Shippingdate")]
        [Bindable(true)]
        public int? Shippingdate 
	    {
		    get
		    {
			    return GetColumnValue<int?>("shippingdate");
		    }
            set 
		    {
			    SetColumnValue("shippingdate", value);
            }
        }
	      
        [XmlAttribute("ProductUseDateEndSet")]
        [Bindable(true)]
        public int? ProductUseDateEndSet 
	    {
		    get
		    {
			    return GetColumnValue<int?>("product_use_date_end_set");
		    }
            set 
		    {
			    SetColumnValue("product_use_date_end_set", value);
            }
        }
	      
        [XmlAttribute("GroupProductUnitPrice")]
        [Bindable(true)]
        public int? GroupProductUnitPrice 
	    {
		    get
		    {
			    return GetColumnValue<int?>("group_product_unit_price");
		    }
            set 
		    {
			    SetColumnValue("group_product_unit_price", value);
            }
        }
	      
        [XmlAttribute("GroupCouponAppStyle")]
        [Bindable(true)]
        public int GroupCouponAppStyle 
	    {
		    get
		    {
			    return GetColumnValue<int>("group_coupon_app_style");
		    }
            set 
		    {
			    SetColumnValue("group_coupon_app_style", value);
            }
        }
	      
        [XmlAttribute("Name")]
        [Bindable(true)]
        public string Name 
	    {
		    get
		    {
			    return GetColumnValue<string>("name");
		    }
            set 
		    {
			    SetColumnValue("name", value);
            }
        }
	      
        [XmlAttribute("Consignment")]
        [Bindable(true)]
        public bool Consignment 
	    {
		    get
		    {
			    return GetColumnValue<bool>("consignment");
		    }
            set 
		    {
			    SetColumnValue("consignment", value);
            }
        }
	      
        [XmlAttribute("ExpirationDate")]
        [Bindable(true)]
        public DateTime? ExpirationDate 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("expiration_date");
		    }
            set 
		    {
			    SetColumnValue("expiration_date", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string CreateTime = @"create_time";
            
            public static string OrderId = @"order_id";
            
            public static string Guid = @"GUID";
            
            public static string OrderStatus = @"order_status";
            
            public static string MemberEmail = @"member_email";
            
            public static string UniqueId = @"unique_id";
            
            public static string Subtotal = @"subtotal";
            
            public static string Total = @"total";
            
            public static string BusinessHourGuid = @"business_hour_guid";
            
            public static string Slug = @"slug";
            
            public static string BusinessHourOrderTimeS = @"business_hour_order_time_s";
            
            public static string BusinessHourOrderTimeE = @"business_hour_order_time_e";
            
            public static string BusinessHourDeliverTimeE = @"business_hour_deliver_time_e";
            
            public static string BusinessHourOrderMinimum = @"business_hour_order_minimum";
            
            public static string ItemOrigPrice = @"item_orig_price";
            
            public static string ItemPrice = @"item_price";
            
            public static string ItemName = @"item_name";
            
            public static string Department = @"department";
            
            public static string CancelStatus = @"cancel_status";
            
            public static string RemainCount = @"remain_count";
            
            public static string TotalCount = @"total_count";
            
            public static string Status = @"status";
            
            public static string BusinessHourDeliverTimeS = @"business_hour_deliver_time_s";
            
            public static string BusinessHourStatus = @"business_hour_status";
            
            public static string DeliveryType = @"delivery_type";
            
            public static string ShoppingCart = @"shopping_cart";
            
            public static string ComboPackCount = @"combo_pack_count";
            
            public static string PrintCount = @"print_count";
            
            public static string CouponUsage = @"coupon_usage";
            
            public static string ImagePath = @"image_path";
            
            public static string ChangedExpireDate = @"changed_expire_date";
            
            public static string CouponCodeType = @"coupon_code_type";
            
            public static string PinType = @"pin_type";
            
            public static string BookingSystemType = @"booking_system_type";
            
            public static string ReturnStatus = @"return_status";
            
            public static string AppTitle = @"app_title";
            
            public static string ShipTime = @"ship_time";
            
            public static string ShipType = @"ship_type";
            
            public static string ShippingdateType = @"shippingdate_type";
            
            public static string Shippingdate = @"shippingdate";
            
            public static string ProductUseDateEndSet = @"product_use_date_end_set";
            
            public static string GroupProductUnitPrice = @"group_product_unit_price";
            
            public static string GroupCouponAppStyle = @"group_coupon_app_style";
            
            public static string Name = @"name";
            
            public static string Consignment = @"consignment";
            
            public static string ExpirationDate = @"expiration_date";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

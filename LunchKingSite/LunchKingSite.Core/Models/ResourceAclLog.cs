using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the ResourceAclLog class.
	/// </summary>
    [Serializable]
	public partial class ResourceAclLogCollection : RepositoryList<ResourceAclLog, ResourceAclLogCollection>
	{	   
		public ResourceAclLogCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>ResourceAclLogCollection</returns>
		public ResourceAclLogCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                ResourceAclLog o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the resource_acl_log table.
	/// </summary>
	[Serializable]
	public partial class ResourceAclLog : RepositoryRecord<ResourceAclLog>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public ResourceAclLog()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public ResourceAclLog(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("resource_acl_log", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarResourceAclId = new TableSchema.TableColumn(schema);
				colvarResourceAclId.ColumnName = "resource_acl_id";
				colvarResourceAclId.DataType = DbType.Int32;
				colvarResourceAclId.MaxLength = 0;
				colvarResourceAclId.AutoIncrement = false;
				colvarResourceAclId.IsNullable = false;
				colvarResourceAclId.IsPrimaryKey = false;
				colvarResourceAclId.IsForeignKey = false;
				colvarResourceAclId.IsReadOnly = false;
				colvarResourceAclId.DefaultSetting = @"";
				colvarResourceAclId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarResourceAclId);
				
				TableSchema.TableColumn colvarAccountId = new TableSchema.TableColumn(schema);
				colvarAccountId.ColumnName = "account_id";
				colvarAccountId.DataType = DbType.String;
				colvarAccountId.MaxLength = 256;
				colvarAccountId.AutoIncrement = false;
				colvarAccountId.IsNullable = false;
				colvarAccountId.IsPrimaryKey = false;
				colvarAccountId.IsForeignKey = false;
				colvarAccountId.IsReadOnly = false;
				colvarAccountId.DefaultSetting = @"";
				colvarAccountId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAccountId);
				
				TableSchema.TableColumn colvarAccountType = new TableSchema.TableColumn(schema);
				colvarAccountType.ColumnName = "account_type";
				colvarAccountType.DataType = DbType.Int32;
				colvarAccountType.MaxLength = 0;
				colvarAccountType.AutoIncrement = false;
				colvarAccountType.IsNullable = false;
				colvarAccountType.IsPrimaryKey = false;
				colvarAccountType.IsForeignKey = false;
				colvarAccountType.IsReadOnly = false;
				colvarAccountType.DefaultSetting = @"";
				colvarAccountType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAccountType);
				
				TableSchema.TableColumn colvarResourceGuid = new TableSchema.TableColumn(schema);
				colvarResourceGuid.ColumnName = "resource_guid";
				colvarResourceGuid.DataType = DbType.Guid;
				colvarResourceGuid.MaxLength = 0;
				colvarResourceGuid.AutoIncrement = false;
				colvarResourceGuid.IsNullable = false;
				colvarResourceGuid.IsPrimaryKey = false;
				colvarResourceGuid.IsForeignKey = false;
				colvarResourceGuid.IsReadOnly = false;
				colvarResourceGuid.DefaultSetting = @"";
				colvarResourceGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarResourceGuid);
				
				TableSchema.TableColumn colvarResourceType = new TableSchema.TableColumn(schema);
				colvarResourceType.ColumnName = "resource_type";
				colvarResourceType.DataType = DbType.Int32;
				colvarResourceType.MaxLength = 0;
				colvarResourceType.AutoIncrement = false;
				colvarResourceType.IsNullable = false;
				colvarResourceType.IsPrimaryKey = false;
				colvarResourceType.IsForeignKey = false;
				colvarResourceType.IsReadOnly = false;
				colvarResourceType.DefaultSetting = @"";
				colvarResourceType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarResourceType);
				
				TableSchema.TableColumn colvarPermissionType = new TableSchema.TableColumn(schema);
				colvarPermissionType.ColumnName = "permission_type";
				colvarPermissionType.DataType = DbType.Int32;
				colvarPermissionType.MaxLength = 0;
				colvarPermissionType.AutoIncrement = false;
				colvarPermissionType.IsNullable = false;
				colvarPermissionType.IsPrimaryKey = false;
				colvarPermissionType.IsForeignKey = false;
				colvarPermissionType.IsReadOnly = false;
				colvarPermissionType.DefaultSetting = @"";
				colvarPermissionType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPermissionType);
				
				TableSchema.TableColumn colvarPermissionSetting = new TableSchema.TableColumn(schema);
				colvarPermissionSetting.ColumnName = "permission_setting";
				colvarPermissionSetting.DataType = DbType.Int32;
				colvarPermissionSetting.MaxLength = 0;
				colvarPermissionSetting.AutoIncrement = false;
				colvarPermissionSetting.IsNullable = false;
				colvarPermissionSetting.IsPrimaryKey = false;
				colvarPermissionSetting.IsForeignKey = false;
				colvarPermissionSetting.IsReadOnly = false;
				colvarPermissionSetting.DefaultSetting = @"";
				colvarPermissionSetting.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPermissionSetting);
				
				TableSchema.TableColumn colvarReason = new TableSchema.TableColumn(schema);
				colvarReason.ColumnName = "reason";
				colvarReason.DataType = DbType.String;
				colvarReason.MaxLength = 100;
				colvarReason.AutoIncrement = false;
				colvarReason.IsNullable = true;
				colvarReason.IsPrimaryKey = false;
				colvarReason.IsForeignKey = false;
				colvarReason.IsReadOnly = false;
				colvarReason.DefaultSetting = @"";
				colvarReason.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReason);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
				colvarCreateId.ColumnName = "create_id";
				colvarCreateId.DataType = DbType.String;
				colvarCreateId.MaxLength = 256;
				colvarCreateId.AutoIncrement = false;
				colvarCreateId.IsNullable = false;
				colvarCreateId.IsPrimaryKey = false;
				colvarCreateId.IsForeignKey = false;
				colvarCreateId.IsReadOnly = false;
				colvarCreateId.DefaultSetting = @"";
				colvarCreateId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateId);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("resource_acl_log",schema);
			}
		}
		#endregion
		
		#region Props
		
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		
		[XmlAttribute("ResourceAclId")]
		[Bindable(true)]
		public int ResourceAclId 
		{
			get { return GetColumnValue<int>(Columns.ResourceAclId); }
			set { SetColumnValue(Columns.ResourceAclId, value); }
		}
		
		[XmlAttribute("AccountId")]
		[Bindable(true)]
		public string AccountId 
		{
			get { return GetColumnValue<string>(Columns.AccountId); }
			set { SetColumnValue(Columns.AccountId, value); }
		}
		
		[XmlAttribute("AccountType")]
		[Bindable(true)]
		public int AccountType 
		{
			get { return GetColumnValue<int>(Columns.AccountType); }
			set { SetColumnValue(Columns.AccountType, value); }
		}
		
		[XmlAttribute("ResourceGuid")]
		[Bindable(true)]
		public Guid ResourceGuid 
		{
			get { return GetColumnValue<Guid>(Columns.ResourceGuid); }
			set { SetColumnValue(Columns.ResourceGuid, value); }
		}
		
		[XmlAttribute("ResourceType")]
		[Bindable(true)]
		public int ResourceType 
		{
			get { return GetColumnValue<int>(Columns.ResourceType); }
			set { SetColumnValue(Columns.ResourceType, value); }
		}
		
		[XmlAttribute("PermissionType")]
		[Bindable(true)]
		public int PermissionType 
		{
			get { return GetColumnValue<int>(Columns.PermissionType); }
			set { SetColumnValue(Columns.PermissionType, value); }
		}
		
		[XmlAttribute("PermissionSetting")]
		[Bindable(true)]
		public int PermissionSetting 
		{
			get { return GetColumnValue<int>(Columns.PermissionSetting); }
			set { SetColumnValue(Columns.PermissionSetting, value); }
		}
		
		[XmlAttribute("Reason")]
		[Bindable(true)]
		public string Reason 
		{
			get { return GetColumnValue<string>(Columns.Reason); }
			set { SetColumnValue(Columns.Reason, value); }
		}
		
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		
		[XmlAttribute("CreateId")]
		[Bindable(true)]
		public string CreateId 
		{
			get { return GetColumnValue<string>(Columns.CreateId); }
			set { SetColumnValue(Columns.CreateId, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn ResourceAclIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn AccountIdColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn AccountTypeColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn ResourceGuidColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn ResourceTypeColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn PermissionTypeColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn PermissionSettingColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn ReasonColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string ResourceAclId = @"resource_acl_id";
			 public static string AccountId = @"account_id";
			 public static string AccountType = @"account_type";
			 public static string ResourceGuid = @"resource_guid";
			 public static string ResourceType = @"resource_type";
			 public static string PermissionType = @"permission_type";
			 public static string PermissionSetting = @"permission_setting";
			 public static string Reason = @"reason";
			 public static string CreateTime = @"create_time";
			 public static string CreateId = @"create_id";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

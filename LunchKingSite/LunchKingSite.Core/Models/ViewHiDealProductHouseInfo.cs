using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewHiDealProductHouseInfo class.
    /// </summary>
    [Serializable]
    public partial class ViewHiDealProductHouseInfoCollection : ReadOnlyList<ViewHiDealProductHouseInfo, ViewHiDealProductHouseInfoCollection>
    {        
        public ViewHiDealProductHouseInfoCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_hi_deal_product_house_info view.
    /// </summary>
    [Serializable]
    public partial class ViewHiDealProductHouseInfo : ReadOnlyRecord<ViewHiDealProductHouseInfo>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_hi_deal_product_house_info", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarOrderId = new TableSchema.TableColumn(schema);
                colvarOrderId.ColumnName = "order_id";
                colvarOrderId.DataType = DbType.AnsiString;
                colvarOrderId.MaxLength = 30;
                colvarOrderId.AutoIncrement = false;
                colvarOrderId.IsNullable = false;
                colvarOrderId.IsPrimaryKey = false;
                colvarOrderId.IsForeignKey = false;
                colvarOrderId.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderId);
                
                TableSchema.TableColumn colvarProductId = new TableSchema.TableColumn(schema);
                colvarProductId.ColumnName = "product_id";
                colvarProductId.DataType = DbType.Int32;
                colvarProductId.MaxLength = 0;
                colvarProductId.AutoIncrement = false;
                colvarProductId.IsNullable = true;
                colvarProductId.IsPrimaryKey = false;
                colvarProductId.IsForeignKey = false;
                colvarProductId.IsReadOnly = false;
                
                schema.Columns.Add(colvarProductId);
                
                TableSchema.TableColumn colvarDeliveryType = new TableSchema.TableColumn(schema);
                colvarDeliveryType.ColumnName = "delivery_type";
                colvarDeliveryType.DataType = DbType.Int32;
                colvarDeliveryType.MaxLength = 0;
                colvarDeliveryType.AutoIncrement = false;
                colvarDeliveryType.IsNullable = true;
                colvarDeliveryType.IsPrimaryKey = false;
                colvarDeliveryType.IsForeignKey = false;
                colvarDeliveryType.IsReadOnly = false;
                
                schema.Columns.Add(colvarDeliveryType);
                
                TableSchema.TableColumn colvarProductName = new TableSchema.TableColumn(schema);
                colvarProductName.ColumnName = "product_name";
                colvarProductName.DataType = DbType.String;
                colvarProductName.MaxLength = 250;
                colvarProductName.AutoIncrement = false;
                colvarProductName.IsNullable = true;
                colvarProductName.IsPrimaryKey = false;
                colvarProductName.IsForeignKey = false;
                colvarProductName.IsReadOnly = false;
                
                schema.Columns.Add(colvarProductName);
                
                TableSchema.TableColumn colvarName = new TableSchema.TableColumn(schema);
                colvarName.ColumnName = "name";
                colvarName.DataType = DbType.String;
                colvarName.MaxLength = 100;
                colvarName.AutoIncrement = false;
                colvarName.IsNullable = true;
                colvarName.IsPrimaryKey = false;
                colvarName.IsForeignKey = false;
                colvarName.IsReadOnly = false;
                
                schema.Columns.Add(colvarName);
                
                TableSchema.TableColumn colvarAddresseeName = new TableSchema.TableColumn(schema);
                colvarAddresseeName.ColumnName = "addressee_name";
                colvarAddresseeName.DataType = DbType.String;
                colvarAddresseeName.MaxLength = 50;
                colvarAddresseeName.AutoIncrement = false;
                colvarAddresseeName.IsNullable = true;
                colvarAddresseeName.IsPrimaryKey = false;
                colvarAddresseeName.IsForeignKey = false;
                colvarAddresseeName.IsReadOnly = false;
                
                schema.Columns.Add(colvarAddresseeName);
                
                TableSchema.TableColumn colvarAddresseePhone = new TableSchema.TableColumn(schema);
                colvarAddresseePhone.ColumnName = "addressee_phone";
                colvarAddresseePhone.DataType = DbType.String;
                colvarAddresseePhone.MaxLength = 50;
                colvarAddresseePhone.AutoIncrement = false;
                colvarAddresseePhone.IsNullable = true;
                colvarAddresseePhone.IsPrimaryKey = false;
                colvarAddresseePhone.IsForeignKey = false;
                colvarAddresseePhone.IsReadOnly = false;
                
                schema.Columns.Add(colvarAddresseePhone);
                
                TableSchema.TableColumn colvarItemQuantity = new TableSchema.TableColumn(schema);
                colvarItemQuantity.ColumnName = "item_quantity";
                colvarItemQuantity.DataType = DbType.Int32;
                colvarItemQuantity.MaxLength = 0;
                colvarItemQuantity.AutoIncrement = false;
                colvarItemQuantity.IsNullable = true;
                colvarItemQuantity.IsPrimaryKey = false;
                colvarItemQuantity.IsForeignKey = false;
                colvarItemQuantity.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemQuantity);
                
                TableSchema.TableColumn colvarDeliveryAddress = new TableSchema.TableColumn(schema);
                colvarDeliveryAddress.ColumnName = "delivery_address";
                colvarDeliveryAddress.DataType = DbType.String;
                colvarDeliveryAddress.MaxLength = 200;
                colvarDeliveryAddress.AutoIncrement = false;
                colvarDeliveryAddress.IsNullable = true;
                colvarDeliveryAddress.IsPrimaryKey = false;
                colvarDeliveryAddress.IsForeignKey = false;
                colvarDeliveryAddress.IsReadOnly = false;
                
                schema.Columns.Add(colvarDeliveryAddress);
                
                TableSchema.TableColumn colvarOptionName1 = new TableSchema.TableColumn(schema);
                colvarOptionName1.ColumnName = "option_name1";
                colvarOptionName1.DataType = DbType.String;
                colvarOptionName1.MaxLength = 50;
                colvarOptionName1.AutoIncrement = false;
                colvarOptionName1.IsNullable = true;
                colvarOptionName1.IsPrimaryKey = false;
                colvarOptionName1.IsForeignKey = false;
                colvarOptionName1.IsReadOnly = false;
                
                schema.Columns.Add(colvarOptionName1);
                
                TableSchema.TableColumn colvarCatgName1 = new TableSchema.TableColumn(schema);
                colvarCatgName1.ColumnName = "catg_name1";
                colvarCatgName1.DataType = DbType.String;
                colvarCatgName1.MaxLength = 25;
                colvarCatgName1.AutoIncrement = false;
                colvarCatgName1.IsNullable = true;
                colvarCatgName1.IsPrimaryKey = false;
                colvarCatgName1.IsForeignKey = false;
                colvarCatgName1.IsReadOnly = false;
                
                schema.Columns.Add(colvarCatgName1);
                
                TableSchema.TableColumn colvarOptionName2 = new TableSchema.TableColumn(schema);
                colvarOptionName2.ColumnName = "option_name2";
                colvarOptionName2.DataType = DbType.String;
                colvarOptionName2.MaxLength = 50;
                colvarOptionName2.AutoIncrement = false;
                colvarOptionName2.IsNullable = true;
                colvarOptionName2.IsPrimaryKey = false;
                colvarOptionName2.IsForeignKey = false;
                colvarOptionName2.IsReadOnly = false;
                
                schema.Columns.Add(colvarOptionName2);
                
                TableSchema.TableColumn colvarCatgName2 = new TableSchema.TableColumn(schema);
                colvarCatgName2.ColumnName = "catg_name2";
                colvarCatgName2.DataType = DbType.String;
                colvarCatgName2.MaxLength = 25;
                colvarCatgName2.AutoIncrement = false;
                colvarCatgName2.IsNullable = true;
                colvarCatgName2.IsPrimaryKey = false;
                colvarCatgName2.IsForeignKey = false;
                colvarCatgName2.IsReadOnly = false;
                
                schema.Columns.Add(colvarCatgName2);
                
                TableSchema.TableColumn colvarOptionName3 = new TableSchema.TableColumn(schema);
                colvarOptionName3.ColumnName = "option_name3";
                colvarOptionName3.DataType = DbType.String;
                colvarOptionName3.MaxLength = 50;
                colvarOptionName3.AutoIncrement = false;
                colvarOptionName3.IsNullable = true;
                colvarOptionName3.IsPrimaryKey = false;
                colvarOptionName3.IsForeignKey = false;
                colvarOptionName3.IsReadOnly = false;
                
                schema.Columns.Add(colvarOptionName3);
                
                TableSchema.TableColumn colvarCatgName3 = new TableSchema.TableColumn(schema);
                colvarCatgName3.ColumnName = "catg_name3";
                colvarCatgName3.DataType = DbType.String;
                colvarCatgName3.MaxLength = 25;
                colvarCatgName3.AutoIncrement = false;
                colvarCatgName3.IsNullable = true;
                colvarCatgName3.IsPrimaryKey = false;
                colvarCatgName3.IsForeignKey = false;
                colvarCatgName3.IsReadOnly = false;
                
                schema.Columns.Add(colvarCatgName3);
                
                TableSchema.TableColumn colvarOptionName4 = new TableSchema.TableColumn(schema);
                colvarOptionName4.ColumnName = "option_name4";
                colvarOptionName4.DataType = DbType.String;
                colvarOptionName4.MaxLength = 50;
                colvarOptionName4.AutoIncrement = false;
                colvarOptionName4.IsNullable = true;
                colvarOptionName4.IsPrimaryKey = false;
                colvarOptionName4.IsForeignKey = false;
                colvarOptionName4.IsReadOnly = false;
                
                schema.Columns.Add(colvarOptionName4);
                
                TableSchema.TableColumn colvarCatgName4 = new TableSchema.TableColumn(schema);
                colvarCatgName4.ColumnName = "catg_name4";
                colvarCatgName4.DataType = DbType.String;
                colvarCatgName4.MaxLength = 25;
                colvarCatgName4.AutoIncrement = false;
                colvarCatgName4.IsNullable = true;
                colvarCatgName4.IsPrimaryKey = false;
                colvarCatgName4.IsForeignKey = false;
                colvarCatgName4.IsReadOnly = false;
                
                schema.Columns.Add(colvarCatgName4);
                
                TableSchema.TableColumn colvarOptionName5 = new TableSchema.TableColumn(schema);
                colvarOptionName5.ColumnName = "option_name5";
                colvarOptionName5.DataType = DbType.String;
                colvarOptionName5.MaxLength = 50;
                colvarOptionName5.AutoIncrement = false;
                colvarOptionName5.IsNullable = true;
                colvarOptionName5.IsPrimaryKey = false;
                colvarOptionName5.IsForeignKey = false;
                colvarOptionName5.IsReadOnly = false;
                
                schema.Columns.Add(colvarOptionName5);
                
                TableSchema.TableColumn colvarCatgName5 = new TableSchema.TableColumn(schema);
                colvarCatgName5.ColumnName = "catg_name5";
                colvarCatgName5.DataType = DbType.String;
                colvarCatgName5.MaxLength = 25;
                colvarCatgName5.AutoIncrement = false;
                colvarCatgName5.IsNullable = true;
                colvarCatgName5.IsPrimaryKey = false;
                colvarCatgName5.IsForeignKey = false;
                colvarCatgName5.IsReadOnly = false;
                
                schema.Columns.Add(colvarCatgName5);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_hi_deal_product_house_info",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewHiDealProductHouseInfo()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewHiDealProductHouseInfo(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewHiDealProductHouseInfo(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewHiDealProductHouseInfo(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("OrderId")]
        [Bindable(true)]
        public string OrderId 
	    {
		    get
		    {
			    return GetColumnValue<string>("order_id");
		    }
            set 
		    {
			    SetColumnValue("order_id", value);
            }
        }
	      
        [XmlAttribute("ProductId")]
        [Bindable(true)]
        public int? ProductId 
	    {
		    get
		    {
			    return GetColumnValue<int?>("product_id");
		    }
            set 
		    {
			    SetColumnValue("product_id", value);
            }
        }
	      
        [XmlAttribute("DeliveryType")]
        [Bindable(true)]
        public int? DeliveryType 
	    {
		    get
		    {
			    return GetColumnValue<int?>("delivery_type");
		    }
            set 
		    {
			    SetColumnValue("delivery_type", value);
            }
        }
	      
        [XmlAttribute("ProductName")]
        [Bindable(true)]
        public string ProductName 
	    {
		    get
		    {
			    return GetColumnValue<string>("product_name");
		    }
            set 
		    {
			    SetColumnValue("product_name", value);
            }
        }
	      
        [XmlAttribute("Name")]
        [Bindable(true)]
        public string Name 
	    {
		    get
		    {
			    return GetColumnValue<string>("name");
		    }
            set 
		    {
			    SetColumnValue("name", value);
            }
        }
	      
        [XmlAttribute("AddresseeName")]
        [Bindable(true)]
        public string AddresseeName 
	    {
		    get
		    {
			    return GetColumnValue<string>("addressee_name");
		    }
            set 
		    {
			    SetColumnValue("addressee_name", value);
            }
        }
	      
        [XmlAttribute("AddresseePhone")]
        [Bindable(true)]
        public string AddresseePhone 
	    {
		    get
		    {
			    return GetColumnValue<string>("addressee_phone");
		    }
            set 
		    {
			    SetColumnValue("addressee_phone", value);
            }
        }
	      
        [XmlAttribute("ItemQuantity")]
        [Bindable(true)]
        public int? ItemQuantity 
	    {
		    get
		    {
			    return GetColumnValue<int?>("item_quantity");
		    }
            set 
		    {
			    SetColumnValue("item_quantity", value);
            }
        }
	      
        [XmlAttribute("DeliveryAddress")]
        [Bindable(true)]
        public string DeliveryAddress 
	    {
		    get
		    {
			    return GetColumnValue<string>("delivery_address");
		    }
            set 
		    {
			    SetColumnValue("delivery_address", value);
            }
        }
	      
        [XmlAttribute("OptionName1")]
        [Bindable(true)]
        public string OptionName1 
	    {
		    get
		    {
			    return GetColumnValue<string>("option_name1");
		    }
            set 
		    {
			    SetColumnValue("option_name1", value);
            }
        }
	      
        [XmlAttribute("CatgName1")]
        [Bindable(true)]
        public string CatgName1 
	    {
		    get
		    {
			    return GetColumnValue<string>("catg_name1");
		    }
            set 
		    {
			    SetColumnValue("catg_name1", value);
            }
        }
	      
        [XmlAttribute("OptionName2")]
        [Bindable(true)]
        public string OptionName2 
	    {
		    get
		    {
			    return GetColumnValue<string>("option_name2");
		    }
            set 
		    {
			    SetColumnValue("option_name2", value);
            }
        }
	      
        [XmlAttribute("CatgName2")]
        [Bindable(true)]
        public string CatgName2 
	    {
		    get
		    {
			    return GetColumnValue<string>("catg_name2");
		    }
            set 
		    {
			    SetColumnValue("catg_name2", value);
            }
        }
	      
        [XmlAttribute("OptionName3")]
        [Bindable(true)]
        public string OptionName3 
	    {
		    get
		    {
			    return GetColumnValue<string>("option_name3");
		    }
            set 
		    {
			    SetColumnValue("option_name3", value);
            }
        }
	      
        [XmlAttribute("CatgName3")]
        [Bindable(true)]
        public string CatgName3 
	    {
		    get
		    {
			    return GetColumnValue<string>("catg_name3");
		    }
            set 
		    {
			    SetColumnValue("catg_name3", value);
            }
        }
	      
        [XmlAttribute("OptionName4")]
        [Bindable(true)]
        public string OptionName4 
	    {
		    get
		    {
			    return GetColumnValue<string>("option_name4");
		    }
            set 
		    {
			    SetColumnValue("option_name4", value);
            }
        }
	      
        [XmlAttribute("CatgName4")]
        [Bindable(true)]
        public string CatgName4 
	    {
		    get
		    {
			    return GetColumnValue<string>("catg_name4");
		    }
            set 
		    {
			    SetColumnValue("catg_name4", value);
            }
        }
	      
        [XmlAttribute("OptionName5")]
        [Bindable(true)]
        public string OptionName5 
	    {
		    get
		    {
			    return GetColumnValue<string>("option_name5");
		    }
            set 
		    {
			    SetColumnValue("option_name5", value);
            }
        }
	      
        [XmlAttribute("CatgName5")]
        [Bindable(true)]
        public string CatgName5 
	    {
		    get
		    {
			    return GetColumnValue<string>("catg_name5");
		    }
            set 
		    {
			    SetColumnValue("catg_name5", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string OrderId = @"order_id";
            
            public static string ProductId = @"product_id";
            
            public static string DeliveryType = @"delivery_type";
            
            public static string ProductName = @"product_name";
            
            public static string Name = @"name";
            
            public static string AddresseeName = @"addressee_name";
            
            public static string AddresseePhone = @"addressee_phone";
            
            public static string ItemQuantity = @"item_quantity";
            
            public static string DeliveryAddress = @"delivery_address";
            
            public static string OptionName1 = @"option_name1";
            
            public static string CatgName1 = @"catg_name1";
            
            public static string OptionName2 = @"option_name2";
            
            public static string CatgName2 = @"catg_name2";
            
            public static string OptionName3 = @"option_name3";
            
            public static string CatgName3 = @"catg_name3";
            
            public static string OptionName4 = @"option_name4";
            
            public static string CatgName4 = @"catg_name4";
            
            public static string OptionName5 = @"option_name5";
            
            public static string CatgName5 = @"catg_name5";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

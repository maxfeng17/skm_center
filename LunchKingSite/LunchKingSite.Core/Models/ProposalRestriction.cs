using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the ProposalRestriction class.
	/// </summary>
    [Serializable]
	public partial class ProposalRestrictionCollection : RepositoryList<ProposalRestriction, ProposalRestrictionCollection>
	{	   
		public ProposalRestrictionCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>ProposalRestrictionCollection</returns>
		public ProposalRestrictionCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                ProposalRestriction o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the proposal_restriction table.
	/// </summary>
	[Serializable]
	public partial class ProposalRestriction : RepositoryRecord<ProposalRestriction>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public ProposalRestriction()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public ProposalRestriction(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("proposal_restriction", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarDeliveryType = new TableSchema.TableColumn(schema);
				colvarDeliveryType.ColumnName = "delivery_type";
				colvarDeliveryType.DataType = DbType.Int32;
				colvarDeliveryType.MaxLength = 0;
				colvarDeliveryType.AutoIncrement = false;
				colvarDeliveryType.IsNullable = false;
				colvarDeliveryType.IsPrimaryKey = false;
				colvarDeliveryType.IsForeignKey = false;
				colvarDeliveryType.IsReadOnly = false;
				colvarDeliveryType.DefaultSetting = @"";
				colvarDeliveryType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDeliveryType);
				
				TableSchema.TableColumn colvarParentCodeId = new TableSchema.TableColumn(schema);
				colvarParentCodeId.ColumnName = "parent_code_id";
				colvarParentCodeId.DataType = DbType.Int32;
				colvarParentCodeId.MaxLength = 0;
				colvarParentCodeId.AutoIncrement = false;
				colvarParentCodeId.IsNullable = false;
				colvarParentCodeId.IsPrimaryKey = false;
				colvarParentCodeId.IsForeignKey = false;
				colvarParentCodeId.IsReadOnly = false;
				colvarParentCodeId.DefaultSetting = @"";
				colvarParentCodeId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarParentCodeId);
				
				TableSchema.TableColumn colvarCodeId = new TableSchema.TableColumn(schema);
				colvarCodeId.ColumnName = "code_id";
				colvarCodeId.DataType = DbType.Int32;
				colvarCodeId.MaxLength = 0;
				colvarCodeId.AutoIncrement = false;
				colvarCodeId.IsNullable = false;
				colvarCodeId.IsPrimaryKey = false;
				colvarCodeId.IsForeignKey = false;
				colvarCodeId.IsReadOnly = false;
				colvarCodeId.DefaultSetting = @"";
				colvarCodeId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCodeId);
				
				TableSchema.TableColumn colvarGrossMarginRange = new TableSchema.TableColumn(schema);
				colvarGrossMarginRange.ColumnName = "gross_margin_range";
				colvarGrossMarginRange.DataType = DbType.Int32;
				colvarGrossMarginRange.MaxLength = 0;
				colvarGrossMarginRange.AutoIncrement = false;
				colvarGrossMarginRange.IsNullable = true;
				colvarGrossMarginRange.IsPrimaryKey = false;
				colvarGrossMarginRange.IsForeignKey = false;
				colvarGrossMarginRange.IsReadOnly = false;
				colvarGrossMarginRange.DefaultSetting = @"";
				colvarGrossMarginRange.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGrossMarginRange);
				
				TableSchema.TableColumn colvarGrossMarginNumber = new TableSchema.TableColumn(schema);
				colvarGrossMarginNumber.ColumnName = "gross_margin_number";
				colvarGrossMarginNumber.DataType = DbType.Int32;
				colvarGrossMarginNumber.MaxLength = 0;
				colvarGrossMarginNumber.AutoIncrement = false;
				colvarGrossMarginNumber.IsNullable = true;
				colvarGrossMarginNumber.IsPrimaryKey = false;
				colvarGrossMarginNumber.IsForeignKey = false;
				colvarGrossMarginNumber.IsReadOnly = false;
				colvarGrossMarginNumber.DefaultSetting = @"";
				colvarGrossMarginNumber.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGrossMarginNumber);
				
				TableSchema.TableColumn colvarCostPercentRange = new TableSchema.TableColumn(schema);
				colvarCostPercentRange.ColumnName = "cost_percent_range";
				colvarCostPercentRange.DataType = DbType.Int32;
				colvarCostPercentRange.MaxLength = 0;
				colvarCostPercentRange.AutoIncrement = false;
				colvarCostPercentRange.IsNullable = true;
				colvarCostPercentRange.IsPrimaryKey = false;
				colvarCostPercentRange.IsForeignKey = false;
				colvarCostPercentRange.IsReadOnly = false;
				colvarCostPercentRange.DefaultSetting = @"";
				colvarCostPercentRange.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCostPercentRange);
				
				TableSchema.TableColumn colvarCostPercentNumber = new TableSchema.TableColumn(schema);
				colvarCostPercentNumber.ColumnName = "cost_percent_number";
				colvarCostPercentNumber.DataType = DbType.Int32;
				colvarCostPercentNumber.MaxLength = 0;
				colvarCostPercentNumber.AutoIncrement = false;
				colvarCostPercentNumber.IsNullable = true;
				colvarCostPercentNumber.IsPrimaryKey = false;
				colvarCostPercentNumber.IsForeignKey = false;
				colvarCostPercentNumber.IsReadOnly = false;
				colvarCostPercentNumber.DefaultSetting = @"";
				colvarCostPercentNumber.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCostPercentNumber);
				
				TableSchema.TableColumn colvarEnable = new TableSchema.TableColumn(schema);
				colvarEnable.ColumnName = "enable";
				colvarEnable.DataType = DbType.Boolean;
				colvarEnable.MaxLength = 0;
				colvarEnable.AutoIncrement = false;
				colvarEnable.IsNullable = false;
				colvarEnable.IsPrimaryKey = false;
				colvarEnable.IsForeignKey = false;
				colvarEnable.IsReadOnly = false;
				colvarEnable.DefaultSetting = @"";
				colvarEnable.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEnable);
				
				TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
				colvarCreateId.ColumnName = "create_id";
				colvarCreateId.DataType = DbType.String;
				colvarCreateId.MaxLength = 50;
				colvarCreateId.AutoIncrement = false;
				colvarCreateId.IsNullable = false;
				colvarCreateId.IsPrimaryKey = false;
				colvarCreateId.IsForeignKey = false;
				colvarCreateId.IsReadOnly = false;
				colvarCreateId.DefaultSetting = @"";
				colvarCreateId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateId);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarModifyId = new TableSchema.TableColumn(schema);
				colvarModifyId.ColumnName = "modify_id";
				colvarModifyId.DataType = DbType.String;
				colvarModifyId.MaxLength = 50;
				colvarModifyId.AutoIncrement = false;
				colvarModifyId.IsNullable = true;
				colvarModifyId.IsPrimaryKey = false;
				colvarModifyId.IsForeignKey = false;
				colvarModifyId.IsReadOnly = false;
				colvarModifyId.DefaultSetting = @"";
				colvarModifyId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyId);
				
				TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
				colvarModifyTime.ColumnName = "modify_time";
				colvarModifyTime.DataType = DbType.DateTime;
				colvarModifyTime.MaxLength = 0;
				colvarModifyTime.AutoIncrement = false;
				colvarModifyTime.IsNullable = true;
				colvarModifyTime.IsPrimaryKey = false;
				colvarModifyTime.IsForeignKey = false;
				colvarModifyTime.IsReadOnly = false;
				colvarModifyTime.DefaultSetting = @"";
				colvarModifyTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyTime);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("proposal_restriction",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("DeliveryType")]
		[Bindable(true)]
		public int DeliveryType 
		{
			get { return GetColumnValue<int>(Columns.DeliveryType); }
			set { SetColumnValue(Columns.DeliveryType, value); }
		}
		  
		[XmlAttribute("ParentCodeId")]
		[Bindable(true)]
		public int ParentCodeId 
		{
			get { return GetColumnValue<int>(Columns.ParentCodeId); }
			set { SetColumnValue(Columns.ParentCodeId, value); }
		}
		  
		[XmlAttribute("CodeId")]
		[Bindable(true)]
		public int CodeId 
		{
			get { return GetColumnValue<int>(Columns.CodeId); }
			set { SetColumnValue(Columns.CodeId, value); }
		}
		  
		[XmlAttribute("GrossMarginRange")]
		[Bindable(true)]
		public int? GrossMarginRange 
		{
			get { return GetColumnValue<int?>(Columns.GrossMarginRange); }
			set { SetColumnValue(Columns.GrossMarginRange, value); }
		}
		  
		[XmlAttribute("GrossMarginNumber")]
		[Bindable(true)]
		public int? GrossMarginNumber 
		{
			get { return GetColumnValue<int?>(Columns.GrossMarginNumber); }
			set { SetColumnValue(Columns.GrossMarginNumber, value); }
		}
		  
		[XmlAttribute("CostPercentRange")]
		[Bindable(true)]
		public int? CostPercentRange 
		{
			get { return GetColumnValue<int?>(Columns.CostPercentRange); }
			set { SetColumnValue(Columns.CostPercentRange, value); }
		}
		  
		[XmlAttribute("CostPercentNumber")]
		[Bindable(true)]
		public int? CostPercentNumber 
		{
			get { return GetColumnValue<int?>(Columns.CostPercentNumber); }
			set { SetColumnValue(Columns.CostPercentNumber, value); }
		}
		  
		[XmlAttribute("Enable")]
		[Bindable(true)]
		public bool Enable 
		{
			get { return GetColumnValue<bool>(Columns.Enable); }
			set { SetColumnValue(Columns.Enable, value); }
		}
		  
		[XmlAttribute("CreateId")]
		[Bindable(true)]
		public string CreateId 
		{
			get { return GetColumnValue<string>(Columns.CreateId); }
			set { SetColumnValue(Columns.CreateId, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		  
		[XmlAttribute("ModifyId")]
		[Bindable(true)]
		public string ModifyId 
		{
			get { return GetColumnValue<string>(Columns.ModifyId); }
			set { SetColumnValue(Columns.ModifyId, value); }
		}
		  
		[XmlAttribute("ModifyTime")]
		[Bindable(true)]
		public DateTime? ModifyTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.ModifyTime); }
			set { SetColumnValue(Columns.ModifyTime, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn DeliveryTypeColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn ParentCodeIdColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn CodeIdColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn GrossMarginRangeColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn GrossMarginNumberColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn CostPercentRangeColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn CostPercentNumberColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn EnableColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyIdColumn
        {
            get { return Schema.Columns[11]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[12]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string DeliveryType = @"delivery_type";
			 public static string ParentCodeId = @"parent_code_id";
			 public static string CodeId = @"code_id";
			 public static string GrossMarginRange = @"gross_margin_range";
			 public static string GrossMarginNumber = @"gross_margin_number";
			 public static string CostPercentRange = @"cost_percent_range";
			 public static string CostPercentNumber = @"cost_percent_number";
			 public static string Enable = @"enable";
			 public static string CreateId = @"create_id";
			 public static string CreateTime = @"create_time";
			 public static string ModifyId = @"modify_id";
			 public static string ModifyTime = @"modify_time";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

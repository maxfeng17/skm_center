using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the MemberFastInfo class.
    /// </summary>
    [Serializable]
    public partial class MemberFastInfoCollection : RepositoryList<MemberFastInfo, MemberFastInfoCollection>
    {
        public MemberFastInfoCollection() { }

        /// <summary>
        /// Filters an existing collection based on the set criteria. This is an in-memory filter
        /// Thanks to developingchris for this!
        /// </summary>
        /// <returns>MemberFastInfoCollection</returns>
        public MemberFastInfoCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                MemberFastInfo o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }


    }
    /// <summary>
    /// This is an ActiveRecord class which wraps the member_fast_info table.
    /// </summary>
    [Serializable]
    public partial class MemberFastInfo : RepositoryRecord<MemberFastInfo>, IRecordBase
    {
        #region .ctors and Default Settings

        public MemberFastInfo()
        {
            SetSQLProps();
            InitSetDefaults();
            MarkNew();
        }

        private void InitSetDefaults() { SetDefaults(); }

        public MemberFastInfo(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
                ForceDefaults();
            MarkNew();
        }

        protected static void SetSQLProps() { GetTableSchema(); }

        #endregion

        #region Schema and Query Accessor	
        public static Query CreateQuery() { return new Query(Schema); }
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                    SetSQLProps();
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("member_fast_info", TableType.Table, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = true;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = true;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                colvarId.DefaultSetting = @"";
                colvarId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarId);

                TableSchema.TableColumn colvarUserId = new TableSchema.TableColumn(schema);
                colvarUserId.ColumnName = "user_id";
                colvarUserId.DataType = DbType.Int32;
                colvarUserId.MaxLength = 0;
                colvarUserId.AutoIncrement = false;
                colvarUserId.IsNullable = true;
                colvarUserId.IsPrimaryKey = false;
                colvarUserId.IsForeignKey = false;
                colvarUserId.IsReadOnly = false;
                colvarUserId.DefaultSetting = @"";
                colvarUserId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarUserId);

                TableSchema.TableColumn colvarGiftCardCount = new TableSchema.TableColumn(schema);
                colvarGiftCardCount.ColumnName = "gift_card_count";
                colvarGiftCardCount.DataType = DbType.Int32;
                colvarGiftCardCount.MaxLength = 0;
                colvarGiftCardCount.AutoIncrement = false;
                colvarGiftCardCount.IsNullable = true;
                colvarGiftCardCount.IsPrimaryKey = false;
                colvarGiftCardCount.IsForeignKey = false;
                colvarGiftCardCount.IsReadOnly = false;
                colvarGiftCardCount.DefaultSetting = @"";
                colvarGiftCardCount.ForeignKeyTableName = "";
                schema.Columns.Add(colvarGiftCardCount);

                TableSchema.TableColumn colvarReplyCardCount = new TableSchema.TableColumn(schema);
                colvarReplyCardCount.ColumnName = "reply_card_count";
                colvarReplyCardCount.DataType = DbType.Int32;
                colvarReplyCardCount.MaxLength = 0;
                colvarReplyCardCount.AutoIncrement = false;
                colvarReplyCardCount.IsNullable = true;
                colvarReplyCardCount.IsPrimaryKey = false;
                colvarReplyCardCount.IsForeignKey = false;
                colvarReplyCardCount.IsReadOnly = false;
                colvarReplyCardCount.DefaultSetting = @"";
                colvarReplyCardCount.ForeignKeyTableName = "";
                schema.Columns.Add(colvarReplyCardCount);

                TableSchema.TableColumn colvarGiftCardCountReload = new TableSchema.TableColumn(schema);
                colvarGiftCardCountReload.ColumnName = "gift_card_count_reload";
                colvarGiftCardCountReload.DataType = DbType.Boolean;
                colvarGiftCardCountReload.MaxLength = 0;
                colvarGiftCardCountReload.AutoIncrement = false;
                colvarGiftCardCountReload.IsNullable = true;
                colvarGiftCardCountReload.IsPrimaryKey = false;
                colvarGiftCardCountReload.IsForeignKey = false;
                colvarGiftCardCountReload.IsReadOnly = false;
                colvarGiftCardCountReload.DefaultSetting = @"";
                colvarGiftCardCountReload.ForeignKeyTableName = "";
                schema.Columns.Add(colvarGiftCardCountReload);

                TableSchema.TableColumn colvarReplyCardCountReload = new TableSchema.TableColumn(schema);
                colvarReplyCardCountReload.ColumnName = "reply_card_count_reload";
                colvarReplyCardCountReload.DataType = DbType.Boolean;
                colvarReplyCardCountReload.MaxLength = 0;
                colvarReplyCardCountReload.AutoIncrement = false;
                colvarReplyCardCountReload.IsNullable = true;
                colvarReplyCardCountReload.IsPrimaryKey = false;
                colvarReplyCardCountReload.IsForeignKey = false;
                colvarReplyCardCountReload.IsReadOnly = false;
                colvarReplyCardCountReload.DefaultSetting = @"";
                colvarReplyCardCountReload.ForeignKeyTableName = "";
                schema.Columns.Add(colvarReplyCardCountReload);

                TableSchema.TableColumn colvarMessageCount = new TableSchema.TableColumn(schema);
                colvarMessageCount.ColumnName = "message_count";
                colvarMessageCount.DataType = DbType.Int32;
                colvarMessageCount.MaxLength = 0;
                colvarMessageCount.AutoIncrement = false;
                colvarMessageCount.IsNullable = false;
                colvarMessageCount.IsPrimaryKey = false;
                colvarMessageCount.IsForeignKey = false;
                colvarMessageCount.IsReadOnly = false;

                colvarMessageCount.DefaultSetting = @"((0))";
                colvarMessageCount.ForeignKeyTableName = "";
                schema.Columns.Add(colvarMessageCount);

                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("member_fast_info", schema);
            }
        }
        #endregion

        #region Props

        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id
        {
            get { return GetColumnValue<int>(Columns.Id); }
            set { SetColumnValue(Columns.Id, value); }
        }

        [XmlAttribute("UserId")]
        [Bindable(true)]
        public int? UserId
        {
            get { return GetColumnValue<int?>(Columns.UserId); }
            set { SetColumnValue(Columns.UserId, value); }
        }

        [XmlAttribute("GiftCardCount")]
        [Bindable(true)]
        public int? GiftCardCount
        {
            get { return GetColumnValue<int?>(Columns.GiftCardCount); }
            set { SetColumnValue(Columns.GiftCardCount, value); }
        }

        [XmlAttribute("ReplyCardCount")]
        [Bindable(true)]
        public int? ReplyCardCount
        {
            get { return GetColumnValue<int?>(Columns.ReplyCardCount); }
            set { SetColumnValue(Columns.ReplyCardCount, value); }
        }

        [XmlAttribute("GiftCardCountReload")]
        [Bindable(true)]
        public bool? GiftCardCountReload
        {
            get { return GetColumnValue<bool?>(Columns.GiftCardCountReload); }
            set { SetColumnValue(Columns.GiftCardCountReload, value); }
        }

        [XmlAttribute("ReplyCardCountReload")]
        [Bindable(true)]
        public bool? ReplyCardCountReload
        {
            get { return GetColumnValue<bool?>(Columns.ReplyCardCountReload); }
            set { SetColumnValue(Columns.ReplyCardCountReload, value); }
        }

        [XmlAttribute("MessageCount")]
        [Bindable(true)]
        public int MessageCount
        {
            get { return GetColumnValue<int>(Columns.MessageCount); }
            set { SetColumnValue(Columns.MessageCount, value); }
        }

        #endregion




        //no foreign key tables defined (0)



        //no ManyToMany tables defined (0)





        #region Typed Columns


        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }



        public static TableSchema.TableColumn UserIdColumn
        {
            get { return Schema.Columns[1]; }
        }



        public static TableSchema.TableColumn GiftCardCountColumn
        {
            get { return Schema.Columns[2]; }
        }



        public static TableSchema.TableColumn ReplyCardCountColumn
        {
            get { return Schema.Columns[3]; }
        }



        public static TableSchema.TableColumn GiftCardCountReloadColumn
        {
            get { return Schema.Columns[4]; }
        }



        public static TableSchema.TableColumn ReplyCardCountReloadColumn
        {
            get { return Schema.Columns[5]; }
        }



        public static TableSchema.TableColumn MessageCountColumn
        {
            get { return Schema.Columns[6]; }
        }



        #endregion
        #region Columns Struct
        public struct Columns
        {
            public static string Id = @"id";
            public static string UserId = @"user_id";
            public static string GiftCardCount = @"gift_card_count";
            public static string ReplyCardCount = @"reply_card_count";
            public static string GiftCardCountReload = @"gift_card_count_reload";
            public static string ReplyCardCountReload = @"reply_card_count_reload";
            public static string MessageCount = @"message_count";

        }
        #endregion

        #region Update PK Collections

        #endregion

        #region Deep Save

        #endregion
    }
}

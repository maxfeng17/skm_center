using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the FreewifiUseLog class.
	/// </summary>
    [Serializable]
	public partial class FreewifiUseLogCollection : RepositoryList<FreewifiUseLog, FreewifiUseLogCollection>
	{	   
		public FreewifiUseLogCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>FreewifiUseLogCollection</returns>
		public FreewifiUseLogCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                FreewifiUseLog o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the freewifi_use_log table.
	/// </summary>
	[Serializable]
	public partial class FreewifiUseLog : RepositoryRecord<FreewifiUseLog>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public FreewifiUseLog()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public FreewifiUseLog(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("freewifi_use_log", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarMacAddress = new TableSchema.TableColumn(schema);
				colvarMacAddress.ColumnName = "mac_address";
				colvarMacAddress.DataType = DbType.String;
				colvarMacAddress.MaxLength = 20;
				colvarMacAddress.AutoIncrement = false;
				colvarMacAddress.IsNullable = true;
				colvarMacAddress.IsPrimaryKey = false;
				colvarMacAddress.IsForeignKey = false;
				colvarMacAddress.IsReadOnly = false;
				colvarMacAddress.DefaultSetting = @"";
				colvarMacAddress.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMacAddress);
				
				TableSchema.TableColumn colvarWifiType = new TableSchema.TableColumn(schema);
				colvarWifiType.ColumnName = "wifi_type";
				colvarWifiType.DataType = DbType.Int32;
				colvarWifiType.MaxLength = 0;
				colvarWifiType.AutoIncrement = false;
				colvarWifiType.IsNullable = true;
				colvarWifiType.IsPrimaryKey = false;
				colvarWifiType.IsForeignKey = false;
				colvarWifiType.IsReadOnly = false;
				colvarWifiType.DefaultSetting = @"";
				colvarWifiType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarWifiType);
				
				TableSchema.TableColumn colvarUseTime = new TableSchema.TableColumn(schema);
				colvarUseTime.ColumnName = "use_time";
				colvarUseTime.DataType = DbType.Int32;
				colvarUseTime.MaxLength = 0;
				colvarUseTime.AutoIncrement = false;
				colvarUseTime.IsNullable = true;
				colvarUseTime.IsPrimaryKey = false;
				colvarUseTime.IsForeignKey = false;
				colvarUseTime.IsReadOnly = false;
				colvarUseTime.DefaultSetting = @"";
				colvarUseTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUseTime);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = true;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("freewifi_use_log",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("MacAddress")]
		[Bindable(true)]
		public string MacAddress 
		{
			get { return GetColumnValue<string>(Columns.MacAddress); }
			set { SetColumnValue(Columns.MacAddress, value); }
		}
		  
		[XmlAttribute("WifiType")]
		[Bindable(true)]
		public int? WifiType 
		{
			get { return GetColumnValue<int?>(Columns.WifiType); }
			set { SetColumnValue(Columns.WifiType, value); }
		}
		  
		[XmlAttribute("UseTime")]
		[Bindable(true)]
		public int? UseTime 
		{
			get { return GetColumnValue<int?>(Columns.UseTime); }
			set { SetColumnValue(Columns.UseTime, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime? CreateTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn MacAddressColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn WifiTypeColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn UseTimeColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string MacAddress = @"mac_address";
			 public static string WifiType = @"wifi_type";
			 public static string UseTime = @"use_time";
			 public static string CreateTime = @"create_time";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

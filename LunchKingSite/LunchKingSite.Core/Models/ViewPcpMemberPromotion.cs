using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the ViewPcpMemberPromotion class.
    /// </summary>
    [Serializable]
    public partial class ViewPcpMemberPromotionCollection : ReadOnlyList<ViewPcpMemberPromotion, ViewPcpMemberPromotionCollection>
    {        
        public ViewPcpMemberPromotionCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_pcp_member_promotion view.
    /// </summary>
    [Serializable]
    public partial class ViewPcpMemberPromotion : ReadOnlyRecord<ViewPcpMemberPromotion>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_pcp_member_promotion", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarUserMemCardId = new TableSchema.TableColumn(schema);
                colvarUserMemCardId.ColumnName = "user_mem_card_id";
                colvarUserMemCardId.DataType = DbType.Int32;
                colvarUserMemCardId.MaxLength = 0;
                colvarUserMemCardId.AutoIncrement = false;
                colvarUserMemCardId.IsNullable = false;
                colvarUserMemCardId.IsPrimaryKey = false;
                colvarUserMemCardId.IsForeignKey = false;
                colvarUserMemCardId.IsReadOnly = false;
                
                schema.Columns.Add(colvarUserMemCardId);
                
                TableSchema.TableColumn colvarDepositId = new TableSchema.TableColumn(schema);
                colvarDepositId.ColumnName = "deposit_id";
                colvarDepositId.DataType = DbType.Int32;
                colvarDepositId.MaxLength = 0;
                colvarDepositId.AutoIncrement = false;
                colvarDepositId.IsNullable = false;
                colvarDepositId.IsPrimaryKey = false;
                colvarDepositId.IsForeignKey = false;
                colvarDepositId.IsReadOnly = false;
                
                schema.Columns.Add(colvarDepositId);
                
                TableSchema.TableColumn colvarWithdrawalId = new TableSchema.TableColumn(schema);
                colvarWithdrawalId.ColumnName = "withdrawal_id";
                colvarWithdrawalId.DataType = DbType.Int32;
                colvarWithdrawalId.MaxLength = 0;
                colvarWithdrawalId.AutoIncrement = false;
                colvarWithdrawalId.IsNullable = false;
                colvarWithdrawalId.IsPrimaryKey = false;
                colvarWithdrawalId.IsForeignKey = false;
                colvarWithdrawalId.IsReadOnly = false;
                
                schema.Columns.Add(colvarWithdrawalId);
                
                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = false;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarCreateTime);
                
                TableSchema.TableColumn colvarExpireTime = new TableSchema.TableColumn(schema);
                colvarExpireTime.ColumnName = "expire_time";
                colvarExpireTime.DataType = DbType.DateTime;
                colvarExpireTime.MaxLength = 0;
                colvarExpireTime.AutoIncrement = false;
                colvarExpireTime.IsNullable = true;
                colvarExpireTime.IsPrimaryKey = false;
                colvarExpireTime.IsForeignKey = false;
                colvarExpireTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarExpireTime);
                
                TableSchema.TableColumn colvarAction = new TableSchema.TableColumn(schema);
                colvarAction.ColumnName = "action";
                colvarAction.DataType = DbType.String;
                colvarAction.MaxLength = 200;
                colvarAction.AutoIncrement = false;
                colvarAction.IsNullable = true;
                colvarAction.IsPrimaryKey = false;
                colvarAction.IsForeignKey = false;
                colvarAction.IsReadOnly = false;
                
                schema.Columns.Add(colvarAction);
                
                TableSchema.TableColumn colvarValueIn = new TableSchema.TableColumn(schema);
                colvarValueIn.ColumnName = "value_in";
                colvarValueIn.DataType = DbType.Double;
                colvarValueIn.MaxLength = 0;
                colvarValueIn.AutoIncrement = false;
                colvarValueIn.IsNullable = false;
                colvarValueIn.IsPrimaryKey = false;
                colvarValueIn.IsForeignKey = false;
                colvarValueIn.IsReadOnly = false;
                
                schema.Columns.Add(colvarValueIn);
                
                TableSchema.TableColumn colvarValueOut = new TableSchema.TableColumn(schema);
                colvarValueOut.ColumnName = "value_out";
                colvarValueOut.DataType = DbType.Double;
                colvarValueOut.MaxLength = 0;
                colvarValueOut.AutoIncrement = false;
                colvarValueOut.IsNullable = false;
                colvarValueOut.IsPrimaryKey = false;
                colvarValueOut.IsForeignKey = false;
                colvarValueOut.IsReadOnly = false;
                
                schema.Columns.Add(colvarValueOut);
                
                TableSchema.TableColumn colvarTotal = new TableSchema.TableColumn(schema);
                colvarTotal.ColumnName = "total";
                colvarTotal.DataType = DbType.Double;
                colvarTotal.MaxLength = 0;
                colvarTotal.AutoIncrement = false;
                colvarTotal.IsNullable = true;
                colvarTotal.IsPrimaryKey = false;
                colvarTotal.IsForeignKey = false;
                colvarTotal.IsReadOnly = false;
                
                schema.Columns.Add(colvarTotal);
                
                TableSchema.TableColumn colvarOrderGuid = new TableSchema.TableColumn(schema);
                colvarOrderGuid.ColumnName = "order_guid";
                colvarOrderGuid.DataType = DbType.Guid;
                colvarOrderGuid.MaxLength = 0;
                colvarOrderGuid.AutoIncrement = false;
                colvarOrderGuid.IsNullable = true;
                colvarOrderGuid.IsPrimaryKey = false;
                colvarOrderGuid.IsForeignKey = false;
                colvarOrderGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderGuid);
                
                TableSchema.TableColumn colvarOrderClassification = new TableSchema.TableColumn(schema);
                colvarOrderClassification.ColumnName = "order_classification";
                colvarOrderClassification.DataType = DbType.Int32;
                colvarOrderClassification.MaxLength = 0;
                colvarOrderClassification.AutoIncrement = false;
                colvarOrderClassification.IsNullable = true;
                colvarOrderClassification.IsPrimaryKey = false;
                colvarOrderClassification.IsForeignKey = false;
                colvarOrderClassification.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderClassification);
                
                TableSchema.TableColumn colvarAccountId = new TableSchema.TableColumn(schema);
                colvarAccountId.ColumnName = "account_id";
                colvarAccountId.DataType = DbType.String;
                colvarAccountId.MaxLength = 256;
                colvarAccountId.AutoIncrement = false;
                colvarAccountId.IsNullable = true;
                colvarAccountId.IsPrimaryKey = false;
                colvarAccountId.IsForeignKey = false;
                colvarAccountId.IsReadOnly = false;
                
                schema.Columns.Add(colvarAccountId);
                
                TableSchema.TableColumn colvarUserId = new TableSchema.TableColumn(schema);
                colvarUserId.ColumnName = "user_id";
                colvarUserId.DataType = DbType.Int32;
                colvarUserId.MaxLength = 0;
                colvarUserId.AutoIncrement = false;
                colvarUserId.IsNullable = true;
                colvarUserId.IsPrimaryKey = false;
                colvarUserId.IsForeignKey = false;
                colvarUserId.IsReadOnly = false;
                
                schema.Columns.Add(colvarUserId);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_pcp_member_promotion",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewPcpMemberPromotion()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewPcpMemberPromotion(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewPcpMemberPromotion(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewPcpMemberPromotion(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("UserMemCardId")]
        [Bindable(true)]
        public int UserMemCardId 
	    {
		    get
		    {
			    return GetColumnValue<int>("user_mem_card_id");
		    }
            set 
		    {
			    SetColumnValue("user_mem_card_id", value);
            }
        }
	      
        [XmlAttribute("DepositId")]
        [Bindable(true)]
        public int DepositId 
	    {
		    get
		    {
			    return GetColumnValue<int>("deposit_id");
		    }
            set 
		    {
			    SetColumnValue("deposit_id", value);
            }
        }
	      
        [XmlAttribute("WithdrawalId")]
        [Bindable(true)]
        public int WithdrawalId 
	    {
		    get
		    {
			    return GetColumnValue<int>("withdrawal_id");
		    }
            set 
		    {
			    SetColumnValue("withdrawal_id", value);
            }
        }
	      
        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime CreateTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("create_time");
		    }
            set 
		    {
			    SetColumnValue("create_time", value);
            }
        }
	      
        [XmlAttribute("ExpireTime")]
        [Bindable(true)]
        public DateTime? ExpireTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("expire_time");
		    }
            set 
		    {
			    SetColumnValue("expire_time", value);
            }
        }
	      
        [XmlAttribute("Action")]
        [Bindable(true)]
        public string Action 
	    {
		    get
		    {
			    return GetColumnValue<string>("action");
		    }
            set 
		    {
			    SetColumnValue("action", value);
            }
        }
	      
        [XmlAttribute("ValueIn")]
        [Bindable(true)]
        public double ValueIn 
	    {
		    get
		    {
			    return GetColumnValue<double>("value_in");
		    }
            set 
		    {
			    SetColumnValue("value_in", value);
            }
        }
	      
        [XmlAttribute("ValueOut")]
        [Bindable(true)]
        public double ValueOut 
	    {
		    get
		    {
			    return GetColumnValue<double>("value_out");
		    }
            set 
		    {
			    SetColumnValue("value_out", value);
            }
        }
	      
        [XmlAttribute("Total")]
        [Bindable(true)]
        public double? Total 
	    {
		    get
		    {
			    return GetColumnValue<double?>("total");
		    }
            set 
		    {
			    SetColumnValue("total", value);
            }
        }
	      
        [XmlAttribute("OrderGuid")]
        [Bindable(true)]
        public Guid? OrderGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid?>("order_guid");
		    }
            set 
		    {
			    SetColumnValue("order_guid", value);
            }
        }
	      
        [XmlAttribute("OrderClassification")]
        [Bindable(true)]
        public int? OrderClassification 
	    {
		    get
		    {
			    return GetColumnValue<int?>("order_classification");
		    }
            set 
		    {
			    SetColumnValue("order_classification", value);
            }
        }
	      
        [XmlAttribute("AccountId")]
        [Bindable(true)]
        public string AccountId 
	    {
		    get
		    {
			    return GetColumnValue<string>("account_id");
		    }
            set 
		    {
			    SetColumnValue("account_id", value);
            }
        }
	      
        [XmlAttribute("UserId")]
        [Bindable(true)]
        public int? UserId 
	    {
		    get
		    {
			    return GetColumnValue<int?>("user_id");
		    }
            set 
		    {
			    SetColumnValue("user_id", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string UserMemCardId = @"user_mem_card_id";
            
            public static string DepositId = @"deposit_id";
            
            public static string WithdrawalId = @"withdrawal_id";
            
            public static string CreateTime = @"create_time";
            
            public static string ExpireTime = @"expire_time";
            
            public static string Action = @"action";
            
            public static string ValueIn = @"value_in";
            
            public static string ValueOut = @"value_out";
            
            public static string Total = @"total";
            
            public static string OrderGuid = @"order_guid";
            
            public static string OrderClassification = @"order_classification";
            
            public static string AccountId = @"account_id";
            
            public static string UserId = @"user_id";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) 
        {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) 
        {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

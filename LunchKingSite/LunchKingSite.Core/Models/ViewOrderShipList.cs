using System;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using SubSonic;

namespace LunchKingSite.DataOrm
{
	[Serializable]
	public partial class ViewOrderShipListCollection : ReadOnlyList<ViewOrderShipList, ViewOrderShipListCollection>
	{
			public ViewOrderShipListCollection() {}

	}

	[Serializable]
	public partial class ViewOrderShipList : ReadOnlyRecord<ViewOrderShipList>, IReadOnlyRecord
	{
		#region .ctors and Default Settings
		public ViewOrderShipList()
		{
			SetSQLProps();
			InitSetDefaults();
			MarkNew();
		}

		private void InitSetDefaults() { SetDefaults(); }

		public ViewOrderShipList(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}

		protected static void SetSQLProps() { GetTableSchema(); }

		public ViewOrderShipList(object keyID)
		{
				SetSQLProps();
				LoadByKey(keyID);
		}

		public ViewOrderShipList(string columnName, object columnValue)
		{
				SetSQLProps();
				LoadByParam(columnName,columnValue);
		}
		#endregion

		#region Schema and Query Accessor
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}

		private static void GetTableSchema()
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("view_order_ship_list", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				TableSchema.TableColumn colvarProductGuid = new TableSchema.TableColumn(schema);
				colvarProductGuid.ColumnName = "product_guid";
				colvarProductGuid.DataType = DbType.Guid;
				colvarProductGuid.MaxLength = 0;
				colvarProductGuid.AutoIncrement = false;
				colvarProductGuid.IsNullable = false;
				colvarProductGuid.IsPrimaryKey = false;
				colvarProductGuid.IsForeignKey = false;
				colvarProductGuid.IsReadOnly = false;
				colvarProductGuid.DefaultSetting = @"";
				colvarProductGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarProductGuid);

				TableSchema.TableColumn colvarOrderGuid = new TableSchema.TableColumn(schema);
				colvarOrderGuid.ColumnName = "order_guid";
				colvarOrderGuid.DataType = DbType.Guid;
				colvarOrderGuid.MaxLength = 0;
				colvarOrderGuid.AutoIncrement = false;
				colvarOrderGuid.IsNullable = false;
				colvarOrderGuid.IsPrimaryKey = false;
				colvarOrderGuid.IsForeignKey = false;
				colvarOrderGuid.IsReadOnly = false;
				colvarOrderGuid.DefaultSetting = @"";
				colvarOrderGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderGuid);

				TableSchema.TableColumn colvarOrderId = new TableSchema.TableColumn(schema);
				colvarOrderId.ColumnName = "order_id";
				colvarOrderId.DataType = DbType.AnsiString;
				colvarOrderId.MaxLength = 30;
				colvarOrderId.AutoIncrement = false;
				colvarOrderId.IsNullable = true;
				colvarOrderId.IsPrimaryKey = false;
				colvarOrderId.IsForeignKey = false;
				colvarOrderId.IsReadOnly = false;
				colvarOrderId.DefaultSetting = @"";
				colvarOrderId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderId);

				TableSchema.TableColumn colvarMemberName = new TableSchema.TableColumn(schema);
				colvarMemberName.ColumnName = "member_name";
				colvarMemberName.DataType = DbType.String;
				colvarMemberName.MaxLength = 50;
				colvarMemberName.AutoIncrement = false;
				colvarMemberName.IsNullable = true;
				colvarMemberName.IsPrimaryKey = false;
				colvarMemberName.IsForeignKey = false;
				colvarMemberName.IsReadOnly = false;
				colvarMemberName.DefaultSetting = @"";
				colvarMemberName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMemberName);

				TableSchema.TableColumn colvarPhoneNumber = new TableSchema.TableColumn(schema);
				colvarPhoneNumber.ColumnName = "phone_number";
				colvarPhoneNumber.DataType = DbType.String;
				colvarPhoneNumber.MaxLength = 50;
				colvarPhoneNumber.AutoIncrement = false;
				colvarPhoneNumber.IsNullable = true;
				colvarPhoneNumber.IsPrimaryKey = false;
				colvarPhoneNumber.IsForeignKey = false;
				colvarPhoneNumber.IsReadOnly = false;
				colvarPhoneNumber.DefaultSetting = @"";
				colvarPhoneNumber.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPhoneNumber);

				TableSchema.TableColumn colvarDeliveryAddress = new TableSchema.TableColumn(schema);
				colvarDeliveryAddress.ColumnName = "delivery_address";
				colvarDeliveryAddress.DataType = DbType.String;
				colvarDeliveryAddress.MaxLength = 200;
				colvarDeliveryAddress.AutoIncrement = false;
				colvarDeliveryAddress.IsNullable = true;
				colvarDeliveryAddress.IsPrimaryKey = false;
				colvarDeliveryAddress.IsForeignKey = false;
				colvarDeliveryAddress.IsReadOnly = false;
				colvarDeliveryAddress.DefaultSetting = @"";
				colvarDeliveryAddress.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDeliveryAddress);

				TableSchema.TableColumn colvarUserMemo = new TableSchema.TableColumn(schema);
				colvarUserMemo.ColumnName = "user_memo";
				colvarUserMemo.DataType = DbType.String;
				colvarUserMemo.MaxLength = 1073741823;
				colvarUserMemo.AutoIncrement = false;
				colvarUserMemo.IsNullable = true;
				colvarUserMemo.IsPrimaryKey = false;
				colvarUserMemo.IsForeignKey = false;
				colvarUserMemo.IsReadOnly = false;
				colvarUserMemo.DefaultSetting = @"";
				colvarUserMemo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUserMemo);

				TableSchema.TableColumn colvarOrderShipId = new TableSchema.TableColumn(schema);
				colvarOrderShipId.ColumnName = "order_ship_id";
				colvarOrderShipId.DataType = DbType.Int32;
				colvarOrderShipId.MaxLength = 0;
				colvarOrderShipId.AutoIncrement = false;
				colvarOrderShipId.IsNullable = true;
				colvarOrderShipId.IsPrimaryKey = false;
				colvarOrderShipId.IsForeignKey = false;
				colvarOrderShipId.IsReadOnly = false;
				colvarOrderShipId.DefaultSetting = @"";
				colvarOrderShipId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderShipId);

				TableSchema.TableColumn colvarShipCompanyId = new TableSchema.TableColumn(schema);
				colvarShipCompanyId.ColumnName = "ship_company_id";
				colvarShipCompanyId.DataType = DbType.Int32;
				colvarShipCompanyId.MaxLength = 0;
				colvarShipCompanyId.AutoIncrement = false;
				colvarShipCompanyId.IsNullable = true;
				colvarShipCompanyId.IsPrimaryKey = false;
				colvarShipCompanyId.IsForeignKey = false;
				colvarShipCompanyId.IsReadOnly = false;
				colvarShipCompanyId.DefaultSetting = @"";
				colvarShipCompanyId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarShipCompanyId);

				TableSchema.TableColumn colvarShipTime = new TableSchema.TableColumn(schema);
				colvarShipTime.ColumnName = "ship_time";
				colvarShipTime.DataType = DbType.DateTime;
				colvarShipTime.MaxLength = 0;
				colvarShipTime.AutoIncrement = false;
				colvarShipTime.IsNullable = true;
				colvarShipTime.IsPrimaryKey = false;
				colvarShipTime.IsForeignKey = false;
				colvarShipTime.IsReadOnly = false;
				colvarShipTime.DefaultSetting = @"";
				colvarShipTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarShipTime);

				TableSchema.TableColumn colvarShipNo = new TableSchema.TableColumn(schema);
				colvarShipNo.ColumnName = "ship_no";
				colvarShipNo.DataType = DbType.String;
				colvarShipNo.MaxLength = 2147483647;
				colvarShipNo.AutoIncrement = false;
				colvarShipNo.IsNullable = true;
				colvarShipNo.IsPrimaryKey = false;
				colvarShipNo.IsForeignKey = false;
				colvarShipNo.IsReadOnly = false;
				colvarShipNo.DefaultSetting = @"";
				colvarShipNo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarShipNo);

				TableSchema.TableColumn colvarShipCompanyName = new TableSchema.TableColumn(schema);
				colvarShipCompanyName.ColumnName = "ship_company_name";
				colvarShipCompanyName.DataType = DbType.String;
				colvarShipCompanyName.MaxLength = 100;
				colvarShipCompanyName.AutoIncrement = false;
				colvarShipCompanyName.IsNullable = true;
				colvarShipCompanyName.IsPrimaryKey = false;
				colvarShipCompanyName.IsForeignKey = false;
				colvarShipCompanyName.IsReadOnly = false;
				colvarShipCompanyName.DefaultSetting = @"";
				colvarShipCompanyName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarShipCompanyName);

				TableSchema.TableColumn colvarShipWebsite = new TableSchema.TableColumn(schema);
				colvarShipWebsite.ColumnName = "ship_website";
				colvarShipWebsite.DataType = DbType.String;
				colvarShipWebsite.MaxLength = 300;
				colvarShipWebsite.AutoIncrement = false;
				colvarShipWebsite.IsNullable = true;
				colvarShipWebsite.IsPrimaryKey = false;
				colvarShipWebsite.IsForeignKey = false;
				colvarShipWebsite.IsReadOnly = false;
				colvarShipWebsite.DefaultSetting = @"";
				colvarShipWebsite.ForeignKeyTableName = "";
				schema.Columns.Add(colvarShipWebsite);

				TableSchema.TableColumn colvarServiceTel = new TableSchema.TableColumn(schema);
				colvarServiceTel.ColumnName = "service_tel";
				colvarServiceTel.DataType = DbType.String;
				colvarServiceTel.MaxLength = 100;
				colvarServiceTel.AutoIncrement = false;
				colvarServiceTel.IsNullable = true;
				colvarServiceTel.IsPrimaryKey = false;
				colvarServiceTel.IsForeignKey = false;
				colvarServiceTel.IsReadOnly = false;
				colvarServiceTel.DefaultSetting = @"";
				colvarServiceTel.ForeignKeyTableName = "";
				schema.Columns.Add(colvarServiceTel);

				TableSchema.TableColumn colvarOrderShipCreateTime = new TableSchema.TableColumn(schema);
				colvarOrderShipCreateTime.ColumnName = "order_ship_create_time";
				colvarOrderShipCreateTime.DataType = DbType.DateTime;
				colvarOrderShipCreateTime.MaxLength = 0;
				colvarOrderShipCreateTime.AutoIncrement = false;
				colvarOrderShipCreateTime.IsNullable = true;
				colvarOrderShipCreateTime.IsPrimaryKey = false;
				colvarOrderShipCreateTime.IsForeignKey = false;
				colvarOrderShipCreateTime.IsReadOnly = false;
				colvarOrderShipCreateTime.DefaultSetting = @"";
				colvarOrderShipCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderShipCreateTime);

				TableSchema.TableColumn colvarOrderShipModifyTime = new TableSchema.TableColumn(schema);
				colvarOrderShipModifyTime.ColumnName = "order_ship_modify_time";
				colvarOrderShipModifyTime.DataType = DbType.DateTime;
				colvarOrderShipModifyTime.MaxLength = 0;
				colvarOrderShipModifyTime.AutoIncrement = false;
				colvarOrderShipModifyTime.IsNullable = true;
				colvarOrderShipModifyTime.IsPrimaryKey = false;
				colvarOrderShipModifyTime.IsForeignKey = false;
				colvarOrderShipModifyTime.IsReadOnly = false;
				colvarOrderShipModifyTime.DefaultSetting = @"";
				colvarOrderShipModifyTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderShipModifyTime);

				TableSchema.TableColumn colvarOrderShipModifyId = new TableSchema.TableColumn(schema);
				colvarOrderShipModifyId.ColumnName = "order_ship_modify_id";
				colvarOrderShipModifyId.DataType = DbType.String;
				colvarOrderShipModifyId.MaxLength = 100;
				colvarOrderShipModifyId.AutoIncrement = false;
				colvarOrderShipModifyId.IsNullable = true;
				colvarOrderShipModifyId.IsPrimaryKey = false;
				colvarOrderShipModifyId.IsForeignKey = false;
				colvarOrderShipModifyId.IsReadOnly = false;
				colvarOrderShipModifyId.DefaultSetting = @"";
				colvarOrderShipModifyId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderShipModifyId);

				TableSchema.TableColumn colvarShipToStockTime = new TableSchema.TableColumn(schema);
				colvarShipToStockTime.ColumnName = "ship_to_stock_time";
				colvarShipToStockTime.DataType = DbType.DateTime;
				colvarShipToStockTime.MaxLength = 0;
				colvarShipToStockTime.AutoIncrement = false;
				colvarShipToStockTime.IsNullable = true;
				colvarShipToStockTime.IsPrimaryKey = false;
				colvarShipToStockTime.IsForeignKey = false;
				colvarShipToStockTime.IsReadOnly = false;
				colvarShipToStockTime.DefaultSetting = @"";
				colvarShipToStockTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarShipToStockTime);

				TableSchema.TableColumn colvarReceiptTime = new TableSchema.TableColumn(schema);
				colvarReceiptTime.ColumnName = "receipt_time";
				colvarReceiptTime.DataType = DbType.DateTime;
				colvarReceiptTime.MaxLength = 0;
				colvarReceiptTime.AutoIncrement = false;
				colvarReceiptTime.IsNullable = true;
				colvarReceiptTime.IsPrimaryKey = false;
				colvarReceiptTime.IsForeignKey = false;
				colvarReceiptTime.IsReadOnly = false;
				colvarReceiptTime.DefaultSetting = @"";
				colvarReceiptTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReceiptTime);

				TableSchema.TableColumn colvarOrderName = new TableSchema.TableColumn(schema);
				colvarOrderName.ColumnName = "order_name";
				colvarOrderName.DataType = DbType.String;
				colvarOrderName.MaxLength = 100;
				colvarOrderName.AutoIncrement = false;
				colvarOrderName.IsNullable = true;
				colvarOrderName.IsPrimaryKey = false;
				colvarOrderName.IsForeignKey = false;
				colvarOrderName.IsReadOnly = false;
				colvarOrderName.DefaultSetting = @"";
				colvarOrderName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderName);

				TableSchema.TableColumn colvarOrderCreateTime = new TableSchema.TableColumn(schema);
				colvarOrderCreateTime.ColumnName = "order_create_time";
				colvarOrderCreateTime.DataType = DbType.DateTime;
				colvarOrderCreateTime.MaxLength = 0;
				colvarOrderCreateTime.AutoIncrement = false;
				colvarOrderCreateTime.IsNullable = false;
				colvarOrderCreateTime.IsPrimaryKey = false;
				colvarOrderCreateTime.IsForeignKey = false;
				colvarOrderCreateTime.IsReadOnly = false;
				colvarOrderCreateTime.DefaultSetting = @"";
				colvarOrderCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderCreateTime);

				TableSchema.TableColumn colvarOrderStatus = new TableSchema.TableColumn(schema);
				colvarOrderStatus.ColumnName = "order_status";
				colvarOrderStatus.DataType = DbType.Int32;
				colvarOrderStatus.MaxLength = 0;
				colvarOrderStatus.AutoIncrement = false;
				colvarOrderStatus.IsNullable = false;
				colvarOrderStatus.IsPrimaryKey = false;
				colvarOrderStatus.IsForeignKey = false;
				colvarOrderStatus.IsReadOnly = false;
				colvarOrderStatus.DefaultSetting = @"";
				colvarOrderStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderStatus);

				TableSchema.TableColumn colvarOrderClassification = new TableSchema.TableColumn(schema);
				colvarOrderClassification.ColumnName = "order_classification";
				colvarOrderClassification.DataType = DbType.Int32;
				colvarOrderClassification.MaxLength = 0;
				colvarOrderClassification.AutoIncrement = false;
				colvarOrderClassification.IsNullable = false;
				colvarOrderClassification.IsPrimaryKey = false;
				colvarOrderClassification.IsForeignKey = false;
				colvarOrderClassification.IsReadOnly = false;
				colvarOrderClassification.DefaultSetting = @"";
				colvarOrderClassification.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderClassification);

				TableSchema.TableColumn colvarShipMemo = new TableSchema.TableColumn(schema);
				colvarShipMemo.ColumnName = "ship_memo";
				colvarShipMemo.DataType = DbType.String;
				colvarShipMemo.MaxLength = 60;
				colvarShipMemo.AutoIncrement = false;
				colvarShipMemo.IsNullable = true;
				colvarShipMemo.IsPrimaryKey = false;
				colvarShipMemo.IsForeignKey = false;
				colvarShipMemo.IsReadOnly = false;
				colvarShipMemo.DefaultSetting = @"";
				colvarShipMemo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarShipMemo);

				TableSchema.TableColumn colvarType = new TableSchema.TableColumn(schema);
				colvarType.ColumnName = "type";
				colvarType.DataType = DbType.Int32;
				colvarType.MaxLength = 0;
				colvarType.AutoIncrement = false;
				colvarType.IsNullable = true;
				colvarType.IsPrimaryKey = false;
				colvarType.IsForeignKey = false;
				colvarType.IsReadOnly = false;
				colvarType.DefaultSetting = @"";
				colvarType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarType);

				TableSchema.TableColumn colvarCartId = new TableSchema.TableColumn(schema);
				colvarCartId.ColumnName = "cart_id";
				colvarCartId.DataType = DbType.AnsiString;
				colvarCartId.MaxLength = 50;
				colvarCartId.AutoIncrement = false;
				colvarCartId.IsNullable = true;
				colvarCartId.IsPrimaryKey = false;
				colvarCartId.IsForeignKey = false;
				colvarCartId.IsReadOnly = false;
				colvarCartId.DefaultSetting = @"";
				colvarCartId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCartId);

				TableSchema.TableColumn colvarSubCartId = new TableSchema.TableColumn(schema);
				colvarSubCartId.ColumnName = "sub_cart_id";
				colvarSubCartId.DataType = DbType.AnsiString;
				colvarSubCartId.MaxLength = 50;
				colvarSubCartId.AutoIncrement = false;
				colvarSubCartId.IsNullable = true;
				colvarSubCartId.IsPrimaryKey = false;
				colvarSubCartId.IsForeignKey = false;
				colvarSubCartId.IsReadOnly = false;
				colvarSubCartId.DefaultSetting = @"";
				colvarSubCartId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSubCartId);

				TableSchema.TableColumn colvarProductDeliveryType = new TableSchema.TableColumn(schema);
				colvarProductDeliveryType.ColumnName = "product_delivery_type";
				colvarProductDeliveryType.DataType = DbType.Int32;
				colvarProductDeliveryType.MaxLength = 0;
				colvarProductDeliveryType.AutoIncrement = false;
				colvarProductDeliveryType.IsNullable = false;
				colvarProductDeliveryType.IsPrimaryKey = false;
				colvarProductDeliveryType.IsForeignKey = false;
				colvarProductDeliveryType.IsReadOnly = false;
				colvarProductDeliveryType.DefaultSetting = @"";
				colvarProductDeliveryType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarProductDeliveryType);

				TableSchema.TableColumn colvarFamilyStoreName = new TableSchema.TableColumn(schema);
				colvarFamilyStoreName.ColumnName = "family_store_name";
				colvarFamilyStoreName.DataType = DbType.String;
				colvarFamilyStoreName.MaxLength = 50;
				colvarFamilyStoreName.AutoIncrement = false;
				colvarFamilyStoreName.IsNullable = true;
				colvarFamilyStoreName.IsPrimaryKey = false;
				colvarFamilyStoreName.IsForeignKey = false;
				colvarFamilyStoreName.IsReadOnly = false;
				colvarFamilyStoreName.DefaultSetting = @"";
				colvarFamilyStoreName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFamilyStoreName);

				TableSchema.TableColumn colvarSellerGoodsStatus = new TableSchema.TableColumn(schema);
				colvarSellerGoodsStatus.ColumnName = "seller_goods_status";
				colvarSellerGoodsStatus.DataType = DbType.Int32;
				colvarSellerGoodsStatus.MaxLength = 0;
				colvarSellerGoodsStatus.AutoIncrement = false;
				colvarSellerGoodsStatus.IsNullable = true;
				colvarSellerGoodsStatus.IsPrimaryKey = false;
				colvarSellerGoodsStatus.IsForeignKey = false;
				colvarSellerGoodsStatus.IsReadOnly = false;
				colvarSellerGoodsStatus.DefaultSetting = @"";
				colvarSellerGoodsStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSellerGoodsStatus);

				TableSchema.TableColumn colvarSevenStoreName = new TableSchema.TableColumn(schema);
				colvarSevenStoreName.ColumnName = "seven_store_name";
				colvarSevenStoreName.DataType = DbType.String;
				colvarSevenStoreName.MaxLength = 50;
				colvarSevenStoreName.AutoIncrement = false;
				colvarSevenStoreName.IsNullable = true;
				colvarSevenStoreName.IsPrimaryKey = false;
				colvarSevenStoreName.IsForeignKey = false;
				colvarSevenStoreName.IsReadOnly = false;
				colvarSevenStoreName.DefaultSetting = @"";
				colvarSevenStoreName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSevenStoreName);

				TableSchema.TableColumn colvarIspOrderType = new TableSchema.TableColumn(schema);
				colvarIspOrderType.ColumnName = "isp_order_type";
				colvarIspOrderType.DataType = DbType.Int32;
				colvarIspOrderType.MaxLength = 0;
				colvarIspOrderType.AutoIncrement = false;
				colvarIspOrderType.IsNullable = true;
				colvarIspOrderType.IsPrimaryKey = false;
				colvarIspOrderType.IsForeignKey = false;
				colvarIspOrderType.IsReadOnly = false;
				colvarIspOrderType.DefaultSetting = @"";
				colvarIspOrderType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIspOrderType);

				TableSchema.TableColumn colvarPreShipNo = new TableSchema.TableColumn(schema);
				colvarPreShipNo.ColumnName = "pre_ship_no";
				colvarPreShipNo.DataType = DbType.AnsiString;
				colvarPreShipNo.MaxLength = 50;
				colvarPreShipNo.AutoIncrement = false;
				colvarPreShipNo.IsNullable = true;
				colvarPreShipNo.IsPrimaryKey = false;
				colvarPreShipNo.IsForeignKey = false;
				colvarPreShipNo.IsReadOnly = false;
				colvarPreShipNo.DefaultSetting = @"";
				colvarPreShipNo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPreShipNo);

				TableSchema.TableColumn colvarProgressStatus = new TableSchema.TableColumn(schema);
				colvarProgressStatus.ColumnName = "progress_status";
				colvarProgressStatus.DataType = DbType.Int32;
				colvarProgressStatus.MaxLength = 0;
				colvarProgressStatus.AutoIncrement = false;
				colvarProgressStatus.IsNullable = true;
				colvarProgressStatus.IsPrimaryKey = false;
				colvarProgressStatus.IsForeignKey = false;
				colvarProgressStatus.IsReadOnly = false;
				colvarProgressStatus.DefaultSetting = @"";
				colvarProgressStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarProgressStatus);

				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("view_order_ship_list",schema);
			}
		}
		#endregion

		#region Props

		[XmlAttribute("ProductGuid")]
		[Bindable(true)]
		public Guid ProductGuid
		{
			get { return GetColumnValue<Guid>(Columns.ProductGuid); }
			set { SetColumnValue(Columns.ProductGuid, value); }
		}

		[XmlAttribute("OrderGuid")]
		[Bindable(true)]
		public Guid OrderGuid
		{
			get { return GetColumnValue<Guid>(Columns.OrderGuid); }
			set { SetColumnValue(Columns.OrderGuid, value); }
		}

		[XmlAttribute("OrderId")]
		[Bindable(true)]
		public string OrderId
		{
			get { return GetColumnValue<string>(Columns.OrderId); }
			set { SetColumnValue(Columns.OrderId, value); }
		}

		[XmlAttribute("MemberName")]
		[Bindable(true)]
		public string MemberName
		{
			get { return GetColumnValue<string>(Columns.MemberName); }
			set { SetColumnValue(Columns.MemberName, value); }
		}

		[XmlAttribute("PhoneNumber")]
		[Bindable(true)]
		public string PhoneNumber
		{
			get { return GetColumnValue<string>(Columns.PhoneNumber); }
			set { SetColumnValue(Columns.PhoneNumber, value); }
		}

		[XmlAttribute("DeliveryAddress")]
		[Bindable(true)]
		public string DeliveryAddress
		{
			get { return GetColumnValue<string>(Columns.DeliveryAddress); }
			set { SetColumnValue(Columns.DeliveryAddress, value); }
		}

		[XmlAttribute("UserMemo")]
		[Bindable(true)]
		public string UserMemo
		{
			get { return GetColumnValue<string>(Columns.UserMemo); }
			set { SetColumnValue(Columns.UserMemo, value); }
		}

		[XmlAttribute("OrderShipId")]
		[Bindable(true)]
		public int? OrderShipId
		{
			get { return GetColumnValue<int?>(Columns.OrderShipId); }
			set { SetColumnValue(Columns.OrderShipId, value); }
		}

		[XmlAttribute("ShipCompanyId")]
		[Bindable(true)]
		public int? ShipCompanyId
		{
			get { return GetColumnValue<int?>(Columns.ShipCompanyId); }
			set { SetColumnValue(Columns.ShipCompanyId, value); }
		}

		[XmlAttribute("ShipTime")]
		[Bindable(true)]
		public DateTime? ShipTime
		{
			get { return GetColumnValue<DateTime?>(Columns.ShipTime); }
			set { SetColumnValue(Columns.ShipTime, value); }
		}

		[XmlAttribute("ShipNo")]
		[Bindable(true)]
		public string ShipNo
		{
			get { return GetColumnValue<string>(Columns.ShipNo); }
			set { SetColumnValue(Columns.ShipNo, value); }
		}

		[XmlAttribute("ShipCompanyName")]
		[Bindable(true)]
		public string ShipCompanyName
		{
			get { return GetColumnValue<string>(Columns.ShipCompanyName); }
			set { SetColumnValue(Columns.ShipCompanyName, value); }
		}

		[XmlAttribute("ShipWebsite")]
		[Bindable(true)]
		public string ShipWebsite
		{
			get { return GetColumnValue<string>(Columns.ShipWebsite); }
			set { SetColumnValue(Columns.ShipWebsite, value); }
		}

		[XmlAttribute("ServiceTel")]
		[Bindable(true)]
		public string ServiceTel
		{
			get { return GetColumnValue<string>(Columns.ServiceTel); }
			set { SetColumnValue(Columns.ServiceTel, value); }
		}

		[XmlAttribute("OrderShipCreateTime")]
		[Bindable(true)]
		public DateTime? OrderShipCreateTime
		{
			get { return GetColumnValue<DateTime?>(Columns.OrderShipCreateTime); }
			set { SetColumnValue(Columns.OrderShipCreateTime, value); }
		}

		[XmlAttribute("OrderShipModifyTime")]
		[Bindable(true)]
		public DateTime? OrderShipModifyTime
		{
			get { return GetColumnValue<DateTime?>(Columns.OrderShipModifyTime); }
			set { SetColumnValue(Columns.OrderShipModifyTime, value); }
		}

		[XmlAttribute("OrderShipModifyId")]
		[Bindable(true)]
		public string OrderShipModifyId
		{
			get { return GetColumnValue<string>(Columns.OrderShipModifyId); }
			set { SetColumnValue(Columns.OrderShipModifyId, value); }
		}

		[XmlAttribute("ShipToStockTime")]
		[Bindable(true)]
		public DateTime? ShipToStockTime
		{
			get { return GetColumnValue<DateTime?>(Columns.ShipToStockTime); }
			set { SetColumnValue(Columns.ShipToStockTime, value); }
		}

		[XmlAttribute("ReceiptTime")]
		[Bindable(true)]
		public DateTime? ReceiptTime
		{
			get { return GetColumnValue<DateTime?>(Columns.ReceiptTime); }
			set { SetColumnValue(Columns.ReceiptTime, value); }
		}

		[XmlAttribute("OrderName")]
		[Bindable(true)]
		public string OrderName
		{
			get { return GetColumnValue<string>(Columns.OrderName); }
			set { SetColumnValue(Columns.OrderName, value); }
		}

		[XmlAttribute("OrderCreateTime")]
		[Bindable(true)]
		public DateTime OrderCreateTime
		{
			get { return GetColumnValue<DateTime>(Columns.OrderCreateTime); }
			set { SetColumnValue(Columns.OrderCreateTime, value); }
		}

		[XmlAttribute("OrderStatus")]
		[Bindable(true)]
		public int OrderStatus
		{
			get { return GetColumnValue<int>(Columns.OrderStatus); }
			set { SetColumnValue(Columns.OrderStatus, value); }
		}

		[XmlAttribute("OrderClassification")]
		[Bindable(true)]
		public int OrderClassification
		{
			get { return GetColumnValue<int>(Columns.OrderClassification); }
			set { SetColumnValue(Columns.OrderClassification, value); }
		}

		[XmlAttribute("ShipMemo")]
		[Bindable(true)]
		public string ShipMemo
		{
			get { return GetColumnValue<string>(Columns.ShipMemo); }
			set { SetColumnValue(Columns.ShipMemo, value); }
		}

		[XmlAttribute("Type")]
		[Bindable(true)]
		public int? Type
		{
			get { return GetColumnValue<int?>(Columns.Type); }
			set { SetColumnValue(Columns.Type, value); }
		}

		[XmlAttribute("CartId")]
		[Bindable(true)]
		public string CartId
		{
			get { return GetColumnValue<string>(Columns.CartId); }
			set { SetColumnValue(Columns.CartId, value); }
		}

		[XmlAttribute("SubCartId")]
		[Bindable(true)]
		public string SubCartId
		{
			get { return GetColumnValue<string>(Columns.SubCartId); }
			set { SetColumnValue(Columns.SubCartId, value); }
		}

		[XmlAttribute("ProductDeliveryType")]
		[Bindable(true)]
		public int ProductDeliveryType
		{
			get { return GetColumnValue<int>(Columns.ProductDeliveryType); }
			set { SetColumnValue(Columns.ProductDeliveryType, value); }
		}

		[XmlAttribute("FamilyStoreName")]
		[Bindable(true)]
		public string FamilyStoreName
		{
			get { return GetColumnValue<string>(Columns.FamilyStoreName); }
			set { SetColumnValue(Columns.FamilyStoreName, value); }
		}

		[XmlAttribute("SellerGoodsStatus")]
		[Bindable(true)]
		public int? SellerGoodsStatus
		{
			get { return GetColumnValue<int?>(Columns.SellerGoodsStatus); }
			set { SetColumnValue(Columns.SellerGoodsStatus, value); }
		}

		[XmlAttribute("SevenStoreName")]
		[Bindable(true)]
		public string SevenStoreName
		{
			get { return GetColumnValue<string>(Columns.SevenStoreName); }
			set { SetColumnValue(Columns.SevenStoreName, value); }
		}

		[XmlAttribute("IspOrderType")]
		[Bindable(true)]
		public int? IspOrderType
		{
			get { return GetColumnValue<int?>(Columns.IspOrderType); }
			set { SetColumnValue(Columns.IspOrderType, value); }
		}

		[XmlAttribute("PreShipNo")]
		[Bindable(true)]
		public string PreShipNo
		{
			get { return GetColumnValue<string>(Columns.PreShipNo); }
			set { SetColumnValue(Columns.PreShipNo, value); }
		}

		[XmlAttribute("ProgressStatus")]
		[Bindable(true)]
		public int? ProgressStatus
		{
			get { return GetColumnValue<int?>(Columns.ProgressStatus); }
			set { SetColumnValue(Columns.ProgressStatus, value); }
		}

		#endregion

		#region Typed Columns

		public static TableSchema.TableColumn ProductGuidColumn
		{
			get { return Schema.Columns[0]; }
		}

		public static TableSchema.TableColumn OrderGuidColumn
		{
			get { return Schema.Columns[1]; }
		}

		public static TableSchema.TableColumn OrderIdColumn
		{
			get { return Schema.Columns[2]; }
		}

		public static TableSchema.TableColumn MemberNameColumn
		{
			get { return Schema.Columns[3]; }
		}

		public static TableSchema.TableColumn PhoneNumberColumn
		{
			get { return Schema.Columns[4]; }
		}

		public static TableSchema.TableColumn DeliveryAddressColumn
		{
			get { return Schema.Columns[5]; }
		}

		public static TableSchema.TableColumn UserMemoColumn
		{
			get { return Schema.Columns[6]; }
		}

		public static TableSchema.TableColumn OrderShipIdColumn
		{
			get { return Schema.Columns[7]; }
		}

		public static TableSchema.TableColumn ShipCompanyIdColumn
		{
			get { return Schema.Columns[8]; }
		}

		public static TableSchema.TableColumn ShipTimeColumn
		{
			get { return Schema.Columns[9]; }
		}

		public static TableSchema.TableColumn ShipNoColumn
		{
			get { return Schema.Columns[10]; }
		}

		public static TableSchema.TableColumn ShipCompanyNameColumn
		{
			get { return Schema.Columns[11]; }
		}

		public static TableSchema.TableColumn ShipWebsiteColumn
		{
			get { return Schema.Columns[12]; }
		}

		public static TableSchema.TableColumn ServiceTelColumn
		{
			get { return Schema.Columns[13]; }
		}

		public static TableSchema.TableColumn OrderShipCreateTimeColumn
		{
			get { return Schema.Columns[14]; }
		}

		public static TableSchema.TableColumn OrderShipModifyTimeColumn
		{
			get { return Schema.Columns[15]; }
		}

		public static TableSchema.TableColumn OrderShipModifyIdColumn
		{
			get { return Schema.Columns[16]; }
		}

		public static TableSchema.TableColumn ShipToStockTimeColumn
		{
			get { return Schema.Columns[17]; }
		}

		public static TableSchema.TableColumn ReceiptTimeColumn
		{
			get { return Schema.Columns[18]; }
		}

		public static TableSchema.TableColumn OrderNameColumn
		{
			get { return Schema.Columns[19]; }
		}

		public static TableSchema.TableColumn OrderCreateTimeColumn
		{
			get { return Schema.Columns[20]; }
		}

		public static TableSchema.TableColumn OrderStatusColumn
		{
			get { return Schema.Columns[21]; }
		}

		public static TableSchema.TableColumn OrderClassificationColumn
		{
			get { return Schema.Columns[22]; }
		}

		public static TableSchema.TableColumn ShipMemoColumn
		{
			get { return Schema.Columns[23]; }
		}

		public static TableSchema.TableColumn TypeColumn
		{
			get { return Schema.Columns[24]; }
		}

		public static TableSchema.TableColumn CartIdColumn
		{
			get { return Schema.Columns[25]; }
		}

		public static TableSchema.TableColumn SubCartIdColumn
		{
			get { return Schema.Columns[26]; }
		}

		public static TableSchema.TableColumn ProductDeliveryTypeColumn
		{
			get { return Schema.Columns[27]; }
		}

		public static TableSchema.TableColumn FamilyStoreNameColumn
		{
			get { return Schema.Columns[28]; }
		}

		public static TableSchema.TableColumn SellerGoodsStatusColumn
		{
			get { return Schema.Columns[29]; }
		}

		public static TableSchema.TableColumn SevenStoreNameColumn
		{
			get { return Schema.Columns[30]; }
		}

		public static TableSchema.TableColumn IspOrderTypeColumn
		{
			get { return Schema.Columns[31]; }
		}

		public static TableSchema.TableColumn PreShipNoColumn
		{
			get { return Schema.Columns[32]; }
		}

		public static TableSchema.TableColumn ProgressStatusColumn
		{
			get { return Schema.Columns[33]; }
		}

		#endregion

		#region Columns Struct

		public struct Columns
		{
			public static string ProductGuid = @"product_guid";
			public static string OrderGuid = @"order_guid";
			public static string OrderId = @"order_id";
			public static string MemberName = @"member_name";
			public static string PhoneNumber = @"phone_number";
			public static string DeliveryAddress = @"delivery_address";
			public static string UserMemo = @"user_memo";
			public static string OrderShipId = @"order_ship_id";
			public static string ShipCompanyId = @"ship_company_id";
			public static string ShipTime = @"ship_time";
			public static string ShipNo = @"ship_no";
			public static string ShipCompanyName = @"ship_company_name";
			public static string ShipWebsite = @"ship_website";
			public static string ServiceTel = @"service_tel";
			public static string OrderShipCreateTime = @"order_ship_create_time";
			public static string OrderShipModifyTime = @"order_ship_modify_time";
			public static string OrderShipModifyId = @"order_ship_modify_id";
			public static string ShipToStockTime = @"ship_to_stock_time";
			public static string ReceiptTime = @"receipt_time";
			public static string OrderName = @"order_name";
			public static string OrderCreateTime = @"order_create_time";
			public static string OrderStatus = @"order_status";
			public static string OrderClassification = @"order_classification";
			public static string ShipMemo = @"ship_memo";
			public static string Type = @"type";
			public static string CartId = @"cart_id";
			public static string SubCartId = @"sub_cart_id";
			public static string ProductDeliveryType = @"product_delivery_type";
			public static string FamilyStoreName = @"family_store_name";
			public static string SellerGoodsStatus = @"seller_goods_status";
			public static string SevenStoreName = @"seven_store_name";
			public static string IspOrderType = @"isp_order_type";
			public static string PreShipNo = @"pre_ship_no";
			public static string ProgressStatus = @"progress_status";
		}

		#endregion

		#region IAbstractRecord Members
		public new CT GetColumnValue<CT>(string columnName)
		{
			return base.GetColumnValue<CT>(columnName);
		}
		public object GetColumnValue(string columnName)
		{
			return base.GetColumnValue<object>(columnName);
		}
		#endregion

	}
}

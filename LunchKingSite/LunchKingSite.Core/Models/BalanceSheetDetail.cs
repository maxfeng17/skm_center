using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the BalanceSheetDetail class.
	/// </summary>
    [Serializable]
	public partial class BalanceSheetDetailCollection : RepositoryList<BalanceSheetDetail, BalanceSheetDetailCollection>
	{	   
		public BalanceSheetDetailCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>BalanceSheetDetailCollection</returns>
		public BalanceSheetDetailCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                BalanceSheetDetail o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the balance_sheet_detail table.
	/// </summary>
	[Serializable]
	public partial class BalanceSheetDetail : RepositoryRecord<BalanceSheetDetail>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public BalanceSheetDetail()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public BalanceSheetDetail(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("balance_sheet_detail", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarBalanceSheetId = new TableSchema.TableColumn(schema);
				colvarBalanceSheetId.ColumnName = "balance_sheet_id";
				colvarBalanceSheetId.DataType = DbType.Int32;
				colvarBalanceSheetId.MaxLength = 0;
				colvarBalanceSheetId.AutoIncrement = false;
				colvarBalanceSheetId.IsNullable = false;
				colvarBalanceSheetId.IsPrimaryKey = true;
				colvarBalanceSheetId.IsForeignKey = true;
				colvarBalanceSheetId.IsReadOnly = false;
				colvarBalanceSheetId.DefaultSetting = @"";
				
					colvarBalanceSheetId.ForeignKeyTableName = "balance_sheet";
				schema.Columns.Add(colvarBalanceSheetId);
				
				TableSchema.TableColumn colvarTrustId = new TableSchema.TableColumn(schema);
				colvarTrustId.ColumnName = "trust_id";
				colvarTrustId.DataType = DbType.Guid;
				colvarTrustId.MaxLength = 0;
				colvarTrustId.AutoIncrement = false;
				colvarTrustId.IsNullable = false;
				colvarTrustId.IsPrimaryKey = true;
				colvarTrustId.IsForeignKey = true;
				colvarTrustId.IsReadOnly = false;
				colvarTrustId.DefaultSetting = @"";
				
					colvarTrustId.ForeignKeyTableName = "cash_trust_log";
				schema.Columns.Add(colvarTrustId);
				
				TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
				colvarStatus.ColumnName = "status";
				colvarStatus.DataType = DbType.Int32;
				colvarStatus.MaxLength = 0;
				colvarStatus.AutoIncrement = false;
				colvarStatus.IsNullable = false;
				colvarStatus.IsPrimaryKey = true;
				colvarStatus.IsForeignKey = false;
				colvarStatus.IsReadOnly = false;
				
						colvarStatus.DefaultSetting = @"((0))";
				colvarStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStatus);
				
				TableSchema.TableColumn colvarUndoTime = new TableSchema.TableColumn(schema);
				colvarUndoTime.ColumnName = "undo_time";
				colvarUndoTime.DataType = DbType.DateTime;
				colvarUndoTime.MaxLength = 0;
				colvarUndoTime.AutoIncrement = false;
				colvarUndoTime.IsNullable = true;
				colvarUndoTime.IsPrimaryKey = false;
				colvarUndoTime.IsForeignKey = false;
				colvarUndoTime.IsReadOnly = false;
				colvarUndoTime.DefaultSetting = @"";
				colvarUndoTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUndoTime);
				
				TableSchema.TableColumn colvarCashTrustStatusLogId = new TableSchema.TableColumn(schema);
				colvarCashTrustStatusLogId.ColumnName = "cash_trust_status_log_id";
				colvarCashTrustStatusLogId.DataType = DbType.Int32;
				colvarCashTrustStatusLogId.MaxLength = 0;
				colvarCashTrustStatusLogId.AutoIncrement = false;
				colvarCashTrustStatusLogId.IsNullable = false;
				colvarCashTrustStatusLogId.IsPrimaryKey = false;
				colvarCashTrustStatusLogId.IsForeignKey = true;
				colvarCashTrustStatusLogId.IsReadOnly = false;
				colvarCashTrustStatusLogId.DefaultSetting = @"";
				
					colvarCashTrustStatusLogId.ForeignKeyTableName = "cash_trust_status_log";
				schema.Columns.Add(colvarCashTrustStatusLogId);
				
				TableSchema.TableColumn colvarUndoId = new TableSchema.TableColumn(schema);
				colvarUndoId.ColumnName = "undo_id";
				colvarUndoId.DataType = DbType.Int32;
				colvarUndoId.MaxLength = 0;
				colvarUndoId.AutoIncrement = false;
				colvarUndoId.IsNullable = true;
				colvarUndoId.IsPrimaryKey = false;
				colvarUndoId.IsForeignKey = false;
				colvarUndoId.IsReadOnly = false;
				colvarUndoId.DefaultSetting = @"";
				colvarUndoId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUndoId);
				
				TableSchema.TableColumn colvarModLogId = new TableSchema.TableColumn(schema);
				colvarModLogId.ColumnName = "mod_log_id";
				colvarModLogId.DataType = DbType.Int32;
				colvarModLogId.MaxLength = 0;
				colvarModLogId.AutoIncrement = false;
				colvarModLogId.IsNullable = true;
				colvarModLogId.IsPrimaryKey = false;
				colvarModLogId.IsForeignKey = true;
				colvarModLogId.IsReadOnly = false;
				colvarModLogId.DefaultSetting = @"";
				
					colvarModLogId.ForeignKeyTableName = "balance_modification_log";
				schema.Columns.Add(colvarModLogId);
				
				TableSchema.TableColumn colvarDeductedBalanceSheetId = new TableSchema.TableColumn(schema);
				colvarDeductedBalanceSheetId.ColumnName = "deducted_balance_sheet_id";
				colvarDeductedBalanceSheetId.DataType = DbType.Int32;
				colvarDeductedBalanceSheetId.MaxLength = 0;
				colvarDeductedBalanceSheetId.AutoIncrement = false;
				colvarDeductedBalanceSheetId.IsNullable = true;
				colvarDeductedBalanceSheetId.IsPrimaryKey = false;
				colvarDeductedBalanceSheetId.IsForeignKey = false;
				colvarDeductedBalanceSheetId.IsReadOnly = false;
				colvarDeductedBalanceSheetId.DefaultSetting = @"";
				colvarDeductedBalanceSheetId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDeductedBalanceSheetId);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("balance_sheet_detail",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("BalanceSheetId")]
		[Bindable(true)]
		public int BalanceSheetId 
		{
			get { return GetColumnValue<int>(Columns.BalanceSheetId); }
			set { SetColumnValue(Columns.BalanceSheetId, value); }
		}
		  
		[XmlAttribute("TrustId")]
		[Bindable(true)]
		public Guid TrustId 
		{
			get { return GetColumnValue<Guid>(Columns.TrustId); }
			set { SetColumnValue(Columns.TrustId, value); }
		}
		  
		[XmlAttribute("Status")]
		[Bindable(true)]
		public int Status 
		{
			get { return GetColumnValue<int>(Columns.Status); }
			set { SetColumnValue(Columns.Status, value); }
		}
		  
		[XmlAttribute("UndoTime")]
		[Bindable(true)]
		public DateTime? UndoTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.UndoTime); }
			set { SetColumnValue(Columns.UndoTime, value); }
		}
		  
		[XmlAttribute("CashTrustStatusLogId")]
		[Bindable(true)]
		public int CashTrustStatusLogId 
		{
			get { return GetColumnValue<int>(Columns.CashTrustStatusLogId); }
			set { SetColumnValue(Columns.CashTrustStatusLogId, value); }
		}
		  
		[XmlAttribute("UndoId")]
		[Bindable(true)]
		public int? UndoId 
		{
			get { return GetColumnValue<int?>(Columns.UndoId); }
			set { SetColumnValue(Columns.UndoId, value); }
		}
		  
		[XmlAttribute("ModLogId")]
		[Bindable(true)]
		public int? ModLogId 
		{
			get { return GetColumnValue<int?>(Columns.ModLogId); }
			set { SetColumnValue(Columns.ModLogId, value); }
		}
		  
		[XmlAttribute("DeductedBalanceSheetId")]
		[Bindable(true)]
		public int? DeductedBalanceSheetId 
		{
			get { return GetColumnValue<int?>(Columns.DeductedBalanceSheetId); }
			set { SetColumnValue(Columns.DeductedBalanceSheetId, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (4)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn BalanceSheetIdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn TrustIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn StatusColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn UndoTimeColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn CashTrustStatusLogIdColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn UndoIdColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn ModLogIdColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn DeductedBalanceSheetIdColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string BalanceSheetId = @"balance_sheet_id";
			 public static string TrustId = @"trust_id";
			 public static string Status = @"status";
			 public static string UndoTime = @"undo_time";
			 public static string CashTrustStatusLogId = @"cash_trust_status_log_id";
			 public static string UndoId = @"undo_id";
			 public static string ModLogId = @"mod_log_id";
			 public static string DeductedBalanceSheetId = @"deducted_balance_sheet_id";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the S2tSmsLog class.
    /// </summary>
    [Serializable]
    public partial class S2tSmsLogCollection : RepositoryList<S2tSmsLog, S2tSmsLogCollection>
    {
        public S2tSmsLogCollection() { }

        /// <summary>
        /// Filters an existing collection based on the set criteria. This is an in-memory filter
        /// Thanks to developingchris for this!
        /// </summary>
        /// <returns>S2tSmsLogCollection</returns>
        public S2tSmsLogCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                S2tSmsLog o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }


    }
    /// <summary>
    /// This is an ActiveRecord class which wraps the s2t_sms_log table.
    /// </summary>
    [Serializable]
    public partial class S2tSmsLog : RepositoryRecord<S2tSmsLog>, IRecordBase
    {
        #region .ctors and Default Settings

        public S2tSmsLog()
        {
            SetSQLProps();
            InitSetDefaults();
            MarkNew();
        }

        private void InitSetDefaults() { SetDefaults(); }

        public S2tSmsLog(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
                ForceDefaults();
            MarkNew();
        }

        protected static void SetSQLProps() { GetTableSchema(); }

        #endregion

        #region Schema and Query Accessor
        public static Query CreateQuery() { return new Query(Schema); }
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                    SetSQLProps();
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("s2t_sms_log", TableType.Table, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarSi = new TableSchema.TableColumn(schema);
                colvarSi.ColumnName = "si";
                colvarSi.DataType = DbType.Int32;
                colvarSi.MaxLength = 0;
                colvarSi.AutoIncrement = true;
                colvarSi.IsNullable = false;
                colvarSi.IsPrimaryKey = true;
                colvarSi.IsForeignKey = false;
                colvarSi.IsReadOnly = false;
                colvarSi.DefaultSetting = @"";
                colvarSi.ForeignKeyTableName = "";
                schema.Columns.Add(colvarSi);

                TableSchema.TableColumn colvarCouponId = new TableSchema.TableColumn(schema);
                colvarCouponId.ColumnName = "coupon_id";
                colvarCouponId.DataType = DbType.Int32;
                colvarCouponId.MaxLength = 0;
                colvarCouponId.AutoIncrement = false;
                colvarCouponId.IsNullable = true;
                colvarCouponId.IsPrimaryKey = false;
                colvarCouponId.IsForeignKey = false;
                colvarCouponId.IsReadOnly = false;
                colvarCouponId.DefaultSetting = @"";
                colvarCouponId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCouponId);

                TableSchema.TableColumn colvarMsg = new TableSchema.TableColumn(schema);
                colvarMsg.ColumnName = "msg";
                colvarMsg.DataType = DbType.String;
                colvarMsg.MaxLength = 500;
                colvarMsg.AutoIncrement = false;
                colvarMsg.IsNullable = true;
                colvarMsg.IsPrimaryKey = false;
                colvarMsg.IsForeignKey = false;
                colvarMsg.IsReadOnly = false;
                colvarMsg.DefaultSetting = @"";
                colvarMsg.ForeignKeyTableName = "";
                schema.Columns.Add(colvarMsg);

                TableSchema.TableColumn colvarPhone = new TableSchema.TableColumn(schema);
                colvarPhone.ColumnName = "phone";
                colvarPhone.DataType = DbType.AnsiString;
                colvarPhone.MaxLength = 50;
                colvarPhone.AutoIncrement = false;
                colvarPhone.IsNullable = true;
                colvarPhone.IsPrimaryKey = false;
                colvarPhone.IsForeignKey = false;
                colvarPhone.IsReadOnly = false;
                colvarPhone.DefaultSetting = @"";
                colvarPhone.ForeignKeyTableName = "";
                schema.Columns.Add(colvarPhone);

                TableSchema.TableColumn colvarResponse = new TableSchema.TableColumn(schema);
                colvarResponse.ColumnName = "response";
                colvarResponse.DataType = DbType.String;
                colvarResponse.MaxLength = 50;
                colvarResponse.AutoIncrement = false;
                colvarResponse.IsNullable = true;
                colvarResponse.IsPrimaryKey = false;
                colvarResponse.IsForeignKey = false;
                colvarResponse.IsReadOnly = false;
                colvarResponse.DefaultSetting = @"";
                colvarResponse.ForeignKeyTableName = "";
                schema.Columns.Add(colvarResponse);

                TableSchema.TableColumn colvarResponseText = new TableSchema.TableColumn(schema);
                colvarResponseText.ColumnName = "response_text";
                colvarResponseText.DataType = DbType.String;
                colvarResponseText.MaxLength = 1500;
                colvarResponseText.AutoIncrement = false;
                colvarResponseText.IsNullable = true;
                colvarResponseText.IsPrimaryKey = false;
                colvarResponseText.IsForeignKey = false;
                colvarResponseText.IsReadOnly = false;
                colvarResponseText.DefaultSetting = @"";
                colvarResponseText.ForeignKeyTableName = "";
                schema.Columns.Add(colvarResponseText);

                TableSchema.TableColumn colvarUid = new TableSchema.TableColumn(schema);
                colvarUid.ColumnName = "uid";
                colvarUid.DataType = DbType.AnsiString;
                colvarUid.MaxLength = 50;
                colvarUid.AutoIncrement = false;
                colvarUid.IsNullable = true;
                colvarUid.IsPrimaryKey = false;
                colvarUid.IsForeignKey = false;
                colvarUid.IsReadOnly = false;
                colvarUid.DefaultSetting = @"";
                colvarUid.ForeignKeyTableName = "";
                schema.Columns.Add(colvarUid);

                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = true;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                colvarCreateTime.DefaultSetting = @"";
                colvarCreateTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateTime);

                TableSchema.TableColumn colvarOrgcode = new TableSchema.TableColumn(schema);
                colvarOrgcode.ColumnName = "orgcode";
                colvarOrgcode.DataType = DbType.AnsiString;
                colvarOrgcode.MaxLength = 50;
                colvarOrgcode.AutoIncrement = false;
                colvarOrgcode.IsNullable = true;
                colvarOrgcode.IsPrimaryKey = false;
                colvarOrgcode.IsForeignKey = false;
                colvarOrgcode.IsReadOnly = false;
                colvarOrgcode.DefaultSetting = @"";
                colvarOrgcode.ForeignKeyTableName = "";
                schema.Columns.Add(colvarOrgcode);

                TableSchema.TableColumn colvarSchedule = new TableSchema.TableColumn(schema);
                colvarSchedule.ColumnName = "schedule";
                colvarSchedule.DataType = DbType.DateTime;
                colvarSchedule.MaxLength = 0;
                colvarSchedule.AutoIncrement = false;
                colvarSchedule.IsNullable = true;
                colvarSchedule.IsPrimaryKey = false;
                colvarSchedule.IsForeignKey = false;
                colvarSchedule.IsReadOnly = false;
                colvarSchedule.DefaultSetting = @"";
                colvarSchedule.ForeignKeyTableName = "";
                schema.Columns.Add(colvarSchedule);

                TableSchema.TableColumn colvarMultiple = new TableSchema.TableColumn(schema);
                colvarMultiple.ColumnName = "multiple";
                colvarMultiple.DataType = DbType.Boolean;
                colvarMultiple.MaxLength = 0;
                colvarMultiple.AutoIncrement = false;
                colvarMultiple.IsNullable = false;
                colvarMultiple.IsPrimaryKey = false;
                colvarMultiple.IsForeignKey = false;
                colvarMultiple.IsReadOnly = false;

                colvarMultiple.DefaultSetting = @"((0))";
                colvarMultiple.ForeignKeyTableName = "";
                schema.Columns.Add(colvarMultiple);

                TableSchema.TableColumn colvarRemark = new TableSchema.TableColumn(schema);
                colvarRemark.ColumnName = "remark";
                colvarRemark.DataType = DbType.String;
                colvarRemark.MaxLength = 500;
                colvarRemark.AutoIncrement = false;
                colvarRemark.IsNullable = true;
                colvarRemark.IsPrimaryKey = false;
                colvarRemark.IsForeignKey = false;
                colvarRemark.IsReadOnly = false;
                colvarRemark.DefaultSetting = @"";
                colvarRemark.ForeignKeyTableName = "";
                schema.Columns.Add(colvarRemark);

                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("s2t_sms_log", schema);
            }
        }
        #endregion

        #region Props

        [XmlAttribute("Si")]
        [Bindable(true)]
        public int Si
        {
            get { return GetColumnValue<int>(Columns.Si); }
            set { SetColumnValue(Columns.Si, value); }
        }

        [XmlAttribute("CouponId")]
        [Bindable(true)]
        public int? CouponId
        {
            get { return GetColumnValue<int?>(Columns.CouponId); }
            set { SetColumnValue(Columns.CouponId, value); }
        }

        [XmlAttribute("Msg")]
        [Bindable(true)]
        public string Msg
        {
            get { return GetColumnValue<string>(Columns.Msg); }
            set { SetColumnValue(Columns.Msg, value); }
        }

        [XmlAttribute("Phone")]
        [Bindable(true)]
        public string Phone
        {
            get { return GetColumnValue<string>(Columns.Phone); }
            set { SetColumnValue(Columns.Phone, value); }
        }

        [XmlAttribute("Response")]
        [Bindable(true)]
        public string Response
        {
            get { return GetColumnValue<string>(Columns.Response); }
            set { SetColumnValue(Columns.Response, value); }
        }

        [XmlAttribute("ResponseText")]
        [Bindable(true)]
        public string ResponseText
        {
            get { return GetColumnValue<string>(Columns.ResponseText); }
            set { SetColumnValue(Columns.ResponseText, value); }
        }

        [XmlAttribute("Uid")]
        [Bindable(true)]
        public string Uid
        {
            get { return GetColumnValue<string>(Columns.Uid); }
            set { SetColumnValue(Columns.Uid, value); }
        }

        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime? CreateTime
        {
            get { return GetColumnValue<DateTime?>(Columns.CreateTime); }
            set { SetColumnValue(Columns.CreateTime, value); }
        }

        [XmlAttribute("Orgcode")]
        [Bindable(true)]
        public string Orgcode
        {
            get { return GetColumnValue<string>(Columns.Orgcode); }
            set { SetColumnValue(Columns.Orgcode, value); }
        }

        [XmlAttribute("Schedule")]
        [Bindable(true)]
        public DateTime? Schedule
        {
            get { return GetColumnValue<DateTime?>(Columns.Schedule); }
            set { SetColumnValue(Columns.Schedule, value); }
        }

        [XmlAttribute("Multiple")]
        [Bindable(true)]
        public bool Multiple
        {
            get { return GetColumnValue<bool>(Columns.Multiple); }
            set { SetColumnValue(Columns.Multiple, value); }
        }

        [XmlAttribute("Remark")]
        [Bindable(true)]
        public string Remark
        {
            get { return GetColumnValue<string>(Columns.Remark); }
            set { SetColumnValue(Columns.Remark, value); }
        }

        #endregion




        //no foreign key tables defined (0)



        //no ManyToMany tables defined (0)





        #region Typed Columns


        public static TableSchema.TableColumn SiColumn
        {
            get { return Schema.Columns[0]; }
        }



        public static TableSchema.TableColumn CouponIdColumn
        {
            get { return Schema.Columns[1]; }
        }



        public static TableSchema.TableColumn MsgColumn
        {
            get { return Schema.Columns[2]; }
        }



        public static TableSchema.TableColumn PhoneColumn
        {
            get { return Schema.Columns[3]; }
        }



        public static TableSchema.TableColumn ResponseColumn
        {
            get { return Schema.Columns[4]; }
        }



        public static TableSchema.TableColumn ResponseTextColumn
        {
            get { return Schema.Columns[5]; }
        }



        public static TableSchema.TableColumn UidColumn
        {
            get { return Schema.Columns[6]; }
        }



        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[7]; }
        }



        public static TableSchema.TableColumn OrgcodeColumn
        {
            get { return Schema.Columns[8]; }
        }



        public static TableSchema.TableColumn ScheduleColumn
        {
            get { return Schema.Columns[9]; }
        }



        public static TableSchema.TableColumn MultipleColumn
        {
            get { return Schema.Columns[10]; }
        }



        public static TableSchema.TableColumn RemarkColumn
        {
            get { return Schema.Columns[11]; }
        }



        #endregion
        #region Columns Struct
        public struct Columns
        {
            public static string Si = @"si";
            public static string CouponId = @"coupon_id";
            public static string Msg = @"msg";
            public static string Phone = @"phone";
            public static string Response = @"response";
            public static string ResponseText = @"response_text";
            public static string Uid = @"uid";
            public static string CreateTime = @"create_time";
            public static string Orgcode = @"orgcode";
            public static string Schedule = @"schedule";
            public static string Multiple = @"multiple";
            public static string Remark = @"remark";

        }
        #endregion

        #region Update PK Collections

        #endregion

        #region Deep Save

        #endregion
    }
}

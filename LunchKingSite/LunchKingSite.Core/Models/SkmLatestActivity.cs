using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the SkmLatestActivity class.
	/// </summary>
    [Serializable]
	public partial class SkmLatestActivityCollection : RepositoryList<SkmLatestActivity, SkmLatestActivityCollection>
	{	   
		public SkmLatestActivityCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>SkmLatestActivityCollection</returns>
		public SkmLatestActivityCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                SkmLatestActivity o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
	}
	
	
	/// <summary>
	/// This is an ActiveRecord class which wraps the skm_latest_activity table.
	/// </summary>
	
	[Serializable]
	public partial class SkmLatestActivity : RepositoryRecord<SkmLatestActivity>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public SkmLatestActivity()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public SkmLatestActivity(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("skm_latest_activity", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarActivityTitle = new TableSchema.TableColumn(schema);
				colvarActivityTitle.ColumnName = "activity_title";
				colvarActivityTitle.DataType = DbType.String;
				colvarActivityTitle.MaxLength = 200;
				colvarActivityTitle.AutoIncrement = false;
				colvarActivityTitle.IsNullable = true;
				colvarActivityTitle.IsPrimaryKey = false;
				colvarActivityTitle.IsForeignKey = false;
				colvarActivityTitle.IsReadOnly = false;
				colvarActivityTitle.DefaultSetting = @"";
				colvarActivityTitle.ForeignKeyTableName = "";
				schema.Columns.Add(colvarActivityTitle);
				
				TableSchema.TableColumn colvarStoreName = new TableSchema.TableColumn(schema);
				colvarStoreName.ColumnName = "store_name";
				colvarStoreName.DataType = DbType.String;
				colvarStoreName.MaxLength = 20;
				colvarStoreName.AutoIncrement = false;
				colvarStoreName.IsNullable = true;
				colvarStoreName.IsPrimaryKey = false;
				colvarStoreName.IsForeignKey = false;
				colvarStoreName.IsReadOnly = false;
				colvarStoreName.DefaultSetting = @"";
				colvarStoreName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStoreName);
				
				TableSchema.TableColumn colvarStoreFloor = new TableSchema.TableColumn(schema);
				colvarStoreFloor.ColumnName = "store_floor";
				colvarStoreFloor.DataType = DbType.AnsiString;
				colvarStoreFloor.MaxLength = 5;
				colvarStoreFloor.AutoIncrement = false;
				colvarStoreFloor.IsNullable = true;
				colvarStoreFloor.IsPrimaryKey = false;
				colvarStoreFloor.IsForeignKey = false;
				colvarStoreFloor.IsReadOnly = false;
				colvarStoreFloor.DefaultSetting = @"";
				colvarStoreFloor.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStoreFloor);
				
				TableSchema.TableColumn colvarLinkUrl = new TableSchema.TableColumn(schema);
				colvarLinkUrl.ColumnName = "link_url";
				colvarLinkUrl.DataType = DbType.AnsiString;
				colvarLinkUrl.MaxLength = -1;
				colvarLinkUrl.AutoIncrement = false;
				colvarLinkUrl.IsNullable = false;
				colvarLinkUrl.IsPrimaryKey = false;
				colvarLinkUrl.IsForeignKey = false;
				colvarLinkUrl.IsReadOnly = false;
				colvarLinkUrl.DefaultSetting = @"";
				colvarLinkUrl.ForeignKeyTableName = "";
				schema.Columns.Add(colvarLinkUrl);
				
				TableSchema.TableColumn colvarImageUrl = new TableSchema.TableColumn(schema);
				colvarImageUrl.ColumnName = "image_url";
				colvarImageUrl.DataType = DbType.AnsiString;
				colvarImageUrl.MaxLength = -1;
				colvarImageUrl.AutoIncrement = false;
				colvarImageUrl.IsNullable = true;
				colvarImageUrl.IsPrimaryKey = false;
				colvarImageUrl.IsForeignKey = false;
				colvarImageUrl.IsReadOnly = false;
				colvarImageUrl.DefaultSetting = @"";
				colvarImageUrl.ForeignKeyTableName = "";
				schema.Columns.Add(colvarImageUrl);
				
				TableSchema.TableColumn colvarSDate = new TableSchema.TableColumn(schema);
				colvarSDate.ColumnName = "s_date";
				colvarSDate.DataType = DbType.DateTime;
				colvarSDate.MaxLength = 0;
				colvarSDate.AutoIncrement = false;
				colvarSDate.IsNullable = false;
				colvarSDate.IsPrimaryKey = false;
				colvarSDate.IsForeignKey = false;
				colvarSDate.IsReadOnly = false;
				colvarSDate.DefaultSetting = @"";
				colvarSDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSDate);
				
				TableSchema.TableColumn colvarEDate = new TableSchema.TableColumn(schema);
				colvarEDate.ColumnName = "e_date";
				colvarEDate.DataType = DbType.DateTime;
				colvarEDate.MaxLength = 0;
				colvarEDate.AutoIncrement = false;
				colvarEDate.IsNullable = false;
				colvarEDate.IsPrimaryKey = false;
				colvarEDate.IsForeignKey = false;
				colvarEDate.IsReadOnly = false;
				colvarEDate.DefaultSetting = @"";
				colvarEDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEDate);
				
				TableSchema.TableColumn colvarActDate = new TableSchema.TableColumn(schema);
				colvarActDate.ColumnName = "act_date";
				colvarActDate.DataType = DbType.DateTime;
				colvarActDate.MaxLength = 0;
				colvarActDate.AutoIncrement = false;
				colvarActDate.IsNullable = false;
				colvarActDate.IsPrimaryKey = false;
				colvarActDate.IsForeignKey = false;
				colvarActDate.IsReadOnly = false;
				colvarActDate.DefaultSetting = @"";
				colvarActDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarActDate);
				
				TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
				colvarStatus.ColumnName = "status";
				colvarStatus.DataType = DbType.Int32;
				colvarStatus.MaxLength = 0;
				colvarStatus.AutoIncrement = false;
				colvarStatus.IsNullable = true;
				colvarStatus.IsPrimaryKey = false;
				colvarStatus.IsForeignKey = false;
				colvarStatus.IsReadOnly = false;
				colvarStatus.DefaultSetting = @"";
				colvarStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStatus);
				
				TableSchema.TableColumn colvarActivityDescription = new TableSchema.TableColumn(schema);
				colvarActivityDescription.ColumnName = "activity_description";
				colvarActivityDescription.DataType = DbType.String;
				colvarActivityDescription.MaxLength = 200;
				colvarActivityDescription.AutoIncrement = false;
				colvarActivityDescription.IsNullable = true;
				colvarActivityDescription.IsPrimaryKey = false;
				colvarActivityDescription.IsForeignKey = false;
				colvarActivityDescription.IsReadOnly = false;
				colvarActivityDescription.DefaultSetting = @"";
				colvarActivityDescription.ForeignKeyTableName = "";
				schema.Columns.Add(colvarActivityDescription);
				
				TableSchema.TableColumn colvarSellerGuid = new TableSchema.TableColumn(schema);
				colvarSellerGuid.ColumnName = "seller_guid";
				colvarSellerGuid.DataType = DbType.Guid;
				colvarSellerGuid.MaxLength = 0;
				colvarSellerGuid.AutoIncrement = false;
				colvarSellerGuid.IsNullable = true;
				colvarSellerGuid.IsPrimaryKey = false;
				colvarSellerGuid.IsForeignKey = false;
				colvarSellerGuid.IsReadOnly = false;
				colvarSellerGuid.DefaultSetting = @"";
				colvarSellerGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSellerGuid);
				
				TableSchema.TableColumn colvarCategoryId = new TableSchema.TableColumn(schema);
				colvarCategoryId.ColumnName = "category_id";
				colvarCategoryId.DataType = DbType.Int32;
				colvarCategoryId.MaxLength = 0;
				colvarCategoryId.AutoIncrement = false;
				colvarCategoryId.IsNullable = true;
				colvarCategoryId.IsPrimaryKey = false;
				colvarCategoryId.IsForeignKey = false;
				colvarCategoryId.IsReadOnly = false;
				colvarCategoryId.DefaultSetting = @"";
				colvarCategoryId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCategoryId);
				
				TableSchema.TableColumn colvarGuid = new TableSchema.TableColumn(schema);
				colvarGuid.ColumnName = "guid";
				colvarGuid.DataType = DbType.Guid;
				colvarGuid.MaxLength = 0;
				colvarGuid.AutoIncrement = false;
				colvarGuid.IsNullable = true;
				colvarGuid.IsPrimaryKey = false;
				colvarGuid.IsForeignKey = false;
				colvarGuid.IsReadOnly = false;
				colvarGuid.DefaultSetting = @"";
				colvarGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGuid);
				
				TableSchema.TableColumn colvarSort = new TableSchema.TableColumn(schema);
				colvarSort.ColumnName = "sort";
				colvarSort.DataType = DbType.Int32;
				colvarSort.MaxLength = 0;
				colvarSort.AutoIncrement = false;
				colvarSort.IsNullable = false;
				colvarSort.IsPrimaryKey = false;
				colvarSort.IsForeignKey = false;
				colvarSort.IsReadOnly = false;
				
						colvarSort.DefaultSetting = @"((0))";
				colvarSort.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSort);
				
				TableSchema.TableColumn colvarIsMustLogin = new TableSchema.TableColumn(schema);
				colvarIsMustLogin.ColumnName = "is_must_login";
				colvarIsMustLogin.DataType = DbType.Boolean;
				colvarIsMustLogin.MaxLength = 0;
				colvarIsMustLogin.AutoIncrement = false;
				colvarIsMustLogin.IsNullable = false;
				colvarIsMustLogin.IsPrimaryKey = false;
				colvarIsMustLogin.IsForeignKey = false;
				colvarIsMustLogin.IsReadOnly = false;
				
						colvarIsMustLogin.DefaultSetting = @"((0))";
				colvarIsMustLogin.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsMustLogin);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("skm_latest_activity",schema);
			}
		}
		#endregion
		
		#region Props
		
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		
		[XmlAttribute("ActivityTitle")]
		[Bindable(true)]
		public string ActivityTitle 
		{
			get { return GetColumnValue<string>(Columns.ActivityTitle); }
			set { SetColumnValue(Columns.ActivityTitle, value); }
		}
		
		[XmlAttribute("StoreName")]
		[Bindable(true)]
		public string StoreName 
		{
			get { return GetColumnValue<string>(Columns.StoreName); }
			set { SetColumnValue(Columns.StoreName, value); }
		}
		
		[XmlAttribute("StoreFloor")]
		[Bindable(true)]
		public string StoreFloor 
		{
			get { return GetColumnValue<string>(Columns.StoreFloor); }
			set { SetColumnValue(Columns.StoreFloor, value); }
		}
		
		[XmlAttribute("LinkUrl")]
		[Bindable(true)]
		public string LinkUrl 
		{
			get { return GetColumnValue<string>(Columns.LinkUrl); }
			set { SetColumnValue(Columns.LinkUrl, value); }
		}
		
		[XmlAttribute("ImageUrl")]
		[Bindable(true)]
		public string ImageUrl 
		{
			get { return GetColumnValue<string>(Columns.ImageUrl); }
			set { SetColumnValue(Columns.ImageUrl, value); }
		}
		
		[XmlAttribute("SDate")]
		[Bindable(true)]
		public DateTime SDate 
		{
			get { return GetColumnValue<DateTime>(Columns.SDate); }
			set { SetColumnValue(Columns.SDate, value); }
		}
		
		[XmlAttribute("EDate")]
		[Bindable(true)]
		public DateTime EDate 
		{
			get { return GetColumnValue<DateTime>(Columns.EDate); }
			set { SetColumnValue(Columns.EDate, value); }
		}
		
		[XmlAttribute("ActDate")]
		[Bindable(true)]
		public DateTime ActDate 
		{
			get { return GetColumnValue<DateTime>(Columns.ActDate); }
			set { SetColumnValue(Columns.ActDate, value); }
		}
		
		[XmlAttribute("Status")]
		[Bindable(true)]
		public int? Status 
		{
			get { return GetColumnValue<int?>(Columns.Status); }
			set { SetColumnValue(Columns.Status, value); }
		}
		
		[XmlAttribute("ActivityDescription")]
		[Bindable(true)]
		public string ActivityDescription 
		{
			get { return GetColumnValue<string>(Columns.ActivityDescription); }
			set { SetColumnValue(Columns.ActivityDescription, value); }
		}
		
		[XmlAttribute("SellerGuid")]
		[Bindable(true)]
		public Guid? SellerGuid 
		{
			get { return GetColumnValue<Guid?>(Columns.SellerGuid); }
			set { SetColumnValue(Columns.SellerGuid, value); }
		}
		
		[XmlAttribute("CategoryId")]
		[Bindable(true)]
		public int? CategoryId 
		{
			get { return GetColumnValue<int?>(Columns.CategoryId); }
			set { SetColumnValue(Columns.CategoryId, value); }
		}
		
		[XmlAttribute("Guid")]
		[Bindable(true)]
		public Guid? Guid 
		{
			get { return GetColumnValue<Guid?>(Columns.Guid); }
			set { SetColumnValue(Columns.Guid, value); }
		}
		
		[XmlAttribute("Sort")]
		[Bindable(true)]
		public int Sort 
		{
			get { return GetColumnValue<int>(Columns.Sort); }
			set { SetColumnValue(Columns.Sort, value); }
		}
		
		[XmlAttribute("IsMustLogin")]
		[Bindable(true)]
		public bool IsMustLogin 
		{
			get { return GetColumnValue<bool>(Columns.IsMustLogin); }
			set { SetColumnValue(Columns.IsMustLogin, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn ActivityTitleColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn StoreNameColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn StoreFloorColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn LinkUrlColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn ImageUrlColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn SDateColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn EDateColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn ActDateColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn StatusColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn ActivityDescriptionColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        public static TableSchema.TableColumn SellerGuidColumn
        {
            get { return Schema.Columns[11]; }
        }
        
        
        
        public static TableSchema.TableColumn CategoryIdColumn
        {
            get { return Schema.Columns[12]; }
        }
        
        
        
        public static TableSchema.TableColumn GuidColumn
        {
            get { return Schema.Columns[13]; }
        }
        
        
        
        public static TableSchema.TableColumn SortColumn
        {
            get { return Schema.Columns[14]; }
        }
        
        
        
        public static TableSchema.TableColumn IsMustLoginColumn
        {
            get { return Schema.Columns[15]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string ActivityTitle = @"activity_title";
			 public static string StoreName = @"store_name";
			 public static string StoreFloor = @"store_floor";
			 public static string LinkUrl = @"link_url";
			 public static string ImageUrl = @"image_url";
			 public static string SDate = @"s_date";
			 public static string EDate = @"e_date";
			 public static string ActDate = @"act_date";
			 public static string Status = @"status";
			 public static string ActivityDescription = @"activity_description";
			 public static string SellerGuid = @"seller_guid";
			 public static string CategoryId = @"category_id";
			 public static string Guid = @"guid";
			 public static string Sort = @"sort";
			 public static string IsMustLogin = @"is_must_login";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

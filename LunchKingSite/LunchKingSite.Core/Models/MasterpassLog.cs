using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the MasterpassLog class.
	/// </summary>
    [Serializable]
	public partial class MasterpassLogCollection : RepositoryList<MasterpassLog, MasterpassLogCollection>
	{	   
		public MasterpassLogCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>MasterpassLogCollection</returns>
		public MasterpassLogCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                MasterpassLog o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
	}
	
	
	/// <summary>
	/// This is an ActiveRecord class which wraps the masterpass_log table.
	/// </summary>
	
	[Serializable]
	public partial class MasterpassLog : RepositoryRecord<MasterpassLog>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public MasterpassLog()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public MasterpassLog(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("masterpass_log", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarOrderGuid = new TableSchema.TableColumn(schema);
				colvarOrderGuid.ColumnName = "order_guid";
				colvarOrderGuid.DataType = DbType.Guid;
				colvarOrderGuid.MaxLength = 0;
				colvarOrderGuid.AutoIncrement = false;
				colvarOrderGuid.IsNullable = false;
				colvarOrderGuid.IsPrimaryKey = false;
				colvarOrderGuid.IsForeignKey = false;
				colvarOrderGuid.IsReadOnly = false;
				colvarOrderGuid.DefaultSetting = @"";
				colvarOrderGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderGuid);
				
				TableSchema.TableColumn colvarResponseContent = new TableSchema.TableColumn(schema);
				colvarResponseContent.ColumnName = "response_content";
				colvarResponseContent.DataType = DbType.String;
				colvarResponseContent.MaxLength = -1;
				colvarResponseContent.AutoIncrement = false;
				colvarResponseContent.IsNullable = true;
				colvarResponseContent.IsPrimaryKey = false;
				colvarResponseContent.IsForeignKey = false;
				colvarResponseContent.IsReadOnly = false;
				colvarResponseContent.DefaultSetting = @"";
				colvarResponseContent.ForeignKeyTableName = "";
				schema.Columns.Add(colvarResponseContent);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				
						colvarCreateTime.DefaultSetting = @"(getdate())";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarUniqueid = new TableSchema.TableColumn(schema);
				colvarUniqueid.ColumnName = "uniqueid";
				colvarUniqueid.DataType = DbType.Int32;
				colvarUniqueid.MaxLength = 0;
				colvarUniqueid.AutoIncrement = false;
				colvarUniqueid.IsNullable = false;
				colvarUniqueid.IsPrimaryKey = false;
				colvarUniqueid.IsForeignKey = false;
				colvarUniqueid.IsReadOnly = false;
				colvarUniqueid.DefaultSetting = @"";
				colvarUniqueid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUniqueid);
				
				TableSchema.TableColumn colvarSendBonus = new TableSchema.TableColumn(schema);
				colvarSendBonus.ColumnName = "send_bonus";
				colvarSendBonus.DataType = DbType.Boolean;
				colvarSendBonus.MaxLength = 0;
				colvarSendBonus.AutoIncrement = false;
				colvarSendBonus.IsNullable = false;
				colvarSendBonus.IsPrimaryKey = false;
				colvarSendBonus.IsForeignKey = false;
				colvarSendBonus.IsReadOnly = false;
				
						colvarSendBonus.DefaultSetting = @"((0))";
				colvarSendBonus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSendBonus);
				
				TableSchema.TableColumn colvarIndicator = new TableSchema.TableColumn(schema);
				colvarIndicator.ColumnName = "indicator";
				colvarIndicator.DataType = DbType.String;
				colvarIndicator.MaxLength = 10;
				colvarIndicator.AutoIncrement = false;
				colvarIndicator.IsNullable = true;
				colvarIndicator.IsPrimaryKey = false;
				colvarIndicator.IsForeignKey = false;
				colvarIndicator.IsReadOnly = false;
				colvarIndicator.DefaultSetting = @"";
				colvarIndicator.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIndicator);
				
				TableSchema.TableColumn colvarOauthToken = new TableSchema.TableColumn(schema);
				colvarOauthToken.ColumnName = "oauth_token";
				colvarOauthToken.DataType = DbType.AnsiString;
				colvarOauthToken.MaxLength = 50;
				colvarOauthToken.AutoIncrement = false;
				colvarOauthToken.IsNullable = true;
				colvarOauthToken.IsPrimaryKey = false;
				colvarOauthToken.IsForeignKey = false;
				colvarOauthToken.IsReadOnly = false;
				colvarOauthToken.DefaultSetting = @"";
				colvarOauthToken.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOauthToken);
				
				TableSchema.TableColumn colvarMasterpassTransid = new TableSchema.TableColumn(schema);
				colvarMasterpassTransid.ColumnName = "masterpass_transid";
				colvarMasterpassTransid.DataType = DbType.AnsiString;
				colvarMasterpassTransid.MaxLength = 20;
				colvarMasterpassTransid.AutoIncrement = false;
				colvarMasterpassTransid.IsNullable = true;
				colvarMasterpassTransid.IsPrimaryKey = false;
				colvarMasterpassTransid.IsForeignKey = false;
				colvarMasterpassTransid.IsReadOnly = false;
				colvarMasterpassTransid.DefaultSetting = @"";
				colvarMasterpassTransid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMasterpassTransid);
				
				TableSchema.TableColumn colvarCardBrandId = new TableSchema.TableColumn(schema);
				colvarCardBrandId.ColumnName = "card_brand_id";
				colvarCardBrandId.DataType = DbType.String;
				colvarCardBrandId.MaxLength = 50;
				colvarCardBrandId.AutoIncrement = false;
				colvarCardBrandId.IsNullable = true;
				colvarCardBrandId.IsPrimaryKey = false;
				colvarCardBrandId.IsForeignKey = false;
				colvarCardBrandId.IsReadOnly = false;
				colvarCardBrandId.DefaultSetting = @"";
				colvarCardBrandId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCardBrandId);
				
				TableSchema.TableColumn colvarCardBrandName = new TableSchema.TableColumn(schema);
				colvarCardBrandName.ColumnName = "card_brand_name";
				colvarCardBrandName.DataType = DbType.String;
				colvarCardBrandName.MaxLength = 50;
				colvarCardBrandName.AutoIncrement = false;
				colvarCardBrandName.IsNullable = true;
				colvarCardBrandName.IsPrimaryKey = false;
				colvarCardBrandName.IsForeignKey = false;
				colvarCardBrandName.IsReadOnly = false;
				colvarCardBrandName.DefaultSetting = @"";
				colvarCardBrandName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCardBrandName);
				
				TableSchema.TableColumn colvarAccountNumber = new TableSchema.TableColumn(schema);
				colvarAccountNumber.ColumnName = "account_number";
				colvarAccountNumber.DataType = DbType.AnsiString;
				colvarAccountNumber.MaxLength = 100;
				colvarAccountNumber.AutoIncrement = false;
				colvarAccountNumber.IsNullable = true;
				colvarAccountNumber.IsPrimaryKey = false;
				colvarAccountNumber.IsForeignKey = false;
				colvarAccountNumber.IsReadOnly = false;
				colvarAccountNumber.DefaultSetting = @"";
				colvarAccountNumber.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAccountNumber);
				
				TableSchema.TableColumn colvarCardHolder = new TableSchema.TableColumn(schema);
				colvarCardHolder.ColumnName = "card_holder";
				colvarCardHolder.DataType = DbType.String;
				colvarCardHolder.MaxLength = 50;
				colvarCardHolder.AutoIncrement = false;
				colvarCardHolder.IsNullable = true;
				colvarCardHolder.IsPrimaryKey = false;
				colvarCardHolder.IsForeignKey = false;
				colvarCardHolder.IsReadOnly = false;
				colvarCardHolder.DefaultSetting = @"";
				colvarCardHolder.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCardHolder);
				
				TableSchema.TableColumn colvarSubtotal = new TableSchema.TableColumn(schema);
				colvarSubtotal.ColumnName = "subtotal";
				colvarSubtotal.DataType = DbType.Int32;
				colvarSubtotal.MaxLength = 0;
				colvarSubtotal.AutoIncrement = false;
				colvarSubtotal.IsNullable = true;
				colvarSubtotal.IsPrimaryKey = false;
				colvarSubtotal.IsForeignKey = false;
				colvarSubtotal.IsReadOnly = false;
				colvarSubtotal.DefaultSetting = @"";
				colvarSubtotal.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSubtotal);
				
				TableSchema.TableColumn colvarInfoNumber = new TableSchema.TableColumn(schema);
				colvarInfoNumber.ColumnName = "info_number";
				colvarInfoNumber.DataType = DbType.AnsiString;
				colvarInfoNumber.MaxLength = 6;
				colvarInfoNumber.AutoIncrement = false;
				colvarInfoNumber.IsNullable = true;
				colvarInfoNumber.IsPrimaryKey = false;
				colvarInfoNumber.IsForeignKey = false;
				colvarInfoNumber.IsReadOnly = false;
				colvarInfoNumber.DefaultSetting = @"";
				colvarInfoNumber.ForeignKeyTableName = "";
				schema.Columns.Add(colvarInfoNumber);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("masterpass_log",schema);
			}
		}
		#endregion
		
		#region Props
		
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		
		[XmlAttribute("OrderGuid")]
		[Bindable(true)]
		public Guid OrderGuid 
		{
			get { return GetColumnValue<Guid>(Columns.OrderGuid); }
			set { SetColumnValue(Columns.OrderGuid, value); }
		}
		
		[XmlAttribute("ResponseContent")]
		[Bindable(true)]
		public string ResponseContent 
		{
			get { return GetColumnValue<string>(Columns.ResponseContent); }
			set { SetColumnValue(Columns.ResponseContent, value); }
		}
		
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		
		[XmlAttribute("Uniqueid")]
		[Bindable(true)]
		public int Uniqueid 
		{
			get { return GetColumnValue<int>(Columns.Uniqueid); }
			set { SetColumnValue(Columns.Uniqueid, value); }
		}
		
		[XmlAttribute("SendBonus")]
		[Bindable(true)]
		public bool SendBonus 
		{
			get { return GetColumnValue<bool>(Columns.SendBonus); }
			set { SetColumnValue(Columns.SendBonus, value); }
		}
		
		[XmlAttribute("Indicator")]
		[Bindable(true)]
		public string Indicator 
		{
			get { return GetColumnValue<string>(Columns.Indicator); }
			set { SetColumnValue(Columns.Indicator, value); }
		}
		
		[XmlAttribute("OauthToken")]
		[Bindable(true)]
		public string OauthToken 
		{
			get { return GetColumnValue<string>(Columns.OauthToken); }
			set { SetColumnValue(Columns.OauthToken, value); }
		}
		
		[XmlAttribute("MasterpassTransid")]
		[Bindable(true)]
		public string MasterpassTransid 
		{
			get { return GetColumnValue<string>(Columns.MasterpassTransid); }
			set { SetColumnValue(Columns.MasterpassTransid, value); }
		}
		
		[XmlAttribute("CardBrandId")]
		[Bindable(true)]
		public string CardBrandId 
		{
			get { return GetColumnValue<string>(Columns.CardBrandId); }
			set { SetColumnValue(Columns.CardBrandId, value); }
		}
		
		[XmlAttribute("CardBrandName")]
		[Bindable(true)]
		public string CardBrandName 
		{
			get { return GetColumnValue<string>(Columns.CardBrandName); }
			set { SetColumnValue(Columns.CardBrandName, value); }
		}
		
		[XmlAttribute("AccountNumber")]
		[Bindable(true)]
		public string AccountNumber 
		{
			get { return GetColumnValue<string>(Columns.AccountNumber); }
			set { SetColumnValue(Columns.AccountNumber, value); }
		}
		
		[XmlAttribute("CardHolder")]
		[Bindable(true)]
		public string CardHolder 
		{
			get { return GetColumnValue<string>(Columns.CardHolder); }
			set { SetColumnValue(Columns.CardHolder, value); }
		}
		
		[XmlAttribute("Subtotal")]
		[Bindable(true)]
		public int? Subtotal 
		{
			get { return GetColumnValue<int?>(Columns.Subtotal); }
			set { SetColumnValue(Columns.Subtotal, value); }
		}
		
		[XmlAttribute("InfoNumber")]
		[Bindable(true)]
		public string InfoNumber 
		{
			get { return GetColumnValue<string>(Columns.InfoNumber); }
			set { SetColumnValue(Columns.InfoNumber, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn OrderGuidColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn ResponseContentColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn UniqueidColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn SendBonusColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn IndicatorColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn OauthTokenColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn MasterpassTransidColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn CardBrandIdColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn CardBrandNameColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        public static TableSchema.TableColumn AccountNumberColumn
        {
            get { return Schema.Columns[11]; }
        }
        
        
        
        public static TableSchema.TableColumn CardHolderColumn
        {
            get { return Schema.Columns[12]; }
        }
        
        
        
        public static TableSchema.TableColumn SubtotalColumn
        {
            get { return Schema.Columns[13]; }
        }
        
        
        
        public static TableSchema.TableColumn InfoNumberColumn
        {
            get { return Schema.Columns[14]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string OrderGuid = @"order_guid";
			 public static string ResponseContent = @"response_content";
			 public static string CreateTime = @"create_time";
			 public static string Uniqueid = @"uniqueid";
			 public static string SendBonus = @"send_bonus";
			 public static string Indicator = @"indicator";
			 public static string OauthToken = @"oauth_token";
			 public static string MasterpassTransid = @"masterpass_transid";
			 public static string CardBrandId = @"card_brand_id";
			 public static string CardBrandName = @"card_brand_name";
			 public static string AccountNumber = @"account_number";
			 public static string CardHolder = @"card_holder";
			 public static string Subtotal = @"subtotal";
			 public static string InfoNumber = @"info_number";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

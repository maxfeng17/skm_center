using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the OrderCorresponding class.
	/// </summary>
    [Serializable]
	public partial class OrderCorrespondingCollection : RepositoryList<OrderCorresponding, OrderCorrespondingCollection>
	{	   
		public OrderCorrespondingCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>OrderCorrespondingCollection</returns>
		public OrderCorrespondingCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                OrderCorresponding o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the order_corresponding table.
	/// </summary>
	[Serializable]
	public partial class OrderCorresponding : RepositoryRecord<OrderCorresponding>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public OrderCorresponding()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public OrderCorresponding(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("order_corresponding", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarOrderGuid = new TableSchema.TableColumn(schema);
				colvarOrderGuid.ColumnName = "order_guid";
				colvarOrderGuid.DataType = DbType.Guid;
				colvarOrderGuid.MaxLength = 0;
				colvarOrderGuid.AutoIncrement = false;
				colvarOrderGuid.IsNullable = false;
				colvarOrderGuid.IsPrimaryKey = false;
				colvarOrderGuid.IsForeignKey = false;
				colvarOrderGuid.IsReadOnly = false;
				colvarOrderGuid.DefaultSetting = @"";
				colvarOrderGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderGuid);
				
				TableSchema.TableColumn colvarOrderId = new TableSchema.TableColumn(schema);
				colvarOrderId.ColumnName = "order_id";
				colvarOrderId.DataType = DbType.AnsiString;
				colvarOrderId.MaxLength = 50;
				colvarOrderId.AutoIncrement = false;
				colvarOrderId.IsNullable = false;
				colvarOrderId.IsPrimaryKey = false;
				colvarOrderId.IsForeignKey = false;
				colvarOrderId.IsReadOnly = false;
				colvarOrderId.DefaultSetting = @"";
				colvarOrderId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderId);
				
				TableSchema.TableColumn colvarBusinessHourGuid = new TableSchema.TableColumn(schema);
				colvarBusinessHourGuid.ColumnName = "business_hour_guid";
				colvarBusinessHourGuid.DataType = DbType.Guid;
				colvarBusinessHourGuid.MaxLength = 0;
				colvarBusinessHourGuid.AutoIncrement = false;
				colvarBusinessHourGuid.IsNullable = false;
				colvarBusinessHourGuid.IsPrimaryKey = false;
				colvarBusinessHourGuid.IsForeignKey = false;
				colvarBusinessHourGuid.IsReadOnly = false;
				colvarBusinessHourGuid.DefaultSetting = @"";
				colvarBusinessHourGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBusinessHourGuid);
				
				TableSchema.TableColumn colvarUniqueId = new TableSchema.TableColumn(schema);
				colvarUniqueId.ColumnName = "unique_id";
				colvarUniqueId.DataType = DbType.Int32;
				colvarUniqueId.MaxLength = 0;
				colvarUniqueId.AutoIncrement = false;
				colvarUniqueId.IsNullable = false;
				colvarUniqueId.IsPrimaryKey = false;
				colvarUniqueId.IsForeignKey = false;
				colvarUniqueId.IsReadOnly = false;
				colvarUniqueId.DefaultSetting = @"";
				colvarUniqueId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUniqueId);
				
				TableSchema.TableColumn colvarRelatedOrderId = new TableSchema.TableColumn(schema);
				colvarRelatedOrderId.ColumnName = "related_order_id";
				colvarRelatedOrderId.DataType = DbType.AnsiString;
				colvarRelatedOrderId.MaxLength = 50;
				colvarRelatedOrderId.AutoIncrement = false;
				colvarRelatedOrderId.IsNullable = false;
				colvarRelatedOrderId.IsPrimaryKey = false;
				colvarRelatedOrderId.IsForeignKey = false;
				colvarRelatedOrderId.IsReadOnly = false;
				colvarRelatedOrderId.DefaultSetting = @"";
				colvarRelatedOrderId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarRelatedOrderId);
				
				TableSchema.TableColumn colvarMobile = new TableSchema.TableColumn(schema);
				colvarMobile.ColumnName = "mobile";
				colvarMobile.DataType = DbType.AnsiString;
				colvarMobile.MaxLength = 50;
				colvarMobile.AutoIncrement = false;
				colvarMobile.IsNullable = false;
				colvarMobile.IsPrimaryKey = false;
				colvarMobile.IsForeignKey = false;
				colvarMobile.IsReadOnly = false;
				colvarMobile.DefaultSetting = @"";
				colvarMobile.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMobile);
				
				TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
				colvarStatus.ColumnName = "status";
				colvarStatus.DataType = DbType.Int32;
				colvarStatus.MaxLength = 0;
				colvarStatus.AutoIncrement = false;
				colvarStatus.IsNullable = false;
				colvarStatus.IsPrimaryKey = false;
				colvarStatus.IsForeignKey = false;
				colvarStatus.IsReadOnly = false;
				
						colvarStatus.DefaultSetting = @"((0))";
				colvarStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStatus);
				
				TableSchema.TableColumn colvarCreatedTime = new TableSchema.TableColumn(schema);
				colvarCreatedTime.ColumnName = "created_time";
				colvarCreatedTime.DataType = DbType.DateTime;
				colvarCreatedTime.MaxLength = 0;
				colvarCreatedTime.AutoIncrement = false;
				colvarCreatedTime.IsNullable = false;
				colvarCreatedTime.IsPrimaryKey = false;
				colvarCreatedTime.IsForeignKey = false;
				colvarCreatedTime.IsReadOnly = false;
				colvarCreatedTime.DefaultSetting = @"";
				colvarCreatedTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreatedTime);
				
				TableSchema.TableColumn colvarUserId = new TableSchema.TableColumn(schema);
				colvarUserId.ColumnName = "user_id";
				colvarUserId.DataType = DbType.Int32;
				colvarUserId.MaxLength = 0;
				colvarUserId.AutoIncrement = false;
				colvarUserId.IsNullable = false;
				colvarUserId.IsPrimaryKey = false;
				colvarUserId.IsForeignKey = false;
				colvarUserId.IsReadOnly = false;
				colvarUserId.DefaultSetting = @"";
				colvarUserId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUserId);
				
				TableSchema.TableColumn colvarMemo = new TableSchema.TableColumn(schema);
				colvarMemo.ColumnName = "memo";
				colvarMemo.DataType = DbType.String;
				colvarMemo.MaxLength = 100;
				colvarMemo.AutoIncrement = false;
				colvarMemo.IsNullable = true;
				colvarMemo.IsPrimaryKey = false;
				colvarMemo.IsForeignKey = false;
				colvarMemo.IsReadOnly = false;
				colvarMemo.DefaultSetting = @"";
				colvarMemo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMemo);
				
				TableSchema.TableColumn colvarType = new TableSchema.TableColumn(schema);
				colvarType.ColumnName = "type";
				colvarType.DataType = DbType.Int32;
				colvarType.MaxLength = 0;
				colvarType.AutoIncrement = false;
				colvarType.IsNullable = false;
				colvarType.IsPrimaryKey = false;
				colvarType.IsForeignKey = false;
				colvarType.IsReadOnly = false;
				
						colvarType.DefaultSetting = @"((0))";
				colvarType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarType);
				
				TableSchema.TableColumn colvarTokenClientId = new TableSchema.TableColumn(schema);
				colvarTokenClientId.ColumnName = "token_client_id";
				colvarTokenClientId.DataType = DbType.Int32;
				colvarTokenClientId.MaxLength = 0;
				colvarTokenClientId.AutoIncrement = false;
				colvarTokenClientId.IsNullable = false;
				colvarTokenClientId.IsPrimaryKey = false;
				colvarTokenClientId.IsForeignKey = false;
				colvarTokenClientId.IsReadOnly = false;
				
						colvarTokenClientId.DefaultSetting = @"((0))";
				colvarTokenClientId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTokenClientId);
				
				TableSchema.TableColumn colvarOrderCheck = new TableSchema.TableColumn(schema);
				colvarOrderCheck.ColumnName = "order_check";
				colvarOrderCheck.DataType = DbType.Int32;
				colvarOrderCheck.MaxLength = 0;
				colvarOrderCheck.AutoIncrement = false;
				colvarOrderCheck.IsNullable = true;
				colvarOrderCheck.IsPrimaryKey = false;
				colvarOrderCheck.IsForeignKey = false;
				colvarOrderCheck.IsReadOnly = false;
				colvarOrderCheck.DefaultSetting = @"";
				colvarOrderCheck.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderCheck);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("order_corresponding",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("OrderGuid")]
		[Bindable(true)]
		public Guid OrderGuid 
		{
			get { return GetColumnValue<Guid>(Columns.OrderGuid); }
			set { SetColumnValue(Columns.OrderGuid, value); }
		}
		  
		[XmlAttribute("OrderId")]
		[Bindable(true)]
		public string OrderId 
		{
			get { return GetColumnValue<string>(Columns.OrderId); }
			set { SetColumnValue(Columns.OrderId, value); }
		}
		  
		[XmlAttribute("BusinessHourGuid")]
		[Bindable(true)]
		public Guid BusinessHourGuid 
		{
			get { return GetColumnValue<Guid>(Columns.BusinessHourGuid); }
			set { SetColumnValue(Columns.BusinessHourGuid, value); }
		}
		  
		[XmlAttribute("UniqueId")]
		[Bindable(true)]
		public int UniqueId 
		{
			get { return GetColumnValue<int>(Columns.UniqueId); }
			set { SetColumnValue(Columns.UniqueId, value); }
		}
		  
		[XmlAttribute("RelatedOrderId")]
		[Bindable(true)]
		public string RelatedOrderId 
		{
			get { return GetColumnValue<string>(Columns.RelatedOrderId); }
			set { SetColumnValue(Columns.RelatedOrderId, value); }
		}
		  
		[XmlAttribute("Mobile")]
		[Bindable(true)]
		public string Mobile 
		{
			get { return GetColumnValue<string>(Columns.Mobile); }
			set { SetColumnValue(Columns.Mobile, value); }
		}
		  
		[XmlAttribute("Status")]
		[Bindable(true)]
		public int Status 
		{
			get { return GetColumnValue<int>(Columns.Status); }
			set { SetColumnValue(Columns.Status, value); }
		}
		  
		[XmlAttribute("CreatedTime")]
		[Bindable(true)]
		public DateTime CreatedTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreatedTime); }
			set { SetColumnValue(Columns.CreatedTime, value); }
		}
		  
		[XmlAttribute("UserId")]
		[Bindable(true)]
		public int UserId 
		{
			get { return GetColumnValue<int>(Columns.UserId); }
			set { SetColumnValue(Columns.UserId, value); }
		}
		  
		[XmlAttribute("Memo")]
		[Bindable(true)]
		public string Memo 
		{
			get { return GetColumnValue<string>(Columns.Memo); }
			set { SetColumnValue(Columns.Memo, value); }
		}
		  
		[XmlAttribute("Type")]
		[Bindable(true)]
		public int Type 
		{
			get { return GetColumnValue<int>(Columns.Type); }
			set { SetColumnValue(Columns.Type, value); }
		}
		  
		[XmlAttribute("TokenClientId")]
		[Bindable(true)]
		public int TokenClientId 
		{
			get { return GetColumnValue<int>(Columns.TokenClientId); }
			set { SetColumnValue(Columns.TokenClientId, value); }
		}
		  
		[XmlAttribute("OrderCheck")]
		[Bindable(true)]
		public int? OrderCheck 
		{
			get { return GetColumnValue<int?>(Columns.OrderCheck); }
			set { SetColumnValue(Columns.OrderCheck, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn OrderGuidColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn OrderIdColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn BusinessHourGuidColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn UniqueIdColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn RelatedOrderIdColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn MobileColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn StatusColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn CreatedTimeColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn UserIdColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn MemoColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        public static TableSchema.TableColumn TypeColumn
        {
            get { return Schema.Columns[11]; }
        }
        
        
        
        public static TableSchema.TableColumn TokenClientIdColumn
        {
            get { return Schema.Columns[12]; }
        }
        
        
        
        public static TableSchema.TableColumn OrderCheckColumn
        {
            get { return Schema.Columns[13]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string OrderGuid = @"order_guid";
			 public static string OrderId = @"order_id";
			 public static string BusinessHourGuid = @"business_hour_guid";
			 public static string UniqueId = @"unique_id";
			 public static string RelatedOrderId = @"related_order_id";
			 public static string Mobile = @"mobile";
			 public static string Status = @"status";
			 public static string CreatedTime = @"created_time";
			 public static string UserId = @"user_id";
			 public static string Memo = @"memo";
			 public static string Type = @"type";
			 public static string TokenClientId = @"token_client_id";
			 public static string OrderCheck = @"order_check";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

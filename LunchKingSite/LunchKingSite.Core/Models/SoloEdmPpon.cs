using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the SoloEdmPpon class.
	/// </summary>
    [Serializable]
	public partial class SoloEdmPponCollection : RepositoryList<SoloEdmPpon, SoloEdmPponCollection>
	{	   
		public SoloEdmPponCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>SoloEdmPponCollection</returns>
		public SoloEdmPponCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                SoloEdmPpon o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the solo_edm_ppon table.
	/// </summary>
	[Serializable]
	public partial class SoloEdmPpon : RepositoryRecord<SoloEdmPpon>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public SoloEdmPpon()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public SoloEdmPpon(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("solo_edm_ppon", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarMainId = new TableSchema.TableColumn(schema);
				colvarMainId.ColumnName = "main_id";
				colvarMainId.DataType = DbType.Int32;
				colvarMainId.MaxLength = 0;
				colvarMainId.AutoIncrement = false;
				colvarMainId.IsNullable = false;
				colvarMainId.IsPrimaryKey = false;
				colvarMainId.IsForeignKey = false;
				colvarMainId.IsReadOnly = false;
				colvarMainId.DefaultSetting = @"";
				colvarMainId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMainId);
				
				TableSchema.TableColumn colvarBid = new TableSchema.TableColumn(schema);
				colvarBid.ColumnName = "bid";
				colvarBid.DataType = DbType.Guid;
				colvarBid.MaxLength = 0;
				colvarBid.AutoIncrement = false;
				colvarBid.IsNullable = false;
				colvarBid.IsPrimaryKey = false;
				colvarBid.IsForeignKey = false;
				colvarBid.IsReadOnly = false;
				colvarBid.DefaultSetting = @"";
				colvarBid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBid);
				
				TableSchema.TableColumn colvarBlackTitle = new TableSchema.TableColumn(schema);
				colvarBlackTitle.ColumnName = "black_title";
				colvarBlackTitle.DataType = DbType.String;
				colvarBlackTitle.MaxLength = 500;
				colvarBlackTitle.AutoIncrement = false;
				colvarBlackTitle.IsNullable = false;
				colvarBlackTitle.IsPrimaryKey = false;
				colvarBlackTitle.IsForeignKey = false;
				colvarBlackTitle.IsReadOnly = false;
				colvarBlackTitle.DefaultSetting = @"";
				colvarBlackTitle.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBlackTitle);
				
				TableSchema.TableColumn colvarOrangeTitle = new TableSchema.TableColumn(schema);
				colvarOrangeTitle.ColumnName = "orange_title";
				colvarOrangeTitle.DataType = DbType.String;
				colvarOrangeTitle.MaxLength = 500;
				colvarOrangeTitle.AutoIncrement = false;
				colvarOrangeTitle.IsNullable = false;
				colvarOrangeTitle.IsPrimaryKey = false;
				colvarOrangeTitle.IsForeignKey = false;
				colvarOrangeTitle.IsReadOnly = false;
				colvarOrangeTitle.DefaultSetting = @"";
				colvarOrangeTitle.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrangeTitle);
				
				TableSchema.TableColumn colvarUrl = new TableSchema.TableColumn(schema);
				colvarUrl.ColumnName = "url";
				colvarUrl.DataType = DbType.String;
				colvarUrl.MaxLength = 200;
				colvarUrl.AutoIncrement = false;
				colvarUrl.IsNullable = false;
				colvarUrl.IsPrimaryKey = false;
				colvarUrl.IsForeignKey = false;
				colvarUrl.IsReadOnly = false;
				colvarUrl.DefaultSetting = @"";
				colvarUrl.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUrl);
				
				TableSchema.TableColumn colvarGroupId = new TableSchema.TableColumn(schema);
				colvarGroupId.ColumnName = "group_id";
				colvarGroupId.DataType = DbType.Int32;
				colvarGroupId.MaxLength = 0;
				colvarGroupId.AutoIncrement = false;
				colvarGroupId.IsNullable = false;
				colvarGroupId.IsPrimaryKey = false;
				colvarGroupId.IsForeignKey = false;
				colvarGroupId.IsReadOnly = false;
				colvarGroupId.DefaultSetting = @"";
				colvarGroupId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGroupId);
				
				TableSchema.TableColumn colvarGroupName = new TableSchema.TableColumn(schema);
				colvarGroupName.ColumnName = "group_name";
				colvarGroupName.DataType = DbType.String;
				colvarGroupName.MaxLength = 100;
				colvarGroupName.AutoIncrement = false;
				colvarGroupName.IsNullable = false;
				colvarGroupName.IsPrimaryKey = false;
				colvarGroupName.IsForeignKey = false;
				colvarGroupName.IsReadOnly = false;
				colvarGroupName.DefaultSetting = @"";
				colvarGroupName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGroupName);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("solo_edm_ppon",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("MainId")]
		[Bindable(true)]
		public int MainId 
		{
			get { return GetColumnValue<int>(Columns.MainId); }
			set { SetColumnValue(Columns.MainId, value); }
		}
		  
		[XmlAttribute("Bid")]
		[Bindable(true)]
		public Guid Bid 
		{
			get { return GetColumnValue<Guid>(Columns.Bid); }
			set { SetColumnValue(Columns.Bid, value); }
		}
		  
		[XmlAttribute("BlackTitle")]
		[Bindable(true)]
		public string BlackTitle 
		{
			get { return GetColumnValue<string>(Columns.BlackTitle); }
			set { SetColumnValue(Columns.BlackTitle, value); }
		}
		  
		[XmlAttribute("OrangeTitle")]
		[Bindable(true)]
		public string OrangeTitle 
		{
			get { return GetColumnValue<string>(Columns.OrangeTitle); }
			set { SetColumnValue(Columns.OrangeTitle, value); }
		}
		  
		[XmlAttribute("Url")]
		[Bindable(true)]
		public string Url 
		{
			get { return GetColumnValue<string>(Columns.Url); }
			set { SetColumnValue(Columns.Url, value); }
		}
		  
		[XmlAttribute("GroupId")]
		[Bindable(true)]
		public int GroupId 
		{
			get { return GetColumnValue<int>(Columns.GroupId); }
			set { SetColumnValue(Columns.GroupId, value); }
		}
		  
		[XmlAttribute("GroupName")]
		[Bindable(true)]
		public string GroupName 
		{
			get { return GetColumnValue<string>(Columns.GroupName); }
			set { SetColumnValue(Columns.GroupName, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn MainIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn BidColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn BlackTitleColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn OrangeTitleColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn UrlColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn GroupIdColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn GroupNameColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string MainId = @"main_id";
			 public static string Bid = @"bid";
			 public static string BlackTitle = @"black_title";
			 public static string OrangeTitle = @"orange_title";
			 public static string Url = @"url";
			 public static string GroupId = @"group_id";
			 public static string GroupName = @"group_name";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

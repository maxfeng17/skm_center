using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the PaymentTypeAmount class.
	/// </summary>
    [Serializable]
	public partial class PaymentTypeAmountCollection : RepositoryList<PaymentTypeAmount, PaymentTypeAmountCollection>
	{	   
		public PaymentTypeAmountCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>PaymentTypeAmountCollection</returns>
		public PaymentTypeAmountCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                PaymentTypeAmount o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the payment_type_amount table.
	/// </summary>
	[Serializable]
	public partial class PaymentTypeAmount : RepositoryRecord<PaymentTypeAmount>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public PaymentTypeAmount()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public PaymentTypeAmount(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("payment_type_amount", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarTrustId = new TableSchema.TableColumn(schema);
				colvarTrustId.ColumnName = "trust_id";
				colvarTrustId.DataType = DbType.Guid;
				colvarTrustId.MaxLength = 0;
				colvarTrustId.AutoIncrement = false;
				colvarTrustId.IsNullable = false;
				colvarTrustId.IsPrimaryKey = false;
				colvarTrustId.IsForeignKey = true;
				colvarTrustId.IsReadOnly = false;
				colvarTrustId.DefaultSetting = @"";
				
					colvarTrustId.ForeignKeyTableName = "cash_trust_log";
				schema.Columns.Add(colvarTrustId);
				
				TableSchema.TableColumn colvarPaymentType = new TableSchema.TableColumn(schema);
				colvarPaymentType.ColumnName = "payment_type";
				colvarPaymentType.DataType = DbType.Int32;
				colvarPaymentType.MaxLength = 0;
				colvarPaymentType.AutoIncrement = false;
				colvarPaymentType.IsNullable = false;
				colvarPaymentType.IsPrimaryKey = false;
				colvarPaymentType.IsForeignKey = false;
				colvarPaymentType.IsReadOnly = false;
				colvarPaymentType.DefaultSetting = @"";
				colvarPaymentType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPaymentType);
				
				TableSchema.TableColumn colvarAmount = new TableSchema.TableColumn(schema);
				colvarAmount.ColumnName = "amount";
				colvarAmount.DataType = DbType.Int32;
				colvarAmount.MaxLength = 0;
				colvarAmount.AutoIncrement = false;
				colvarAmount.IsNullable = false;
				colvarAmount.IsPrimaryKey = false;
				colvarAmount.IsForeignKey = false;
				colvarAmount.IsReadOnly = false;
				colvarAmount.DefaultSetting = @"";
				colvarAmount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAmount);
				
				TableSchema.TableColumn colvarOrderGuid = new TableSchema.TableColumn(schema);
				colvarOrderGuid.ColumnName = "order_guid";
				colvarOrderGuid.DataType = DbType.Guid;
				colvarOrderGuid.MaxLength = 0;
				colvarOrderGuid.AutoIncrement = false;
				colvarOrderGuid.IsNullable = false;
				colvarOrderGuid.IsPrimaryKey = false;
				colvarOrderGuid.IsForeignKey = false;
				colvarOrderGuid.IsReadOnly = false;
				colvarOrderGuid.DefaultSetting = @"";
				colvarOrderGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderGuid);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("payment_type_amount",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("TrustId")]
		[Bindable(true)]
		public Guid TrustId 
		{
			get { return GetColumnValue<Guid>(Columns.TrustId); }
			set { SetColumnValue(Columns.TrustId, value); }
		}
		  
		[XmlAttribute("PaymentType")]
		[Bindable(true)]
		public int PaymentType 
		{
			get { return GetColumnValue<int>(Columns.PaymentType); }
			set { SetColumnValue(Columns.PaymentType, value); }
		}
		  
		[XmlAttribute("Amount")]
		[Bindable(true)]
		public int Amount 
		{
			get { return GetColumnValue<int>(Columns.Amount); }
			set { SetColumnValue(Columns.Amount, value); }
		}
		  
		[XmlAttribute("OrderGuid")]
		[Bindable(true)]
		public Guid OrderGuid 
		{
			get { return GetColumnValue<Guid>(Columns.OrderGuid); }
			set { SetColumnValue(Columns.OrderGuid, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (1)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn TrustIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn PaymentTypeColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn AmountColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn OrderGuidColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string TrustId = @"trust_id";
			 public static string PaymentType = @"payment_type";
			 public static string Amount = @"amount";
			 public static string OrderGuid = @"order_guid";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

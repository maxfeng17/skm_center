using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewOrderProductDelivery class.
    /// </summary>
    [Serializable]
    public partial class ViewOrderProductDeliveryCollection : ReadOnlyList<ViewOrderProductDelivery, ViewOrderProductDeliveryCollection>
    {        
        public ViewOrderProductDeliveryCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_order_product_delivery view.
    /// </summary>
    [Serializable]
    public partial class ViewOrderProductDelivery : ReadOnlyRecord<ViewOrderProductDelivery>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_order_product_delivery", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = false;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = false;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                
                schema.Columns.Add(colvarId);
                
                TableSchema.TableColumn colvarOrderGuid = new TableSchema.TableColumn(schema);
                colvarOrderGuid.ColumnName = "order_guid";
                colvarOrderGuid.DataType = DbType.Guid;
                colvarOrderGuid.MaxLength = 0;
                colvarOrderGuid.AutoIncrement = false;
                colvarOrderGuid.IsNullable = false;
                colvarOrderGuid.IsPrimaryKey = false;
                colvarOrderGuid.IsForeignKey = false;
                colvarOrderGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderGuid);
                
                TableSchema.TableColumn colvarOrderId = new TableSchema.TableColumn(schema);
                colvarOrderId.ColumnName = "order_id";
                colvarOrderId.DataType = DbType.AnsiString;
                colvarOrderId.MaxLength = 30;
                colvarOrderId.AutoIncrement = false;
                colvarOrderId.IsNullable = true;
                colvarOrderId.IsPrimaryKey = false;
                colvarOrderId.IsForeignKey = false;
                colvarOrderId.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderId);
                
                TableSchema.TableColumn colvarOrderStatus = new TableSchema.TableColumn(schema);
                colvarOrderStatus.ColumnName = "order_status";
                colvarOrderStatus.DataType = DbType.Int32;
                colvarOrderStatus.MaxLength = 0;
                colvarOrderStatus.AutoIncrement = false;
                colvarOrderStatus.IsNullable = false;
                colvarOrderStatus.IsPrimaryKey = false;
                colvarOrderStatus.IsForeignKey = false;
                colvarOrderStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderStatus);
                
                TableSchema.TableColumn colvarProductDeliveryType = new TableSchema.TableColumn(schema);
                colvarProductDeliveryType.ColumnName = "product_delivery_type";
                colvarProductDeliveryType.DataType = DbType.Int32;
                colvarProductDeliveryType.MaxLength = 0;
                colvarProductDeliveryType.AutoIncrement = false;
                colvarProductDeliveryType.IsNullable = false;
                colvarProductDeliveryType.IsPrimaryKey = false;
                colvarProductDeliveryType.IsForeignKey = false;
                colvarProductDeliveryType.IsReadOnly = false;
                
                schema.Columns.Add(colvarProductDeliveryType);
                
                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = false;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarCreateTime);
                
                TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
                colvarModifyTime.ColumnName = "modify_time";
                colvarModifyTime.DataType = DbType.DateTime;
                colvarModifyTime.MaxLength = 0;
                colvarModifyTime.AutoIncrement = false;
                colvarModifyTime.IsNullable = true;
                colvarModifyTime.IsPrimaryKey = false;
                colvarModifyTime.IsForeignKey = false;
                colvarModifyTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarModifyTime);
                
                TableSchema.TableColumn colvarLastShipDate = new TableSchema.TableColumn(schema);
                colvarLastShipDate.ColumnName = "last_ship_date";
                colvarLastShipDate.DataType = DbType.DateTime;
                colvarLastShipDate.MaxLength = 0;
                colvarLastShipDate.AutoIncrement = false;
                colvarLastShipDate.IsNullable = true;
                colvarLastShipDate.IsPrimaryKey = false;
                colvarLastShipDate.IsForeignKey = false;
                colvarLastShipDate.IsReadOnly = false;
                
                schema.Columns.Add(colvarLastShipDate);
                
                TableSchema.TableColumn colvarDeliveryId = new TableSchema.TableColumn(schema);
                colvarDeliveryId.ColumnName = "delivery_id";
                colvarDeliveryId.DataType = DbType.Int32;
                colvarDeliveryId.MaxLength = 0;
                colvarDeliveryId.AutoIncrement = false;
                colvarDeliveryId.IsNullable = true;
                colvarDeliveryId.IsPrimaryKey = false;
                colvarDeliveryId.IsForeignKey = false;
                colvarDeliveryId.IsReadOnly = false;
                
                schema.Columns.Add(colvarDeliveryId);
                
                TableSchema.TableColumn colvarFamilyStoreId = new TableSchema.TableColumn(schema);
                colvarFamilyStoreId.ColumnName = "family_store_id";
                colvarFamilyStoreId.DataType = DbType.String;
                colvarFamilyStoreId.MaxLength = 10;
                colvarFamilyStoreId.AutoIncrement = false;
                colvarFamilyStoreId.IsNullable = true;
                colvarFamilyStoreId.IsPrimaryKey = false;
                colvarFamilyStoreId.IsForeignKey = false;
                colvarFamilyStoreId.IsReadOnly = false;
                
                schema.Columns.Add(colvarFamilyStoreId);
                
                TableSchema.TableColumn colvarFamilyStoreName = new TableSchema.TableColumn(schema);
                colvarFamilyStoreName.ColumnName = "family_store_name";
                colvarFamilyStoreName.DataType = DbType.String;
                colvarFamilyStoreName.MaxLength = 50;
                colvarFamilyStoreName.AutoIncrement = false;
                colvarFamilyStoreName.IsNullable = true;
                colvarFamilyStoreName.IsPrimaryKey = false;
                colvarFamilyStoreName.IsForeignKey = false;
                colvarFamilyStoreName.IsReadOnly = false;
                
                schema.Columns.Add(colvarFamilyStoreName);
                
                TableSchema.TableColumn colvarFamilyStoreTel = new TableSchema.TableColumn(schema);
                colvarFamilyStoreTel.ColumnName = "family_store_tel";
                colvarFamilyStoreTel.DataType = DbType.String;
                colvarFamilyStoreTel.MaxLength = 50;
                colvarFamilyStoreTel.AutoIncrement = false;
                colvarFamilyStoreTel.IsNullable = true;
                colvarFamilyStoreTel.IsPrimaryKey = false;
                colvarFamilyStoreTel.IsForeignKey = false;
                colvarFamilyStoreTel.IsReadOnly = false;
                
                schema.Columns.Add(colvarFamilyStoreTel);
                
                TableSchema.TableColumn colvarFamilyStoreAddr = new TableSchema.TableColumn(schema);
                colvarFamilyStoreAddr.ColumnName = "family_store_addr";
                colvarFamilyStoreAddr.DataType = DbType.String;
                colvarFamilyStoreAddr.MaxLength = 200;
                colvarFamilyStoreAddr.AutoIncrement = false;
                colvarFamilyStoreAddr.IsNullable = true;
                colvarFamilyStoreAddr.IsPrimaryKey = false;
                colvarFamilyStoreAddr.IsForeignKey = false;
                colvarFamilyStoreAddr.IsReadOnly = false;
                
                schema.Columns.Add(colvarFamilyStoreAddr);
                
                TableSchema.TableColumn colvarSevenStoreId = new TableSchema.TableColumn(schema);
                colvarSevenStoreId.ColumnName = "seven_store_id";
                colvarSevenStoreId.DataType = DbType.String;
                colvarSevenStoreId.MaxLength = 10;
                colvarSevenStoreId.AutoIncrement = false;
                colvarSevenStoreId.IsNullable = true;
                colvarSevenStoreId.IsPrimaryKey = false;
                colvarSevenStoreId.IsForeignKey = false;
                colvarSevenStoreId.IsReadOnly = false;
                
                schema.Columns.Add(colvarSevenStoreId);
                
                TableSchema.TableColumn colvarSevenStoreName = new TableSchema.TableColumn(schema);
                colvarSevenStoreName.ColumnName = "seven_store_name";
                colvarSevenStoreName.DataType = DbType.String;
                colvarSevenStoreName.MaxLength = 50;
                colvarSevenStoreName.AutoIncrement = false;
                colvarSevenStoreName.IsNullable = true;
                colvarSevenStoreName.IsPrimaryKey = false;
                colvarSevenStoreName.IsForeignKey = false;
                colvarSevenStoreName.IsReadOnly = false;
                
                schema.Columns.Add(colvarSevenStoreName);
                
                TableSchema.TableColumn colvarSevenStoreTel = new TableSchema.TableColumn(schema);
                colvarSevenStoreTel.ColumnName = "seven_store_tel";
                colvarSevenStoreTel.DataType = DbType.String;
                colvarSevenStoreTel.MaxLength = 50;
                colvarSevenStoreTel.AutoIncrement = false;
                colvarSevenStoreTel.IsNullable = true;
                colvarSevenStoreTel.IsPrimaryKey = false;
                colvarSevenStoreTel.IsForeignKey = false;
                colvarSevenStoreTel.IsReadOnly = false;
                
                schema.Columns.Add(colvarSevenStoreTel);
                
                TableSchema.TableColumn colvarSevenStoreAddr = new TableSchema.TableColumn(schema);
                colvarSevenStoreAddr.ColumnName = "seven_store_addr";
                colvarSevenStoreAddr.DataType = DbType.String;
                colvarSevenStoreAddr.MaxLength = 200;
                colvarSevenStoreAddr.AutoIncrement = false;
                colvarSevenStoreAddr.IsNullable = true;
                colvarSevenStoreAddr.IsPrimaryKey = false;
                colvarSevenStoreAddr.IsForeignKey = false;
                colvarSevenStoreAddr.IsReadOnly = false;
                
                schema.Columns.Add(colvarSevenStoreAddr);
                
                TableSchema.TableColumn colvarGoodsStatus = new TableSchema.TableColumn(schema);
                colvarGoodsStatus.ColumnName = "goods_status";
                colvarGoodsStatus.DataType = DbType.Int32;
                colvarGoodsStatus.MaxLength = 0;
                colvarGoodsStatus.AutoIncrement = false;
                colvarGoodsStatus.IsNullable = false;
                colvarGoodsStatus.IsPrimaryKey = false;
                colvarGoodsStatus.IsForeignKey = false;
                colvarGoodsStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarGoodsStatus);
                
                TableSchema.TableColumn colvarGoodsStatusMessage = new TableSchema.TableColumn(schema);
                colvarGoodsStatusMessage.ColumnName = "goods_status_message";
                colvarGoodsStatusMessage.DataType = DbType.String;
                colvarGoodsStatusMessage.MaxLength = 200;
                colvarGoodsStatusMessage.AutoIncrement = false;
                colvarGoodsStatusMessage.IsNullable = true;
                colvarGoodsStatusMessage.IsPrimaryKey = false;
                colvarGoodsStatusMessage.IsForeignKey = false;
                colvarGoodsStatusMessage.IsReadOnly = false;
                
                schema.Columns.Add(colvarGoodsStatusMessage);
                
                TableSchema.TableColumn colvarIsApplicationServerConnected = new TableSchema.TableColumn(schema);
                colvarIsApplicationServerConnected.ColumnName = "is_application_server_connected";
                colvarIsApplicationServerConnected.DataType = DbType.Int32;
                colvarIsApplicationServerConnected.MaxLength = 0;
                colvarIsApplicationServerConnected.AutoIncrement = false;
                colvarIsApplicationServerConnected.IsNullable = false;
                colvarIsApplicationServerConnected.IsPrimaryKey = false;
                colvarIsApplicationServerConnected.IsForeignKey = false;
                colvarIsApplicationServerConnected.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsApplicationServerConnected);
                
                TableSchema.TableColumn colvarSellerGoodsStatus = new TableSchema.TableColumn(schema);
                colvarSellerGoodsStatus.ColumnName = "seller_goods_status";
                colvarSellerGoodsStatus.DataType = DbType.Int32;
                colvarSellerGoodsStatus.MaxLength = 0;
                colvarSellerGoodsStatus.AutoIncrement = false;
                colvarSellerGoodsStatus.IsNullable = false;
                colvarSellerGoodsStatus.IsPrimaryKey = false;
                colvarSellerGoodsStatus.IsForeignKey = false;
                colvarSellerGoodsStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerGoodsStatus);
                
                TableSchema.TableColumn colvarSellerGoodsStatusMessage = new TableSchema.TableColumn(schema);
                colvarSellerGoodsStatusMessage.ColumnName = "seller_goods_status_message";
                colvarSellerGoodsStatusMessage.DataType = DbType.String;
                colvarSellerGoodsStatusMessage.MaxLength = 200;
                colvarSellerGoodsStatusMessage.AutoIncrement = false;
                colvarSellerGoodsStatusMessage.IsNullable = true;
                colvarSellerGoodsStatusMessage.IsPrimaryKey = false;
                colvarSellerGoodsStatusMessage.IsForeignKey = false;
                colvarSellerGoodsStatusMessage.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerGoodsStatusMessage);
                
                TableSchema.TableColumn colvarIsCancel = new TableSchema.TableColumn(schema);
                colvarIsCancel.ColumnName = "is_cancel";
                colvarIsCancel.DataType = DbType.Boolean;
                colvarIsCancel.MaxLength = 0;
                colvarIsCancel.AutoIncrement = false;
                colvarIsCancel.IsNullable = false;
                colvarIsCancel.IsPrimaryKey = false;
                colvarIsCancel.IsForeignKey = false;
                colvarIsCancel.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsCancel);
                
                TableSchema.TableColumn colvarIsCompleted = new TableSchema.TableColumn(schema);
                colvarIsCompleted.ColumnName = "is_completed";
                colvarIsCompleted.DataType = DbType.Boolean;
                colvarIsCompleted.MaxLength = 0;
                colvarIsCompleted.AutoIncrement = false;
                colvarIsCompleted.IsNullable = false;
                colvarIsCompleted.IsPrimaryKey = false;
                colvarIsCompleted.IsForeignKey = false;
                colvarIsCompleted.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsCompleted);
                
                TableSchema.TableColumn colvarOrderShipId = new TableSchema.TableColumn(schema);
                colvarOrderShipId.ColumnName = "order_ship_id";
                colvarOrderShipId.DataType = DbType.Int32;
                colvarOrderShipId.MaxLength = 0;
                colvarOrderShipId.AutoIncrement = false;
                colvarOrderShipId.IsNullable = true;
                colvarOrderShipId.IsPrimaryKey = false;
                colvarOrderShipId.IsForeignKey = false;
                colvarOrderShipId.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderShipId);
                
                TableSchema.TableColumn colvarGoodStatusHistory = new TableSchema.TableColumn(schema);
                colvarGoodStatusHistory.ColumnName = "good_status_history";
                colvarGoodStatusHistory.DataType = DbType.String;
                colvarGoodStatusHistory.MaxLength = -1;
                colvarGoodStatusHistory.AutoIncrement = false;
                colvarGoodStatusHistory.IsNullable = true;
                colvarGoodStatusHistory.IsPrimaryKey = false;
                colvarGoodStatusHistory.IsForeignKey = false;
                colvarGoodStatusHistory.IsReadOnly = false;
                
                schema.Columns.Add(colvarGoodStatusHistory);
                
                TableSchema.TableColumn colvarIsShipmentPdf = new TableSchema.TableColumn(schema);
                colvarIsShipmentPdf.ColumnName = "is_shipment_pdf";
                colvarIsShipmentPdf.DataType = DbType.Boolean;
                colvarIsShipmentPdf.MaxLength = 0;
                colvarIsShipmentPdf.AutoIncrement = false;
                colvarIsShipmentPdf.IsNullable = false;
                colvarIsShipmentPdf.IsPrimaryKey = false;
                colvarIsShipmentPdf.IsForeignKey = false;
                colvarIsShipmentPdf.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsShipmentPdf);
                
                TableSchema.TableColumn colvarPreShipNo = new TableSchema.TableColumn(schema);
                colvarPreShipNo.ColumnName = "pre_ship_no";
                colvarPreShipNo.DataType = DbType.AnsiString;
                colvarPreShipNo.MaxLength = 50;
                colvarPreShipNo.AutoIncrement = false;
                colvarPreShipNo.IsNullable = true;
                colvarPreShipNo.IsPrimaryKey = false;
                colvarPreShipNo.IsForeignKey = false;
                colvarPreShipNo.IsReadOnly = false;
                
                schema.Columns.Add(colvarPreShipNo);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_order_product_delivery",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewOrderProductDelivery()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewOrderProductDelivery(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewOrderProductDelivery(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewOrderProductDelivery(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id 
	    {
		    get
		    {
			    return GetColumnValue<int>("id");
		    }
            set 
		    {
			    SetColumnValue("id", value);
            }
        }
	      
        [XmlAttribute("OrderGuid")]
        [Bindable(true)]
        public Guid OrderGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("order_guid");
		    }
            set 
		    {
			    SetColumnValue("order_guid", value);
            }
        }
	      
        [XmlAttribute("OrderId")]
        [Bindable(true)]
        public string OrderId 
	    {
		    get
		    {
			    return GetColumnValue<string>("order_id");
		    }
            set 
		    {
			    SetColumnValue("order_id", value);
            }
        }
	      
        [XmlAttribute("OrderStatus")]
        [Bindable(true)]
        public int OrderStatus 
	    {
		    get
		    {
			    return GetColumnValue<int>("order_status");
		    }
            set 
		    {
			    SetColumnValue("order_status", value);
            }
        }
	      
        [XmlAttribute("ProductDeliveryType")]
        [Bindable(true)]
        public int ProductDeliveryType 
	    {
		    get
		    {
			    return GetColumnValue<int>("product_delivery_type");
		    }
            set 
		    {
			    SetColumnValue("product_delivery_type", value);
            }
        }
	      
        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime CreateTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("create_time");
		    }
            set 
		    {
			    SetColumnValue("create_time", value);
            }
        }
	      
        [XmlAttribute("ModifyTime")]
        [Bindable(true)]
        public DateTime? ModifyTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("modify_time");
		    }
            set 
		    {
			    SetColumnValue("modify_time", value);
            }
        }
	      
        [XmlAttribute("LastShipDate")]
        [Bindable(true)]
        public DateTime? LastShipDate 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("last_ship_date");
		    }
            set 
		    {
			    SetColumnValue("last_ship_date", value);
            }
        }
	      
        [XmlAttribute("DeliveryId")]
        [Bindable(true)]
        public int? DeliveryId 
	    {
		    get
		    {
			    return GetColumnValue<int?>("delivery_id");
		    }
            set 
		    {
			    SetColumnValue("delivery_id", value);
            }
        }
	      
        [XmlAttribute("FamilyStoreId")]
        [Bindable(true)]
        public string FamilyStoreId 
	    {
		    get
		    {
			    return GetColumnValue<string>("family_store_id");
		    }
            set 
		    {
			    SetColumnValue("family_store_id", value);
            }
        }
	      
        [XmlAttribute("FamilyStoreName")]
        [Bindable(true)]
        public string FamilyStoreName 
	    {
		    get
		    {
			    return GetColumnValue<string>("family_store_name");
		    }
            set 
		    {
			    SetColumnValue("family_store_name", value);
            }
        }
	      
        [XmlAttribute("FamilyStoreTel")]
        [Bindable(true)]
        public string FamilyStoreTel 
	    {
		    get
		    {
			    return GetColumnValue<string>("family_store_tel");
		    }
            set 
		    {
			    SetColumnValue("family_store_tel", value);
            }
        }
	      
        [XmlAttribute("FamilyStoreAddr")]
        [Bindable(true)]
        public string FamilyStoreAddr 
	    {
		    get
		    {
			    return GetColumnValue<string>("family_store_addr");
		    }
            set 
		    {
			    SetColumnValue("family_store_addr", value);
            }
        }
	      
        [XmlAttribute("SevenStoreId")]
        [Bindable(true)]
        public string SevenStoreId 
	    {
		    get
		    {
			    return GetColumnValue<string>("seven_store_id");
		    }
            set 
		    {
			    SetColumnValue("seven_store_id", value);
            }
        }
	      
        [XmlAttribute("SevenStoreName")]
        [Bindable(true)]
        public string SevenStoreName 
	    {
		    get
		    {
			    return GetColumnValue<string>("seven_store_name");
		    }
            set 
		    {
			    SetColumnValue("seven_store_name", value);
            }
        }
	      
        [XmlAttribute("SevenStoreTel")]
        [Bindable(true)]
        public string SevenStoreTel 
	    {
		    get
		    {
			    return GetColumnValue<string>("seven_store_tel");
		    }
            set 
		    {
			    SetColumnValue("seven_store_tel", value);
            }
        }
	      
        [XmlAttribute("SevenStoreAddr")]
        [Bindable(true)]
        public string SevenStoreAddr 
	    {
		    get
		    {
			    return GetColumnValue<string>("seven_store_addr");
		    }
            set 
		    {
			    SetColumnValue("seven_store_addr", value);
            }
        }
	      
        [XmlAttribute("GoodsStatus")]
        [Bindable(true)]
        public int GoodsStatus 
	    {
		    get
		    {
			    return GetColumnValue<int>("goods_status");
		    }
            set 
		    {
			    SetColumnValue("goods_status", value);
            }
        }
	      
        [XmlAttribute("GoodsStatusMessage")]
        [Bindable(true)]
        public string GoodsStatusMessage 
	    {
		    get
		    {
			    return GetColumnValue<string>("goods_status_message");
		    }
            set 
		    {
			    SetColumnValue("goods_status_message", value);
            }
        }
	      
        [XmlAttribute("IsApplicationServerConnected")]
        [Bindable(true)]
        public int IsApplicationServerConnected 
	    {
		    get
		    {
			    return GetColumnValue<int>("is_application_server_connected");
		    }
            set 
		    {
			    SetColumnValue("is_application_server_connected", value);
            }
        }
	      
        [XmlAttribute("SellerGoodsStatus")]
        [Bindable(true)]
        public int SellerGoodsStatus 
	    {
		    get
		    {
			    return GetColumnValue<int>("seller_goods_status");
		    }
            set 
		    {
			    SetColumnValue("seller_goods_status", value);
            }
        }
	      
        [XmlAttribute("SellerGoodsStatusMessage")]
        [Bindable(true)]
        public string SellerGoodsStatusMessage 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_goods_status_message");
		    }
            set 
		    {
			    SetColumnValue("seller_goods_status_message", value);
            }
        }
	      
        [XmlAttribute("IsCancel")]
        [Bindable(true)]
        public bool IsCancel 
	    {
		    get
		    {
			    return GetColumnValue<bool>("is_cancel");
		    }
            set 
		    {
			    SetColumnValue("is_cancel", value);
            }
        }
	      
        [XmlAttribute("IsCompleted")]
        [Bindable(true)]
        public bool IsCompleted 
	    {
		    get
		    {
			    return GetColumnValue<bool>("is_completed");
		    }
            set 
		    {
			    SetColumnValue("is_completed", value);
            }
        }
	      
        [XmlAttribute("OrderShipId")]
        [Bindable(true)]
        public int? OrderShipId 
	    {
		    get
		    {
			    return GetColumnValue<int?>("order_ship_id");
		    }
            set 
		    {
			    SetColumnValue("order_ship_id", value);
            }
        }
	      
        [XmlAttribute("GoodStatusHistory")]
        [Bindable(true)]
        public string GoodStatusHistory 
	    {
		    get
		    {
			    return GetColumnValue<string>("good_status_history");
		    }
            set 
		    {
			    SetColumnValue("good_status_history", value);
            }
        }
	      
        [XmlAttribute("IsShipmentPdf")]
        [Bindable(true)]
        public bool IsShipmentPdf 
	    {
		    get
		    {
			    return GetColumnValue<bool>("is_shipment_pdf");
		    }
            set 
		    {
			    SetColumnValue("is_shipment_pdf", value);
            }
        }
	      
        [XmlAttribute("PreShipNo")]
        [Bindable(true)]
        public string PreShipNo 
	    {
		    get
		    {
			    return GetColumnValue<string>("pre_ship_no");
		    }
            set 
		    {
			    SetColumnValue("pre_ship_no", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string Id = @"id";
            
            public static string OrderGuid = @"order_guid";
            
            public static string OrderId = @"order_id";
            
            public static string OrderStatus = @"order_status";
            
            public static string ProductDeliveryType = @"product_delivery_type";
            
            public static string CreateTime = @"create_time";
            
            public static string ModifyTime = @"modify_time";
            
            public static string LastShipDate = @"last_ship_date";
            
            public static string DeliveryId = @"delivery_id";
            
            public static string FamilyStoreId = @"family_store_id";
            
            public static string FamilyStoreName = @"family_store_name";
            
            public static string FamilyStoreTel = @"family_store_tel";
            
            public static string FamilyStoreAddr = @"family_store_addr";
            
            public static string SevenStoreId = @"seven_store_id";
            
            public static string SevenStoreName = @"seven_store_name";
            
            public static string SevenStoreTel = @"seven_store_tel";
            
            public static string SevenStoreAddr = @"seven_store_addr";
            
            public static string GoodsStatus = @"goods_status";
            
            public static string GoodsStatusMessage = @"goods_status_message";
            
            public static string IsApplicationServerConnected = @"is_application_server_connected";
            
            public static string SellerGoodsStatus = @"seller_goods_status";
            
            public static string SellerGoodsStatusMessage = @"seller_goods_status_message";
            
            public static string IsCancel = @"is_cancel";
            
            public static string IsCompleted = @"is_completed";
            
            public static string OrderShipId = @"order_ship_id";
            
            public static string GoodStatusHistory = @"good_status_history";
            
            public static string IsShipmentPdf = @"is_shipment_pdf";
            
            public static string PreShipNo = @"pre_ship_no";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

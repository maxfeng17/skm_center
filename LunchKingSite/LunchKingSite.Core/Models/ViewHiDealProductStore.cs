using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewHiDealProductStore class.
    /// </summary>
    [Serializable]
    public partial class ViewHiDealProductStoreCollection : ReadOnlyList<ViewHiDealProductStore, ViewHiDealProductStoreCollection>
    {        
        public ViewHiDealProductStoreCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_hi_deal_product_store view.
    /// </summary>
    [Serializable]
    public partial class ViewHiDealProductStore : ReadOnlyRecord<ViewHiDealProductStore>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_hi_deal_product_store", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarHiDealProductId = new TableSchema.TableColumn(schema);
                colvarHiDealProductId.ColumnName = "hi_deal_product_id";
                colvarHiDealProductId.DataType = DbType.Int32;
                colvarHiDealProductId.MaxLength = 0;
                colvarHiDealProductId.AutoIncrement = false;
                colvarHiDealProductId.IsNullable = false;
                colvarHiDealProductId.IsPrimaryKey = false;
                colvarHiDealProductId.IsForeignKey = false;
                colvarHiDealProductId.IsReadOnly = false;
                
                schema.Columns.Add(colvarHiDealProductId);
                
                TableSchema.TableColumn colvarStoreGuid = new TableSchema.TableColumn(schema);
                colvarStoreGuid.ColumnName = "store_guid";
                colvarStoreGuid.DataType = DbType.Guid;
                colvarStoreGuid.MaxLength = 0;
                colvarStoreGuid.AutoIncrement = false;
                colvarStoreGuid.IsNullable = false;
                colvarStoreGuid.IsPrimaryKey = false;
                colvarStoreGuid.IsForeignKey = false;
                colvarStoreGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarStoreGuid);
                
                TableSchema.TableColumn colvarSeq = new TableSchema.TableColumn(schema);
                colvarSeq.ColumnName = "seq";
                colvarSeq.DataType = DbType.Int32;
                colvarSeq.MaxLength = 0;
                colvarSeq.AutoIncrement = false;
                colvarSeq.IsNullable = true;
                colvarSeq.IsPrimaryKey = false;
                colvarSeq.IsForeignKey = false;
                colvarSeq.IsReadOnly = false;
                
                schema.Columns.Add(colvarSeq);
                
                TableSchema.TableColumn colvarQuantityLimit = new TableSchema.TableColumn(schema);
                colvarQuantityLimit.ColumnName = "quantity_limit";
                colvarQuantityLimit.DataType = DbType.Int32;
                colvarQuantityLimit.MaxLength = 0;
                colvarQuantityLimit.AutoIncrement = false;
                colvarQuantityLimit.IsNullable = true;
                colvarQuantityLimit.IsPrimaryKey = false;
                colvarQuantityLimit.IsForeignKey = false;
                colvarQuantityLimit.IsReadOnly = false;
                
                schema.Columns.Add(colvarQuantityLimit);
                
                TableSchema.TableColumn colvarSellerGuid = new TableSchema.TableColumn(schema);
                colvarSellerGuid.ColumnName = "seller_guid";
                colvarSellerGuid.DataType = DbType.Guid;
                colvarSellerGuid.MaxLength = 0;
                colvarSellerGuid.AutoIncrement = false;
                colvarSellerGuid.IsNullable = false;
                colvarSellerGuid.IsPrimaryKey = false;
                colvarSellerGuid.IsForeignKey = false;
                colvarSellerGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerGuid);
                
                TableSchema.TableColumn colvarStoreName = new TableSchema.TableColumn(schema);
                colvarStoreName.ColumnName = "store_name";
                colvarStoreName.DataType = DbType.String;
                colvarStoreName.MaxLength = 256;
                colvarStoreName.AutoIncrement = false;
                colvarStoreName.IsNullable = true;
                colvarStoreName.IsPrimaryKey = false;
                colvarStoreName.IsForeignKey = false;
                colvarStoreName.IsReadOnly = false;
                
                schema.Columns.Add(colvarStoreName);
                
                TableSchema.TableColumn colvarPhone = new TableSchema.TableColumn(schema);
                colvarPhone.ColumnName = "phone";
                colvarPhone.DataType = DbType.AnsiString;
                colvarPhone.MaxLength = 15;
                colvarPhone.AutoIncrement = false;
                colvarPhone.IsNullable = true;
                colvarPhone.IsPrimaryKey = false;
                colvarPhone.IsForeignKey = false;
                colvarPhone.IsReadOnly = false;
                
                schema.Columns.Add(colvarPhone);
                
                TableSchema.TableColumn colvarCityId = new TableSchema.TableColumn(schema);
                colvarCityId.ColumnName = "city_id";
                colvarCityId.DataType = DbType.Int32;
                colvarCityId.MaxLength = 0;
                colvarCityId.AutoIncrement = false;
                colvarCityId.IsNullable = true;
                colvarCityId.IsPrimaryKey = false;
                colvarCityId.IsForeignKey = false;
                colvarCityId.IsReadOnly = false;
                
                schema.Columns.Add(colvarCityId);
                
                TableSchema.TableColumn colvarCityName = new TableSchema.TableColumn(schema);
                colvarCityName.ColumnName = "city_name";
                colvarCityName.DataType = DbType.String;
                colvarCityName.MaxLength = 20;
                colvarCityName.AutoIncrement = false;
                colvarCityName.IsNullable = true;
                colvarCityName.IsPrimaryKey = false;
                colvarCityName.IsForeignKey = false;
                colvarCityName.IsReadOnly = false;
                
                schema.Columns.Add(colvarCityName);
                
                TableSchema.TableColumn colvarTownshipName = new TableSchema.TableColumn(schema);
                colvarTownshipName.ColumnName = "township_name";
                colvarTownshipName.DataType = DbType.String;
                colvarTownshipName.MaxLength = 20;
                colvarTownshipName.AutoIncrement = false;
                colvarTownshipName.IsNullable = true;
                colvarTownshipName.IsPrimaryKey = false;
                colvarTownshipName.IsForeignKey = false;
                colvarTownshipName.IsReadOnly = false;
                
                schema.Columns.Add(colvarTownshipName);
                
                TableSchema.TableColumn colvarTownshipId = new TableSchema.TableColumn(schema);
                colvarTownshipId.ColumnName = "township_id";
                colvarTownshipId.DataType = DbType.Int32;
                colvarTownshipId.MaxLength = 0;
                colvarTownshipId.AutoIncrement = false;
                colvarTownshipId.IsNullable = true;
                colvarTownshipId.IsPrimaryKey = false;
                colvarTownshipId.IsForeignKey = false;
                colvarTownshipId.IsReadOnly = false;
                
                schema.Columns.Add(colvarTownshipId);
                
                TableSchema.TableColumn colvarAddressString = new TableSchema.TableColumn(schema);
                colvarAddressString.ColumnName = "address_string";
                colvarAddressString.DataType = DbType.String;
                colvarAddressString.MaxLength = 100;
                colvarAddressString.AutoIncrement = false;
                colvarAddressString.IsNullable = true;
                colvarAddressString.IsPrimaryKey = false;
                colvarAddressString.IsForeignKey = false;
                colvarAddressString.IsReadOnly = false;
                
                schema.Columns.Add(colvarAddressString);
                
                TableSchema.TableColumn colvarOpenTime = new TableSchema.TableColumn(schema);
                colvarOpenTime.ColumnName = "open_time";
                colvarOpenTime.DataType = DbType.String;
                colvarOpenTime.MaxLength = 256;
                colvarOpenTime.AutoIncrement = false;
                colvarOpenTime.IsNullable = true;
                colvarOpenTime.IsPrimaryKey = false;
                colvarOpenTime.IsForeignKey = false;
                colvarOpenTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarOpenTime);
                
                TableSchema.TableColumn colvarCloseDate = new TableSchema.TableColumn(schema);
                colvarCloseDate.ColumnName = "close_date";
                colvarCloseDate.DataType = DbType.String;
                colvarCloseDate.MaxLength = 256;
                colvarCloseDate.AutoIncrement = false;
                colvarCloseDate.IsNullable = true;
                colvarCloseDate.IsPrimaryKey = false;
                colvarCloseDate.IsForeignKey = false;
                colvarCloseDate.IsReadOnly = false;
                
                schema.Columns.Add(colvarCloseDate);
                
                TableSchema.TableColumn colvarRemarks = new TableSchema.TableColumn(schema);
                colvarRemarks.ColumnName = "remarks";
                colvarRemarks.DataType = DbType.String;
                colvarRemarks.MaxLength = 256;
                colvarRemarks.AutoIncrement = false;
                colvarRemarks.IsNullable = true;
                colvarRemarks.IsPrimaryKey = false;
                colvarRemarks.IsForeignKey = false;
                colvarRemarks.IsReadOnly = false;
                
                schema.Columns.Add(colvarRemarks);
                
                TableSchema.TableColumn colvarMrt = new TableSchema.TableColumn(schema);
                colvarMrt.ColumnName = "mrt";
                colvarMrt.DataType = DbType.String;
                colvarMrt.MaxLength = 256;
                colvarMrt.AutoIncrement = false;
                colvarMrt.IsNullable = true;
                colvarMrt.IsPrimaryKey = false;
                colvarMrt.IsForeignKey = false;
                colvarMrt.IsReadOnly = false;
                
                schema.Columns.Add(colvarMrt);
                
                TableSchema.TableColumn colvarCar = new TableSchema.TableColumn(schema);
                colvarCar.ColumnName = "car";
                colvarCar.DataType = DbType.String;
                colvarCar.MaxLength = 256;
                colvarCar.AutoIncrement = false;
                colvarCar.IsNullable = true;
                colvarCar.IsPrimaryKey = false;
                colvarCar.IsForeignKey = false;
                colvarCar.IsReadOnly = false;
                
                schema.Columns.Add(colvarCar);
                
                TableSchema.TableColumn colvarBus = new TableSchema.TableColumn(schema);
                colvarBus.ColumnName = "bus";
                colvarBus.DataType = DbType.String;
                colvarBus.MaxLength = 256;
                colvarBus.AutoIncrement = false;
                colvarBus.IsNullable = true;
                colvarBus.IsPrimaryKey = false;
                colvarBus.IsForeignKey = false;
                colvarBus.IsReadOnly = false;
                
                schema.Columns.Add(colvarBus);
                
                TableSchema.TableColumn colvarOtherVehicles = new TableSchema.TableColumn(schema);
                colvarOtherVehicles.ColumnName = "other_vehicles";
                colvarOtherVehicles.DataType = DbType.String;
                colvarOtherVehicles.MaxLength = 256;
                colvarOtherVehicles.AutoIncrement = false;
                colvarOtherVehicles.IsNullable = true;
                colvarOtherVehicles.IsPrimaryKey = false;
                colvarOtherVehicles.IsForeignKey = false;
                colvarOtherVehicles.IsReadOnly = false;
                
                schema.Columns.Add(colvarOtherVehicles);
                
                TableSchema.TableColumn colvarWebUrl = new TableSchema.TableColumn(schema);
                colvarWebUrl.ColumnName = "web_url";
                colvarWebUrl.DataType = DbType.String;
                colvarWebUrl.MaxLength = 256;
                colvarWebUrl.AutoIncrement = false;
                colvarWebUrl.IsNullable = true;
                colvarWebUrl.IsPrimaryKey = false;
                colvarWebUrl.IsForeignKey = false;
                colvarWebUrl.IsReadOnly = false;
                
                schema.Columns.Add(colvarWebUrl);
                
                TableSchema.TableColumn colvarFacebookUrl = new TableSchema.TableColumn(schema);
                colvarFacebookUrl.ColumnName = "facebook_url";
                colvarFacebookUrl.DataType = DbType.String;
                colvarFacebookUrl.MaxLength = 256;
                colvarFacebookUrl.AutoIncrement = false;
                colvarFacebookUrl.IsNullable = true;
                colvarFacebookUrl.IsPrimaryKey = false;
                colvarFacebookUrl.IsForeignKey = false;
                colvarFacebookUrl.IsReadOnly = false;
                
                schema.Columns.Add(colvarFacebookUrl);
                
                TableSchema.TableColumn colvarPlurkUrl = new TableSchema.TableColumn(schema);
                colvarPlurkUrl.ColumnName = "plurk_url";
                colvarPlurkUrl.DataType = DbType.String;
                colvarPlurkUrl.MaxLength = 256;
                colvarPlurkUrl.AutoIncrement = false;
                colvarPlurkUrl.IsNullable = true;
                colvarPlurkUrl.IsPrimaryKey = false;
                colvarPlurkUrl.IsForeignKey = false;
                colvarPlurkUrl.IsReadOnly = false;
                
                schema.Columns.Add(colvarPlurkUrl);
                
                TableSchema.TableColumn colvarBlogUrl = new TableSchema.TableColumn(schema);
                colvarBlogUrl.ColumnName = "blog_url";
                colvarBlogUrl.DataType = DbType.String;
                colvarBlogUrl.MaxLength = 256;
                colvarBlogUrl.AutoIncrement = false;
                colvarBlogUrl.IsNullable = true;
                colvarBlogUrl.IsPrimaryKey = false;
                colvarBlogUrl.IsForeignKey = false;
                colvarBlogUrl.IsReadOnly = false;
                
                schema.Columns.Add(colvarBlogUrl);
                
                TableSchema.TableColumn colvarOtherUrl = new TableSchema.TableColumn(schema);
                colvarOtherUrl.ColumnName = "other_url";
                colvarOtherUrl.DataType = DbType.String;
                colvarOtherUrl.MaxLength = 256;
                colvarOtherUrl.AutoIncrement = false;
                colvarOtherUrl.IsNullable = true;
                colvarOtherUrl.IsPrimaryKey = false;
                colvarOtherUrl.IsForeignKey = false;
                colvarOtherUrl.IsReadOnly = false;
                
                schema.Columns.Add(colvarOtherUrl);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_hi_deal_product_store",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewHiDealProductStore()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewHiDealProductStore(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewHiDealProductStore(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewHiDealProductStore(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("HiDealProductId")]
        [Bindable(true)]
        public int HiDealProductId 
	    {
		    get
		    {
			    return GetColumnValue<int>("hi_deal_product_id");
		    }
            set 
		    {
			    SetColumnValue("hi_deal_product_id", value);
            }
        }
	      
        [XmlAttribute("StoreGuid")]
        [Bindable(true)]
        public Guid StoreGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("store_guid");
		    }
            set 
		    {
			    SetColumnValue("store_guid", value);
            }
        }
	      
        [XmlAttribute("Seq")]
        [Bindable(true)]
        public int? Seq 
	    {
		    get
		    {
			    return GetColumnValue<int?>("seq");
		    }
            set 
		    {
			    SetColumnValue("seq", value);
            }
        }
	      
        [XmlAttribute("QuantityLimit")]
        [Bindable(true)]
        public int? QuantityLimit 
	    {
		    get
		    {
			    return GetColumnValue<int?>("quantity_limit");
		    }
            set 
		    {
			    SetColumnValue("quantity_limit", value);
            }
        }
	      
        [XmlAttribute("SellerGuid")]
        [Bindable(true)]
        public Guid SellerGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("seller_guid");
		    }
            set 
		    {
			    SetColumnValue("seller_guid", value);
            }
        }
	      
        [XmlAttribute("StoreName")]
        [Bindable(true)]
        public string StoreName 
	    {
		    get
		    {
			    return GetColumnValue<string>("store_name");
		    }
            set 
		    {
			    SetColumnValue("store_name", value);
            }
        }
	      
        [XmlAttribute("Phone")]
        [Bindable(true)]
        public string Phone 
	    {
		    get
		    {
			    return GetColumnValue<string>("phone");
		    }
            set 
		    {
			    SetColumnValue("phone", value);
            }
        }
	      
        [XmlAttribute("CityId")]
        [Bindable(true)]
        public int? CityId 
	    {
		    get
		    {
			    return GetColumnValue<int?>("city_id");
		    }
            set 
		    {
			    SetColumnValue("city_id", value);
            }
        }
	      
        [XmlAttribute("CityName")]
        [Bindable(true)]
        public string CityName 
	    {
		    get
		    {
			    return GetColumnValue<string>("city_name");
		    }
            set 
		    {
			    SetColumnValue("city_name", value);
            }
        }
	      
        [XmlAttribute("TownshipName")]
        [Bindable(true)]
        public string TownshipName 
	    {
		    get
		    {
			    return GetColumnValue<string>("township_name");
		    }
            set 
		    {
			    SetColumnValue("township_name", value);
            }
        }
	      
        [XmlAttribute("TownshipId")]
        [Bindable(true)]
        public int? TownshipId 
	    {
		    get
		    {
			    return GetColumnValue<int?>("township_id");
		    }
            set 
		    {
			    SetColumnValue("township_id", value);
            }
        }
	      
        [XmlAttribute("AddressString")]
        [Bindable(true)]
        public string AddressString 
	    {
		    get
		    {
			    return GetColumnValue<string>("address_string");
		    }
            set 
		    {
			    SetColumnValue("address_string", value);
            }
        }
	      
        [XmlAttribute("OpenTime")]
        [Bindable(true)]
        public string OpenTime 
	    {
		    get
		    {
			    return GetColumnValue<string>("open_time");
		    }
            set 
		    {
			    SetColumnValue("open_time", value);
            }
        }
	      
        [XmlAttribute("CloseDate")]
        [Bindable(true)]
        public string CloseDate 
	    {
		    get
		    {
			    return GetColumnValue<string>("close_date");
		    }
            set 
		    {
			    SetColumnValue("close_date", value);
            }
        }
	      
        [XmlAttribute("Remarks")]
        [Bindable(true)]
        public string Remarks 
	    {
		    get
		    {
			    return GetColumnValue<string>("remarks");
		    }
            set 
		    {
			    SetColumnValue("remarks", value);
            }
        }
	      
        [XmlAttribute("Mrt")]
        [Bindable(true)]
        public string Mrt 
	    {
		    get
		    {
			    return GetColumnValue<string>("mrt");
		    }
            set 
		    {
			    SetColumnValue("mrt", value);
            }
        }
	      
        [XmlAttribute("Car")]
        [Bindable(true)]
        public string Car 
	    {
		    get
		    {
			    return GetColumnValue<string>("car");
		    }
            set 
		    {
			    SetColumnValue("car", value);
            }
        }
	      
        [XmlAttribute("Bus")]
        [Bindable(true)]
        public string Bus 
	    {
		    get
		    {
			    return GetColumnValue<string>("bus");
		    }
            set 
		    {
			    SetColumnValue("bus", value);
            }
        }
	      
        [XmlAttribute("OtherVehicles")]
        [Bindable(true)]
        public string OtherVehicles 
	    {
		    get
		    {
			    return GetColumnValue<string>("other_vehicles");
		    }
            set 
		    {
			    SetColumnValue("other_vehicles", value);
            }
        }
	      
        [XmlAttribute("WebUrl")]
        [Bindable(true)]
        public string WebUrl 
	    {
		    get
		    {
			    return GetColumnValue<string>("web_url");
		    }
            set 
		    {
			    SetColumnValue("web_url", value);
            }
        }
	      
        [XmlAttribute("FacebookUrl")]
        [Bindable(true)]
        public string FacebookUrl 
	    {
		    get
		    {
			    return GetColumnValue<string>("facebook_url");
		    }
            set 
		    {
			    SetColumnValue("facebook_url", value);
            }
        }
	      
        [XmlAttribute("PlurkUrl")]
        [Bindable(true)]
        public string PlurkUrl 
	    {
		    get
		    {
			    return GetColumnValue<string>("plurk_url");
		    }
            set 
		    {
			    SetColumnValue("plurk_url", value);
            }
        }
	      
        [XmlAttribute("BlogUrl")]
        [Bindable(true)]
        public string BlogUrl 
	    {
		    get
		    {
			    return GetColumnValue<string>("blog_url");
		    }
            set 
		    {
			    SetColumnValue("blog_url", value);
            }
        }
	      
        [XmlAttribute("OtherUrl")]
        [Bindable(true)]
        public string OtherUrl 
	    {
		    get
		    {
			    return GetColumnValue<string>("other_url");
		    }
            set 
		    {
			    SetColumnValue("other_url", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string HiDealProductId = @"hi_deal_product_id";
            
            public static string StoreGuid = @"store_guid";
            
            public static string Seq = @"seq";
            
            public static string QuantityLimit = @"quantity_limit";
            
            public static string SellerGuid = @"seller_guid";
            
            public static string StoreName = @"store_name";
            
            public static string Phone = @"phone";
            
            public static string CityId = @"city_id";
            
            public static string CityName = @"city_name";
            
            public static string TownshipName = @"township_name";
            
            public static string TownshipId = @"township_id";
            
            public static string AddressString = @"address_string";
            
            public static string OpenTime = @"open_time";
            
            public static string CloseDate = @"close_date";
            
            public static string Remarks = @"remarks";
            
            public static string Mrt = @"mrt";
            
            public static string Car = @"car";
            
            public static string Bus = @"bus";
            
            public static string OtherVehicles = @"other_vehicles";
            
            public static string WebUrl = @"web_url";
            
            public static string FacebookUrl = @"facebook_url";
            
            public static string PlurkUrl = @"plurk_url";
            
            public static string BlogUrl = @"blog_url";
            
            public static string OtherUrl = @"other_url";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the DailyFamiportItemTriggerReport class.
	/// </summary>
    [Serializable]
	public partial class DailyFamiportItemTriggerReportCollection : RepositoryList<DailyFamiportItemTriggerReport, DailyFamiportItemTriggerReportCollection>
	{	   
		public DailyFamiportItemTriggerReportCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>DailyFamiportItemTriggerReportCollection</returns>
		public DailyFamiportItemTriggerReportCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                DailyFamiportItemTriggerReport o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the daily_famiport_item_trigger_report table.
	/// </summary>
	[Serializable]
	public partial class DailyFamiportItemTriggerReport : RepositoryRecord<DailyFamiportItemTriggerReport>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public DailyFamiportItemTriggerReport()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public DailyFamiportItemTriggerReport(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("daily_famiport_item_trigger_report", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "Id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarFamiportId = new TableSchema.TableColumn(schema);
				colvarFamiportId.ColumnName = "famiport_id";
				colvarFamiportId.DataType = DbType.Int32;
				colvarFamiportId.MaxLength = 0;
				colvarFamiportId.AutoIncrement = false;
				colvarFamiportId.IsNullable = false;
				colvarFamiportId.IsPrimaryKey = false;
				colvarFamiportId.IsForeignKey = false;
				colvarFamiportId.IsReadOnly = false;
				colvarFamiportId.DefaultSetting = @"";
				colvarFamiportId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFamiportId);
				
				TableSchema.TableColumn colvarFamiportType = new TableSchema.TableColumn(schema);
				colvarFamiportType.ColumnName = "famiport_type";
				colvarFamiportType.DataType = DbType.Int32;
				colvarFamiportType.MaxLength = 0;
				colvarFamiportType.AutoIncrement = false;
				colvarFamiportType.IsNullable = false;
				colvarFamiportType.IsPrimaryKey = false;
				colvarFamiportType.IsForeignKey = false;
				colvarFamiportType.IsReadOnly = false;
				colvarFamiportType.DefaultSetting = @"";
				colvarFamiportType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFamiportType);
				
				TableSchema.TableColumn colvarItemName = new TableSchema.TableColumn(schema);
				colvarItemName.ColumnName = "item_name";
				colvarItemName.DataType = DbType.String;
				colvarItemName.MaxLength = 15;
				colvarItemName.AutoIncrement = false;
				colvarItemName.IsNullable = true;
				colvarItemName.IsPrimaryKey = false;
				colvarItemName.IsForeignKey = false;
				colvarItemName.IsReadOnly = false;
				colvarItemName.DefaultSetting = @"";
				colvarItemName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarItemName);
				
				TableSchema.TableColumn colvarDailyStartTime = new TableSchema.TableColumn(schema);
				colvarDailyStartTime.ColumnName = "daily_start_time";
				colvarDailyStartTime.DataType = DbType.DateTime;
				colvarDailyStartTime.MaxLength = 0;
				colvarDailyStartTime.AutoIncrement = false;
				colvarDailyStartTime.IsNullable = false;
				colvarDailyStartTime.IsPrimaryKey = false;
				colvarDailyStartTime.IsForeignKey = false;
				colvarDailyStartTime.IsReadOnly = false;
				colvarDailyStartTime.DefaultSetting = @"";
				colvarDailyStartTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDailyStartTime);
				
				TableSchema.TableColumn colvarDailyEndTime = new TableSchema.TableColumn(schema);
				colvarDailyEndTime.ColumnName = "daily_end_time";
				colvarDailyEndTime.DataType = DbType.DateTime;
				colvarDailyEndTime.MaxLength = 0;
				colvarDailyEndTime.AutoIncrement = false;
				colvarDailyEndTime.IsNullable = false;
				colvarDailyEndTime.IsPrimaryKey = false;
				colvarDailyEndTime.IsForeignKey = false;
				colvarDailyEndTime.IsReadOnly = false;
				colvarDailyEndTime.DefaultSetting = @"";
				colvarDailyEndTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDailyEndTime);
				
				TableSchema.TableColumn colvarBeaconTriggerAmount = new TableSchema.TableColumn(schema);
				colvarBeaconTriggerAmount.ColumnName = "beacon_trigger_amount";
				colvarBeaconTriggerAmount.DataType = DbType.Int32;
				colvarBeaconTriggerAmount.MaxLength = 0;
				colvarBeaconTriggerAmount.AutoIncrement = false;
				colvarBeaconTriggerAmount.IsNullable = false;
				colvarBeaconTriggerAmount.IsPrimaryKey = false;
				colvarBeaconTriggerAmount.IsForeignKey = false;
				colvarBeaconTriggerAmount.IsReadOnly = false;
				
						colvarBeaconTriggerAmount.DefaultSetting = @"((0))";
				colvarBeaconTriggerAmount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBeaconTriggerAmount);
				
				TableSchema.TableColumn colvarSoonTriggerAmount = new TableSchema.TableColumn(schema);
				colvarSoonTriggerAmount.ColumnName = "soon_trigger_amount";
				colvarSoonTriggerAmount.DataType = DbType.Int32;
				colvarSoonTriggerAmount.MaxLength = 0;
				colvarSoonTriggerAmount.AutoIncrement = false;
				colvarSoonTriggerAmount.IsNullable = false;
				colvarSoonTriggerAmount.IsPrimaryKey = false;
				colvarSoonTriggerAmount.IsForeignKey = false;
				colvarSoonTriggerAmount.IsReadOnly = false;
				
						colvarSoonTriggerAmount.DefaultSetting = @"((0))";
				colvarSoonTriggerAmount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSoonTriggerAmount);
				
				TableSchema.TableColumn colvarCouponAmount = new TableSchema.TableColumn(schema);
				colvarCouponAmount.ColumnName = "coupon_amount";
				colvarCouponAmount.DataType = DbType.Int32;
				colvarCouponAmount.MaxLength = 0;
				colvarCouponAmount.AutoIncrement = false;
				colvarCouponAmount.IsNullable = false;
				colvarCouponAmount.IsPrimaryKey = false;
				colvarCouponAmount.IsForeignKey = false;
				colvarCouponAmount.IsReadOnly = false;
				
						colvarCouponAmount.DefaultSetting = @"((0))";
				colvarCouponAmount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCouponAmount);
				
				TableSchema.TableColumn colvarPayAmount = new TableSchema.TableColumn(schema);
				colvarPayAmount.ColumnName = "pay_amount";
				colvarPayAmount.DataType = DbType.Int32;
				colvarPayAmount.MaxLength = 0;
				colvarPayAmount.AutoIncrement = false;
				colvarPayAmount.IsNullable = false;
				colvarPayAmount.IsPrimaryKey = false;
				colvarPayAmount.IsForeignKey = false;
				colvarPayAmount.IsReadOnly = false;
				
						colvarPayAmount.DefaultSetting = @"((0))";
				colvarPayAmount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPayAmount);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
				colvarCreateId.ColumnName = "create_id";
				colvarCreateId.DataType = DbType.String;
				colvarCreateId.MaxLength = 256;
				colvarCreateId.AutoIncrement = false;
				colvarCreateId.IsNullable = false;
				colvarCreateId.IsPrimaryKey = false;
				colvarCreateId.IsForeignKey = false;
				colvarCreateId.IsReadOnly = false;
				colvarCreateId.DefaultSetting = @"";
				colvarCreateId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateId);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("daily_famiport_item_trigger_report",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("FamiportId")]
		[Bindable(true)]
		public int FamiportId 
		{
			get { return GetColumnValue<int>(Columns.FamiportId); }
			set { SetColumnValue(Columns.FamiportId, value); }
		}
		  
		[XmlAttribute("FamiportType")]
		[Bindable(true)]
		public int FamiportType 
		{
			get { return GetColumnValue<int>(Columns.FamiportType); }
			set { SetColumnValue(Columns.FamiportType, value); }
		}
		  
		[XmlAttribute("ItemName")]
		[Bindable(true)]
		public string ItemName 
		{
			get { return GetColumnValue<string>(Columns.ItemName); }
			set { SetColumnValue(Columns.ItemName, value); }
		}
		  
		[XmlAttribute("DailyStartTime")]
		[Bindable(true)]
		public DateTime DailyStartTime 
		{
			get { return GetColumnValue<DateTime>(Columns.DailyStartTime); }
			set { SetColumnValue(Columns.DailyStartTime, value); }
		}
		  
		[XmlAttribute("DailyEndTime")]
		[Bindable(true)]
		public DateTime DailyEndTime 
		{
			get { return GetColumnValue<DateTime>(Columns.DailyEndTime); }
			set { SetColumnValue(Columns.DailyEndTime, value); }
		}
		  
		[XmlAttribute("BeaconTriggerAmount")]
		[Bindable(true)]
		public int BeaconTriggerAmount 
		{
			get { return GetColumnValue<int>(Columns.BeaconTriggerAmount); }
			set { SetColumnValue(Columns.BeaconTriggerAmount, value); }
		}
		  
		[XmlAttribute("SoonTriggerAmount")]
		[Bindable(true)]
		public int SoonTriggerAmount 
		{
			get { return GetColumnValue<int>(Columns.SoonTriggerAmount); }
			set { SetColumnValue(Columns.SoonTriggerAmount, value); }
		}
		  
		[XmlAttribute("CouponAmount")]
		[Bindable(true)]
		public int CouponAmount 
		{
			get { return GetColumnValue<int>(Columns.CouponAmount); }
			set { SetColumnValue(Columns.CouponAmount, value); }
		}
		  
		[XmlAttribute("PayAmount")]
		[Bindable(true)]
		public int PayAmount 
		{
			get { return GetColumnValue<int>(Columns.PayAmount); }
			set { SetColumnValue(Columns.PayAmount, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		  
		[XmlAttribute("CreateId")]
		[Bindable(true)]
		public string CreateId 
		{
			get { return GetColumnValue<string>(Columns.CreateId); }
			set { SetColumnValue(Columns.CreateId, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn FamiportIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn FamiportTypeColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn ItemNameColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn DailyStartTimeColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn DailyEndTimeColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn BeaconTriggerAmountColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn SoonTriggerAmountColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn CouponAmountColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn PayAmountColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[11]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"Id";
			 public static string FamiportId = @"famiport_id";
			 public static string FamiportType = @"famiport_type";
			 public static string ItemName = @"item_name";
			 public static string DailyStartTime = @"daily_start_time";
			 public static string DailyEndTime = @"daily_end_time";
			 public static string BeaconTriggerAmount = @"beacon_trigger_amount";
			 public static string SoonTriggerAmount = @"soon_trigger_amount";
			 public static string CouponAmount = @"coupon_amount";
			 public static string PayAmount = @"pay_amount";
			 public static string CreateTime = @"create_time";
			 public static string CreateId = @"create_id";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

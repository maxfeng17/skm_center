using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the EventPromoItem class.
	/// </summary>
    [Serializable]
	public partial class EventPromoItemCollection : RepositoryList<EventPromoItem, EventPromoItemCollection>
	{	   
		public EventPromoItemCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>EventPromoItemCollection</returns>
		public EventPromoItemCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                EventPromoItem o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the event_promo_item table.
	/// </summary>
	[Serializable]
	public partial class EventPromoItem : RepositoryRecord<EventPromoItem>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public EventPromoItem()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public EventPromoItem(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("event_promo_item", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "Id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarMainId = new TableSchema.TableColumn(schema);
				colvarMainId.ColumnName = "MainId";
				colvarMainId.DataType = DbType.Int32;
				colvarMainId.MaxLength = 0;
				colvarMainId.AutoIncrement = false;
				colvarMainId.IsNullable = false;
				colvarMainId.IsPrimaryKey = false;
				colvarMainId.IsForeignKey = false;
				colvarMainId.IsReadOnly = false;
				colvarMainId.DefaultSetting = @"";
				colvarMainId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMainId);
				
				TableSchema.TableColumn colvarTitle = new TableSchema.TableColumn(schema);
				colvarTitle.ColumnName = "Title";
				colvarTitle.DataType = DbType.String;
				colvarTitle.MaxLength = 200;
				colvarTitle.AutoIncrement = false;
				colvarTitle.IsNullable = false;
				colvarTitle.IsPrimaryKey = false;
				colvarTitle.IsForeignKey = false;
				colvarTitle.IsReadOnly = false;
				colvarTitle.DefaultSetting = @"";
				colvarTitle.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTitle);
				
				TableSchema.TableColumn colvarDescription = new TableSchema.TableColumn(schema);
				colvarDescription.ColumnName = "Description";
				colvarDescription.DataType = DbType.String;
				colvarDescription.MaxLength = 300;
				colvarDescription.AutoIncrement = false;
				colvarDescription.IsNullable = false;
				colvarDescription.IsPrimaryKey = false;
				colvarDescription.IsForeignKey = false;
				colvarDescription.IsReadOnly = false;
				colvarDescription.DefaultSetting = @"";
				colvarDescription.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDescription);
				
				TableSchema.TableColumn colvarSeq = new TableSchema.TableColumn(schema);
				colvarSeq.ColumnName = "Seq";
				colvarSeq.DataType = DbType.Int32;
				colvarSeq.MaxLength = 0;
				colvarSeq.AutoIncrement = false;
				colvarSeq.IsNullable = false;
				colvarSeq.IsPrimaryKey = false;
				colvarSeq.IsForeignKey = false;
				colvarSeq.IsReadOnly = false;
				
						colvarSeq.DefaultSetting = @"((0))";
				colvarSeq.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSeq);
				
				TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
				colvarStatus.ColumnName = "Status";
				colvarStatus.DataType = DbType.Boolean;
				colvarStatus.MaxLength = 0;
				colvarStatus.AutoIncrement = false;
				colvarStatus.IsNullable = false;
				colvarStatus.IsPrimaryKey = false;
				colvarStatus.IsForeignKey = false;
				colvarStatus.IsReadOnly = false;
				
						colvarStatus.DefaultSetting = @"((1))";
				colvarStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStatus);
				
				TableSchema.TableColumn colvarCreator = new TableSchema.TableColumn(schema);
				colvarCreator.ColumnName = "Creator";
				colvarCreator.DataType = DbType.AnsiString;
				colvarCreator.MaxLength = 100;
				colvarCreator.AutoIncrement = false;
				colvarCreator.IsNullable = false;
				colvarCreator.IsPrimaryKey = false;
				colvarCreator.IsForeignKey = false;
				colvarCreator.IsReadOnly = false;
				colvarCreator.DefaultSetting = @"";
				colvarCreator.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreator);
				
				TableSchema.TableColumn colvarCdt = new TableSchema.TableColumn(schema);
				colvarCdt.ColumnName = "CDT";
				colvarCdt.DataType = DbType.DateTime;
				colvarCdt.MaxLength = 0;
				colvarCdt.AutoIncrement = false;
				colvarCdt.IsNullable = false;
				colvarCdt.IsPrimaryKey = false;
				colvarCdt.IsForeignKey = false;
				colvarCdt.IsReadOnly = false;
				colvarCdt.DefaultSetting = @"";
				colvarCdt.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCdt);
				
				TableSchema.TableColumn colvarMessage = new TableSchema.TableColumn(schema);
				colvarMessage.ColumnName = "Message";
				colvarMessage.DataType = DbType.String;
				colvarMessage.MaxLength = 1073741823;
				colvarMessage.AutoIncrement = false;
				colvarMessage.IsNullable = true;
				colvarMessage.IsPrimaryKey = false;
				colvarMessage.IsForeignKey = false;
				colvarMessage.IsReadOnly = false;
				colvarMessage.DefaultSetting = @"";
				colvarMessage.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMessage);
				
				TableSchema.TableColumn colvarCategory = new TableSchema.TableColumn(schema);
				colvarCategory.ColumnName = "category";
				colvarCategory.DataType = DbType.String;
				colvarCategory.MaxLength = 200;
				colvarCategory.AutoIncrement = false;
				colvarCategory.IsNullable = true;
				colvarCategory.IsPrimaryKey = false;
				colvarCategory.IsForeignKey = false;
				colvarCategory.IsReadOnly = false;
				colvarCategory.DefaultSetting = @"";
				colvarCategory.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCategory);
				
				TableSchema.TableColumn colvarItemId = new TableSchema.TableColumn(schema);
				colvarItemId.ColumnName = "item_id";
				colvarItemId.DataType = DbType.Int32;
				colvarItemId.MaxLength = 0;
				colvarItemId.AutoIncrement = false;
				colvarItemId.IsNullable = false;
				colvarItemId.IsPrimaryKey = false;
				colvarItemId.IsForeignKey = false;
				colvarItemId.IsReadOnly = false;
				
						colvarItemId.DefaultSetting = @"((0))";
				colvarItemId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarItemId);
				
				TableSchema.TableColumn colvarItemType = new TableSchema.TableColumn(schema);
				colvarItemType.ColumnName = "item_type";
				colvarItemType.DataType = DbType.Int32;
				colvarItemType.MaxLength = 0;
				colvarItemType.AutoIncrement = false;
				colvarItemType.IsNullable = false;
				colvarItemType.IsPrimaryKey = false;
				colvarItemType.IsForeignKey = false;
				colvarItemType.IsReadOnly = false;
				
						colvarItemType.DefaultSetting = @"((0))";
				colvarItemType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarItemType);
				
				TableSchema.TableColumn colvarItemStartDate = new TableSchema.TableColumn(schema);
				colvarItemStartDate.ColumnName = "item_start_date";
				colvarItemStartDate.DataType = DbType.DateTime;
				colvarItemStartDate.MaxLength = 0;
				colvarItemStartDate.AutoIncrement = false;
				colvarItemStartDate.IsNullable = true;
				colvarItemStartDate.IsPrimaryKey = false;
				colvarItemStartDate.IsForeignKey = false;
				colvarItemStartDate.IsReadOnly = false;
				colvarItemStartDate.DefaultSetting = @"";
				colvarItemStartDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarItemStartDate);
				
				TableSchema.TableColumn colvarItemEndDate = new TableSchema.TableColumn(schema);
				colvarItemEndDate.ColumnName = "item_end_date";
				colvarItemEndDate.DataType = DbType.DateTime;
				colvarItemEndDate.MaxLength = 0;
				colvarItemEndDate.AutoIncrement = false;
				colvarItemEndDate.IsNullable = true;
				colvarItemEndDate.IsPrimaryKey = false;
				colvarItemEndDate.IsForeignKey = false;
				colvarItemEndDate.IsReadOnly = false;
				colvarItemEndDate.DefaultSetting = @"";
				colvarItemEndDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarItemEndDate);
				
				TableSchema.TableColumn colvarItemUrl = new TableSchema.TableColumn(schema);
				colvarItemUrl.ColumnName = "item_url";
				colvarItemUrl.DataType = DbType.AnsiString;
				colvarItemUrl.MaxLength = 500;
				colvarItemUrl.AutoIncrement = false;
				colvarItemUrl.IsNullable = true;
				colvarItemUrl.IsPrimaryKey = false;
				colvarItemUrl.IsForeignKey = false;
				colvarItemUrl.IsReadOnly = false;
				colvarItemUrl.DefaultSetting = @"";
				colvarItemUrl.ForeignKeyTableName = "";
				schema.Columns.Add(colvarItemUrl);
				
				TableSchema.TableColumn colvarItemPicUrl = new TableSchema.TableColumn(schema);
				colvarItemPicUrl.ColumnName = "item_pic_url";
				colvarItemPicUrl.DataType = DbType.AnsiString;
				colvarItemPicUrl.MaxLength = 500;
				colvarItemPicUrl.AutoIncrement = false;
				colvarItemPicUrl.IsNullable = true;
				colvarItemPicUrl.IsPrimaryKey = false;
				colvarItemPicUrl.IsForeignKey = false;
				colvarItemPicUrl.IsReadOnly = false;
				colvarItemPicUrl.DefaultSetting = @"";
				colvarItemPicUrl.ForeignKeyTableName = "";
				schema.Columns.Add(colvarItemPicUrl);
				
				TableSchema.TableColumn colvarSubCategory = new TableSchema.TableColumn(schema);
				colvarSubCategory.ColumnName = "sub_category";
				colvarSubCategory.DataType = DbType.String;
				colvarSubCategory.MaxLength = 50;
				colvarSubCategory.AutoIncrement = false;
				colvarSubCategory.IsNullable = true;
				colvarSubCategory.IsPrimaryKey = false;
				colvarSubCategory.IsForeignKey = false;
				colvarSubCategory.IsReadOnly = false;
				colvarSubCategory.DefaultSetting = @"";
				colvarSubCategory.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSubCategory);
				
				TableSchema.TableColumn colvarLinkVourcherId = new TableSchema.TableColumn(schema);
				colvarLinkVourcherId.ColumnName = "link_vourcher_id";
				colvarLinkVourcherId.DataType = DbType.Int32;
				colvarLinkVourcherId.MaxLength = 0;
				colvarLinkVourcherId.AutoIncrement = false;
				colvarLinkVourcherId.IsNullable = false;
				colvarLinkVourcherId.IsPrimaryKey = false;
				colvarLinkVourcherId.IsForeignKey = false;
				colvarLinkVourcherId.IsReadOnly = false;
				
						colvarLinkVourcherId.DefaultSetting = @"((0))";
				colvarLinkVourcherId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarLinkVourcherId);
				
				TableSchema.TableColumn colvarLinkItemType = new TableSchema.TableColumn(schema);
				colvarLinkItemType.ColumnName = "link_item_type";
				colvarLinkItemType.DataType = DbType.Int32;
				colvarLinkItemType.MaxLength = 0;
				colvarLinkItemType.AutoIncrement = false;
				colvarLinkItemType.IsNullable = false;
				colvarLinkItemType.IsPrimaryKey = false;
				colvarLinkItemType.IsForeignKey = false;
				colvarLinkItemType.IsReadOnly = false;
				
						colvarLinkItemType.DefaultSetting = @"((0))";
				colvarLinkItemType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarLinkItemType);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("event_promo_item",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("MainId")]
		[Bindable(true)]
		public int MainId 
		{
			get { return GetColumnValue<int>(Columns.MainId); }
			set { SetColumnValue(Columns.MainId, value); }
		}
		  
		[XmlAttribute("Title")]
		[Bindable(true)]
		public string Title 
		{
			get { return GetColumnValue<string>(Columns.Title); }
			set { SetColumnValue(Columns.Title, value); }
		}
		  
		[XmlAttribute("Description")]
		[Bindable(true)]
		public string Description 
		{
			get { return GetColumnValue<string>(Columns.Description); }
			set { SetColumnValue(Columns.Description, value); }
		}
		  
		[XmlAttribute("Seq")]
		[Bindable(true)]
		public int Seq 
		{
			get { return GetColumnValue<int>(Columns.Seq); }
			set { SetColumnValue(Columns.Seq, value); }
		}
		  
		[XmlAttribute("Status")]
		[Bindable(true)]
		public bool Status 
		{
			get { return GetColumnValue<bool>(Columns.Status); }
			set { SetColumnValue(Columns.Status, value); }
		}
		  
		[XmlAttribute("Creator")]
		[Bindable(true)]
		public string Creator 
		{
			get { return GetColumnValue<string>(Columns.Creator); }
			set { SetColumnValue(Columns.Creator, value); }
		}
		  
		[XmlAttribute("Cdt")]
		[Bindable(true)]
		public DateTime Cdt 
		{
			get { return GetColumnValue<DateTime>(Columns.Cdt); }
			set { SetColumnValue(Columns.Cdt, value); }
		}
		  
		[XmlAttribute("Message")]
		[Bindable(true)]
		public string Message 
		{
			get { return GetColumnValue<string>(Columns.Message); }
			set { SetColumnValue(Columns.Message, value); }
		}
		  
		[XmlAttribute("Category")]
		[Bindable(true)]
		public string Category 
		{
			get { return GetColumnValue<string>(Columns.Category); }
			set { SetColumnValue(Columns.Category, value); }
		}
		  
		[XmlAttribute("ItemId")]
		[Bindable(true)]
		public int ItemId 
		{
			get { return GetColumnValue<int>(Columns.ItemId); }
			set { SetColumnValue(Columns.ItemId, value); }
		}
		  
		[XmlAttribute("ItemType")]
		[Bindable(true)]
		public int ItemType 
		{
			get { return GetColumnValue<int>(Columns.ItemType); }
			set { SetColumnValue(Columns.ItemType, value); }
		}
		  
		[XmlAttribute("ItemStartDate")]
		[Bindable(true)]
		public DateTime? ItemStartDate 
		{
			get { return GetColumnValue<DateTime?>(Columns.ItemStartDate); }
			set { SetColumnValue(Columns.ItemStartDate, value); }
		}
		  
		[XmlAttribute("ItemEndDate")]
		[Bindable(true)]
		public DateTime? ItemEndDate 
		{
			get { return GetColumnValue<DateTime?>(Columns.ItemEndDate); }
			set { SetColumnValue(Columns.ItemEndDate, value); }
		}
		  
		[XmlAttribute("ItemUrl")]
		[Bindable(true)]
		public string ItemUrl 
		{
			get { return GetColumnValue<string>(Columns.ItemUrl); }
			set { SetColumnValue(Columns.ItemUrl, value); }
		}
		  
		[XmlAttribute("ItemPicUrl")]
		[Bindable(true)]
		public string ItemPicUrl 
		{
			get { return GetColumnValue<string>(Columns.ItemPicUrl); }
			set { SetColumnValue(Columns.ItemPicUrl, value); }
		}
		  
		[XmlAttribute("SubCategory")]
		[Bindable(true)]
		public string SubCategory 
		{
			get { return GetColumnValue<string>(Columns.SubCategory); }
			set { SetColumnValue(Columns.SubCategory, value); }
		}
		  
		[XmlAttribute("LinkVourcherId")]
		[Bindable(true)]
		public int LinkVourcherId 
		{
			get { return GetColumnValue<int>(Columns.LinkVourcherId); }
			set { SetColumnValue(Columns.LinkVourcherId, value); }
		}
		  
		[XmlAttribute("LinkItemType")]
		[Bindable(true)]
		public int LinkItemType 
		{
			get { return GetColumnValue<int>(Columns.LinkItemType); }
			set { SetColumnValue(Columns.LinkItemType, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn MainIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn TitleColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn DescriptionColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn SeqColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn StatusColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn CreatorColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn CdtColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn MessageColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn CategoryColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn ItemIdColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        public static TableSchema.TableColumn ItemTypeColumn
        {
            get { return Schema.Columns[11]; }
        }
        
        
        
        public static TableSchema.TableColumn ItemStartDateColumn
        {
            get { return Schema.Columns[12]; }
        }
        
        
        
        public static TableSchema.TableColumn ItemEndDateColumn
        {
            get { return Schema.Columns[13]; }
        }
        
        
        
        public static TableSchema.TableColumn ItemUrlColumn
        {
            get { return Schema.Columns[14]; }
        }
        
        
        
        public static TableSchema.TableColumn ItemPicUrlColumn
        {
            get { return Schema.Columns[15]; }
        }
        
        
        
        public static TableSchema.TableColumn SubCategoryColumn
        {
            get { return Schema.Columns[16]; }
        }
        
        
        
        public static TableSchema.TableColumn LinkVourcherIdColumn
        {
            get { return Schema.Columns[17]; }
        }
        
        
        
        public static TableSchema.TableColumn LinkItemTypeColumn
        {
            get { return Schema.Columns[18]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"Id";
			 public static string MainId = @"MainId";
			 public static string Title = @"Title";
			 public static string Description = @"Description";
			 public static string Seq = @"Seq";
			 public static string Status = @"Status";
			 public static string Creator = @"Creator";
			 public static string Cdt = @"CDT";
			 public static string Message = @"Message";
			 public static string Category = @"category";
			 public static string ItemId = @"item_id";
			 public static string ItemType = @"item_type";
			 public static string ItemStartDate = @"item_start_date";
			 public static string ItemEndDate = @"item_end_date";
			 public static string ItemUrl = @"item_url";
			 public static string ItemPicUrl = @"item_pic_url";
			 public static string SubCategory = @"sub_category";
			 public static string LinkVourcherId = @"link_vourcher_id";
			 public static string LinkItemType = @"link_item_type";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the PcpUserDeposit class.
	/// </summary>
    [Serializable]
	public partial class PcpUserDepositCollection : RepositoryList<PcpUserDeposit, PcpUserDepositCollection>
	{	   
		public PcpUserDepositCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>PcpUserDepositCollection</returns>
		public PcpUserDepositCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                PcpUserDeposit o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the pcp_user_deposit table.
	/// </summary>
	[Serializable]
	public partial class PcpUserDeposit : RepositoryRecord<PcpUserDeposit>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public PcpUserDeposit()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public PcpUserDeposit(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("pcp_user_deposit", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarUserId = new TableSchema.TableColumn(schema);
				colvarUserId.ColumnName = "user_id";
				colvarUserId.DataType = DbType.Int32;
				colvarUserId.MaxLength = 0;
				colvarUserId.AutoIncrement = false;
				colvarUserId.IsNullable = false;
				colvarUserId.IsPrimaryKey = false;
				colvarUserId.IsForeignKey = false;
				colvarUserId.IsReadOnly = false;
				colvarUserId.DefaultSetting = @"";
				colvarUserId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUserId);
				
				TableSchema.TableColumn colvarTotalAmount = new TableSchema.TableColumn(schema);
				colvarTotalAmount.ColumnName = "total_amount";
				colvarTotalAmount.DataType = DbType.Int32;
				colvarTotalAmount.MaxLength = 0;
				colvarTotalAmount.AutoIncrement = false;
				colvarTotalAmount.IsNullable = false;
				colvarTotalAmount.IsPrimaryKey = false;
				colvarTotalAmount.IsForeignKey = false;
				colvarTotalAmount.IsReadOnly = false;
				colvarTotalAmount.DefaultSetting = @"";
				colvarTotalAmount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTotalAmount);
				
				TableSchema.TableColumn colvarRemainAmount = new TableSchema.TableColumn(schema);
				colvarRemainAmount.ColumnName = "remain_amount";
				colvarRemainAmount.DataType = DbType.Int32;
				colvarRemainAmount.MaxLength = 0;
				colvarRemainAmount.AutoIncrement = false;
				colvarRemainAmount.IsNullable = false;
				colvarRemainAmount.IsPrimaryKey = false;
				colvarRemainAmount.IsForeignKey = false;
				colvarRemainAmount.IsReadOnly = false;
				colvarRemainAmount.DefaultSetting = @"";
				colvarRemainAmount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarRemainAmount);
				
				TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
				colvarCreateId.ColumnName = "create_id";
				colvarCreateId.DataType = DbType.Int32;
				colvarCreateId.MaxLength = 0;
				colvarCreateId.AutoIncrement = false;
				colvarCreateId.IsNullable = false;
				colvarCreateId.IsPrimaryKey = false;
				colvarCreateId.IsForeignKey = false;
				colvarCreateId.IsReadOnly = false;
				colvarCreateId.DefaultSetting = @"";
				colvarCreateId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateId);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarLastUseTime = new TableSchema.TableColumn(schema);
				colvarLastUseTime.ColumnName = "last_use_time";
				colvarLastUseTime.DataType = DbType.DateTime;
				colvarLastUseTime.MaxLength = 0;
				colvarLastUseTime.AutoIncrement = false;
				colvarLastUseTime.IsNullable = false;
				colvarLastUseTime.IsPrimaryKey = false;
				colvarLastUseTime.IsForeignKey = false;
				colvarLastUseTime.IsReadOnly = false;
				colvarLastUseTime.DefaultSetting = @"";
				colvarLastUseTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarLastUseTime);
				
				TableSchema.TableColumn colvarPcpOrderGuid = new TableSchema.TableColumn(schema);
				colvarPcpOrderGuid.ColumnName = "pcp_order_guid";
				colvarPcpOrderGuid.DataType = DbType.Guid;
				colvarPcpOrderGuid.MaxLength = 0;
				colvarPcpOrderGuid.AutoIncrement = false;
				colvarPcpOrderGuid.IsNullable = false;
				colvarPcpOrderGuid.IsPrimaryKey = false;
				colvarPcpOrderGuid.IsForeignKey = false;
				colvarPcpOrderGuid.IsReadOnly = false;
				colvarPcpOrderGuid.DefaultSetting = @"";
				colvarPcpOrderGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPcpOrderGuid);
				
				TableSchema.TableColumn colvarDepositItemId = new TableSchema.TableColumn(schema);
				colvarDepositItemId.ColumnName = "deposit_item_id";
				colvarDepositItemId.DataType = DbType.Int32;
				colvarDepositItemId.MaxLength = 0;
				colvarDepositItemId.AutoIncrement = false;
				colvarDepositItemId.IsNullable = false;
				colvarDepositItemId.IsPrimaryKey = false;
				colvarDepositItemId.IsForeignKey = false;
				colvarDepositItemId.IsReadOnly = false;
				colvarDepositItemId.DefaultSetting = @"";
				colvarDepositItemId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDepositItemId);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("pcp_user_deposit",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("UserId")]
		[Bindable(true)]
		public int UserId 
		{
			get { return GetColumnValue<int>(Columns.UserId); }
			set { SetColumnValue(Columns.UserId, value); }
		}
		  
		[XmlAttribute("TotalAmount")]
		[Bindable(true)]
		public int TotalAmount 
		{
			get { return GetColumnValue<int>(Columns.TotalAmount); }
			set { SetColumnValue(Columns.TotalAmount, value); }
		}
		  
		[XmlAttribute("RemainAmount")]
		[Bindable(true)]
		public int RemainAmount 
		{
			get { return GetColumnValue<int>(Columns.RemainAmount); }
			set { SetColumnValue(Columns.RemainAmount, value); }
		}
		  
		[XmlAttribute("CreateId")]
		[Bindable(true)]
		public int CreateId 
		{
			get { return GetColumnValue<int>(Columns.CreateId); }
			set { SetColumnValue(Columns.CreateId, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		  
		[XmlAttribute("LastUseTime")]
		[Bindable(true)]
		public DateTime LastUseTime 
		{
			get { return GetColumnValue<DateTime>(Columns.LastUseTime); }
			set { SetColumnValue(Columns.LastUseTime, value); }
		}
		  
		[XmlAttribute("PcpOrderGuid")]
		[Bindable(true)]
		public Guid PcpOrderGuid 
		{
			get { return GetColumnValue<Guid>(Columns.PcpOrderGuid); }
			set { SetColumnValue(Columns.PcpOrderGuid, value); }
		}
		  
		[XmlAttribute("DepositItemId")]
		[Bindable(true)]
		public int DepositItemId 
		{
			get { return GetColumnValue<int>(Columns.DepositItemId); }
			set { SetColumnValue(Columns.DepositItemId, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn UserIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn TotalAmountColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn RemainAmountColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn LastUseTimeColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn PcpOrderGuidColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn DepositItemIdColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string UserId = @"user_id";
			 public static string TotalAmount = @"total_amount";
			 public static string RemainAmount = @"remain_amount";
			 public static string CreateId = @"create_id";
			 public static string CreateTime = @"create_time";
			 public static string LastUseTime = @"last_use_time";
			 public static string PcpOrderGuid = @"pcp_order_guid";
			 public static string DepositItemId = @"deposit_item_id";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

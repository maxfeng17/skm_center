using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the HiDealStaticDeal class.
	/// </summary>
    [Serializable]
	public partial class HiDealStaticDealCollection : RepositoryList<HiDealStaticDeal, HiDealStaticDealCollection>
	{	   
		public HiDealStaticDealCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>HiDealStaticDealCollection</returns>
		public HiDealStaticDealCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                HiDealStaticDeal o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the hi_deal_static_deal table.
	/// </summary>
	[Serializable]
	public partial class HiDealStaticDeal : RepositoryRecord<HiDealStaticDeal>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public HiDealStaticDeal()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public HiDealStaticDeal(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("hi_deal_static_deal", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarSi = new TableSchema.TableColumn(schema);
				colvarSi.ColumnName = "si";
				colvarSi.DataType = DbType.Int32;
				colvarSi.MaxLength = 0;
				colvarSi.AutoIncrement = true;
				colvarSi.IsNullable = false;
				colvarSi.IsPrimaryKey = true;
				colvarSi.IsForeignKey = false;
				colvarSi.IsReadOnly = false;
				colvarSi.DefaultSetting = @"";
				colvarSi.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSi);
				
				TableSchema.TableColumn colvarDealId = new TableSchema.TableColumn(schema);
				colvarDealId.ColumnName = "deal_id";
				colvarDealId.DataType = DbType.Int32;
				colvarDealId.MaxLength = 0;
				colvarDealId.AutoIncrement = false;
				colvarDealId.IsNullable = false;
				colvarDealId.IsPrimaryKey = false;
				colvarDealId.IsForeignKey = false;
				colvarDealId.IsReadOnly = false;
				colvarDealId.DefaultSetting = @"";
				colvarDealId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDealId);
				
				TableSchema.TableColumn colvarChangeSet = new TableSchema.TableColumn(schema);
				colvarChangeSet.ColumnName = "change_set";
				colvarChangeSet.DataType = DbType.Int32;
				colvarChangeSet.MaxLength = 0;
				colvarChangeSet.AutoIncrement = false;
				colvarChangeSet.IsNullable = false;
				colvarChangeSet.IsPrimaryKey = false;
				colvarChangeSet.IsForeignKey = false;
				colvarChangeSet.IsReadOnly = false;
				colvarChangeSet.DefaultSetting = @"";
				colvarChangeSet.ForeignKeyTableName = "";
				schema.Columns.Add(colvarChangeSet);
				
				TableSchema.TableColumn colvarDealName = new TableSchema.TableColumn(schema);
				colvarDealName.ColumnName = "deal_name";
				colvarDealName.DataType = DbType.String;
				colvarDealName.MaxLength = 50;
				colvarDealName.AutoIncrement = false;
				colvarDealName.IsNullable = false;
				colvarDealName.IsPrimaryKey = false;
				colvarDealName.IsForeignKey = false;
				colvarDealName.IsReadOnly = false;
				colvarDealName.DefaultSetting = @"";
				colvarDealName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDealName);
				
				TableSchema.TableColumn colvarProductId = new TableSchema.TableColumn(schema);
				colvarProductId.ColumnName = "product_id";
				colvarProductId.DataType = DbType.Int32;
				colvarProductId.MaxLength = 0;
				colvarProductId.AutoIncrement = false;
				colvarProductId.IsNullable = false;
				colvarProductId.IsPrimaryKey = false;
				colvarProductId.IsForeignKey = false;
				colvarProductId.IsReadOnly = false;
				colvarProductId.DefaultSetting = @"";
				colvarProductId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarProductId);
				
				TableSchema.TableColumn colvarProductName = new TableSchema.TableColumn(schema);
				colvarProductName.ColumnName = "product_name";
				colvarProductName.DataType = DbType.String;
				colvarProductName.MaxLength = 250;
				colvarProductName.AutoIncrement = false;
				colvarProductName.IsNullable = false;
				colvarProductName.IsPrimaryKey = false;
				colvarProductName.IsForeignKey = false;
				colvarProductName.IsReadOnly = false;
				colvarProductName.DefaultSetting = @"";
				colvarProductName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarProductName);
				
				TableSchema.TableColumn colvarStoreGuid = new TableSchema.TableColumn(schema);
				colvarStoreGuid.ColumnName = "store_guid";
				colvarStoreGuid.DataType = DbType.Guid;
				colvarStoreGuid.MaxLength = 0;
				colvarStoreGuid.AutoIncrement = false;
				colvarStoreGuid.IsNullable = true;
				colvarStoreGuid.IsPrimaryKey = false;
				colvarStoreGuid.IsForeignKey = false;
				colvarStoreGuid.IsReadOnly = false;
				colvarStoreGuid.DefaultSetting = @"";
				colvarStoreGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStoreGuid);
				
				TableSchema.TableColumn colvarQuantity = new TableSchema.TableColumn(schema);
				colvarQuantity.ColumnName = "quantity";
				colvarQuantity.DataType = DbType.Int32;
				colvarQuantity.MaxLength = 0;
				colvarQuantity.AutoIncrement = false;
				colvarQuantity.IsNullable = false;
				colvarQuantity.IsPrimaryKey = false;
				colvarQuantity.IsForeignKey = false;
				colvarQuantity.IsReadOnly = false;
				colvarQuantity.DefaultSetting = @"";
				colvarQuantity.ForeignKeyTableName = "";
				schema.Columns.Add(colvarQuantity);
				
				TableSchema.TableColumn colvarHasBranchStoreQuantityLimit = new TableSchema.TableColumn(schema);
				colvarHasBranchStoreQuantityLimit.ColumnName = "has_branch_store_quantity_limit";
				colvarHasBranchStoreQuantityLimit.DataType = DbType.Boolean;
				colvarHasBranchStoreQuantityLimit.MaxLength = 0;
				colvarHasBranchStoreQuantityLimit.AutoIncrement = false;
				colvarHasBranchStoreQuantityLimit.IsNullable = false;
				colvarHasBranchStoreQuantityLimit.IsPrimaryKey = false;
				colvarHasBranchStoreQuantityLimit.IsForeignKey = false;
				colvarHasBranchStoreQuantityLimit.IsReadOnly = false;
				colvarHasBranchStoreQuantityLimit.DefaultSetting = @"";
				colvarHasBranchStoreQuantityLimit.ForeignKeyTableName = "";
				schema.Columns.Add(colvarHasBranchStoreQuantityLimit);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
				colvarCreateId.ColumnName = "create_id";
				colvarCreateId.DataType = DbType.String;
				colvarCreateId.MaxLength = 256;
				colvarCreateId.AutoIncrement = false;
				colvarCreateId.IsNullable = false;
				colvarCreateId.IsPrimaryKey = false;
				colvarCreateId.IsForeignKey = false;
				colvarCreateId.IsReadOnly = false;
				colvarCreateId.DefaultSetting = @"";
				colvarCreateId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateId);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("hi_deal_static_deal",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Si")]
		[Bindable(true)]
		public int Si 
		{
			get { return GetColumnValue<int>(Columns.Si); }
			set { SetColumnValue(Columns.Si, value); }
		}
		  
		[XmlAttribute("DealId")]
		[Bindable(true)]
		public int DealId 
		{
			get { return GetColumnValue<int>(Columns.DealId); }
			set { SetColumnValue(Columns.DealId, value); }
		}
		  
		[XmlAttribute("ChangeSet")]
		[Bindable(true)]
		public int ChangeSet 
		{
			get { return GetColumnValue<int>(Columns.ChangeSet); }
			set { SetColumnValue(Columns.ChangeSet, value); }
		}
		  
		[XmlAttribute("DealName")]
		[Bindable(true)]
		public string DealName 
		{
			get { return GetColumnValue<string>(Columns.DealName); }
			set { SetColumnValue(Columns.DealName, value); }
		}
		  
		[XmlAttribute("ProductId")]
		[Bindable(true)]
		public int ProductId 
		{
			get { return GetColumnValue<int>(Columns.ProductId); }
			set { SetColumnValue(Columns.ProductId, value); }
		}
		  
		[XmlAttribute("ProductName")]
		[Bindable(true)]
		public string ProductName 
		{
			get { return GetColumnValue<string>(Columns.ProductName); }
			set { SetColumnValue(Columns.ProductName, value); }
		}
		  
		[XmlAttribute("StoreGuid")]
		[Bindable(true)]
		public Guid? StoreGuid 
		{
			get { return GetColumnValue<Guid?>(Columns.StoreGuid); }
			set { SetColumnValue(Columns.StoreGuid, value); }
		}
		  
		[XmlAttribute("Quantity")]
		[Bindable(true)]
		public int Quantity 
		{
			get { return GetColumnValue<int>(Columns.Quantity); }
			set { SetColumnValue(Columns.Quantity, value); }
		}
		  
		[XmlAttribute("HasBranchStoreQuantityLimit")]
		[Bindable(true)]
		public bool HasBranchStoreQuantityLimit 
		{
			get { return GetColumnValue<bool>(Columns.HasBranchStoreQuantityLimit); }
			set { SetColumnValue(Columns.HasBranchStoreQuantityLimit, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		  
		[XmlAttribute("CreateId")]
		[Bindable(true)]
		public string CreateId 
		{
			get { return GetColumnValue<string>(Columns.CreateId); }
			set { SetColumnValue(Columns.CreateId, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn SiColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn DealIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn ChangeSetColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn DealNameColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn ProductIdColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn ProductNameColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn StoreGuidColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn QuantityColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn HasBranchStoreQuantityLimitColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Si = @"si";
			 public static string DealId = @"deal_id";
			 public static string ChangeSet = @"change_set";
			 public static string DealName = @"deal_name";
			 public static string ProductId = @"product_id";
			 public static string ProductName = @"product_name";
			 public static string StoreGuid = @"store_guid";
			 public static string Quantity = @"quantity";
			 public static string HasBranchStoreQuantityLimit = @"has_branch_store_quantity_limit";
			 public static string CreateTime = @"create_time";
			 public static string CreateId = @"create_id";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

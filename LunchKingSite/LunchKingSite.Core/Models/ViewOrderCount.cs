using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewOrderCount class.
    /// </summary>
    [Serializable]
    public partial class ViewOrderCountCollection : ReadOnlyList<ViewOrderCount, ViewOrderCountCollection>
    {        
        public ViewOrderCountCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_order_count view.
    /// </summary>
    [Serializable]
    public partial class ViewOrderCount : ReadOnlyRecord<ViewOrderCount>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_order_count", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarMainBusinessHourGuid = new TableSchema.TableColumn(schema);
                colvarMainBusinessHourGuid.ColumnName = "MainBusinessHourGuid";
                colvarMainBusinessHourGuid.DataType = DbType.Guid;
                colvarMainBusinessHourGuid.MaxLength = 0;
                colvarMainBusinessHourGuid.AutoIncrement = false;
                colvarMainBusinessHourGuid.IsNullable = true;
                colvarMainBusinessHourGuid.IsPrimaryKey = false;
                colvarMainBusinessHourGuid.IsForeignKey = false;
                colvarMainBusinessHourGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarMainBusinessHourGuid);
                
                TableSchema.TableColumn colvarUniqueId = new TableSchema.TableColumn(schema);
                colvarUniqueId.ColumnName = "unique_id";
                colvarUniqueId.DataType = DbType.Int32;
                colvarUniqueId.MaxLength = 0;
                colvarUniqueId.AutoIncrement = false;
                colvarUniqueId.IsNullable = false;
                colvarUniqueId.IsPrimaryKey = false;
                colvarUniqueId.IsForeignKey = false;
                colvarUniqueId.IsReadOnly = false;
                
                schema.Columns.Add(colvarUniqueId);
                
                TableSchema.TableColumn colvarDeptId = new TableSchema.TableColumn(schema);
                colvarDeptId.ColumnName = "dept_id";
                colvarDeptId.DataType = DbType.AnsiString;
                colvarDeptId.MaxLength = 10;
                colvarDeptId.AutoIncrement = false;
                colvarDeptId.IsNullable = true;
                colvarDeptId.IsPrimaryKey = false;
                colvarDeptId.IsForeignKey = false;
                colvarDeptId.IsReadOnly = false;
                
                schema.Columns.Add(colvarDeptId);
                
                TableSchema.TableColumn colvarItemName = new TableSchema.TableColumn(schema);
                colvarItemName.ColumnName = "item_name";
                colvarItemName.DataType = DbType.String;
                colvarItemName.MaxLength = 750;
                colvarItemName.AutoIncrement = false;
                colvarItemName.IsNullable = false;
                colvarItemName.IsPrimaryKey = false;
                colvarItemName.IsForeignKey = false;
                colvarItemName.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemName);
                
                TableSchema.TableColumn colvarAppTitle = new TableSchema.TableColumn(schema);
                colvarAppTitle.ColumnName = "app_title";
                colvarAppTitle.DataType = DbType.String;
                colvarAppTitle.MaxLength = 40;
                colvarAppTitle.AutoIncrement = false;
                colvarAppTitle.IsNullable = true;
                colvarAppTitle.IsPrimaryKey = false;
                colvarAppTitle.IsForeignKey = false;
                colvarAppTitle.IsReadOnly = false;
                
                schema.Columns.Add(colvarAppTitle);
                
                TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
                colvarStatus.ColumnName = "status";
                colvarStatus.DataType = DbType.String;
                colvarStatus.MaxLength = 2;
                colvarStatus.AutoIncrement = false;
                colvarStatus.IsNullable = false;
                colvarStatus.IsPrimaryKey = false;
                colvarStatus.IsForeignKey = false;
                colvarStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarStatus);
                
                TableSchema.TableColumn colvarCchannel = new TableSchema.TableColumn(schema);
                colvarCchannel.ColumnName = "cchannel";
                colvarCchannel.DataType = DbType.String;
                colvarCchannel.MaxLength = 8;
                colvarCchannel.AutoIncrement = false;
                colvarCchannel.IsNullable = false;
                colvarCchannel.IsPrimaryKey = false;
                colvarCchannel.IsForeignKey = false;
                colvarCchannel.IsReadOnly = false;
                
                schema.Columns.Add(colvarCchannel);
                
                TableSchema.TableColumn colvarNum = new TableSchema.TableColumn(schema);
                colvarNum.ColumnName = "num";
                colvarNum.DataType = DbType.Int32;
                colvarNum.MaxLength = 0;
                colvarNum.AutoIncrement = false;
                colvarNum.IsNullable = true;
                colvarNum.IsPrimaryKey = false;
                colvarNum.IsForeignKey = false;
                colvarNum.IsReadOnly = false;
                
                schema.Columns.Add(colvarNum);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_order_count",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewOrderCount()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewOrderCount(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewOrderCount(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewOrderCount(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("MainBusinessHourGuid")]
        [Bindable(true)]
        public Guid? MainBusinessHourGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid?>("MainBusinessHourGuid");
		    }
            set 
		    {
			    SetColumnValue("MainBusinessHourGuid", value);
            }
        }
	      
        [XmlAttribute("UniqueId")]
        [Bindable(true)]
        public int UniqueId 
	    {
		    get
		    {
			    return GetColumnValue<int>("unique_id");
		    }
            set 
		    {
			    SetColumnValue("unique_id", value);
            }
        }
	      
        [XmlAttribute("DeptId")]
        [Bindable(true)]
        public string DeptId 
	    {
		    get
		    {
			    return GetColumnValue<string>("dept_id");
		    }
            set 
		    {
			    SetColumnValue("dept_id", value);
            }
        }
	      
        [XmlAttribute("ItemName")]
        [Bindable(true)]
        public string ItemName 
	    {
		    get
		    {
			    return GetColumnValue<string>("item_name");
		    }
            set 
		    {
			    SetColumnValue("item_name", value);
            }
        }
	      
        [XmlAttribute("AppTitle")]
        [Bindable(true)]
        public string AppTitle 
	    {
		    get
		    {
			    return GetColumnValue<string>("app_title");
		    }
            set 
		    {
			    SetColumnValue("app_title", value);
            }
        }
	      
        [XmlAttribute("Status")]
        [Bindable(true)]
        public string Status 
	    {
		    get
		    {
			    return GetColumnValue<string>("status");
		    }
            set 
		    {
			    SetColumnValue("status", value);
            }
        }
	      
        [XmlAttribute("Cchannel")]
        [Bindable(true)]
        public string Cchannel 
	    {
		    get
		    {
			    return GetColumnValue<string>("cchannel");
		    }
            set 
		    {
			    SetColumnValue("cchannel", value);
            }
        }
	      
        [XmlAttribute("Num")]
        [Bindable(true)]
        public int? Num 
	    {
		    get
		    {
			    return GetColumnValue<int?>("num");
		    }
            set 
		    {
			    SetColumnValue("num", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string MainBusinessHourGuid = @"MainBusinessHourGuid";
            
            public static string UniqueId = @"unique_id";
            
            public static string DeptId = @"dept_id";
            
            public static string ItemName = @"item_name";
            
            public static string AppTitle = @"app_title";
            
            public static string Status = @"status";
            
            public static string Cchannel = @"cchannel";
            
            public static string Num = @"num";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

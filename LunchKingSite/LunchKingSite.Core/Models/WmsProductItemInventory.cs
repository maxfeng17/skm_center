using System;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using SubSonic;

namespace LunchKingSite.DataOrm
{
    [Serializable]
    public partial class WmsProductItemInventoryCollection : RepositoryList<WmsProductItemInventory, WmsProductItemInventoryCollection>
    {
        public WmsProductItemInventoryCollection() { }

        public WmsProductItemInventoryCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                WmsProductItemInventory o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
    }

    [Serializable]
    public partial class WmsProductItemInventory : RepositoryRecord<WmsProductItemInventory>, IRecordBase
    {
        #region .ctors and Default Settings
        public WmsProductItemInventory()
        {
            SetSQLProps();
            InitSetDefaults();
            MarkNew();
        }

        private void InitSetDefaults() { SetDefaults(); }

        public WmsProductItemInventory(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
                ForceDefaults();
            MarkNew();
        }

        protected static void SetSQLProps() { GetTableSchema(); }

        #endregion

        #region Schema and Query Accessor
        public static Query CreateQuery() { return new Query(Schema); }
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                    SetSQLProps();
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("wms_product_item_inventory", TableType.Table, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = true;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = true;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                colvarId.DefaultSetting = @"";
                colvarId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarId);

                TableSchema.TableColumn colvarItemGuid = new TableSchema.TableColumn(schema);
                colvarItemGuid.ColumnName = "item_guid";
                colvarItemGuid.DataType = DbType.Guid;
                colvarItemGuid.MaxLength = 0;
                colvarItemGuid.AutoIncrement = false;
                colvarItemGuid.IsNullable = false;
                colvarItemGuid.IsPrimaryKey = false;
                colvarItemGuid.IsForeignKey = false;
                colvarItemGuid.IsReadOnly = false;
                colvarItemGuid.DefaultSetting = @"";
                colvarItemGuid.ForeignKeyTableName = "";
                schema.Columns.Add(colvarItemGuid);

                TableSchema.TableColumn colvarPchomeProdId = new TableSchema.TableColumn(schema);
                colvarPchomeProdId.ColumnName = "pchome_prod_id";
                colvarPchomeProdId.DataType = DbType.String;
                colvarPchomeProdId.MaxLength = 50;
                colvarPchomeProdId.AutoIncrement = false;
                colvarPchomeProdId.IsNullable = false;
                colvarPchomeProdId.IsPrimaryKey = false;
                colvarPchomeProdId.IsForeignKey = false;
                colvarPchomeProdId.IsReadOnly = false;
                colvarPchomeProdId.DefaultSetting = @"";
                colvarPchomeProdId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarPchomeProdId);

                TableSchema.TableColumn colvarChecking = new TableSchema.TableColumn(schema);
                colvarChecking.ColumnName = "checking";
                colvarChecking.DataType = DbType.Int32;
                colvarChecking.MaxLength = 0;
                colvarChecking.AutoIncrement = false;
                colvarChecking.IsNullable = false;
                colvarChecking.IsPrimaryKey = false;
                colvarChecking.IsForeignKey = false;
                colvarChecking.IsReadOnly = false;
                colvarChecking.DefaultSetting = @"";
                colvarChecking.ForeignKeyTableName = "";
                schema.Columns.Add(colvarChecking);

                TableSchema.TableColumn colvarGoodOnsale = new TableSchema.TableColumn(schema);
                colvarGoodOnsale.ColumnName = "good_onsale";
                colvarGoodOnsale.DataType = DbType.Int32;
                colvarGoodOnsale.MaxLength = 0;
                colvarGoodOnsale.AutoIncrement = false;
                colvarGoodOnsale.IsNullable = false;
                colvarGoodOnsale.IsPrimaryKey = false;
                colvarGoodOnsale.IsForeignKey = false;
                colvarGoodOnsale.IsReadOnly = false;
                colvarGoodOnsale.DefaultSetting = @"";
                colvarGoodOnsale.ForeignKeyTableName = "";
                schema.Columns.Add(colvarGoodOnsale);

                TableSchema.TableColumn colvarGoodWaiting = new TableSchema.TableColumn(schema);
                colvarGoodWaiting.ColumnName = "good_waiting";
                colvarGoodWaiting.DataType = DbType.Int32;
                colvarGoodWaiting.MaxLength = 0;
                colvarGoodWaiting.AutoIncrement = false;
                colvarGoodWaiting.IsNullable = false;
                colvarGoodWaiting.IsPrimaryKey = false;
                colvarGoodWaiting.IsForeignKey = false;
                colvarGoodWaiting.IsReadOnly = false;
                colvarGoodWaiting.DefaultSetting = @"";
                colvarGoodWaiting.ForeignKeyTableName = "";
                schema.Columns.Add(colvarGoodWaiting);

                TableSchema.TableColumn colvarBad = new TableSchema.TableColumn(schema);
                colvarBad.ColumnName = "bad";
                colvarBad.DataType = DbType.Int32;
                colvarBad.MaxLength = 0;
                colvarBad.AutoIncrement = false;
                colvarBad.IsNullable = false;
                colvarBad.IsPrimaryKey = false;
                colvarBad.IsForeignKey = false;
                colvarBad.IsReadOnly = false;
                colvarBad.DefaultSetting = @"";
                colvarBad.ForeignKeyTableName = "";
                schema.Columns.Add(colvarBad);

                TableSchema.TableColumn colvarLost = new TableSchema.TableColumn(schema);
                colvarLost.ColumnName = "lost";
                colvarLost.DataType = DbType.Int32;
                colvarLost.MaxLength = 0;
                colvarLost.AutoIncrement = false;
                colvarLost.IsNullable = false;
                colvarLost.IsPrimaryKey = false;
                colvarLost.IsForeignKey = false;
                colvarLost.IsReadOnly = false;
                colvarLost.DefaultSetting = @"";
                colvarLost.ForeignKeyTableName = "";
                schema.Columns.Add(colvarLost);

                TableSchema.TableColumn colvarRefund = new TableSchema.TableColumn(schema);
                colvarRefund.ColumnName = "refund";
                colvarRefund.DataType = DbType.Int32;
                colvarRefund.MaxLength = 0;
                colvarRefund.AutoIncrement = false;
                colvarRefund.IsNullable = false;
                colvarRefund.IsPrimaryKey = false;
                colvarRefund.IsForeignKey = false;
                colvarRefund.IsReadOnly = false;
                colvarRefund.DefaultSetting = @"";
                colvarRefund.ForeignKeyTableName = "";
                schema.Columns.Add(colvarRefund);

                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = false;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                colvarCreateTime.DefaultSetting = @"";
                colvarCreateTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateTime);

                TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
                colvarModifyTime.ColumnName = "modify_time";
                colvarModifyTime.DataType = DbType.DateTime;
                colvarModifyTime.MaxLength = 0;
                colvarModifyTime.AutoIncrement = false;
                colvarModifyTime.IsNullable = false;
                colvarModifyTime.IsPrimaryKey = false;
                colvarModifyTime.IsForeignKey = false;
                colvarModifyTime.IsReadOnly = false;
                colvarModifyTime.DefaultSetting = @"";
                colvarModifyTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarModifyTime);

                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("wms_product_item_inventory", schema);
            }
        }
        #endregion

        #region Props

        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id
        {
            get { return GetColumnValue<int>(Columns.Id); }
            set { SetColumnValue(Columns.Id, value); }
        }

        [XmlAttribute("ItemGuid")]
        [Bindable(true)]
        public Guid ItemGuid
        {
            get { return GetColumnValue<Guid>(Columns.ItemGuid); }
            set { SetColumnValue(Columns.ItemGuid, value); }
        }

        [XmlAttribute("PchomeProdId")]
        [Bindable(true)]
        public string PchomeProdId
        {
            get { return GetColumnValue<string>(Columns.PchomeProdId); }
            set { SetColumnValue(Columns.PchomeProdId, value); }
        }

        [XmlAttribute("Checking")]
        [Bindable(true)]
        public int Checking
        {
            get { return GetColumnValue<int>(Columns.Checking); }
            set { SetColumnValue(Columns.Checking, value); }
        }

        [XmlAttribute("GoodOnsale")]
        [Bindable(true)]
        public int GoodOnsale
        {
            get { return GetColumnValue<int>(Columns.GoodOnsale); }
            set { SetColumnValue(Columns.GoodOnsale, value); }
        }

        [XmlAttribute("GoodWaiting")]
        [Bindable(true)]
        public int GoodWaiting
        {
            get { return GetColumnValue<int>(Columns.GoodWaiting); }
            set { SetColumnValue(Columns.GoodWaiting, value); }
        }

        [XmlAttribute("Bad")]
        [Bindable(true)]
        public int Bad
        {
            get { return GetColumnValue<int>(Columns.Bad); }
            set { SetColumnValue(Columns.Bad, value); }
        }

        [XmlAttribute("Lost")]
        [Bindable(true)]
        public int Lost
        {
            get { return GetColumnValue<int>(Columns.Lost); }
            set { SetColumnValue(Columns.Lost, value); }
        }

        [XmlAttribute("Refund")]
        [Bindable(true)]
        public int Refund
        {
            get { return GetColumnValue<int>(Columns.Refund); }
            set { SetColumnValue(Columns.Refund, value); }
        }

        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime CreateTime
        {
            get { return GetColumnValue<DateTime>(Columns.CreateTime); }
            set { SetColumnValue(Columns.CreateTime, value); }
        }

        [XmlAttribute("ModifyTime")]
        [Bindable(true)]
        public DateTime ModifyTime
        {
            get { return GetColumnValue<DateTime>(Columns.ModifyTime); }
            set { SetColumnValue(Columns.ModifyTime, value); }
        }

        #endregion

        #region Typed Columns

        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }

        public static TableSchema.TableColumn ItemGuidColumn
        {
            get { return Schema.Columns[1]; }
        }

        public static TableSchema.TableColumn PchomeProdIdColumn
        {
            get { return Schema.Columns[2]; }
        }

        public static TableSchema.TableColumn CheckingColumn
        {
            get { return Schema.Columns[3]; }
        }

        public static TableSchema.TableColumn GoodOnsaleColumn
        {
            get { return Schema.Columns[4]; }
        }

        public static TableSchema.TableColumn GoodWaitingColumn
        {
            get { return Schema.Columns[5]; }
        }

        public static TableSchema.TableColumn BadColumn
        {
            get { return Schema.Columns[6]; }
        }

        public static TableSchema.TableColumn LostColumn
        {
            get { return Schema.Columns[7]; }
        }

        public static TableSchema.TableColumn RefundColumn
        {
            get { return Schema.Columns[8]; }
        }

        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[9]; }
        }

        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[10]; }
        }

        #endregion

        #region Columns Struct

        public struct Columns
        {
            public static string Id = @"id";
            public static string ItemGuid = @"item_guid";
            public static string PchomeProdId = @"pchome_prod_id";
            public static string Checking = @"checking";
            public static string GoodOnsale = @"good_onsale";
            public static string GoodWaiting = @"good_waiting";
            public static string Bad = @"bad";
            public static string Lost = @"lost";
            public static string Refund = @"refund";
            public static string CreateTime = @"create_time";
            public static string ModifyTime = @"modify_time";
        }

        #endregion

    }
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the ShoppingGuideChannel class.
	/// </summary>
    [Serializable]
	public partial class ShoppingGuideChannelCollection : RepositoryList<ShoppingGuideChannel, ShoppingGuideChannelCollection>
	{	   
		public ShoppingGuideChannelCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>ShoppingGuideChannelCollection</returns>
		public ShoppingGuideChannelCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                ShoppingGuideChannel o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
	}
	
	
	/// <summary>
	/// This is an ActiveRecord class which wraps the shopping_guide_channel table.
	/// </summary>
	
	[Serializable]
	public partial class ShoppingGuideChannel : RepositoryRecord<ShoppingGuideChannel>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public ShoppingGuideChannel()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public ShoppingGuideChannel(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("shopping_guide_channel", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarChannelId = new TableSchema.TableColumn(schema);
				colvarChannelId.ColumnName = "channel_id";
				colvarChannelId.DataType = DbType.Byte;
				colvarChannelId.MaxLength = 0;
				colvarChannelId.AutoIncrement = false;
				colvarChannelId.IsNullable = false;
				colvarChannelId.IsPrimaryKey = true;
				colvarChannelId.IsForeignKey = false;
				colvarChannelId.IsReadOnly = false;
				colvarChannelId.DefaultSetting = @"";
				colvarChannelId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarChannelId);
				
				TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
				colvarModifyTime.ColumnName = "modify_time";
				colvarModifyTime.DataType = DbType.DateTime;
				colvarModifyTime.MaxLength = 0;
				colvarModifyTime.AutoIncrement = false;
				colvarModifyTime.IsNullable = false;
				colvarModifyTime.IsPrimaryKey = false;
				colvarModifyTime.IsForeignKey = false;
				colvarModifyTime.IsReadOnly = false;
				colvarModifyTime.DefaultSetting = @"";
				colvarModifyTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyTime);
				
				TableSchema.TableColumn colvarDealCount = new TableSchema.TableColumn(schema);
				colvarDealCount.ColumnName = "deal_count";
				colvarDealCount.DataType = DbType.Int32;
				colvarDealCount.MaxLength = 0;
				colvarDealCount.AutoIncrement = false;
				colvarDealCount.IsNullable = false;
				colvarDealCount.IsPrimaryKey = false;
				colvarDealCount.IsForeignKey = false;
				colvarDealCount.IsReadOnly = false;
				colvarDealCount.DefaultSetting = @"";
				colvarDealCount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDealCount);
				
				TableSchema.TableColumn colvarBatchId = new TableSchema.TableColumn(schema);
				colvarBatchId.ColumnName = "batch_id";
				colvarBatchId.DataType = DbType.AnsiString;
				colvarBatchId.MaxLength = 36;
				colvarBatchId.AutoIncrement = false;
				colvarBatchId.IsNullable = false;
				colvarBatchId.IsPrimaryKey = false;
				colvarBatchId.IsForeignKey = false;
				colvarBatchId.IsReadOnly = false;
				colvarBatchId.DefaultSetting = @"";
				colvarBatchId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBatchId);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("shopping_guide_channel",schema);
			}
		}
		#endregion
		
		#region Props
		
		[XmlAttribute("ChannelId")]
		[Bindable(true)]
		public byte ChannelId 
		{
			get { return GetColumnValue<byte>(Columns.ChannelId); }
			set { SetColumnValue(Columns.ChannelId, value); }
		}
		
		[XmlAttribute("ModifyTime")]
		[Bindable(true)]
		public DateTime ModifyTime 
		{
			get { return GetColumnValue<DateTime>(Columns.ModifyTime); }
			set { SetColumnValue(Columns.ModifyTime, value); }
		}
		
		[XmlAttribute("DealCount")]
		[Bindable(true)]
		public int DealCount 
		{
			get { return GetColumnValue<int>(Columns.DealCount); }
			set { SetColumnValue(Columns.DealCount, value); }
		}
		
		[XmlAttribute("BatchId")]
		[Bindable(true)]
		public string BatchId 
		{
			get { return GetColumnValue<string>(Columns.BatchId); }
			set { SetColumnValue(Columns.BatchId, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn ChannelIdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn DealCountColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn BatchIdColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string ChannelId = @"channel_id";
			 public static string ModifyTime = @"modify_time";
			 public static string DealCount = @"deal_count";
			 public static string BatchId = @"batch_id";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

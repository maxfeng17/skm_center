using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the HiDealCategory class.
	/// </summary>
    [Serializable]
	public partial class HiDealCategoryCollection : RepositoryList<HiDealCategory, HiDealCategoryCollection>
	{	   
		public HiDealCategoryCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>HiDealCategoryCollection</returns>
		public HiDealCategoryCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                HiDealCategory o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the hi_deal_category table.
	/// </summary>
	[Serializable]
	public partial class HiDealCategory : RepositoryRecord<HiDealCategory>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public HiDealCategory()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public HiDealCategory(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("hi_deal_category", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarHiDealId = new TableSchema.TableColumn(schema);
				colvarHiDealId.ColumnName = "hi_deal_id";
				colvarHiDealId.DataType = DbType.Int32;
				colvarHiDealId.MaxLength = 0;
				colvarHiDealId.AutoIncrement = false;
				colvarHiDealId.IsNullable = false;
				colvarHiDealId.IsPrimaryKey = true;
				colvarHiDealId.IsForeignKey = true;
				colvarHiDealId.IsReadOnly = false;
				colvarHiDealId.DefaultSetting = @"";
				
					colvarHiDealId.ForeignKeyTableName = "hi_deal_deals";
				schema.Columns.Add(colvarHiDealId);
				
				TableSchema.TableColumn colvarCategoryCodeGroup = new TableSchema.TableColumn(schema);
				colvarCategoryCodeGroup.ColumnName = "category_code_group";
				colvarCategoryCodeGroup.DataType = DbType.AnsiString;
				colvarCategoryCodeGroup.MaxLength = 50;
				colvarCategoryCodeGroup.AutoIncrement = false;
				colvarCategoryCodeGroup.IsNullable = false;
				colvarCategoryCodeGroup.IsPrimaryKey = false;
				colvarCategoryCodeGroup.IsForeignKey = true;
				colvarCategoryCodeGroup.IsReadOnly = false;
				colvarCategoryCodeGroup.DefaultSetting = @"";
				
					colvarCategoryCodeGroup.ForeignKeyTableName = "system_code";
				schema.Columns.Add(colvarCategoryCodeGroup);
				
				TableSchema.TableColumn colvarCategoryCodeId = new TableSchema.TableColumn(schema);
				colvarCategoryCodeId.ColumnName = "category_code_id";
				colvarCategoryCodeId.DataType = DbType.Int32;
				colvarCategoryCodeId.MaxLength = 0;
				colvarCategoryCodeId.AutoIncrement = false;
				colvarCategoryCodeId.IsNullable = false;
				colvarCategoryCodeId.IsPrimaryKey = false;
				colvarCategoryCodeId.IsForeignKey = true;
				colvarCategoryCodeId.IsReadOnly = false;
				colvarCategoryCodeId.DefaultSetting = @"";
				
					colvarCategoryCodeId.ForeignKeyTableName = "system_code";
				schema.Columns.Add(colvarCategoryCodeId);
				
				TableSchema.TableColumn colvarCategoryCodeName = new TableSchema.TableColumn(schema);
				colvarCategoryCodeName.ColumnName = "category_code_name";
				colvarCategoryCodeName.DataType = DbType.String;
				colvarCategoryCodeName.MaxLength = 50;
				colvarCategoryCodeName.AutoIncrement = false;
				colvarCategoryCodeName.IsNullable = false;
				colvarCategoryCodeName.IsPrimaryKey = true;
				colvarCategoryCodeName.IsForeignKey = false;
				colvarCategoryCodeName.IsReadOnly = false;
				colvarCategoryCodeName.DefaultSetting = @"";
				colvarCategoryCodeName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCategoryCodeName);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("hi_deal_category",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("HiDealId")]
		[Bindable(true)]
		public int HiDealId 
		{
			get { return GetColumnValue<int>(Columns.HiDealId); }
			set { SetColumnValue(Columns.HiDealId, value); }
		}
		  
		[XmlAttribute("CategoryCodeGroup")]
		[Bindable(true)]
		public string CategoryCodeGroup 
		{
			get { return GetColumnValue<string>(Columns.CategoryCodeGroup); }
			set { SetColumnValue(Columns.CategoryCodeGroup, value); }
		}
		  
		[XmlAttribute("CategoryCodeId")]
		[Bindable(true)]
		public int CategoryCodeId 
		{
			get { return GetColumnValue<int>(Columns.CategoryCodeId); }
			set { SetColumnValue(Columns.CategoryCodeId, value); }
		}
		  
		[XmlAttribute("CategoryCodeName")]
		[Bindable(true)]
		public string CategoryCodeName 
		{
			get { return GetColumnValue<string>(Columns.CategoryCodeName); }
			set { SetColumnValue(Columns.CategoryCodeName, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (2)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn HiDealIdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn CategoryCodeGroupColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn CategoryCodeIdColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn CategoryCodeNameColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string HiDealId = @"hi_deal_id";
			 public static string CategoryCodeGroup = @"category_code_group";
			 public static string CategoryCodeId = @"category_code_id";
			 public static string CategoryCodeName = @"category_code_name";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

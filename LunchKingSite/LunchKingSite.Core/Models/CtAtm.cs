using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the CtAtm class.
	/// </summary>
    [Serializable]
	public partial class CtAtmCollection : RepositoryList<CtAtm, CtAtmCollection>
	{	   
		public CtAtmCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>CtAtmCollection</returns>
		public CtAtmCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                CtAtm o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the ct_atm table.
	/// </summary>
	[Serializable]
	public partial class CtAtm : RepositoryRecord<CtAtm>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public CtAtm()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public CtAtm(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("ct_atm", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarSi = new TableSchema.TableColumn(schema);
				colvarSi.ColumnName = "si";
				colvarSi.DataType = DbType.Int32;
				colvarSi.MaxLength = 0;
				colvarSi.AutoIncrement = true;
				colvarSi.IsNullable = false;
				colvarSi.IsPrimaryKey = true;
				colvarSi.IsForeignKey = false;
				colvarSi.IsReadOnly = false;
				colvarSi.DefaultSetting = @"";
				colvarSi.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSi);
				
				TableSchema.TableColumn colvarPaymentId = new TableSchema.TableColumn(schema);
				colvarPaymentId.ColumnName = "payment_id";
				colvarPaymentId.DataType = DbType.AnsiString;
				colvarPaymentId.MaxLength = 50;
				colvarPaymentId.AutoIncrement = false;
				colvarPaymentId.IsNullable = false;
				colvarPaymentId.IsPrimaryKey = false;
				colvarPaymentId.IsForeignKey = false;
				colvarPaymentId.IsReadOnly = false;
				colvarPaymentId.DefaultSetting = @"";
				colvarPaymentId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPaymentId);
				
				TableSchema.TableColumn colvarAmount = new TableSchema.TableColumn(schema);
				colvarAmount.ColumnName = "amount";
				colvarAmount.DataType = DbType.Int32;
				colvarAmount.MaxLength = 0;
				colvarAmount.AutoIncrement = false;
				colvarAmount.IsNullable = false;
				colvarAmount.IsPrimaryKey = false;
				colvarAmount.IsForeignKey = false;
				colvarAmount.IsReadOnly = false;
				colvarAmount.DefaultSetting = @"";
				colvarAmount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAmount);
				
				TableSchema.TableColumn colvarVirtualAccount = new TableSchema.TableColumn(schema);
				colvarVirtualAccount.ColumnName = "virtual_account";
				colvarVirtualAccount.DataType = DbType.AnsiString;
				colvarVirtualAccount.MaxLength = 16;
				colvarVirtualAccount.AutoIncrement = false;
				colvarVirtualAccount.IsNullable = false;
				colvarVirtualAccount.IsPrimaryKey = false;
				colvarVirtualAccount.IsForeignKey = false;
				colvarVirtualAccount.IsReadOnly = false;
				colvarVirtualAccount.DefaultSetting = @"";
				colvarVirtualAccount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarVirtualAccount);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarPayTime = new TableSchema.TableColumn(schema);
				colvarPayTime.ColumnName = "pay_time";
				colvarPayTime.DataType = DbType.DateTime;
				colvarPayTime.MaxLength = 0;
				colvarPayTime.AutoIncrement = false;
				colvarPayTime.IsNullable = true;
				colvarPayTime.IsPrimaryKey = false;
				colvarPayTime.IsForeignKey = false;
				colvarPayTime.IsReadOnly = false;
				colvarPayTime.DefaultSetting = @"";
				colvarPayTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPayTime);
				
				TableSchema.TableColumn colvarBusinessGuid = new TableSchema.TableColumn(schema);
				colvarBusinessGuid.ColumnName = "business_guid";
				colvarBusinessGuid.DataType = DbType.Guid;
				colvarBusinessGuid.MaxLength = 0;
				colvarBusinessGuid.AutoIncrement = false;
				colvarBusinessGuid.IsNullable = false;
				colvarBusinessGuid.IsPrimaryKey = false;
				colvarBusinessGuid.IsForeignKey = false;
				colvarBusinessGuid.IsReadOnly = false;
				colvarBusinessGuid.DefaultSetting = @"";
				colvarBusinessGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBusinessGuid);
				
				TableSchema.TableColumn colvarOrderId = new TableSchema.TableColumn(schema);
				colvarOrderId.ColumnName = "order_id";
				colvarOrderId.DataType = DbType.AnsiString;
				colvarOrderId.MaxLength = 30;
				colvarOrderId.AutoIncrement = false;
				colvarOrderId.IsNullable = false;
				colvarOrderId.IsPrimaryKey = false;
				colvarOrderId.IsForeignKey = false;
				colvarOrderId.IsReadOnly = false;
				colvarOrderId.DefaultSetting = @"";
				colvarOrderId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderId);
				
				TableSchema.TableColumn colvarOrderGuid = new TableSchema.TableColumn(schema);
				colvarOrderGuid.ColumnName = "order_guid";
				colvarOrderGuid.DataType = DbType.Guid;
				colvarOrderGuid.MaxLength = 0;
				colvarOrderGuid.AutoIncrement = false;
				colvarOrderGuid.IsNullable = false;
				colvarOrderGuid.IsPrimaryKey = false;
				colvarOrderGuid.IsForeignKey = false;
				colvarOrderGuid.IsReadOnly = false;
				colvarOrderGuid.DefaultSetting = @"";
				colvarOrderGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderGuid);
				
				TableSchema.TableColumn colvarSenq = new TableSchema.TableColumn(schema);
				colvarSenq.ColumnName = "senq";
				colvarSenq.DataType = DbType.AnsiString;
				colvarSenq.MaxLength = 30;
				colvarSenq.AutoIncrement = false;
				colvarSenq.IsNullable = false;
				colvarSenq.IsPrimaryKey = false;
				colvarSenq.IsForeignKey = false;
				colvarSenq.IsReadOnly = false;
				colvarSenq.DefaultSetting = @"";
				colvarSenq.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSenq);
				
				TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
				colvarStatus.ColumnName = "status";
				colvarStatus.DataType = DbType.Int32;
				colvarStatus.MaxLength = 0;
				colvarStatus.AutoIncrement = false;
				colvarStatus.IsNullable = false;
				colvarStatus.IsPrimaryKey = false;
				colvarStatus.IsForeignKey = false;
				colvarStatus.IsReadOnly = false;
				colvarStatus.DefaultSetting = @"";
				colvarStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStatus);
				
				TableSchema.TableColumn colvarUserId = new TableSchema.TableColumn(schema);
				colvarUserId.ColumnName = "user_id";
				colvarUserId.DataType = DbType.Int32;
				colvarUserId.MaxLength = 0;
				colvarUserId.AutoIncrement = false;
				colvarUserId.IsNullable = false;
				colvarUserId.IsPrimaryKey = false;
				colvarUserId.IsForeignKey = true;
				colvarUserId.IsReadOnly = false;
				
						colvarUserId.DefaultSetting = @"((0))";
				
					colvarUserId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUserId);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("ct_atm",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Si")]
		[Bindable(true)]
		public int Si 
		{
			get { return GetColumnValue<int>(Columns.Si); }
			set { SetColumnValue(Columns.Si, value); }
		}
		  
		[XmlAttribute("PaymentId")]
		[Bindable(true)]
		public string PaymentId 
		{
			get { return GetColumnValue<string>(Columns.PaymentId); }
			set { SetColumnValue(Columns.PaymentId, value); }
		}
		  
		[XmlAttribute("Amount")]
		[Bindable(true)]
		public int Amount 
		{
			get { return GetColumnValue<int>(Columns.Amount); }
			set { SetColumnValue(Columns.Amount, value); }
		}
		  
		[XmlAttribute("VirtualAccount")]
		[Bindable(true)]
		public string VirtualAccount 
		{
			get { return GetColumnValue<string>(Columns.VirtualAccount); }
			set { SetColumnValue(Columns.VirtualAccount, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		  
		[XmlAttribute("PayTime")]
		[Bindable(true)]
		public DateTime? PayTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.PayTime); }
			set { SetColumnValue(Columns.PayTime, value); }
		}
		  
		[XmlAttribute("BusinessGuid")]
		[Bindable(true)]
		public Guid BusinessGuid 
		{
			get { return GetColumnValue<Guid>(Columns.BusinessGuid); }
			set { SetColumnValue(Columns.BusinessGuid, value); }
		}
		  
		[XmlAttribute("OrderId")]
		[Bindable(true)]
		public string OrderId 
		{
			get { return GetColumnValue<string>(Columns.OrderId); }
			set { SetColumnValue(Columns.OrderId, value); }
		}
		  
		[XmlAttribute("OrderGuid")]
		[Bindable(true)]
		public Guid OrderGuid 
		{
			get { return GetColumnValue<Guid>(Columns.OrderGuid); }
			set { SetColumnValue(Columns.OrderGuid, value); }
		}
		  
		[XmlAttribute("Senq")]
		[Bindable(true)]
		public string Senq 
		{
			get { return GetColumnValue<string>(Columns.Senq); }
			set { SetColumnValue(Columns.Senq, value); }
		}
		  
		[XmlAttribute("Status")]
		[Bindable(true)]
		public int Status 
		{
			get { return GetColumnValue<int>(Columns.Status); }
			set { SetColumnValue(Columns.Status, value); }
		}
		  
		[XmlAttribute("UserId")]
		[Bindable(true)]
		public int UserId 
		{
			get { return GetColumnValue<int>(Columns.UserId); }
			set { SetColumnValue(Columns.UserId, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn SiColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn PaymentIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn AmountColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn VirtualAccountColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn PayTimeColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn BusinessGuidColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn OrderIdColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn OrderGuidColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn SenqColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn StatusColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        public static TableSchema.TableColumn UserIdColumn
        {
            get { return Schema.Columns[11]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Si = @"si";
			 public static string PaymentId = @"payment_id";
			 public static string Amount = @"amount";
			 public static string VirtualAccount = @"virtual_account";
			 public static string CreateTime = @"create_time";
			 public static string PayTime = @"pay_time";
			 public static string BusinessGuid = @"business_guid";
			 public static string OrderId = @"order_id";
			 public static string OrderGuid = @"order_guid";
			 public static string Senq = @"senq";
			 public static string Status = @"status";
			 public static string UserId = @"user_id";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

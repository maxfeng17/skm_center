using System;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using SubSonic;

namespace LunchKingSite.DataOrm
{
	[Serializable]
	public partial class OrderLogCollection : RepositoryList<OrderLog, OrderLogCollection>
	{
			public OrderLogCollection() {}

			public OrderLogCollection Filter()
			{
					for (int i = this.Count - 1; i > -1; i--)
					{
							OrderLog o = this[i];
							foreach (SubSonic.Where w in this.wheres)
							{
									bool remove = false;
									System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
									if(pi.CanRead)
									{
											object val = pi.GetValue(o, null);
											switch(w.Comparison)
											{
													case SubSonic.Comparison.Equals:
															if(!val.Equals(w.ParameterValue))
															{
																	remove = true;
															}
															break;
											}
									}
									if(remove)
									{
											this.Remove(o);
											break;
									}
							}
					}
					return this;
			}
	}

	[Serializable]
	public partial class OrderLog : RepositoryRecord<OrderLog>, IRecordBase
	{
		#region .ctors and Default Settings
		public OrderLog()
		{
			SetSQLProps();
			InitSetDefaults();
			MarkNew();
		}

		private void InitSetDefaults() { SetDefaults(); }

		public OrderLog(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}

		protected static void SetSQLProps() { GetTableSchema(); }

		#endregion

		#region Schema and Query Accessor
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}

		private static void GetTableSchema()
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("order_log", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);

				TableSchema.TableColumn colvarContent = new TableSchema.TableColumn(schema);
				colvarContent.ColumnName = "content";
				colvarContent.DataType = DbType.String;
				colvarContent.MaxLength = 2147483647;
				colvarContent.AutoIncrement = false;
				colvarContent.IsNullable = true;
				colvarContent.IsPrimaryKey = false;
				colvarContent.IsForeignKey = false;
				colvarContent.IsReadOnly = false;
				colvarContent.DefaultSetting = @"";
				colvarContent.ForeignKeyTableName = "";
				schema.Columns.Add(colvarContent);

				TableSchema.TableColumn colvarUserId = new TableSchema.TableColumn(schema);
				colvarUserId.ColumnName = "user_id";
				colvarUserId.DataType = DbType.Int32;
				colvarUserId.MaxLength = 0;
				colvarUserId.AutoIncrement = false;
				colvarUserId.IsNullable = false;
				colvarUserId.IsPrimaryKey = false;
				colvarUserId.IsForeignKey = false;
				colvarUserId.IsReadOnly = false;
				colvarUserId.DefaultSetting = @"";
				colvarUserId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUserId);

				TableSchema.TableColumn colvarResult = new TableSchema.TableColumn(schema);
				colvarResult.ColumnName = "result";
				colvarResult.DataType = DbType.Int32;
				colvarResult.MaxLength = 0;
				colvarResult.AutoIncrement = false;
				colvarResult.IsNullable = true;
				colvarResult.IsPrimaryKey = false;
				colvarResult.IsForeignKey = false;
				colvarResult.IsReadOnly = false;
				colvarResult.DefaultSetting = @"";
				colvarResult.ForeignKeyTableName = "";
				schema.Columns.Add(colvarResult);

				TableSchema.TableColumn colvarIsValid = new TableSchema.TableColumn(schema);
				colvarIsValid.ColumnName = "is_valid";
				colvarIsValid.DataType = DbType.Boolean;
				colvarIsValid.MaxLength = 0;
				colvarIsValid.AutoIncrement = false;
				colvarIsValid.IsNullable = false;
				colvarIsValid.IsPrimaryKey = false;
				colvarIsValid.IsForeignKey = false;
				colvarIsValid.IsReadOnly = false;
				colvarIsValid.DefaultSetting = @"";
				colvarIsValid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsValid);

				TableSchema.TableColumn colvarOrderFromType = new TableSchema.TableColumn(schema);
				colvarOrderFromType.ColumnName = "order_from_type";
				colvarOrderFromType.DataType = DbType.Int32;
				colvarOrderFromType.MaxLength = 0;
				colvarOrderFromType.AutoIncrement = false;
				colvarOrderFromType.IsNullable = false;
				colvarOrderFromType.IsPrimaryKey = false;
				colvarOrderFromType.IsForeignKey = false;
				colvarOrderFromType.IsReadOnly = false;
				colvarOrderFromType.DefaultSetting = @"";
				colvarOrderFromType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderFromType);

				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);

				TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
				colvarModifyTime.ColumnName = "modify_time";
				colvarModifyTime.DataType = DbType.DateTime;
				colvarModifyTime.MaxLength = 0;
				colvarModifyTime.AutoIncrement = false;
				colvarModifyTime.IsNullable = true;
				colvarModifyTime.IsPrimaryKey = false;
				colvarModifyTime.IsForeignKey = false;
				colvarModifyTime.IsReadOnly = false;
				colvarModifyTime.DefaultSetting = @"";
				colvarModifyTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyTime);

				TableSchema.TableColumn colvarServerName = new TableSchema.TableColumn(schema);
				colvarServerName.ColumnName = "server_name";
				colvarServerName.DataType = DbType.String;
				colvarServerName.MaxLength = 100;
				colvarServerName.AutoIncrement = false;
				colvarServerName.IsNullable = true;
				colvarServerName.IsPrimaryKey = false;
				colvarServerName.IsForeignKey = false;
				colvarServerName.IsReadOnly = false;
				colvarServerName.DefaultSetting = @"";
				colvarServerName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarServerName);

				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("order_log",schema);
			}
		}
		#endregion

		#region Props

		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}

		[XmlAttribute("Content")]
		[Bindable(true)]
		public string Content
		{
			get { return GetColumnValue<string>(Columns.Content); }
			set { SetColumnValue(Columns.Content, value); }
		}

		[XmlAttribute("UserId")]
		[Bindable(true)]
		public int UserId
		{
			get { return GetColumnValue<int>(Columns.UserId); }
			set { SetColumnValue(Columns.UserId, value); }
		}

		[XmlAttribute("Result")]
		[Bindable(true)]
		public int? Result
		{
			get { return GetColumnValue<int?>(Columns.Result); }
			set { SetColumnValue(Columns.Result, value); }
		}

		[XmlAttribute("IsValid")]
		[Bindable(true)]
		public bool IsValid
		{
			get { return GetColumnValue<bool>(Columns.IsValid); }
			set { SetColumnValue(Columns.IsValid, value); }
		}

		[XmlAttribute("OrderFromType")]
		[Bindable(true)]
		public int OrderFromType
		{
			get { return GetColumnValue<int>(Columns.OrderFromType); }
			set { SetColumnValue(Columns.OrderFromType, value); }
		}

		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}

		[XmlAttribute("ModifyTime")]
		[Bindable(true)]
		public DateTime? ModifyTime
		{
			get { return GetColumnValue<DateTime?>(Columns.ModifyTime); }
			set { SetColumnValue(Columns.ModifyTime, value); }
		}

		[XmlAttribute("ServerName")]
		[Bindable(true)]
		public string ServerName
		{
			get { return GetColumnValue<string>(Columns.ServerName); }
			set { SetColumnValue(Columns.ServerName, value); }
		}

		#endregion

		#region Typed Columns

		public static TableSchema.TableColumn IdColumn
		{
			get { return Schema.Columns[0]; }
		}

		public static TableSchema.TableColumn ContentColumn
		{
			get { return Schema.Columns[1]; }
		}

		public static TableSchema.TableColumn UserIdColumn
		{
			get { return Schema.Columns[2]; }
		}

		public static TableSchema.TableColumn ResultColumn
		{
			get { return Schema.Columns[3]; }
		}

		public static TableSchema.TableColumn IsValidColumn
		{
			get { return Schema.Columns[4]; }
		}

		public static TableSchema.TableColumn OrderFromTypeColumn
		{
			get { return Schema.Columns[5]; }
		}

		public static TableSchema.TableColumn CreateTimeColumn
		{
			get { return Schema.Columns[6]; }
		}

		public static TableSchema.TableColumn ModifyTimeColumn
		{
			get { return Schema.Columns[7]; }
		}

		public static TableSchema.TableColumn ServerNameColumn
		{
			get { return Schema.Columns[8]; }
		}

		#endregion

		#region Columns Struct

		public struct Columns
		{
			public static string Id = @"id";
			public static string Content = @"content";
			public static string UserId = @"user_id";
			public static string Result = @"result";
			public static string IsValid = @"is_valid";
			public static string OrderFromType = @"order_from_type";
			public static string CreateTime = @"create_time";
			public static string ModifyTime = @"modify_time";
			public static string ServerName = @"server_name";
		}

		#endregion

	}
}

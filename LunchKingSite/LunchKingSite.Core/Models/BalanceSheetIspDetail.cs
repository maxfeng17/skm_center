using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the BalanceSheetIspDetail class.
	/// </summary>
    [Serializable]
	public partial class BalanceSheetIspDetailCollection : RepositoryList<BalanceSheetIspDetail, BalanceSheetIspDetailCollection>
	{	   
		public BalanceSheetIspDetailCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>BalanceSheetIspDetailCollection</returns>
		public BalanceSheetIspDetailCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                BalanceSheetIspDetail o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the balance_sheet_isp_detail table.
	/// </summary>
	[Serializable]
	public partial class BalanceSheetIspDetail : RepositoryRecord<BalanceSheetIspDetail>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public BalanceSheetIspDetail()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public BalanceSheetIspDetail(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("balance_sheet_isp_detail", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarBalanceSheetId = new TableSchema.TableColumn(schema);
				colvarBalanceSheetId.ColumnName = "balance_sheet_id";
				colvarBalanceSheetId.DataType = DbType.Int32;
				colvarBalanceSheetId.MaxLength = 0;
				colvarBalanceSheetId.AutoIncrement = false;
				colvarBalanceSheetId.IsNullable = false;
				colvarBalanceSheetId.IsPrimaryKey = true;
				colvarBalanceSheetId.IsForeignKey = true;
				colvarBalanceSheetId.IsReadOnly = false;
				colvarBalanceSheetId.DefaultSetting = @"";
				
					colvarBalanceSheetId.ForeignKeyTableName = "balance_sheet";
				schema.Columns.Add(colvarBalanceSheetId);
				
				TableSchema.TableColumn colvarOrderGuid = new TableSchema.TableColumn(schema);
				colvarOrderGuid.ColumnName = "order_guid";
				colvarOrderGuid.DataType = DbType.Guid;
				colvarOrderGuid.MaxLength = 0;
				colvarOrderGuid.AutoIncrement = false;
				colvarOrderGuid.IsNullable = false;
				colvarOrderGuid.IsPrimaryKey = true;
				colvarOrderGuid.IsForeignKey = false;
				colvarOrderGuid.IsReadOnly = false;
				colvarOrderGuid.DefaultSetting = @"";
				colvarOrderGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderGuid);
				
				TableSchema.TableColumn colvarProductDeliveryType = new TableSchema.TableColumn(schema);
				colvarProductDeliveryType.ColumnName = "product_delivery_type";
				colvarProductDeliveryType.DataType = DbType.Int32;
				colvarProductDeliveryType.MaxLength = 0;
				colvarProductDeliveryType.AutoIncrement = false;
				colvarProductDeliveryType.IsNullable = false;
				colvarProductDeliveryType.IsPrimaryKey = false;
				colvarProductDeliveryType.IsForeignKey = false;
				colvarProductDeliveryType.IsReadOnly = false;
				colvarProductDeliveryType.DefaultSetting = @"";
				colvarProductDeliveryType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarProductDeliveryType);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("balance_sheet_isp_detail",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("BalanceSheetId")]
		[Bindable(true)]
		public int BalanceSheetId 
		{
			get { return GetColumnValue<int>(Columns.BalanceSheetId); }
			set { SetColumnValue(Columns.BalanceSheetId, value); }
		}
		  
		[XmlAttribute("OrderGuid")]
		[Bindable(true)]
		public Guid OrderGuid 
		{
			get { return GetColumnValue<Guid>(Columns.OrderGuid); }
			set { SetColumnValue(Columns.OrderGuid, value); }
		}
		  
		[XmlAttribute("ProductDeliveryType")]
		[Bindable(true)]
		public int ProductDeliveryType 
		{
			get { return GetColumnValue<int>(Columns.ProductDeliveryType); }
			set { SetColumnValue(Columns.ProductDeliveryType, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (1)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn BalanceSheetIdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn OrderGuidColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn ProductDeliveryTypeColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string BalanceSheetId = @"balance_sheet_id";
			 public static string OrderGuid = @"order_guid";
			 public static string ProductDeliveryType = @"product_delivery_type";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

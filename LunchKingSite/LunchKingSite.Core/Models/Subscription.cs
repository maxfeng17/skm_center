using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the Subscription class.
	/// </summary>
    [Serializable]
	public partial class SubscriptionCollection : RepositoryList<Subscription, SubscriptionCollection>
	{	   
		public SubscriptionCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>SubscriptionCollection</returns>
		public SubscriptionCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                Subscription o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the subscription table.
	/// </summary>
	[Serializable]
	public partial class Subscription : RepositoryRecord<Subscription>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public Subscription()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public Subscription(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("subscription", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarCityId = new TableSchema.TableColumn(schema);
				colvarCityId.ColumnName = "city_id";
				colvarCityId.DataType = DbType.Int32;
				colvarCityId.MaxLength = 0;
				colvarCityId.AutoIncrement = false;
				colvarCityId.IsNullable = true;
				colvarCityId.IsPrimaryKey = false;
				colvarCityId.IsForeignKey = true;
				colvarCityId.IsReadOnly = false;
				colvarCityId.DefaultSetting = @"";
				
					colvarCityId.ForeignKeyTableName = "city";
				schema.Columns.Add(colvarCityId);
				
				TableSchema.TableColumn colvarEmail = new TableSchema.TableColumn(schema);
				colvarEmail.ColumnName = "email";
				colvarEmail.DataType = DbType.String;
				colvarEmail.MaxLength = 250;
				colvarEmail.AutoIncrement = false;
				colvarEmail.IsNullable = true;
				colvarEmail.IsPrimaryKey = false;
				colvarEmail.IsForeignKey = false;
				colvarEmail.IsReadOnly = false;
				colvarEmail.DefaultSetting = @"";
				colvarEmail.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEmail);
				
				TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
				colvarStatus.ColumnName = "status";
				colvarStatus.DataType = DbType.Int32;
				colvarStatus.MaxLength = 0;
				colvarStatus.AutoIncrement = false;
				colvarStatus.IsNullable = true;
				colvarStatus.IsPrimaryKey = false;
				colvarStatus.IsForeignKey = false;
				colvarStatus.IsReadOnly = false;
				colvarStatus.DefaultSetting = @"";
				colvarStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStatus);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = true;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarUnsubscriptionTime = new TableSchema.TableColumn(schema);
				colvarUnsubscriptionTime.ColumnName = "unsubscription_time";
				colvarUnsubscriptionTime.DataType = DbType.DateTime;
				colvarUnsubscriptionTime.MaxLength = 0;
				colvarUnsubscriptionTime.AutoIncrement = false;
				colvarUnsubscriptionTime.IsNullable = true;
				colvarUnsubscriptionTime.IsPrimaryKey = false;
				colvarUnsubscriptionTime.IsForeignKey = false;
				colvarUnsubscriptionTime.IsReadOnly = false;
				colvarUnsubscriptionTime.DefaultSetting = @"";
				colvarUnsubscriptionTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUnsubscriptionTime);
				
				TableSchema.TableColumn colvarResubscriptionTime = new TableSchema.TableColumn(schema);
				colvarResubscriptionTime.ColumnName = "resubscription_time";
				colvarResubscriptionTime.DataType = DbType.DateTime;
				colvarResubscriptionTime.MaxLength = 0;
				colvarResubscriptionTime.AutoIncrement = false;
				colvarResubscriptionTime.IsNullable = true;
				colvarResubscriptionTime.IsPrimaryKey = false;
				colvarResubscriptionTime.IsForeignKey = false;
				colvarResubscriptionTime.IsReadOnly = false;
				colvarResubscriptionTime.DefaultSetting = @"";
				colvarResubscriptionTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarResubscriptionTime);
				
				TableSchema.TableColumn colvarReliable = new TableSchema.TableColumn(schema);
				colvarReliable.ColumnName = "reliable";
				colvarReliable.DataType = DbType.Boolean;
				colvarReliable.MaxLength = 0;
				colvarReliable.AutoIncrement = false;
				colvarReliable.IsNullable = true;
				colvarReliable.IsPrimaryKey = false;
				colvarReliable.IsForeignKey = false;
				colvarReliable.IsReadOnly = false;
				colvarReliable.DefaultSetting = @"";
				colvarReliable.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReliable);
				
				TableSchema.TableColumn colvarCategoryId = new TableSchema.TableColumn(schema);
				colvarCategoryId.ColumnName = "category_id";
				colvarCategoryId.DataType = DbType.Int32;
				colvarCategoryId.MaxLength = 0;
				colvarCategoryId.AutoIncrement = false;
				colvarCategoryId.IsNullable = false;
				colvarCategoryId.IsPrimaryKey = false;
				colvarCategoryId.IsForeignKey = false;
				colvarCategoryId.IsReadOnly = false;
				colvarCategoryId.DefaultSetting = @"";
				colvarCategoryId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCategoryId);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("subscription",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
          [Obsolete]
		[XmlAttribute("CityId")]
		[Bindable(true)]
		public int? CityId 
		{
			get { return GetColumnValue<int?>(Columns.CityId); }
			set { SetColumnValue(Columns.CityId, value); }
		}
		  
		[XmlAttribute("Email")]
		[Bindable(true)]
		public string Email 
		{
			get { return GetColumnValue<string>(Columns.Email); }
			set { SetColumnValue(Columns.Email, value); }
		}
		  
		[XmlAttribute("Status")]
		[Bindable(true)]
		public int? Status 
		{
			get { return GetColumnValue<int?>(Columns.Status); }
			set { SetColumnValue(Columns.Status, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime? CreateTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		  
		[XmlAttribute("UnsubscriptionTime")]
		[Bindable(true)]
		public DateTime? UnsubscriptionTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.UnsubscriptionTime); }
			set { SetColumnValue(Columns.UnsubscriptionTime, value); }
		}
		  
		[XmlAttribute("ResubscriptionTime")]
		[Bindable(true)]
		public DateTime? ResubscriptionTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.ResubscriptionTime); }
			set { SetColumnValue(Columns.ResubscriptionTime, value); }
		}
		  
		[XmlAttribute("Reliable")]
		[Bindable(true)]
		public bool? Reliable 
		{
			get { return GetColumnValue<bool?>(Columns.Reliable); }
			set { SetColumnValue(Columns.Reliable, value); }
		}
		  
		[XmlAttribute("CategoryId")]
		[Bindable(true)]
		public int CategoryId 
		{
			get { return GetColumnValue<int>(Columns.CategoryId); }
			set { SetColumnValue(Columns.CategoryId, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (1)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn CityIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn EmailColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn StatusColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn UnsubscriptionTimeColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn ResubscriptionTimeColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn ReliableColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn CategoryIdColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string CityId = @"city_id";
			 public static string Email = @"email";
			 public static string Status = @"status";
			 public static string CreateTime = @"create_time";
			 public static string UnsubscriptionTime = @"unsubscription_time";
			 public static string ResubscriptionTime = @"resubscription_time";
			 public static string Reliable = @"reliable";
			 public static string CategoryId = @"category_id";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

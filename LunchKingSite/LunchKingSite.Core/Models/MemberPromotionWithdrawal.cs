using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the MemberPromotionWithdrawal class.
	/// </summary>
    [Serializable]
	public partial class MemberPromotionWithdrawalCollection : RepositoryList<MemberPromotionWithdrawal, MemberPromotionWithdrawalCollection>
	{	   
		public MemberPromotionWithdrawalCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>MemberPromotionWithdrawalCollection</returns>
		public MemberPromotionWithdrawalCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                MemberPromotionWithdrawal o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the member_promotion_withdrawal table.
	/// </summary>
	[Serializable]
	public partial class MemberPromotionWithdrawal : RepositoryRecord<MemberPromotionWithdrawal>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public MemberPromotionWithdrawal()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public MemberPromotionWithdrawal(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("member_promotion_withdrawal", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarDepositId = new TableSchema.TableColumn(schema);
				colvarDepositId.ColumnName = "deposit_ID";
				colvarDepositId.DataType = DbType.Int32;
				colvarDepositId.MaxLength = 0;
				colvarDepositId.AutoIncrement = false;
				colvarDepositId.IsNullable = false;
				colvarDepositId.IsPrimaryKey = false;
				colvarDepositId.IsForeignKey = true;
				colvarDepositId.IsReadOnly = false;
				colvarDepositId.DefaultSetting = @"";
				
					colvarDepositId.ForeignKeyTableName = "member_promotion_deposit";
				schema.Columns.Add(colvarDepositId);
				
				TableSchema.TableColumn colvarPromotionValue = new TableSchema.TableColumn(schema);
				colvarPromotionValue.ColumnName = "promotion_value";
				colvarPromotionValue.DataType = DbType.Double;
				colvarPromotionValue.MaxLength = 0;
				colvarPromotionValue.AutoIncrement = false;
				colvarPromotionValue.IsNullable = false;
				colvarPromotionValue.IsPrimaryKey = false;
				colvarPromotionValue.IsForeignKey = false;
				colvarPromotionValue.IsReadOnly = false;
				colvarPromotionValue.DefaultSetting = @"";
				colvarPromotionValue.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPromotionValue);
				
				TableSchema.TableColumn colvarRedeemOrderGuid = new TableSchema.TableColumn(schema);
				colvarRedeemOrderGuid.ColumnName = "redeem_order_GUID";
				colvarRedeemOrderGuid.DataType = DbType.Guid;
				colvarRedeemOrderGuid.MaxLength = 0;
				colvarRedeemOrderGuid.AutoIncrement = false;
				colvarRedeemOrderGuid.IsNullable = true;
				colvarRedeemOrderGuid.IsPrimaryKey = false;
				colvarRedeemOrderGuid.IsForeignKey = true;
				colvarRedeemOrderGuid.IsReadOnly = false;
				colvarRedeemOrderGuid.DefaultSetting = @"";
				
					colvarRedeemOrderGuid.ForeignKeyTableName = "redeem_order";
				schema.Columns.Add(colvarRedeemOrderGuid);
				
				TableSchema.TableColumn colvarOrderGuid = new TableSchema.TableColumn(schema);
				colvarOrderGuid.ColumnName = "order_GUID";
				colvarOrderGuid.DataType = DbType.Guid;
				colvarOrderGuid.MaxLength = 0;
				colvarOrderGuid.AutoIncrement = false;
				colvarOrderGuid.IsNullable = true;
				colvarOrderGuid.IsPrimaryKey = false;
				colvarOrderGuid.IsForeignKey = false;
				colvarOrderGuid.IsReadOnly = false;
				colvarOrderGuid.DefaultSetting = @"";
				colvarOrderGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderGuid);
				
				TableSchema.TableColumn colvarType = new TableSchema.TableColumn(schema);
				colvarType.ColumnName = "type";
				colvarType.DataType = DbType.Int32;
				colvarType.MaxLength = 0;
				colvarType.AutoIncrement = false;
				colvarType.IsNullable = false;
				colvarType.IsPrimaryKey = false;
				colvarType.IsForeignKey = false;
				colvarType.IsReadOnly = false;
				colvarType.DefaultSetting = @"";
				colvarType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarType);
				
				TableSchema.TableColumn colvarAction = new TableSchema.TableColumn(schema);
				colvarAction.ColumnName = "action";
				colvarAction.DataType = DbType.String;
				colvarAction.MaxLength = 200;
				colvarAction.AutoIncrement = false;
				colvarAction.IsNullable = true;
				colvarAction.IsPrimaryKey = false;
				colvarAction.IsForeignKey = false;
				colvarAction.IsReadOnly = false;
				colvarAction.DefaultSetting = @"";
				colvarAction.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAction);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
				colvarCreateId.ColumnName = "create_id";
				colvarCreateId.DataType = DbType.String;
				colvarCreateId.MaxLength = 256;
				colvarCreateId.AutoIncrement = false;
				colvarCreateId.IsNullable = true;
				colvarCreateId.IsPrimaryKey = false;
				colvarCreateId.IsForeignKey = false;
				colvarCreateId.IsReadOnly = false;
				colvarCreateId.DefaultSetting = @"";
				colvarCreateId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateId);
				
				TableSchema.TableColumn colvarOrderClassification = new TableSchema.TableColumn(schema);
				colvarOrderClassification.ColumnName = "order_classification";
				colvarOrderClassification.DataType = DbType.Int32;
				colvarOrderClassification.MaxLength = 0;
				colvarOrderClassification.AutoIncrement = false;
				colvarOrderClassification.IsNullable = true;
				colvarOrderClassification.IsPrimaryKey = false;
				colvarOrderClassification.IsForeignKey = false;
				colvarOrderClassification.IsReadOnly = false;
				colvarOrderClassification.DefaultSetting = @"";
				colvarOrderClassification.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderClassification);
				
				TableSchema.TableColumn colvarReceivableId = new TableSchema.TableColumn(schema);
				colvarReceivableId.ColumnName = "receivable_id";
				colvarReceivableId.DataType = DbType.Int32;
				colvarReceivableId.MaxLength = 0;
				colvarReceivableId.AutoIncrement = false;
				colvarReceivableId.IsNullable = true;
				colvarReceivableId.IsPrimaryKey = false;
				colvarReceivableId.IsForeignKey = false;
				colvarReceivableId.IsReadOnly = false;
				colvarReceivableId.DefaultSetting = @"";
				colvarReceivableId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReceivableId);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("member_promotion_withdrawal",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("DepositId")]
		[Bindable(true)]
		public int DepositId 
		{
			get { return GetColumnValue<int>(Columns.DepositId); }
			set { SetColumnValue(Columns.DepositId, value); }
		}
		  
		[XmlAttribute("PromotionValue")]
		[Bindable(true)]
		public double PromotionValue 
		{
			get { return GetColumnValue<double>(Columns.PromotionValue); }
			set { SetColumnValue(Columns.PromotionValue, value); }
		}
		  
		[XmlAttribute("RedeemOrderGuid")]
		[Bindable(true)]
		public Guid? RedeemOrderGuid 
		{
			get { return GetColumnValue<Guid?>(Columns.RedeemOrderGuid); }
			set { SetColumnValue(Columns.RedeemOrderGuid, value); }
		}
		  
		[XmlAttribute("OrderGuid")]
		[Bindable(true)]
		public Guid? OrderGuid 
		{
			get { return GetColumnValue<Guid?>(Columns.OrderGuid); }
			set { SetColumnValue(Columns.OrderGuid, value); }
		}
		  
		[XmlAttribute("Type")]
		[Bindable(true)]
		public int Type 
		{
			get { return GetColumnValue<int>(Columns.Type); }
			set { SetColumnValue(Columns.Type, value); }
		}
		  
		[XmlAttribute("Action")]
		[Bindable(true)]
		public string Action 
		{
			get { return GetColumnValue<string>(Columns.Action); }
			set { SetColumnValue(Columns.Action, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		  
		[XmlAttribute("CreateId")]
		[Bindable(true)]
		public string CreateId 
		{
			get { return GetColumnValue<string>(Columns.CreateId); }
			set { SetColumnValue(Columns.CreateId, value); }
		}
		  
		[XmlAttribute("OrderClassification")]
		[Bindable(true)]
		public int? OrderClassification 
		{
			get { return GetColumnValue<int?>(Columns.OrderClassification); }
			set { SetColumnValue(Columns.OrderClassification, value); }
		}
		  
		[XmlAttribute("ReceivableId")]
		[Bindable(true)]
		public int? ReceivableId 
		{
			get { return GetColumnValue<int?>(Columns.ReceivableId); }
			set { SetColumnValue(Columns.ReceivableId, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (2)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn DepositIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn PromotionValueColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn RedeemOrderGuidColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn OrderGuidColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn TypeColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn ActionColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn OrderClassificationColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn ReceivableIdColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string DepositId = @"deposit_ID";
			 public static string PromotionValue = @"promotion_value";
			 public static string RedeemOrderGuid = @"redeem_order_GUID";
			 public static string OrderGuid = @"order_GUID";
			 public static string Type = @"type";
			 public static string Action = @"action";
			 public static string CreateTime = @"create_time";
			 public static string CreateId = @"create_id";
			 public static string OrderClassification = @"order_classification";
			 public static string ReceivableId = @"receivable_id";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

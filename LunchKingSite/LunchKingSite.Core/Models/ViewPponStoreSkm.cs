using System;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using SubSonic;

namespace LunchKingSite.DataOrm
{
	[Serializable]
	public partial class ViewPponStoreSkmCollection : ReadOnlyList<ViewPponStoreSkm, ViewPponStoreSkmCollection>
	{
			public ViewPponStoreSkmCollection() {}

	}

	[Serializable]
	public partial class ViewPponStoreSkm : ReadOnlyRecord<ViewPponStoreSkm>, IReadOnlyRecord
	{
		#region .ctors and Default Settings
		public ViewPponStoreSkm()
		{
			SetSQLProps();
			InitSetDefaults();
			MarkNew();
		}

		private void InitSetDefaults() { SetDefaults(); }

		public ViewPponStoreSkm(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}

		protected static void SetSQLProps() { GetTableSchema(); }

		public ViewPponStoreSkm(object keyID)
		{
				SetSQLProps();
				LoadByKey(keyID);
		}

		public ViewPponStoreSkm(string columnName, object columnValue)
		{
				SetSQLProps();
				LoadByParam(columnName,columnValue);
		}
		#endregion

		#region Schema and Query Accessor
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}

		private static void GetTableSchema()
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("view_ppon_store_skm", TableType.View, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				TableSchema.TableColumn colvarBusinessHourGuid = new TableSchema.TableColumn(schema);
				colvarBusinessHourGuid.ColumnName = "business_hour_guid";
				colvarBusinessHourGuid.DataType = DbType.Guid;
				colvarBusinessHourGuid.MaxLength = 0;
				colvarBusinessHourGuid.AutoIncrement = false;
				colvarBusinessHourGuid.IsNullable = false;
				colvarBusinessHourGuid.IsPrimaryKey = false;
				colvarBusinessHourGuid.IsForeignKey = false;
				colvarBusinessHourGuid.IsReadOnly = false;
				colvarBusinessHourGuid.DefaultSetting = @"";
				colvarBusinessHourGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBusinessHourGuid);

				TableSchema.TableColumn colvarStoreGuid = new TableSchema.TableColumn(schema);
				colvarStoreGuid.ColumnName = "store_guid";
				colvarStoreGuid.DataType = DbType.Guid;
				colvarStoreGuid.MaxLength = 0;
				colvarStoreGuid.AutoIncrement = false;
				colvarStoreGuid.IsNullable = false;
				colvarStoreGuid.IsPrimaryKey = false;
				colvarStoreGuid.IsForeignKey = false;
				colvarStoreGuid.IsReadOnly = false;
				colvarStoreGuid.DefaultSetting = @"";
				colvarStoreGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStoreGuid);

				TableSchema.TableColumn colvarStoreName = new TableSchema.TableColumn(schema);
				colvarStoreName.ColumnName = "store_name";
				colvarStoreName.DataType = DbType.String;
				colvarStoreName.MaxLength = 100;
				colvarStoreName.AutoIncrement = false;
				colvarStoreName.IsNullable = false;
				colvarStoreName.IsPrimaryKey = false;
				colvarStoreName.IsForeignKey = false;
				colvarStoreName.IsReadOnly = false;
				colvarStoreName.DefaultSetting = @"";
				colvarStoreName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStoreName);

				TableSchema.TableColumn colvarSellerGuid = new TableSchema.TableColumn(schema);
				colvarSellerGuid.ColumnName = "seller_guid";
				colvarSellerGuid.DataType = DbType.Guid;
				colvarSellerGuid.MaxLength = 0;
				colvarSellerGuid.AutoIncrement = false;
				colvarSellerGuid.IsNullable = false;
				colvarSellerGuid.IsPrimaryKey = false;
				colvarSellerGuid.IsForeignKey = false;
				colvarSellerGuid.IsReadOnly = false;
				colvarSellerGuid.DefaultSetting = @"";
				colvarSellerGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSellerGuid);

				TableSchema.TableColumn colvarSellerName = new TableSchema.TableColumn(schema);
				colvarSellerName.ColumnName = "seller_name";
				colvarSellerName.DataType = DbType.String;
				colvarSellerName.MaxLength = 100;
				colvarSellerName.AutoIncrement = false;
				colvarSellerName.IsNullable = false;
				colvarSellerName.IsPrimaryKey = false;
				colvarSellerName.IsForeignKey = false;
				colvarSellerName.IsReadOnly = false;
				colvarSellerName.DefaultSetting = @"";
				colvarSellerName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSellerName);

				TableSchema.TableColumn colvarShopCode = new TableSchema.TableColumn(schema);
				colvarShopCode.ColumnName = "shop_code";
				colvarShopCode.DataType = DbType.AnsiString;
				colvarShopCode.MaxLength = 4;
				colvarShopCode.AutoIncrement = false;
				colvarShopCode.IsNullable = true;
				colvarShopCode.IsPrimaryKey = false;
				colvarShopCode.IsForeignKey = false;
				colvarShopCode.IsReadOnly = false;
				colvarShopCode.DefaultSetting = @"";
				colvarShopCode.ForeignKeyTableName = "";
				schema.Columns.Add(colvarShopCode);

				TableSchema.TableColumn colvarShopName = new TableSchema.TableColumn(schema);
				colvarShopName.ColumnName = "shop_name";
				colvarShopName.DataType = DbType.String;
				colvarShopName.MaxLength = 50;
				colvarShopName.AutoIncrement = false;
				colvarShopName.IsNullable = true;
				colvarShopName.IsPrimaryKey = false;
				colvarShopName.IsForeignKey = false;
				colvarShopName.IsReadOnly = false;
				colvarShopName.DefaultSetting = @"";
				colvarShopName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarShopName);

				TableSchema.TableColumn colvarBrandCounterCode = new TableSchema.TableColumn(schema);
				colvarBrandCounterCode.ColumnName = "brand_counter_code";
				colvarBrandCounterCode.DataType = DbType.AnsiString;
				colvarBrandCounterCode.MaxLength = 10;
				colvarBrandCounterCode.AutoIncrement = false;
				colvarBrandCounterCode.IsNullable = true;
				colvarBrandCounterCode.IsPrimaryKey = false;
				colvarBrandCounterCode.IsForeignKey = false;
				colvarBrandCounterCode.IsReadOnly = false;
				colvarBrandCounterCode.DefaultSetting = @"";
				colvarBrandCounterCode.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBrandCounterCode);

				TableSchema.TableColumn colvarBrandCounterName = new TableSchema.TableColumn(schema);
				colvarBrandCounterName.ColumnName = "brand_counter_name";
				colvarBrandCounterName.DataType = DbType.String;
				colvarBrandCounterName.MaxLength = 50;
				colvarBrandCounterName.AutoIncrement = false;
				colvarBrandCounterName.IsNullable = true;
				colvarBrandCounterName.IsPrimaryKey = false;
				colvarBrandCounterName.IsForeignKey = false;
				colvarBrandCounterName.IsReadOnly = false;
				colvarBrandCounterName.DefaultSetting = @"";
				colvarBrandCounterName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBrandCounterName);

				TableSchema.TableColumn colvarIsShoppe = new TableSchema.TableColumn(schema);
				colvarIsShoppe.ColumnName = "is_shoppe";
				colvarIsShoppe.DataType = DbType.Boolean;
				colvarIsShoppe.MaxLength = 0;
				colvarIsShoppe.AutoIncrement = false;
				colvarIsShoppe.IsNullable = true;
				colvarIsShoppe.IsPrimaryKey = false;
				colvarIsShoppe.IsForeignKey = false;
				colvarIsShoppe.IsReadOnly = false;
				colvarIsShoppe.DefaultSetting = @"";
				colvarIsShoppe.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsShoppe);

				TableSchema.TableColumn colvarFloor = new TableSchema.TableColumn(schema);
				colvarFloor.ColumnName = "floor";
				colvarFloor.DataType = DbType.String;
				colvarFloor.MaxLength = 10;
				colvarFloor.AutoIncrement = false;
				colvarFloor.IsNullable = true;
				colvarFloor.IsPrimaryKey = false;
				colvarFloor.IsForeignKey = false;
				colvarFloor.IsReadOnly = false;
				colvarFloor.DefaultSetting = @"";
				colvarFloor.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFloor);

				TableSchema.TableColumn colvarVbsRight = new TableSchema.TableColumn(schema);
				colvarVbsRight.ColumnName = "vbs_right";
				colvarVbsRight.DataType = DbType.Int32;
				colvarVbsRight.MaxLength = 0;
				colvarVbsRight.AutoIncrement = false;
				colvarVbsRight.IsNullable = false;
				colvarVbsRight.IsPrimaryKey = false;
				colvarVbsRight.IsForeignKey = false;
				colvarVbsRight.IsReadOnly = false;
				colvarVbsRight.DefaultSetting = @"";
				colvarVbsRight.ForeignKeyTableName = "";
				schema.Columns.Add(colvarVbsRight);

				TableSchema.TableColumn colvarSkmStoreName = new TableSchema.TableColumn(schema);
				colvarSkmStoreName.ColumnName = "skm_store_name";
				colvarSkmStoreName.DataType = DbType.String;
				colvarSkmStoreName.MaxLength = 200;
				colvarSkmStoreName.AutoIncrement = false;
				colvarSkmStoreName.IsNullable = true;
				colvarSkmStoreName.IsPrimaryKey = false;
				colvarSkmStoreName.IsForeignKey = false;
				colvarSkmStoreName.IsReadOnly = false;
				colvarSkmStoreName.DefaultSetting = @"";
				colvarSkmStoreName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSkmStoreName);

				TableSchema.TableColumn colvarSkmStoreGuid = new TableSchema.TableColumn(schema);
				colvarSkmStoreGuid.ColumnName = "skm_store_guid";
				colvarSkmStoreGuid.DataType = DbType.Guid;
				colvarSkmStoreGuid.MaxLength = 0;
				colvarSkmStoreGuid.AutoIncrement = false;
				colvarSkmStoreGuid.IsNullable = true;
				colvarSkmStoreGuid.IsPrimaryKey = false;
				colvarSkmStoreGuid.IsForeignKey = false;
				colvarSkmStoreGuid.IsReadOnly = false;
				colvarSkmStoreGuid.DefaultSetting = @"";
				colvarSkmStoreGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSkmStoreGuid);

				TableSchema.TableColumn colvarBrandGuid = new TableSchema.TableColumn(schema);
				colvarBrandGuid.ColumnName = "brand_guid";
				colvarBrandGuid.DataType = DbType.Guid;
				colvarBrandGuid.MaxLength = 0;
				colvarBrandGuid.AutoIncrement = false;
				colvarBrandGuid.IsNullable = true;
				colvarBrandGuid.IsPrimaryKey = false;
				colvarBrandGuid.IsForeignKey = false;
				colvarBrandGuid.IsReadOnly = false;
				colvarBrandGuid.DefaultSetting = @"";
				colvarBrandGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBrandGuid);

				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("view_ppon_store_skm",schema);
			}
		}
		#endregion

		#region Props

		[XmlAttribute("BusinessHourGuid")]
		[Bindable(true)]
		public Guid BusinessHourGuid
		{
			get { return GetColumnValue<Guid>(Columns.BusinessHourGuid); }
			set { SetColumnValue(Columns.BusinessHourGuid, value); }
		}

		[XmlAttribute("StoreGuid")]
		[Bindable(true)]
		public Guid StoreGuid
		{
			get { return GetColumnValue<Guid>(Columns.StoreGuid); }
			set { SetColumnValue(Columns.StoreGuid, value); }
		}

		[XmlAttribute("StoreName")]
		[Bindable(true)]
		public string StoreName
		{
			get { return GetColumnValue<string>(Columns.StoreName); }
			set { SetColumnValue(Columns.StoreName, value); }
		}

		[XmlAttribute("SellerGuid")]
		[Bindable(true)]
		public Guid SellerGuid
		{
			get { return GetColumnValue<Guid>(Columns.SellerGuid); }
			set { SetColumnValue(Columns.SellerGuid, value); }
		}

		[XmlAttribute("SellerName")]
		[Bindable(true)]
		public string SellerName
		{
			get { return GetColumnValue<string>(Columns.SellerName); }
			set { SetColumnValue(Columns.SellerName, value); }
		}

		[XmlAttribute("ShopCode")]
		[Bindable(true)]
		public string ShopCode
		{
			get { return GetColumnValue<string>(Columns.ShopCode); }
			set { SetColumnValue(Columns.ShopCode, value); }
		}

		[XmlAttribute("ShopName")]
		[Bindable(true)]
		public string ShopName
		{
			get { return GetColumnValue<string>(Columns.ShopName); }
			set { SetColumnValue(Columns.ShopName, value); }
		}

		[XmlAttribute("BrandCounterCode")]
		[Bindable(true)]
		public string BrandCounterCode
		{
			get { return GetColumnValue<string>(Columns.BrandCounterCode); }
			set { SetColumnValue(Columns.BrandCounterCode, value); }
		}

		[XmlAttribute("BrandCounterName")]
		[Bindable(true)]
		public string BrandCounterName
		{
			get { return GetColumnValue<string>(Columns.BrandCounterName); }
			set { SetColumnValue(Columns.BrandCounterName, value); }
		}

		[XmlAttribute("IsShoppe")]
		[Bindable(true)]
		public bool? IsShoppe
		{
			get { return GetColumnValue<bool?>(Columns.IsShoppe); }
			set { SetColumnValue(Columns.IsShoppe, value); }
		}

		[XmlAttribute("Floor")]
		[Bindable(true)]
		public string Floor
		{
			get { return GetColumnValue<string>(Columns.Floor); }
			set { SetColumnValue(Columns.Floor, value); }
		}

		[XmlAttribute("VbsRight")]
		[Bindable(true)]
		public int VbsRight
		{
			get { return GetColumnValue<int>(Columns.VbsRight); }
			set { SetColumnValue(Columns.VbsRight, value); }
		}

		[XmlAttribute("SkmStoreName")]
		[Bindable(true)]
		public string SkmStoreName
		{
			get { return GetColumnValue<string>(Columns.SkmStoreName); }
			set { SetColumnValue(Columns.SkmStoreName, value); }
		}

		[XmlAttribute("SkmStoreGuid")]
		[Bindable(true)]
		public Guid? SkmStoreGuid
		{
			get { return GetColumnValue<Guid?>(Columns.SkmStoreGuid); }
			set { SetColumnValue(Columns.SkmStoreGuid, value); }
		}

		[XmlAttribute("BrandGuid")]
		[Bindable(true)]
		public Guid? BrandGuid
		{
			get { return GetColumnValue<Guid?>(Columns.BrandGuid); }
			set { SetColumnValue(Columns.BrandGuid, value); }
		}

		#endregion

		#region Typed Columns

		public static TableSchema.TableColumn BusinessHourGuidColumn
		{
			get { return Schema.Columns[0]; }
		}

		public static TableSchema.TableColumn StoreGuidColumn
		{
			get { return Schema.Columns[1]; }
		}

		public static TableSchema.TableColumn StoreNameColumn
		{
			get { return Schema.Columns[2]; }
		}

		public static TableSchema.TableColumn SellerGuidColumn
		{
			get { return Schema.Columns[3]; }
		}

		public static TableSchema.TableColumn SellerNameColumn
		{
			get { return Schema.Columns[4]; }
		}

		public static TableSchema.TableColumn ShopCodeColumn
		{
			get { return Schema.Columns[5]; }
		}

		public static TableSchema.TableColumn ShopNameColumn
		{
			get { return Schema.Columns[6]; }
		}

		public static TableSchema.TableColumn BrandCounterCodeColumn
		{
			get { return Schema.Columns[7]; }
		}

		public static TableSchema.TableColumn BrandCounterNameColumn
		{
			get { return Schema.Columns[8]; }
		}

		public static TableSchema.TableColumn IsShoppeColumn
		{
			get { return Schema.Columns[9]; }
		}

		public static TableSchema.TableColumn FloorColumn
		{
			get { return Schema.Columns[10]; }
		}

		public static TableSchema.TableColumn VbsRightColumn
		{
			get { return Schema.Columns[11]; }
		}

		public static TableSchema.TableColumn SkmStoreNameColumn
		{
			get { return Schema.Columns[12]; }
		}

		public static TableSchema.TableColumn SkmStoreGuidColumn
		{
			get { return Schema.Columns[13]; }
		}

		public static TableSchema.TableColumn BrandGuidColumn
		{
			get { return Schema.Columns[14]; }
		}

		#endregion

		#region Columns Struct

		public struct Columns
		{
			public static string BusinessHourGuid = @"business_hour_guid";
			public static string StoreGuid = @"store_guid";
			public static string StoreName = @"store_name";
			public static string SellerGuid = @"seller_guid";
			public static string SellerName = @"seller_name";
			public static string ShopCode = @"shop_code";
			public static string ShopName = @"shop_name";
			public static string BrandCounterCode = @"brand_counter_code";
			public static string BrandCounterName = @"brand_counter_name";
			public static string IsShoppe = @"is_shoppe";
			public static string Floor = @"floor";
			public static string VbsRight = @"vbs_right";
			public static string SkmStoreName = @"skm_store_name";
			public static string SkmStoreGuid = @"skm_store_guid";
			public static string BrandGuid = @"brand_guid";
		}

		#endregion

		#region IAbstractRecord Members
		public new CT GetColumnValue<CT>(string columnName)
		{
			return base.GetColumnValue<CT>(columnName);
		}
		public object GetColumnValue(string columnName)
		{
			return base.GetColumnValue<object>(columnName);
		}
		#endregion

	}
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the ChangeLog class.
	/// </summary>
    [Serializable]
	public partial class ChangeLogCollection : RepositoryList<ChangeLog, ChangeLogCollection>
	{	   
		public ChangeLogCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>ChangeLogCollection</returns>
		public ChangeLogCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                ChangeLog o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the change_log table.
	/// </summary>
	[Serializable]
	public partial class ChangeLog : RepositoryRecord<ChangeLog>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public ChangeLog()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public ChangeLog(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("change_log", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int64;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarChangeObject = new TableSchema.TableColumn(schema);
				colvarChangeObject.ColumnName = "change_object";
				colvarChangeObject.DataType = DbType.String;
				colvarChangeObject.MaxLength = 100;
				colvarChangeObject.AutoIncrement = false;
				colvarChangeObject.IsNullable = false;
				colvarChangeObject.IsPrimaryKey = false;
				colvarChangeObject.IsForeignKey = false;
				colvarChangeObject.IsReadOnly = false;
				colvarChangeObject.DefaultSetting = @"";
				colvarChangeObject.ForeignKeyTableName = "";
				schema.Columns.Add(colvarChangeObject);
				
				TableSchema.TableColumn colvarKeyVal = new TableSchema.TableColumn(schema);
				colvarKeyVal.ColumnName = "key_val";
				colvarKeyVal.DataType = DbType.AnsiString;
				colvarKeyVal.MaxLength = 38;
				colvarKeyVal.AutoIncrement = false;
				colvarKeyVal.IsNullable = false;
				colvarKeyVal.IsPrimaryKey = false;
				colvarKeyVal.IsForeignKey = false;
				colvarKeyVal.IsReadOnly = false;
				colvarKeyVal.DefaultSetting = @"";
				colvarKeyVal.ForeignKeyTableName = "";
				schema.Columns.Add(colvarKeyVal);
				
				TableSchema.TableColumn colvarContent = new TableSchema.TableColumn(schema);
				colvarContent.ColumnName = "content";
				colvarContent.DataType = DbType.String;
				colvarContent.MaxLength = -1;
				colvarContent.AutoIncrement = false;
				colvarContent.IsNullable = false;
				colvarContent.IsPrimaryKey = false;
				colvarContent.IsForeignKey = false;
				colvarContent.IsReadOnly = false;
				colvarContent.DefaultSetting = @"";
				colvarContent.ForeignKeyTableName = "";
				schema.Columns.Add(colvarContent);
				
				TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
				colvarModifyTime.ColumnName = "modify_time";
				colvarModifyTime.DataType = DbType.DateTime;
				colvarModifyTime.MaxLength = 0;
				colvarModifyTime.AutoIncrement = false;
				colvarModifyTime.IsNullable = false;
				colvarModifyTime.IsPrimaryKey = false;
				colvarModifyTime.IsForeignKey = false;
				colvarModifyTime.IsReadOnly = false;
				
						colvarModifyTime.DefaultSetting = @"(getdate())";
				colvarModifyTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyTime);
				
				TableSchema.TableColumn colvarModifyUser = new TableSchema.TableColumn(schema);
				colvarModifyUser.ColumnName = "modify_user";
				colvarModifyUser.DataType = DbType.String;
				colvarModifyUser.MaxLength = 100;
				colvarModifyUser.AutoIncrement = false;
				colvarModifyUser.IsNullable = false;
				colvarModifyUser.IsPrimaryKey = false;
				colvarModifyUser.IsForeignKey = false;
				colvarModifyUser.IsReadOnly = false;
				colvarModifyUser.DefaultSetting = @"";
				colvarModifyUser.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyUser);
				
				TableSchema.TableColumn colvarEnabled = new TableSchema.TableColumn(schema);
				colvarEnabled.ColumnName = "enabled";
				colvarEnabled.DataType = DbType.Boolean;
				colvarEnabled.MaxLength = 0;
				colvarEnabled.AutoIncrement = false;
				colvarEnabled.IsNullable = false;
				colvarEnabled.IsPrimaryKey = false;
				colvarEnabled.IsForeignKey = false;
				colvarEnabled.IsReadOnly = false;
				
						colvarEnabled.DefaultSetting = @"((1))";
				colvarEnabled.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEnabled);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("change_log",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public long Id 
		{
			get { return GetColumnValue<long>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("ChangeObject")]
		[Bindable(true)]
		public string ChangeObject 
		{
			get { return GetColumnValue<string>(Columns.ChangeObject); }
			set { SetColumnValue(Columns.ChangeObject, value); }
		}
		  
		[XmlAttribute("KeyVal")]
		[Bindable(true)]
		public string KeyVal 
		{
			get { return GetColumnValue<string>(Columns.KeyVal); }
			set { SetColumnValue(Columns.KeyVal, value); }
		}
		  
		[XmlAttribute("Content")]
		[Bindable(true)]
		public string Content 
		{
			get { return GetColumnValue<string>(Columns.Content); }
			set { SetColumnValue(Columns.Content, value); }
		}
		  
		[XmlAttribute("ModifyTime")]
		[Bindable(true)]
		public DateTime ModifyTime 
		{
			get { return GetColumnValue<DateTime>(Columns.ModifyTime); }
			set { SetColumnValue(Columns.ModifyTime, value); }
		}
		  
		[XmlAttribute("ModifyUser")]
		[Bindable(true)]
		public string ModifyUser 
		{
			get { return GetColumnValue<string>(Columns.ModifyUser); }
			set { SetColumnValue(Columns.ModifyUser, value); }
		}
		  
		[XmlAttribute("Enabled")]
		[Bindable(true)]
		public bool Enabled 
		{
			get { return GetColumnValue<bool>(Columns.Enabled); }
			set { SetColumnValue(Columns.Enabled, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn ChangeObjectColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn KeyValColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn ContentColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyUserColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn EnabledColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string ChangeObject = @"change_object";
			 public static string KeyVal = @"key_val";
			 public static string Content = @"content";
			 public static string ModifyTime = @"modify_time";
			 public static string ModifyUser = @"modify_user";
			 public static string Enabled = @"enabled";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

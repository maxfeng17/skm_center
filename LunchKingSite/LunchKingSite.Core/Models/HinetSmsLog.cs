using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the HinetSmsLog class.
	/// </summary>
    [Serializable]
	public partial class HinetSmsLogCollection : RepositoryList<HinetSmsLog, HinetSmsLogCollection>
	{	   
		public HinetSmsLogCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>HinetSmsLogCollection</returns>
		public HinetSmsLogCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                HinetSmsLog o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the HinetSmsLog table.
	/// </summary>
	[Serializable]
	public partial class HinetSmsLog : RepositoryRecord<HinetSmsLog>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public HinetSmsLog()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public HinetSmsLog(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("HinetSmsLog", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarSi = new TableSchema.TableColumn(schema);
				colvarSi.ColumnName = "si";
				colvarSi.DataType = DbType.Int32;
				colvarSi.MaxLength = 0;
				colvarSi.AutoIncrement = true;
				colvarSi.IsNullable = false;
				colvarSi.IsPrimaryKey = true;
				colvarSi.IsForeignKey = false;
				colvarSi.IsReadOnly = false;
				colvarSi.DefaultSetting = @"";
				colvarSi.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSi);
				
				TableSchema.TableColumn colvarCouponId = new TableSchema.TableColumn(schema);
				colvarCouponId.ColumnName = "couponId";
				colvarCouponId.DataType = DbType.Int32;
				colvarCouponId.MaxLength = 0;
				colvarCouponId.AutoIncrement = false;
				colvarCouponId.IsNullable = false;
				colvarCouponId.IsPrimaryKey = false;
				colvarCouponId.IsForeignKey = false;
				colvarCouponId.IsReadOnly = false;
				colvarCouponId.DefaultSetting = @"";
				colvarCouponId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCouponId);
				
				TableSchema.TableColumn colvarTicketId = new TableSchema.TableColumn(schema);
				colvarTicketId.ColumnName = "ticketId";
				colvarTicketId.DataType = DbType.AnsiString;
				colvarTicketId.MaxLength = 100;
				colvarTicketId.AutoIncrement = false;
				colvarTicketId.IsNullable = false;
				colvarTicketId.IsPrimaryKey = false;
				colvarTicketId.IsForeignKey = false;
				colvarTicketId.IsReadOnly = false;
				colvarTicketId.DefaultSetting = @"";
				colvarTicketId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTicketId);
				
				TableSchema.TableColumn colvarMessage = new TableSchema.TableColumn(schema);
				colvarMessage.ColumnName = "message";
				colvarMessage.DataType = DbType.String;
				colvarMessage.MaxLength = 500;
				colvarMessage.AutoIncrement = false;
				colvarMessage.IsNullable = false;
				colvarMessage.IsPrimaryKey = false;
				colvarMessage.IsForeignKey = false;
				colvarMessage.IsReadOnly = false;
				colvarMessage.DefaultSetting = @"";
				colvarMessage.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMessage);
				
				TableSchema.TableColumn colvarDestination = new TableSchema.TableColumn(schema);
				colvarDestination.ColumnName = "destination";
				colvarDestination.DataType = DbType.AnsiString;
				colvarDestination.MaxLength = 50;
				colvarDestination.AutoIncrement = false;
				colvarDestination.IsNullable = false;
				colvarDestination.IsPrimaryKey = false;
				colvarDestination.IsForeignKey = false;
				colvarDestination.IsReadOnly = false;
				colvarDestination.DefaultSetting = @"";
				colvarDestination.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDestination);
				
				TableSchema.TableColumn colvarProcessDate = new TableSchema.TableColumn(schema);
				colvarProcessDate.ColumnName = "processDate";
				colvarProcessDate.DataType = DbType.AnsiString;
				colvarProcessDate.MaxLength = 50;
				colvarProcessDate.AutoIncrement = false;
				colvarProcessDate.IsNullable = true;
				colvarProcessDate.IsPrimaryKey = false;
				colvarProcessDate.IsForeignKey = false;
				colvarProcessDate.IsReadOnly = false;
				colvarProcessDate.DefaultSetting = @"";
				colvarProcessDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarProcessDate);
				
				TableSchema.TableColumn colvarStatusCode = new TableSchema.TableColumn(schema);
				colvarStatusCode.ColumnName = "statusCode";
				colvarStatusCode.DataType = DbType.AnsiString;
				colvarStatusCode.MaxLength = 50;
				colvarStatusCode.AutoIncrement = false;
				colvarStatusCode.IsNullable = true;
				colvarStatusCode.IsPrimaryKey = false;
				colvarStatusCode.IsForeignKey = false;
				colvarStatusCode.IsReadOnly = false;
				colvarStatusCode.DefaultSetting = @"";
				colvarStatusCode.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStatusCode);
				
				TableSchema.TableColumn colvarStatusDesc = new TableSchema.TableColumn(schema);
				colvarStatusDesc.ColumnName = "statusDesc";
				colvarStatusDesc.DataType = DbType.String;
				colvarStatusDesc.MaxLength = 100;
				colvarStatusDesc.AutoIncrement = false;
				colvarStatusDesc.IsNullable = true;
				colvarStatusDesc.IsPrimaryKey = false;
				colvarStatusDesc.IsForeignKey = false;
				colvarStatusDesc.IsReadOnly = false;
				colvarStatusDesc.DefaultSetting = @"";
				colvarStatusDesc.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStatusDesc);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "createTime";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("HinetSmsLog",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Si")]
		[Bindable(true)]
		public int Si 
		{
			get { return GetColumnValue<int>(Columns.Si); }
			set { SetColumnValue(Columns.Si, value); }
		}
		  
		[XmlAttribute("CouponId")]
		[Bindable(true)]
		public int CouponId 
		{
			get { return GetColumnValue<int>(Columns.CouponId); }
			set { SetColumnValue(Columns.CouponId, value); }
		}
		  
		[XmlAttribute("TicketId")]
		[Bindable(true)]
		public string TicketId 
		{
			get { return GetColumnValue<string>(Columns.TicketId); }
			set { SetColumnValue(Columns.TicketId, value); }
		}
		  
		[XmlAttribute("Message")]
		[Bindable(true)]
		public string Message 
		{
			get { return GetColumnValue<string>(Columns.Message); }
			set { SetColumnValue(Columns.Message, value); }
		}
		  
		[XmlAttribute("Destination")]
		[Bindable(true)]
		public string Destination 
		{
			get { return GetColumnValue<string>(Columns.Destination); }
			set { SetColumnValue(Columns.Destination, value); }
		}
		  
		[XmlAttribute("ProcessDate")]
		[Bindable(true)]
		public string ProcessDate 
		{
			get { return GetColumnValue<string>(Columns.ProcessDate); }
			set { SetColumnValue(Columns.ProcessDate, value); }
		}
		  
		[XmlAttribute("StatusCode")]
		[Bindable(true)]
		public string StatusCode 
		{
			get { return GetColumnValue<string>(Columns.StatusCode); }
			set { SetColumnValue(Columns.StatusCode, value); }
		}
		  
		[XmlAttribute("StatusDesc")]
		[Bindable(true)]
		public string StatusDesc 
		{
			get { return GetColumnValue<string>(Columns.StatusDesc); }
			set { SetColumnValue(Columns.StatusDesc, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn SiColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn CouponIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn TicketIdColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn MessageColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn DestinationColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn ProcessDateColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn StatusCodeColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn StatusDescColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Si = @"si";
			 public static string CouponId = @"couponId";
			 public static string TicketId = @"ticketId";
			 public static string Message = @"message";
			 public static string Destination = @"destination";
			 public static string ProcessDate = @"processDate";
			 public static string StatusCode = @"statusCode";
			 public static string StatusDesc = @"statusDesc";
			 public static string CreateTime = @"createTime";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

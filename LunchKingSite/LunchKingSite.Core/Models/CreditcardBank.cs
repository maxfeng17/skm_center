using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the CreditcardBank class.
    /// </summary>
    [Serializable]
    public partial class CreditcardBankCollection : RepositoryList<CreditcardBank, CreditcardBankCollection>
    {
        public CreditcardBankCollection() { }

        /// <summary>
        /// Filters an existing collection based on the set criteria. This is an in-memory filter
        /// Thanks to developingchris for this!
        /// </summary>
        /// <returns>CreditcardBankCollection</returns>
        public CreditcardBankCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                CreditcardBank o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }


    }
    /// <summary>
    /// This is an ActiveRecord class which wraps the creditcard_bank table.
    /// </summary>
    [Serializable]
    public partial class CreditcardBank : RepositoryRecord<CreditcardBank>, IRecordBase
    {
        #region .ctors and Default Settings

        public CreditcardBank()
        {
            SetSQLProps();
            InitSetDefaults();
            MarkNew();
        }

        private void InitSetDefaults() { SetDefaults(); }

        public CreditcardBank(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
                ForceDefaults();
            MarkNew();
        }

        protected static void SetSQLProps() { GetTableSchema(); }

        #endregion

        #region Schema and Query Accessor	
        public static Query CreateQuery() { return new Query(Schema); }
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                    SetSQLProps();
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("creditcard_bank", TableType.Table, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = true;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = true;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                colvarId.DefaultSetting = @"";
                colvarId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarId);

                TableSchema.TableColumn colvarBankName = new TableSchema.TableColumn(schema);
                colvarBankName.ColumnName = "bank_name";
                colvarBankName.DataType = DbType.String;
                colvarBankName.MaxLength = 255;
                colvarBankName.AutoIncrement = false;
                colvarBankName.IsNullable = false;
                colvarBankName.IsPrimaryKey = false;
                colvarBankName.IsForeignKey = false;
                colvarBankName.IsReadOnly = false;
                colvarBankName.DefaultSetting = @"";
                colvarBankName.ForeignKeyTableName = "";
                schema.Columns.Add(colvarBankName);

                TableSchema.TableColumn colvarIsInstallment = new TableSchema.TableColumn(schema);
                colvarIsInstallment.ColumnName = "is_installment";
                colvarIsInstallment.DataType = DbType.Boolean;
                colvarIsInstallment.MaxLength = 0;
                colvarIsInstallment.AutoIncrement = false;
                colvarIsInstallment.IsNullable = true;
                colvarIsInstallment.IsPrimaryKey = false;
                colvarIsInstallment.IsForeignKey = false;
                colvarIsInstallment.IsReadOnly = false;

                colvarIsInstallment.DefaultSetting = @"((0))";
                colvarIsInstallment.ForeignKeyTableName = "";
                schema.Columns.Add(colvarIsInstallment);

                TableSchema.TableColumn colvarUserId = new TableSchema.TableColumn(schema);
                colvarUserId.ColumnName = "user_id";
                colvarUserId.DataType = DbType.Int32;
                colvarUserId.MaxLength = 0;
                colvarUserId.AutoIncrement = false;
                colvarUserId.IsNullable = false;
                colvarUserId.IsPrimaryKey = false;
                colvarUserId.IsForeignKey = false;
                colvarUserId.IsReadOnly = false;
                colvarUserId.DefaultSetting = @"";
                colvarUserId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarUserId);

                TableSchema.TableColumn colvarLastUpdateDate = new TableSchema.TableColumn(schema);
                colvarLastUpdateDate.ColumnName = "last_update_date";
                colvarLastUpdateDate.DataType = DbType.DateTime;
                colvarLastUpdateDate.MaxLength = 0;
                colvarLastUpdateDate.AutoIncrement = false;
                colvarLastUpdateDate.IsNullable = true;
                colvarLastUpdateDate.IsPrimaryKey = false;
                colvarLastUpdateDate.IsForeignKey = false;
                colvarLastUpdateDate.IsReadOnly = false;

                colvarLastUpdateDate.DefaultSetting = @"(getdate())";
                colvarLastUpdateDate.ForeignKeyTableName = "";
                schema.Columns.Add(colvarLastUpdateDate);

                TableSchema.TableColumn colvarLastUpdateLog = new TableSchema.TableColumn(schema);
                colvarLastUpdateLog.ColumnName = "last_update_log";
                colvarLastUpdateLog.DataType = DbType.String;
                colvarLastUpdateLog.MaxLength = 255;
                colvarLastUpdateLog.AutoIncrement = false;
                colvarLastUpdateLog.IsNullable = true;
                colvarLastUpdateLog.IsPrimaryKey = false;
                colvarLastUpdateLog.IsForeignKey = false;
                colvarLastUpdateLog.IsReadOnly = false;
                colvarLastUpdateLog.DefaultSetting = @"";
                colvarLastUpdateLog.ForeignKeyTableName = "";
                schema.Columns.Add(colvarLastUpdateLog);

                TableSchema.TableColumn colvarServicePhone = new TableSchema.TableColumn(schema);
                colvarServicePhone.ColumnName = "service_phone";
                colvarServicePhone.DataType = DbType.AnsiString;
                colvarServicePhone.MaxLength = 100;
                colvarServicePhone.AutoIncrement = false;
                colvarServicePhone.IsNullable = true;
                colvarServicePhone.IsPrimaryKey = false;
                colvarServicePhone.IsForeignKey = false;
                colvarServicePhone.IsReadOnly = false;
                colvarServicePhone.DefaultSetting = @"";
                colvarServicePhone.ForeignKeyTableName = "";
                schema.Columns.Add(colvarServicePhone);

                TableSchema.TableColumn colvarBankId = new TableSchema.TableColumn(schema);
                colvarBankId.ColumnName = "bank_id";
                colvarBankId.DataType = DbType.AnsiString;
                colvarBankId.MaxLength = 5;
                colvarBankId.AutoIncrement = false;
                colvarBankId.IsNullable = true;
                colvarBankId.IsPrimaryKey = false;
                colvarBankId.IsForeignKey = false;
                colvarBankId.IsReadOnly = false;
                colvarBankId.DefaultSetting = @"";
                colvarBankId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarBankId);

                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("creditcard_bank", schema);
            }
        }
        #endregion

        #region Props

        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id
        {
            get { return GetColumnValue<int>(Columns.Id); }
            set { SetColumnValue(Columns.Id, value); }
        }

        [XmlAttribute("BankName")]
        [Bindable(true)]
        public string BankName
        {
            get { return GetColumnValue<string>(Columns.BankName); }
            set { SetColumnValue(Columns.BankName, value); }
        }

        [XmlAttribute("IsInstallment")]
        [Bindable(true)]
        public bool? IsInstallment
        {
            get { return GetColumnValue<bool?>(Columns.IsInstallment); }
            set { SetColumnValue(Columns.IsInstallment, value); }
        }

        [XmlAttribute("UserId")]
        [Bindable(true)]
        public int UserId
        {
            get { return GetColumnValue<int>(Columns.UserId); }
            set { SetColumnValue(Columns.UserId, value); }
        }

        [XmlAttribute("LastUpdateDate")]
        [Bindable(true)]
        public DateTime? LastUpdateDate
        {
            get { return GetColumnValue<DateTime?>(Columns.LastUpdateDate); }
            set { SetColumnValue(Columns.LastUpdateDate, value); }
        }

        [XmlAttribute("LastUpdateLog")]
        [Bindable(true)]
        public string LastUpdateLog
        {
            get { return GetColumnValue<string>(Columns.LastUpdateLog); }
            set { SetColumnValue(Columns.LastUpdateLog, value); }
        }

        [XmlAttribute("ServicePhone")]
        [Bindable(true)]
        public string ServicePhone
        {
            get { return GetColumnValue<string>(Columns.ServicePhone); }
            set { SetColumnValue(Columns.ServicePhone, value); }
        }

        [XmlAttribute("BankId")]
        [Bindable(true)]
        public string BankId
        {
            get { return GetColumnValue<string>(Columns.BankId); }
            set { SetColumnValue(Columns.BankId, value); }
        }

        #endregion




        //no foreign key tables defined (0)



        //no ManyToMany tables defined (0)





        #region Typed Columns


        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }



        public static TableSchema.TableColumn BankNameColumn
        {
            get { return Schema.Columns[1]; }
        }



        public static TableSchema.TableColumn IsInstallmentColumn
        {
            get { return Schema.Columns[2]; }
        }



        public static TableSchema.TableColumn UserIdColumn
        {
            get { return Schema.Columns[3]; }
        }



        public static TableSchema.TableColumn LastUpdateDateColumn
        {
            get { return Schema.Columns[4]; }
        }



        public static TableSchema.TableColumn LastUpdateLogColumn
        {
            get { return Schema.Columns[5]; }
        }



        public static TableSchema.TableColumn ServicePhoneColumn
        {
            get { return Schema.Columns[6]; }
        }



        public static TableSchema.TableColumn BankIdColumn
        {
            get { return Schema.Columns[7]; }
        }



        #endregion
        #region Columns Struct
        public struct Columns
        {
            public static string Id = @"id";
            public static string BankName = @"bank_name";
            public static string IsInstallment = @"is_installment";
            public static string UserId = @"user_id";
            public static string LastUpdateDate = @"last_update_date";
            public static string LastUpdateLog = @"last_update_log";
            public static string ServicePhone = @"service_phone";
            public static string BankId = @"bank_id";

        }
        #endregion

        #region Update PK Collections

        #endregion

        #region Deep Save

        #endregion
    }
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the HiDealCoupon class.
	/// </summary>
    [Serializable]
	public partial class HiDealCouponCollection : RepositoryList<HiDealCoupon, HiDealCouponCollection>
	{	   
		public HiDealCouponCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>HiDealCouponCollection</returns>
		public HiDealCouponCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                HiDealCoupon o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the hi_deal_coupon table.
	/// </summary>
	[Serializable]
	public partial class HiDealCoupon : RepositoryRecord<HiDealCoupon>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public HiDealCoupon()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public HiDealCoupon(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("hi_deal_coupon", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int64;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarProductId = new TableSchema.TableColumn(schema);
				colvarProductId.ColumnName = "product_id";
				colvarProductId.DataType = DbType.Int32;
				colvarProductId.MaxLength = 0;
				colvarProductId.AutoIncrement = false;
				colvarProductId.IsNullable = false;
				colvarProductId.IsPrimaryKey = false;
				colvarProductId.IsForeignKey = false;
				colvarProductId.IsReadOnly = false;
				colvarProductId.DefaultSetting = @"";
				colvarProductId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarProductId);
				
				TableSchema.TableColumn colvarDealId = new TableSchema.TableColumn(schema);
				colvarDealId.ColumnName = "deal_id";
				colvarDealId.DataType = DbType.Int32;
				colvarDealId.MaxLength = 0;
				colvarDealId.AutoIncrement = false;
				colvarDealId.IsNullable = false;
				colvarDealId.IsPrimaryKey = false;
				colvarDealId.IsForeignKey = false;
				colvarDealId.IsReadOnly = false;
				colvarDealId.DefaultSetting = @"";
				colvarDealId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDealId);
				
				TableSchema.TableColumn colvarStoreGuid = new TableSchema.TableColumn(schema);
				colvarStoreGuid.ColumnName = "store_guid";
				colvarStoreGuid.DataType = DbType.Guid;
				colvarStoreGuid.MaxLength = 0;
				colvarStoreGuid.AutoIncrement = false;
				colvarStoreGuid.IsNullable = true;
				colvarStoreGuid.IsPrimaryKey = false;
				colvarStoreGuid.IsForeignKey = false;
				colvarStoreGuid.IsReadOnly = false;
				colvarStoreGuid.DefaultSetting = @"";
				colvarStoreGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStoreGuid);
				
				TableSchema.TableColumn colvarOrderPk = new TableSchema.TableColumn(schema);
				colvarOrderPk.ColumnName = "order_pk";
				colvarOrderPk.DataType = DbType.Int32;
				colvarOrderPk.MaxLength = 0;
				colvarOrderPk.AutoIncrement = false;
				colvarOrderPk.IsNullable = true;
				colvarOrderPk.IsPrimaryKey = false;
				colvarOrderPk.IsForeignKey = false;
				colvarOrderPk.IsReadOnly = false;
				colvarOrderPk.DefaultSetting = @"";
				colvarOrderPk.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderPk);
				
				TableSchema.TableColumn colvarOrderDetailGuid = new TableSchema.TableColumn(schema);
				colvarOrderDetailGuid.ColumnName = "order_detail_guid";
				colvarOrderDetailGuid.DataType = DbType.Guid;
				colvarOrderDetailGuid.MaxLength = 0;
				colvarOrderDetailGuid.AutoIncrement = false;
				colvarOrderDetailGuid.IsNullable = true;
				colvarOrderDetailGuid.IsPrimaryKey = false;
				colvarOrderDetailGuid.IsForeignKey = false;
				colvarOrderDetailGuid.IsReadOnly = false;
				colvarOrderDetailGuid.DefaultSetting = @"";
				colvarOrderDetailGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderDetailGuid);
				
				TableSchema.TableColumn colvarPrefix = new TableSchema.TableColumn(schema);
				colvarPrefix.ColumnName = "prefix";
				colvarPrefix.DataType = DbType.AnsiString;
				colvarPrefix.MaxLength = 10;
				colvarPrefix.AutoIncrement = false;
				colvarPrefix.IsNullable = true;
				colvarPrefix.IsPrimaryKey = false;
				colvarPrefix.IsForeignKey = false;
				colvarPrefix.IsReadOnly = false;
				colvarPrefix.DefaultSetting = @"";
				colvarPrefix.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPrefix);
				
				TableSchema.TableColumn colvarSequence = new TableSchema.TableColumn(schema);
				colvarSequence.ColumnName = "sequence";
				colvarSequence.DataType = DbType.AnsiString;
				colvarSequence.MaxLength = 20;
				colvarSequence.AutoIncrement = false;
				colvarSequence.IsNullable = false;
				colvarSequence.IsPrimaryKey = false;
				colvarSequence.IsForeignKey = false;
				colvarSequence.IsReadOnly = false;
				colvarSequence.DefaultSetting = @"";
				colvarSequence.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSequence);
				
				TableSchema.TableColumn colvarCode = new TableSchema.TableColumn(schema);
				colvarCode.ColumnName = "code";
				colvarCode.DataType = DbType.AnsiString;
				colvarCode.MaxLength = 10;
				colvarCode.AutoIncrement = false;
				colvarCode.IsNullable = false;
				colvarCode.IsPrimaryKey = false;
				colvarCode.IsForeignKey = false;
				colvarCode.IsReadOnly = false;
				colvarCode.DefaultSetting = @"";
				colvarCode.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCode);
				
				TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
				colvarStatus.ColumnName = "status";
				colvarStatus.DataType = DbType.Int32;
				colvarStatus.MaxLength = 0;
				colvarStatus.AutoIncrement = false;
				colvarStatus.IsNullable = false;
				colvarStatus.IsPrimaryKey = false;
				colvarStatus.IsForeignKey = false;
				colvarStatus.IsReadOnly = false;
				colvarStatus.DefaultSetting = @"";
				colvarStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStatus);
				
				TableSchema.TableColumn colvarCreateDate = new TableSchema.TableColumn(schema);
				colvarCreateDate.ColumnName = "create_date";
				colvarCreateDate.DataType = DbType.DateTime;
				colvarCreateDate.MaxLength = 0;
				colvarCreateDate.AutoIncrement = false;
				colvarCreateDate.IsNullable = true;
				colvarCreateDate.IsPrimaryKey = false;
				colvarCreateDate.IsForeignKey = false;
				colvarCreateDate.IsReadOnly = false;
				colvarCreateDate.DefaultSetting = @"";
				colvarCreateDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateDate);
				
				TableSchema.TableColumn colvarBoughtDate = new TableSchema.TableColumn(schema);
				colvarBoughtDate.ColumnName = "bought_date";
				colvarBoughtDate.DataType = DbType.DateTime;
				colvarBoughtDate.MaxLength = 0;
				colvarBoughtDate.AutoIncrement = false;
				colvarBoughtDate.IsNullable = true;
				colvarBoughtDate.IsPrimaryKey = false;
				colvarBoughtDate.IsForeignKey = false;
				colvarBoughtDate.IsReadOnly = false;
				colvarBoughtDate.DefaultSetting = @"";
				colvarBoughtDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBoughtDate);
				
				TableSchema.TableColumn colvarUsedDate = new TableSchema.TableColumn(schema);
				colvarUsedDate.ColumnName = "used_date";
				colvarUsedDate.DataType = DbType.DateTime;
				colvarUsedDate.MaxLength = 0;
				colvarUsedDate.AutoIncrement = false;
				colvarUsedDate.IsNullable = true;
				colvarUsedDate.IsPrimaryKey = false;
				colvarUsedDate.IsForeignKey = false;
				colvarUsedDate.IsReadOnly = false;
				colvarUsedDate.DefaultSetting = @"";
				colvarUsedDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUsedDate);
				
				TableSchema.TableColumn colvarRefundDate = new TableSchema.TableColumn(schema);
				colvarRefundDate.ColumnName = "refund_date";
				colvarRefundDate.DataType = DbType.DateTime;
				colvarRefundDate.MaxLength = 0;
				colvarRefundDate.AutoIncrement = false;
				colvarRefundDate.IsNullable = true;
				colvarRefundDate.IsPrimaryKey = false;
				colvarRefundDate.IsForeignKey = false;
				colvarRefundDate.IsReadOnly = false;
				colvarRefundDate.DefaultSetting = @"";
				colvarRefundDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarRefundDate);
				
				TableSchema.TableColumn colvarCost = new TableSchema.TableColumn(schema);
				colvarCost.ColumnName = "cost";
				colvarCost.DataType = DbType.Currency;
				colvarCost.MaxLength = 0;
				colvarCost.AutoIncrement = false;
				colvarCost.IsNullable = false;
				colvarCost.IsPrimaryKey = false;
				colvarCost.IsForeignKey = false;
				colvarCost.IsReadOnly = false;
				
						colvarCost.DefaultSetting = @"((0))";
				colvarCost.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCost);
				
				TableSchema.TableColumn colvarUserId = new TableSchema.TableColumn(schema);
				colvarUserId.ColumnName = "user_id";
				colvarUserId.DataType = DbType.Int32;
				colvarUserId.MaxLength = 0;
				colvarUserId.AutoIncrement = false;
				colvarUserId.IsNullable = true;
				colvarUserId.IsPrimaryKey = false;
				colvarUserId.IsForeignKey = false;
				colvarUserId.IsReadOnly = false;
				colvarUserId.DefaultSetting = @"";
				colvarUserId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUserId);
				
				TableSchema.TableColumn colvarIsReservationLock = new TableSchema.TableColumn(schema);
				colvarIsReservationLock.ColumnName = "is_reservation_lock";
				colvarIsReservationLock.DataType = DbType.Boolean;
				colvarIsReservationLock.MaxLength = 0;
				colvarIsReservationLock.AutoIncrement = false;
				colvarIsReservationLock.IsNullable = false;
				colvarIsReservationLock.IsPrimaryKey = false;
				colvarIsReservationLock.IsForeignKey = false;
				colvarIsReservationLock.IsReadOnly = false;
				
						colvarIsReservationLock.DefaultSetting = @"((0))";
				colvarIsReservationLock.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsReservationLock);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("hi_deal_coupon",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public long Id 
		{
			get { return GetColumnValue<long>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("ProductId")]
		[Bindable(true)]
		public int ProductId 
		{
			get { return GetColumnValue<int>(Columns.ProductId); }
			set { SetColumnValue(Columns.ProductId, value); }
		}
		  
		[XmlAttribute("DealId")]
		[Bindable(true)]
		public int DealId 
		{
			get { return GetColumnValue<int>(Columns.DealId); }
			set { SetColumnValue(Columns.DealId, value); }
		}
		  
		[XmlAttribute("StoreGuid")]
		[Bindable(true)]
		public Guid? StoreGuid 
		{
			get { return GetColumnValue<Guid?>(Columns.StoreGuid); }
			set { SetColumnValue(Columns.StoreGuid, value); }
		}
		  
		[XmlAttribute("OrderPk")]
		[Bindable(true)]
		public int? OrderPk 
		{
			get { return GetColumnValue<int?>(Columns.OrderPk); }
			set { SetColumnValue(Columns.OrderPk, value); }
		}
		  
		[XmlAttribute("OrderDetailGuid")]
		[Bindable(true)]
		public Guid? OrderDetailGuid 
		{
			get { return GetColumnValue<Guid?>(Columns.OrderDetailGuid); }
			set { SetColumnValue(Columns.OrderDetailGuid, value); }
		}
		  
		[XmlAttribute("Prefix")]
		[Bindable(true)]
		public string Prefix 
		{
			get { return GetColumnValue<string>(Columns.Prefix); }
			set { SetColumnValue(Columns.Prefix, value); }
		}
		  
		[XmlAttribute("Sequence")]
		[Bindable(true)]
		public string Sequence 
		{
			get { return GetColumnValue<string>(Columns.Sequence); }
			set { SetColumnValue(Columns.Sequence, value); }
		}
		  
		[XmlAttribute("Code")]
		[Bindable(true)]
		public string Code 
		{
			get { return GetColumnValue<string>(Columns.Code); }
			set { SetColumnValue(Columns.Code, value); }
		}
		  
		[XmlAttribute("Status")]
		[Bindable(true)]
		public int Status 
		{
			get { return GetColumnValue<int>(Columns.Status); }
			set { SetColumnValue(Columns.Status, value); }
		}
		  
		[XmlAttribute("CreateDate")]
		[Bindable(true)]
		public DateTime? CreateDate 
		{
			get { return GetColumnValue<DateTime?>(Columns.CreateDate); }
			set { SetColumnValue(Columns.CreateDate, value); }
		}
		  
		[XmlAttribute("BoughtDate")]
		[Bindable(true)]
		public DateTime? BoughtDate 
		{
			get { return GetColumnValue<DateTime?>(Columns.BoughtDate); }
			set { SetColumnValue(Columns.BoughtDate, value); }
		}
		  
		[XmlAttribute("UsedDate")]
		[Bindable(true)]
		public DateTime? UsedDate 
		{
			get { return GetColumnValue<DateTime?>(Columns.UsedDate); }
			set { SetColumnValue(Columns.UsedDate, value); }
		}
		  
		[XmlAttribute("RefundDate")]
		[Bindable(true)]
		public DateTime? RefundDate 
		{
			get { return GetColumnValue<DateTime?>(Columns.RefundDate); }
			set { SetColumnValue(Columns.RefundDate, value); }
		}
		  
		[XmlAttribute("Cost")]
		[Bindable(true)]
		public decimal Cost 
		{
			get { return GetColumnValue<decimal>(Columns.Cost); }
			set { SetColumnValue(Columns.Cost, value); }
		}
		  
		[XmlAttribute("UserId")]
		[Bindable(true)]
		public int? UserId 
		{
			get { return GetColumnValue<int?>(Columns.UserId); }
			set { SetColumnValue(Columns.UserId, value); }
		}
		  
		[XmlAttribute("IsReservationLock")]
		[Bindable(true)]
		public bool IsReservationLock 
		{
			get { return GetColumnValue<bool>(Columns.IsReservationLock); }
			set { SetColumnValue(Columns.IsReservationLock, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn ProductIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn DealIdColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn StoreGuidColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn OrderPkColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn OrderDetailGuidColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn PrefixColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn SequenceColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn CodeColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn StatusColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateDateColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        public static TableSchema.TableColumn BoughtDateColumn
        {
            get { return Schema.Columns[11]; }
        }
        
        
        
        public static TableSchema.TableColumn UsedDateColumn
        {
            get { return Schema.Columns[12]; }
        }
        
        
        
        public static TableSchema.TableColumn RefundDateColumn
        {
            get { return Schema.Columns[13]; }
        }
        
        
        
        public static TableSchema.TableColumn CostColumn
        {
            get { return Schema.Columns[14]; }
        }
        
        
        
        public static TableSchema.TableColumn UserIdColumn
        {
            get { return Schema.Columns[15]; }
        }
        
        
        
        public static TableSchema.TableColumn IsReservationLockColumn
        {
            get { return Schema.Columns[16]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string ProductId = @"product_id";
			 public static string DealId = @"deal_id";
			 public static string StoreGuid = @"store_guid";
			 public static string OrderPk = @"order_pk";
			 public static string OrderDetailGuid = @"order_detail_guid";
			 public static string Prefix = @"prefix";
			 public static string Sequence = @"sequence";
			 public static string Code = @"code";
			 public static string Status = @"status";
			 public static string CreateDate = @"create_date";
			 public static string BoughtDate = @"bought_date";
			 public static string UsedDate = @"used_date";
			 public static string RefundDate = @"refund_date";
			 public static string Cost = @"cost";
			 public static string UserId = @"user_id";
			 public static string IsReservationLock = @"is_reservation_lock";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

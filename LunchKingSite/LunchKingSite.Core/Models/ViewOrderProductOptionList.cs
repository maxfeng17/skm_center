using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewOrderProductOptionList class.
    /// </summary>
    [Serializable]
    public partial class ViewOrderProductOptionListCollection : ReadOnlyList<ViewOrderProductOptionList, ViewOrderProductOptionListCollection>
    {        
        public ViewOrderProductOptionListCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_order_product_option_list view.
    /// </summary>
    [Serializable]
    public partial class ViewOrderProductOptionList : ReadOnlyRecord<ViewOrderProductOptionList>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_order_product_option_list", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarProductId = new TableSchema.TableColumn(schema);
                colvarProductId.ColumnName = "product_id";
                colvarProductId.DataType = DbType.Int32;
                colvarProductId.MaxLength = 0;
                colvarProductId.AutoIncrement = false;
                colvarProductId.IsNullable = false;
                colvarProductId.IsPrimaryKey = false;
                colvarProductId.IsForeignKey = false;
                colvarProductId.IsReadOnly = false;
                
                schema.Columns.Add(colvarProductId);
                
                TableSchema.TableColumn colvarOrderGuid = new TableSchema.TableColumn(schema);
                colvarOrderGuid.ColumnName = "order_guid";
                colvarOrderGuid.DataType = DbType.Guid;
                colvarOrderGuid.MaxLength = 0;
                colvarOrderGuid.AutoIncrement = false;
                colvarOrderGuid.IsNullable = false;
                colvarOrderGuid.IsPrimaryKey = false;
                colvarOrderGuid.IsForeignKey = false;
                colvarOrderGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderGuid);
                
                TableSchema.TableColumn colvarCatgName = new TableSchema.TableColumn(schema);
                colvarCatgName.ColumnName = "catg_name";
                colvarCatgName.DataType = DbType.String;
                colvarCatgName.MaxLength = 50;
                colvarCatgName.AutoIncrement = false;
                colvarCatgName.IsNullable = true;
                colvarCatgName.IsPrimaryKey = false;
                colvarCatgName.IsForeignKey = false;
                colvarCatgName.IsReadOnly = false;
                
                schema.Columns.Add(colvarCatgName);
                
                TableSchema.TableColumn colvarCatgSeq = new TableSchema.TableColumn(schema);
                colvarCatgSeq.ColumnName = "catg_seq";
                colvarCatgSeq.DataType = DbType.Int32;
                colvarCatgSeq.MaxLength = 0;
                colvarCatgSeq.AutoIncrement = false;
                colvarCatgSeq.IsNullable = true;
                colvarCatgSeq.IsPrimaryKey = false;
                colvarCatgSeq.IsForeignKey = false;
                colvarCatgSeq.IsReadOnly = false;
                
                schema.Columns.Add(colvarCatgSeq);
                
                TableSchema.TableColumn colvarOptionName = new TableSchema.TableColumn(schema);
                colvarOptionName.ColumnName = "option_name";
                colvarOptionName.DataType = DbType.String;
                colvarOptionName.MaxLength = 50;
                colvarOptionName.AutoIncrement = false;
                colvarOptionName.IsNullable = true;
                colvarOptionName.IsPrimaryKey = false;
                colvarOptionName.IsForeignKey = false;
                colvarOptionName.IsReadOnly = false;
                
                schema.Columns.Add(colvarOptionName);
                
                TableSchema.TableColumn colvarItemNo = new TableSchema.TableColumn(schema);
                colvarItemNo.ColumnName = "item_no";
                colvarItemNo.DataType = DbType.String;
                colvarItemNo.MaxLength = 100;
                colvarItemNo.AutoIncrement = false;
                colvarItemNo.IsNullable = true;
                colvarItemNo.IsPrimaryKey = false;
                colvarItemNo.IsForeignKey = false;
                colvarItemNo.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemNo);
                
                TableSchema.TableColumn colvarOrderProductId = new TableSchema.TableColumn(schema);
                colvarOrderProductId.ColumnName = "order_product_id";
                colvarOrderProductId.DataType = DbType.Int32;
                colvarOrderProductId.MaxLength = 0;
                colvarOrderProductId.AutoIncrement = false;
                colvarOrderProductId.IsNullable = false;
                colvarOrderProductId.IsPrimaryKey = false;
                colvarOrderProductId.IsForeignKey = false;
                colvarOrderProductId.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderProductId);
                
                TableSchema.TableColumn colvarIsCurrent = new TableSchema.TableColumn(schema);
                colvarIsCurrent.ColumnName = "is_current";
                colvarIsCurrent.DataType = DbType.Boolean;
                colvarIsCurrent.MaxLength = 0;
                colvarIsCurrent.AutoIncrement = false;
                colvarIsCurrent.IsNullable = false;
                colvarIsCurrent.IsPrimaryKey = false;
                colvarIsCurrent.IsForeignKey = false;
                colvarIsCurrent.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsCurrent);
                
                TableSchema.TableColumn colvarIsReturned = new TableSchema.TableColumn(schema);
                colvarIsReturned.ColumnName = "is_returned";
                colvarIsReturned.DataType = DbType.Boolean;
                colvarIsReturned.MaxLength = 0;
                colvarIsReturned.AutoIncrement = false;
                colvarIsReturned.IsNullable = false;
                colvarIsReturned.IsPrimaryKey = false;
                colvarIsReturned.IsForeignKey = false;
                colvarIsReturned.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsReturned);
                
                TableSchema.TableColumn colvarIsReturning = new TableSchema.TableColumn(schema);
                colvarIsReturning.ColumnName = "is_returning";
                colvarIsReturning.DataType = DbType.Boolean;
                colvarIsReturning.MaxLength = 0;
                colvarIsReturning.AutoIncrement = false;
                colvarIsReturning.IsNullable = false;
                colvarIsReturning.IsPrimaryKey = false;
                colvarIsReturning.IsForeignKey = false;
                colvarIsReturning.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsReturning);
                
                TableSchema.TableColumn colvarDealType = new TableSchema.TableColumn(schema);
                colvarDealType.ColumnName = "deal_type";
                colvarDealType.DataType = DbType.Int32;
                colvarDealType.MaxLength = 0;
                colvarDealType.AutoIncrement = false;
                colvarDealType.IsNullable = false;
                colvarDealType.IsPrimaryKey = false;
                colvarDealType.IsForeignKey = false;
                colvarDealType.IsReadOnly = false;
                
                schema.Columns.Add(colvarDealType);
                
                TableSchema.TableColumn colvarIsMergeCount = new TableSchema.TableColumn(schema);
                colvarIsMergeCount.ColumnName = "is_merge_count";
                colvarIsMergeCount.DataType = DbType.Int32;
                colvarIsMergeCount.MaxLength = 0;
                colvarIsMergeCount.AutoIncrement = false;
                colvarIsMergeCount.IsNullable = false;
                colvarIsMergeCount.IsPrimaryKey = false;
                colvarIsMergeCount.IsForeignKey = false;
                colvarIsMergeCount.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsMergeCount);
                
                TableSchema.TableColumn colvarItemName = new TableSchema.TableColumn(schema);
                colvarItemName.ColumnName = "item_name";
                colvarItemName.DataType = DbType.String;
                colvarItemName.MaxLength = 4000;
                colvarItemName.AutoIncrement = false;
                colvarItemName.IsNullable = false;
                colvarItemName.IsPrimaryKey = false;
                colvarItemName.IsForeignKey = false;
                colvarItemName.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemName);
                
                TableSchema.TableColumn colvarQuantityMultiplier = new TableSchema.TableColumn(schema);
                colvarQuantityMultiplier.ColumnName = "quantity_multiplier";
                colvarQuantityMultiplier.DataType = DbType.Int32;
                colvarQuantityMultiplier.MaxLength = 0;
                colvarQuantityMultiplier.AutoIncrement = false;
                colvarQuantityMultiplier.IsNullable = true;
                colvarQuantityMultiplier.IsPrimaryKey = false;
                colvarQuantityMultiplier.IsForeignKey = false;
                colvarQuantityMultiplier.IsReadOnly = false;
                
                schema.Columns.Add(colvarQuantityMultiplier);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_order_product_option_list",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewOrderProductOptionList()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewOrderProductOptionList(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewOrderProductOptionList(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewOrderProductOptionList(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("ProductId")]
        [Bindable(true)]
        public int ProductId 
	    {
		    get
		    {
			    return GetColumnValue<int>("product_id");
		    }
            set 
		    {
			    SetColumnValue("product_id", value);
            }
        }
	      
        [XmlAttribute("OrderGuid")]
        [Bindable(true)]
        public Guid OrderGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("order_guid");
		    }
            set 
		    {
			    SetColumnValue("order_guid", value);
            }
        }
	      
        [XmlAttribute("CatgName")]
        [Bindable(true)]
        public string CatgName 
	    {
		    get
		    {
			    return GetColumnValue<string>("catg_name");
		    }
            set 
		    {
			    SetColumnValue("catg_name", value);
            }
        }
	      
        [XmlAttribute("CatgSeq")]
        [Bindable(true)]
        public int? CatgSeq 
	    {
		    get
		    {
			    return GetColumnValue<int?>("catg_seq");
		    }
            set 
		    {
			    SetColumnValue("catg_seq", value);
            }
        }
	      
        [XmlAttribute("OptionName")]
        [Bindable(true)]
        public string OptionName 
	    {
		    get
		    {
			    return GetColumnValue<string>("option_name");
		    }
            set 
		    {
			    SetColumnValue("option_name", value);
            }
        }
	      
        [XmlAttribute("ItemNo")]
        [Bindable(true)]
        public string ItemNo 
	    {
		    get
		    {
			    return GetColumnValue<string>("item_no");
		    }
            set 
		    {
			    SetColumnValue("item_no", value);
            }
        }
	      
        [XmlAttribute("OrderProductId")]
        [Bindable(true)]
        public int OrderProductId 
	    {
		    get
		    {
			    return GetColumnValue<int>("order_product_id");
		    }
            set 
		    {
			    SetColumnValue("order_product_id", value);
            }
        }
	      
        [XmlAttribute("IsCurrent")]
        [Bindable(true)]
        public bool IsCurrent 
	    {
		    get
		    {
			    return GetColumnValue<bool>("is_current");
		    }
            set 
		    {
			    SetColumnValue("is_current", value);
            }
        }
	      
        [XmlAttribute("IsReturned")]
        [Bindable(true)]
        public bool IsReturned 
	    {
		    get
		    {
			    return GetColumnValue<bool>("is_returned");
		    }
            set 
		    {
			    SetColumnValue("is_returned", value);
            }
        }
	      
        [XmlAttribute("IsReturning")]
        [Bindable(true)]
        public bool IsReturning 
	    {
		    get
		    {
			    return GetColumnValue<bool>("is_returning");
		    }
            set 
		    {
			    SetColumnValue("is_returning", value);
            }
        }
	      
        [XmlAttribute("DealType")]
        [Bindable(true)]
        public int DealType 
	    {
		    get
		    {
			    return GetColumnValue<int>("deal_type");
		    }
            set 
		    {
			    SetColumnValue("deal_type", value);
            }
        }
	      
        [XmlAttribute("IsMergeCount")]
        [Bindable(true)]
        public int IsMergeCount 
	    {
		    get
		    {
			    return GetColumnValue<int>("is_merge_count");
		    }
            set 
		    {
			    SetColumnValue("is_merge_count", value);
            }
        }
	      
        [XmlAttribute("ItemName")]
        [Bindable(true)]
        public string ItemName 
	    {
		    get
		    {
			    return GetColumnValue<string>("item_name");
		    }
            set 
		    {
			    SetColumnValue("item_name", value);
            }
        }
	      
        [XmlAttribute("QuantityMultiplier")]
        [Bindable(true)]
        public int? QuantityMultiplier 
	    {
		    get
		    {
			    return GetColumnValue<int?>("quantity_multiplier");
		    }
            set 
		    {
			    SetColumnValue("quantity_multiplier", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string ProductId = @"product_id";
            
            public static string OrderGuid = @"order_guid";
            
            public static string CatgName = @"catg_name";
            
            public static string CatgSeq = @"catg_seq";
            
            public static string OptionName = @"option_name";
            
            public static string ItemNo = @"item_no";
            
            public static string OrderProductId = @"order_product_id";
            
            public static string IsCurrent = @"is_current";
            
            public static string IsReturned = @"is_returned";
            
            public static string IsReturning = @"is_returning";
            
            public static string DealType = @"deal_type";
            
            public static string IsMergeCount = @"is_merge_count";
            
            public static string ItemName = @"item_name";
            
            public static string QuantityMultiplier = @"quantity_multiplier";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

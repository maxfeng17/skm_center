using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewHiDealCouponListSequence class.
    /// </summary>
    [Serializable]
    public partial class ViewHiDealCouponListSequenceCollection : ReadOnlyList<ViewHiDealCouponListSequence, ViewHiDealCouponListSequenceCollection>
    {        
        public ViewHiDealCouponListSequenceCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_hi_deal_coupon_list_sequence view.
    /// </summary>
    [Serializable]
    public partial class ViewHiDealCouponListSequence : ReadOnlyRecord<ViewHiDealCouponListSequence>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_hi_deal_coupon_list_sequence", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarBankStatus = new TableSchema.TableColumn(schema);
                colvarBankStatus.ColumnName = "bank_status";
                colvarBankStatus.DataType = DbType.Int32;
                colvarBankStatus.MaxLength = 0;
                colvarBankStatus.AutoIncrement = false;
                colvarBankStatus.IsNullable = false;
                colvarBankStatus.IsPrimaryKey = false;
                colvarBankStatus.IsForeignKey = false;
                colvarBankStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarBankStatus);
                
                TableSchema.TableColumn colvarCtlStatus = new TableSchema.TableColumn(schema);
                colvarCtlStatus.ColumnName = "ctl_status";
                colvarCtlStatus.DataType = DbType.Int32;
                colvarCtlStatus.MaxLength = 0;
                colvarCtlStatus.AutoIncrement = false;
                colvarCtlStatus.IsNullable = false;
                colvarCtlStatus.IsPrimaryKey = false;
                colvarCtlStatus.IsForeignKey = false;
                colvarCtlStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarCtlStatus);
                
                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int64;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = false;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = false;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                
                schema.Columns.Add(colvarId);
                
                TableSchema.TableColumn colvarPrefix = new TableSchema.TableColumn(schema);
                colvarPrefix.ColumnName = "prefix";
                colvarPrefix.DataType = DbType.AnsiString;
                colvarPrefix.MaxLength = 10;
                colvarPrefix.AutoIncrement = false;
                colvarPrefix.IsNullable = true;
                colvarPrefix.IsPrimaryKey = false;
                colvarPrefix.IsForeignKey = false;
                colvarPrefix.IsReadOnly = false;
                
                schema.Columns.Add(colvarPrefix);
                
                TableSchema.TableColumn colvarSequence = new TableSchema.TableColumn(schema);
                colvarSequence.ColumnName = "sequence";
                colvarSequence.DataType = DbType.AnsiString;
                colvarSequence.MaxLength = 20;
                colvarSequence.AutoIncrement = false;
                colvarSequence.IsNullable = false;
                colvarSequence.IsPrimaryKey = false;
                colvarSequence.IsForeignKey = false;
                colvarSequence.IsReadOnly = false;
                
                schema.Columns.Add(colvarSequence);
                
                TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
                colvarStatus.ColumnName = "status";
                colvarStatus.DataType = DbType.Int32;
                colvarStatus.MaxLength = 0;
                colvarStatus.AutoIncrement = false;
                colvarStatus.IsNullable = false;
                colvarStatus.IsPrimaryKey = false;
                colvarStatus.IsForeignKey = false;
                colvarStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarStatus);
                
                TableSchema.TableColumn colvarGuid = new TableSchema.TableColumn(schema);
                colvarGuid.ColumnName = "guid";
                colvarGuid.DataType = DbType.Guid;
                colvarGuid.MaxLength = 0;
                colvarGuid.AutoIncrement = false;
                colvarGuid.IsNullable = false;
                colvarGuid.IsPrimaryKey = false;
                colvarGuid.IsForeignKey = false;
                colvarGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarGuid);
                
                TableSchema.TableColumn colvarProductName = new TableSchema.TableColumn(schema);
                colvarProductName.ColumnName = "product_name";
                colvarProductName.DataType = DbType.String;
                colvarProductName.MaxLength = 250;
                colvarProductName.AutoIncrement = false;
                colvarProductName.IsNullable = false;
                colvarProductName.IsPrimaryKey = false;
                colvarProductName.IsForeignKey = false;
                colvarProductName.IsReadOnly = false;
                
                schema.Columns.Add(colvarProductName);
                
                TableSchema.TableColumn colvarUseStartTime = new TableSchema.TableColumn(schema);
                colvarUseStartTime.ColumnName = "use_start_time";
                colvarUseStartTime.DataType = DbType.DateTime;
                colvarUseStartTime.MaxLength = 0;
                colvarUseStartTime.AutoIncrement = false;
                colvarUseStartTime.IsNullable = true;
                colvarUseStartTime.IsPrimaryKey = false;
                colvarUseStartTime.IsForeignKey = false;
                colvarUseStartTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarUseStartTime);
                
                TableSchema.TableColumn colvarUseEndTime = new TableSchema.TableColumn(schema);
                colvarUseEndTime.ColumnName = "use_end_time";
                colvarUseEndTime.DataType = DbType.DateTime;
                colvarUseEndTime.MaxLength = 0;
                colvarUseEndTime.AutoIncrement = false;
                colvarUseEndTime.IsNullable = true;
                colvarUseEndTime.IsPrimaryKey = false;
                colvarUseEndTime.IsForeignKey = false;
                colvarUseEndTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarUseEndTime);
                
                TableSchema.TableColumn colvarIsTaxFree = new TableSchema.TableColumn(schema);
                colvarIsTaxFree.ColumnName = "is_tax_free";
                colvarIsTaxFree.DataType = DbType.Boolean;
                colvarIsTaxFree.MaxLength = 0;
                colvarIsTaxFree.AutoIncrement = false;
                colvarIsTaxFree.IsNullable = true;
                colvarIsTaxFree.IsPrimaryKey = false;
                colvarIsTaxFree.IsForeignKey = false;
                colvarIsTaxFree.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsTaxFree);
                
                TableSchema.TableColumn colvarIsSmsClose = new TableSchema.TableColumn(schema);
                colvarIsSmsClose.ColumnName = "is_sms_close";
                colvarIsSmsClose.DataType = DbType.Boolean;
                colvarIsSmsClose.MaxLength = 0;
                colvarIsSmsClose.AutoIncrement = false;
                colvarIsSmsClose.IsNullable = true;
                colvarIsSmsClose.IsPrimaryKey = false;
                colvarIsSmsClose.IsForeignKey = false;
                colvarIsSmsClose.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsSmsClose);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_hi_deal_coupon_list_sequence",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewHiDealCouponListSequence()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewHiDealCouponListSequence(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewHiDealCouponListSequence(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewHiDealCouponListSequence(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("BankStatus")]
        [Bindable(true)]
        public int BankStatus 
	    {
		    get
		    {
			    return GetColumnValue<int>("bank_status");
		    }
            set 
		    {
			    SetColumnValue("bank_status", value);
            }
        }
	      
        [XmlAttribute("CtlStatus")]
        [Bindable(true)]
        public int CtlStatus 
	    {
		    get
		    {
			    return GetColumnValue<int>("ctl_status");
		    }
            set 
		    {
			    SetColumnValue("ctl_status", value);
            }
        }
	      
        [XmlAttribute("Id")]
        [Bindable(true)]
        public long Id 
	    {
		    get
		    {
			    return GetColumnValue<long>("id");
		    }
            set 
		    {
			    SetColumnValue("id", value);
            }
        }
	      
        [XmlAttribute("Prefix")]
        [Bindable(true)]
        public string Prefix 
	    {
		    get
		    {
			    return GetColumnValue<string>("prefix");
		    }
            set 
		    {
			    SetColumnValue("prefix", value);
            }
        }
	      
        [XmlAttribute("Sequence")]
        [Bindable(true)]
        public string Sequence 
	    {
		    get
		    {
			    return GetColumnValue<string>("sequence");
		    }
            set 
		    {
			    SetColumnValue("sequence", value);
            }
        }
	      
        [XmlAttribute("Status")]
        [Bindable(true)]
        public int Status 
	    {
		    get
		    {
			    return GetColumnValue<int>("status");
		    }
            set 
		    {
			    SetColumnValue("status", value);
            }
        }
	      
        [XmlAttribute("Guid")]
        [Bindable(true)]
        public Guid Guid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("guid");
		    }
            set 
		    {
			    SetColumnValue("guid", value);
            }
        }
	      
        [XmlAttribute("ProductName")]
        [Bindable(true)]
        public string ProductName 
	    {
		    get
		    {
			    return GetColumnValue<string>("product_name");
		    }
            set 
		    {
			    SetColumnValue("product_name", value);
            }
        }
	      
        [XmlAttribute("UseStartTime")]
        [Bindable(true)]
        public DateTime? UseStartTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("use_start_time");
		    }
            set 
		    {
			    SetColumnValue("use_start_time", value);
            }
        }
	      
        [XmlAttribute("UseEndTime")]
        [Bindable(true)]
        public DateTime? UseEndTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("use_end_time");
		    }
            set 
		    {
			    SetColumnValue("use_end_time", value);
            }
        }
	      
        [XmlAttribute("IsTaxFree")]
        [Bindable(true)]
        public bool? IsTaxFree 
	    {
		    get
		    {
			    return GetColumnValue<bool?>("is_tax_free");
		    }
            set 
		    {
			    SetColumnValue("is_tax_free", value);
            }
        }
	      
        [XmlAttribute("IsSmsClose")]
        [Bindable(true)]
        public bool? IsSmsClose 
	    {
		    get
		    {
			    return GetColumnValue<bool?>("is_sms_close");
		    }
            set 
		    {
			    SetColumnValue("is_sms_close", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string BankStatus = @"bank_status";
            
            public static string CtlStatus = @"ctl_status";
            
            public static string Id = @"id";
            
            public static string Prefix = @"prefix";
            
            public static string Sequence = @"sequence";
            
            public static string Status = @"status";
            
            public static string Guid = @"guid";
            
            public static string ProductName = @"product_name";
            
            public static string UseStartTime = @"use_start_time";
            
            public static string UseEndTime = @"use_end_time";
            
            public static string IsTaxFree = @"is_tax_free";
            
            public static string IsSmsClose = @"is_sms_close";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

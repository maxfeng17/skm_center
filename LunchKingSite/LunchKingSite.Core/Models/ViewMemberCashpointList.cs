using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the ViewMemberCashpointList class.
    /// </summary>
    [Serializable]
    public partial class ViewMemberCashpointListCollection : ReadOnlyList<ViewMemberCashpointList, ViewMemberCashpointListCollection>
    {
        public ViewMemberCashpointListCollection() { }
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_member_cashpoint_list view.
    /// </summary>
    [Serializable]
    public partial class ViewMemberCashpointList : ReadOnlyRecord<ViewMemberCashpointList>, IReadOnlyRecord
    {

        #region Default Settings
        protected static void SetSQLProps()
        {
            GetTableSchema();
        }
        #endregion
        #region Schema Accessor
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_member_cashpoint_list", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = false;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = false;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;

                schema.Columns.Add(colvarId);

                TableSchema.TableColumn colvarAmount = new TableSchema.TableColumn(schema);
                colvarAmount.ColumnName = "amount";
                colvarAmount.DataType = DbType.Currency;
                colvarAmount.MaxLength = 0;
                colvarAmount.AutoIncrement = false;
                colvarAmount.IsNullable = false;
                colvarAmount.IsPrimaryKey = false;
                colvarAmount.IsForeignKey = false;
                colvarAmount.IsReadOnly = false;

                schema.Columns.Add(colvarAmount);

                TableSchema.TableColumn colvarReference = new TableSchema.TableColumn(schema);
                colvarReference.ColumnName = "reference";
                colvarReference.DataType = DbType.Guid;
                colvarReference.MaxLength = 0;
                colvarReference.AutoIncrement = false;
                colvarReference.IsNullable = true;
                colvarReference.IsPrimaryKey = false;
                colvarReference.IsForeignKey = false;
                colvarReference.IsReadOnly = false;

                schema.Columns.Add(colvarReference);

                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = false;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;

                schema.Columns.Add(colvarCreateTime);

                TableSchema.TableColumn colvarUsername = new TableSchema.TableColumn(schema);
                colvarUsername.ColumnName = "username";
                colvarUsername.DataType = DbType.String;
                colvarUsername.MaxLength = 50;
                colvarUsername.AutoIncrement = false;
                colvarUsername.IsNullable = false;
                colvarUsername.IsPrimaryKey = false;
                colvarUsername.IsForeignKey = false;
                colvarUsername.IsReadOnly = false;

                schema.Columns.Add(colvarUsername);

                TableSchema.TableColumn colvarType = new TableSchema.TableColumn(schema);
                colvarType.ColumnName = "type";
                colvarType.DataType = DbType.Int32;
                colvarType.MaxLength = 0;
                colvarType.AutoIncrement = false;
                colvarType.IsNullable = false;
                colvarType.IsPrimaryKey = false;
                colvarType.IsForeignKey = false;
                colvarType.IsReadOnly = false;

                schema.Columns.Add(colvarType);

                TableSchema.TableColumn colvarScashlistMessage = new TableSchema.TableColumn(schema);
                colvarScashlistMessage.ColumnName = "scashlist_message";
                colvarScashlistMessage.DataType = DbType.String;
                colvarScashlistMessage.MaxLength = 1073741823;
                colvarScashlistMessage.AutoIncrement = false;
                colvarScashlistMessage.IsNullable = true;
                colvarScashlistMessage.IsPrimaryKey = false;
                colvarScashlistMessage.IsForeignKey = false;
                colvarScashlistMessage.IsReadOnly = false;

                schema.Columns.Add(colvarScashlistMessage);

                TableSchema.TableColumn colvarItemName = new TableSchema.TableColumn(schema);
                colvarItemName.ColumnName = "item_name";
                colvarItemName.DataType = DbType.String;
                colvarItemName.MaxLength = 300;
                colvarItemName.AutoIncrement = false;
                colvarItemName.IsNullable = true;
                colvarItemName.IsPrimaryKey = false;
                colvarItemName.IsForeignKey = false;
                colvarItemName.IsReadOnly = false;

                schema.Columns.Add(colvarItemName);

                TableSchema.TableColumn colvarOrderdetailStatus = new TableSchema.TableColumn(schema);
                colvarOrderdetailStatus.ColumnName = "orderdetail_status";
                colvarOrderdetailStatus.DataType = DbType.Int32;
                colvarOrderdetailStatus.MaxLength = 0;
                colvarOrderdetailStatus.AutoIncrement = false;
                colvarOrderdetailStatus.IsNullable = true;
                colvarOrderdetailStatus.IsPrimaryKey = false;
                colvarOrderdetailStatus.IsForeignKey = false;
                colvarOrderdetailStatus.IsReadOnly = false;

                schema.Columns.Add(colvarOrderdetailStatus);

                TableSchema.TableColumn colvarBusinessHourGuid = new TableSchema.TableColumn(schema);
                colvarBusinessHourGuid.ColumnName = "business_hour_guid";
                colvarBusinessHourGuid.DataType = DbType.Guid;
                colvarBusinessHourGuid.MaxLength = 0;
                colvarBusinessHourGuid.AutoIncrement = false;
                colvarBusinessHourGuid.IsNullable = true;
                colvarBusinessHourGuid.IsPrimaryKey = false;
                colvarBusinessHourGuid.IsForeignKey = false;
                colvarBusinessHourGuid.IsReadOnly = false;

                schema.Columns.Add(colvarBusinessHourGuid);

                TableSchema.TableColumn colvarMessage = new TableSchema.TableColumn(schema);
                colvarMessage.ColumnName = "message";
                colvarMessage.DataType = DbType.String;
                colvarMessage.MaxLength = 1073741823;
                colvarMessage.AutoIncrement = false;
                colvarMessage.IsNullable = true;
                colvarMessage.IsPrimaryKey = false;
                colvarMessage.IsForeignKey = false;
                colvarMessage.IsReadOnly = false;

                schema.Columns.Add(colvarMessage);

                TableSchema.TableColumn colvarCashpointorderStatus = new TableSchema.TableColumn(schema);
                colvarCashpointorderStatus.ColumnName = "cashpointorder_status";
                colvarCashpointorderStatus.DataType = DbType.Int32;
                colvarCashpointorderStatus.MaxLength = 0;
                colvarCashpointorderStatus.AutoIncrement = false;
                colvarCashpointorderStatus.IsNullable = true;
                colvarCashpointorderStatus.IsPrimaryKey = false;
                colvarCashpointorderStatus.IsForeignKey = false;
                colvarCashpointorderStatus.IsReadOnly = false;

                schema.Columns.Add(colvarCashpointorderStatus);

                TableSchema.TableColumn colvarOrderStatus = new TableSchema.TableColumn(schema);
                colvarOrderStatus.ColumnName = "order_status";
                colvarOrderStatus.DataType = DbType.Int32;
                colvarOrderStatus.MaxLength = 0;
                colvarOrderStatus.AutoIncrement = false;
                colvarOrderStatus.IsNullable = true;
                colvarOrderStatus.IsPrimaryKey = false;
                colvarOrderStatus.IsForeignKey = false;
                colvarOrderStatus.IsReadOnly = false;

                schema.Columns.Add(colvarOrderStatus);

                TableSchema.TableColumn colvarSumamount = new TableSchema.TableColumn(schema);
                colvarSumamount.ColumnName = "sumamount";
                colvarSumamount.DataType = DbType.Currency;
                colvarSumamount.MaxLength = 0;
                colvarSumamount.AutoIncrement = false;
                colvarSumamount.IsNullable = true;
                colvarSumamount.IsPrimaryKey = false;
                colvarSumamount.IsForeignKey = false;
                colvarSumamount.IsReadOnly = false;

                schema.Columns.Add(colvarSumamount);

                TableSchema.TableColumn colvarOrderClassification = new TableSchema.TableColumn(schema);
                colvarOrderClassification.ColumnName = "order_classification";
                colvarOrderClassification.DataType = DbType.Int32;
                colvarOrderClassification.MaxLength = 0;
                colvarOrderClassification.AutoIncrement = false;
                colvarOrderClassification.IsNullable = true;
                colvarOrderClassification.IsPrimaryKey = false;
                colvarOrderClassification.IsForeignKey = false;
                colvarOrderClassification.IsReadOnly = false;

                schema.Columns.Add(colvarOrderClassification);


                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_member_cashpoint_list", schema);
            }
        }
        #endregion

        #region Query Accessor
        public static Query CreateQuery()
        {
            return new Query(Schema);
        }
        #endregion

        #region .ctors
        public ViewMemberCashpointList()
        {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewMemberCashpointList(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
            {
                ForceDefaults();
            }
            MarkNew();
        }

        public ViewMemberCashpointList(object keyID)
        {
            SetSQLProps();
            LoadByKey(keyID);
        }

        public ViewMemberCashpointList(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName, columnValue);
        }

        #endregion

        #region Props


        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id
        {
            get
            {
                return GetColumnValue<int>("id");
            }
            set
            {
                SetColumnValue("id", value);
            }
        }

        [XmlAttribute("Amount")]
        [Bindable(true)]
        public decimal Amount
        {
            get
            {
                return GetColumnValue<decimal>("amount");
            }
            set
            {
                SetColumnValue("amount", value);
            }
        }

        [XmlAttribute("Reference")]
        [Bindable(true)]
        public Guid? Reference
        {
            get
            {
                return GetColumnValue<Guid?>("reference");
            }
            set
            {
                SetColumnValue("reference", value);
            }
        }

        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime CreateTime
        {
            get
            {
                return GetColumnValue<DateTime>("create_time");
            }
            set
            {
                SetColumnValue("create_time", value);
            }
        }

        [XmlAttribute("Username")]
        [Bindable(true)]
        public string Username
        {
            get
            {
                return GetColumnValue<string>("username");
            }
            set
            {
                SetColumnValue("username", value);
            }
        }

        [XmlAttribute("Type")]
        [Bindable(true)]
        public int Type
        {
            get
            {
                return GetColumnValue<int>("type");
            }
            set
            {
                SetColumnValue("type", value);
            }
        }

        [XmlAttribute("ScashlistMessage")]
        [Bindable(true)]
        public string ScashlistMessage
        {
            get
            {
                return GetColumnValue<string>("scashlist_message");
            }
            set
            {
                SetColumnValue("scashlist_message", value);
            }
        }

        [XmlAttribute("ItemName")]
        [Bindable(true)]
        public string ItemName
        {
            get
            {
                return GetColumnValue<string>("item_name");
            }
            set
            {
                SetColumnValue("item_name", value);
            }
        }

        [XmlAttribute("OrderdetailStatus")]
        [Bindable(true)]
        public int? OrderdetailStatus
        {
            get
            {
                return GetColumnValue<int?>("orderdetail_status");
            }
            set
            {
                SetColumnValue("orderdetail_status", value);
            }
        }

        [XmlAttribute("BusinessHourGuid")]
        [Bindable(true)]
        public Guid? BusinessHourGuid
        {
            get
            {
                return GetColumnValue<Guid?>("business_hour_guid");
            }
            set
            {
                SetColumnValue("business_hour_guid", value);
            }
        }

        [XmlAttribute("Message")]
        [Bindable(true)]
        public string Message
        {
            get
            {
                return GetColumnValue<string>("message");
            }
            set
            {
                SetColumnValue("message", value);
            }
        }

        [XmlAttribute("CashpointorderStatus")]
        [Bindable(true)]
        public int? CashpointorderStatus
        {
            get
            {
                return GetColumnValue<int?>("cashpointorder_status");
            }
            set
            {
                SetColumnValue("cashpointorder_status", value);
            }
        }

        [XmlAttribute("OrderStatus")]
        [Bindable(true)]
        public int? OrderStatus
        {
            get
            {
                return GetColumnValue<int?>("order_status");
            }
            set
            {
                SetColumnValue("order_status", value);
            }
        }

        [XmlAttribute("Sumamount")]
        [Bindable(true)]
        public decimal? Sumamount
        {
            get
            {
                return GetColumnValue<decimal?>("sumamount");
            }
            set
            {
                SetColumnValue("sumamount", value);
            }
        }

        [XmlAttribute("OrderClassification")]
        [Bindable(true)]
        public int? OrderClassification
        {
            get
            {
                return GetColumnValue<int?>("order_classification");
            }
            set
            {
                SetColumnValue("order_classification", value);
            }
        }

        #endregion

        #region Columns Struct
        public struct Columns
        {


            public static string Id = @"id";

            public static string Amount = @"amount";

            public static string Reference = @"reference";

            public static string CreateTime = @"create_time";

            public static string Username = @"username";

            public static string Type = @"type";

            public static string ScashlistMessage = @"scashlist_message";

            public static string ItemName = @"item_name";

            public static string OrderdetailStatus = @"orderdetail_status";

            public static string BusinessHourGuid = @"business_hour_guid";

            public static string Message = @"message";

            public static string CashpointorderStatus = @"cashpointorder_status";

            public static string OrderStatus = @"order_status";

            public static string Sumamount = @"sumamount";

            public static string OrderClassification = @"order_classification";

        }
        #endregion


        #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName)
        {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName)
        {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion

    }
}

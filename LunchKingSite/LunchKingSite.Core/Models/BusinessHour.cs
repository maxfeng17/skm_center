using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the BusinessHour class.
    /// </summary>
    [Serializable]
    public partial class BusinessHourCollection : RepositoryList<BusinessHour, BusinessHourCollection>
    {
        public BusinessHourCollection() { }

        /// <summary>
        /// Filters an existing collection based on the set criteria. This is an in-memory filter
        /// Thanks to developingchris for this!
        /// </summary>
        /// <returns>BusinessHourCollection</returns>
        public BusinessHourCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                BusinessHour o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }


    }
    /// <summary>
    /// This is an ActiveRecord class which wraps the business_hour table.
    /// </summary>
    [Serializable]
    public partial class BusinessHour : RepositoryRecord<BusinessHour>, IRecordBase
    {
        #region .ctors and Default Settings

        public BusinessHour()
        {
            SetSQLProps();
            InitSetDefaults();
            MarkNew();
        }

        private void InitSetDefaults() { SetDefaults(); }

        public BusinessHour(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
                ForceDefaults();
            MarkNew();
        }

        protected static void SetSQLProps() { GetTableSchema(); }

        #endregion

        #region Schema and Query Accessor	
        public static Query CreateQuery() { return new Query(Schema); }
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                    SetSQLProps();
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("business_hour", TableType.Table, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarGuid = new TableSchema.TableColumn(schema);
                colvarGuid.ColumnName = "GUID";
                colvarGuid.DataType = DbType.Guid;
                colvarGuid.MaxLength = 0;
                colvarGuid.AutoIncrement = false;
                colvarGuid.IsNullable = false;
                colvarGuid.IsPrimaryKey = true;
                colvarGuid.IsForeignKey = false;
                colvarGuid.IsReadOnly = false;
                colvarGuid.DefaultSetting = @"";
                colvarGuid.ForeignKeyTableName = "";
                schema.Columns.Add(colvarGuid);

                TableSchema.TableColumn colvarBusinessHourId = new TableSchema.TableColumn(schema);
                colvarBusinessHourId.ColumnName = "business_hour_id";
                colvarBusinessHourId.DataType = DbType.AnsiString;
                colvarBusinessHourId.MaxLength = 20;
                colvarBusinessHourId.AutoIncrement = false;
                colvarBusinessHourId.IsNullable = true;
                colvarBusinessHourId.IsPrimaryKey = false;
                colvarBusinessHourId.IsForeignKey = false;
                colvarBusinessHourId.IsReadOnly = false;
                colvarBusinessHourId.DefaultSetting = @"";
                colvarBusinessHourId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarBusinessHourId);

                TableSchema.TableColumn colvarBusinessHourTypeId = new TableSchema.TableColumn(schema);
                colvarBusinessHourTypeId.ColumnName = "business_hour_type_id";
                colvarBusinessHourTypeId.DataType = DbType.Int32;
                colvarBusinessHourTypeId.MaxLength = 0;
                colvarBusinessHourTypeId.AutoIncrement = false;
                colvarBusinessHourTypeId.IsNullable = false;
                colvarBusinessHourTypeId.IsPrimaryKey = false;
                colvarBusinessHourTypeId.IsForeignKey = false;
                colvarBusinessHourTypeId.IsReadOnly = false;
                colvarBusinessHourTypeId.DefaultSetting = @"";
                colvarBusinessHourTypeId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarBusinessHourTypeId);

                TableSchema.TableColumn colvarSellerGuid = new TableSchema.TableColumn(schema);
                colvarSellerGuid.ColumnName = "seller_GUID";
                colvarSellerGuid.DataType = DbType.Guid;
                colvarSellerGuid.MaxLength = 0;
                colvarSellerGuid.AutoIncrement = false;
                colvarSellerGuid.IsNullable = false;
                colvarSellerGuid.IsPrimaryKey = false;
                colvarSellerGuid.IsForeignKey = true;
                colvarSellerGuid.IsReadOnly = false;
                colvarSellerGuid.DefaultSetting = @"";

                colvarSellerGuid.ForeignKeyTableName = "seller";
                schema.Columns.Add(colvarSellerGuid);

                TableSchema.TableColumn colvarBusinessHourOrderTimeS = new TableSchema.TableColumn(schema);
                colvarBusinessHourOrderTimeS.ColumnName = "business_hour_order_time_s";
                colvarBusinessHourOrderTimeS.DataType = DbType.DateTime;
                colvarBusinessHourOrderTimeS.MaxLength = 0;
                colvarBusinessHourOrderTimeS.AutoIncrement = false;
                colvarBusinessHourOrderTimeS.IsNullable = false;
                colvarBusinessHourOrderTimeS.IsPrimaryKey = false;
                colvarBusinessHourOrderTimeS.IsForeignKey = false;
                colvarBusinessHourOrderTimeS.IsReadOnly = false;
                colvarBusinessHourOrderTimeS.DefaultSetting = @"";
                colvarBusinessHourOrderTimeS.ForeignKeyTableName = "";
                schema.Columns.Add(colvarBusinessHourOrderTimeS);

                TableSchema.TableColumn colvarBusinessHourOrderTimeE = new TableSchema.TableColumn(schema);
                colvarBusinessHourOrderTimeE.ColumnName = "business_hour_order_time_e";
                colvarBusinessHourOrderTimeE.DataType = DbType.DateTime;
                colvarBusinessHourOrderTimeE.MaxLength = 0;
                colvarBusinessHourOrderTimeE.AutoIncrement = false;
                colvarBusinessHourOrderTimeE.IsNullable = false;
                colvarBusinessHourOrderTimeE.IsPrimaryKey = false;
                colvarBusinessHourOrderTimeE.IsForeignKey = false;
                colvarBusinessHourOrderTimeE.IsReadOnly = false;
                colvarBusinessHourOrderTimeE.DefaultSetting = @"";
                colvarBusinessHourOrderTimeE.ForeignKeyTableName = "";
                schema.Columns.Add(colvarBusinessHourOrderTimeE);

                TableSchema.TableColumn colvarBusinessHourDeliverTimeS = new TableSchema.TableColumn(schema);
                colvarBusinessHourDeliverTimeS.ColumnName = "business_hour_deliver_time_s";
                colvarBusinessHourDeliverTimeS.DataType = DbType.DateTime;
                colvarBusinessHourDeliverTimeS.MaxLength = 0;
                colvarBusinessHourDeliverTimeS.AutoIncrement = false;
                colvarBusinessHourDeliverTimeS.IsNullable = true;
                colvarBusinessHourDeliverTimeS.IsPrimaryKey = false;
                colvarBusinessHourDeliverTimeS.IsForeignKey = false;
                colvarBusinessHourDeliverTimeS.IsReadOnly = false;
                colvarBusinessHourDeliverTimeS.DefaultSetting = @"";
                colvarBusinessHourDeliverTimeS.ForeignKeyTableName = "";
                schema.Columns.Add(colvarBusinessHourDeliverTimeS);

                TableSchema.TableColumn colvarBusinessHourDeliverTimeE = new TableSchema.TableColumn(schema);
                colvarBusinessHourDeliverTimeE.ColumnName = "business_hour_deliver_time_e";
                colvarBusinessHourDeliverTimeE.DataType = DbType.DateTime;
                colvarBusinessHourDeliverTimeE.MaxLength = 0;
                colvarBusinessHourDeliverTimeE.AutoIncrement = false;
                colvarBusinessHourDeliverTimeE.IsNullable = true;
                colvarBusinessHourDeliverTimeE.IsPrimaryKey = false;
                colvarBusinessHourDeliverTimeE.IsForeignKey = false;
                colvarBusinessHourDeliverTimeE.IsReadOnly = false;
                colvarBusinessHourDeliverTimeE.DefaultSetting = @"";
                colvarBusinessHourDeliverTimeE.ForeignKeyTableName = "";
                schema.Columns.Add(colvarBusinessHourDeliverTimeE);

                TableSchema.TableColumn colvarBusinessHourDeliveryCharge = new TableSchema.TableColumn(schema);
                colvarBusinessHourDeliveryCharge.ColumnName = "business_hour_delivery_charge";
                colvarBusinessHourDeliveryCharge.DataType = DbType.Currency;
                colvarBusinessHourDeliveryCharge.MaxLength = 0;
                colvarBusinessHourDeliveryCharge.AutoIncrement = false;
                colvarBusinessHourDeliveryCharge.IsNullable = false;
                colvarBusinessHourDeliveryCharge.IsPrimaryKey = false;
                colvarBusinessHourDeliveryCharge.IsForeignKey = false;
                colvarBusinessHourDeliveryCharge.IsReadOnly = false;
                colvarBusinessHourDeliveryCharge.DefaultSetting = @"";
                colvarBusinessHourDeliveryCharge.ForeignKeyTableName = "";
                schema.Columns.Add(colvarBusinessHourDeliveryCharge);

                TableSchema.TableColumn colvarBusinessHourOrderMinimum = new TableSchema.TableColumn(schema);
                colvarBusinessHourOrderMinimum.ColumnName = "business_hour_order_minimum";
                colvarBusinessHourOrderMinimum.DataType = DbType.Currency;
                colvarBusinessHourOrderMinimum.MaxLength = 0;
                colvarBusinessHourOrderMinimum.AutoIncrement = false;
                colvarBusinessHourOrderMinimum.IsNullable = false;
                colvarBusinessHourOrderMinimum.IsPrimaryKey = false;
                colvarBusinessHourOrderMinimum.IsForeignKey = false;
                colvarBusinessHourOrderMinimum.IsReadOnly = false;
                colvarBusinessHourOrderMinimum.DefaultSetting = @"";
                colvarBusinessHourOrderMinimum.ForeignKeyTableName = "";
                schema.Columns.Add(colvarBusinessHourOrderMinimum);

                TableSchema.TableColumn colvarBusinessHourPreparationTime = new TableSchema.TableColumn(schema);
                colvarBusinessHourPreparationTime.ColumnName = "business_hour_preparation_time";
                colvarBusinessHourPreparationTime.DataType = DbType.Int32;
                colvarBusinessHourPreparationTime.MaxLength = 0;
                colvarBusinessHourPreparationTime.AutoIncrement = false;
                colvarBusinessHourPreparationTime.IsNullable = false;
                colvarBusinessHourPreparationTime.IsPrimaryKey = false;
                colvarBusinessHourPreparationTime.IsForeignKey = false;
                colvarBusinessHourPreparationTime.IsReadOnly = false;
                colvarBusinessHourPreparationTime.DefaultSetting = @"";
                colvarBusinessHourPreparationTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarBusinessHourPreparationTime);

                TableSchema.TableColumn colvarBusinessHourOnline = new TableSchema.TableColumn(schema);
                colvarBusinessHourOnline.ColumnName = "business_hour_online";
                colvarBusinessHourOnline.DataType = DbType.Boolean;
                colvarBusinessHourOnline.MaxLength = 0;
                colvarBusinessHourOnline.AutoIncrement = false;
                colvarBusinessHourOnline.IsNullable = false;
                colvarBusinessHourOnline.IsPrimaryKey = false;
                colvarBusinessHourOnline.IsForeignKey = false;
                colvarBusinessHourOnline.IsReadOnly = false;
                colvarBusinessHourOnline.DefaultSetting = @"";
                colvarBusinessHourOnline.ForeignKeyTableName = "";
                schema.Columns.Add(colvarBusinessHourOnline);

                TableSchema.TableColumn colvarBusinessHourStatus = new TableSchema.TableColumn(schema);
                colvarBusinessHourStatus.ColumnName = "business_hour_status";
                colvarBusinessHourStatus.DataType = DbType.Int32;
                colvarBusinessHourStatus.MaxLength = 0;
                colvarBusinessHourStatus.AutoIncrement = false;
                colvarBusinessHourStatus.IsNullable = false;
                colvarBusinessHourStatus.IsPrimaryKey = false;
                colvarBusinessHourStatus.IsForeignKey = false;
                colvarBusinessHourStatus.IsReadOnly = false;
                colvarBusinessHourStatus.DefaultSetting = @"";
                colvarBusinessHourStatus.ForeignKeyTableName = "";
                schema.Columns.Add(colvarBusinessHourStatus);

                TableSchema.TableColumn colvarOrderTotalLimit = new TableSchema.TableColumn(schema);
                colvarOrderTotalLimit.ColumnName = "order_total_limit";
                colvarOrderTotalLimit.DataType = DbType.Currency;
                colvarOrderTotalLimit.MaxLength = 0;
                colvarOrderTotalLimit.AutoIncrement = false;
                colvarOrderTotalLimit.IsNullable = true;
                colvarOrderTotalLimit.IsPrimaryKey = false;
                colvarOrderTotalLimit.IsForeignKey = false;
                colvarOrderTotalLimit.IsReadOnly = false;
                colvarOrderTotalLimit.DefaultSetting = @"";
                colvarOrderTotalLimit.ForeignKeyTableName = "";
                schema.Columns.Add(colvarOrderTotalLimit);

                TableSchema.TableColumn colvarDeliveryLimit = new TableSchema.TableColumn(schema);
                colvarDeliveryLimit.ColumnName = "delivery_limit";
                colvarDeliveryLimit.DataType = DbType.Int32;
                colvarDeliveryLimit.MaxLength = 0;
                colvarDeliveryLimit.AutoIncrement = false;
                colvarDeliveryLimit.IsNullable = true;
                colvarDeliveryLimit.IsPrimaryKey = false;
                colvarDeliveryLimit.IsForeignKey = false;
                colvarDeliveryLimit.IsReadOnly = false;
                colvarDeliveryLimit.DefaultSetting = @"";
                colvarDeliveryLimit.ForeignKeyTableName = "";
                schema.Columns.Add(colvarDeliveryLimit);

                TableSchema.TableColumn colvarHoliday = new TableSchema.TableColumn(schema);
                colvarHoliday.ColumnName = "holiday";
                colvarHoliday.DataType = DbType.Int32;
                colvarHoliday.MaxLength = 0;
                colvarHoliday.AutoIncrement = false;
                colvarHoliday.IsNullable = false;
                colvarHoliday.IsPrimaryKey = false;
                colvarHoliday.IsForeignKey = false;
                colvarHoliday.IsReadOnly = false;
                colvarHoliday.DefaultSetting = @"";
                colvarHoliday.ForeignKeyTableName = "";
                schema.Columns.Add(colvarHoliday);

                TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
                colvarCreateId.ColumnName = "create_id";
                colvarCreateId.DataType = DbType.String;
                colvarCreateId.MaxLength = 30;
                colvarCreateId.AutoIncrement = false;
                colvarCreateId.IsNullable = false;
                colvarCreateId.IsPrimaryKey = false;
                colvarCreateId.IsForeignKey = false;
                colvarCreateId.IsReadOnly = false;
                colvarCreateId.DefaultSetting = @"";
                colvarCreateId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateId);

                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = false;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                colvarCreateTime.DefaultSetting = @"";
                colvarCreateTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateTime);

                TableSchema.TableColumn colvarModifyId = new TableSchema.TableColumn(schema);
                colvarModifyId.ColumnName = "modify_id";
                colvarModifyId.DataType = DbType.String;
                colvarModifyId.MaxLength = 30;
                colvarModifyId.AutoIncrement = false;
                colvarModifyId.IsNullable = true;
                colvarModifyId.IsPrimaryKey = false;
                colvarModifyId.IsForeignKey = false;
                colvarModifyId.IsReadOnly = false;
                colvarModifyId.DefaultSetting = @"";
                colvarModifyId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarModifyId);

                TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
                colvarModifyTime.ColumnName = "modify_time";
                colvarModifyTime.DataType = DbType.DateTime;
                colvarModifyTime.MaxLength = 0;
                colvarModifyTime.AutoIncrement = false;
                colvarModifyTime.IsNullable = true;
                colvarModifyTime.IsPrimaryKey = false;
                colvarModifyTime.IsForeignKey = false;
                colvarModifyTime.IsReadOnly = false;
                colvarModifyTime.DefaultSetting = @"";
                colvarModifyTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarModifyTime);

                TableSchema.TableColumn colvarBusinessHourAtmMaximum = new TableSchema.TableColumn(schema);
                colvarBusinessHourAtmMaximum.ColumnName = "business_hour_atm_maximum";
                colvarBusinessHourAtmMaximum.DataType = DbType.Int32;
                colvarBusinessHourAtmMaximum.MaxLength = 0;
                colvarBusinessHourAtmMaximum.AutoIncrement = false;
                colvarBusinessHourAtmMaximum.IsNullable = false;
                colvarBusinessHourAtmMaximum.IsPrimaryKey = false;
                colvarBusinessHourAtmMaximum.IsForeignKey = false;
                colvarBusinessHourAtmMaximum.IsReadOnly = false;

                colvarBusinessHourAtmMaximum.DefaultSetting = @"((0))";
                colvarBusinessHourAtmMaximum.ForeignKeyTableName = "";
                schema.Columns.Add(colvarBusinessHourAtmMaximum);

                TableSchema.TableColumn colvarPageTitle = new TableSchema.TableColumn(schema);
                colvarPageTitle.ColumnName = "page_title";
                colvarPageTitle.DataType = DbType.String;
                colvarPageTitle.MaxLength = 36;
                colvarPageTitle.AutoIncrement = false;
                colvarPageTitle.IsNullable = true;
                colvarPageTitle.IsPrimaryKey = false;
                colvarPageTitle.IsForeignKey = false;
                colvarPageTitle.IsReadOnly = false;
                colvarPageTitle.DefaultSetting = @"";
                colvarPageTitle.ForeignKeyTableName = "";
                schema.Columns.Add(colvarPageTitle);

                TableSchema.TableColumn colvarPageDesc = new TableSchema.TableColumn(schema);
                colvarPageDesc.ColumnName = "page_desc";
                colvarPageDesc.DataType = DbType.String;
                colvarPageDesc.MaxLength = 350;
                colvarPageDesc.AutoIncrement = false;
                colvarPageDesc.IsNullable = true;
                colvarPageDesc.IsPrimaryKey = false;
                colvarPageDesc.IsForeignKey = false;
                colvarPageDesc.IsReadOnly = false;
                colvarPageDesc.DefaultSetting = @"";
                colvarPageDesc.ForeignKeyTableName = "";
                schema.Columns.Add(colvarPageDesc);

                TableSchema.TableColumn colvarPicAlt = new TableSchema.TableColumn(schema);
                colvarPicAlt.ColumnName = "pic_alt";
                colvarPicAlt.DataType = DbType.String;
                colvarPicAlt.MaxLength = 120;
                colvarPicAlt.AutoIncrement = false;
                colvarPicAlt.IsNullable = true;
                colvarPicAlt.IsPrimaryKey = false;
                colvarPicAlt.IsForeignKey = false;
                colvarPicAlt.IsReadOnly = false;
                colvarPicAlt.DefaultSetting = @"";
                colvarPicAlt.ForeignKeyTableName = "";
                schema.Columns.Add(colvarPicAlt);

                TableSchema.TableColumn colvarChangedExpireDate = new TableSchema.TableColumn(schema);
                colvarChangedExpireDate.ColumnName = "changed_expire_date";
                colvarChangedExpireDate.DataType = DbType.DateTime;
                colvarChangedExpireDate.MaxLength = 0;
                colvarChangedExpireDate.AutoIncrement = false;
                colvarChangedExpireDate.IsNullable = true;
                colvarChangedExpireDate.IsPrimaryKey = false;
                colvarChangedExpireDate.IsForeignKey = false;
                colvarChangedExpireDate.IsReadOnly = false;
                colvarChangedExpireDate.DefaultSetting = @"";
                colvarChangedExpireDate.ForeignKeyTableName = "";
                schema.Columns.Add(colvarChangedExpireDate);

                TableSchema.TableColumn colvarSettlementTime = new TableSchema.TableColumn(schema);
                colvarSettlementTime.ColumnName = "settlement_time";
                colvarSettlementTime.DataType = DbType.DateTime;
                colvarSettlementTime.MaxLength = 0;
                colvarSettlementTime.AutoIncrement = false;
                colvarSettlementTime.IsNullable = true;
                colvarSettlementTime.IsPrimaryKey = false;
                colvarSettlementTime.IsForeignKey = false;
                colvarSettlementTime.IsReadOnly = false;
                colvarSettlementTime.DefaultSetting = @"";
                colvarSettlementTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarSettlementTime);

                TableSchema.TableColumn colvarExpireRedirectDisplay = new TableSchema.TableColumn(schema);
                colvarExpireRedirectDisplay.ColumnName = "expire_redirect_display";
                colvarExpireRedirectDisplay.DataType = DbType.Int32;
                colvarExpireRedirectDisplay.MaxLength = 0;
                colvarExpireRedirectDisplay.AutoIncrement = false;
                colvarExpireRedirectDisplay.IsNullable = true;
                colvarExpireRedirectDisplay.IsPrimaryKey = false;
                colvarExpireRedirectDisplay.IsForeignKey = false;
                colvarExpireRedirectDisplay.IsReadOnly = false;
                colvarExpireRedirectDisplay.DefaultSetting = @"";
                colvarExpireRedirectDisplay.ForeignKeyTableName = "";
                schema.Columns.Add(colvarExpireRedirectDisplay);

                TableSchema.TableColumn colvarExpireRedirectUrl = new TableSchema.TableColumn(schema);
                colvarExpireRedirectUrl.ColumnName = "expire_redirect_url";
                colvarExpireRedirectUrl.DataType = DbType.String;
                colvarExpireRedirectUrl.MaxLength = 500;
                colvarExpireRedirectUrl.AutoIncrement = false;
                colvarExpireRedirectUrl.IsNullable = true;
                colvarExpireRedirectUrl.IsPrimaryKey = false;
                colvarExpireRedirectUrl.IsForeignKey = false;
                colvarExpireRedirectUrl.IsReadOnly = false;
                colvarExpireRedirectUrl.DefaultSetting = @"";
                colvarExpireRedirectUrl.ForeignKeyTableName = "";
                schema.Columns.Add(colvarExpireRedirectUrl);

                TableSchema.TableColumn colvarUseExpireRedirectUrlAsCanonical = new TableSchema.TableColumn(schema);
                colvarUseExpireRedirectUrlAsCanonical.ColumnName = "use_expire_redirect_url_as_canonical";
                colvarUseExpireRedirectUrlAsCanonical.DataType = DbType.Boolean;
                colvarUseExpireRedirectUrlAsCanonical.MaxLength = 0;
                colvarUseExpireRedirectUrlAsCanonical.AutoIncrement = false;
                colvarUseExpireRedirectUrlAsCanonical.IsNullable = true;
                colvarUseExpireRedirectUrlAsCanonical.IsPrimaryKey = false;
                colvarUseExpireRedirectUrlAsCanonical.IsForeignKey = false;
                colvarUseExpireRedirectUrlAsCanonical.IsReadOnly = false;
                colvarUseExpireRedirectUrlAsCanonical.DefaultSetting = @"";
                colvarUseExpireRedirectUrlAsCanonical.ForeignKeyTableName = "";
                schema.Columns.Add(colvarUseExpireRedirectUrlAsCanonical);

                TableSchema.TableColumn colvarExpireRedirectManual = new TableSchema.TableColumn(schema);
                colvarExpireRedirectManual.ColumnName = "expire_redirect_manual";
                colvarExpireRedirectManual.DataType = DbType.Boolean;
                colvarExpireRedirectManual.MaxLength = 0;
                colvarExpireRedirectManual.AutoIncrement = false;
                colvarExpireRedirectManual.IsNullable = false;
                colvarExpireRedirectManual.IsPrimaryKey = false;
                colvarExpireRedirectManual.IsForeignKey = false;
                colvarExpireRedirectManual.IsReadOnly = false;

                colvarExpireRedirectManual.DefaultSetting = @"((0))";
                colvarExpireRedirectManual.ForeignKeyTableName = "";
                schema.Columns.Add(colvarExpireRedirectManual);

                TableSchema.TableColumn colvarExpireRedirectChangeDate = new TableSchema.TableColumn(schema);
                colvarExpireRedirectChangeDate.ColumnName = "expire_redirect_change_date";
                colvarExpireRedirectChangeDate.DataType = DbType.DateTime;
                colvarExpireRedirectChangeDate.MaxLength = 0;
                colvarExpireRedirectChangeDate.AutoIncrement = false;
                colvarExpireRedirectChangeDate.IsNullable = true;
                colvarExpireRedirectChangeDate.IsPrimaryKey = false;
                colvarExpireRedirectChangeDate.IsForeignKey = false;
                colvarExpireRedirectChangeDate.IsReadOnly = false;
                colvarExpireRedirectChangeDate.DefaultSetting = @"";
                colvarExpireRedirectChangeDate.ForeignKeyTableName = "";
                schema.Columns.Add(colvarExpireRedirectChangeDate);

                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("business_hour", schema);
            }
        }
        #endregion

        #region Props

        [XmlAttribute("Guid")]
        [Bindable(true)]
        public Guid Guid
        {
            get { return GetColumnValue<Guid>(Columns.Guid); }
            set { SetColumnValue(Columns.Guid, value); }
        }

        [XmlAttribute("BusinessHourId")]
        [Bindable(true)]
        public string BusinessHourId
        {
            get { return GetColumnValue<string>(Columns.BusinessHourId); }
            set { SetColumnValue(Columns.BusinessHourId, value); }
        }

        [XmlAttribute("BusinessHourTypeId")]
        [Bindable(true)]
        public int BusinessHourTypeId
        {
            get { return GetColumnValue<int>(Columns.BusinessHourTypeId); }
            set { SetColumnValue(Columns.BusinessHourTypeId, value); }
        }

        [XmlAttribute("SellerGuid")]
        [Bindable(true)]
        public Guid SellerGuid
        {
            get { return GetColumnValue<Guid>(Columns.SellerGuid); }
            set { SetColumnValue(Columns.SellerGuid, value); }
        }

        [XmlAttribute("BusinessHourOrderTimeS")]
        [Bindable(true)]
        public DateTime BusinessHourOrderTimeS
        {
            get { return GetColumnValue<DateTime>(Columns.BusinessHourOrderTimeS); }
            set { SetColumnValue(Columns.BusinessHourOrderTimeS, value); }
        }

        [XmlAttribute("BusinessHourOrderTimeE")]
        [Bindable(true)]
        public DateTime BusinessHourOrderTimeE
        {
            get { return GetColumnValue<DateTime>(Columns.BusinessHourOrderTimeE); }
            set { SetColumnValue(Columns.BusinessHourOrderTimeE, value); }
        }

        [XmlAttribute("BusinessHourDeliverTimeS")]
        [Bindable(true)]
        public DateTime? BusinessHourDeliverTimeS
        {
            get { return GetColumnValue<DateTime?>(Columns.BusinessHourDeliverTimeS); }
            set { SetColumnValue(Columns.BusinessHourDeliverTimeS, value); }
        }

        [XmlAttribute("BusinessHourDeliverTimeE")]
        [Bindable(true)]
        public DateTime? BusinessHourDeliverTimeE
        {
            get { return GetColumnValue<DateTime?>(Columns.BusinessHourDeliverTimeE); }
            set { SetColumnValue(Columns.BusinessHourDeliverTimeE, value); }
        }

        [XmlAttribute("BusinessHourDeliveryCharge")]
        [Bindable(true)]
        public decimal BusinessHourDeliveryCharge
        {
            get { return GetColumnValue<decimal>(Columns.BusinessHourDeliveryCharge); }
            set { SetColumnValue(Columns.BusinessHourDeliveryCharge, value); }
        }

        [XmlAttribute("BusinessHourOrderMinimum")]
        [Bindable(true)]
        public decimal BusinessHourOrderMinimum
        {
            get { return GetColumnValue<decimal>(Columns.BusinessHourOrderMinimum); }
            set { SetColumnValue(Columns.BusinessHourOrderMinimum, value); }
        }

        [XmlAttribute("BusinessHourPreparationTime")]
        [Bindable(true)]
        public int BusinessHourPreparationTime
        {
            get { return GetColumnValue<int>(Columns.BusinessHourPreparationTime); }
            set { SetColumnValue(Columns.BusinessHourPreparationTime, value); }
        }

        [XmlAttribute("BusinessHourOnline")]
        [Bindable(true)]
        public bool BusinessHourOnline
        {
            get { return GetColumnValue<bool>(Columns.BusinessHourOnline); }
            set { SetColumnValue(Columns.BusinessHourOnline, value); }
        }

        [XmlAttribute("BusinessHourStatus")]
        [Bindable(true)]
        public int BusinessHourStatus
        {
            get { return GetColumnValue<int>(Columns.BusinessHourStatus); }
            set { SetColumnValue(Columns.BusinessHourStatus, value); }
        }

        [XmlAttribute("OrderTotalLimit")]
        [Bindable(true)]
        public decimal? OrderTotalLimit
        {
            get { return GetColumnValue<decimal?>(Columns.OrderTotalLimit); }
            set { SetColumnValue(Columns.OrderTotalLimit, value); }
        }

        [XmlAttribute("DeliveryLimit")]
        [Bindable(true)]
        public int? DeliveryLimit
        {
            get { return GetColumnValue<int?>(Columns.DeliveryLimit); }
            set { SetColumnValue(Columns.DeliveryLimit, value); }
        }

        [XmlAttribute("Holiday")]
        [Bindable(true)]
        public int Holiday
        {
            get { return GetColumnValue<int>(Columns.Holiday); }
            set { SetColumnValue(Columns.Holiday, value); }
        }

        [XmlAttribute("CreateId")]
        [Bindable(true)]
        public string CreateId
        {
            get { return GetColumnValue<string>(Columns.CreateId); }
            set { SetColumnValue(Columns.CreateId, value); }
        }

        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime CreateTime
        {
            get { return GetColumnValue<DateTime>(Columns.CreateTime); }
            set { SetColumnValue(Columns.CreateTime, value); }
        }

        [XmlAttribute("ModifyId")]
        [Bindable(true)]
        public string ModifyId
        {
            get { return GetColumnValue<string>(Columns.ModifyId); }
            set { SetColumnValue(Columns.ModifyId, value); }
        }

        [XmlAttribute("ModifyTime")]
        [Bindable(true)]
        public DateTime? ModifyTime
        {
            get { return GetColumnValue<DateTime?>(Columns.ModifyTime); }
            set { SetColumnValue(Columns.ModifyTime, value); }
        }

        [XmlAttribute("BusinessHourAtmMaximum")]
        [Bindable(true)]
        public int BusinessHourAtmMaximum
        {
            get { return GetColumnValue<int>(Columns.BusinessHourAtmMaximum); }
            set { SetColumnValue(Columns.BusinessHourAtmMaximum, value); }
        }

        [XmlAttribute("PageTitle")]
        [Bindable(true)]
        public string PageTitle
        {
            get { return GetColumnValue<string>(Columns.PageTitle); }
            set { SetColumnValue(Columns.PageTitle, value); }
        }

        [XmlAttribute("PageDesc")]
        [Bindable(true)]
        public string PageDesc
        {
            get { return GetColumnValue<string>(Columns.PageDesc); }
            set { SetColumnValue(Columns.PageDesc, value); }
        }

        [XmlAttribute("PicAlt")]
        [Bindable(true)]
        public string PicAlt
        {
            get { return GetColumnValue<string>(Columns.PicAlt); }
            set { SetColumnValue(Columns.PicAlt, value); }
        }

        [XmlAttribute("ChangedExpireDate")]
        [Bindable(true)]
        public DateTime? ChangedExpireDate
        {
            get { return GetColumnValue<DateTime?>(Columns.ChangedExpireDate); }
            set { SetColumnValue(Columns.ChangedExpireDate, value); }
        }

        [XmlAttribute("SettlementTime")]
        [Bindable(true)]
        public DateTime? SettlementTime
        {
            get { return GetColumnValue<DateTime?>(Columns.SettlementTime); }
            set { SetColumnValue(Columns.SettlementTime, value); }
        }

        [XmlAttribute("ExpireRedirectDisplay")]
        [Bindable(true)]
        public int? ExpireRedirectDisplay
        {
            get { return GetColumnValue<int?>(Columns.ExpireRedirectDisplay); }
            set { SetColumnValue(Columns.ExpireRedirectDisplay, value); }
        }

        [XmlAttribute("ExpireRedirectUrl")]
        [Bindable(true)]
        public string ExpireRedirectUrl
        {
            get { return GetColumnValue<string>(Columns.ExpireRedirectUrl); }
            set { SetColumnValue(Columns.ExpireRedirectUrl, value); }
        }

        [XmlAttribute("UseExpireRedirectUrlAsCanonical")]
        [Bindable(true)]
        public bool? UseExpireRedirectUrlAsCanonical
        {
            get { return GetColumnValue<bool?>(Columns.UseExpireRedirectUrlAsCanonical); }
            set { SetColumnValue(Columns.UseExpireRedirectUrlAsCanonical, value); }
        }

        [XmlAttribute("ExpireRedirectManual")]
        [Bindable(true)]
        public bool ExpireRedirectManual
        {
            get { return GetColumnValue<bool>(Columns.ExpireRedirectManual); }
            set { SetColumnValue(Columns.ExpireRedirectManual, value); }
        }

        [XmlAttribute("ExpireRedirectChangeDate")]
        [Bindable(true)]
        public DateTime? ExpireRedirectChangeDate
        {
            get { return GetColumnValue<DateTime?>(Columns.ExpireRedirectChangeDate); }
            set { SetColumnValue(Columns.ExpireRedirectChangeDate, value); }
        }

        #endregion




        //no foreign key tables defined (1)



        //no ManyToMany tables defined (2)





        #region Typed Columns


        public static TableSchema.TableColumn GuidColumn
        {
            get { return Schema.Columns[0]; }
        }



        public static TableSchema.TableColumn BusinessHourIdColumn
        {
            get { return Schema.Columns[1]; }
        }



        public static TableSchema.TableColumn BusinessHourTypeIdColumn
        {
            get { return Schema.Columns[2]; }
        }



        public static TableSchema.TableColumn SellerGuidColumn
        {
            get { return Schema.Columns[3]; }
        }



        public static TableSchema.TableColumn BusinessHourOrderTimeSColumn
        {
            get { return Schema.Columns[4]; }
        }



        public static TableSchema.TableColumn BusinessHourOrderTimeEColumn
        {
            get { return Schema.Columns[5]; }
        }



        public static TableSchema.TableColumn BusinessHourDeliverTimeSColumn
        {
            get { return Schema.Columns[6]; }
        }



        public static TableSchema.TableColumn BusinessHourDeliverTimeEColumn
        {
            get { return Schema.Columns[7]; }
        }



        public static TableSchema.TableColumn BusinessHourDeliveryChargeColumn
        {
            get { return Schema.Columns[8]; }
        }



        public static TableSchema.TableColumn BusinessHourOrderMinimumColumn
        {
            get { return Schema.Columns[9]; }
        }



        public static TableSchema.TableColumn BusinessHourPreparationTimeColumn
        {
            get { return Schema.Columns[10]; }
        }



        public static TableSchema.TableColumn BusinessHourOnlineColumn
        {
            get { return Schema.Columns[11]; }
        }



        public static TableSchema.TableColumn BusinessHourStatusColumn
        {
            get { return Schema.Columns[12]; }
        }



        public static TableSchema.TableColumn OrderTotalLimitColumn
        {
            get { return Schema.Columns[13]; }
        }



        public static TableSchema.TableColumn DeliveryLimitColumn
        {
            get { return Schema.Columns[14]; }
        }



        public static TableSchema.TableColumn HolidayColumn
        {
            get { return Schema.Columns[15]; }
        }



        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[16]; }
        }



        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[17]; }
        }



        public static TableSchema.TableColumn ModifyIdColumn
        {
            get { return Schema.Columns[18]; }
        }



        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[19]; }
        }



        public static TableSchema.TableColumn BusinessHourAtmMaximumColumn
        {
            get { return Schema.Columns[20]; }
        }



        public static TableSchema.TableColumn PageTitleColumn
        {
            get { return Schema.Columns[21]; }
        }



        public static TableSchema.TableColumn PageDescColumn
        {
            get { return Schema.Columns[22]; }
        }



        public static TableSchema.TableColumn PicAltColumn
        {
            get { return Schema.Columns[23]; }
        }



        public static TableSchema.TableColumn ChangedExpireDateColumn
        {
            get { return Schema.Columns[24]; }
        }



        public static TableSchema.TableColumn SettlementTimeColumn
        {
            get { return Schema.Columns[25]; }
        }



        public static TableSchema.TableColumn ExpireRedirectDisplayColumn
        {
            get { return Schema.Columns[26]; }
        }



        public static TableSchema.TableColumn ExpireRedirectUrlColumn
        {
            get { return Schema.Columns[27]; }
        }



        public static TableSchema.TableColumn UseExpireRedirectUrlAsCanonicalColumn
        {
            get { return Schema.Columns[28]; }
        }



        public static TableSchema.TableColumn ExpireRedirectManualColumn
        {
            get { return Schema.Columns[29]; }
        }



        public static TableSchema.TableColumn ExpireRedirectChangeDateColumn
        {
            get { return Schema.Columns[30]; }
        }



        #endregion
        #region Columns Struct
        public struct Columns
        {
            public static string Guid = @"GUID";
            public static string BusinessHourId = @"business_hour_id";
            public static string BusinessHourTypeId = @"business_hour_type_id";
            public static string SellerGuid = @"seller_GUID";
            public static string BusinessHourOrderTimeS = @"business_hour_order_time_s";
            public static string BusinessHourOrderTimeE = @"business_hour_order_time_e";
            public static string BusinessHourDeliverTimeS = @"business_hour_deliver_time_s";
            public static string BusinessHourDeliverTimeE = @"business_hour_deliver_time_e";
            public static string BusinessHourDeliveryCharge = @"business_hour_delivery_charge";
            public static string BusinessHourOrderMinimum = @"business_hour_order_minimum";
            public static string BusinessHourPreparationTime = @"business_hour_preparation_time";
            public static string BusinessHourOnline = @"business_hour_online";
            public static string BusinessHourStatus = @"business_hour_status";
            public static string OrderTotalLimit = @"order_total_limit";
            public static string DeliveryLimit = @"delivery_limit";
            public static string Holiday = @"holiday";
            public static string CreateId = @"create_id";
            public static string CreateTime = @"create_time";
            public static string ModifyId = @"modify_id";
            public static string ModifyTime = @"modify_time";
            public static string BusinessHourAtmMaximum = @"business_hour_atm_maximum";
            public static string PageTitle = @"page_title";
            public static string PageDesc = @"page_desc";
            public static string PicAlt = @"pic_alt";
            public static string ChangedExpireDate = @"changed_expire_date";
            public static string SettlementTime = @"settlement_time";
            public static string ExpireRedirectDisplay = @"expire_redirect_display";
            public static string ExpireRedirectUrl = @"expire_redirect_url";
            public static string UseExpireRedirectUrlAsCanonical = @"use_expire_redirect_url_as_canonical";
            public static string ExpireRedirectManual = @"expire_redirect_manual";
            public static string ExpireRedirectChangeDate = @"expire_redirect_change_date";

        }
        #endregion

        #region Update PK Collections

        #endregion

        #region Deep Save

        #endregion
    }
}

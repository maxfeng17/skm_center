using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the VourcherStore class.
	/// </summary>
    [Serializable]
	public partial class VourcherStoreCollection : RepositoryList<VourcherStore, VourcherStoreCollection>
	{	   
		public VourcherStoreCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>VourcherStoreCollection</returns>
		public VourcherStoreCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                VourcherStore o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the vourcher_store table.
	/// </summary>
	[Serializable]
	public partial class VourcherStore : RepositoryRecord<VourcherStore>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public VourcherStore()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public VourcherStore(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("vourcher_store", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarVourcherEventId = new TableSchema.TableColumn(schema);
				colvarVourcherEventId.ColumnName = "vourcher_event_id";
				colvarVourcherEventId.DataType = DbType.Int32;
				colvarVourcherEventId.MaxLength = 0;
				colvarVourcherEventId.AutoIncrement = false;
				colvarVourcherEventId.IsNullable = false;
				colvarVourcherEventId.IsPrimaryKey = false;
				colvarVourcherEventId.IsForeignKey = false;
				colvarVourcherEventId.IsReadOnly = false;
				colvarVourcherEventId.DefaultSetting = @"";
				colvarVourcherEventId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarVourcherEventId);
				
				TableSchema.TableColumn colvarSellerGuid = new TableSchema.TableColumn(schema);
				colvarSellerGuid.ColumnName = "seller_guid";
				colvarSellerGuid.DataType = DbType.Guid;
				colvarSellerGuid.MaxLength = 0;
				colvarSellerGuid.AutoIncrement = false;
				colvarSellerGuid.IsNullable = false;
				colvarSellerGuid.IsPrimaryKey = false;
				colvarSellerGuid.IsForeignKey = false;
				colvarSellerGuid.IsReadOnly = false;
				colvarSellerGuid.DefaultSetting = @"";
				colvarSellerGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSellerGuid);
				
				TableSchema.TableColumn colvarStoreGuid = new TableSchema.TableColumn(schema);
				colvarStoreGuid.ColumnName = "store_guid";
				colvarStoreGuid.DataType = DbType.Guid;
				colvarStoreGuid.MaxLength = 0;
				colvarStoreGuid.AutoIncrement = false;
				colvarStoreGuid.IsNullable = false;
				colvarStoreGuid.IsPrimaryKey = false;
				colvarStoreGuid.IsForeignKey = false;
				colvarStoreGuid.IsReadOnly = false;
				colvarStoreGuid.DefaultSetting = @"";
				colvarStoreGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStoreGuid);
				
				TableSchema.TableColumn colvarPageCount = new TableSchema.TableColumn(schema);
				colvarPageCount.ColumnName = "page_count";
				colvarPageCount.DataType = DbType.Int32;
				colvarPageCount.MaxLength = 0;
				colvarPageCount.AutoIncrement = false;
				colvarPageCount.IsNullable = false;
				colvarPageCount.IsPrimaryKey = false;
				colvarPageCount.IsForeignKey = false;
				colvarPageCount.IsReadOnly = false;
				
						colvarPageCount.DefaultSetting = @"((0))";
				colvarPageCount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPageCount);
				
				TableSchema.TableColumn colvarMaxQuantity = new TableSchema.TableColumn(schema);
				colvarMaxQuantity.ColumnName = "max_quantity";
				colvarMaxQuantity.DataType = DbType.Int32;
				colvarMaxQuantity.MaxLength = 0;
				colvarMaxQuantity.AutoIncrement = false;
				colvarMaxQuantity.IsNullable = false;
				colvarMaxQuantity.IsPrimaryKey = false;
				colvarMaxQuantity.IsForeignKey = false;
				colvarMaxQuantity.IsReadOnly = false;
				
						colvarMaxQuantity.DefaultSetting = @"((0))";
				colvarMaxQuantity.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMaxQuantity);
				
				TableSchema.TableColumn colvarCurrentQuantity = new TableSchema.TableColumn(schema);
				colvarCurrentQuantity.ColumnName = "current_quantity";
				colvarCurrentQuantity.DataType = DbType.Int32;
				colvarCurrentQuantity.MaxLength = 0;
				colvarCurrentQuantity.AutoIncrement = false;
				colvarCurrentQuantity.IsNullable = false;
				colvarCurrentQuantity.IsPrimaryKey = false;
				colvarCurrentQuantity.IsForeignKey = false;
				colvarCurrentQuantity.IsReadOnly = false;
				
						colvarCurrentQuantity.DefaultSetting = @"((0))";
				colvarCurrentQuantity.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCurrentQuantity);
				
				TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
				colvarCreateId.ColumnName = "create_id";
				colvarCreateId.DataType = DbType.AnsiString;
				colvarCreateId.MaxLength = 50;
				colvarCreateId.AutoIncrement = false;
				colvarCreateId.IsNullable = false;
				colvarCreateId.IsPrimaryKey = false;
				colvarCreateId.IsForeignKey = false;
				colvarCreateId.IsReadOnly = false;
				colvarCreateId.DefaultSetting = @"";
				colvarCreateId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateId);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("vourcher_store",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("VourcherEventId")]
		[Bindable(true)]
		public int VourcherEventId 
		{
			get { return GetColumnValue<int>(Columns.VourcherEventId); }
			set { SetColumnValue(Columns.VourcherEventId, value); }
		}
		  
		[XmlAttribute("SellerGuid")]
		[Bindable(true)]
		public Guid SellerGuid 
		{
			get { return GetColumnValue<Guid>(Columns.SellerGuid); }
			set { SetColumnValue(Columns.SellerGuid, value); }
		}
		  
		[XmlAttribute("StoreGuid")]
		[Bindable(true)]
		public Guid StoreGuid 
		{
			get { return GetColumnValue<Guid>(Columns.StoreGuid); }
			set { SetColumnValue(Columns.StoreGuid, value); }
		}
		  
		[XmlAttribute("PageCount")]
		[Bindable(true)]
		public int PageCount 
		{
			get { return GetColumnValue<int>(Columns.PageCount); }
			set { SetColumnValue(Columns.PageCount, value); }
		}
		  
		[XmlAttribute("MaxQuantity")]
		[Bindable(true)]
		public int MaxQuantity 
		{
			get { return GetColumnValue<int>(Columns.MaxQuantity); }
			set { SetColumnValue(Columns.MaxQuantity, value); }
		}
		  
		[XmlAttribute("CurrentQuantity")]
		[Bindable(true)]
		public int CurrentQuantity 
		{
			get { return GetColumnValue<int>(Columns.CurrentQuantity); }
			set { SetColumnValue(Columns.CurrentQuantity, value); }
		}
		  
		[XmlAttribute("CreateId")]
		[Bindable(true)]
		public string CreateId 
		{
			get { return GetColumnValue<string>(Columns.CreateId); }
			set { SetColumnValue(Columns.CreateId, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn VourcherEventIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn SellerGuidColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn StoreGuidColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn PageCountColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn MaxQuantityColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn CurrentQuantityColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string VourcherEventId = @"vourcher_event_id";
			 public static string SellerGuid = @"seller_guid";
			 public static string StoreGuid = @"store_guid";
			 public static string PageCount = @"page_count";
			 public static string MaxQuantity = @"max_quantity";
			 public static string CurrentQuantity = @"current_quantity";
			 public static string CreateId = @"create_id";
			 public static string CreateTime = @"create_time";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

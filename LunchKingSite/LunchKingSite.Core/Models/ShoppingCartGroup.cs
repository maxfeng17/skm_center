﻿using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the ShoppingCartGroup class.
    /// </summary>
    [Serializable]
    public partial class ShoppingCartGroupCollection : RepositoryList<ShoppingCartGroup, ShoppingCartGroupCollection>
    {
        public ShoppingCartGroupCollection() { }

        /// <summary>
        /// Filters an existing collection based on the set criteria. This is an in-memory filter
        /// Thanks to developingchris for this!
        /// </summary>
        /// <returns>ShoppingCartGroupCollection</returns>
        public ShoppingCartGroupCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                ShoppingCartGroup o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }


    }
    /// <summary>
    /// This is an ActiveRecord class which wraps the shopping_cart_group table.
    /// </summary>
    [Serializable]
    public partial class ShoppingCartGroup : RepositoryRecord<ShoppingCartGroup>, IRecordBase
    {
        #region .ctors and Default Settings

        public ShoppingCartGroup()
        {
            SetSQLProps();
            InitSetDefaults();
            MarkNew();
        }

        private void InitSetDefaults() { SetDefaults(); }

        public ShoppingCartGroup(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
                ForceDefaults();
            MarkNew();
        }

        protected static void SetSQLProps() { GetTableSchema(); }

        #endregion

        #region Schema and Query Accessor	
        public static Query CreateQuery() { return new Query(Schema); }
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                    SetSQLProps();
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("shopping_cart_group", TableType.Table, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarGuid = new TableSchema.TableColumn(schema);
                colvarGuid.ColumnName = "guid";
                colvarGuid.DataType = DbType.Guid;
                colvarGuid.MaxLength = 0;
                colvarGuid.AutoIncrement = false;
                colvarGuid.IsNullable = false;
                colvarGuid.IsPrimaryKey = true;
                colvarGuid.IsForeignKey = false;
                colvarGuid.IsReadOnly = false;
                colvarGuid.DefaultSetting = @"";
                colvarGuid.ForeignKeyTableName = "";
                schema.Columns.Add(colvarGuid);

                TableSchema.TableColumn colvarCartStatus = new TableSchema.TableColumn(schema);
                colvarCartStatus.ColumnName = "cart_status";
                colvarCartStatus.DataType = DbType.Int32;
                colvarCartStatus.MaxLength = 0;
                colvarCartStatus.AutoIncrement = false;
                colvarCartStatus.IsNullable = false;
                colvarCartStatus.IsPrimaryKey = false;
                colvarCartStatus.IsForeignKey = false;
                colvarCartStatus.IsReadOnly = false;
                colvarCartStatus.DefaultSetting = @"";
                colvarCartStatus.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCartStatus);

                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = false;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                colvarCreateTime.DefaultSetting = @"";
                colvarCreateTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateTime);

                TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
                colvarCreateId.ColumnName = "create_id";
                colvarCreateId.DataType = DbType.Int32;
                colvarCreateId.MaxLength = 0;
                colvarCreateId.AutoIncrement = false;
                colvarCreateId.IsNullable = false;
                colvarCreateId.IsPrimaryKey = false;
                colvarCreateId.IsForeignKey = false;
                colvarCreateId.IsReadOnly = false;
                colvarCreateId.DefaultSetting = @"";
                colvarCreateId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateId);

                TableSchema.TableColumn colvarCartId = new TableSchema.TableColumn(schema);
                colvarCartId.ColumnName = "cart_id";
                colvarCartId.DataType = DbType.AnsiString;
                colvarCartId.MaxLength = 50;
                colvarCartId.AutoIncrement = false;
                colvarCartId.IsNullable = false;
                colvarCartId.IsPrimaryKey = false;
                colvarCartId.IsForeignKey = false;
                colvarCartId.IsReadOnly = false;

                colvarCartId.DefaultSetting = @"('')";
                colvarCartId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCartId);

                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("shopping_cart_group", schema);
            }
        }
        #endregion

        #region Props

        [XmlAttribute("Guid")]
        [Bindable(true)]
        public Guid Guid
        {
            get { return GetColumnValue<Guid>(Columns.Guid); }
            set { SetColumnValue(Columns.Guid, value); }
        }

        [XmlAttribute("CartStatus")]
        [Bindable(true)]
        public int CartStatus
        {
            get { return GetColumnValue<int>(Columns.CartStatus); }
            set { SetColumnValue(Columns.CartStatus, value); }
        }

        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime CreateTime
        {
            get { return GetColumnValue<DateTime>(Columns.CreateTime); }
            set { SetColumnValue(Columns.CreateTime, value); }
        }

        [XmlAttribute("CreateId")]
        [Bindable(true)]
        public int CreateId
        {
            get { return GetColumnValue<int>(Columns.CreateId); }
            set { SetColumnValue(Columns.CreateId, value); }
        }

        [XmlAttribute("CartId")]
        [Bindable(true)]
        public string CartId
        {
            get { return GetColumnValue<string>(Columns.CartId); }
            set { SetColumnValue(Columns.CartId, value); }
        }

        #endregion




        //no foreign key tables defined (0)



        //no ManyToMany tables defined (0)





        #region Typed Columns


        public static TableSchema.TableColumn GuidColumn
        {
            get { return Schema.Columns[0]; }
        }



        public static TableSchema.TableColumn CartStatusColumn
        {
            get { return Schema.Columns[1]; }
        }



        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[2]; }
        }



        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[3]; }
        }



        public static TableSchema.TableColumn CartIdColumn
        {
            get { return Schema.Columns[4]; }
        }



        #endregion
        #region Columns Struct
        public struct Columns
        {
            public static string Guid = @"guid";
            public static string CartStatus = @"cart_status";
            public static string CreateTime = @"create_time";
            public static string CreateId = @"create_id";
            public static string CartId = @"cart_id";

        }
        #endregion

        #region Update PK Collections

        #endregion

        #region Deep Save

        #endregion
    }
}

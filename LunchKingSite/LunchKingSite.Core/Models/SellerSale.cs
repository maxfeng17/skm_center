using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the SellerSale class.
	/// </summary>
    [Serializable]
	public partial class SellerSaleCollection : RepositoryList<SellerSale, SellerSaleCollection>
	{	   
		public SellerSaleCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>SellerSaleCollection</returns>
		public SellerSaleCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                SellerSale o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the seller_sales table.
	/// </summary>
	[Serializable]
	public partial class SellerSale : RepositoryRecord<SellerSale>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public SellerSale()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public SellerSale(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("seller_sales", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarSellerGuid = new TableSchema.TableColumn(schema);
				colvarSellerGuid.ColumnName = "seller_GUID";
				colvarSellerGuid.DataType = DbType.Guid;
				colvarSellerGuid.MaxLength = 0;
				colvarSellerGuid.AutoIncrement = false;
				colvarSellerGuid.IsNullable = false;
				colvarSellerGuid.IsPrimaryKey = false;
				colvarSellerGuid.IsForeignKey = false;
				colvarSellerGuid.IsReadOnly = false;
				colvarSellerGuid.DefaultSetting = @"";
				colvarSellerGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSellerGuid);
				
				TableSchema.TableColumn colvarSellerSalesId = new TableSchema.TableColumn(schema);
				colvarSellerSalesId.ColumnName = "seller_sales_id";
				colvarSellerSalesId.DataType = DbType.Int32;
				colvarSellerSalesId.MaxLength = 0;
				colvarSellerSalesId.AutoIncrement = false;
				colvarSellerSalesId.IsNullable = false;
				colvarSellerSalesId.IsPrimaryKey = false;
				colvarSellerSalesId.IsForeignKey = false;
				colvarSellerSalesId.IsReadOnly = false;
				colvarSellerSalesId.DefaultSetting = @"";
				colvarSellerSalesId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSellerSalesId);
				
				TableSchema.TableColumn colvarSalesType = new TableSchema.TableColumn(schema);
				colvarSalesType.ColumnName = "sales_type";
				colvarSalesType.DataType = DbType.Int32;
				colvarSalesType.MaxLength = 0;
				colvarSalesType.AutoIncrement = false;
				colvarSalesType.IsNullable = false;
				colvarSalesType.IsPrimaryKey = false;
				colvarSalesType.IsForeignKey = false;
				colvarSalesType.IsReadOnly = false;
				colvarSalesType.DefaultSetting = @"";
				colvarSalesType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSalesType);
				
				TableSchema.TableColumn colvarSalesGroup = new TableSchema.TableColumn(schema);
				colvarSalesGroup.ColumnName = "sales_group";
				colvarSalesGroup.DataType = DbType.Int32;
				colvarSalesGroup.MaxLength = 0;
				colvarSalesGroup.AutoIncrement = false;
				colvarSalesGroup.IsNullable = false;
				colvarSalesGroup.IsPrimaryKey = false;
				colvarSalesGroup.IsForeignKey = false;
				colvarSalesGroup.IsReadOnly = false;
				colvarSalesGroup.DefaultSetting = @"";
				colvarSalesGroup.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSalesGroup);
				
				TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
				colvarCreateId.ColumnName = "create_id";
				colvarCreateId.DataType = DbType.String;
				colvarCreateId.MaxLength = 30;
				colvarCreateId.AutoIncrement = false;
				colvarCreateId.IsNullable = false;
				colvarCreateId.IsPrimaryKey = false;
				colvarCreateId.IsForeignKey = false;
				colvarCreateId.IsReadOnly = false;
				colvarCreateId.DefaultSetting = @"";
				colvarCreateId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateId);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("seller_sales",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("SellerGuid")]
		[Bindable(true)]
		public Guid SellerGuid 
		{
			get { return GetColumnValue<Guid>(Columns.SellerGuid); }
			set { SetColumnValue(Columns.SellerGuid, value); }
		}
		  
		[XmlAttribute("SellerSalesId")]
		[Bindable(true)]
		public int SellerSalesId 
		{
			get { return GetColumnValue<int>(Columns.SellerSalesId); }
			set { SetColumnValue(Columns.SellerSalesId, value); }
		}
		  
		[XmlAttribute("SalesType")]
		[Bindable(true)]
		public int SalesType 
		{
			get { return GetColumnValue<int>(Columns.SalesType); }
			set { SetColumnValue(Columns.SalesType, value); }
		}
		  
		[XmlAttribute("SalesGroup")]
		[Bindable(true)]
		public int SalesGroup 
		{
			get { return GetColumnValue<int>(Columns.SalesGroup); }
			set { SetColumnValue(Columns.SalesGroup, value); }
		}
		  
		[XmlAttribute("CreateId")]
		[Bindable(true)]
		public string CreateId 
		{
			get { return GetColumnValue<string>(Columns.CreateId); }
			set { SetColumnValue(Columns.CreateId, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn SellerGuidColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn SellerSalesIdColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn SalesTypeColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn SalesGroupColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string SellerGuid = @"seller_GUID";
			 public static string SellerSalesId = @"seller_sales_id";
			 public static string SalesType = @"sales_type";
			 public static string SalesGroup = @"sales_group";
			 public static string CreateId = @"create_id";
			 public static string CreateTime = @"create_time";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the WeeklyPayAccount class.
	/// </summary>
    [Serializable]
	public partial class WeeklyPayAccountCollection : RepositoryList<WeeklyPayAccount, WeeklyPayAccountCollection>
	{	   
		public WeeklyPayAccountCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>WeeklyPayAccountCollection</returns>
		public WeeklyPayAccountCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                WeeklyPayAccount o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the weekly_pay_account table.
	/// </summary>
	[Serializable]
	public partial class WeeklyPayAccount : RepositoryRecord<WeeklyPayAccount>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public WeeklyPayAccount()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public WeeklyPayAccount(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("weekly_pay_account", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarBusinessHourGuid = new TableSchema.TableColumn(schema);
				colvarBusinessHourGuid.ColumnName = "business_hour_guid";
				colvarBusinessHourGuid.DataType = DbType.Guid;
				colvarBusinessHourGuid.MaxLength = 0;
				colvarBusinessHourGuid.AutoIncrement = false;
				colvarBusinessHourGuid.IsNullable = false;
				colvarBusinessHourGuid.IsPrimaryKey = false;
				colvarBusinessHourGuid.IsForeignKey = false;
				colvarBusinessHourGuid.IsReadOnly = false;
				colvarBusinessHourGuid.DefaultSetting = @"";
				colvarBusinessHourGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBusinessHourGuid);
				
				TableSchema.TableColumn colvarEventName = new TableSchema.TableColumn(schema);
				colvarEventName.ColumnName = "event_name";
				colvarEventName.DataType = DbType.String;
				colvarEventName.MaxLength = 500;
				colvarEventName.AutoIncrement = false;
				colvarEventName.IsNullable = true;
				colvarEventName.IsPrimaryKey = false;
				colvarEventName.IsForeignKey = false;
				colvarEventName.IsReadOnly = false;
				colvarEventName.DefaultSetting = @"";
				colvarEventName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEventName);
				
				TableSchema.TableColumn colvarCompanyName = new TableSchema.TableColumn(schema);
				colvarCompanyName.ColumnName = "company_name";
				colvarCompanyName.DataType = DbType.String;
				colvarCompanyName.MaxLength = 200;
				colvarCompanyName.AutoIncrement = false;
				colvarCompanyName.IsNullable = false;
				colvarCompanyName.IsPrimaryKey = false;
				colvarCompanyName.IsForeignKey = false;
				colvarCompanyName.IsReadOnly = false;
				colvarCompanyName.DefaultSetting = @"";
				colvarCompanyName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCompanyName);
				
				TableSchema.TableColumn colvarSellerName = new TableSchema.TableColumn(schema);
				colvarSellerName.ColumnName = "seller_name";
				colvarSellerName.DataType = DbType.String;
				colvarSellerName.MaxLength = 100;
				colvarSellerName.AutoIncrement = false;
				colvarSellerName.IsNullable = false;
				colvarSellerName.IsPrimaryKey = false;
				colvarSellerName.IsForeignKey = false;
				colvarSellerName.IsReadOnly = false;
				colvarSellerName.DefaultSetting = @"";
				colvarSellerName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSellerName);
				
				TableSchema.TableColumn colvarAccountName = new TableSchema.TableColumn(schema);
				colvarAccountName.ColumnName = "account_name";
				colvarAccountName.DataType = DbType.String;
				colvarAccountName.MaxLength = 100;
				colvarAccountName.AutoIncrement = false;
				colvarAccountName.IsNullable = false;
				colvarAccountName.IsPrimaryKey = false;
				colvarAccountName.IsForeignKey = false;
				colvarAccountName.IsReadOnly = false;
				colvarAccountName.DefaultSetting = @"";
				colvarAccountName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAccountName);
				
				TableSchema.TableColumn colvarAccountId = new TableSchema.TableColumn(schema);
				colvarAccountId.ColumnName = "account_id";
				colvarAccountId.DataType = DbType.AnsiString;
				colvarAccountId.MaxLength = 10;
				colvarAccountId.AutoIncrement = false;
				colvarAccountId.IsNullable = false;
				colvarAccountId.IsPrimaryKey = false;
				colvarAccountId.IsForeignKey = false;
				colvarAccountId.IsReadOnly = false;
				colvarAccountId.DefaultSetting = @"";
				colvarAccountId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAccountId);
				
				TableSchema.TableColumn colvarBankNo = new TableSchema.TableColumn(schema);
				colvarBankNo.ColumnName = "bank_no";
				colvarBankNo.DataType = DbType.AnsiString;
				colvarBankNo.MaxLength = 20;
				colvarBankNo.AutoIncrement = false;
				colvarBankNo.IsNullable = false;
				colvarBankNo.IsPrimaryKey = false;
				colvarBankNo.IsForeignKey = false;
				colvarBankNo.IsReadOnly = false;
				colvarBankNo.DefaultSetting = @"";
				colvarBankNo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBankNo);
				
				TableSchema.TableColumn colvarBranchNo = new TableSchema.TableColumn(schema);
				colvarBranchNo.ColumnName = "branch_no";
				colvarBranchNo.DataType = DbType.AnsiString;
				colvarBranchNo.MaxLength = 20;
				colvarBranchNo.AutoIncrement = false;
				colvarBranchNo.IsNullable = false;
				colvarBranchNo.IsPrimaryKey = false;
				colvarBranchNo.IsForeignKey = false;
				colvarBranchNo.IsReadOnly = false;
				colvarBranchNo.DefaultSetting = @"";
				colvarBranchNo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBranchNo);
				
				TableSchema.TableColumn colvarAccountNo = new TableSchema.TableColumn(schema);
				colvarAccountNo.ColumnName = "account_no";
				colvarAccountNo.DataType = DbType.AnsiString;
				colvarAccountNo.MaxLength = 50;
				colvarAccountNo.AutoIncrement = false;
				colvarAccountNo.IsNullable = false;
				colvarAccountNo.IsPrimaryKey = false;
				colvarAccountNo.IsForeignKey = false;
				colvarAccountNo.IsReadOnly = false;
				colvarAccountNo.DefaultSetting = @"";
				colvarAccountNo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAccountNo);
				
				TableSchema.TableColumn colvarEmail = new TableSchema.TableColumn(schema);
				colvarEmail.ColumnName = "email";
				colvarEmail.DataType = DbType.String;
				colvarEmail.MaxLength = 200;
				colvarEmail.AutoIncrement = false;
				colvarEmail.IsNullable = true;
				colvarEmail.IsPrimaryKey = false;
				colvarEmail.IsForeignKey = false;
				colvarEmail.IsReadOnly = false;
				colvarEmail.DefaultSetting = @"";
				colvarEmail.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEmail);
				
				TableSchema.TableColumn colvarCreateDate = new TableSchema.TableColumn(schema);
				colvarCreateDate.ColumnName = "create_date";
				colvarCreateDate.DataType = DbType.DateTime;
				colvarCreateDate.MaxLength = 0;
				colvarCreateDate.AutoIncrement = false;
				colvarCreateDate.IsNullable = false;
				colvarCreateDate.IsPrimaryKey = false;
				colvarCreateDate.IsForeignKey = false;
				colvarCreateDate.IsReadOnly = false;
				colvarCreateDate.DefaultSetting = @"";
				colvarCreateDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateDate);
				
				TableSchema.TableColumn colvarCreator = new TableSchema.TableColumn(schema);
				colvarCreator.ColumnName = "creator";
				colvarCreator.DataType = DbType.String;
				colvarCreator.MaxLength = 50;
				colvarCreator.AutoIncrement = false;
				colvarCreator.IsNullable = false;
				colvarCreator.IsPrimaryKey = false;
				colvarCreator.IsForeignKey = false;
				colvarCreator.IsReadOnly = false;
				colvarCreator.DefaultSetting = @"";
				colvarCreator.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreator);
				
				TableSchema.TableColumn colvarMessage = new TableSchema.TableColumn(schema);
				colvarMessage.ColumnName = "message";
				colvarMessage.DataType = DbType.String;
				colvarMessage.MaxLength = 1000;
				colvarMessage.AutoIncrement = false;
				colvarMessage.IsNullable = true;
				colvarMessage.IsPrimaryKey = false;
				colvarMessage.IsForeignKey = false;
				colvarMessage.IsReadOnly = false;
				colvarMessage.DefaultSetting = @"";
				colvarMessage.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMessage);
				
				TableSchema.TableColumn colvarSignCompanyID = new TableSchema.TableColumn(schema);
				colvarSignCompanyID.ColumnName = "SignCompanyID";
				colvarSignCompanyID.DataType = DbType.AnsiString;
				colvarSignCompanyID.MaxLength = 20;
				colvarSignCompanyID.AutoIncrement = false;
				colvarSignCompanyID.IsNullable = true;
				colvarSignCompanyID.IsPrimaryKey = false;
				colvarSignCompanyID.IsForeignKey = false;
				colvarSignCompanyID.IsReadOnly = false;
				colvarSignCompanyID.DefaultSetting = @"";
				colvarSignCompanyID.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSignCompanyID);
				
				TableSchema.TableColumn colvarAccountantName = new TableSchema.TableColumn(schema);
				colvarAccountantName.ColumnName = "accountant_name";
				colvarAccountantName.DataType = DbType.String;
				colvarAccountantName.MaxLength = 20;
				colvarAccountantName.AutoIncrement = false;
				colvarAccountantName.IsNullable = true;
				colvarAccountantName.IsPrimaryKey = false;
				colvarAccountantName.IsForeignKey = false;
				colvarAccountantName.IsReadOnly = false;
				colvarAccountantName.DefaultSetting = @"";
				colvarAccountantName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAccountantName);
				
				TableSchema.TableColumn colvarAccountantTel = new TableSchema.TableColumn(schema);
				colvarAccountantTel.ColumnName = "accountant_tel";
				colvarAccountantTel.DataType = DbType.AnsiString;
				colvarAccountantTel.MaxLength = 20;
				colvarAccountantTel.AutoIncrement = false;
				colvarAccountantTel.IsNullable = true;
				colvarAccountantTel.IsPrimaryKey = false;
				colvarAccountantTel.IsForeignKey = false;
				colvarAccountantTel.IsReadOnly = false;
				colvarAccountantTel.DefaultSetting = @"";
				colvarAccountantTel.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAccountantTel);
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarStoreGuid = new TableSchema.TableColumn(schema);
				colvarStoreGuid.ColumnName = "store_guid";
				colvarStoreGuid.DataType = DbType.Guid;
				colvarStoreGuid.MaxLength = 0;
				colvarStoreGuid.AutoIncrement = false;
				colvarStoreGuid.IsNullable = true;
				colvarStoreGuid.IsPrimaryKey = false;
				colvarStoreGuid.IsForeignKey = false;
				colvarStoreGuid.IsReadOnly = false;
				colvarStoreGuid.DefaultSetting = @"";
				colvarStoreGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStoreGuid);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("weekly_pay_account",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("BusinessHourGuid")]
		[Bindable(true)]
		public Guid BusinessHourGuid 
		{
			get { return GetColumnValue<Guid>(Columns.BusinessHourGuid); }
			set { SetColumnValue(Columns.BusinessHourGuid, value); }
		}
		  
		[XmlAttribute("EventName")]
		[Bindable(true)]
		public string EventName 
		{
			get { return GetColumnValue<string>(Columns.EventName); }
			set { SetColumnValue(Columns.EventName, value); }
		}
		  
		[XmlAttribute("CompanyName")]
		[Bindable(true)]
		public string CompanyName 
		{
			get { return GetColumnValue<string>(Columns.CompanyName); }
			set { SetColumnValue(Columns.CompanyName, value); }
		}
		  
		[XmlAttribute("SellerName")]
		[Bindable(true)]
		public string SellerName 
		{
			get { return GetColumnValue<string>(Columns.SellerName); }
			set { SetColumnValue(Columns.SellerName, value); }
		}
		  
		[XmlAttribute("AccountName")]
		[Bindable(true)]
		public string AccountName 
		{
			get { return GetColumnValue<string>(Columns.AccountName); }
			set { SetColumnValue(Columns.AccountName, value); }
		}
		  
		[XmlAttribute("AccountId")]
		[Bindable(true)]
		public string AccountId 
		{
			get { return GetColumnValue<string>(Columns.AccountId); }
			set { SetColumnValue(Columns.AccountId, value); }
		}
		  
		[XmlAttribute("BankNo")]
		[Bindable(true)]
		public string BankNo 
		{
			get { return GetColumnValue<string>(Columns.BankNo); }
			set { SetColumnValue(Columns.BankNo, value); }
		}
		  
		[XmlAttribute("BranchNo")]
		[Bindable(true)]
		public string BranchNo 
		{
			get { return GetColumnValue<string>(Columns.BranchNo); }
			set { SetColumnValue(Columns.BranchNo, value); }
		}
		  
		[XmlAttribute("AccountNo")]
		[Bindable(true)]
		public string AccountNo 
		{
			get { return GetColumnValue<string>(Columns.AccountNo); }
			set { SetColumnValue(Columns.AccountNo, value); }
		}
		  
		[XmlAttribute("Email")]
		[Bindable(true)]
		public string Email 
		{
			get { return GetColumnValue<string>(Columns.Email); }
			set { SetColumnValue(Columns.Email, value); }
		}
		  
		[XmlAttribute("CreateDate")]
		[Bindable(true)]
		public DateTime CreateDate 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateDate); }
			set { SetColumnValue(Columns.CreateDate, value); }
		}
		  
		[XmlAttribute("Creator")]
		[Bindable(true)]
		public string Creator 
		{
			get { return GetColumnValue<string>(Columns.Creator); }
			set { SetColumnValue(Columns.Creator, value); }
		}
		  
		[XmlAttribute("Message")]
		[Bindable(true)]
		public string Message 
		{
			get { return GetColumnValue<string>(Columns.Message); }
			set { SetColumnValue(Columns.Message, value); }
		}
		  
		[XmlAttribute("SignCompanyID")]
		[Bindable(true)]
		public string SignCompanyID 
		{
			get { return GetColumnValue<string>(Columns.SignCompanyID); }
			set { SetColumnValue(Columns.SignCompanyID, value); }
		}
		  
		[XmlAttribute("AccountantName")]
		[Bindable(true)]
		public string AccountantName 
		{
			get { return GetColumnValue<string>(Columns.AccountantName); }
			set { SetColumnValue(Columns.AccountantName, value); }
		}
		  
		[XmlAttribute("AccountantTel")]
		[Bindable(true)]
		public string AccountantTel 
		{
			get { return GetColumnValue<string>(Columns.AccountantTel); }
			set { SetColumnValue(Columns.AccountantTel, value); }
		}
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("StoreGuid")]
		[Bindable(true)]
		public Guid? StoreGuid 
		{
			get { return GetColumnValue<Guid?>(Columns.StoreGuid); }
			set { SetColumnValue(Columns.StoreGuid, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn BusinessHourGuidColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn EventNameColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn CompanyNameColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn SellerNameColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn AccountNameColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn AccountIdColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn BankNoColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn BranchNoColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn AccountNoColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn EmailColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateDateColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        public static TableSchema.TableColumn CreatorColumn
        {
            get { return Schema.Columns[11]; }
        }
        
        
        
        public static TableSchema.TableColumn MessageColumn
        {
            get { return Schema.Columns[12]; }
        }
        
        
        
        public static TableSchema.TableColumn SignCompanyIDColumn
        {
            get { return Schema.Columns[13]; }
        }
        
        
        
        public static TableSchema.TableColumn AccountantNameColumn
        {
            get { return Schema.Columns[14]; }
        }
        
        
        
        public static TableSchema.TableColumn AccountantTelColumn
        {
            get { return Schema.Columns[15]; }
        }
        
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[16]; }
        }
        
        
        
        public static TableSchema.TableColumn StoreGuidColumn
        {
            get { return Schema.Columns[17]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string BusinessHourGuid = @"business_hour_guid";
			 public static string EventName = @"event_name";
			 public static string CompanyName = @"company_name";
			 public static string SellerName = @"seller_name";
			 public static string AccountName = @"account_name";
			 public static string AccountId = @"account_id";
			 public static string BankNo = @"bank_no";
			 public static string BranchNo = @"branch_no";
			 public static string AccountNo = @"account_no";
			 public static string Email = @"email";
			 public static string CreateDate = @"create_date";
			 public static string Creator = @"creator";
			 public static string Message = @"message";
			 public static string SignCompanyID = @"SignCompanyID";
			 public static string AccountantName = @"accountant_name";
			 public static string AccountantTel = @"accountant_tel";
			 public static string Id = @"id";
			 public static string StoreGuid = @"store_guid";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the Faq class.
	/// </summary>
    [Serializable]
	public partial class FaqCollection : RepositoryList<Faq, FaqCollection>
	{	   
		public FaqCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>FaqCollection</returns>
		public FaqCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                Faq o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the faq table.
	/// </summary>
	[Serializable]
	public partial class Faq : RepositoryRecord<Faq>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public Faq()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public Faq(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("faq", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarStage = new TableSchema.TableColumn(schema);
				colvarStage.ColumnName = "stage";
				colvarStage.DataType = DbType.Int32;
				colvarStage.MaxLength = 0;
				colvarStage.AutoIncrement = false;
				colvarStage.IsNullable = true;
				colvarStage.IsPrimaryKey = false;
				colvarStage.IsForeignKey = false;
				colvarStage.IsReadOnly = false;
				colvarStage.DefaultSetting = @"";
				colvarStage.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStage);
				
				TableSchema.TableColumn colvarSequence = new TableSchema.TableColumn(schema);
				colvarSequence.ColumnName = "sequence";
				colvarSequence.DataType = DbType.Int32;
				colvarSequence.MaxLength = 0;
				colvarSequence.AutoIncrement = false;
				colvarSequence.IsNullable = false;
				colvarSequence.IsPrimaryKey = false;
				colvarSequence.IsForeignKey = false;
				colvarSequence.IsReadOnly = false;
				colvarSequence.DefaultSetting = @"";
				colvarSequence.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSequence);
				
				TableSchema.TableColumn colvarPid = new TableSchema.TableColumn(schema);
				colvarPid.ColumnName = "pid";
				colvarPid.DataType = DbType.Int32;
				colvarPid.MaxLength = 0;
				colvarPid.AutoIncrement = false;
				colvarPid.IsNullable = false;
				colvarPid.IsPrimaryKey = false;
				colvarPid.IsForeignKey = false;
				colvarPid.IsReadOnly = false;
				colvarPid.DefaultSetting = @"";
				colvarPid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPid);
				
				TableSchema.TableColumn colvarContents = new TableSchema.TableColumn(schema);
				colvarContents.ColumnName = "contents";
				colvarContents.DataType = DbType.String;
				colvarContents.MaxLength = 1073741823;
				colvarContents.AutoIncrement = false;
				colvarContents.IsNullable = false;
				colvarContents.IsPrimaryKey = false;
				colvarContents.IsForeignKey = false;
				colvarContents.IsReadOnly = false;
				colvarContents.DefaultSetting = @"";
				colvarContents.ForeignKeyTableName = "";
				schema.Columns.Add(colvarContents);
				
				TableSchema.TableColumn colvarMark = new TableSchema.TableColumn(schema);
				colvarMark.ColumnName = "mark";
				colvarMark.DataType = DbType.AnsiString;
				colvarMark.MaxLength = 50;
				colvarMark.AutoIncrement = false;
				colvarMark.IsNullable = true;
				colvarMark.IsPrimaryKey = false;
				colvarMark.IsForeignKey = false;
				colvarMark.IsReadOnly = false;
				colvarMark.DefaultSetting = @"";
				colvarMark.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMark);
				
				TableSchema.TableColumn colvarCreator = new TableSchema.TableColumn(schema);
				colvarCreator.ColumnName = "creator";
				colvarCreator.DataType = DbType.AnsiString;
				colvarCreator.MaxLength = 50;
				colvarCreator.AutoIncrement = false;
				colvarCreator.IsNullable = false;
				colvarCreator.IsPrimaryKey = false;
				colvarCreator.IsForeignKey = false;
				colvarCreator.IsReadOnly = false;
				colvarCreator.DefaultSetting = @"";
				colvarCreator.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreator);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
				colvarStatus.ColumnName = "status";
				colvarStatus.DataType = DbType.Boolean;
				colvarStatus.MaxLength = 0;
				colvarStatus.AutoIncrement = false;
				colvarStatus.IsNullable = false;
				colvarStatus.IsPrimaryKey = false;
				colvarStatus.IsForeignKey = false;
				colvarStatus.IsReadOnly = false;
				
						colvarStatus.DefaultSetting = @"((1))";
				colvarStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStatus);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("faq",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		
		
		//private int? propStage;
		[XmlAttribute("Stage")]
		[Bindable(true)]
		public int Stage 
		{
			get
			{
			int? propStage = GetColumnValue<int?>(Columns.Stage);
				if (!propStage.HasValue)
					return 0;
				return propStage.Value;
			}
		   set { SetColumnValue(Columns.Stage, value); }
		}
		
		[XmlIgnore]
		public bool StageHasValue
		{
			get { return GetColumnValue<int?>("stage") != null; }
			set
			{
				int? propStage = GetColumnValue<int?>(Columns.Stage);
				if (!value)
					SetColumnValue(Columns.Stage, null);
				else if (value && !propStage.HasValue)
					SetColumnValue(Columns.Stage, 0);
		   }
		}
		
		  
		[XmlAttribute("Sequence")]
		[Bindable(true)]
		public int Sequence 
		{
			get { return GetColumnValue<int>(Columns.Sequence); }
			set { SetColumnValue(Columns.Sequence, value); }
		}
		  
		[XmlAttribute("Pid")]
		[Bindable(true)]
		public int Pid 
		{
			get { return GetColumnValue<int>(Columns.Pid); }
			set { SetColumnValue(Columns.Pid, value); }
		}
		  
		[XmlAttribute("Contents")]
		[Bindable(true)]
		public string Contents 
		{
			get { return GetColumnValue<string>(Columns.Contents); }
			set { SetColumnValue(Columns.Contents, value); }
		}
		  
		[XmlAttribute("Mark")]
		[Bindable(true)]
		public string Mark 
		{
			get { return GetColumnValue<string>(Columns.Mark); }
			set { SetColumnValue(Columns.Mark, value); }
		}
		  
		[XmlAttribute("Creator")]
		[Bindable(true)]
		public string Creator 
		{
			get { return GetColumnValue<string>(Columns.Creator); }
			set { SetColumnValue(Columns.Creator, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		  
		[XmlAttribute("Status")]
		[Bindable(true)]
		public bool Status 
		{
			get { return GetColumnValue<bool>(Columns.Status); }
			set { SetColumnValue(Columns.Status, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn StageColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn SequenceColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn PidColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn ContentsColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn MarkColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn CreatorColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn StatusColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string Stage = @"stage";
			 public static string Sequence = @"sequence";
			 public static string Pid = @"pid";
			 public static string Contents = @"contents";
			 public static string Mark = @"mark";
			 public static string Creator = @"creator";
			 public static string CreateTime = @"create_time";
			 public static string Status = @"status";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

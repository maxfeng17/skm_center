using System;
using System.ComponentModel;
using System.Data;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the ViewAccessoryGroupCategory class.
    /// </summary>
    [Serializable]
    public partial class ViewAccessoryGroupCategoryCollection : ReadOnlyList<ViewAccessoryGroupCategory, ViewAccessoryGroupCategoryCollection>
    {        
        public ViewAccessoryGroupCategoryCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_accessory_group_category view.
    /// </summary>
    [Serializable]
    public partial class ViewAccessoryGroupCategory : ReadOnlyRecord<ViewAccessoryGroupCategory>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_accessory_group_category", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarAccessoryGroupId = new TableSchema.TableColumn(schema);
                colvarAccessoryGroupId.ColumnName = "accessory_group_id";
                colvarAccessoryGroupId.DataType = DbType.AnsiString;
                colvarAccessoryGroupId.MaxLength = 20;
                colvarAccessoryGroupId.AutoIncrement = false;
                colvarAccessoryGroupId.IsNullable = true;
                colvarAccessoryGroupId.IsPrimaryKey = false;
                colvarAccessoryGroupId.IsForeignKey = false;
                colvarAccessoryGroupId.IsReadOnly = false;
                
                schema.Columns.Add(colvarAccessoryGroupId);
                
                TableSchema.TableColumn colvarAccessoryGroupName = new TableSchema.TableColumn(schema);
                colvarAccessoryGroupName.ColumnName = "accessory_group_name";
                colvarAccessoryGroupName.DataType = DbType.String;
                colvarAccessoryGroupName.MaxLength = 50;
                colvarAccessoryGroupName.AutoIncrement = false;
                colvarAccessoryGroupName.IsNullable = true;
                colvarAccessoryGroupName.IsPrimaryKey = false;
                colvarAccessoryGroupName.IsForeignKey = false;
                colvarAccessoryGroupName.IsReadOnly = false;
                
                schema.Columns.Add(colvarAccessoryGroupName);
                
                TableSchema.TableColumn colvarSellerGuid = new TableSchema.TableColumn(schema);
                colvarSellerGuid.ColumnName = "seller_GUID";
                colvarSellerGuid.DataType = DbType.Guid;
                colvarSellerGuid.MaxLength = 0;
                colvarSellerGuid.AutoIncrement = false;
                colvarSellerGuid.IsNullable = true;
                colvarSellerGuid.IsPrimaryKey = false;
                colvarSellerGuid.IsForeignKey = false;
                colvarSellerGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerGuid);
                
                TableSchema.TableColumn colvarAccessoryGroupCreateId = new TableSchema.TableColumn(schema);
                colvarAccessoryGroupCreateId.ColumnName = "accessory_group_create_id";
                colvarAccessoryGroupCreateId.DataType = DbType.String;
                colvarAccessoryGroupCreateId.MaxLength = 30;
                colvarAccessoryGroupCreateId.AutoIncrement = false;
                colvarAccessoryGroupCreateId.IsNullable = false;
                colvarAccessoryGroupCreateId.IsPrimaryKey = false;
                colvarAccessoryGroupCreateId.IsForeignKey = false;
                colvarAccessoryGroupCreateId.IsReadOnly = false;
                
                schema.Columns.Add(colvarAccessoryGroupCreateId);
                
                TableSchema.TableColumn colvarAccessoryGroupCreateTime = new TableSchema.TableColumn(schema);
                colvarAccessoryGroupCreateTime.ColumnName = "accessory_group_create_time";
                colvarAccessoryGroupCreateTime.DataType = DbType.DateTime;
                colvarAccessoryGroupCreateTime.MaxLength = 0;
                colvarAccessoryGroupCreateTime.AutoIncrement = false;
                colvarAccessoryGroupCreateTime.IsNullable = false;
                colvarAccessoryGroupCreateTime.IsPrimaryKey = false;
                colvarAccessoryGroupCreateTime.IsForeignKey = false;
                colvarAccessoryGroupCreateTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarAccessoryGroupCreateTime);
                
                TableSchema.TableColumn colvarAccessoryGroupModifyId = new TableSchema.TableColumn(schema);
                colvarAccessoryGroupModifyId.ColumnName = "accessory_group_modify_id";
                colvarAccessoryGroupModifyId.DataType = DbType.String;
                colvarAccessoryGroupModifyId.MaxLength = 30;
                colvarAccessoryGroupModifyId.AutoIncrement = false;
                colvarAccessoryGroupModifyId.IsNullable = true;
                colvarAccessoryGroupModifyId.IsPrimaryKey = false;
                colvarAccessoryGroupModifyId.IsForeignKey = false;
                colvarAccessoryGroupModifyId.IsReadOnly = false;
                
                schema.Columns.Add(colvarAccessoryGroupModifyId);
                
                TableSchema.TableColumn colvarAccessoryGroupModifyTime = new TableSchema.TableColumn(schema);
                colvarAccessoryGroupModifyTime.ColumnName = "accessory_group_modify_time";
                colvarAccessoryGroupModifyTime.DataType = DbType.DateTime;
                colvarAccessoryGroupModifyTime.MaxLength = 0;
                colvarAccessoryGroupModifyTime.AutoIncrement = false;
                colvarAccessoryGroupModifyTime.IsNullable = true;
                colvarAccessoryGroupModifyTime.IsPrimaryKey = false;
                colvarAccessoryGroupModifyTime.IsForeignKey = false;
                colvarAccessoryGroupModifyTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarAccessoryGroupModifyTime);
                
                TableSchema.TableColumn colvarAccessoryGroupGuid = new TableSchema.TableColumn(schema);
                colvarAccessoryGroupGuid.ColumnName = "accessory_group_GUID";
                colvarAccessoryGroupGuid.DataType = DbType.Guid;
                colvarAccessoryGroupGuid.MaxLength = 0;
                colvarAccessoryGroupGuid.AutoIncrement = false;
                colvarAccessoryGroupGuid.IsNullable = false;
                colvarAccessoryGroupGuid.IsPrimaryKey = false;
                colvarAccessoryGroupGuid.IsForeignKey = false;
                colvarAccessoryGroupGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarAccessoryGroupGuid);
                
                TableSchema.TableColumn colvarAccessoryGuid = new TableSchema.TableColumn(schema);
                colvarAccessoryGuid.ColumnName = "accessory_GUID";
                colvarAccessoryGuid.DataType = DbType.Guid;
                colvarAccessoryGuid.MaxLength = 0;
                colvarAccessoryGuid.AutoIncrement = false;
                colvarAccessoryGuid.IsNullable = false;
                colvarAccessoryGuid.IsPrimaryKey = false;
                colvarAccessoryGuid.IsForeignKey = false;
                colvarAccessoryGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarAccessoryGuid);
                
                TableSchema.TableColumn colvarAccessoryGroupMemberGuid = new TableSchema.TableColumn(schema);
                colvarAccessoryGroupMemberGuid.ColumnName = "accessory_group_member_GUID";
                colvarAccessoryGroupMemberGuid.DataType = DbType.Guid;
                colvarAccessoryGroupMemberGuid.MaxLength = 0;
                colvarAccessoryGroupMemberGuid.AutoIncrement = false;
                colvarAccessoryGroupMemberGuid.IsNullable = false;
                colvarAccessoryGroupMemberGuid.IsPrimaryKey = false;
                colvarAccessoryGroupMemberGuid.IsForeignKey = false;
                colvarAccessoryGroupMemberGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarAccessoryGroupMemberGuid);
                
                TableSchema.TableColumn colvarAccessoryGroupMemberId = new TableSchema.TableColumn(schema);
                colvarAccessoryGroupMemberId.ColumnName = "accessory_group_member_id";
                colvarAccessoryGroupMemberId.DataType = DbType.AnsiString;
                colvarAccessoryGroupMemberId.MaxLength = 20;
                colvarAccessoryGroupMemberId.AutoIncrement = false;
                colvarAccessoryGroupMemberId.IsNullable = true;
                colvarAccessoryGroupMemberId.IsPrimaryKey = false;
                colvarAccessoryGroupMemberId.IsForeignKey = false;
                colvarAccessoryGroupMemberId.IsReadOnly = false;
                
                schema.Columns.Add(colvarAccessoryGroupMemberId);
                
                TableSchema.TableColumn colvarAccessoryGroupMemberPrice = new TableSchema.TableColumn(schema);
                colvarAccessoryGroupMemberPrice.ColumnName = "accessory_group_member_price";
                colvarAccessoryGroupMemberPrice.DataType = DbType.Currency;
                colvarAccessoryGroupMemberPrice.MaxLength = 0;
                colvarAccessoryGroupMemberPrice.AutoIncrement = false;
                colvarAccessoryGroupMemberPrice.IsNullable = false;
                colvarAccessoryGroupMemberPrice.IsPrimaryKey = false;
                colvarAccessoryGroupMemberPrice.IsForeignKey = false;
                colvarAccessoryGroupMemberPrice.IsReadOnly = false;
                
                schema.Columns.Add(colvarAccessoryGroupMemberPrice);
                
                TableSchema.TableColumn colvarAccessoryGroupMemberNote = new TableSchema.TableColumn(schema);
                colvarAccessoryGroupMemberNote.ColumnName = "accessory_group_member_note";
                colvarAccessoryGroupMemberNote.DataType = DbType.String;
                colvarAccessoryGroupMemberNote.MaxLength = 50;
                colvarAccessoryGroupMemberNote.AutoIncrement = false;
                colvarAccessoryGroupMemberNote.IsNullable = true;
                colvarAccessoryGroupMemberNote.IsPrimaryKey = false;
                colvarAccessoryGroupMemberNote.IsForeignKey = false;
                colvarAccessoryGroupMemberNote.IsReadOnly = false;
                
                schema.Columns.Add(colvarAccessoryGroupMemberNote);
                
                TableSchema.TableColumn colvarSequence = new TableSchema.TableColumn(schema);
                colvarSequence.ColumnName = "sequence";
                colvarSequence.DataType = DbType.Int32;
                colvarSequence.MaxLength = 0;
                colvarSequence.AutoIncrement = false;
                colvarSequence.IsNullable = true;
                colvarSequence.IsPrimaryKey = false;
                colvarSequence.IsForeignKey = false;
                colvarSequence.IsReadOnly = false;
                
                schema.Columns.Add(colvarSequence);
                
                TableSchema.TableColumn colvarAccessoryGroupMemberCreateId = new TableSchema.TableColumn(schema);
                colvarAccessoryGroupMemberCreateId.ColumnName = "accessory_group_member_create_id";
                colvarAccessoryGroupMemberCreateId.DataType = DbType.String;
                colvarAccessoryGroupMemberCreateId.MaxLength = 30;
                colvarAccessoryGroupMemberCreateId.AutoIncrement = false;
                colvarAccessoryGroupMemberCreateId.IsNullable = false;
                colvarAccessoryGroupMemberCreateId.IsPrimaryKey = false;
                colvarAccessoryGroupMemberCreateId.IsForeignKey = false;
                colvarAccessoryGroupMemberCreateId.IsReadOnly = false;
                
                schema.Columns.Add(colvarAccessoryGroupMemberCreateId);
                
                TableSchema.TableColumn colvarAccessoryGroupMemberCreateTime = new TableSchema.TableColumn(schema);
                colvarAccessoryGroupMemberCreateTime.ColumnName = "accessory_group_member_create_time";
                colvarAccessoryGroupMemberCreateTime.DataType = DbType.DateTime;
                colvarAccessoryGroupMemberCreateTime.MaxLength = 0;
                colvarAccessoryGroupMemberCreateTime.AutoIncrement = false;
                colvarAccessoryGroupMemberCreateTime.IsNullable = false;
                colvarAccessoryGroupMemberCreateTime.IsPrimaryKey = false;
                colvarAccessoryGroupMemberCreateTime.IsForeignKey = false;
                colvarAccessoryGroupMemberCreateTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarAccessoryGroupMemberCreateTime);
                
                TableSchema.TableColumn colvarAccessoryGroupMemberModifyId = new TableSchema.TableColumn(schema);
                colvarAccessoryGroupMemberModifyId.ColumnName = "accessory_group_member_modify_id";
                colvarAccessoryGroupMemberModifyId.DataType = DbType.String;
                colvarAccessoryGroupMemberModifyId.MaxLength = 30;
                colvarAccessoryGroupMemberModifyId.AutoIncrement = false;
                colvarAccessoryGroupMemberModifyId.IsNullable = true;
                colvarAccessoryGroupMemberModifyId.IsPrimaryKey = false;
                colvarAccessoryGroupMemberModifyId.IsForeignKey = false;
                colvarAccessoryGroupMemberModifyId.IsReadOnly = false;
                
                schema.Columns.Add(colvarAccessoryGroupMemberModifyId);
                
                TableSchema.TableColumn colvarAccessoryGroupMemberModifyTime = new TableSchema.TableColumn(schema);
                colvarAccessoryGroupMemberModifyTime.ColumnName = "accessory_group_member_modify_time";
                colvarAccessoryGroupMemberModifyTime.DataType = DbType.DateTime;
                colvarAccessoryGroupMemberModifyTime.MaxLength = 0;
                colvarAccessoryGroupMemberModifyTime.AutoIncrement = false;
                colvarAccessoryGroupMemberModifyTime.IsNullable = true;
                colvarAccessoryGroupMemberModifyTime.IsPrimaryKey = false;
                colvarAccessoryGroupMemberModifyTime.IsForeignKey = false;
                colvarAccessoryGroupMemberModifyTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarAccessoryGroupMemberModifyTime);
                
                TableSchema.TableColumn colvarAccessoryCategoryGuid = new TableSchema.TableColumn(schema);
                colvarAccessoryCategoryGuid.ColumnName = "accessory_category_GUID";
                colvarAccessoryCategoryGuid.DataType = DbType.Guid;
                colvarAccessoryCategoryGuid.MaxLength = 0;
                colvarAccessoryCategoryGuid.AutoIncrement = false;
                colvarAccessoryCategoryGuid.IsNullable = false;
                colvarAccessoryCategoryGuid.IsPrimaryKey = false;
                colvarAccessoryCategoryGuid.IsForeignKey = false;
                colvarAccessoryCategoryGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarAccessoryCategoryGuid);
                
                TableSchema.TableColumn colvarAccessoryId = new TableSchema.TableColumn(schema);
                colvarAccessoryId.ColumnName = "accessory_id";
                colvarAccessoryId.DataType = DbType.AnsiString;
                colvarAccessoryId.MaxLength = 20;
                colvarAccessoryId.AutoIncrement = false;
                colvarAccessoryId.IsNullable = false;
                colvarAccessoryId.IsPrimaryKey = false;
                colvarAccessoryId.IsForeignKey = false;
                colvarAccessoryId.IsReadOnly = false;
                
                schema.Columns.Add(colvarAccessoryId);
                
                TableSchema.TableColumn colvarAccessoryName = new TableSchema.TableColumn(schema);
                colvarAccessoryName.ColumnName = "accessory_name";
                colvarAccessoryName.DataType = DbType.String;
                colvarAccessoryName.MaxLength = 50;
                colvarAccessoryName.AutoIncrement = false;
                colvarAccessoryName.IsNullable = false;
                colvarAccessoryName.IsPrimaryKey = false;
                colvarAccessoryName.IsForeignKey = false;
                colvarAccessoryName.IsReadOnly = false;
                
                schema.Columns.Add(colvarAccessoryName);
                
                TableSchema.TableColumn colvarAccessoryCreateId = new TableSchema.TableColumn(schema);
                colvarAccessoryCreateId.ColumnName = "accessory_create_id";
                colvarAccessoryCreateId.DataType = DbType.String;
                colvarAccessoryCreateId.MaxLength = 30;
                colvarAccessoryCreateId.AutoIncrement = false;
                colvarAccessoryCreateId.IsNullable = false;
                colvarAccessoryCreateId.IsPrimaryKey = false;
                colvarAccessoryCreateId.IsForeignKey = false;
                colvarAccessoryCreateId.IsReadOnly = false;
                
                schema.Columns.Add(colvarAccessoryCreateId);
                
                TableSchema.TableColumn colvarAccessoryCreateTime = new TableSchema.TableColumn(schema);
                colvarAccessoryCreateTime.ColumnName = "accessory_create_time";
                colvarAccessoryCreateTime.DataType = DbType.DateTime;
                colvarAccessoryCreateTime.MaxLength = 0;
                colvarAccessoryCreateTime.AutoIncrement = false;
                colvarAccessoryCreateTime.IsNullable = false;
                colvarAccessoryCreateTime.IsPrimaryKey = false;
                colvarAccessoryCreateTime.IsForeignKey = false;
                colvarAccessoryCreateTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarAccessoryCreateTime);
                
                TableSchema.TableColumn colvarAccessoryModifyId = new TableSchema.TableColumn(schema);
                colvarAccessoryModifyId.ColumnName = "accessory_modify_id";
                colvarAccessoryModifyId.DataType = DbType.String;
                colvarAccessoryModifyId.MaxLength = 30;
                colvarAccessoryModifyId.AutoIncrement = false;
                colvarAccessoryModifyId.IsNullable = true;
                colvarAccessoryModifyId.IsPrimaryKey = false;
                colvarAccessoryModifyId.IsForeignKey = false;
                colvarAccessoryModifyId.IsReadOnly = false;
                
                schema.Columns.Add(colvarAccessoryModifyId);
                
                TableSchema.TableColumn colvarAccessoryModifyTime = new TableSchema.TableColumn(schema);
                colvarAccessoryModifyTime.ColumnName = "accessory_modify_time";
                colvarAccessoryModifyTime.DataType = DbType.DateTime;
                colvarAccessoryModifyTime.MaxLength = 0;
                colvarAccessoryModifyTime.AutoIncrement = false;
                colvarAccessoryModifyTime.IsNullable = true;
                colvarAccessoryModifyTime.IsPrimaryKey = false;
                colvarAccessoryModifyTime.IsForeignKey = false;
                colvarAccessoryModifyTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarAccessoryModifyTime);
                
                TableSchema.TableColumn colvarAccessoryCategoryId = new TableSchema.TableColumn(schema);
                colvarAccessoryCategoryId.ColumnName = "accessory_category_id";
                colvarAccessoryCategoryId.DataType = DbType.AnsiString;
                colvarAccessoryCategoryId.MaxLength = 20;
                colvarAccessoryCategoryId.AutoIncrement = false;
                colvarAccessoryCategoryId.IsNullable = true;
                colvarAccessoryCategoryId.IsPrimaryKey = false;
                colvarAccessoryCategoryId.IsForeignKey = false;
                colvarAccessoryCategoryId.IsReadOnly = false;
                
                schema.Columns.Add(colvarAccessoryCategoryId);
                
                TableSchema.TableColumn colvarAccessoryCategoryName = new TableSchema.TableColumn(schema);
                colvarAccessoryCategoryName.ColumnName = "accessory_category_name";
                colvarAccessoryCategoryName.DataType = DbType.String;
                colvarAccessoryCategoryName.MaxLength = 50;
                colvarAccessoryCategoryName.AutoIncrement = false;
                colvarAccessoryCategoryName.IsNullable = false;
                colvarAccessoryCategoryName.IsPrimaryKey = false;
                colvarAccessoryCategoryName.IsForeignKey = false;
                colvarAccessoryCategoryName.IsReadOnly = false;
                
                schema.Columns.Add(colvarAccessoryCategoryName);
                
                TableSchema.TableColumn colvarAccessoryCategoryCreateId = new TableSchema.TableColumn(schema);
                colvarAccessoryCategoryCreateId.ColumnName = "accessory_category_create_id";
                colvarAccessoryCategoryCreateId.DataType = DbType.String;
                colvarAccessoryCategoryCreateId.MaxLength = 30;
                colvarAccessoryCategoryCreateId.AutoIncrement = false;
                colvarAccessoryCategoryCreateId.IsNullable = false;
                colvarAccessoryCategoryCreateId.IsPrimaryKey = false;
                colvarAccessoryCategoryCreateId.IsForeignKey = false;
                colvarAccessoryCategoryCreateId.IsReadOnly = false;
                
                schema.Columns.Add(colvarAccessoryCategoryCreateId);
                
                TableSchema.TableColumn colvarAccessoryCategoryCreateTime = new TableSchema.TableColumn(schema);
                colvarAccessoryCategoryCreateTime.ColumnName = "accessory_category_create_time";
                colvarAccessoryCategoryCreateTime.DataType = DbType.DateTime;
                colvarAccessoryCategoryCreateTime.MaxLength = 0;
                colvarAccessoryCategoryCreateTime.AutoIncrement = false;
                colvarAccessoryCategoryCreateTime.IsNullable = false;
                colvarAccessoryCategoryCreateTime.IsPrimaryKey = false;
                colvarAccessoryCategoryCreateTime.IsForeignKey = false;
                colvarAccessoryCategoryCreateTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarAccessoryCategoryCreateTime);
                
                TableSchema.TableColumn colvarAccessoryCategoryModifyId = new TableSchema.TableColumn(schema);
                colvarAccessoryCategoryModifyId.ColumnName = "accessory_category_modify_id";
                colvarAccessoryCategoryModifyId.DataType = DbType.String;
                colvarAccessoryCategoryModifyId.MaxLength = 30;
                colvarAccessoryCategoryModifyId.AutoIncrement = false;
                colvarAccessoryCategoryModifyId.IsNullable = true;
                colvarAccessoryCategoryModifyId.IsPrimaryKey = false;
                colvarAccessoryCategoryModifyId.IsForeignKey = false;
                colvarAccessoryCategoryModifyId.IsReadOnly = false;
                
                schema.Columns.Add(colvarAccessoryCategoryModifyId);
                
                TableSchema.TableColumn colvarAccessoryCategoryModifyTime = new TableSchema.TableColumn(schema);
                colvarAccessoryCategoryModifyTime.ColumnName = "accessory_category_modify_time";
                colvarAccessoryCategoryModifyTime.DataType = DbType.DateTime;
                colvarAccessoryCategoryModifyTime.MaxLength = 0;
                colvarAccessoryCategoryModifyTime.AutoIncrement = false;
                colvarAccessoryCategoryModifyTime.IsNullable = true;
                colvarAccessoryCategoryModifyTime.IsPrimaryKey = false;
                colvarAccessoryCategoryModifyTime.IsForeignKey = false;
                colvarAccessoryCategoryModifyTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarAccessoryCategoryModifyTime);
                
                TableSchema.TableColumn colvarAccessoryGroupType = new TableSchema.TableColumn(schema);
                colvarAccessoryGroupType.ColumnName = "accessory_group_type";
                colvarAccessoryGroupType.DataType = DbType.Int32;
                colvarAccessoryGroupType.MaxLength = 0;
                colvarAccessoryGroupType.AutoIncrement = false;
                colvarAccessoryGroupType.IsNullable = false;
                colvarAccessoryGroupType.IsPrimaryKey = false;
                colvarAccessoryGroupType.IsForeignKey = false;
                colvarAccessoryGroupType.IsReadOnly = false;
                
                schema.Columns.Add(colvarAccessoryGroupType);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_accessory_group_category",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewAccessoryGroupCategory()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewAccessoryGroupCategory(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewAccessoryGroupCategory(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewAccessoryGroupCategory(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("AccessoryGroupId")]
        [Bindable(true)]
        public string AccessoryGroupId 
	    {
		    get
		    {
			    return GetColumnValue<string>("accessory_group_id");
		    }
            set 
		    {
			    SetColumnValue("accessory_group_id", value);
            }
        }
	      
        [XmlAttribute("AccessoryGroupName")]
        [Bindable(true)]
        public string AccessoryGroupName 
	    {
		    get
		    {
			    return GetColumnValue<string>("accessory_group_name");
		    }
            set 
		    {
			    SetColumnValue("accessory_group_name", value);
            }
        }
	      
        [XmlAttribute("SellerGuid")]
        [Bindable(true)]
        public Guid? SellerGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid?>("seller_GUID");
		    }
            set 
		    {
			    SetColumnValue("seller_GUID", value);
            }
        }
	      
        [XmlAttribute("AccessoryGroupCreateId")]
        [Bindable(true)]
        public string AccessoryGroupCreateId 
	    {
		    get
		    {
			    return GetColumnValue<string>("accessory_group_create_id");
		    }
            set 
		    {
			    SetColumnValue("accessory_group_create_id", value);
            }
        }
	      
        [XmlAttribute("AccessoryGroupCreateTime")]
        [Bindable(true)]
        public DateTime AccessoryGroupCreateTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("accessory_group_create_time");
		    }
            set 
		    {
			    SetColumnValue("accessory_group_create_time", value);
            }
        }
	      
        [XmlAttribute("AccessoryGroupModifyId")]
        [Bindable(true)]
        public string AccessoryGroupModifyId 
	    {
		    get
		    {
			    return GetColumnValue<string>("accessory_group_modify_id");
		    }
            set 
		    {
			    SetColumnValue("accessory_group_modify_id", value);
            }
        }
	      
        [XmlAttribute("AccessoryGroupModifyTime")]
        [Bindable(true)]
        public DateTime? AccessoryGroupModifyTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("accessory_group_modify_time");
		    }
            set 
		    {
			    SetColumnValue("accessory_group_modify_time", value);
            }
        }
	      
        [XmlAttribute("AccessoryGroupGuid")]
        [Bindable(true)]
        public Guid AccessoryGroupGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("accessory_group_GUID");
		    }
            set 
		    {
			    SetColumnValue("accessory_group_GUID", value);
            }
        }
	      
        [XmlAttribute("AccessoryGuid")]
        [Bindable(true)]
        public Guid AccessoryGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("accessory_GUID");
		    }
            set 
		    {
			    SetColumnValue("accessory_GUID", value);
            }
        }
	      
        [XmlAttribute("AccessoryGroupMemberGuid")]
        [Bindable(true)]
        public Guid AccessoryGroupMemberGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("accessory_group_member_GUID");
		    }
            set 
		    {
			    SetColumnValue("accessory_group_member_GUID", value);
            }
        }
	      
        [XmlAttribute("AccessoryGroupMemberId")]
        [Bindable(true)]
        public string AccessoryGroupMemberId 
	    {
		    get
		    {
			    return GetColumnValue<string>("accessory_group_member_id");
		    }
            set 
		    {
			    SetColumnValue("accessory_group_member_id", value);
            }
        }
	      
        [XmlAttribute("AccessoryGroupMemberPrice")]
        [Bindable(true)]
        public decimal AccessoryGroupMemberPrice 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("accessory_group_member_price");
		    }
            set 
		    {
			    SetColumnValue("accessory_group_member_price", value);
            }
        }
	      
        [XmlAttribute("AccessoryGroupMemberNote")]
        [Bindable(true)]
        public string AccessoryGroupMemberNote 
	    {
		    get
		    {
			    return GetColumnValue<string>("accessory_group_member_note");
		    }
            set 
		    {
			    SetColumnValue("accessory_group_member_note", value);
            }
        }
	      
        [XmlAttribute("Sequence")]
        [Bindable(true)]
        public int? Sequence 
	    {
		    get
		    {
			    return GetColumnValue<int?>("sequence");
		    }
            set 
		    {
			    SetColumnValue("sequence", value);
            }
        }
	      
        [XmlAttribute("AccessoryGroupMemberCreateId")]
        [Bindable(true)]
        public string AccessoryGroupMemberCreateId 
	    {
		    get
		    {
			    return GetColumnValue<string>("accessory_group_member_create_id");
		    }
            set 
		    {
			    SetColumnValue("accessory_group_member_create_id", value);
            }
        }
	      
        [XmlAttribute("AccessoryGroupMemberCreateTime")]
        [Bindable(true)]
        public DateTime AccessoryGroupMemberCreateTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("accessory_group_member_create_time");
		    }
            set 
		    {
			    SetColumnValue("accessory_group_member_create_time", value);
            }
        }
	      
        [XmlAttribute("AccessoryGroupMemberModifyId")]
        [Bindable(true)]
        public string AccessoryGroupMemberModifyId 
	    {
		    get
		    {
			    return GetColumnValue<string>("accessory_group_member_modify_id");
		    }
            set 
		    {
			    SetColumnValue("accessory_group_member_modify_id", value);
            }
        }
	      
        [XmlAttribute("AccessoryGroupMemberModifyTime")]
        [Bindable(true)]
        public DateTime? AccessoryGroupMemberModifyTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("accessory_group_member_modify_time");
		    }
            set 
		    {
			    SetColumnValue("accessory_group_member_modify_time", value);
            }
        }
	      
        [XmlAttribute("AccessoryCategoryGuid")]
        [Bindable(true)]
        public Guid AccessoryCategoryGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("accessory_category_GUID");
		    }
            set 
		    {
			    SetColumnValue("accessory_category_GUID", value);
            }
        }
	      
        [XmlAttribute("AccessoryId")]
        [Bindable(true)]
        public string AccessoryId 
	    {
		    get
		    {
			    return GetColumnValue<string>("accessory_id");
		    }
            set 
		    {
			    SetColumnValue("accessory_id", value);
            }
        }
	      
        [XmlAttribute("AccessoryName")]
        [Bindable(true)]
        public string AccessoryName 
	    {
		    get
		    {
			    return GetColumnValue<string>("accessory_name");
		    }
            set 
		    {
			    SetColumnValue("accessory_name", value);
            }
        }
	      
        [XmlAttribute("AccessoryCreateId")]
        [Bindable(true)]
        public string AccessoryCreateId 
	    {
		    get
		    {
			    return GetColumnValue<string>("accessory_create_id");
		    }
            set 
		    {
			    SetColumnValue("accessory_create_id", value);
            }
        }
	      
        [XmlAttribute("AccessoryCreateTime")]
        [Bindable(true)]
        public DateTime AccessoryCreateTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("accessory_create_time");
		    }
            set 
		    {
			    SetColumnValue("accessory_create_time", value);
            }
        }
	      
        [XmlAttribute("AccessoryModifyId")]
        [Bindable(true)]
        public string AccessoryModifyId 
	    {
		    get
		    {
			    return GetColumnValue<string>("accessory_modify_id");
		    }
            set 
		    {
			    SetColumnValue("accessory_modify_id", value);
            }
        }
	      
        [XmlAttribute("AccessoryModifyTime")]
        [Bindable(true)]
        public DateTime? AccessoryModifyTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("accessory_modify_time");
		    }
            set 
		    {
			    SetColumnValue("accessory_modify_time", value);
            }
        }
	      
        [XmlAttribute("AccessoryCategoryId")]
        [Bindable(true)]
        public string AccessoryCategoryId 
	    {
		    get
		    {
			    return GetColumnValue<string>("accessory_category_id");
		    }
            set 
		    {
			    SetColumnValue("accessory_category_id", value);
            }
        }
	      
        [XmlAttribute("AccessoryCategoryName")]
        [Bindable(true)]
        public string AccessoryCategoryName 
	    {
		    get
		    {
			    return GetColumnValue<string>("accessory_category_name");
		    }
            set 
		    {
			    SetColumnValue("accessory_category_name", value);
            }
        }
	      
        [XmlAttribute("AccessoryCategoryCreateId")]
        [Bindable(true)]
        public string AccessoryCategoryCreateId 
	    {
		    get
		    {
			    return GetColumnValue<string>("accessory_category_create_id");
		    }
            set 
		    {
			    SetColumnValue("accessory_category_create_id", value);
            }
        }
	      
        [XmlAttribute("AccessoryCategoryCreateTime")]
        [Bindable(true)]
        public DateTime AccessoryCategoryCreateTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("accessory_category_create_time");
		    }
            set 
		    {
			    SetColumnValue("accessory_category_create_time", value);
            }
        }
	      
        [XmlAttribute("AccessoryCategoryModifyId")]
        [Bindable(true)]
        public string AccessoryCategoryModifyId 
	    {
		    get
		    {
			    return GetColumnValue<string>("accessory_category_modify_id");
		    }
            set 
		    {
			    SetColumnValue("accessory_category_modify_id", value);
            }
        }
	      
        [XmlAttribute("AccessoryCategoryModifyTime")]
        [Bindable(true)]
        public DateTime? AccessoryCategoryModifyTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("accessory_category_modify_time");
		    }
            set 
		    {
			    SetColumnValue("accessory_category_modify_time", value);
            }
        }
	      
        [XmlAttribute("AccessoryGroupType")]
        [Bindable(true)]
        public int AccessoryGroupType 
	    {
		    get
		    {
			    return GetColumnValue<int>("accessory_group_type");
		    }
            set 
		    {
			    SetColumnValue("accessory_group_type", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string AccessoryGroupId = @"accessory_group_id";
            
            public static string AccessoryGroupName = @"accessory_group_name";
            
            public static string SellerGuid = @"seller_GUID";
            
            public static string AccessoryGroupCreateId = @"accessory_group_create_id";
            
            public static string AccessoryGroupCreateTime = @"accessory_group_create_time";
            
            public static string AccessoryGroupModifyId = @"accessory_group_modify_id";
            
            public static string AccessoryGroupModifyTime = @"accessory_group_modify_time";
            
            public static string AccessoryGroupGuid = @"accessory_group_GUID";
            
            public static string AccessoryGuid = @"accessory_GUID";
            
            public static string AccessoryGroupMemberGuid = @"accessory_group_member_GUID";
            
            public static string AccessoryGroupMemberId = @"accessory_group_member_id";
            
            public static string AccessoryGroupMemberPrice = @"accessory_group_member_price";
            
            public static string AccessoryGroupMemberNote = @"accessory_group_member_note";
            
            public static string Sequence = @"sequence";
            
            public static string AccessoryGroupMemberCreateId = @"accessory_group_member_create_id";
            
            public static string AccessoryGroupMemberCreateTime = @"accessory_group_member_create_time";
            
            public static string AccessoryGroupMemberModifyId = @"accessory_group_member_modify_id";
            
            public static string AccessoryGroupMemberModifyTime = @"accessory_group_member_modify_time";
            
            public static string AccessoryCategoryGuid = @"accessory_category_GUID";
            
            public static string AccessoryId = @"accessory_id";
            
            public static string AccessoryName = @"accessory_name";
            
            public static string AccessoryCreateId = @"accessory_create_id";
            
            public static string AccessoryCreateTime = @"accessory_create_time";
            
            public static string AccessoryModifyId = @"accessory_modify_id";
            
            public static string AccessoryModifyTime = @"accessory_modify_time";
            
            public static string AccessoryCategoryId = @"accessory_category_id";
            
            public static string AccessoryCategoryName = @"accessory_category_name";
            
            public static string AccessoryCategoryCreateId = @"accessory_category_create_id";
            
            public static string AccessoryCategoryCreateTime = @"accessory_category_create_time";
            
            public static string AccessoryCategoryModifyId = @"accessory_category_modify_id";
            
            public static string AccessoryCategoryModifyTime = @"accessory_category_modify_time";
            
            public static string AccessoryGroupType = @"accessory_group_type";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

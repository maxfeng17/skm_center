using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the FamiportPinRollbackLog class.
	/// </summary>
    [Serializable]
	public partial class FamiportPinRollbackLogCollection : RepositoryList<FamiportPinRollbackLog, FamiportPinRollbackLogCollection>
	{	   
		public FamiportPinRollbackLogCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>FamiportPinRollbackLogCollection</returns>
		public FamiportPinRollbackLogCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                FamiportPinRollbackLog o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the famiport_pin_rollback_log table.
	/// </summary>
	[Serializable]
	public partial class FamiportPinRollbackLog : RepositoryRecord<FamiportPinRollbackLog>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public FamiportPinRollbackLog()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public FamiportPinRollbackLog(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("famiport_pin_rollback_log", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarFamiportId = new TableSchema.TableColumn(schema);
				colvarFamiportId.ColumnName = "famiport_id";
				colvarFamiportId.DataType = DbType.Int32;
				colvarFamiportId.MaxLength = 0;
				colvarFamiportId.AutoIncrement = false;
				colvarFamiportId.IsNullable = false;
				colvarFamiportId.IsPrimaryKey = false;
				colvarFamiportId.IsForeignKey = false;
				colvarFamiportId.IsReadOnly = false;
				colvarFamiportId.DefaultSetting = @"";
				colvarFamiportId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFamiportId);
				
				TableSchema.TableColumn colvarPeztempId = new TableSchema.TableColumn(schema);
				colvarPeztempId.ColumnName = "peztemp_id";
				colvarPeztempId.DataType = DbType.Int32;
				colvarPeztempId.MaxLength = 0;
				colvarPeztempId.AutoIncrement = false;
				colvarPeztempId.IsNullable = false;
				colvarPeztempId.IsPrimaryKey = false;
				colvarPeztempId.IsForeignKey = false;
				colvarPeztempId.IsReadOnly = false;
				colvarPeztempId.DefaultSetting = @"";
				colvarPeztempId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPeztempId);
				
				TableSchema.TableColumn colvarTranNo = new TableSchema.TableColumn(schema);
				colvarTranNo.ColumnName = "tran_no";
				colvarTranNo.DataType = DbType.AnsiString;
				colvarTranNo.MaxLength = 20;
				colvarTranNo.AutoIncrement = false;
				colvarTranNo.IsNullable = false;
				colvarTranNo.IsPrimaryKey = false;
				colvarTranNo.IsForeignKey = false;
				colvarTranNo.IsReadOnly = false;
				colvarTranNo.DefaultSetting = @"";
				colvarTranNo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTranNo);
				
				TableSchema.TableColumn colvarTenCode = new TableSchema.TableColumn(schema);
				colvarTenCode.ColumnName = "ten_code";
				colvarTenCode.DataType = DbType.AnsiString;
				colvarTenCode.MaxLength = 6;
				colvarTenCode.AutoIncrement = false;
				colvarTenCode.IsNullable = false;
				colvarTenCode.IsPrimaryKey = false;
				colvarTenCode.IsForeignKey = false;
				colvarTenCode.IsReadOnly = false;
				colvarTenCode.DefaultSetting = @"";
				colvarTenCode.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTenCode);
				
				TableSchema.TableColumn colvarMmkId = new TableSchema.TableColumn(schema);
				colvarMmkId.ColumnName = "mmk_id";
				colvarMmkId.DataType = DbType.AnsiString;
				colvarMmkId.MaxLength = 4;
				colvarMmkId.AutoIncrement = false;
				colvarMmkId.IsNullable = false;
				colvarMmkId.IsPrimaryKey = false;
				colvarMmkId.IsForeignKey = false;
				colvarMmkId.IsReadOnly = false;
				colvarMmkId.DefaultSetting = @"";
				colvarMmkId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMmkId);
				
				TableSchema.TableColumn colvarMmkIdName = new TableSchema.TableColumn(schema);
				colvarMmkIdName.ColumnName = "mmk_id_name";
				colvarMmkIdName.DataType = DbType.String;
				colvarMmkIdName.MaxLength = 30;
				colvarMmkIdName.AutoIncrement = false;
				colvarMmkIdName.IsNullable = false;
				colvarMmkIdName.IsPrimaryKey = false;
				colvarMmkIdName.IsForeignKey = false;
				colvarMmkIdName.IsReadOnly = false;
				colvarMmkIdName.DefaultSetting = @"";
				colvarMmkIdName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMmkIdName);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				
						colvarCreateTime.DefaultSetting = @"(getdate())";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarCreateUserId = new TableSchema.TableColumn(schema);
				colvarCreateUserId.ColumnName = "create_user_id";
				colvarCreateUserId.DataType = DbType.Int32;
				colvarCreateUserId.MaxLength = 0;
				colvarCreateUserId.AutoIncrement = false;
				colvarCreateUserId.IsNullable = false;
				colvarCreateUserId.IsPrimaryKey = false;
				colvarCreateUserId.IsForeignKey = false;
				colvarCreateUserId.IsReadOnly = false;
				colvarCreateUserId.DefaultSetting = @"";
				colvarCreateUserId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateUserId);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("famiport_pin_rollback_log",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("FamiportId")]
		[Bindable(true)]
		public int FamiportId 
		{
			get { return GetColumnValue<int>(Columns.FamiportId); }
			set { SetColumnValue(Columns.FamiportId, value); }
		}
		  
		[XmlAttribute("PeztempId")]
		[Bindable(true)]
		public int PeztempId 
		{
			get { return GetColumnValue<int>(Columns.PeztempId); }
			set { SetColumnValue(Columns.PeztempId, value); }
		}
		  
		[XmlAttribute("TranNo")]
		[Bindable(true)]
		public string TranNo 
		{
			get { return GetColumnValue<string>(Columns.TranNo); }
			set { SetColumnValue(Columns.TranNo, value); }
		}
		  
		[XmlAttribute("TenCode")]
		[Bindable(true)]
		public string TenCode 
		{
			get { return GetColumnValue<string>(Columns.TenCode); }
			set { SetColumnValue(Columns.TenCode, value); }
		}
		  
		[XmlAttribute("MmkId")]
		[Bindable(true)]
		public string MmkId 
		{
			get { return GetColumnValue<string>(Columns.MmkId); }
			set { SetColumnValue(Columns.MmkId, value); }
		}
		  
		[XmlAttribute("MmkIdName")]
		[Bindable(true)]
		public string MmkIdName 
		{
			get { return GetColumnValue<string>(Columns.MmkIdName); }
			set { SetColumnValue(Columns.MmkIdName, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		  
		[XmlAttribute("CreateUserId")]
		[Bindable(true)]
		public int CreateUserId 
		{
			get { return GetColumnValue<int>(Columns.CreateUserId); }
			set { SetColumnValue(Columns.CreateUserId, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn FamiportIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn PeztempIdColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn TranNoColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn TenCodeColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn MmkIdColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn MmkIdNameColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateUserIdColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string FamiportId = @"famiport_id";
			 public static string PeztempId = @"peztemp_id";
			 public static string TranNo = @"tran_no";
			 public static string TenCode = @"ten_code";
			 public static string MmkId = @"mmk_id";
			 public static string MmkIdName = @"mmk_id_name";
			 public static string CreateTime = @"create_time";
			 public static string CreateUserId = @"create_user_id";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

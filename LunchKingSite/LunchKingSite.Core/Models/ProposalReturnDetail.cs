using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the ProposalReturnDetail class.
	/// </summary>
    [Serializable]
	public partial class ProposalReturnDetailCollection : RepositoryList<ProposalReturnDetail, ProposalReturnDetailCollection>
	{	   
		public ProposalReturnDetailCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>ProposalReturnDetailCollection</returns>
		public ProposalReturnDetailCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                ProposalReturnDetail o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the proposal_return_detail table.
	/// </summary>
	[Serializable]
	public partial class ProposalReturnDetail : RepositoryRecord<ProposalReturnDetail>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public ProposalReturnDetail()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public ProposalReturnDetail(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("proposal_return_detail", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarProposalId = new TableSchema.TableColumn(schema);
				colvarProposalId.ColumnName = "proposal_id";
				colvarProposalId.DataType = DbType.Int32;
				colvarProposalId.MaxLength = 0;
				colvarProposalId.AutoIncrement = false;
				colvarProposalId.IsNullable = false;
				colvarProposalId.IsPrimaryKey = false;
				colvarProposalId.IsForeignKey = false;
				colvarProposalId.IsReadOnly = false;
				colvarProposalId.DefaultSetting = @"";
				colvarProposalId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarProposalId);
				
				TableSchema.TableColumn colvarProposalLogId = new TableSchema.TableColumn(schema);
				colvarProposalLogId.ColumnName = "proposal_log_id";
				colvarProposalLogId.DataType = DbType.Int32;
				colvarProposalLogId.MaxLength = 0;
				colvarProposalLogId.AutoIncrement = false;
				colvarProposalLogId.IsNullable = false;
				colvarProposalLogId.IsPrimaryKey = false;
				colvarProposalLogId.IsForeignKey = false;
				colvarProposalLogId.IsReadOnly = false;
				colvarProposalLogId.DefaultSetting = @"";
				colvarProposalLogId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarProposalLogId);
				
				TableSchema.TableColumn colvarReturnReasonBinary = new TableSchema.TableColumn(schema);
				colvarReturnReasonBinary.ColumnName = "return_reason_binary";
				colvarReturnReasonBinary.DataType = DbType.Int32;
				colvarReturnReasonBinary.MaxLength = 0;
				colvarReturnReasonBinary.AutoIncrement = false;
				colvarReturnReasonBinary.IsNullable = false;
				colvarReturnReasonBinary.IsPrimaryKey = false;
				colvarReturnReasonBinary.IsForeignKey = false;
				colvarReturnReasonBinary.IsReadOnly = false;
				colvarReturnReasonBinary.DefaultSetting = @"";
				colvarReturnReasonBinary.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReturnReasonBinary);
				
				TableSchema.TableColumn colvarSystemCodeGroup = new TableSchema.TableColumn(schema);
				colvarSystemCodeGroup.ColumnName = "system_code_group";
				colvarSystemCodeGroup.DataType = DbType.AnsiString;
				colvarSystemCodeGroup.MaxLength = 50;
				colvarSystemCodeGroup.AutoIncrement = false;
				colvarSystemCodeGroup.IsNullable = true;
				colvarSystemCodeGroup.IsPrimaryKey = false;
				colvarSystemCodeGroup.IsForeignKey = false;
				colvarSystemCodeGroup.IsReadOnly = false;
				colvarSystemCodeGroup.DefaultSetting = @"";
				colvarSystemCodeGroup.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSystemCodeGroup);
				
				TableSchema.TableColumn colvarDetail = new TableSchema.TableColumn(schema);
				colvarDetail.ColumnName = "detail";
				colvarDetail.DataType = DbType.String;
				colvarDetail.MaxLength = 200;
				colvarDetail.AutoIncrement = false;
				colvarDetail.IsNullable = false;
				colvarDetail.IsPrimaryKey = false;
				colvarDetail.IsForeignKey = false;
				colvarDetail.IsReadOnly = false;
				colvarDetail.DefaultSetting = @"";
				colvarDetail.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDetail);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("proposal_return_detail",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("ProposalId")]
		[Bindable(true)]
		public int ProposalId 
		{
			get { return GetColumnValue<int>(Columns.ProposalId); }
			set { SetColumnValue(Columns.ProposalId, value); }
		}
		  
		[XmlAttribute("ProposalLogId")]
		[Bindable(true)]
		public int ProposalLogId 
		{
			get { return GetColumnValue<int>(Columns.ProposalLogId); }
			set { SetColumnValue(Columns.ProposalLogId, value); }
		}
		  
		[XmlAttribute("ReturnReasonBinary")]
		[Bindable(true)]
		public int ReturnReasonBinary 
		{
			get { return GetColumnValue<int>(Columns.ReturnReasonBinary); }
			set { SetColumnValue(Columns.ReturnReasonBinary, value); }
		}
		  
		[XmlAttribute("SystemCodeGroup")]
		[Bindable(true)]
		public string SystemCodeGroup 
		{
			get { return GetColumnValue<string>(Columns.SystemCodeGroup); }
			set { SetColumnValue(Columns.SystemCodeGroup, value); }
		}
		  
		[XmlAttribute("Detail")]
		[Bindable(true)]
		public string Detail 
		{
			get { return GetColumnValue<string>(Columns.Detail); }
			set { SetColumnValue(Columns.Detail, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn ProposalIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn ProposalLogIdColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn ReturnReasonBinaryColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn SystemCodeGroupColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn DetailColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string ProposalId = @"proposal_id";
			 public static string ProposalLogId = @"proposal_log_id";
			 public static string ReturnReasonBinary = @"return_reason_binary";
			 public static string SystemCodeGroup = @"system_code_group";
			 public static string Detail = @"detail";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

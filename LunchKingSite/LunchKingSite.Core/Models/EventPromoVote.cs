using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the EventPromoVote class.
	/// </summary>
    [Serializable]
	public partial class EventPromoVoteCollection : RepositoryList<EventPromoVote, EventPromoVoteCollection>
	{	   
		public EventPromoVoteCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>EventPromoVoteCollection</returns>
		public EventPromoVoteCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                EventPromoVote o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the event_promo_vote table.
	/// </summary>
	[Serializable]
	public partial class EventPromoVote : RepositoryRecord<EventPromoVote>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public EventPromoVote()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public EventPromoVote(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("event_promo_vote", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarEventPromoItemId = new TableSchema.TableColumn(schema);
				colvarEventPromoItemId.ColumnName = "event_promo_item_id";
				colvarEventPromoItemId.DataType = DbType.Int32;
				colvarEventPromoItemId.MaxLength = 0;
				colvarEventPromoItemId.AutoIncrement = false;
				colvarEventPromoItemId.IsNullable = false;
				colvarEventPromoItemId.IsPrimaryKey = false;
				colvarEventPromoItemId.IsForeignKey = false;
				colvarEventPromoItemId.IsReadOnly = false;
				colvarEventPromoItemId.DefaultSetting = @"";
				colvarEventPromoItemId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEventPromoItemId);
				
				TableSchema.TableColumn colvarMemberUniqueId = new TableSchema.TableColumn(schema);
				colvarMemberUniqueId.ColumnName = "member_unique_id";
				colvarMemberUniqueId.DataType = DbType.Int32;
				colvarMemberUniqueId.MaxLength = 0;
				colvarMemberUniqueId.AutoIncrement = false;
				colvarMemberUniqueId.IsNullable = false;
				colvarMemberUniqueId.IsPrimaryKey = false;
				colvarMemberUniqueId.IsForeignKey = false;
				colvarMemberUniqueId.IsReadOnly = false;
				colvarMemberUniqueId.DefaultSetting = @"";
				colvarMemberUniqueId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMemberUniqueId);
				
				TableSchema.TableColumn colvarVoteTime = new TableSchema.TableColumn(schema);
				colvarVoteTime.ColumnName = "vote_time";
				colvarVoteTime.DataType = DbType.DateTime;
				colvarVoteTime.MaxLength = 0;
				colvarVoteTime.AutoIncrement = false;
				colvarVoteTime.IsNullable = false;
				colvarVoteTime.IsPrimaryKey = false;
				colvarVoteTime.IsForeignKey = false;
				colvarVoteTime.IsReadOnly = false;
				
						colvarVoteTime.DefaultSetting = @"(getdate())";
				colvarVoteTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarVoteTime);
				
				TableSchema.TableColumn colvarIpAddress = new TableSchema.TableColumn(schema);
				colvarIpAddress.ColumnName = "ip_address";
				colvarIpAddress.DataType = DbType.AnsiString;
				colvarIpAddress.MaxLength = 40;
				colvarIpAddress.AutoIncrement = false;
				colvarIpAddress.IsNullable = true;
				colvarIpAddress.IsPrimaryKey = false;
				colvarIpAddress.IsForeignKey = false;
				colvarIpAddress.IsReadOnly = false;
				colvarIpAddress.DefaultSetting = @"";
				colvarIpAddress.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIpAddress);
				
				TableSchema.TableColumn colvarDiscountCodeId = new TableSchema.TableColumn(schema);
				colvarDiscountCodeId.ColumnName = "discount_code_id";
				colvarDiscountCodeId.DataType = DbType.Int32;
				colvarDiscountCodeId.MaxLength = 0;
				colvarDiscountCodeId.AutoIncrement = false;
				colvarDiscountCodeId.IsNullable = false;
				colvarDiscountCodeId.IsPrimaryKey = false;
				colvarDiscountCodeId.IsForeignKey = false;
				colvarDiscountCodeId.IsReadOnly = false;
				
						colvarDiscountCodeId.DefaultSetting = @"((0))";
				colvarDiscountCodeId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDiscountCodeId);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("event_promo_vote",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("EventPromoItemId")]
		[Bindable(true)]
		public int EventPromoItemId 
		{
			get { return GetColumnValue<int>(Columns.EventPromoItemId); }
			set { SetColumnValue(Columns.EventPromoItemId, value); }
		}
		  
		[XmlAttribute("MemberUniqueId")]
		[Bindable(true)]
		public int MemberUniqueId 
		{
			get { return GetColumnValue<int>(Columns.MemberUniqueId); }
			set { SetColumnValue(Columns.MemberUniqueId, value); }
		}
		  
		[XmlAttribute("VoteTime")]
		[Bindable(true)]
		public DateTime VoteTime 
		{
			get { return GetColumnValue<DateTime>(Columns.VoteTime); }
			set { SetColumnValue(Columns.VoteTime, value); }
		}
		  
		[XmlAttribute("IpAddress")]
		[Bindable(true)]
		public string IpAddress 
		{
			get { return GetColumnValue<string>(Columns.IpAddress); }
			set { SetColumnValue(Columns.IpAddress, value); }
		}
		  
		[XmlAttribute("DiscountCodeId")]
		[Bindable(true)]
		public int DiscountCodeId 
		{
			get { return GetColumnValue<int>(Columns.DiscountCodeId); }
			set { SetColumnValue(Columns.DiscountCodeId, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn EventPromoItemIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn MemberUniqueIdColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn VoteTimeColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn IpAddressColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn DiscountCodeIdColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string EventPromoItemId = @"event_promo_item_id";
			 public static string MemberUniqueId = @"member_unique_id";
			 public static string VoteTime = @"vote_time";
			 public static string IpAddress = @"ip_address";
			 public static string DiscountCodeId = @"discount_code_id";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

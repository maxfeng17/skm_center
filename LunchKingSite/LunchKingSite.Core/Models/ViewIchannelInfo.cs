using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewIchannelInfo class.
    /// </summary>
    [Serializable]
    public partial class ViewIchannelInfoCollection : ReadOnlyList<ViewIchannelInfo, ViewIchannelInfoCollection>
    {        
        public ViewIchannelInfoCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_ichannel_info view.
    /// </summary>
    [Serializable]
    public partial class ViewIchannelInfo : ReadOnlyRecord<ViewIchannelInfo>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_ichannel_info", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = false;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = false;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                
                schema.Columns.Add(colvarId);
                
                TableSchema.TableColumn colvarOrderGuid = new TableSchema.TableColumn(schema);
                colvarOrderGuid.ColumnName = "order_guid";
                colvarOrderGuid.DataType = DbType.Guid;
                colvarOrderGuid.MaxLength = 0;
                colvarOrderGuid.AutoIncrement = false;
                colvarOrderGuid.IsNullable = false;
                colvarOrderGuid.IsPrimaryKey = false;
                colvarOrderGuid.IsForeignKey = false;
                colvarOrderGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderGuid);
                
                TableSchema.TableColumn colvarBid = new TableSchema.TableColumn(schema);
                colvarBid.ColumnName = "bid";
                colvarBid.DataType = DbType.Guid;
                colvarBid.MaxLength = 0;
                colvarBid.AutoIncrement = false;
                colvarBid.IsNullable = false;
                colvarBid.IsPrimaryKey = false;
                colvarBid.IsForeignKey = false;
                colvarBid.IsReadOnly = false;
                
                schema.Columns.Add(colvarBid);
                
                TableSchema.TableColumn colvarGid = new TableSchema.TableColumn(schema);
                colvarGid.ColumnName = "gid";
                colvarGid.DataType = DbType.AnsiString;
                colvarGid.MaxLength = 100;
                colvarGid.AutoIncrement = false;
                colvarGid.IsNullable = false;
                colvarGid.IsPrimaryKey = false;
                colvarGid.IsForeignKey = false;
                colvarGid.IsReadOnly = false;
                
                schema.Columns.Add(colvarGid);
                
                TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
                colvarModifyTime.ColumnName = "modify_time";
                colvarModifyTime.DataType = DbType.DateTime;
                colvarModifyTime.MaxLength = 0;
                colvarModifyTime.AutoIncrement = false;
                colvarModifyTime.IsNullable = true;
                colvarModifyTime.IsPrimaryKey = false;
                colvarModifyTime.IsForeignKey = false;
                colvarModifyTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarModifyTime);
                
                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = false;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarCreateTime);
                
                TableSchema.TableColumn colvarGuid = new TableSchema.TableColumn(schema);
                colvarGuid.ColumnName = "GUID";
                colvarGuid.DataType = DbType.Guid;
                colvarGuid.MaxLength = 0;
                colvarGuid.AutoIncrement = false;
                colvarGuid.IsNullable = false;
                colvarGuid.IsPrimaryKey = false;
                colvarGuid.IsForeignKey = false;
                colvarGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarGuid);
                
                TableSchema.TableColumn colvarOrderId = new TableSchema.TableColumn(schema);
                colvarOrderId.ColumnName = "order_id";
                colvarOrderId.DataType = DbType.AnsiString;
                colvarOrderId.MaxLength = 30;
                colvarOrderId.AutoIncrement = false;
                colvarOrderId.IsNullable = true;
                colvarOrderId.IsPrimaryKey = false;
                colvarOrderId.IsForeignKey = false;
                colvarOrderId.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderId);
                
                TableSchema.TableColumn colvarOrderStatus = new TableSchema.TableColumn(schema);
                colvarOrderStatus.ColumnName = "order_status";
                colvarOrderStatus.DataType = DbType.Int32;
                colvarOrderStatus.MaxLength = 0;
                colvarOrderStatus.AutoIncrement = false;
                colvarOrderStatus.IsNullable = false;
                colvarOrderStatus.IsPrimaryKey = false;
                colvarOrderStatus.IsForeignKey = false;
                colvarOrderStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderStatus);
                
                TableSchema.TableColumn colvarTrustId = new TableSchema.TableColumn(schema);
                colvarTrustId.ColumnName = "trust_id";
                colvarTrustId.DataType = DbType.Guid;
                colvarTrustId.MaxLength = 0;
                colvarTrustId.AutoIncrement = false;
                colvarTrustId.IsNullable = false;
                colvarTrustId.IsPrimaryKey = false;
                colvarTrustId.IsForeignKey = false;
                colvarTrustId.IsReadOnly = false;
                
                schema.Columns.Add(colvarTrustId);
                
                TableSchema.TableColumn colvarCouponId = new TableSchema.TableColumn(schema);
                colvarCouponId.ColumnName = "coupon_id";
                colvarCouponId.DataType = DbType.Int32;
                colvarCouponId.MaxLength = 0;
                colvarCouponId.AutoIncrement = false;
                colvarCouponId.IsNullable = true;
                colvarCouponId.IsPrimaryKey = false;
                colvarCouponId.IsForeignKey = false;
                colvarCouponId.IsReadOnly = false;
                
                schema.Columns.Add(colvarCouponId);
                
                TableSchema.TableColumn colvarCouponSequenceNumber = new TableSchema.TableColumn(schema);
                colvarCouponSequenceNumber.ColumnName = "coupon_sequence_number";
                colvarCouponSequenceNumber.DataType = DbType.String;
                colvarCouponSequenceNumber.MaxLength = 50;
                colvarCouponSequenceNumber.AutoIncrement = false;
                colvarCouponSequenceNumber.IsNullable = true;
                colvarCouponSequenceNumber.IsPrimaryKey = false;
                colvarCouponSequenceNumber.IsForeignKey = false;
                colvarCouponSequenceNumber.IsReadOnly = false;
                
                schema.Columns.Add(colvarCouponSequenceNumber);
                
                TableSchema.TableColumn colvarUserId = new TableSchema.TableColumn(schema);
                colvarUserId.ColumnName = "user_id";
                colvarUserId.DataType = DbType.Int32;
                colvarUserId.MaxLength = 0;
                colvarUserId.AutoIncrement = false;
                colvarUserId.IsNullable = false;
                colvarUserId.IsPrimaryKey = false;
                colvarUserId.IsForeignKey = false;
                colvarUserId.IsReadOnly = false;
                
                schema.Columns.Add(colvarUserId);
                
                TableSchema.TableColumn colvarDeliveryType = new TableSchema.TableColumn(schema);
                colvarDeliveryType.ColumnName = "delivery_type";
                colvarDeliveryType.DataType = DbType.Int32;
                colvarDeliveryType.MaxLength = 0;
                colvarDeliveryType.AutoIncrement = false;
                colvarDeliveryType.IsNullable = false;
                colvarDeliveryType.IsPrimaryKey = false;
                colvarDeliveryType.IsForeignKey = false;
                colvarDeliveryType.IsReadOnly = false;
                
                schema.Columns.Add(colvarDeliveryType);
                
                TableSchema.TableColumn colvarAmount = new TableSchema.TableColumn(schema);
                colvarAmount.ColumnName = "amount";
                colvarAmount.DataType = DbType.Currency;
                colvarAmount.MaxLength = 0;
                colvarAmount.AutoIncrement = false;
                colvarAmount.IsNullable = false;
                colvarAmount.IsPrimaryKey = false;
                colvarAmount.IsForeignKey = false;
                colvarAmount.IsReadOnly = false;
                
                schema.Columns.Add(colvarAmount);
                
                TableSchema.TableColumn colvarCashTrustLogStatus = new TableSchema.TableColumn(schema);
                colvarCashTrustLogStatus.ColumnName = "cash_trust_log_status";
                colvarCashTrustLogStatus.DataType = DbType.Int32;
                colvarCashTrustLogStatus.MaxLength = 0;
                colvarCashTrustLogStatus.AutoIncrement = false;
                colvarCashTrustLogStatus.IsNullable = false;
                colvarCashTrustLogStatus.IsPrimaryKey = false;
                colvarCashTrustLogStatus.IsForeignKey = false;
                colvarCashTrustLogStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarCashTrustLogStatus);
                
                TableSchema.TableColumn colvarOrderTime = new TableSchema.TableColumn(schema);
                colvarOrderTime.ColumnName = "order_time";
                colvarOrderTime.DataType = DbType.DateTime;
                colvarOrderTime.MaxLength = 0;
                colvarOrderTime.AutoIncrement = false;
                colvarOrderTime.IsNullable = false;
                colvarOrderTime.IsPrimaryKey = false;
                colvarOrderTime.IsForeignKey = false;
                colvarOrderTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderTime);
                
                TableSchema.TableColumn colvarItemName = new TableSchema.TableColumn(schema);
                colvarItemName.ColumnName = "item_name";
                colvarItemName.DataType = DbType.String;
                colvarItemName.MaxLength = -1;
                colvarItemName.AutoIncrement = false;
                colvarItemName.IsNullable = false;
                colvarItemName.IsPrimaryKey = false;
                colvarItemName.IsForeignKey = false;
                colvarItemName.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemName);
                
                TableSchema.TableColumn colvarSendType = new TableSchema.TableColumn(schema);
                colvarSendType.ColumnName = "send_type";
                colvarSendType.DataType = DbType.Int32;
                colvarSendType.MaxLength = 0;
                colvarSendType.AutoIncrement = false;
                colvarSendType.IsNullable = false;
                colvarSendType.IsPrimaryKey = false;
                colvarSendType.IsForeignKey = false;
                colvarSendType.IsReadOnly = false;
                
                schema.Columns.Add(colvarSendType);
                
                TableSchema.TableColumn colvarFirstRsrc = new TableSchema.TableColumn(schema);
                colvarFirstRsrc.ColumnName = "first_rsrc";
                colvarFirstRsrc.DataType = DbType.String;
                colvarFirstRsrc.MaxLength = 50;
                colvarFirstRsrc.AutoIncrement = false;
                colvarFirstRsrc.IsNullable = true;
                colvarFirstRsrc.IsPrimaryKey = false;
                colvarFirstRsrc.IsForeignKey = false;
                colvarFirstRsrc.IsReadOnly = false;
                
                schema.Columns.Add(colvarFirstRsrc);


                TableSchema.TableColumn colvarLastRsrc = new TableSchema.TableColumn(schema);
                colvarLastRsrc.ColumnName = "last_rsrc";
                colvarLastRsrc.DataType = DbType.String;
                colvarLastRsrc.MaxLength = 50;
                colvarLastRsrc.AutoIncrement = false;
                colvarLastRsrc.IsNullable = true;
                colvarLastRsrc.IsPrimaryKey = false;
                colvarLastRsrc.IsForeignKey = false;
                colvarLastRsrc.IsReadOnly = false;

                schema.Columns.Add(colvarLastRsrc);

                TableSchema.TableColumn colvarLastExternalRsrc = new TableSchema.TableColumn(schema);
                colvarLastExternalRsrc.ColumnName = "last_external_rsrc";
                colvarLastExternalRsrc.DataType = DbType.String;
                colvarLastExternalRsrc.MaxLength = 50;
                colvarLastExternalRsrc.AutoIncrement = false;
                colvarLastExternalRsrc.IsNullable = true;
                colvarLastExternalRsrc.IsPrimaryKey = false;
                colvarLastExternalRsrc.IsForeignKey = false;
                colvarLastExternalRsrc.IsReadOnly = false;

                schema.Columns.Add(colvarLastExternalRsrc);


                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_ichannel_info",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewIchannelInfo()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewIchannelInfo(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewIchannelInfo(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewIchannelInfo(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id 
	    {
		    get
		    {
			    return GetColumnValue<int>("id");
		    }
            set 
		    {
			    SetColumnValue("id", value);
            }
        }
	      
        [XmlAttribute("OrderGuid")]
        [Bindable(true)]
        public Guid OrderGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("order_guid");
		    }
            set 
		    {
			    SetColumnValue("order_guid", value);
            }
        }
	      
        [XmlAttribute("Bid")]
        [Bindable(true)]
        public Guid Bid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("bid");
		    }
            set 
		    {
			    SetColumnValue("bid", value);
            }
        }
	      
        [XmlAttribute("Gid")]
        [Bindable(true)]
        public string Gid 
	    {
		    get
		    {
			    return GetColumnValue<string>("gid");
		    }
            set 
		    {
			    SetColumnValue("gid", value);
            }
        }
	      
        [XmlAttribute("ModifyTime")]
        [Bindable(true)]
        public DateTime? ModifyTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("modify_time");
		    }
            set 
		    {
			    SetColumnValue("modify_time", value);
            }
        }
	      
        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime CreateTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("create_time");
		    }
            set 
		    {
			    SetColumnValue("create_time", value);
            }
        }
	      
        [XmlAttribute("Guid")]
        [Bindable(true)]
        public Guid Guid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("GUID");
		    }
            set 
		    {
			    SetColumnValue("GUID", value);
            }
        }
	      
        [XmlAttribute("OrderId")]
        [Bindable(true)]
        public string OrderId 
	    {
		    get
		    {
			    return GetColumnValue<string>("order_id");
		    }
            set 
		    {
			    SetColumnValue("order_id", value);
            }
        }
	      
        [XmlAttribute("OrderStatus")]
        [Bindable(true)]
        public int OrderStatus 
	    {
		    get
		    {
			    return GetColumnValue<int>("order_status");
		    }
            set 
		    {
			    SetColumnValue("order_status", value);
            }
        }
	      
        [XmlAttribute("TrustId")]
        [Bindable(true)]
        public Guid TrustId 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("trust_id");
		    }
            set 
		    {
			    SetColumnValue("trust_id", value);
            }
        }
	      
        [XmlAttribute("CouponId")]
        [Bindable(true)]
        public int? CouponId 
	    {
		    get
		    {
			    return GetColumnValue<int?>("coupon_id");
		    }
            set 
		    {
			    SetColumnValue("coupon_id", value);
            }
        }
	      
        [XmlAttribute("CouponSequenceNumber")]
        [Bindable(true)]
        public string CouponSequenceNumber 
	    {
		    get
		    {
			    return GetColumnValue<string>("coupon_sequence_number");
		    }
            set 
		    {
			    SetColumnValue("coupon_sequence_number", value);
            }
        }
	      
        [XmlAttribute("UserId")]
        [Bindable(true)]
        public int UserId 
	    {
		    get
		    {
			    return GetColumnValue<int>("user_id");
		    }
            set 
		    {
			    SetColumnValue("user_id", value);
            }
        }
	      
        [XmlAttribute("DeliveryType")]
        [Bindable(true)]
        public int DeliveryType 
	    {
		    get
		    {
			    return GetColumnValue<int>("delivery_type");
		    }
            set 
		    {
			    SetColumnValue("delivery_type", value);
            }
        }
	      
        [XmlAttribute("Amount")]
        [Bindable(true)]
        public decimal Amount 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("amount");
		    }
            set 
		    {
			    SetColumnValue("amount", value);
            }
        }
	      
        [XmlAttribute("CashTrustLogStatus")]
        [Bindable(true)]
        public int CashTrustLogStatus 
	    {
		    get
		    {
			    return GetColumnValue<int>("cash_trust_log_status");
		    }
            set 
		    {
			    SetColumnValue("cash_trust_log_status", value);
            }
        }
	      
        [XmlAttribute("OrderTime")]
        [Bindable(true)]
        public DateTime OrderTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("order_time");
		    }
            set 
		    {
			    SetColumnValue("order_time", value);
            }
        }
	      
        [XmlAttribute("ItemName")]
        [Bindable(true)]
        public string ItemName 
	    {
		    get
		    {
			    return GetColumnValue<string>("item_name");
		    }
            set 
		    {
			    SetColumnValue("item_name", value);
            }
        }
	      
        [XmlAttribute("SendType")]
        [Bindable(true)]
        public int SendType 
	    {
		    get
		    {
			    return GetColumnValue<int>("send_type");
		    }
            set 
		    {
			    SetColumnValue("send_type", value);
            }
        }
	      
        [XmlAttribute("FirstRsrc")]
        [Bindable(true)]
        public string FirstRsrc 
	    {
		    get
		    {
			    return GetColumnValue<string>("first_rsrc");
		    }
            set 
		    {
			    SetColumnValue("first_rsrc", value);
            }
        }

        [XmlAttribute("LastRsrc")]
        [Bindable(true)]
        public string LastRsrc
        {
            get
            {
                return GetColumnValue<string>("last_rsrc");
            }
            set
            {
                SetColumnValue("last_rsrc", value);
            }
        }

        [XmlAttribute("LastExternalRsrc")]
        [Bindable(true)]
        public string LastExternalRsrc
        {
            get
            {
                return GetColumnValue<string>("last_external_rsrc");
            }
            set
            {
                SetColumnValue("last_external_rsrc", value);
            }
        }
        #endregion

        #region Columns Struct
        public struct Columns
	    {
		    
		    
            public static string Id = @"id";
            
            public static string OrderGuid = @"order_guid";
            
            public static string Bid = @"bid";
            
            public static string Gid = @"gid";
            
            public static string ModifyTime = @"modify_time";
            
            public static string CreateTime = @"create_time";
            
            public static string Guid = @"GUID";
            
            public static string OrderId = @"order_id";
            
            public static string OrderStatus = @"order_status";
            
            public static string TrustId = @"trust_id";
            
            public static string CouponId = @"coupon_id";
            
            public static string CouponSequenceNumber = @"coupon_sequence_number";
            
            public static string UserId = @"user_id";
            
            public static string DeliveryType = @"delivery_type";
            
            public static string Amount = @"amount";
            
            public static string CashTrustLogStatus = @"cash_trust_log_status";
            
            public static string OrderTime = @"order_time";
            
            public static string ItemName = @"item_name";
            
            public static string SendType = @"send_type";
            
            public static string FirstRsrc = @"first_rsrc";

            public static string LastRsrc = @"last_rsrc";

	        public static string LastExternalRsrc = @"last_external_rsrc";
        }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

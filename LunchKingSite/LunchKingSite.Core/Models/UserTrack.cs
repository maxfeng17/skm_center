using System;
using System.ComponentModel;
using System.Data;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the UserTrack class.
	/// </summary>
    [Serializable]
	public partial class UserTrackCollection : RepositoryList<UserTrack, UserTrackCollection>
	{	   
		public UserTrackCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>UserTrackCollection</returns>
		public UserTrackCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                UserTrack o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the user_track table.
	/// </summary>
	[Serializable]
	public partial class UserTrack : RepositoryRecord<UserTrack>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public UserTrack()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public UserTrack(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("user_track", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarLogId = new TableSchema.TableColumn(schema);
				colvarLogId.ColumnName = "log_id";
				colvarLogId.DataType = DbType.Int32;
				colvarLogId.MaxLength = 0;
				colvarLogId.AutoIncrement = true;
				colvarLogId.IsNullable = false;
				colvarLogId.IsPrimaryKey = true;
				colvarLogId.IsForeignKey = false;
				colvarLogId.IsReadOnly = false;
				colvarLogId.DefaultSetting = @"";
				colvarLogId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarLogId);
				
				TableSchema.TableColumn colvarSessionId = new TableSchema.TableColumn(schema);
				colvarSessionId.ColumnName = "session_id";
				colvarSessionId.DataType = DbType.AnsiString;
				colvarSessionId.MaxLength = 100;
				colvarSessionId.AutoIncrement = false;
				colvarSessionId.IsNullable = false;
				colvarSessionId.IsPrimaryKey = false;
				colvarSessionId.IsForeignKey = false;
				colvarSessionId.IsReadOnly = false;
				colvarSessionId.DefaultSetting = @"";
				colvarSessionId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSessionId);
				
				TableSchema.TableColumn colvarDisplayName = new TableSchema.TableColumn(schema);
				colvarDisplayName.ColumnName = "display_name";
				colvarDisplayName.DataType = DbType.String;
				colvarDisplayName.MaxLength = 100;
				colvarDisplayName.AutoIncrement = false;
				colvarDisplayName.IsNullable = true;
				colvarDisplayName.IsPrimaryKey = false;
				colvarDisplayName.IsForeignKey = false;
				colvarDisplayName.IsReadOnly = false;
				colvarDisplayName.DefaultSetting = @"";
				colvarDisplayName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDisplayName);
				
				TableSchema.TableColumn colvarUrl = new TableSchema.TableColumn(schema);
				colvarUrl.ColumnName = "url";
				colvarUrl.DataType = DbType.String;
				colvarUrl.MaxLength = 200;
				colvarUrl.AutoIncrement = false;
				colvarUrl.IsNullable = false;
				colvarUrl.IsPrimaryKey = false;
				colvarUrl.IsForeignKey = false;
				colvarUrl.IsReadOnly = false;
				colvarUrl.DefaultSetting = @"";
				colvarUrl.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUrl);
				
				TableSchema.TableColumn colvarQuerystring = new TableSchema.TableColumn(schema);
				colvarQuerystring.ColumnName = "querystring";
				colvarQuerystring.DataType = DbType.String;
				colvarQuerystring.MaxLength = 300;
				colvarQuerystring.AutoIncrement = false;
				colvarQuerystring.IsNullable = true;
				colvarQuerystring.IsPrimaryKey = false;
				colvarQuerystring.IsForeignKey = false;
				colvarQuerystring.IsReadOnly = false;
				colvarQuerystring.DefaultSetting = @"";
				colvarQuerystring.ForeignKeyTableName = "";
				schema.Columns.Add(colvarQuerystring);
				
				TableSchema.TableColumn colvarContext = new TableSchema.TableColumn(schema);
				colvarContext.ColumnName = "context";
				colvarContext.DataType = DbType.String;
				colvarContext.MaxLength = 1000;
				colvarContext.AutoIncrement = false;
				colvarContext.IsNullable = true;
				colvarContext.IsPrimaryKey = false;
				colvarContext.IsForeignKey = false;
				colvarContext.IsReadOnly = false;
				colvarContext.DefaultSetting = @"";
				colvarContext.ForeignKeyTableName = "";
				schema.Columns.Add(colvarContext);
				
				TableSchema.TableColumn colvarCreateDate = new TableSchema.TableColumn(schema);
				colvarCreateDate.ColumnName = "create_date";
				colvarCreateDate.DataType = DbType.DateTime;
				colvarCreateDate.MaxLength = 0;
				colvarCreateDate.AutoIncrement = false;
				colvarCreateDate.IsNullable = false;
				colvarCreateDate.IsPrimaryKey = false;
				colvarCreateDate.IsForeignKey = false;
				colvarCreateDate.IsReadOnly = false;
				colvarCreateDate.DefaultSetting = @"";
				colvarCreateDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateDate);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("user_track",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("LogId")]
		[Bindable(true)]
		public int LogId 
		{
			get { return GetColumnValue<int>(Columns.LogId); }
			set { SetColumnValue(Columns.LogId, value); }
		}
		  
		[XmlAttribute("SessionId")]
		[Bindable(true)]
		public string SessionId 
		{
			get { return GetColumnValue<string>(Columns.SessionId); }
			set { SetColumnValue(Columns.SessionId, value); }
		}
		  
		[XmlAttribute("DisplayName")]
		[Bindable(true)]
		public string DisplayName 
		{
			get { return GetColumnValue<string>(Columns.DisplayName); }
			set { SetColumnValue(Columns.DisplayName, value); }
		}
		  
		[XmlAttribute("Url")]
		[Bindable(true)]
		public string Url 
		{
			get { return GetColumnValue<string>(Columns.Url); }
			set { SetColumnValue(Columns.Url, value); }
		}
		  
		[XmlAttribute("Querystring")]
		[Bindable(true)]
		public string Querystring 
		{
			get { return GetColumnValue<string>(Columns.Querystring); }
			set { SetColumnValue(Columns.Querystring, value); }
		}
		  
		[XmlAttribute("Context")]
		[Bindable(true)]
		public string Context 
		{
			get { return GetColumnValue<string>(Columns.Context); }
			set { SetColumnValue(Columns.Context, value); }
		}
		  
		[XmlAttribute("CreateDate")]
		[Bindable(true)]
		public DateTime CreateDate 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateDate); }
			set { SetColumnValue(Columns.CreateDate, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn LogIdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn SessionIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn DisplayNameColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn UrlColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn QuerystringColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn ContextColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateDateColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string LogId = @"log_id";
			 public static string SessionId = @"session_id";
			 public static string DisplayName = @"display_name";
			 public static string Url = @"url";
			 public static string Querystring = @"querystring";
			 public static string Context = @"context";
			 public static string CreateDate = @"create_date";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

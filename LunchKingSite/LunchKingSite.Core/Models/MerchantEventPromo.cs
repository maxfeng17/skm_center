using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the MerchantEventPromo class.
	/// </summary>
    [Serializable]
	public partial class MerchantEventPromoCollection : RepositoryList<MerchantEventPromo, MerchantEventPromoCollection>
	{	   
		public MerchantEventPromoCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>MerchantEventPromoCollection</returns>
		public MerchantEventPromoCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                MerchantEventPromo o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
	}
	
	
	/// <summary>
	/// This is an ActiveRecord class which wraps the merchant_event_promo table.
	/// </summary>
	
	[Serializable]
	public partial class MerchantEventPromo : RepositoryRecord<MerchantEventPromo>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public MerchantEventPromo()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public MerchantEventPromo(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("merchant_event_promo", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "Id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarTitle = new TableSchema.TableColumn(schema);
				colvarTitle.ColumnName = "Title";
				colvarTitle.DataType = DbType.String;
				colvarTitle.MaxLength = 250;
				colvarTitle.AutoIncrement = false;
				colvarTitle.IsNullable = false;
				colvarTitle.IsPrimaryKey = false;
				colvarTitle.IsForeignKey = false;
				colvarTitle.IsReadOnly = false;
				colvarTitle.DefaultSetting = @"";
				colvarTitle.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTitle);
				
				TableSchema.TableColumn colvarUrl = new TableSchema.TableColumn(schema);
				colvarUrl.ColumnName = "Url";
				colvarUrl.DataType = DbType.AnsiString;
				colvarUrl.MaxLength = 1000;
				colvarUrl.AutoIncrement = false;
				colvarUrl.IsNullable = true;
				colvarUrl.IsPrimaryKey = false;
				colvarUrl.IsForeignKey = false;
				colvarUrl.IsReadOnly = false;
				colvarUrl.DefaultSetting = @"";
				colvarUrl.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUrl);
				
				TableSchema.TableColumn colvarStartDate = new TableSchema.TableColumn(schema);
				colvarStartDate.ColumnName = "StartDate";
				colvarStartDate.DataType = DbType.DateTime;
				colvarStartDate.MaxLength = 0;
				colvarStartDate.AutoIncrement = false;
				colvarStartDate.IsNullable = false;
				colvarStartDate.IsPrimaryKey = false;
				colvarStartDate.IsForeignKey = false;
				colvarStartDate.IsReadOnly = false;
				colvarStartDate.DefaultSetting = @"";
				colvarStartDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStartDate);
				
				TableSchema.TableColumn colvarEndDate = new TableSchema.TableColumn(schema);
				colvarEndDate.ColumnName = "EndDate";
				colvarEndDate.DataType = DbType.DateTime;
				colvarEndDate.MaxLength = 0;
				colvarEndDate.AutoIncrement = false;
				colvarEndDate.IsNullable = false;
				colvarEndDate.IsPrimaryKey = false;
				colvarEndDate.IsForeignKey = false;
				colvarEndDate.IsReadOnly = false;
				colvarEndDate.DefaultSetting = @"";
				colvarEndDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEndDate);
				
				TableSchema.TableColumn colvarLocation = new TableSchema.TableColumn(schema);
				colvarLocation.ColumnName = "Location";
				colvarLocation.DataType = DbType.AnsiString;
				colvarLocation.MaxLength = 2500;
				colvarLocation.AutoIncrement = false;
				colvarLocation.IsNullable = true;
				colvarLocation.IsPrimaryKey = false;
				colvarLocation.IsForeignKey = false;
				colvarLocation.IsReadOnly = false;
				colvarLocation.DefaultSetting = @"";
				colvarLocation.ForeignKeyTableName = "";
				schema.Columns.Add(colvarLocation);
				
				TableSchema.TableColumn colvarMainPic = new TableSchema.TableColumn(schema);
				colvarMainPic.ColumnName = "MainPic";
				colvarMainPic.DataType = DbType.AnsiString;
				colvarMainPic.MaxLength = 250;
				colvarMainPic.AutoIncrement = false;
				colvarMainPic.IsNullable = true;
				colvarMainPic.IsPrimaryKey = false;
				colvarMainPic.IsForeignKey = false;
				colvarMainPic.IsReadOnly = false;
				colvarMainPic.DefaultSetting = @"";
				colvarMainPic.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMainPic);
				
				TableSchema.TableColumn colvarType = new TableSchema.TableColumn(schema);
				colvarType.ColumnName = "Type";
				colvarType.DataType = DbType.Int32;
				colvarType.MaxLength = 0;
				colvarType.AutoIncrement = false;
				colvarType.IsNullable = false;
				colvarType.IsPrimaryKey = false;
				colvarType.IsForeignKey = false;
				colvarType.IsReadOnly = false;
				colvarType.DefaultSetting = @"";
				colvarType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarType);
				
				TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
				colvarStatus.ColumnName = "Status";
				colvarStatus.DataType = DbType.Int32;
				colvarStatus.MaxLength = 0;
				colvarStatus.AutoIncrement = false;
				colvarStatus.IsNullable = false;
				colvarStatus.IsPrimaryKey = false;
				colvarStatus.IsForeignKey = false;
				colvarStatus.IsReadOnly = false;
				colvarStatus.DefaultSetting = @"";
				colvarStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStatus);
				
				TableSchema.TableColumn colvarSort = new TableSchema.TableColumn(schema);
				colvarSort.ColumnName = "Sort";
				colvarSort.DataType = DbType.Int32;
				colvarSort.MaxLength = 0;
				colvarSort.AutoIncrement = false;
				colvarSort.IsNullable = false;
				colvarSort.IsPrimaryKey = false;
				colvarSort.IsForeignKey = false;
				colvarSort.IsReadOnly = false;
				colvarSort.DefaultSetting = @"";
				colvarSort.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSort);
				
				TableSchema.TableColumn colvarCreateDate = new TableSchema.TableColumn(schema);
				colvarCreateDate.ColumnName = "Create_date";
				colvarCreateDate.DataType = DbType.DateTime;
				colvarCreateDate.MaxLength = 0;
				colvarCreateDate.AutoIncrement = false;
				colvarCreateDate.IsNullable = false;
				colvarCreateDate.IsPrimaryKey = false;
				colvarCreateDate.IsForeignKey = false;
				colvarCreateDate.IsReadOnly = false;
				colvarCreateDate.DefaultSetting = @"";
				colvarCreateDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateDate);
				
				TableSchema.TableColumn colvarCreateUser = new TableSchema.TableColumn(schema);
				colvarCreateUser.ColumnName = "Create_user";
				colvarCreateUser.DataType = DbType.AnsiString;
				colvarCreateUser.MaxLength = 50;
				colvarCreateUser.AutoIncrement = false;
				colvarCreateUser.IsNullable = false;
				colvarCreateUser.IsPrimaryKey = false;
				colvarCreateUser.IsForeignKey = false;
				colvarCreateUser.IsReadOnly = false;
				colvarCreateUser.DefaultSetting = @"";
				colvarCreateUser.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateUser);
				
				TableSchema.TableColumn colvarModifyDate = new TableSchema.TableColumn(schema);
				colvarModifyDate.ColumnName = "Modify_date";
				colvarModifyDate.DataType = DbType.DateTime;
				colvarModifyDate.MaxLength = 0;
				colvarModifyDate.AutoIncrement = false;
				colvarModifyDate.IsNullable = false;
				colvarModifyDate.IsPrimaryKey = false;
				colvarModifyDate.IsForeignKey = false;
				colvarModifyDate.IsReadOnly = false;
				colvarModifyDate.DefaultSetting = @"";
				colvarModifyDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyDate);
				
				TableSchema.TableColumn colvarModifyUser = new TableSchema.TableColumn(schema);
				colvarModifyUser.ColumnName = "Modify_user";
				colvarModifyUser.DataType = DbType.AnsiString;
				colvarModifyUser.MaxLength = 50;
				colvarModifyUser.AutoIncrement = false;
				colvarModifyUser.IsNullable = false;
				colvarModifyUser.IsPrimaryKey = false;
				colvarModifyUser.IsForeignKey = false;
				colvarModifyUser.IsReadOnly = false;
				colvarModifyUser.DefaultSetting = @"";
				colvarModifyUser.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyUser);
				
				TableSchema.TableColumn colvarGuid = new TableSchema.TableColumn(schema);
				colvarGuid.ColumnName = "guid";
				colvarGuid.DataType = DbType.Guid;
				colvarGuid.MaxLength = 0;
				colvarGuid.AutoIncrement = false;
				colvarGuid.IsNullable = true;
				colvarGuid.IsPrimaryKey = false;
				colvarGuid.IsForeignKey = false;
				colvarGuid.IsReadOnly = false;
				colvarGuid.DefaultSetting = @"";
				colvarGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGuid);
				
				TableSchema.TableColumn colvarSellerGuid = new TableSchema.TableColumn(schema);
				colvarSellerGuid.ColumnName = "Seller_Guid";
				colvarSellerGuid.DataType = DbType.Guid;
				colvarSellerGuid.MaxLength = 0;
				colvarSellerGuid.AutoIncrement = false;
				colvarSellerGuid.IsNullable = true;
				colvarSellerGuid.IsPrimaryKey = false;
				colvarSellerGuid.IsForeignKey = false;
				colvarSellerGuid.IsReadOnly = false;
				colvarSellerGuid.DefaultSetting = @"";
				colvarSellerGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSellerGuid);
				
				TableSchema.TableColumn colvarIsMustLogin = new TableSchema.TableColumn(schema);
				colvarIsMustLogin.ColumnName = "is_must_login";
				colvarIsMustLogin.DataType = DbType.Boolean;
				colvarIsMustLogin.MaxLength = 0;
				colvarIsMustLogin.AutoIncrement = false;
				colvarIsMustLogin.IsNullable = false;
				colvarIsMustLogin.IsPrimaryKey = false;
				colvarIsMustLogin.IsForeignKey = false;
				colvarIsMustLogin.IsReadOnly = false;
				
						colvarIsMustLogin.DefaultSetting = @"((0))";
				colvarIsMustLogin.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsMustLogin);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("merchant_event_promo",schema);
			}
		}
		#endregion
		
		#region Props
		
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		
		[XmlAttribute("Title")]
		[Bindable(true)]
		public string Title 
		{
			get { return GetColumnValue<string>(Columns.Title); }
			set { SetColumnValue(Columns.Title, value); }
		}
		
		[XmlAttribute("Url")]
		[Bindable(true)]
		public string Url 
		{
			get { return GetColumnValue<string>(Columns.Url); }
			set { SetColumnValue(Columns.Url, value); }
		}
		
		[XmlAttribute("StartDate")]
		[Bindable(true)]
		public DateTime StartDate 
		{
			get { return GetColumnValue<DateTime>(Columns.StartDate); }
			set { SetColumnValue(Columns.StartDate, value); }
		}
		
		[XmlAttribute("EndDate")]
		[Bindable(true)]
		public DateTime EndDate 
		{
			get { return GetColumnValue<DateTime>(Columns.EndDate); }
			set { SetColumnValue(Columns.EndDate, value); }
		}
		
		[XmlAttribute("Location")]
		[Bindable(true)]
		public string Location 
		{
			get { return GetColumnValue<string>(Columns.Location); }
			set { SetColumnValue(Columns.Location, value); }
		}
		
		[XmlAttribute("MainPic")]
		[Bindable(true)]
		public string MainPic 
		{
			get { return GetColumnValue<string>(Columns.MainPic); }
			set { SetColumnValue(Columns.MainPic, value); }
		}
		
		[XmlAttribute("Type")]
		[Bindable(true)]
		public int Type 
		{
			get { return GetColumnValue<int>(Columns.Type); }
			set { SetColumnValue(Columns.Type, value); }
		}
		
		[XmlAttribute("Status")]
		[Bindable(true)]
		public int Status 
		{
			get { return GetColumnValue<int>(Columns.Status); }
			set { SetColumnValue(Columns.Status, value); }
		}
		
		[XmlAttribute("Sort")]
		[Bindable(true)]
		public int Sort 
		{
			get { return GetColumnValue<int>(Columns.Sort); }
			set { SetColumnValue(Columns.Sort, value); }
		}
		
		[XmlAttribute("CreateDate")]
		[Bindable(true)]
		public DateTime CreateDate 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateDate); }
			set { SetColumnValue(Columns.CreateDate, value); }
		}
		
		[XmlAttribute("CreateUser")]
		[Bindable(true)]
		public string CreateUser 
		{
			get { return GetColumnValue<string>(Columns.CreateUser); }
			set { SetColumnValue(Columns.CreateUser, value); }
		}
		
		[XmlAttribute("ModifyDate")]
		[Bindable(true)]
		public DateTime ModifyDate 
		{
			get { return GetColumnValue<DateTime>(Columns.ModifyDate); }
			set { SetColumnValue(Columns.ModifyDate, value); }
		}
		
		[XmlAttribute("ModifyUser")]
		[Bindable(true)]
		public string ModifyUser 
		{
			get { return GetColumnValue<string>(Columns.ModifyUser); }
			set { SetColumnValue(Columns.ModifyUser, value); }
		}
		
		[XmlAttribute("Guid")]
		[Bindable(true)]
		public Guid? Guid 
		{
			get { return GetColumnValue<Guid?>(Columns.Guid); }
			set { SetColumnValue(Columns.Guid, value); }
		}
		
		[XmlAttribute("SellerGuid")]
		[Bindable(true)]
		public Guid? SellerGuid 
		{
			get { return GetColumnValue<Guid?>(Columns.SellerGuid); }
			set { SetColumnValue(Columns.SellerGuid, value); }
		}
		
		[XmlAttribute("IsMustLogin")]
		[Bindable(true)]
		public bool IsMustLogin 
		{
			get { return GetColumnValue<bool>(Columns.IsMustLogin); }
			set { SetColumnValue(Columns.IsMustLogin, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn TitleColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn UrlColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn StartDateColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn EndDateColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn LocationColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn MainPicColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn TypeColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn StatusColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn SortColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateDateColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateUserColumn
        {
            get { return Schema.Columns[11]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyDateColumn
        {
            get { return Schema.Columns[12]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyUserColumn
        {
            get { return Schema.Columns[13]; }
        }
        
        
        
        public static TableSchema.TableColumn GuidColumn
        {
            get { return Schema.Columns[14]; }
        }
        
        
        
        public static TableSchema.TableColumn SellerGuidColumn
        {
            get { return Schema.Columns[15]; }
        }
        
        
        
        public static TableSchema.TableColumn IsMustLoginColumn
        {
            get { return Schema.Columns[16]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"Id";
			 public static string Title = @"Title";
			 public static string Url = @"Url";
			 public static string StartDate = @"StartDate";
			 public static string EndDate = @"EndDate";
			 public static string Location = @"Location";
			 public static string MainPic = @"MainPic";
			 public static string Type = @"Type";
			 public static string Status = @"Status";
			 public static string Sort = @"Sort";
			 public static string CreateDate = @"Create_date";
			 public static string CreateUser = @"Create_user";
			 public static string ModifyDate = @"Modify_date";
			 public static string ModifyUser = @"Modify_user";
			 public static string Guid = @"guid";
			 public static string SellerGuid = @"Seller_Guid";
			 public static string IsMustLogin = @"is_must_login";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

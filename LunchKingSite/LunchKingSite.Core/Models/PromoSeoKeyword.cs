using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the PromoSeoKeyword class.
    /// </summary>
    [Serializable]
    public partial class PromoSeoKeywordCollection : RepositoryList<PromoSeoKeyword, PromoSeoKeywordCollection>
    {
        public PromoSeoKeywordCollection() { }

        /// <summary>
        /// Filters an existing collection based on the set criteria. This is an in-memory filter
        /// Thanks to developingchris for this!
        /// </summary>
        /// <returns>PromoSeoKeywordCollection</returns>
        public PromoSeoKeywordCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                PromoSeoKeyword o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }


    }
    /// <summary>
    /// This is an ActiveRecord class which wraps the promo_seo_keyword table.
    /// </summary>
    [Serializable]
    public partial class PromoSeoKeyword : RepositoryRecord<PromoSeoKeyword>, IRecordBase
    {
        #region .ctors and Default Settings

        public PromoSeoKeyword()
        {
            SetSQLProps();
            InitSetDefaults();
            MarkNew();
        }

        private void InitSetDefaults() { SetDefaults(); }

        public PromoSeoKeyword(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
                ForceDefaults();
            MarkNew();
        }

        protected static void SetSQLProps() { GetTableSchema(); }

        #endregion

        #region Schema and Query Accessor
        public static Query CreateQuery() { return new Query(Schema); }
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                    SetSQLProps();
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("promo_seo_keyword", TableType.Table, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = true;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = true;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                colvarId.DefaultSetting = @"";
                colvarId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarId);

                TableSchema.TableColumn colvarPromoType = new TableSchema.TableColumn(schema);
                colvarPromoType.ColumnName = "promo_type";
                colvarPromoType.DataType = DbType.Int32;
                colvarPromoType.MaxLength = 0;
                colvarPromoType.AutoIncrement = false;
                colvarPromoType.IsNullable = false;
                colvarPromoType.IsPrimaryKey = false;
                colvarPromoType.IsForeignKey = false;
                colvarPromoType.IsReadOnly = false;
                colvarPromoType.DefaultSetting = @"";
                colvarPromoType.ForeignKeyTableName = "";
                schema.Columns.Add(colvarPromoType);

                TableSchema.TableColumn colvarActivityId = new TableSchema.TableColumn(schema);
                colvarActivityId.ColumnName = "activity_id";
                colvarActivityId.DataType = DbType.Int32;
                colvarActivityId.MaxLength = 0;
                colvarActivityId.AutoIncrement = false;
                colvarActivityId.IsNullable = false;
                colvarActivityId.IsPrimaryKey = false;
                colvarActivityId.IsForeignKey = false;
                colvarActivityId.IsReadOnly = false;
                colvarActivityId.DefaultSetting = @"";
                colvarActivityId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarActivityId);

                TableSchema.TableColumn colvarDescription = new TableSchema.TableColumn(schema);
                colvarDescription.ColumnName = "description";
                colvarDescription.DataType = DbType.String;
                colvarDescription.MaxLength = -1;
                colvarDescription.AutoIncrement = false;
                colvarDescription.IsNullable = false;
                colvarDescription.IsPrimaryKey = false;
                colvarDescription.IsForeignKey = false;
                colvarDescription.IsReadOnly = false;
                colvarDescription.DefaultSetting = @"";
                colvarDescription.ForeignKeyTableName = "";
                schema.Columns.Add(colvarDescription);

                TableSchema.TableColumn colvarKeyword = new TableSchema.TableColumn(schema);
                colvarKeyword.ColumnName = "keyword";
                colvarKeyword.DataType = DbType.String;
                colvarKeyword.MaxLength = 256;
                colvarKeyword.AutoIncrement = false;
                colvarKeyword.IsNullable = false;
                colvarKeyword.IsPrimaryKey = false;
                colvarKeyword.IsForeignKey = false;
                colvarKeyword.IsReadOnly = false;
                colvarKeyword.DefaultSetting = @"";
                colvarKeyword.ForeignKeyTableName = "";
                schema.Columns.Add(colvarKeyword);

                TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
                colvarCreateId.ColumnName = "create_id";
                colvarCreateId.DataType = DbType.String;
                colvarCreateId.MaxLength = 30;
                colvarCreateId.AutoIncrement = false;
                colvarCreateId.IsNullable = false;
                colvarCreateId.IsPrimaryKey = false;
                colvarCreateId.IsForeignKey = false;
                colvarCreateId.IsReadOnly = false;
                colvarCreateId.DefaultSetting = @"";
                colvarCreateId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateId);

                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = false;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                colvarCreateTime.DefaultSetting = @"";
                colvarCreateTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateTime);

                TableSchema.TableColumn colvarModifyId = new TableSchema.TableColumn(schema);
                colvarModifyId.ColumnName = "modify_id";
                colvarModifyId.DataType = DbType.String;
                colvarModifyId.MaxLength = 30;
                colvarModifyId.AutoIncrement = false;
                colvarModifyId.IsNullable = false;
                colvarModifyId.IsPrimaryKey = false;
                colvarModifyId.IsForeignKey = false;
                colvarModifyId.IsReadOnly = false;
                colvarModifyId.DefaultSetting = @"";
                colvarModifyId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarModifyId);

                TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
                colvarModifyTime.ColumnName = "modify_time";
                colvarModifyTime.DataType = DbType.DateTime;
                colvarModifyTime.MaxLength = 0;
                colvarModifyTime.AutoIncrement = false;
                colvarModifyTime.IsNullable = false;
                colvarModifyTime.IsPrimaryKey = false;
                colvarModifyTime.IsForeignKey = false;
                colvarModifyTime.IsReadOnly = false;
                colvarModifyTime.DefaultSetting = @"";
                colvarModifyTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarModifyTime);

                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("promo_seo_keyword", schema);
            }
        }
        #endregion

        #region Props

        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id
        {
            get { return GetColumnValue<int>(Columns.Id); }
            set { SetColumnValue(Columns.Id, value); }
        }

        [XmlAttribute("PromoType")]
        [Bindable(true)]
        public int PromoType
        {
            get { return GetColumnValue<int>(Columns.PromoType); }
            set { SetColumnValue(Columns.PromoType, value); }
        }

        [XmlAttribute("ActivityId")]
        [Bindable(true)]
        public int ActivityId
        {
            get { return GetColumnValue<int>(Columns.ActivityId); }
            set { SetColumnValue(Columns.ActivityId, value); }
        }

        [XmlAttribute("Description")]
        [Bindable(true)]
        public string Description
        {
            get { return GetColumnValue<string>(Columns.Description); }
            set { SetColumnValue(Columns.Description, value); }
        }

        [XmlAttribute("Keyword")]
        [Bindable(true)]
        public string Keyword
        {
            get { return GetColumnValue<string>(Columns.Keyword); }
            set { SetColumnValue(Columns.Keyword, value); }
        }

        [XmlAttribute("CreateId")]
        [Bindable(true)]
        public string CreateId
        {
            get { return GetColumnValue<string>(Columns.CreateId); }
            set { SetColumnValue(Columns.CreateId, value); }
        }

        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime CreateTime
        {
            get { return GetColumnValue<DateTime>(Columns.CreateTime); }
            set { SetColumnValue(Columns.CreateTime, value); }
        }

        [XmlAttribute("ModifyId")]
        [Bindable(true)]
        public string ModifyId
        {
            get { return GetColumnValue<string>(Columns.ModifyId); }
            set { SetColumnValue(Columns.ModifyId, value); }
        }

        [XmlAttribute("ModifyTime")]
        [Bindable(true)]
        public DateTime ModifyTime
        {
            get { return GetColumnValue<DateTime>(Columns.ModifyTime); }
            set { SetColumnValue(Columns.ModifyTime, value); }
        }

        #endregion




        //no foreign key tables defined (0)



        //no ManyToMany tables defined (0)





        #region Typed Columns


        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }



        public static TableSchema.TableColumn PromoTypeColumn
        {
            get { return Schema.Columns[1]; }
        }



        public static TableSchema.TableColumn ActivityIdColumn
        {
            get { return Schema.Columns[2]; }
        }



        public static TableSchema.TableColumn DescriptionColumn
        {
            get { return Schema.Columns[3]; }
        }



        public static TableSchema.TableColumn KeywordColumn
        {
            get { return Schema.Columns[4]; }
        }



        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[5]; }
        }



        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[6]; }
        }



        public static TableSchema.TableColumn ModifyIdColumn
        {
            get { return Schema.Columns[7]; }
        }



        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[8]; }
        }



        #endregion
        #region Columns Struct
        public struct Columns
        {
            public static string Id = @"id";
            public static string PromoType = @"promo_type";
            public static string ActivityId = @"activity_id";
            public static string Description = @"description";
            public static string Keyword = @"keyword";
            public static string CreateId = @"create_id";
            public static string CreateTime = @"create_time";
            public static string ModifyId = @"modify_id";
            public static string ModifyTime = @"modify_time";

        }
        #endregion

        #region Update PK Collections

        #endregion

        #region Deep Save

        #endregion
    }
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewPponOrderDetail class.
    /// </summary>
    [Serializable]
    public partial class ViewPponOrderDetailCollection : ReadOnlyList<ViewPponOrderDetail, ViewPponOrderDetailCollection>
    {        
        public ViewPponOrderDetailCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_ppon_order_detail view.
    /// </summary>
    [Serializable]
    public partial class ViewPponOrderDetail : ReadOnlyRecord<ViewPponOrderDetail>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_ppon_order_detail", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarOrderDetailGuid = new TableSchema.TableColumn(schema);
                colvarOrderDetailGuid.ColumnName = "order_detail_guid";
                colvarOrderDetailGuid.DataType = DbType.Guid;
                colvarOrderDetailGuid.MaxLength = 0;
                colvarOrderDetailGuid.AutoIncrement = false;
                colvarOrderDetailGuid.IsNullable = false;
                colvarOrderDetailGuid.IsPrimaryKey = false;
                colvarOrderDetailGuid.IsForeignKey = false;
                colvarOrderDetailGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderDetailGuid);
                
                TableSchema.TableColumn colvarOrderGuid = new TableSchema.TableColumn(schema);
                colvarOrderGuid.ColumnName = "order_GUID";
                colvarOrderGuid.DataType = DbType.Guid;
                colvarOrderGuid.MaxLength = 0;
                colvarOrderGuid.AutoIncrement = false;
                colvarOrderGuid.IsNullable = false;
                colvarOrderGuid.IsPrimaryKey = false;
                colvarOrderGuid.IsForeignKey = false;
                colvarOrderGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderGuid);
                
                TableSchema.TableColumn colvarItemName = new TableSchema.TableColumn(schema);
                colvarItemName.ColumnName = "item_name";
                colvarItemName.DataType = DbType.String;
                colvarItemName.MaxLength = 4000;
                colvarItemName.AutoIncrement = false;
                colvarItemName.IsNullable = true;
                colvarItemName.IsPrimaryKey = false;
                colvarItemName.IsForeignKey = false;
                colvarItemName.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemName);
                
                TableSchema.TableColumn colvarItemGuid = new TableSchema.TableColumn(schema);
                colvarItemGuid.ColumnName = "item_GUID";
                colvarItemGuid.DataType = DbType.Guid;
                colvarItemGuid.MaxLength = 0;
                colvarItemGuid.AutoIncrement = false;
                colvarItemGuid.IsNullable = true;
                colvarItemGuid.IsPrimaryKey = false;
                colvarItemGuid.IsForeignKey = false;
                colvarItemGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemGuid);
                
                TableSchema.TableColumn colvarItemUnitPrice = new TableSchema.TableColumn(schema);
                colvarItemUnitPrice.ColumnName = "item_unit_price";
                colvarItemUnitPrice.DataType = DbType.Currency;
                colvarItemUnitPrice.MaxLength = 0;
                colvarItemUnitPrice.AutoIncrement = false;
                colvarItemUnitPrice.IsNullable = false;
                colvarItemUnitPrice.IsPrimaryKey = false;
                colvarItemUnitPrice.IsForeignKey = false;
                colvarItemUnitPrice.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemUnitPrice);
                
                TableSchema.TableColumn colvarItemQuantity = new TableSchema.TableColumn(schema);
                colvarItemQuantity.ColumnName = "item_quantity";
                colvarItemQuantity.DataType = DbType.Int32;
                colvarItemQuantity.MaxLength = 0;
                colvarItemQuantity.AutoIncrement = false;
                colvarItemQuantity.IsNullable = false;
                colvarItemQuantity.IsPrimaryKey = false;
                colvarItemQuantity.IsForeignKey = false;
                colvarItemQuantity.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemQuantity);
                
                TableSchema.TableColumn colvarOrderDetailTotal = new TableSchema.TableColumn(schema);
                colvarOrderDetailTotal.ColumnName = "order_detail_total";
                colvarOrderDetailTotal.DataType = DbType.Currency;
                colvarOrderDetailTotal.MaxLength = 0;
                colvarOrderDetailTotal.AutoIncrement = false;
                colvarOrderDetailTotal.IsNullable = true;
                colvarOrderDetailTotal.IsPrimaryKey = false;
                colvarOrderDetailTotal.IsForeignKey = false;
                colvarOrderDetailTotal.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderDetailTotal);
                
                TableSchema.TableColumn colvarConsumerName = new TableSchema.TableColumn(schema);
                colvarConsumerName.ColumnName = "consumer_name";
                colvarConsumerName.DataType = DbType.String;
                colvarConsumerName.MaxLength = 100;
                colvarConsumerName.AutoIncrement = false;
                colvarConsumerName.IsNullable = true;
                colvarConsumerName.IsPrimaryKey = false;
                colvarConsumerName.IsForeignKey = false;
                colvarConsumerName.IsReadOnly = false;
                
                schema.Columns.Add(colvarConsumerName);
                
                TableSchema.TableColumn colvarConsumerTeleExt = new TableSchema.TableColumn(schema);
                colvarConsumerTeleExt.ColumnName = "consumer_tele_ext";
                colvarConsumerTeleExt.DataType = DbType.AnsiString;
                colvarConsumerTeleExt.MaxLength = 10;
                colvarConsumerTeleExt.AutoIncrement = false;
                colvarConsumerTeleExt.IsNullable = true;
                colvarConsumerTeleExt.IsPrimaryKey = false;
                colvarConsumerTeleExt.IsForeignKey = false;
                colvarConsumerTeleExt.IsReadOnly = false;
                
                schema.Columns.Add(colvarConsumerTeleExt);
                
                TableSchema.TableColumn colvarConsumerGroup = new TableSchema.TableColumn(schema);
                colvarConsumerGroup.ColumnName = "consumer_group";
                colvarConsumerGroup.DataType = DbType.String;
                colvarConsumerGroup.MaxLength = 50;
                colvarConsumerGroup.AutoIncrement = false;
                colvarConsumerGroup.IsNullable = true;
                colvarConsumerGroup.IsPrimaryKey = false;
                colvarConsumerGroup.IsForeignKey = false;
                colvarConsumerGroup.IsReadOnly = false;
                
                schema.Columns.Add(colvarConsumerGroup);
                
                TableSchema.TableColumn colvarOrderDetailStatus = new TableSchema.TableColumn(schema);
                colvarOrderDetailStatus.ColumnName = "order_detail_status";
                colvarOrderDetailStatus.DataType = DbType.Int32;
                colvarOrderDetailStatus.MaxLength = 0;
                colvarOrderDetailStatus.AutoIncrement = false;
                colvarOrderDetailStatus.IsNullable = false;
                colvarOrderDetailStatus.IsPrimaryKey = false;
                colvarOrderDetailStatus.IsForeignKey = false;
                colvarOrderDetailStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderDetailStatus);
                
                TableSchema.TableColumn colvarItemId = new TableSchema.TableColumn(schema);
                colvarItemId.ColumnName = "item_id";
                colvarItemId.DataType = DbType.String;
                colvarItemId.MaxLength = 100;
                colvarItemId.AutoIncrement = false;
                colvarItemId.IsNullable = true;
                colvarItemId.IsPrimaryKey = false;
                colvarItemId.IsForeignKey = false;
                colvarItemId.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemId);
                
                TableSchema.TableColumn colvarOrderId = new TableSchema.TableColumn(schema);
                colvarOrderId.ColumnName = "order_id";
                colvarOrderId.DataType = DbType.AnsiString;
                colvarOrderId.MaxLength = 30;
                colvarOrderId.AutoIncrement = false;
                colvarOrderId.IsNullable = true;
                colvarOrderId.IsPrimaryKey = false;
                colvarOrderId.IsForeignKey = false;
                colvarOrderId.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderId);
                
                TableSchema.TableColumn colvarSellerGuid = new TableSchema.TableColumn(schema);
                colvarSellerGuid.ColumnName = "seller_GUID";
                colvarSellerGuid.DataType = DbType.Guid;
                colvarSellerGuid.MaxLength = 0;
                colvarSellerGuid.AutoIncrement = false;
                colvarSellerGuid.IsNullable = false;
                colvarSellerGuid.IsPrimaryKey = false;
                colvarSellerGuid.IsForeignKey = false;
                colvarSellerGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerGuid);
                
                TableSchema.TableColumn colvarSellerName = new TableSchema.TableColumn(schema);
                colvarSellerName.ColumnName = "seller_name";
                colvarSellerName.DataType = DbType.String;
                colvarSellerName.MaxLength = 50;
                colvarSellerName.AutoIncrement = false;
                colvarSellerName.IsNullable = false;
                colvarSellerName.IsPrimaryKey = false;
                colvarSellerName.IsForeignKey = false;
                colvarSellerName.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerName);
                
                TableSchema.TableColumn colvarMemberEmail = new TableSchema.TableColumn(schema);
                colvarMemberEmail.ColumnName = "member_email";
                colvarMemberEmail.DataType = DbType.String;
                colvarMemberEmail.MaxLength = 256;
                colvarMemberEmail.AutoIncrement = false;
                colvarMemberEmail.IsNullable = false;
                colvarMemberEmail.IsPrimaryKey = false;
                colvarMemberEmail.IsForeignKey = false;
                colvarMemberEmail.IsReadOnly = false;
                
                schema.Columns.Add(colvarMemberEmail);
                
                TableSchema.TableColumn colvarMemberName = new TableSchema.TableColumn(schema);
                colvarMemberName.ColumnName = "member_name";
                colvarMemberName.DataType = DbType.String;
                colvarMemberName.MaxLength = 50;
                colvarMemberName.AutoIncrement = false;
                colvarMemberName.IsNullable = false;
                colvarMemberName.IsPrimaryKey = false;
                colvarMemberName.IsForeignKey = false;
                colvarMemberName.IsReadOnly = false;
                
                schema.Columns.Add(colvarMemberName);
                
                TableSchema.TableColumn colvarPhoneNumber = new TableSchema.TableColumn(schema);
                colvarPhoneNumber.ColumnName = "phone_number";
                colvarPhoneNumber.DataType = DbType.AnsiString;
                colvarPhoneNumber.MaxLength = 50;
                colvarPhoneNumber.AutoIncrement = false;
                colvarPhoneNumber.IsNullable = true;
                colvarPhoneNumber.IsPrimaryKey = false;
                colvarPhoneNumber.IsForeignKey = false;
                colvarPhoneNumber.IsReadOnly = false;
                
                schema.Columns.Add(colvarPhoneNumber);
                
                TableSchema.TableColumn colvarMobileNumber = new TableSchema.TableColumn(schema);
                colvarMobileNumber.ColumnName = "mobile_number";
                colvarMobileNumber.DataType = DbType.AnsiString;
                colvarMobileNumber.MaxLength = 50;
                colvarMobileNumber.AutoIncrement = false;
                colvarMobileNumber.IsNullable = true;
                colvarMobileNumber.IsPrimaryKey = false;
                colvarMobileNumber.IsForeignKey = false;
                colvarMobileNumber.IsReadOnly = false;
                
                schema.Columns.Add(colvarMobileNumber);
                
                TableSchema.TableColumn colvarDeliveryAddress = new TableSchema.TableColumn(schema);
                colvarDeliveryAddress.ColumnName = "delivery_address";
                colvarDeliveryAddress.DataType = DbType.String;
                colvarDeliveryAddress.MaxLength = 200;
                colvarDeliveryAddress.AutoIncrement = false;
                colvarDeliveryAddress.IsNullable = true;
                colvarDeliveryAddress.IsPrimaryKey = false;
                colvarDeliveryAddress.IsForeignKey = false;
                colvarDeliveryAddress.IsReadOnly = false;
                
                schema.Columns.Add(colvarDeliveryAddress);
                
                TableSchema.TableColumn colvarDeliveryTime = new TableSchema.TableColumn(schema);
                colvarDeliveryTime.ColumnName = "delivery_time";
                colvarDeliveryTime.DataType = DbType.DateTime;
                colvarDeliveryTime.MaxLength = 0;
                colvarDeliveryTime.AutoIncrement = false;
                colvarDeliveryTime.IsNullable = true;
                colvarDeliveryTime.IsPrimaryKey = false;
                colvarDeliveryTime.IsForeignKey = false;
                colvarDeliveryTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarDeliveryTime);
                
                TableSchema.TableColumn colvarOrderSubtotal = new TableSchema.TableColumn(schema);
                colvarOrderSubtotal.ColumnName = "order_subtotal";
                colvarOrderSubtotal.DataType = DbType.Currency;
                colvarOrderSubtotal.MaxLength = 0;
                colvarOrderSubtotal.AutoIncrement = false;
                colvarOrderSubtotal.IsNullable = false;
                colvarOrderSubtotal.IsPrimaryKey = false;
                colvarOrderSubtotal.IsForeignKey = false;
                colvarOrderSubtotal.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderSubtotal);
                
                TableSchema.TableColumn colvarOrderTotal = new TableSchema.TableColumn(schema);
                colvarOrderTotal.ColumnName = "order_total";
                colvarOrderTotal.DataType = DbType.Currency;
                colvarOrderTotal.MaxLength = 0;
                colvarOrderTotal.AutoIncrement = false;
                colvarOrderTotal.IsNullable = false;
                colvarOrderTotal.IsPrimaryKey = false;
                colvarOrderTotal.IsForeignKey = false;
                colvarOrderTotal.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderTotal);
                
                TableSchema.TableColumn colvarUserMemo = new TableSchema.TableColumn(schema);
                colvarUserMemo.ColumnName = "user_memo";
                colvarUserMemo.DataType = DbType.String;
                colvarUserMemo.MaxLength = 1073741823;
                colvarUserMemo.AutoIncrement = false;
                colvarUserMemo.IsNullable = true;
                colvarUserMemo.IsPrimaryKey = false;
                colvarUserMemo.IsForeignKey = false;
                colvarUserMemo.IsReadOnly = false;
                
                schema.Columns.Add(colvarUserMemo);
                
                TableSchema.TableColumn colvarOrderMemo = new TableSchema.TableColumn(schema);
                colvarOrderMemo.ColumnName = "order_memo";
                colvarOrderMemo.DataType = DbType.String;
                colvarOrderMemo.MaxLength = 1073741823;
                colvarOrderMemo.AutoIncrement = false;
                colvarOrderMemo.IsNullable = true;
                colvarOrderMemo.IsPrimaryKey = false;
                colvarOrderMemo.IsForeignKey = false;
                colvarOrderMemo.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderMemo);
                
                TableSchema.TableColumn colvarOrderStatus = new TableSchema.TableColumn(schema);
                colvarOrderStatus.ColumnName = "order_status";
                colvarOrderStatus.DataType = DbType.Int32;
                colvarOrderStatus.MaxLength = 0;
                colvarOrderStatus.AutoIncrement = false;
                colvarOrderStatus.IsNullable = false;
                colvarOrderStatus.IsPrimaryKey = false;
                colvarOrderStatus.IsForeignKey = false;
                colvarOrderStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderStatus);
                
                TableSchema.TableColumn colvarOrderStage = new TableSchema.TableColumn(schema);
                colvarOrderStage.ColumnName = "order_stage";
                colvarOrderStage.DataType = DbType.Int32;
                colvarOrderStage.MaxLength = 0;
                colvarOrderStage.AutoIncrement = false;
                colvarOrderStage.IsNullable = false;
                colvarOrderStage.IsPrimaryKey = false;
                colvarOrderStage.IsForeignKey = false;
                colvarOrderStage.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderStage);
                
                TableSchema.TableColumn colvarParentOrderId = new TableSchema.TableColumn(schema);
                colvarParentOrderId.ColumnName = "parent_order_id";
                colvarParentOrderId.DataType = DbType.Guid;
                colvarParentOrderId.MaxLength = 0;
                colvarParentOrderId.AutoIncrement = false;
                colvarParentOrderId.IsNullable = true;
                colvarParentOrderId.IsPrimaryKey = false;
                colvarParentOrderId.IsForeignKey = false;
                colvarParentOrderId.IsReadOnly = false;
                
                schema.Columns.Add(colvarParentOrderId);
                
                TableSchema.TableColumn colvarName = new TableSchema.TableColumn(schema);
                colvarName.ColumnName = "name";
                colvarName.DataType = DbType.String;
                colvarName.MaxLength = 250;
                colvarName.AutoIncrement = false;
                colvarName.IsNullable = true;
                colvarName.IsPrimaryKey = false;
                colvarName.IsForeignKey = false;
                colvarName.IsReadOnly = false;
                
                schema.Columns.Add(colvarName);
                
                TableSchema.TableColumn colvarEventImagePath = new TableSchema.TableColumn(schema);
                colvarEventImagePath.ColumnName = "event_image_path";
                colvarEventImagePath.DataType = DbType.String;
                colvarEventImagePath.MaxLength = 500;
                colvarEventImagePath.AutoIncrement = false;
                colvarEventImagePath.IsNullable = true;
                colvarEventImagePath.IsPrimaryKey = false;
                colvarEventImagePath.IsForeignKey = false;
                colvarEventImagePath.IsReadOnly = false;
                
                schema.Columns.Add(colvarEventImagePath);
                
                TableSchema.TableColumn colvarIntroduction = new TableSchema.TableColumn(schema);
                colvarIntroduction.ColumnName = "introduction";
                colvarIntroduction.DataType = DbType.String;
                colvarIntroduction.MaxLength = 1073741823;
                colvarIntroduction.AutoIncrement = false;
                colvarIntroduction.IsNullable = true;
                colvarIntroduction.IsPrimaryKey = false;
                colvarIntroduction.IsForeignKey = false;
                colvarIntroduction.IsReadOnly = false;
                
                schema.Columns.Add(colvarIntroduction);
                
                TableSchema.TableColumn colvarRestrictions = new TableSchema.TableColumn(schema);
                colvarRestrictions.ColumnName = "restrictions";
                colvarRestrictions.DataType = DbType.String;
                colvarRestrictions.MaxLength = 1073741823;
                colvarRestrictions.AutoIncrement = false;
                colvarRestrictions.IsNullable = true;
                colvarRestrictions.IsPrimaryKey = false;
                colvarRestrictions.IsForeignKey = false;
                colvarRestrictions.IsReadOnly = false;
                
                schema.Columns.Add(colvarRestrictions);
                
                TableSchema.TableColumn colvarDescription = new TableSchema.TableColumn(schema);
                colvarDescription.ColumnName = "description";
                colvarDescription.DataType = DbType.String;
                colvarDescription.MaxLength = 1073741823;
                colvarDescription.AutoIncrement = false;
                colvarDescription.IsNullable = true;
                colvarDescription.IsPrimaryKey = false;
                colvarDescription.IsForeignKey = false;
                colvarDescription.IsReadOnly = false;
                
                schema.Columns.Add(colvarDescription);
                
                TableSchema.TableColumn colvarReferenceText = new TableSchema.TableColumn(schema);
                colvarReferenceText.ColumnName = "reference_text";
                colvarReferenceText.DataType = DbType.String;
                colvarReferenceText.MaxLength = 1073741823;
                colvarReferenceText.AutoIncrement = false;
                colvarReferenceText.IsNullable = true;
                colvarReferenceText.IsPrimaryKey = false;
                colvarReferenceText.IsForeignKey = false;
                colvarReferenceText.IsReadOnly = false;
                
                schema.Columns.Add(colvarReferenceText);
                
                TableSchema.TableColumn colvarCouponUsage = new TableSchema.TableColumn(schema);
                colvarCouponUsage.ColumnName = "coupon_usage";
                colvarCouponUsage.DataType = DbType.String;
                colvarCouponUsage.MaxLength = 1073741823;
                colvarCouponUsage.AutoIncrement = false;
                colvarCouponUsage.IsNullable = true;
                colvarCouponUsage.IsPrimaryKey = false;
                colvarCouponUsage.IsForeignKey = false;
                colvarCouponUsage.IsReadOnly = false;
                
                schema.Columns.Add(colvarCouponUsage);
                
                TableSchema.TableColumn colvarRemark = new TableSchema.TableColumn(schema);
                colvarRemark.ColumnName = "remark";
                colvarRemark.DataType = DbType.String;
                colvarRemark.MaxLength = 1073741823;
                colvarRemark.AutoIncrement = false;
                colvarRemark.IsNullable = true;
                colvarRemark.IsPrimaryKey = false;
                colvarRemark.IsForeignKey = false;
                colvarRemark.IsReadOnly = false;
                
                schema.Columns.Add(colvarRemark);
                
                TableSchema.TableColumn colvarBusinessHourGuid = new TableSchema.TableColumn(schema);
                colvarBusinessHourGuid.ColumnName = "business_hour_guid";
                colvarBusinessHourGuid.DataType = DbType.Guid;
                colvarBusinessHourGuid.MaxLength = 0;
                colvarBusinessHourGuid.AutoIncrement = false;
                colvarBusinessHourGuid.IsNullable = true;
                colvarBusinessHourGuid.IsPrimaryKey = false;
                colvarBusinessHourGuid.IsForeignKey = false;
                colvarBusinessHourGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourGuid);
                
                TableSchema.TableColumn colvarOrderDetailCreateId = new TableSchema.TableColumn(schema);
                colvarOrderDetailCreateId.ColumnName = "order_detail_create_id";
                colvarOrderDetailCreateId.DataType = DbType.String;
                colvarOrderDetailCreateId.MaxLength = 30;
                colvarOrderDetailCreateId.AutoIncrement = false;
                colvarOrderDetailCreateId.IsNullable = false;
                colvarOrderDetailCreateId.IsPrimaryKey = false;
                colvarOrderDetailCreateId.IsForeignKey = false;
                colvarOrderDetailCreateId.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderDetailCreateId);
                
                TableSchema.TableColumn colvarOrderDetailCreateTime = new TableSchema.TableColumn(schema);
                colvarOrderDetailCreateTime.ColumnName = "order_detail_create_time";
                colvarOrderDetailCreateTime.DataType = DbType.DateTime;
                colvarOrderDetailCreateTime.MaxLength = 0;
                colvarOrderDetailCreateTime.AutoIncrement = false;
                colvarOrderDetailCreateTime.IsNullable = false;
                colvarOrderDetailCreateTime.IsPrimaryKey = false;
                colvarOrderDetailCreateTime.IsForeignKey = false;
                colvarOrderDetailCreateTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderDetailCreateTime);
                
                TableSchema.TableColumn colvarOrderDetailModifyId = new TableSchema.TableColumn(schema);
                colvarOrderDetailModifyId.ColumnName = "order_detail_modify_id";
                colvarOrderDetailModifyId.DataType = DbType.String;
                colvarOrderDetailModifyId.MaxLength = 30;
                colvarOrderDetailModifyId.AutoIncrement = false;
                colvarOrderDetailModifyId.IsNullable = true;
                colvarOrderDetailModifyId.IsPrimaryKey = false;
                colvarOrderDetailModifyId.IsForeignKey = false;
                colvarOrderDetailModifyId.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderDetailModifyId);
                
                TableSchema.TableColumn colvarOrderDetailModifyTime = new TableSchema.TableColumn(schema);
                colvarOrderDetailModifyTime.ColumnName = "order_detail_modify_time";
                colvarOrderDetailModifyTime.DataType = DbType.DateTime;
                colvarOrderDetailModifyTime.MaxLength = 0;
                colvarOrderDetailModifyTime.AutoIncrement = false;
                colvarOrderDetailModifyTime.IsNullable = true;
                colvarOrderDetailModifyTime.IsPrimaryKey = false;
                colvarOrderDetailModifyTime.IsForeignKey = false;
                colvarOrderDetailModifyTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderDetailModifyTime);
                
                TableSchema.TableColumn colvarOrderCreateId = new TableSchema.TableColumn(schema);
                colvarOrderCreateId.ColumnName = "order_create_id";
                colvarOrderCreateId.DataType = DbType.String;
                colvarOrderCreateId.MaxLength = 30;
                colvarOrderCreateId.AutoIncrement = false;
                colvarOrderCreateId.IsNullable = false;
                colvarOrderCreateId.IsPrimaryKey = false;
                colvarOrderCreateId.IsForeignKey = false;
                colvarOrderCreateId.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderCreateId);
                
                TableSchema.TableColumn colvarOrderCreateTime = new TableSchema.TableColumn(schema);
                colvarOrderCreateTime.ColumnName = "order_create_time";
                colvarOrderCreateTime.DataType = DbType.DateTime;
                colvarOrderCreateTime.MaxLength = 0;
                colvarOrderCreateTime.AutoIncrement = false;
                colvarOrderCreateTime.IsNullable = false;
                colvarOrderCreateTime.IsPrimaryKey = false;
                colvarOrderCreateTime.IsForeignKey = false;
                colvarOrderCreateTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderCreateTime);
                
                TableSchema.TableColumn colvarOrderModifyId = new TableSchema.TableColumn(schema);
                colvarOrderModifyId.ColumnName = "order_modify_id";
                colvarOrderModifyId.DataType = DbType.String;
                colvarOrderModifyId.MaxLength = 30;
                colvarOrderModifyId.AutoIncrement = false;
                colvarOrderModifyId.IsNullable = true;
                colvarOrderModifyId.IsPrimaryKey = false;
                colvarOrderModifyId.IsForeignKey = false;
                colvarOrderModifyId.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderModifyId);
                
                TableSchema.TableColumn colvarOrderModifyTime = new TableSchema.TableColumn(schema);
                colvarOrderModifyTime.ColumnName = "order_modify_time";
                colvarOrderModifyTime.DataType = DbType.DateTime;
                colvarOrderModifyTime.MaxLength = 0;
                colvarOrderModifyTime.AutoIncrement = false;
                colvarOrderModifyTime.IsNullable = true;
                colvarOrderModifyTime.IsPrimaryKey = false;
                colvarOrderModifyTime.IsForeignKey = false;
                colvarOrderModifyTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderModifyTime);
                
                TableSchema.TableColumn colvarGroupOrderCreateTime = new TableSchema.TableColumn(schema);
                colvarGroupOrderCreateTime.ColumnName = "group_order_create_time";
                colvarGroupOrderCreateTime.DataType = DbType.DateTime;
                colvarGroupOrderCreateTime.MaxLength = 0;
                colvarGroupOrderCreateTime.AutoIncrement = false;
                colvarGroupOrderCreateTime.IsNullable = false;
                colvarGroupOrderCreateTime.IsPrimaryKey = false;
                colvarGroupOrderCreateTime.IsForeignKey = false;
                colvarGroupOrderCreateTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarGroupOrderCreateTime);
                
                TableSchema.TableColumn colvarGroupOrderCloseTime = new TableSchema.TableColumn(schema);
                colvarGroupOrderCloseTime.ColumnName = "group_order_close_time";
                colvarGroupOrderCloseTime.DataType = DbType.DateTime;
                colvarGroupOrderCloseTime.MaxLength = 0;
                colvarGroupOrderCloseTime.AutoIncrement = false;
                colvarGroupOrderCloseTime.IsNullable = false;
                colvarGroupOrderCloseTime.IsPrimaryKey = false;
                colvarGroupOrderCloseTime.IsForeignKey = false;
                colvarGroupOrderCloseTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarGroupOrderCloseTime);
                
                TableSchema.TableColumn colvarGroupOrderStatus = new TableSchema.TableColumn(schema);
                colvarGroupOrderStatus.ColumnName = "group_order_status";
                colvarGroupOrderStatus.DataType = DbType.Int32;
                colvarGroupOrderStatus.MaxLength = 0;
                colvarGroupOrderStatus.AutoIncrement = false;
                colvarGroupOrderStatus.IsNullable = true;
                colvarGroupOrderStatus.IsPrimaryKey = false;
                colvarGroupOrderStatus.IsForeignKey = false;
                colvarGroupOrderStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarGroupOrderStatus);
                
                TableSchema.TableColumn colvarSlug = new TableSchema.TableColumn(schema);
                colvarSlug.ColumnName = "slug";
                colvarSlug.DataType = DbType.String;
                colvarSlug.MaxLength = 100;
                colvarSlug.AutoIncrement = false;
                colvarSlug.IsNullable = true;
                colvarSlug.IsPrimaryKey = false;
                colvarSlug.IsForeignKey = false;
                colvarSlug.IsReadOnly = false;
                
                schema.Columns.Add(colvarSlug);
                
                TableSchema.TableColumn colvarAvailability = new TableSchema.TableColumn(schema);
                colvarAvailability.ColumnName = "availability";
                colvarAvailability.DataType = DbType.String;
                colvarAvailability.MaxLength = 1073741823;
                colvarAvailability.AutoIncrement = false;
                colvarAvailability.IsNullable = true;
                colvarAvailability.IsPrimaryKey = false;
                colvarAvailability.IsForeignKey = false;
                colvarAvailability.IsReadOnly = false;
                
                schema.Columns.Add(colvarAvailability);
                
                TableSchema.TableColumn colvarStoreGuid = new TableSchema.TableColumn(schema);
                colvarStoreGuid.ColumnName = "store_guid";
                colvarStoreGuid.DataType = DbType.Guid;
                colvarStoreGuid.MaxLength = 0;
                colvarStoreGuid.AutoIncrement = false;
                colvarStoreGuid.IsNullable = true;
                colvarStoreGuid.IsPrimaryKey = false;
                colvarStoreGuid.IsForeignKey = false;
                colvarStoreGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarStoreGuid);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_ppon_order_detail",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewPponOrderDetail()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewPponOrderDetail(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewPponOrderDetail(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewPponOrderDetail(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("OrderDetailGuid")]
        [Bindable(true)]
        public Guid OrderDetailGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("order_detail_guid");
		    }
            set 
		    {
			    SetColumnValue("order_detail_guid", value);
            }
        }
	      
        [XmlAttribute("OrderGuid")]
        [Bindable(true)]
        public Guid OrderGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("order_GUID");
		    }
            set 
		    {
			    SetColumnValue("order_GUID", value);
            }
        }
	      
        [XmlAttribute("ItemName")]
        [Bindable(true)]
        public string ItemName 
	    {
		    get
		    {
			    return GetColumnValue<string>("item_name");
		    }
            set 
		    {
			    SetColumnValue("item_name", value);
            }
        }
	      
        [XmlAttribute("ItemGuid")]
        [Bindable(true)]
        public Guid? ItemGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid?>("item_GUID");
		    }
            set 
		    {
			    SetColumnValue("item_GUID", value);
            }
        }
	      
        [XmlAttribute("ItemUnitPrice")]
        [Bindable(true)]
        public decimal ItemUnitPrice 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("item_unit_price");
		    }
            set 
		    {
			    SetColumnValue("item_unit_price", value);
            }
        }
	      
        [XmlAttribute("ItemQuantity")]
        [Bindable(true)]
        public int ItemQuantity 
	    {
		    get
		    {
			    return GetColumnValue<int>("item_quantity");
		    }
            set 
		    {
			    SetColumnValue("item_quantity", value);
            }
        }
	      
        [XmlAttribute("OrderDetailTotal")]
        [Bindable(true)]
        public decimal? OrderDetailTotal 
	    {
		    get
		    {
			    return GetColumnValue<decimal?>("order_detail_total");
		    }
            set 
		    {
			    SetColumnValue("order_detail_total", value);
            }
        }
	      
        [XmlAttribute("ConsumerName")]
        [Bindable(true)]
        public string ConsumerName 
	    {
		    get
		    {
			    return GetColumnValue<string>("consumer_name");
		    }
            set 
		    {
			    SetColumnValue("consumer_name", value);
            }
        }
	      
        [XmlAttribute("ConsumerTeleExt")]
        [Bindable(true)]
        public string ConsumerTeleExt 
	    {
		    get
		    {
			    return GetColumnValue<string>("consumer_tele_ext");
		    }
            set 
		    {
			    SetColumnValue("consumer_tele_ext", value);
            }
        }
	      
        [XmlAttribute("ConsumerGroup")]
        [Bindable(true)]
        public string ConsumerGroup 
	    {
		    get
		    {
			    return GetColumnValue<string>("consumer_group");
		    }
            set 
		    {
			    SetColumnValue("consumer_group", value);
            }
        }
	      
        [XmlAttribute("OrderDetailStatus")]
        [Bindable(true)]
        public int OrderDetailStatus 
	    {
		    get
		    {
			    return GetColumnValue<int>("order_detail_status");
		    }
            set 
		    {
			    SetColumnValue("order_detail_status", value);
            }
        }
	      
        [XmlAttribute("ItemId")]
        [Bindable(true)]
        public string ItemId 
	    {
		    get
		    {
			    return GetColumnValue<string>("item_id");
		    }
            set 
		    {
			    SetColumnValue("item_id", value);
            }
        }
	      
        [XmlAttribute("OrderId")]
        [Bindable(true)]
        public string OrderId 
	    {
		    get
		    {
			    return GetColumnValue<string>("order_id");
		    }
            set 
		    {
			    SetColumnValue("order_id", value);
            }
        }
	      
        [XmlAttribute("SellerGuid")]
        [Bindable(true)]
        public Guid SellerGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("seller_GUID");
		    }
            set 
		    {
			    SetColumnValue("seller_GUID", value);
            }
        }
	      
        [XmlAttribute("SellerName")]
        [Bindable(true)]
        public string SellerName 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_name");
		    }
            set 
		    {
			    SetColumnValue("seller_name", value);
            }
        }
	      
        [XmlAttribute("MemberEmail")]
        [Bindable(true)]
        public string MemberEmail 
	    {
		    get
		    {
			    return GetColumnValue<string>("member_email");
		    }
            set 
		    {
			    SetColumnValue("member_email", value);
            }
        }
	      
        [XmlAttribute("MemberName")]
        [Bindable(true)]
        public string MemberName 
	    {
		    get
		    {
			    return GetColumnValue<string>("member_name");
		    }
            set 
		    {
			    SetColumnValue("member_name", value);
            }
        }
	      
        [XmlAttribute("PhoneNumber")]
        [Bindable(true)]
        public string PhoneNumber 
	    {
		    get
		    {
			    return GetColumnValue<string>("phone_number");
		    }
            set 
		    {
			    SetColumnValue("phone_number", value);
            }
        }
	      
        [XmlAttribute("MobileNumber")]
        [Bindable(true)]
        public string MobileNumber 
	    {
		    get
		    {
			    return GetColumnValue<string>("mobile_number");
		    }
            set 
		    {
			    SetColumnValue("mobile_number", value);
            }
        }
	      
        [XmlAttribute("DeliveryAddress")]
        [Bindable(true)]
        public string DeliveryAddress 
	    {
		    get
		    {
			    return GetColumnValue<string>("delivery_address");
		    }
            set 
		    {
			    SetColumnValue("delivery_address", value);
            }
        }
	      
        [XmlAttribute("DeliveryTime")]
        [Bindable(true)]
        public DateTime? DeliveryTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("delivery_time");
		    }
            set 
		    {
			    SetColumnValue("delivery_time", value);
            }
        }
	      
        [XmlAttribute("OrderSubtotal")]
        [Bindable(true)]
        public decimal OrderSubtotal 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("order_subtotal");
		    }
            set 
		    {
			    SetColumnValue("order_subtotal", value);
            }
        }
	      
        [XmlAttribute("OrderTotal")]
        [Bindable(true)]
        public decimal OrderTotal 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("order_total");
		    }
            set 
		    {
			    SetColumnValue("order_total", value);
            }
        }
	      
        [XmlAttribute("UserMemo")]
        [Bindable(true)]
        public string UserMemo 
	    {
		    get
		    {
			    return GetColumnValue<string>("user_memo");
		    }
            set 
		    {
			    SetColumnValue("user_memo", value);
            }
        }
	      
        [XmlAttribute("OrderMemo")]
        [Bindable(true)]
        public string OrderMemo 
	    {
		    get
		    {
			    return GetColumnValue<string>("order_memo");
		    }
            set 
		    {
			    SetColumnValue("order_memo", value);
            }
        }
	      
        [XmlAttribute("OrderStatus")]
        [Bindable(true)]
        public int OrderStatus 
	    {
		    get
		    {
			    return GetColumnValue<int>("order_status");
		    }
            set 
		    {
			    SetColumnValue("order_status", value);
            }
        }
	      
        [XmlAttribute("OrderStage")]
        [Bindable(true)]
        public int OrderStage 
	    {
		    get
		    {
			    return GetColumnValue<int>("order_stage");
		    }
            set 
		    {
			    SetColumnValue("order_stage", value);
            }
        }
	      
        [XmlAttribute("ParentOrderId")]
        [Bindable(true)]
        public Guid? ParentOrderId 
	    {
		    get
		    {
			    return GetColumnValue<Guid?>("parent_order_id");
		    }
            set 
		    {
			    SetColumnValue("parent_order_id", value);
            }
        }
	      
        [XmlAttribute("Name")]
        [Bindable(true)]
        public string Name 
	    {
		    get
		    {
			    return GetColumnValue<string>("name");
		    }
            set 
		    {
			    SetColumnValue("name", value);
            }
        }
	      
        [XmlAttribute("EventImagePath")]
        [Bindable(true)]
        public string EventImagePath 
	    {
		    get
		    {
			    return GetColumnValue<string>("event_image_path");
		    }
            set 
		    {
			    SetColumnValue("event_image_path", value);
            }
        }
	      
        [XmlAttribute("Introduction")]
        [Bindable(true)]
        public string Introduction 
	    {
		    get
		    {
			    return GetColumnValue<string>("introduction");
		    }
            set 
		    {
			    SetColumnValue("introduction", value);
            }
        }
	      
        [XmlAttribute("Restrictions")]
        [Bindable(true)]
        public string Restrictions 
	    {
		    get
		    {
			    return GetColumnValue<string>("restrictions");
		    }
            set 
		    {
			    SetColumnValue("restrictions", value);
            }
        }
	      
        [XmlAttribute("Description")]
        [Bindable(true)]
        public string Description 
	    {
		    get
		    {
			    return GetColumnValue<string>("description");
		    }
            set 
		    {
			    SetColumnValue("description", value);
            }
        }
	      
        [XmlAttribute("ReferenceText")]
        [Bindable(true)]
        public string ReferenceText 
	    {
		    get
		    {
			    return GetColumnValue<string>("reference_text");
		    }
            set 
		    {
			    SetColumnValue("reference_text", value);
            }
        }
	      
        [XmlAttribute("CouponUsage")]
        [Bindable(true)]
        public string CouponUsage 
	    {
		    get
		    {
			    return GetColumnValue<string>("coupon_usage");
		    }
            set 
		    {
			    SetColumnValue("coupon_usage", value);
            }
        }
	      
        [XmlAttribute("Remark")]
        [Bindable(true)]
        public string Remark 
	    {
		    get
		    {
			    return GetColumnValue<string>("remark");
		    }
            set 
		    {
			    SetColumnValue("remark", value);
            }
        }
	      
        [XmlAttribute("BusinessHourGuid")]
        [Bindable(true)]
        public Guid? BusinessHourGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid?>("business_hour_guid");
		    }
            set 
		    {
			    SetColumnValue("business_hour_guid", value);
            }
        }
	      
        [XmlAttribute("OrderDetailCreateId")]
        [Bindable(true)]
        public string OrderDetailCreateId 
	    {
		    get
		    {
			    return GetColumnValue<string>("order_detail_create_id");
		    }
            set 
		    {
			    SetColumnValue("order_detail_create_id", value);
            }
        }
	      
        [XmlAttribute("OrderDetailCreateTime")]
        [Bindable(true)]
        public DateTime OrderDetailCreateTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("order_detail_create_time");
		    }
            set 
		    {
			    SetColumnValue("order_detail_create_time", value);
            }
        }
	      
        [XmlAttribute("OrderDetailModifyId")]
        [Bindable(true)]
        public string OrderDetailModifyId 
	    {
		    get
		    {
			    return GetColumnValue<string>("order_detail_modify_id");
		    }
            set 
		    {
			    SetColumnValue("order_detail_modify_id", value);
            }
        }
	      
        [XmlAttribute("OrderDetailModifyTime")]
        [Bindable(true)]
        public DateTime? OrderDetailModifyTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("order_detail_modify_time");
		    }
            set 
		    {
			    SetColumnValue("order_detail_modify_time", value);
            }
        }
	      
        [XmlAttribute("OrderCreateId")]
        [Bindable(true)]
        public string OrderCreateId 
	    {
		    get
		    {
			    return GetColumnValue<string>("order_create_id");
		    }
            set 
		    {
			    SetColumnValue("order_create_id", value);
            }
        }
	      
        [XmlAttribute("OrderCreateTime")]
        [Bindable(true)]
        public DateTime OrderCreateTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("order_create_time");
		    }
            set 
		    {
			    SetColumnValue("order_create_time", value);
            }
        }
	      
        [XmlAttribute("OrderModifyId")]
        [Bindable(true)]
        public string OrderModifyId 
	    {
		    get
		    {
			    return GetColumnValue<string>("order_modify_id");
		    }
            set 
		    {
			    SetColumnValue("order_modify_id", value);
            }
        }
	      
        [XmlAttribute("OrderModifyTime")]
        [Bindable(true)]
        public DateTime? OrderModifyTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("order_modify_time");
		    }
            set 
		    {
			    SetColumnValue("order_modify_time", value);
            }
        }
	      
        [XmlAttribute("GroupOrderCreateTime")]
        [Bindable(true)]
        public DateTime GroupOrderCreateTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("group_order_create_time");
		    }
            set 
		    {
			    SetColumnValue("group_order_create_time", value);
            }
        }
	      
        [XmlAttribute("GroupOrderCloseTime")]
        [Bindable(true)]
        public DateTime GroupOrderCloseTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("group_order_close_time");
		    }
            set 
		    {
			    SetColumnValue("group_order_close_time", value);
            }
        }
	      
        [XmlAttribute("GroupOrderStatus")]
        [Bindable(true)]
        public int? GroupOrderStatus 
	    {
		    get
		    {
			    return GetColumnValue<int?>("group_order_status");
		    }
            set 
		    {
			    SetColumnValue("group_order_status", value);
            }
        }
	      
        [XmlAttribute("Slug")]
        [Bindable(true)]
        public string Slug 
	    {
		    get
		    {
			    return GetColumnValue<string>("slug");
		    }
            set 
		    {
			    SetColumnValue("slug", value);
            }
        }
	      
        [XmlAttribute("Availability")]
        [Bindable(true)]
        public string Availability 
	    {
		    get
		    {
			    return GetColumnValue<string>("availability");
		    }
            set 
		    {
			    SetColumnValue("availability", value);
            }
        }
	      
        [XmlAttribute("StoreGuid")]
        [Bindable(true)]
        public Guid? StoreGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid?>("store_guid");
		    }
            set 
		    {
			    SetColumnValue("store_guid", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string OrderDetailGuid = @"order_detail_guid";
            
            public static string OrderGuid = @"order_GUID";
            
            public static string ItemName = @"item_name";
            
            public static string ItemGuid = @"item_GUID";
            
            public static string ItemUnitPrice = @"item_unit_price";
            
            public static string ItemQuantity = @"item_quantity";
            
            public static string OrderDetailTotal = @"order_detail_total";
            
            public static string ConsumerName = @"consumer_name";
            
            public static string ConsumerTeleExt = @"consumer_tele_ext";
            
            public static string ConsumerGroup = @"consumer_group";
            
            public static string OrderDetailStatus = @"order_detail_status";
            
            public static string ItemId = @"item_id";
            
            public static string OrderId = @"order_id";
            
            public static string SellerGuid = @"seller_GUID";
            
            public static string SellerName = @"seller_name";
            
            public static string MemberEmail = @"member_email";
            
            public static string MemberName = @"member_name";
            
            public static string PhoneNumber = @"phone_number";
            
            public static string MobileNumber = @"mobile_number";
            
            public static string DeliveryAddress = @"delivery_address";
            
            public static string DeliveryTime = @"delivery_time";
            
            public static string OrderSubtotal = @"order_subtotal";
            
            public static string OrderTotal = @"order_total";
            
            public static string UserMemo = @"user_memo";
            
            public static string OrderMemo = @"order_memo";
            
            public static string OrderStatus = @"order_status";
            
            public static string OrderStage = @"order_stage";
            
            public static string ParentOrderId = @"parent_order_id";
            
            public static string Name = @"name";
            
            public static string EventImagePath = @"event_image_path";
            
            public static string Introduction = @"introduction";
            
            public static string Restrictions = @"restrictions";
            
            public static string Description = @"description";
            
            public static string ReferenceText = @"reference_text";
            
            public static string CouponUsage = @"coupon_usage";
            
            public static string Remark = @"remark";
            
            public static string BusinessHourGuid = @"business_hour_guid";
            
            public static string OrderDetailCreateId = @"order_detail_create_id";
            
            public static string OrderDetailCreateTime = @"order_detail_create_time";
            
            public static string OrderDetailModifyId = @"order_detail_modify_id";
            
            public static string OrderDetailModifyTime = @"order_detail_modify_time";
            
            public static string OrderCreateId = @"order_create_id";
            
            public static string OrderCreateTime = @"order_create_time";
            
            public static string OrderModifyId = @"order_modify_id";
            
            public static string OrderModifyTime = @"order_modify_time";
            
            public static string GroupOrderCreateTime = @"group_order_create_time";
            
            public static string GroupOrderCloseTime = @"group_order_close_time";
            
            public static string GroupOrderStatus = @"group_order_status";
            
            public static string Slug = @"slug";
            
            public static string Availability = @"availability";
            
            public static string StoreGuid = @"store_guid";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

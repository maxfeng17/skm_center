﻿using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the TaishinEcPayCardLog class.
    /// </summary>
    [Serializable]
    public partial class TaishinEcPayCardLogCollection : RepositoryList<TaishinEcPayCardLog, TaishinEcPayCardLogCollection>
    {
        public TaishinEcPayCardLogCollection() { }

        /// <summary>
        /// Filters an existing collection based on the set criteria. This is an in-memory filter
        /// Thanks to developingchris for this!
        /// </summary>
        /// <returns>TaishinEcPayCardLogCollection</returns>
        public TaishinEcPayCardLogCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                TaishinEcPayCardLog o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }


    }
    /// <summary>
    /// This is an ActiveRecord class which wraps the taishin_ec_pay_card_Log table.
    /// </summary>
    [Serializable]
    public partial class TaishinEcPayCardLog : RepositoryRecord<TaishinEcPayCardLog>, IRecordBase
    {
        #region .ctors and Default Settings

        public TaishinEcPayCardLog()
        {
            SetSQLProps();
            InitSetDefaults();
            MarkNew();
        }

        private void InitSetDefaults() { SetDefaults(); }

        public TaishinEcPayCardLog(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
                ForceDefaults();
            MarkNew();
        }

        protected static void SetSQLProps() { GetTableSchema(); }

        #endregion

        #region Schema and Query Accessor	
        public static Query CreateQuery() { return new Query(Schema); }
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                    SetSQLProps();
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("taishin_ec_pay_card_Log", TableType.Table, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarCardKey = new TableSchema.TableColumn(schema);
                colvarCardKey.ColumnName = "card_key";
                colvarCardKey.DataType = DbType.AnsiString;
                colvarCardKey.MaxLength = 100;
                colvarCardKey.AutoIncrement = false;
                colvarCardKey.IsNullable = false;
                colvarCardKey.IsPrimaryKey = false;
                colvarCardKey.IsForeignKey = false;
                colvarCardKey.IsReadOnly = false;
                colvarCardKey.DefaultSetting = @"";
                colvarCardKey.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCardKey);

                TableSchema.TableColumn colvarCardToken = new TableSchema.TableColumn(schema);
                colvarCardToken.ColumnName = "card_token";
                colvarCardToken.DataType = DbType.AnsiString;
                colvarCardToken.MaxLength = 100;
                colvarCardToken.AutoIncrement = false;
                colvarCardToken.IsNullable = false;
                colvarCardToken.IsPrimaryKey = true;
                colvarCardToken.IsForeignKey = false;
                colvarCardToken.IsReadOnly = false;
                colvarCardToken.DefaultSetting = @"";
                colvarCardToken.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCardToken);

                TableSchema.TableColumn colvarUserId = new TableSchema.TableColumn(schema);
                colvarUserId.ColumnName = "user_id";
                colvarUserId.DataType = DbType.AnsiString;
                colvarUserId.MaxLength = 50;
                colvarUserId.AutoIncrement = false;
                colvarUserId.IsNullable = false;
                colvarUserId.IsPrimaryKey = false;
                colvarUserId.IsForeignKey = false;
                colvarUserId.IsReadOnly = false;
                colvarUserId.DefaultSetting = @"";
                colvarUserId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarUserId);

                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = false;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                colvarCreateTime.DefaultSetting = @"";
                colvarCreateTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateTime);

                TableSchema.TableColumn colvarCardStatus = new TableSchema.TableColumn(schema);
                colvarCardStatus.ColumnName = "card_status";
                colvarCardStatus.DataType = DbType.Int32;
                colvarCardStatus.MaxLength = 0;
                colvarCardStatus.AutoIncrement = false;
                colvarCardStatus.IsNullable = false;
                colvarCardStatus.IsPrimaryKey = false;
                colvarCardStatus.IsForeignKey = false;
                colvarCardStatus.IsReadOnly = false;
                colvarCardStatus.DefaultSetting = @"";
                colvarCardStatus.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCardStatus);

                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("taishin_ec_pay_card_Log", schema);
            }
        }
        #endregion

        #region Props

        [XmlAttribute("CardKey")]
        [Bindable(true)]
        public string CardKey
        {
            get { return GetColumnValue<string>(Columns.CardKey); }
            set { SetColumnValue(Columns.CardKey, value); }
        }

        [XmlAttribute("CardToken")]
        [Bindable(true)]
        public string CardToken
        {
            get { return GetColumnValue<string>(Columns.CardToken); }
            set { SetColumnValue(Columns.CardToken, value); }
        }

        [XmlAttribute("UserId")]
        [Bindable(true)]
        public string UserId
        {
            get { return GetColumnValue<string>(Columns.UserId); }
            set { SetColumnValue(Columns.UserId, value); }
        }

        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime CreateTime
        {
            get { return GetColumnValue<DateTime>(Columns.CreateTime); }
            set { SetColumnValue(Columns.CreateTime, value); }
        }

        [XmlAttribute("CardStatus")]
        [Bindable(true)]
        public int CardStatus
        {
            get { return GetColumnValue<int>(Columns.CardStatus); }
            set { SetColumnValue(Columns.CardStatus, value); }
        }

        #endregion




        //no foreign key tables defined (0)



        //no ManyToMany tables defined (0)





        #region Typed Columns


        public static TableSchema.TableColumn CardKeyColumn
        {
            get { return Schema.Columns[0]; }
        }



        public static TableSchema.TableColumn CardTokenColumn
        {
            get { return Schema.Columns[1]; }
        }



        public static TableSchema.TableColumn UserIdColumn
        {
            get { return Schema.Columns[2]; }
        }



        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[3]; }
        }



        public static TableSchema.TableColumn CardStatusColumn
        {
            get { return Schema.Columns[4]; }
        }



        #endregion
        #region Columns Struct
        public struct Columns
        {
            public static string CardKey = @"card_key";
            public static string CardToken = @"card_token";
            public static string UserId = @"user_id";
            public static string CreateTime = @"create_time";
            public static string CardStatus = @"card_status";

        }
        #endregion

        #region Update PK Collections

        #endregion

        #region Deep Save

        #endregion
    }
}

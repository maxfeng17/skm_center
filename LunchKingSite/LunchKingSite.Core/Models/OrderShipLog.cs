using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the OrderShipLog class.
	/// </summary>
    [Serializable]
	public partial class OrderShipLogCollection : RepositoryList<OrderShipLog, OrderShipLogCollection>
	{	   
		public OrderShipLogCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>OrderShipLogCollection</returns>
		public OrderShipLogCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                OrderShipLog o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the order_ship_log table.
	/// </summary>
	[Serializable]
	public partial class OrderShipLog : RepositoryRecord<OrderShipLog>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public OrderShipLog()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public OrderShipLog(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("order_ship_log", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarOrderShipId = new TableSchema.TableColumn(schema);
				colvarOrderShipId.ColumnName = "order_ship_id";
				colvarOrderShipId.DataType = DbType.Int32;
				colvarOrderShipId.MaxLength = 0;
				colvarOrderShipId.AutoIncrement = false;
				colvarOrderShipId.IsNullable = false;
				colvarOrderShipId.IsPrimaryKey = false;
				colvarOrderShipId.IsForeignKey = true;
				colvarOrderShipId.IsReadOnly = false;
				colvarOrderShipId.DefaultSetting = @"";
				
					colvarOrderShipId.ForeignKeyTableName = "order_ship";
				schema.Columns.Add(colvarOrderShipId);
				
				TableSchema.TableColumn colvarShipCompanyId = new TableSchema.TableColumn(schema);
				colvarShipCompanyId.ColumnName = "ship_company_id";
				colvarShipCompanyId.DataType = DbType.Int32;
				colvarShipCompanyId.MaxLength = 0;
				colvarShipCompanyId.AutoIncrement = false;
				colvarShipCompanyId.IsNullable = true;
				colvarShipCompanyId.IsPrimaryKey = false;
				colvarShipCompanyId.IsForeignKey = true;
				colvarShipCompanyId.IsReadOnly = false;
				colvarShipCompanyId.DefaultSetting = @"";
				
					colvarShipCompanyId.ForeignKeyTableName = "ship_company";
				schema.Columns.Add(colvarShipCompanyId);
				
				TableSchema.TableColumn colvarShipTime = new TableSchema.TableColumn(schema);
				colvarShipTime.ColumnName = "ship_time";
				colvarShipTime.DataType = DbType.DateTime;
				colvarShipTime.MaxLength = 0;
				colvarShipTime.AutoIncrement = false;
				colvarShipTime.IsNullable = true;
				colvarShipTime.IsPrimaryKey = false;
				colvarShipTime.IsForeignKey = false;
				colvarShipTime.IsReadOnly = false;
				colvarShipTime.DefaultSetting = @"";
				colvarShipTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarShipTime);
				
				TableSchema.TableColumn colvarShipNo = new TableSchema.TableColumn(schema);
				colvarShipNo.ColumnName = "ship_no";
				colvarShipNo.DataType = DbType.String;
				colvarShipNo.MaxLength = -1;
				colvarShipNo.AutoIncrement = false;
				colvarShipNo.IsNullable = true;
				colvarShipNo.IsPrimaryKey = false;
				colvarShipNo.IsForeignKey = false;
				colvarShipNo.IsReadOnly = false;
				colvarShipNo.DefaultSetting = @"";
				colvarShipNo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarShipNo);
				
				TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
				colvarCreateId.ColumnName = "create_id";
				colvarCreateId.DataType = DbType.String;
				colvarCreateId.MaxLength = 100;
				colvarCreateId.AutoIncrement = false;
				colvarCreateId.IsNullable = false;
				colvarCreateId.IsPrimaryKey = false;
				colvarCreateId.IsForeignKey = false;
				colvarCreateId.IsReadOnly = false;
				colvarCreateId.DefaultSetting = @"";
				colvarCreateId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateId);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarShipMemo = new TableSchema.TableColumn(schema);
				colvarShipMemo.ColumnName = "ship_memo";
				colvarShipMemo.DataType = DbType.String;
				colvarShipMemo.MaxLength = 60;
				colvarShipMemo.AutoIncrement = false;
				colvarShipMemo.IsNullable = true;
				colvarShipMemo.IsPrimaryKey = false;
				colvarShipMemo.IsForeignKey = false;
				colvarShipMemo.IsReadOnly = false;
				colvarShipMemo.DefaultSetting = @"";
				colvarShipMemo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarShipMemo);
				
				TableSchema.TableColumn colvarType = new TableSchema.TableColumn(schema);
				colvarType.ColumnName = "type";
				colvarType.DataType = DbType.Int32;
				colvarType.MaxLength = 0;
				colvarType.AutoIncrement = false;
				colvarType.IsNullable = false;
				colvarType.IsPrimaryKey = false;
				colvarType.IsForeignKey = false;
				colvarType.IsReadOnly = false;
				
						colvarType.DefaultSetting = @"((1))";
				colvarType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarType);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("order_ship_log",schema);
			}
		}
		#endregion
		
		#region Props
		
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		
		[XmlAttribute("OrderShipId")]
		[Bindable(true)]
		public int OrderShipId 
		{
			get { return GetColumnValue<int>(Columns.OrderShipId); }
			set { SetColumnValue(Columns.OrderShipId, value); }
		}
		
		[XmlAttribute("ShipCompanyId")]
		[Bindable(true)]
		public int? ShipCompanyId 
		{
			get { return GetColumnValue<int?>(Columns.ShipCompanyId); }
			set { SetColumnValue(Columns.ShipCompanyId, value); }
		}
		
		[XmlAttribute("ShipTime")]
		[Bindable(true)]
		public DateTime? ShipTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.ShipTime); }
			set { SetColumnValue(Columns.ShipTime, value); }
		}
		
		[XmlAttribute("ShipNo")]
		[Bindable(true)]
		public string ShipNo 
		{
			get { return GetColumnValue<string>(Columns.ShipNo); }
			set { SetColumnValue(Columns.ShipNo, value); }
		}
		
		[XmlAttribute("CreateId")]
		[Bindable(true)]
		public string CreateId 
		{
			get { return GetColumnValue<string>(Columns.CreateId); }
			set { SetColumnValue(Columns.CreateId, value); }
		}
		
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		
		[XmlAttribute("ShipMemo")]
		[Bindable(true)]
		public string ShipMemo 
		{
			get { return GetColumnValue<string>(Columns.ShipMemo); }
			set { SetColumnValue(Columns.ShipMemo, value); }
		}
		
		[XmlAttribute("Type")]
		[Bindable(true)]
		public int Type 
		{
			get { return GetColumnValue<int>(Columns.Type); }
			set { SetColumnValue(Columns.Type, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (2)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn OrderShipIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn ShipCompanyIdColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn ShipTimeColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn ShipNoColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn ShipMemoColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn TypeColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string OrderShipId = @"order_ship_id";
			 public static string ShipCompanyId = @"ship_company_id";
			 public static string ShipTime = @"ship_time";
			 public static string ShipNo = @"ship_no";
			 public static string CreateId = @"create_id";
			 public static string CreateTime = @"create_time";
			 public static string ShipMemo = @"ship_memo";
			 public static string Type = @"type";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the FetSmsLog class.
	/// </summary>
    [Serializable]
	public partial class FetSmsLogCollection : RepositoryList<FetSmsLog, FetSmsLogCollection>
	{	   
		public FetSmsLogCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>FetSmsLogCollection</returns>
		public FetSmsLogCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                FetSmsLog o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the FET_sms_log table.
	/// </summary>
	[Serializable]
	public partial class FetSmsLog : RepositoryRecord<FetSmsLog>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public FetSmsLog()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public FetSmsLog(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("FET_sms_log", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarSi = new TableSchema.TableColumn(schema);
				colvarSi.ColumnName = "si";
				colvarSi.DataType = DbType.Int32;
				colvarSi.MaxLength = 0;
				colvarSi.AutoIncrement = true;
				colvarSi.IsNullable = false;
				colvarSi.IsPrimaryKey = true;
				colvarSi.IsForeignKey = false;
				colvarSi.IsReadOnly = false;
				colvarSi.DefaultSetting = @"";
				colvarSi.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSi);
				
				TableSchema.TableColumn colvarCouponId = new TableSchema.TableColumn(schema);
				colvarCouponId.ColumnName = "couponId";
				colvarCouponId.DataType = DbType.Int32;
				colvarCouponId.MaxLength = 0;
				colvarCouponId.AutoIncrement = false;
				colvarCouponId.IsNullable = true;
				colvarCouponId.IsPrimaryKey = false;
				colvarCouponId.IsForeignKey = false;
				colvarCouponId.IsReadOnly = false;
				colvarCouponId.DefaultSetting = @"";
				colvarCouponId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCouponId);
				
				TableSchema.TableColumn colvarMessage = new TableSchema.TableColumn(schema);
				colvarMessage.ColumnName = "message";
				colvarMessage.DataType = DbType.String;
				colvarMessage.MaxLength = 500;
				colvarMessage.AutoIncrement = false;
				colvarMessage.IsNullable = true;
				colvarMessage.IsPrimaryKey = false;
				colvarMessage.IsForeignKey = false;
				colvarMessage.IsReadOnly = false;
				colvarMessage.DefaultSetting = @"";
				colvarMessage.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMessage);
				
				TableSchema.TableColumn colvarDestination = new TableSchema.TableColumn(schema);
				colvarDestination.ColumnName = "destination";
				colvarDestination.DataType = DbType.AnsiString;
				colvarDestination.MaxLength = 50;
				colvarDestination.AutoIncrement = false;
				colvarDestination.IsNullable = true;
				colvarDestination.IsPrimaryKey = false;
				colvarDestination.IsForeignKey = false;
				colvarDestination.IsReadOnly = false;
				colvarDestination.DefaultSetting = @"";
				colvarDestination.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDestination);
				
				TableSchema.TableColumn colvarResultCode = new TableSchema.TableColumn(schema);
				colvarResultCode.ColumnName = "resultCode";
				colvarResultCode.DataType = DbType.Int32;
				colvarResultCode.MaxLength = 0;
				colvarResultCode.AutoIncrement = false;
				colvarResultCode.IsNullable = true;
				colvarResultCode.IsPrimaryKey = false;
				colvarResultCode.IsForeignKey = false;
				colvarResultCode.IsReadOnly = false;
				colvarResultCode.DefaultSetting = @"";
				colvarResultCode.ForeignKeyTableName = "";
				schema.Columns.Add(colvarResultCode);
				
				TableSchema.TableColumn colvarResultText = new TableSchema.TableColumn(schema);
				colvarResultText.ColumnName = "resultText";
				colvarResultText.DataType = DbType.AnsiString;
				colvarResultText.MaxLength = 100;
				colvarResultText.AutoIncrement = false;
				colvarResultText.IsNullable = true;
				colvarResultText.IsPrimaryKey = false;
				colvarResultText.IsForeignKey = false;
				colvarResultText.IsReadOnly = false;
				colvarResultText.DefaultSetting = @"";
				colvarResultText.ForeignKeyTableName = "";
				schema.Columns.Add(colvarResultText);
				
				TableSchema.TableColumn colvarMessageId = new TableSchema.TableColumn(schema);
				colvarMessageId.ColumnName = "messageId";
				colvarMessageId.DataType = DbType.AnsiString;
				colvarMessageId.MaxLength = 50;
				colvarMessageId.AutoIncrement = false;
				colvarMessageId.IsNullable = true;
				colvarMessageId.IsPrimaryKey = false;
				colvarMessageId.IsForeignKey = false;
				colvarMessageId.IsReadOnly = false;
				colvarMessageId.DefaultSetting = @"";
				colvarMessageId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMessageId);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "createTime";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = true;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				
						colvarCreateTime.DefaultSetting = @"(getdate())";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("FET_sms_log",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Si")]
		[Bindable(true)]
		public int Si 
		{
			get { return GetColumnValue<int>(Columns.Si); }
			set { SetColumnValue(Columns.Si, value); }
		}
		  
		[XmlAttribute("CouponId")]
		[Bindable(true)]
		public int? CouponId 
		{
			get { return GetColumnValue<int?>(Columns.CouponId); }
			set { SetColumnValue(Columns.CouponId, value); }
		}
		  
		[XmlAttribute("Message")]
		[Bindable(true)]
		public string Message 
		{
			get { return GetColumnValue<string>(Columns.Message); }
			set { SetColumnValue(Columns.Message, value); }
		}
		  
		[XmlAttribute("Destination")]
		[Bindable(true)]
		public string Destination 
		{
			get { return GetColumnValue<string>(Columns.Destination); }
			set { SetColumnValue(Columns.Destination, value); }
		}
		  
		[XmlAttribute("ResultCode")]
		[Bindable(true)]
		public int? ResultCode 
		{
			get { return GetColumnValue<int?>(Columns.ResultCode); }
			set { SetColumnValue(Columns.ResultCode, value); }
		}
		  
		[XmlAttribute("ResultText")]
		[Bindable(true)]
		public string ResultText 
		{
			get { return GetColumnValue<string>(Columns.ResultText); }
			set { SetColumnValue(Columns.ResultText, value); }
		}
		  
		[XmlAttribute("MessageId")]
		[Bindable(true)]
		public string MessageId 
		{
			get { return GetColumnValue<string>(Columns.MessageId); }
			set { SetColumnValue(Columns.MessageId, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime? CreateTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn SiColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn CouponIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn MessageColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn DestinationColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn ResultCodeColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn ResultTextColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn MessageIdColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Si = @"si";
			 public static string CouponId = @"couponId";
			 public static string Message = @"message";
			 public static string Destination = @"destination";
			 public static string ResultCode = @"resultCode";
			 public static string ResultText = @"resultText";
			 public static string MessageId = @"messageId";
			 public static string CreateTime = @"createTime";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

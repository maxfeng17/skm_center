using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the DealAccounting class.
	/// </summary>
    [Serializable]
	public partial class DealAccountingCollection : RepositoryList<DealAccounting, DealAccountingCollection>
	{	   
		public DealAccountingCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>DealAccountingCollection</returns>
		public DealAccountingCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                DealAccounting o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the deal_accounting table.
	/// </summary>
	[Serializable]
	public partial class DealAccounting : RepositoryRecord<DealAccounting>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public DealAccounting()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public DealAccounting(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("deal_accounting", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarBusinessHourGuid = new TableSchema.TableColumn(schema);
				colvarBusinessHourGuid.ColumnName = "business_hour_guid";
				colvarBusinessHourGuid.DataType = DbType.Guid;
				colvarBusinessHourGuid.MaxLength = 0;
				colvarBusinessHourGuid.AutoIncrement = false;
				colvarBusinessHourGuid.IsNullable = false;
				colvarBusinessHourGuid.IsPrimaryKey = true;
				colvarBusinessHourGuid.IsForeignKey = false;
				colvarBusinessHourGuid.IsReadOnly = false;
				colvarBusinessHourGuid.DefaultSetting = @"";
				colvarBusinessHourGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBusinessHourGuid);
				
				TableSchema.TableColumn colvarSalesId = new TableSchema.TableColumn(schema);
				colvarSalesId.ColumnName = "sales_id";
				colvarSalesId.DataType = DbType.String;
				colvarSalesId.MaxLength = 50;
				colvarSalesId.AutoIncrement = false;
				colvarSalesId.IsNullable = true;
				colvarSalesId.IsPrimaryKey = false;
				colvarSalesId.IsForeignKey = false;
				colvarSalesId.IsReadOnly = false;
				colvarSalesId.DefaultSetting = @"";
				colvarSalesId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSalesId);
				
				TableSchema.TableColumn colvarSalesCommission = new TableSchema.TableColumn(schema);
				colvarSalesCommission.ColumnName = "sales_commission";
				colvarSalesCommission.DataType = DbType.Double;
				colvarSalesCommission.MaxLength = 0;
				colvarSalesCommission.AutoIncrement = false;
				colvarSalesCommission.IsNullable = true;
				colvarSalesCommission.IsPrimaryKey = false;
				colvarSalesCommission.IsForeignKey = false;
				colvarSalesCommission.IsReadOnly = false;
				colvarSalesCommission.DefaultSetting = @"";
				colvarSalesCommission.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSalesCommission);
				
				TableSchema.TableColumn colvarSalesBonus = new TableSchema.TableColumn(schema);
				colvarSalesBonus.ColumnName = "sales_bonus";
				colvarSalesBonus.DataType = DbType.Currency;
				colvarSalesBonus.MaxLength = 0;
				colvarSalesBonus.AutoIncrement = false;
				colvarSalesBonus.IsNullable = true;
				colvarSalesBonus.IsPrimaryKey = false;
				colvarSalesBonus.IsForeignKey = false;
				colvarSalesBonus.IsReadOnly = false;
				colvarSalesBonus.DefaultSetting = @"";
				colvarSalesBonus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSalesBonus);
				
				TableSchema.TableColumn colvarChargingDate = new TableSchema.TableColumn(schema);
				colvarChargingDate.ColumnName = "charging_date";
				colvarChargingDate.DataType = DbType.DateTime;
				colvarChargingDate.MaxLength = 0;
				colvarChargingDate.AutoIncrement = false;
				colvarChargingDate.IsNullable = true;
				colvarChargingDate.IsPrimaryKey = false;
				colvarChargingDate.IsForeignKey = false;
				colvarChargingDate.IsReadOnly = false;
				colvarChargingDate.DefaultSetting = @"";
				colvarChargingDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarChargingDate);
				
				TableSchema.TableColumn colvarEnterDate = new TableSchema.TableColumn(schema);
				colvarEnterDate.ColumnName = "enter_date";
				colvarEnterDate.DataType = DbType.DateTime;
				colvarEnterDate.MaxLength = 0;
				colvarEnterDate.AutoIncrement = false;
				colvarEnterDate.IsNullable = true;
				colvarEnterDate.IsPrimaryKey = false;
				colvarEnterDate.IsForeignKey = false;
				colvarEnterDate.IsReadOnly = false;
				colvarEnterDate.DefaultSetting = @"";
				colvarEnterDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEnterDate);
				
				TableSchema.TableColumn colvarInvoiceDate = new TableSchema.TableColumn(schema);
				colvarInvoiceDate.ColumnName = "invoice_date";
				colvarInvoiceDate.DataType = DbType.DateTime;
				colvarInvoiceDate.MaxLength = 0;
				colvarInvoiceDate.AutoIncrement = false;
				colvarInvoiceDate.IsNullable = true;
				colvarInvoiceDate.IsPrimaryKey = false;
				colvarInvoiceDate.IsForeignKey = false;
				colvarInvoiceDate.IsReadOnly = false;
				colvarInvoiceDate.DefaultSetting = @"";
				colvarInvoiceDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarInvoiceDate);
				
				TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
				colvarStatus.ColumnName = "status";
				colvarStatus.DataType = DbType.Int32;
				colvarStatus.MaxLength = 0;
				colvarStatus.AutoIncrement = false;
				colvarStatus.IsNullable = true;
				colvarStatus.IsPrimaryKey = false;
				colvarStatus.IsForeignKey = false;
				colvarStatus.IsReadOnly = false;
				colvarStatus.DefaultSetting = @"";
				colvarStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStatus);
				
				TableSchema.TableColumn colvarSettleDay1 = new TableSchema.TableColumn(schema);
				colvarSettleDay1.ColumnName = "settle_day1";
				colvarSettleDay1.DataType = DbType.Int32;
				colvarSettleDay1.MaxLength = 0;
				colvarSettleDay1.AutoIncrement = false;
				colvarSettleDay1.IsNullable = true;
				colvarSettleDay1.IsPrimaryKey = false;
				colvarSettleDay1.IsForeignKey = false;
				colvarSettleDay1.IsReadOnly = false;
				colvarSettleDay1.DefaultSetting = @"";
				colvarSettleDay1.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSettleDay1);
				
				TableSchema.TableColumn colvarSettleDay2 = new TableSchema.TableColumn(schema);
				colvarSettleDay2.ColumnName = "settle_day2";
				colvarSettleDay2.DataType = DbType.Int32;
				colvarSettleDay2.MaxLength = 0;
				colvarSettleDay2.AutoIncrement = false;
				colvarSettleDay2.IsNullable = true;
				colvarSettleDay2.IsPrimaryKey = false;
				colvarSettleDay2.IsForeignKey = false;
				colvarSettleDay2.IsReadOnly = false;
				colvarSettleDay2.DefaultSetting = @"";
				colvarSettleDay2.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSettleDay2);
				
				TableSchema.TableColumn colvarSettleDay3 = new TableSchema.TableColumn(schema);
				colvarSettleDay3.ColumnName = "settle_day3";
				colvarSettleDay3.DataType = DbType.Int32;
				colvarSettleDay3.MaxLength = 0;
				colvarSettleDay3.AutoIncrement = false;
				colvarSettleDay3.IsNullable = true;
				colvarSettleDay3.IsPrimaryKey = false;
				colvarSettleDay3.IsForeignKey = false;
				colvarSettleDay3.IsReadOnly = false;
				colvarSettleDay3.DefaultSetting = @"";
				colvarSettleDay3.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSettleDay3);
				
				TableSchema.TableColumn colvarAccCity = new TableSchema.TableColumn(schema);
				colvarAccCity.ColumnName = "acc_city";
				colvarAccCity.DataType = DbType.Int32;
				colvarAccCity.MaxLength = 0;
				colvarAccCity.AutoIncrement = false;
				colvarAccCity.IsNullable = true;
				colvarAccCity.IsPrimaryKey = false;
				colvarAccCity.IsForeignKey = false;
				colvarAccCity.IsReadOnly = false;
				colvarAccCity.DefaultSetting = @"";
				colvarAccCity.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAccCity);
				
				TableSchema.TableColumn colvarDeptId = new TableSchema.TableColumn(schema);
				colvarDeptId.ColumnName = "dept_id";
				colvarDeptId.DataType = DbType.AnsiString;
				colvarDeptId.MaxLength = 10;
				colvarDeptId.AutoIncrement = false;
				colvarDeptId.IsNullable = true;
				colvarDeptId.IsPrimaryKey = false;
				colvarDeptId.IsForeignKey = true;
				colvarDeptId.IsReadOnly = false;
				colvarDeptId.DefaultSetting = @"";
				
					colvarDeptId.ForeignKeyTableName = "department";
				schema.Columns.Add(colvarDeptId);
				
				TableSchema.TableColumn colvarDownPayment = new TableSchema.TableColumn(schema);
				colvarDownPayment.ColumnName = "down_payment";
				colvarDownPayment.DataType = DbType.Double;
				colvarDownPayment.MaxLength = 0;
				colvarDownPayment.AutoIncrement = false;
				colvarDownPayment.IsNullable = true;
				colvarDownPayment.IsPrimaryKey = false;
				colvarDownPayment.IsForeignKey = false;
				colvarDownPayment.IsReadOnly = false;
				colvarDownPayment.DefaultSetting = @"";
				colvarDownPayment.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDownPayment);
				
				TableSchema.TableColumn colvarSellerVerifiedQuantity = new TableSchema.TableColumn(schema);
				colvarSellerVerifiedQuantity.ColumnName = "seller_verified_quantity";
				colvarSellerVerifiedQuantity.DataType = DbType.Int32;
				colvarSellerVerifiedQuantity.MaxLength = 0;
				colvarSellerVerifiedQuantity.AutoIncrement = false;
				colvarSellerVerifiedQuantity.IsNullable = true;
				colvarSellerVerifiedQuantity.IsPrimaryKey = false;
				colvarSellerVerifiedQuantity.IsForeignKey = false;
				colvarSellerVerifiedQuantity.IsReadOnly = false;
				colvarSellerVerifiedQuantity.DefaultSetting = @"";
				colvarSellerVerifiedQuantity.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSellerVerifiedQuantity);
				
				TableSchema.TableColumn colvarSellerVerifiedTime = new TableSchema.TableColumn(schema);
				colvarSellerVerifiedTime.ColumnName = "seller_verified_time";
				colvarSellerVerifiedTime.DataType = DbType.DateTime;
				colvarSellerVerifiedTime.MaxLength = 0;
				colvarSellerVerifiedTime.AutoIncrement = false;
				colvarSellerVerifiedTime.IsNullable = true;
				colvarSellerVerifiedTime.IsPrimaryKey = false;
				colvarSellerVerifiedTime.IsForeignKey = false;
				colvarSellerVerifiedTime.IsReadOnly = false;
				colvarSellerVerifiedTime.DefaultSetting = @"";
				colvarSellerVerifiedTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSellerVerifiedTime);
				
				TableSchema.TableColumn colvarFlag = new TableSchema.TableColumn(schema);
				colvarFlag.ColumnName = "flag";
				colvarFlag.DataType = DbType.Int32;
				colvarFlag.MaxLength = 0;
				colvarFlag.AutoIncrement = false;
				colvarFlag.IsNullable = false;
				colvarFlag.IsPrimaryKey = false;
				colvarFlag.IsForeignKey = false;
				colvarFlag.IsReadOnly = false;
				
						colvarFlag.DefaultSetting = @"((0))";
				colvarFlag.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFlag);
				
				TableSchema.TableColumn colvarPaytocompany = new TableSchema.TableColumn(schema);
				colvarPaytocompany.ColumnName = "paytocompany";
				colvarPaytocompany.DataType = DbType.Int32;
				colvarPaytocompany.MaxLength = 0;
				colvarPaytocompany.AutoIncrement = false;
				colvarPaytocompany.IsNullable = false;
				colvarPaytocompany.IsPrimaryKey = false;
				colvarPaytocompany.IsForeignKey = false;
				colvarPaytocompany.IsReadOnly = false;
				
						colvarPaytocompany.DefaultSetting = @"((1))";
				colvarPaytocompany.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPaytocompany);
				
				TableSchema.TableColumn colvarShippedDate = new TableSchema.TableColumn(schema);
				colvarShippedDate.ColumnName = "shipped_date";
				colvarShippedDate.DataType = DbType.DateTime;
				colvarShippedDate.MaxLength = 0;
				colvarShippedDate.AutoIncrement = false;
				colvarShippedDate.IsNullable = true;
				colvarShippedDate.IsPrimaryKey = false;
				colvarShippedDate.IsForeignKey = false;
				colvarShippedDate.IsReadOnly = false;
				colvarShippedDate.DefaultSetting = @"";
				colvarShippedDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarShippedDate);
				
				TableSchema.TableColumn colvarVendorBillingModel = new TableSchema.TableColumn(schema);
				colvarVendorBillingModel.ColumnName = "vendor_billing_model";
				colvarVendorBillingModel.DataType = DbType.Int32;
				colvarVendorBillingModel.MaxLength = 0;
				colvarVendorBillingModel.AutoIncrement = false;
				colvarVendorBillingModel.IsNullable = false;
				colvarVendorBillingModel.IsPrimaryKey = false;
				colvarVendorBillingModel.IsForeignKey = false;
				colvarVendorBillingModel.IsReadOnly = false;
				
						colvarVendorBillingModel.DefaultSetting = @"((0))";
				colvarVendorBillingModel.ForeignKeyTableName = "";
				schema.Columns.Add(colvarVendorBillingModel);
				
				TableSchema.TableColumn colvarVendorReceiptType = new TableSchema.TableColumn(schema);
				colvarVendorReceiptType.ColumnName = "vendor_receipt_type";
				colvarVendorReceiptType.DataType = DbType.Int32;
				colvarVendorReceiptType.MaxLength = 0;
				colvarVendorReceiptType.AutoIncrement = false;
				colvarVendorReceiptType.IsNullable = false;
				colvarVendorReceiptType.IsPrimaryKey = false;
				colvarVendorReceiptType.IsForeignKey = false;
				colvarVendorReceiptType.IsReadOnly = false;
				
						colvarVendorReceiptType.DefaultSetting = @"((0))";
				colvarVendorReceiptType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarVendorReceiptType);
				
				TableSchema.TableColumn colvarRemittanceType = new TableSchema.TableColumn(schema);
				colvarRemittanceType.ColumnName = "remittance_type";
				colvarRemittanceType.DataType = DbType.Int32;
				colvarRemittanceType.MaxLength = 0;
				colvarRemittanceType.AutoIncrement = false;
				colvarRemittanceType.IsNullable = false;
				colvarRemittanceType.IsPrimaryKey = false;
				colvarRemittanceType.IsForeignKey = false;
				colvarRemittanceType.IsReadOnly = false;
				
						colvarRemittanceType.DefaultSetting = @"((0))";
				colvarRemittanceType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarRemittanceType);
				
				TableSchema.TableColumn colvarIsInputTaxRequired = new TableSchema.TableColumn(schema);
				colvarIsInputTaxRequired.ColumnName = "is_input_tax_required";
				colvarIsInputTaxRequired.DataType = DbType.Boolean;
				colvarIsInputTaxRequired.MaxLength = 0;
				colvarIsInputTaxRequired.AutoIncrement = false;
				colvarIsInputTaxRequired.IsNullable = false;
				colvarIsInputTaxRequired.IsPrimaryKey = false;
				colvarIsInputTaxRequired.IsForeignKey = false;
				colvarIsInputTaxRequired.IsReadOnly = false;
				
						colvarIsInputTaxRequired.DefaultSetting = @"((1))";
				colvarIsInputTaxRequired.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsInputTaxRequired);
				
				TableSchema.TableColumn colvarMessage = new TableSchema.TableColumn(schema);
				colvarMessage.ColumnName = "message";
				colvarMessage.DataType = DbType.String;
				colvarMessage.MaxLength = 50;
				colvarMessage.AutoIncrement = false;
				colvarMessage.IsNullable = true;
				colvarMessage.IsPrimaryKey = false;
				colvarMessage.IsForeignKey = false;
				colvarMessage.IsReadOnly = false;
				colvarMessage.DefaultSetting = @"";
				colvarMessage.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMessage);
				
				TableSchema.TableColumn colvarCommission = new TableSchema.TableColumn(schema);
				colvarCommission.ColumnName = "commission";
				colvarCommission.DataType = DbType.Int32;
				colvarCommission.MaxLength = 0;
				colvarCommission.AutoIncrement = false;
				colvarCommission.IsNullable = false;
				colvarCommission.IsPrimaryKey = false;
				colvarCommission.IsForeignKey = false;
				colvarCommission.IsReadOnly = false;
				
						colvarCommission.DefaultSetting = @"((0))";
				colvarCommission.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCommission);
				
				TableSchema.TableColumn colvarFinalBalanceSheetDate = new TableSchema.TableColumn(schema);
				colvarFinalBalanceSheetDate.ColumnName = "final_balance_sheet_date";
				colvarFinalBalanceSheetDate.DataType = DbType.DateTime;
				colvarFinalBalanceSheetDate.MaxLength = 0;
				colvarFinalBalanceSheetDate.AutoIncrement = false;
				colvarFinalBalanceSheetDate.IsNullable = true;
				colvarFinalBalanceSheetDate.IsPrimaryKey = false;
				colvarFinalBalanceSheetDate.IsForeignKey = false;
				colvarFinalBalanceSheetDate.IsReadOnly = false;
				colvarFinalBalanceSheetDate.DefaultSetting = @"";
				colvarFinalBalanceSheetDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFinalBalanceSheetDate);
				
				TableSchema.TableColumn colvarPartiallyPaymentDate = new TableSchema.TableColumn(schema);
				colvarPartiallyPaymentDate.ColumnName = "partially_payment_date";
				colvarPartiallyPaymentDate.DataType = DbType.DateTime;
				colvarPartiallyPaymentDate.MaxLength = 0;
				colvarPartiallyPaymentDate.AutoIncrement = false;
				colvarPartiallyPaymentDate.IsNullable = true;
				colvarPartiallyPaymentDate.IsPrimaryKey = false;
				colvarPartiallyPaymentDate.IsForeignKey = false;
				colvarPartiallyPaymentDate.IsReadOnly = false;
				colvarPartiallyPaymentDate.DefaultSetting = @"";
				colvarPartiallyPaymentDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPartiallyPaymentDate);
				
				TableSchema.TableColumn colvarBalanceSheetCreateDate = new TableSchema.TableColumn(schema);
				colvarBalanceSheetCreateDate.ColumnName = "balance_sheet_create_date";
				colvarBalanceSheetCreateDate.DataType = DbType.DateTime;
				colvarBalanceSheetCreateDate.MaxLength = 0;
				colvarBalanceSheetCreateDate.AutoIncrement = false;
				colvarBalanceSheetCreateDate.IsNullable = true;
				colvarBalanceSheetCreateDate.IsPrimaryKey = false;
				colvarBalanceSheetCreateDate.IsForeignKey = false;
				colvarBalanceSheetCreateDate.IsReadOnly = false;
				colvarBalanceSheetCreateDate.DefaultSetting = @"";
				colvarBalanceSheetCreateDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBalanceSheetCreateDate);
				
				TableSchema.TableColumn colvarOperationSalesId = new TableSchema.TableColumn(schema);
				colvarOperationSalesId.ColumnName = "operation_sales_id";
				colvarOperationSalesId.DataType = DbType.String;
				colvarOperationSalesId.MaxLength = 50;
				colvarOperationSalesId.AutoIncrement = false;
				colvarOperationSalesId.IsNullable = true;
				colvarOperationSalesId.IsPrimaryKey = false;
				colvarOperationSalesId.IsForeignKey = false;
				colvarOperationSalesId.IsReadOnly = false;
				colvarOperationSalesId.DefaultSetting = @"";
				colvarOperationSalesId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOperationSalesId);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("deal_accounting",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("BusinessHourGuid")]
		[Bindable(true)]
		public Guid BusinessHourGuid 
		{
			get { return GetColumnValue<Guid>(Columns.BusinessHourGuid); }
			set { SetColumnValue(Columns.BusinessHourGuid, value); }
		}
		  
		[XmlAttribute("SalesId")]
		[Bindable(true)]
		public string SalesId 
		{
			get { return GetColumnValue<string>(Columns.SalesId); }
			set { SetColumnValue(Columns.SalesId, value); }
		}
		  
		[XmlAttribute("SalesCommission")]
		[Bindable(true)]
		public double? SalesCommission 
		{
			get { return GetColumnValue<double?>(Columns.SalesCommission); }
			set { SetColumnValue(Columns.SalesCommission, value); }
		}
		  
		[XmlAttribute("SalesBonus")]
		[Bindable(true)]
		public decimal? SalesBonus 
		{
			get { return GetColumnValue<decimal?>(Columns.SalesBonus); }
			set { SetColumnValue(Columns.SalesBonus, value); }
		}
		  
		[XmlAttribute("ChargingDate")]
		[Bindable(true)]
		public DateTime? ChargingDate 
		{
			get { return GetColumnValue<DateTime?>(Columns.ChargingDate); }
			set { SetColumnValue(Columns.ChargingDate, value); }
		}
		  
		[XmlAttribute("EnterDate")]
		[Bindable(true)]
		public DateTime? EnterDate 
		{
			get { return GetColumnValue<DateTime?>(Columns.EnterDate); }
			set { SetColumnValue(Columns.EnterDate, value); }
		}
		  
		[XmlAttribute("InvoiceDate")]
		[Bindable(true)]
		public DateTime? InvoiceDate 
		{
			get { return GetColumnValue<DateTime?>(Columns.InvoiceDate); }
			set { SetColumnValue(Columns.InvoiceDate, value); }
		}
		  
		[XmlAttribute("Status")]
		[Bindable(true)]
		public int? Status 
		{
			get { return GetColumnValue<int?>(Columns.Status); }
			set { SetColumnValue(Columns.Status, value); }
		}
		  
		[XmlAttribute("SettleDay1")]
		[Bindable(true)]
		public int? SettleDay1 
		{
			get { return GetColumnValue<int?>(Columns.SettleDay1); }
			set { SetColumnValue(Columns.SettleDay1, value); }
		}
		  
		[XmlAttribute("SettleDay2")]
		[Bindable(true)]
		public int? SettleDay2 
		{
			get { return GetColumnValue<int?>(Columns.SettleDay2); }
			set { SetColumnValue(Columns.SettleDay2, value); }
		}
		  
		[XmlAttribute("SettleDay3")]
		[Bindable(true)]
		public int? SettleDay3 
		{
			get { return GetColumnValue<int?>(Columns.SettleDay3); }
			set { SetColumnValue(Columns.SettleDay3, value); }
		}
		  
		[XmlAttribute("AccCity")]
		[Bindable(true)]
		public int? AccCity 
		{
			get { return GetColumnValue<int?>(Columns.AccCity); }
			set { SetColumnValue(Columns.AccCity, value); }
		}
		  
		[XmlAttribute("DeptId")]
		[Bindable(true)]
		public string DeptId 
		{
			get { return GetColumnValue<string>(Columns.DeptId); }
			set { SetColumnValue(Columns.DeptId, value); }
		}
		  
		[XmlAttribute("DownPayment")]
		[Bindable(true)]
		public double? DownPayment 
		{
			get { return GetColumnValue<double?>(Columns.DownPayment); }
			set { SetColumnValue(Columns.DownPayment, value); }
		}
		  
		[XmlAttribute("SellerVerifiedQuantity")]
		[Bindable(true)]
		public int? SellerVerifiedQuantity 
		{
			get { return GetColumnValue<int?>(Columns.SellerVerifiedQuantity); }
			set { SetColumnValue(Columns.SellerVerifiedQuantity, value); }
		}
		  
		[XmlAttribute("SellerVerifiedTime")]
		[Bindable(true)]
		public DateTime? SellerVerifiedTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.SellerVerifiedTime); }
			set { SetColumnValue(Columns.SellerVerifiedTime, value); }
		}
		  
		[XmlAttribute("Flag")]
		[Bindable(true)]
		public int Flag 
		{
			get { return GetColumnValue<int>(Columns.Flag); }
			set { SetColumnValue(Columns.Flag, value); }
		}
		  
		[XmlAttribute("Paytocompany")]
		[Bindable(true)]
		public int Paytocompany 
		{
			get { return GetColumnValue<int>(Columns.Paytocompany); }
			set { SetColumnValue(Columns.Paytocompany, value); }
		}
		  
		[XmlAttribute("ShippedDate")]
		[Bindable(true)]
		public DateTime? ShippedDate 
		{
			get { return GetColumnValue<DateTime?>(Columns.ShippedDate); }
			set { SetColumnValue(Columns.ShippedDate, value); }
		}
		  
		[XmlAttribute("VendorBillingModel")]
		[Bindable(true)]
		public int VendorBillingModel 
		{
			get { return GetColumnValue<int>(Columns.VendorBillingModel); }
			set { SetColumnValue(Columns.VendorBillingModel, value); }
		}
		  
		[XmlAttribute("VendorReceiptType")]
		[Bindable(true)]
		public int VendorReceiptType 
		{
			get { return GetColumnValue<int>(Columns.VendorReceiptType); }
			set { SetColumnValue(Columns.VendorReceiptType, value); }
		}
		  
		[XmlAttribute("RemittanceType")]
		[Bindable(true)]
		public int RemittanceType 
		{
			get { return GetColumnValue<int>(Columns.RemittanceType); }
			set { SetColumnValue(Columns.RemittanceType, value); }
		}
		  
		[XmlAttribute("IsInputTaxRequired")]
		[Bindable(true)]
		public bool IsInputTaxRequired 
		{
			get { return GetColumnValue<bool>(Columns.IsInputTaxRequired); }
			set { SetColumnValue(Columns.IsInputTaxRequired, value); }
		}
		  
		[XmlAttribute("Message")]
		[Bindable(true)]
		public string Message 
		{
			get { return GetColumnValue<string>(Columns.Message); }
			set { SetColumnValue(Columns.Message, value); }
		}
		  
		[XmlAttribute("Commission")]
		[Bindable(true)]
		public int Commission 
		{
			get { return GetColumnValue<int>(Columns.Commission); }
			set { SetColumnValue(Columns.Commission, value); }
		}
		  
		[XmlAttribute("FinalBalanceSheetDate")]
		[Bindable(true)]
		public DateTime? FinalBalanceSheetDate 
		{
			get { return GetColumnValue<DateTime?>(Columns.FinalBalanceSheetDate); }
			set { SetColumnValue(Columns.FinalBalanceSheetDate, value); }
		}
		  
		[XmlAttribute("PartiallyPaymentDate")]
		[Bindable(true)]
		public DateTime? PartiallyPaymentDate 
		{
			get { return GetColumnValue<DateTime?>(Columns.PartiallyPaymentDate); }
			set { SetColumnValue(Columns.PartiallyPaymentDate, value); }
		}
		  
		[XmlAttribute("BalanceSheetCreateDate")]
		[Bindable(true)]
		public DateTime? BalanceSheetCreateDate 
		{
			get { return GetColumnValue<DateTime?>(Columns.BalanceSheetCreateDate); }
			set { SetColumnValue(Columns.BalanceSheetCreateDate, value); }
		}
		  
		[XmlAttribute("OperationSalesId")]
		[Bindable(true)]
		public string OperationSalesId 
		{
			get { return GetColumnValue<string>(Columns.OperationSalesId); }
			set { SetColumnValue(Columns.OperationSalesId, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (1)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn BusinessHourGuidColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn SalesIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn SalesCommissionColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn SalesBonusColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn ChargingDateColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn EnterDateColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn InvoiceDateColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn StatusColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn SettleDay1Column
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn SettleDay2Column
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn SettleDay3Column
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        public static TableSchema.TableColumn AccCityColumn
        {
            get { return Schema.Columns[11]; }
        }
        
        
        
        public static TableSchema.TableColumn DeptIdColumn
        {
            get { return Schema.Columns[12]; }
        }
        
        
        
        public static TableSchema.TableColumn DownPaymentColumn
        {
            get { return Schema.Columns[13]; }
        }
        
        
        
        public static TableSchema.TableColumn SellerVerifiedQuantityColumn
        {
            get { return Schema.Columns[14]; }
        }
        
        
        
        public static TableSchema.TableColumn SellerVerifiedTimeColumn
        {
            get { return Schema.Columns[15]; }
        }
        
        
        
        public static TableSchema.TableColumn FlagColumn
        {
            get { return Schema.Columns[16]; }
        }
        
        
        
        public static TableSchema.TableColumn PaytocompanyColumn
        {
            get { return Schema.Columns[17]; }
        }
        
        
        
        public static TableSchema.TableColumn ShippedDateColumn
        {
            get { return Schema.Columns[18]; }
        }
        
        
        
        public static TableSchema.TableColumn VendorBillingModelColumn
        {
            get { return Schema.Columns[19]; }
        }
        
        
        
        public static TableSchema.TableColumn VendorReceiptTypeColumn
        {
            get { return Schema.Columns[20]; }
        }
        
        
        
        public static TableSchema.TableColumn RemittanceTypeColumn
        {
            get { return Schema.Columns[21]; }
        }
        
        
        
        public static TableSchema.TableColumn IsInputTaxRequiredColumn
        {
            get { return Schema.Columns[22]; }
        }
        
        
        
        public static TableSchema.TableColumn MessageColumn
        {
            get { return Schema.Columns[23]; }
        }
        
        
        
        public static TableSchema.TableColumn CommissionColumn
        {
            get { return Schema.Columns[24]; }
        }
        
        
        
        public static TableSchema.TableColumn FinalBalanceSheetDateColumn
        {
            get { return Schema.Columns[25]; }
        }
        
        
        
        public static TableSchema.TableColumn PartiallyPaymentDateColumn
        {
            get { return Schema.Columns[26]; }
        }
        
        
        
        public static TableSchema.TableColumn BalanceSheetCreateDateColumn
        {
            get { return Schema.Columns[27]; }
        }
        
        
        
        public static TableSchema.TableColumn OperationSalesIdColumn
        {
            get { return Schema.Columns[28]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string BusinessHourGuid = @"business_hour_guid";
			 public static string SalesId = @"sales_id";
			 public static string SalesCommission = @"sales_commission";
			 public static string SalesBonus = @"sales_bonus";
			 public static string ChargingDate = @"charging_date";
			 public static string EnterDate = @"enter_date";
			 public static string InvoiceDate = @"invoice_date";
			 public static string Status = @"status";
			 public static string SettleDay1 = @"settle_day1";
			 public static string SettleDay2 = @"settle_day2";
			 public static string SettleDay3 = @"settle_day3";
			 public static string AccCity = @"acc_city";
			 public static string DeptId = @"dept_id";
			 public static string DownPayment = @"down_payment";
			 public static string SellerVerifiedQuantity = @"seller_verified_quantity";
			 public static string SellerVerifiedTime = @"seller_verified_time";
			 public static string Flag = @"flag";
			 public static string Paytocompany = @"paytocompany";
			 public static string ShippedDate = @"shipped_date";
			 public static string VendorBillingModel = @"vendor_billing_model";
			 public static string VendorReceiptType = @"vendor_receipt_type";
			 public static string RemittanceType = @"remittance_type";
			 public static string IsInputTaxRequired = @"is_input_tax_required";
			 public static string Message = @"message";
			 public static string Commission = @"commission";
			 public static string FinalBalanceSheetDate = @"final_balance_sheet_date";
			 public static string PartiallyPaymentDate = @"partially_payment_date";
			 public static string BalanceSheetCreateDate = @"balance_sheet_create_date";
			 public static string OperationSalesId = @"operation_sales_id";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

using System;
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using SubSonic;

namespace LunchKingSite.DataOrm
{
	[Serializable]
	public partial class ViewScashTransactionCollection : ReadOnlyList<ViewScashTransaction, ViewScashTransactionCollection>
	{
			public ViewScashTransactionCollection() {}

	}

	[Serializable]
	public partial class ViewScashTransaction : ReadOnlyRecord<ViewScashTransaction>, IReadOnlyRecord
	{
		#region .ctors and Default Settings
		public ViewScashTransaction()
		{
			SetSQLProps();
			InitSetDefaults();
			MarkNew();
		}

		private void InitSetDefaults() { SetDefaults(); }

		public ViewScashTransaction(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}

		protected static void SetSQLProps() { GetTableSchema(); }

		public ViewScashTransaction(object keyID)
		{
				SetSQLProps();
				LoadByKey(keyID);
		}

		public ViewScashTransaction(string columnName, object columnValue)
		{
				SetSQLProps();
				LoadByParam(columnName,columnValue);
		}
		#endregion

		#region Schema and Query Accessor
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}

		private static void GetTableSchema()
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("view_scash_transaction", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				TableSchema.TableColumn colvarDepositId = new TableSchema.TableColumn(schema);
				colvarDepositId.ColumnName = "deposit_id";
				colvarDepositId.DataType = DbType.Int32;
				colvarDepositId.MaxLength = 0;
				colvarDepositId.AutoIncrement = false;
				colvarDepositId.IsNullable = false;
				colvarDepositId.IsPrimaryKey = false;
				colvarDepositId.IsForeignKey = false;
				colvarDepositId.IsReadOnly = false;
				colvarDepositId.DefaultSetting = @"";
				colvarDepositId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDepositId);

				TableSchema.TableColumn colvarWithdrawalId = new TableSchema.TableColumn(schema);
				colvarWithdrawalId.ColumnName = "withdrawal_id";
				colvarWithdrawalId.DataType = DbType.Int32;
				colvarWithdrawalId.MaxLength = 0;
				colvarWithdrawalId.AutoIncrement = false;
				colvarWithdrawalId.IsNullable = false;
				colvarWithdrawalId.IsPrimaryKey = false;
				colvarWithdrawalId.IsForeignKey = false;
				colvarWithdrawalId.IsReadOnly = false;
				colvarWithdrawalId.DefaultSetting = @"";
				colvarWithdrawalId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarWithdrawalId);

				TableSchema.TableColumn colvarCashpointOrderid = new TableSchema.TableColumn(schema);
				colvarCashpointOrderid.ColumnName = "cashpoint_orderid";
				colvarCashpointOrderid.DataType = DbType.Guid;
				colvarCashpointOrderid.MaxLength = 0;
				colvarCashpointOrderid.AutoIncrement = false;
				colvarCashpointOrderid.IsNullable = true;
				colvarCashpointOrderid.IsPrimaryKey = false;
				colvarCashpointOrderid.IsForeignKey = false;
				colvarCashpointOrderid.IsReadOnly = false;
				colvarCashpointOrderid.DefaultSetting = @"";
				colvarCashpointOrderid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCashpointOrderid);

				TableSchema.TableColumn colvarAmount = new TableSchema.TableColumn(schema);
				colvarAmount.ColumnName = "amount";
				colvarAmount.DataType = DbType.Currency;
				colvarAmount.MaxLength = 0;
				colvarAmount.AutoIncrement = false;
				colvarAmount.IsNullable = true;
				colvarAmount.IsPrimaryKey = false;
				colvarAmount.IsForeignKey = false;
				colvarAmount.IsReadOnly = false;
				colvarAmount.DefaultSetting = @"";
				colvarAmount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAmount);

				TableSchema.TableColumn colvarUserId = new TableSchema.TableColumn(schema);
				colvarUserId.ColumnName = "user_id";
				colvarUserId.DataType = DbType.Int32;
				colvarUserId.MaxLength = 0;
				colvarUserId.AutoIncrement = false;
				colvarUserId.IsNullable = false;
				colvarUserId.IsPrimaryKey = false;
				colvarUserId.IsForeignKey = false;
				colvarUserId.IsReadOnly = false;
				colvarUserId.DefaultSetting = @"";
				colvarUserId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUserId);

				TableSchema.TableColumn colvarUserName = new TableSchema.TableColumn(schema);
				colvarUserName.ColumnName = "user_name";
				colvarUserName.DataType = DbType.String;
				colvarUserName.MaxLength = 256;
				colvarUserName.AutoIncrement = false;
				colvarUserName.IsNullable = false;
				colvarUserName.IsPrimaryKey = false;
				colvarUserName.IsForeignKey = false;
				colvarUserName.IsReadOnly = false;
				colvarUserName.DefaultSetting = @"";
				colvarUserName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUserName);

				TableSchema.TableColumn colvarOrderGuid = new TableSchema.TableColumn(schema);
				colvarOrderGuid.ColumnName = "order_guid";
				colvarOrderGuid.DataType = DbType.Guid;
				colvarOrderGuid.MaxLength = 0;
				colvarOrderGuid.AutoIncrement = false;
				colvarOrderGuid.IsNullable = true;
				colvarOrderGuid.IsPrimaryKey = false;
				colvarOrderGuid.IsForeignKey = false;
				colvarOrderGuid.IsReadOnly = false;
				colvarOrderGuid.DefaultSetting = @"";
				colvarOrderGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderGuid);

				TableSchema.TableColumn colvarMessage = new TableSchema.TableColumn(schema);
				colvarMessage.ColumnName = "message";
				colvarMessage.DataType = DbType.String;
				colvarMessage.MaxLength = 200;
				colvarMessage.AutoIncrement = false;
				colvarMessage.IsNullable = true;
				colvarMessage.IsPrimaryKey = false;
				colvarMessage.IsForeignKey = false;
				colvarMessage.IsReadOnly = false;
				colvarMessage.DefaultSetting = @"";
				colvarMessage.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMessage);

				TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
				colvarCreateId.ColumnName = "create_id";
				colvarCreateId.DataType = DbType.AnsiString;
				colvarCreateId.MaxLength = 256;
				colvarCreateId.AutoIncrement = false;
				colvarCreateId.IsNullable = false;
				colvarCreateId.IsPrimaryKey = false;
				colvarCreateId.IsForeignKey = false;
				colvarCreateId.IsReadOnly = false;
				colvarCreateId.DefaultSetting = @"";
				colvarCreateId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateId);

				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);

				TableSchema.TableColumn colvarInvoiced = new TableSchema.TableColumn(schema);
				colvarInvoiced.ColumnName = "invoiced";
				colvarInvoiced.DataType = DbType.Boolean;
				colvarInvoiced.MaxLength = 0;
				colvarInvoiced.AutoIncrement = false;
				colvarInvoiced.IsNullable = false;
				colvarInvoiced.IsPrimaryKey = false;
				colvarInvoiced.IsForeignKey = false;
				colvarInvoiced.IsReadOnly = false;
				colvarInvoiced.DefaultSetting = @"";
				colvarInvoiced.ForeignKeyTableName = "";
				schema.Columns.Add(colvarInvoiced);

				TableSchema.TableColumn colvarOrderClassification = new TableSchema.TableColumn(schema);
				colvarOrderClassification.ColumnName = "order_classification";
				colvarOrderClassification.DataType = DbType.Int32;
				colvarOrderClassification.MaxLength = 0;
				colvarOrderClassification.AutoIncrement = false;
				colvarOrderClassification.IsNullable = true;
				colvarOrderClassification.IsPrimaryKey = false;
				colvarOrderClassification.IsForeignKey = false;
				colvarOrderClassification.IsReadOnly = false;
				colvarOrderClassification.DefaultSetting = @"";
				colvarOrderClassification.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderClassification);

				TableSchema.TableColumn colvarSource = new TableSchema.TableColumn(schema);
				colvarSource.ColumnName = "source";
				colvarSource.DataType = DbType.Int32;
				colvarSource.MaxLength = 0;
				colvarSource.AutoIncrement = false;
				colvarSource.IsNullable = false;
				colvarSource.IsPrimaryKey = false;
				colvarSource.IsForeignKey = false;
				colvarSource.IsReadOnly = false;
				colvarSource.DefaultSetting = @"";
				colvarSource.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSource);

				TableSchema.TableColumn colvarBalance = new TableSchema.TableColumn(schema);
				colvarBalance.ColumnName = "balance";
				colvarBalance.DataType = DbType.Currency;
				colvarBalance.MaxLength = 0;
				colvarBalance.AutoIncrement = false;
				colvarBalance.IsNullable = true;
				colvarBalance.IsPrimaryKey = false;
				colvarBalance.IsForeignKey = false;
				colvarBalance.IsReadOnly = false;
				colvarBalance.DefaultSetting = @"";
				colvarBalance.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBalance);

				TableSchema.TableColumn colvarOrderStatus = new TableSchema.TableColumn(schema);
				colvarOrderStatus.ColumnName = "order_status";
				colvarOrderStatus.DataType = DbType.Int32;
				colvarOrderStatus.MaxLength = 0;
				colvarOrderStatus.AutoIncrement = false;
				colvarOrderStatus.IsNullable = true;
				colvarOrderStatus.IsPrimaryKey = false;
				colvarOrderStatus.IsForeignKey = false;
				colvarOrderStatus.IsReadOnly = false;
				colvarOrderStatus.DefaultSetting = @"";
				colvarOrderStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderStatus);

				TableSchema.TableColumn colvarOrderId = new TableSchema.TableColumn(schema);
				colvarOrderId.ColumnName = "order_id";
				colvarOrderId.DataType = DbType.AnsiString;
				colvarOrderId.MaxLength = 30;
				colvarOrderId.AutoIncrement = false;
				colvarOrderId.IsNullable = true;
				colvarOrderId.IsPrimaryKey = false;
				colvarOrderId.IsForeignKey = false;
				colvarOrderId.IsReadOnly = false;
				colvarOrderId.DefaultSetting = @"";
				colvarOrderId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderId);

				TableSchema.TableColumn colvarSellerGuid = new TableSchema.TableColumn(schema);
				colvarSellerGuid.ColumnName = "seller_GUID";
				colvarSellerGuid.DataType = DbType.Guid;
				colvarSellerGuid.MaxLength = 0;
				colvarSellerGuid.AutoIncrement = false;
				colvarSellerGuid.IsNullable = true;
				colvarSellerGuid.IsPrimaryKey = false;
				colvarSellerGuid.IsForeignKey = false;
				colvarSellerGuid.IsReadOnly = false;
				colvarSellerGuid.DefaultSetting = @"";
				colvarSellerGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSellerGuid);

				TableSchema.TableColumn colvarSellerName = new TableSchema.TableColumn(schema);
				colvarSellerName.ColumnName = "seller_name";
				colvarSellerName.DataType = DbType.String;
				colvarSellerName.MaxLength = 50;
				colvarSellerName.AutoIncrement = false;
				colvarSellerName.IsNullable = true;
				colvarSellerName.IsPrimaryKey = false;
				colvarSellerName.IsForeignKey = false;
				colvarSellerName.IsReadOnly = false;
				colvarSellerName.DefaultSetting = @"";
				colvarSellerName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSellerName);

				TableSchema.TableColumn colvarCouponUsage = new TableSchema.TableColumn(schema);
				colvarCouponUsage.ColumnName = "coupon_usage";
				colvarCouponUsage.DataType = DbType.String;
				colvarCouponUsage.MaxLength = 500;
				colvarCouponUsage.AutoIncrement = false;
				colvarCouponUsage.IsNullable = true;
				colvarCouponUsage.IsPrimaryKey = false;
				colvarCouponUsage.IsForeignKey = false;
				colvarCouponUsage.IsReadOnly = false;
				colvarCouponUsage.DefaultSetting = @"";
				colvarCouponUsage.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCouponUsage);

				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("view_scash_transaction",schema);
			}
		}
		#endregion

		#region Props

		[XmlAttribute("DepositId")]
		[Bindable(true)]
		public int DepositId
		{
			get { return GetColumnValue<int>(Columns.DepositId); }
			set { SetColumnValue(Columns.DepositId, value); }
		}

		[XmlAttribute("WithdrawalId")]
		[Bindable(true)]
		public int WithdrawalId
		{
			get { return GetColumnValue<int>(Columns.WithdrawalId); }
			set { SetColumnValue(Columns.WithdrawalId, value); }
		}

		[XmlAttribute("CashpointOrderid")]
		[Bindable(true)]
		public Guid? CashpointOrderid
		{
			get { return GetColumnValue<Guid?>(Columns.CashpointOrderid); }
			set { SetColumnValue(Columns.CashpointOrderid, value); }
		}

		[XmlAttribute("Amount")]
		[Bindable(true)]
		public decimal? Amount
		{
			get { return GetColumnValue<decimal?>(Columns.Amount); }
			set { SetColumnValue(Columns.Amount, value); }
		}

		[XmlAttribute("UserId")]
		[Bindable(true)]
		public int UserId
		{
			get { return GetColumnValue<int>(Columns.UserId); }
			set { SetColumnValue(Columns.UserId, value); }
		}

		[XmlAttribute("UserName")]
		[Bindable(true)]
		public string UserName
		{
			get { return GetColumnValue<string>(Columns.UserName); }
			set { SetColumnValue(Columns.UserName, value); }
		}

		[XmlAttribute("OrderGuid")]
		[Bindable(true)]
		public Guid? OrderGuid
		{
			get { return GetColumnValue<Guid?>(Columns.OrderGuid); }
			set { SetColumnValue(Columns.OrderGuid, value); }
		}

		[XmlAttribute("Message")]
		[Bindable(true)]
		public string Message
		{
			get { return GetColumnValue<string>(Columns.Message); }
			set { SetColumnValue(Columns.Message, value); }
		}

		[XmlAttribute("CreateId")]
		[Bindable(true)]
		public string CreateId
		{
			get { return GetColumnValue<string>(Columns.CreateId); }
			set { SetColumnValue(Columns.CreateId, value); }
		}

		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}

		[XmlAttribute("Invoiced")]
		[Bindable(true)]
		public bool Invoiced
		{
			get { return GetColumnValue<bool>(Columns.Invoiced); }
			set { SetColumnValue(Columns.Invoiced, value); }
		}

		[XmlAttribute("OrderClassification")]
		[Bindable(true)]
		public int? OrderClassification
		{
			get { return GetColumnValue<int?>(Columns.OrderClassification); }
			set { SetColumnValue(Columns.OrderClassification, value); }
		}

		[XmlAttribute("Source")]
		[Bindable(true)]
		public int Source
		{
			get { return GetColumnValue<int>(Columns.Source); }
			set { SetColumnValue(Columns.Source, value); }
		}

		[XmlAttribute("Balance")]
		[Bindable(true)]
		public decimal? Balance
		{
			get { return GetColumnValue<decimal?>(Columns.Balance); }
			set { SetColumnValue(Columns.Balance, value); }
		}

		[XmlAttribute("OrderStatus")]
		[Bindable(true)]
		public int? OrderStatus
		{
			get { return GetColumnValue<int?>(Columns.OrderStatus); }
			set { SetColumnValue(Columns.OrderStatus, value); }
		}

		[XmlAttribute("OrderId")]
		[Bindable(true)]
		public string OrderId
		{
			get { return GetColumnValue<string>(Columns.OrderId); }
			set { SetColumnValue(Columns.OrderId, value); }
		}

		[XmlAttribute("SellerGuid")]
		[Bindable(true)]
		public Guid? SellerGuid
		{
			get { return GetColumnValue<Guid?>(Columns.SellerGuid); }
			set { SetColumnValue(Columns.SellerGuid, value); }
		}

		[XmlAttribute("SellerName")]
		[Bindable(true)]
		public string SellerName
		{
			get { return GetColumnValue<string>(Columns.SellerName); }
			set { SetColumnValue(Columns.SellerName, value); }
		}

		[XmlAttribute("CouponUsage")]
		[Bindable(true)]
		public string CouponUsage
		{
			get { return GetColumnValue<string>(Columns.CouponUsage); }
			set { SetColumnValue(Columns.CouponUsage, value); }
		}

		#endregion

		#region Typed Columns

		public static TableSchema.TableColumn DepositIdColumn
		{
			get { return Schema.Columns[0]; }
		}

		public static TableSchema.TableColumn WithdrawalIdColumn
		{
			get { return Schema.Columns[1]; }
		}

		public static TableSchema.TableColumn CashpointOrderidColumn
		{
			get { return Schema.Columns[2]; }
		}

		public static TableSchema.TableColumn AmountColumn
		{
			get { return Schema.Columns[3]; }
		}

		public static TableSchema.TableColumn UserIdColumn
		{
			get { return Schema.Columns[4]; }
		}

		public static TableSchema.TableColumn UserNameColumn
		{
			get { return Schema.Columns[5]; }
		}

		public static TableSchema.TableColumn OrderGuidColumn
		{
			get { return Schema.Columns[6]; }
		}

		public static TableSchema.TableColumn MessageColumn
		{
			get { return Schema.Columns[7]; }
		}

		public static TableSchema.TableColumn CreateIdColumn
		{
			get { return Schema.Columns[8]; }
		}

		public static TableSchema.TableColumn CreateTimeColumn
		{
			get { return Schema.Columns[9]; }
		}

		public static TableSchema.TableColumn InvoicedColumn
		{
			get { return Schema.Columns[10]; }
		}

		public static TableSchema.TableColumn OrderClassificationColumn
		{
			get { return Schema.Columns[11]; }
		}

		public static TableSchema.TableColumn SourceColumn
		{
			get { return Schema.Columns[12]; }
		}

		public static TableSchema.TableColumn BalanceColumn
		{
			get { return Schema.Columns[13]; }
		}

		public static TableSchema.TableColumn OrderStatusColumn
		{
			get { return Schema.Columns[14]; }
		}

		public static TableSchema.TableColumn OrderIdColumn
		{
			get { return Schema.Columns[15]; }
		}

		public static TableSchema.TableColumn SellerGuidColumn
		{
			get { return Schema.Columns[16]; }
		}

		public static TableSchema.TableColumn SellerNameColumn
		{
			get { return Schema.Columns[17]; }
		}

		public static TableSchema.TableColumn CouponUsageColumn
		{
			get { return Schema.Columns[18]; }
		}

		#endregion

		#region Columns Struct

		public struct Columns
		{
			public static string DepositId = @"deposit_id";
			public static string WithdrawalId = @"withdrawal_id";
			public static string CashpointOrderid = @"cashpoint_orderid";
			public static string Amount = @"amount";
			public static string UserId = @"user_id";
			public static string UserName = @"user_name";
			public static string OrderGuid = @"order_guid";
			public static string Message = @"message";
			public static string CreateId = @"create_id";
			public static string CreateTime = @"create_time";
			public static string Invoiced = @"invoiced";
			public static string OrderClassification = @"order_classification";
			public static string Source = @"source";
			public static string Balance = @"balance";
			public static string OrderStatus = @"order_status";
			public static string OrderId = @"order_id";
			public static string SellerGuid = @"seller_GUID";
			public static string SellerName = @"seller_name";
			public static string CouponUsage = @"coupon_usage";
		}

		#endregion

		#region IAbstractRecord Members
		public new CT GetColumnValue<CT>(string columnName)
		{
			return base.GetColumnValue<CT>(columnName);
		}
		public object GetColumnValue(string columnName)
		{
			return base.GetColumnValue<object>(columnName);
		}
		#endregion

	}
}

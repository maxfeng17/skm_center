using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the PcpDepositMenu class.
	/// </summary>
    [Serializable]
	public partial class PcpDepositMenuCollection : RepositoryList<PcpDepositMenu, PcpDepositMenuCollection>
	{	   
		public PcpDepositMenuCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>PcpDepositMenuCollection</returns>
		public PcpDepositMenuCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                PcpDepositMenu o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the pcp_deposit_menu table.
	/// </summary>
	[Serializable]
	public partial class PcpDepositMenu : RepositoryRecord<PcpDepositMenu>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public PcpDepositMenu()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public PcpDepositMenu(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("pcp_deposit_menu", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarTitle = new TableSchema.TableColumn(schema);
				colvarTitle.ColumnName = "title";
				colvarTitle.DataType = DbType.String;
				colvarTitle.MaxLength = 100;
				colvarTitle.AutoIncrement = false;
				colvarTitle.IsNullable = false;
				colvarTitle.IsPrimaryKey = false;
				colvarTitle.IsForeignKey = false;
				colvarTitle.IsReadOnly = false;
				colvarTitle.DefaultSetting = @"";
				colvarTitle.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTitle);
				
				TableSchema.TableColumn colvarDescription = new TableSchema.TableColumn(schema);
				colvarDescription.ColumnName = "description";
				colvarDescription.DataType = DbType.String;
				colvarDescription.MaxLength = 200;
				colvarDescription.AutoIncrement = false;
				colvarDescription.IsNullable = true;
				colvarDescription.IsPrimaryKey = false;
				colvarDescription.IsForeignKey = false;
				colvarDescription.IsReadOnly = false;
				colvarDescription.DefaultSetting = @"";
				colvarDescription.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDescription);
				
				TableSchema.TableColumn colvarImageUrl = new TableSchema.TableColumn(schema);
				colvarImageUrl.ColumnName = "image_url";
				colvarImageUrl.DataType = DbType.AnsiString;
				colvarImageUrl.MaxLength = -1;
				colvarImageUrl.AutoIncrement = false;
				colvarImageUrl.IsNullable = true;
				colvarImageUrl.IsPrimaryKey = false;
				colvarImageUrl.IsForeignKey = false;
				colvarImageUrl.IsReadOnly = false;
				colvarImageUrl.DefaultSetting = @"";
				colvarImageUrl.ForeignKeyTableName = "";
				schema.Columns.Add(colvarImageUrl);
				
				TableSchema.TableColumn colvarSTime = new TableSchema.TableColumn(schema);
				colvarSTime.ColumnName = "s_time";
				colvarSTime.DataType = DbType.DateTime;
				colvarSTime.MaxLength = 0;
				colvarSTime.AutoIncrement = false;
				colvarSTime.IsNullable = false;
				colvarSTime.IsPrimaryKey = false;
				colvarSTime.IsForeignKey = false;
				colvarSTime.IsReadOnly = false;
				colvarSTime.DefaultSetting = @"";
				colvarSTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSTime);
				
				TableSchema.TableColumn colvarETime = new TableSchema.TableColumn(schema);
				colvarETime.ColumnName = "e_time";
				colvarETime.DataType = DbType.DateTime;
				colvarETime.MaxLength = 0;
				colvarETime.AutoIncrement = false;
				colvarETime.IsNullable = false;
				colvarETime.IsPrimaryKey = false;
				colvarETime.IsForeignKey = false;
				colvarETime.IsReadOnly = false;
				colvarETime.DefaultSetting = @"";
				colvarETime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarETime);
				
				TableSchema.TableColumn colvarEnabled = new TableSchema.TableColumn(schema);
				colvarEnabled.ColumnName = "enabled";
				colvarEnabled.DataType = DbType.Int32;
				colvarEnabled.MaxLength = 0;
				colvarEnabled.AutoIncrement = false;
				colvarEnabled.IsNullable = false;
				colvarEnabled.IsPrimaryKey = false;
				colvarEnabled.IsForeignKey = false;
				colvarEnabled.IsReadOnly = false;
				colvarEnabled.DefaultSetting = @"";
				colvarEnabled.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEnabled);
				
				TableSchema.TableColumn colvarCreatedId = new TableSchema.TableColumn(schema);
				colvarCreatedId.ColumnName = "created_id";
				colvarCreatedId.DataType = DbType.String;
				colvarCreatedId.MaxLength = 100;
				colvarCreatedId.AutoIncrement = false;
				colvarCreatedId.IsNullable = false;
				colvarCreatedId.IsPrimaryKey = false;
				colvarCreatedId.IsForeignKey = false;
				colvarCreatedId.IsReadOnly = false;
				colvarCreatedId.DefaultSetting = @"";
				colvarCreatedId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreatedId);
				
				TableSchema.TableColumn colvarModifiedTime = new TableSchema.TableColumn(schema);
				colvarModifiedTime.ColumnName = "modified_time";
				colvarModifiedTime.DataType = DbType.DateTime;
				colvarModifiedTime.MaxLength = 0;
				colvarModifiedTime.AutoIncrement = false;
				colvarModifiedTime.IsNullable = true;
				colvarModifiedTime.IsPrimaryKey = false;
				colvarModifiedTime.IsForeignKey = false;
				colvarModifiedTime.IsReadOnly = false;
				colvarModifiedTime.DefaultSetting = @"";
				colvarModifiedTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifiedTime);
				
				TableSchema.TableColumn colvarCreatedTime = new TableSchema.TableColumn(schema);
				colvarCreatedTime.ColumnName = "created_time";
				colvarCreatedTime.DataType = DbType.DateTime;
				colvarCreatedTime.MaxLength = 0;
				colvarCreatedTime.AutoIncrement = false;
				colvarCreatedTime.IsNullable = false;
				colvarCreatedTime.IsPrimaryKey = false;
				colvarCreatedTime.IsForeignKey = false;
				colvarCreatedTime.IsReadOnly = false;
				colvarCreatedTime.DefaultSetting = @"";
				colvarCreatedTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreatedTime);
				
				TableSchema.TableColumn colvarDefaultAmount = new TableSchema.TableColumn(schema);
				colvarDefaultAmount.ColumnName = "default_amount";
				colvarDefaultAmount.DataType = DbType.Int32;
				colvarDefaultAmount.MaxLength = 0;
				colvarDefaultAmount.AutoIncrement = false;
				colvarDefaultAmount.IsNullable = false;
				colvarDefaultAmount.IsPrimaryKey = false;
				colvarDefaultAmount.IsForeignKey = false;
				colvarDefaultAmount.IsReadOnly = false;
				
						colvarDefaultAmount.DefaultSetting = @"((0))";
				colvarDefaultAmount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDefaultAmount);
				
				TableSchema.TableColumn colvarGroupId = new TableSchema.TableColumn(schema);
				colvarGroupId.ColumnName = "group_id";
				colvarGroupId.DataType = DbType.Int32;
				colvarGroupId.MaxLength = 0;
				colvarGroupId.AutoIncrement = false;
				colvarGroupId.IsNullable = false;
				colvarGroupId.IsPrimaryKey = false;
				colvarGroupId.IsForeignKey = false;
				colvarGroupId.IsReadOnly = false;
				
						colvarGroupId.DefaultSetting = @"((0))";
				colvarGroupId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGroupId);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("pcp_deposit_menu",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("Title")]
		[Bindable(true)]
		public string Title 
		{
			get { return GetColumnValue<string>(Columns.Title); }
			set { SetColumnValue(Columns.Title, value); }
		}
		  
		[XmlAttribute("Description")]
		[Bindable(true)]
		public string Description 
		{
			get { return GetColumnValue<string>(Columns.Description); }
			set { SetColumnValue(Columns.Description, value); }
		}
		  
		[XmlAttribute("ImageUrl")]
		[Bindable(true)]
		public string ImageUrl 
		{
			get { return GetColumnValue<string>(Columns.ImageUrl); }
			set { SetColumnValue(Columns.ImageUrl, value); }
		}
		  
		[XmlAttribute("STime")]
		[Bindable(true)]
		public DateTime STime 
		{
			get { return GetColumnValue<DateTime>(Columns.STime); }
			set { SetColumnValue(Columns.STime, value); }
		}
		  
		[XmlAttribute("ETime")]
		[Bindable(true)]
		public DateTime ETime 
		{
			get { return GetColumnValue<DateTime>(Columns.ETime); }
			set { SetColumnValue(Columns.ETime, value); }
		}
		  
		[XmlAttribute("Enabled")]
		[Bindable(true)]
		public int Enabled 
		{
			get { return GetColumnValue<int>(Columns.Enabled); }
			set { SetColumnValue(Columns.Enabled, value); }
		}
		  
		[XmlAttribute("CreatedId")]
		[Bindable(true)]
		public string CreatedId 
		{
			get { return GetColumnValue<string>(Columns.CreatedId); }
			set { SetColumnValue(Columns.CreatedId, value); }
		}
		  
		[XmlAttribute("ModifiedTime")]
		[Bindable(true)]
		public DateTime? ModifiedTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.ModifiedTime); }
			set { SetColumnValue(Columns.ModifiedTime, value); }
		}
		  
		[XmlAttribute("CreatedTime")]
		[Bindable(true)]
		public DateTime CreatedTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreatedTime); }
			set { SetColumnValue(Columns.CreatedTime, value); }
		}
		  
		[XmlAttribute("DefaultAmount")]
		[Bindable(true)]
		public int DefaultAmount 
		{
			get { return GetColumnValue<int>(Columns.DefaultAmount); }
			set { SetColumnValue(Columns.DefaultAmount, value); }
		}
		  
		[XmlAttribute("GroupId")]
		[Bindable(true)]
		public int GroupId 
		{
			get { return GetColumnValue<int>(Columns.GroupId); }
			set { SetColumnValue(Columns.GroupId, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn TitleColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn DescriptionColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn ImageUrlColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn STimeColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn ETimeColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn EnabledColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn CreatedIdColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifiedTimeColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn CreatedTimeColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn DefaultAmountColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        public static TableSchema.TableColumn GroupIdColumn
        {
            get { return Schema.Columns[11]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string Title = @"title";
			 public static string Description = @"description";
			 public static string ImageUrl = @"image_url";
			 public static string STime = @"s_time";
			 public static string ETime = @"e_time";
			 public static string Enabled = @"enabled";
			 public static string CreatedId = @"created_id";
			 public static string ModifiedTime = @"modified_time";
			 public static string CreatedTime = @"created_time";
			 public static string DefaultAmount = @"default_amount";
			 public static string GroupId = @"group_id";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

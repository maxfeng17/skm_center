﻿using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the UserEvaluateDetail class.
    /// </summary>
    [Serializable]
    public partial class UserEvaluateDetailCollection : RepositoryList<UserEvaluateDetail, UserEvaluateDetailCollection>
    {
        public UserEvaluateDetailCollection() { }

        /// <summary>
        /// Filters an existing collection based on the set criteria. This is an in-memory filter
        /// Thanks to developingchris for this!
        /// </summary>
        /// <returns>UserEvaluateDetailCollection</returns>
        public UserEvaluateDetailCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                UserEvaluateDetail o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }


    }
    /// <summary>
    /// This is an ActiveRecord class which wraps the user_evaluate_detail table.
    /// </summary>
    [Serializable]
    public partial class UserEvaluateDetail : RepositoryRecord<UserEvaluateDetail>, IRecordBase
    {
        #region .ctors and Default Settings

        public UserEvaluateDetail()
        {
            SetSQLProps();
            InitSetDefaults();
            MarkNew();
        }

        private void InitSetDefaults() { SetDefaults(); }

        public UserEvaluateDetail(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
                ForceDefaults();
            MarkNew();
        }

        public UserEvaluateDetail(object keyID)
        {
            SetSQLProps();
            InitSetDefaults();
        }

        public UserEvaluateDetail(string columnName, object columnValue)
        {
            SetSQLProps();
            InitSetDefaults();
        }

        protected static void SetSQLProps() { GetTableSchema(); }

        #endregion

        #region Schema and Query Accessor
        public static Query CreateQuery() { return new Query(Schema); }
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                    SetSQLProps();
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("user_evaluate_detail", TableType.Table, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = true;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = true;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                colvarId.DefaultSetting = @"";
                colvarId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarId);

                TableSchema.TableColumn colvarMainId = new TableSchema.TableColumn(schema);
                colvarMainId.ColumnName = "main_id";
                colvarMainId.DataType = DbType.Int32;
                colvarMainId.MaxLength = 0;
                colvarMainId.AutoIncrement = false;
                colvarMainId.IsNullable = false;
                colvarMainId.IsPrimaryKey = false;
                colvarMainId.IsForeignKey = false;
                colvarMainId.IsReadOnly = false;
                colvarMainId.DefaultSetting = @"";
                colvarMainId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarMainId);

                TableSchema.TableColumn colvarItemId = new TableSchema.TableColumn(schema);
                colvarItemId.ColumnName = "item_id";
                colvarItemId.DataType = DbType.Int32;
                colvarItemId.MaxLength = 0;
                colvarItemId.AutoIncrement = false;
                colvarItemId.IsNullable = false;
                colvarItemId.IsPrimaryKey = false;
                colvarItemId.IsForeignKey = false;
                colvarItemId.IsReadOnly = false;
                colvarItemId.DefaultSetting = @"";
                colvarItemId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarItemId);

                TableSchema.TableColumn colvarRating = new TableSchema.TableColumn(schema);
                colvarRating.ColumnName = "rating";
                colvarRating.DataType = DbType.Int32;
                colvarRating.MaxLength = 0;
                colvarRating.AutoIncrement = false;
                colvarRating.IsNullable = true;
                colvarRating.IsPrimaryKey = false;
                colvarRating.IsForeignKey = false;
                colvarRating.IsReadOnly = false;
                colvarRating.DefaultSetting = @"";
                colvarRating.ForeignKeyTableName = "";
                schema.Columns.Add(colvarRating);

                TableSchema.TableColumn colvarComment = new TableSchema.TableColumn(schema);
                colvarComment.ColumnName = "comment";
                colvarComment.DataType = DbType.String;
                colvarComment.MaxLength = 300;
                colvarComment.AutoIncrement = false;
                colvarComment.IsNullable = true;
                colvarComment.IsPrimaryKey = false;
                colvarComment.IsForeignKey = false;
                colvarComment.IsReadOnly = false;
                colvarComment.DefaultSetting = @"";
                colvarComment.ForeignKeyTableName = "";
                schema.Columns.Add(colvarComment);

                TableSchema.TableColumn colvarDataType = new TableSchema.TableColumn(schema);
                colvarDataType.ColumnName = "data_type";
                colvarDataType.DataType = DbType.Int32;
                colvarDataType.MaxLength = 0;
                colvarDataType.AutoIncrement = false;
                colvarDataType.IsNullable = false;
                colvarDataType.IsPrimaryKey = false;
                colvarDataType.IsForeignKey = false;
                colvarDataType.IsReadOnly = false;
                colvarDataType.DefaultSetting = @"((0))";
                colvarDataType.ForeignKeyTableName = "";
                schema.Columns.Add(colvarDataType);

                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("user_evaluate_detail", schema);
            }
        }
        #endregion

        #region Props

        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id
        {
            get { return GetColumnValue<int>(Columns.Id); }
            set { SetColumnValue(Columns.Id, value); }
        }

        [XmlAttribute("MainId")]
        [Bindable(true)]
        public int MainId
        {
            get { return GetColumnValue<int>(Columns.MainId); }
            set { SetColumnValue(Columns.MainId, value); }
        }

        [XmlAttribute("ItemId")]
        [Bindable(true)]
        public int ItemId
        {
            get { return GetColumnValue<int>(Columns.ItemId); }
            set { SetColumnValue(Columns.ItemId, value); }
        }

        [XmlAttribute("Rating")]
        [Bindable(true)]
        public int? Rating
        {
            get { return GetColumnValue<int?>(Columns.Rating); }
            set { SetColumnValue(Columns.Rating, value); }
        }

        [XmlAttribute("Comment")]
        [Bindable(true)]
        public string Comment
        {
            get { return GetColumnValue<string>(Columns.Comment); }
            set { SetColumnValue(Columns.Comment, value); }
        }

        [XmlAttribute("DataType")]
        [Bindable(true)]
        public int DataType
        {
            get { return GetColumnValue<int>(Columns.DataType); }
            set { SetColumnValue(Columns.DataType, value); }
        }

        #endregion

        #region Typed Columns


        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }



        public static TableSchema.TableColumn MainIdColumn
        {
            get { return Schema.Columns[1]; }
        }



        public static TableSchema.TableColumn ItemIdColumn
        {
            get { return Schema.Columns[2]; }
        }



        public static TableSchema.TableColumn RatingColumn
        {
            get { return Schema.Columns[3]; }
        }



        public static TableSchema.TableColumn CommentColumn
        {
            get { return Schema.Columns[4]; }
        }



        public static TableSchema.TableColumn DataTypeColumn
        {
            get { return Schema.Columns[5]; }
        }


        
        #endregion
        #region Columns Struct
        public struct Columns
        {
            public static string Id = @"id";
            public static string MainId = @"main_id";
            public static string ItemId = @"item_id";
            public static string Rating = @"rating";
            public static string Comment = @"comment";
            public static string DataType = @"data_type";
        }
        #endregion

        #region Update PK Collections

        #endregion

        #region Deep Save

        #endregion
    }
}

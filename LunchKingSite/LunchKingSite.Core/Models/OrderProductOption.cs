using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the OrderProductOption class.
	/// </summary>
    [Serializable]
	public partial class OrderProductOptionCollection : RepositoryList<OrderProductOption, OrderProductOptionCollection>
	{	   
		public OrderProductOptionCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>OrderProductOptionCollection</returns>
		public OrderProductOptionCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                OrderProductOption o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the order_product_option table.
	/// </summary>
	[Serializable]
	public partial class OrderProductOption : RepositoryRecord<OrderProductOption>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public OrderProductOption()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public OrderProductOption(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("order_product_option", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarOrderProductId = new TableSchema.TableColumn(schema);
				colvarOrderProductId.ColumnName = "order_product_id";
				colvarOrderProductId.DataType = DbType.Int32;
				colvarOrderProductId.MaxLength = 0;
				colvarOrderProductId.AutoIncrement = false;
				colvarOrderProductId.IsNullable = false;
				colvarOrderProductId.IsPrimaryKey = true;
				colvarOrderProductId.IsForeignKey = true;
				colvarOrderProductId.IsReadOnly = false;
				colvarOrderProductId.DefaultSetting = @"";
				
					colvarOrderProductId.ForeignKeyTableName = "order_product";
				schema.Columns.Add(colvarOrderProductId);
				
				TableSchema.TableColumn colvarOptionId = new TableSchema.TableColumn(schema);
				colvarOptionId.ColumnName = "option_id";
				colvarOptionId.DataType = DbType.Int32;
				colvarOptionId.MaxLength = 0;
				colvarOptionId.AutoIncrement = false;
				colvarOptionId.IsNullable = false;
				colvarOptionId.IsPrimaryKey = true;
				colvarOptionId.IsForeignKey = false;
				colvarOptionId.IsReadOnly = false;
				colvarOptionId.DefaultSetting = @"";
				colvarOptionId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOptionId);
				
				TableSchema.TableColumn colvarIsOldProduct = new TableSchema.TableColumn(schema);
				colvarIsOldProduct.ColumnName = "is_old_product";
				colvarIsOldProduct.DataType = DbType.Boolean;
				colvarIsOldProduct.MaxLength = 0;
				colvarIsOldProduct.AutoIncrement = false;
				colvarIsOldProduct.IsNullable = false;
				colvarIsOldProduct.IsPrimaryKey = false;
				colvarIsOldProduct.IsForeignKey = false;
				colvarIsOldProduct.IsReadOnly = false;
				colvarIsOldProduct.DefaultSetting = @"";
				colvarIsOldProduct.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsOldProduct);
				
				TableSchema.TableColumn colvarOldProductOptions = new TableSchema.TableColumn(schema);
				colvarOldProductOptions.ColumnName = "old_product_options";
				colvarOldProductOptions.DataType = DbType.String;
				colvarOldProductOptions.MaxLength = 200;
				colvarOldProductOptions.AutoIncrement = false;
				colvarOldProductOptions.IsNullable = true;
				colvarOldProductOptions.IsPrimaryKey = false;
				colvarOldProductOptions.IsForeignKey = false;
				colvarOldProductOptions.IsReadOnly = false;
				colvarOldProductOptions.DefaultSetting = @"";
				colvarOldProductOptions.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOldProductOptions);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("order_product_option",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("OrderProductId")]
		[Bindable(true)]
		public int OrderProductId 
		{
			get { return GetColumnValue<int>(Columns.OrderProductId); }
			set { SetColumnValue(Columns.OrderProductId, value); }
		}
		  
		[XmlAttribute("OptionId")]
		[Bindable(true)]
		public int OptionId 
		{
			get { return GetColumnValue<int>(Columns.OptionId); }
			set { SetColumnValue(Columns.OptionId, value); }
		}
		  
		[XmlAttribute("IsOldProduct")]
		[Bindable(true)]
		public bool IsOldProduct 
		{
			get { return GetColumnValue<bool>(Columns.IsOldProduct); }
			set { SetColumnValue(Columns.IsOldProduct, value); }
		}
		  
		[XmlAttribute("OldProductOptions")]
		[Bindable(true)]
		public string OldProductOptions 
		{
			get { return GetColumnValue<string>(Columns.OldProductOptions); }
			set { SetColumnValue(Columns.OldProductOptions, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (1)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn OrderProductIdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn OptionIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn IsOldProductColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn OldProductOptionsColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string OrderProductId = @"order_product_id";
			 public static string OptionId = @"option_id";
			 public static string IsOldProduct = @"is_old_product";
			 public static string OldProductOptions = @"old_product_options";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

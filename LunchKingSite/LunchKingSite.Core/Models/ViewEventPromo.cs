using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewEventPromo class.
    /// </summary>
    [Serializable]
    public partial class ViewEventPromoCollection : ReadOnlyList<ViewEventPromo, ViewEventPromoCollection>
    {        
        public ViewEventPromoCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_event_promo view.
    /// </summary>
    [Serializable]
    public partial class ViewEventPromo : ReadOnlyRecord<ViewEventPromo>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_event_promo", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarEventId = new TableSchema.TableColumn(schema);
                colvarEventId.ColumnName = "EventId";
                colvarEventId.DataType = DbType.Int32;
                colvarEventId.MaxLength = 0;
                colvarEventId.AutoIncrement = false;
                colvarEventId.IsNullable = false;
                colvarEventId.IsPrimaryKey = false;
                colvarEventId.IsForeignKey = false;
                colvarEventId.IsReadOnly = false;
                
                schema.Columns.Add(colvarEventId);
                
                TableSchema.TableColumn colvarEventTitle = new TableSchema.TableColumn(schema);
                colvarEventTitle.ColumnName = "EventTitle";
                colvarEventTitle.DataType = DbType.String;
                colvarEventTitle.MaxLength = 500;
                colvarEventTitle.AutoIncrement = false;
                colvarEventTitle.IsNullable = false;
                colvarEventTitle.IsPrimaryKey = false;
                colvarEventTitle.IsForeignKey = false;
                colvarEventTitle.IsReadOnly = false;
                
                schema.Columns.Add(colvarEventTitle);
                
                TableSchema.TableColumn colvarUrl = new TableSchema.TableColumn(schema);
                colvarUrl.ColumnName = "Url";
                colvarUrl.DataType = DbType.AnsiString;
                colvarUrl.MaxLength = 100;
                colvarUrl.AutoIncrement = false;
                colvarUrl.IsNullable = false;
                colvarUrl.IsPrimaryKey = false;
                colvarUrl.IsForeignKey = false;
                colvarUrl.IsReadOnly = false;
                
                schema.Columns.Add(colvarUrl);
                
                TableSchema.TableColumn colvarCpa = new TableSchema.TableColumn(schema);
                colvarCpa.ColumnName = "Cpa";
                colvarCpa.DataType = DbType.AnsiString;
                colvarCpa.MaxLength = 100;
                colvarCpa.AutoIncrement = false;
                colvarCpa.IsNullable = true;
                colvarCpa.IsPrimaryKey = false;
                colvarCpa.IsForeignKey = false;
                colvarCpa.IsReadOnly = false;
                
                schema.Columns.Add(colvarCpa);
                
                TableSchema.TableColumn colvarStartDate = new TableSchema.TableColumn(schema);
                colvarStartDate.ColumnName = "StartDate";
                colvarStartDate.DataType = DbType.DateTime;
                colvarStartDate.MaxLength = 0;
                colvarStartDate.AutoIncrement = false;
                colvarStartDate.IsNullable = false;
                colvarStartDate.IsPrimaryKey = false;
                colvarStartDate.IsForeignKey = false;
                colvarStartDate.IsReadOnly = false;
                
                schema.Columns.Add(colvarStartDate);
                
                TableSchema.TableColumn colvarEndDate = new TableSchema.TableColumn(schema);
                colvarEndDate.ColumnName = "EndDate";
                colvarEndDate.DataType = DbType.DateTime;
                colvarEndDate.MaxLength = 0;
                colvarEndDate.AutoIncrement = false;
                colvarEndDate.IsNullable = false;
                colvarEndDate.IsPrimaryKey = false;
                colvarEndDate.IsForeignKey = false;
                colvarEndDate.IsReadOnly = false;
                
                schema.Columns.Add(colvarEndDate);
                
                TableSchema.TableColumn colvarMainPic = new TableSchema.TableColumn(schema);
                colvarMainPic.ColumnName = "MainPic";
                colvarMainPic.DataType = DbType.String;
                colvarMainPic.MaxLength = 1073741823;
                colvarMainPic.AutoIncrement = false;
                colvarMainPic.IsNullable = false;
                colvarMainPic.IsPrimaryKey = false;
                colvarMainPic.IsForeignKey = false;
                colvarMainPic.IsReadOnly = false;
                
                schema.Columns.Add(colvarMainPic);
                
                TableSchema.TableColumn colvarBgPic = new TableSchema.TableColumn(schema);
                colvarBgPic.ColumnName = "BgPic";
                colvarBgPic.DataType = DbType.AnsiString;
                colvarBgPic.MaxLength = 200;
                colvarBgPic.AutoIncrement = false;
                colvarBgPic.IsNullable = true;
                colvarBgPic.IsPrimaryKey = false;
                colvarBgPic.IsForeignKey = false;
                colvarBgPic.IsReadOnly = false;
                
                schema.Columns.Add(colvarBgPic);
                
                TableSchema.TableColumn colvarBgColor = new TableSchema.TableColumn(schema);
                colvarBgColor.ColumnName = "BgColor";
                colvarBgColor.DataType = DbType.AnsiString;
                colvarBgColor.MaxLength = 20;
                colvarBgColor.AutoIncrement = false;
                colvarBgColor.IsNullable = true;
                colvarBgColor.IsPrimaryKey = false;
                colvarBgColor.IsForeignKey = false;
                colvarBgColor.IsReadOnly = false;
                
                schema.Columns.Add(colvarBgColor);
                
                TableSchema.TableColumn colvarEventDescription = new TableSchema.TableColumn(schema);
                colvarEventDescription.ColumnName = "EventDescription";
                colvarEventDescription.DataType = DbType.String;
                colvarEventDescription.MaxLength = 1073741823;
                colvarEventDescription.AutoIncrement = false;
                colvarEventDescription.IsNullable = true;
                colvarEventDescription.IsPrimaryKey = false;
                colvarEventDescription.IsForeignKey = false;
                colvarEventDescription.IsReadOnly = false;
                
                schema.Columns.Add(colvarEventDescription);
                
                TableSchema.TableColumn colvarEventStatus = new TableSchema.TableColumn(schema);
                colvarEventStatus.ColumnName = "EventStatus";
                colvarEventStatus.DataType = DbType.Boolean;
                colvarEventStatus.MaxLength = 0;
                colvarEventStatus.AutoIncrement = false;
                colvarEventStatus.IsNullable = false;
                colvarEventStatus.IsPrimaryKey = false;
                colvarEventStatus.IsForeignKey = false;
                colvarEventStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarEventStatus);
                
                TableSchema.TableColumn colvarBtnOriginal = new TableSchema.TableColumn(schema);
                colvarBtnOriginal.ColumnName = "btn_original";
                colvarBtnOriginal.DataType = DbType.AnsiString;
                colvarBtnOriginal.MaxLength = 200;
                colvarBtnOriginal.AutoIncrement = false;
                colvarBtnOriginal.IsNullable = true;
                colvarBtnOriginal.IsPrimaryKey = false;
                colvarBtnOriginal.IsForeignKey = false;
                colvarBtnOriginal.IsReadOnly = false;
                
                schema.Columns.Add(colvarBtnOriginal);
                
                TableSchema.TableColumn colvarBtnHover = new TableSchema.TableColumn(schema);
                colvarBtnHover.ColumnName = "btn_hover";
                colvarBtnHover.DataType = DbType.AnsiString;
                colvarBtnHover.MaxLength = 200;
                colvarBtnHover.AutoIncrement = false;
                colvarBtnHover.IsNullable = true;
                colvarBtnHover.IsPrimaryKey = false;
                colvarBtnHover.IsForeignKey = false;
                colvarBtnHover.IsReadOnly = false;
                
                schema.Columns.Add(colvarBtnHover);
                
                TableSchema.TableColumn colvarBtnActive = new TableSchema.TableColumn(schema);
                colvarBtnActive.ColumnName = "btn_active";
                colvarBtnActive.DataType = DbType.AnsiString;
                colvarBtnActive.MaxLength = 200;
                colvarBtnActive.AutoIncrement = false;
                colvarBtnActive.IsNullable = true;
                colvarBtnActive.IsPrimaryKey = false;
                colvarBtnActive.IsForeignKey = false;
                colvarBtnActive.IsReadOnly = false;
                
                schema.Columns.Add(colvarBtnActive);
                
                TableSchema.TableColumn colvarBtnFontColor = new TableSchema.TableColumn(schema);
                colvarBtnFontColor.ColumnName = "btn_font_color";
                colvarBtnFontColor.DataType = DbType.AnsiString;
                colvarBtnFontColor.MaxLength = 20;
                colvarBtnFontColor.AutoIncrement = false;
                colvarBtnFontColor.IsNullable = true;
                colvarBtnFontColor.IsPrimaryKey = false;
                colvarBtnFontColor.IsForeignKey = false;
                colvarBtnFontColor.IsReadOnly = false;
                
                schema.Columns.Add(colvarBtnFontColor);
                
                TableSchema.TableColumn colvarDealPromoImage = new TableSchema.TableColumn(schema);
                colvarDealPromoImage.ColumnName = "deal_promo_image";
                colvarDealPromoImage.DataType = DbType.String;
                colvarDealPromoImage.MaxLength = 120;
                colvarDealPromoImage.AutoIncrement = false;
                colvarDealPromoImage.IsNullable = true;
                colvarDealPromoImage.IsPrimaryKey = false;
                colvarDealPromoImage.IsForeignKey = false;
                colvarDealPromoImage.IsReadOnly = false;
                
                schema.Columns.Add(colvarDealPromoImage);
                
                TableSchema.TableColumn colvarMobileMainPic = new TableSchema.TableColumn(schema);
                colvarMobileMainPic.ColumnName = "mobile_main_pic";
                colvarMobileMainPic.DataType = DbType.String;
                colvarMobileMainPic.MaxLength = -1;
                colvarMobileMainPic.AutoIncrement = false;
                colvarMobileMainPic.IsNullable = true;
                colvarMobileMainPic.IsPrimaryKey = false;
                colvarMobileMainPic.IsForeignKey = false;
                colvarMobileMainPic.IsReadOnly = false;
                
                schema.Columns.Add(colvarMobileMainPic);
                
                TableSchema.TableColumn colvarBtnFontColorHover = new TableSchema.TableColumn(schema);
                colvarBtnFontColorHover.ColumnName = "btn_font_color_hover";
                colvarBtnFontColorHover.DataType = DbType.AnsiString;
                colvarBtnFontColorHover.MaxLength = 6;
                colvarBtnFontColorHover.AutoIncrement = false;
                colvarBtnFontColorHover.IsNullable = true;
                colvarBtnFontColorHover.IsPrimaryKey = false;
                colvarBtnFontColorHover.IsForeignKey = false;
                colvarBtnFontColorHover.IsReadOnly = false;
                
                schema.Columns.Add(colvarBtnFontColorHover);
                
                TableSchema.TableColumn colvarBtnFontColorActive = new TableSchema.TableColumn(schema);
                colvarBtnFontColorActive.ColumnName = "btn_font_color_active";
                colvarBtnFontColorActive.DataType = DbType.AnsiString;
                colvarBtnFontColorActive.MaxLength = 6;
                colvarBtnFontColorActive.AutoIncrement = false;
                colvarBtnFontColorActive.IsNullable = true;
                colvarBtnFontColorActive.IsPrimaryKey = false;
                colvarBtnFontColorActive.IsForeignKey = false;
                colvarBtnFontColorActive.IsReadOnly = false;
                
                schema.Columns.Add(colvarBtnFontColorActive);
                
                TableSchema.TableColumn colvarSubCategoryBgColor = new TableSchema.TableColumn(schema);
                colvarSubCategoryBgColor.ColumnName = "sub_category_bg_color";
                colvarSubCategoryBgColor.DataType = DbType.AnsiString;
                colvarSubCategoryBgColor.MaxLength = 6;
                colvarSubCategoryBgColor.AutoIncrement = false;
                colvarSubCategoryBgColor.IsNullable = true;
                colvarSubCategoryBgColor.IsPrimaryKey = false;
                colvarSubCategoryBgColor.IsForeignKey = false;
                colvarSubCategoryBgColor.IsReadOnly = false;
                
                schema.Columns.Add(colvarSubCategoryBgColor);
                
                TableSchema.TableColumn colvarSubCategoryFontColor = new TableSchema.TableColumn(schema);
                colvarSubCategoryFontColor.ColumnName = "sub_category_font_color";
                colvarSubCategoryFontColor.DataType = DbType.AnsiString;
                colvarSubCategoryFontColor.MaxLength = 6;
                colvarSubCategoryFontColor.AutoIncrement = false;
                colvarSubCategoryFontColor.IsNullable = true;
                colvarSubCategoryFontColor.IsPrimaryKey = false;
                colvarSubCategoryFontColor.IsForeignKey = false;
                colvarSubCategoryFontColor.IsReadOnly = false;
                
                schema.Columns.Add(colvarSubCategoryFontColor);
                
                TableSchema.TableColumn colvarSubCategoryShowImage = new TableSchema.TableColumn(schema);
                colvarSubCategoryShowImage.ColumnName = "sub_category_show_image";
                colvarSubCategoryShowImage.DataType = DbType.Boolean;
                colvarSubCategoryShowImage.MaxLength = 0;
                colvarSubCategoryShowImage.AutoIncrement = false;
                colvarSubCategoryShowImage.IsNullable = false;
                colvarSubCategoryShowImage.IsPrimaryKey = false;
                colvarSubCategoryShowImage.IsForeignKey = false;
                colvarSubCategoryShowImage.IsReadOnly = false;
                
                schema.Columns.Add(colvarSubCategoryShowImage);
                
                TableSchema.TableColumn colvarSubCategoryBtnColorActive = new TableSchema.TableColumn(schema);
                colvarSubCategoryBtnColorActive.ColumnName = "sub_category_btn_color_active";
                colvarSubCategoryBtnColorActive.DataType = DbType.AnsiString;
                colvarSubCategoryBtnColorActive.MaxLength = 6;
                colvarSubCategoryBtnColorActive.AutoIncrement = false;
                colvarSubCategoryBtnColorActive.IsNullable = true;
                colvarSubCategoryBtnColorActive.IsPrimaryKey = false;
                colvarSubCategoryBtnColorActive.IsForeignKey = false;
                colvarSubCategoryBtnColorActive.IsReadOnly = false;
                
                schema.Columns.Add(colvarSubCategoryBtnColorActive);
                
                TableSchema.TableColumn colvarSubCategoryBtnColorHover = new TableSchema.TableColumn(schema);
                colvarSubCategoryBtnColorHover.ColumnName = "sub_category_btn_color_hover";
                colvarSubCategoryBtnColorHover.DataType = DbType.AnsiString;
                colvarSubCategoryBtnColorHover.MaxLength = 6;
                colvarSubCategoryBtnColorHover.AutoIncrement = false;
                colvarSubCategoryBtnColorHover.IsNullable = true;
                colvarSubCategoryBtnColorHover.IsPrimaryKey = false;
                colvarSubCategoryBtnColorHover.IsForeignKey = false;
                colvarSubCategoryBtnColorHover.IsReadOnly = false;
                
                schema.Columns.Add(colvarSubCategoryBtnColorHover);
                
                TableSchema.TableColumn colvarSubCategoryBtnColorOriginal = new TableSchema.TableColumn(schema);
                colvarSubCategoryBtnColorOriginal.ColumnName = "sub_category_btn_color_original";
                colvarSubCategoryBtnColorOriginal.DataType = DbType.AnsiString;
                colvarSubCategoryBtnColorOriginal.MaxLength = 6;
                colvarSubCategoryBtnColorOriginal.AutoIncrement = false;
                colvarSubCategoryBtnColorOriginal.IsNullable = true;
                colvarSubCategoryBtnColorOriginal.IsPrimaryKey = false;
                colvarSubCategoryBtnColorOriginal.IsForeignKey = false;
                colvarSubCategoryBtnColorOriginal.IsReadOnly = false;
                
                schema.Columns.Add(colvarSubCategoryBtnColorOriginal);
                
                TableSchema.TableColumn colvarSubCategoryBtnImageOriginal = new TableSchema.TableColumn(schema);
                colvarSubCategoryBtnImageOriginal.ColumnName = "sub_category_btn_image_original";
                colvarSubCategoryBtnImageOriginal.DataType = DbType.AnsiString;
                colvarSubCategoryBtnImageOriginal.MaxLength = 150;
                colvarSubCategoryBtnImageOriginal.AutoIncrement = false;
                colvarSubCategoryBtnImageOriginal.IsNullable = true;
                colvarSubCategoryBtnImageOriginal.IsPrimaryKey = false;
                colvarSubCategoryBtnImageOriginal.IsForeignKey = false;
                colvarSubCategoryBtnImageOriginal.IsReadOnly = false;
                
                schema.Columns.Add(colvarSubCategoryBtnImageOriginal);
                
                TableSchema.TableColumn colvarSubCategoryBtnImageHover = new TableSchema.TableColumn(schema);
                colvarSubCategoryBtnImageHover.ColumnName = "sub_category_btn_image_hover";
                colvarSubCategoryBtnImageHover.DataType = DbType.AnsiString;
                colvarSubCategoryBtnImageHover.MaxLength = 150;
                colvarSubCategoryBtnImageHover.AutoIncrement = false;
                colvarSubCategoryBtnImageHover.IsNullable = true;
                colvarSubCategoryBtnImageHover.IsPrimaryKey = false;
                colvarSubCategoryBtnImageHover.IsForeignKey = false;
                colvarSubCategoryBtnImageHover.IsReadOnly = false;
                
                schema.Columns.Add(colvarSubCategoryBtnImageHover);
                
                TableSchema.TableColumn colvarSubCategoryBtnImageActive = new TableSchema.TableColumn(schema);
                colvarSubCategoryBtnImageActive.ColumnName = "sub_category_btn_image_active";
                colvarSubCategoryBtnImageActive.DataType = DbType.AnsiString;
                colvarSubCategoryBtnImageActive.MaxLength = 150;
                colvarSubCategoryBtnImageActive.AutoIncrement = false;
                colvarSubCategoryBtnImageActive.IsNullable = true;
                colvarSubCategoryBtnImageActive.IsPrimaryKey = false;
                colvarSubCategoryBtnImageActive.IsForeignKey = false;
                colvarSubCategoryBtnImageActive.IsReadOnly = false;
                
                schema.Columns.Add(colvarSubCategoryBtnImageActive);
                
                TableSchema.TableColumn colvarSubCategoryBtnFontOriginal = new TableSchema.TableColumn(schema);
                colvarSubCategoryBtnFontOriginal.ColumnName = "sub_category_btn_font_original";
                colvarSubCategoryBtnFontOriginal.DataType = DbType.AnsiString;
                colvarSubCategoryBtnFontOriginal.MaxLength = 6;
                colvarSubCategoryBtnFontOriginal.AutoIncrement = false;
                colvarSubCategoryBtnFontOriginal.IsNullable = true;
                colvarSubCategoryBtnFontOriginal.IsPrimaryKey = false;
                colvarSubCategoryBtnFontOriginal.IsForeignKey = false;
                colvarSubCategoryBtnFontOriginal.IsReadOnly = false;
                
                schema.Columns.Add(colvarSubCategoryBtnFontOriginal);
                
                TableSchema.TableColumn colvarSubCategoryBtnFontHover = new TableSchema.TableColumn(schema);
                colvarSubCategoryBtnFontHover.ColumnName = "sub_category_btn_font_hover";
                colvarSubCategoryBtnFontHover.DataType = DbType.AnsiString;
                colvarSubCategoryBtnFontHover.MaxLength = 6;
                colvarSubCategoryBtnFontHover.AutoIncrement = false;
                colvarSubCategoryBtnFontHover.IsNullable = true;
                colvarSubCategoryBtnFontHover.IsPrimaryKey = false;
                colvarSubCategoryBtnFontHover.IsForeignKey = false;
                colvarSubCategoryBtnFontHover.IsReadOnly = false;
                
                schema.Columns.Add(colvarSubCategoryBtnFontHover);
                
                TableSchema.TableColumn colvarSubCategoryBtnFontActive = new TableSchema.TableColumn(schema);
                colvarSubCategoryBtnFontActive.ColumnName = "sub_category_btn_font_active";
                colvarSubCategoryBtnFontActive.DataType = DbType.AnsiString;
                colvarSubCategoryBtnFontActive.MaxLength = 6;
                colvarSubCategoryBtnFontActive.AutoIncrement = false;
                colvarSubCategoryBtnFontActive.IsNullable = true;
                colvarSubCategoryBtnFontActive.IsPrimaryKey = false;
                colvarSubCategoryBtnFontActive.IsForeignKey = false;
                colvarSubCategoryBtnFontActive.IsReadOnly = false;
                
                schema.Columns.Add(colvarSubCategoryBtnFontActive);
                
                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "Id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = false;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = false;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                
                schema.Columns.Add(colvarId);
                
                TableSchema.TableColumn colvarItemEndDate = new TableSchema.TableColumn(schema);
                colvarItemEndDate.ColumnName = "item_end_date";
                colvarItemEndDate.DataType = DbType.DateTime;
                colvarItemEndDate.MaxLength = 0;
                colvarItemEndDate.AutoIncrement = false;
                colvarItemEndDate.IsNullable = true;
                colvarItemEndDate.IsPrimaryKey = false;
                colvarItemEndDate.IsForeignKey = false;
                colvarItemEndDate.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemEndDate);
                
                TableSchema.TableColumn colvarTitle = new TableSchema.TableColumn(schema);
                colvarTitle.ColumnName = "Title";
                colvarTitle.DataType = DbType.String;
                colvarTitle.MaxLength = 200;
                colvarTitle.AutoIncrement = false;
                colvarTitle.IsNullable = false;
                colvarTitle.IsPrimaryKey = false;
                colvarTitle.IsForeignKey = false;
                colvarTitle.IsReadOnly = false;
                
                schema.Columns.Add(colvarTitle);
                
                TableSchema.TableColumn colvarDescription = new TableSchema.TableColumn(schema);
                colvarDescription.ColumnName = "Description";
                colvarDescription.DataType = DbType.String;
                colvarDescription.MaxLength = 300;
                colvarDescription.AutoIncrement = false;
                colvarDescription.IsNullable = false;
                colvarDescription.IsPrimaryKey = false;
                colvarDescription.IsForeignKey = false;
                colvarDescription.IsReadOnly = false;
                
                schema.Columns.Add(colvarDescription);
                
                TableSchema.TableColumn colvarSeq = new TableSchema.TableColumn(schema);
                colvarSeq.ColumnName = "Seq";
                colvarSeq.DataType = DbType.Int32;
                colvarSeq.MaxLength = 0;
                colvarSeq.AutoIncrement = false;
                colvarSeq.IsNullable = false;
                colvarSeq.IsPrimaryKey = false;
                colvarSeq.IsForeignKey = false;
                colvarSeq.IsReadOnly = false;
                
                schema.Columns.Add(colvarSeq);
                
                TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
                colvarStatus.ColumnName = "Status";
                colvarStatus.DataType = DbType.Boolean;
                colvarStatus.MaxLength = 0;
                colvarStatus.AutoIncrement = false;
                colvarStatus.IsNullable = false;
                colvarStatus.IsPrimaryKey = false;
                colvarStatus.IsForeignKey = false;
                colvarStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarStatus);
                
                TableSchema.TableColumn colvarCategory = new TableSchema.TableColumn(schema);
                colvarCategory.ColumnName = "category";
                colvarCategory.DataType = DbType.String;
                colvarCategory.MaxLength = 200;
                colvarCategory.AutoIncrement = false;
                colvarCategory.IsNullable = false;
                colvarCategory.IsPrimaryKey = false;
                colvarCategory.IsForeignKey = false;
                colvarCategory.IsReadOnly = false;
                
                schema.Columns.Add(colvarCategory);
                
                TableSchema.TableColumn colvarItemId = new TableSchema.TableColumn(schema);
                colvarItemId.ColumnName = "item_id";
                colvarItemId.DataType = DbType.Int32;
                colvarItemId.MaxLength = 0;
                colvarItemId.AutoIncrement = false;
                colvarItemId.IsNullable = false;
                colvarItemId.IsPrimaryKey = false;
                colvarItemId.IsForeignKey = false;
                colvarItemId.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemId);
                
                TableSchema.TableColumn colvarItemStartDate = new TableSchema.TableColumn(schema);
                colvarItemStartDate.ColumnName = "item_start_date";
                colvarItemStartDate.DataType = DbType.DateTime;
                colvarItemStartDate.MaxLength = 0;
                colvarItemStartDate.AutoIncrement = false;
                colvarItemStartDate.IsNullable = true;
                colvarItemStartDate.IsPrimaryKey = false;
                colvarItemStartDate.IsForeignKey = false;
                colvarItemStartDate.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemStartDate);
                
                TableSchema.TableColumn colvarSubCategory = new TableSchema.TableColumn(schema);
                colvarSubCategory.ColumnName = "sub_category";
                colvarSubCategory.DataType = DbType.String;
                colvarSubCategory.MaxLength = 50;
                colvarSubCategory.AutoIncrement = false;
                colvarSubCategory.IsNullable = false;
                colvarSubCategory.IsPrimaryKey = false;
                colvarSubCategory.IsForeignKey = false;
                colvarSubCategory.IsReadOnly = false;
                
                schema.Columns.Add(colvarSubCategory);
                
                TableSchema.TableColumn colvarBusinessHourGuid = new TableSchema.TableColumn(schema);
                colvarBusinessHourGuid.ColumnName = "business_hour_guid";
                colvarBusinessHourGuid.DataType = DbType.Guid;
                colvarBusinessHourGuid.MaxLength = 0;
                colvarBusinessHourGuid.AutoIncrement = false;
                colvarBusinessHourGuid.IsNullable = false;
                colvarBusinessHourGuid.IsPrimaryKey = false;
                colvarBusinessHourGuid.IsForeignKey = false;
                colvarBusinessHourGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourGuid);
                
                TableSchema.TableColumn colvarEntrustSell = new TableSchema.TableColumn(schema);
                colvarEntrustSell.ColumnName = "entrust_sell";
                colvarEntrustSell.DataType = DbType.Int32;
                colvarEntrustSell.MaxLength = 0;
                colvarEntrustSell.AutoIncrement = false;
                colvarEntrustSell.IsNullable = false;
                colvarEntrustSell.IsPrimaryKey = false;
                colvarEntrustSell.IsForeignKey = false;
                colvarEntrustSell.IsReadOnly = false;
                
                schema.Columns.Add(colvarEntrustSell);
                
                TableSchema.TableColumn colvarCityList = new TableSchema.TableColumn(schema);
                colvarCityList.ColumnName = "city_list";
                colvarCityList.DataType = DbType.AnsiString;
                colvarCityList.MaxLength = 255;
                colvarCityList.AutoIncrement = false;
                colvarCityList.IsNullable = true;
                colvarCityList.IsPrimaryKey = false;
                colvarCityList.IsForeignKey = false;
                colvarCityList.IsReadOnly = false;
                
                schema.Columns.Add(colvarCityList);
                
                TableSchema.TableColumn colvarBusinessHourOrderTimeS = new TableSchema.TableColumn(schema);
                colvarBusinessHourOrderTimeS.ColumnName = "business_hour_order_time_s";
                colvarBusinessHourOrderTimeS.DataType = DbType.DateTime;
                colvarBusinessHourOrderTimeS.MaxLength = 0;
                colvarBusinessHourOrderTimeS.AutoIncrement = false;
                colvarBusinessHourOrderTimeS.IsNullable = false;
                colvarBusinessHourOrderTimeS.IsPrimaryKey = false;
                colvarBusinessHourOrderTimeS.IsForeignKey = false;
                colvarBusinessHourOrderTimeS.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourOrderTimeS);
                
                TableSchema.TableColumn colvarBusinessHourOrderTimeE = new TableSchema.TableColumn(schema);
                colvarBusinessHourOrderTimeE.ColumnName = "business_hour_order_time_e";
                colvarBusinessHourOrderTimeE.DataType = DbType.DateTime;
                colvarBusinessHourOrderTimeE.MaxLength = 0;
                colvarBusinessHourOrderTimeE.AutoIncrement = false;
                colvarBusinessHourOrderTimeE.IsNullable = false;
                colvarBusinessHourOrderTimeE.IsPrimaryKey = false;
                colvarBusinessHourOrderTimeE.IsForeignKey = false;
                colvarBusinessHourOrderTimeE.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourOrderTimeE);
                
                TableSchema.TableColumn colvarBusinessHourStatus = new TableSchema.TableColumn(schema);
                colvarBusinessHourStatus.ColumnName = "business_hour_status";
                colvarBusinessHourStatus.DataType = DbType.Int32;
                colvarBusinessHourStatus.MaxLength = 0;
                colvarBusinessHourStatus.AutoIncrement = false;
                colvarBusinessHourStatus.IsNullable = false;
                colvarBusinessHourStatus.IsPrimaryKey = false;
                colvarBusinessHourStatus.IsForeignKey = false;
                colvarBusinessHourStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourStatus);
                
                TableSchema.TableColumn colvarItemOrigPrice = new TableSchema.TableColumn(schema);
                colvarItemOrigPrice.ColumnName = "item_orig_price";
                colvarItemOrigPrice.DataType = DbType.Currency;
                colvarItemOrigPrice.MaxLength = 0;
                colvarItemOrigPrice.AutoIncrement = false;
                colvarItemOrigPrice.IsNullable = false;
                colvarItemOrigPrice.IsPrimaryKey = false;
                colvarItemOrigPrice.IsForeignKey = false;
                colvarItemOrigPrice.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemOrigPrice);
                
                TableSchema.TableColumn colvarItemPrice = new TableSchema.TableColumn(schema);
                colvarItemPrice.ColumnName = "item_price";
                colvarItemPrice.DataType = DbType.Currency;
                colvarItemPrice.MaxLength = 0;
                colvarItemPrice.AutoIncrement = false;
                colvarItemPrice.IsNullable = false;
                colvarItemPrice.IsPrimaryKey = false;
                colvarItemPrice.IsForeignKey = false;
                colvarItemPrice.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemPrice);
                
                TableSchema.TableColumn colvarDiscount = new TableSchema.TableColumn(schema);
                colvarDiscount.ColumnName = "Discount";
                colvarDiscount.DataType = DbType.Currency;
                colvarDiscount.MaxLength = 0;
                colvarDiscount.AutoIncrement = false;
                colvarDiscount.IsNullable = true;
                colvarDiscount.IsPrimaryKey = false;
                colvarDiscount.IsForeignKey = false;
                colvarDiscount.IsReadOnly = false;
                
                schema.Columns.Add(colvarDiscount);
                
                TableSchema.TableColumn colvarImagePath = new TableSchema.TableColumn(schema);
                colvarImagePath.ColumnName = "image_path";
                colvarImagePath.DataType = DbType.String;
                colvarImagePath.MaxLength = 1000;
                colvarImagePath.AutoIncrement = false;
                colvarImagePath.IsNullable = true;
                colvarImagePath.IsPrimaryKey = false;
                colvarImagePath.IsForeignKey = false;
                colvarImagePath.IsReadOnly = false;
                
                schema.Columns.Add(colvarImagePath);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_event_promo",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewEventPromo()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewEventPromo(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewEventPromo(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewEventPromo(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("EventId")]
        [Bindable(true)]
        public int EventId 
	    {
		    get
		    {
			    return GetColumnValue<int>("EventId");
		    }
            set 
		    {
			    SetColumnValue("EventId", value);
            }
        }
	      
        [XmlAttribute("EventTitle")]
        [Bindable(true)]
        public string EventTitle 
	    {
		    get
		    {
			    return GetColumnValue<string>("EventTitle");
		    }
            set 
		    {
			    SetColumnValue("EventTitle", value);
            }
        }
	      
        [XmlAttribute("Url")]
        [Bindable(true)]
        public string Url 
	    {
		    get
		    {
			    return GetColumnValue<string>("Url");
		    }
            set 
		    {
			    SetColumnValue("Url", value);
            }
        }
	      
        [XmlAttribute("Cpa")]
        [Bindable(true)]
        public string Cpa 
	    {
		    get
		    {
			    return GetColumnValue<string>("Cpa");
		    }
            set 
		    {
			    SetColumnValue("Cpa", value);
            }
        }
	      
        [XmlAttribute("StartDate")]
        [Bindable(true)]
        public DateTime StartDate 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("StartDate");
		    }
            set 
		    {
			    SetColumnValue("StartDate", value);
            }
        }
	      
        [XmlAttribute("EndDate")]
        [Bindable(true)]
        public DateTime EndDate 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("EndDate");
		    }
            set 
		    {
			    SetColumnValue("EndDate", value);
            }
        }
	      
        [XmlAttribute("MainPic")]
        [Bindable(true)]
        public string MainPic 
	    {
		    get
		    {
			    return GetColumnValue<string>("MainPic");
		    }
            set 
		    {
			    SetColumnValue("MainPic", value);
            }
        }
	      
        [XmlAttribute("BgPic")]
        [Bindable(true)]
        public string BgPic 
	    {
		    get
		    {
			    return GetColumnValue<string>("BgPic");
		    }
            set 
		    {
			    SetColumnValue("BgPic", value);
            }
        }
	      
        [XmlAttribute("BgColor")]
        [Bindable(true)]
        public string BgColor 
	    {
		    get
		    {
			    return GetColumnValue<string>("BgColor");
		    }
            set 
		    {
			    SetColumnValue("BgColor", value);
            }
        }
	      
        [XmlAttribute("EventDescription")]
        [Bindable(true)]
        public string EventDescription 
	    {
		    get
		    {
			    return GetColumnValue<string>("EventDescription");
		    }
            set 
		    {
			    SetColumnValue("EventDescription", value);
            }
        }
	      
        [XmlAttribute("EventStatus")]
        [Bindable(true)]
        public bool EventStatus 
	    {
		    get
		    {
			    return GetColumnValue<bool>("EventStatus");
		    }
            set 
		    {
			    SetColumnValue("EventStatus", value);
            }
        }
	      
        [XmlAttribute("BtnOriginal")]
        [Bindable(true)]
        public string BtnOriginal 
	    {
		    get
		    {
			    return GetColumnValue<string>("btn_original");
		    }
            set 
		    {
			    SetColumnValue("btn_original", value);
            }
        }
	      
        [XmlAttribute("BtnHover")]
        [Bindable(true)]
        public string BtnHover 
	    {
		    get
		    {
			    return GetColumnValue<string>("btn_hover");
		    }
            set 
		    {
			    SetColumnValue("btn_hover", value);
            }
        }
	      
        [XmlAttribute("BtnActive")]
        [Bindable(true)]
        public string BtnActive 
	    {
		    get
		    {
			    return GetColumnValue<string>("btn_active");
		    }
            set 
		    {
			    SetColumnValue("btn_active", value);
            }
        }
	      
        [XmlAttribute("BtnFontColor")]
        [Bindable(true)]
        public string BtnFontColor 
	    {
		    get
		    {
			    return GetColumnValue<string>("btn_font_color");
		    }
            set 
		    {
			    SetColumnValue("btn_font_color", value);
            }
        }
	      
        [XmlAttribute("DealPromoImage")]
        [Bindable(true)]
        public string DealPromoImage 
	    {
		    get
		    {
			    return GetColumnValue<string>("deal_promo_image");
		    }
            set 
		    {
			    SetColumnValue("deal_promo_image", value);
            }
        }
	      
        [XmlAttribute("MobileMainPic")]
        [Bindable(true)]
        public string MobileMainPic 
	    {
		    get
		    {
			    return GetColumnValue<string>("mobile_main_pic");
		    }
            set 
		    {
			    SetColumnValue("mobile_main_pic", value);
            }
        }
	      
        [XmlAttribute("BtnFontColorHover")]
        [Bindable(true)]
        public string BtnFontColorHover 
	    {
		    get
		    {
			    return GetColumnValue<string>("btn_font_color_hover");
		    }
            set 
		    {
			    SetColumnValue("btn_font_color_hover", value);
            }
        }
	      
        [XmlAttribute("BtnFontColorActive")]
        [Bindable(true)]
        public string BtnFontColorActive 
	    {
		    get
		    {
			    return GetColumnValue<string>("btn_font_color_active");
		    }
            set 
		    {
			    SetColumnValue("btn_font_color_active", value);
            }
        }
	      
        [XmlAttribute("SubCategoryBgColor")]
        [Bindable(true)]
        public string SubCategoryBgColor 
	    {
		    get
		    {
			    return GetColumnValue<string>("sub_category_bg_color");
		    }
            set 
		    {
			    SetColumnValue("sub_category_bg_color", value);
            }
        }
	      
        [XmlAttribute("SubCategoryFontColor")]
        [Bindable(true)]
        public string SubCategoryFontColor 
	    {
		    get
		    {
			    return GetColumnValue<string>("sub_category_font_color");
		    }
            set 
		    {
			    SetColumnValue("sub_category_font_color", value);
            }
        }
	      
        [XmlAttribute("SubCategoryShowImage")]
        [Bindable(true)]
        public bool SubCategoryShowImage 
	    {
		    get
		    {
			    return GetColumnValue<bool>("sub_category_show_image");
		    }
            set 
		    {
			    SetColumnValue("sub_category_show_image", value);
            }
        }
	      
        [XmlAttribute("SubCategoryBtnColorActive")]
        [Bindable(true)]
        public string SubCategoryBtnColorActive 
	    {
		    get
		    {
			    return GetColumnValue<string>("sub_category_btn_color_active");
		    }
            set 
		    {
			    SetColumnValue("sub_category_btn_color_active", value);
            }
        }
	      
        [XmlAttribute("SubCategoryBtnColorHover")]
        [Bindable(true)]
        public string SubCategoryBtnColorHover 
	    {
		    get
		    {
			    return GetColumnValue<string>("sub_category_btn_color_hover");
		    }
            set 
		    {
			    SetColumnValue("sub_category_btn_color_hover", value);
            }
        }
	      
        [XmlAttribute("SubCategoryBtnColorOriginal")]
        [Bindable(true)]
        public string SubCategoryBtnColorOriginal 
	    {
		    get
		    {
			    return GetColumnValue<string>("sub_category_btn_color_original");
		    }
            set 
		    {
			    SetColumnValue("sub_category_btn_color_original", value);
            }
        }
	      
        [XmlAttribute("SubCategoryBtnImageOriginal")]
        [Bindable(true)]
        public string SubCategoryBtnImageOriginal 
	    {
		    get
		    {
			    return GetColumnValue<string>("sub_category_btn_image_original");
		    }
            set 
		    {
			    SetColumnValue("sub_category_btn_image_original", value);
            }
        }
	      
        [XmlAttribute("SubCategoryBtnImageHover")]
        [Bindable(true)]
        public string SubCategoryBtnImageHover 
	    {
		    get
		    {
			    return GetColumnValue<string>("sub_category_btn_image_hover");
		    }
            set 
		    {
			    SetColumnValue("sub_category_btn_image_hover", value);
            }
        }
	      
        [XmlAttribute("SubCategoryBtnImageActive")]
        [Bindable(true)]
        public string SubCategoryBtnImageActive 
	    {
		    get
		    {
			    return GetColumnValue<string>("sub_category_btn_image_active");
		    }
            set 
		    {
			    SetColumnValue("sub_category_btn_image_active", value);
            }
        }
	      
        [XmlAttribute("SubCategoryBtnFontOriginal")]
        [Bindable(true)]
        public string SubCategoryBtnFontOriginal 
	    {
		    get
		    {
			    return GetColumnValue<string>("sub_category_btn_font_original");
		    }
            set 
		    {
			    SetColumnValue("sub_category_btn_font_original", value);
            }
        }
	      
        [XmlAttribute("SubCategoryBtnFontHover")]
        [Bindable(true)]
        public string SubCategoryBtnFontHover 
	    {
		    get
		    {
			    return GetColumnValue<string>("sub_category_btn_font_hover");
		    }
            set 
		    {
			    SetColumnValue("sub_category_btn_font_hover", value);
            }
        }
	      
        [XmlAttribute("SubCategoryBtnFontActive")]
        [Bindable(true)]
        public string SubCategoryBtnFontActive 
	    {
		    get
		    {
			    return GetColumnValue<string>("sub_category_btn_font_active");
		    }
            set 
		    {
			    SetColumnValue("sub_category_btn_font_active", value);
            }
        }
	      
        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id 
	    {
		    get
		    {
			    return GetColumnValue<int>("Id");
		    }
            set 
		    {
			    SetColumnValue("Id", value);
            }
        }
	      
        [XmlAttribute("ItemEndDate")]
        [Bindable(true)]
        public DateTime? ItemEndDate 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("item_end_date");
		    }
            set 
		    {
			    SetColumnValue("item_end_date", value);
            }
        }
	      
        [XmlAttribute("Title")]
        [Bindable(true)]
        public string Title 
	    {
		    get
		    {
			    return GetColumnValue<string>("Title");
		    }
            set 
		    {
			    SetColumnValue("Title", value);
            }
        }
	      
        [XmlAttribute("Description")]
        [Bindable(true)]
        public string Description 
	    {
		    get
		    {
			    return GetColumnValue<string>("Description");
		    }
            set 
		    {
			    SetColumnValue("Description", value);
            }
        }
	      
        [XmlAttribute("Seq")]
        [Bindable(true)]
        public int Seq 
	    {
		    get
		    {
			    return GetColumnValue<int>("Seq");
		    }
            set 
		    {
			    SetColumnValue("Seq", value);
            }
        }
	      
        [XmlAttribute("Status")]
        [Bindable(true)]
        public bool Status 
	    {
		    get
		    {
			    return GetColumnValue<bool>("Status");
		    }
            set 
		    {
			    SetColumnValue("Status", value);
            }
        }
	      
        [XmlAttribute("Category")]
        [Bindable(true)]
        public string Category 
	    {
		    get
		    {
			    return GetColumnValue<string>("category");
		    }
            set 
		    {
			    SetColumnValue("category", value);
            }
        }
	      
        [XmlAttribute("ItemId")]
        [Bindable(true)]
        public int ItemId 
	    {
		    get
		    {
			    return GetColumnValue<int>("item_id");
		    }
            set 
		    {
			    SetColumnValue("item_id", value);
            }
        }
	      
        [XmlAttribute("ItemStartDate")]
        [Bindable(true)]
        public DateTime? ItemStartDate 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("item_start_date");
		    }
            set 
		    {
			    SetColumnValue("item_start_date", value);
            }
        }
	      
        [XmlAttribute("SubCategory")]
        [Bindable(true)]
        public string SubCategory 
	    {
		    get
		    {
			    return GetColumnValue<string>("sub_category");
		    }
            set 
		    {
			    SetColumnValue("sub_category", value);
            }
        }
	      
        [XmlAttribute("BusinessHourGuid")]
        [Bindable(true)]
        public Guid BusinessHourGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("business_hour_guid");
		    }
            set 
		    {
			    SetColumnValue("business_hour_guid", value);
            }
        }
	      
        [XmlAttribute("EntrustSell")]
        [Bindable(true)]
        public int EntrustSell 
	    {
		    get
		    {
			    return GetColumnValue<int>("entrust_sell");
		    }
            set 
		    {
			    SetColumnValue("entrust_sell", value);
            }
        }
	      
        [XmlAttribute("CityList")]
        [Bindable(true)]
        public string CityList 
	    {
		    get
		    {
			    return GetColumnValue<string>("city_list");
		    }
            set 
		    {
			    SetColumnValue("city_list", value);
            }
        }
	      
        [XmlAttribute("BusinessHourOrderTimeS")]
        [Bindable(true)]
        public DateTime BusinessHourOrderTimeS 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("business_hour_order_time_s");
		    }
            set 
		    {
			    SetColumnValue("business_hour_order_time_s", value);
            }
        }
	      
        [XmlAttribute("BusinessHourOrderTimeE")]
        [Bindable(true)]
        public DateTime BusinessHourOrderTimeE 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("business_hour_order_time_e");
		    }
            set 
		    {
			    SetColumnValue("business_hour_order_time_e", value);
            }
        }
	      
        [XmlAttribute("BusinessHourStatus")]
        [Bindable(true)]
        public int BusinessHourStatus 
	    {
		    get
		    {
			    return GetColumnValue<int>("business_hour_status");
		    }
            set 
		    {
			    SetColumnValue("business_hour_status", value);
            }
        }
	      
        [XmlAttribute("ItemOrigPrice")]
        [Bindable(true)]
        public decimal ItemOrigPrice 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("item_orig_price");
		    }
            set 
		    {
			    SetColumnValue("item_orig_price", value);
            }
        }
	      
        [XmlAttribute("ItemPrice")]
        [Bindable(true)]
        public decimal ItemPrice 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("item_price");
		    }
            set 
		    {
			    SetColumnValue("item_price", value);
            }
        }
	      
        [XmlAttribute("Discount")]
        [Bindable(true)]
        public decimal? Discount 
	    {
		    get
		    {
			    return GetColumnValue<decimal?>("Discount");
		    }
            set 
		    {
			    SetColumnValue("Discount", value);
            }
        }
	      
        [XmlAttribute("ImagePath")]
        [Bindable(true)]
        public string ImagePath 
	    {
		    get
		    {
			    return GetColumnValue<string>("image_path");
		    }
            set 
		    {
			    SetColumnValue("image_path", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string EventId = @"EventId";
            
            public static string EventTitle = @"EventTitle";
            
            public static string Url = @"Url";
            
            public static string Cpa = @"Cpa";
            
            public static string StartDate = @"StartDate";
            
            public static string EndDate = @"EndDate";
            
            public static string MainPic = @"MainPic";
            
            public static string BgPic = @"BgPic";
            
            public static string BgColor = @"BgColor";
            
            public static string EventDescription = @"EventDescription";
            
            public static string EventStatus = @"EventStatus";
            
            public static string BtnOriginal = @"btn_original";
            
            public static string BtnHover = @"btn_hover";
            
            public static string BtnActive = @"btn_active";
            
            public static string BtnFontColor = @"btn_font_color";
            
            public static string DealPromoImage = @"deal_promo_image";
            
            public static string MobileMainPic = @"mobile_main_pic";
            
            public static string BtnFontColorHover = @"btn_font_color_hover";
            
            public static string BtnFontColorActive = @"btn_font_color_active";
            
            public static string SubCategoryBgColor = @"sub_category_bg_color";
            
            public static string SubCategoryFontColor = @"sub_category_font_color";
            
            public static string SubCategoryShowImage = @"sub_category_show_image";
            
            public static string SubCategoryBtnColorActive = @"sub_category_btn_color_active";
            
            public static string SubCategoryBtnColorHover = @"sub_category_btn_color_hover";
            
            public static string SubCategoryBtnColorOriginal = @"sub_category_btn_color_original";
            
            public static string SubCategoryBtnImageOriginal = @"sub_category_btn_image_original";
            
            public static string SubCategoryBtnImageHover = @"sub_category_btn_image_hover";
            
            public static string SubCategoryBtnImageActive = @"sub_category_btn_image_active";
            
            public static string SubCategoryBtnFontOriginal = @"sub_category_btn_font_original";
            
            public static string SubCategoryBtnFontHover = @"sub_category_btn_font_hover";
            
            public static string SubCategoryBtnFontActive = @"sub_category_btn_font_active";
            
            public static string Id = @"Id";
            
            public static string ItemEndDate = @"item_end_date";
            
            public static string Title = @"Title";
            
            public static string Description = @"Description";
            
            public static string Seq = @"Seq";
            
            public static string Status = @"Status";
            
            public static string Category = @"category";
            
            public static string ItemId = @"item_id";
            
            public static string ItemStartDate = @"item_start_date";
            
            public static string SubCategory = @"sub_category";
            
            public static string BusinessHourGuid = @"business_hour_guid";
            
            public static string EntrustSell = @"entrust_sell";
            
            public static string CityList = @"city_list";
            
            public static string BusinessHourOrderTimeS = @"business_hour_order_time_s";
            
            public static string BusinessHourOrderTimeE = @"business_hour_order_time_e";
            
            public static string BusinessHourStatus = @"business_hour_status";
            
            public static string ItemOrigPrice = @"item_orig_price";
            
            public static string ItemPrice = @"item_price";
            
            public static string Discount = @"Discount";
            
            public static string ImagePath = @"image_path";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

﻿using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the ViewMembershipCardGroupSplit class.
    /// </summary>
    [Serializable]
    public partial class ViewMembershipCardGroupSplitCollection : ReadOnlyList<ViewMembershipCardGroupSplit, ViewMembershipCardGroupSplitCollection>
    {
        public ViewMembershipCardGroupSplitCollection() { }
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_membership_card_group_split view.
    /// </summary>
    [Serializable]
    public partial class ViewMembershipCardGroupSplit : ReadOnlyRecord<ViewMembershipCardGroupSplit>, IReadOnlyRecord
    {

        #region Default Settings
        protected static void SetSQLProps()
        {
            GetTableSchema();
        }
        #endregion
        #region Schema Accessor
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_membership_card_group_split", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = false;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = false;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;

                schema.Columns.Add(colvarId);

                TableSchema.TableColumn colvarSellerUserId = new TableSchema.TableColumn(schema);
                colvarSellerUserId.ColumnName = "seller_user_id";
                colvarSellerUserId.DataType = DbType.Int32;
                colvarSellerUserId.MaxLength = 0;
                colvarSellerUserId.AutoIncrement = false;
                colvarSellerUserId.IsNullable = false;
                colvarSellerUserId.IsPrimaryKey = false;
                colvarSellerUserId.IsForeignKey = false;
                colvarSellerUserId.IsReadOnly = false;

                schema.Columns.Add(colvarSellerUserId);

                TableSchema.TableColumn colvarCategoryList = new TableSchema.TableColumn(schema);
                colvarCategoryList.ColumnName = "category_list";
                colvarCategoryList.DataType = DbType.AnsiString;
                colvarCategoryList.MaxLength = -1;
                colvarCategoryList.AutoIncrement = false;
                colvarCategoryList.IsNullable = false;
                colvarCategoryList.IsPrimaryKey = false;
                colvarCategoryList.IsForeignKey = false;
                colvarCategoryList.IsReadOnly = false;

                schema.Columns.Add(colvarCategoryList);

                TableSchema.TableColumn colvarHotPoint = new TableSchema.TableColumn(schema);
                colvarHotPoint.ColumnName = "hot_point";
                colvarHotPoint.DataType = DbType.Int32;
                colvarHotPoint.MaxLength = 0;
                colvarHotPoint.AutoIncrement = false;
                colvarHotPoint.IsNullable = false;
                colvarHotPoint.IsPrimaryKey = false;
                colvarHotPoint.IsForeignKey = false;
                colvarHotPoint.IsReadOnly = false;

                schema.Columns.Add(colvarHotPoint);

                TableSchema.TableColumn colvarCategory = new TableSchema.TableColumn(schema);
                colvarCategory.ColumnName = "category";
                colvarCategory.DataType = DbType.String;
                colvarCategory.MaxLength = -1;
                colvarCategory.AutoIncrement = false;
                colvarCategory.IsNullable = true;
                colvarCategory.IsPrimaryKey = false;
                colvarCategory.IsForeignKey = false;
                colvarCategory.IsReadOnly = false;

                schema.Columns.Add(colvarCategory);


                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_membership_card_group_split", schema);
            }
        }
        #endregion

        #region Query Accessor
        public static Query CreateQuery()
        {
            return new Query(Schema);
        }
        #endregion

        #region .ctors
        public ViewMembershipCardGroupSplit()
        {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewMembershipCardGroupSplit(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
            {
                ForceDefaults();
            }
            MarkNew();
        }

        public ViewMembershipCardGroupSplit(object keyID)
        {
            SetSQLProps();
            LoadByKey(keyID);
        }

        public ViewMembershipCardGroupSplit(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName, columnValue);
        }

        #endregion

        #region Props


        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id
        {
            get
            {
                return GetColumnValue<int>("id");
            }
            set
            {
                SetColumnValue("id", value);
            }
        }

        [XmlAttribute("SellerUserId")]
        [Bindable(true)]
        public int SellerUserId
        {
            get
            {
                return GetColumnValue<int>("seller_user_id");
            }
            set
            {
                SetColumnValue("seller_user_id", value);
            }
        }

        [XmlAttribute("CategoryList")]
        [Bindable(true)]
        public string CategoryList
        {
            get
            {
                return GetColumnValue<string>("category_list");
            }
            set
            {
                SetColumnValue("category_list", value);
            }
        }

        [XmlAttribute("HotPoint")]
        [Bindable(true)]
        public int HotPoint
        {
            get
            {
                return GetColumnValue<int>("hot_point");
            }
            set
            {
                SetColumnValue("hot_point", value);
            }
        }

        [XmlAttribute("Category")]
        [Bindable(true)]
        public string Category
        {
            get
            {
                return GetColumnValue<string>("category");
            }
            set
            {
                SetColumnValue("category", value);
            }
        }

        #endregion

        #region Columns Struct
        public struct Columns
        {


            public static string Id = @"id";

            public static string SellerUserId = @"seller_user_id";

            public static string CategoryList = @"category_list";

            public static string HotPoint = @"hot_point";

            public static string Category = @"category";

        }
        #endregion


        #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName)
        {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName)
        {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion

    }
}

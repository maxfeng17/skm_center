using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the SystemFunction class.
    /// </summary>
    [Serializable]
    public partial class SystemFunctionCollection : RepositoryList<SystemFunction, SystemFunctionCollection>
    {
        public SystemFunctionCollection() { }

        /// <summary>
        /// Filters an existing collection based on the set criteria. This is an in-memory filter
        /// Thanks to developingchris for this!
        /// </summary>
        /// <returns>SystemFunctionCollection</returns>
        public SystemFunctionCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                SystemFunction o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }


    }
    /// <summary>
    /// This is an ActiveRecord class which wraps the system_function table.
    /// </summary>
    [Serializable]
    public partial class SystemFunction : RepositoryRecord<SystemFunction>, IRecordBase
    {
        #region .ctors and Default Settings

        public SystemFunction()
        {
            SetSQLProps();
            InitSetDefaults();
            MarkNew();
        }

        private void InitSetDefaults() { SetDefaults(); }

        public SystemFunction(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
                ForceDefaults();
            MarkNew();
        }

        protected static void SetSQLProps() { GetTableSchema(); }

        #endregion

        #region Schema and Query Accessor
        public static Query CreateQuery() { return new Query(Schema); }
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                    SetSQLProps();
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("system_function", TableType.Table, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = true;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = true;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                colvarId.DefaultSetting = @"";
                colvarId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarId);

                TableSchema.TableColumn colvarFuncName = new TableSchema.TableColumn(schema);
                colvarFuncName.ColumnName = "func_name";
                colvarFuncName.DataType = DbType.String;
                colvarFuncName.MaxLength = 256;
                colvarFuncName.AutoIncrement = false;
                colvarFuncName.IsNullable = false;
                colvarFuncName.IsPrimaryKey = false;
                colvarFuncName.IsForeignKey = false;
                colvarFuncName.IsReadOnly = false;
                colvarFuncName.DefaultSetting = @"";
                colvarFuncName.ForeignKeyTableName = "";
                schema.Columns.Add(colvarFuncName);

                TableSchema.TableColumn colvarLink = new TableSchema.TableColumn(schema);
                colvarLink.ColumnName = "link";
                colvarLink.DataType = DbType.String;
                colvarLink.MaxLength = 256;
                colvarLink.AutoIncrement = false;
                colvarLink.IsNullable = false;
                colvarLink.IsPrimaryKey = false;
                colvarLink.IsForeignKey = false;
                colvarLink.IsReadOnly = false;
                colvarLink.DefaultSetting = @"";
                colvarLink.ForeignKeyTableName = "";
                schema.Columns.Add(colvarLink);

                TableSchema.TableColumn colvarDisplayName = new TableSchema.TableColumn(schema);
                colvarDisplayName.ColumnName = "display_name";
                colvarDisplayName.DataType = DbType.String;
                colvarDisplayName.MaxLength = 256;
                colvarDisplayName.AutoIncrement = false;
                colvarDisplayName.IsNullable = false;
                colvarDisplayName.IsPrimaryKey = false;
                colvarDisplayName.IsForeignKey = false;
                colvarDisplayName.IsReadOnly = false;
                colvarDisplayName.DefaultSetting = @"";
                colvarDisplayName.ForeignKeyTableName = "";
                schema.Columns.Add(colvarDisplayName);

                TableSchema.TableColumn colvarParentFunc = new TableSchema.TableColumn(schema);
                colvarParentFunc.ColumnName = "parent_func";
                colvarParentFunc.DataType = DbType.String;
                colvarParentFunc.MaxLength = 256;
                colvarParentFunc.AutoIncrement = false;
                colvarParentFunc.IsNullable = true;
                colvarParentFunc.IsPrimaryKey = false;
                colvarParentFunc.IsForeignKey = false;
                colvarParentFunc.IsReadOnly = false;
                colvarParentFunc.DefaultSetting = @"";
                colvarParentFunc.ForeignKeyTableName = "";
                schema.Columns.Add(colvarParentFunc);

                TableSchema.TableColumn colvarFuncType = new TableSchema.TableColumn(schema);
                colvarFuncType.ColumnName = "func_type";
                colvarFuncType.DataType = DbType.String;
                colvarFuncType.MaxLength = 256;
                colvarFuncType.AutoIncrement = false;
                colvarFuncType.IsNullable = true;
                colvarFuncType.IsPrimaryKey = false;
                colvarFuncType.IsForeignKey = false;
                colvarFuncType.IsReadOnly = false;
                colvarFuncType.DefaultSetting = @"";
                colvarFuncType.ForeignKeyTableName = "";
                schema.Columns.Add(colvarFuncType);

                TableSchema.TableColumn colvarTypeDesc = new TableSchema.TableColumn(schema);
                colvarTypeDesc.ColumnName = "type_desc";
                colvarTypeDesc.DataType = DbType.String;
                colvarTypeDesc.MaxLength = 256;
                colvarTypeDesc.AutoIncrement = false;
                colvarTypeDesc.IsNullable = true;
                colvarTypeDesc.IsPrimaryKey = false;
                colvarTypeDesc.IsForeignKey = false;
                colvarTypeDesc.IsReadOnly = false;
                colvarTypeDesc.DefaultSetting = @"";
                colvarTypeDesc.ForeignKeyTableName = "";
                schema.Columns.Add(colvarTypeDesc);

                TableSchema.TableColumn colvarSortOrder = new TableSchema.TableColumn(schema);
                colvarSortOrder.ColumnName = "sort_order";
                colvarSortOrder.DataType = DbType.Int32;
                colvarSortOrder.MaxLength = 0;
                colvarSortOrder.AutoIncrement = false;
                colvarSortOrder.IsNullable = false;
                colvarSortOrder.IsPrimaryKey = false;
                colvarSortOrder.IsForeignKey = false;
                colvarSortOrder.IsReadOnly = false;
                colvarSortOrder.DefaultSetting = @"";
                colvarSortOrder.ForeignKeyTableName = "";
                schema.Columns.Add(colvarSortOrder);

                TableSchema.TableColumn colvarParentSortOrder = new TableSchema.TableColumn(schema);
                colvarParentSortOrder.ColumnName = "parent_sort_order";
                colvarParentSortOrder.DataType = DbType.Int32;
                colvarParentSortOrder.MaxLength = 0;
                colvarParentSortOrder.AutoIncrement = false;
                colvarParentSortOrder.IsNullable = false;
                colvarParentSortOrder.IsPrimaryKey = false;
                colvarParentSortOrder.IsForeignKey = false;
                colvarParentSortOrder.IsReadOnly = false;
                colvarParentSortOrder.DefaultSetting = @"";
                colvarParentSortOrder.ForeignKeyTableName = "";
                schema.Columns.Add(colvarParentSortOrder);

                TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
                colvarCreateId.ColumnName = "create_id";
                colvarCreateId.DataType = DbType.String;
                colvarCreateId.MaxLength = 256;
                colvarCreateId.AutoIncrement = false;
                colvarCreateId.IsNullable = false;
                colvarCreateId.IsPrimaryKey = false;
                colvarCreateId.IsForeignKey = false;
                colvarCreateId.IsReadOnly = false;
                colvarCreateId.DefaultSetting = @"";
                colvarCreateId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateId);

                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = false;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                colvarCreateTime.DefaultSetting = @"";
                colvarCreateTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateTime);

                TableSchema.TableColumn colvarModifyId = new TableSchema.TableColumn(schema);
                colvarModifyId.ColumnName = "modify_id";
                colvarModifyId.DataType = DbType.String;
                colvarModifyId.MaxLength = 256;
                colvarModifyId.AutoIncrement = false;
                colvarModifyId.IsNullable = true;
                colvarModifyId.IsPrimaryKey = false;
                colvarModifyId.IsForeignKey = false;
                colvarModifyId.IsReadOnly = false;
                colvarModifyId.DefaultSetting = @"";
                colvarModifyId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarModifyId);

                TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
                colvarModifyTime.ColumnName = "modify_time";
                colvarModifyTime.DataType = DbType.DateTime;
                colvarModifyTime.MaxLength = 0;
                colvarModifyTime.AutoIncrement = false;
                colvarModifyTime.IsNullable = true;
                colvarModifyTime.IsPrimaryKey = false;
                colvarModifyTime.IsForeignKey = false;
                colvarModifyTime.IsReadOnly = false;
                colvarModifyTime.DefaultSetting = @"";
                colvarModifyTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarModifyTime);

                TableSchema.TableColumn colvarVisible = new TableSchema.TableColumn(schema);
                colvarVisible.ColumnName = "visible";
                colvarVisible.DataType = DbType.Boolean;
                colvarVisible.MaxLength = 0;
                colvarVisible.AutoIncrement = false;
                colvarVisible.IsNullable = false;
                colvarVisible.IsPrimaryKey = false;
                colvarVisible.IsForeignKey = false;
                colvarVisible.IsReadOnly = false;

                colvarVisible.DefaultSetting = @"((1))";
                colvarVisible.ForeignKeyTableName = "";
                schema.Columns.Add(colvarVisible);

                TableSchema.TableColumn colvarEipUsed = new TableSchema.TableColumn(schema);
                colvarEipUsed.ColumnName = "eip_used";
                colvarEipUsed.DataType = DbType.Boolean;
                colvarEipUsed.MaxLength = 0;
                colvarEipUsed.AutoIncrement = false;
                colvarEipUsed.IsNullable = true;
                colvarEipUsed.IsPrimaryKey = false;
                colvarEipUsed.IsForeignKey = false;
                colvarEipUsed.IsReadOnly = false;
                colvarEipUsed.DefaultSetting = @"";
                colvarEipUsed.ForeignKeyTableName = "";
                schema.Columns.Add(colvarEipUsed);

                TableSchema.TableColumn colvarMenuIcon = new TableSchema.TableColumn(schema);
                colvarMenuIcon.ColumnName = "menu_icon";
                colvarMenuIcon.DataType = DbType.AnsiString;
                colvarMenuIcon.MaxLength = 50;
                colvarMenuIcon.AutoIncrement = false;
                colvarMenuIcon.IsNullable = true;
                colvarMenuIcon.IsPrimaryKey = false;
                colvarMenuIcon.IsForeignKey = false;
                colvarMenuIcon.IsReadOnly = false;
                colvarMenuIcon.DefaultSetting = @"";
                colvarMenuIcon.ForeignKeyTableName = "";
                schema.Columns.Add(colvarMenuIcon);

                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("system_function", schema);
            }
        }
        #endregion

        #region Props

        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id
        {
            get { return GetColumnValue<int>(Columns.Id); }
            set { SetColumnValue(Columns.Id, value); }
        }

        [XmlAttribute("FuncName")]
        [Bindable(true)]
        public string FuncName
        {
            get { return GetColumnValue<string>(Columns.FuncName); }
            set { SetColumnValue(Columns.FuncName, value); }
        }

        [XmlAttribute("Link")]
        [Bindable(true)]
        public string Link
        {
            get { return GetColumnValue<string>(Columns.Link); }
            set { SetColumnValue(Columns.Link, value); }
        }

        [XmlAttribute("DisplayName")]
        [Bindable(true)]
        public string DisplayName
        {
            get { return GetColumnValue<string>(Columns.DisplayName); }
            set { SetColumnValue(Columns.DisplayName, value); }
        }

        [XmlAttribute("ParentFunc")]
        [Bindable(true)]
        public string ParentFunc
        {
            get { return GetColumnValue<string>(Columns.ParentFunc); }
            set { SetColumnValue(Columns.ParentFunc, value); }
        }

        [XmlAttribute("FuncType")]
        [Bindable(true)]
        public string FuncType
        {
            get { return GetColumnValue<string>(Columns.FuncType); }
            set { SetColumnValue(Columns.FuncType, value); }
        }

        [XmlAttribute("TypeDesc")]
        [Bindable(true)]
        public string TypeDesc
        {
            get { return GetColumnValue<string>(Columns.TypeDesc); }
            set { SetColumnValue(Columns.TypeDesc, value); }
        }

        [XmlAttribute("SortOrder")]
        [Bindable(true)]
        public int SortOrder
        {
            get { return GetColumnValue<int>(Columns.SortOrder); }
            set { SetColumnValue(Columns.SortOrder, value); }
        }

        [XmlAttribute("ParentSortOrder")]
        [Bindable(true)]
        public int ParentSortOrder
        {
            get { return GetColumnValue<int>(Columns.ParentSortOrder); }
            set { SetColumnValue(Columns.ParentSortOrder, value); }
        }

        [XmlAttribute("CreateId")]
        [Bindable(true)]
        public string CreateId
        {
            get { return GetColumnValue<string>(Columns.CreateId); }
            set { SetColumnValue(Columns.CreateId, value); }
        }

        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime CreateTime
        {
            get { return GetColumnValue<DateTime>(Columns.CreateTime); }
            set { SetColumnValue(Columns.CreateTime, value); }
        }

        [XmlAttribute("ModifyId")]
        [Bindable(true)]
        public string ModifyId
        {
            get { return GetColumnValue<string>(Columns.ModifyId); }
            set { SetColumnValue(Columns.ModifyId, value); }
        }

        [XmlAttribute("ModifyTime")]
        [Bindable(true)]
        public DateTime? ModifyTime
        {
            get { return GetColumnValue<DateTime?>(Columns.ModifyTime); }
            set { SetColumnValue(Columns.ModifyTime, value); }
        }

        [XmlAttribute("Visible")]
        [Bindable(true)]
        public bool Visible
        {
            get { return GetColumnValue<bool>(Columns.Visible); }
            set { SetColumnValue(Columns.Visible, value); }
        }

        [XmlAttribute("EipUsed")]
        [Bindable(true)]
        public bool? EipUsed
        {
            get { return GetColumnValue<bool?>(Columns.EipUsed); }
            set { SetColumnValue(Columns.EipUsed, value); }
        }

        [XmlAttribute("MenuIcon")]
        [Bindable(true)]
        public string MenuIcon
        {
            get { return GetColumnValue<string>(Columns.MenuIcon); }
            set { SetColumnValue(Columns.MenuIcon, value); }
        }

        #endregion




        //no foreign key tables defined (0)



        //no ManyToMany tables defined (1)





        #region Typed Columns


        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }



        public static TableSchema.TableColumn FuncNameColumn
        {
            get { return Schema.Columns[1]; }
        }



        public static TableSchema.TableColumn LinkColumn
        {
            get { return Schema.Columns[2]; }
        }



        public static TableSchema.TableColumn DisplayNameColumn
        {
            get { return Schema.Columns[3]; }
        }



        public static TableSchema.TableColumn ParentFuncColumn
        {
            get { return Schema.Columns[4]; }
        }



        public static TableSchema.TableColumn FuncTypeColumn
        {
            get { return Schema.Columns[5]; }
        }



        public static TableSchema.TableColumn TypeDescColumn
        {
            get { return Schema.Columns[6]; }
        }



        public static TableSchema.TableColumn SortOrderColumn
        {
            get { return Schema.Columns[7]; }
        }



        public static TableSchema.TableColumn ParentSortOrderColumn
        {
            get { return Schema.Columns[8]; }
        }



        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[9]; }
        }



        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[10]; }
        }



        public static TableSchema.TableColumn ModifyIdColumn
        {
            get { return Schema.Columns[11]; }
        }



        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[12]; }
        }



        public static TableSchema.TableColumn VisibleColumn
        {
            get { return Schema.Columns[13]; }
        }



        public static TableSchema.TableColumn EipUsedColumn
        {
            get { return Schema.Columns[14]; }
        }



        public static TableSchema.TableColumn MenuIconColumn
        {
            get { return Schema.Columns[15]; }
        }



        #endregion
        #region Columns Struct
        public struct Columns
        {
            public static string Id = @"id";
            public static string FuncName = @"func_name";
            public static string Link = @"link";
            public static string DisplayName = @"display_name";
            public static string ParentFunc = @"parent_func";
            public static string FuncType = @"func_type";
            public static string TypeDesc = @"type_desc";
            public static string SortOrder = @"sort_order";
            public static string ParentSortOrder = @"parent_sort_order";
            public static string CreateId = @"create_id";
            public static string CreateTime = @"create_time";
            public static string ModifyId = @"modify_id";
            public static string ModifyTime = @"modify_time";
            public static string Visible = @"visible";
            public static string EipUsed = @"eip_used";
            public static string MenuIcon = @"menu_icon";

        }
        #endregion

        #region Update PK Collections

        #endregion

        #region Deep Save

        #endregion
    }
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewEntrustSellReceipt class.
    /// </summary>
    [Serializable]
    public partial class ViewEntrustSellReceiptCollection : ReadOnlyList<ViewEntrustSellReceipt, ViewEntrustSellReceiptCollection>
    {        
        public ViewEntrustSellReceiptCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_entrust_sell_receipt view.
    /// </summary>
    [Serializable]
    public partial class ViewEntrustSellReceipt : ReadOnlyRecord<ViewEntrustSellReceipt>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_entrust_sell_receipt", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarVerifiedTime = new TableSchema.TableColumn(schema);
                colvarVerifiedTime.ColumnName = "verified_time";
                colvarVerifiedTime.DataType = DbType.DateTime;
                colvarVerifiedTime.MaxLength = 0;
                colvarVerifiedTime.AutoIncrement = false;
                colvarVerifiedTime.IsNullable = true;
                colvarVerifiedTime.IsPrimaryKey = false;
                colvarVerifiedTime.IsForeignKey = false;
                colvarVerifiedTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarVerifiedTime);
                
                TableSchema.TableColumn colvarOrderGuid = new TableSchema.TableColumn(schema);
                colvarOrderGuid.ColumnName = "order_guid";
                colvarOrderGuid.DataType = DbType.Guid;
                colvarOrderGuid.MaxLength = 0;
                colvarOrderGuid.AutoIncrement = false;
                colvarOrderGuid.IsNullable = false;
                colvarOrderGuid.IsPrimaryKey = false;
                colvarOrderGuid.IsForeignKey = false;
                colvarOrderGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderGuid);
                
                TableSchema.TableColumn colvarOrderId = new TableSchema.TableColumn(schema);
                colvarOrderId.ColumnName = "order_id";
                colvarOrderId.DataType = DbType.AnsiString;
                colvarOrderId.MaxLength = 45;
                colvarOrderId.AutoIncrement = false;
                colvarOrderId.IsNullable = true;
                colvarOrderId.IsPrimaryKey = false;
                colvarOrderId.IsForeignKey = false;
                colvarOrderId.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderId);
                
                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = false;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = false;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                
                schema.Columns.Add(colvarId);
                
                TableSchema.TableColumn colvarOriginalOrderId = new TableSchema.TableColumn(schema);
                colvarOriginalOrderId.ColumnName = "original_order_id";
                colvarOriginalOrderId.DataType = DbType.AnsiString;
                colvarOriginalOrderId.MaxLength = 30;
                colvarOriginalOrderId.AutoIncrement = false;
                colvarOriginalOrderId.IsNullable = true;
                colvarOriginalOrderId.IsPrimaryKey = false;
                colvarOriginalOrderId.IsForeignKey = false;
                colvarOriginalOrderId.IsReadOnly = false;
                
                schema.Columns.Add(colvarOriginalOrderId);
                
                TableSchema.TableColumn colvarProductQc = new TableSchema.TableColumn(schema);
                colvarProductQc.ColumnName = "product_qc";
                colvarProductQc.DataType = DbType.Guid;
                colvarProductQc.MaxLength = 0;
                colvarProductQc.AutoIncrement = false;
                colvarProductQc.IsNullable = false;
                colvarProductQc.IsPrimaryKey = false;
                colvarProductQc.IsForeignKey = false;
                colvarProductQc.IsReadOnly = false;
                
                schema.Columns.Add(colvarProductQc);
                
                TableSchema.TableColumn colvarBuyerName = new TableSchema.TableColumn(schema);
                colvarBuyerName.ColumnName = "buyer_name";
                colvarBuyerName.DataType = DbType.String;
                colvarBuyerName.MaxLength = 50;
                colvarBuyerName.AutoIncrement = false;
                colvarBuyerName.IsNullable = true;
                colvarBuyerName.IsPrimaryKey = false;
                colvarBuyerName.IsForeignKey = false;
                colvarBuyerName.IsReadOnly = false;
                
                schema.Columns.Add(colvarBuyerName);
                
                TableSchema.TableColumn colvarMemberId = new TableSchema.TableColumn(schema);
                colvarMemberId.ColumnName = "member_id";
                colvarMemberId.DataType = DbType.AnsiString;
                colvarMemberId.MaxLength = 50;
                colvarMemberId.AutoIncrement = false;
                colvarMemberId.IsNullable = false;
                colvarMemberId.IsPrimaryKey = false;
                colvarMemberId.IsForeignKey = false;
                colvarMemberId.IsReadOnly = false;
                
                schema.Columns.Add(colvarMemberId);
                
                TableSchema.TableColumn colvarMemberName = new TableSchema.TableColumn(schema);
                colvarMemberName.ColumnName = "member_name";
                colvarMemberName.DataType = DbType.String;
                colvarMemberName.MaxLength = 50;
                colvarMemberName.AutoIncrement = false;
                colvarMemberName.IsNullable = false;
                colvarMemberName.IsPrimaryKey = false;
                colvarMemberName.IsForeignKey = false;
                colvarMemberName.IsReadOnly = false;
                
                schema.Columns.Add(colvarMemberName);
                
                TableSchema.TableColumn colvarSellerName = new TableSchema.TableColumn(schema);
                colvarSellerName.ColumnName = "seller_name";
                colvarSellerName.DataType = DbType.String;
                colvarSellerName.MaxLength = 50;
                colvarSellerName.AutoIncrement = false;
                colvarSellerName.IsNullable = false;
                colvarSellerName.IsPrimaryKey = false;
                colvarSellerName.IsForeignKey = false;
                colvarSellerName.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerName);
                
                TableSchema.TableColumn colvarItemName = new TableSchema.TableColumn(schema);
                colvarItemName.ColumnName = "item_name";
                colvarItemName.DataType = DbType.String;
                colvarItemName.MaxLength = 250;
                colvarItemName.AutoIncrement = false;
                colvarItemName.IsNullable = true;
                colvarItemName.IsPrimaryKey = false;
                colvarItemName.IsForeignKey = false;
                colvarItemName.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemName);
                
                TableSchema.TableColumn colvarComId = new TableSchema.TableColumn(schema);
                colvarComId.ColumnName = "com_id";
                colvarComId.DataType = DbType.AnsiString;
                colvarComId.MaxLength = 10;
                colvarComId.AutoIncrement = false;
                colvarComId.IsNullable = true;
                colvarComId.IsPrimaryKey = false;
                colvarComId.IsForeignKey = false;
                colvarComId.IsReadOnly = false;
                
                schema.Columns.Add(colvarComId);
                
                TableSchema.TableColumn colvarBuyerAddress = new TableSchema.TableColumn(schema);
                colvarBuyerAddress.ColumnName = "buyer_address";
                colvarBuyerAddress.DataType = DbType.String;
                colvarBuyerAddress.MaxLength = 250;
                colvarBuyerAddress.AutoIncrement = false;
                colvarBuyerAddress.IsNullable = true;
                colvarBuyerAddress.IsPrimaryKey = false;
                colvarBuyerAddress.IsForeignKey = false;
                colvarBuyerAddress.IsReadOnly = false;
                
                schema.Columns.Add(colvarBuyerAddress);
                
                TableSchema.TableColumn colvarAccountCode = new TableSchema.TableColumn(schema);
                colvarAccountCode.ColumnName = "account_code";
                colvarAccountCode.DataType = DbType.AnsiString;
                colvarAccountCode.MaxLength = 1;
                colvarAccountCode.AutoIncrement = false;
                colvarAccountCode.IsNullable = false;
                colvarAccountCode.IsPrimaryKey = false;
                colvarAccountCode.IsForeignKey = false;
                colvarAccountCode.IsReadOnly = false;
                
                schema.Columns.Add(colvarAccountCode);
                
                TableSchema.TableColumn colvarItemQuantity = new TableSchema.TableColumn(schema);
                colvarItemQuantity.ColumnName = "item_quantity";
                colvarItemQuantity.DataType = DbType.Int32;
                colvarItemQuantity.MaxLength = 0;
                colvarItemQuantity.AutoIncrement = false;
                colvarItemQuantity.IsNullable = false;
                colvarItemQuantity.IsPrimaryKey = false;
                colvarItemQuantity.IsForeignKey = false;
                colvarItemQuantity.IsReadOnly = false;
                
                schema.Columns.Add(colvarItemQuantity);
                
                TableSchema.TableColumn colvarUnitPrice = new TableSchema.TableColumn(schema);
                colvarUnitPrice.ColumnName = "unit_price";
                colvarUnitPrice.DataType = DbType.Currency;
                colvarUnitPrice.MaxLength = 0;
                colvarUnitPrice.AutoIncrement = false;
                colvarUnitPrice.IsNullable = false;
                colvarUnitPrice.IsPrimaryKey = false;
                colvarUnitPrice.IsForeignKey = false;
                colvarUnitPrice.IsReadOnly = false;
                
                schema.Columns.Add(colvarUnitPrice);
                
                TableSchema.TableColumn colvarAmountReceived = new TableSchema.TableColumn(schema);
                colvarAmountReceived.ColumnName = "amount_received";
                colvarAmountReceived.DataType = DbType.Currency;
                colvarAmountReceived.MaxLength = 0;
                colvarAmountReceived.AutoIncrement = false;
                colvarAmountReceived.IsNullable = false;
                colvarAmountReceived.IsPrimaryKey = false;
                colvarAmountReceived.IsForeignKey = false;
                colvarAmountReceived.IsReadOnly = false;
                
                schema.Columns.Add(colvarAmountReceived);
                
                TableSchema.TableColumn colvarAmountPaid = new TableSchema.TableColumn(schema);
                colvarAmountPaid.ColumnName = "amount_paid";
                colvarAmountPaid.DataType = DbType.Currency;
                colvarAmountPaid.MaxLength = 0;
                colvarAmountPaid.AutoIncrement = false;
                colvarAmountPaid.IsNullable = false;
                colvarAmountPaid.IsPrimaryKey = false;
                colvarAmountPaid.IsForeignKey = false;
                colvarAmountPaid.IsReadOnly = false;
                
                schema.Columns.Add(colvarAmountPaid);
                
                TableSchema.TableColumn colvarIsPhysical = new TableSchema.TableColumn(schema);
                colvarIsPhysical.ColumnName = "is_physical";
                colvarIsPhysical.DataType = DbType.Boolean;
                colvarIsPhysical.MaxLength = 0;
                colvarIsPhysical.AutoIncrement = false;
                colvarIsPhysical.IsNullable = false;
                colvarIsPhysical.IsPrimaryKey = false;
                colvarIsPhysical.IsForeignKey = false;
                colvarIsPhysical.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsPhysical);
                
                TableSchema.TableColumn colvarIsExported = new TableSchema.TableColumn(schema);
                colvarIsExported.ColumnName = "is_exported";
                colvarIsExported.DataType = DbType.Boolean;
                colvarIsExported.MaxLength = 0;
                colvarIsExported.AutoIncrement = false;
                colvarIsExported.IsNullable = false;
                colvarIsExported.IsPrimaryKey = false;
                colvarIsExported.IsForeignKey = false;
                colvarIsExported.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsExported);
                
                TableSchema.TableColumn colvarReceiptCode = new TableSchema.TableColumn(schema);
                colvarReceiptCode.ColumnName = "receipt_code";
                colvarReceiptCode.DataType = DbType.AnsiString;
                colvarReceiptCode.MaxLength = 20;
                colvarReceiptCode.AutoIncrement = false;
                colvarReceiptCode.IsNullable = true;
                colvarReceiptCode.IsPrimaryKey = false;
                colvarReceiptCode.IsForeignKey = false;
                colvarReceiptCode.IsReadOnly = false;
                
                schema.Columns.Add(colvarReceiptCode);
                
                TableSchema.TableColumn colvarCouponId = new TableSchema.TableColumn(schema);
                colvarCouponId.ColumnName = "coupon_id";
                colvarCouponId.DataType = DbType.Int32;
                colvarCouponId.MaxLength = 0;
                colvarCouponId.AutoIncrement = false;
                colvarCouponId.IsNullable = true;
                colvarCouponId.IsPrimaryKey = false;
                colvarCouponId.IsForeignKey = false;
                colvarCouponId.IsReadOnly = false;
                
                schema.Columns.Add(colvarCouponId);
                
                TableSchema.TableColumn colvarOrderTime = new TableSchema.TableColumn(schema);
                colvarOrderTime.ColumnName = "order_time";
                colvarOrderTime.DataType = DbType.DateTime;
                colvarOrderTime.MaxLength = 0;
                colvarOrderTime.AutoIncrement = false;
                colvarOrderTime.IsNullable = false;
                colvarOrderTime.IsPrimaryKey = false;
                colvarOrderTime.IsForeignKey = false;
                colvarOrderTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderTime);
                
                TableSchema.TableColumn colvarReceiptId = new TableSchema.TableColumn(schema);
                colvarReceiptId.ColumnName = "receipt_id";
                colvarReceiptId.DataType = DbType.Int32;
                colvarReceiptId.MaxLength = 0;
                colvarReceiptId.AutoIncrement = false;
                colvarReceiptId.IsNullable = false;
                colvarReceiptId.IsPrimaryKey = false;
                colvarReceiptId.IsForeignKey = false;
                colvarReceiptId.IsReadOnly = false;
                
                schema.Columns.Add(colvarReceiptId);
                
                TableSchema.TableColumn colvarProductUid = new TableSchema.TableColumn(schema);
                colvarProductUid.ColumnName = "product_uid";
                colvarProductUid.DataType = DbType.Int32;
                colvarProductUid.MaxLength = 0;
                colvarProductUid.AutoIncrement = false;
                colvarProductUid.IsNullable = false;
                colvarProductUid.IsPrimaryKey = false;
                colvarProductUid.IsForeignKey = false;
                colvarProductUid.IsReadOnly = false;
                
                schema.Columns.Add(colvarProductUid);
                
                TableSchema.TableColumn colvarMemberUid = new TableSchema.TableColumn(schema);
                colvarMemberUid.ColumnName = "member_uid";
                colvarMemberUid.DataType = DbType.Int32;
                colvarMemberUid.MaxLength = 0;
                colvarMemberUid.AutoIncrement = false;
                colvarMemberUid.IsNullable = false;
                colvarMemberUid.IsPrimaryKey = false;
                colvarMemberUid.IsForeignKey = false;
                colvarMemberUid.IsReadOnly = false;
                
                schema.Columns.Add(colvarMemberUid);
                
                TableSchema.TableColumn colvarMemberFirstName = new TableSchema.TableColumn(schema);
                colvarMemberFirstName.ColumnName = "member_first_name";
                colvarMemberFirstName.DataType = DbType.String;
                colvarMemberFirstName.MaxLength = 50;
                colvarMemberFirstName.AutoIncrement = false;
                colvarMemberFirstName.IsNullable = true;
                colvarMemberFirstName.IsPrimaryKey = false;
                colvarMemberFirstName.IsForeignKey = false;
                colvarMemberFirstName.IsReadOnly = false;
                
                schema.Columns.Add(colvarMemberFirstName);
                
                TableSchema.TableColumn colvarMemberLastName = new TableSchema.TableColumn(schema);
                colvarMemberLastName.ColumnName = "member_last_name";
                colvarMemberLastName.DataType = DbType.String;
                colvarMemberLastName.MaxLength = 50;
                colvarMemberLastName.AutoIncrement = false;
                colvarMemberLastName.IsNullable = true;
                colvarMemberLastName.IsPrimaryKey = false;
                colvarMemberLastName.IsForeignKey = false;
                colvarMemberLastName.IsReadOnly = false;
                
                schema.Columns.Add(colvarMemberLastName);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_entrust_sell_receipt",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewEntrustSellReceipt()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewEntrustSellReceipt(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewEntrustSellReceipt(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewEntrustSellReceipt(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("VerifiedTime")]
        [Bindable(true)]
        public DateTime? VerifiedTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("verified_time");
		    }
            set 
		    {
			    SetColumnValue("verified_time", value);
            }
        }
	      
        [XmlAttribute("OrderGuid")]
        [Bindable(true)]
        public Guid OrderGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("order_guid");
		    }
            set 
		    {
			    SetColumnValue("order_guid", value);
            }
        }
	      
        [XmlAttribute("OrderId")]
        [Bindable(true)]
        public string OrderId 
	    {
		    get
		    {
			    return GetColumnValue<string>("order_id");
		    }
            set 
		    {
			    SetColumnValue("order_id", value);
            }
        }
	      
        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id 
	    {
		    get
		    {
			    return GetColumnValue<int>("id");
		    }
            set 
		    {
			    SetColumnValue("id", value);
            }
        }
	      
        [XmlAttribute("OriginalOrderId")]
        [Bindable(true)]
        public string OriginalOrderId 
	    {
		    get
		    {
			    return GetColumnValue<string>("original_order_id");
		    }
            set 
		    {
			    SetColumnValue("original_order_id", value);
            }
        }
	      
        [XmlAttribute("ProductQc")]
        [Bindable(true)]
        public Guid ProductQc 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("product_qc");
		    }
            set 
		    {
			    SetColumnValue("product_qc", value);
            }
        }
	      
        [XmlAttribute("BuyerName")]
        [Bindable(true)]
        public string BuyerName 
	    {
		    get
		    {
			    return GetColumnValue<string>("buyer_name");
		    }
            set 
		    {
			    SetColumnValue("buyer_name", value);
            }
        }
	      
        [XmlAttribute("MemberId")]
        [Bindable(true)]
        public string MemberId 
	    {
		    get
		    {
			    return GetColumnValue<string>("member_id");
		    }
            set 
		    {
			    SetColumnValue("member_id", value);
            }
        }
	      
        [XmlAttribute("MemberName")]
        [Bindable(true)]
        public string MemberName 
	    {
		    get
		    {
			    return GetColumnValue<string>("member_name");
		    }
            set 
		    {
			    SetColumnValue("member_name", value);
            }
        }
	      
        [XmlAttribute("SellerName")]
        [Bindable(true)]
        public string SellerName 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_name");
		    }
            set 
		    {
			    SetColumnValue("seller_name", value);
            }
        }
	      
        [XmlAttribute("ItemName")]
        [Bindable(true)]
        public string ItemName 
	    {
		    get
		    {
			    return GetColumnValue<string>("item_name");
		    }
            set 
		    {
			    SetColumnValue("item_name", value);
            }
        }
	      
        [XmlAttribute("ComId")]
        [Bindable(true)]
        public string ComId 
	    {
		    get
		    {
			    return GetColumnValue<string>("com_id");
		    }
            set 
		    {
			    SetColumnValue("com_id", value);
            }
        }
	      
        [XmlAttribute("BuyerAddress")]
        [Bindable(true)]
        public string BuyerAddress 
	    {
		    get
		    {
			    return GetColumnValue<string>("buyer_address");
		    }
            set 
		    {
			    SetColumnValue("buyer_address", value);
            }
        }
	      
        [XmlAttribute("AccountCode")]
        [Bindable(true)]
        public string AccountCode 
	    {
		    get
		    {
			    return GetColumnValue<string>("account_code");
		    }
            set 
		    {
			    SetColumnValue("account_code", value);
            }
        }
	      
        [XmlAttribute("ItemQuantity")]
        [Bindable(true)]
        public int ItemQuantity 
	    {
		    get
		    {
			    return GetColumnValue<int>("item_quantity");
		    }
            set 
		    {
			    SetColumnValue("item_quantity", value);
            }
        }
	      
        [XmlAttribute("UnitPrice")]
        [Bindable(true)]
        public decimal UnitPrice 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("unit_price");
		    }
            set 
		    {
			    SetColumnValue("unit_price", value);
            }
        }
	      
        [XmlAttribute("AmountReceived")]
        [Bindable(true)]
        public decimal AmountReceived 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("amount_received");
		    }
            set 
		    {
			    SetColumnValue("amount_received", value);
            }
        }
	      
        [XmlAttribute("AmountPaid")]
        [Bindable(true)]
        public decimal AmountPaid 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("amount_paid");
		    }
            set 
		    {
			    SetColumnValue("amount_paid", value);
            }
        }
	      
        [XmlAttribute("IsPhysical")]
        [Bindable(true)]
        public bool IsPhysical 
	    {
		    get
		    {
			    return GetColumnValue<bool>("is_physical");
		    }
            set 
		    {
			    SetColumnValue("is_physical", value);
            }
        }
	      
        [XmlAttribute("IsExported")]
        [Bindable(true)]
        public bool IsExported 
	    {
		    get
		    {
			    return GetColumnValue<bool>("is_exported");
		    }
            set 
		    {
			    SetColumnValue("is_exported", value);
            }
        }
	      
        [XmlAttribute("ReceiptCode")]
        [Bindable(true)]
        public string ReceiptCode 
	    {
		    get
		    {
			    return GetColumnValue<string>("receipt_code");
		    }
            set 
		    {
			    SetColumnValue("receipt_code", value);
            }
        }
	      
        [XmlAttribute("CouponId")]
        [Bindable(true)]
        public int? CouponId 
	    {
		    get
		    {
			    return GetColumnValue<int?>("coupon_id");
		    }
            set 
		    {
			    SetColumnValue("coupon_id", value);
            }
        }
	      
        [XmlAttribute("OrderTime")]
        [Bindable(true)]
        public DateTime OrderTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("order_time");
		    }
            set 
		    {
			    SetColumnValue("order_time", value);
            }
        }
	      
        [XmlAttribute("ReceiptId")]
        [Bindable(true)]
        public int ReceiptId 
	    {
		    get
		    {
			    return GetColumnValue<int>("receipt_id");
		    }
            set 
		    {
			    SetColumnValue("receipt_id", value);
            }
        }
	      
        [XmlAttribute("ProductUid")]
        [Bindable(true)]
        public int ProductUid 
	    {
		    get
		    {
			    return GetColumnValue<int>("product_uid");
		    }
            set 
		    {
			    SetColumnValue("product_uid", value);
            }
        }
	      
        [XmlAttribute("MemberUid")]
        [Bindable(true)]
        public int MemberUid 
	    {
		    get
		    {
			    return GetColumnValue<int>("member_uid");
		    }
            set 
		    {
			    SetColumnValue("member_uid", value);
            }
        }
	      
        [XmlAttribute("MemberFirstName")]
        [Bindable(true)]
        public string MemberFirstName 
	    {
		    get
		    {
			    return GetColumnValue<string>("member_first_name");
		    }
            set 
		    {
			    SetColumnValue("member_first_name", value);
            }
        }
	      
        [XmlAttribute("MemberLastName")]
        [Bindable(true)]
        public string MemberLastName 
	    {
		    get
		    {
			    return GetColumnValue<string>("member_last_name");
		    }
            set 
		    {
			    SetColumnValue("member_last_name", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string VerifiedTime = @"verified_time";
            
            public static string OrderGuid = @"order_guid";
            
            public static string OrderId = @"order_id";
            
            public static string Id = @"id";
            
            public static string OriginalOrderId = @"original_order_id";
            
            public static string ProductQc = @"product_qc";
            
            public static string BuyerName = @"buyer_name";
            
            public static string MemberId = @"member_id";
            
            public static string MemberName = @"member_name";
            
            public static string SellerName = @"seller_name";
            
            public static string ItemName = @"item_name";
            
            public static string ComId = @"com_id";
            
            public static string BuyerAddress = @"buyer_address";
            
            public static string AccountCode = @"account_code";
            
            public static string ItemQuantity = @"item_quantity";
            
            public static string UnitPrice = @"unit_price";
            
            public static string AmountReceived = @"amount_received";
            
            public static string AmountPaid = @"amount_paid";
            
            public static string IsPhysical = @"is_physical";
            
            public static string IsExported = @"is_exported";
            
            public static string ReceiptCode = @"receipt_code";
            
            public static string CouponId = @"coupon_id";
            
            public static string OrderTime = @"order_time";
            
            public static string ReceiptId = @"receipt_id";
            
            public static string ProductUid = @"product_uid";
            
            public static string MemberUid = @"member_uid";
            
            public static string MemberFirstName = @"member_first_name";
            
            public static string MemberLastName = @"member_last_name";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

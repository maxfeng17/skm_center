using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the ShoppingGuideCategoryTemp class.
	/// </summary>
    [Serializable]
	public partial class ShoppingGuideCategoryTempCollection : RepositoryList<ShoppingGuideCategoryTemp, ShoppingGuideCategoryTempCollection>
	{	   
		public ShoppingGuideCategoryTempCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>ShoppingGuideCategoryTempCollection</returns>
		public ShoppingGuideCategoryTempCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                ShoppingGuideCategoryTemp o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
	}
	
	
	/// <summary>
	/// This is an ActiveRecord class which wraps the shopping_guide_category_temp table.
	/// </summary>
	
	[Serializable]
	public partial class ShoppingGuideCategoryTemp : RepositoryRecord<ShoppingGuideCategoryTemp>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public ShoppingGuideCategoryTemp()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public ShoppingGuideCategoryTemp(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("shopping_guide_category_temp", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarBid = new TableSchema.TableColumn(schema);
				colvarBid.ColumnName = "bid";
				colvarBid.DataType = DbType.Guid;
				colvarBid.MaxLength = 0;
				colvarBid.AutoIncrement = false;
				colvarBid.IsNullable = false;
				colvarBid.IsPrimaryKey = false;
				colvarBid.IsForeignKey = false;
				colvarBid.IsReadOnly = false;
				colvarBid.DefaultSetting = @"";
				colvarBid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBid);
				
				TableSchema.TableColumn colvarDealCategoryJson = new TableSchema.TableColumn(schema);
				colvarDealCategoryJson.ColumnName = "deal_category_json";
				colvarDealCategoryJson.DataType = DbType.String;
				colvarDealCategoryJson.MaxLength = -1;
				colvarDealCategoryJson.AutoIncrement = false;
				colvarDealCategoryJson.IsNullable = false;
				colvarDealCategoryJson.IsPrimaryKey = false;
				colvarDealCategoryJson.IsForeignKey = false;
				colvarDealCategoryJson.IsReadOnly = false;
				colvarDealCategoryJson.DefaultSetting = @"";
				colvarDealCategoryJson.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDealCategoryJson);
				
				TableSchema.TableColumn colvarBatchId = new TableSchema.TableColumn(schema);
				colvarBatchId.ColumnName = "batch_id";
				colvarBatchId.DataType = DbType.AnsiString;
				colvarBatchId.MaxLength = 36;
				colvarBatchId.AutoIncrement = false;
				colvarBatchId.IsNullable = false;
				colvarBatchId.IsPrimaryKey = false;
				colvarBatchId.IsForeignKey = false;
				colvarBatchId.IsReadOnly = false;
				colvarBatchId.DefaultSetting = @"";
				colvarBatchId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBatchId);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("shopping_guide_category_temp",schema);
			}
		}
		#endregion
		
		#region Props
		
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		
		[XmlAttribute("Bid")]
		[Bindable(true)]
		public Guid Bid 
		{
			get { return GetColumnValue<Guid>(Columns.Bid); }
			set { SetColumnValue(Columns.Bid, value); }
		}
		
		[XmlAttribute("DealCategoryJson")]
		[Bindable(true)]
		public string DealCategoryJson 
		{
			get { return GetColumnValue<string>(Columns.DealCategoryJson); }
			set { SetColumnValue(Columns.DealCategoryJson, value); }
		}
		
		[XmlAttribute("BatchId")]
		[Bindable(true)]
		public string BatchId 
		{
			get { return GetColumnValue<string>(Columns.BatchId); }
			set { SetColumnValue(Columns.BatchId, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn BidColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn DealCategoryJsonColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn BatchIdColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string Bid = @"bid";
			 public static string DealCategoryJson = @"deal_category_json";
			 public static string BatchId = @"batch_id";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

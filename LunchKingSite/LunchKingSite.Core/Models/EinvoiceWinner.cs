using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the EinvoiceWinner class.
    /// </summary>
    [Serializable]
    public partial class EinvoiceWinnerCollection : RepositoryList<EinvoiceWinner, EinvoiceWinnerCollection>
    {
        public EinvoiceWinnerCollection() { }

        /// <summary>
        /// Filters an existing collection based on the set criteria. This is an in-memory filter
        /// Thanks to developingchris for this!
        /// </summary>
        /// <returns>EinvoiceWinnerCollection</returns>
        public EinvoiceWinnerCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                EinvoiceWinner o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }


    }
    /// <summary>
    /// This is an ActiveRecord class which wraps the einvoice_winner table.
    /// </summary>
    [Serializable]
    public partial class EinvoiceWinner : RepositoryRecord<EinvoiceWinner>, IRecordBase
    {
        #region .ctors and Default Settings

        public EinvoiceWinner()
        {
            SetSQLProps();
            InitSetDefaults();
            MarkNew();
        }

        private void InitSetDefaults() { SetDefaults(); }

        public EinvoiceWinner(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
                ForceDefaults();
            MarkNew();
        }

        protected static void SetSQLProps() { GetTableSchema(); }

        #endregion

        #region Schema and Query Accessor	
        public static Query CreateQuery() { return new Query(Schema); }
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                    SetSQLProps();
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("einvoice_winner", TableType.Table, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = true;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = true;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                colvarId.DefaultSetting = @"";
                colvarId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarId);

                TableSchema.TableColumn colvarUserId = new TableSchema.TableColumn(schema);
                colvarUserId.ColumnName = "user_id";
                colvarUserId.DataType = DbType.Int32;
                colvarUserId.MaxLength = 0;
                colvarUserId.AutoIncrement = false;
                colvarUserId.IsNullable = false;
                colvarUserId.IsPrimaryKey = false;
                colvarUserId.IsForeignKey = false;
                colvarUserId.IsReadOnly = false;
                colvarUserId.DefaultSetting = @"";
                colvarUserId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarUserId);

                TableSchema.TableColumn colvarUserName = new TableSchema.TableColumn(schema);
                colvarUserName.ColumnName = "user_name";
                colvarUserName.DataType = DbType.String;
                colvarUserName.MaxLength = 50;
                colvarUserName.AutoIncrement = false;
                colvarUserName.IsNullable = false;
                colvarUserName.IsPrimaryKey = false;
                colvarUserName.IsForeignKey = false;
                colvarUserName.IsReadOnly = false;
                colvarUserName.DefaultSetting = @"";
                colvarUserName.ForeignKeyTableName = "";
                schema.Columns.Add(colvarUserName);

                TableSchema.TableColumn colvarMobile = new TableSchema.TableColumn(schema);
                colvarMobile.ColumnName = "mobile";
                colvarMobile.DataType = DbType.AnsiString;
                colvarMobile.MaxLength = 50;
                colvarMobile.AutoIncrement = false;
                colvarMobile.IsNullable = false;
                colvarMobile.IsPrimaryKey = false;
                colvarMobile.IsForeignKey = false;
                colvarMobile.IsReadOnly = false;
                colvarMobile.DefaultSetting = @"";
                colvarMobile.ForeignKeyTableName = "";
                schema.Columns.Add(colvarMobile);

                TableSchema.TableColumn colvarAdress = new TableSchema.TableColumn(schema);
                colvarAdress.ColumnName = "adress";
                colvarAdress.DataType = DbType.String;
                colvarAdress.MaxLength = 200;
                colvarAdress.AutoIncrement = false;
                colvarAdress.IsNullable = false;
                colvarAdress.IsPrimaryKey = false;
                colvarAdress.IsForeignKey = false;
                colvarAdress.IsReadOnly = false;
                colvarAdress.DefaultSetting = @"";
                colvarAdress.ForeignKeyTableName = "";
                schema.Columns.Add(colvarAdress);

                TableSchema.TableColumn colvarModificationTime = new TableSchema.TableColumn(schema);
                colvarModificationTime.ColumnName = "modification_time";
                colvarModificationTime.DataType = DbType.DateTime;
                colvarModificationTime.MaxLength = 0;
                colvarModificationTime.AutoIncrement = false;
                colvarModificationTime.IsNullable = false;
                colvarModificationTime.IsPrimaryKey = false;
                colvarModificationTime.IsForeignKey = false;
                colvarModificationTime.IsReadOnly = false;
                colvarModificationTime.DefaultSetting = @"";
                colvarModificationTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarModificationTime);

                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("einvoice_winner", schema);
            }
        }
        #endregion

        #region Props

        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id
        {
            get { return GetColumnValue<int>(Columns.Id); }
            set { SetColumnValue(Columns.Id, value); }
        }

        [XmlAttribute("UserId")]
        [Bindable(true)]
        public int UserId
        {
            get { return GetColumnValue<int>(Columns.UserId); }
            set { SetColumnValue(Columns.UserId, value); }
        }

        [XmlAttribute("UserName")]
        [Bindable(true)]
        public string UserName
        {
            get { return GetColumnValue<string>(Columns.UserName); }
            set { SetColumnValue(Columns.UserName, value); }
        }

        [XmlAttribute("Mobile")]
        [Bindable(true)]
        public string Mobile
        {
            get { return GetColumnValue<string>(Columns.Mobile); }
            set { SetColumnValue(Columns.Mobile, value); }
        }

        [XmlAttribute("Adress")]
        [Bindable(true)]
        public string Adress
        {
            get { return GetColumnValue<string>(Columns.Adress); }
            set { SetColumnValue(Columns.Adress, value); }
        }

        [XmlAttribute("ModificationTime")]
        [Bindable(true)]
        public DateTime ModificationTime
        {
            get { return GetColumnValue<DateTime>(Columns.ModificationTime); }
            set { SetColumnValue(Columns.ModificationTime, value); }
        }

        #endregion




        //no foreign key tables defined (0)



        //no ManyToMany tables defined (0)





        #region Typed Columns


        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }



        public static TableSchema.TableColumn UserIdColumn
        {
            get { return Schema.Columns[1]; }
        }



        public static TableSchema.TableColumn UserNameColumn
        {
            get { return Schema.Columns[2]; }
        }



        public static TableSchema.TableColumn MobileColumn
        {
            get { return Schema.Columns[3]; }
        }



        public static TableSchema.TableColumn AdressColumn
        {
            get { return Schema.Columns[4]; }
        }



        public static TableSchema.TableColumn ModificationTimeColumn
        {
            get { return Schema.Columns[5]; }
        }



        #endregion
        #region Columns Struct
        public struct Columns
        {
            public static string Id = @"id";
            public static string UserId = @"user_id";
            public static string UserName = @"user_name";
            public static string Mobile = @"mobile";
            public static string Adress = @"adress";
            public static string ModificationTime = @"modification_time";

        }
        #endregion

        #region Update PK Collections

        #endregion

        #region Deep Save

        #endregion
    }
}

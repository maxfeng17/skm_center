﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LunchKingSite.Core.Enumeration;

namespace LunchKingSite.Core.Models.GameEntities
{
    /// <summary>
    /// 預設遊戲規則
    /// 產生game時，會複寫資料到game的資料裏
    /// 上架者可在game的資料做調整
    /// </summary>
	[Table("game_rule")]
	public class GameRule
	{
		public GameRule()
		{
			Title = string.Empty;
			CreateId = string.Empty;
		}

        [Key]
        [Column("id")]
        public int Id { get; set; }

		[Column("title")]
		public string Title { get; set; }

        [Column("type")]
        public int Type { get; set; }

        [Column("level")]
        public GameLevel Level { get; set; }

		[Column("hours")]
		public int Hours { get; set; }

		[Column("people")]
		public int People { get; set; }

		[Column("create_id")]
		public string CreateId { get; set; }

		[Column("create_time")]
		public DateTime CreateTime { get; set; }

		[Column("modify_id")]
		public string ModifyId { get; set; }

		[Column("modify_time")]
		public DateTime? ModifyTime { get; set; }
        
    }
}

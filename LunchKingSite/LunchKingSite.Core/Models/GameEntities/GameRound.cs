﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using LunchKingSite.Core.Enumeration;

namespace LunchKingSite.Core.Models.GameEntities
{
    /// <summary>
    /// 一場遊戲
    /// </summary>
    //public class GameRound
    //{
    //    public Guid Id { get; set; }
    //    public int OwnerId { get; set; }
    //    public Guid ActivityId { get; set; }
    //    public Guid GameId { get; set; }
    //    public DateTime CreateTime { get; set; }
    //    public int Status { get; set; }
    //}

    /// <summary>
    /// 一場遊戲
    /// </summary>
	[Table("game_round")]
	public class GameRound
	{
		public GameRound()
		{
		}

        [Key]
        [Column("id")]
        public int Id { get; set; }

        [Column("guid")]
        public Guid Guid { get; set; }

		[Column("owner_id")]
		public int OwnerId { get; set; }

		[Column("game_id")]
		public int GameId { get; set; }

		[Column("activity_id")]
		public int ActivityId { get; set; }

        [Column("expired_time")]
		public DateTime ExpiredTime { get; set; }

		[Column("status")]
		public GameRoundStatus Status { get; set; }

        [Column("completed_time")]
        public DateTime? CompletedTime { get; set; }

		[Column("create_id")]
		public string CreateId { get; set; }

		[Column("create_time")]
		public DateTime CreateTime { get; set; }

		[Column("modify_id")]
		public string ModifyId { get; set; }

		[Column("modify_time")]
		public DateTime? ModifyTime { get; set; }

	}
}

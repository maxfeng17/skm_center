﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using LunchKingSite.Core.Enumeration;

namespace LunchKingSite.Core.Models.GameEntities
{
    /// <summary>
    /// 可選擇遊戲的難易度
    /// 對映到子檔
    /// </summary>
    [Table("game_game")]
    public class GameGame
    {
        public GameGame()
        {
            Title = string.Empty;
            CreateId = string.Empty;
        }

        [Key]
        [Column("id")]
        public int Id { get; set; }

        [Column("guid")]
        public Guid Guid { get; set; }

		[Column("activity_id")]
		public int ActivityId { get; set; }

		[Column("title")]
		public string Title { get; set; }

        [Column("original_price")]
		public int OriginalPrice { get; set; }

        [Column("price")]
		public int Price { get; set; }

        /// <summary>
        /// 對映到遊戲獎品的子檔
        /// </summary>
		[Column("reference_id")]
		public Guid ReferenceId { get; set; }

        /// <summary>
        /// 可完成遊戲的最大數
        /// </summary>
		[Column("maximum")]
		public int Maximum { get; set; }
        /// <summary>
        /// 完成人數以到達最大量
        /// </summary>
        [Column("run_out")]
        public bool RunOut { get; set; }

        [Column("rule_title")]
		public string RuleTitle { get; set; }

        [Column("rule_level")]
		public GameLevel RuleLevel { get; set; }

        [Column("rule_type")]
		public int RuleType { get; set; }

        [Column("rule_people")]
		public int RulePeople { get; set; }

        [Column("rule_hours")]
		public int RuleHours { get; set; }

		[Column("create_id")]
		public string CreateId { get; set; }

		[Column("create_time")]
		public DateTime CreateTime { get; set; }

		[Column("modify_id")]
		public string ModifyId { get; set; }

		[Column("modify_time")]
		public DateTime? ModifyTime { get; set; }

        public override string ToString()
        {
            return Title;
        }
    }  
}

﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LunchKingSite.Core.Models.GameEntities
{
    [Table("game_deal_order")]
	public class GameDealOrder
	{
		public GameDealOrder()
		{
		}

		[Key]
		[Column("id")]
		public int Id { get; set; }

		[Column("order_guid")]
		public Guid OrderGuid { get; set; }

		[Column("activity_id")]
		public int ActivityId { get; set; }

        [Column("game_id")]
		public int GameId { get; set; }

		[Column("permission_id")]
		public int? PermissionId { get; set; }

		[Column("user_id")]
		public int UserId { get; set; }

		[Column("create_time")]
		public DateTime CreateTime { get; set; }
	}
}
﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LunchKingSite.Core.Models.GameEntities
{
    /// <summary>
    /// 遊戲場景
    /// 一個遊戲主題裏，有多個遊戲場景
    /// 對映到母檔
    /// </summary>
	[Table("game_activity")]
    public class GameActivity
    {
        public GameActivity()
        {
            Title = string.Empty;
            SubTitle = string.Empty;
            Image = string.Empty;
            Image2 = string.Empty;
        }

        [Key]
        [Column("id")]
        public int Id { get; set; }

        [Column("guid")]
        public Guid Guid { get; set; }

        [Column("campaign_id")]
        public int CampaignId { get; set; }

        /// 對映到遊戲獎品的母檔
		[Column("reference_id")]
        public Guid ReferenceId { get; set; }

        [Column("title")]
        public string Title { get; set; }

        [Column("sub_title")]
        public string SubTitle { get; set; }

        [Column("image")]
        public string Image { get; set; }

        [Column("image2")]
        public string Image2 { get; set; }

        [Column("original_price")]
        public int OriginalPrice { get; set; }
        /// <summary>
        /// 顯示在畫面上，但不能建立遊戲，手動停止
        /// </summary>
		[Column("closed")]
        public bool Closed { get; set; }
        /// <summary>
        /// 完成人數以到達最大量
        /// </summary>
        [Column("run_out")]
        public bool RunOut { get; set; }

        [Column("start_time")]
        public DateTime StartTime { get; set; }

        [Column("end_time")]
        public DateTime EndTime { get; set; }

        [Column("seqence")]
        public int Seqence { get; set; }

		[Column("create_id")]
		public string CreateId { get; set; }

		[Column("create_time")]
		public DateTime CreateTime { get; set; }

		[Column("modify_id")]
		public string ModifyId { get; set; }

		[Column("modify_time")]
		public DateTime? ModifyTime { get; set; }

        public override string ToString()
        {
            return Title;
        }
    }
}

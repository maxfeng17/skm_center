﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace LunchKingSite.Core.Models.GameEntities
{
	[Table("game_buddy")]
	public class GameBuddy
	{
		public GameBuddy()
		{
			ExternalId = string.Empty;
			Name = string.Empty;
		}

		[Key]
		[Column("id")]
		public int Id { get; set; }

		[Column("round_id")]
		public int RoundId { get; set; }

		[Column("source")]
		public int Source { get; set; }

		[Column("external_id")]
		public string ExternalId { get; set; }

        [JsonIgnore]
		[Column("reward_id")]
		public int? RewardId { get; set; }

		[Column("name")]
		public string Name { get; set; }

		[Column("pic")]
		public string Pic { get; set; }

		[Column("message")]
		public string Message { get; set; }

		[Column("create_time")]
		public DateTime CreateTime { get; set; }
	}
}

﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LunchKingSite.Core.Models.GameEntities
{
    [Table("game_deal_permission")]
	public class GameDealPermission
	{
		public GameDealPermission()
		{
		}

		[Key]
		[Column("id")]
		public int Id { get; set; }

		[Column("deal_id")]
		public Guid DealId { get; set; }

        [Column("main_deal_id")]
		public Guid MainDealId { get; set; }

		[Column("user_id")]
		public int UserId { get; set; }

		[Column("round_id")]
		public int RoundId { get; set; }

        [Column("expired_time")]
        public DateTime ExpiredTime { get; set; }

        [Column("used")]
        public bool Used { get; set; }

		[Column("create_time")]
		public DateTime CreateTime { get; set; }

        [Column("modify_time")]
		public DateTime? ModifyTime { get; set; }
	}
}
﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LunchKingSite.Core.Models.GameEntities
{
    /// <summary>
    /// 遊戲主題，譬如 1111活動
    /// </summary>
    [Table("game_campaign")]
	public class GameCampaign
	{
		public GameCampaign()
		{
			Title = string.Empty;
		}

        [Key]
        [Column("id")]
        public int Id { get; set; }

        [Column("guid")]
        public Guid Guid { get; set; }

        [Column("name")]
		public string Name { get; set; }

		[Column("title")]
		public string Title { get; set; }

		[Column("image")]
		public string Image { get; set; }

		[Column("start_time")]
		public DateTime StartTime { get; set; }

		[Column("end_time")]
		public DateTime EndTime { get; set; }

        [Column("preview_code")]
        public string PreviewCode { get; set; }

        [Column("enable_buddy_reward")]
        public bool EnableBuddyReward { get; set; }

        [Column("bonus_total")]
        public int BonusTotal { get; set; }

        [Column("create_id")]
		public string CreateId { get; set; }

		[Column("create_time")]
		public DateTime CreateTime { get; set; }

		[Column("modify_id")]
		public string ModifyId { get; set; }

		[Column("modify_time")]
		public DateTime? ModifyTime { get; set; }

        public override string ToString()
        {
            return Title;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LunchKingSite.Core.Enumeration;

namespace LunchKingSite.Core.Models.GameEntities
{
    public class TopGameView
    {
        public Guid Guid { get; set; }
        public string Title { get; set; }
        public string Image { get; set; }
        public int Price { get; set; }
        public bool RunOut { get; set; }
        public int RuleHours { get; set; }
        public int RulePeople { get; set; }
        public GameLevel RuleLevel { get; set; }
        public override string ToString()
        {
            return string.Format("{0} {1}", RuleLevel, Title);
        }
    }

    public class GameRoundView
    {
        public int Id { get; set; }
        public Guid Guid { get; set; }
        public string Title { get; set; }
        public string SubTitle { get; set; }
        public string Image { get; set; }
        public int GameId { get; set; }
        public int ActivityId { get; set; }
        public DateTime ExpiredTime { get; set; }
        public int CountdownSecs { get
            {
                return (int)(ExpiredTime - DateTime.Now).TotalSeconds;
            }
        }
        public GameRoundStatus Status { get; set; }
        public GameRoundRewardStatus RewardStatus { get; set; }

        public override string ToString()
        {
            return string.Format("{0} - {1}", Title, Status);
        }
    }


    public class GameActivityView
    {
        public Guid Guid { get; set; }
        public string Title { get; set; }
        public string SubTitle { get; set; }
        public string Image { get; set; }
        public int HigestOriginalPrice { get; set; }
        public int LowestPrice { get; set; }
        public bool RunOut { get; set; }
        public GameLevel RuleLevel { get; set; }
        public int RulePeople { get; set; }
        public int RuleHours { get; set; }
        public override string ToString()
        {
            return string.Format("{0} {1}", RuleLevel, Title);
        }
    }
}

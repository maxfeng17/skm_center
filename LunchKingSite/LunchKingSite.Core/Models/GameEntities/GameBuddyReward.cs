﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.Core.Models.GameEntities
{
    /// <summary>
    /// 
    /// </summary>
	[Table("game_buddy_reward")]
	public class GameBuddyReward
	{
		public GameBuddyReward()
		{
		}

		[Key]
		[Column("id")]
		public int Id { get; set; }

		[Column("buddy_id")]
		public int BuddyId { get; set; }

		[Column("activity_id")]
		public int ActivityId { get; set; }

		[Column("deposit_id")]
		public int DepositId { get; set; }

		[Column("bonus_value")]
		public int BonusValue { get; set; }

		[Column("user_id")]
		public int UserId { get; set; }

		[Column("the_date")]
		public DateTime TheDate { get; set; }

		[Column("create_time")]
		public DateTime CreateTime { get; set; }
	}
}

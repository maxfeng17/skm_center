using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewVbsToHouseDeal class.
    /// </summary>
    [Serializable]
    public partial class ViewVbsToHouseDealCollection : ReadOnlyList<ViewVbsToHouseDeal, ViewVbsToHouseDealCollection>
    {        
        public ViewVbsToHouseDealCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_vbs_to_house_deal view.
    /// </summary>
    [Serializable]
    public partial class ViewVbsToHouseDeal : ReadOnlyRecord<ViewVbsToHouseDeal>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_vbs_to_house_deal", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarProductId = new TableSchema.TableColumn(schema);
                colvarProductId.ColumnName = "product_id";
                colvarProductId.DataType = DbType.Int32;
                colvarProductId.MaxLength = 0;
                colvarProductId.AutoIncrement = false;
                colvarProductId.IsNullable = false;
                colvarProductId.IsPrimaryKey = false;
                colvarProductId.IsForeignKey = false;
                colvarProductId.IsReadOnly = false;
                
                schema.Columns.Add(colvarProductId);
                
                TableSchema.TableColumn colvarProductGuid = new TableSchema.TableColumn(schema);
                colvarProductGuid.ColumnName = "product_guid";
                colvarProductGuid.DataType = DbType.Guid;
                colvarProductGuid.MaxLength = 0;
                colvarProductGuid.AutoIncrement = false;
                colvarProductGuid.IsNullable = false;
                colvarProductGuid.IsPrimaryKey = false;
                colvarProductGuid.IsForeignKey = false;
                colvarProductGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarProductGuid);
                
                TableSchema.TableColumn colvarProductName = new TableSchema.TableColumn(schema);
                colvarProductName.ColumnName = "product_name";
                colvarProductName.DataType = DbType.String;
                colvarProductName.MaxLength = 4000;
                colvarProductName.AutoIncrement = false;
                colvarProductName.IsNullable = true;
                colvarProductName.IsPrimaryKey = false;
                colvarProductName.IsForeignKey = false;
                colvarProductName.IsReadOnly = false;
                
                schema.Columns.Add(colvarProductName);
                
                TableSchema.TableColumn colvarAppTitle = new TableSchema.TableColumn(schema);
                colvarAppTitle.ColumnName = "app_title";
                colvarAppTitle.DataType = DbType.String;
                colvarAppTitle.MaxLength = 40;
                colvarAppTitle.AutoIncrement = false;
                colvarAppTitle.IsNullable = true;
                colvarAppTitle.IsPrimaryKey = false;
                colvarAppTitle.IsForeignKey = false;
                colvarAppTitle.IsReadOnly = false;
                
                schema.Columns.Add(colvarAppTitle);
                
                TableSchema.TableColumn colvarDealStartTime = new TableSchema.TableColumn(schema);
                colvarDealStartTime.ColumnName = "deal_start_time";
                colvarDealStartTime.DataType = DbType.DateTime;
                colvarDealStartTime.MaxLength = 0;
                colvarDealStartTime.AutoIncrement = false;
                colvarDealStartTime.IsNullable = true;
                colvarDealStartTime.IsPrimaryKey = false;
                colvarDealStartTime.IsForeignKey = false;
                colvarDealStartTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarDealStartTime);
                
                TableSchema.TableColumn colvarDealEndTime = new TableSchema.TableColumn(schema);
                colvarDealEndTime.ColumnName = "deal_end_time";
                colvarDealEndTime.DataType = DbType.DateTime;
                colvarDealEndTime.MaxLength = 0;
                colvarDealEndTime.AutoIncrement = false;
                colvarDealEndTime.IsNullable = true;
                colvarDealEndTime.IsPrimaryKey = false;
                colvarDealEndTime.IsForeignKey = false;
                colvarDealEndTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarDealEndTime);
                
                TableSchema.TableColumn colvarUseStartTime = new TableSchema.TableColumn(schema);
                colvarUseStartTime.ColumnName = "use_start_time";
                colvarUseStartTime.DataType = DbType.DateTime;
                colvarUseStartTime.MaxLength = 0;
                colvarUseStartTime.AutoIncrement = false;
                colvarUseStartTime.IsNullable = true;
                colvarUseStartTime.IsPrimaryKey = false;
                colvarUseStartTime.IsForeignKey = false;
                colvarUseStartTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarUseStartTime);
                
                TableSchema.TableColumn colvarUseEndTime = new TableSchema.TableColumn(schema);
                colvarUseEndTime.ColumnName = "use_end_time";
                colvarUseEndTime.DataType = DbType.DateTime;
                colvarUseEndTime.MaxLength = 0;
                colvarUseEndTime.AutoIncrement = false;
                colvarUseEndTime.IsNullable = true;
                colvarUseEndTime.IsPrimaryKey = false;
                colvarUseEndTime.IsForeignKey = false;
                colvarUseEndTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarUseEndTime);
                
                TableSchema.TableColumn colvarVendorBillingModel = new TableSchema.TableColumn(schema);
                colvarVendorBillingModel.ColumnName = "vendor_billing_model";
                colvarVendorBillingModel.DataType = DbType.Int32;
                colvarVendorBillingModel.MaxLength = 0;
                colvarVendorBillingModel.AutoIncrement = false;
                colvarVendorBillingModel.IsNullable = false;
                colvarVendorBillingModel.IsPrimaryKey = false;
                colvarVendorBillingModel.IsForeignKey = false;
                colvarVendorBillingModel.IsReadOnly = false;
                
                schema.Columns.Add(colvarVendorBillingModel);
                
                TableSchema.TableColumn colvarSellerId = new TableSchema.TableColumn(schema);
                colvarSellerId.ColumnName = "seller_id";
                colvarSellerId.DataType = DbType.AnsiString;
                colvarSellerId.MaxLength = 20;
                colvarSellerId.AutoIncrement = false;
                colvarSellerId.IsNullable = true;
                colvarSellerId.IsPrimaryKey = false;
                colvarSellerId.IsForeignKey = false;
                colvarSellerId.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerId);
                
                TableSchema.TableColumn colvarSellerName = new TableSchema.TableColumn(schema);
                colvarSellerName.ColumnName = "seller_name";
                colvarSellerName.DataType = DbType.String;
                colvarSellerName.MaxLength = 100;
                colvarSellerName.AutoIncrement = false;
                colvarSellerName.IsNullable = false;
                colvarSellerName.IsPrimaryKey = false;
                colvarSellerName.IsForeignKey = false;
                colvarSellerName.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerName);
                
                TableSchema.TableColumn colvarShippedDate = new TableSchema.TableColumn(schema);
                colvarShippedDate.ColumnName = "shipped_date";
                colvarShippedDate.DataType = DbType.DateTime;
                colvarShippedDate.MaxLength = 0;
                colvarShippedDate.AutoIncrement = false;
                colvarShippedDate.IsNullable = true;
                colvarShippedDate.IsPrimaryKey = false;
                colvarShippedDate.IsForeignKey = false;
                colvarShippedDate.IsReadOnly = false;
                
                schema.Columns.Add(colvarShippedDate);
                
                TableSchema.TableColumn colvarDealEstablished = new TableSchema.TableColumn(schema);
                colvarDealEstablished.ColumnName = "deal_established";
                colvarDealEstablished.DataType = DbType.Int32;
                colvarDealEstablished.MaxLength = 0;
                colvarDealEstablished.AutoIncrement = false;
                colvarDealEstablished.IsNullable = true;
                colvarDealEstablished.IsPrimaryKey = false;
                colvarDealEstablished.IsForeignKey = false;
                colvarDealEstablished.IsReadOnly = false;
                
                schema.Columns.Add(colvarDealEstablished);
                
                TableSchema.TableColumn colvarDealId = new TableSchema.TableColumn(schema);
                colvarDealId.ColumnName = "deal_id";
                colvarDealId.DataType = DbType.Int32;
                colvarDealId.MaxLength = 0;
                colvarDealId.AutoIncrement = false;
                colvarDealId.IsNullable = true;
                colvarDealId.IsPrimaryKey = false;
                colvarDealId.IsForeignKey = false;
                colvarDealId.IsReadOnly = false;
                
                schema.Columns.Add(colvarDealId);
                
                TableSchema.TableColumn colvarDealType = new TableSchema.TableColumn(schema);
                colvarDealType.ColumnName = "deal_type";
                colvarDealType.DataType = DbType.Int32;
                colvarDealType.MaxLength = 0;
                colvarDealType.AutoIncrement = false;
                colvarDealType.IsNullable = false;
                colvarDealType.IsPrimaryKey = false;
                colvarDealType.IsForeignKey = false;
                colvarDealType.IsReadOnly = false;
                
                schema.Columns.Add(colvarDealType);
                
                TableSchema.TableColumn colvarComboPackCount = new TableSchema.TableColumn(schema);
                colvarComboPackCount.ColumnName = "combo_pack_count";
                colvarComboPackCount.DataType = DbType.Int32;
                colvarComboPackCount.MaxLength = 0;
                colvarComboPackCount.AutoIncrement = false;
                colvarComboPackCount.IsNullable = true;
                colvarComboPackCount.IsPrimaryKey = false;
                colvarComboPackCount.IsForeignKey = false;
                colvarComboPackCount.IsReadOnly = false;
                
                schema.Columns.Add(colvarComboPackCount);
                
                TableSchema.TableColumn colvarLabelIconList = new TableSchema.TableColumn(schema);
                colvarLabelIconList.ColumnName = "label_icon_list";
                colvarLabelIconList.DataType = DbType.AnsiString;
                colvarLabelIconList.MaxLength = 255;
                colvarLabelIconList.AutoIncrement = false;
                colvarLabelIconList.IsNullable = true;
                colvarLabelIconList.IsPrimaryKey = false;
                colvarLabelIconList.IsForeignKey = false;
                colvarLabelIconList.IsReadOnly = false;
                
                schema.Columns.Add(colvarLabelIconList);
                
                TableSchema.TableColumn colvarShipType = new TableSchema.TableColumn(schema);
                colvarShipType.ColumnName = "ship_type";
                colvarShipType.DataType = DbType.Int32;
                colvarShipType.MaxLength = 0;
                colvarShipType.AutoIncrement = false;
                colvarShipType.IsNullable = true;
                colvarShipType.IsPrimaryKey = false;
                colvarShipType.IsForeignKey = false;
                colvarShipType.IsReadOnly = false;
                
                schema.Columns.Add(colvarShipType);
                
                TableSchema.TableColumn colvarFinalBalanceSheetDate = new TableSchema.TableColumn(schema);
                colvarFinalBalanceSheetDate.ColumnName = "final_balance_sheet_date";
                colvarFinalBalanceSheetDate.DataType = DbType.DateTime;
                colvarFinalBalanceSheetDate.MaxLength = 0;
                colvarFinalBalanceSheetDate.AutoIncrement = false;
                colvarFinalBalanceSheetDate.IsNullable = true;
                colvarFinalBalanceSheetDate.IsPrimaryKey = false;
                colvarFinalBalanceSheetDate.IsForeignKey = false;
                colvarFinalBalanceSheetDate.IsReadOnly = false;
                
                schema.Columns.Add(colvarFinalBalanceSheetDate);
                
                TableSchema.TableColumn colvarBusinessHourStatus = new TableSchema.TableColumn(schema);
                colvarBusinessHourStatus.ColumnName = "business_hour_status";
                colvarBusinessHourStatus.DataType = DbType.Int32;
                colvarBusinessHourStatus.MaxLength = 0;
                colvarBusinessHourStatus.AutoIncrement = false;
                colvarBusinessHourStatus.IsNullable = true;
                colvarBusinessHourStatus.IsPrimaryKey = false;
                colvarBusinessHourStatus.IsForeignKey = false;
                colvarBusinessHourStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarBusinessHourStatus);
                
                TableSchema.TableColumn colvarSlug = new TableSchema.TableColumn(schema);
                colvarSlug.ColumnName = "slug";
                colvarSlug.DataType = DbType.Int32;
                colvarSlug.MaxLength = 0;
                colvarSlug.AutoIncrement = false;
                colvarSlug.IsNullable = true;
                colvarSlug.IsPrimaryKey = false;
                colvarSlug.IsForeignKey = false;
                colvarSlug.IsReadOnly = false;
                
                schema.Columns.Add(colvarSlug);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_vbs_to_house_deal",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewVbsToHouseDeal()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewVbsToHouseDeal(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewVbsToHouseDeal(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewVbsToHouseDeal(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("ProductId")]
        [Bindable(true)]
        public int ProductId 
	    {
		    get
		    {
			    return GetColumnValue<int>("product_id");
		    }
            set 
		    {
			    SetColumnValue("product_id", value);
            }
        }
	      
        [XmlAttribute("ProductGuid")]
        [Bindable(true)]
        public Guid ProductGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("product_guid");
		    }
            set 
		    {
			    SetColumnValue("product_guid", value);
            }
        }
	      
        [XmlAttribute("ProductName")]
        [Bindable(true)]
        public string ProductName 
	    {
		    get
		    {
			    return GetColumnValue<string>("product_name");
		    }
            set 
		    {
			    SetColumnValue("product_name", value);
            }
        }
	      
        [XmlAttribute("AppTitle")]
        [Bindable(true)]
        public string AppTitle 
	    {
		    get
		    {
			    return GetColumnValue<string>("app_title");
		    }
            set 
		    {
			    SetColumnValue("app_title", value);
            }
        }
	      
        [XmlAttribute("DealStartTime")]
        [Bindable(true)]
        public DateTime? DealStartTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("deal_start_time");
		    }
            set 
		    {
			    SetColumnValue("deal_start_time", value);
            }
        }
	      
        [XmlAttribute("DealEndTime")]
        [Bindable(true)]
        public DateTime? DealEndTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("deal_end_time");
		    }
            set 
		    {
			    SetColumnValue("deal_end_time", value);
            }
        }
	      
        [XmlAttribute("UseStartTime")]
        [Bindable(true)]
        public DateTime? UseStartTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("use_start_time");
		    }
            set 
		    {
			    SetColumnValue("use_start_time", value);
            }
        }
	      
        [XmlAttribute("UseEndTime")]
        [Bindable(true)]
        public DateTime? UseEndTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("use_end_time");
		    }
            set 
		    {
			    SetColumnValue("use_end_time", value);
            }
        }
	      
        [XmlAttribute("VendorBillingModel")]
        [Bindable(true)]
        public int VendorBillingModel 
	    {
		    get
		    {
			    return GetColumnValue<int>("vendor_billing_model");
		    }
            set 
		    {
			    SetColumnValue("vendor_billing_model", value);
            }
        }
	      
        [XmlAttribute("SellerId")]
        [Bindable(true)]
        public string SellerId 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_id");
		    }
            set 
		    {
			    SetColumnValue("seller_id", value);
            }
        }
	      
        [XmlAttribute("SellerName")]
        [Bindable(true)]
        public string SellerName 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_name");
		    }
            set 
		    {
			    SetColumnValue("seller_name", value);
            }
        }
	      
        [XmlAttribute("ShippedDate")]
        [Bindable(true)]
        public DateTime? ShippedDate 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("shipped_date");
		    }
            set 
		    {
			    SetColumnValue("shipped_date", value);
            }
        }
	      
        [XmlAttribute("DealEstablished")]
        [Bindable(true)]
        public int? DealEstablished 
	    {
		    get
		    {
			    return GetColumnValue<int?>("deal_established");
		    }
            set 
		    {
			    SetColumnValue("deal_established", value);
            }
        }
	      
        [XmlAttribute("DealId")]
        [Bindable(true)]
        public int? DealId 
	    {
		    get
		    {
			    return GetColumnValue<int?>("deal_id");
		    }
            set 
		    {
			    SetColumnValue("deal_id", value);
            }
        }
	      
        [XmlAttribute("DealType")]
        [Bindable(true)]
        public int DealType 
	    {
		    get
		    {
			    return GetColumnValue<int>("deal_type");
		    }
            set 
		    {
			    SetColumnValue("deal_type", value);
            }
        }
	      
        [XmlAttribute("ComboPackCount")]
        [Bindable(true)]
        public int? ComboPackCount 
	    {
		    get
		    {
			    return GetColumnValue<int?>("combo_pack_count");
		    }
            set 
		    {
			    SetColumnValue("combo_pack_count", value);
            }
        }
	      
        [XmlAttribute("LabelIconList")]
        [Bindable(true)]
        public string LabelIconList 
	    {
		    get
		    {
			    return GetColumnValue<string>("label_icon_list");
		    }
            set 
		    {
			    SetColumnValue("label_icon_list", value);
            }
        }
	      
        [XmlAttribute("ShipType")]
        [Bindable(true)]
        public int? ShipType 
	    {
		    get
		    {
			    return GetColumnValue<int?>("ship_type");
		    }
            set 
		    {
			    SetColumnValue("ship_type", value);
            }
        }
	      
        [XmlAttribute("FinalBalanceSheetDate")]
        [Bindable(true)]
        public DateTime? FinalBalanceSheetDate 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("final_balance_sheet_date");
		    }
            set 
		    {
			    SetColumnValue("final_balance_sheet_date", value);
            }
        }
	      
        [XmlAttribute("BusinessHourStatus")]
        [Bindable(true)]
        public int? BusinessHourStatus 
	    {
		    get
		    {
			    return GetColumnValue<int?>("business_hour_status");
		    }
            set 
		    {
			    SetColumnValue("business_hour_status", value);
            }
        }
	      
        [XmlAttribute("Slug")]
        [Bindable(true)]
        public int? Slug 
	    {
		    get
		    {
			    return GetColumnValue<int?>("slug");
		    }
            set 
		    {
			    SetColumnValue("slug", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string ProductId = @"product_id";
            
            public static string ProductGuid = @"product_guid";
            
            public static string ProductName = @"product_name";
            
            public static string AppTitle = @"app_title";
            
            public static string DealStartTime = @"deal_start_time";
            
            public static string DealEndTime = @"deal_end_time";
            
            public static string UseStartTime = @"use_start_time";
            
            public static string UseEndTime = @"use_end_time";
            
            public static string VendorBillingModel = @"vendor_billing_model";
            
            public static string SellerId = @"seller_id";
            
            public static string SellerName = @"seller_name";
            
            public static string ShippedDate = @"shipped_date";
            
            public static string DealEstablished = @"deal_established";
            
            public static string DealId = @"deal_id";
            
            public static string DealType = @"deal_type";
            
            public static string ComboPackCount = @"combo_pack_count";
            
            public static string LabelIconList = @"label_icon_list";
            
            public static string ShipType = @"ship_type";
            
            public static string FinalBalanceSheetDate = @"final_balance_sheet_date";
            
            public static string BusinessHourStatus = @"business_hour_status";
            
            public static string Slug = @"slug";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

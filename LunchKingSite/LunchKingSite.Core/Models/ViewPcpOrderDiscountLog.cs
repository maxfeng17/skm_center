﻿using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the ViewPcpOrderDiscountLog class.
    /// </summary>
    [Serializable]
    public partial class ViewPcpOrderDiscountLogCollection : ReadOnlyList<ViewPcpOrderDiscountLog, ViewPcpOrderDiscountLogCollection>
    {
        public ViewPcpOrderDiscountLogCollection() { }
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_pcp_order_discount_log view.
    /// </summary>
    [Serializable]
    public partial class ViewPcpOrderDiscountLog : ReadOnlyRecord<ViewPcpOrderDiscountLog>, IReadOnlyRecord
    {

        #region Default Settings
        protected static void SetSQLProps()
        {
            GetTableSchema();
        }
        #endregion
        #region Schema Accessor
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_pcp_order_discount_log", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarUserMemCardId = new TableSchema.TableColumn(schema);
                colvarUserMemCardId.ColumnName = "user_mem_card_id";
                colvarUserMemCardId.DataType = DbType.Int32;
                colvarUserMemCardId.MaxLength = 0;
                colvarUserMemCardId.AutoIncrement = false;
                colvarUserMemCardId.IsNullable = false;
                colvarUserMemCardId.IsPrimaryKey = false;
                colvarUserMemCardId.IsForeignKey = false;
                colvarUserMemCardId.IsReadOnly = false;

                schema.Columns.Add(colvarUserMemCardId);

                TableSchema.TableColumn colvarOrderTime = new TableSchema.TableColumn(schema);
                colvarOrderTime.ColumnName = "order_time";
                colvarOrderTime.DataType = DbType.DateTime;
                colvarOrderTime.MaxLength = 0;
                colvarOrderTime.AutoIncrement = false;
                colvarOrderTime.IsNullable = false;
                colvarOrderTime.IsPrimaryKey = false;
                colvarOrderTime.IsForeignKey = false;
                colvarOrderTime.IsReadOnly = false;

                schema.Columns.Add(colvarOrderTime);

                TableSchema.TableColumn colvarOrderAmount = new TableSchema.TableColumn(schema);
                colvarOrderAmount.ColumnName = "order_amount";
                colvarOrderAmount.DataType = DbType.Currency;
                colvarOrderAmount.MaxLength = 0;
                colvarOrderAmount.AutoIncrement = false;
                colvarOrderAmount.IsNullable = true;
                colvarOrderAmount.IsPrimaryKey = false;
                colvarOrderAmount.IsForeignKey = false;
                colvarOrderAmount.IsReadOnly = false;

                schema.Columns.Add(colvarOrderAmount);

                TableSchema.TableColumn colvarDiscountAmount = new TableSchema.TableColumn(schema);
                colvarDiscountAmount.ColumnName = "discount_amount";
                colvarDiscountAmount.DataType = DbType.Currency;
                colvarDiscountAmount.MaxLength = 0;
                colvarDiscountAmount.AutoIncrement = false;
                colvarDiscountAmount.IsNullable = true;
                colvarDiscountAmount.IsPrimaryKey = false;
                colvarDiscountAmount.IsForeignKey = false;
                colvarDiscountAmount.IsReadOnly = false;

                schema.Columns.Add(colvarDiscountAmount);

                TableSchema.TableColumn colvarSendDate = new TableSchema.TableColumn(schema);
                colvarSendDate.ColumnName = "send_date";
                colvarSendDate.DataType = DbType.DateTime;
                colvarSendDate.MaxLength = 0;
                colvarSendDate.AutoIncrement = false;
                colvarSendDate.IsNullable = true;
                colvarSendDate.IsPrimaryKey = false;
                colvarSendDate.IsForeignKey = false;
                colvarSendDate.IsReadOnly = false;

                schema.Columns.Add(colvarSendDate);

                TableSchema.TableColumn colvarBonusPoint = new TableSchema.TableColumn(schema);
                colvarBonusPoint.ColumnName = "bonus_point";
                colvarBonusPoint.DataType = DbType.Currency;
                colvarBonusPoint.MaxLength = 0;
                colvarBonusPoint.AutoIncrement = false;
                colvarBonusPoint.IsNullable = true;
                colvarBonusPoint.IsPrimaryKey = false;
                colvarBonusPoint.IsForeignKey = false;
                colvarBonusPoint.IsReadOnly = false;

                schema.Columns.Add(colvarBonusPoint);

                TableSchema.TableColumn colvarCode = new TableSchema.TableColumn(schema);
                colvarCode.ColumnName = "code";
                colvarCode.DataType = DbType.AnsiString;
                colvarCode.MaxLength = 20;
                colvarCode.AutoIncrement = false;
                colvarCode.IsNullable = true;
                colvarCode.IsPrimaryKey = false;
                colvarCode.IsForeignKey = false;
                colvarCode.IsReadOnly = false;

                schema.Columns.Add(colvarCode);

                TableSchema.TableColumn colvarOwnerName = new TableSchema.TableColumn(schema);
                colvarOwnerName.ColumnName = "owner_name";
                colvarOwnerName.DataType = DbType.String;
                colvarOwnerName.MaxLength = 256;
                colvarOwnerName.AutoIncrement = false;
                colvarOwnerName.IsNullable = true;
                colvarOwnerName.IsPrimaryKey = false;
                colvarOwnerName.IsForeignKey = false;
                colvarOwnerName.IsReadOnly = false;

                schema.Columns.Add(colvarOwnerName);

                TableSchema.TableColumn colvarVerifyName = new TableSchema.TableColumn(schema);
                colvarVerifyName.ColumnName = "verify_name";
                colvarVerifyName.DataType = DbType.String;
                colvarVerifyName.MaxLength = 256;
                colvarVerifyName.AutoIncrement = false;
                colvarVerifyName.IsNullable = true;
                colvarVerifyName.IsPrimaryKey = false;
                colvarVerifyName.IsForeignKey = false;
                colvarVerifyName.IsReadOnly = false;

                schema.Columns.Add(colvarVerifyName);

                TableSchema.TableColumn colvarType = new TableSchema.TableColumn(schema);
                colvarType.ColumnName = "type";
                colvarType.DataType = DbType.Int32;
                colvarType.MaxLength = 0;
                colvarType.AutoIncrement = false;
                colvarType.IsNullable = true;
                colvarType.IsPrimaryKey = false;
                colvarType.IsForeignKey = false;
                colvarType.IsReadOnly = false;

                schema.Columns.Add(colvarType);

                TableSchema.TableColumn colvarSuperBonusCumulative = new TableSchema.TableColumn(schema);
                colvarSuperBonusCumulative.ColumnName = "super_bonus_cumulative";
                colvarSuperBonusCumulative.DataType = DbType.Currency;
                colvarSuperBonusCumulative.MaxLength = 0;
                colvarSuperBonusCumulative.AutoIncrement = false;
                colvarSuperBonusCumulative.IsNullable = true;
                colvarSuperBonusCumulative.IsPrimaryKey = false;
                colvarSuperBonusCumulative.IsForeignKey = false;
                colvarSuperBonusCumulative.IsReadOnly = false;

                schema.Columns.Add(colvarSuperBonusCumulative);


                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_pcp_order_discount_log", schema);
            }
        }
        #endregion

        #region Query Accessor
        public static Query CreateQuery()
        {
            return new Query(Schema);
        }
        #endregion

        #region .ctors
        public ViewPcpOrderDiscountLog()
        {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewPcpOrderDiscountLog(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
            {
                ForceDefaults();
            }
            MarkNew();
        }

        public ViewPcpOrderDiscountLog(object keyID)
        {
            SetSQLProps();
            LoadByKey(keyID);
        }

        public ViewPcpOrderDiscountLog(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName, columnValue);
        }

        #endregion

        #region Props


        [XmlAttribute("UserMemCardId")]
        [Bindable(true)]
        public int UserMemCardId
        {
            get
            {
                return GetColumnValue<int>("user_mem_card_id");
            }
            set
            {
                SetColumnValue("user_mem_card_id", value);
            }
        }

        [XmlAttribute("OrderTime")]
        [Bindable(true)]
        public DateTime OrderTime
        {
            get
            {
                return GetColumnValue<DateTime>("order_time");
            }
            set
            {
                SetColumnValue("order_time", value);
            }
        }

        [XmlAttribute("OrderAmount")]
        [Bindable(true)]
        public decimal? OrderAmount
        {
            get
            {
                return GetColumnValue<decimal?>("order_amount");
            }
            set
            {
                SetColumnValue("order_amount", value);
            }
        }

        [XmlAttribute("DiscountAmount")]
        [Bindable(true)]
        public decimal? DiscountAmount
        {
            get
            {
                return GetColumnValue<decimal?>("discount_amount");
            }
            set
            {
                SetColumnValue("discount_amount", value);
            }
        }

        [XmlAttribute("SendDate")]
        [Bindable(true)]
        public DateTime? SendDate
        {
            get
            {
                return GetColumnValue<DateTime?>("send_date");
            }
            set
            {
                SetColumnValue("send_date", value);
            }
        }

        [XmlAttribute("BonusPoint")]
        [Bindable(true)]
        public decimal? BonusPoint
        {
            get
            {
                return GetColumnValue<decimal?>("bonus_point");
            }
            set
            {
                SetColumnValue("bonus_point", value);
            }
        }

        [XmlAttribute("Code")]
        [Bindable(true)]
        public string Code
        {
            get
            {
                return GetColumnValue<string>("code");
            }
            set
            {
                SetColumnValue("code", value);
            }
        }

        [XmlAttribute("OwnerName")]
        [Bindable(true)]
        public string OwnerName
        {
            get
            {
                return GetColumnValue<string>("owner_name");
            }
            set
            {
                SetColumnValue("owner_name", value);
            }
        }

        [XmlAttribute("VerifyName")]
        [Bindable(true)]
        public string VerifyName
        {
            get
            {
                return GetColumnValue<string>("verify_name");
            }
            set
            {
                SetColumnValue("verify_name", value);
            }
        }

        [XmlAttribute("Type")]
        [Bindable(true)]
        public int? Type
        {
            get
            {
                return GetColumnValue<int?>("type");
            }
            set
            {
                SetColumnValue("type", value);
            }
        }

        [XmlAttribute("SuperBonusCumulative")]
        [Bindable(true)]
        public decimal? SuperBonusCumulative
        {
            get
            {
                return GetColumnValue<decimal?>("super_bonus_cumulative");
            }
            set
            {
                SetColumnValue("super_bonus_cumulative", value);
            }
        }

        #endregion

        #region Columns Struct
        public struct Columns
        {


            public static string UserMemCardId = @"user_mem_card_id";

            public static string OrderTime = @"order_time";

            public static string OrderAmount = @"order_amount";

            public static string DiscountAmount = @"discount_amount";

            public static string SendDate = @"send_date";

            public static string BonusPoint = @"bonus_point";

            public static string Code = @"code";

            public static string OwnerName = @"owner_name";

            public static string VerifyName = @"verify_name";

            public static string Type = @"type";

            public static string SuperBonusCumulative = @"super_bonus_cumulative";

        }
        #endregion


        #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName)
        {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName)
        {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion

    }
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewPcpOrderInfo class.
    /// </summary>
    [Serializable]
    public partial class ViewPcpOrderInfoCollection : ReadOnlyList<ViewPcpOrderInfo, ViewPcpOrderInfoCollection>
    {        
        public ViewPcpOrderInfoCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_pcp_order_info view.
    /// </summary>
    [Serializable]
    public partial class ViewPcpOrderInfo : ReadOnlyRecord<ViewPcpOrderInfo>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_pcp_order_info", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarPk = new TableSchema.TableColumn(schema);
                colvarPk.ColumnName = "pk";
                colvarPk.DataType = DbType.Int32;
                colvarPk.MaxLength = 0;
                colvarPk.AutoIncrement = false;
                colvarPk.IsNullable = false;
                colvarPk.IsPrimaryKey = false;
                colvarPk.IsForeignKey = false;
                colvarPk.IsReadOnly = false;
                
                schema.Columns.Add(colvarPk);
                
                TableSchema.TableColumn colvarGuid = new TableSchema.TableColumn(schema);
                colvarGuid.ColumnName = "guid";
                colvarGuid.DataType = DbType.Guid;
                colvarGuid.MaxLength = 0;
                colvarGuid.AutoIncrement = false;
                colvarGuid.IsNullable = false;
                colvarGuid.IsPrimaryKey = false;
                colvarGuid.IsForeignKey = false;
                colvarGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarGuid);
                
                TableSchema.TableColumn colvarUserId = new TableSchema.TableColumn(schema);
                colvarUserId.ColumnName = "user_id";
                colvarUserId.DataType = DbType.Int32;
                colvarUserId.MaxLength = 0;
                colvarUserId.AutoIncrement = false;
                colvarUserId.IsNullable = false;
                colvarUserId.IsPrimaryKey = false;
                colvarUserId.IsForeignKey = false;
                colvarUserId.IsReadOnly = false;
                
                schema.Columns.Add(colvarUserId);
                
                TableSchema.TableColumn colvarCardGroupId = new TableSchema.TableColumn(schema);
                colvarCardGroupId.ColumnName = "card_group_id";
                colvarCardGroupId.DataType = DbType.Int32;
                colvarCardGroupId.MaxLength = 0;
                colvarCardGroupId.AutoIncrement = false;
                colvarCardGroupId.IsNullable = true;
                colvarCardGroupId.IsPrimaryKey = false;
                colvarCardGroupId.IsForeignKey = false;
                colvarCardGroupId.IsReadOnly = false;
                
                schema.Columns.Add(colvarCardGroupId);
                
                TableSchema.TableColumn colvarUserCardId = new TableSchema.TableColumn(schema);
                colvarUserCardId.ColumnName = "user_card_id";
                colvarUserCardId.DataType = DbType.Int32;
                colvarUserCardId.MaxLength = 0;
                colvarUserCardId.AutoIncrement = false;
                colvarUserCardId.IsNullable = true;
                colvarUserCardId.IsPrimaryKey = false;
                colvarUserCardId.IsForeignKey = false;
                colvarUserCardId.IsReadOnly = false;
                
                schema.Columns.Add(colvarUserCardId);
                
                TableSchema.TableColumn colvarAmount = new TableSchema.TableColumn(schema);
                colvarAmount.ColumnName = "amount";
                colvarAmount.DataType = DbType.Currency;
                colvarAmount.MaxLength = 0;
                colvarAmount.AutoIncrement = false;
                colvarAmount.IsNullable = true;
                colvarAmount.IsPrimaryKey = false;
                colvarAmount.IsForeignKey = false;
                colvarAmount.IsReadOnly = false;
                
                schema.Columns.Add(colvarAmount);
                
                TableSchema.TableColumn colvarDiscountCodeId = new TableSchema.TableColumn(schema);
                colvarDiscountCodeId.ColumnName = "discount_code_id";
                colvarDiscountCodeId.DataType = DbType.Int32;
                colvarDiscountCodeId.MaxLength = 0;
                colvarDiscountCodeId.AutoIncrement = false;
                colvarDiscountCodeId.IsNullable = true;
                colvarDiscountCodeId.IsPrimaryKey = false;
                colvarDiscountCodeId.IsForeignKey = false;
                colvarDiscountCodeId.IsReadOnly = false;
                
                schema.Columns.Add(colvarDiscountCodeId);
                
                TableSchema.TableColumn colvarBonusPoint = new TableSchema.TableColumn(schema);
                colvarBonusPoint.ColumnName = "bonus_point";
                colvarBonusPoint.DataType = DbType.Currency;
                colvarBonusPoint.MaxLength = 0;
                colvarBonusPoint.AutoIncrement = false;
                colvarBonusPoint.IsNullable = true;
                colvarBonusPoint.IsPrimaryKey = false;
                colvarBonusPoint.IsForeignKey = false;
                colvarBonusPoint.IsReadOnly = false;
                
                schema.Columns.Add(colvarBonusPoint);
                
                TableSchema.TableColumn colvarOrderTime = new TableSchema.TableColumn(schema);
                colvarOrderTime.ColumnName = "order_time";
                colvarOrderTime.DataType = DbType.DateTime;
                colvarOrderTime.MaxLength = 0;
                colvarOrderTime.AutoIncrement = false;
                colvarOrderTime.IsNullable = false;
                colvarOrderTime.IsPrimaryKey = false;
                colvarOrderTime.IsForeignKey = false;
                colvarOrderTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderTime);
                
                TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
                colvarStatus.ColumnName = "status";
                colvarStatus.DataType = DbType.Int32;
                colvarStatus.MaxLength = 0;
                colvarStatus.AutoIncrement = false;
                colvarStatus.IsNullable = false;
                colvarStatus.IsPrimaryKey = false;
                colvarStatus.IsForeignKey = false;
                colvarStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarStatus);
                
                TableSchema.TableColumn colvarStatusDescription = new TableSchema.TableColumn(schema);
                colvarStatusDescription.ColumnName = "status_description";
                colvarStatusDescription.DataType = DbType.String;
                colvarStatusDescription.MaxLength = -1;
                colvarStatusDescription.AutoIncrement = false;
                colvarStatusDescription.IsNullable = true;
                colvarStatusDescription.IsPrimaryKey = false;
                colvarStatusDescription.IsForeignKey = false;
                colvarStatusDescription.IsReadOnly = false;
                
                schema.Columns.Add(colvarStatusDescription);
                
                TableSchema.TableColumn colvarIdentityCodeId = new TableSchema.TableColumn(schema);
                colvarIdentityCodeId.ColumnName = "identity_code_id";
                colvarIdentityCodeId.DataType = DbType.Int64;
                colvarIdentityCodeId.MaxLength = 0;
                colvarIdentityCodeId.AutoIncrement = false;
                colvarIdentityCodeId.IsNullable = true;
                colvarIdentityCodeId.IsPrimaryKey = false;
                colvarIdentityCodeId.IsForeignKey = false;
                colvarIdentityCodeId.IsReadOnly = false;
                
                schema.Columns.Add(colvarIdentityCodeId);
                
                TableSchema.TableColumn colvarDiscountAmount = new TableSchema.TableColumn(schema);
                colvarDiscountAmount.ColumnName = "discount_amount";
                colvarDiscountAmount.DataType = DbType.Currency;
                colvarDiscountAmount.MaxLength = 0;
                colvarDiscountAmount.AutoIncrement = false;
                colvarDiscountAmount.IsNullable = true;
                colvarDiscountAmount.IsPrimaryKey = false;
                colvarDiscountAmount.IsForeignKey = false;
                colvarDiscountAmount.IsReadOnly = false;
                
                schema.Columns.Add(colvarDiscountAmount);
                
                TableSchema.TableColumn colvarStoreGuid = new TableSchema.TableColumn(schema);
                colvarStoreGuid.ColumnName = "store_guid";
                colvarStoreGuid.DataType = DbType.Guid;
                colvarStoreGuid.MaxLength = 0;
                colvarStoreGuid.AutoIncrement = false;
                colvarStoreGuid.IsNullable = false;
                colvarStoreGuid.IsPrimaryKey = false;
                colvarStoreGuid.IsForeignKey = false;
                colvarStoreGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarStoreGuid);
                
                TableSchema.TableColumn colvarExpr1 = new TableSchema.TableColumn(schema);
                colvarExpr1.ColumnName = "Expr1";
                colvarExpr1.DataType = DbType.Guid;
                colvarExpr1.MaxLength = 0;
                colvarExpr1.AutoIncrement = false;
                colvarExpr1.IsNullable = false;
                colvarExpr1.IsPrimaryKey = false;
                colvarExpr1.IsForeignKey = false;
                colvarExpr1.IsReadOnly = false;
                
                schema.Columns.Add(colvarExpr1);
                
                TableSchema.TableColumn colvarSellerName = new TableSchema.TableColumn(schema);
                colvarSellerName.ColumnName = "seller_name";
                colvarSellerName.DataType = DbType.String;
                colvarSellerName.MaxLength = 100;
                colvarSellerName.AutoIncrement = false;
                colvarSellerName.IsNullable = false;
                colvarSellerName.IsPrimaryKey = false;
                colvarSellerName.IsForeignKey = false;
                colvarSellerName.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerName);
                
                TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
                colvarCreateId.ColumnName = "create_id";
                colvarCreateId.DataType = DbType.Int32;
                colvarCreateId.MaxLength = 0;
                colvarCreateId.AutoIncrement = false;
                colvarCreateId.IsNullable = true;
                colvarCreateId.IsPrimaryKey = false;
                colvarCreateId.IsForeignKey = false;
                colvarCreateId.IsReadOnly = false;
                
                schema.Columns.Add(colvarCreateId);
                
                TableSchema.TableColumn colvarAccountId = new TableSchema.TableColumn(schema);
                colvarAccountId.ColumnName = "account_id";
                colvarAccountId.DataType = DbType.String;
                colvarAccountId.MaxLength = 256;
                colvarAccountId.AutoIncrement = false;
                colvarAccountId.IsNullable = true;
                colvarAccountId.IsPrimaryKey = false;
                colvarAccountId.IsForeignKey = false;
                colvarAccountId.IsReadOnly = false;
                
                schema.Columns.Add(colvarAccountId);
                
                TableSchema.TableColumn colvarAccountName = new TableSchema.TableColumn(schema);
                colvarAccountName.ColumnName = "account_name";
                colvarAccountName.DataType = DbType.String;
                colvarAccountName.MaxLength = 100;
                colvarAccountName.AutoIncrement = false;
                colvarAccountName.IsNullable = true;
                colvarAccountName.IsPrimaryKey = false;
                colvarAccountName.IsForeignKey = false;
                colvarAccountName.IsReadOnly = false;
                
                schema.Columns.Add(colvarAccountName);
                
                TableSchema.TableColumn colvarMemberFirstName = new TableSchema.TableColumn(schema);
                colvarMemberFirstName.ColumnName = "member_first_name";
                colvarMemberFirstName.DataType = DbType.String;
                colvarMemberFirstName.MaxLength = 50;
                colvarMemberFirstName.AutoIncrement = false;
                colvarMemberFirstName.IsNullable = true;
                colvarMemberFirstName.IsPrimaryKey = false;
                colvarMemberFirstName.IsForeignKey = false;
                colvarMemberFirstName.IsReadOnly = false;
                
                schema.Columns.Add(colvarMemberFirstName);
                
                TableSchema.TableColumn colvarMemberLastName = new TableSchema.TableColumn(schema);
                colvarMemberLastName.ColumnName = "member_last_name";
                colvarMemberLastName.DataType = DbType.String;
                colvarMemberLastName.MaxLength = 50;
                colvarMemberLastName.AutoIncrement = false;
                colvarMemberLastName.IsNullable = true;
                colvarMemberLastName.IsPrimaryKey = false;
                colvarMemberLastName.IsForeignKey = false;
                colvarMemberLastName.IsReadOnly = false;
                
                schema.Columns.Add(colvarMemberLastName);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_pcp_order_info",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewPcpOrderInfo()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewPcpOrderInfo(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewPcpOrderInfo(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewPcpOrderInfo(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("Pk")]
        [Bindable(true)]
        public int Pk 
	    {
		    get
		    {
			    return GetColumnValue<int>("pk");
		    }
            set 
		    {
			    SetColumnValue("pk", value);
            }
        }
	      
        [XmlAttribute("Guid")]
        [Bindable(true)]
        public Guid Guid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("guid");
		    }
            set 
		    {
			    SetColumnValue("guid", value);
            }
        }
	      
        [XmlAttribute("UserId")]
        [Bindable(true)]
        public int UserId 
	    {
		    get
		    {
			    return GetColumnValue<int>("user_id");
		    }
            set 
		    {
			    SetColumnValue("user_id", value);
            }
        }
	      
        [XmlAttribute("CardGroupId")]
        [Bindable(true)]
        public int? CardGroupId 
	    {
		    get
		    {
			    return GetColumnValue<int?>("card_group_id");
		    }
            set 
		    {
			    SetColumnValue("card_group_id", value);
            }
        }
	      
        [XmlAttribute("UserCardId")]
        [Bindable(true)]
        public int? UserCardId 
	    {
		    get
		    {
			    return GetColumnValue<int?>("user_card_id");
		    }
            set 
		    {
			    SetColumnValue("user_card_id", value);
            }
        }
	      
        [XmlAttribute("Amount")]
        [Bindable(true)]
        public decimal? Amount 
	    {
		    get
		    {
			    return GetColumnValue<decimal?>("amount");
		    }
            set 
		    {
			    SetColumnValue("amount", value);
            }
        }
	      
        [XmlAttribute("DiscountCodeId")]
        [Bindable(true)]
        public int? DiscountCodeId 
	    {
		    get
		    {
			    return GetColumnValue<int?>("discount_code_id");
		    }
            set 
		    {
			    SetColumnValue("discount_code_id", value);
            }
        }
	      
        [XmlAttribute("BonusPoint")]
        [Bindable(true)]
        public decimal? BonusPoint 
	    {
		    get
		    {
			    return GetColumnValue<decimal?>("bonus_point");
		    }
            set 
		    {
			    SetColumnValue("bonus_point", value);
            }
        }
	      
        [XmlAttribute("OrderTime")]
        [Bindable(true)]
        public DateTime OrderTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("order_time");
		    }
            set 
		    {
			    SetColumnValue("order_time", value);
            }
        }
	      
        [XmlAttribute("Status")]
        [Bindable(true)]
        public int Status 
	    {
		    get
		    {
			    return GetColumnValue<int>("status");
		    }
            set 
		    {
			    SetColumnValue("status", value);
            }
        }
	      
        [XmlAttribute("StatusDescription")]
        [Bindable(true)]
        public string StatusDescription 
	    {
		    get
		    {
			    return GetColumnValue<string>("status_description");
		    }
            set 
		    {
			    SetColumnValue("status_description", value);
            }
        }
	      
        [XmlAttribute("IdentityCodeId")]
        [Bindable(true)]
        public long? IdentityCodeId 
	    {
		    get
		    {
			    return GetColumnValue<long?>("identity_code_id");
		    }
            set 
		    {
			    SetColumnValue("identity_code_id", value);
            }
        }
	      
        [XmlAttribute("DiscountAmount")]
        [Bindable(true)]
        public decimal? DiscountAmount 
	    {
		    get
		    {
			    return GetColumnValue<decimal?>("discount_amount");
		    }
            set 
		    {
			    SetColumnValue("discount_amount", value);
            }
        }
	      
        [XmlAttribute("StoreGuid")]
        [Bindable(true)]
        public Guid StoreGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("store_guid");
		    }
            set 
		    {
			    SetColumnValue("store_guid", value);
            }
        }
	      
        [XmlAttribute("Expr1")]
        [Bindable(true)]
        public Guid Expr1 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("Expr1");
		    }
            set 
		    {
			    SetColumnValue("Expr1", value);
            }
        }
	      
        [XmlAttribute("SellerName")]
        [Bindable(true)]
        public string SellerName 
	    {
		    get
		    {
			    return GetColumnValue<string>("seller_name");
		    }
            set 
		    {
			    SetColumnValue("seller_name", value);
            }
        }
	      
        [XmlAttribute("CreateId")]
        [Bindable(true)]
        public int? CreateId 
	    {
		    get
		    {
			    return GetColumnValue<int?>("create_id");
		    }
            set 
		    {
			    SetColumnValue("create_id", value);
            }
        }
	      
        [XmlAttribute("AccountId")]
        [Bindable(true)]
        public string AccountId 
	    {
		    get
		    {
			    return GetColumnValue<string>("account_id");
		    }
            set 
		    {
			    SetColumnValue("account_id", value);
            }
        }
	      
        [XmlAttribute("AccountName")]
        [Bindable(true)]
        public string AccountName 
	    {
		    get
		    {
			    return GetColumnValue<string>("account_name");
		    }
            set 
		    {
			    SetColumnValue("account_name", value);
            }
        }
	      
        [XmlAttribute("MemberFirstName")]
        [Bindable(true)]
        public string MemberFirstName 
	    {
		    get
		    {
			    return GetColumnValue<string>("member_first_name");
		    }
            set 
		    {
			    SetColumnValue("member_first_name", value);
            }
        }
	      
        [XmlAttribute("MemberLastName")]
        [Bindable(true)]
        public string MemberLastName 
	    {
		    get
		    {
			    return GetColumnValue<string>("member_last_name");
		    }
            set 
		    {
			    SetColumnValue("member_last_name", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string Pk = @"pk";
            
            public static string Guid = @"guid";
            
            public static string UserId = @"user_id";
            
            public static string CardGroupId = @"card_group_id";
            
            public static string UserCardId = @"user_card_id";
            
            public static string Amount = @"amount";
            
            public static string DiscountCodeId = @"discount_code_id";
            
            public static string BonusPoint = @"bonus_point";
            
            public static string OrderTime = @"order_time";
            
            public static string Status = @"status";
            
            public static string StatusDescription = @"status_description";
            
            public static string IdentityCodeId = @"identity_code_id";
            
            public static string DiscountAmount = @"discount_amount";
            
            public static string StoreGuid = @"store_guid";
            
            public static string Expr1 = @"Expr1";
            
            public static string SellerName = @"seller_name";
            
            public static string CreateId = @"create_id";
            
            public static string AccountId = @"account_id";
            
            public static string AccountName = @"account_name";
            
            public static string MemberFirstName = @"member_first_name";
            
            public static string MemberLastName = @"member_last_name";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

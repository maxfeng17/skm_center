using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the PcpAssignmentMember class.
	/// </summary>
    [Serializable]
	public partial class PcpAssignmentMemberCollection : RepositoryList<PcpAssignmentMember, PcpAssignmentMemberCollection>
	{	   
		public PcpAssignmentMemberCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>PcpAssignmentMemberCollection</returns>
		public PcpAssignmentMemberCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                PcpAssignmentMember o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the pcp_assignment_member table.
	/// </summary>
	[Serializable]
	public partial class PcpAssignmentMember : RepositoryRecord<PcpAssignmentMember>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public PcpAssignmentMember()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public PcpAssignmentMember(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("pcp_assignment_member", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int64;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarAssignmentId = new TableSchema.TableColumn(schema);
				colvarAssignmentId.ColumnName = "assignment_id";
				colvarAssignmentId.DataType = DbType.Int32;
				colvarAssignmentId.MaxLength = 0;
				colvarAssignmentId.AutoIncrement = false;
				colvarAssignmentId.IsNullable = false;
				colvarAssignmentId.IsPrimaryKey = false;
				colvarAssignmentId.IsForeignKey = false;
				colvarAssignmentId.IsReadOnly = false;
				colvarAssignmentId.DefaultSetting = @"";
				colvarAssignmentId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAssignmentId);
				
				TableSchema.TableColumn colvarMemberId = new TableSchema.TableColumn(schema);
				colvarMemberId.ColumnName = "member_id";
				colvarMemberId.DataType = DbType.Int32;
				colvarMemberId.MaxLength = 0;
				colvarMemberId.AutoIncrement = false;
				colvarMemberId.IsNullable = false;
				colvarMemberId.IsPrimaryKey = false;
				colvarMemberId.IsForeignKey = false;
				colvarMemberId.IsReadOnly = false;
				colvarMemberId.DefaultSetting = @"";
				colvarMemberId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMemberId);
				
				TableSchema.TableColumn colvarMemberType = new TableSchema.TableColumn(schema);
				colvarMemberType.ColumnName = "member_type";
				colvarMemberType.DataType = DbType.Int32;
				colvarMemberType.MaxLength = 0;
				colvarMemberType.AutoIncrement = false;
				colvarMemberType.IsNullable = false;
				colvarMemberType.IsPrimaryKey = false;
				colvarMemberType.IsForeignKey = false;
				colvarMemberType.IsReadOnly = false;
				colvarMemberType.DefaultSetting = @"";
				colvarMemberType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMemberType);
				
				TableSchema.TableColumn colvarSendTime = new TableSchema.TableColumn(schema);
				colvarSendTime.ColumnName = "send_time";
				colvarSendTime.DataType = DbType.DateTime;
				colvarSendTime.MaxLength = 0;
				colvarSendTime.AutoIncrement = false;
				colvarSendTime.IsNullable = true;
				colvarSendTime.IsPrimaryKey = false;
				colvarSendTime.IsForeignKey = false;
				colvarSendTime.IsReadOnly = false;
				colvarSendTime.DefaultSetting = @"";
				colvarSendTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSendTime);
				
				TableSchema.TableColumn colvarSmsLogId = new TableSchema.TableColumn(schema);
				colvarSmsLogId.ColumnName = "sms_log_id";
				colvarSmsLogId.DataType = DbType.Int32;
				colvarSmsLogId.MaxLength = 0;
				colvarSmsLogId.AutoIncrement = false;
				colvarSmsLogId.IsNullable = true;
				colvarSmsLogId.IsPrimaryKey = false;
				colvarSmsLogId.IsForeignKey = false;
				colvarSmsLogId.IsReadOnly = false;
				colvarSmsLogId.DefaultSetting = @"";
				colvarSmsLogId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSmsLogId);
				
				TableSchema.TableColumn colvarDiscountCodeId = new TableSchema.TableColumn(schema);
				colvarDiscountCodeId.ColumnName = "discount_code_id";
				colvarDiscountCodeId.DataType = DbType.Int32;
				colvarDiscountCodeId.MaxLength = 0;
				colvarDiscountCodeId.AutoIncrement = false;
				colvarDiscountCodeId.IsNullable = true;
				colvarDiscountCodeId.IsPrimaryKey = false;
				colvarDiscountCodeId.IsForeignKey = false;
				colvarDiscountCodeId.IsReadOnly = false;
				colvarDiscountCodeId.DefaultSetting = @"";
				colvarDiscountCodeId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDiscountCodeId);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("pcp_assignment_member",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public long Id 
		{
			get { return GetColumnValue<long>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("AssignmentId")]
		[Bindable(true)]
		public int AssignmentId 
		{
			get { return GetColumnValue<int>(Columns.AssignmentId); }
			set { SetColumnValue(Columns.AssignmentId, value); }
		}
		  
		[XmlAttribute("MemberId")]
		[Bindable(true)]
		public int MemberId 
		{
			get { return GetColumnValue<int>(Columns.MemberId); }
			set { SetColumnValue(Columns.MemberId, value); }
		}
		  
		[XmlAttribute("MemberType")]
		[Bindable(true)]
		public int MemberType 
		{
			get { return GetColumnValue<int>(Columns.MemberType); }
			set { SetColumnValue(Columns.MemberType, value); }
		}
		  
		[XmlAttribute("SendTime")]
		[Bindable(true)]
		public DateTime? SendTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.SendTime); }
			set { SetColumnValue(Columns.SendTime, value); }
		}
		  
		[XmlAttribute("SmsLogId")]
		[Bindable(true)]
		public int? SmsLogId 
		{
			get { return GetColumnValue<int?>(Columns.SmsLogId); }
			set { SetColumnValue(Columns.SmsLogId, value); }
		}
		  
		[XmlAttribute("DiscountCodeId")]
		[Bindable(true)]
		public int? DiscountCodeId 
		{
			get { return GetColumnValue<int?>(Columns.DiscountCodeId); }
			set { SetColumnValue(Columns.DiscountCodeId, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn AssignmentIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn MemberIdColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn MemberTypeColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn SendTimeColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn SmsLogIdColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn DiscountCodeIdColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string AssignmentId = @"assignment_id";
			 public static string MemberId = @"member_id";
			 public static string MemberType = @"member_type";
			 public static string SendTime = @"send_time";
			 public static string SmsLogId = @"sms_log_id";
			 public static string DiscountCodeId = @"discount_code_id";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

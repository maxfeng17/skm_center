using System; 
using System.Data;
using System.ComponentModel;
using System.Xml.Serialization;
using SubSonic; 
namespace LunchKingSite.DataOrm
{
    [Serializable]
	public partial class MemberCheckoutInfoCollection : RepositoryList<MemberCheckoutInfo, MemberCheckoutInfoCollection>
	{	   
		public MemberCheckoutInfoCollection() {}
        
		public MemberCheckoutInfoCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                MemberCheckoutInfo o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if(pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch(w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if(!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if(remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
	}

	[Serializable]
	public partial class MemberCheckoutInfo : RepositoryRecord<MemberCheckoutInfo>, IRecordBase
	{
		#region .ctors and Default Settings
		public MemberCheckoutInfo()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public MemberCheckoutInfo(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("member_checkout_info", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarUserId = new TableSchema.TableColumn(schema);
				colvarUserId.ColumnName = "user_id";
				colvarUserId.DataType = DbType.Int32;
				colvarUserId.MaxLength = 0;
				colvarUserId.AutoIncrement = false;
				colvarUserId.IsNullable = false;
				colvarUserId.IsPrimaryKey = false;
				colvarUserId.IsForeignKey = false;
				colvarUserId.IsReadOnly = false;
				colvarUserId.DefaultSetting = @"";
				colvarUserId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUserId);
				
				TableSchema.TableColumn colvarFamilyStoreId = new TableSchema.TableColumn(schema);
				colvarFamilyStoreId.ColumnName = "family_store_id";
				colvarFamilyStoreId.DataType = DbType.String;
				colvarFamilyStoreId.MaxLength = 10;
				colvarFamilyStoreId.AutoIncrement = false;
				colvarFamilyStoreId.IsNullable = true;
				colvarFamilyStoreId.IsPrimaryKey = false;
				colvarFamilyStoreId.IsForeignKey = false;
				colvarFamilyStoreId.IsReadOnly = false;
				colvarFamilyStoreId.DefaultSetting = @"";
				colvarFamilyStoreId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFamilyStoreId);
				
				TableSchema.TableColumn colvarFamilyStoreName = new TableSchema.TableColumn(schema);
				colvarFamilyStoreName.ColumnName = "family_store_name";
				colvarFamilyStoreName.DataType = DbType.String;
				colvarFamilyStoreName.MaxLength = 50;
				colvarFamilyStoreName.AutoIncrement = false;
				colvarFamilyStoreName.IsNullable = true;
				colvarFamilyStoreName.IsPrimaryKey = false;
				colvarFamilyStoreName.IsForeignKey = false;
				colvarFamilyStoreName.IsReadOnly = false;
				colvarFamilyStoreName.DefaultSetting = @"";
				colvarFamilyStoreName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFamilyStoreName);
				
				TableSchema.TableColumn colvarFamilyStoreTel = new TableSchema.TableColumn(schema);
				colvarFamilyStoreTel.ColumnName = "family_store_tel";
				colvarFamilyStoreTel.DataType = DbType.String;
				colvarFamilyStoreTel.MaxLength = 50;
				colvarFamilyStoreTel.AutoIncrement = false;
				colvarFamilyStoreTel.IsNullable = true;
				colvarFamilyStoreTel.IsPrimaryKey = false;
				colvarFamilyStoreTel.IsForeignKey = false;
				colvarFamilyStoreTel.IsReadOnly = false;
				colvarFamilyStoreTel.DefaultSetting = @"";
				colvarFamilyStoreTel.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFamilyStoreTel);
				
				TableSchema.TableColumn colvarFamilyStoreAddr = new TableSchema.TableColumn(schema);
				colvarFamilyStoreAddr.ColumnName = "family_store_addr";
				colvarFamilyStoreAddr.DataType = DbType.String;
				colvarFamilyStoreAddr.MaxLength = 200;
				colvarFamilyStoreAddr.AutoIncrement = false;
				colvarFamilyStoreAddr.IsNullable = true;
				colvarFamilyStoreAddr.IsPrimaryKey = false;
				colvarFamilyStoreAddr.IsForeignKey = false;
				colvarFamilyStoreAddr.IsReadOnly = false;
				colvarFamilyStoreAddr.DefaultSetting = @"";
				colvarFamilyStoreAddr.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFamilyStoreAddr);
				
				TableSchema.TableColumn colvarSevenStoreId = new TableSchema.TableColumn(schema);
				colvarSevenStoreId.ColumnName = "seven_store_id";
				colvarSevenStoreId.DataType = DbType.String;
				colvarSevenStoreId.MaxLength = 10;
				colvarSevenStoreId.AutoIncrement = false;
				colvarSevenStoreId.IsNullable = true;
				colvarSevenStoreId.IsPrimaryKey = false;
				colvarSevenStoreId.IsForeignKey = false;
				colvarSevenStoreId.IsReadOnly = false;
				colvarSevenStoreId.DefaultSetting = @"";
				colvarSevenStoreId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSevenStoreId);
				
				TableSchema.TableColumn colvarSevenStoreName = new TableSchema.TableColumn(schema);
				colvarSevenStoreName.ColumnName = "seven_store_name";
				colvarSevenStoreName.DataType = DbType.String;
				colvarSevenStoreName.MaxLength = 50;
				colvarSevenStoreName.AutoIncrement = false;
				colvarSevenStoreName.IsNullable = true;
				colvarSevenStoreName.IsPrimaryKey = false;
				colvarSevenStoreName.IsForeignKey = false;
				colvarSevenStoreName.IsReadOnly = false;
				colvarSevenStoreName.DefaultSetting = @"";
				colvarSevenStoreName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSevenStoreName);
				
				TableSchema.TableColumn colvarSevenStoreTel = new TableSchema.TableColumn(schema);
				colvarSevenStoreTel.ColumnName = "seven_store_tel";
				colvarSevenStoreTel.DataType = DbType.String;
				colvarSevenStoreTel.MaxLength = 50;
				colvarSevenStoreTel.AutoIncrement = false;
				colvarSevenStoreTel.IsNullable = true;
				colvarSevenStoreTel.IsPrimaryKey = false;
				colvarSevenStoreTel.IsForeignKey = false;
				colvarSevenStoreTel.IsReadOnly = false;
				colvarSevenStoreTel.DefaultSetting = @"";
				colvarSevenStoreTel.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSevenStoreTel);
				
				TableSchema.TableColumn colvarSevenStoreAddr = new TableSchema.TableColumn(schema);
				colvarSevenStoreAddr.ColumnName = "seven_store_addr";
				colvarSevenStoreAddr.DataType = DbType.String;
				colvarSevenStoreAddr.MaxLength = 200;
				colvarSevenStoreAddr.AutoIncrement = false;
				colvarSevenStoreAddr.IsNullable = true;
				colvarSevenStoreAddr.IsPrimaryKey = false;
				colvarSevenStoreAddr.IsForeignKey = false;
				colvarSevenStoreAddr.IsReadOnly = false;
				colvarSevenStoreAddr.DefaultSetting = @"";
				colvarSevenStoreAddr.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSevenStoreAddr);
				
				TableSchema.TableColumn colvarLastProductDeliveryType = new TableSchema.TableColumn(schema);
				colvarLastProductDeliveryType.ColumnName = "last_product_delivery_type";
				colvarLastProductDeliveryType.DataType = DbType.Int32;
				colvarLastProductDeliveryType.MaxLength = 0;
				colvarLastProductDeliveryType.AutoIncrement = false;
				colvarLastProductDeliveryType.IsNullable = false;
				colvarLastProductDeliveryType.IsPrimaryKey = false;
				colvarLastProductDeliveryType.IsForeignKey = false;
				colvarLastProductDeliveryType.IsReadOnly = false;
				colvarLastProductDeliveryType.DefaultSetting = @"";
				colvarLastProductDeliveryType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarLastProductDeliveryType);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
                colvarCreateTime.DefaultSetting = @"(getdate())";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
				colvarModifyTime.ColumnName = "modify_time";
				colvarModifyTime.DataType = DbType.DateTime;
				colvarModifyTime.MaxLength = 0;
				colvarModifyTime.AutoIncrement = false;
				colvarModifyTime.IsNullable = true;
				colvarModifyTime.IsPrimaryKey = false;
				colvarModifyTime.IsForeignKey = false;
				colvarModifyTime.IsReadOnly = false;
				colvarModifyTime.DefaultSetting = @"";
				colvarModifyTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyTime);
				
				TableSchema.TableColumn colvarDeliveryId = new TableSchema.TableColumn(schema);
				colvarDeliveryId.ColumnName = "delivery_id";
				colvarDeliveryId.DataType = DbType.Int32;
				colvarDeliveryId.MaxLength = 0;
				colvarDeliveryId.AutoIncrement = false;
				colvarDeliveryId.IsNullable = true;
				colvarDeliveryId.IsPrimaryKey = false;
				colvarDeliveryId.IsForeignKey = false;
				colvarDeliveryId.IsReadOnly = false;
				colvarDeliveryId.DefaultSetting = @"";
				colvarDeliveryId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDeliveryId);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("member_checkout_info",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("UserId")]
		[Bindable(true)]
		public int UserId 
		{
			get { return GetColumnValue<int>(Columns.UserId); }
			set { SetColumnValue(Columns.UserId, value); }
		}
		  
		[XmlAttribute("FamilyStoreId")]
		[Bindable(true)]
		public string FamilyStoreId 
		{
			get { return GetColumnValue<string>(Columns.FamilyStoreId); }
			set { SetColumnValue(Columns.FamilyStoreId, value); }
		}
		  
		[XmlAttribute("FamilyStoreName")]
		[Bindable(true)]
		public string FamilyStoreName 
		{
			get { return GetColumnValue<string>(Columns.FamilyStoreName); }
			set { SetColumnValue(Columns.FamilyStoreName, value); }
		}
		  
		[XmlAttribute("FamilyStoreTel")]
		[Bindable(true)]
		public string FamilyStoreTel 
		{
			get { return GetColumnValue<string>(Columns.FamilyStoreTel); }
			set { SetColumnValue(Columns.FamilyStoreTel, value); }
		}
		  
		[XmlAttribute("FamilyStoreAddr")]
		[Bindable(true)]
		public string FamilyStoreAddr 
		{
			get { return GetColumnValue<string>(Columns.FamilyStoreAddr); }
			set { SetColumnValue(Columns.FamilyStoreAddr, value); }
		}
		  
		[XmlAttribute("SevenStoreId")]
		[Bindable(true)]
		public string SevenStoreId 
		{
			get { return GetColumnValue<string>(Columns.SevenStoreId); }
			set { SetColumnValue(Columns.SevenStoreId, value); }
		}
		  
		[XmlAttribute("SevenStoreName")]
		[Bindable(true)]
		public string SevenStoreName 
		{
			get { return GetColumnValue<string>(Columns.SevenStoreName); }
			set { SetColumnValue(Columns.SevenStoreName, value); }
		}
		  
		[XmlAttribute("SevenStoreTel")]
		[Bindable(true)]
		public string SevenStoreTel 
		{
			get { return GetColumnValue<string>(Columns.SevenStoreTel); }
			set { SetColumnValue(Columns.SevenStoreTel, value); }
		}
		  
		[XmlAttribute("SevenStoreAddr")]
		[Bindable(true)]
		public string SevenStoreAddr 
		{
			get { return GetColumnValue<string>(Columns.SevenStoreAddr); }
			set { SetColumnValue(Columns.SevenStoreAddr, value); }
		}
		  
		[XmlAttribute("LastProductDeliveryType")]
		[Bindable(true)]
		public int LastProductDeliveryType 
		{
			get { return GetColumnValue<int>(Columns.LastProductDeliveryType); }
			set { SetColumnValue(Columns.LastProductDeliveryType, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		  
		[XmlAttribute("ModifyTime")]
		[Bindable(true)]
		public DateTime? ModifyTime 
		{
			get { return GetColumnValue<DateTime?>(Columns.ModifyTime); }
			set { SetColumnValue(Columns.ModifyTime, value); }
		}
		  
		[XmlAttribute("DeliveryId")]
		[Bindable(true)]
		public int? DeliveryId 
		{
			get { return GetColumnValue<int?>(Columns.DeliveryId); }
			set { SetColumnValue(Columns.DeliveryId, value); }
		}
		
		#endregion
		
        #region Typed Columns
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        public static TableSchema.TableColumn UserIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        public static TableSchema.TableColumn FamilyStoreIdColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        public static TableSchema.TableColumn FamilyStoreNameColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        public static TableSchema.TableColumn FamilyStoreTelColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        public static TableSchema.TableColumn FamilyStoreAddrColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        public static TableSchema.TableColumn SevenStoreIdColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        public static TableSchema.TableColumn SevenStoreNameColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        public static TableSchema.TableColumn SevenStoreTelColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        public static TableSchema.TableColumn SevenStoreAddrColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        public static TableSchema.TableColumn LastProductDeliveryTypeColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[11]; }
        }
        
        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[12]; }
        }
        
        public static TableSchema.TableColumn DeliveryIdColumn
        {
            get { return Schema.Columns[13]; }
        }
        
        #endregion

		#region Columns Struct

		public struct Columns
		{
			 public static string Id = @"id";
			 public static string UserId = @"user_id";
			 public static string FamilyStoreId = @"family_store_id";
			 public static string FamilyStoreName = @"family_store_name";
			 public static string FamilyStoreTel = @"family_store_tel";
			 public static string FamilyStoreAddr = @"family_store_addr";
			 public static string SevenStoreId = @"seven_store_id";
			 public static string SevenStoreName = @"seven_store_name";
			 public static string SevenStoreTel = @"seven_store_tel";
			 public static string SevenStoreAddr = @"seven_store_addr";
			 public static string LastProductDeliveryType = @"last_product_delivery_type";
			 public static string CreateTime = @"create_time";
			 public static string ModifyTime = @"modify_time";
			 public static string DeliveryId = @"delivery_id";
		}
		
        #endregion

	}
}

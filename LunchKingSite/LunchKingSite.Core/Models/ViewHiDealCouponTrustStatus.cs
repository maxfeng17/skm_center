using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the ViewHiDealCouponTrustStatus class.
    /// </summary>
    [Serializable]
    public partial class ViewHiDealCouponTrustStatusCollection : ReadOnlyList<ViewHiDealCouponTrustStatus, ViewHiDealCouponTrustStatusCollection>
    {        
        public ViewHiDealCouponTrustStatusCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_hi_deal_coupon_trust_status view.
    /// </summary>
    [Serializable]
    public partial class ViewHiDealCouponTrustStatus : ReadOnlyRecord<ViewHiDealCouponTrustStatus>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_hi_deal_coupon_trust_status", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int64;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = false;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = false;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                
                schema.Columns.Add(colvarId);
                
                TableSchema.TableColumn colvarProductId = new TableSchema.TableColumn(schema);
                colvarProductId.ColumnName = "product_id";
                colvarProductId.DataType = DbType.Int32;
                colvarProductId.MaxLength = 0;
                colvarProductId.AutoIncrement = false;
                colvarProductId.IsNullable = false;
                colvarProductId.IsPrimaryKey = false;
                colvarProductId.IsForeignKey = false;
                colvarProductId.IsReadOnly = false;
                
                schema.Columns.Add(colvarProductId);
                
                TableSchema.TableColumn colvarDealId = new TableSchema.TableColumn(schema);
                colvarDealId.ColumnName = "deal_id";
                colvarDealId.DataType = DbType.Int32;
                colvarDealId.MaxLength = 0;
                colvarDealId.AutoIncrement = false;
                colvarDealId.IsNullable = false;
                colvarDealId.IsPrimaryKey = false;
                colvarDealId.IsForeignKey = false;
                colvarDealId.IsReadOnly = false;
                
                schema.Columns.Add(colvarDealId);
                
                TableSchema.TableColumn colvarStoreGuid = new TableSchema.TableColumn(schema);
                colvarStoreGuid.ColumnName = "store_guid";
                colvarStoreGuid.DataType = DbType.Guid;
                colvarStoreGuid.MaxLength = 0;
                colvarStoreGuid.AutoIncrement = false;
                colvarStoreGuid.IsNullable = true;
                colvarStoreGuid.IsPrimaryKey = false;
                colvarStoreGuid.IsForeignKey = false;
                colvarStoreGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarStoreGuid);
                
                TableSchema.TableColumn colvarOrderPk = new TableSchema.TableColumn(schema);
                colvarOrderPk.ColumnName = "order_pk";
                colvarOrderPk.DataType = DbType.Int32;
                colvarOrderPk.MaxLength = 0;
                colvarOrderPk.AutoIncrement = false;
                colvarOrderPk.IsNullable = true;
                colvarOrderPk.IsPrimaryKey = false;
                colvarOrderPk.IsForeignKey = false;
                colvarOrderPk.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderPk);
                
                TableSchema.TableColumn colvarOrderDetailGuid = new TableSchema.TableColumn(schema);
                colvarOrderDetailGuid.ColumnName = "order_detail_guid";
                colvarOrderDetailGuid.DataType = DbType.Guid;
                colvarOrderDetailGuid.MaxLength = 0;
                colvarOrderDetailGuid.AutoIncrement = false;
                colvarOrderDetailGuid.IsNullable = true;
                colvarOrderDetailGuid.IsPrimaryKey = false;
                colvarOrderDetailGuid.IsForeignKey = false;
                colvarOrderDetailGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarOrderDetailGuid);
                
                TableSchema.TableColumn colvarPrefix = new TableSchema.TableColumn(schema);
                colvarPrefix.ColumnName = "prefix";
                colvarPrefix.DataType = DbType.AnsiString;
                colvarPrefix.MaxLength = 10;
                colvarPrefix.AutoIncrement = false;
                colvarPrefix.IsNullable = true;
                colvarPrefix.IsPrimaryKey = false;
                colvarPrefix.IsForeignKey = false;
                colvarPrefix.IsReadOnly = false;
                
                schema.Columns.Add(colvarPrefix);
                
                TableSchema.TableColumn colvarSequence = new TableSchema.TableColumn(schema);
                colvarSequence.ColumnName = "sequence";
                colvarSequence.DataType = DbType.AnsiString;
                colvarSequence.MaxLength = 20;
                colvarSequence.AutoIncrement = false;
                colvarSequence.IsNullable = false;
                colvarSequence.IsPrimaryKey = false;
                colvarSequence.IsForeignKey = false;
                colvarSequence.IsReadOnly = false;
                
                schema.Columns.Add(colvarSequence);
                
                TableSchema.TableColumn colvarCode = new TableSchema.TableColumn(schema);
                colvarCode.ColumnName = "code";
                colvarCode.DataType = DbType.AnsiString;
                colvarCode.MaxLength = 10;
                colvarCode.AutoIncrement = false;
                colvarCode.IsNullable = false;
                colvarCode.IsPrimaryKey = false;
                colvarCode.IsForeignKey = false;
                colvarCode.IsReadOnly = false;
                
                schema.Columns.Add(colvarCode);
                
                TableSchema.TableColumn colvarUserName = new TableSchema.TableColumn(schema);
                colvarUserName.ColumnName = "user_name";
                colvarUserName.DataType = DbType.String;
                colvarUserName.MaxLength = 256;
                colvarUserName.AutoIncrement = false;
                colvarUserName.IsNullable = false;
                colvarUserName.IsPrimaryKey = false;
                colvarUserName.IsForeignKey = false;
                colvarUserName.IsReadOnly = false;
                
                schema.Columns.Add(colvarUserName);
                
                TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
                colvarStatus.ColumnName = "status";
                colvarStatus.DataType = DbType.Int32;
                colvarStatus.MaxLength = 0;
                colvarStatus.AutoIncrement = false;
                colvarStatus.IsNullable = false;
                colvarStatus.IsPrimaryKey = false;
                colvarStatus.IsForeignKey = false;
                colvarStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarStatus);
                
                TableSchema.TableColumn colvarCreateDate = new TableSchema.TableColumn(schema);
                colvarCreateDate.ColumnName = "create_date";
                colvarCreateDate.DataType = DbType.DateTime;
                colvarCreateDate.MaxLength = 0;
                colvarCreateDate.AutoIncrement = false;
                colvarCreateDate.IsNullable = true;
                colvarCreateDate.IsPrimaryKey = false;
                colvarCreateDate.IsForeignKey = false;
                colvarCreateDate.IsReadOnly = false;
                
                schema.Columns.Add(colvarCreateDate);
                
                TableSchema.TableColumn colvarBoughtDate = new TableSchema.TableColumn(schema);
                colvarBoughtDate.ColumnName = "bought_date";
                colvarBoughtDate.DataType = DbType.DateTime;
                colvarBoughtDate.MaxLength = 0;
                colvarBoughtDate.AutoIncrement = false;
                colvarBoughtDate.IsNullable = true;
                colvarBoughtDate.IsPrimaryKey = false;
                colvarBoughtDate.IsForeignKey = false;
                colvarBoughtDate.IsReadOnly = false;
                
                schema.Columns.Add(colvarBoughtDate);
                
                TableSchema.TableColumn colvarUsedDate = new TableSchema.TableColumn(schema);
                colvarUsedDate.ColumnName = "used_date";
                colvarUsedDate.DataType = DbType.DateTime;
                colvarUsedDate.MaxLength = 0;
                colvarUsedDate.AutoIncrement = false;
                colvarUsedDate.IsNullable = true;
                colvarUsedDate.IsPrimaryKey = false;
                colvarUsedDate.IsForeignKey = false;
                colvarUsedDate.IsReadOnly = false;
                
                schema.Columns.Add(colvarUsedDate);
                
                TableSchema.TableColumn colvarRefundDate = new TableSchema.TableColumn(schema);
                colvarRefundDate.ColumnName = "refund_date";
                colvarRefundDate.DataType = DbType.DateTime;
                colvarRefundDate.MaxLength = 0;
                colvarRefundDate.AutoIncrement = false;
                colvarRefundDate.IsNullable = true;
                colvarRefundDate.IsPrimaryKey = false;
                colvarRefundDate.IsForeignKey = false;
                colvarRefundDate.IsReadOnly = false;
                
                schema.Columns.Add(colvarRefundDate);
                
                TableSchema.TableColumn colvarCost = new TableSchema.TableColumn(schema);
                colvarCost.ColumnName = "cost";
                colvarCost.DataType = DbType.Currency;
                colvarCost.MaxLength = 0;
                colvarCost.AutoIncrement = false;
                colvarCost.IsNullable = false;
                colvarCost.IsPrimaryKey = false;
                colvarCost.IsForeignKey = false;
                colvarCost.IsReadOnly = false;
                
                schema.Columns.Add(colvarCost);
                
                TableSchema.TableColumn colvarLogSataus = new TableSchema.TableColumn(schema);
                colvarLogSataus.ColumnName = "logSataus";
                colvarLogSataus.DataType = DbType.Int32;
                colvarLogSataus.MaxLength = 0;
                colvarLogSataus.AutoIncrement = false;
                colvarLogSataus.IsNullable = true;
                colvarLogSataus.IsPrimaryKey = false;
                colvarLogSataus.IsForeignKey = false;
                colvarLogSataus.IsReadOnly = false;
                
                schema.Columns.Add(colvarLogSataus);
                
                TableSchema.TableColumn colvarTrustId = new TableSchema.TableColumn(schema);
                colvarTrustId.ColumnName = "trust_id";
                colvarTrustId.DataType = DbType.Guid;
                colvarTrustId.MaxLength = 0;
                colvarTrustId.AutoIncrement = false;
                colvarTrustId.IsNullable = true;
                colvarTrustId.IsPrimaryKey = false;
                colvarTrustId.IsForeignKey = false;
                colvarTrustId.IsReadOnly = false;
                
                schema.Columns.Add(colvarTrustId);
                
                TableSchema.TableColumn colvarIsReservationLock = new TableSchema.TableColumn(schema);
                colvarIsReservationLock.ColumnName = "is_reservation_lock";
                colvarIsReservationLock.DataType = DbType.Boolean;
                colvarIsReservationLock.MaxLength = 0;
                colvarIsReservationLock.AutoIncrement = false;
                colvarIsReservationLock.IsNullable = false;
                colvarIsReservationLock.IsPrimaryKey = false;
                colvarIsReservationLock.IsForeignKey = false;
                colvarIsReservationLock.IsReadOnly = false;
                
                schema.Columns.Add(colvarIsReservationLock);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_hi_deal_coupon_trust_status",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewHiDealCouponTrustStatus()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewHiDealCouponTrustStatus(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewHiDealCouponTrustStatus(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewHiDealCouponTrustStatus(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("Id")]
        [Bindable(true)]
        public long Id 
	    {
		    get
		    {
			    return GetColumnValue<long>("id");
		    }
            set 
		    {
			    SetColumnValue("id", value);
            }
        }
	      
        [XmlAttribute("ProductId")]
        [Bindable(true)]
        public int ProductId 
	    {
		    get
		    {
			    return GetColumnValue<int>("product_id");
		    }
            set 
		    {
			    SetColumnValue("product_id", value);
            }
        }
	      
        [XmlAttribute("DealId")]
        [Bindable(true)]
        public int DealId 
	    {
		    get
		    {
			    return GetColumnValue<int>("deal_id");
		    }
            set 
		    {
			    SetColumnValue("deal_id", value);
            }
        }
	      
        [XmlAttribute("StoreGuid")]
        [Bindable(true)]
        public Guid? StoreGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid?>("store_guid");
		    }
            set 
		    {
			    SetColumnValue("store_guid", value);
            }
        }
	      
        [XmlAttribute("OrderPk")]
        [Bindable(true)]
        public int? OrderPk 
	    {
		    get
		    {
			    return GetColumnValue<int?>("order_pk");
		    }
            set 
		    {
			    SetColumnValue("order_pk", value);
            }
        }
	      
        [XmlAttribute("OrderDetailGuid")]
        [Bindable(true)]
        public Guid? OrderDetailGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid?>("order_detail_guid");
		    }
            set 
		    {
			    SetColumnValue("order_detail_guid", value);
            }
        }
	      
        [XmlAttribute("Prefix")]
        [Bindable(true)]
        public string Prefix 
	    {
		    get
		    {
			    return GetColumnValue<string>("prefix");
		    }
            set 
		    {
			    SetColumnValue("prefix", value);
            }
        }
	      
        [XmlAttribute("Sequence")]
        [Bindable(true)]
        public string Sequence 
	    {
		    get
		    {
			    return GetColumnValue<string>("sequence");
		    }
            set 
		    {
			    SetColumnValue("sequence", value);
            }
        }
	      
        [XmlAttribute("Code")]
        [Bindable(true)]
        public string Code 
	    {
		    get
		    {
			    return GetColumnValue<string>("code");
		    }
            set 
		    {
			    SetColumnValue("code", value);
            }
        }
	      
        [XmlAttribute("UserName")]
        [Bindable(true)]
        public string UserName 
	    {
		    get
		    {
			    return GetColumnValue<string>("user_name");
		    }
            set 
		    {
			    SetColumnValue("user_name", value);
            }
        }
	      
        [XmlAttribute("Status")]
        [Bindable(true)]
        public int Status 
	    {
		    get
		    {
			    return GetColumnValue<int>("status");
		    }
            set 
		    {
			    SetColumnValue("status", value);
            }
        }
	      
        [XmlAttribute("CreateDate")]
        [Bindable(true)]
        public DateTime? CreateDate 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("create_date");
		    }
            set 
		    {
			    SetColumnValue("create_date", value);
            }
        }
	      
        [XmlAttribute("BoughtDate")]
        [Bindable(true)]
        public DateTime? BoughtDate 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("bought_date");
		    }
            set 
		    {
			    SetColumnValue("bought_date", value);
            }
        }
	      
        [XmlAttribute("UsedDate")]
        [Bindable(true)]
        public DateTime? UsedDate 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("used_date");
		    }
            set 
		    {
			    SetColumnValue("used_date", value);
            }
        }
	      
        [XmlAttribute("RefundDate")]
        [Bindable(true)]
        public DateTime? RefundDate 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("refund_date");
		    }
            set 
		    {
			    SetColumnValue("refund_date", value);
            }
        }
	      
        [XmlAttribute("Cost")]
        [Bindable(true)]
        public decimal Cost 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("cost");
		    }
            set 
		    {
			    SetColumnValue("cost", value);
            }
        }
	      
        [XmlAttribute("LogSataus")]
        [Bindable(true)]
        public int? LogSataus 
	    {
		    get
		    {
			    return GetColumnValue<int?>("logSataus");
		    }
            set 
		    {
			    SetColumnValue("logSataus", value);
            }
        }
	      
        [XmlAttribute("TrustId")]
        [Bindable(true)]
        public Guid? TrustId 
	    {
		    get
		    {
			    return GetColumnValue<Guid?>("trust_id");
		    }
            set 
		    {
			    SetColumnValue("trust_id", value);
            }
        }
	      
        [XmlAttribute("IsReservationLock")]
        [Bindable(true)]
        public bool IsReservationLock 
	    {
		    get
		    {
			    return GetColumnValue<bool>("is_reservation_lock");
		    }
            set 
		    {
			    SetColumnValue("is_reservation_lock", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string Id = @"id";
            
            public static string ProductId = @"product_id";
            
            public static string DealId = @"deal_id";
            
            public static string StoreGuid = @"store_guid";
            
            public static string OrderPk = @"order_pk";
            
            public static string OrderDetailGuid = @"order_detail_guid";
            
            public static string Prefix = @"prefix";
            
            public static string Sequence = @"sequence";
            
            public static string Code = @"code";
            
            public static string UserName = @"user_name";
            
            public static string Status = @"status";
            
            public static string CreateDate = @"create_date";
            
            public static string BoughtDate = @"bought_date";
            
            public static string UsedDate = @"used_date";
            
            public static string RefundDate = @"refund_date";
            
            public static string Cost = @"cost";
            
            public static string LogSataus = @"logSataus";
            
            public static string TrustId = @"trust_id";
            
            public static string IsReservationLock = @"is_reservation_lock";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) 
        {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) 
        {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

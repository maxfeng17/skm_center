using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the CtAtmP1log class.
	/// </summary>
    [Serializable]
	public partial class CtAtmP1logCollection : RepositoryList<CtAtmP1log, CtAtmP1logCollection>
	{	   
		public CtAtmP1logCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>CtAtmP1logCollection</returns>
		public CtAtmP1logCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                CtAtmP1log o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the ct_atm_p1log table.
	/// </summary>
	[Serializable]
	public partial class CtAtmP1log : RepositoryRecord<CtAtmP1log>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public CtAtmP1log()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public CtAtmP1log(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("ct_atm_p1log", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarSi = new TableSchema.TableColumn(schema);
				colvarSi.ColumnName = "si";
				colvarSi.DataType = DbType.Int32;
				colvarSi.MaxLength = 0;
				colvarSi.AutoIncrement = true;
				colvarSi.IsNullable = false;
				colvarSi.IsPrimaryKey = true;
				colvarSi.IsForeignKey = false;
				colvarSi.IsReadOnly = false;
				colvarSi.DefaultSetting = @"";
				colvarSi.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSi);
				
				TableSchema.TableColumn colvarUserName = new TableSchema.TableColumn(schema);
				colvarUserName.ColumnName = "user_name";
				colvarUserName.DataType = DbType.AnsiString;
				colvarUserName.MaxLength = 250;
				colvarUserName.AutoIncrement = false;
				colvarUserName.IsNullable = false;
				colvarUserName.IsPrimaryKey = false;
				colvarUserName.IsForeignKey = false;
				colvarUserName.IsReadOnly = false;
				colvarUserName.DefaultSetting = @"";
				colvarUserName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUserName);
				
				TableSchema.TableColumn colvarBody = new TableSchema.TableColumn(schema);
				colvarBody.ColumnName = "body";
				colvarBody.DataType = DbType.String;
				colvarBody.MaxLength = 1073741823;
				colvarBody.AutoIncrement = false;
				colvarBody.IsNullable = false;
				colvarBody.IsPrimaryKey = false;
				colvarBody.IsForeignKey = false;
				colvarBody.IsReadOnly = false;
				colvarBody.DefaultSetting = @"";
				colvarBody.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBody);
				
				TableSchema.TableColumn colvarAmount = new TableSchema.TableColumn(schema);
				colvarAmount.ColumnName = "amount";
				colvarAmount.DataType = DbType.Int32;
				colvarAmount.MaxLength = 0;
				colvarAmount.AutoIncrement = false;
				colvarAmount.IsNullable = true;
				colvarAmount.IsPrimaryKey = false;
				colvarAmount.IsForeignKey = false;
				colvarAmount.IsReadOnly = false;
				colvarAmount.DefaultSetting = @"";
				colvarAmount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAmount);
				
				TableSchema.TableColumn colvarCount = new TableSchema.TableColumn(schema);
				colvarCount.ColumnName = "count";
				colvarCount.DataType = DbType.Int32;
				colvarCount.MaxLength = 0;
				colvarCount.AutoIncrement = false;
				colvarCount.IsNullable = true;
				colvarCount.IsPrimaryKey = false;
				colvarCount.IsForeignKey = false;
				colvarCount.IsReadOnly = false;
				colvarCount.DefaultSetting = @"";
				colvarCount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCount);
				
				TableSchema.TableColumn colvarTargetDate = new TableSchema.TableColumn(schema);
				colvarTargetDate.ColumnName = "target_date";
				colvarTargetDate.DataType = DbType.DateTime;
				colvarTargetDate.MaxLength = 0;
				colvarTargetDate.AutoIncrement = false;
				colvarTargetDate.IsNullable = false;
				colvarTargetDate.IsPrimaryKey = false;
				colvarTargetDate.IsForeignKey = false;
				colvarTargetDate.IsReadOnly = false;
				colvarTargetDate.DefaultSetting = @"";
				colvarTargetDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTargetDate);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarRefundlist = new TableSchema.TableColumn(schema);
				colvarRefundlist.ColumnName = "refundlist";
				colvarRefundlist.DataType = DbType.AnsiString;
				colvarRefundlist.MaxLength = 2147483647;
				colvarRefundlist.AutoIncrement = false;
				colvarRefundlist.IsNullable = true;
				colvarRefundlist.IsPrimaryKey = false;
				colvarRefundlist.IsForeignKey = false;
				colvarRefundlist.IsReadOnly = false;
				colvarRefundlist.DefaultSetting = @"";
				colvarRefundlist.ForeignKeyTableName = "";
				schema.Columns.Add(colvarRefundlist);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("ct_atm_p1log",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Si")]
		[Bindable(true)]
		public int Si 
		{
			get { return GetColumnValue<int>(Columns.Si); }
			set { SetColumnValue(Columns.Si, value); }
		}
		  
		[XmlAttribute("UserName")]
		[Bindable(true)]
		public string UserName 
		{
			get { return GetColumnValue<string>(Columns.UserName); }
			set { SetColumnValue(Columns.UserName, value); }
		}
		  
		[XmlAttribute("Body")]
		[Bindable(true)]
		public string Body 
		{
			get { return GetColumnValue<string>(Columns.Body); }
			set { SetColumnValue(Columns.Body, value); }
		}
		  
		[XmlAttribute("Amount")]
		[Bindable(true)]
		public int? Amount 
		{
			get { return GetColumnValue<int?>(Columns.Amount); }
			set { SetColumnValue(Columns.Amount, value); }
		}
		  
		[XmlAttribute("Count")]
		[Bindable(true)]
		public int? Count 
		{
			get { return GetColumnValue<int?>(Columns.Count); }
			set { SetColumnValue(Columns.Count, value); }
		}
		  
		[XmlAttribute("TargetDate")]
		[Bindable(true)]
		public DateTime TargetDate 
		{
			get { return GetColumnValue<DateTime>(Columns.TargetDate); }
			set { SetColumnValue(Columns.TargetDate, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		  
		[XmlAttribute("Refundlist")]
		[Bindable(true)]
		public string Refundlist 
		{
			get { return GetColumnValue<string>(Columns.Refundlist); }
			set { SetColumnValue(Columns.Refundlist, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn SiColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn UserNameColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn BodyColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn AmountColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn CountColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn TargetDateColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn RefundlistColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Si = @"si";
			 public static string UserName = @"user_name";
			 public static string Body = @"body";
			 public static string Amount = @"amount";
			 public static string Count = @"count";
			 public static string TargetDate = @"target_date";
			 public static string CreateTime = @"create_time";
			 public static string Refundlist = @"refundlist";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

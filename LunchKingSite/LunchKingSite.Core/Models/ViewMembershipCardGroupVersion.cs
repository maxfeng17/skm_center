using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewMembershipCardGroupVersion class.
    /// </summary>
    [Serializable]
    public partial class ViewMembershipCardGroupVersionCollection : ReadOnlyList<ViewMembershipCardGroupVersion, ViewMembershipCardGroupVersionCollection>
    {        
        public ViewMembershipCardGroupVersionCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_membership_card_group_version view.
    /// </summary>
    [Serializable]
    public partial class ViewMembershipCardGroupVersion : ReadOnlyRecord<ViewMembershipCardGroupVersion>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_membership_card_group_version", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = false;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = false;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                
                schema.Columns.Add(colvarId);
                
                TableSchema.TableColumn colvarCardGroupId = new TableSchema.TableColumn(schema);
                colvarCardGroupId.ColumnName = "card_group_id";
                colvarCardGroupId.DataType = DbType.Int32;
                colvarCardGroupId.MaxLength = 0;
                colvarCardGroupId.AutoIncrement = false;
                colvarCardGroupId.IsNullable = false;
                colvarCardGroupId.IsPrimaryKey = false;
                colvarCardGroupId.IsForeignKey = false;
                colvarCardGroupId.IsReadOnly = false;
                
                schema.Columns.Add(colvarCardGroupId);
                
                TableSchema.TableColumn colvarStatus = new TableSchema.TableColumn(schema);
                colvarStatus.ColumnName = "status";
                colvarStatus.DataType = DbType.Int32;
                colvarStatus.MaxLength = 0;
                colvarStatus.AutoIncrement = false;
                colvarStatus.IsNullable = false;
                colvarStatus.IsPrimaryKey = false;
                colvarStatus.IsForeignKey = false;
                colvarStatus.IsReadOnly = false;
                
                schema.Columns.Add(colvarStatus);
                
                TableSchema.TableColumn colvarOpenTime = new TableSchema.TableColumn(schema);
                colvarOpenTime.ColumnName = "open_time";
                colvarOpenTime.DataType = DbType.DateTime;
                colvarOpenTime.MaxLength = 0;
                colvarOpenTime.AutoIncrement = false;
                colvarOpenTime.IsNullable = false;
                colvarOpenTime.IsPrimaryKey = false;
                colvarOpenTime.IsForeignKey = false;
                colvarOpenTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarOpenTime);
                
                TableSchema.TableColumn colvarCloseTime = new TableSchema.TableColumn(schema);
                colvarCloseTime.ColumnName = "close_time";
                colvarCloseTime.DataType = DbType.DateTime;
                colvarCloseTime.MaxLength = 0;
                colvarCloseTime.AutoIncrement = false;
                colvarCloseTime.IsNullable = false;
                colvarCloseTime.IsPrimaryKey = false;
                colvarCloseTime.IsForeignKey = false;
                colvarCloseTime.IsReadOnly = false;
                
                schema.Columns.Add(colvarCloseTime);
                
                TableSchema.TableColumn colvarImagePath = new TableSchema.TableColumn(schema);
                colvarImagePath.ColumnName = "image_path";
                colvarImagePath.DataType = DbType.String;
                colvarImagePath.MaxLength = 250;
                colvarImagePath.AutoIncrement = false;
                colvarImagePath.IsNullable = true;
                colvarImagePath.IsPrimaryKey = false;
                colvarImagePath.IsForeignKey = false;
                colvarImagePath.IsReadOnly = false;
                
                schema.Columns.Add(colvarImagePath);
                
                TableSchema.TableColumn colvarBackgroundColor = new TableSchema.TableColumn(schema);
                colvarBackgroundColor.ColumnName = "background_color";
                colvarBackgroundColor.DataType = DbType.String;
                colvarBackgroundColor.MaxLength = 9;
                colvarBackgroundColor.AutoIncrement = false;
                colvarBackgroundColor.IsNullable = true;
                colvarBackgroundColor.IsPrimaryKey = false;
                colvarBackgroundColor.IsForeignKey = false;
                colvarBackgroundColor.IsReadOnly = false;
                
                schema.Columns.Add(colvarBackgroundColor);
                
                TableSchema.TableColumn colvarBackgroundImage = new TableSchema.TableColumn(schema);
                colvarBackgroundImage.ColumnName = "background_image";
                colvarBackgroundImage.DataType = DbType.String;
                colvarBackgroundImage.MaxLength = 250;
                colvarBackgroundImage.AutoIncrement = false;
                colvarBackgroundImage.IsNullable = true;
                colvarBackgroundImage.IsPrimaryKey = false;
                colvarBackgroundImage.IsForeignKey = false;
                colvarBackgroundImage.IsReadOnly = false;
                
                schema.Columns.Add(colvarBackgroundImage);
                
                TableSchema.TableColumn colvarIconImage = new TableSchema.TableColumn(schema);
                colvarIconImage.ColumnName = "icon_image";
                colvarIconImage.DataType = DbType.String;
                colvarIconImage.MaxLength = 250;
                colvarIconImage.AutoIncrement = false;
                colvarIconImage.IsNullable = true;
                colvarIconImage.IsPrimaryKey = false;
                colvarIconImage.IsForeignKey = false;
                colvarIconImage.IsReadOnly = false;
                
                schema.Columns.Add(colvarIconImage);
                
                TableSchema.TableColumn colvarSellerUserId = new TableSchema.TableColumn(schema);
                colvarSellerUserId.ColumnName = "seller_user_id";
                colvarSellerUserId.DataType = DbType.Int32;
                colvarSellerUserId.MaxLength = 0;
                colvarSellerUserId.AutoIncrement = false;
                colvarSellerUserId.IsNullable = false;
                colvarSellerUserId.IsPrimaryKey = false;
                colvarSellerUserId.IsForeignKey = false;
                colvarSellerUserId.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerUserId);
                
                TableSchema.TableColumn colvarName = new TableSchema.TableColumn(schema);
                colvarName.ColumnName = "name";
                colvarName.DataType = DbType.String;
                colvarName.MaxLength = 100;
                colvarName.AutoIncrement = false;
                colvarName.IsNullable = false;
                colvarName.IsPrimaryKey = false;
                colvarName.IsForeignKey = false;
                colvarName.IsReadOnly = false;
                
                schema.Columns.Add(colvarName);
                
                TableSchema.TableColumn colvarBackgroundColorId = new TableSchema.TableColumn(schema);
                colvarBackgroundColorId.ColumnName = "background_color_id";
                colvarBackgroundColorId.DataType = DbType.Int32;
                colvarBackgroundColorId.MaxLength = 0;
                colvarBackgroundColorId.AutoIncrement = false;
                colvarBackgroundColorId.IsNullable = false;
                colvarBackgroundColorId.IsPrimaryKey = false;
                colvarBackgroundColorId.IsForeignKey = false;
                colvarBackgroundColorId.IsReadOnly = false;
                
                schema.Columns.Add(colvarBackgroundColorId);
                
                TableSchema.TableColumn colvarBackgroundImageId = new TableSchema.TableColumn(schema);
                colvarBackgroundImageId.ColumnName = "background_image_id";
                colvarBackgroundImageId.DataType = DbType.Int32;
                colvarBackgroundImageId.MaxLength = 0;
                colvarBackgroundImageId.AutoIncrement = false;
                colvarBackgroundImageId.IsNullable = false;
                colvarBackgroundImageId.IsPrimaryKey = false;
                colvarBackgroundImageId.IsForeignKey = false;
                colvarBackgroundImageId.IsReadOnly = false;
                
                schema.Columns.Add(colvarBackgroundImageId);
                
                TableSchema.TableColumn colvarIconImageId = new TableSchema.TableColumn(schema);
                colvarIconImageId.ColumnName = "icon_image_id";
                colvarIconImageId.DataType = DbType.Int32;
                colvarIconImageId.MaxLength = 0;
                colvarIconImageId.AutoIncrement = false;
                colvarIconImageId.IsNullable = false;
                colvarIconImageId.IsPrimaryKey = false;
                colvarIconImageId.IsForeignKey = false;
                colvarIconImageId.IsReadOnly = false;
                
                schema.Columns.Add(colvarIconImageId);
                
                TableSchema.TableColumn colvarSellerGuid = new TableSchema.TableColumn(schema);
                colvarSellerGuid.ColumnName = "seller_guid";
                colvarSellerGuid.DataType = DbType.Guid;
                colvarSellerGuid.MaxLength = 0;
                colvarSellerGuid.AutoIncrement = false;
                colvarSellerGuid.IsNullable = false;
                colvarSellerGuid.IsPrimaryKey = false;
                colvarSellerGuid.IsForeignKey = false;
                colvarSellerGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerGuid);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_membership_card_group_version",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewMembershipCardGroupVersion()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewMembershipCardGroupVersion(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewMembershipCardGroupVersion(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewMembershipCardGroupVersion(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id 
	    {
		    get
		    {
			    return GetColumnValue<int>("id");
		    }
            set 
		    {
			    SetColumnValue("id", value);
            }
        }
	      
        [XmlAttribute("CardGroupId")]
        [Bindable(true)]
        public int CardGroupId 
	    {
		    get
		    {
			    return GetColumnValue<int>("card_group_id");
		    }
            set 
		    {
			    SetColumnValue("card_group_id", value);
            }
        }
	      
        [XmlAttribute("Status")]
        [Bindable(true)]
        public int Status 
	    {
		    get
		    {
			    return GetColumnValue<int>("status");
		    }
            set 
		    {
			    SetColumnValue("status", value);
            }
        }
	      
        [XmlAttribute("OpenTime")]
        [Bindable(true)]
        public DateTime OpenTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("open_time");
		    }
            set 
		    {
			    SetColumnValue("open_time", value);
            }
        }
	      
        [XmlAttribute("CloseTime")]
        [Bindable(true)]
        public DateTime CloseTime 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("close_time");
		    }
            set 
		    {
			    SetColumnValue("close_time", value);
            }
        }
	      
        [XmlAttribute("ImagePath")]
        [Bindable(true)]
        public string ImagePath 
	    {
		    get
		    {
			    return GetColumnValue<string>("image_path");
		    }
            set 
		    {
			    SetColumnValue("image_path", value);
            }
        }
	      
        [XmlAttribute("BackgroundColor")]
        [Bindable(true)]
        public string BackgroundColor 
	    {
		    get
		    {
			    return GetColumnValue<string>("background_color");
		    }
            set 
		    {
			    SetColumnValue("background_color", value);
            }
        }
	      
        [XmlAttribute("BackgroundImage")]
        [Bindable(true)]
        public string BackgroundImage 
	    {
		    get
		    {
			    return GetColumnValue<string>("background_image");
		    }
            set 
		    {
			    SetColumnValue("background_image", value);
            }
        }
	      
        [XmlAttribute("IconImage")]
        [Bindable(true)]
        public string IconImage 
	    {
		    get
		    {
			    return GetColumnValue<string>("icon_image");
		    }
            set 
		    {
			    SetColumnValue("icon_image", value);
            }
        }
	      
        [XmlAttribute("SellerUserId")]
        [Bindable(true)]
        public int SellerUserId 
	    {
		    get
		    {
			    return GetColumnValue<int>("seller_user_id");
		    }
            set 
		    {
			    SetColumnValue("seller_user_id", value);
            }
        }
	      
        [XmlAttribute("Name")]
        [Bindable(true)]
        public string Name 
	    {
		    get
		    {
			    return GetColumnValue<string>("name");
		    }
            set 
		    {
			    SetColumnValue("name", value);
            }
        }
	      
        [XmlAttribute("BackgroundColorId")]
        [Bindable(true)]
        public int BackgroundColorId 
	    {
		    get
		    {
			    return GetColumnValue<int>("background_color_id");
		    }
            set 
		    {
			    SetColumnValue("background_color_id", value);
            }
        }
	      
        [XmlAttribute("BackgroundImageId")]
        [Bindable(true)]
        public int BackgroundImageId 
	    {
		    get
		    {
			    return GetColumnValue<int>("background_image_id");
		    }
            set 
		    {
			    SetColumnValue("background_image_id", value);
            }
        }
	      
        [XmlAttribute("IconImageId")]
        [Bindable(true)]
        public int IconImageId 
	    {
		    get
		    {
			    return GetColumnValue<int>("icon_image_id");
		    }
            set 
		    {
			    SetColumnValue("icon_image_id", value);
            }
        }
	      
        [XmlAttribute("SellerGuid")]
        [Bindable(true)]
        public Guid SellerGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("seller_guid");
		    }
            set 
		    {
			    SetColumnValue("seller_guid", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string Id = @"id";
            
            public static string CardGroupId = @"card_group_id";
            
            public static string Status = @"status";
            
            public static string OpenTime = @"open_time";
            
            public static string CloseTime = @"close_time";
            
            public static string ImagePath = @"image_path";
            
            public static string BackgroundColor = @"background_color";
            
            public static string BackgroundImage = @"background_image";
            
            public static string IconImage = @"icon_image";
            
            public static string SellerUserId = @"seller_user_id";
            
            public static string Name = @"name";
            
            public static string BackgroundColorId = @"background_color_id";
            
            public static string BackgroundImageId = @"background_image_id";
            
            public static string IconImageId = @"icon_image_id";
            
            public static string SellerGuid = @"seller_guid";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

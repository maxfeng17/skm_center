using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the BookingSystemReservation class.
	/// </summary>
    [Serializable]
	public partial class BookingSystemReservationCollection : RepositoryList<BookingSystemReservation, BookingSystemReservationCollection>
	{	   
		public BookingSystemReservationCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>BookingSystemReservationCollection</returns>
		public BookingSystemReservationCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                BookingSystemReservation o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the booking_system_reservation table.
	/// </summary>
	[Serializable]
	public partial class BookingSystemReservation : RepositoryRecord<BookingSystemReservation>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public BookingSystemReservation()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public BookingSystemReservation(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("booking_system_reservation", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarTimeSlotId = new TableSchema.TableColumn(schema);
				colvarTimeSlotId.ColumnName = "time_slot_id";
				colvarTimeSlotId.DataType = DbType.Int32;
				colvarTimeSlotId.MaxLength = 0;
				colvarTimeSlotId.AutoIncrement = false;
				colvarTimeSlotId.IsNullable = false;
				colvarTimeSlotId.IsPrimaryKey = false;
				colvarTimeSlotId.IsForeignKey = false;
				colvarTimeSlotId.IsReadOnly = false;
				colvarTimeSlotId.DefaultSetting = @"";
				colvarTimeSlotId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTimeSlotId);
				
				TableSchema.TableColumn colvarReservationDate = new TableSchema.TableColumn(schema);
				colvarReservationDate.ColumnName = "reservation_date";
				colvarReservationDate.DataType = DbType.DateTime;
				colvarReservationDate.MaxLength = 0;
				colvarReservationDate.AutoIncrement = false;
				colvarReservationDate.IsNullable = false;
				colvarReservationDate.IsPrimaryKey = false;
				colvarReservationDate.IsForeignKey = false;
				colvarReservationDate.IsReadOnly = false;
				colvarReservationDate.DefaultSetting = @"";
				colvarReservationDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReservationDate);
				
				TableSchema.TableColumn colvarNumberOfPeople = new TableSchema.TableColumn(schema);
				colvarNumberOfPeople.ColumnName = "number_of_people";
				colvarNumberOfPeople.DataType = DbType.Int32;
				colvarNumberOfPeople.MaxLength = 0;
				colvarNumberOfPeople.AutoIncrement = false;
				colvarNumberOfPeople.IsNullable = false;
				colvarNumberOfPeople.IsPrimaryKey = false;
				colvarNumberOfPeople.IsForeignKey = false;
				colvarNumberOfPeople.IsReadOnly = false;
				colvarNumberOfPeople.DefaultSetting = @"";
				colvarNumberOfPeople.ForeignKeyTableName = "";
				schema.Columns.Add(colvarNumberOfPeople);
				
				TableSchema.TableColumn colvarRemark = new TableSchema.TableColumn(schema);
				colvarRemark.ColumnName = "remark";
				colvarRemark.DataType = DbType.String;
				colvarRemark.MaxLength = 256;
				colvarRemark.AutoIncrement = false;
				colvarRemark.IsNullable = false;
				colvarRemark.IsPrimaryKey = false;
				colvarRemark.IsForeignKey = false;
				colvarRemark.IsReadOnly = false;
				colvarRemark.DefaultSetting = @"";
				colvarRemark.ForeignKeyTableName = "";
				schema.Columns.Add(colvarRemark);
				
				TableSchema.TableColumn colvarContactName = new TableSchema.TableColumn(schema);
				colvarContactName.ColumnName = "contact_name";
				colvarContactName.DataType = DbType.String;
				colvarContactName.MaxLength = 30;
				colvarContactName.AutoIncrement = false;
				colvarContactName.IsNullable = false;
				colvarContactName.IsPrimaryKey = false;
				colvarContactName.IsForeignKey = false;
				colvarContactName.IsReadOnly = false;
				colvarContactName.DefaultSetting = @"";
				colvarContactName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarContactName);
				
				TableSchema.TableColumn colvarContactGender = new TableSchema.TableColumn(schema);
				colvarContactGender.ColumnName = "contact_Gender";
				colvarContactGender.DataType = DbType.Boolean;
				colvarContactGender.MaxLength = 0;
				colvarContactGender.AutoIncrement = false;
				colvarContactGender.IsNullable = false;
				colvarContactGender.IsPrimaryKey = false;
				colvarContactGender.IsForeignKey = false;
				colvarContactGender.IsReadOnly = false;
				
						colvarContactGender.DefaultSetting = @"((0))";
				colvarContactGender.ForeignKeyTableName = "";
				schema.Columns.Add(colvarContactGender);
				
				TableSchema.TableColumn colvarContactNumber = new TableSchema.TableColumn(schema);
				colvarContactNumber.ColumnName = "contact_number";
				colvarContactNumber.DataType = DbType.AnsiString;
				colvarContactNumber.MaxLength = 12;
				colvarContactNumber.AutoIncrement = false;
				colvarContactNumber.IsNullable = false;
				colvarContactNumber.IsPrimaryKey = false;
				colvarContactNumber.IsForeignKey = false;
				colvarContactNumber.IsReadOnly = false;
				colvarContactNumber.DefaultSetting = @"";
				colvarContactNumber.ForeignKeyTableName = "";
				schema.Columns.Add(colvarContactNumber);
				
				TableSchema.TableColumn colvarCreateDatetime = new TableSchema.TableColumn(schema);
				colvarCreateDatetime.ColumnName = "create_datetime";
				colvarCreateDatetime.DataType = DbType.DateTime;
				colvarCreateDatetime.MaxLength = 0;
				colvarCreateDatetime.AutoIncrement = false;
				colvarCreateDatetime.IsNullable = false;
				colvarCreateDatetime.IsPrimaryKey = false;
				colvarCreateDatetime.IsForeignKey = false;
				colvarCreateDatetime.IsReadOnly = false;
				
						colvarCreateDatetime.DefaultSetting = @"(getdate())";
				colvarCreateDatetime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateDatetime);
				
				TableSchema.TableColumn colvarIsCheckin = new TableSchema.TableColumn(schema);
				colvarIsCheckin.ColumnName = "is_checkin";
				colvarIsCheckin.DataType = DbType.Boolean;
				colvarIsCheckin.MaxLength = 0;
				colvarIsCheckin.AutoIncrement = false;
				colvarIsCheckin.IsNullable = false;
				colvarIsCheckin.IsPrimaryKey = false;
				colvarIsCheckin.IsForeignKey = false;
				colvarIsCheckin.IsReadOnly = false;
				
						colvarIsCheckin.DefaultSetting = @"((0))";
				colvarIsCheckin.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsCheckin);
				
				TableSchema.TableColumn colvarIsCancel = new TableSchema.TableColumn(schema);
				colvarIsCancel.ColumnName = "is_cancel";
				colvarIsCancel.DataType = DbType.Boolean;
				colvarIsCancel.MaxLength = 0;
				colvarIsCancel.AutoIncrement = false;
				colvarIsCancel.IsNullable = false;
				colvarIsCancel.IsPrimaryKey = false;
				colvarIsCancel.IsForeignKey = false;
				colvarIsCancel.IsReadOnly = false;
				
						colvarIsCancel.DefaultSetting = @"((0))";
				colvarIsCancel.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsCancel);
				
				TableSchema.TableColumn colvarOrderKey = new TableSchema.TableColumn(schema);
				colvarOrderKey.ColumnName = "order_key";
				colvarOrderKey.DataType = DbType.AnsiString;
				colvarOrderKey.MaxLength = 50;
				colvarOrderKey.AutoIncrement = false;
				colvarOrderKey.IsNullable = true;
				colvarOrderKey.IsPrimaryKey = false;
				colvarOrderKey.IsForeignKey = false;
				colvarOrderKey.IsReadOnly = false;
				colvarOrderKey.DefaultSetting = @"";
				colvarOrderKey.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderKey);
				
				TableSchema.TableColumn colvarMemberKey = new TableSchema.TableColumn(schema);
				colvarMemberKey.ColumnName = "member_key";
				colvarMemberKey.DataType = DbType.AnsiString;
				colvarMemberKey.MaxLength = 50;
				colvarMemberKey.AutoIncrement = false;
				colvarMemberKey.IsNullable = true;
				colvarMemberKey.IsPrimaryKey = false;
				colvarMemberKey.IsForeignKey = false;
				colvarMemberKey.IsReadOnly = false;
				colvarMemberKey.DefaultSetting = @"";
				colvarMemberKey.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMemberKey);
				
				TableSchema.TableColumn colvarApiId = new TableSchema.TableColumn(schema);
				colvarApiId.ColumnName = "api_id";
				colvarApiId.DataType = DbType.Int32;
				colvarApiId.MaxLength = 0;
				colvarApiId.AutoIncrement = false;
				colvarApiId.IsNullable = true;
				colvarApiId.IsPrimaryKey = false;
				colvarApiId.IsForeignKey = false;
				colvarApiId.IsReadOnly = false;
				colvarApiId.DefaultSetting = @"";
				colvarApiId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarApiId);
				
				TableSchema.TableColumn colvarCouponInfo = new TableSchema.TableColumn(schema);
				colvarCouponInfo.ColumnName = "coupon_info";
				colvarCouponInfo.DataType = DbType.String;
				colvarCouponInfo.MaxLength = 256;
				colvarCouponInfo.AutoIncrement = false;
				colvarCouponInfo.IsNullable = true;
				colvarCouponInfo.IsPrimaryKey = false;
				colvarCouponInfo.IsForeignKey = false;
				colvarCouponInfo.IsReadOnly = false;
				colvarCouponInfo.DefaultSetting = @"";
				colvarCouponInfo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCouponInfo);
				
				TableSchema.TableColumn colvarCouponLink = new TableSchema.TableColumn(schema);
				colvarCouponLink.ColumnName = "coupon_link";
				colvarCouponLink.DataType = DbType.AnsiString;
				colvarCouponLink.MaxLength = 128;
				colvarCouponLink.AutoIncrement = false;
				colvarCouponLink.IsNullable = true;
				colvarCouponLink.IsPrimaryKey = false;
				colvarCouponLink.IsForeignKey = false;
				colvarCouponLink.IsReadOnly = false;
				colvarCouponLink.DefaultSetting = @"";
				colvarCouponLink.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCouponLink);
				
				TableSchema.TableColumn colvarCancelDate = new TableSchema.TableColumn(schema);
				colvarCancelDate.ColumnName = "cancel_date";
				colvarCancelDate.DataType = DbType.DateTime;
				colvarCancelDate.MaxLength = 0;
				colvarCancelDate.AutoIncrement = false;
				colvarCancelDate.IsNullable = true;
				colvarCancelDate.IsPrimaryKey = false;
				colvarCancelDate.IsForeignKey = false;
				colvarCancelDate.IsReadOnly = false;
				colvarCancelDate.DefaultSetting = @"";
				colvarCancelDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCancelDate);
				
				TableSchema.TableColumn colvarIsLock = new TableSchema.TableColumn(schema);
				colvarIsLock.ColumnName = "is_lock";
				colvarIsLock.DataType = DbType.Boolean;
				colvarIsLock.MaxLength = 0;
				colvarIsLock.AutoIncrement = false;
				colvarIsLock.IsNullable = false;
				colvarIsLock.IsPrimaryKey = false;
				colvarIsLock.IsForeignKey = false;
				colvarIsLock.IsReadOnly = false;
				
						colvarIsLock.DefaultSetting = @"((0))";
				colvarIsLock.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsLock);
				
				TableSchema.TableColumn colvarContactEmail = new TableSchema.TableColumn(schema);
				colvarContactEmail.ColumnName = "contact_email";
				colvarContactEmail.DataType = DbType.String;
				colvarContactEmail.MaxLength = 256;
				colvarContactEmail.AutoIncrement = false;
				colvarContactEmail.IsNullable = false;
				colvarContactEmail.IsPrimaryKey = false;
				colvarContactEmail.IsForeignKey = false;
				colvarContactEmail.IsReadOnly = false;
				
						colvarContactEmail.DefaultSetting = @"('')";
				colvarContactEmail.ForeignKeyTableName = "";
				schema.Columns.Add(colvarContactEmail);
				
				TableSchema.TableColumn colvarCancelId = new TableSchema.TableColumn(schema);
				colvarCancelId.ColumnName = "cancel_id";
				colvarCancelId.DataType = DbType.String;
				colvarCancelId.MaxLength = 50;
				colvarCancelId.AutoIncrement = false;
				colvarCancelId.IsNullable = true;
				colvarCancelId.IsPrimaryKey = false;
				colvarCancelId.IsForeignKey = false;
				colvarCancelId.IsReadOnly = false;
				colvarCancelId.DefaultSetting = @"";
				colvarCancelId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCancelId);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("booking_system_reservation",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("TimeSlotId")]
		[Bindable(true)]
		public int TimeSlotId 
		{
			get { return GetColumnValue<int>(Columns.TimeSlotId); }
			set { SetColumnValue(Columns.TimeSlotId, value); }
		}
		  
		[XmlAttribute("ReservationDate")]
		[Bindable(true)]
		public DateTime ReservationDate 
		{
			get { return GetColumnValue<DateTime>(Columns.ReservationDate); }
			set { SetColumnValue(Columns.ReservationDate, value); }
		}
		  
		[XmlAttribute("NumberOfPeople")]
		[Bindable(true)]
		public int NumberOfPeople 
		{
			get { return GetColumnValue<int>(Columns.NumberOfPeople); }
			set { SetColumnValue(Columns.NumberOfPeople, value); }
		}
		  
		[XmlAttribute("Remark")]
		[Bindable(true)]
		public string Remark 
		{
			get { return GetColumnValue<string>(Columns.Remark); }
			set { SetColumnValue(Columns.Remark, value); }
		}
		  
		[XmlAttribute("ContactName")]
		[Bindable(true)]
		public string ContactName 
		{
			get { return GetColumnValue<string>(Columns.ContactName); }
			set { SetColumnValue(Columns.ContactName, value); }
		}
		  
		[XmlAttribute("ContactGender")]
		[Bindable(true)]
		public bool ContactGender 
		{
			get { return GetColumnValue<bool>(Columns.ContactGender); }
			set { SetColumnValue(Columns.ContactGender, value); }
		}
		  
		[XmlAttribute("ContactNumber")]
		[Bindable(true)]
		public string ContactNumber 
		{
			get { return GetColumnValue<string>(Columns.ContactNumber); }
			set { SetColumnValue(Columns.ContactNumber, value); }
		}
		  
		[XmlAttribute("CreateDatetime")]
		[Bindable(true)]
		public DateTime CreateDatetime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateDatetime); }
			set { SetColumnValue(Columns.CreateDatetime, value); }
		}
		  
		[XmlAttribute("IsCheckin")]
		[Bindable(true)]
		public bool IsCheckin 
		{
			get { return GetColumnValue<bool>(Columns.IsCheckin); }
			set { SetColumnValue(Columns.IsCheckin, value); }
		}
		  
		[XmlAttribute("IsCancel")]
		[Bindable(true)]
		public bool IsCancel 
		{
			get { return GetColumnValue<bool>(Columns.IsCancel); }
			set { SetColumnValue(Columns.IsCancel, value); }
		}
		  
		[XmlAttribute("OrderKey")]
		[Bindable(true)]
		public string OrderKey 
		{
			get { return GetColumnValue<string>(Columns.OrderKey); }
			set { SetColumnValue(Columns.OrderKey, value); }
		}
		  
		[XmlAttribute("MemberKey")]
		[Bindable(true)]
		public string MemberKey 
		{
			get { return GetColumnValue<string>(Columns.MemberKey); }
			set { SetColumnValue(Columns.MemberKey, value); }
		}
		  
		[XmlAttribute("ApiId")]
		[Bindable(true)]
		public int? ApiId 
		{
			get { return GetColumnValue<int?>(Columns.ApiId); }
			set { SetColumnValue(Columns.ApiId, value); }
		}
		  
		[XmlAttribute("CouponInfo")]
		[Bindable(true)]
		public string CouponInfo 
		{
			get { return GetColumnValue<string>(Columns.CouponInfo); }
			set { SetColumnValue(Columns.CouponInfo, value); }
		}
		  
		[XmlAttribute("CouponLink")]
		[Bindable(true)]
		public string CouponLink 
		{
			get { return GetColumnValue<string>(Columns.CouponLink); }
			set { SetColumnValue(Columns.CouponLink, value); }
		}
		  
		[XmlAttribute("CancelDate")]
		[Bindable(true)]
		public DateTime? CancelDate 
		{
			get { return GetColumnValue<DateTime?>(Columns.CancelDate); }
			set { SetColumnValue(Columns.CancelDate, value); }
		}
		  
		[XmlAttribute("IsLock")]
		[Bindable(true)]
		public bool IsLock 
		{
			get { return GetColumnValue<bool>(Columns.IsLock); }
			set { SetColumnValue(Columns.IsLock, value); }
		}
		  
		[XmlAttribute("ContactEmail")]
		[Bindable(true)]
		public string ContactEmail 
		{
			get { return GetColumnValue<string>(Columns.ContactEmail); }
			set { SetColumnValue(Columns.ContactEmail, value); }
		}
		  
		[XmlAttribute("CancelId")]
		[Bindable(true)]
		public string CancelId 
		{
			get { return GetColumnValue<string>(Columns.CancelId); }
			set { SetColumnValue(Columns.CancelId, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn TimeSlotIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn ReservationDateColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn NumberOfPeopleColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn RemarkColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn ContactNameColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn ContactGenderColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn ContactNumberColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateDatetimeColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn IsCheckinColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn IsCancelColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        public static TableSchema.TableColumn OrderKeyColumn
        {
            get { return Schema.Columns[11]; }
        }
        
        
        
        public static TableSchema.TableColumn MemberKeyColumn
        {
            get { return Schema.Columns[12]; }
        }
        
        
        
        public static TableSchema.TableColumn ApiIdColumn
        {
            get { return Schema.Columns[13]; }
        }
        
        
        
        public static TableSchema.TableColumn CouponInfoColumn
        {
            get { return Schema.Columns[14]; }
        }
        
        
        
        public static TableSchema.TableColumn CouponLinkColumn
        {
            get { return Schema.Columns[15]; }
        }
        
        
        
        public static TableSchema.TableColumn CancelDateColumn
        {
            get { return Schema.Columns[16]; }
        }
        
        
        
        public static TableSchema.TableColumn IsLockColumn
        {
            get { return Schema.Columns[17]; }
        }
        
        
        
        public static TableSchema.TableColumn ContactEmailColumn
        {
            get { return Schema.Columns[18]; }
        }
        
        
        
        public static TableSchema.TableColumn CancelIdColumn
        {
            get { return Schema.Columns[19]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string TimeSlotId = @"time_slot_id";
			 public static string ReservationDate = @"reservation_date";
			 public static string NumberOfPeople = @"number_of_people";
			 public static string Remark = @"remark";
			 public static string ContactName = @"contact_name";
			 public static string ContactGender = @"contact_Gender";
			 public static string ContactNumber = @"contact_number";
			 public static string CreateDatetime = @"create_datetime";
			 public static string IsCheckin = @"is_checkin";
			 public static string IsCancel = @"is_cancel";
			 public static string OrderKey = @"order_key";
			 public static string MemberKey = @"member_key";
			 public static string ApiId = @"api_id";
			 public static string CouponInfo = @"coupon_info";
			 public static string CouponLink = @"coupon_link";
			 public static string CancelDate = @"cancel_date";
			 public static string IsLock = @"is_lock";
			 public static string ContactEmail = @"contact_email";
			 public static string CancelId = @"cancel_id";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

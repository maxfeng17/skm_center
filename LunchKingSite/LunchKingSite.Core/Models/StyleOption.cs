using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the StyleOption class.
	/// </summary>
    [Serializable]
	public partial class StyleOptionCollection : RepositoryList<StyleOption, StyleOptionCollection>
	{	   
		public StyleOptionCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>StyleOptionCollection</returns>
		public StyleOptionCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                StyleOption o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the style_option table.
	/// </summary>
	[Serializable]
	public partial class StyleOption : RepositoryRecord<StyleOption>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public StyleOption()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public StyleOption(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("style_option", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarOptionType = new TableSchema.TableColumn(schema);
				colvarOptionType.ColumnName = "option_type";
				colvarOptionType.DataType = DbType.Int32;
				colvarOptionType.MaxLength = 0;
				colvarOptionType.AutoIncrement = false;
				colvarOptionType.IsNullable = false;
				colvarOptionType.IsPrimaryKey = true;
				colvarOptionType.IsForeignKey = false;
				colvarOptionType.IsReadOnly = false;
				colvarOptionType.DefaultSetting = @"";
				colvarOptionType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOptionType);
				
				TableSchema.TableColumn colvarOptionId = new TableSchema.TableColumn(schema);
				colvarOptionId.ColumnName = "option_id";
				colvarOptionId.DataType = DbType.Int32;
				colvarOptionId.MaxLength = 0;
				colvarOptionId.AutoIncrement = false;
				colvarOptionId.IsNullable = false;
				colvarOptionId.IsPrimaryKey = true;
				colvarOptionId.IsForeignKey = false;
				colvarOptionId.IsReadOnly = false;
				colvarOptionId.DefaultSetting = @"";
				colvarOptionId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOptionId);
				
				TableSchema.TableColumn colvarOptionValue = new TableSchema.TableColumn(schema);
				colvarOptionValue.ColumnName = "option_value";
				colvarOptionValue.DataType = DbType.String;
				colvarOptionValue.MaxLength = -1;
				colvarOptionValue.AutoIncrement = false;
				colvarOptionValue.IsNullable = false;
				colvarOptionValue.IsPrimaryKey = false;
				colvarOptionValue.IsForeignKey = false;
				colvarOptionValue.IsReadOnly = false;
				colvarOptionValue.DefaultSetting = @"";
				colvarOptionValue.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOptionValue);
				
				TableSchema.TableColumn colvarSeq = new TableSchema.TableColumn(schema);
				colvarSeq.ColumnName = "seq";
				colvarSeq.DataType = DbType.Int32;
				colvarSeq.MaxLength = 0;
				colvarSeq.AutoIncrement = false;
				colvarSeq.IsNullable = false;
				colvarSeq.IsPrimaryKey = false;
				colvarSeq.IsForeignKey = false;
				colvarSeq.IsReadOnly = false;
				colvarSeq.DefaultSetting = @"";
				colvarSeq.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSeq);
				
				TableSchema.TableColumn colvarEnabled = new TableSchema.TableColumn(schema);
				colvarEnabled.ColumnName = "enabled";
				colvarEnabled.DataType = DbType.Boolean;
				colvarEnabled.MaxLength = 0;
				colvarEnabled.AutoIncrement = false;
				colvarEnabled.IsNullable = false;
				colvarEnabled.IsPrimaryKey = false;
				colvarEnabled.IsForeignKey = false;
				colvarEnabled.IsReadOnly = false;
				colvarEnabled.DefaultSetting = @"";
				colvarEnabled.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEnabled);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("style_option",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("OptionType")]
		[Bindable(true)]
		public int OptionType 
		{
			get { return GetColumnValue<int>(Columns.OptionType); }
			set { SetColumnValue(Columns.OptionType, value); }
		}
		  
		[XmlAttribute("OptionId")]
		[Bindable(true)]
		public int OptionId 
		{
			get { return GetColumnValue<int>(Columns.OptionId); }
			set { SetColumnValue(Columns.OptionId, value); }
		}
		  
		[XmlAttribute("OptionValue")]
		[Bindable(true)]
		public string OptionValue 
		{
			get { return GetColumnValue<string>(Columns.OptionValue); }
			set { SetColumnValue(Columns.OptionValue, value); }
		}
		  
		[XmlAttribute("Seq")]
		[Bindable(true)]
		public int Seq 
		{
			get { return GetColumnValue<int>(Columns.Seq); }
			set { SetColumnValue(Columns.Seq, value); }
		}
		  
		[XmlAttribute("Enabled")]
		[Bindable(true)]
		public bool Enabled 
		{
			get { return GetColumnValue<bool>(Columns.Enabled); }
			set { SetColumnValue(Columns.Enabled, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn OptionTypeColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn OptionIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn OptionValueColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn SeqColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn EnabledColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string OptionType = @"option_type";
			 public static string OptionId = @"option_id";
			 public static string OptionValue = @"option_value";
			 public static string Seq = @"seq";
			 public static string Enabled = @"enabled";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

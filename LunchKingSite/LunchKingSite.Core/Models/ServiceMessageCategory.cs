using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the ServiceMessageCategory class.
	/// </summary>
    [Serializable]
	public partial class ServiceMessageCategoryCollection : RepositoryList<ServiceMessageCategory, ServiceMessageCategoryCollection>
	{	   
		public ServiceMessageCategoryCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>ServiceMessageCategoryCollection</returns>
		public ServiceMessageCategoryCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                ServiceMessageCategory o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the service_message_category table.
	/// </summary>
	[Serializable]
	public partial class ServiceMessageCategory : RepositoryRecord<ServiceMessageCategory>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public ServiceMessageCategory()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public ServiceMessageCategory(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("service_message_category", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarCategoryId = new TableSchema.TableColumn(schema);
				colvarCategoryId.ColumnName = "category_id";
				colvarCategoryId.DataType = DbType.Int32;
				colvarCategoryId.MaxLength = 0;
				colvarCategoryId.AutoIncrement = true;
				colvarCategoryId.IsNullable = false;
				colvarCategoryId.IsPrimaryKey = true;
				colvarCategoryId.IsForeignKey = false;
				colvarCategoryId.IsReadOnly = false;
				colvarCategoryId.DefaultSetting = @"";
				colvarCategoryId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCategoryId);
				
				TableSchema.TableColumn colvarParentCategoryId = new TableSchema.TableColumn(schema);
				colvarParentCategoryId.ColumnName = "parent_category_id";
				colvarParentCategoryId.DataType = DbType.Int32;
				colvarParentCategoryId.MaxLength = 0;
				colvarParentCategoryId.AutoIncrement = false;
				colvarParentCategoryId.IsNullable = true;
				colvarParentCategoryId.IsPrimaryKey = false;
				colvarParentCategoryId.IsForeignKey = false;
				colvarParentCategoryId.IsReadOnly = false;
				colvarParentCategoryId.DefaultSetting = @"";
				colvarParentCategoryId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarParentCategoryId);
				
				TableSchema.TableColumn colvarCategoryName = new TableSchema.TableColumn(schema);
				colvarCategoryName.ColumnName = "category_name";
				colvarCategoryName.DataType = DbType.String;
				colvarCategoryName.MaxLength = 100;
				colvarCategoryName.AutoIncrement = false;
				colvarCategoryName.IsNullable = false;
				colvarCategoryName.IsPrimaryKey = false;
				colvarCategoryName.IsForeignKey = false;
				colvarCategoryName.IsReadOnly = false;
				colvarCategoryName.DefaultSetting = @"";
				colvarCategoryName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCategoryName);
				
				TableSchema.TableColumn colvarCategoryOrder = new TableSchema.TableColumn(schema);
				colvarCategoryOrder.ColumnName = "category_order";
				colvarCategoryOrder.DataType = DbType.Int32;
				colvarCategoryOrder.MaxLength = 0;
				colvarCategoryOrder.AutoIncrement = false;
				colvarCategoryOrder.IsNullable = true;
				colvarCategoryOrder.IsPrimaryKey = false;
				colvarCategoryOrder.IsForeignKey = false;
				colvarCategoryOrder.IsReadOnly = false;
				colvarCategoryOrder.DefaultSetting = @"";
				colvarCategoryOrder.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCategoryOrder);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("service_message_category",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("CategoryId")]
		[Bindable(true)]
		public int CategoryId 
		{
			get { return GetColumnValue<int>(Columns.CategoryId); }
			set { SetColumnValue(Columns.CategoryId, value); }
		}
		  
		[XmlAttribute("ParentCategoryId")]
		[Bindable(true)]
		public int? ParentCategoryId 
		{
			get { return GetColumnValue<int?>(Columns.ParentCategoryId); }
			set { SetColumnValue(Columns.ParentCategoryId, value); }
		}
		  
		[XmlAttribute("CategoryName")]
		[Bindable(true)]
		public string CategoryName 
		{
			get { return GetColumnValue<string>(Columns.CategoryName); }
			set { SetColumnValue(Columns.CategoryName, value); }
		}
		  
		[XmlAttribute("CategoryOrder")]
		[Bindable(true)]
		public int? CategoryOrder 
		{
			get { return GetColumnValue<int?>(Columns.CategoryOrder); }
			set { SetColumnValue(Columns.CategoryOrder, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn CategoryIdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn ParentCategoryIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn CategoryNameColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn CategoryOrderColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string CategoryId = @"category_id";
			 public static string ParentCategoryId = @"parent_category_id";
			 public static string CategoryName = @"category_name";
			 public static string CategoryOrder = @"category_order";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

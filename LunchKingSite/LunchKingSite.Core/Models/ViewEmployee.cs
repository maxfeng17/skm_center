using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the ViewEmployee class.
    /// </summary>
    [Serializable]
    public partial class ViewEmployeeCollection : ReadOnlyList<ViewEmployee, ViewEmployeeCollection>
    {
        public ViewEmployeeCollection() { }
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_employee view.
    /// </summary>
    [Serializable]
    public partial class ViewEmployee : ReadOnlyRecord<ViewEmployee>, IReadOnlyRecord
    {

        #region Default Settings
        protected static void SetSQLProps()
        {
            GetTableSchema();
        }
        #endregion
        #region Schema Accessor
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_employee", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarEmpId = new TableSchema.TableColumn(schema);
                colvarEmpId.ColumnName = "emp_id";
                colvarEmpId.DataType = DbType.AnsiString;
                colvarEmpId.MaxLength = 50;
                colvarEmpId.AutoIncrement = false;
                colvarEmpId.IsNullable = false;
                colvarEmpId.IsPrimaryKey = false;
                colvarEmpId.IsForeignKey = false;
                colvarEmpId.IsReadOnly = false;

                schema.Columns.Add(colvarEmpId);

                TableSchema.TableColumn colvarEmpNo = new TableSchema.TableColumn(schema);
                colvarEmpNo.ColumnName = "emp_no";
                colvarEmpNo.DataType = DbType.AnsiString;
                colvarEmpNo.MaxLength = 50;
                colvarEmpNo.AutoIncrement = false;
                colvarEmpNo.IsNullable = true;
                colvarEmpNo.IsPrimaryKey = false;
                colvarEmpNo.IsForeignKey = false;
                colvarEmpNo.IsReadOnly = false;

                schema.Columns.Add(colvarEmpNo);

                TableSchema.TableColumn colvarEmpName = new TableSchema.TableColumn(schema);
                colvarEmpName.ColumnName = "emp_name";
                colvarEmpName.DataType = DbType.String;
                colvarEmpName.MaxLength = 50;
                colvarEmpName.AutoIncrement = false;
                colvarEmpName.IsNullable = true;
                colvarEmpName.IsPrimaryKey = false;
                colvarEmpName.IsForeignKey = false;
                colvarEmpName.IsReadOnly = false;

                schema.Columns.Add(colvarEmpName);

                TableSchema.TableColumn colvarDeptId = new TableSchema.TableColumn(schema);
                colvarDeptId.ColumnName = "dept_id";
                colvarDeptId.DataType = DbType.AnsiString;
                colvarDeptId.MaxLength = 50;
                colvarDeptId.AutoIncrement = false;
                colvarDeptId.IsNullable = true;
                colvarDeptId.IsPrimaryKey = false;
                colvarDeptId.IsForeignKey = false;
                colvarDeptId.IsReadOnly = false;

                schema.Columns.Add(colvarDeptId);

                TableSchema.TableColumn colvarAvailableDate = new TableSchema.TableColumn(schema);
                colvarAvailableDate.ColumnName = "available_date";
                colvarAvailableDate.DataType = DbType.DateTime;
                colvarAvailableDate.MaxLength = 0;
                colvarAvailableDate.AutoIncrement = false;
                colvarAvailableDate.IsNullable = true;
                colvarAvailableDate.IsPrimaryKey = false;
                colvarAvailableDate.IsForeignKey = false;
                colvarAvailableDate.IsReadOnly = false;

                schema.Columns.Add(colvarAvailableDate);

                TableSchema.TableColumn colvarDepartureDate = new TableSchema.TableColumn(schema);
                colvarDepartureDate.ColumnName = "departure_date";
                colvarDepartureDate.DataType = DbType.DateTime;
                colvarDepartureDate.MaxLength = 0;
                colvarDepartureDate.AutoIncrement = false;
                colvarDepartureDate.IsNullable = true;
                colvarDepartureDate.IsPrimaryKey = false;
                colvarDepartureDate.IsForeignKey = false;
                colvarDepartureDate.IsReadOnly = false;

                schema.Columns.Add(colvarDepartureDate);

                TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
                colvarCreateId.ColumnName = "create_id";
                colvarCreateId.DataType = DbType.String;
                colvarCreateId.MaxLength = 256;
                colvarCreateId.AutoIncrement = false;
                colvarCreateId.IsNullable = true;
                colvarCreateId.IsPrimaryKey = false;
                colvarCreateId.IsForeignKey = false;
                colvarCreateId.IsReadOnly = false;

                schema.Columns.Add(colvarCreateId);

                TableSchema.TableColumn colvarExtension = new TableSchema.TableColumn(schema);
                colvarExtension.ColumnName = "extension";
                colvarExtension.DataType = DbType.String;
                colvarExtension.MaxLength = 20;
                colvarExtension.AutoIncrement = false;
                colvarExtension.IsNullable = true;
                colvarExtension.IsPrimaryKey = false;
                colvarExtension.IsForeignKey = false;
                colvarExtension.IsReadOnly = false;

                schema.Columns.Add(colvarExtension);

                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = true;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;

                schema.Columns.Add(colvarCreateTime);

                TableSchema.TableColumn colvarModifyId = new TableSchema.TableColumn(schema);
                colvarModifyId.ColumnName = "modify_id";
                colvarModifyId.DataType = DbType.String;
                colvarModifyId.MaxLength = 256;
                colvarModifyId.AutoIncrement = false;
                colvarModifyId.IsNullable = true;
                colvarModifyId.IsPrimaryKey = false;
                colvarModifyId.IsForeignKey = false;
                colvarModifyId.IsReadOnly = false;

                schema.Columns.Add(colvarModifyId);

                TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
                colvarModifyTime.ColumnName = "modify_time";
                colvarModifyTime.DataType = DbType.DateTime;
                colvarModifyTime.MaxLength = 0;
                colvarModifyTime.AutoIncrement = false;
                colvarModifyTime.IsNullable = true;
                colvarModifyTime.IsPrimaryKey = false;
                colvarModifyTime.IsForeignKey = false;
                colvarModifyTime.IsReadOnly = false;

                schema.Columns.Add(colvarModifyTime);

                TableSchema.TableColumn colvarMobile = new TableSchema.TableColumn(schema);
                colvarMobile.ColumnName = "mobile";
                colvarMobile.DataType = DbType.String;
                colvarMobile.MaxLength = 50;
                colvarMobile.AutoIncrement = false;
                colvarMobile.IsNullable = true;
                colvarMobile.IsPrimaryKey = false;
                colvarMobile.IsForeignKey = false;
                colvarMobile.IsReadOnly = false;

                schema.Columns.Add(colvarMobile);

                TableSchema.TableColumn colvarIsInvisible = new TableSchema.TableColumn(schema);
                colvarIsInvisible.ColumnName = "is_invisible";
                colvarIsInvisible.DataType = DbType.Boolean;
                colvarIsInvisible.MaxLength = 0;
                colvarIsInvisible.AutoIncrement = false;
                colvarIsInvisible.IsNullable = false;
                colvarIsInvisible.IsPrimaryKey = false;
                colvarIsInvisible.IsForeignKey = false;
                colvarIsInvisible.IsReadOnly = false;

                schema.Columns.Add(colvarIsInvisible);

                TableSchema.TableColumn colvarUserId = new TableSchema.TableColumn(schema);
                colvarUserId.ColumnName = "user_id";
                colvarUserId.DataType = DbType.Int32;
                colvarUserId.MaxLength = 0;
                colvarUserId.AutoIncrement = false;
                colvarUserId.IsNullable = false;
                colvarUserId.IsPrimaryKey = false;
                colvarUserId.IsForeignKey = false;
                colvarUserId.IsReadOnly = false;

                schema.Columns.Add(colvarUserId);

                TableSchema.TableColumn colvarEmpLevel = new TableSchema.TableColumn(schema);
                colvarEmpLevel.ColumnName = "emp_level";
                colvarEmpLevel.DataType = DbType.Int32;
                colvarEmpLevel.MaxLength = 0;
                colvarEmpLevel.AutoIncrement = false;
                colvarEmpLevel.IsNullable = true;
                colvarEmpLevel.IsPrimaryKey = false;
                colvarEmpLevel.IsForeignKey = false;
                colvarEmpLevel.IsReadOnly = false;

                schema.Columns.Add(colvarEmpLevel);

                TableSchema.TableColumn colvarTeamNo = new TableSchema.TableColumn(schema);
                colvarTeamNo.ColumnName = "team_no";
                colvarTeamNo.DataType = DbType.Int32;
                colvarTeamNo.MaxLength = 0;
                colvarTeamNo.AutoIncrement = false;
                colvarTeamNo.IsNullable = true;
                colvarTeamNo.IsPrimaryKey = false;
                colvarTeamNo.IsForeignKey = false;
                colvarTeamNo.IsReadOnly = false;

                schema.Columns.Add(colvarTeamNo);

                TableSchema.TableColumn colvarTeamManager = new TableSchema.TableColumn(schema);
                colvarTeamManager.ColumnName = "team_manager";
                colvarTeamManager.DataType = DbType.Boolean;
                colvarTeamManager.MaxLength = 0;
                colvarTeamManager.AutoIncrement = false;
                colvarTeamManager.IsNullable = false;
                colvarTeamManager.IsPrimaryKey = false;
                colvarTeamManager.IsForeignKey = false;
                colvarTeamManager.IsReadOnly = false;

                schema.Columns.Add(colvarTeamManager);

                TableSchema.TableColumn colvarEmail = new TableSchema.TableColumn(schema);
                colvarEmail.ColumnName = "email";
                colvarEmail.DataType = DbType.String;
                colvarEmail.MaxLength = 256;
                colvarEmail.AutoIncrement = false;
                colvarEmail.IsNullable = false;
                colvarEmail.IsPrimaryKey = false;
                colvarEmail.IsForeignKey = false;
                colvarEmail.IsReadOnly = false;

                schema.Columns.Add(colvarEmail);

                TableSchema.TableColumn colvarDeptName = new TableSchema.TableColumn(schema);
                colvarDeptName.ColumnName = "dept_name";
                colvarDeptName.DataType = DbType.String;
                colvarDeptName.MaxLength = 100;
                colvarDeptName.AutoIncrement = false;
                colvarDeptName.IsNullable = true;
                colvarDeptName.IsPrimaryKey = false;
                colvarDeptName.IsForeignKey = false;
                colvarDeptName.IsReadOnly = false;

                schema.Columns.Add(colvarDeptName);

                TableSchema.TableColumn colvarKpiTarget = new TableSchema.TableColumn(schema);
                colvarKpiTarget.ColumnName = "KPI_TARGET";
                colvarKpiTarget.DataType = DbType.Double;
                colvarKpiTarget.MaxLength = 0;
                colvarKpiTarget.AutoIncrement = false;
                colvarKpiTarget.IsNullable = true;
                colvarKpiTarget.IsPrimaryKey = false;
                colvarKpiTarget.IsForeignKey = false;
                colvarKpiTarget.IsReadOnly = false;

                schema.Columns.Add(colvarKpiTarget);

                TableSchema.TableColumn colvarDealTarget = new TableSchema.TableColumn(schema);
                colvarDealTarget.ColumnName = "DEAL_TARGET";
                colvarDealTarget.DataType = DbType.Int32;
                colvarDealTarget.MaxLength = 0;
                colvarDealTarget.AutoIncrement = false;
                colvarDealTarget.IsNullable = true;
                colvarDealTarget.IsPrimaryKey = false;
                colvarDealTarget.IsForeignKey = false;
                colvarDealTarget.IsReadOnly = false;

                schema.Columns.Add(colvarDealTarget);

                TableSchema.TableColumn colvarDeptManager = new TableSchema.TableColumn(schema);
                colvarDeptManager.ColumnName = "dept_manager";
                colvarDeptManager.DataType = DbType.AnsiString;
                colvarDeptManager.MaxLength = 100;
                colvarDeptManager.AutoIncrement = false;
                colvarDeptManager.IsNullable = true;
                colvarDeptManager.IsPrimaryKey = false;
                colvarDeptManager.IsForeignKey = false;
                colvarDeptManager.IsReadOnly = false;

                schema.Columns.Add(colvarDeptManager);

                TableSchema.TableColumn colvarCrossDeptTeam = new TableSchema.TableColumn(schema);
                colvarCrossDeptTeam.ColumnName = "cross_dept_team";
                colvarCrossDeptTeam.DataType = DbType.AnsiString;
                colvarCrossDeptTeam.MaxLength = 100;
                colvarCrossDeptTeam.AutoIncrement = false;
                colvarCrossDeptTeam.IsNullable = true;
                colvarCrossDeptTeam.IsPrimaryKey = false;
                colvarCrossDeptTeam.IsForeignKey = false;
                colvarCrossDeptTeam.IsReadOnly = false;

                schema.Columns.Add(colvarCrossDeptTeam);

                TableSchema.TableColumn colvarIsGrpPerformance = new TableSchema.TableColumn(schema);
                colvarIsGrpPerformance.ColumnName = "is_grp_performance";
                colvarIsGrpPerformance.DataType = DbType.Int32;
                colvarIsGrpPerformance.MaxLength = 0;
                colvarIsGrpPerformance.AutoIncrement = false;
                colvarIsGrpPerformance.IsNullable = false;
                colvarIsGrpPerformance.IsPrimaryKey = false;
                colvarIsGrpPerformance.IsForeignKey = false;
                colvarIsGrpPerformance.IsReadOnly = false;

                schema.Columns.Add(colvarIsGrpPerformance);

                TableSchema.TableColumn colvarIsOfficial = new TableSchema.TableColumn(schema);
                colvarIsOfficial.ColumnName = "is_official";
                colvarIsOfficial.DataType = DbType.Int32;
                colvarIsOfficial.MaxLength = 0;
                colvarIsOfficial.AutoIncrement = false;
                colvarIsOfficial.IsNullable = false;
                colvarIsOfficial.IsPrimaryKey = false;
                colvarIsOfficial.IsForeignKey = false;
                colvarIsOfficial.IsReadOnly = false;

                schema.Columns.Add(colvarIsOfficial);

                TableSchema.TableColumn colvarName = new TableSchema.TableColumn(schema);
                colvarName.ColumnName = "name";
                colvarName.DataType = DbType.String;
                colvarName.MaxLength = 100;
                colvarName.AutoIncrement = false;
                colvarName.IsNullable = false;
                colvarName.IsPrimaryKey = false;
                colvarName.IsForeignKey = false;
                colvarName.IsReadOnly = false;

                schema.Columns.Add(colvarName);


                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_employee", schema);
            }
        }
        #endregion

        #region Query Accessor
        public static Query CreateQuery()
        {
            return new Query(Schema);
        }
        #endregion

        #region .ctors
        public ViewEmployee()
        {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewEmployee(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
            {
                ForceDefaults();
            }
            MarkNew();
        }

        public ViewEmployee(object keyID)
        {
            SetSQLProps();
            LoadByKey(keyID);
        }

        public ViewEmployee(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName, columnValue);
        }

        #endregion

        #region Props


        [XmlAttribute("EmpId")]
        [Bindable(true)]
        public string EmpId
        {
            get
            {
                return GetColumnValue<string>("emp_id");
            }
            set
            {
                SetColumnValue("emp_id", value);
            }
        }

        [XmlAttribute("EmpNo")]
        [Bindable(true)]
        public string EmpNo
        {
            get
            {
                return GetColumnValue<string>("emp_no");
            }
            set
            {
                SetColumnValue("emp_no", value);
            }
        }

        [XmlAttribute("EmpName")]
        [Bindable(true)]
        public string EmpName
        {
            get
            {
                return GetColumnValue<string>("emp_name");
            }
            set
            {
                SetColumnValue("emp_name", value);
            }
        }

        [XmlAttribute("DeptId")]
        [Bindable(true)]
        public string DeptId
        {
            get
            {
                return GetColumnValue<string>("dept_id");
            }
            set
            {
                SetColumnValue("dept_id", value);
            }
        }

        [XmlAttribute("AvailableDate")]
        [Bindable(true)]
        public DateTime? AvailableDate
        {
            get
            {
                return GetColumnValue<DateTime?>("available_date");
            }
            set
            {
                SetColumnValue("available_date", value);
            }
        }

        [XmlAttribute("DepartureDate")]
        [Bindable(true)]
        public DateTime? DepartureDate
        {
            get
            {
                return GetColumnValue<DateTime?>("departure_date");
            }
            set
            {
                SetColumnValue("departure_date", value);
            }
        }

        [XmlAttribute("CreateId")]
        [Bindable(true)]
        public string CreateId
        {
            get
            {
                return GetColumnValue<string>("create_id");
            }
            set
            {
                SetColumnValue("create_id", value);
            }
        }

        [XmlAttribute("Extension")]
        [Bindable(true)]
        public string Extension
        {
            get
            {
                return GetColumnValue<string>("extension");
            }
            set
            {
                SetColumnValue("extension", value);
            }
        }

        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime? CreateTime
        {
            get
            {
                return GetColumnValue<DateTime?>("create_time");
            }
            set
            {
                SetColumnValue("create_time", value);
            }
        }

        [XmlAttribute("ModifyId")]
        [Bindable(true)]
        public string ModifyId
        {
            get
            {
                return GetColumnValue<string>("modify_id");
            }
            set
            {
                SetColumnValue("modify_id", value);
            }
        }

        [XmlAttribute("ModifyTime")]
        [Bindable(true)]
        public DateTime? ModifyTime
        {
            get
            {
                return GetColumnValue<DateTime?>("modify_time");
            }
            set
            {
                SetColumnValue("modify_time", value);
            }
        }

        [XmlAttribute("Mobile")]
        [Bindable(true)]
        public string Mobile
        {
            get
            {
                return GetColumnValue<string>("mobile");
            }
            set
            {
                SetColumnValue("mobile", value);
            }
        }

        [XmlAttribute("IsInvisible")]
        [Bindable(true)]
        public bool IsInvisible
        {
            get
            {
                return GetColumnValue<bool>("is_invisible");
            }
            set
            {
                SetColumnValue("is_invisible", value);
            }
        }

        [XmlAttribute("UserId")]
        [Bindable(true)]
        public int UserId
        {
            get
            {
                return GetColumnValue<int>("user_id");
            }
            set
            {
                SetColumnValue("user_id", value);
            }
        }

        [XmlAttribute("EmpLevel")]
        [Bindable(true)]
        public int? EmpLevel
        {
            get
            {
                return GetColumnValue<int?>("emp_level");
            }
            set
            {
                SetColumnValue("emp_level", value);
            }
        }

        [XmlAttribute("TeamNo")]
        [Bindable(true)]
        public int? TeamNo
        {
            get
            {
                return GetColumnValue<int?>("team_no");
            }
            set
            {
                SetColumnValue("team_no", value);
            }
        }

        [XmlAttribute("TeamManager")]
        [Bindable(true)]
        public bool TeamManager
        {
            get
            {
                return GetColumnValue<bool>("team_manager");
            }
            set
            {
                SetColumnValue("team_manager", value);
            }
        }

        [XmlAttribute("Email")]
        [Bindable(true)]
        public string Email
        {
            get
            {
                return GetColumnValue<string>("email");
            }
            set
            {
                SetColumnValue("email", value);
            }
        }

        [XmlAttribute("DeptName")]
        [Bindable(true)]
        public string DeptName
        {
            get
            {
                return GetColumnValue<string>("dept_name");
            }
            set
            {
                SetColumnValue("dept_name", value);
            }
        }

        [XmlAttribute("KpiTarget")]
        [Bindable(true)]
        public double? KpiTarget
        {
            get
            {
                return GetColumnValue<double?>("KPI_TARGET");
            }
            set
            {
                SetColumnValue("KPI_TARGET", value);
            }
        }

        [XmlAttribute("DealTarget")]
        [Bindable(true)]
        public int? DealTarget
        {
            get
            {
                return GetColumnValue<int?>("DEAL_TARGET");
            }
            set
            {
                SetColumnValue("DEAL_TARGET", value);
            }
        }

        [XmlAttribute("DeptManager")]
        [Bindable(true)]
        public string DeptManager
        {
            get
            {
                return GetColumnValue<string>("dept_manager");
            }
            set
            {
                SetColumnValue("dept_manager", value);
            }
        }

        [XmlAttribute("CrossDeptTeam")]
        [Bindable(true)]
        public string CrossDeptTeam
        {
            get
            {
                return GetColumnValue<string>("cross_dept_team");
            }
            set
            {
                SetColumnValue("cross_dept_team", value);
            }
        }

        [XmlAttribute("IsGrpPerformance")]
        [Bindable(true)]
        public int IsGrpPerformance
        {
            get
            {
                return GetColumnValue<int>("is_grp_performance");
            }
            set
            {
                SetColumnValue("is_grp_performance", value);
            }
        }

        [XmlAttribute("IsOfficial")]
        [Bindable(true)]
        public int IsOfficial
        {
            get
            {
                return GetColumnValue<int>("is_official");
            }
            set
            {
                SetColumnValue("is_official", value);
            }
        }

        [XmlAttribute("Name")]
        [Bindable(true)]
        public string Name
        {
            get
            {
                return GetColumnValue<string>("name");
            }
            set
            {
                SetColumnValue("name", value);
            }
        }

        #endregion

        #region Columns Struct
        public struct Columns
        {


            public static string EmpId = @"emp_id";

            public static string EmpNo = @"emp_no";

            public static string EmpName = @"emp_name";

            public static string DeptId = @"dept_id";

            public static string AvailableDate = @"available_date";

            public static string DepartureDate = @"departure_date";

            public static string CreateId = @"create_id";

            public static string Extension = @"extension";

            public static string CreateTime = @"create_time";

            public static string ModifyId = @"modify_id";

            public static string ModifyTime = @"modify_time";

            public static string Mobile = @"mobile";

            public static string IsInvisible = @"is_invisible";

            public static string UserId = @"user_id";

            public static string EmpLevel = @"emp_level";

            public static string TeamNo = @"team_no";

            public static string TeamManager = @"team_manager";

            public static string Email = @"email";

            public static string DeptName = @"dept_name";

            public static string KpiTarget = @"KPI_TARGET";

            public static string DealTarget = @"DEAL_TARGET";

            public static string DeptManager = @"dept_manager";

            public static string CrossDeptTeam = @"cross_dept_team";

            public static string IsGrpPerformance = @"is_grp_performance";

            public static string IsOfficial = @"is_official";

            public static string Name = @"name";

        }
        #endregion


        #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName)
        {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName)
        {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion

    }
}

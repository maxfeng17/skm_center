using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the PartnerEinvoiceMain class.
	/// </summary>
	[Serializable]
	public partial class PartnerEinvoiceMainCollection : RepositoryList<PartnerEinvoiceMain, PartnerEinvoiceMainCollection>
	{
		public PartnerEinvoiceMainCollection() { }

		/// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
		/// </summary>
		/// <returns>PartnerEinvoiceMainCollection</returns>
		public PartnerEinvoiceMainCollection Filter()
		{
			for (int i = this.Count - 1; i > -1; i--)
			{
				PartnerEinvoiceMain o = this[i];
				foreach (SubSonic.Where w in this.wheres)
				{
					bool remove = false;
					System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
					if (pi.CanRead)
					{
						object val = pi.GetValue(o, null);
						switch (w.Comparison)
						{
							case SubSonic.Comparison.Equals:
								if (!val.Equals(w.ParameterValue))
								{
									remove = true;
								}
								break;
						}
					}
					if (remove)
					{
						this.Remove(o);
						break;
					}
				}
			}
			return this;
		}


	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the partner_einvoice_main table.
	/// </summary>
	[Serializable]
	public partial class PartnerEinvoiceMain : RepositoryRecord<PartnerEinvoiceMain>, IRecordBase
	{
		#region .ctors and Default Settings

		public PartnerEinvoiceMain()
		{
			SetSQLProps();
			InitSetDefaults();
			MarkNew();
		}

		private void InitSetDefaults() { SetDefaults(); }

		public PartnerEinvoiceMain(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if (useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}

		protected static void SetSQLProps() { GetTableSchema(); }

		#endregion

		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}

		private static void GetTableSchema()
		{
			if (!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("partner_einvoice_main", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns

				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);

				TableSchema.TableColumn colvarInvoiceNumber = new TableSchema.TableColumn(schema);
				colvarInvoiceNumber.ColumnName = "invoice_number";
				colvarInvoiceNumber.DataType = DbType.String;
				colvarInvoiceNumber.MaxLength = 10;
				colvarInvoiceNumber.AutoIncrement = false;
				colvarInvoiceNumber.IsNullable = true;
				colvarInvoiceNumber.IsPrimaryKey = false;
				colvarInvoiceNumber.IsForeignKey = false;
				colvarInvoiceNumber.IsReadOnly = false;
				colvarInvoiceNumber.DefaultSetting = @"";
				colvarInvoiceNumber.ForeignKeyTableName = "";
				schema.Columns.Add(colvarInvoiceNumber);

				TableSchema.TableColumn colvarInvoiceNumberTime = new TableSchema.TableColumn(schema);
				colvarInvoiceNumberTime.ColumnName = "invoice_number_time";
				colvarInvoiceNumberTime.DataType = DbType.DateTime;
				colvarInvoiceNumberTime.MaxLength = 0;
				colvarInvoiceNumberTime.AutoIncrement = false;
				colvarInvoiceNumberTime.IsNullable = true;
				colvarInvoiceNumberTime.IsPrimaryKey = false;
				colvarInvoiceNumberTime.IsForeignKey = false;
				colvarInvoiceNumberTime.IsReadOnly = false;
				colvarInvoiceNumberTime.DefaultSetting = @"";
				colvarInvoiceNumberTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarInvoiceNumberTime);

				TableSchema.TableColumn colvarInvoiceComId = new TableSchema.TableColumn(schema);
				colvarInvoiceComId.ColumnName = "invoice_com_id";
				colvarInvoiceComId.DataType = DbType.String;
				colvarInvoiceComId.MaxLength = 10;
				colvarInvoiceComId.AutoIncrement = false;
				colvarInvoiceComId.IsNullable = true;
				colvarInvoiceComId.IsPrimaryKey = false;
				colvarInvoiceComId.IsForeignKey = false;
				colvarInvoiceComId.IsReadOnly = false;
				colvarInvoiceComId.DefaultSetting = @"";
				colvarInvoiceComId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarInvoiceComId);

				TableSchema.TableColumn colvarInvoiceComName = new TableSchema.TableColumn(schema);
				colvarInvoiceComName.ColumnName = "invoice_com_name";
				colvarInvoiceComName.DataType = DbType.String;
				colvarInvoiceComName.MaxLength = 250;
				colvarInvoiceComName.AutoIncrement = false;
				colvarInvoiceComName.IsNullable = true;
				colvarInvoiceComName.IsPrimaryKey = false;
				colvarInvoiceComName.IsForeignKey = false;
				colvarInvoiceComName.IsReadOnly = false;
				colvarInvoiceComName.DefaultSetting = @"";
				colvarInvoiceComName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarInvoiceComName);

				TableSchema.TableColumn colvarInvoiceBuyerName = new TableSchema.TableColumn(schema);
				colvarInvoiceBuyerName.ColumnName = "invoice_buyer_name";
				colvarInvoiceBuyerName.DataType = DbType.String;
				colvarInvoiceBuyerName.MaxLength = 50;
				colvarInvoiceBuyerName.AutoIncrement = false;
				colvarInvoiceBuyerName.IsNullable = true;
				colvarInvoiceBuyerName.IsPrimaryKey = false;
				colvarInvoiceBuyerName.IsForeignKey = false;
				colvarInvoiceBuyerName.IsReadOnly = false;
				colvarInvoiceBuyerName.DefaultSetting = @"";
				colvarInvoiceBuyerName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarInvoiceBuyerName);

				TableSchema.TableColumn colvarInvoiceBuyerAddress = new TableSchema.TableColumn(schema);
				colvarInvoiceBuyerAddress.ColumnName = "invoice_buyer_address";
				colvarInvoiceBuyerAddress.DataType = DbType.String;
				colvarInvoiceBuyerAddress.MaxLength = 250;
				colvarInvoiceBuyerAddress.AutoIncrement = false;
				colvarInvoiceBuyerAddress.IsNullable = true;
				colvarInvoiceBuyerAddress.IsPrimaryKey = false;
				colvarInvoiceBuyerAddress.IsForeignKey = false;
				colvarInvoiceBuyerAddress.IsReadOnly = false;
				colvarInvoiceBuyerAddress.DefaultSetting = @"";
				colvarInvoiceBuyerAddress.ForeignKeyTableName = "";
				schema.Columns.Add(colvarInvoiceBuyerAddress);

				TableSchema.TableColumn colvarInvoicePass = new TableSchema.TableColumn(schema);
				colvarInvoicePass.ColumnName = "invoice_pass";
				colvarInvoicePass.DataType = DbType.AnsiStringFixedLength;
				colvarInvoicePass.MaxLength = 4;
				colvarInvoicePass.AutoIncrement = false;
				colvarInvoicePass.IsNullable = false;
				colvarInvoicePass.IsPrimaryKey = false;
				colvarInvoicePass.IsForeignKey = false;
				colvarInvoicePass.IsReadOnly = false;
				colvarInvoicePass.DefaultSetting = @"";
				colvarInvoicePass.ForeignKeyTableName = "";
				schema.Columns.Add(colvarInvoicePass);

				TableSchema.TableColumn colvarPartnerOrderId = new TableSchema.TableColumn(schema);
				colvarPartnerOrderId.ColumnName = "partner_order_id";
				colvarPartnerOrderId.DataType = DbType.AnsiString;
				colvarPartnerOrderId.MaxLength = 30;
				colvarPartnerOrderId.AutoIncrement = false;
				colvarPartnerOrderId.IsNullable = false;
				colvarPartnerOrderId.IsPrimaryKey = false;
				colvarPartnerOrderId.IsForeignKey = false;
				colvarPartnerOrderId.IsReadOnly = false;
				colvarPartnerOrderId.DefaultSetting = @"";
				colvarPartnerOrderId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPartnerOrderId);

				TableSchema.TableColumn colvarPartnerType = new TableSchema.TableColumn(schema);
				colvarPartnerType.ColumnName = "partner_type";
				colvarPartnerType.DataType = DbType.Int32;
				colvarPartnerType.MaxLength = 0;
				colvarPartnerType.AutoIncrement = false;
				colvarPartnerType.IsNullable = false;
				colvarPartnerType.IsPrimaryKey = false;
				colvarPartnerType.IsForeignKey = false;
				colvarPartnerType.IsReadOnly = false;
				colvarPartnerType.DefaultSetting = @"";
				colvarPartnerType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPartnerType);

				TableSchema.TableColumn colvarOrderItem = new TableSchema.TableColumn(schema);
				colvarOrderItem.ColumnName = "order_item";
				colvarOrderItem.DataType = DbType.String;
				colvarOrderItem.MaxLength = 250;
				colvarOrderItem.AutoIncrement = false;
				colvarOrderItem.IsNullable = true;
				colvarOrderItem.IsPrimaryKey = false;
				colvarOrderItem.IsForeignKey = false;
				colvarOrderItem.IsReadOnly = false;
				colvarOrderItem.DefaultSetting = @"";
				colvarOrderItem.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderItem);

				TableSchema.TableColumn colvarOrderAmount = new TableSchema.TableColumn(schema);
				colvarOrderAmount.ColumnName = "order_amount";
				colvarOrderAmount.DataType = DbType.Currency;
				colvarOrderAmount.MaxLength = 0;
				colvarOrderAmount.AutoIncrement = false;
				colvarOrderAmount.IsNullable = false;
				colvarOrderAmount.IsPrimaryKey = false;
				colvarOrderAmount.IsForeignKey = false;
				colvarOrderAmount.IsReadOnly = false;
				colvarOrderAmount.DefaultSetting = @"";
				colvarOrderAmount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderAmount);

				TableSchema.TableColumn colvarInvoiceSumAmount = new TableSchema.TableColumn(schema);
				colvarInvoiceSumAmount.ColumnName = "invoice_sum_amount";
				colvarInvoiceSumAmount.DataType = DbType.Currency;
				colvarInvoiceSumAmount.MaxLength = 0;
				colvarInvoiceSumAmount.AutoIncrement = false;
				colvarInvoiceSumAmount.IsNullable = false;
				colvarInvoiceSumAmount.IsPrimaryKey = false;
				colvarInvoiceSumAmount.IsForeignKey = false;
				colvarInvoiceSumAmount.IsReadOnly = false;
				colvarInvoiceSumAmount.DefaultSetting = @"";
				colvarInvoiceSumAmount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarInvoiceSumAmount);

				TableSchema.TableColumn colvarInvoiceTax = new TableSchema.TableColumn(schema);
				colvarInvoiceTax.ColumnName = "invoice_tax";
				colvarInvoiceTax.DataType = DbType.Decimal;
				colvarInvoiceTax.MaxLength = 0;
				colvarInvoiceTax.AutoIncrement = false;
				colvarInvoiceTax.IsNullable = false;
				colvarInvoiceTax.IsPrimaryKey = false;
				colvarInvoiceTax.IsForeignKey = false;
				colvarInvoiceTax.IsReadOnly = false;
				colvarInvoiceTax.DefaultSetting = @"";
				colvarInvoiceTax.ForeignKeyTableName = "";
				schema.Columns.Add(colvarInvoiceTax);

				TableSchema.TableColumn colvarInvoiceRequestTime = new TableSchema.TableColumn(schema);
				colvarInvoiceRequestTime.ColumnName = "invoice_request_time";
				colvarInvoiceRequestTime.DataType = DbType.DateTime;
				colvarInvoiceRequestTime.MaxLength = 0;
				colvarInvoiceRequestTime.AutoIncrement = false;
				colvarInvoiceRequestTime.IsNullable = true;
				colvarInvoiceRequestTime.IsPrimaryKey = false;
				colvarInvoiceRequestTime.IsForeignKey = false;
				colvarInvoiceRequestTime.IsReadOnly = false;
				colvarInvoiceRequestTime.DefaultSetting = @"";
				colvarInvoiceRequestTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarInvoiceRequestTime);

				TableSchema.TableColumn colvarInvoicePaperedTime = new TableSchema.TableColumn(schema);
				colvarInvoicePaperedTime.ColumnName = "invoice_papered_time";
				colvarInvoicePaperedTime.DataType = DbType.DateTime;
				colvarInvoicePaperedTime.MaxLength = 0;
				colvarInvoicePaperedTime.AutoIncrement = false;
				colvarInvoicePaperedTime.IsNullable = true;
				colvarInvoicePaperedTime.IsPrimaryKey = false;
				colvarInvoicePaperedTime.IsForeignKey = false;
				colvarInvoicePaperedTime.IsReadOnly = false;
				colvarInvoicePaperedTime.DefaultSetting = @"";
				colvarInvoicePaperedTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarInvoicePaperedTime);

				TableSchema.TableColumn colvarInvoiceWinning = new TableSchema.TableColumn(schema);
				colvarInvoiceWinning.ColumnName = "invoice_winning";
				colvarInvoiceWinning.DataType = DbType.Boolean;
				colvarInvoiceWinning.MaxLength = 0;
				colvarInvoiceWinning.AutoIncrement = false;
				colvarInvoiceWinning.IsNullable = false;
				colvarInvoiceWinning.IsPrimaryKey = false;
				colvarInvoiceWinning.IsForeignKey = false;
				colvarInvoiceWinning.IsReadOnly = false;
				colvarInvoiceWinning.DefaultSetting = @"";
				colvarInvoiceWinning.ForeignKeyTableName = "";
				schema.Columns.Add(colvarInvoiceWinning);

				TableSchema.TableColumn colvarInvoicePapered = new TableSchema.TableColumn(schema);
				colvarInvoicePapered.ColumnName = "invoice_papered";
				colvarInvoicePapered.DataType = DbType.Boolean;
				colvarInvoicePapered.MaxLength = 0;
				colvarInvoicePapered.AutoIncrement = false;
				colvarInvoicePapered.IsNullable = false;
				colvarInvoicePapered.IsPrimaryKey = false;
				colvarInvoicePapered.IsForeignKey = false;
				colvarInvoicePapered.IsReadOnly = false;
				colvarInvoicePapered.DefaultSetting = @"";
				colvarInvoicePapered.ForeignKeyTableName = "";
				schema.Columns.Add(colvarInvoicePapered);

				TableSchema.TableColumn colvarInvoiceWinnerresponseTime = new TableSchema.TableColumn(schema);
				colvarInvoiceWinnerresponseTime.ColumnName = "invoice_winnerresponse_time";
				colvarInvoiceWinnerresponseTime.DataType = DbType.DateTime;
				colvarInvoiceWinnerresponseTime.MaxLength = 0;
				colvarInvoiceWinnerresponseTime.AutoIncrement = false;
				colvarInvoiceWinnerresponseTime.IsNullable = true;
				colvarInvoiceWinnerresponseTime.IsPrimaryKey = false;
				colvarInvoiceWinnerresponseTime.IsForeignKey = false;
				colvarInvoiceWinnerresponseTime.IsReadOnly = false;
				colvarInvoiceWinnerresponseTime.DefaultSetting = @"";
				colvarInvoiceWinnerresponseTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarInvoiceWinnerresponseTime);

				TableSchema.TableColumn colvarInvoiceWinnerresponsePhone = new TableSchema.TableColumn(schema);
				colvarInvoiceWinnerresponsePhone.ColumnName = "invoice_winnerresponse_phone";
				colvarInvoiceWinnerresponsePhone.DataType = DbType.String;
				colvarInvoiceWinnerresponsePhone.MaxLength = 100;
				colvarInvoiceWinnerresponsePhone.AutoIncrement = false;
				colvarInvoiceWinnerresponsePhone.IsNullable = true;
				colvarInvoiceWinnerresponsePhone.IsPrimaryKey = false;
				colvarInvoiceWinnerresponsePhone.IsForeignKey = false;
				colvarInvoiceWinnerresponsePhone.IsReadOnly = false;
				colvarInvoiceWinnerresponsePhone.DefaultSetting = @"";
				colvarInvoiceWinnerresponsePhone.ForeignKeyTableName = "";
				schema.Columns.Add(colvarInvoiceWinnerresponsePhone);

				TableSchema.TableColumn colvarInvoiceStatus = new TableSchema.TableColumn(schema);
				colvarInvoiceStatus.ColumnName = "invoice_status";
				colvarInvoiceStatus.DataType = DbType.Int32;
				colvarInvoiceStatus.MaxLength = 0;
				colvarInvoiceStatus.AutoIncrement = false;
				colvarInvoiceStatus.IsNullable = true;
				colvarInvoiceStatus.IsPrimaryKey = false;
				colvarInvoiceStatus.IsForeignKey = false;
				colvarInvoiceStatus.IsReadOnly = false;
				colvarInvoiceStatus.DefaultSetting = @"";
				colvarInvoiceStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarInvoiceStatus);

				TableSchema.TableColumn colvarInvoiceMailbackPaper = new TableSchema.TableColumn(schema);
				colvarInvoiceMailbackPaper.ColumnName = "invoice_mailback_paper";
				colvarInvoiceMailbackPaper.DataType = DbType.Boolean;
				colvarInvoiceMailbackPaper.MaxLength = 0;
				colvarInvoiceMailbackPaper.AutoIncrement = false;
				colvarInvoiceMailbackPaper.IsNullable = true;
				colvarInvoiceMailbackPaper.IsPrimaryKey = false;
				colvarInvoiceMailbackPaper.IsForeignKey = false;
				colvarInvoiceMailbackPaper.IsReadOnly = false;
				colvarInvoiceMailbackPaper.DefaultSetting = @"";
				colvarInvoiceMailbackPaper.ForeignKeyTableName = "";
				schema.Columns.Add(colvarInvoiceMailbackPaper);

				TableSchema.TableColumn colvarInvoiceMailbackAllowance = new TableSchema.TableColumn(schema);
				colvarInvoiceMailbackAllowance.ColumnName = "invoice_mailback_allowance";
				colvarInvoiceMailbackAllowance.DataType = DbType.Boolean;
				colvarInvoiceMailbackAllowance.MaxLength = 0;
				colvarInvoiceMailbackAllowance.AutoIncrement = false;
				colvarInvoiceMailbackAllowance.IsNullable = true;
				colvarInvoiceMailbackAllowance.IsPrimaryKey = false;
				colvarInvoiceMailbackAllowance.IsForeignKey = false;
				colvarInvoiceMailbackAllowance.IsReadOnly = false;
				colvarInvoiceMailbackAllowance.DefaultSetting = @"";
				colvarInvoiceMailbackAllowance.ForeignKeyTableName = "";
				schema.Columns.Add(colvarInvoiceMailbackAllowance);

				TableSchema.TableColumn colvarVerifiedTime = new TableSchema.TableColumn(schema);
				colvarVerifiedTime.ColumnName = "verified_time";
				colvarVerifiedTime.DataType = DbType.DateTime;
				colvarVerifiedTime.MaxLength = 0;
				colvarVerifiedTime.AutoIncrement = false;
				colvarVerifiedTime.IsNullable = true;
				colvarVerifiedTime.IsPrimaryKey = false;
				colvarVerifiedTime.IsForeignKey = false;
				colvarVerifiedTime.IsReadOnly = false;
				colvarVerifiedTime.DefaultSetting = @"";
				colvarVerifiedTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarVerifiedTime);

				TableSchema.TableColumn colvarInvoiceMode2 = new TableSchema.TableColumn(schema);
				colvarInvoiceMode2.ColumnName = "invoice_mode2";
				colvarInvoiceMode2.DataType = DbType.Int32;
				colvarInvoiceMode2.MaxLength = 0;
				colvarInvoiceMode2.AutoIncrement = false;
				colvarInvoiceMode2.IsNullable = false;
				colvarInvoiceMode2.IsPrimaryKey = false;
				colvarInvoiceMode2.IsForeignKey = false;
				colvarInvoiceMode2.IsReadOnly = false;
				colvarInvoiceMode2.DefaultSetting = @"";
				colvarInvoiceMode2.ForeignKeyTableName = "";
				schema.Columns.Add(colvarInvoiceMode2);

				TableSchema.TableColumn colvarCarrierId = new TableSchema.TableColumn(schema);
				colvarCarrierId.ColumnName = "carrier_id";
				colvarCarrierId.DataType = DbType.String;
				colvarCarrierId.MaxLength = 64;
				colvarCarrierId.AutoIncrement = false;
				colvarCarrierId.IsNullable = true;
				colvarCarrierId.IsPrimaryKey = false;
				colvarCarrierId.IsForeignKey = false;
				colvarCarrierId.IsReadOnly = false;
				colvarCarrierId.DefaultSetting = @"";
				colvarCarrierId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCarrierId);

				TableSchema.TableColumn colvarLoveCode = new TableSchema.TableColumn(schema);
				colvarLoveCode.ColumnName = "love_code";
				colvarLoveCode.DataType = DbType.String;
				colvarLoveCode.MaxLength = 8;
				colvarLoveCode.AutoIncrement = false;
				colvarLoveCode.IsNullable = true;
				colvarLoveCode.IsPrimaryKey = false;
				colvarLoveCode.IsForeignKey = false;
				colvarLoveCode.IsReadOnly = false;
				colvarLoveCode.DefaultSetting = @"";
				colvarLoveCode.ForeignKeyTableName = "";
				schema.Columns.Add(colvarLoveCode);

				TableSchema.TableColumn colvarCarrierType = new TableSchema.TableColumn(schema);
				colvarCarrierType.ColumnName = "carrier_type";
				colvarCarrierType.DataType = DbType.Int32;
				colvarCarrierType.MaxLength = 0;
				colvarCarrierType.AutoIncrement = false;
				colvarCarrierType.IsNullable = false;
				colvarCarrierType.IsPrimaryKey = false;
				colvarCarrierType.IsForeignKey = false;
				colvarCarrierType.IsReadOnly = false;
				colvarCarrierType.DefaultSetting = @"";
				colvarCarrierType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCarrierType);

				TableSchema.TableColumn colvarInvoiceVoidTime = new TableSchema.TableColumn(schema);
				colvarInvoiceVoidTime.ColumnName = "invoice_void_time";
				colvarInvoiceVoidTime.DataType = DbType.DateTime;
				colvarInvoiceVoidTime.MaxLength = 0;
				colvarInvoiceVoidTime.AutoIncrement = false;
				colvarInvoiceVoidTime.IsNullable = true;
				colvarInvoiceVoidTime.IsPrimaryKey = false;
				colvarInvoiceVoidTime.IsForeignKey = false;
				colvarInvoiceVoidTime.IsReadOnly = false;
				colvarInvoiceVoidTime.DefaultSetting = @"";
				colvarInvoiceVoidTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarInvoiceVoidTime);

				TableSchema.TableColumn colvarInvoiceVoidMsg = new TableSchema.TableColumn(schema);
				colvarInvoiceVoidMsg.ColumnName = "invoice_void_msg";
				colvarInvoiceVoidMsg.DataType = DbType.String;
				colvarInvoiceVoidMsg.MaxLength = 250;
				colvarInvoiceVoidMsg.AutoIncrement = false;
				colvarInvoiceVoidMsg.IsNullable = true;
				colvarInvoiceVoidMsg.IsPrimaryKey = false;
				colvarInvoiceVoidMsg.IsForeignKey = false;
				colvarInvoiceVoidMsg.IsReadOnly = false;
				colvarInvoiceVoidMsg.DefaultSetting = @"";
				colvarInvoiceVoidMsg.ForeignKeyTableName = "";
				schema.Columns.Add(colvarInvoiceVoidMsg);

				TableSchema.TableColumn colvarCreditCardTail = new TableSchema.TableColumn(schema);
				colvarCreditCardTail.ColumnName = "credit_card_tail";
				colvarCreditCardTail.DataType = DbType.AnsiStringFixedLength;
				colvarCreditCardTail.MaxLength = 4;
				colvarCreditCardTail.AutoIncrement = false;
				colvarCreditCardTail.IsNullable = true;
				colvarCreditCardTail.IsPrimaryKey = false;
				colvarCreditCardTail.IsForeignKey = false;
				colvarCreditCardTail.IsReadOnly = false;
				colvarCreditCardTail.DefaultSetting = @"";
				colvarCreditCardTail.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreditCardTail);

				TableSchema.TableColumn colvarCancelDate = new TableSchema.TableColumn(schema);
				colvarCancelDate.ColumnName = "cancel_date";
				colvarCancelDate.DataType = DbType.DateTime;
				colvarCancelDate.MaxLength = 0;
				colvarCancelDate.AutoIncrement = false;
				colvarCancelDate.IsNullable = true;
				colvarCancelDate.IsPrimaryKey = false;
				colvarCancelDate.IsForeignKey = false;
				colvarCancelDate.IsReadOnly = false;
				colvarCancelDate.DefaultSetting = @"";
				colvarCancelDate.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCancelDate);

				TableSchema.TableColumn colvarInvoiceSerialId = new TableSchema.TableColumn(schema);
				colvarInvoiceSerialId.ColumnName = "invoice_serial_id";
				colvarInvoiceSerialId.DataType = DbType.Int32;
				colvarInvoiceSerialId.MaxLength = 0;
				colvarInvoiceSerialId.AutoIncrement = false;
				colvarInvoiceSerialId.IsNullable = false;
				colvarInvoiceSerialId.IsPrimaryKey = false;
				colvarInvoiceSerialId.IsForeignKey = false;
				colvarInvoiceSerialId.IsReadOnly = false;
				colvarInvoiceSerialId.DefaultSetting = @"";
				colvarInvoiceSerialId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarInvoiceSerialId);

				TableSchema.TableColumn colvarCouponId = new TableSchema.TableColumn(schema);
				colvarCouponId.ColumnName = "coupon_id";
				colvarCouponId.DataType = DbType.Int32;
				colvarCouponId.MaxLength = 0;
				colvarCouponId.AutoIncrement = false;
				colvarCouponId.IsNullable = true;
				colvarCouponId.IsPrimaryKey = false;
				colvarCouponId.IsForeignKey = false;
				colvarCouponId.IsReadOnly = false;
				colvarCouponId.DefaultSetting = @"";
				colvarCouponId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCouponId);

				TableSchema.TableColumn colvarWinnerMail = new TableSchema.TableColumn(schema);
				colvarWinnerMail.ColumnName = "winner_mail";
				colvarWinnerMail.DataType = DbType.String;
				colvarWinnerMail.MaxLength = 50;
				colvarWinnerMail.AutoIncrement = false;
				colvarWinnerMail.IsNullable = true;
				colvarWinnerMail.IsPrimaryKey = false;
				colvarWinnerMail.IsForeignKey = false;
				colvarWinnerMail.IsReadOnly = false;
				colvarWinnerMail.DefaultSetting = @"";
				colvarWinnerMail.ForeignKeyTableName = "";
				schema.Columns.Add(colvarWinnerMail);

				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("partner_einvoice_main", schema);
			}
		}
		#endregion

		#region Props

		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}

		[XmlAttribute("InvoiceNumber")]
		[Bindable(true)]
		public string InvoiceNumber
		{
			get { return GetColumnValue<string>(Columns.InvoiceNumber); }
			set { SetColumnValue(Columns.InvoiceNumber, value); }
		}

		[XmlAttribute("InvoiceNumberTime")]
		[Bindable(true)]
		public DateTime? InvoiceNumberTime
		{
			get { return GetColumnValue<DateTime?>(Columns.InvoiceNumberTime); }
			set { SetColumnValue(Columns.InvoiceNumberTime, value); }
		}

		[XmlAttribute("InvoiceComId")]
		[Bindable(true)]
		public string InvoiceComId
		{
			get { return GetColumnValue<string>(Columns.InvoiceComId); }
			set { SetColumnValue(Columns.InvoiceComId, value); }
		}

		[XmlAttribute("InvoiceComName")]
		[Bindable(true)]
		public string InvoiceComName
		{
			get { return GetColumnValue<string>(Columns.InvoiceComName); }
			set { SetColumnValue(Columns.InvoiceComName, value); }
		}

		[XmlAttribute("InvoiceBuyerName")]
		[Bindable(true)]
		public string InvoiceBuyerName
		{
			get { return GetColumnValue<string>(Columns.InvoiceBuyerName); }
			set { SetColumnValue(Columns.InvoiceBuyerName, value); }
		}

		[XmlAttribute("InvoiceBuyerAddress")]
		[Bindable(true)]
		public string InvoiceBuyerAddress
		{
			get { return GetColumnValue<string>(Columns.InvoiceBuyerAddress); }
			set { SetColumnValue(Columns.InvoiceBuyerAddress, value); }
		}

		[XmlAttribute("InvoicePass")]
		[Bindable(true)]
		public string InvoicePass
		{
			get { return GetColumnValue<string>(Columns.InvoicePass); }
			set { SetColumnValue(Columns.InvoicePass, value); }
		}

		[XmlAttribute("PartnerOrderId")]
		[Bindable(true)]
		public string PartnerOrderId
		{
			get { return GetColumnValue<string>(Columns.PartnerOrderId); }
			set { SetColumnValue(Columns.PartnerOrderId, value); }
		}

		[XmlAttribute("PartnerType")]
		[Bindable(true)]
		public int PartnerType
		{
			get { return GetColumnValue<int>(Columns.PartnerType); }
			set { SetColumnValue(Columns.PartnerType, value); }
		}

		[XmlAttribute("OrderItem")]
		[Bindable(true)]
		public string OrderItem
		{
			get { return GetColumnValue<string>(Columns.OrderItem); }
			set { SetColumnValue(Columns.OrderItem, value); }
		}

		[XmlAttribute("OrderAmount")]
		[Bindable(true)]
		public decimal OrderAmount
		{
			get { return GetColumnValue<decimal>(Columns.OrderAmount); }
			set { SetColumnValue(Columns.OrderAmount, value); }
		}

		[XmlAttribute("InvoiceSumAmount")]
		[Bindable(true)]
		public decimal InvoiceSumAmount
		{
			get { return GetColumnValue<decimal>(Columns.InvoiceSumAmount); }
			set { SetColumnValue(Columns.InvoiceSumAmount, value); }
		}

		[XmlAttribute("InvoiceTax")]
		[Bindable(true)]
		public decimal InvoiceTax
		{
			get { return GetColumnValue<decimal>(Columns.InvoiceTax); }
			set { SetColumnValue(Columns.InvoiceTax, value); }
		}

		[XmlAttribute("InvoiceRequestTime")]
		[Bindable(true)]
		public DateTime? InvoiceRequestTime
		{
			get { return GetColumnValue<DateTime?>(Columns.InvoiceRequestTime); }
			set { SetColumnValue(Columns.InvoiceRequestTime, value); }
		}

		[XmlAttribute("InvoicePaperedTime")]
		[Bindable(true)]
		public DateTime? InvoicePaperedTime
		{
			get { return GetColumnValue<DateTime?>(Columns.InvoicePaperedTime); }
			set { SetColumnValue(Columns.InvoicePaperedTime, value); }
		}

		[XmlAttribute("InvoiceWinning")]
		[Bindable(true)]
		public bool InvoiceWinning
		{
			get { return GetColumnValue<bool>(Columns.InvoiceWinning); }
			set { SetColumnValue(Columns.InvoiceWinning, value); }
		}

		[XmlAttribute("InvoicePapered")]
		[Bindable(true)]
		public bool InvoicePapered
		{
			get { return GetColumnValue<bool>(Columns.InvoicePapered); }
			set { SetColumnValue(Columns.InvoicePapered, value); }
		}

		[XmlAttribute("InvoiceWinnerresponseTime")]
		[Bindable(true)]
		public DateTime? InvoiceWinnerresponseTime
		{
			get { return GetColumnValue<DateTime?>(Columns.InvoiceWinnerresponseTime); }
			set { SetColumnValue(Columns.InvoiceWinnerresponseTime, value); }
		}

		[XmlAttribute("InvoiceWinnerresponsePhone")]
		[Bindable(true)]
		public string InvoiceWinnerresponsePhone
		{
			get { return GetColumnValue<string>(Columns.InvoiceWinnerresponsePhone); }
			set { SetColumnValue(Columns.InvoiceWinnerresponsePhone, value); }
		}

		[XmlAttribute("InvoiceStatus")]
		[Bindable(true)]
		public int? InvoiceStatus
		{
			get { return GetColumnValue<int?>(Columns.InvoiceStatus); }
			set { SetColumnValue(Columns.InvoiceStatus, value); }
		}

		[XmlAttribute("InvoiceMailbackPaper")]
		[Bindable(true)]
		public bool? InvoiceMailbackPaper
		{
			get { return GetColumnValue<bool?>(Columns.InvoiceMailbackPaper); }
			set { SetColumnValue(Columns.InvoiceMailbackPaper, value); }
		}

		[XmlAttribute("InvoiceMailbackAllowance")]
		[Bindable(true)]
		public bool? InvoiceMailbackAllowance
		{
			get { return GetColumnValue<bool?>(Columns.InvoiceMailbackAllowance); }
			set { SetColumnValue(Columns.InvoiceMailbackAllowance, value); }
		}

		[XmlAttribute("VerifiedTime")]
		[Bindable(true)]
		public DateTime? VerifiedTime
		{
			get { return GetColumnValue<DateTime?>(Columns.VerifiedTime); }
			set { SetColumnValue(Columns.VerifiedTime, value); }
		}

		[XmlAttribute("InvoiceMode2")]
		[Bindable(true)]
		public int InvoiceMode2
		{
			get { return GetColumnValue<int>(Columns.InvoiceMode2); }
			set { SetColumnValue(Columns.InvoiceMode2, value); }
		}

		[XmlAttribute("CarrierId")]
		[Bindable(true)]
		public string CarrierId
		{
			get { return GetColumnValue<string>(Columns.CarrierId); }
			set { SetColumnValue(Columns.CarrierId, value); }
		}

		[XmlAttribute("LoveCode")]
		[Bindable(true)]
		public string LoveCode
		{
			get { return GetColumnValue<string>(Columns.LoveCode); }
			set { SetColumnValue(Columns.LoveCode, value); }
		}

		[XmlAttribute("CarrierType")]
		[Bindable(true)]
		public int CarrierType
		{
			get { return GetColumnValue<int>(Columns.CarrierType); }
			set { SetColumnValue(Columns.CarrierType, value); }
		}

		[XmlAttribute("InvoiceVoidTime")]
		[Bindable(true)]
		public DateTime? InvoiceVoidTime
		{
			get { return GetColumnValue<DateTime?>(Columns.InvoiceVoidTime); }
			set { SetColumnValue(Columns.InvoiceVoidTime, value); }
		}

		[XmlAttribute("InvoiceVoidMsg")]
		[Bindable(true)]
		public string InvoiceVoidMsg
		{
			get { return GetColumnValue<string>(Columns.InvoiceVoidMsg); }
			set { SetColumnValue(Columns.InvoiceVoidMsg, value); }
		}

		[XmlAttribute("CreditCardTail")]
		[Bindable(true)]
		public string CreditCardTail
		{
			get { return GetColumnValue<string>(Columns.CreditCardTail); }
			set { SetColumnValue(Columns.CreditCardTail, value); }
		}

		[XmlAttribute("CancelDate")]
		[Bindable(true)]
		public DateTime? CancelDate
		{
			get { return GetColumnValue<DateTime?>(Columns.CancelDate); }
			set { SetColumnValue(Columns.CancelDate, value); }
		}

		[XmlAttribute("InvoiceSerialId")]
		[Bindable(true)]
		public int InvoiceSerialId
		{
			get { return GetColumnValue<int>(Columns.InvoiceSerialId); }
			set { SetColumnValue(Columns.InvoiceSerialId, value); }
		}

		[XmlAttribute("CouponId")]
		[Bindable(true)]
		public int? CouponId
		{
			get { return GetColumnValue<int?>(Columns.CouponId); }
			set { SetColumnValue(Columns.CouponId, value); }
		}

		[XmlAttribute("WinnerMail")]
		[Bindable(true)]
		public string WinnerMail
		{
			get { return GetColumnValue<string>(Columns.WinnerMail); }
			set { SetColumnValue(Columns.WinnerMail, value); }
		}

		#endregion




		//no foreign key tables defined (0)



		//no ManyToMany tables defined (0)





		#region Typed Columns


		public static TableSchema.TableColumn IdColumn
		{
			get { return Schema.Columns[0]; }
		}



		public static TableSchema.TableColumn InvoiceNumberColumn
		{
			get { return Schema.Columns[1]; }
		}



		public static TableSchema.TableColumn InvoiceNumberTimeColumn
		{
			get { return Schema.Columns[2]; }
		}



		public static TableSchema.TableColumn InvoiceComIdColumn
		{
			get { return Schema.Columns[3]; }
		}



		public static TableSchema.TableColumn InvoiceComNameColumn
		{
			get { return Schema.Columns[4]; }
		}



		public static TableSchema.TableColumn InvoiceBuyerNameColumn
		{
			get { return Schema.Columns[5]; }
		}



		public static TableSchema.TableColumn InvoiceBuyerAddressColumn
		{
			get { return Schema.Columns[6]; }
		}



		public static TableSchema.TableColumn InvoicePassColumn
		{
			get { return Schema.Columns[7]; }
		}



		public static TableSchema.TableColumn PartnerOrderIdColumn
		{
			get { return Schema.Columns[8]; }
		}



		public static TableSchema.TableColumn PartnerTypeColumn
		{
			get { return Schema.Columns[9]; }
		}



		public static TableSchema.TableColumn OrderItemColumn
		{
			get { return Schema.Columns[10]; }
		}



		public static TableSchema.TableColumn OrderAmountColumn
		{
			get { return Schema.Columns[11]; }
		}



		public static TableSchema.TableColumn InvoiceSumAmountColumn
		{
			get { return Schema.Columns[12]; }
		}



		public static TableSchema.TableColumn InvoiceTaxColumn
		{
			get { return Schema.Columns[13]; }
		}



		public static TableSchema.TableColumn InvoiceRequestTimeColumn
		{
			get { return Schema.Columns[14]; }
		}



		public static TableSchema.TableColumn InvoicePaperedTimeColumn
		{
			get { return Schema.Columns[15]; }
		}



		public static TableSchema.TableColumn InvoiceWinningColumn
		{
			get { return Schema.Columns[16]; }
		}



		public static TableSchema.TableColumn InvoicePaperedColumn
		{
			get { return Schema.Columns[17]; }
		}



		public static TableSchema.TableColumn InvoiceWinnerresponseTimeColumn
		{
			get { return Schema.Columns[18]; }
		}



		public static TableSchema.TableColumn InvoiceWinnerresponsePhoneColumn
		{
			get { return Schema.Columns[19]; }
		}



		public static TableSchema.TableColumn InvoiceStatusColumn
		{
			get { return Schema.Columns[20]; }
		}



		public static TableSchema.TableColumn InvoiceMailbackPaperColumn
		{
			get { return Schema.Columns[21]; }
		}



		public static TableSchema.TableColumn InvoiceMailbackAllowanceColumn
		{
			get { return Schema.Columns[22]; }
		}



		public static TableSchema.TableColumn VerifiedTimeColumn
		{
			get { return Schema.Columns[23]; }
		}



		public static TableSchema.TableColumn InvoiceMode2Column
		{
			get { return Schema.Columns[24]; }
		}



		public static TableSchema.TableColumn CarrierIdColumn
		{
			get { return Schema.Columns[25]; }
		}



		public static TableSchema.TableColumn LoveCodeColumn
		{
			get { return Schema.Columns[26]; }
		}



		public static TableSchema.TableColumn CarrierTypeColumn
		{
			get { return Schema.Columns[27]; }
		}



		public static TableSchema.TableColumn InvoiceVoidTimeColumn
		{
			get { return Schema.Columns[28]; }
		}



		public static TableSchema.TableColumn InvoiceVoidMsgColumn
		{
			get { return Schema.Columns[29]; }
		}



		public static TableSchema.TableColumn CreditCardTailColumn
		{
			get { return Schema.Columns[30]; }
		}



		public static TableSchema.TableColumn CancelDateColumn
		{
			get { return Schema.Columns[31]; }
		}



		public static TableSchema.TableColumn InvoiceSerialIdColumn
		{
			get { return Schema.Columns[32]; }
		}



		public static TableSchema.TableColumn CouponIdColumn
		{
			get { return Schema.Columns[33]; }
		}



		public static TableSchema.TableColumn WinnerMailColumn
		{
			get { return Schema.Columns[34]; }
		}



		#endregion
		#region Columns Struct
		public struct Columns
		{
			public static string Id = @"id";
			public static string InvoiceNumber = @"invoice_number";
			public static string InvoiceNumberTime = @"invoice_number_time";
			public static string InvoiceComId = @"invoice_com_id";
			public static string InvoiceComName = @"invoice_com_name";
			public static string InvoiceBuyerName = @"invoice_buyer_name";
			public static string InvoiceBuyerAddress = @"invoice_buyer_address";
			public static string InvoicePass = @"invoice_pass";
			public static string PartnerOrderId = @"partner_order_id";
			public static string PartnerType = @"partner_type";
			public static string OrderItem = @"order_item";
			public static string OrderAmount = @"order_amount";
			public static string InvoiceSumAmount = @"invoice_sum_amount";
			public static string InvoiceTax = @"invoice_tax";
			public static string InvoiceRequestTime = @"invoice_request_time";
			public static string InvoicePaperedTime = @"invoice_papered_time";
			public static string InvoiceWinning = @"invoice_winning";
			public static string InvoicePapered = @"invoice_papered";
			public static string InvoiceWinnerresponseTime = @"invoice_winnerresponse_time";
			public static string InvoiceWinnerresponsePhone = @"invoice_winnerresponse_phone";
			public static string InvoiceStatus = @"invoice_status";
			public static string InvoiceMailbackPaper = @"invoice_mailback_paper";
			public static string InvoiceMailbackAllowance = @"invoice_mailback_allowance";
			public static string VerifiedTime = @"verified_time";
			public static string InvoiceMode2 = @"invoice_mode2";
			public static string CarrierId = @"carrier_id";
			public static string LoveCode = @"love_code";
			public static string CarrierType = @"carrier_type";
			public static string InvoiceVoidTime = @"invoice_void_time";
			public static string InvoiceVoidMsg = @"invoice_void_msg";
			public static string CreditCardTail = @"credit_card_tail";
			public static string CancelDate = @"cancel_date";
			public static string InvoiceSerialId = @"invoice_serial_id";
			public static string CouponId = @"coupon_id";
			public static string WinnerMail = @"winner_mail";

		}
		#endregion

		#region Update PK Collections

		#endregion

		#region Deep Save

		#endregion
	}
}

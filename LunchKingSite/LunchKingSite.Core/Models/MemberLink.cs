using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the MemberLink class.
	/// </summary>
    [Serializable]
	public partial class MemberLinkCollection : RepositoryList<MemberLink, MemberLinkCollection>
	{	   
		public MemberLinkCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>MemberLinkCollection</returns>
		public MemberLinkCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                MemberLink o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the member_link table.
	/// </summary>
	[Serializable]
	public partial class MemberLink : RepositoryRecord<MemberLink>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public MemberLink()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public MemberLink(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("member_link", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarExternalOrg = new TableSchema.TableColumn(schema);
				colvarExternalOrg.ColumnName = "external_org";
				colvarExternalOrg.DataType = DbType.Int32;
				colvarExternalOrg.MaxLength = 0;
				colvarExternalOrg.AutoIncrement = false;
				colvarExternalOrg.IsNullable = false;
				colvarExternalOrg.IsPrimaryKey = true;
				colvarExternalOrg.IsForeignKey = false;
				colvarExternalOrg.IsReadOnly = false;
				colvarExternalOrg.DefaultSetting = @"";
				colvarExternalOrg.ForeignKeyTableName = "";
				schema.Columns.Add(colvarExternalOrg);
				
				TableSchema.TableColumn colvarExternalUserId = new TableSchema.TableColumn(schema);
				colvarExternalUserId.ColumnName = "external_user_id";
				colvarExternalUserId.DataType = DbType.String;
				colvarExternalUserId.MaxLength = 256;
				colvarExternalUserId.AutoIncrement = false;
				colvarExternalUserId.IsNullable = false;
				colvarExternalUserId.IsPrimaryKey = false;
				colvarExternalUserId.IsForeignKey = false;
				colvarExternalUserId.IsReadOnly = false;
				colvarExternalUserId.DefaultSetting = @"";
				colvarExternalUserId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarExternalUserId);
				
				TableSchema.TableColumn colvarExtraParameter = new TableSchema.TableColumn(schema);
				colvarExtraParameter.ColumnName = "extra_parameter";
				colvarExtraParameter.DataType = DbType.String;
				colvarExtraParameter.MaxLength = 2000;
				colvarExtraParameter.AutoIncrement = false;
				colvarExtraParameter.IsNullable = true;
				colvarExtraParameter.IsPrimaryKey = false;
				colvarExtraParameter.IsForeignKey = false;
				colvarExtraParameter.IsReadOnly = false;
				colvarExtraParameter.DefaultSetting = @"";
				colvarExtraParameter.ForeignKeyTableName = "";
				schema.Columns.Add(colvarExtraParameter);
				
				TableSchema.TableColumn colvarUserId = new TableSchema.TableColumn(schema);
				colvarUserId.ColumnName = "user_id";
				colvarUserId.DataType = DbType.Int32;
				colvarUserId.MaxLength = 0;
				colvarUserId.AutoIncrement = false;
				colvarUserId.IsNullable = false;
				colvarUserId.IsPrimaryKey = true;
				colvarUserId.IsForeignKey = true;
				colvarUserId.IsReadOnly = false;
				
						colvarUserId.DefaultSetting = @"((0))";
				
					colvarUserId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUserId);
				
				TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
				colvarCreateId.ColumnName = "create_id";
				colvarCreateId.DataType = DbType.String;
				colvarCreateId.MaxLength = 256;
				colvarCreateId.AutoIncrement = false;
				colvarCreateId.IsNullable = true;
				colvarCreateId.IsPrimaryKey = false;
				colvarCreateId.IsForeignKey = false;
				colvarCreateId.IsReadOnly = false;
				colvarCreateId.DefaultSetting = @"";
				colvarCreateId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateId);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("member_link",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("ExternalOrg")]
		[Bindable(true)]
		public int ExternalOrg 
		{
			get { return GetColumnValue<int>(Columns.ExternalOrg); }
			set { SetColumnValue(Columns.ExternalOrg, value); }
		}
		  
		[XmlAttribute("ExternalUserId")]
		[Bindable(true)]
		public string ExternalUserId 
		{
			get { return GetColumnValue<string>(Columns.ExternalUserId); }
			set { SetColumnValue(Columns.ExternalUserId, value); }
		}
		  
		[XmlAttribute("ExtraParameter")]
		[Bindable(true)]
		public string ExtraParameter 
		{
			get { return GetColumnValue<string>(Columns.ExtraParameter); }
			set { SetColumnValue(Columns.ExtraParameter, value); }
		}
		  
		[XmlAttribute("UserId")]
		[Bindable(true)]
		public int UserId 
		{
			get { return GetColumnValue<int>(Columns.UserId); }
			set { SetColumnValue(Columns.UserId, value); }
		}
		  
		[XmlAttribute("CreateId")]
		[Bindable(true)]
		public string CreateId 
		{
			get { return GetColumnValue<string>(Columns.CreateId); }
			set { SetColumnValue(Columns.CreateId, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn ExternalOrgColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn ExternalUserIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn ExtraParameterColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn UserIdColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string ExternalOrg = @"external_org";
			 public static string ExternalUserId = @"external_user_id";
			 public static string ExtraParameter = @"extra_parameter";
			 public static string UserId = @"user_id";
			 public static string CreateId = @"create_id";
			 public static string CreateTime = @"create_time";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

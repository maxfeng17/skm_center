using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the SalesVolumeMonth class.
    /// </summary>
    [Serializable]
    public partial class SalesVolumeMonthCollection : RepositoryList<SalesVolumeMonth, SalesVolumeMonthCollection>
    {
        public SalesVolumeMonthCollection() { }

        /// <summary>
        /// Filters an existing collection based on the set criteria. This is an in-memory filter
        /// Thanks to developingchris for this!
        /// </summary>
        /// <returns>SalesVolumeMonthCollection</returns>
        public SalesVolumeMonthCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                SalesVolumeMonth o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }


    }
    /// <summary>
    /// This is an ActiveRecord class which wraps the sales_volume_month table.
    /// </summary>
    [Serializable]
    public partial class SalesVolumeMonth : RepositoryRecord<SalesVolumeMonth>, IRecordBase
    {
        #region .ctors and Default Settings

        public SalesVolumeMonth()
        {
            SetSQLProps();
            InitSetDefaults();
            MarkNew();
        }

        private void InitSetDefaults() { SetDefaults(); }

        public SalesVolumeMonth(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
                ForceDefaults();
            MarkNew();
        }

        protected static void SetSQLProps() { GetTableSchema(); }

        #endregion

        #region Schema and Query Accessor
        public static Query CreateQuery() { return new Query(Schema); }
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                    SetSQLProps();
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("sales_volume_month", TableType.Table, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "ID";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = true;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = true;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                colvarId.DefaultSetting = @"";
                colvarId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarId);

                TableSchema.TableColumn colvarYearMon = new TableSchema.TableColumn(schema);
                colvarYearMon.ColumnName = "YEAR_MON";
                colvarYearMon.DataType = DbType.AnsiString;
                colvarYearMon.MaxLength = 6;
                colvarYearMon.AutoIncrement = false;
                colvarYearMon.IsNullable = false;
                colvarYearMon.IsPrimaryKey = false;
                colvarYearMon.IsForeignKey = false;
                colvarYearMon.IsReadOnly = false;
                colvarYearMon.DefaultSetting = @"";
                colvarYearMon.ForeignKeyTableName = "";
                schema.Columns.Add(colvarYearMon);

                TableSchema.TableColumn colvarSalesId = new TableSchema.TableColumn(schema);
                colvarSalesId.ColumnName = "SALES_ID";
                colvarSalesId.DataType = DbType.String;
                colvarSalesId.MaxLength = 10;
                colvarSalesId.AutoIncrement = false;
                colvarSalesId.IsNullable = false;
                colvarSalesId.IsPrimaryKey = false;
                colvarSalesId.IsForeignKey = false;
                colvarSalesId.IsReadOnly = false;
                colvarSalesId.DefaultSetting = @"";
                colvarSalesId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarSalesId);

                TableSchema.TableColumn colvarForecastRangeId = new TableSchema.TableColumn(schema);
                colvarForecastRangeId.ColumnName = "FORECAST_RANGE_ID";
                colvarForecastRangeId.DataType = DbType.Byte;
                colvarForecastRangeId.MaxLength = 0;
                colvarForecastRangeId.AutoIncrement = false;
                colvarForecastRangeId.IsNullable = true;
                colvarForecastRangeId.IsPrimaryKey = false;
                colvarForecastRangeId.IsForeignKey = false;
                colvarForecastRangeId.IsReadOnly = false;
                colvarForecastRangeId.DefaultSetting = @"";
                colvarForecastRangeId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarForecastRangeId);

                TableSchema.TableColumn colvarRealRangeId = new TableSchema.TableColumn(schema);
                colvarRealRangeId.ColumnName = "REAL_RANGE_ID";
                colvarRealRangeId.DataType = DbType.Byte;
                colvarRealRangeId.MaxLength = 0;
                colvarRealRangeId.AutoIncrement = false;
                colvarRealRangeId.IsNullable = true;
                colvarRealRangeId.IsPrimaryKey = false;
                colvarRealRangeId.IsForeignKey = false;
                colvarRealRangeId.IsReadOnly = false;
                colvarRealRangeId.DefaultSetting = @"";
                colvarRealRangeId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarRealRangeId);

                TableSchema.TableColumn colvarRealVolume = new TableSchema.TableColumn(schema);
                colvarRealVolume.ColumnName = "REAL_VOLUME";
                colvarRealVolume.DataType = DbType.Double;
                colvarRealVolume.MaxLength = 0;
                colvarRealVolume.AutoIncrement = false;
                colvarRealVolume.IsNullable = true;
                colvarRealVolume.IsPrimaryKey = false;
                colvarRealVolume.IsForeignKey = false;
                colvarRealVolume.IsReadOnly = false;
                colvarRealVolume.DefaultSetting = @"";
                colvarRealVolume.ForeignKeyTableName = "";
                schema.Columns.Add(colvarRealVolume);

                TableSchema.TableColumn colvarBonusRate = new TableSchema.TableColumn(schema);
                colvarBonusRate.ColumnName = "BONUS_RATE";
                colvarBonusRate.DataType = DbType.Double;
                colvarBonusRate.MaxLength = 0;
                colvarBonusRate.AutoIncrement = false;
                colvarBonusRate.IsNullable = true;
                colvarBonusRate.IsPrimaryKey = false;
                colvarBonusRate.IsForeignKey = false;
                colvarBonusRate.IsReadOnly = false;
                colvarBonusRate.DefaultSetting = @"";
                colvarBonusRate.ForeignKeyTableName = "";
                schema.Columns.Add(colvarBonusRate);

                TableSchema.TableColumn colvarBonus = new TableSchema.TableColumn(schema);
                colvarBonus.ColumnName = "BONUS";
                colvarBonus.DataType = DbType.Double;
                colvarBonus.MaxLength = 0;
                colvarBonus.AutoIncrement = false;
                colvarBonus.IsNullable = true;
                colvarBonus.IsPrimaryKey = false;
                colvarBonus.IsForeignKey = false;
                colvarBonus.IsReadOnly = false;
                colvarBonus.DefaultSetting = @"";
                colvarBonus.ForeignKeyTableName = "";
                schema.Columns.Add(colvarBonus);

                TableSchema.TableColumn colvarPointBonus = new TableSchema.TableColumn(schema);
                colvarPointBonus.ColumnName = "POINT_BONUS";
                colvarPointBonus.DataType = DbType.Double;
                colvarPointBonus.MaxLength = 0;
                colvarPointBonus.AutoIncrement = false;
                colvarPointBonus.IsNullable = true;
                colvarPointBonus.IsPrimaryKey = false;
                colvarPointBonus.IsForeignKey = false;
                colvarPointBonus.IsReadOnly = false;
                colvarPointBonus.DefaultSetting = @"";
                colvarPointBonus.ForeignKeyTableName = "";
                schema.Columns.Add(colvarPointBonus);

                TableSchema.TableColumn colvarTotal = new TableSchema.TableColumn(schema);
                colvarTotal.ColumnName = "TOTAL";
                colvarTotal.DataType = DbType.Double;
                colvarTotal.MaxLength = 0;
                colvarTotal.AutoIncrement = false;
                colvarTotal.IsNullable = true;
                colvarTotal.IsPrimaryKey = false;
                colvarTotal.IsForeignKey = false;
                colvarTotal.IsReadOnly = false;
                colvarTotal.DefaultSetting = @"";
                colvarTotal.ForeignKeyTableName = "";
                schema.Columns.Add(colvarTotal);

                TableSchema.TableColumn colvarKpiTarget = new TableSchema.TableColumn(schema);
                colvarKpiTarget.ColumnName = "KPI_TARGET";
                colvarKpiTarget.DataType = DbType.Double;
                colvarKpiTarget.MaxLength = 0;
                colvarKpiTarget.AutoIncrement = false;
                colvarKpiTarget.IsNullable = true;
                colvarKpiTarget.IsPrimaryKey = false;
                colvarKpiTarget.IsForeignKey = false;
                colvarKpiTarget.IsReadOnly = false;
                colvarKpiTarget.DefaultSetting = @"";
                colvarKpiTarget.ForeignKeyTableName = "";
                schema.Columns.Add(colvarKpiTarget);

                TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
                colvarCreateId.ColumnName = "CREATE_ID";
                colvarCreateId.DataType = DbType.String;
                colvarCreateId.MaxLength = 256;
                colvarCreateId.AutoIncrement = false;
                colvarCreateId.IsNullable = true;
                colvarCreateId.IsPrimaryKey = false;
                colvarCreateId.IsForeignKey = false;
                colvarCreateId.IsReadOnly = false;
                colvarCreateId.DefaultSetting = @"";
                colvarCreateId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateId);

                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "CREATE_TIME";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = true;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                colvarCreateTime.DefaultSetting = @"";
                colvarCreateTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateTime);

                TableSchema.TableColumn colvarDealTarget = new TableSchema.TableColumn(schema);
                colvarDealTarget.ColumnName = "DEAL_TARGET";
                colvarDealTarget.DataType = DbType.Int32;
                colvarDealTarget.MaxLength = 0;
                colvarDealTarget.AutoIncrement = false;
                colvarDealTarget.IsNullable = true;
                colvarDealTarget.IsPrimaryKey = false;
                colvarDealTarget.IsForeignKey = false;
                colvarDealTarget.IsReadOnly = false;
                colvarDealTarget.DefaultSetting = @"";
                colvarDealTarget.ForeignKeyTableName = "";
                schema.Columns.Add(colvarDealTarget);

                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("sales_volume_month", schema);
            }
        }
        #endregion

        #region Props

        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id
        {
            get { return GetColumnValue<int>(Columns.Id); }
            set { SetColumnValue(Columns.Id, value); }
        }

        [XmlAttribute("YearMon")]
        [Bindable(true)]
        public string YearMon
        {
            get { return GetColumnValue<string>(Columns.YearMon); }
            set { SetColumnValue(Columns.YearMon, value); }
        }

        [XmlAttribute("SalesId")]
        [Bindable(true)]
        public string SalesId
        {
            get { return GetColumnValue<string>(Columns.SalesId); }
            set { SetColumnValue(Columns.SalesId, value); }
        }

        [XmlAttribute("ForecastRangeId")]
        [Bindable(true)]
        public byte? ForecastRangeId
        {
            get { return GetColumnValue<byte?>(Columns.ForecastRangeId); }
            set { SetColumnValue(Columns.ForecastRangeId, value); }
        }

        [XmlAttribute("RealRangeId")]
        [Bindable(true)]
        public byte? RealRangeId
        {
            get { return GetColumnValue<byte?>(Columns.RealRangeId); }
            set { SetColumnValue(Columns.RealRangeId, value); }
        }

        [XmlAttribute("RealVolume")]
        [Bindable(true)]
        public double? RealVolume
        {
            get { return GetColumnValue<double?>(Columns.RealVolume); }
            set { SetColumnValue(Columns.RealVolume, value); }
        }

        [XmlAttribute("BonusRate")]
        [Bindable(true)]
        public double? BonusRate
        {
            get { return GetColumnValue<double?>(Columns.BonusRate); }
            set { SetColumnValue(Columns.BonusRate, value); }
        }

        [XmlAttribute("Bonus")]
        [Bindable(true)]
        public double? Bonus
        {
            get { return GetColumnValue<double?>(Columns.Bonus); }
            set { SetColumnValue(Columns.Bonus, value); }
        }

        [XmlAttribute("PointBonus")]
        [Bindable(true)]
        public double? PointBonus
        {
            get { return GetColumnValue<double?>(Columns.PointBonus); }
            set { SetColumnValue(Columns.PointBonus, value); }
        }

        [XmlAttribute("Total")]
        [Bindable(true)]
        public double? Total
        {
            get { return GetColumnValue<double?>(Columns.Total); }
            set { SetColumnValue(Columns.Total, value); }
        }

        [XmlAttribute("KpiTarget")]
        [Bindable(true)]
        public double? KpiTarget
        {
            get { return GetColumnValue<double?>(Columns.KpiTarget); }
            set { SetColumnValue(Columns.KpiTarget, value); }
        }

        [XmlAttribute("CreateId")]
        [Bindable(true)]
        public string CreateId
        {
            get { return GetColumnValue<string>(Columns.CreateId); }
            set { SetColumnValue(Columns.CreateId, value); }
        }

        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime? CreateTime
        {
            get { return GetColumnValue<DateTime?>(Columns.CreateTime); }
            set { SetColumnValue(Columns.CreateTime, value); }
        }

        [XmlAttribute("DealTarget")]
        [Bindable(true)]
        public int? DealTarget
        {
            get { return GetColumnValue<int?>(Columns.DealTarget); }
            set { SetColumnValue(Columns.DealTarget, value); }
        }


        #endregion

        #region Typed Columns


        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }



        public static TableSchema.TableColumn YearMonColumn
        {
            get { return Schema.Columns[1]; }
        }



        public static TableSchema.TableColumn SalesIdColumn
        {
            get { return Schema.Columns[2]; }
        }



        public static TableSchema.TableColumn ForecastRangeIdColumn
        {
            get { return Schema.Columns[3]; }
        }



        public static TableSchema.TableColumn RealRangeIdColumn
        {
            get { return Schema.Columns[4]; }
        }



        public static TableSchema.TableColumn RealVolumeColumn
        {
            get { return Schema.Columns[5]; }
        }



        public static TableSchema.TableColumn BonusRateColumn
        {
            get { return Schema.Columns[6]; }
        }



        public static TableSchema.TableColumn BonusColumn
        {
            get { return Schema.Columns[7]; }
        }



        public static TableSchema.TableColumn PointBonusColumn
        {
            get { return Schema.Columns[8]; }
        }



        public static TableSchema.TableColumn TotalColumn
        {
            get { return Schema.Columns[9]; }
        }



        public static TableSchema.TableColumn KpiTargetColumn
        {
            get { return Schema.Columns[10]; }
        }



        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[11]; }
        }



        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[12]; }
        }



        public static TableSchema.TableColumn DealTargetColumn
        {
            get { return Schema.Columns[13]; }
        }



        #endregion
        #region Columns Struct
        public struct Columns
        {
            public static string Id = @"ID";
            public static string YearMon = @"YEAR_MON";
            public static string SalesId = @"SALES_ID";
            public static string ForecastRangeId = @"FORECAST_RANGE_ID";
            public static string RealRangeId = @"REAL_RANGE_ID";
            public static string RealVolume = @"REAL_VOLUME";
            public static string BonusRate = @"BONUS_RATE";
            public static string Bonus = @"BONUS";
            public static string PointBonus = @"POINT_BONUS";
            public static string Total = @"TOTAL";
            public static string KpiTarget = @"KPI_TARGET";
            public static string CreateId = @"CREATE_ID";
            public static string CreateTime = @"CREATE_TIME";
            public static string DealTarget = @"DEAL_TARGET";

        }
        #endregion

        #region Update PK Collections

        #endregion

        #region Deep Save

        #endregion
    }
}

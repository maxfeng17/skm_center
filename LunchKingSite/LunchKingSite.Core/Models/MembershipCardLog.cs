using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the MembershipCardLog class.
	/// </summary>
    [Serializable]
	public partial class MembershipCardLogCollection : RepositoryList<MembershipCardLog, MembershipCardLogCollection>
	{	   
		public MembershipCardLogCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>MembershipCardLogCollection</returns>
		public MembershipCardLogCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                MembershipCardLog o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the membership_card_log table.
	/// </summary>
	[Serializable]
	public partial class MembershipCardLog : RepositoryRecord<MembershipCardLog>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public MembershipCardLog()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public MembershipCardLog(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("membership_card_log", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarMembershipCardIdBefore = new TableSchema.TableColumn(schema);
				colvarMembershipCardIdBefore.ColumnName = "membership_card_id_before";
				colvarMembershipCardIdBefore.DataType = DbType.Int32;
				colvarMembershipCardIdBefore.MaxLength = 0;
				colvarMembershipCardIdBefore.AutoIncrement = false;
				colvarMembershipCardIdBefore.IsNullable = false;
				colvarMembershipCardIdBefore.IsPrimaryKey = false;
				colvarMembershipCardIdBefore.IsForeignKey = false;
				colvarMembershipCardIdBefore.IsReadOnly = false;
				colvarMembershipCardIdBefore.DefaultSetting = @"";
				colvarMembershipCardIdBefore.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMembershipCardIdBefore);
				
				TableSchema.TableColumn colvarMembershipCardIdAfter = new TableSchema.TableColumn(schema);
				colvarMembershipCardIdAfter.ColumnName = "membership_card_id_after";
				colvarMembershipCardIdAfter.DataType = DbType.Int32;
				colvarMembershipCardIdAfter.MaxLength = 0;
				colvarMembershipCardIdAfter.AutoIncrement = false;
				colvarMembershipCardIdAfter.IsNullable = false;
				colvarMembershipCardIdAfter.IsPrimaryKey = false;
				colvarMembershipCardIdAfter.IsForeignKey = false;
				colvarMembershipCardIdAfter.IsReadOnly = false;
				colvarMembershipCardIdAfter.DefaultSetting = @"";
				colvarMembershipCardIdAfter.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMembershipCardIdAfter);
				
				TableSchema.TableColumn colvarUserMembershipCardId = new TableSchema.TableColumn(schema);
				colvarUserMembershipCardId.ColumnName = "user_membership_card_id";
				colvarUserMembershipCardId.DataType = DbType.Int32;
				colvarUserMembershipCardId.MaxLength = 0;
				colvarUserMembershipCardId.AutoIncrement = false;
				colvarUserMembershipCardId.IsNullable = true;
				colvarUserMembershipCardId.IsPrimaryKey = false;
				colvarUserMembershipCardId.IsForeignKey = false;
				colvarUserMembershipCardId.IsReadOnly = false;
				colvarUserMembershipCardId.DefaultSetting = @"";
				colvarUserMembershipCardId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUserMembershipCardId);
				
				TableSchema.TableColumn colvarMemo = new TableSchema.TableColumn(schema);
				colvarMemo.ColumnName = "memo";
				colvarMemo.DataType = DbType.String;
				colvarMemo.MaxLength = -1;
				colvarMemo.AutoIncrement = false;
				colvarMemo.IsNullable = false;
				colvarMemo.IsPrimaryKey = false;
				colvarMemo.IsForeignKey = false;
				colvarMemo.IsReadOnly = false;
				colvarMemo.DefaultSetting = @"";
				colvarMemo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMemo);
				
				TableSchema.TableColumn colvarType = new TableSchema.TableColumn(schema);
				colvarType.ColumnName = "type";
				colvarType.DataType = DbType.Byte;
				colvarType.MaxLength = 0;
				colvarType.AutoIncrement = false;
				colvarType.IsNullable = false;
				colvarType.IsPrimaryKey = false;
				colvarType.IsForeignKey = false;
				colvarType.IsReadOnly = false;
				
						colvarType.DefaultSetting = @"((0))";
				colvarType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarType);
				
				TableSchema.TableColumn colvarCreateId = new TableSchema.TableColumn(schema);
				colvarCreateId.ColumnName = "create_id";
				colvarCreateId.DataType = DbType.Int32;
				colvarCreateId.MaxLength = 0;
				colvarCreateId.AutoIncrement = false;
				colvarCreateId.IsNullable = false;
				colvarCreateId.IsPrimaryKey = false;
				colvarCreateId.IsForeignKey = false;
				colvarCreateId.IsReadOnly = false;
				colvarCreateId.DefaultSetting = @"";
				colvarCreateId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateId);
				
				TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
				colvarCreateTime.ColumnName = "create_time";
				colvarCreateTime.DataType = DbType.DateTime;
				colvarCreateTime.MaxLength = 0;
				colvarCreateTime.AutoIncrement = false;
				colvarCreateTime.IsNullable = false;
				colvarCreateTime.IsPrimaryKey = false;
				colvarCreateTime.IsForeignKey = false;
				colvarCreateTime.IsReadOnly = false;
				colvarCreateTime.DefaultSetting = @"";
				colvarCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCreateTime);
				
				TableSchema.TableColumn colvarPcpOrderGuid = new TableSchema.TableColumn(schema);
				colvarPcpOrderGuid.ColumnName = "pcp_order_guid";
				colvarPcpOrderGuid.DataType = DbType.Guid;
				colvarPcpOrderGuid.MaxLength = 0;
				colvarPcpOrderGuid.AutoIncrement = false;
				colvarPcpOrderGuid.IsNullable = true;
				colvarPcpOrderGuid.IsPrimaryKey = false;
				colvarPcpOrderGuid.IsForeignKey = false;
				colvarPcpOrderGuid.IsReadOnly = false;
				colvarPcpOrderGuid.DefaultSetting = @"";
				colvarPcpOrderGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPcpOrderGuid);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("membership_card_log",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("MembershipCardIdBefore")]
		[Bindable(true)]
		public int MembershipCardIdBefore 
		{
			get { return GetColumnValue<int>(Columns.MembershipCardIdBefore); }
			set { SetColumnValue(Columns.MembershipCardIdBefore, value); }
		}
		  
		[XmlAttribute("MembershipCardIdAfter")]
		[Bindable(true)]
		public int MembershipCardIdAfter 
		{
			get { return GetColumnValue<int>(Columns.MembershipCardIdAfter); }
			set { SetColumnValue(Columns.MembershipCardIdAfter, value); }
		}
		  
		[XmlAttribute("UserMembershipCardId")]
		[Bindable(true)]
		public int? UserMembershipCardId 
		{
			get { return GetColumnValue<int?>(Columns.UserMembershipCardId); }
			set { SetColumnValue(Columns.UserMembershipCardId, value); }
		}
		  
		[XmlAttribute("Memo")]
		[Bindable(true)]
		public string Memo 
		{
			get { return GetColumnValue<string>(Columns.Memo); }
			set { SetColumnValue(Columns.Memo, value); }
		}
		  
		[XmlAttribute("Type")]
		[Bindable(true)]
		public byte Type 
		{
			get { return GetColumnValue<byte>(Columns.Type); }
			set { SetColumnValue(Columns.Type, value); }
		}
		  
		[XmlAttribute("CreateId")]
		[Bindable(true)]
		public int CreateId 
		{
			get { return GetColumnValue<int>(Columns.CreateId); }
			set { SetColumnValue(Columns.CreateId, value); }
		}
		  
		[XmlAttribute("CreateTime")]
		[Bindable(true)]
		public DateTime CreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.CreateTime); }
			set { SetColumnValue(Columns.CreateTime, value); }
		}
		  
		[XmlAttribute("PcpOrderGuid")]
		[Bindable(true)]
		public Guid? PcpOrderGuid 
		{
			get { return GetColumnValue<Guid?>(Columns.PcpOrderGuid); }
			set { SetColumnValue(Columns.PcpOrderGuid, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn MembershipCardIdBeforeColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn MembershipCardIdAfterColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn UserMembershipCardIdColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn MemoColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn TypeColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateIdColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn PcpOrderGuidColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string MembershipCardIdBefore = @"membership_card_id_before";
			 public static string MembershipCardIdAfter = @"membership_card_id_after";
			 public static string UserMembershipCardId = @"user_membership_card_id";
			 public static string Memo = @"memo";
			 public static string Type = @"type";
			 public static string CreateId = @"create_id";
			 public static string CreateTime = @"create_time";
			 public static string PcpOrderGuid = @"pcp_order_guid";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm{
    /// <summary>
    /// Strongly-typed collection for the ViewPponExpiration class.
    /// </summary>
    [Serializable]
    public partial class ViewPponExpirationCollection : ReadOnlyList<ViewPponExpiration, ViewPponExpirationCollection>
    {        
        public ViewPponExpirationCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the view_ppon_expiration view.
    /// </summary>
    [Serializable]
    public partial class ViewPponExpiration : ReadOnlyRecord<ViewPponExpiration>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("view_ppon_expiration", TableType.View, DataService.GetInstance("LKSiteDB"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarBid = new TableSchema.TableColumn(schema);
                colvarBid.ColumnName = "bid";
                colvarBid.DataType = DbType.Guid;
                colvarBid.MaxLength = 0;
                colvarBid.AutoIncrement = false;
                colvarBid.IsNullable = false;
                colvarBid.IsPrimaryKey = false;
                colvarBid.IsForeignKey = false;
                colvarBid.IsReadOnly = false;
                
                schema.Columns.Add(colvarBid);
                
                TableSchema.TableColumn colvarSellerGuid = new TableSchema.TableColumn(schema);
                colvarSellerGuid.ColumnName = "seller_guid";
                colvarSellerGuid.DataType = DbType.Guid;
                colvarSellerGuid.MaxLength = 0;
                colvarSellerGuid.AutoIncrement = false;
                colvarSellerGuid.IsNullable = false;
                colvarSellerGuid.IsPrimaryKey = false;
                colvarSellerGuid.IsForeignKey = false;
                colvarSellerGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerGuid);
                
                TableSchema.TableColumn colvarStoreGuid = new TableSchema.TableColumn(schema);
                colvarStoreGuid.ColumnName = "store_guid";
                colvarStoreGuid.DataType = DbType.Guid;
                colvarStoreGuid.MaxLength = 0;
                colvarStoreGuid.AutoIncrement = false;
                colvarStoreGuid.IsNullable = true;
                colvarStoreGuid.IsPrimaryKey = false;
                colvarStoreGuid.IsForeignKey = false;
                colvarStoreGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarStoreGuid);
                
                TableSchema.TableColumn colvarBhDeliverTimeStart = new TableSchema.TableColumn(schema);
                colvarBhDeliverTimeStart.ColumnName = "bh_deliver_time_start";
                colvarBhDeliverTimeStart.DataType = DbType.DateTime;
                colvarBhDeliverTimeStart.MaxLength = 0;
                colvarBhDeliverTimeStart.AutoIncrement = false;
                colvarBhDeliverTimeStart.IsNullable = true;
                colvarBhDeliverTimeStart.IsPrimaryKey = false;
                colvarBhDeliverTimeStart.IsForeignKey = false;
                colvarBhDeliverTimeStart.IsReadOnly = false;
                
                schema.Columns.Add(colvarBhDeliverTimeStart);
                
                TableSchema.TableColumn colvarBhDeliverTimeEnd = new TableSchema.TableColumn(schema);
                colvarBhDeliverTimeEnd.ColumnName = "bh_deliver_time_end";
                colvarBhDeliverTimeEnd.DataType = DbType.DateTime;
                colvarBhDeliverTimeEnd.MaxLength = 0;
                colvarBhDeliverTimeEnd.AutoIncrement = false;
                colvarBhDeliverTimeEnd.IsNullable = true;
                colvarBhDeliverTimeEnd.IsPrimaryKey = false;
                colvarBhDeliverTimeEnd.IsForeignKey = false;
                colvarBhDeliverTimeEnd.IsReadOnly = false;
                
                schema.Columns.Add(colvarBhDeliverTimeEnd);
                
                TableSchema.TableColumn colvarBhChangedExpireDate = new TableSchema.TableColumn(schema);
                colvarBhChangedExpireDate.ColumnName = "bh_changed_expire_date";
                colvarBhChangedExpireDate.DataType = DbType.DateTime;
                colvarBhChangedExpireDate.MaxLength = 0;
                colvarBhChangedExpireDate.AutoIncrement = false;
                colvarBhChangedExpireDate.IsNullable = true;
                colvarBhChangedExpireDate.IsPrimaryKey = false;
                colvarBhChangedExpireDate.IsForeignKey = false;
                colvarBhChangedExpireDate.IsReadOnly = false;
                
                schema.Columns.Add(colvarBhChangedExpireDate);
                
                TableSchema.TableColumn colvarStoreChangedExpireDate = new TableSchema.TableColumn(schema);
                colvarStoreChangedExpireDate.ColumnName = "store_changed_expire_date";
                colvarStoreChangedExpireDate.DataType = DbType.DateTime;
                colvarStoreChangedExpireDate.MaxLength = 0;
                colvarStoreChangedExpireDate.AutoIncrement = false;
                colvarStoreChangedExpireDate.IsNullable = true;
                colvarStoreChangedExpireDate.IsPrimaryKey = false;
                colvarStoreChangedExpireDate.IsForeignKey = false;
                colvarStoreChangedExpireDate.IsReadOnly = false;
                
                schema.Columns.Add(colvarStoreChangedExpireDate);
                
                TableSchema.TableColumn colvarSellerCloseDownDate = new TableSchema.TableColumn(schema);
                colvarSellerCloseDownDate.ColumnName = "seller_close_down_date";
                colvarSellerCloseDownDate.DataType = DbType.DateTime;
                colvarSellerCloseDownDate.MaxLength = 0;
                colvarSellerCloseDownDate.AutoIncrement = false;
                colvarSellerCloseDownDate.IsNullable = true;
                colvarSellerCloseDownDate.IsPrimaryKey = false;
                colvarSellerCloseDownDate.IsForeignKey = false;
                colvarSellerCloseDownDate.IsReadOnly = false;
                
                schema.Columns.Add(colvarSellerCloseDownDate);
                
                TableSchema.TableColumn colvarStoreCloseDownDate = new TableSchema.TableColumn(schema);
                colvarStoreCloseDownDate.ColumnName = "store_close_down_date";
                colvarStoreCloseDownDate.DataType = DbType.DateTime;
                colvarStoreCloseDownDate.MaxLength = 0;
                colvarStoreCloseDownDate.AutoIncrement = false;
                colvarStoreCloseDownDate.IsNullable = true;
                colvarStoreCloseDownDate.IsPrimaryKey = false;
                colvarStoreCloseDownDate.IsForeignKey = false;
                colvarStoreCloseDownDate.IsReadOnly = false;
                
                schema.Columns.Add(colvarStoreCloseDownDate);
                
                TableSchema.TableColumn colvarBhOrderTimeStart = new TableSchema.TableColumn(schema);
                colvarBhOrderTimeStart.ColumnName = "bh_order_time_start";
                colvarBhOrderTimeStart.DataType = DbType.DateTime;
                colvarBhOrderTimeStart.MaxLength = 0;
                colvarBhOrderTimeStart.AutoIncrement = false;
                colvarBhOrderTimeStart.IsNullable = false;
                colvarBhOrderTimeStart.IsPrimaryKey = false;
                colvarBhOrderTimeStart.IsForeignKey = false;
                colvarBhOrderTimeStart.IsReadOnly = false;
                
                schema.Columns.Add(colvarBhOrderTimeStart);
                
                TableSchema.TableColumn colvarBhOrderTimeEnd = new TableSchema.TableColumn(schema);
                colvarBhOrderTimeEnd.ColumnName = "bh_order_time_end";
                colvarBhOrderTimeEnd.DataType = DbType.DateTime;
                colvarBhOrderTimeEnd.MaxLength = 0;
                colvarBhOrderTimeEnd.AutoIncrement = false;
                colvarBhOrderTimeEnd.IsNullable = false;
                colvarBhOrderTimeEnd.IsPrimaryKey = false;
                colvarBhOrderTimeEnd.IsForeignKey = false;
                colvarBhOrderTimeEnd.IsReadOnly = false;
                
                schema.Columns.Add(colvarBhOrderTimeEnd);
                
                TableSchema.TableColumn colvarVendorBillingModel = new TableSchema.TableColumn(schema);
                colvarVendorBillingModel.ColumnName = "vendor_billing_model";
                colvarVendorBillingModel.DataType = DbType.Int32;
                colvarVendorBillingModel.MaxLength = 0;
                colvarVendorBillingModel.AutoIncrement = false;
                colvarVendorBillingModel.IsNullable = false;
                colvarVendorBillingModel.IsPrimaryKey = false;
                colvarVendorBillingModel.IsForeignKey = false;
                colvarVendorBillingModel.IsReadOnly = false;
                
                schema.Columns.Add(colvarVendorBillingModel);
                
                TableSchema.TableColumn colvarRemittanceType = new TableSchema.TableColumn(schema);
                colvarRemittanceType.ColumnName = "remittance_type";
                colvarRemittanceType.DataType = DbType.Int32;
                colvarRemittanceType.MaxLength = 0;
                colvarRemittanceType.AutoIncrement = false;
                colvarRemittanceType.IsNullable = false;
                colvarRemittanceType.IsPrimaryKey = false;
                colvarRemittanceType.IsForeignKey = false;
                colvarRemittanceType.IsReadOnly = false;
                
                schema.Columns.Add(colvarRemittanceType);
                
                TableSchema.TableColumn colvarFinalBalanceSheetDate = new TableSchema.TableColumn(schema);
                colvarFinalBalanceSheetDate.ColumnName = "final_balance_sheet_date";
                colvarFinalBalanceSheetDate.DataType = DbType.DateTime;
                colvarFinalBalanceSheetDate.MaxLength = 0;
                colvarFinalBalanceSheetDate.AutoIncrement = false;
                colvarFinalBalanceSheetDate.IsNullable = true;
                colvarFinalBalanceSheetDate.IsPrimaryKey = false;
                colvarFinalBalanceSheetDate.IsForeignKey = false;
                colvarFinalBalanceSheetDate.IsReadOnly = false;
                
                schema.Columns.Add(colvarFinalBalanceSheetDate);
                
                TableSchema.TableColumn colvarBalanceSheetCreateDate = new TableSchema.TableColumn(schema);
                colvarBalanceSheetCreateDate.ColumnName = "balance_sheet_create_date";
                colvarBalanceSheetCreateDate.DataType = DbType.DateTime;
                colvarBalanceSheetCreateDate.MaxLength = 0;
                colvarBalanceSheetCreateDate.AutoIncrement = false;
                colvarBalanceSheetCreateDate.IsNullable = true;
                colvarBalanceSheetCreateDate.IsPrimaryKey = false;
                colvarBalanceSheetCreateDate.IsForeignKey = false;
                colvarBalanceSheetCreateDate.IsReadOnly = false;
                
                schema.Columns.Add(colvarBalanceSheetCreateDate);
                
                TableSchema.TableColumn colvarDeliveryType = new TableSchema.TableColumn(schema);
                colvarDeliveryType.ColumnName = "delivery_type";
                colvarDeliveryType.DataType = DbType.Int32;
                colvarDeliveryType.MaxLength = 0;
                colvarDeliveryType.AutoIncrement = false;
                colvarDeliveryType.IsNullable = true;
                colvarDeliveryType.IsPrimaryKey = false;
                colvarDeliveryType.IsForeignKey = false;
                colvarDeliveryType.IsReadOnly = false;
                
                schema.Columns.Add(colvarDeliveryType);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["LKSiteDB"].AddSchema("view_ppon_expiration",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewPponExpiration()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewPponExpiration(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewPponExpiration(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewPponExpiration(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("Bid")]
        [Bindable(true)]
        public Guid Bid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("bid");
		    }
            set 
		    {
			    SetColumnValue("bid", value);
            }
        }
	      
        [XmlAttribute("SellerGuid")]
        [Bindable(true)]
        public Guid SellerGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid>("seller_guid");
		    }
            set 
		    {
			    SetColumnValue("seller_guid", value);
            }
        }
	      
        [XmlAttribute("StoreGuid")]
        [Bindable(true)]
        public Guid? StoreGuid 
	    {
		    get
		    {
			    return GetColumnValue<Guid?>("store_guid");
		    }
            set 
		    {
			    SetColumnValue("store_guid", value);
            }
        }
	      
        [XmlAttribute("BhDeliverTimeStart")]
        [Bindable(true)]
        public DateTime? BhDeliverTimeStart 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("bh_deliver_time_start");
		    }
            set 
		    {
			    SetColumnValue("bh_deliver_time_start", value);
            }
        }
	      
        [XmlAttribute("BhDeliverTimeEnd")]
        [Bindable(true)]
        public DateTime? BhDeliverTimeEnd 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("bh_deliver_time_end");
		    }
            set 
		    {
			    SetColumnValue("bh_deliver_time_end", value);
            }
        }
	      
        [XmlAttribute("BhChangedExpireDate")]
        [Bindable(true)]
        public DateTime? BhChangedExpireDate 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("bh_changed_expire_date");
		    }
            set 
		    {
			    SetColumnValue("bh_changed_expire_date", value);
            }
        }
	      
        [XmlAttribute("StoreChangedExpireDate")]
        [Bindable(true)]
        public DateTime? StoreChangedExpireDate 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("store_changed_expire_date");
		    }
            set 
		    {
			    SetColumnValue("store_changed_expire_date", value);
            }
        }
	      
        [XmlAttribute("SellerCloseDownDate")]
        [Bindable(true)]
        public DateTime? SellerCloseDownDate 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("seller_close_down_date");
		    }
            set 
		    {
			    SetColumnValue("seller_close_down_date", value);
            }
        }
	      
        [XmlAttribute("StoreCloseDownDate")]
        [Bindable(true)]
        public DateTime? StoreCloseDownDate 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("store_close_down_date");
		    }
            set 
		    {
			    SetColumnValue("store_close_down_date", value);
            }
        }
	      
        [XmlAttribute("BhOrderTimeStart")]
        [Bindable(true)]
        public DateTime BhOrderTimeStart 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("bh_order_time_start");
		    }
            set 
		    {
			    SetColumnValue("bh_order_time_start", value);
            }
        }
	      
        [XmlAttribute("BhOrderTimeEnd")]
        [Bindable(true)]
        public DateTime BhOrderTimeEnd 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("bh_order_time_end");
		    }
            set 
		    {
			    SetColumnValue("bh_order_time_end", value);
            }
        }
	      
        [XmlAttribute("VendorBillingModel")]
        [Bindable(true)]
        public int VendorBillingModel 
	    {
		    get
		    {
			    return GetColumnValue<int>("vendor_billing_model");
		    }
            set 
		    {
			    SetColumnValue("vendor_billing_model", value);
            }
        }
	      
        [XmlAttribute("RemittanceType")]
        [Bindable(true)]
        public int RemittanceType 
	    {
		    get
		    {
			    return GetColumnValue<int>("remittance_type");
		    }
            set 
		    {
			    SetColumnValue("remittance_type", value);
            }
        }
	      
        [XmlAttribute("FinalBalanceSheetDate")]
        [Bindable(true)]
        public DateTime? FinalBalanceSheetDate 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("final_balance_sheet_date");
		    }
            set 
		    {
			    SetColumnValue("final_balance_sheet_date", value);
            }
        }
	      
        [XmlAttribute("BalanceSheetCreateDate")]
        [Bindable(true)]
        public DateTime? BalanceSheetCreateDate 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("balance_sheet_create_date");
		    }
            set 
		    {
			    SetColumnValue("balance_sheet_create_date", value);
            }
        }
	      
        [XmlAttribute("DeliveryType")]
        [Bindable(true)]
        public int? DeliveryType 
	    {
		    get
		    {
			    return GetColumnValue<int?>("delivery_type");
		    }
            set 
		    {
			    SetColumnValue("delivery_type", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string Bid = @"bid";
            
            public static string SellerGuid = @"seller_guid";
            
            public static string StoreGuid = @"store_guid";
            
            public static string BhDeliverTimeStart = @"bh_deliver_time_start";
            
            public static string BhDeliverTimeEnd = @"bh_deliver_time_end";
            
            public static string BhChangedExpireDate = @"bh_changed_expire_date";
            
            public static string StoreChangedExpireDate = @"store_changed_expire_date";
            
            public static string SellerCloseDownDate = @"seller_close_down_date";
            
            public static string StoreCloseDownDate = @"store_close_down_date";
            
            public static string BhOrderTimeStart = @"bh_order_time_start";
            
            public static string BhOrderTimeEnd = @"bh_order_time_end";
            
            public static string VendorBillingModel = @"vendor_billing_model";
            
            public static string RemittanceType = @"remittance_type";
            
            public static string FinalBalanceSheetDate = @"final_balance_sheet_date";
            
            public static string BalanceSheetCreateDate = @"balance_sheet_create_date";
            
            public static string DeliveryType = @"delivery_type";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}

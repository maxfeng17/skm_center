using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the ExternalDealRelationStore class.
	/// </summary>
    [Serializable]
	public partial class ExternalDealRelationStoreCollection : RepositoryList<ExternalDealRelationStore, ExternalDealRelationStoreCollection>
	{	   
		public ExternalDealRelationStoreCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>ExternalDealRelationStoreCollection</returns>
		public ExternalDealRelationStoreCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                ExternalDealRelationStore o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the external_deal_relation_store table.
	/// </summary>
	[Serializable]
	public partial class ExternalDealRelationStore : RepositoryRecord<ExternalDealRelationStore>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public ExternalDealRelationStore()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public ExternalDealRelationStore(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("external_deal_relation_store", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "Id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarDealGuid = new TableSchema.TableColumn(schema);
				colvarDealGuid.ColumnName = "deal_guid";
				colvarDealGuid.DataType = DbType.Guid;
				colvarDealGuid.MaxLength = 0;
				colvarDealGuid.AutoIncrement = false;
				colvarDealGuid.IsNullable = false;
				colvarDealGuid.IsPrimaryKey = false;
				colvarDealGuid.IsForeignKey = false;
				colvarDealGuid.IsReadOnly = false;
				colvarDealGuid.DefaultSetting = @"";
				colvarDealGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDealGuid);
				
				TableSchema.TableColumn colvarStoreGuid = new TableSchema.TableColumn(schema);
				colvarStoreGuid.ColumnName = "store_guid";
				colvarStoreGuid.DataType = DbType.Guid;
				colvarStoreGuid.MaxLength = 0;
				colvarStoreGuid.AutoIncrement = false;
				colvarStoreGuid.IsNullable = false;
				colvarStoreGuid.IsPrimaryKey = false;
				colvarStoreGuid.IsForeignKey = false;
				colvarStoreGuid.IsReadOnly = false;
				colvarStoreGuid.DefaultSetting = @"";
				colvarStoreGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarStoreGuid);
				
				TableSchema.TableColumn colvarSellerGuid = new TableSchema.TableColumn(schema);
				colvarSellerGuid.ColumnName = "seller_guid";
				colvarSellerGuid.DataType = DbType.Guid;
				colvarSellerGuid.MaxLength = 0;
				colvarSellerGuid.AutoIncrement = false;
				colvarSellerGuid.IsNullable = true;
				colvarSellerGuid.IsPrimaryKey = false;
				colvarSellerGuid.IsForeignKey = false;
				colvarSellerGuid.IsReadOnly = false;
				colvarSellerGuid.DefaultSetting = @"";
				colvarSellerGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSellerGuid);
				
				TableSchema.TableColumn colvarModifyTime = new TableSchema.TableColumn(schema);
				colvarModifyTime.ColumnName = "modify_time";
				colvarModifyTime.DataType = DbType.DateTime;
				colvarModifyTime.MaxLength = 0;
				colvarModifyTime.AutoIncrement = false;
				colvarModifyTime.IsNullable = false;
				colvarModifyTime.IsPrimaryKey = false;
				colvarModifyTime.IsForeignKey = false;
				colvarModifyTime.IsReadOnly = false;
				colvarModifyTime.DefaultSetting = @"";
				colvarModifyTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarModifyTime);
				
				TableSchema.TableColumn colvarBid = new TableSchema.TableColumn(schema);
				colvarBid.ColumnName = "bid";
				colvarBid.DataType = DbType.Guid;
				colvarBid.MaxLength = 0;
				colvarBid.AutoIncrement = false;
				colvarBid.IsNullable = true;
				colvarBid.IsPrimaryKey = false;
				colvarBid.IsForeignKey = false;
				colvarBid.IsReadOnly = false;
				colvarBid.DefaultSetting = @"";
				colvarBid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBid);
				
				TableSchema.TableColumn colvarIsDel = new TableSchema.TableColumn(schema);
				colvarIsDel.ColumnName = "is_del";
				colvarIsDel.DataType = DbType.Boolean;
				colvarIsDel.MaxLength = 0;
				colvarIsDel.AutoIncrement = false;
				colvarIsDel.IsNullable = false;
				colvarIsDel.IsPrimaryKey = false;
				colvarIsDel.IsForeignKey = false;
				colvarIsDel.IsReadOnly = false;
				
						colvarIsDel.DefaultSetting = @"((0))";
				colvarIsDel.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsDel);
				
				TableSchema.TableColumn colvarParentSellerGuid = new TableSchema.TableColumn(schema);
				colvarParentSellerGuid.ColumnName = "parent_seller_guid";
				colvarParentSellerGuid.DataType = DbType.Guid;
				colvarParentSellerGuid.MaxLength = 0;
				colvarParentSellerGuid.AutoIncrement = false;
				colvarParentSellerGuid.IsNullable = false;
				colvarParentSellerGuid.IsPrimaryKey = false;
				colvarParentSellerGuid.IsForeignKey = false;
				colvarParentSellerGuid.IsReadOnly = false;
				
						colvarParentSellerGuid.DefaultSetting = @"(CONVERT([uniqueidentifier],CONVERT([binary],(0),(0)),(0)))";
				colvarParentSellerGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarParentSellerGuid);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("external_deal_relation_store",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("DealGuid")]
		[Bindable(true)]
		public Guid DealGuid 
		{
			get { return GetColumnValue<Guid>(Columns.DealGuid); }
			set { SetColumnValue(Columns.DealGuid, value); }
		}
		  
		[XmlAttribute("StoreGuid")]
		[Bindable(true)]
		public Guid StoreGuid 
		{
			get { return GetColumnValue<Guid>(Columns.StoreGuid); }
			set { SetColumnValue(Columns.StoreGuid, value); }
		}
		  
		[XmlAttribute("SellerGuid")]
		[Bindable(true)]
		public Guid? SellerGuid 
		{
			get { return GetColumnValue<Guid?>(Columns.SellerGuid); }
			set { SetColumnValue(Columns.SellerGuid, value); }
		}
		  
		[XmlAttribute("ModifyTime")]
		[Bindable(true)]
		public DateTime ModifyTime 
		{
			get { return GetColumnValue<DateTime>(Columns.ModifyTime); }
			set { SetColumnValue(Columns.ModifyTime, value); }
		}
		  
		[XmlAttribute("Bid")]
		[Bindable(true)]
		public Guid? Bid 
		{
			get { return GetColumnValue<Guid?>(Columns.Bid); }
			set { SetColumnValue(Columns.Bid, value); }
		}
		  
		[XmlAttribute("IsDel")]
		[Bindable(true)]
		public bool IsDel 
		{
			get { return GetColumnValue<bool>(Columns.IsDel); }
			set { SetColumnValue(Columns.IsDel, value); }
		}
		  
		[XmlAttribute("ParentSellerGuid")]
		[Bindable(true)]
		public Guid ParentSellerGuid 
		{
			get { return GetColumnValue<Guid>(Columns.ParentSellerGuid); }
			set { SetColumnValue(Columns.ParentSellerGuid, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn DealGuidColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn StoreGuidColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn SellerGuidColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn ModifyTimeColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn BidColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn IsDelColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn ParentSellerGuidColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"Id";
			 public static string DealGuid = @"deal_guid";
			 public static string StoreGuid = @"store_guid";
			 public static string SellerGuid = @"seller_guid";
			 public static string ModifyTime = @"modify_time";
			 public static string Bid = @"bid";
			 public static string IsDel = @"is_del";
			 public static string ParentSellerGuid = @"parent_seller_guid";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

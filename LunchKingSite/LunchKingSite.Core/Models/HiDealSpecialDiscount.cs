using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the HiDealSpecialDiscount class.
	/// </summary>
    [Serializable]
	public partial class HiDealSpecialDiscountCollection : RepositoryList<HiDealSpecialDiscount, HiDealSpecialDiscountCollection>
	{	   
		public HiDealSpecialDiscountCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>HiDealSpecialDiscountCollection</returns>
		public HiDealSpecialDiscountCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                HiDealSpecialDiscount o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the hi_deal_special_discount table.
	/// </summary>
	[Serializable]
	public partial class HiDealSpecialDiscount : RepositoryRecord<HiDealSpecialDiscount>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public HiDealSpecialDiscount()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public HiDealSpecialDiscount(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("hi_deal_special_discount", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "id";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarHiDealId = new TableSchema.TableColumn(schema);
				colvarHiDealId.ColumnName = "hi_deal_id";
				colvarHiDealId.DataType = DbType.Int32;
				colvarHiDealId.MaxLength = 0;
				colvarHiDealId.AutoIncrement = false;
				colvarHiDealId.IsNullable = false;
				colvarHiDealId.IsPrimaryKey = false;
				colvarHiDealId.IsForeignKey = true;
				colvarHiDealId.IsReadOnly = false;
				colvarHiDealId.DefaultSetting = @"";
				
					colvarHiDealId.ForeignKeyTableName = "hi_deal_deal";
				schema.Columns.Add(colvarHiDealId);
				
				TableSchema.TableColumn colvarCodeId = new TableSchema.TableColumn(schema);
				colvarCodeId.ColumnName = "code_id";
				colvarCodeId.DataType = DbType.Int32;
				colvarCodeId.MaxLength = 0;
				colvarCodeId.AutoIncrement = false;
				colvarCodeId.IsNullable = false;
				colvarCodeId.IsPrimaryKey = false;
				colvarCodeId.IsForeignKey = true;
				colvarCodeId.IsReadOnly = false;
				colvarCodeId.DefaultSetting = @"";
				
					colvarCodeId.ForeignKeyTableName = "system_code";
				schema.Columns.Add(colvarCodeId);
				
				TableSchema.TableColumn colvarCodeGroup = new TableSchema.TableColumn(schema);
				colvarCodeGroup.ColumnName = "code_group";
				colvarCodeGroup.DataType = DbType.AnsiString;
				colvarCodeGroup.MaxLength = 50;
				colvarCodeGroup.AutoIncrement = false;
				colvarCodeGroup.IsNullable = false;
				colvarCodeGroup.IsPrimaryKey = false;
				colvarCodeGroup.IsForeignKey = true;
				colvarCodeGroup.IsReadOnly = false;
				colvarCodeGroup.DefaultSetting = @"";
				
					colvarCodeGroup.ForeignKeyTableName = "system_code";
				schema.Columns.Add(colvarCodeGroup);
				
				TableSchema.TableColumn colvarCodeName = new TableSchema.TableColumn(schema);
				colvarCodeName.ColumnName = "code_name";
				colvarCodeName.DataType = DbType.String;
				colvarCodeName.MaxLength = 50;
				colvarCodeName.AutoIncrement = false;
				colvarCodeName.IsNullable = true;
				colvarCodeName.IsPrimaryKey = false;
				colvarCodeName.IsForeignKey = false;
				colvarCodeName.IsReadOnly = false;
				colvarCodeName.DefaultSetting = @"";
				colvarCodeName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCodeName);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("hi_deal_special_discount",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("HiDealId")]
		[Bindable(true)]
		public int HiDealId 
		{
			get { return GetColumnValue<int>(Columns.HiDealId); }
			set { SetColumnValue(Columns.HiDealId, value); }
		}
		  
		[XmlAttribute("CodeId")]
		[Bindable(true)]
		public int CodeId 
		{
			get { return GetColumnValue<int>(Columns.CodeId); }
			set { SetColumnValue(Columns.CodeId, value); }
		}
		  
		[XmlAttribute("CodeGroup")]
		[Bindable(true)]
		public string CodeGroup 
		{
			get { return GetColumnValue<string>(Columns.CodeGroup); }
			set { SetColumnValue(Columns.CodeGroup, value); }
		}
		  
		[XmlAttribute("CodeName")]
		[Bindable(true)]
		public string CodeName 
		{
			get { return GetColumnValue<string>(Columns.CodeName); }
			set { SetColumnValue(Columns.CodeName, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (2)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn HiDealIdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn CodeIdColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn CodeGroupColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn CodeNameColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"id";
			 public static string HiDealId = @"hi_deal_id";
			 public static string CodeId = @"code_id";
			 public static string CodeGroup = @"code_group";
			 public static string CodeName = @"code_name";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

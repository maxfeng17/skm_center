using System;
using System.ComponentModel;
using System.Data;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the ItemAccessoryGroupList class.
	/// </summary>
    [Serializable]
	public partial class ItemAccessoryGroupListCollection : RepositoryList<ItemAccessoryGroupList, ItemAccessoryGroupListCollection>
	{	   
		public ItemAccessoryGroupListCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>ItemAccessoryGroupListCollection</returns>
		public ItemAccessoryGroupListCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                ItemAccessoryGroupList o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the item_accessory_group_list table.
	/// </summary>
	[Serializable]
	public partial class ItemAccessoryGroupList : RepositoryRecord<ItemAccessoryGroupList>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public ItemAccessoryGroupList()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public ItemAccessoryGroupList(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("item_accessory_group_list", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarGuid = new TableSchema.TableColumn(schema);
				colvarGuid.ColumnName = "GUID";
				colvarGuid.DataType = DbType.Guid;
				colvarGuid.MaxLength = 0;
				colvarGuid.AutoIncrement = false;
				colvarGuid.IsNullable = false;
				colvarGuid.IsPrimaryKey = true;
				colvarGuid.IsForeignKey = false;
				colvarGuid.IsReadOnly = false;
				colvarGuid.DefaultSetting = @"";
				colvarGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGuid);
				
				TableSchema.TableColumn colvarItemGuid = new TableSchema.TableColumn(schema);
				colvarItemGuid.ColumnName = "item_GUID";
				colvarItemGuid.DataType = DbType.Guid;
				colvarItemGuid.MaxLength = 0;
				colvarItemGuid.AutoIncrement = false;
				colvarItemGuid.IsNullable = false;
				colvarItemGuid.IsPrimaryKey = false;
				colvarItemGuid.IsForeignKey = true;
				colvarItemGuid.IsReadOnly = false;
				colvarItemGuid.DefaultSetting = @"";
				
					colvarItemGuid.ForeignKeyTableName = "item";
				schema.Columns.Add(colvarItemGuid);
				
				TableSchema.TableColumn colvarAccessoryGroupGuid = new TableSchema.TableColumn(schema);
				colvarAccessoryGroupGuid.ColumnName = "accessory_group_GUID";
				colvarAccessoryGroupGuid.DataType = DbType.Guid;
				colvarAccessoryGroupGuid.MaxLength = 0;
				colvarAccessoryGroupGuid.AutoIncrement = false;
				colvarAccessoryGroupGuid.IsNullable = false;
				colvarAccessoryGroupGuid.IsPrimaryKey = false;
				colvarAccessoryGroupGuid.IsForeignKey = true;
				colvarAccessoryGroupGuid.IsReadOnly = false;
				colvarAccessoryGroupGuid.DefaultSetting = @"";
				
					colvarAccessoryGroupGuid.ForeignKeyTableName = "accessory_group";
				schema.Columns.Add(colvarAccessoryGroupGuid);
				
				TableSchema.TableColumn colvarSequence = new TableSchema.TableColumn(schema);
				colvarSequence.ColumnName = "sequence";
				colvarSequence.DataType = DbType.Int32;
				colvarSequence.MaxLength = 0;
				colvarSequence.AutoIncrement = false;
				colvarSequence.IsNullable = true;
				colvarSequence.IsPrimaryKey = false;
				colvarSequence.IsForeignKey = false;
				colvarSequence.IsReadOnly = false;
				colvarSequence.DefaultSetting = @"";
				colvarSequence.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSequence);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("item_accessory_group_list",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Guid")]
		[Bindable(true)]
		public Guid Guid 
		{
			get { return GetColumnValue<Guid>(Columns.Guid); }
			set { SetColumnValue(Columns.Guid, value); }
		}
		  
		[XmlAttribute("ItemGuid")]
		[Bindable(true)]
		public Guid ItemGuid 
		{
			get { return GetColumnValue<Guid>(Columns.ItemGuid); }
			set { SetColumnValue(Columns.ItemGuid, value); }
		}
		  
		[XmlAttribute("AccessoryGroupGuid")]
		[Bindable(true)]
		public Guid AccessoryGroupGuid 
		{
			get { return GetColumnValue<Guid>(Columns.AccessoryGroupGuid); }
			set { SetColumnValue(Columns.AccessoryGroupGuid, value); }
		}
		  
		[XmlAttribute("Sequence")]
		[Bindable(true)]
		public int? Sequence 
		{
			get { return GetColumnValue<int?>(Columns.Sequence); }
			set { SetColumnValue(Columns.Sequence, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (2)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn GuidColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn ItemGuidColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn AccessoryGroupGuidColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn SequenceColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Guid = @"GUID";
			 public static string ItemGuid = @"item_GUID";
			 public static string AccessoryGroupGuid = @"accessory_group_GUID";
			 public static string Sequence = @"sequence";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

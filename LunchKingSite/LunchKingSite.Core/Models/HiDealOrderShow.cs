using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
	/// <summary>
	/// Strongly-typed collection for the HiDealOrderShow class.
	/// </summary>
    [Serializable]
	public partial class HiDealOrderShowCollection : RepositoryList<HiDealOrderShow, HiDealOrderShowCollection>
	{	   
		public HiDealOrderShowCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>HiDealOrderShowCollection</returns>
		public HiDealOrderShowCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                HiDealOrderShow o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the hi_deal_order_show table.
	/// </summary>
	[Serializable]
	public partial class HiDealOrderShow : RepositoryRecord<HiDealOrderShow>, IRecordBase
	{
		#region .ctors and Default Settings
		
		public HiDealOrderShow()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public HiDealOrderShow(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("hi_deal_order_show", TableType.Table, DataService.GetInstance("LKSiteDB"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarOrderGuid = new TableSchema.TableColumn(schema);
				colvarOrderGuid.ColumnName = "order_guid";
				colvarOrderGuid.DataType = DbType.Guid;
				colvarOrderGuid.MaxLength = 0;
				colvarOrderGuid.AutoIncrement = false;
				colvarOrderGuid.IsNullable = false;
				colvarOrderGuid.IsPrimaryKey = true;
				colvarOrderGuid.IsForeignKey = false;
				colvarOrderGuid.IsReadOnly = false;
				colvarOrderGuid.DefaultSetting = @"";
				colvarOrderGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderGuid);
				
				TableSchema.TableColumn colvarOrderPk = new TableSchema.TableColumn(schema);
				colvarOrderPk.ColumnName = "order_pk";
				colvarOrderPk.DataType = DbType.Int32;
				colvarOrderPk.MaxLength = 0;
				colvarOrderPk.AutoIncrement = false;
				colvarOrderPk.IsNullable = false;
				colvarOrderPk.IsPrimaryKey = false;
				colvarOrderPk.IsForeignKey = true;
				colvarOrderPk.IsReadOnly = false;
				colvarOrderPk.DefaultSetting = @"";
				
					colvarOrderPk.ForeignKeyTableName = "hi_deal_order";
				schema.Columns.Add(colvarOrderPk);
				
				TableSchema.TableColumn colvarOrderId = new TableSchema.TableColumn(schema);
				colvarOrderId.ColumnName = "order_id";
				colvarOrderId.DataType = DbType.AnsiString;
				colvarOrderId.MaxLength = 30;
				colvarOrderId.AutoIncrement = false;
				colvarOrderId.IsNullable = false;
				colvarOrderId.IsPrimaryKey = false;
				colvarOrderId.IsForeignKey = false;
				colvarOrderId.IsReadOnly = false;
				colvarOrderId.DefaultSetting = @"";
				colvarOrderId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderId);
				
				TableSchema.TableColumn colvarProductId = new TableSchema.TableColumn(schema);
				colvarProductId.ColumnName = "product_id";
				colvarProductId.DataType = DbType.Int32;
				colvarProductId.MaxLength = 0;
				colvarProductId.AutoIncrement = false;
				colvarProductId.IsNullable = false;
				colvarProductId.IsPrimaryKey = false;
				colvarProductId.IsForeignKey = true;
				colvarProductId.IsReadOnly = false;
				colvarProductId.DefaultSetting = @"";
				
					colvarProductId.ForeignKeyTableName = "hi_deal_product";
				schema.Columns.Add(colvarProductId);
				
				TableSchema.TableColumn colvarProductName = new TableSchema.TableColumn(schema);
				colvarProductName.ColumnName = "product_name";
				colvarProductName.DataType = DbType.String;
				colvarProductName.MaxLength = 250;
				colvarProductName.AutoIncrement = false;
				colvarProductName.IsNullable = false;
				colvarProductName.IsPrimaryKey = false;
				colvarProductName.IsForeignKey = false;
				colvarProductName.IsReadOnly = false;
				colvarProductName.DefaultSetting = @"";
				colvarProductName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarProductName);
				
				TableSchema.TableColumn colvarDeliveryType = new TableSchema.TableColumn(schema);
				colvarDeliveryType.ColumnName = "delivery_type";
				colvarDeliveryType.DataType = DbType.Int32;
				colvarDeliveryType.MaxLength = 0;
				colvarDeliveryType.AutoIncrement = false;
				colvarDeliveryType.IsNullable = false;
				colvarDeliveryType.IsPrimaryKey = false;
				colvarDeliveryType.IsForeignKey = false;
				colvarDeliveryType.IsReadOnly = false;
				colvarDeliveryType.DefaultSetting = @"";
				colvarDeliveryType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDeliveryType);
				
				TableSchema.TableColumn colvarPaymentType = new TableSchema.TableColumn(schema);
				colvarPaymentType.ColumnName = "payment_type";
				colvarPaymentType.DataType = DbType.Int32;
				colvarPaymentType.MaxLength = 0;
				colvarPaymentType.AutoIncrement = false;
				colvarPaymentType.IsNullable = false;
				colvarPaymentType.IsPrimaryKey = false;
				colvarPaymentType.IsForeignKey = false;
				colvarPaymentType.IsReadOnly = false;
				colvarPaymentType.DefaultSetting = @"";
				colvarPaymentType.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPaymentType);
				
				TableSchema.TableColumn colvarOrderShowStatus = new TableSchema.TableColumn(schema);
				colvarOrderShowStatus.ColumnName = "order_show_status";
				colvarOrderShowStatus.DataType = DbType.Int32;
				colvarOrderShowStatus.MaxLength = 0;
				colvarOrderShowStatus.AutoIncrement = false;
				colvarOrderShowStatus.IsNullable = false;
				colvarOrderShowStatus.IsPrimaryKey = false;
				colvarOrderShowStatus.IsForeignKey = false;
				colvarOrderShowStatus.IsReadOnly = false;
				colvarOrderShowStatus.DefaultSetting = @"";
				colvarOrderShowStatus.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderShowStatus);
				
				TableSchema.TableColumn colvarRemainCount = new TableSchema.TableColumn(schema);
				colvarRemainCount.ColumnName = "remain_count";
				colvarRemainCount.DataType = DbType.Int32;
				colvarRemainCount.MaxLength = 0;
				colvarRemainCount.AutoIncrement = false;
				colvarRemainCount.IsNullable = true;
				colvarRemainCount.IsPrimaryKey = false;
				colvarRemainCount.IsForeignKey = false;
				colvarRemainCount.IsReadOnly = false;
				colvarRemainCount.DefaultSetting = @"";
				colvarRemainCount.ForeignKeyTableName = "";
				schema.Columns.Add(colvarRemainCount);
				
				TableSchema.TableColumn colvarOrderCreateTime = new TableSchema.TableColumn(schema);
				colvarOrderCreateTime.ColumnName = "order_create_time";
				colvarOrderCreateTime.DataType = DbType.DateTime;
				colvarOrderCreateTime.MaxLength = 0;
				colvarOrderCreateTime.AutoIncrement = false;
				colvarOrderCreateTime.IsNullable = false;
				colvarOrderCreateTime.IsPrimaryKey = false;
				colvarOrderCreateTime.IsForeignKey = false;
				colvarOrderCreateTime.IsReadOnly = false;
				colvarOrderCreateTime.DefaultSetting = @"";
				colvarOrderCreateTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderCreateTime);
				
				TableSchema.TableColumn colvarUseStartTime = new TableSchema.TableColumn(schema);
				colvarUseStartTime.ColumnName = "use_start_time";
				colvarUseStartTime.DataType = DbType.DateTime;
				colvarUseStartTime.MaxLength = 0;
				colvarUseStartTime.AutoIncrement = false;
				colvarUseStartTime.IsNullable = false;
				colvarUseStartTime.IsPrimaryKey = false;
				colvarUseStartTime.IsForeignKey = false;
				colvarUseStartTime.IsReadOnly = false;
				colvarUseStartTime.DefaultSetting = @"";
				colvarUseStartTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUseStartTime);
				
				TableSchema.TableColumn colvarUseEndTime = new TableSchema.TableColumn(schema);
				colvarUseEndTime.ColumnName = "use_end_time";
				colvarUseEndTime.DataType = DbType.DateTime;
				colvarUseEndTime.MaxLength = 0;
				colvarUseEndTime.AutoIncrement = false;
				colvarUseEndTime.IsNullable = false;
				colvarUseEndTime.IsPrimaryKey = false;
				colvarUseEndTime.IsForeignKey = false;
				colvarUseEndTime.IsReadOnly = false;
				colvarUseEndTime.DefaultSetting = @"";
				colvarUseEndTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUseEndTime);
				
				TableSchema.TableColumn colvarOrderEndTime = new TableSchema.TableColumn(schema);
				colvarOrderEndTime.ColumnName = "order_end_time";
				colvarOrderEndTime.DataType = DbType.DateTime;
				colvarOrderEndTime.MaxLength = 0;
				colvarOrderEndTime.AutoIncrement = false;
				colvarOrderEndTime.IsNullable = false;
				colvarOrderEndTime.IsPrimaryKey = false;
				colvarOrderEndTime.IsForeignKey = false;
				colvarOrderEndTime.IsReadOnly = false;
				colvarOrderEndTime.DefaultSetting = @"";
				colvarOrderEndTime.ForeignKeyTableName = "";
				schema.Columns.Add(colvarOrderEndTime);
				
				TableSchema.TableColumn colvarIsOrderComplete = new TableSchema.TableColumn(schema);
				colvarIsOrderComplete.ColumnName = "is_order_complete";
				colvarIsOrderComplete.DataType = DbType.Boolean;
				colvarIsOrderComplete.MaxLength = 0;
				colvarIsOrderComplete.AutoIncrement = false;
				colvarIsOrderComplete.IsNullable = false;
				colvarIsOrderComplete.IsPrimaryKey = false;
				colvarIsOrderComplete.IsForeignKey = false;
				colvarIsOrderComplete.IsReadOnly = false;
				
						colvarIsOrderComplete.DefaultSetting = @"((0))";
				colvarIsOrderComplete.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsOrderComplete);
				
				TableSchema.TableColumn colvarIsOrderNorefund = new TableSchema.TableColumn(schema);
				colvarIsOrderNorefund.ColumnName = "is_order_norefund";
				colvarIsOrderNorefund.DataType = DbType.Boolean;
				colvarIsOrderNorefund.MaxLength = 0;
				colvarIsOrderNorefund.AutoIncrement = false;
				colvarIsOrderNorefund.IsNullable = false;
				colvarIsOrderNorefund.IsPrimaryKey = false;
				colvarIsOrderNorefund.IsForeignKey = false;
				colvarIsOrderNorefund.IsReadOnly = false;
				
						colvarIsOrderNorefund.DefaultSetting = @"((0))";
				colvarIsOrderNorefund.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsOrderNorefund);
				
				TableSchema.TableColumn colvarIsOrderDaysNoRefund = new TableSchema.TableColumn(schema);
				colvarIsOrderDaysNoRefund.ColumnName = "is_order_days_no_refund";
				colvarIsOrderDaysNoRefund.DataType = DbType.Boolean;
				colvarIsOrderDaysNoRefund.MaxLength = 0;
				colvarIsOrderDaysNoRefund.AutoIncrement = false;
				colvarIsOrderDaysNoRefund.IsNullable = false;
				colvarIsOrderDaysNoRefund.IsPrimaryKey = false;
				colvarIsOrderDaysNoRefund.IsForeignKey = false;
				colvarIsOrderDaysNoRefund.IsReadOnly = false;
				
						colvarIsOrderDaysNoRefund.DefaultSetting = @"((0))";
				colvarIsOrderDaysNoRefund.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIsOrderDaysNoRefund);
				
				TableSchema.TableColumn colvarUserId = new TableSchema.TableColumn(schema);
				colvarUserId.ColumnName = "user_id";
				colvarUserId.DataType = DbType.Int32;
				colvarUserId.MaxLength = 0;
				colvarUserId.AutoIncrement = false;
				colvarUserId.IsNullable = false;
				colvarUserId.IsPrimaryKey = false;
				colvarUserId.IsForeignKey = false;
				colvarUserId.IsReadOnly = false;
				
						colvarUserId.DefaultSetting = @"((0))";
				colvarUserId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUserId);
				
				TableSchema.TableColumn colvarDealName = new TableSchema.TableColumn(schema);
				colvarDealName.ColumnName = "deal_name";
				colvarDealName.DataType = DbType.String;
				colvarDealName.MaxLength = 200;
				colvarDealName.AutoIncrement = false;
				colvarDealName.IsNullable = true;
				colvarDealName.IsPrimaryKey = false;
				colvarDealName.IsForeignKey = false;
				colvarDealName.IsReadOnly = false;
				colvarDealName.DefaultSetting = @"";
				colvarDealName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDealName);
				
				TableSchema.TableColumn colvarDealId = new TableSchema.TableColumn(schema);
				colvarDealId.ColumnName = "deal_id";
				colvarDealId.DataType = DbType.Int32;
				colvarDealId.MaxLength = 0;
				colvarDealId.AutoIncrement = false;
				colvarDealId.IsNullable = false;
				colvarDealId.IsPrimaryKey = false;
				colvarDealId.IsForeignKey = false;
				colvarDealId.IsReadOnly = false;
				colvarDealId.DefaultSetting = @"";
				colvarDealId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDealId);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["LKSiteDB"].AddSchema("hi_deal_order_show",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("OrderGuid")]
		[Bindable(true)]
		public Guid OrderGuid 
		{
			get { return GetColumnValue<Guid>(Columns.OrderGuid); }
			set { SetColumnValue(Columns.OrderGuid, value); }
		}
		  
		[XmlAttribute("OrderPk")]
		[Bindable(true)]
		public int OrderPk 
		{
			get { return GetColumnValue<int>(Columns.OrderPk); }
			set { SetColumnValue(Columns.OrderPk, value); }
		}
		  
		[XmlAttribute("OrderId")]
		[Bindable(true)]
		public string OrderId 
		{
			get { return GetColumnValue<string>(Columns.OrderId); }
			set { SetColumnValue(Columns.OrderId, value); }
		}
		  
		[XmlAttribute("ProductId")]
		[Bindable(true)]
		public int ProductId 
		{
			get { return GetColumnValue<int>(Columns.ProductId); }
			set { SetColumnValue(Columns.ProductId, value); }
		}
		  
		[XmlAttribute("ProductName")]
		[Bindable(true)]
		public string ProductName 
		{
			get { return GetColumnValue<string>(Columns.ProductName); }
			set { SetColumnValue(Columns.ProductName, value); }
		}
		  
		[XmlAttribute("DeliveryType")]
		[Bindable(true)]
		public int DeliveryType 
		{
			get { return GetColumnValue<int>(Columns.DeliveryType); }
			set { SetColumnValue(Columns.DeliveryType, value); }
		}
		  
		[XmlAttribute("PaymentType")]
		[Bindable(true)]
		public int PaymentType 
		{
			get { return GetColumnValue<int>(Columns.PaymentType); }
			set { SetColumnValue(Columns.PaymentType, value); }
		}
		  
		[XmlAttribute("OrderShowStatus")]
		[Bindable(true)]
		public int OrderShowStatus 
		{
			get { return GetColumnValue<int>(Columns.OrderShowStatus); }
			set { SetColumnValue(Columns.OrderShowStatus, value); }
		}
		  
		[XmlAttribute("RemainCount")]
		[Bindable(true)]
		public int? RemainCount 
		{
			get { return GetColumnValue<int?>(Columns.RemainCount); }
			set { SetColumnValue(Columns.RemainCount, value); }
		}
		  
		[XmlAttribute("OrderCreateTime")]
		[Bindable(true)]
		public DateTime OrderCreateTime 
		{
			get { return GetColumnValue<DateTime>(Columns.OrderCreateTime); }
			set { SetColumnValue(Columns.OrderCreateTime, value); }
		}
		  
		[XmlAttribute("UseStartTime")]
		[Bindable(true)]
		public DateTime UseStartTime 
		{
			get { return GetColumnValue<DateTime>(Columns.UseStartTime); }
			set { SetColumnValue(Columns.UseStartTime, value); }
		}
		  
		[XmlAttribute("UseEndTime")]
		[Bindable(true)]
		public DateTime UseEndTime 
		{
			get { return GetColumnValue<DateTime>(Columns.UseEndTime); }
			set { SetColumnValue(Columns.UseEndTime, value); }
		}
		  
		[XmlAttribute("OrderEndTime")]
		[Bindable(true)]
		public DateTime OrderEndTime 
		{
			get { return GetColumnValue<DateTime>(Columns.OrderEndTime); }
			set { SetColumnValue(Columns.OrderEndTime, value); }
		}
		  
		[XmlAttribute("IsOrderComplete")]
		[Bindable(true)]
		public bool IsOrderComplete 
		{
			get { return GetColumnValue<bool>(Columns.IsOrderComplete); }
			set { SetColumnValue(Columns.IsOrderComplete, value); }
		}
		  
		[XmlAttribute("IsOrderNorefund")]
		[Bindable(true)]
		public bool IsOrderNorefund 
		{
			get { return GetColumnValue<bool>(Columns.IsOrderNorefund); }
			set { SetColumnValue(Columns.IsOrderNorefund, value); }
		}
		  
		[XmlAttribute("IsOrderDaysNoRefund")]
		[Bindable(true)]
		public bool IsOrderDaysNoRefund 
		{
			get { return GetColumnValue<bool>(Columns.IsOrderDaysNoRefund); }
			set { SetColumnValue(Columns.IsOrderDaysNoRefund, value); }
		}
		  
		[XmlAttribute("UserId")]
		[Bindable(true)]
		public int UserId 
		{
			get { return GetColumnValue<int>(Columns.UserId); }
			set { SetColumnValue(Columns.UserId, value); }
		}
		  
		[XmlAttribute("DealName")]
		[Bindable(true)]
		public string DealName 
		{
			get { return GetColumnValue<string>(Columns.DealName); }
			set { SetColumnValue(Columns.DealName, value); }
		}
		  
		[XmlAttribute("DealId")]
		[Bindable(true)]
		public int DealId 
		{
			get { return GetColumnValue<int>(Columns.DealId); }
			set { SetColumnValue(Columns.DealId, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (2)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn OrderGuidColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn OrderPkColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn OrderIdColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn ProductIdColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn ProductNameColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn DeliveryTypeColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn PaymentTypeColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn OrderShowStatusColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn RemainCountColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn OrderCreateTimeColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn UseStartTimeColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        public static TableSchema.TableColumn UseEndTimeColumn
        {
            get { return Schema.Columns[11]; }
        }
        
        
        
        public static TableSchema.TableColumn OrderEndTimeColumn
        {
            get { return Schema.Columns[12]; }
        }
        
        
        
        public static TableSchema.TableColumn IsOrderCompleteColumn
        {
            get { return Schema.Columns[13]; }
        }
        
        
        
        public static TableSchema.TableColumn IsOrderNorefundColumn
        {
            get { return Schema.Columns[14]; }
        }
        
        
        
        public static TableSchema.TableColumn IsOrderDaysNoRefundColumn
        {
            get { return Schema.Columns[15]; }
        }
        
        
        
        public static TableSchema.TableColumn UserIdColumn
        {
            get { return Schema.Columns[16]; }
        }
        
        
        
        public static TableSchema.TableColumn DealNameColumn
        {
            get { return Schema.Columns[17]; }
        }
        
        
        
        public static TableSchema.TableColumn DealIdColumn
        {
            get { return Schema.Columns[18]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string OrderGuid = @"order_guid";
			 public static string OrderPk = @"order_pk";
			 public static string OrderId = @"order_id";
			 public static string ProductId = @"product_id";
			 public static string ProductName = @"product_name";
			 public static string DeliveryType = @"delivery_type";
			 public static string PaymentType = @"payment_type";
			 public static string OrderShowStatus = @"order_show_status";
			 public static string RemainCount = @"remain_count";
			 public static string OrderCreateTime = @"order_create_time";
			 public static string UseStartTime = @"use_start_time";
			 public static string UseEndTime = @"use_end_time";
			 public static string OrderEndTime = @"order_end_time";
			 public static string IsOrderComplete = @"is_order_complete";
			 public static string IsOrderNorefund = @"is_order_norefund";
			 public static string IsOrderDaysNoRefund = @"is_order_days_no_refund";
			 public static string UserId = @"user_id";
			 public static string DealName = @"deal_name";
			 public static string DealId = @"deal_id";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}

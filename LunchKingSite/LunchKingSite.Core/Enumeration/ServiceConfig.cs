﻿using System;
using System.ComponentModel;
using LunchKingSite.Core.Component;

namespace LunchKingSite.Core.Enumeration
{
    public class ServiceConfig
    {

        /// <summary>
        /// 定義客服回覆會員的緊急程度, Normal = 普通, High= 急, Extreme = 特急
        /// </summary>
        public enum replyPriority
        {
            Normal = 0,
            High = 1,
            Extreme = 2,
        }
        public struct replyPriorityString
        {
            public const string Normal = "普通";
            public const string High = "急";
            public const string Extreme = "特急";
        }
        public struct statusCode
        {
            public const string Wait = "wait";
            public const string Process = "process";
            public const string Complete = "complete";
        }

        public struct statusString
        {
            public const string Wait = "待處理";
            public const string Process = "處理中";
            public const string Complete = "完成";
        }


    }

    public enum statusConvert
    {
        [Description("未認領")]
        wait = 0,
        [Description("處理中")]
        process = 1,
        /// <summary>
        /// 客服轉商訊息
        /// </summary>
        [Description("轉單")]
        transfer = 2,
        /// <summary>
        /// 商家回覆
        /// </summary>
        [Description("轉單回覆")]
        transferback = 3,
        [Description("已結案")]
        complete = 4,
        /// <summary>
        /// 通知第二客服
        /// </summary>
        [Description("通知第二客服")]
        secServiceNotify = 5,
        /// <summary>
        /// 通知第一客服
        /// </summary>
        [Description("通知第一客服")]
        ServiceNotify = 6,
    }

    public enum priorityConvert
    {
        [Description("一般")]
        normal = 0,
        [Description("急")]
        quick = 1,
        [Description("特急")]
        fast = 2,
   }

    public enum messageConvert
    {
        [Description("線上詢問")]
        online = 0,
        [Description("來電")]
        mobile = 1,
        [Description("Mail")]
        mail = 2,
    }

    public enum methodConvert
    {
        /// <summary>
        /// 未定義
        /// </summary>
        None = -1,
        [Description("使用者詢問")]
        user = 0,
        [Description("線上回覆")]
        online = 1,
        [Description("Mail回覆")]
        mail = 2,
        [Description("簡訊回覆")]
        mobile = 3
    }

    public enum PushMethod
    {
        serviceNo=1,
        orderGuid=2
    }

    public enum IsRead
    {
        /// <summary>
        /// 未讀
        /// </summary>
        No = 0,
        /// <summary>
        /// 已讀
        /// </summary>
        Yes = 1
    }

    public enum Source
    {
        [Description("Web")]
        web =0,
        [Description("App")]
        app =1,
        [Description("Android")]
        android =2,
        [Description("IOS")]
        ios =3
    }

    public enum LionTravelOrderFilterType
    {
        [Localization("LionTravelOrderFilterTypeLionTravelOrderId")]
        LionTravelOrderId,
        [Localization("LionTravelOrderFilterTypeOrderGuid")]
        OrderGuid,
        [Localization("LionTravelOrderFilterTypeOrderId")]
        OrderId,
        [Localization("LionTravelOrderFilterTypeMobile")]
        Mobile
    }

    /// <summary>
    /// 問題類型
    /// </summary>
    public enum ServiceIssueType
    {
        /// <summary>
        /// 一般問題
        /// </summary>
        GeneralIssue = 1,
        /// <summary>
        /// 訂單問題
        /// </summary>
        OrderIssue = 2
    }


    public enum IssueFromType
    {
        [Description("17Life")]
        Local = 0,
        [Description("台新")]
        TaiShin = 1,
        [Description("PChome")]
        PChome = 2,
    }
}

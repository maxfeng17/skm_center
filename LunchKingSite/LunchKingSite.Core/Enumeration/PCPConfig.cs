﻿using LunchKingSite.Core.Component;
using System.ComponentModel;

namespace LunchKingSite.Core.Enumeration
{
    public enum MembershipCardGroupStatus
    {
        /// <summary>
        /// 尚未發布
        /// </summary>
        Unpublished = 0,
        /// <summary>
        /// 發布中
        /// </summary>
        Published = 1,
        /// <summary>
        /// 已發布過但隱藏
        /// </summary>
        Hidden = 2,
    }

    /// <summary>
    /// 會員卡升等描述
    /// </summary>
    public enum MembershipCardLevelUpdateMessage
    {
        /// <summary>
        /// 金卡
        /// </summary>
        [Localization("MembershipCardLevelUpdateMessageGold")]
        Gold = 2,
        /// <summary>
        /// 白金卡
        /// </summary>
        [Localization("MembershipCardLevelUpdateMessagePlatinum")]
        Platinum = 3,
        /// <summary>
        /// Vip
        /// </summary>
        [Localization("MembershipCardLevelUpdateMessageVip")]
        Vip = 4

    }
    
    public enum MembershipCardLevel
    {
        /// <summary>
        /// 零級卡片
        /// </summary>
        [Localization("MembershipCardLevelNone")]
        [Description("零級卡")]
        Level0 = 0,
        /// <summary>
        /// 等級一定名普卡
        /// </summary>
        [Localization("MembershipCardLevelLevel1")]
        [Description("普卡")]
        Level1 = 1,
        /// <summary>
        /// 等級二定名金卡
        /// </summary>
        [Localization("MembershipCardLevelLevel2")]
        [Description("金卡")]
        Level2 = 2,
        /// <summary>
        /// 等級三定名白金卡
        /// </summary>
        [Localization("MembershipCardLevelLevel3")]
        [Description("白金卡")]
        Level3 = 3,
        /// <summary>
        /// 鈦金卡
        /// </summary>
        [Localization("MembershipCardLevelLevel4")]
        [Description("鈦金卡")]
        Level4 = 4,
    }


    /// <summary>
    /// identity_code.status
    /// </summary>
    public enum IdentityCodeStatus
    {
        /// <summary>
        /// 新增
        /// </summary>
        Create = 0,
        /// <summary>
        /// 已使用
        /// </summary>
        Used = 1,
        /// <summary>
        /// 作廢
        /// </summary>
        Cancel = 2
    }

    /// <summary>
    /// discount_campaign.type
    /// </summary>
    public enum DiscountCodeType
    {
        /// <summary>
        /// 未定義
        /// </summary>
        None = 0,
        /// <summary>
        /// 17life折價券
        /// </summary>
        PponDiscountTicket = 1,
        /// <summary>
        /// 熟客券
        /// </summary>
        RegularsTicket = 2,
        /// <summary>
        /// 公關券
        /// </summary>
        FavorTicket = 3,
        /// <summary>
        /// 超級折價券
        /// </summary>
        SuperDiscountTicket = 4,
    }

    public enum PcpCheckoutResult
    {
        None = 0,
        /// <summary>
        /// 交易成功
        /// </summary>
        Success = 1,
        /// <summary>
        /// 交易失敗
        /// </summary>
        PcpOrderTransactionFail = 2,
        /// <summary>
        /// 更新Discount Code為已使用失敗
        /// </summary>
        UpdateDiscountCodeFail = 3,
        /// <summary>
        /// 消費者無紅利可扣
        /// </summary>
        NotEnoughBonus = 4,
        /// <summary>
        /// 使用的超級紅利大於結帳金額
        /// </summary>
        SuperBonusOverAmount = 5,
        /// <summary>
        /// Identity Code 已使用
        /// </summary>
        PcpCodeIsUsed = 6,
        /// <summary>
        /// Identity Code 已作廢
        /// </summary>
        PcpCodeIsCancel = 7,
    }

    public enum DepositPageDisplayType
    {
        /// <summary>
        /// 不曾寄杯
        /// </summary>
        Never = 1,
        /// <summary>
        /// 有寄過杯
        /// </summary>
        StillHaveStock = 2,
        /// <summary>
        /// 有寄過杯但已用完
        /// </summary>
        RunOut = 3
    }

    /// <summary>
    /// pcp_order.status
    /// </summary>
    public enum PcpOrderStatus
    {
        /// <summary>
        /// 作廢
        /// </summary>
        Cancel = 0,
        /// <summary>
        /// 正常單
        /// </summary>
        Normal = 1
    }

    public enum PcpOrderType
    {
        [Description("熟客核銷")]
        MembershipCard =0,
        [Description("寄杯核銷")]
        Deposit =1,
        [Description("點數核銷")]
        Point
    }

    public enum PcpDespositCouponStatus
    {
        /// <summary>
        /// 未使用
        /// </summary>
        [Description("未使用")]
        Unused = 0,
        /// <summary>
        /// 已使用
        /// </summary>
        [Description("已使用")]
        Used = 1,
        /// <summary>
        /// 作廢
        /// </summary>
        [Description("作廢")]
        Invalid = 2
    }

    /// <summary>
    /// pcp_point.type
    /// </summary>
    public enum PcpPointType
    {
        /// <summary>
        /// 熟客點
        /// </summary>
        RegularsPoint = 0,
        /// <summary>
        /// 公關點
        /// </summary>
        FavorPoint = 1
    }

    /// <summary>
    /// membership_card.status
    /// </summary>
    public enum MembershipCardStatus
    {
        /// <summary>
        /// 草稿
        /// </summary>
        [Description("草稿")]
        Draft = 0,
        /// <summary>
        /// 上架
        /// </summary>
        [Description("上架")]
        Open = 1,
        /// <summary>
        /// 作廢
        /// </summary>
        [Description("作廢")]
        Cancel = 2
    }

    /// <summary>
    /// membership_card_log.type
    /// </summary>
    public enum MembershipCardLogType
    {
        /// <summary>
        /// 系統
        /// </summary>
        System = 0,
        /// <summary>
        /// 人工
        /// </summary>
        Manual = 1
    }

    public enum Quarter
    {
        /// <summary>
        /// 第一季
        /// </summary>
        Quarter1 = 1,
        /// <summary>
        /// 第二季
        /// </summary>
        Quarter2 = 2,
        /// <summary>
        /// 第三季
        /// </summary>
        Quarter3 = 3,
        /// <summary>
        /// 第四季
        /// </summary>
        Quarter4 = 4
    }

    /// <summary>
    /// PcpPointWithdrawalOverall.type
    /// </summary>
    public enum PcpPointWithdrawalOverallType
    {
        /// <summary>
        /// pcp交易
        /// </summary>
        PcpTransaction = 0,
        /// <summary>
        /// 任務交易
        /// </summary>
        AssignmentTransaction = 1,
        /// <summary>
        /// 系統
        /// </summary>
        System = 2
    }

    /// <summary>
    /// 條件運算子
    /// </summary>
    public enum Operators
    {
        /// <summary>
        /// 等於
        /// </summary>
        IsEqualTo,
        /// <summary>
        /// 大於
        /// </summary>
        GreaterThan,
        /// <summary>
        /// 小於
        /// </summary>
        LessThan,
        /// <summary>
        /// 大於等於
        /// </summary>
        GreaterThanEqual,
        /// <summary>
        /// 小於等於
        /// </summary>
        LessThanEqual,
        /// <summary>
        /// 介於兩者之間
        /// </summary>
        BetweenAnd
    }

    /// <summary>
    /// 任務方式 pcp_assignment.assignment_type
    /// </summary>
    public enum AssignmentType
    {
        /// <summary>
        /// 簡訊
        /// </summary>
        Sms = 1,
        /// <summary>
        /// 推播2公里
        /// </summary>
        PushMsg2Km = 2,
        /// <summary>
        /// 推播17Life
        /// </summary>
        PushMsg17Life = 3
    }

    /// <summary>
    /// 訊息傳送方式 1：簡訊   2：推播
    /// 訊息傳送方式 pcp_assignment.send_type
    /// </summary>
    public enum AssignmentSendType
    {
        SMS = 1,
        Push = 2
    }

    /// <summary>
    /// 訊息類型 pcp_assignment.message_type
    /// </summary>
    public enum AssignmentMessageType
    {
        /// <summary>
        /// 非訊息類型的任務
        /// </summary>
        NonMessageType,
        /// <summary>
        /// 熟客券
        /// </summary>
        DiscountCode,
        /// <summary>
        /// 熟客卡
        /// </summary>
        RegularsCard,
        /// <summary>
        /// 商家訊息
        /// </summary>
        SellerPromoMessage
    }

    /// <summary>
    /// 任務狀態 pcp_assignment.status
    /// 0：新任務  1：名單確認  2：執行中   3：完成   4：失敗   5：取消
    /// </summary>
    public enum AssignmentStatus
    {
        NewAssign = 0,
        /// <summary>
        /// 確認名單
        /// </summary>
        Ready = 1,
        Executing = 2,
        Complete = 3,
        Failure = 4,
        Cancel = 5
    }

    public enum AssignmentMemberType
    {
        /// <summary>
        /// 17Life 網站會員 member
        /// </summary>
        WebSiteMember = 1,
        /// <summary>
        /// 商家會員 seller_member
        /// </summary>
        SellerMember = 2,
        /// <summary>
        /// 熟客卡會員
        /// </summary>
        MembershipCardMember = 3
    }

    /// <summary>
    /// pcp_assignment 任務篩選條件
    /// </summary>
    public enum AssignmentFilterType
    {
        /// <summary>
        /// 卡片等級
        /// </summary>
        MembershipCardLevel = 0,
        /// <summary>
        /// 城市
        /// </summary>
        City = 1,
        /// <summary>
        /// 鄉鎮市區
        /// </summary>
        District = 2,
        /// <summary>
        /// 購買類型
        /// </summary>
        DealCategory = 3,
        /// <summary>
        /// 生日月份
        /// </summary>
        BirthdayByMonth = 4,
        /// <summary>
        /// 年齡
        /// </summary>
        Age = 5,
        /// <summary>
        /// 會員卡編號, 對應單人直發
        /// </summary>
        UserMembershipCardId = 6,
    }
    public enum PcpTransactionOrderStatus
    {
        /// <summary>
        /// 不使用
        /// </summary>
        Nothing = 0,
        /// <summary>
        /// 訂單處理中
        /// </summary>
        Processing = 1,
        /// <summary>
        /// ATM 訂單
        /// </summary>
        AtmOrder = 2,
        /// <summary>
        /// 訂單取消
        /// </summary>
        Cancel = 3,
        /// <summary>
        /// 訂單成立
        /// </summary>
        Success = 4
    }

    public enum PcpQueueTickerType
    {
        /// <summary>
        /// 商家:熟客券, 公關券
        /// </summary>
        D,
        /// <summary>
        /// 商家:推播
        /// </summary>
        M,
        /// <summary>
        /// 消費者:熟客卡
        /// </summary>
        U,
        /// <summary>
        /// 消費者熟客券
        /// </summary>
        R,
        /// <summary>
        /// 消費者公關券
        /// </summary>
        F,
    }

    /// <summary>
    /// 執行類型 1：即刻發送  2：特定時間發送
    /// </summary>
    public enum AssignmentExecutionType
    {
        Instant = 1,
        Scheduled = 2
    }

    public enum MembershipCardGroupOrderby 
    { 
        Hot = 0,
        Close = 1,
        Newest = 2,
        CustomSeq = 3,
        DiscountPercent = 4,
        PublishTime=5,
        RecentlyUsed=6
    }

    public enum StyleOptionType
    {
        background_color_id=1,
        background_image_id,
        icon_image_id,
    }

    public enum PcpCardType
    {
        [Description("一般卡")]
        None,
        [Description("KA卡")]
        KA
    }

    public enum PcpImageType
    {
        /// <summary>
        /// 未指定 (用於搜尋全部)
        /// </summary>
        None,
        /// <summary>
        /// 形象圖片(KA背景圖)
        /// </summary>
        [Description("照片")]
        CorporateImage = 1,
        /// <summary>
        /// 卡片圖片
        /// </summary>
        Card = 2,
        /// <summary>
        /// 餐點
        /// </summary>
        [Description("餐點圖")]
        Service = 3,
        /// <summary>
        /// 環境圖
        /// </summary>
        [Description("環境圖")]
        EnvironmentAroundStore = 4,
        /// <summary>
        /// Logo圖片
        /// </summary>
        Logo = 5,
    }

    /// <summary>
    /// pcp_intro type
    /// </summary>
    public enum PcpIntroType
    {
        /// <summary>
        /// 商家簡介
        /// </summary>
        Seller = 1
    }

    /// <summary>
    /// pcp_intro type
    /// </summary>
    public enum PcpLevelChangeType
    {
        /// <summary>
        /// 升級
        /// </summary>
        Up = 1,
        /// <summary>
        /// 降級
        /// </summary>
        Down = 2
    }

    /// <summary>
    /// Pos交易類型
    /// </summary>
    public enum PcpPosTranType
    {
        /// <summary>
        /// 核銷
        /// </summary>
        Verify = 1,
        /// <summary>
        /// 反核銷
        /// </summary>
        Cancel = 2
    }

    public enum VbsBindAccountErrorType
    {
        NONE = 0,
        /// <summary>
        /// 17Life帳號(userName/mobileNumber)不存在
        /// </summary>
        AccountIsNotExist = 1,
        /// <summary>
        /// 17Life帳號重複被綁定
        /// </summary>
        AccountIsRepeat = 2,
        /// <summary>
        /// 例外狀況
        /// </summary>
        ElseException = 3
    }

    public enum PcpPollingUserActionType
    {
        /// <summary>
        /// 開啟QrCode 等待商家核銷
        /// </summary>
        Waiting = 0,
        /// <summary>
        /// 商家掃了QrCode，開始交易
        /// </summary>
        Start = 1,
        /// <summary>
        /// 使用者同意條款
        /// </summary>
        Agree = 2,
        /// <summary>
        /// 商家交易失敗，或者離開
        /// </summary>
        TransactionFail = 3,
        /// <summary>
        /// 商家完成熟客卡交易
        /// </summary>
        Complete = 4,
        /// <summary>
        /// 交易Token失效
        /// </summary>
        TokenExpired = 5
    }

    public enum PcpPollingUserAlive
    {
        /// <summary>
        /// 存活著 
        /// </summary>
        Alive = 1,
        /// <summary>
        /// 不存活了
        /// </summary>
        Dead = 0
    }

    /// <summary>
    /// 目前兩種情境(APP呼叫、POS機呼叫)
    /// </summary>
    public enum PcpType
    {
        App = 1,
        POS = 2
    }

    public enum PcpPointCollectRuleType
    {
        /// <summary>
        /// 根據多少元一點
        /// </summary>
        ByPrice = 0,
        /// <summary>
        /// 根據寄幾杯一點
        /// </summary>
        ByDepositCup = 1
    }

    public enum PcpPointRuleChangeLogRuleType
    {
        /// <summary>
        /// 集點規則變更
        /// </summary>
        CollectRule = 0,
        /// <summary>
        /// 兌點規則變更
        /// </summary>
        ExchangeRule = 1
    }
    public enum PcpPointRuleChangeLogChangeType
    {
        /// <summary>
        /// 新增
        /// </summary>
        Create = 1,
        /// <summary>
        /// 修改
        /// </summary>
        Update = 2,
        /// <summary>
        /// 刪除
        /// </summary>
        Delete = 3
    }



    public enum PcpUserPointSetType
    {
        [Description("集點")]
        Collect = 0,
        [Description("兌點")]
        Exchange = 1,
        [Description("更正")]
        Correct = 2,
    }

    public enum PcpUserDepositLogType
    {
        /// <summary>
        /// 新增寄杯
        /// </summary>
        [Description("新增寄杯")]
        Add = 0,
        /// <summary>
        /// 兌換寄杯
        /// </summary>
        [Description("兌換寄杯")]
        Use = 1,
        /// <summary>
        /// 更正寄杯
        /// </summary>
        [Description("更正寄杯")]
        Correct = 2
    }

    public enum PcpTransactionType
    {
        /// <summary>
        /// 熟客優惠
        /// </summary>
        Membership = 0,
        /// <summary>
        /// 寄杯
        /// </summary>
        Deposit = 1,
        /// <summary>
        /// 集點
        /// </summary>
        Point = 2,
        /// <summary>
        /// 尚未完成交易
        /// </summary>
        None = 3
    }

    public enum PcpDepositMenuEnabled
    {
        /// <summary>
        /// 不啟用
        /// </summary>
        Disabled = 0,
        /// <summary>
        /// 啟用
        /// </summary>
        Enabled = 1
    }

    public enum PcpDepositTemplateType
    {
        /// <summary>
        /// 寄杯活動
        /// </summary>
        [Description("寄杯活動")]
        Menu = 1,
        /// <summary>
        /// 寄杯商品
        /// </summary>
        [Description("寄杯商品")]
        Item = 2
    }

    /// <summary>
    /// 熟客系統功能
    /// </summary>
    public enum MembershipService
    {
        /// <summary>
        /// 會員優惠
        /// </summary>
        [Description("會員優惠")]
        Vip = 1,
        /// <summary>
        /// 寄杯功能
        /// </summary>
        [Description("寄杯功能")]
        Deposit = 2,
        /// <summary>
        /// 點數功能
        /// </summary>
        [Description("點數功能")]
        Point = 3
    }

    /// <summary>
    /// 升等條件邏輯 0:次數與金額擇一達成, 1:金額次數都需滿足條件
    /// </summary>
    public enum MembershipCardConditionalLogic
    {
        AmountNeededOrOrderNeeded = 0,
        AmountNeededAndOrderNeeded = 1

    }

    public enum VbsBindAccountType
    {
        Email = 1,
        MobileNumber = 2
    }

    /// <summary>
    /// 會員詳細頁回上一頁的來源
    /// </summary>
    public enum MemberDetailReturnSource
    {
        /// <summary>
        /// 從會員資訊進入 (App首頁的會員資訊-點選某個消費者)
        /// </summary>
        Member = 1,
        /// <summary>
        /// 從熟客核銷進入 (掃完消費者QRcode功能頁的查詢明細)
        /// </summary>
        VerifyPortal =2
    }

    public enum MembersInputModelFilter
    {
        /// <summary>
        /// 全部會員
        /// </summary>
        All = 0,
        /// <summary>
        /// 新會員
        /// </summary>
        NewMember,
        /// <summary>
        /// 會員卡level欄位為1
        /// </summary>
        MembershipCardLevel1,
        /// <summary>
        /// 會員卡level欄位為2
        /// </summary>
        MembershipCardLevel2,
        /// <summary>
        /// 會員卡level欄位為3
        /// </summary>
        MembershipCardLevel3,
        /// <summary>
        /// 會員卡level欄位為4
        /// </summary>
        MembershipCardLevel4,
    }

    #region 熟客會員相關

    /// <summary>
    /// 排序條件
    /// </summary>
    public enum MembersOrderByField
    {
        /// <summary>
        /// 最近使用近→遠：依據熟客卡使用時間排序，從最接近當下時間排起，為預設值
        /// </summary>
        DistanceDesc = 0,
        /// <summary>
        /// 使用次數多→少：依據累積消費次數排序，從多排到少
        /// </summary>
        UsageCountDesc = 1,
        /// <summary>
        /// 消費金額多→少：依據累積消費金額排序，從多排到少
        /// </summary>
        AmountOfConsumptionDesc = 2,
        /// <summary>
        /// 寄杯數量多→少：依據寄杯數量排序，從多排到少
        /// </summary>
        NumberOfCupsDesc = 3,
        /// <summary>
        /// 累積點數多→少：依據累積點數排序，從多排到少
        /// </summary>
        CumulativePoints = 4,
    }

    /// <summary>
    /// 消費使用期間
    /// </summary>
    public enum ConsumptionDetailUseTimeRange
    {
        Hour24 = 0,
        Hour72 = 1,
        AWeek = 2,
        AMonth = 3,
        ThreeMonths = 4,
        All = 5
    }

    public enum TabShowLogic
    {
        Hide = 0,
        Show = 1,
        ShowFirst =2
    }

    #endregion
}

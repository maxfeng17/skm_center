using System;
using LunchKingSite.Core.Component;

namespace LunchKingSite.Core
{
    [Flags]
    public enum ItemStatusFlag
    {
        Online      = 0x00000001,
        Popular     = 0x00000002,
        Recommend   = 0x00000004,
        All         = 0x00000007,
    };

    public enum ItemAccessoryListType
    {
        Single = 1,
        Multiple = 2,
        Description = 3,
        ForceDropDown = 4,
    };

    public enum ItemTemplateType
    {
        [Localization("ItemTemplatePhoto")]
        ItemTemplatePhoto = 1,

        [Localization("ItemTemplateRegular")]
        ItemTemplateRegular = 2,

        ItemTemplateUnDef = 3,

        [Localization("ItemTemplateComplex")]
        ItemTemplateComplex = 4,

        [Localization("ItemTemplateRainBow")]
        ItemTemplateiRainBow = 5,

        ItemTemplateWebBank = 6,
        ItemTemplatePizzaHut1 = 7,
        ItemTemplatePizzaHut2 = 8,
        ItemTemplatePizzaHut3 = 9,
        ItemTemplatePizzaHut4 = 10,

        [Localization("ItemTemplatePhotoDiscount")]
        ItemTemplatePhotoDiscount = 11,

        [Localization("DItemTemplateRegular")]
        DItemTemplateRegular = 12,

        [Localization("DItemTemplatePhoto")]
        DItemTemplatePhoto = 13,

        [Localization("DItemTemplateDiscount")]
        DItemTemplateDiscount = 14,
    };

    [Flags]
    public enum MenuStatusFlag
    {
        ReplenishStockMask = 3,
        NegativeStockAllowed = 4,
    }

    public enum ReplenishStockType
    {
        [Localization("RST_DoNothing")]
        DoNothing = 0,
        [Localization("RST_Restore2Def")]
        RestoreToDefault = 1,
        [Localization("RST_RestoreTillDef")]
        RechargeTillDefault = 2,
    }
}

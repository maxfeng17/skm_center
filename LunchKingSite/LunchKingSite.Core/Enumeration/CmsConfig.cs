﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.Core
{
    public enum RandomCmsType
    {
        //P好康輪播
        PponRandomCms = 0,
        //PiinLife輪播
        PiinLifeRandomCms = 1,
        //跳窗
        PopUpCities = 2,
        //與 https://www.freeliving.com.tw/visa/ 串接，可由我方將行銷banner自行上稿並設定期間與輪播。
        VBO = 3,
        //新版EDM
        NewEdmAd = 4,
        //BabyHome
        BBH = 5,
        //時尚玩家
        SuperTaste = 6,
        //Master Page
        PponMasterPage = 7,
        //WeChat
        WeChat = 8,
        //PponBuyAd  付款頁AD Banner
        PponBuyAd = 9,
        //新板首頁 2018
        Home2018 = 10,
    }

    public enum CmsSpecialCityType
    {
        ///<summary>不分區</summary>
        All = -1,
        ///<summary>策展頻道</summary>
        CurationPage = -2
    }
}

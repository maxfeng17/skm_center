﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.Core.Component;

namespace LunchKingSite.Core
{
    /// <summary>
    /// HiDeal商品類型
    /// </summary>
    public enum HiDealProductType
    {
        /// <summary>
        /// 一般商品
        /// </summary>
        [SystemCode("HiDealProductType", 1)]
        Product = 1,
        /// <summary>
        /// 運費
        /// </summary>
        [SystemCode("HiDealProductType", 2)]
        Freight = 2,
    }
    /// <summary>
    /// HiDeal交貨型態
    /// </summary>
    public enum HiDealDeliveryType
    {
        /// <summary>
        /// 非商品
        /// </summary>
        Other = 0,
        /// <summary>
        /// 到店
        /// </summary>
        [SystemCode("HiDealDeliveryType", 1)]
        ToShop = 1,
        /// <summary>
        /// 到府
        /// </summary>
        [SystemCode("HiDealDeliveryType", 2)]
        ToHouse = 2,        
    }


    /// <summary>
    /// Coupon類運費設定分類
    /// </summary>
    public enum HiDealFreightType
    {
        /// <summary>
        /// 收入
        /// </summary>
        [SystemCode("HiDealFreightType", 0)]
        Income = 0,
        /// <summary>
        /// 支出
        /// </summary>
        [SystemCode("HiDealFreightType", 1)]
        Cost = 1,
    }

    /// <summary>
    /// 訂單狀態
    /// </summary>
    public enum HiDealOrderStatus
    {
        /// <summary>
        /// 新成立訂單
        /// </summary>
        [SystemCode("HiDealOrderStatus", 1)]
        Create = 1,
        /// <summary>
        /// 已確認訂單內容
        /// </summary>
        [SystemCode("HiDealOrderStatus", 2)]
        Confirm = 2,
        /// <summary>
        /// 付款完成
        /// </summary>
        [SystemCode("HiDealOrderStatus", 3)]
        Completed = 3,
        /// <summary>
        /// 取消單
        /// </summary>
        [SystemCode("HiDealOrderStatus", 4)]
        Cancel = 4,
    }
    /// <summary>
    /// HiDeal發票處理類型
    /// </summary>
    public enum HiDealInvoiceType
    {
        /// <summary>
        /// 不用開立發票
        /// </summary>
        [SystemCode("HiDealInvoiceType", 0)]
        None = 0,
        /// <summary>
        /// 發票託管
        /// </summary>
        [SystemCode("HiDealInvoiceType", 1)]
        Managed = 1,
        /// <summary>
        /// 南投縣青少年空手道推展協會
        /// </summary>
        [SystemCode("HiDealInvoiceType", 2)]
        Karate = 2,
        /// <summary>
        /// 創世社會福利基金會
        /// </summary>
        [SystemCode("HiDealInvoiceType", 3)]
        GSWF = 3,
        /// <summary>
        /// 三聯式發票
        /// </summary>
        [SystemCode("HiDealInvoiceType", 4)]
        TripleType = 4,
    }

    public enum HiDealSpecialDiscountType
    {
        NotVisa = 0,
        /// <summary>
        /// VISA貴賓專屬
        /// </summary>
        [SystemCode("HiDealSpecialDiscount", 1)]
        VisaPrivate = 1,
        [SystemCode("HiDealSpecialDiscount", 2)]
        VisaPriority = 2
    }


    public enum HiDealCouponStatus
    {
        /// <summary>
        /// 可用(未分配)
        /// </summary>
        Available = 0,
        /// <summary>
        /// 分配
        /// </summary>
        Assigned = 1,
        /// <summary>
        /// 使用
        /// </summary>
        Used = 2,
        /// <summary>
        /// 因憑證總數調整而取消
        /// </summary>
        Cancel = 3,
        /// <summary>
        /// 退貨與作廢憑證等-作廢時需產生新的可用憑證。
        /// </summary>
        Refund = 4,
        /// <summary>
        /// 資料處理中的標記。
        /// </summary>
        Marked = 5,
    }

    public enum HiDealCouponLogStatus
    {
        /// <summary>
        /// 建立
        /// </summary>
        Create = 0,
        /// <summary>
        /// 修改
        /// </summary>
        Modify = 1,
        /// <summary>
        /// 增加數量
        /// </summary>
        Add = 2,
    }

    public enum HiDealOrderShowStatus
    {
        /// <summary>
        /// 建立訂單
        /// </summary>
        [SystemCode("HiDealOrderShowStatus", 0)]
        Create = 0,
        /// <summary>
        /// 退貨處理中
        /// </summary>
        [SystemCode("HiDealOrderShowStatus", 1)]
        RefundProcessing = 1,
        /// <summary>
        /// 退貨失敗
        /// </summary>
        [SystemCode("HiDealOrderShowStatus", 2)]
        RefundFail = 2,
        /// <summary>
        /// 退貨成功
        /// </summary>
        [SystemCode("HiDealOrderShowStatus", 3)]
        RefundSuccess = 3,
    }

    public enum HiDealOrderShowPaymentType
    {
        CreditCard = 0,
        ATM = 1,
    }
    /// <summary>
    /// 退貨單狀態
    /// </summary>
    public enum HiDealReturnedStatus
    {
        /// <summary>
        /// 新成立退貨單
        /// </summary>
        [SystemCode("HiDealReturnedStatus", 1)]
        Create = 1,
        /// <summary>
        /// 退貨完成
        /// </summary>
        [SystemCode("HiDealReturnedStatus", 2)]
        Completed = 2,
        /// <summary>
        /// 作廢退貨單
        /// </summary>
        [SystemCode("HiDealReturnedStatus", 3)]
        Cancel = 3,
        /// <summary>
        /// 退貨失敗
        /// </summary>
        [SystemCode("HiDealReturnedStatus", 4)]
        Fail = 4,
    }

    public enum HiDealVisaCardType
    {
        /// <summary>
        /// 非Visa特別卡別的歸類在這
        /// </summary>
        Normal = 0,
        /// <summary>
        /// 無限卡
        /// </summary>
        Infinite = 1,
        /// <summary>
        /// 玉璽卡
        /// </summary>
        Signature = 2,
        /// <summary>
        /// 白金卡
        /// </summary>
        Platinum = 3,
        /// <summary>
        /// 新光商圈活動-台新卡
        /// </summary>
        Taishin  = 4

    }

    public enum HiDealRegionSystemCode
    {
        /// <summary>
        /// 台北
        /// </summary>
        Taipei = 1,
        /// <summary>
        /// 總覽
        /// </summary>
        Overview = 2,
        /// <summary>
        /// 高雄
        /// </summary>
        Kaohsiung = 3,
        /// <summary>
        /// 台中
        /// </summary>
        Taichung = 4,
        /// <summary>
        /// 台南
        /// </summary>
        Tainan = 5,
    }
}

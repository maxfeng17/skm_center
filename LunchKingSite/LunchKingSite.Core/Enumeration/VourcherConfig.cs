﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.Core.Component;

namespace LunchKingSite.Core.Enumeration
{
    public enum VourcherSortType
    {
        /// <summary>
        /// 熱門關注
        /// </summary>
        [Localization("VourcherSortTypePopular")]
        Popular,
        /// <summary>
        /// 離我最近
        /// </summary>
        [Localization("VourcherSortTypeNear")]
        Near,
        /// <summary>
        /// 最新優惠
        /// </summary>
        [Localization("VourcherSortTypeNewVourcher")]
        NewVourcher,
    }


    public enum VourcherGroupMainType
    {
        /// <summary>
        /// 可用的
        /// </summary>
        [Localization("VourcherGroupMainTypeAvailable")]
        Available,
        /// <summary>
        /// 逾期的
        /// </summary>
        [Localization("VourcherGroupMainTypeOverdue")]
        Overdue
    }
}

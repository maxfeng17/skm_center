﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace LunchKingSite.Core
{
    /// <summary>
    /// 服務代碼(同peztemp_batch_log.service_code)
    /// </summary>
    public enum PeztempServiceCode
    {
        /// <summary>
        /// 無(非全家檔，以及未使用自動產碼服務的舊全家檔)
        /// </summary>
        None,
        /// <summary>
        /// 全家 & 17Life
        /// </summary>
        [Description("Family")]
        Famiport,
        /// <summary>
        /// 全家(全網) Pincode
        /// </summary>
        [Description("FamilyNet")]
        FamilynetPincode,
        /// <summary>
        /// HiLife Pincode
        /// </summary>
        [Description("HiLifeNet")]
        HiLifenetPincode,
    }

    /// <summary>
    /// 標記序號目前狀態為可用(為避免序號重複而以此作為重複檢查判斷依據之一)
    /// </summary>
    public enum PeztempStatus
    {
        /// <summary>
        /// 無(非全家、或尚未被領取)
        /// </summary>
        None,
        /// <summary>
        /// 使用中(產碼時此序號不可用)
        /// </summary>
        Using,
        /// <summary>
        /// 已過期(產碼時此序號可用)
        /// </summary>
        Expired,
    }
}

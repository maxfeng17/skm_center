﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using LunchKingSite.Core.Component;

namespace LunchKingSite.Core
{
    public enum AccBusinessGroupStatus
    {
        Disable = 0,
        Enabled = 1,
    }

    public enum DealCloseResultStatus
    {
        UnKnown,
        AlreadyClose,
        Success,
        Fail,
        TimeError
    }


    public enum PponDealPromoUrlType
    {
        /// <summary>
        /// 未知狀態的錯誤，無法回傳分享用的網址
        /// </summary>
        Error,
        /// <summary>
        /// 使用者登入的情況，分享可得紅利的url
        /// </summary>
        BonusFeedback,
        /// <summary>
        /// 單純的方想連結，不具有任何回饋功能
        /// </summary>
        OnlyShare,
    }

    #region yahoo大團購相關
    /// <summary>
    /// Yahoo大團購檔次類型
    /// </summary>
    public enum YahooPropertyType
    {
        /// <summary>
        /// P好康
        /// </summary>
        [Localization("YahooPropertyTypePpon")]
        Ppon,
        /// <summary>
        /// 品生活
        /// </summary>
        [Localization("YahooPropertyTypePiinlife")]
        Piinlife
    }

    /// <summary>
    /// Yahoo大團購檔次操作
    /// </summary>
    public enum YahooPropertyAction
    {
        /// <summary>
        /// No
        /// </summary>
        [Localization("YahooPropertyActionOrdinary")]
        Ordinary,
        /// <summary>
        /// 新增檔次
        /// </summary>
        [Localization("YahooPropertyActionNew")]
        New,
        /// <summary>
        /// 修改檔次
        /// </summary>
        [Localization("YahooPropertyActionUpdate")]
        Update,
        /// <summary>
        /// 刪除檔次(純紀錄)
        /// </summary>
        [Localization("YahooPropertyActionDelete")]
        Delete,
        /// <summary>
        /// 移除檔次(經由Api呼叫後刪除檔次(Delete)將會設定為移除，不可再使用/異動)
        /// </summary>
        [Localization("YahooPropertyActionRemove")]
        Remove,
    }

    /// <summary>
    /// Yahoo大團購檔次分類
    /// </summary>
    public enum YahooPropertyCategory
    {
        /// <summary>
        /// 美食
        /// </summary>
        [Localization("YahooPropertyCategoryFood")]
        food,
        /// <summary>
        /// 美容保養
        /// </summary>
        [Localization("YahooPropertyCategoryBeauty")]
        beauty,
        /// <summary>
        /// 時尚流行
        /// </summary>
        [Localization("YahooPropertyCategoryFashion")]
        fashion,
        /// <summary>
        /// 旅遊
        /// </summary>
        [Localization("YahooPropertyCategoryTravel")]
        travel,
        /// <summary>
        /// 宅配
        /// </summary>
        [Localization("YahooPropertyCategoryDelivery")]
        delivery,
        /// <summary>
        /// 餐廳
        /// </summary>
        [Localization("YahooPropertyCategoryRestaurant")]
        restaurant=5,
        /// <summary>
        /// 居家生活
        /// </summary>
        [Localization("YahooPropertyCategoryLife")]
        life = 6,
        /// <summary>
        /// 戶外運動
        /// </summary>
        [Localization("YahooPropertyCategorySports")]
        sports=7,
        /// <summary>
        /// 美妝保養
        /// </summary>
        [Localization("YahooPropertyCategoryCosmetic")]
        cosmetic=8,
        /// <summary>
        /// 3C家電
        /// </summary>       
        [Localization("YahooPropertyCategory3C")]
        computer=9,
        /// <summary>
        /// 服飾鞋包
        /// </summary>
        [Localization("YahooPropertyCategoryDress")]
        dress=10,
        /// <summary>
        /// 全部
        /// </summary>
        [Localization("YahooPropertyCategoryALL")]
        all=11,
    }

    /// <summary>
    /// Yahoo大團購檔次區域(北區)(舊)
    /// </summary>
    public enum YahooPropertyAreaNorth
    {
        /// <summary>
        /// 台北
        /// </summary>
        [Localization("YahooPropertyAreaNorthTaipei")]
        taipei,
        /// <summary>
        /// 桃園
        /// </summary>
        [Localization("YahooPropertyAreaNorthTaoyuan")]
        taoyuan,
        /// <summary>
        /// 新竹
        /// </summary>
        [Localization("YahooPropertyAreaNorthHsinchu")]
        hsinchu,
        /// <summary>
        /// 基隆
        /// </summary>
        [Localization("YahooPropertyAreaNorthKeelung")]
        keelung,
    }

    /// <summary>
    /// Yahoo大團購檔次區域(中區)(舊)
    /// </summary>
    public enum YahooPropertyAreaCentral
    {
        /// <summary>
        /// 台中
        /// </summary>
        [Localization("YahooPropertyAreaCentralTaichung")]
        taichung,
        /// <summary>
        /// 苗栗
        /// </summary>
        [Localization("YahooPropertyAreaCentralMiaoli")]
        miaoli,
        /// <summary>
        /// 彰化
        /// </summary>
        [Localization("YahooPropertyAreaCentralChanghua")]
        changhua,
        /// <summary>
        /// 南投
        /// </summary>
        [Localization("YahooPropertyAreaCentralNantou")]
        nantou,
        /// <summary>
        /// 雲林
        /// </summary>
        [Localization("YahooPropertyAreaCentralYunlin")]
        yunlin,
    }

    /// <summary>
    /// Yahoo大團購檔次區域(南區)(舊)
    /// </summary>
    public enum YahooPropertyAreaSouth
    {
        /// <summary>
        /// 高雄
        /// </summary>
        [Localization("YahooPropertyAreaSouthKaohsiung")]
        kaohsiung,
        /// <summary>
        /// 台南
        /// </summary>
        [Localization("YahooPropertyAreaSouthTainan")]
        tainan,
        /// <summary>
        /// 嘉義
        /// </summary>
        [Localization("YahooPropertyAreaSouthChiayi")]
        chiayi,
        /// <summary>
        /// 屏東
        /// </summary>
        [Localization("YahooPropertyAreaSouthPingtung")]
        pingtung,
    }

    /// <summary>
    /// 新Yahoo大團購區域設定
    /// </summary>
    [Flags]
    [Serializable]
    public enum YahooPropertyArea
    {
        [Localization("YahooPropertyAreaAll")]
        all=1,
        /// <summary>
        /// 台北
        /// </summary>
        [Localization("YahooPropertyAreaNorthTaipei")]
        taipei=2,
        /// <summary>
        /// 桃園
        /// </summary>
        [Localization("YahooPropertyAreaNorthTaoyuan")]
        taoyuan=4,
        /// <summary>
        /// 新竹
        /// </summary>
        [Localization("YahooPropertyAreaNorthHsinchu")]
        hsinchu=8,
        /// <summary>
        /// 基隆
        /// </summary>
        [Localization("YahooPropertyAreaNorthKeelung")]
        keelung=16,
        /// <summary>
        /// 台中
        /// </summary>
        [Localization("YahooPropertyAreaCentralTaichung")]
        taichung=32,
        /// <summary>
        /// 苗栗
        /// </summary>
        [Localization("YahooPropertyAreaCentralMiaoli")]
        miaoli=64,
        /// <summary>
        /// 彰化
        /// </summary>
        [Localization("YahooPropertyAreaCentralChanghua")]
        changhua=128,
        /// <summary>
        /// 南投
        /// </summary>
        [Localization("YahooPropertyAreaCentralNantou")]
        nantou=256,
        /// <summary>
        /// 雲林
        /// </summary>
        [Localization("YahooPropertyAreaCentralYunlin")]
        yunlin=512,
        /// <summary>
        /// 高雄
        /// </summary>
        [Localization("YahooPropertyAreaSouthKaohsiung")]
        kaohsiung=1024,
        /// <summary>
        /// 台南
        /// </summary>
        [Localization("YahooPropertyAreaSouthTainan")]
        tainan=2048,
        /// <summary>
        /// 嘉義
        /// </summary>
        [Localization("YahooPropertyAreaSouthChiayi")]
        chiayi=4096,
        /// <summary>
        /// 屏東
        /// </summary>
        [Localization("YahooPropertyAreaSouthPingtung")]
        pingtung=8192,
        /// <summary>
        /// 外島、離島
        /// </summary>
        [Localization("YahooPropertyAreaOuterIslands")]
        other = 16384,
        /// <summary>
        /// 新北
        /// </summary>
        [Localization("YahooPropertyAreaNew_taipei")]
        new_taipei=32768,
        /// <summary>
        /// 宜蘭
        /// </summary>
        [Localization("YahooPropertyAreaYilan")]
        yilan=65536,
        /// <summary>
        /// 花蓮
        /// </summary>
        [Localization("YahooPropertyAreaHualien")]
        hualien=131072,
        /// <summary>
        /// 台東
        /// </summary>
        [Localization("YahooPropertyAreaTaitung")]
        taitung=262144,


    }

    #endregion yahoo大團購相關

    /// <summary>
    /// 表示優惠檔次產出的憑證顯示方式
    /// </summary>
    public enum DealCouponType
    {
        /// <summary>
        /// 不為憑證檔次
        /// </summary>
        NotCounpnDeal = 0,
        /// <summary>
        /// 17life產出之coupon (一QRCode對應一商品)
        /// </summary>
        ContactCoupon = 1,
        /// <summary>
        /// 全家提供之pinCode碼
        /// </summary>
        FamiPinCode = 2,
        /// <summary>
        /// 全家提供之barCode碼
        /// </summary>
        FamiBarCode = 3,
        /// <summary>
        /// 廠商提供之各種序號
        /// </summary>
        SellerProvided = 4,
        /// <summary>
        /// 三段式Code39條碼
        /// </summary>
        ThreeStageBarcode = 5,
        /// <summary>
        /// 新光QRCode
        /// </summary>
        SkmQrCode = 6,
        /// <summary>
        /// 全網四段條碼
        /// </summary>
        FamilyNetPincode = 7,
        /// <summary>
        /// 丹堤成套 (廠商序號成套，多個QRCode對應多張憑證)
        /// </summary>
        ManyToManyQrCode = 8,
        /// <summary>
        /// 萊爾富兩段式條碼
        /// </summary>
        HiLifePincode = 9,
        /// <summary>
        /// 全家一段碼
        /// </summary>
        FamiSingleBarcode = 10,
    }

    /// <summary>
    /// barcode 形態
    /// </summary>
    public enum CouponCodeType
    {
        None = 0,
        Barcode128 = 1,
        BarcodeEAN13 = 2,
        /// <summary>
        /// 全家三段式條碼 barcode 39
        /// </summary>
        Pincode = 3,
        /// <summary>
        /// 全家指定商品四段式條碼 EAN13(1) + barcode39(3)
        /// </summary>
        FamiItemCode = 4,
        /// <summary>
        /// HiLife兩段式條碼 (ode39與barcode 128皆可)
        /// </summary>
        HiLifeItemCode = 5,
        /// <summary>
        /// 全家一段式條碼 
        /// </summary>
        FamiSingleBarcode = 6,
    }

    /// <summary>
    /// 只有一組序號(萬用序號)
    /// </summary>
    public enum PinType
    {
        /// <summary>
        /// 一般多筆
        /// </summary>
        Normal = 0,
        /// <summary>
        /// 唯一碼
        /// </summary>
        Single = 1,
    }

    public enum ApiMultiDealType
    {
        /// <summary>
        /// 非多檔次
        /// </summary>
        NotMultiDeal,
        /// <summary>
        /// 多檔次代表檔
        /// </summary>
        MainDeal,
        /// <summary>
        /// 多檔次子檔
        /// </summary>
        SubDeal,
    }

    public enum VbsDealType
    {
        /// <summary>
        /// 好康
        /// </summary>
        Ppon = 1,
        /// <summary>
        /// 品生活
        /// </summary>
        PiinLife = 2
    }

    public enum PponDealSpecialCityType
    {
        /// <summary>
        /// 排除所有特別檔次
        /// </summary>
        WithOutSpecialCity,
        /// <summary>
        /// 公司行號 (ex:payeasy)
        /// </summary>
        Com,
        /// <summary>
        /// 天貓
        /// </summary>
        Tmall,
        /// <summary>
        /// 品生活
        /// </summary>
        Piinlife,
        /// <summary>
        /// 精選好康
        /// </summary>
        PponSelect,
        /// <summary>
        /// 團購+品生活
        /// </summary>
        PponAndPiinlife,
    }

    /// <summary>
    /// 導購輸出檔次類型
    /// </summary>
    public enum ShoppingGuideDealType
    {
        /// <summary>
        /// 全部
        /// </summary>
        [Description("全部")]
        Overall = 0,
        /// <summary>
        /// 憑證類
        /// </summary>
        [Description("憑證")]
        Coupon = 1,
        /// <summary>
        /// 宅配類
        /// </summary>
        [Description("宅配")]
        Delivery = 2,
        /// <summary>
        /// 品生活
        /// </summary>
        [Description("品生活")]
        Piinlife = 3,
        /// <summary>
        /// 美食
        /// </summary>
        [Description("憑證-在地美食")]
        Food = 4,
        /// <summary>
        /// 旅遊
        /// </summary>
        [Description("憑證-旅遊")]
        Travel = 5,
        /// <summary>
        /// 美類
        /// </summary>
        [Description("憑證-玩美休閒")]
        BeautyAndLeisure = 6
    }

    public enum DealTimeSlotElement
    {
        /// <summary>
        /// 毛利率
        /// </summary>
        GrossMargin = 1,
        /// <summary>
        /// 毛利額
        /// </summary>
        GrossProfit = 2, 
        /// <summary>
        /// 銷量
        /// </summary>
        Quantity = 3,
        /// <summary>
        /// 折數
        /// </summary>
        DiscountRate = 4,
        /// <summary>
        /// 營收
        /// </summary>
        Revenue = 5,
        /// <summary>
        /// 上架天數
        /// </summary>
        Days = 6,
        /// <summary>
        /// 單價
        /// </summary>
        Price = 7,
        /// <summary>
        /// 賣家規模
        /// </summary>
        SellerLevel = 8

    }

    public enum BehaviorType
    {
        /// <summary>
        /// 好康
        /// </summary>
        Ppon = 1,
        /// <summary>
        /// 全家
        /// </summary>
        Fami = 2,
        /// <summary>
        /// 品生活
        /// </summary>
        PiinLife = 3,
        /// <summary>
        /// 新光三越
        /// </summary>
        Skm = 4
    }

	/// <summary>
	/// 折扣方式
	/// </summary>
    public enum PponDiscountType
    {
        /// <summary>
        /// 售價/原價
        /// </summary>
        None = 0,
        /// <summary>
        /// 兌換價
        /// </summary>
        ExchangePrice = 1,
        /// <summary>
        /// 折抵現金
        /// </summary>
        DiscountCash = 2,
        /// <summary>
        /// 打折優惠
        /// </summary>
        DiscountPercent = 3,
        /// <summary>
        /// 優惠價
        /// </summary>
        FavoPrice = 4,
        /// <summary>
        /// 特選
        /// </summary>
        Recommended = 5,
        /// <summary>
        /// 優惠兌換價
        /// </summary>
        FavoExchangePrice = 6,
        /// <summary>
        /// 折扣兌換價
        /// </summary>
        DiscountExchangePrice = 7,
        /// <summary>
        /// 特選兌換價
        /// </summary>
        RecommendedExchangePrice = 8
    }

    /// <summary>
    /// 折扣顯示方式
    /// </summary>
    public enum DiscountDisplayType
    {
        /// <summary>
        /// Price / OrigPrice
        /// </summary>
        None = 0,
        /// <summary>
        /// DiscountString | Price / OrigPrice
        /// </summary>
        DPO = 1,
        /// <summary>
        /// DiscountString  | Price
        /// </summary>
        DP = 2,
        /// <summary>
        /// DiscountString
        /// </summary>
        DiscountStringOnly = 3,
        /// <summary>
        /// DiscountString | (兌換價)Price / OrigPrice
        /// </summary>
        DEPO = 4
    }


	/// <summary>
    /// 應用賣家階層架構，檔次 核銷/對帳 權限拆分：
    /// 設定檔次於商家系統可應用之權限類別
    /// </summary>
    [Flags]
    public enum  VbsRightFlag
    {
        /// <summary>
        /// 預設，單純為前台顯示店鋪資訊使用
        /// </summary>
        Location = 1,
        /// <summary>
        /// 核銷
        /// </summary>
        Verify = 2,
        /// <summary>
        /// 出帳
        /// </summary>
        Accouting = 4,
        /// <summary>
        /// 檢視對帳單
        /// </summary>
        ViewBalanceSheet = 8,
        /// <summary>
        /// 帳務資料鎖定不讓檔次賣家檢視
        /// 針對有開啟ViewBalanceSheet的檔次賣家
        /// </summary>
        BalanceSheetHideFromDealSeller = 16,
        /// <summary>
        /// 可用(銷售)店鋪(購買頁面分店列表中顯示之分店)
        /// </summary>
        VerifyShop = 32,
        /// <summary>
        /// 可用(銷售)店鋪，於購買頁面分店列表中隱藏
        /// </summary>
        HideFromVerifyShop = 64
    }

    /// <summary>
    /// 出貨方式:Deal_Property
    /// </summary>
    public enum DealShipType
    {
        /// <summary>
        /// 一般出貨
        /// </summary>
        [Localization("ProposalShipTypeNormal")]
        Normal = 0,
        /// <summary>
        /// 24小時出貨
        /// </summary>
        [Localization("ProposalShipTypeFaster")]
        Ship72Hrs = 1,
        /// <summary>
        /// 最後出貨日
        /// </summary>
        [Localization("ProposalShipTypeShipment")]
        LastShipment = 2,
        /// <summary>
        /// 最後到貨日
        /// </summary>
        [Localization("ProposalShipTypeDelivery")]
        LastDelivery = 3,
        /// <summary>
        /// 24小時到貨
        /// </summary>
        [Localization("ProposalShipTypeWms")]
        Wms = 4,
        /// <summary>
        /// 其他 (即指定出貨日)
        /// </summary>
        [Localization("ProposalShipTypeOther")]
        Other = 99,
    }

    /// <summary>
    /// 出貨方式次分類:Deal_Property
    /// </summary>
    public enum DealShippingDateType
    {
        /// <summary>
        /// 訂單成立後天數-一般出貨
        /// </summary>
        Normal = 0,
        /// <summary>
        /// 最早出貨天數-統一出貨
        /// </summary>
        Special = 1
    }

    /// <summary>
    /// 每日一物類別
    /// </summary>
    public enum EveryDayNewDeal
    {
        None = 0,
        /// <summary>
        /// 24小時
        /// </summary>
        Limited24HR = 1,
        /// <summary>
        /// 48小時
        /// </summary>
        Limited48HR = 2,
        /// <summary>
        /// 72小時
        /// </summary>
        Limited72HR = 3,
        /// <summary>
        /// 長假
        /// </summary>
        Time = 4
    }

    /// <summary>
    /// 圖片Pool類別
    /// </summary>
    public enum DealImageType
    {
        ContentImage = 1
    }

    /// <summary>
    ///  Scupio 事件類別
    /// </summary>
    public enum ScupioEventCategory
    {
        //使用者瀏覽了某種類別
        [Description("PageView")]
        PageView,
        //使用者將某些商品加入了購物車
        //[Description("AddToCart")]
        //AddToCart,
        [Description("AddToCart")]
        AddToCart,
        //使用者將某些商品加入了願望清單、我的最愛
        [Description("AddToWishlist")]
        AddToWishlist,
        //使用者購買了某些商品
        [Description("Purchase")]
        Purchase,
        //使用者填了表單、參加活動
        [Description("Lead")]
        Lead,
        //使用者成功地註冊成會員
        [Description("CompleteRegistration")]
        CompleteRegistration,
    }

    /// <summary>
    ///  Scupio 頁面類別
    /// </summary>
    public enum ScupioPageType
    {
        //首頁
        [Description("Home")]
        Home,
        //活動頁
        [Description("Promotion")]
        Promotion,
        //註冊頁
        [Description("SignUpPage")]
        SignUpPage,
        //類別
        [Description("CategoryPage")]
        CategoryPage,
        //商品頁
        [Description("ProductPage")]
        ProductPage,
        //搜尋結果頁
        [Description("SearchResults")]
        SearchResults,
        //購物車頁
        [Description("CartPage")]
        CartPage,
        //結帳頁
        [Description("CheckoutPage")]
        CheckoutPage,
        //加入購物車頁
        [Description("AddToCart")]
        AddToCart
    }
}

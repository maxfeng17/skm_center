﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace LunchKingSite.Core.Enumeration
{
    /// <summary>
    /// 
    /// </summary>
    public enum CouponType
    {
        /// <summary>
        /// 一般
        /// </summary>
        Normal = 0,
        /// <summary>
        /// 序號
        /// </summary>
        Serial = 1
    }

    public enum PosApiType
    {
        /// <summary>
        /// NONE
        /// </summary>
        None = 0,
        /// <summary>
        /// 憑證資訊確認
        /// </summary>
        VerifyCheck = 1,
        /// <summary>
        /// 核銷
        /// </summary>
        VerifyCoupon = 2,
        /// <summary>
        /// 反核銷資訊確認
        /// </summary>
        UndoVerifiedCheck = 3,
        /// <summary>
        /// 取消核銷
        /// </summary>
        UndoVerifiedCoupon = 4,
        /// <summary>
        /// 成套商品資訊確認
        /// </summary>
        VerifyCheckGroupCoupon = 5,
        /// <summary>
        /// 成套商品核銷
        /// </summary>
        VerifyGroupCoupon = 6
    }
}

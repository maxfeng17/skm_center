﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.Core.Component;

namespace LunchKingSite.Core
{
    public enum PromotionTriggerType
    {
        SellerGuid = 1,
        BusinessHourGuid = 2,
    }

    [Flags]
    public enum PromotionStatus
    {
        [ExcludeBinding]
        Nothing = 0,
        Enabled = 1,
    }

    public enum BonusTransactionType
    {
        /// <summary>
        /// 系統調整(發放)
        /// </summary>
        SystemAdjustment = 1,
        /// <summary>
        /// 紅利點數兌換商品或PAYEASY購物金
        /// </summary>
        UserRedeeming = 2,
        /// <summary>
        /// 人工調整
        /// </summary>
        ManualAdjustment = 3,
        /// <summary>
        /// 紅利點數支付訂單費用
        /// </summary>
        OrderAmtRedeeming = 4,
        /// <summary>
        /// 紅利點數支付訂單費用紀錄作廢
        /// </summary>
        InvalidOrderAmtRedeeming = 5,
        /// <summary>
        /// 正項紅利點數作廢時產生的負項紀錄
        /// </summary>
        PayOffAdjustment = 6,
        /// <summary>
        /// 系統扣除紅利，當使用者取得紅利資格消失時，系統自動扣除紅利
        /// 比如:被推薦人退貨，扣除推薦人500點紅利。
        /// </summary>
        SystemDeduction = 7,
    }


    public enum DiscountCodeStatus
    {
        /// <summary>
        /// 無狀態，沒資料可以分析。
        /// </summary>
        None,
        /// <summary>
        /// 尚未啟用且尚未生效
        /// </summary>
        NotAppplyAndStart,
        /// <summary>
        /// 日期已到生效日期但尚未啟用
        /// </summary>
        NotApply,
        /// <summary>
        /// 已啟用，帶時間未到生效日期
        /// </summary>
        NotStartly,
        /// <summary>
        /// 可以使用
        /// </summary>
        CanUse,
        /// <summary>
        /// 已被使用
        /// </summary>
        IsUsed,
        /// <summary>
        /// 過期
        /// </summary>
        OverTime,
        /// <summary>
        /// 作廢
        /// </summary>
        Disabled,
        /// <summary>
        /// 館別受限
        /// </summary>
        Restricted,
        /// <summary>
        /// 低於最低訂單金額
        /// </summary>
        UnderMinimumAmount,
        /// <summary>
        ///已達發送上限
        /// </summary>
        UpToQuantity,
        /// <summary>
        /// 檔次分類錯誤
        /// </summary>
        DiscountCodeCategoryWrong,
        /// <summary>
        /// 非本人使用
        /// </summary>
        OwnerUseOnlyWrong,
        /// <summary>
        /// 不可與熟客卡一併使用
        /// </summary>
        CanNotCardCombineUse,
        /// <summary>
        /// 使用範圍限制不符
        /// </summary>
        CategoryUseLimit
    }

    public enum EventActivityType
    {
        /// <summary>
        /// 一般點點名
        /// </summary>
        Ticket,
        /// <summary>
        /// 贈紅利
        /// </summary>
        Bouns,
        /// <summary>
        /// 送折價券
        /// </summary>
        DiscountCode,
        /// <summary>
        /// 跳窗
        /// </summary>
        PopUp = 3,
        /// <summary>
        /// 跳窗EDM
        /// </summary>
        PopUpEdm,
        /// <summary>
        /// 分享
        /// </summary>
        InviteFriends
    }

    public enum EventActivityMode
    {
        /// <summary>
        /// 無限制
        /// </summary>
        NoLimit,
        /// <summary>
        /// 活動期間 僅一次
        /// </summary>
        ByPeriod,
        /// <summary>
        /// 活動期間 每天一次
        /// </summary>
        ByDay,
        /// <summary>
        /// 依訂單
        /// </summary>
        ByOrder,
        /// <summary>
        /// 依發送的序號
        /// </summary>
        ByCode,
        /// <summary>
        /// 拼圖
        /// </summary>
        ByCollection,
        /// <summary>
        /// 新註冊會員
        /// </summary>
        ByNewMember,
    }

    //新版CPA 紀錄種類
    public enum CpaType
    {
        /// <summary>
        /// 僅追蹤購買付款頁，購買的商品
        /// </summary>
        [Localization("TraceWithPayment")]
        TraceWithPayment
    }
    /// <summary>
    /// 優惠券活動類型
    /// </summary>
    public enum VourcherEventType
    {
        /// <summary>
        /// 直接折扣
        /// </summary>
        [Localization("VourcherEventTypeDirectDiscount")]
        DirectDiscount,
        /// <summary>
        /// 直接折扣金額
        /// </summary>
        [Localization("VourcherEventTypeDirectOffset")]
        DirectOffset,
        /// <summary>
        /// 第二件打折
        /// </summary>
        [Localization("VourcherEventTypeDiscountSecondOne")]
        DiscountSecondOne,
        /// <summary>
        /// 升等優惠
        /// </summary>
        [Localization("VourcherEventTypeParticularProduct")]
        ParticularProduct,
        /// <summary>
        /// 指定商品優惠價
        /// </summary>
        [Localization("VourcherEventTypeUpgrade")]
        Upgrade,
        /// <summary>
        /// 送招牌商品
        /// </summary>
        [Localization("VourcherEventTypeGift")]
        Gift,
        /// <summary>
        /// 多人同行一人免費
        /// </summary>
        [Localization("VourcherEventTypeCustomersGetOneFree")]
        CustomersGetOneFree,
        /// <summary>
        /// 買多送一
        /// </summary>
        [Localization("VourcherEventTypeConsumptionGetOnFree")]
        ConsumptionGetOnFree,
        /// <summary>
        /// 壽星優惠
        /// </summary>
        [Localization("VourcherEventTypeBirthDay")]
        BirthDay,
        /// <summary>
        /// 其他
        /// </summary>
        [Localization("VourcherEventTypeOther")]
        Other,
        /// <summary>
        /// 抽獎活動
        /// </summary>
        [Localization("VourcherEventTypeLottery")]
        Lottery,
    }
    /// <summary>
    /// 優惠券審核狀態
    /// </summary>
    public enum VourcherEventStatus
    {
        /// <summary>
        /// 業務申請優惠券
        /// </summary>
        [Localization("VourcherEventStatusApplyEvent")]
        ApplyEvent,
        /// <summary>
        /// 退回申請
        /// </summary>
        [Localization("VourcherEventStatusReturnApply")]
        ReturnApply,
        /// <summary>
        /// 申請審核通過
        /// </summary>
        [Localization("VourcherEventStatusEventChecked")]
        EventChecked
    }
    /// <summary>
    /// 優惠券活動發放類型
    /// </summary>
    public enum VourcherEventMode
    {
        /// <summary>
        /// 起訖區間，無其他規則
        /// </summary>
        [Localization("VourcherEventModeNone")]
        None,
        /// <summary>
        /// 不限發送總量，但限制每人一次
        /// </summary>
        [Localization("NoEventLimitQuantyOnlyOneTime")]
        NoEventLimitQuantyOnlyOneTime,
        /// <summary>
        /// 以活動做發送總量限制，並限制每人使用一次
        /// </summary>
        [Localization("EventLimitQuantityOnlyOneTime")]
        EventLimitQuantityOnlyOneTime,
        /// <summary>
        /// 以活動做發送總量限制，不限制每人使用次數
        /// </summary>
        [Localization("EventLimitQuantityNoUsingTimes")]
        EventLimitQuantityNoUsingTimes
        //以各分店做發送限制
        //[Localization("VourcherEventModeStoreLimitQuantity")]
        //StoreLimitQuantity
    }
    /// <summary>
    /// 合約未回類型
    /// </summary>
    public enum ContractSendStatus
    {
        /// <summary>
        /// 無
        /// </summary>
        [Localization("ContractSendStatusContractNone")]
        None,
        /// <summary>
        /// 合約已簽回
        /// </summary>
        [Localization("ContractSendStatusContractReturned")]
        ContractReturned,
        /// <summary>
        /// 業務尚未簽回
        /// </summary>
        [Localization("ContractSendStatusContractNoReturn")]
        ContractNoReturn,
        /// <summary>
        /// 資料有誤退回
        /// </summary>
        [Localization("ContractSendStatusWrongContract")]
        WrongContract,
        /// <summary>
        /// 資料尚未備齊
        /// </summary>
        [Localization("ContractSendStatusContractIncomplete")]
        ContractIncomplete,
        /// <summary>
        /// 賣家流程處理中
        /// </summary>
        [Localization("ContractSendStatusSellerFlowProcessing")]
        SellerFlowProcessing,
        /// <summary>
        /// 其他
        /// </summary>
        [Localization("ContractSendStatusOthers")]
        Others,
    }
    public enum VourcherCollectType
    {
        /// <summary>
        /// 收藏
        /// </summary>
        Favorite,
    }
    public enum VourcherCollectStatus
    {
        /// <summary>
        /// 收藏
        /// </summary>
        Initial,
        /// <summary>
        /// 取消收藏
        /// </summary>
        Disabled,
    }
    public enum VourcherOrderStatus
    {
        /// <summary>
        /// 初始
        /// </summary>
        Initial = 0,
        /// <summary>
        /// 已使用
        /// </summary>
        Used = 1
    }
    /// <summary>
    /// 優惠券行銷分類
    /// </summary>
    public enum VourcherPromoType
    {
        //賣家
        Seller,
        //優惠券
        Vourcher,
        /// <summary>
        /// 優惠券tag (yahoo)
        /// </summary>
        VoucherTag,
        /// <summary>
        /// 編輯區塊 (yahoo)
        /// </summary>
        VoucherBlock,
        /// <summary>
        /// 排行 (yahoo)
        /// </summary>
        VoucherRanking,
        /// <summary>
        /// 關聯分類的tag
        /// </summary>
        TagCategory,
        /// <summary>
        /// 右邊廣告
        /// </summary>
        Ad
    }
    /// <summary>
    /// 主題活動類別
    /// </summary>
    public enum EventPromoType
    {
        /// <summary>
        /// P好康
        /// </summary>
        [Localization("EventPromoTypePpon")]
        Ppon,
        /// <summary>
        /// WeChat活動內容(文字訊息)
        /// </summary>
        [Localization("EventPromoTypeWeChatMessage")]
        WeChatPromoMessage,
        /// <summary>
        /// WeChat活動內容(圖文訊息)
        /// </summary>
        [Localization("EventPromoTypeWeChatArticle")]
        WeChatPromoArticle

    }
    
    /// <summary>
    /// 主題活動商品類別
    /// </summary>
    public enum EventPromoItemType
    {
        /// <summary>
        /// 團購券
        /// </summary>
        [Localization("EventPromoItemTypePpon")]
        Ppon = 0,
        /// <summary>
        /// 品生活
        /// </summary>
        [Localization("EventPromoItemTypePiinlife")]
        Piinlife = 1,
        /// <summary>
        /// 優惠券
        /// </summary>
        [Localization("EventPromoItemTypeVourcher")]
        Vourcher = 2,
        /// <summary>
        /// 圖片投票
        /// </summary>
        [Localization("EventPromoItemTypePicVote")]
        PicVote = 3,
    }
    /// <summary>
    /// 主題活動商品類別
    /// </summary>
    public enum EventPromoEventType
    {
        /// <summary>
        /// 商品主題
        /// </summary>
        [Localization("EventPromoItemEventTypeMarketing")]
        Marketing = 0,
        /// <summary>
        /// 策展1.0
        /// </summary>
        [Localization("EventPromoItemEventTypeCuration")]
        Curation = 1,
        /// <summary>
        /// 策展2.0
        /// </summary>
        [Localization("EventPromoItemEventTypeCurationTwo")]
        CurationTwo = 2,
    }
    /// <summary>
    /// 商品主題活動版型
    /// </summary>
    public enum EventPromoTemplateType
    {
        /// <summary>
        /// 團購_基本版型
        /// </summary>
        [Localization("EventPromoTemplateTypePponOnly")]
        PponOnly,
        /// <summary>
        /// 團購 + 舊品生活
        /// </summary>
        [Localization("EventPromoTemplateTypeJoinPiinlife")]
        JoinPiinlife,
        /// <summary>
        /// 團購 + 優惠券
        /// </summary>
        [Localization("EventPromoTemplateTypeCommercial")]
        Commercial,
        /// <summary>
        /// 團購_捐款
        /// </summary>
        [Localization("EventPromoTemplateTypeKind")]
        Kind,
        /// <summary>
        /// 團購_0元好康
        /// </summary>
        [Localization("EventPromoTemplateTypeZero")]
        Zero,
        /// <summary>
        /// 只有優惠券
        /// </summary>
        [Localization("EventPromoTemplateTypeVourcherOnly")]
        VourcherOnly,
        /// <summary>
        /// 團購 + 優惠券(新光)
        /// </summary>
        [Localization("EventPromoTemplateTypeSkmEvnet")]
        SkmEvent,
        /// <summary>
        /// 投票_團購加品生活混合
        /// </summary>
        [Localization("EventPromoTemplateTypeVoteMix")]
        VoteMix,

        /// <summary>
        /// 團購 + 新品生活
        /// </summary>
        [Localization("EventPromoTemplateTypeNewJoinPiinlife")]
        NewJoinPiinlife,
        /// <summary>
        /// 新品生活專屬版型
        /// </summary>
        [Localization("EventPromoTemplateTypeOnlyNewPiinlife")]
        OnlyNewPiinlife,

    }

    /// <summary>
    /// 贈品主題活動類型
    /// </summary>
    public enum EventPremiumPromoType
    {
        /// <summary>
        /// 基本版型-索取贈品+登錄贈品收件人資訊
        /// </summary>
        [Localization("EventPremiumPromoTypeBasic")]
        Basic = 0,
    }

    /// <summary>
    /// 商品主題活動投票周期類型
    /// </summary>
    public enum EventPromoVoteCycleType
    {
        None = 0,
        /// <summary>
        /// 每日
        /// </summary>
        Everyday = 1,
        /// <summary>
        /// 至活動截止
        /// </summary>
        AllEventCycle = 2
    }

    /// <summary>
    /// 商品主題活動投票周期類型
    /// </summary>
    public enum EventPromoSetUpDiscountType
    {
        /// <summary>
        /// 顯示字體是否為紅色
        /// </summary>
        RedWord = 1,
        /// <summary>
        /// 是否開放可使用折價券
        /// </summary>
        UseDiscount = 2,
        /// <summary>
        /// 是否顯示毛利率
        /// </summary>
        DisplayGrossMargin = 4
    }

    /// <summary>
    /// 熱門關鍵字搜尋類型
    /// </summary>
    public enum PromoSearchKeySetUpQuryType
    {
        /// <summary>
        /// 新增時間
        /// </summary>
        CreateTime,
        /// <summary>
        /// 修改時間
        /// </summary>
        ModifyTime
    }

    /// <summary>
    /// APP策展圖片顯示位置
    /// </summary>
    public enum EventBannerType
    {
        /// <summary>
        /// 首頁上方
        /// </summary>
        MainUp = 1,
        /// <summary>
        /// 首頁下方產品列表輪播
        /// </summary>
        MainDown = 2,
        /// <summary>
        /// 頻道上方
        /// </summary>
        ChannelUp = 4,
        /// <summary>
        /// 頻道下方產品列表輪播
        /// </summary>
        ChannelDown = 8,
    }

    /// <summary>
    /// 折價券領取狀態
    /// </summary>
    public enum DiscountCampaignStatus
    {
        /// <summary>
        /// 可使用
        /// </summary>
        Available = 0,
        /// <summary>
        /// 索取完畢
        /// </summary>
        Complete = 1,
        /// <summary>
        /// 已索取
        /// </summary>
        Received = 2,
        /// <summary>
        /// 已過期
        /// </summary>
        Expired = 3
    }

    /// <summary>
    /// 策展商品折價券狀態
    /// </summary>
    public enum BrandDiscountStatus
    {
        /// <summary>
        /// 變更
        /// </summary>
        Change = 0,
        /// <summary>
        /// 還原
        /// </summary>
        Reduction = 1,
        /// <summary>
        /// 變更後已還原
        /// </summary>
        ChangeReduction = 2,
    }

    /// <summary>
    /// 策展主題類型
    /// </summary>
    public enum ThemeType
    {
        /// <summary>
        /// 策展活動
        /// </summary>
        Normal = 0,
        /// <summary>
        /// 策展頻道
        /// </summary>
        Channel = 1,
    }
}

﻿using System;
using LunchKingSite.Core.Component;
using System.ComponentModel;

namespace LunchKingSite.Core
{
    /// <summary>
    /// Table IDevice_Token  -- MobileOsType
    /// </summary>
    [Flags]
    [Serializable]
    public enum MobileOsType
    {
        /// <summary>
        /// 通用
        /// </summary>
        Universal = 0,
        iOS = 1,
        Android = 2,
        WinPhone = 3
    };

    /// <summary>
    /// APP DeviceToken or RegId 狀態
    /// </summary>
    [Flags]
    [Serializable]
    public enum DeviceTokenStatus
    {
        /// <summary>
        /// DeviceToken or RegId無效
        /// </summary>
        Invalid = 0,  
        /// <summary>
        /// DeviceToken or RegId有效
        /// </summary>
        Valid = 1,    
        /// <summary>
        /// DeviceToken or RegId是過期
        /// </summary>
        Expired = 2,  
        /// <summary>
        /// DeviceToken or RegId是舊的
        /// </summary>
        Old = 3
    };

    [Flags]
    [Serializable]
    public enum SubscriptNotification
    {
        /// <summary>
        /// 裝置是否訂閱推播通知
        /// </summary>
        Disabled = 0,  //取消訂閱推播通知
        Enabled = 1    //訂閱推播通知
    };

    /// <summary>
    /// App推播類型
    /// </summary>
    public enum PushAppType
    {
        /// <summary>
        /// 好康團購券
        /// </summary>
        [Localization("PushAppPponDeal")]
        PponDeal,
        /// <summary>
        /// 優惠券
        /// </summary>
        [Localization("PushAppVourcher")]
        Vourcher,
        /// <summary>
        /// 商品主題活動頁
        /// </summary>
        [Localization("PushAppEventPromo")]
        EventPromo,
        /// <summary>
        /// 自訂連結
        /// </summary>
        [Localization("PushAppCustomUrl")]
        CustomUrl,
        /// <summary>
        /// 策展2.0
        /// </summary>
        [Localization("PushAppBrandPromo")]
        BrandPromo
    };

    public enum AppImageSize
    {
        none,
        [Description("drawable-mdpi")]
        android_M = 1,
        [Description("drawable-hdpi")]
        android_H,
        [Description("drawable-xhdpi")]
        andoid_XH,
        [Description("drawable-xxhdpi")]
        android_XXH,
        [Description("drawable-xxxhdpi")]
        android_XXXH,
        iOS_Basic = 20,
        iOS2x,
        iOS3x
    }

    /// <summary>
    /// 目前只用 APP = 3 ，也不會再用其它的
    /// </summary>
    public enum PromoSearchKeyUseMode
    {
        NONE = 0,
        WEB = 1,
        RWD = 2,
        APP = 3,
        Mobile = 4
    }

    public enum AuthorizedUserType
    {
        //網站會員
        Consumer, 
        //商家
        Vendor
    }
}

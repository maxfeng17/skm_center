﻿using System;

namespace LunchKingSite.Core
{
    /// <summary>
    /// url 更新頻率
    /// </summary>
    public enum Changefreq 
    {
        always,
        hourly,
        daily,
        weekly,
        monthly,
        yearly,
        never
    }

    public struct Priority 
    {
        public const string _1point0 = "1.0";
        public const string _0point9 = "0.9";
        public const string _0point8 = "0.8";
        public const string _0point7 = "0.7";
        public const string _0point6 = "0.6";
        public const string _0point5 = "0.5";
        public const string _0point4 = "0.4";
        public const string _0point3 = "0.3";
        public const string _0point2 = "0.2";
        public const string _0point1 = "0.1";
    }

}

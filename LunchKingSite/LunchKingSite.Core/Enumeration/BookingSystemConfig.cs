﻿using System;
using LunchKingSite.Core.Component;

namespace LunchKingSite.Core
{
    /// <summary>
    /// booking_system_date.day_type
    /// </summary>
    public enum DayType 
    {
        /// <summary>
        /// 指定日期
        /// </summary>
        AssignDay = 0,
        /// <summary>
        /// 星期一
        /// </summary>
        MON = 1,
        /// <summary>
        /// 星期二
        /// </summary>
        TUE = 2,
        /// <summary>
        /// 星期三
        /// </summary>
        WED = 3,
        /// <summary>
        /// 星期四
        /// </summary>
        THU = 4,
        /// <summary>
        /// 星期五
        /// </summary>
        FRI = 5,
        /// <summary>
        /// 星期六
        /// </summary>
        SAT = 6,
        /// <summary>
        /// 星期日
        /// </summary>
        SUN = 7,
        /// <summary>
        /// 默認設定
        /// </summary>
        DefaultSet = 8,
    }

    public enum BookingType 
    {
        None = 0,
        /// <summary>
        /// 憑證類
        /// </summary>
        Coupon = 1,
        /// <summary>
        /// 旅遊類
        /// </summary>
        Travel = 2,
    }

    /// <summary>
    /// Table:booking_system_api
    /// service_name 與 api_id 對應
    /// </summary>
    public enum BookingSystemServiceName 
    {
        None = 0,
        /// <summary>
        /// 17Life
        /// </summary>
        _17Life = 1,
        /// <summary>
        /// 品生活
        /// </summary>
        HiDeal = 2,
    }
    public enum BookingSystemRecordType 
    {
        /// <summary>
        /// 未過期預約
        /// </summary>
        Unexpired = 0,
        /// <summary>
        /// 已過期預約
        /// </summary>
        Expired = 1,
    }
    /// <summary>
    /// 訂位執行結果
    /// </summary>
    public enum BookingSystemMakeReservationRecordResult 
    {
        /// <summary>
        /// 同一張Coupon重覆訂位
        /// </summary>
        DoubleUsingCoupon = -1,
        /// <summary>
        /// 訂位 Insert 失敗
        /// </summary>
        Fail = 0,
        /// <summary>
        /// 成功
        /// </summary>
        Success = 1,
    }

    public enum BookingSystemApiServiceType 
    {
        PponService = 0,
        ThirdPartyCoupon = 1,
        ThirdPartyTravel = 2,
    }
    /// <summary>
    /// 鎖定憑證搜尋類型
    /// </summary>
    public enum CouponLockSearchType
    {
        None = 0,
        ReservationDate = 1,
        ModifityDate = 2
    }
}

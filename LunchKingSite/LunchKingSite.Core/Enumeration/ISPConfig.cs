﻿using System.ComponentModel;

namespace LunchKingSite.Core
{
    public enum ISPStatus
    {
        /// <summary>
        /// 
        /// </summary>
        [Description("未申請")]
        Initial = 0,
        /// <summary>
        /// 送件17審核
        /// </summary>
        [Description("待審核")]
        Verifying = 1,
        /// <summary>
        /// 審核退回
        /// </summary>
        [Description("審核失敗")]
        Reject = 2,
        /// <summary>
        /// 17 PASS 給超商
        /// </summary>
        [Description("超商建檔中")]
        StoreCreate = 3,
        /// <summary>
        /// 超商狀態更新成申請中
        /// </summary>
        [Description("測標中")]
        Checking = 4,
        /// <summary>
        /// 測標失敗
        /// </summary>
        [Description("測標失敗")]
        Failed = 5,
        /// <summary>
        /// 開通完成
        /// </summary>
        [Description("開通完成")]
        Complete = 6,
    }

    public enum ServiceChannel
    {
        [Description("全家")]
        FamilyMart = 1,
        [Description("7-11")]
        SevenEleven = 2,
    }

    /// <summary>
    /// 通路商母代碼類型(for 7-11 C2C 母代碼有四個..)
    /// </summary>
    public enum IspChannelServiceType
    {
        /// <summary>
        /// B2C
        /// </summary>
        [Description("B2C")]
        B2C = 0,

        /// <summary>
        /// C2C
        /// </summary>
        [Description("C2C")]
        C2C = 1,
    }

    public enum RrturnCycle
    {
        /// <summary>
        /// 日退
        /// </summary>
        [Description("日退")]
        Daily = 0,
        /// <summary>
        /// 週退-週一
        /// </summary>
        [Description("週退-週一")]
        Mon = 1,
        /// <summary>
        /// 週退-週二
        /// </summary>
        [Description("週退-週二")]
        Thu = 2,
        /// <summary>
        /// 週退-週三
        /// </summary>
        [Description("週退-週三")]
        Wed = 3,
        /// <summary>
        /// 週退-週四
        /// </summary>
        [Description("週退-週四")]
        Tue = 4,
        /// <summary>
        /// 週退-週五
        /// </summary>
        [Description("週退-週五")]
        Fri = 5,
    }

    public enum RetrunShip
    {
        [Description("自行取貨")]
        SelfPicking = 1,
        [Description("全家日翊物流")]
        Family = 2,
        [Description("統一速達(宅急便)")]
        Uni = 3,
        [Description("宅配通")]
        Pelican = 4,
        [Description("新竹貨運")]
        Hct = 5,
        [Description("大榮貨運")]
        Kerrytj = 6,
        [Description("其他貨運")]
        Other = 9
    }

    public enum ApplicationStatusCode
    {
        Apply = 0,
        Verifying = 1,
        Pass = 2,
        Update = 3,
        Fail = 4,
        Stop = 5,
        ReStart = 6,
        Reject = 7
    }

    public enum CheckLableCode
    {
        [Description("合格")]
        Pass = 0,
        [Description("不合格")]
        Fail = 1,
        [Description("未寄標籤")]
        UnSend = 2,
    }

    /// <summary>
    /// order_product_delivery.goods_status
    /// </summary>
    public enum GoodsStatus
    {
        /// <summary>
        /// 訂單成立
        /// </summary>
        [Description("訂單成立")]
        OrderValid = 0,
        /// <summary>
        /// 訂單處理中
        /// </summary>
        [Description("訂單處理中")]
        OrderInProcess = 1,
        /// <summary>
        /// 包裹前往物流中心
        /// </summary>
        [Description("包裹前往物流中心")]
        GoodsSentToDc = 2,
        /// <summary>
        /// 包裹送達物流中心
        /// </summary>
        [Description("包裹送達物流中心")]
        DcReceiveSuccess = 3,
        /// <summary>
        /// 包裹至物流中心驗收異常
        /// </summary>
        [Description("包裹至物流中心驗收異常")]
        DcReceiveFail = 4,
        /// <summary>
        /// 包裹配達取件門市
        /// </summary>
        [Description("包裹配達取件門市")]
        StoreReceived = 5,
        /// <summary>
        /// 買家領取包裹
        /// </summary>
        [Description("買家領取包裹")]
        CustomerReceived = 6,
        /// <summary>
        /// 包裹到店逾時未取，物流取回
        /// </summary>
        [Description("包裹到店逾時未取，物流取回")]
        DcReturn = 7,
        /// <summary>
        /// 賣家取回包裹
        /// </summary>
        [Description("賣家取回包裹")]
        DcReceiveReturn = 8,
        /// <summary>
        /// 退貨包裹送達物流中心
        /// </summary>
        [Description("退貨包裹送達物流中心")]
        ReverseDcReceiveSuccess = 9,
        /// <summary>
        /// 退貨包裹到店逾時未取，物流取回
        /// </summary>
        [Description("退貨包裹到店逾時未取，物流取回")]
        ReverseDcReturn = 10,
        /// <summary>
        /// 退貨包裹配達寄件門市
        /// </summary>
        [Description("退貨包裹配達寄件門市")]
        ReverseStoreReceive = 11,
        /// <summary>
        /// 退貨包裹至物流中心驗收異常
        /// </summary>
        [Description("退貨包裹至物流中心驗收異常")]
        ReverseDcReceiveFail = 12,
        /// <summary>
        /// 包裹遺失
        /// </summary>
        [Description("包裹遺失")]
        GoodsLost = 13
    }

    public enum IspHistoryType
    {
        /// <summary>
        /// 貨態歷程 (可給消費者看的)
        /// </summary>
        [Description("貨態歷程")]
        DeliveryStatus = 1,
        /// <summary>
        /// 賣家歷程
        /// </summary>
        [Description("賣家歷程")]
        Seller = 3
    }

    public enum IspIsApplicationServerConnectedStatus
    {
        [Description("尚未與Isp應用平台連結訂單")]
        NotYetConnected = 0,
        [Description("與Isp應用平台連結訂單成功")]
        SuccessConnected = 1,
        [Description("與Isp應用平台連結訂單失敗")]
        FailConnected = 2,
    }

    public enum IspDcAcceptStatus
    {
        /// <summary>
        /// 物流尚未驗收
        /// </summary>
        [Description("物流尚未驗收")]
        Default = 0,
        /// <summary>
        /// 物流驗收成功
        /// </summary>
        [Description("物流驗收成功")]
        Success = 1,
        /// <summary>
        /// 小物流遺失
        /// </summary>
        [Description("小物流遺失")]
        S03 = 2,
        /// <summary>
        /// 門市遺失
        /// </summary>
        [Description("門市遺失")]
        N05 = 3,
        /// <summary>
        /// 小物流破損
        /// </summary>
        [Description("小物流破損")]
        S06 = 4,
        /// <summary>
        /// 包裝廠包裝不良(滲漏)
        /// </summary>
        [Description("包裝廠包裝不良(滲漏)")]
        D04 = 5,
        /// <summary>
        /// 門市反映商品包裝不良(滲漏)
        /// </summary>
        [Description("門市反映商品包裝不良(滲漏)")]
        S07 = 6,
        /// <summary>
        /// 閉店 整修、無路線路順
        /// </summary>
        [Description("閉店 整修、無路線路順")]
        T01 = 7,
        /// <summary>
        /// 條碼錯誤
        /// </summary>
        [Description("條碼錯誤")]
        T03 = 8,
        /// <summary>
        /// 條碼重複
        /// </summary>
        [Description("條碼重複")]
        T04 = 9,
        /// <summary>
        /// 超才
        /// </summary>
        [Description("超才")]
        T08 = 10,
        /// <summary>
        /// 未到貨-取消
        /// </summary>
        [Description("未到貨-取消")]
        XXX = 12,
        /// <summary>
        /// 店家未到貨
        /// </summary>
        [Description("店家未到貨")]
        None = 13,
        /// <summary>
        /// 違禁品
        /// </summary>
        [Description("違禁品")]
        Contraband = 14,
        /// <summary>
        /// 已過門市進貨日
        /// </summary>
        [Description("已過門市進貨日")]
        Expired = 15,
        /// <summary>
        /// 條碼無法判讀
        /// </summary>
        [Description("條碼無法判讀")]
        CannotBeInterpreted = 16,
        /// <summary>
        /// 商品捆包
        /// </summary>
        [Description("商品捆包")]
        PackageError = 17,
        /// <summary>
        /// 無標籤
        /// </summary>
        [Description("無標籤")]
        NoLabel = 18,
        /// <summary>
        /// 商品外袋透明
        /// </summary>
        [Description("無標籤")]
        Transparent = 19,
        /// <summary>
        /// 多標籤
        /// </summary>
        [Description("多標籤")]
        MutiLabel = 20,
        /// <summary>
        /// 物流中心理貨中
        /// </summary>
        [Description("物流中心理貨中")]
        Tally = 21,
        /// <summary>
        /// 商品遺失
        /// </summary>
        [Description("商品遺失")]
        LosGoods = 22,
        /// <summary>
        /// 門市不配送
        /// </summary>
        [Description("門市不配送")]
        StoreNotSend = 23,
        /// <summary>
        /// 包裹異常不配送
        /// </summary>
        [Description("包裹異常不配送")]
        OddNotSend = 24,
        /// <summary>
        /// 不正常到貨
        /// </summary>
        [Description("不正常到貨")]
        Unusual = 25
    }
}

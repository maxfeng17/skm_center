using System;
using System.ComponentModel.DataAnnotations;
using LunchKingSite.Core.Component;
using System.ComponentModel;

namespace LunchKingSite.Core
{
    public enum BusinessHourType
    {
        [Localization("BHT_Breakfast")]
        Breakfast = 1,
        [Localization("BHT_Lunch")]
        Lunch = 2,
        [Localization("BHT_AfternoonTea")]
        AfternoonTea = 3,
        [Localization("BHT_Dinner")]
        Dinner = 4,
        [Localization("BHT_NightSnack")]
        NightSnack = 5,
        [Localization("BHT_Generic")]
        Generic = 6,
        [Localization("BHT_WeekendCarryOut")]
        WeekendCarryOut = 7,
        [Localization("BHT_Premium")]
        Premium = 8,
        [Localization("BHT_SelfDefined")]
        SelfDefined = 9,
        [Localization("BHT_Ppon")]
        Ppon = 10,
    };

    [Flags]
    public enum BusinessHourStatus
    {
        [Obsolete]
        [ExcludeBinding]
        OpenMask = 3,
        [Obsolete]
        [Localization("IgnoreTimeMask")]
        IgnoreTime = 4,
        [Obsolete]
        [Localization("IgnoreAreaMask")]
        IgnoreArea = 16,
        [ExcludeBinding]
        ServiceMask = 96, // 32 + 64 (please reserve 1 more bit for this during next definition, in case if we need it)
        [ExcludeBinding]
        FranchisePponMask = 384, // 256 + 128  這兩個BIT, 已無使用
        /// <summary>
        /// 每周核銷出帳
        /// </summary>
        WeeklyPay = 512,
        /// <summary>
        /// 一檔多選項，代表檔
        /// </summary>
        ComboDealMain = 1024,
        /// <summary>
        /// 一檔多選項，子檔
        /// </summary>
        ComboDealSub = 2048,
        /// <summary>
        /// 免運，顯示貨到收運費
        /// </summary>
        NoShippingFeeMessage=4096,
        /// <summary>
        /// 倒店 or 截止時間有調整
        /// </summary>
        FinalExpireTimeChange = 8192,   
        /// <summary>
        /// 0元好康顯示原價
        /// </summary>
        PriceZeorShowOriginalPrice=16384,
        /// <summary>
        /// 無法配送離島
        /// </summary>
        NotDeliveryIslands=32768, 
        /// <summary>
        /// 信託廠商 65536 + 131072 這兩個bit
        /// </summary>
        TrustMask = 196608, 
        /// <summary>
        /// 不開立發票 2014/01/15 因刷卡轉購物金機制，無論如何都得開立發票 by pacy
        /// </summary>
        [Obsolete]
        NoInvoiceCreate = 262144, 
        /// <summary>
        /// 天貓檔次
        /// </summary>
        TmallDeal=524288,
        /// <summary>
        /// 不顯示折數
        /// </summary>
        NoDiscountShown = 1048576,
        /// <summary>
        /// 低毛利率可用折價券
        /// </summary>
        LowGrossMarginAllowedDiscount = 2097152,
        /// <summary>
        /// 買斷票券
        /// </summary>
        BuyoutCoupon = 4194304,
        /// <summary>
        /// 成套商品券
        /// </summary>
        GroupCoupon = 8388608,

        /// <summary>
        /// 不可使用折價券
        ///  20170418 停用
        /// </summary>
        //NotAllowedDiscount = 16777216,

        /// <summary>
        /// 通用券商品
        /// </summary>
        NoRestrictedStore = 33554432,
        /// <summary>
        /// 檔次資訊不出現於Web/無條件導回首頁
        /// </summary>
        NotShowDealDetailOnWeb = 67108864
    };

    public enum BusinessHourStatusBitShift
    {
        OpenMask = 0,
        ServiceMask = 5,
        FranchisePponMask = 7, /*已無使用*/
        TrustMask = 16,
    }

    [Flags]
    public enum DayFlag
    {
        [Localization("Monday")]
        Monday = 0x00000001,
        [Localization("Tuesday")]
        Tuesday = 0x00000002,
        [Localization("Wednesday")]
        Wednesday = 0x00000004,
        [Localization("Thursday")]
        Thursday = 0x00000008,
        [Localization("Friday")]
        Friday = 0x00000010,
        [Localization("Saturday")]
        Saturday = 0x00000020,
        [Localization("Sunday")]
        Sunday = 0x00000040,

        [Localization("NewYearEve")]
        NewYearEve = 0x00010000,
        [Localization("NewYear")]
        NewYear = 0x00020000,
        [Localization("ChineseNewYearEve")]
        ChineseNewYearEve = 0x00040000, // 除夕
        [Localization("ChineseNewYearDay1")]
        ChineseNewYearDay1 = 0x00080000, // 初一
        [Localization("ChineseNewYearDay2")]
        ChineseNewYearDay2 = 0x00100000, // 初二
        [Localization("ChineseNewYearDay3")]
        ChineseNewYearDay3 = 0x00200000, // 初三
        [Localization("ChineseNewYearDay4")]
        ChineseNewYearDay4 = 0x00400000, // 初四
        [Localization("ChineseNewYearDay5")]
        ChineseNewYearDay5 = 0x00800000, // 初五
        [Localization("LaborDay")]
        LaborDay = 0x01000000, // 05/01 勞動節
        [Localization("DoubleTen")]
        DoubleTen = 0x02000000, // 10/10 國慶
        [Localization("TombSweeping")]
        TombSweeping = 0x04000000, // 04/05 清明節
        [Localization("MoonFestival")]
        MoonFestival = 0x08000000, // 中秋
        [Localization("DragonBoat")]
        DragonBoat = 0x10000000, // 端午
    };

    public enum BusinessGroup
    {
        [Localization("BusinessGroupPpon")]
        Ppon = 1,
        [Localization("BusinessGroupPponitem")]
        Pponitem = 2,
        [Localization("BusinessGroupPeauty")]
        Peauty = 3,
        [Localization("BusinessGroupEasyfree")]
        Easyfree = 4,
        [Localization("BusinessGroupTravel")]
        Travel = 5,
        [Localization("BusinessGroupPiinlife")]
        Piinlife = 6,
        [Localization("BusinessGroupProduct")]
        Product = 7,
    };

    /// <summary>
    /// 2019/01/09統計，有使用的為 1,4,24,25,33.  37即將使用，為新24h到貨
    /// </summary>
    public enum DealLabelSystemCode
    {
        /// <summary>
        /// 24小時到貨 (舊，不再使用)
        /// </summary>
        ArriveIn24Hrs = 0,
        /// <summary>
        /// 72小時到貨
        /// </summary>
        ArriveIn72Hrs = 1,
        /// <summary>
        /// 五星級飯店
        /// </summary>
        FiveStarHotel = 2,
        /// <summary>
        /// 四星級飯店
        /// </summary>
        FourStarHotel = 3,
        /// <summary>
        /// 即買即用
        /// </summary>
        CanBeUsedImmediately = 4,
        /// <summary>
        /// 破萬熱銷
        /// </summary>
        [Obsolete]
        SellOverTenThousand = 5,
        /// <summary>
        /// 破千熱銷
        /// </summary>
        SellOverOneThousand = 6,
        /// <summary>
        /// 限內用
        /// </summary>
        OnlyStayIn = 7,
        /// <summary>
        /// 限外帶
        /// </summary>
        OnlyToGo = 8,
        /// <summary>
        /// 需預約
        /// </summary>
        NeedReservation = 9,
        /// <summary>
        /// 限紙本憑證
        /// </summary>
        OnlyPaperFormatCoupon = 10,
        /// <summary>
        /// 每次限用一張
        /// </summary>
        OnlyOnceEachUse = 11,
        /// <summary>
        /// 每人限用一次
        /// </summary>
        OnlyOneTimeEachPeople = 12,
        /// <summary>
        /// 女性專屬
        /// </summary>
        OnlyFemale = 13,
        /// <summary>
        /// 免運費
        /// </summary>
        FreeShipping = 14,
        /// <summary>
        /// 貨到付運費
        /// </summary>
        PayOnDelivery = 15,
        /// <summary>
        /// 含運費
        /// </summary>
        IncludedDeliveryCharge = 16,
        /// <summary>
        /// 不適用離島及外島地區
        /// </summary>
        NAOutlyingIslands = 17,
        /// <summary>
        /// 實體票券
        /// </summary>
        Tickets = 18,
        /// <summary>
        /// 禁止攜帶寵物
        /// </summary>
        PetsNotAllowed = 19,
        /// <summary>
        /// 不可連續住宿
        /// </summary>
        OneTimeAccommodation = 20,
        /// <summary>
        /// 品生活活動專區
        /// </summary>
        PiinlifeEvent = 23,
        /// <summary>
        /// Visa專區
        /// </summary>
        Visa = 24,
        /// <summary>
        /// APP限定
        /// </summary>
        AppLimitedEdition = 25,
        /// <summary>
        /// 最後一天
        /// </summary>
        SoldEndAfterOneDay=26,
        /// <summary>
        /// 即將售完
        /// </summary>
        SoldEndAfterThreeDay = 27,
        /// <summary>
        /// 品生活活動專區-情人專屬
        /// </summary>
        PiinlifeValentinesEvent = 28,
        /// <summary>
        /// 品生活活動專區-父親節精選
        /// </summary>
        PiinlifeFatherEvent=29,
        /// <summary>
        /// 品生活活動專區-賞悅中秋
        /// </summary>
        PiinlifeMoonEvent=30,
        /// <summary>
        /// 品生活活動專區-花博MAJI廣場
        /// </summary>
        PiinlifeMajiEvent=31,
        /// <summary>
        /// 品生活活動專區-每月推薦
        /// </summary>
        PiinlifeRecommend = 32,
        /// <summary>
        /// 成套票券
        /// </summary>
        GroupCoupon = 33,
        /// <summary>
        /// 可使用折價券
        /// </summary>
        DiscountCanUse = 34,
        /// <summary>
        /// 超商取貨
        /// </summary>
        IspShipping = 35,
        /// <summary>
        /// 咖啡寄杯
        /// </summary>
        DepositCoffee = 36,
        /// <summary>
        /// 24H到貨 (新)
        /// </summary>
        TwentyFourHoursArrival = 37
    };

    public enum DealLabelType
    { 
        /// <summary>
        /// 憑證類 Deal Label
        /// </summary>
        Coupon = 1,
        /// <summary>
        /// 宅配類 Deal Label
        /// </summary>
        Delivery = 2,
    };

    public enum DealLabelShowType
    { 
        icon,
        tag,
        travel,
    }

    public enum AccBusinessGroupNo 
    {
        /// <summary>
        /// 在地好康
        /// </summary>
        LocalPpon = 1,
        /// <summary>
        /// 在地宅配
        /// </summary>
        LocalDelivery = 2,
        /// <summary>
        /// 女性專區
        /// </summary>
        FemaleArea = 3,
        /// <summary>
        /// 旅遊度假
        /// </summary>
        Travel = 5,
        /// <summary>
        /// 品生活
        /// </summary>
        PiinLife = 6,
        /// <summary>
        /// 全國宅配
        /// </summary>
        Delivery = 7,
        /// <summary>
        /// 愛心公益
        /// </summary>
        Kind = 8,
        /// <summary>
        /// 新光專區
        /// </summary>
        SKM = 9,
        /// <summary>
        /// 全家專區
        /// </summary>
        Fami = 10,
    }

    public enum ExpireRedirectDisplay
    {
        [Description("喔喔！商品太搶手，已經賣光光！17Life已迅速為您找到相近或相似商品，請快按【確定】前往。")]
        Display1 = 1,
    }
    
}

﻿using System;
using LunchKingSite.Core.Component;
using System.ComponentModel;

namespace LunchKingSite.Core
{
    public enum GenderType
    {
        Unknown = 0,
        [Localization("Male")]
        Male = 1,
        [Localization("Female")]
        Female = 2
    };

    public enum ContactMethod
    {
        LandLine = 0,
        Mobile = 1,
    }

    [Flags]
    public enum MemberStatusFlag
    {
        None = 0x00000000,
        [Localization("Locked")]
        Locked = 0x00000001,/*未使用 有資料*/
        [Localization("Confirmed")]
        Confirmed = 0x00000002,/*未使用 有資料*/
        [Localization("Disabled")]
        Disabled = 0x00000004,/*未使用 有資料*/
        [Localization("ReceiveAdMail")]
        ReceiveAdMail = 0x00000008,
        [Localization("HasOrdered")]
        HasOrdered = 0x00000010,
        //[Localization("DoNotAskMerge")]
        //DoNotAskMerge = 0x00000020,
        [Localization("Active17Life")]
        Active17Life = 0x00000040,
        [Localization("IsOldBuyer")]
        IsOldBuyer = 0x00000080,
        [ExcludeBinding]
        DonationReceiptsMark = 0x00000F00,
        [Localization("HidealMember")]
        Hideal = 0x00001000,
        [Localization("MobileMember")]
        Mobile = 0x00002000,
        [Localization("IsBlacklisted")]
        IsBlacklisted = 0x00004000,/*未使用 無資料*/
    }

    public enum MobileMemberStatusType
    {
        None = 0,
        /// <summary>
        /// NumberChecked 的狀態必需設了密碼，才會改成 Activated
        /// </summary>
        NumberChecked = 1,
        Activated = 2
    }

    public enum DonationReceiptsType
    {
        None = 0,
        /// <summary>
        /// 南投縣青少年空手道推展協會
        /// </summary>
        Karate = 1,
        /// <summary>
        /// 創世社會福利基金會
        /// </summary>
        GSWF = 2,
        /// <summary>
        /// 我不想捐發票
        /// </summary>
        DoNotContribute = 15

    }


    public enum MemberRoles
    {
        RegisteredUser,
        Employee,
        Editor,
        OrderAdmin,
        MemberAdmin,
        Administrator,
        /// <summary>
        /// 工單-業務
        /// </summary>
        Sales,
        /// <summary>
        /// 工單-業務主管
        /// </summary>
        SalesManager,
        /// <summary>
        /// 工單-營運稽核
        /// </summary>
        Auditor,
        /// <summary>
        /// 創編
        /// </summary>
        Production,
        /// <summary>
        /// 財會
        /// </summary>
        Financial,
        /// <summary>
        /// 台新
        /// </summary>
        TaiShin,
        /// <summary>
        /// 陽信
        /// </summary>
        ysbank,
        /// <summary>
        /// 華泰
        /// </summary>
        Hwatai,
        /// <summary>
        /// 工單-瀏覽者
        /// </summary>
        BusinessOrderViewer,
        /// <summary>
        /// 企劃
        /// </summary>
        Planning,
        /// <summary>
        /// 業務助理
        /// </summary>
        SalesAssistant,
        /// <summary>
        /// 行銷
        /// </summary>
        Marketing,
        /// <summary>
        /// 商品整合
        /// </summary>
        StoreIntegration,
         /// <summary>
        /// 創編主管
        /// </summary>
        ProductionManager,
        /// <summary>
        /// 法務部
        /// </summary>
        Lawyer,
        /// <summary>
        /// 排檔中心
        /// </summary>
        Schedule,
        /// <summary>
        /// 業務組長
        /// </summary>
        SalesLeader,
        /// <summary>
        /// 製檔人員
        /// </summary>
        ProposalSetting
    };

    public enum SingleSignOnSource
    {
        PayEasy = 0,
        Facebook = 1,
        ContactDigitalIntegration = 2,
        /// <summary>
        /// 未尚啟用
        /// </summary>
        Google = 3,
        Mobile17Life = 4,
        VbsMember = 5,
        SkmMember = 6,
        Line = 7,
        Undefined = 9,
        Entrepot = 10
    };

    /// <summary>
    /// 登入作業回傳訊息
    /// </summary>
    public enum SignInReply
    {
        /// <summary>
        /// 其他系統原因
        /// </summary>
        [Description("")]
        OtherError = -1,
        /// <summary>
        /// 登入成功
        /// </summary>
        [Description("")]
        Success = 0,
        /// <summary>
        /// 帳號不存在
        /// </summary>
        [Description("無此帳號！請輸入已認證的手機號碼或電子信箱")]
        AccountNotExist = 1,
        /// <summary>
        /// 帳號被鎖定或失效
        /// </summary>
        [Description("您的帳號已被鎖定，暫時無法登入！為確保帳戶安全，請您至忘記密碼重新設定密碼，即可再次登入使用")]
        AccountIsLockedOut = 2,
        /// <summary>
        /// 無串接資料
        /// </summary>
        [Description("無此帳號！請輸入已認證的手機號碼或電子信箱")]
        MemberLinkNotExist = 3,
        /// <summary>
        /// 查無登入憑證資料或憑證已過期
        /// </summary>
        [Description("")]
        TicketSignInError = 4,
        /// <summary>
        /// 註冊作業錯誤
        /// </summary>
        [Description("")]
        RegisterError = 5,
        /// <summary>
        /// EMAIL尚未開通
        /// </summary>
        [Description("")]
        EmailInactive = 6,
        /// <summary>
        /// 串接失敗，帳號已存在
        /// </summary>
        [Description("")]
        LinkErrorEmailExists = 7,
        /// <summary>
        /// 帳號不被允許
        /// </summary>
        [Description("您的帳號已被鎖定，暫時無法登入！如有疑問請洽客服中心。")]
        AccountNotApproved = 8,

        #region 網頁用

        /// <summary>
        /// 有串接資料，但使用錯誤的串接方式登入
        /// </summary>
        [Description("您的帳號應使用{0}登入！")]
        MemberLinkExistButWrongWay = 10,

        /// <summary>
        /// 用手機號碼登入，但該手機未開通(尚未設整密碼)
        /// </summary>
        [Description("")]
        MobileInactive = 11,
        [Description("帳號密碼不符。為了您的帳號安全，連續錯誤5次帳號將被鎖定。若您忘了密碼，請點選忘記密碼按鈕")]
        PasswordError = 12,

        #endregion
    }

    /// <summary>
    /// 登入作業回傳訊息(網頁版)
    /// 無奈跟手機板的 SignInReply 就是不大一樣放棄整合
    /// </summary>
    public enum MemberValidateReply
    {
        /// <summary>
        /// 其他系統原因
        /// </summary>
        OtherError = -1,
        /// <summary>
        /// 登入成功
        /// </summary>
        Success = 0,
        /// <summary>
        /// 帳號不存在
        /// </summary>
        AccountNotExist = 1,
        /// <summary>
        /// 帳號被鎖定或失效
        /// </summary>
        AccountIsLockedOut = 2,
        /// <summary>
        /// 無串接資料
        /// </summary>
        MemberLinkNotExist = 3,
        /// <summary>
        /// 有串接資料，但使用錯誤的串接方式登入
        /// </summary>
        MemberLinkExistButWrongWay = 4,
        /// <summary>
        /// EMAIL尚未開通
        /// </summary>
        EmailInactive = 6,
        /// <summary>
        /// 用手機號碼登入，但該手機未開通(尚未設整密碼)
        /// </summary>
        MobileInactive = 7,
        PasswordError,

    }

    public enum MemberValidateMobileCodeReply
    {
        CodeError, /*驗證碼輸入錯誤*/
        Success,
        MobileUsedByOtherMember, /*其他的會員己經用這個手機號碼認證過了，也就是規則暫訂為，一隻手機號碼只能認證一個帳號*/
        ActivedAndSkip,
        DataError,
        TimeError, /*驗證碼過期*/
        OtherError
    }

    public enum SendMobileAuthCodeReply
    {
        SmsError,
        Success,
        MobileUsedByOtherMember, /*其他的會員己經用這個手機號碼認證過了，也就是規則暫訂為，一隻手機號碼只能認證一個帳號*/
        DataError,
        OtherError,
        MobileError,
        TooManySmsSent,
    }

    public enum MemberSetPasswordReply
    {
        EmptyError,
        Success,
        PasswordFormatError,
        DataError,
        OtherError,
    }

    public enum PEZType
    {
        none = 0,
        WelfareMember = 1,
    }

    public enum MemberBasicCheckType
    {
        /// <summary>
        /// 點餐
        /// </summary>
        Meal,
        /// <summary>
        /// 團購
        /// </summary>
        GroupBuy,
        /// <summary>
        /// 17P服務
        /// </summary>
        Ppon,
        /// <summary>
        /// 17P商品
        /// </summary>
        PponItem,
    }

    public enum TrustStatus
    {
        /// <summary>
        /// 信託準備中
        /// </summary>
        [Localization("TrustStatus_Initial")]
        Initial = 0 ,
        /// <summary>
        /// 已信託 (沒使用過的憑證, 每月11號就是要信託)
        /// </summary>
        [Localization("TrustStatus_Trusted")]
        Trusted = 1,
        /// <summary>
        /// 核銷
        /// </summary>
        [Localization("TrustStatus_Verified")]
        Verified = 2,
        /// <summary>
        /// 退貨 (退到購物金)
        /// </summary>
        [Localization("TrustStatus_Returned")]
        Returned = 3,
        /// <summary>
        /// 刷退 (退到購物金後再退現金)
        /// </summary>
        [Localization("TrustStatus_Refunded")]
        Refunded = 4,
        /// <summary>
        /// ATM (訂單尚未成立, 要匯款成功才成立)
        /// </summary>
        [Localization("TrustStatus_ATM")]
        ATM = 5,
        /// <summary>
        /// 全家超商付款 (訂單尚未成立, 要於超商付款成功才成立)
        /// </summary>
        [Localization("TrustStatus_FamilyIsp")]
        FamilyIsp = 6,
        /// <summary>
        /// 7-11超商付款 (訂單尚未成立, 要於超商付款成功才成立)
        /// </summary>
        [Localization("TrustStatus_SevenIsp")]
        SevenIsp = 7
    }

    public enum TrustBankStatus
    {
        /// 信託準備中
        [Localization("TrustBankStatus_Initial")]
        Initial = 0,
        /// 已信託
        [Localization("TrustBankStatus_Trusted")]
        Trusted = 1,
        /// 核銷
        [Localization("TrustBankStatus_Verified")]
        Verified = 2
    }

    public enum TrustSpecialStatus
    {
        /// <summary>
        /// 強制退貨
        /// </summary>
        [Localization("TrustReturnForced")]
        ReturnForced = 1,

        /// <summary>
        /// 強制核銷
        /// </summary>
        [Localization("TrustVerificationForced")]
        VerificationForced = 2,

        /// <summary>
        /// 清冊遺失
        /// </summary>
        [Localization("TrustVerificationLost")]
        VerificationLost = 4,

        /// <summary>
        /// 強制核銷未回收款項
        /// </summary>
        [Localization("TrustVerificationUnrecovered")]
        VerificationUnrecovered = 8,

        /// <summary>
        /// 信託運費
        /// </summary>
        [Localization("TrustFreight")]
        Freight = 16,

        /// <summary>
        /// ATM退款中
        /// </summary>
        [Localization("TrustAtmRefunding")]
        AtmRefunding = 32,

        /// <summary>
        /// 取消退貨
        /// </summary>
        [Localization("TrustReturnCanceled")]
        ReturnCanceled = 64,

        /// <summary>
        /// 延遲出貨
        /// </summary>
        DeliveryDelay = 128,
        
        /// <summary>
        /// 請款凍結:
        /// 由人工進行凍結及取消凍結作業
        /// </summary>
        Freeze = 256,

        /// <summary>
        /// 成套票券贈品
        /// </summary>
        Giveaway = 512,

        /// <summary>
        /// 成套票券特殊面額
        /// </summary>
        SpecialDiscount = 1024
    }

    public enum TrustProvider
    {
        /// <summary>
        /// 信託準備中
        /// </summary>
        Initial,
        /// <summary>
        /// 台新銀行
        /// </summary>
        [Localization("TrustProvider_TaiShin")]
        TaiShin,
        /// <summary>
        /// 陽信銀行
        /// </summary>
        [Localization("TrustProvider_Sunny")]
        Sunny,
        /// <summary>
        /// 華泰銀行
        /// </summary>
        [Localization("TrustProvider_Hwatai")]
        Hwatai
    }

    public enum TrustLogType
    {
        //憑證查詢
        [Localization("CouponCashTrust")]
        CouponCashTrust = 0,
        //購物金帳戶查詢
        [Localization("UserCashTrust")]
        UserCashTrust = 1
    }

    public enum TrustVerificationReportType
    {
        //憑證與購物金
        CouponAndCash = 1,
        //PCP熟客點
        PcpRegularsPoint = 2,
    }

    public enum TrustBankType
    {
        //信託
        Trust=1,
        //履約保證
        Escrow =2,
    }

    public enum TrustFlag
    {
        /// <summary>
        /// 信託準備中
        /// </summary>
        Initial = 0,
        /// <summary>
        /// 進入信託執行排程
        /// </summary>
        Ready = 1,
        /// <summary>
        /// 完成信託
        /// </summary>
        Done = 2,
        /// <summary>
        /// 排程產出信託檔案
        /// </summary>
        GenerateFile = 3,
        /// <summary>
        /// 發送報表
        /// </summary>
        SendReport = 4,
    }

    public enum UserTrustType
    {
        //購買
        [Localization("UserTrustType_Buy")]
        Buy = 0,
        //退貨
        [Localization("UserTrustType_Return")]
        Return = 1,
        //退款
        [Localization("UserTrustType_Refund")]
        Refund = 2,
    }
    public enum TrustCheckOutType
    {
        Initial = 0,
        //每週依核銷出帳
        WeeklyPay = 1
    }
    public enum ServiceMessageType
    {
        Ppon = 0,
        Hideal = 1
    }

    /// <summary>
    /// 帳號動作的稽核. 對應欄位: [account_audit].[action]
    /// </summary>
    public enum AccountAuditAction
    {
        /// <summary>
        /// 稽核用payez登入方式. 
        /// </summary>
        LoginUsingPayezAccount = 0,

        /// <summary>
        /// 稽核用facebook登入方式. 
        /// </summary>
        LoginUsingFbAccount = 1,

        /// <summary>
        /// 稽核用17Life登入方式. 註: 稽核登入的結果, 而不是認證的結果 ( 認證 != 登入).
        /// </summary>
        LoginUsing17LifeAccount = 2,

        /// <summary>
        /// 使用者更改Email
        /// </summary>
        ChangeEmail = 3,
        LoginUsingGoogleAccount = 4,
        LoginByAdministrator = 5,
        ChangePassword = 6,
        BindMemberLink = 7,
        ActiveMobile = 8,
        LoginUsingVbsMember = 9,
        LoginUsingMobile17Life = 13,
        LogNewTicket = 14,
        /// <summary>
        /// 登入EIP
        /// </summary>
        LoginEIP = 15,

        /// <summary>
        /// 台新儲值支付-綁定帳號
        /// </summary>
        TaishinPayBindUser = 16,
        /// <summary>
        /// 台新儲值支付-解除綁定帳號
        /// </summary>
        TaishinPayUnbindUser = 17,
        LoginUsingSkmMember = 18,
        Deleted = 19,
        LoginByToken = 20,
        LoginUsingLineAccount = 21,
        LoginUsingEntrepotMember = 22
    }

    public enum ResetPasswordReason
    {
        MemberChange,
        Bind17Link,
        MemberForgot,
        AdminChange,
        ForceAccountOpen,
        BindMobile,
        GuestMemberSetPassword,
    }

    public enum VbsAccountAuditAction
    {
        Login = 1,
        LoginBy17LifeSimulate = 2
    }

    public enum VbsMembershipAccountType
    {
        None = 0,

        /// <summary>
        /// 17Life 核銷帳號
        /// </summary>
        VerificationAccount,

        /// <summary>
        /// 商家帳號
        /// </summary>
        VendorAccount,

        /// <summary>
        /// 展示用帳號
        /// </summary>
        DemoUser
    }

    public enum VbsMembershipPasswordFormat
    {
        Clear,
        SHA256,
    }

    /// <summary>
    /// 
    /// </summary>
    public enum MemberEdmType
    {
        /// <summary>
        /// p好康(優惠活動)
        /// </summary>
        PponEvent = 1,
        /// <summary>
        /// p好康(開賣通知)
        /// </summary>
        PponStartedSell = 2,
        /// <summary>
        /// 品生活(優惠活動)
        /// </summary>
        PiinlifeEvent = 4,
        /// <summary>
        /// 品生活(開賣通知)
        /// </summary>
        PiinlifeStartedSell = 8
    }


    public enum MemberRegisterEmailCheck
    {
        /// <summary>
        /// 如果目前沒有該email資料
        /// </summary>
        EmailCanUse = 1,
        /// <summary>
        ///  已註冊但未開通
        /// </summary>
        RegisterNoOpen = 2,
        /// <summary>
        /// 舊會員，不論有無開通都可使用
        /// </summary>
        OldMember = 3,
        /// <summary>
        /// Email格式不符合規定
        /// </summary>
        EmailError = 4,

    }

    public enum MemberRegisterReplyType
    {
        /// <summary>
        /// 其他系統錯誤
        /// </summary>
        [Description("其他系統錯誤")]
        OtherError = -1,
        /// <summary>
        /// 註冊成功
        /// </summary>
        [Description("註冊成功")]
        RegisterSuccess = 0,
        /// <summary>
        ///  已註冊但未開通
        /// </summary>
        [Description("已註冊但未開通")]
        MailRegisterInactive = 2,
        /// <summary>
        /// 舊會員，未開通但不須開通即可使用
        /// </summary>
        [Description("舊會員，未開通但不須開通即可使用")]
        MailOldMember = 3,
        /// <summary>
        /// Email格式不符合規定
        /// </summary>
        [Description("Email格式不符合規定")]
        EmailError = 4,
        /// <summary>
        /// 註冊中發生錯誤
        /// </summary>
        [Description("註冊中發生錯誤")]
        RegisterError = 5,
        /// <summary>
        /// 註冊申請時，輸入的資料不足
        /// </summary>
        [Description("註冊申請時，輸入的資料不足")]
        DataInadequate = 6,
        /// <summary>
        /// 串接註冊，資料已申請過
        /// </summary>
        [Description("串接註冊，資料已申請過")]
        ExternalUserIdExists = 7,
        /// <summary>
        /// 會員已存在
        /// </summary>
        [Description("會員已存在")]
        MemberExisted = 8,
        /// <summary>
        /// 手機號碼格式錯誤
        /// </summary>
        [Description("手機號碼格式錯誤")]
        MobileFormatError = 9,
        /// <summary>
        /// 手機號碼格式錯誤
        /// </summary>
        [Description("帳號已註冊但未啟用")]
        MobileAccountInactive = 10,
        /// <summary>
        /// 此帳號已被使用
        /// </summary>
        [Description("此帳號已使用")]
        MobileWasUsed = 11,
        /// <summary>
        /// 密碼格式錯誤，請使用6~12碼，英文或數字
        /// </summary>
        [Description("密碼格式錯誤，請使用6~12碼，英文或數字")]
        PasswordFormatError = 12,
        /// <summary>
        /// 通訊信箱格式錯誤
        /// </summary>
        [Description("通訊信箱格式錯誤")]
        UserEmailFormatError = 13,
        /// <summary>
        /// 已註冊但未開通，跟MailRegisterInactive不一樣在，會繼續完成註冊
        /// </summary>
        [Description("繼續完成註冊")]
        RegisterContinue = 14
    }

    public enum MemberActiveStatus
    {
        Active,
        Inactive,
        UserNotFound,
        NotLife17Member
    }

    public enum MemberSendConfirmMailReplyType
    {
        /// <summary>
        /// 寄送成功
        /// </summary>
        Success,
        /// <summary>
        /// 寄送EMAIL錯誤
        /// </summary>
        SendMailError,
        /// <summary>
        /// 會員資料不存在
        /// </summary>
        MemberNoExists,
        /// <summary>
        /// 無帳號驗證資料
        /// </summary>
        MemberAuthNoExists,
        MemberAlreadyActived
    }

    public enum ResourceAclAccountType
    {
        None = 0,
        /// <summary>
        /// 17life 會員帳號
        /// </summary>
        ContactDigital = 1,
        /// <summary>
        /// 商家核銷對帳後台會員帳號
        /// </summary>
        VendorBillingSystem = 2
    }

    public enum ResourceType
    {
        None = 0,
        Seller = 1,
        Store = 2,
        HiDealProduct = 3,
        BusinessHour = 4,
        HiDealProductStore = 5,
        PponStore = 6,
    }

    [Flags]
    public enum PermissionType
    {
        None = 0x0000,
        Read = 0x0001,
    }

    /// <summary>
    /// 會員前台紅利共物金列表分類
    /// </summary>
    public enum BounsListType
    {
        //購物金
        Scash,
        //紅利
        Bouns,
        //PEZ購物金
        Pcash,
        //舊版17購物金
        OldScash,
        //折價券
        Discount,
        //Default (Page Load)
        Default,
    }

    /// <summary>
    /// login_Ticket來源列表
    /// </summary>
    public enum LoginTicketType
    {
        /// <summary>
        /// 不使用
        /// </summary>
        None = 0,
        /// <summary>
        /// login動作來自手機APP
        /// </summary>
        AppLogin = 1,
        /// <summary>
        /// login動作來自核銷系統 
        /// </summary>
        VbsLogin = 2
    }

    public enum EmailAvailableStatus
    {
        /// <summary>
        /// 格式錯誤
        /// </summary>
        IllegalFormat = -1,
        Undefined = 0,
        /// <summary>
        /// 可以被使用
        /// </summary>
        Available = 1,
        /// <summary>
        /// 已被其他會員申請註冊
        /// </summary>
        WasUsed = 2,
        /// <summary>
        /// 未開通的電子信箱，或信箱已被使用
        /// </summary>
        Authorizing = 3,
        /// <summary>
        /// 拋棄式Email檢查
        /// </summary>
        DisposableEmailAddress = 4
    }

    /// <summary>
    /// 準備取代 EmailAvailableStatus
    /// </summary>
    public enum AccountAvailableStatus
    {
        /// <summary>
        /// 格式錯誤
        /// </summary>
        IllegalFormat = -1,
        Undefined = 0,
        /// <summary>
        /// 可以被使用
        /// </summary>
        Available = 1,
        /// <summary>
        /// 已被其他會員申請註冊
        /// </summary>
        WasUsed = 2,
        /// <summary>
        /// 未開通，認證中
        /// </summary>
        Authorizing = 3,
        /// <summary>
        /// 拋棄式、或其它禁止使用的帳號
        /// </summary>
        Disposable = 4
    }

    public enum ReAuthStatus
    {
        OK,
        EmailEmpty,
        IllegalMailFormat,
        ExcceedMailLimit,
        SameEmailSkip,
        UserDataNotFound,
        Unavailable,
        InvalidAuthData,
        DisposableEmailAddress,
        ServerFail,
    }

    public enum UserDeviceType
    {
        Unidentified = 0,
        Web = 1,
        App = 2,
    }

    /// <summary>
    /// 載具類型
    /// </summary>
    public enum CarrierType
    {
        /// <summary>
        /// 無(個人紙本發票(二聯式)/捐贈發票/公司式紙本發票(三聯式))
        /// </summary>
        [Localization("CarrierTypeNone")]
        None = 0,
        /// <summary>
        /// 會員載具 EJ0018
        /// </summary>
        [Localization("CarrierTypeMember")]
        Member = 1,
        /// <summary>
        /// 手機條碼載具 3J0002
        /// </summary>
        /// 
        [Localization("CarrierTypePhone")]
        Phone = 2,
        /// <summary>
        /// 自然人憑證載具 CQ0001
        /// </summary>
        [Localization("CarrierTypePersonalCertificate")]
        PersonalCertificate = 3
    }

    /// <summary>
    /// 收藏狀態
    /// </summary>
    public enum MemberCollectDealStatus
    {
        /// <summary>
        /// 已收藏
        /// </summary>
        Collected = 0,
        /// <summary>
        /// 取消收藏
        /// </summary>
        Removed = 1,
        /// <summary>
        /// 刪除過期收藏
        /// </summary>
        CancelOutOfDateDeal = 2,
    }

    /// <summary>
    /// 收藏類型
    /// </summary>
    public enum MemberCollectDealType
    {
        /// <summary>
        /// 一般檔次
        /// </summary>
        Coupon = 0,
        /// <summary>
        /// 體驗商品
        /// </summary>
        Experience = 1,

    }

    /// <summary>
    /// 收藏檔次通知信Type
    /// </summary>
    public enum MemberCollectDealExpireLogSendType
    {
        /// <summary>
        /// 即將到期
        /// </summary>
        Expire = 0,
        /// <summary>
        /// 即將賣完
        /// </summary>
        SoldOut = 1,
    }

    /// <summary>
    /// 商家系統 評價功能 資料顯示區間
    /// </summary>
    public enum EvaluateShowDataRange
    {
        [Description("兌換中檔次")]
        Now = 1,
        [Description("兌換已結束檔次")]
        History,
        [Description("全部檔次")]
        All
    }

    /// <summary>
    /// 商家系統 評價功能 資料顯示區間
    /// </summary>
    public enum EvaluateShowStoreDataRange
    {
        [Description("全部分店")]
        All = 1,
        [Description("兌換中檔次之分店")]
        Now
    }

    /// <summary>
    /// 商家系統 評價功能 顯示種類
    /// </summary>
    public enum EvaluateOrderDesc
    {
        [Description("兌換截止日近到遠")]
        DeliveryEndNearToFar = 1,
        [Description("兌換截止日遠到近")]
        DeliveryEndFarToNear,
        [Description("兌換起始日近到遠")]
        DeliveryStartNearToFar,
        [Description("兌換起始日遠到近")]
        DeliveryStartFarToNear,
        [Description("整體滿意度高到低")]
        TotalSatisfactoryHighToLow,
        [Description("整體滿意度低到高")]
        TotalSatisfactoryLowToHight,
        [Description("再訪意願高到低")]
        ComeAgainHighToLow,
        [Description("再訪意願低到高")]
        ComeAgainLowToHight
    }

    /// <summary>
    /// 商家系統 評價功能 顯示種類
    /// </summary>
    public enum EvaluateStoreOrderDesc
    {
        [Description("整體滿意度高到低")]
        TotalSatisfactoryHighToLow = 1,
        [Description("整體滿意度低到高")]
        TotalSatisfactoryLowToHight,
        [Description("再訪意願高到低")]
        ComeAgainHighToLow,
        [Description("再訪意願低到高")]
        ComeAgainLowToHight,
        [Description("環境整潔度高到低")]
        EnvironmentHighToLow,
        [Description("環境整潔度低到高")]
        EnvironmentLowToHight,
        [Description("服務親切度高到低")]
        ServiceHighToLow,
        [Description("服務親切度低到高")]
        ServiceLowToHight
    }

    public enum EvaluateCondition
    {
        [Description("檔號")]
        UniqueId = 1,
        [Description("檔名")]
        Name,
        [Description("賣家編號")]
        SellerId,
        [Description("賣家名稱")]
        SellerName
    }

    /// <summary>
    /// 商家系統 評價功能 顯示種類
    /// </summary>
    public enum EvaluateType
    {
        /// <summary>
        /// 無
        /// </summary>
        Nan = 0,
        /// <summary>
        /// 星號
        /// </summary>
        Rate = 1,
        /// <summary>
        /// 使用者輸入
        /// </summary>
        Comment = 2,
    }

    public enum EvaluateSourceType
    {
        None = 0, 
        /// <summary>
        /// EMAIL
        /// </summary>
        Email = 1,
        /// <summary>
        /// Web
        /// </summary>
        Web = 2,
        /// <summary>
        /// App
        /// </summary>
        App = 3
    }

    public enum EvaluateDataType
    {
        /// <summary>
        /// Final
        /// </summary>
        Final = 0,
        /// <summary>
        /// Temp
        /// </summary>
        Temp = 1,
    }

    public enum MemberPromotionType
    {
        Bouns=0,
        SuperBouns=1,
        SuperDiscountTicketPoint=2
    }

    //訊息中心
    public enum ActionEventType
    {
        /// <summary>
        /// 未指定
        /// </summary>
        None=0,
        /// <summary>
        /// 推Beacon
        /// </summary>
        BeaconPush=1,
        /// <summary>
        /// 推單檔
        /// </summary>
        RemotePushBid = 2,
        /// <summary>
        /// 推頻
        /// </summary>
        RemotePushChannel = 3,
        /// <summary>
        /// 推優惠券
        /// </summary>
        RemotePushVourcher = 4,
        /// <summary>
        /// 推店家
        /// </summary>
        RemotePushSeller = 5,
        /// <summary>
        /// 推商品主題活動
        /// </summary>
        RemotePushEvnetPromo = 6,
        /// <summary>
        /// 推指定連結
        /// </summary>
        RemotePushCustomUrl =7,
        /// <summary>
        /// 推策展2.0
        /// </summary>
        RemotePushBrandPromo = 8,
        /// <summary>
        /// MGM 用
        /// </summary>
        RemotePushMessageCenter = 9,
        /// <summary>
        /// MGM 用
        /// </summary>
        RemotePushMGMCard = 10,
        /// <summary>
        /// CustomerService 用
        /// </summary>
        RemotePushCustomerService = 11,
        /// <summary>
        /// 顯示單筆訂單詳細內容
        /// </summary>
        ShowOrderDetail = 12,
        /// <summary>
        /// 回傳訊息已讀
        /// </summary>
        MarkMessageRead = 13,
    }
    /// <summary>
    /// 凍結狀態
    /// </summary>
    public enum FreezeType
    { 
        Unknow = 0,
        FreezeAccounting = 1,   //整檔凍結
        FreezeOrder = 2,        //單筆訂單凍結
        CancelFreezeAccounting = 3, //取消整檔凍結
        CancelFreezeOrder = 4   //取消單筆凍結
    }

    public enum VbsMemberStatus
    {
        None = 0,
        IsPcpMember = 1 << 0,
        IsCardOn = 1 << 1,
        IsCardOwner = 1 << 2,
    }

    /// <summary>
    /// 商家角色列舉
    /// </summary>
    /// <remarks>
    /// 原本是PcpPermission，後為了與17life會員角色區分，改成VbsRole
    /// </remarks>
    public enum VendorRole
    {
        PcpAdmin = 1,
        PcpEmployee = 2
    }

    /// <summary>
    /// 拋棄式Email Domain的狀態
    /// </summary>
    public enum DEAStatus
    {
        OK = 1,
        Disposable = 2,
        Invalid = 3,
    }

    public enum UpdateMemberPicStatus
    {
        [Description("完成")]
        OK = 1,
        [Description("會員找不到或未登入")]
        MemberNotFound = 2,
        [Description("更新失敗")]
        SaveFail = 3,
        [Description("超過最大上傳檔案大小限制，目前限制為1mb")]
        ExceedSizeLimit = 4,
        [Description("沒有上傳檔案")]
        FileNotFound = 5
    }

    public enum ForgetPasswordErrorType
    {
        None = 0,
        AccountInputError = 1,
        CaptchaResponseInputError = 2,
        NoContactPerson = 3
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace LunchKingSite.Core
{
    /// <summary>
    /// 第三方付款方式, 需加上 Description (正式對外名稱)
    /// DB Type tinyint, value 0~255
    /// </summary>
    public enum ThirdPartyPayment
    {
        /// <summary>
        /// 信用卡/ATM 等非第三方系統
        /// </summary>
        [Description("")]
        None = 0,
        /// <summary>
        /// Line Pay
        /// </summary>
        [Description("LINE Pay")]
        LinePay = 1,
        /// <summary>
        /// Line Pay
        /// </summary>
        [Description("台新儲值支付")]
        TaishinPay = 2,
    }

    #region LinePay

    /// <summary>
    /// line_pay_trans.status 狀態
    /// </summary>
    public enum LinePayTransStatus
    {
        /// <summary>
        /// 建立 Line Pay 交易
        /// </summary>
        Created = 0,
        /// <summary>
        /// 建立交易序號與導頁網址
        /// </summary>
        GetReserveSuccessAndRedirect = 1,
        /// <summary>
        /// 取得交易序號失敗
        /// </summary>
        GetReserveFail = 2,
        /// <summary>
        /// 取得交易序號Api連線失敗
        /// </summary>
        ReserveApiConnectFail = 3,
        /// <summary>
        /// 使用者於Line Pay頁面取消交易
        /// </summary>
        UserCancel = 4,
        /// <summary>
        /// Line Pay完成付款後導回 17Life buy 頁時, Line交易序號與原始取得的Line交易序號不一致
        /// </summary>
        LinePayTransactionIdNotEqual = 5,
        /// <summary>
        /// 確認授權成功
        /// </summary>
        ConfirmSuccess = 6,
        /// <summary>
        /// 確認授權 Api 連線失敗
        /// </summary>
        ConfirmApiConnectFail = 7,
        /// <summary>
        /// 確認授權失敗
        /// </summary>
        ConfirmFail = 8,
        /// <summary>
        /// 請款成功
        /// </summary>
        CaptureSuccess = 9,
        /// <summary>
        /// 請款失敗
        /// </summary>
        CaptureFail = 10,
        /// <summary>
        /// 請款 Api 連線失敗
        /// </summary>
        CaptureConnectFail = 11,
        /// <summary>
        /// 取消授權成功
        /// </summary>
        VoidSuccess = 12,
        /// <summary>
        /// 取消授權失敗
        /// </summary>
        VoidFail = 13,
        /// <summary>
        /// 取消授權 api 連線失敗
        /// </summary>
        VoidConnectFail = 14,
        /// <summary>
        /// 購買失敗退Line Pay成功
        /// </summary>
        BuyFailRefundSuccess = 15,
        /// <summary>
        /// 購買失敗退Line Pay失敗
        /// </summary>
        BuyFailRefundFail = 16,
        /// <summary>
        /// 購買失敗退Line Pay Api 連線失敗
        /// </summary>
        BuyFailRefundConnectFail = 17,
        /// <summary>
        /// 取得 Line 交易序號失敗
        /// </summary>
        RequestTransactionIdFail = 18,
        /// <summary>
        /// 訂單未成立退款成功
        /// </summary>
        OrderFailRefundSuccess = 19,
        /// <summary>
        /// 訂單未成立退款失敗
        /// </summary>
        OrderFailRefundFail = 20
    }

    public enum LinePayResultHistoryType
    {
        /// <summary>
        /// 建立交易
        /// </summary>
        Reserve = 1,
        /// <summary>
        /// 付款
        /// </summary>
        Confirm = 2,
        /// <summary>
        /// 請款
        /// </summary>
        Capture = 3,
        /// <summary>
        /// 取消授權
        /// </summary>
        Void = 4,
        /// <summary>
        /// 退款
        /// </summary>
        Refund = 5
    }

    public enum LinePayRefundStatus
    {
        /// <summary>
        /// 建立請求
        /// </summary>
        Created = 0,
        /// <summary>
        /// 要求中
        /// </summary>
        Requested = 1,
        /// <summary>
        /// call Line Pay refund success
        /// </summary>
        RefundSuccess = 2,
        /// <summary>
        /// 要求失敗
        /// </summary>
        RequestedFail = 3
    }

    #endregion LinePay

    #region TaishinPay

    public enum TaishinPayTransStatus
    {
        /// <summary>
        /// 請求支付Code
        /// </summary>
        StartPay = 0,
        /// <summary>
        /// 取得支付Code
        /// </summary>
        ReceivePayCode = 1,
        /// <summary>
        /// 同意支付
        /// </summary>
        AgreeToPay = 2,
        /// <summary>
        /// 付款成功
        /// </summary>
        PaySuccess = 3,
        /// <summary>
        /// 付款失敗
        /// </summary>
        PayFail = 4,
        /// <summary>
        /// 取消付款
        /// </summary>
        PayCancel = 5
    }

    public enum TaishinPayReturnCodeAndMsg 
    {
        /// <summary>
        /// 成功
        /// </summary>
        [Description("")]
        Success = 0,
        
        /// <summary>
        /// 身分證字號(NID)為空或無法辨識
        /// </summary>
        [Description("【身分證字號或使用者代號】輸入錯誤，請確認後重新再操作一次。")]
        NidIsNullOrUnrecognized = -2030,

        /// <summary>
        /// 身分證字號(NID)不存在
        /// </summary>
        [Description("【身分證字號或使用者代號】輸入錯誤，請確認後重新再操作一次。")]
        NidIsNotExist = -2040,
        
        /// <summary>
        /// 使用者綁定存取記號(access_token)錯誤
        /// </summary>
        [Description("使用者綁定存取記號錯誤")]
        BindAccessTokenError = -2120,
        
        /// <summary>
        /// 訂單金額(amount)超出儲值帳戶餘額
        /// </summary>
        [Description("您的儲值餘額不足，請進行加值後再交易，謝謝。")]
        AmountIsNotEnough = -2180,

        /// <summary>
        /// 訂單金額(amount)超出儲值帳戶交易限額
        /// </summary>
        [Description("超過您的儲值交易限額。")]
        OverAccountLimitAmount = -2190,

        /// <summary>
        /// 超出儲值帳戶限額
        /// </summary>
        [Description("超過您的儲值交易限額。")]
        OverTransactionLimitAmount = -2250,

        /// <summary>
        /// 付款失敗
        /// </summary>
        [Description("系統異常或超過儲值交易限額，請至【台新儲值支付平台】，檢查您的「交易金額限制」額度再重新操作，或聯絡台新銀行客服中心。 (02) 2655-3355。")]
        PaymentFail = -2280,

        /// <summary>
        /// 尚未付款
        /// </summary>
        [Description("系統異常，請重新操作或聯絡台新銀行客服中心 (02) 2655-3355。")]
        NotPaidYet = -2290,

        /// <summary>
        /// 交易異常
        /// </summary>
        [Description("系統異常，請重新操作或聯絡台新銀行客服中心。 (02) 2655-3355。")]
        TradingException = -2300,

        /// <summary>
        /// 該平台已綁定
        /// </summary>
        [Description("您登入的儲值帳戶已綁定其他17Life會員帳號，請確認資料是否正確。")]
        AlreadyBind = -2310,

        /// <summary>
        /// 交易密碼為空
        /// </summary>
        [Description("【使用者密碼】輸入錯誤，請確認後重新再操作一次。")]
        PasswordIsNull = -2320,

        /// <summary>
        /// 交易密碼錯誤
        /// </summary>
        [Description("【使用者密碼】輸入錯誤，請確認後重新再操作一次。")]
        PasswordError = -2330,

        /// <summary>
        /// 此帳號無法使用支付功能
        /// </summary>
        [Description("系統異常，請重新操作或聯絡台新銀行客服中心。 (02) 2655-3355。")]
        AccountCanNotBeUsedToPay = -2340,

        /// <summary>
        /// 中心系統忙碌中
        /// </summary>
        [Description("台新銀行系統忙碌中，請稍後再使用，或聯絡客服人員為您服務。 (02) 2655-3355。")]
        SystemIsBusy = -7003,
    }

    public enum TaishinPayShowResult 
    {
        /// <summary>
        /// 綁定成功
        /// </summary>
        [Description("綁定成功!")]
        NormalBindSuccess = 1701,

        /// <summary>
        /// 綁定失敗
        /// </summary>
        [Description("綁定失敗!")]
        NormalBindFaild = 1702,

        /// <summary>
        /// 重覆綁定
        /// </summary>
        [Description("重覆綁定!")]
        NormalAlreadyBind = 1703,
    }

    #endregion TaishinPay

    #region MasterPass

    public enum MasterPassApiResult
    {
        /// <summary>
        /// 無法取得MasterPass資料，Server與MasterPass交易失敗
        /// </summary>
        DataNotExist = 0,
        /// <summary>
        /// 成功
        /// </summary>
        Success = 1,
        /// <summary>
        /// 使用者於過程中取消使用MasterpPass
        /// </summary>
        Cancel = 2,
        /// <summary>
        /// 使用者未登入
        /// </summary>
        UserNotSingIn = 0
    }

    #endregion MasterPass
}

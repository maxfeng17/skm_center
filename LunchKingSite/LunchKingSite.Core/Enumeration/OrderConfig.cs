﻿using System;
using LunchKingSite.Core.Component;
using System.ComponentModel;

namespace LunchKingSite.Core
{
    [Flags]
    [Serializable]
    public enum OrderStatus
    {
        Nothing = 0,
        /// <summary>
        /// 已傳真
        /// </summary>
        [Localization("OrderFaxed")]
        Faxed = 1,
        /// <summary>
        /// 結案
        /// </summary>
        [Localization("OrderClosed")]
        Closed = 2,
        /// <summary>
        /// 已送出評鑑信
        /// </summary>
        [Localization("OrderReviewSent")]
        ReviewSent = 4,
        /// <summary>
        /// 訂單成立
        /// </summary>
        [Localization("OrderComplete")]
        Complete = 8,
        /// <summary>
        /// 團購
        /// </summary>
        [Localization("GroupOrder")]
        GroupOrder = 16,
        /// <summary>
        /// 優先處理
        /// </summary>
        [Localization("OrderEmergency")]
        Emergency = 32,
        /// <summary>
        /// 測試單
        /// </summary>
        [Localization("OrderTest")]
        Test = 64,
        /// <summary>
        /// 外帶
        /// </summary>
        [Localization("TakeOutService")]
        TakeOut = 128,
        // wei: please reserve 256 in case we need to expand status for 128
        /// <summary>
        /// 取消訂單
        /// </summary>
        [Localization("OrderCancel")]
        Cancel = 512,
        /// <summary>
        /// 初次訂購會員
        /// </summary>
        [Localization("FirstOrder")]
        FirstOrder = 1024,
        /// <summary>
        /// 訂單傳送確認
        /// </summary>
        [Localization("TransportConfirm")]
        TransportConfirm = 2048,
        /// <summary>
        /// 訂單傳送失敗
        /// </summary>
        [Localization("TransportFail")]
        TransportFail = 4096,
        /// <summary>
        /// 忽略配送時段
        /// </summary>
        [Obsolete]
        [Localization("IgnoreTimeMask")]
        IgnoreTimeMask = 8192,
        /// <summary>
        /// 處理中
        /// </summary>
        [Localization("OrderLocked")]
        Locked = 32768,
        /// <summary>
        /// 備貨中
        /// </summary>
        [Localization("ProductPreparing")]
        ProductPreparing = 65536,
        /// <summary>
        /// 已出貨
        /// </summary>
        [Localization("ProductShipped")]
        ProductShipped = 131072,
        /// <summary>
        /// 結檔前退貨
        /// </summary>
        [Localization("CancelBeforeClosed")]
        CancelBeforeClosed = 262144,    // 結檔時間容易調整, 報表不會依據這個標記
        /// <summary>
        /// 部份退貨
        /// </summary>
        [Localization("PartialCancel")]
        PartialCancel = 524288,
        /// <summary>
        /// ATM
        /// </summary>
        [Localization("ATMOrder")]
        ATMOrder = 1048576,
        /// <summary>
        /// 全家超商付款
        /// </summary>
        [Localization("FamilyIspOrder")]
        FamilyIspOrder = 2097152,
        /// <summary>
        /// 7-11超商付款
        /// </summary>
        [Localization("SevenIspOrder")]
        SevenIspOrder = 4194304
    };

    public enum OrderReviewType
    {
        [Localization("AllReview")]
        AllReview = -1,
        [Localization("Bad")]
        Bad = 0,
        [Localization("Normal")]
        Normal = 1,
        [Localization("Good")]
        Good = 2,
    };

    public enum OrderModeType
    {
        Error = -1,
        Regular = 0,
        SimpleGroup = 1,
        FamilyGroup = 2,
    }

    [Flags]
    public enum OrderDetailStatus
    {
        None = 0,
        SmpGrpOrdPaid = 1,
        SystemEntry = 2,
    }

    public enum OrderDetailTypes
    {
        All = -1,
        Regular = 0,
        System = 1,
    }

    [Flags]
    public enum GroupOrderStatus
    {
        None = 0,
        Completed = 1,
        NotNotified = 2,
        UnderPaymentRequest = 4,
        PaymentReqSucceeded = 8,
        PaymentReqPartiallySucceeded = 16,
        PponCouponGenerated = 32,
        Hidden = 64,
        SoldOut = 128,
        PponProcessed = 256,
        DisableSMS = 512,
        NoRefund = 1024,
        /// <summary>
        /// 廠商提供序號檔次(訂單列表不提供下載)
        /// </summary>
        PEZevent = 2048,
        /// <summary>
        ///  不成為輪播的主檔，Add by Max. 2011/4/15
        /// </summary>
        NeverBeMain = 4096,
        NoTax = 8192,
        NotInBBH = 16384,
        /// <summary>
        /// 銷量低於預設500份，仍要顯示
        /// </summary>
        LowQuantityToShow = 32768,
        /// <summary>
        /// 銷量高於預設500份，但不顯示
        /// </summary>
        HighQuantityNotToShow = 65536,
        /// <summary>
        /// 過期不給退
        /// </summary>
        ExpireNoRefund = 131072,
        NoInvoice = 262144,
        /// <summary>
        /// 廠商提供序號檔次(強制訂單列表提供下載)
        /// </summary>
        PEZeventCouponDownload = 524288,
        /// <summary>
        ///  結檔後7天不能退貨
        /// </summary>
        DaysNoRefund = 1048576,
        /// <summary>
        /// 廠商提供序號檔次(序號綁userid)
        /// </summary>
        PEZeventWithUserId = 2097152,
        /// <summary>
        /// 全家檔次
        /// </summary>
        FamiDeal = 4194304,
        /// <summary>
        /// 公益檔次
        /// </summary>
        KindDeal = 8388608,
        /// <summary>
        ///  演出時間前十日內不能退貨
        /// </summary>
        NoRefundBeforeDays = 16777216,
        /// <summary>
        /// 暫付七成異常（退換貨佔30％以上）
        /// </summary>
        PartialFail = 33554432,
        /// <summary>
        /// 新光三越App檔次
        /// </summary>
        SKMDeal = 67108864,
        /// <summary>
        /// 萊爾富檔次
        /// </summary>
        HiLifeDeal = 134217728,
    }

    [Flags]
    public enum CouponStatus
    {
        None = 0,
        UserMarkUsed = 1,
        Locked = 2,
    }

    public enum OrderType
    {
        Today = 0,
        Future = 1,
        Premium = 2,
        LongDistance = 3,
    }

    public enum FaxServiceAgent
    {
        EFaxAgent = 0,
        EFaxAgentFixedDest,
        DummyAgent
    }

    public enum OrderTransmissionType
    {
        [ExcludeBinding]
        Undefined = -1,
        Fax = 0,
        Email = 1,
        PizzaHut = 2,
        HttpPost = 3,
        WebService = 4,
        FTP = 5,
        Ivr = 6,
        Vbs = 7,
    }

    public enum PponDealStage
    {
        NotExist = 0,
        Created,            // deal exists but group order not yet created
        Ready,              // group order is created
        Running,            // group order created, not yet reach order minimum
        RunningAndOn,       // reached order minimum
        RunningAndFull,
        ClosedAndOn,        // deal closed and is on
        ClosedAndFail,      // deal closed but failing to reach minimum
        CouponGenerated,    // deal is on and coupon is generated
    }

    public enum CouponListSortType
    {
        [Localization("CouponListFilterTypeProduct")]
        None = 0,
        [Localization("CouponListSortTypeDeliverEndTimeAsc")]
        DeliverEndTimeAsc = 1,
        [Localization("CouponListSortTypeDeliverEndTimeDesc")]
        DeliverEndTimeDesc = 2,
        [Localization("CouponListSortTypeCreateTimeAsc")]
        CreateTimeAsc = 3,
        [Localization("CouponListSortTypeCreateTimeDesc")]
        CreateTimeDesc = 4,
        [Localization("CouponListSortTypeUseOfStatus")]
        UseOfStatus = 5,
        [Localization("CouponListSortTypeEventName")]
        EventName = 6,
        [Localization("CouponListSortTypeSellerTypePponItem")]
        SellerTypePponItem = 7,
        [Localization("CouponListSortTypeSellerTypePpon")]
        SellerTypePpon = 8,
        [Localization("CouponListSortTypeUseOfStatusDesc")]
        UseOfStatusDesc = 9,
        [Localization("CouponListSortTypeEventNameDesc")]
        EventNameDesc = 10,
        [Localization("CouponListSortTypeGroupOrderCompleted")]
        GroupOrderCompleted = 11,
        [Localization("CouponListSortTypeGroupOrder")]
        GroupOrder = 12,
    }

    public enum MailOrderDuringFilter
    {
        /// <summary>
        /// 全部
        /// </summary>
        [Localization("MailOrderDuringFilterAll")]
        All = -1,
        /// <summary>
        /// 全部
        /// </summary>
        [Localization("MailOrderDuringFilterThreeMonths")]
        ThreeMonths = 3,
        /// <summary>
        /// 全部
        /// </summary>
        [Localization("MailOrderDuringFilterSixMonths")]
        SixMonths = 6,
    }

    public enum MailCouponListStatusType
    {
        /// <summary>
        /// 隱藏
        /// </summary>
        [Localization("MailCouponListTypeNone")]
        None = 0,

        /// <summary>
        /// 未使用
        /// </summary>
        [Localization("MailCouponListTypeNotUsed")]
        NotUsed = 1,

        /// <summary>
        /// 已使用完畢
        /// </summary>
        [Localization("MailCouponListTypeUsed")]
        Used = 2,

        /// <summary>
        /// 已過期
        /// </summary>
        [Localization("MailCouponListTypeExpired")]
        Expired = 3,

        /// <summary>
        /// 已退貨
        /// </summary>
        [Localization("MailCouponListTypeRefund")]
        Refund = 4
    }


    #region 訂單列表的Filter

    #region 舊版

    /// <summary>
    /// [v1.0] 訂單列表Filter
    /// </summary>
    public enum CouponListFilterType
    {
        /// <summary>
        /// 無狀態，部分時候代表全部訂單
        /// </summary>
        [Localization("CouponListFilterTypeNone")]
        None = 0,
        /// <summary>
        /// 未完成付款
        /// </summary>
        [Localization("CouponListFilterTypeNotComplete")]
        NotCompleted = 1,
        /// <summary>
        /// 憑證未使用
        /// </summary>
        [Localization("CouponListFilterTypeNotUsed")]
        NotUsed = 2,
        /// <summary>
        /// 憑證使用完畢
        /// </summary>
        [Localization("CouponListFilterTypeUsed")]
        Used = 3,
        /// <summary>
        /// 憑證已過期
        /// </summary>
        [Localization("CouponListFilterTypeExpired")]
        Expired = 4,
        /// <summary>
        /// 憑證未過期
        /// </summary>
        [Localization("CouponListFilterTypeNotExpired")]
        NotExpired = 5,
        /// <summary>
        /// 宅配商品
        /// </summary>
        [Localization("CouponListFilterTypeProduct")]
        Product = 6,
        /// <summary>
        /// 優惠活動
        /// </summary>
        [Localization("CouponListFilterTypeEvent")]
        Event = 7,
        /// <summary>
        /// 已取消訂單
        /// </summary>
        [Localization("CouponListFilterTypeCancel")]
        Cancel = 8,
        /// <summary>
        /// 愛心公益
        /// </summary>
        [Localization("CouponListFilterTypeKind")]
        Kind = 9,
    }

    /// <summary>
    /// [v2.0] 訂單列表Filter
    /// </summary>
    public enum NewCouponListFilterType
    {
        /// <summary>
        /// 無狀態，部分時候代表全部訂單，須排除已使用、已過期的全家憑證
        /// </summary>
        [Localization("NewCouponListFilterTypeNone")]
        None = 0,
        /// <summary>
        /// 可使用憑證
        /// </summary>
        [Localization("NewCouponListFilterTypeNotUsed")]
        NotUsed = 1,
        /// <summary>
        /// 購物商品未出貨
        /// </summary>
        [Localization("NewCouponListFilterTypeNotShipped")]
        NotShipped = 2,
        /// <summary>
        /// 全家憑證待使用
        /// </summary>
        [Localization("NewCouponListFilterTypeFami")]
        Fami = 3,
        /// <summary>
        /// 好康憑證
        /// </summary>
        [Localization("NewCouponListFilterTypeCouponOnly")]
        CouponOnly = 4,
        /// <summary>
        /// 購物商品
        /// </summary>
        [Localization("NewCouponListFilterTypeProduct")]
        Product = 5,
        /// <summary>
        /// 活動與抽獎
        /// </summary>
        [Localization("NewCouponListFilterTypeEvent")]
        Event = 6,
        /// <summary>
        /// 已取消訂單
        /// </summary>
        [Localization("NewCouponListFilterTypeCancel")]
        Cancel = 7,
        /// <summary>
        /// 逾期未使用憑證
        /// </summary>
        [Localization("NewCouponListFilterTypeOverdueNotUsed")]
        OverdueNotUsed = 8,
        /// <summary>
        /// 商品券(成套票券)
        /// </summary>
        [Localization("NewCouponListFilterTypeGroupCoupon")]
        GroupCoupon = 9,
        /// <summary>
        /// 商品券(成套票券)待使用
        /// </summary>
        [Localization("NewCouponListFilterTypeGroupCouponReady")]
        GroupCouponReady = 10,
        /// <summary>
        /// 商品券(成套票券)逾期未使用
        /// </summary>
        [Localization("NewCouponListFilterTypeGroupCouponNotUsed")]
        GroupCouponNotUsed = 11,
        /// <summary>
        /// 新光三越憑證
        /// </summary>
        [Localization("NewCouponListFilterTypeSKM")]
        SKM = 12,
        /// <summary>
        /// 待評價憑證
        /// </summary>
        [ExcludeBinding]
        [Localization("NewCouponListFilterTypeCouponToEvaluate")]
        CouponToEvaluate = 13,
        /// <summary>
        /// 待評價商品券(成套票券)
        /// </summary>
        [ExcludeBinding]
        [Localization("NewCouponListFilterTypeGroupCouponToEvaluate")]
        GroupCouponToEvaluate = 14,
        /// <summary>
        /// 全家寄杯
        /// </summary>
        [ExcludeBinding]
        [Localization("NewCouponListFilterTypeFamiCoffee")]
        FamiCoffee = 15,
        /// <summary>
        /// 換貨申請中
        /// </summary>
        [Localization("NewCouponListFilterTypeExchanging")]
        Exchanging = 16,
        /// <summary>
        /// 咖啡寄杯
        /// </summary>
        [ExcludeBinding]
        [Localization("NewCouponListFilterTypeDepositCoffee")]
        DepositCoffee = 17,
    }

    /// <summary>
    /// [v2.0] 憑證列表群組類型
    /// </summary>
    public enum CouponListGroupType
    {
        /// <summary>
        /// 一般憑證(團購/宅配/全家0元檔)
        /// </summary>
        Coupon = 0,
        /// <summary>
        /// 成套禮券
        /// </summary>
        GroupCoupon = 1,
        /// <summary>
        /// 寄杯
        /// </summary>
        CoffeeGroup = 2
    }

    #endregion

    /// <summary>
    /// [v3.0] 訂單主分類
    /// </summary>
    public enum MemberOrderMainFilterType
    {
        [Description("全部")]
        [Localization("MemberOrderMainFilterTypeAll")]
        All = 0,
        [Description("購物商品")]
        [Localization("MemberOrderMainFilterTypeGoods")]
        Goods = 1,
        [Description("電子票券")]
        [Localization("MemberOrderMainFilterTypePpon")]
        Ppon = 2,
        [Description("咖啡寄杯")]
        [Localization("MemberOrderMainFilterTypeCoffee")]
        Coffee = 3
    }

    /// <summary>
    /// [v3.0] 訂單子分類
    /// </summary>
    public enum MemberOrderSubFilterType
    {
        None = 0,

        /// <summary>
        /// 購物商品-待出貨
        /// </summary>
        [Description("待出貨")]
        GoodsReadyToShip = 1,
        /// <summary>
        /// 購物商品-已出貨
        /// </summary>
        [Description("已出貨")]
        GoodsShipped = 2,
        /// <summary>
        /// 購物商品-取消
        /// </summary>
        [Description("取消")]
        GoodsCancel = 3,

        /// <summary>
        /// 電子票券-待使用
        /// </summary>
        [Description("待使用")]
        PponCanUse = 4,
        /// <summary>
        /// 電子票券-使用完畢
        /// </summary>
        [Description("使用完畢")]
        PponUsed = 5,
        /// <summary>
        /// 電子票券-取消
        /// </summary>
        [Description("取消")]
        PponCancel = 6,

        /// <summary>
        /// 咖啡寄杯
        /// </summary>
        [Description("咖啡寄杯")]
        Coffee = 7,
    }

    public enum OrderFilterVerion
    {
        /// <summary>
        /// 訂單篩選 v1.0
        /// </summary>
        CouponListFilterType = 1,
        /// <summary>
        /// 訂單篩選 v2.0
        /// </summary>
        NewCouponListFilterType = 2,
        /// <summary>
        /// 訂單篩選 v3.0
        /// </summary>
        MemberOrderMainFilterType = 3
    }

    #endregion


    /// <summary>
    /// 成套票券檔次模式
    /// 1.依售價平均面額 2.依原始面額
    /// </summary>
    public enum GroupCouponDealType
    {
        [Description("")]
        None = 0,
        /// <summary>
        /// A型平均售價
        /// </summary>
        [Description("A型平均售價")]
        AvgAssign = 1,
        /// <summary>
        /// B型原價分配
        /// </summary>
        [Description("B型原價分配")]
        CostAssign = 2
    }

    public enum DealTimeSlotStatus
    {
        /// <summary>
        /// 預設模式，進入ppon/default頁面並且未傳入BID時，DEAL需加入判斷是主DEAL或副DEAL
        /// </summary>
        Default = 0,
        /// <summary>
        /// 進入ppon/default頁面且未傳入BID時，此DEAL不顯示
        /// </summary>
        NotShowInPponDefault = 1,
    }

    public enum SmsStatus
    {
        [Localization("SmsSuccess")]
        Success = 0,
        [Localization("SmsFail")]
        Fail = 1,
        [Localization("SmsQueue")]
        Queue = 2,
        [Localization("SmsReset")]
        Reset = 3,
        [Localization("SmsAtm")]
        Atm = 4,
        [Localization("SmsPpon")]
        Ppon = 5,
        [Localization("SmsCancel")]
        Cancel = 6,
        Sending = 7,
    }

    public enum SmsType
    {
        Ppon = 0,
        System = 1,
        HiDeal = 2,
        Member = 3
    }

    /// <summary>
    /// 紅利訂單狀態
    /// </summary>
    public enum CashPointStatus
    {
        Pending = 0,
        Approve = 1,
    }

    public enum CashPointListType
    {
        Income = 0,
        Expense = 1,
        Reject = 2,
    }

    public enum OrderLogStatus
    {
        [Localization("RefundCancel")]
        Cancel = -1,
        [Localization("RefundProcessing")]
        Processing = 0,
        [Localization("RefundSuccess")]
        Success = 1,
        [Localization("RefundFailure")]
        Failure = 2,
        [Localization("ProductPreparing")]
        ProductPreparing = 3,
        [Localization("ProductShipped")]
        ProductShipped = 4,
        [Localization("RefundAutoProcessing")]
        AutoProcessing = 5,
        [Localization("RefundAutoProcessingScashOnly")]
        AutoProcessingScashOnly = 6,
        [Localization("RefundProcessingScashOnly")]
        ProcessingScashOnly = 7,
        [Localization("ExchangeProcessing")]
        ExchangeProcessing = 8,
        [Localization("ExchangeSuccess")]
        ExchangeSuccess = 9,
        [Localization("ExchangeFailure")]
        ExchangeFailure = 10,
        [Localization("ExchangeCancel")]
        ExchangeCancel = 11,
        [Localization("SendToSeller")]
        SendToSeller = 12
    }

    //新版電子發票上線後， 將A0401改為C0401 ...
    public enum EinvoiceType
    {
        [Localization("NotComplete")]//ATM、超取未付款
        NotComplete = -1,
        Initial = 0,
        [Localization("A0401")]//開立 新版 : C0401
        C0401 = 1,
        [Localization("B0301")]//折讓 新版 : D0401
        D0401 = 2,
        [Localization("A0501")]//開立作廢 新版 : C0501
        C0501 = 3,
        [Localization("B0401")]//折讓作廢 新版 : D0501
        D0501 = 4,
        [Localization("Paper2Einvoice")]
        Paper2Einvoice = 5,
        [Localization("Paper3Einvoice")]
        Paper3Einvoice = 6,
        [Localization("Paper3EinvoiceMedia")]
        Paper3EinvoiceMedia = 7,
        [Localization("EinvoiceJobMessage")]
        EinvoiceJobMessage = 8,
        [Localization("DailyReport")]//每日統計
        DailyReport = 9,
        [Localization("ManualInsert")]//人工新增
        ManualInsert = 10,
        /// <summary>
        /// 新版 : 註銷發票
        /// </summary>
        C0701 = 11,
        /// <summary>
        /// 中獎相關
        /// </summary>
        Winning = 12,
    }

    public enum EinvoiceMode
    {
        /// <summary>
        /// 作廢發票
        /// </summary>
        [Localization("VoidInvoice")]
        VoidInvoice = -1,
        /// <summary>
        /// 捐贈青少年空手道推展協會
        /// </summary>
        [Localization("DonationA")]
        DonationA = 1,
        /// <summary>
        /// 捐贈創世社會福利基金會
        /// </summary>
        [Localization("DonationB")]
        DonationB = 2,
        /// <summary>
        /// 紙本發票
        /// </summary>
        [Localization("PaperInvoice")]
        PaperInvoice = 15,
        /// <summary>
        /// 紙本二聯式發票
        /// </summary>
        [Localization("PaperInvoice2")]
        PaperInvoice2 = 17,
        /// <summary>
        /// 紙本三聯式發票
        /// </summary>
        [Localization("PaperInvoice3")]
        PaperInvoice3 = 18,
        /// <summary>
        /// 電子發票
        /// </summary>
        [Localization("Einvoice")]
        Einvoice = 19,
        /// <summary>
        /// 電子二聯式發票
        /// </summary>
        [Localization("Einvoice2")]
        Einvoice2 = 21,
        /// <summary>
        /// 電子三聯式發票
        /// </summary>
        [Localization("Einvoice3")]
        Einvoice3 = 22,
    }


    /// <summary>
    /// 2014/3/1號的發票類型
    /// </summary>
    public enum InvoiceMode2
    {
        /// <summary>
        /// 2014/3/1號"前"的發票, 皆填 0
        /// </summary>
        [Description("舊版紙本")]
        Undefined = 0,
        /// <summary>
        /// 三聯式
        /// </summary>
        [Description("三聯式")]
        Triplicate = 1,
        /// <summary>
        /// 二聯式
        /// </summary>
        [Description("二聯式")]
        Duplicate = 2

        /*
            01：三聯式 
            02：二聯式 
            03：二聯式收銀機 
            04：特種稅額 
            05：電子計算機 
            06：三聯式收銀機 
         */
    }

    public enum InvoiceUserType
    {
        [Description("消費者")]
        Customer = 1,
        [Description("廠商")]
        Vendor = 2
    }

    public enum InvoiceQueryOption
    {
        InvoiceNumberTime,
        RequestTime,
    }

    public enum EinvoiceAction
    {
        /// <summary>
        /// 未定義狀態
        /// </summary>
        Init = 0,
        /// <summary>
        /// 發票開立
        /// </summary>
        New = 1,
        /// <summary>
        /// 申請印製(2聯)
        /// </summary>
        RequestDupl = 2,
        /// <summary>
        /// 申請印製(3聯)
        /// </summary>
        RequestTrip = 3,
        /// <summary>
        /// 取消申請印製
        /// </summary>
        RequestReverse = 4,
        /// <summary>
        /// 印製發票正本
        /// </summary>
        Print = 5,
        /// <summary>
        /// 印製發票副本
        /// </summary>
        PrintCopy = 6,
        /// <summary>
        /// 發票作廢
        /// </summary>
        Cancel = 7,
        /// <summary>
        /// 發票折讓
        /// </summary>
        Allowance = 8,
        /// <summary>
        /// 發票註銷
        /// </summary>
        Void = 9,
        /// <summary>
        /// 取消折讓
        /// </summary>
        CancelAllowance = 10,
        /// <summary>
        /// 發票中獎
        /// </summary>
        Winning = 11,
        /// <summary>
        /// 中獎發票回覆
        /// </summary>
        WinningResponse = 12,
        /// <summary>
        /// 抽回發票
        /// </summary>
        Withdraw = 13,
        /// <summary>
        /// 單筆印製正本
        /// </summary>
        SinglePrint = 14,
        /// <summary>
        /// 單筆印製副本
        /// </summary>
        SinglePrintCopy = 15,
        /// <summary>
        /// 2聯轉3聯
        /// </summary>
        Invoice2To3 = 16,
        /// <summary>
        /// 捐贈轉3聯
        /// </summary>
        DonateTo3 = 17,
    }

    /// <summary>
    /// 折讓單狀態:紙本折讓、電子折讓
    /// </summary>
    public enum AllowanceStatus
    {
        /// <summary>
        /// 紙本折讓
        /// </summary>
        PaperAllowance = 1,
        /// <summary>
        /// 電子折讓
        /// </summary>
        ElecAllowance = 2,
        /// <summary>
        /// 不需折讓
        /// </summary>
        None = 3,
    }

    /// <summary>
    /// 2013年3月1號為分水嶺
    /// </summary>
    public enum InvoiceVersion
    {
        Old = 0,
        CarrierEra = 1
    }

    public enum EinvoiceMailType
    {
        EinvoiceInfoMail = 1,
        EvincoeWinnerMail = 2
    }

    public enum WinnerMailSendStatus
    {
        Initial = 0,
        Ready = 1,
        Success = 2,
        Faild = 3
    }

    /// <summary>
    /// Coupon類運費設定分類
    /// </summary>
    public enum CouponFreightType
    {
        /// <summary>
        /// 收入
        /// </summary>
        Income = 0,
        /// <summary>
        /// 支出
        /// </summary>
        Cost = 1,
    }

    /// <summary>
    /// Coupon進貨運費付款對象
    /// </summary>
    public enum CouponFreightPayTo
    {
        /// <summary>
        /// 貨運公司
        /// </summary>
        [Localization("Freighter")]
        Freighter = 0,
        /// <summary>
        /// 廠商
        /// </summary>
        [Localization("Supplier")]
        Supplier = 1
    }

    /// <summary>
    /// Table CT_ATM using
    /// </summary>
    public enum AtmStatus
    {
        [Localization("Initial")]           //Order成立
        Initial = 0,
        [Localization("Paid")]              //已付款
        Paid = 1,
        [Localization("Refund")]            //退貨完成
        Refund = 2,
        [Localization("Expired")]           //過期
        Expired = 3,
        [Localization("RefundRequest")]     //使用者提出退貨
        RefundRequest = 4,
        [Localization("RefundSend")]        //退貨ACH送出
        RefundSend = 5,
        [Localization("RefundFail")]        //退貨失敗
        RefundFail = 6,
    };

    /// <summary>
    /// Table CT_ATM_MessageInform using
    /// </summary>
    public enum AtmProcessStatus
    {
        [Localization("Initial")]           //虛擬帳號產生
        Initial = 0,
        [Localization("Done")]              //AP2AP確認匯款
        Done = 1,
        [Localization("NotFound")]          //查無此帳號
        NotFound = 2,
    };

    /// <summary>
    /// Table CT_ATM_Refund using
    /// </summary>
    public enum AtmRefundStatus
    {
        [Localization("Initial")]       //退貨申請
        [Description("退貨申請")]
        Initial = -1,
        [Localization("Request")]       //提出退貨需求
        [Description("提出退貨需求")]
        Request = 0,
        [Localization("ACHProcess")]    //產生ACH P01中
        [Description("產生ACH P01中")]
        AchProcess = 1,
        [Localization("ACHSend")]       //產生完成
        [Description("產生完成")]
        AchSend = 2,
        [Localization("RefundSuccess")] //退貨完成
        [Description("退貨完成")]
        RefundSuccess = 3,
        [Localization("RefundFail")]    //退貨失敗
        [Description("退貨失敗")]
        RefundFail = 4,
    };

    /// <summary>
    /// Table CT_ATM_Refund flag
    /// </summary>
    public enum AtmRefundFlag
    {
        [Localization("AtmRefundChecked")]    //已檢查
        Checked = 1,
    };

    public enum OrderReturnStatus
    {
        [Localization("RefundCancel")]
        Cancel = -1,
        [Localization("RefundProcessing")]
        Processing = 0,
        [Localization("RefundSuccess")]
        Success = 1,
        [Localization("RefundFailure")]
        Failure = 2,
        [Localization("ExchangeProcessing")]
        ExchangeProcessing = 3,
        [Localization("ExchangeSuccess")]
        ExchangeSuccess = 4,
        [Localization("ExchangeFailure")]
        ExchangeFailure = 5,
        [Localization("ExchangeCancel")]
        ExchangeCancel = 6,
        [Localization("SendToSeller")]
        SendToSeller = 7
    }

    [Flags]
    public enum OrderReturnFlags
    {
        [Localization("Initial")]
        Initial = 0,
        [Localization("RefundManual")]      //標記此flag即人工退貨,未標記即自動退貨
        RefundManual = 1,
        [Localization("RefundScashOnly")]   //標記此flag即購物金退貨,未標記即退現金
        RefundScashOnly = 2,
    }

    /// <summary>
    /// 退款方式
    /// </summary>
    public enum RefundType
    {
        Unknown = 0,
        /// <summary>
        /// 退 17Life 購物金
        /// </summary>
        Scash = 1,
        /// <summary>
        /// 退現金 (刷退)
        /// </summary>
        Cash = 2,
        /// <summary>
        /// 用 ATM 帳號退款
        /// </summary>
        Atm = 3,
        /// <summary>
        /// 購物金轉刷退
        /// </summary>
        ScashToCash = 4,
        /// <summary>
        /// 購物金轉 ATM 退款
        /// </summary>
        ScashToAtm = 5,
        /// <summary>
        /// 退至第三方支付帳戶
        /// </summary>
        Tcash = 6,
        /// <summary>
        /// 購物金轉第三方支付帳戶
        /// </summary>
        ScashToTcash = 7
    }

    /// <summary>
    /// api 退款方式
    /// </summary>
    public enum ApiRefundType
    {
        Unknown = 0,
        /// <summary>
        /// 退 17Life 購物金
        /// </summary>
        Scash = 1,
        /// <summary>
        /// 退現金 (刷退、第三方支付)
        /// </summary>
        Cash = 2,
        /// <summary>
        /// 用 ATM 帳號退款
        /// </summary>
        Atm = 3,
        /// <summary>
        /// 超商取貨未付款
        /// </summary>
        IspUnpaid = 4
    }

    /// <summary>
    /// 退貨單進度
    /// </summary>
    public enum ProgressStatus
    {
        /// <summary>
        /// 未申請退貨
        /// </summary>
        NotRefound = -1,
        Unknown = 0,
        /// <summary>
        /// 退貨處理中
        /// </summary>
        Processing = 1,
        /// <summary>
        /// 退貨已完成
        /// </summary>
        Completed = 2,
        /// <summary>
        /// 退貨失敗 (憑證14天內已核銷; 其他退款失敗狀況)
        /// </summary>
        Unreturnable = 3,
        /// <summary>
        /// 取消退貨
        /// </summary>
        Canceled = 4,
        /// <summary>
        /// 申請 ATM 退款中
        /// </summary>
        AtmQueueing = 5,
        /// <summary>
        /// ATM 退款完成
        /// </summary>
        AtmQueueSucceeded = 6,
        /// <summary>
        /// 立即刷退型
        /// </summary>
        CompletedWithCreditCardQueued = 7,
        /// <summary>
        /// ATM 退款失敗
        /// </summary>
        AtmFailed = 8,
        /// <summary>
        /// 已前往收件(PC訂單使用,商品在消費者手上)
        /// </summary>
        Retrieving = 9,
        /// <summary>
        /// 客服待處理(PC訂單使用,商品不在消費者手上,待客服確認 || 客服建立)
        /// </summary>
        ConfirmedForCS = 10,
        /// <summary>
        /// 前往PC倉庫收件(PC訂單使用)
        /// </summary>
        RetrieveToPC = 11,
        /// <summary>
        /// 前往消費者處收件(PC訂單使用)
        /// </summary>
        RetrieveToCustomer = 12,
        /// <summary>
        /// 廠商待確認(PC訂單使用,逆物流取貨3天內已配送完成,待商家確認)
        /// </summary>
        ConfirmedForVendor = 13,
        /// <summary>
        /// 客服待處理(逆物流取貨日超過天數尚未送達,待客服確認)
        /// </summary>
        ConfirmedForUnArrival = 14,

    }

    /// <summary>
    /// 代銷退貨狀態
    /// </summary>
    public enum ChannelProgressStatus
    {
        Unknown = 0,
        /// <summary>
        /// 退貨處理中
        /// </summary>
        Processing = 1,
        /// <summary>
        /// 退貨已完成
        /// </summary>
        Completed = 2,
        /// <summary>
        /// 退貨失敗
        /// </summary>
        Unreturnable = 3,
        /// <summary>
        /// 取消退貨
        /// </summary>
        Canceled = 4
    }

    public enum VendorProgressStatus
    {
        Unknown = 0,
        /// <summary>
        /// 待處理
        /// </summary>
        [Localization("Processing")]
        Processing = 1,
        /// <summary>
        /// 待處理-已前往收件 
        /// </summary>
        [Localization("Retrieving")]
        Retrieving = 2,
        /// <summary>
        /// 待處理-無法退貨
        /// </summary>
        [Localization("UnReturnable")]
        Unreturnable = 3,
        /// <summary>
        /// 退貨已完成-已收回商品
        /// </summary>
        [Localization("CompletedAndRetrievied")]
        CompletedAndRetrievied = 4,
        /// <summary>
        /// 退貨已完成-不收回商品
        /// </summary>
        [Localization("CompletedAndNoRetrieving")]
        CompletedAndNoRetrieving = 5,
        /// <summary>
        /// 退貨已完成-未出貨
        /// </summary>
        [Localization("CompletedAndUnShip")]
        CompletedAndUnShip = 6,
        /// <summary>
        /// 待處理-無法退貨(客服已處理)
        /// </summary>
        [Localization("UnReturnable")]
        UnreturnableProcessed = 7,
        /// <summary>
        /// 退貨已完成-客服已處理
        /// </summary>
        [Localization("CompletedByCustomerService")]
        CompletedByCustomerService = 8,
        /// <summary>
        /// 退貨已完成-系統自動退
        /// </summary>
        [Localization("Automatic")]
        Automatic = 9,
        /// <summary>
        /// 待確認收件(PC訂單使用,逆物流取貨3天內已配送完成,待商家確認)
        /// </summary>
        [Localization("ConfirmedForVendor")]
        ConfirmedForVendor = 10,
        /// <summary>
        /// 客服待處理(PC訂單使用,商品不在消費者手上,待客服確認 || 客服建立)
        /// </summary>
        [Localization("ConfirmedForCS")]
        ConfirmedForCS = 11,
        /// <summary>
        /// 客服待處理(逆物流取貨日超過天數尚未送達,待客服確認)
        /// </summary>
        [Localization("ConfirmedForUnArrival")]
        ConfirmedForUnArrival = 12
    }

    public enum OrderClassification
    {
        /// <summary>
        /// 其他狀況
        /// </summary>
        Other = 0,
        /// <summary>
        /// 午餐王與P好康訂單
        /// </summary>
        LkSite = 1,
        /// <summary>
        /// 品生活
        /// </summary>
        HiDeal = 2,
        /// <summary>
        /// 17life購物金的訂單
        /// </summary>
        CashPointOrder = 3,
        /// <summary>
        /// 天貓訂單
        /// </summary>
        TmallOrder = 4,
        /// <summary>
        /// pcp交易
        /// </summary>
        PcpOrder = 5,
        /// <summary>
        /// 熟客點訂單
        /// </summary>
        RegularsOrder = 6,

        /// <summary>
        /// 雄獅
        /// </summary>
        //[Description("雄獅")]
        //LionTravelOrder = AgentOrderClassification.LionTravelOrder,

        /// <summary>
        /// 全家票券代銷
        /// </summary>
        FamiOrder = 8,

        ///// <summary>
        ///// Payeasy
        ///// </summary>
        //[Description("PayEasy")]
        //PayEasyOrder = AgentOrderClassification.PayEasyOrder,
        ///// <summary>
        ///// 消創會
        ///// </summary>
        //[Description("消創會")]
        //BuyerToBoss = AgentOrderClassification.BuyerToBoss,

        /// <summary>
        /// 新光
        /// </summary>
        [Description("新光")]
        Skm = 11,

        [Description("用PCash買購物金")]
        PcashXchOrder = 20,
        ///// <summary>
        ///// 易遊網
        ///// </summary>
        //[Description("易遊網")]
        //EzTravel = AgentOrderClassification.EzTravel,
        ///// <summary>
        ///// 寶獅
        ///// </summary>
        //[Description("寶獅")]
        //LionItri = AgentOrderClassification.LionItri
    }

    /// <summary>
    /// 交易卡片異常狀態
    /// 此狀態設定來自 HiTRUSTpay 整理之表格，請洽 Gaby 
    /// 2012/05 Create By Ahdaa
    /// </summary>
    public enum CreditcardResult
    {
        #region 銀行回傳定義代碼
        /// <summary>
        /// 援權成功
        /// </summary>
        Approve = 0,
        /// <summary>
        /// 當天交易筆數過多，或是金額過高
        /// </summary>
        ReferToCardIssue = 1,
        /// <summary>
        /// 此為查見錯誤訊息，可能原因為 超額/管制/效期錯
        /// </summary>
        UnauththorizedUsage = 5,
        /// <summary>
        /// 掛失卡
        /// </summary>
        PickupCard = 7,
        /// <summary>
        /// 無效交易(常發生於 3D 驗證失敗)
        /// </summary>
        InvalidTranscation = 12,
        /// <summary>
        /// 卡號錯誤
        /// </summary>
        InvalidCardNumber = 14,
        /// <summary>
        /// 遺失卡
        /// </summary>
        LossCard = 41,
        /// <summary>
        /// 被竊卡
        /// </summary>
        StolenCard = 43,
        /// <summary>
        /// 超額婉拒交易
        /// </summary>
        NotSufficientFund = 51,
        /// <summary>
        /// 過期卡/有效日期錯誤
        /// </summary>
        ExpiredCard = 54,
        /// <summary>
        /// 超額
        /// </summary>
        OverTransaction = 61,
        #endregion

        #region 自定義代碼，為避開銀行可能用到之數值，所以用一個極大數定義之
        /// <summary>
        /// 未定義的自定代碼，目前有可能是因為無法由數值轉字串
        /// </summary>
        UnDefinedBySelf = 10000000,
        /// <summary>
        /// 單筆消費超出上限(同一卡號交易上限為5,000元)
        /// </summary>
        OverQuantity = 10000001,
        /// <summary>
        /// 短期間連續消費(同一卡號於1小時內，連續消費總額達5,000元以上)
        /// </summary>
        OverQuantityWithin1Hr = 10000002,
        /// <summary>
        /// 國外卡
        /// </summary>
        IsForeignCard = 10000003,
        /// <summary>
        /// 被設定為黑名單
        /// </summary>
        IsBlacklist = 10000004,
        /// <summary>
        /// 單卡24h內累積消費超出上限(同一卡號於24小時內，連續消費總額達10,000元以上)
        /// </summary>
        OverQuantityWithin24Hr = 10000005,
        /// <summary>
        /// 否認交易
        /// </summary>
        DenyTranscation = 10000006,
        /// <summary>
        /// 特殊客戶
        /// </summary>
        SpecialCustomer = 10000007,
        /// <summary>
        /// 風險IP
        /// </summary>
        CreditcardFraudIp = 10000008,
        /// <summary>
        /// 風險地址
        /// </summary>
        CreditcardFraudAddress = 10000009,
        /// <summary>
        /// 風險帳號
        /// </summary>
        CreditcardFraudAccount = 10000010,
        /// <summary>
        /// 風險訂單
        /// </summary>
        CreditcardFraudOrder = 10000011,
        #endregion
    }

    /// <summary>
    /// 委託銷售
    /// </summary>
    public enum EntrustSellType
    {
        No = 0,
        PayEasyTravel = 1,
        /// <summary>
        /// 公益檔次
        /// </summary>
        KindReceipt = 2,
    }



    /// <summary>
    /// 熟客卡適用日期
    /// </summary>
    public enum AvailableDateType
    {
        /// <summary>
        /// 未設定
        /// </summary>
        [Description("")]
        None = 0,
        [Description("平日、六日及特殊節日均適用")]
        AllDay = 1,
        [Description("週六、日不適用")]
        ExceptWeekend = 2,
        [Description("特殊假日不適用")]
        ExceptSpecialHoliday = 3,
        [Description("六日及特殊節日不適用")]
        ExceptWeekendAndSpecialHoliday = 4
    }

    /// <summary>
    /// 即時產生發送折價券，回傳訊息
    /// </summary>
    public enum InstantGenerateDiscountCampaignResult
    {
        /// <summary>
        /// 成功
        /// </summary>
        [Localization("CampaignDiscountCodeSuccess")]
        [Description("折價券發送成功")]
        Success = 0,
        /// <summary>
        /// 過期
        /// </summary>
        [Localization("CampaignDiscountCodeExpire")]
        [Description("折價券過期")]
        Expire = 1,
        /// <summary>
        /// 超過
        /// </summary>
        [Localization("CampaignDiscountCodeExceed")]
        [Description("折價券已超過領取上限")]
        Exceed = 2,
        /// <summary>
        /// 不存在
        /// </summary>
        [Localization("CampaignDiscountCodeNotExist")]
        [Description("折價券活動不存在")]
        NotExist = 3,
        /// <summary>
        /// 非即時產生類型
        /// </summary>
        [Localization("CampaignDiscountCodeTypeMistake")]
        [Description("非即時產生類型")]
        TypeMistake = 4,
        /// <summary>
        /// 未經審核
        /// </summary>
        [Localization("CampaignDiscountCodeTypeUnaudited")]
        [Description("折價券未經審核")]
        Unaudited = 5,
        /// <summary>
        /// 關閉
        /// </summary>
        [Localization("CampaignDiscountCodeTypeCancel")]
        [Description("折價券已關閉")]
        Cancel = 6,
        /// <summary>
        /// 未開始
        /// </summary>
        [Description("折價券活動尚未開始")]
        NotStarted = 7,
        /// <summary>
        /// 已經發送過
        /// </summary>
        [Description("一人限領一次，已發送過折價券")]
        AlreadySent = 8,
        /// <summary>
        /// 已有該活動折價券
        /// </summary>
        [Description("一人同時間同活動預期只有(也只需要)一張")]
        AlreadyOwn = 9
    }

    /// <summary>
    /// 邀請親友折價券發送狀態
    /// </summary>
    public enum DiscountReferrerStatus
    {
        /// <summary>
        /// 初始
        /// </summary>
        Initial,
        /// <summary>
        /// 已發送
        /// </summary>
        Send,
        /// <summary>
        /// 已取消
        /// </summary>
        Cancel,
    }

    public enum DiscountReferrerUrlType
    {
        /// <summary>
        /// 檔次分享
        /// </summary>
        Deal,
        /// <summary>
        /// 活動分享
        /// </summary>
        Event,
    }

    /// <summary>
    /// Table order_return_list type
    /// </summary>
    public enum OrderReturnType
    {
        [Localization("PayTransRefund")]
        Refund = 1,
        [Localization("Exchange")]
        Exchange = 2,
    }

    /// <summary>
    /// 墨攻核銷使用狀態
    /// </summary>
    public enum MohistVerifyStatus
    {
        /// <summary>
        /// 初始
        /// </summary>
        Initial,
        /// <summary>
        /// 未使用
        /// </summary>
        NotUsed,
        /// <summary>
        /// 已使用
        /// </summary>
        Used,
        /// <summary>
        /// 作廢(退貨)
        /// </summary>
        Invalid
    }

    /// <summary>
    /// 墨攻核銷類型
    /// </summary>
    public enum MohistVerifyType
    {
        /// <summary>
        /// 核銷
        /// </summary>
        Verify,
        /// <summary>
        /// 購物金
        /// </summary>
        Ecash,
        /// <summary>
        /// 作廢
        /// </summary>
        Invalid
    }

    public enum MohistRedoFlag
    {
        Finish = 0,
        Product = 1,
        OrderDeal = 2,
        Confirm = 4
    }

    /// <summary>
    /// 訂單狀態分類
    /// </summary>
    public enum PponOrderGroupCountType
    {
        /// <summary>
        /// 未核銷
        /// </summary>
        NoVerification = 0,
        /// <summary>
        /// 已核銷
        /// </summary>
        Verification = 1,
        /// <summary>
        /// 宅配
        /// </summary>
        HomeDelivery = 2,
        /// <summary>
        /// 其他
        /// </summary>
        Other = 3,
        /// <summary>
        /// 零元檔次
        /// </summary>
        FreeDeal = 4,
    }

    /// <summary>
    /// 訂單建檔來源(Web,IOS,Android)
    /// </summary>
    public enum OrderFromType
    {
        ByWebService = 0,
        ByWeb = 1,

        #region App
        ByIOS = 2,
        ByAndroid = 3,
        ByWinPhone = 4,
        #endregion

        ByMweb = 5,
        ByMwebAndroid = 6,
        ByMwebiPad = 7,
        ByMwebiPhone = 8,
        ByMwebiPod = 9,
        Unknown = 14,
    }

    public enum GuestBuyType
    {
        MemberBuy = 0,
        MemberGuestBuy = 1,
        GuestBuy = 2,
    }

    /// <summary>
    /// 標記一筆訂單的退貨情況, 全部退貨/部份退貨/無退貨
    /// </summary>
    public enum OrderGoodsReturnStatus
    {
        NoneReturn = 0,
        PartialReturned = 1,
        AllReturned = 2
    }

    /// <summary>
    /// api回覆的訂單狀態
    /// </summary>
    public enum ApiOrderStatus
    {
        /// <summary>
        /// 錯誤或未知狀態
        /// </summary>
        None = 0,

        /// <summary>
        /// 尚未開始使用
        /// </summary>
        NotYetStarted = 1,

        /// <summary>
        /// 不能使用-檔次未成立
        /// </summary>
        FailDeal = 2,

        /// <summary>
        /// ATM未付款
        /// </summary>
        AtmUnpaid = 3,

        /// <summary>
        /// ATM超過付款期限
        /// </summary>
        AtmOverdue = 4,

        /// <summary>
        /// 未使用
        /// </summary>
        Unused = 5,

        /// <summary>
        /// 憑證已使用
        /// </summary>
        CouponAllUsed = 6,

        /// <summary>
        /// 憑證已過期
        /// </summary>
        CouponOverdue = 7,

        /// <summary>
        /// 宅配商品
        /// </summary>
        ToHouse = 8,

        /// <summary>
        /// 退貨處理中
        /// </summary>
        WaitForRefund = 9,

        /// <summary>
        /// 退貨完成
        /// </summary>
        RefundComplete = 10,

        /// <summary>
        /// 已出貨
        /// </summary>
        GoodsShipped = 11,

        /// <summary>
        /// 已過鑑賞期
        /// </summary>
        GoodsExpired = 12,

        /// <summary>
        /// 退貨失敗
        /// </summary>
        Unreturnable = 13,

        /// <summary>
        /// 換貨處理中
        /// </summary>
        ExchangeProcessing = 14,

        /// <summary>
        /// 換貨成功
        /// </summary>
        ExchangeSuccess = 15,

        /// <summary>
        /// 換貨失敗
        /// </summary>
        ExchangeFailure = 16,

        /// <summary>
        /// 換貨取消
        /// </summary>
        ExchangeCancel = 17,
        /// <summary>
        /// 準備出貨
        /// </summary>
        NotYetShipped = 18,
    }

    /// <summary>
    /// iChannel 訂單狀態
    /// </summary>
    public enum IChannelInfoStatus
    {
        /// <summary>
        /// 未確認(一般都標註為未確認)
        /// </summary>
        Unconfirmed = 0,
        /// <summary>
        /// 有效(除特殊狀況否則不會特別將訂單狀態變更為有效)
        /// </summary>
        Valid = 1,
        /// <summary>
        /// 無效(僅整筆訂單取消時才會將訂單狀態變更為無效)
        /// </summary>
        Invalid = 2,
    }

    /// <summary>
    /// 信用卡別
    /// 對應 CreditCardBankInfo 資料表 bank_id
    /// </summary>
    public enum CreditCardType
    {
        None = 0,
        VisaCard = 1,
        MasterCard = 2,
        AmericanExpress = 3,
        JCBCard = 4,
        UCard = 5,
        UnionPay = 6,
        SmartPay = 7
    }

    /// <summary>
    /// 行動支付別
    /// </summary>
    public enum MobilePayType
    {
        ApplePay = 1,
        E7LifePay = 2,
        ApplePayEMV = 3
    }

    /// <summary>
    /// 交易刷卡分期期數
    /// </summary>
    public enum OrderInstallment
    {
        No = 0,
        In3Months = 3,
        In6Months = 6,
        In12Months = 12
    }

    /// <summary>
    /// 結帳方式類型
    /// </summary>
    public enum ChannelClientPropertyCheckType
    {
        /// <summary>
        /// 月結
        /// </summary>
        Month = 1,
        /// <summary>
        /// 週結
        /// </summary>
        Week = 2
    }

    /// <summary>
    /// 出貨單類型:OrderShip
    /// </summary>
    public enum OrderShipType
    {
        /// <summary>
        /// 一般出貨
        /// </summary>
        Normal = 1,
        /// <summary>
        /// 換貨商品出貨
        /// </summary>
        Exchange = 2
    }

    /// <summary>
    /// 換貨管理:廠商處理進度
    /// </summary>
    public enum ExchangeVendorProgressStatus
    {
        Unknown = 0,
        /// <summary>
        /// 待處理:廠商處理進度未標註之預設狀態
        /// </summary>
        [Localization("Processing")]
        Processing = 1,
        /// <summary>
        /// 已前往換貨
        /// </summary>
        [Localization("ExchangeProceed")]
        ExchangeProceed = 2,
        /// <summary>
        /// 無法完成換貨
        /// </summary>
        [Localization("UnExchangeable")]
        UnExchangeable = 3,
        /// <summary>
        /// 完成換貨
        /// </summary>
        [Localization("ExchangeCompleted")]
        ExchangeCompleted = 4,
    }

    public enum CartStatus
    {
        /// <summary>
        /// 初始
        /// </summary>
        Init = 0,
        /// <summary>
        /// 已完成
        /// </summary>
        Complete = 1
    }

    public enum CartDealStatus
    {
        /// <summary>
        /// 正常
        /// </summary>
        Normal = 0,
        /// <summary>
        /// 已結檔
        /// </summary>
        Closed = 1,
        /// <summary>
        /// 已售完
        /// </summary>
        SoldOut = 2,
        /// <summary>
        /// 數量不足
        /// </summary>
        Insufficient = 3
    }

    /// <summary>
    /// 購物車折價券使用狀態
    /// </summary>
    public enum CartDiscountUseState
    {
        /// <summary>
        /// 可使用
        /// </summary>
        CanUse = 0,
        /// <summary>
        /// 使用中
        /// </summary>
        Using = 1,
        /// <summary>
        /// 已被使用(或無法使用)
        /// </summary>
        Used = 2,
        /// <summary>
        /// 無此折價券
        /// </summary>
        NoDiscount = 3
    }
    public enum ProductItemStatus
    {
        /// <summary>
        /// 已刪除
        /// </summary>
        Deleted = -1,
        /// <summary>
        /// 正常
        /// </summary>
        Normal = 0,
        /// <summary>
        /// 停用
        /// </summary>
        Disabled = 1
    }
    public enum ProductDataSource
    {
        /// <summary>
        /// 系統KeyIn
        /// </summary>
        KeyIn = 1,
        /// <summary>
        /// 匯入
        /// </summary>
        Import = 2,
    }

    public enum ProductStockFilter
    {
        [Description("正常供貨(大於0)")]
        Normal = 1,
        [Description("小於等於安全庫存量")]
        LessThanBase = 2,
        [Description("已無庫存(小於等於0)")]
        Zero = 3

    }

    public enum ProductDeliveryType
    {
        /// <summary>
        /// 一般宅配
        /// </summary>
        [Description("宅配")]
        Normal = 0,
        /// <summary>
        /// Family店取
        /// </summary>
        [Description("全家超取")]
        FamilyPickup = 1,
        /// <summary>
        /// Seven店取
        /// </summary>
        [Description("7-11超取")]
        SevenPickup = 2,
        /// <summary>
        /// Wms (Pchome)
        /// </summary>
        [Description("Wms 宅配")]
        Wms = 3,
    }

    /// <summary>
    /// 訂單超取服務類型
    /// </summary>
    public enum IspOrderType
    {
        /// <summary>
        /// B2C
        /// </summary>
        [Description("B2C")]
        B2C = 1,
        /// <summary>
        /// C2C
        /// </summary>
        [Description("C2C")]
        C2C = 2
    }

    public enum CreditcardFraudDataType
    {
        /// <summary>
        /// CustomerCare
        /// </summary>
        [Description("客服提供")]
        CustomerCare = 1,
        /// <summary>
        /// IP
        /// </summary>
        [Description("相關風險IP")]
        IP = 2,
        /// <summary>
        /// Account
        /// </summary>
        [Description("相關風險帳號")]
        Account = 3,
        /// <summary>
        /// CheckAccount
        /// </summary>
        [Description("確認中的風險帳號")]
        CheckAccount = 4,
        /// <summary>
        /// CheckAccount
        /// </summary>
        [Description("安全帳號")]
        SafeAccount = 5,
        /// <summary>
        /// Test
        /// </summary>
        [Description("測試")]
        Test = 10
    }

    /// <summary>
    /// Shopback 分潤種類
    /// </summary>
    public enum ShopbackCommissionType
    {
        /// <summary>
        /// 家電-a3
        /// </summary> 
        a3 = 0,
        /// <summary>
        /// 宅配(不含家電)有用券-d7
        /// </summary> 
        d7 = 1,
        /// <summary>
        /// 宅配(不含家電)沒用券-d3
        /// </summary> 
        d3 = 2,
        /// <summary>
        /// 票券有用券-t5
        /// </summary> 
        t5 = 3,
        /// <summary>
        /// 票券沒用券-t2
        /// </summary> 
        t2 = 4,
    }

    public enum ShopbackValidationStatus
    {
        rejected = 0,

        approved = 1,
    }

    public enum ShopbackSendType
    {
        /// <summary>
        /// 還未呼叫 shopback api
        /// </summary>
        ReadySend = 0,
        /// <summary>
        /// 呼叫 shopback order api 完成
        /// </summary>
        OrderAPIFinish = 1,
        /// <summary>
        /// 呼叫 shopback validation api 完成
        /// </summary>
        ValidationAPIFinish = 2,
        /// <summary>
        /// 中止(異常)
        /// </summary>
        Abort = 3,
    }

    /// <summary>
    /// PezChannel 訂單狀態
    /// </summary>
    public enum PezChannelStatus
    {
        /// <summary>
        /// 未確認
        /// </summary>
        Unconfirmed = 0,
        /// <summary>
        /// 有效
        /// </summary>
        Valid = 1,
        /// <summary>
        /// 無效
        /// </summary>
        Invalid = 2,
    }

    public enum LineShopSendType
    {
        /// <summary>
        /// 還未呼叫 LineShop api
        /// </summary>
        ReadySend = 0,
        /// <summary>
        /// 呼叫 LineShop orderinfo api 完成
        /// </summary>
        OrderInfoAPIFinish = 1,
        /// <summary>
        /// 呼叫 LineShop orderfinish api 完成
        /// </summary>
        OrderFinishAPIFinish = 2,
        /// <summary>
        /// 中止(異常)
        /// </summary>
        Abort = 3,
    }

    public enum AffiliatesSendType
    {
        /// <summary>
        /// 還未呼叫 api
        /// </summary>
        ReadySend = 0,
        /// <summary>
        /// 呼叫 order api 完成
        /// </summary>
        OrderAPIFinish = 1,
        /// <summary>
        /// 呼叫 validation api 完成
        /// </summary>
        ValidationAPIFinish = 2,
        /// <summary>
        /// 中止(異常)
        /// </summary>
        Abort = 3,
    }
}


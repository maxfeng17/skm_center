﻿
using System.ComponentModel;

namespace LunchKingSite.Core
{
    /// <summary>
    /// 付款方式
    /// </summary>
    public enum BuyTransPaymentMethod
    {
        [Description("未選擇付款方式")]
        Init = 0,
        [Description("紅利、購物金一次折抵")]
        BonusOrScashPayOff = 1,
        [Description("信用卡一次付清")]
        CreaditCardPayOff = 2,
        [Description("信用卡分期")]
        CreaditCardInstallment = 3,
        [Description("ATM")]
        Atm = 4,
        [Description("MasterPass")]
        MasterPass = 5,
        [Description("TaishinPay")]
        TaishinPay = 6,
        [Description("LinePay")]
        LinePay = 7,
        [Description("ApplePay")]
        ApplePay = 8,
        [Description("Family超商付款")]
        FamilyIsp = 9,
        [Description("Seven超商付款")]
        SevenIsp = 10,
    }

    /// <summary>
    /// 付款階段 (按順序編號)
    /// </summary>
    public enum BuyTransPaymentSection
    {
        [Description("階段一-進入Buy頁")]
        BuyInit = 10,
        [Description("階段二-送出訂單(已選擇付款方式)")]
        BuySubmitOrder = 20,
        [Description("階段三-輸完信用卡資料")]
        BuyTypeCreaditCardNumEnd = 30,
        [Description("階段三-結束交易")]
        BuyEnd = 40,
    }
}

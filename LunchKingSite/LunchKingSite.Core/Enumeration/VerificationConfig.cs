﻿using System;
using System.ComponentModel;
using LunchKingSite.Core.Component;

namespace LunchKingSite.Core
{
    /// <summary>
    /// 執行核銷的狀態(Verification Log)
    /// </summary>
    public enum VerificationStatus
    {
        CouponInit = 0,     //初始
        CouponChk = 1,      //核銷確認中。於verification_log資料庫中若是此數據，表示憑證已通過審核，但店家點選取消，未真的核銷之！
        CouponOk = 2,       //核銷完成
        CouponNone = 3,     //無此憑證
        CouponOver = 4,     //憑證不在使用期限內
        CouponUsed = 5,     //憑證已使用
        CouponReted = 6,    //退貨或刷退
        CouponError = 7,    //憑證號碼錯誤
        CashTrustLogErr = 8,        //CashTrustLog寫入錯誤
        CashTrustStatusLogErr = 9,  //CashTrustStatusLog寫入錯誤
        DuInTime = 10,       //已過兌換時間，但仍在一期間的緩衝兌換時間內(現在為一個月)
        CouponReting = 11,   //整份訂單退貨申請中
        UndoVerify = 12,     //恢復成未核銷狀態
        CouponLocked = 13,   //憑證鎖定中不可使用
    }

    /// <summary>
    /// 建立帳號之帳號(Store Acct Info Log)
    /// </summary>
    public enum VerificationAcctStatus
    {
        Invalid = 0,        //無效/停權
        Create = 1,         //初始/建立
        Login = 2,          //登入
        Register = 4,       //註冊
        ReRegister = 8,     //重新註冊

        //修改 10bits
        ModifyRegisterCode = 1024,  //修改註冊碼
        ModifyAcct = 2048,          //修改登入帳號
        ModifyPw = 4096,            //修改登入密碼
        ModifyIMEI = 8192,          //修改機碼
        ModifySIM = 16384,          //修改SIM卡號
    }

    public enum VerificationAccountStatusDetail
    {
        Invalid = 0,
        
        //Account Status
        Login = 100,            //登入
        Logout = 101,           //登出
        Logined = 102,          //已登入
        LoginTimeout = 103,     //已過登出時間，跨日
        LoginError = 104,       //登入錯誤
        ReLogin = 105,          //重登入
        ReLoginError = 106,     //重登入錯誤

        //Register Status
        Regist = 200,               //已註冊
        ReRegist = 201,             //重新註冊
        CancelRegist = 202,         //取消註冊(停權)
        UnRegist = 203,             //尚未註冊
        RegistOk = 204,             //註冊完成
        RegistError = 205,          //註冊錯誤
        Registed = 206,             //已註冊過
        ResetRegistOk = 207,        //重新註冊完成
        ResetRegistError = 208,     //重新註冊錯誤
        UndoRegistOk = 209,         //清除註冊完成
        UndoRegistError = 210,      //清除註冊錯誤
        ModifyRegisterCode = 211,   //修改註冊碼
        ModifyAcct = 212,           //修改登入帳號
        ModifyPw = 213,             //修改登入密碼
        ModifyIMEI = 214,           //修改機碼
        ModifySIM = 215,            //修改SIM卡號
        ModifyIMSEI = 216,          //修改SIM以及機碼號

        //Deal Status
        DealNone = 301,             //沒有檔次
        DealDeliver = 302,          //在兌換期間內
        DealDeliverNotYet = 303,    //檔次尚未開始兌換
        DealDeliverOverTime = 304,  //該檔次已超過兌換期間
        DealDeliverOver = 305,      //檔次已全部兌換完畢
    }

    public enum UndoVerificationFlag
    {
        Init = 0,   //初始
        Undo = 1,   //將 Verifcation Log 的 status 為核銷完成之 undo_flag 標記設定為恢復核銷
    }

    /// <summary>
    /// [verification_statistics_log].[status] 
    /// </summary>
    [Flags]
    public enum VerificationStatisticsLogStatus
    {
        /// <summary>
        /// 核銷鎖定
        /// </summary>
        Locked = 1,

        /// <summary>
        /// 核銷完成
        /// </summary>
        Finished = 2,

        /// <summary>
        /// 核銷結案
        /// </summary>
        Closed = 4,

        /// <summary>
        /// 清冊遺失
        /// </summary>
        Lost = 8,

        /// <summary>
        /// 強制核銷
        /// </summary>
        ForcedVerified = 16,

        /// <summary>
        /// 強制退貨
        /// </summary>
        ForcedReturned = 32,

        /// <summary>
        /// 對帳單
        /// </summary>
        BalanceSheet = 64
    }

    public enum VerificationQueryStatus
    {
        /// <summary>
        /// 未核銷
        /// </summary>
        [Localization("VerificationQueryStatus_Init")]
        Init = 1,
        /// <summary>
        /// 已核銷
        /// </summary>
        [Localization("VerificationQueryStatus_Verified")]
        Verified = 2,
        /// <summary>
        /// 退貨
        /// </summary>
        [Localization("VerificationQueryStatus_Refund")]
        Refund = 3,
    }


    public enum VbsCouponVerifyState
    {
        /// <summary>
        /// 無效
        /// </summary>
        [Description("")]
        Null,
        /// <summary>
        /// 已核銷
        /// </summary>
        [Description("已核銷")]
        Verified,
        /// <summary>
        /// 未核銷
        /// </summary>
        [Description("未核銷")]
        UnUsed,
        /// <summary>
        /// 退貨
        /// </summary>
        [Description("完成退貨")]
        Return,
        /// <summary>
        /// 退貨中
        /// </summary>
        [Description("退貨中")]
        Returning,
        /// <summary>
        /// 系統銷帳
        /// </summary>
        [Description("逾期未使用 系統銷帳")]
        ExpiredVerified
    }

    /// <summary>
    /// 核銷憑證列表，畫面顯示的過瀘選項
    /// </summary>
    public enum VbsVerificationShowType
    {
        [Description("全部")] All,
        [Description("已核銷")] Verified,
        [Description("未核銷")] Unused,
        [Description("已退貨")] Return,
        [Description("退貨中")] Returning,
        [Description("逾期未使用 系統銷帳")] SysExpiredVerified
    }

    public enum VbsStoreFilter
    {
        ///<summary>
        ///分店
        ///</summary>
        BranchStore,
        ///<summary>
        ///總店
        ///</summary>
        HeaderOffice,
        ///<summary>
        ///分店與總店
        ///</summary>
        BranchStoreAndHeaderOffice,
        ///<summary>
        ///全部，不做任何過瀘
        ///</summary>
        All,
    }

    /// <summary>
    /// 商家公佈欄的公布對象
    /// </summary>
    public enum VbsBulletinBoardPublishSellerType 
    { 
        /// <summary>
        /// 都不公布
        /// </summary>
        None,
        /// <summary>
        /// 憑證店家
        /// </summary>
        ToShop = 1,
        /// <summary>
        /// 宅配店家
        /// </summary>
        ToHouse = 2,
        /// <summary>
        /// 全都公布
        /// </summary>
        All = 3
    }

    /// <summary>
    /// 商家公佈欄的訊息狀態
    /// </summary>
    public enum VbsBulletinBoardPublishStatus 
    { 
        /// <summary>
        /// 啟用
        /// </summary>
        Enabled = 1,
        /// <summary>
        /// 刪除
        /// </summary>
        Deleted = 2
    }

    /// <summary>
    /// 成套商品核銷檢核結果
    /// </summary>
    public enum GroupCouponVerifyCheckType
    {
        /// <summary>
        /// 數量足夠
        /// </summary>
        Success = 0,
        /// <summary>
        /// 總數量不足
        /// </summary>
        QtyNotEnough = 1,
        /// <summary>
        /// 有價憑證不足
        /// </summary>
        CouponNotEnough = 2,
        /// <summary>
        /// 贈品不足
        /// </summary>
        GiveawayCouponNotEnouth = 3
    }
}

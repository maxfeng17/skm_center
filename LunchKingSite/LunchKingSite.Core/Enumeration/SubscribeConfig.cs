﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.Core
{
    /// <summary>
    /// 訂閱電子的狀態
    /// </summary>
    /// <remarks>
    /// [2011/05/13] Ahdaa 增加取消訂閱代號:4
    /// </remarks>
    public enum SubscribeType
    {
        /// <summary>
        /// 一般訂閱(非活躍會員)
        /// </summary>
        Sleep = 0,
        /// <summary>
        /// 一般訂閱(活躍會員))
        /// </summary>
        Default = 1,
        /// <summary>
        /// 訂閱送$50活動 (2011/3)
        /// </summary>
        Subscribe50 = 2,
        /// <summary>
        /// 取消訂閱 (2011/5)
        /// </summary>
        Unsubscribe = 4,
        /// <summary>
        /// 訂閱活動 (2011/8)
        /// </summary>
        Event = 8
    }

    public enum SubscribeQuantitySent
    {
        PeautyEdmDealNo = 7,  //1筆Main Deal，6筆Side Deal
        PponEdmPeautyDealNo = 1, //各城市EDM附帶一筆P玩美
        PponEdmDealNo = 4
    }

    //新版EDM設定型態
    public enum EdmMainType
    {
        //每日固定發送EDM
        Daily,
        //手動新增發送
        Manual
    }

    //新版EDM版型區塊
    public enum EdmDetailType
    {
        //AD Banner區塊
        AD,
        //PayEasy廣告區塊
        PEZ,
        //該區域主區塊1(預設1檔)
        MainDeal_1,
        MainDeal_1_Items,
        //該區域主區塊2
        MainDeal_2,
        MainDeal_2_Items,
        //其他附加區域1
        Area_1,
        Area_1_Items,
        //其他附加區域2
        Area_2,
        Area_2_Items,
        //其他附加區域3
        Area_3,
        Area_3_Items,
        //附加區域PiinLife
        PiinLife_1,
        PiinLife_Items_1,
        PiinLife_2,
        PiinLife_Items_2,
        PponPiinLife_1,
        PponPiinLife_Item_1

    }

    /// <summary>
    ///  eDM 編輯檔次排序方式， for EdmDetailsSetup.ascx
    /// </summary>
    public enum EDMDetailsSortType
    {
        BySequence = 0,
        ByOrderedQuantity = 1,
        ByOrderedTotal = 2,
        ByOrderedDate = 3,
    }

    /// <summary>
    ///  eDM 編輯檔次排序方式， for EdmDetailsSetup.ascx
    /// </summary>
    public enum EDMAreaType
    {
        Close = 0,
        Open = 1,
        Delete = 2,
    }
}

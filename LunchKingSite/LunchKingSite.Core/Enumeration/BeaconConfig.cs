﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.Core.Enumeration
{
    public enum BeaconUuidType
    {
        /// <summary>
        /// 電量UUID
        /// </summary>
        ElectricUuid = 0,
        SellerUuid = 1,
    }

    public enum BeaconTriggerAppType
    {
        Life17Beacon = 1,
        SkmBeaconBeacon = 2
    }

    public enum BeaconTriggerAppFunctionName
    {
        [Description("17Life")]
        life17 = 1,
        [Description("新光三越")]
        skm = 2,
    }

    public enum BeaconPushShowType
    {
        /// <summary>
        /// 沒有任何顯示通知
        /// </summary>
        NoAlert = 0,

        /// <summary>
        /// 會秀視窗顯示通知
        /// </summary>
        ShowAlert = 1,
    }

    public enum BeaconTriggerType
    {
        /// <summary>
        /// 全Beacon
        /// </summary>
        All = 0,
        /// <summary>
        /// 群組
        /// </summary>
        Group = 1,
        /// <summary>
        /// 群組&指定Beacon
        /// </summary>
        GroupAndCustom = 2,
        /// <summary>
        /// 指定Beacon
        /// </summary>
        Custom = 3
    }

    public enum LinkType
    {
        /// <summary>
        /// 群組類型Beacon
        /// </summary>
        Group = 0,
        /// <summary>
        /// 自訂類型(單獨)Beacon
        /// </summary>
        GroupInCustom = 1
    }

    /// <summary>
    /// 父訊息，子訊息共用此類別
    /// </summary>
    public enum EventType
    {
        /// <summary>
        /// 純訊息類別
        /// </summary>
        Message = 1,
        /// <summary>
        /// 檔次類別
        /// </summary>
        Bid = 2,
        /// <summary>
        /// 網址類別
        /// </summary>
        Url = 3,
        /// <summary>
        /// 子訊息類型
        /// </summary>
        SubEvent = 4,
        /// <summary>
        /// SkmPayOrder
        /// </summary>
        SkmPayMsg = 5,
        /// <summary>
        /// 訂單列表(未取貨)
        /// </summary>
        [Description("skmapp://www.skm.com/orderlist?type=0")]
        OrderListNotVerified = 6,
        /// <summary>
        /// 訂單列表(已取貨)
        /// </summary>
        [Description("skmapp://www.skm.com/orderlist?type=1")]
        OrderListVerified = 7,
        /// <summary>
        /// 訂單列表(取消)
        /// </summary>
        [Description("skmapp://www.skm.com/orderlist?type=2")]
        OrderListCanceled = 8,
        /// <summary>
        /// 優惠券列表(未使用)
        /// </summary>
        [Description("skmapp://www.skm.com/couponlist?type=0")]
        CouponListNotVerified = 9,
        /// <summary>
        /// 優惠券列表(全部)
        /// </summary>
        [Description("skmapp://www.skm.com/couponlist?type=1")]
        CouponListVerified = 10,
    }

    /// <summary>
    /// Beacon表單名稱
    /// </summary>
    public enum BeaconTableName
    {
        /// <summary>
        /// 主訊息事件表單
        /// </summary>
        Event = 0,
        /// <summary>
        /// 子訊息事件表單
        /// </summary>
        SubEvent = 1
    }

    /// <summary>
    /// 會員條件
    /// </summary>
    public enum BeaconIsMember
    {
        /// <summary>
        /// 全會員
        /// </summary>
        all = 0,
        /// <summary>
        /// 會員
        /// </summary>
        IsMember = 1,
        /// <summary>
        /// 非會員
        /// </summary>
        IsNotMember = 2,

    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace LunchKingSite.Core.Enumeration
{
    public enum ExhibitionEventStatus
    {
        /// <summary>
        /// 草稿
        /// </summary>
        Draft = 0,
        /// <summary>
        /// 確認
        /// </summary>
        Confirmed = 1,
        /// <summary>
        /// 手動關閉
        /// </summary>
        Cancel = 2
    }

    public enum ExhibitionLogType
    {
        System = 0,
        Category = 1,
        Event = 2,
        Store = 3,
        Deal = 4,
        Icon = 5
    }

    public enum ExhibitionLogCrud
    {
        SystemLog = 0,
        Create = 1,
        Read = 2,
        Update = 3,
        Delete = 4
    }
}
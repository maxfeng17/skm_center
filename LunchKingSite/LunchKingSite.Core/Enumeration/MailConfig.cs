﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using LunchKingSite.Core.Component;

namespace LunchKingSite.Core
{
    public enum MailAgent
    {
        SiteMailer = 0,
        Dummy = 100,
    };

    public enum SendPriorityType
    {
        Normal = 0,
        Immediate = 1,
        Async = 2
    };

    public enum SmtpMailServerAgent
    {
        /// <summary>
        /// 正常功能
        /// </summary>
        SmtpMailAgent = 0,
        /// <summary>
        /// 只有HOST與ServiceEmail的HOST相同才會寄出
        /// </summary>
        SiteSmtpMailAgentFilterHost = 1,
        /// <summary>
        /// 正常功能，有負載平衡
        /// </summary>
        SmtpMailLoadBalanceAgent = 2,
    }

    public enum MailServerSendType
    {
        /// <summary>
        /// 使用PostMan物件寄送
        /// </summary>
        PostMan = 0,
        /// <summary>
        /// 使用外部Mail Server寄送
        /// </summary>
        MailServer = 1
    } ;

    public enum MailContentType
    {
        Authorized,
        RefundApply,
        RefundResult,
        ReturnAll,
        ReturnPartial,
        ReturnFailure,
        DealSuccess,
        /// <summary>
        /// 購物金轉現金
        /// </summary>
        RefundCreditcard,
        /// <summary>
        /// 無折讓單 寄回提醒
        /// </summary>
        RefundNoticeDiscount,       
        /// <summary>
        /// 無發票 寄回提醒
        /// </summary>
        RefundNoticeInvoice,       
        /// <summary>
        /// 無申請書 寄回提醒
        /// </summary>
        RefundNoticePaper,          
        /// <summary>
        /// 無折讓單+發票 寄回提醒
        /// </summary>
        RefundNoticeDiscountInvoice,
        /// <summary>
        /// 無折讓單+申請書 寄回提醒
        /// </summary>
        RefundNoticeDiscountPaper,  
        /// <summary>
        /// 無發票+申請書 寄回提醒
        /// </summary>
        RefundNoticeInvoicePaper,   
        /// <summary>
        /// 缺資料 自動轉退購物金
        /// </summary>
        RefundToScashNoPaper,
        /// <summary>
        /// 補寄退貨申請書
        /// </summary>
        ReturnApplicationformResend,
        /// <summary>
        /// 補寄折讓單
        /// </summary>
        ReturnDiscountResend,       
    };

    public enum MailSendCity
    {
        [Localization("EDM_Null")]
        Null,
        [Localization("EDM_AllCitys")]
        AllCitys,
        [Localization("EDM_PBU")]
        Pbu,
        [Localization("EDM_TRA")]
        Tra,
        [Localization("EDM_Delivery")]
        Delivery,
        [Localization("EDM_TP")]
        TP,
        [Localization("EDM_TY")]
        TY,
        [Localization("EDM_XZ")]
        XZ,
        [Localization("EDM_TC")]
        TC,
        [Localization("EDM_TN")]
        TN,
        [Localization("EDM_GX")]
        GX,
        [Localization("EDM_North")]
        North,
        [Localization("EDM_South")]
        South,
        Old
    };

    public enum PiinlifeMailSendCity
    {
        /// <summary>
        /// 不寄送
        /// </summary>
        [Localization("EDM_Null")]
        Null = -9999,
        /// <summary>
        /// 全送(排除重復以及取消訂閱者)
        /// </summary>
        [Localization("Piinlife_EDM_All")]
        All = -1,
        /// <summary>
        /// 所有城市
        /// </summary>
        [Localization("Piinlife_EDM_AllCity")]
        AllCity = 0,
        /// <summary>
        /// 臺北
        /// </summary>
        [Localization("EDM_TP")]
        TP = 1,
        /// <summary>
        /// 總覽
        /// </summary>
        [Localization("Piinlife_EDM_Overview")]
        Overview = 2,
        /// <summary>
        /// 高雄
        /// </summary>
        [Localization("EDM_GX")]
        GX = 3,
        /// <summary>
        /// 臺中
        /// </summary>
        [Localization("EDM_TC")]
        TC = 4,
        /// <summary>
        /// 臺南
        /// </summary>
        [Localization("EDM_TN")]
        TN = 5,
    }

    public enum IDEASMailContentType
    {
        /// <summary>
        /// 付款完成
        /// </summary>
        PaymentCompleted,
        /// <summary>
        /// 退貨完成。
        /// </summary>
        ReturnAll,
    }

    public enum PromoSEOType
    {
        /// <summary>
        /// 商品主題活動頁
        /// </summary>
        EventPromo = 0,
        /// <summary>
        /// 行銷活動列表
        /// </summary>
        PromoActivit = 1,
        /// <summary>
        /// 行銷Promo頁管理
        /// </summary>
        PromoManage = 2
    }

    public enum ContactType
    {
        /// <summary>
        /// 實體商品
        /// </summary>
        BusinessMail = 0,
        /// <summary>
        /// 非實體商品
        /// </summary>
        MarketMail = 1,
        /// <summary>
        /// 行銷與公益合作
        /// </summary>
        KindMail = 2
    }

    public enum MailTemplateType
    {
        [Description("其它")]
        Other = 0,
        /// <summary>
        /// 會員重設密碼通知
        /// </summary>
        [Description("會員-重設密碼通知")]
        MemberForgetPassword = 1000,
        /// <summary>
        /// 會員付款成功通知
        /// </summary>
        [Description("會員-付款成功通知")]
        MemberPponPaymentSuccessful = 1010,
        /// <summary>
        /// 會員ATM付款成功通知
        /// </summary>
        [Description("會員-ATM付款通知")]
        MemberPponATMPaymentSuccessful = 1011,
        /// <summary>
        /// 新版憑證退貨通知信
        /// </summary>
        [Description("會員-憑證退貨通知")]
        MemberRefundFormNoticeMail = 1020,
        /// <summary>
        /// 新版宅配退貨通知信
        /// </summary>
        [Description("會員-宅配退貨通知")]
        MemberRefundFormNoticeMailP = 1021,
    }

    public enum EdmLogType
    {
        /// <summary>
        /// 訂閱
        /// </summary>
        Default = 0,
        /// <summary>
        /// 訂閱
        /// </summary>
        subscription = 1,
        /// <summary>
        /// 取消訂閱
        /// </summary>
        unsubscribe = 2
    };
}

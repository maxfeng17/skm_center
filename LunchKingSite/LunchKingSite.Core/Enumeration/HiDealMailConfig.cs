﻿using LunchKingSite.Core.Component;

namespace LunchKingSite.Core.Enumeration
{
    public class HiDealMailConfig
    {
        public enum PiinlifeEDMGetTempStatus
        {
            OK = 0,
            /// <summary>
            /// 發生錯誤
            /// </summary>
            Error = 1,
            /// <summary>
            /// 路徑不存在
            /// </summary>
            PathNotExists = 2,
            /// <summary>
            /// 檔案不存在
            /// </summary>
            FileNotExists = 3,
            /// <summary>
            /// 郵件地址不合法
            /// </summary>
            EmailIllegal = 4,
            /// <summary>
            /// 使用 Postman 寄送錯誤
            /// </summary>
            SendWithPostManError = 5,
        }

        public enum PiinlifeEDMMailServerType
        {
            MailServer,
            PostMan
        }

    }

    public enum PiinlifeNotificationType
    {
        /// <summary>
        /// 密碼設定信
        /// </summary>
        PasswordResetConfirmation,

        /// <summary>
        /// 購買成功通知信
        /// </summary>
        PurchaseSuccessNotification,

        /// <summary>
        /// 等待付款通知信
        /// </summary>
        AtmTransferReminder,

        /// <summary>
        /// 電子發票開立通知信
        /// </summary>
        EInvoice,

        /// <summary>
        /// 發票中獎通知信
        /// </summary>
        EInvoiceWonNotification,

        /// <summary>
        /// 退貨成功通知信
        /// </summary>
        RefundSuccessNotification,

        /// <summary>
        /// 退貨退款失敗通知信
        /// </summary>
        RefundFailureNotification,

        /// <summary>
        /// 退貨申請書通知信
        /// </summary>
        RefundApplicationFormNotification,

        /// <summary>
        /// 優惠使用提醒通知信
        /// </summary>
        StartUseReminder,

        /// <summary>
        /// 優惠到期提醒通知信
        /// </summary>
        ExpireReminder,

        /// <summary>
        /// 使用者索取好康憑證
        /// </summary>
        CouponMail,

        /// <summary>
        /// (憑證)退貨申請書通知信
        /// </summary>
        RefundCouponNotification,

        /// <summary>
        /// (商品)退貨申請書通知信
        /// </summary>
        RefundGoodsNotification,

        /// <summary>
        /// 退回或折讓單
        /// </summary>
        DiscountSingleFormNotification,
    }

    public enum PiinlifeSubscriptionPromoStatus
    {
        /// <summary>
        /// 初始值，可用狀態
        /// </summary>
        Init = 0,
        /// <summary>
        /// 取消
        /// </summary>
        Cancel = 1,
        /// <summary>
        /// 被刪除
        /// </summary>
        Delete = 2,
    }

    public enum PiinlifeSubscriptionFlag
    {
        /// <summary>
        /// 一般狀態
        /// </summary>
        [SystemCode("General", 0)]
        General = 0,
        /// <summary>
        /// 取消訂閱
        /// </summary>
        [SystemCode("Cancel", 1)]
        Cancel = 1,
        /// <summary>
        /// 重新訂閱
        /// </summary>
        [SystemCode("Resubscribe", 2)]
        Resubscribe = 2,
    }

    //refund_form_Type 
    public enum HiDealRefundFormType
    {
        None = 0,                 //RefundApplicationForm及DiscountSingleForm皆不產出
        RefundApplicationForm = 1,//RefundApplicationForm 產出
        DiscountSingleForm = 2,   //DiscountSingleForm 產出
        All = 3,                  //RefundApplicationForm及DiscountSingleForm皆產出
    }
}

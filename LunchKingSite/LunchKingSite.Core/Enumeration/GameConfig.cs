﻿using System.ComponentModel;

namespace LunchKingSite.Core.Enumeration
{
    public enum GameLevel
    {
        [Description("簡單")]
        Easy = 1,
        [Description("普通")]
        Medium = 2,
        [Description("困難")]
        Hard = 3,
    }
    /// <summary>
    /// 目前就一個砍價遊戲
    /// </summary>
    public enum GameType
    {
        Undefined = 0,
        CutDown = 1,
    }
    /// <summary>
    /// 如果是 0  需再加以判斷遊戲時間與完成人數，超過時間，0改成-1
    /// 如果是 0  需再加以判斷遊戲時間與完成人數，達成條件，0改成 1
    /// </summary>
    public enum GameRoundStatus
    {
        /// <summary>
        /// 判斷Buddy數、判斷有效時間
        /// </summary>
        Running = 0,
        /// <summary>
        /// 判斷Buddy數、判斷有效時間、是否己領取
        /// </summary>
        Completed = 1,
        /// <summary>
        /// 判斷Buddy數、判斷有效時間
        /// </summary>
        Fail = -1    
    }

    public enum GameRoundRewardStatus
    {
        None = 0,
        /// <summary>
        /// 如果有 deal_game_order 則為 true
        /// </summary>
        Redeemed = 1,
        /// <summary>
        /// 超過48小時資格失效
        /// </summary>
        Expired = -1
    }

    public enum RewardMemberGameBonusStatus
    {
        /// <summary>
        /// 資料不符、會員不符
        /// </summary>
        NoMatching = 0,
        /// <summary>
        /// 領取獎勵成功
        /// </summary>
        Success = 1,
        /// <summary>
        /// 不符獎勵條件(一個帳號同一天只能收到一次獎勵)
        /// </summary>
        NoMatchingGrantCondition = 2,
        /// <summary>
        /// 紅利金已經發放完畢 
        /// </summary>
        OverBonusLimit = 3,
    }
}

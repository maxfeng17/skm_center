﻿namespace LunchKingSite.Core
{
    public enum EmployeeDept
    {
        None,
        /// <summary>
        /// 業務部
        /// </summary>
        S000,
        /// <summary>
        /// 創意編輯部
        /// </summary>
        P000,
        /// <summary>
        /// 營運稽核部
        /// </summary>
        A000,
        /// <summary>
        /// 稽核部
        /// </summary>
        AA00,
        /// <summary>
        /// 財務部
        /// </summary>
        F000,
        /// <summary>
        /// 客服部
        /// </summary>
        C000,
        /// <summary>
        /// 企業服務部
        /// </summary>
        CCS0,
        /// <summary>
        /// 行銷部
        /// </summary>
        M000,
        /// <summary>
        /// 商審部
        /// </summary>
        Q000,
        /// <summary>
        /// 產品處
        /// </summary>
        PD00,
        /// <summary>
        /// 其他
        /// </summary>
        ETC0,
    }

    public enum EmployeeChildDept
    {
        /// <summary>
        /// 業務部(台北)
        /// </summary>
        S001,
        /// <summary>
        /// 業務部(桃園)
        /// </summary>
        S002,
        /// <summary>
        /// 業務部(竹苗)
        /// </summary>
        S003,
        /// <summary>
        /// 業務部(中彰)
        /// </summary>
        S004,
        /// <summary>
        /// 業務部(高屏)
        /// </summary>
        S005,
        /// <summary>
        /// 業務部(嘉南)
        /// </summary>
        S006,
        /// <summary>
        /// 業務部(玩美)
        /// </summary>
        S007,
        /// <summary>
        /// 業務部(旅遊)
        /// </summary>
        S008,
        /// <summary>
        /// 業務部(福利)
        /// </summary>
        S009,
        /// <summary>
        /// 業務部(全國宅配)
        /// </summary>
        S010,
        /// <summary>
        /// 業務部(KA)
        /// </summary>
        S011,
        /// <summary>
        /// 業務部(品生活)
        /// </summary>
        S012,
        /// <summary>
        /// 系統業務部
        /// </summary>
        S013,
        /// <summary>
        /// 通路發展部
        /// </summary>
        S014
    }

    public enum EmployeeLevel
    {
        None = 0,
        LV1 = 1,
        LV2,
        LV3,
        LV4,
        LV5,
        LV6,
        LV7
    }

    public enum OrganizationFlag
    {
        /// <summary>
        /// 一般
        /// </summary>
        General = 0,
        /// <summary>
        /// 停用
        /// </summary>
        Disabled = 1,
    }
}

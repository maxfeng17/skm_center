﻿using System;
using LunchKingSite.Core.Component;
using System.ComponentModel;

namespace LunchKingSite.Core
{
    [Flags]
    public enum SellerStatusFlag
    {
        [Localization("Confirmed")]
        Confirmed = 0x00000001,

        [ExcludeBinding]
        OnlineMask = 0x00000006,

        [Localization("IncludeDistribSlip")]
        IncludeDistribSlip = 0x00000008,

        [ExcludeBinding]
        TransportConfidenceMask = 0x00000030,

        [ExcludeBinding]
        RewardMask = 0x00000F00,

        [Localization("Popular")]
        Popular = 0x00004000,
    };

    public enum SellerStatusBitShift
    {
        OnlineType = 1,
        TransportConfidence = 4,
        Reward = 8,
    }

    public enum DepartmentTypes
    {
        [Localization("SDT_lunchking")]
        Meal = 0,
        [Localization("SDT_cosmetics")]
        Cosmetics = 1,
        [Localization("SDT_delicacies")]
        Delicacies = 2,
        [Localization("SDT_Ppon")]
        Ppon = 3,
        [Localization("SDT_Ppon_Item")]
        PponItem = 4,
        [Localization("SDT_Peauty")]
        Peauty = 5,
        [Localization("SDT_HiDeal")]
        HiDeal = 6,
    }

    public enum SellerInvoiceType
    {
        Invoice = 1,
        Receipt = 2,
        NoInvoiceReceipt = 4,
    };

    public enum SellerCommissionType
    {
        Rate = 1,
        Regular = 2,
    }

    public enum SellerTransportConfidence
    {
        [Localization("TransportConfidenceNone")]
        None = 0,
        [Localization("TransportConfidenceLow")]
        Low = 1,
        [Localization("TransportConfidenceHigh")]
        High = 2
    }

    public enum PostCheckoutActionType
    {
        NothingToDo = 0,
        RedirectToStaticPage = 1,
        PayEasyQsConnect = 2,
    }

    /// <summary>
    /// 訂單由系統代發或使用者自行訂購
    /// </summary>
    public enum SellerOrderType
    {
        System = 0,
        User = 1
    }

    public enum PaymentMode
    {
        [Localization("AtmTransfer")]
        AtmTransfer = 1,
        [Localization("CashOnDelivery")]
        CashOnDelivery = 2,
        [Localization("CsvPickUp")]
        CsvPickUp = 3,
        [Localization("CreditCard")]
        CreditCard = 4
    }

    public enum SellerOnlineType
    {
        [Localization("SellerOffline")]
        Offline = 0,
        [Localization("SellerOnline")]
        Online = 1,
        [Localization("SellerPrivate")]
        Private = 2
    }

    public enum RewardType
    {
        [Localization("None")]
        None = 0,
        [Localization("StoreWithRewardPoint")]
        Original = 1,
        [Localization("RewardTriple")]
        TriplePoints = 2,
        [Localization("RewardQuintuple")]
        QuintuplePoints = 3,
        [Localization("RewardDecuple")]
        DecuplePoints = 4,
        [Localization("RewardOneHundred")]
        OneHundredPoints = 8,
        [Localization("RewardTwoHundred")]
        TwoHundredPoints = 11,
        [Localization("RewardThreeHundred")]
        ThreeHundredPoints = 12,
        [Localization("RewardFiveHundred")]
        FiveHundredPoints = 9,
        [Localization("RewardOneThousand")]
        OneThousandPoints = 10,

    }

    /// <summary>
    /// 17Life好康分辨買給朋友買給自己還是買商品用
    /// </summary>
    public enum PayTypes
    {
        ForMe,
        ForFriend,
        ForItem,
    }

    /// <summary>
    /// 核銷類別
    /// </summary>
    public enum SellerVerifyType
    {
        [Localization("VerificationOffline")]
        VerificationOffline = 1,
        [Localization("VerificationOnline")]
        VerificationOnline = 2,
    }

    public enum DeliveryType
    {
        /// <summary>
        /// 憑證消費(到店)
        /// </summary>
        [Localization("DeliveryTypeToShop")]
        [SystemCode("DeliveryType", 1)]
        ToShop = 1,
        /// <summary>
        /// 宅配(到府)
        /// </summary>
        [Localization("DeliveryTypeToHouse")]
        [SystemCode("DeliveryType", 2)]
        ToHouse = 2,

    }

    /// <summary>
    /// 店鋪顯示狀態
    /// </summary>
    public enum StoreStatus
    {
        /// <summary>
        /// 顯示
        /// </summary>
        Available,
        /// <summary>
        /// 隱藏
        /// </summary>
        Cancel,
    }

    /// <summary>
    /// Category種類區分為店家和P好康首頁分類(固定和行銷自建)
    /// </summary>
    public enum CategoryType
    {
        /// <summary>
        /// 未設定
        /// </summary>
        None = -1,
        /// <summary>
        /// 店家
        /// </summary>
        Seller = 0,
        /// <summary>
        /// 好康首頁分類
        /// </summary>
        PponFix = 1,
        /// <summary>
        /// 旅遊區域
        /// </summary>
        Travel = 2,
        /// <summary>
        /// 商圈
        /// </summary>
        CommercialCircle = 3,
        /// <summary>
        /// 團購頻道
        /// </summary>
        PponChannel = 4,
        /// <summary>
        /// 頻道區域分區
        /// </summary>
        PponChannelArea = 5,
        /// <summary>
        /// 檔次分類(新版分類2)
        /// </summary>
        DealCategory = 6,
        /// <summary>
        /// 取貨類型
        /// </summary>
        DeliveryType = 7,
        /// <summary>
        /// 商品特殊類型，會於頁面顯示ICON的
        /// </summary>
        DealSpecialCategory = 8,
        /// <summary>
        /// 優惠券
        /// </summary>
        Voucher = 9,
        /// <summary>
        /// 優惠券區域
        /// </summary>
        VoucherArea = 10,
        /// <summary>
        /// 優惠券類別
        /// </summary>
        VoucherCategory = 11,
        /// <summary>
        /// 區域(行政區)分類
        /// </summary>
        StandardRegion = 12,
        /// <summary>
        /// 區域(商圈、景點)分類
        /// </summary>
        SpecialRegion = 13,
        /// <summary>
        /// 熟客卡分類
        /// </summary>
        MembershipCard = 14,
        /// <summary>
        /// MRT 捷運附近店家 分類
        /// </summary>
        MRTLocation = 15,
    }

    public enum CategoryPponType
    {
        /// <summary>
        /// 熱門推薦
        /// </summary>
        TopRecommend = 1,
        /// <summary>
        /// 新鮮貨
        /// </summary>
        TopNews = 2,
        /// <summary>
        /// 暢銷排行榜
        /// </summary>
        TopBillboard = 3,
        /// <summary>
        /// 24H到貨
        /// </summary>
        Arrived_24H = 4,
        /// <summary>
        /// 72H到貨
        /// </summary>
        Arrived_72H = 5,
    }

    public enum CategoryStatus
    {
        Disabled = 0,
        Enabled = 1,
    }

    /// <summary>
    /// P好康首頁排序功能
    /// </summary>
    public enum CategorySortType
    {
        /// <summary>
        /// 預設
        /// </summary>
        [Localization("CategorySortTypeDefault")]
        Default,
        /// <summary>
        /// 最新
        /// </summary>
        [Localization("CategorySortTypeTopNews")]
        TopNews,
        /// <summary>
        /// 銷量
        /// </summary>
        [Localization("CategorySortTypeTopOrderTotal")]
        TopOrderTotal,
        /// <summary>
        /// 折數(低到高)
        /// </summary>
        [Localization("CategorySortTypeDiscountAsc")]
        DiscountAsc,
        /// <summary>
        /// 價格(高到低)
        /// </summary>
        [Localization("CategorySortTypePriceDesc")]
        PriceDesc,
        /// <summary>
        /// 價格(低到高)
        /// </summary>
        [Localization("CategorySortTypePriceAsc")]
        PriceAsc,
        /// <summary>
        /// 收藏數量(高到低)
        /// </summary>
        MemberCollectDesc,
        /// <summary>
        /// 收藏數量(低到高)
        /// </summary>
        MemberCollectAsc
    }

    public enum PponDealFeedCategoryType
    {
        /// <summary>
        /// 所有類別
        /// </summary>
        All = 0,
        /// <summary>
        /// 旅遊
        /// </summary>
        Travel = 1,
        /// <summary>
        /// Google類別
        /// </summary>
        Google = 2,
        /// <summary>
        /// Facebook類別
        /// </summary>
        FB = 3
    }

    public enum PponDealFeedType
    {
        /// <summary>
        /// Google Product Feed
        /// </summary>
        Google = 0,
        /// <summary>
        /// Facebook Product Feed
        /// </summary>
        FB = 1,
        /// <summary>
        /// Yahoo product Feed
        /// </summary>
        Yahoo = 2
    }

    public enum PponDealType
    {
        /// <summary>
        /// All Product
        /// </summary>
        All = 0,
        /// <summary>
        /// ToHouse Product Only
        /// </summary>
        ToHouse = 1
    }

    /// <summary>
    /// 分類標籤 頻道上方小標籤
    /// </summary>
    public enum CategoryIconType
    {
        None,
        Hot,
        New,
        Top,
        /// <summary>
        /// 全家咖啡
        /// </summary>
        FamiCoffee,
        /// <summary>
        /// 超商取貨
        /// </summary>
        ISP
    }

    /// <summary>
    /// 區域分類顯示時的特殊處理 (分類樹用)
    /// </summary>
    public enum CategoryDisplayRuleFlag
    {
        None = 0,

        /// <summary>
        /// 是否要用新的名稱 (沒設的話顯示category.name)
        /// </summary>
        ShowDisplayName = 1,

        /// <summary>
        /// 是否啟用tab頁 (啟用tab頁就不顯示該category.name，如要顯示請設DisplaySummaryItem)
        /// </summary>
        EnableTab = 2,

        /// <summary>
        /// 如果tab頁已開啟，再點一次可以導頁
        /// </summary>
        TabAsLink = 4,

        /// <summary>
        /// 子項目清單是否要自動產生一個父項目的連結
        /// </summary>
        /// <example>
        /// A -「A1、A2...」  to  A -「A、A1、A2、...」
        /// </example>
        DisplaySummaryItem = 8,

        /// <summary>
        /// DisplaySummaryItem有設的時候，該項目是否要重新命名 (有設的話顯示summary_item_name)
        /// </summary>
        /// <example>
        /// A -「A、A1、A2、...」 →  A -「whatever、A1、A2、...」
        /// </example>
        ReNameSummaryItem = 16
    }

    public enum SellerTabPage
    {

        /// <summary>
        /// 基本資料
        /// </summary>
        Info = 0,

        /// <summary>
        /// 時段列表
        /// </summary>
        TimeList = 1,

        /// <summary>
        /// 菜單列表
        /// </summary>
        MenuList = 2,

        /// <summary>
        /// 配料列表
        /// </summary>
        FoodIngredientsList = 3,

        /// <summary>
        /// 17Life好康設定
        /// </summary>
        PponSetup = 4,

        /// <summary>
        /// 商家圖檔設定
        /// </summary>
        SellerPicSetup = 5,

        /// <summary>
        /// 商家訂單金額調整
        /// </summary>
        SellerOrderAdjustSum = 6,

        /// <summary>
        /// 商家月繳趴數調整
        /// </summary>
        SellerAdjustPay = 7,

        /// <summary>
        /// 其他
        /// </summary>
        Other = 8,

        /// <summary>
        /// 店家首頁優惠列表
        /// </summary>
        StorePromoList = 9,

        /// <summary>
        /// 商家店鋪列表
        /// </summary>
        StoreList = 10,

        /// <summary>
        /// 商家資訊
        /// </summary>
        StoreInfo = 11,

        /// <summary>
        /// 品生活設定
        /// </summary>
        PiinLifeSetUp = 12,
    }

    /// <summary>
    /// 廠商上傳序號檔次分類
    /// </summary>
    public enum SellerSerialKeyDealType
    {
        /// <summary>
        /// 序號搭配檔次BID
        /// </summary>
        [Localization("SerialKeyDealTypeOnlyBid")]
        OnlyBid,
        /// <summary>
        /// 序號搭配檔次BID和MemberLink的UserId
        /// </summary>
        [Localization("SerialKeyDealTypeBidWithUserId")]
        BidWithUserId

    }

    /// <summary>
    /// 賣家資料狀態(由前台業務自建)
    /// </summary>
    public enum SellerTempStatus
    {
        //完成審核
        [Localization("SellerTempStatusCompleted")]
        Completed = 0,
        //新增申請
        [Localization("SellerTempStatusApplied")]
        Applied = 1,
        //退回重審
        [Localization("SellerTempStatusReturned")]
        Returned = 2
    }

    public enum ProposalDealType
    {
        None,
        /// <summary>
        /// 在地好康
        /// </summary>
        [Localization("ProposalDealTypeLocalDeal")]
        LocalDeal,
        /// <summary>
        /// 玩美
        /// </summary>
        [Localization("ProposalDealTypePBeauty")]
        PBeauty,
        /// <summary>
        /// 品生活
        /// </summary>
        [Localization("ProposalDealTypePiinlife")]
        Piinlife,
        /// <summary>
        /// 旅遊
        /// </summary>
        [Localization("ProposalDealTypeTravel")]
        Travel,
        /// <summary>
        /// 宅配
        /// </summary>
        [Localization("ProposalDealTypeProduct")]
        Product,
    }

    public enum ProposalDealSubType
    {
        None,
        /// <summary>
        /// 套餐
        /// </summary>
        [Localization("ProposalDealSubTypeDealSet")]
        DealSet,
        /// <summary>
        /// 折抵金額
        /// </summary>
        [Localization("ProposalDealSubTypeOffsetting")]
        Offsetting,
        /// <summary>
        /// 到店取貨
        /// </summary>
        [Localization("ProposalDealSubTypePropoduct")]
        Propoduct,
        /// <summary>
        /// 吃到飽
        /// </summary>
        [Localization("ProposalDealSubTypeEatToDie")]
        EatToDie,
        /// <summary>
        /// SPA
        /// </summary>
        [Localization("ProposalDealSubTypeSPA")]
        SPA,
        /// <summary>
        /// 美髮
        /// </summary>
        [Localization("ProposalDealSubTypeHairSalon")]
        HairSalon,
        /// <summary>
        /// 美甲/美睫
        /// </summary>
        [Localization("ProposalDealSubTypeNailSalon")]
        NailSalon,
        /// <summary>
        /// 按摩
        /// </summary>
        [Localization("ProposalDealSubTypeMassage")]
        Massage,
        /// <summary>
        /// 課程
        /// </summary>
        [Localization("ProposalDealSubTypeCourse")]
        Course,
        /// <summary>
        /// 飯店旅館
        /// </summary>
        [Localization("ProposalDealSubTypeHotel")]
        Hotel,
        /// <summary>
        /// 民宿
        /// </summary>
        [Localization("ProposalDealSubTypeBedAndBreakfast")]
        BedAndBreakfast,
        /// <summary>
        /// 休憩
        /// </summary>
        [Localization("ProposalDealSubTypeHotelRest")]
        HotelRest,
        /// <summary>
        /// Motel
        /// </summary>
        [Localization("ProposalDealSubTypeMotel")]
        Motel,
        /// <summary>
        /// 旅遊行程
        /// </summary>
        [Localization("ProposalDealSubTypeTravel")]
        Travel,
        /// <summary>
        /// 票券
        /// </summary>
        [Localization("ProposalDealSubTypeTickets")]
        Tickets,
        /// <summary>
        /// 泡湯
        /// </summary>
        [Localization("ProposalDealSubTypeHotSpring")]
        HotSpring,
        /// <summary>
        /// 食品
        /// </summary>
        [Localization("ProposalDealSubTypeFood")]
        Food,
        /// <summary>
        /// 居家生活
        /// </summary>
        [Localization("ProposalDealSubTypeLiving")]
        Living,
        /// <summary>
        /// 寢家具
        /// </summary>
        [Localization("ProposalDealSubTypeFurniture")]
        Furniture,
        /// <summary>
        /// 流行
        /// </summary>
        [Localization("ProposalDealSubTypePopular")]
        Popular,
        /// <summary>
        /// 3C
        /// </summary>
        [Localization("ProposalDealSubTypeConsumerElectronic")]
        ConsumerElectronic,
        /// <summary>
        /// 家電
        /// </summary>
        [Localization("ProposalDealSubTypeHomeAppliances")]
        HomeAppliances,
        /// <summary>
        /// 美妝
        /// </summary>
        [Localization("ProposalDealSubTypeBeauty")]
        Beauty,
        /// <summary>
        /// 婦幼
        /// </summary>
        [Localization("ProposalDealSubTypeChild")]
        Child,
        /// <summary>
        /// 其他
        /// </summary>
        [Localization("ProposalDealSubTypeOthers")]
        Others,
    }

    /// <summary>
    /// 出貨方式
    /// </summary>
    //public enum ProposalShipType
    //{
    //    /// <summary>
    //    /// 一般出貨
    //    /// </summary>
    //    [Localization("ProposalShipTypeNormal")]
    //    Normal = -1,
    //    /// <summary>
    //    /// 快速出貨
    //    /// </summary>
    //    [Localization("ProposalShipTypeFaster")]
    //    Faster = 0,
    //}

    /// <summary>
    /// 店家來源
    /// </summary>
    public enum ProposalSellerFrom
    {
        /// <summary>
        /// 自行開發
        /// </summary>
        [Description("自行開發")]
        SelfDevelop = 1,
        /// <summary>
        /// ListTeam
        /// </summary>
        [Description("ListTeam")]
        ListTeam = 2,
        /// <summary>
        /// 舊店家
        /// </summary>
        [Description("舊店家")]
        OldSeller = 3
    }

    /// <summary>
    /// 店家屬性
    /// </summary>
    public enum ProposalSellerPorperty
    {
        /// <summary>
        /// 好康
        /// </summary>
        [Description("團購好康")]
        Ppon = 1,
        /// <summary>
        /// 宅配
        /// </summary>
        [Description("團購宅配")]
        HomeDelivery = 2,
        /// <summary>
        /// 熟客卡
        /// </summary>
        [Description("熟客卡")]
        RegularsCard = 3,
        /// <summary>
        /// 雄獅
        /// </summary>
        [Description("雄獅")]
        LionTravel = 4,
        /// <summary>
        /// 新光
        /// </summary>
        [Description("新光")]
        Skm = 5,
        /// <summary>
        /// 台新
        /// </summary>
        [Description("台新")]
        Taishin = 6,
        /// <summary>
        /// 成套票券
        /// </summary>
        [Description("成套票券")]
        GroupCoupon = 7
    }

    public enum ProposalContact
    {
        /// <summary>
        /// 一般(頁確)聯絡人
        /// </summary>
        [Description("一般(頁確)聯絡人")]
        Normal = 1,
        /// <summary>
        /// 退換貨聯絡人
        /// </summary>
        [Description("退換貨聯絡人")]
        ReturnPerson = 2,
        /// <summary>
        /// 其他
        /// </summary>
        [Description("其他")]
        Other = 99
    }

    public enum ProposalSourceType
    {
        /// <summary>
        /// 舊提案單
        /// </summary>
        Original = 0,
        /// <summary>
        /// 新版宅配提案單
        /// </summary>
        House = 1,
        /// <summary>
        /// 新版憑證提案單
        /// </summary>
        Shop = 2
    }

    public enum DevelopStatus
    {
        [Description("未聯絡")]
        NonContact = 1,
        [Description("待追蹤")]
        ToBeTraced = 2,
        [Description("已發MAIL再追蹤")]
        Mailed = 3,
        [Description("已約訪")]
        Visited = 4,
        [Description("已面訪")]
        Interviewing = 5,
        [Description("未找到KEYMAN")]
        NonKeyMan = 6,
        [Description("店家拒絕")]
        SellerReject = 7,
        [Description("店名重覆")]
        SellerNameRepeat = 8,
        [Description("已歇業")]
        OutOfBusiness = 9,
        [Description("已簽約")]
        Signed = 10,
        [Description("已電話聯絡")]
        Phone = 11,
        [Description("店家考慮中")]
        Consideration = 12,
        [Description("不優")]
        NotGifted = 13,
        [Description("其他")]
        Other = 14,
        [Description("未簽約")]
        NotSigned = 15,
        [Description("黑名單店家")]
        BlackList = 99
    }

    public enum SellerContractType
    {
        [Description("團購主約(含熟客卡)")]
        Main = 1,
        [Description("團購主約異動")]
        Activity = 2,
        [Description("優惠券")]
        Coupon = 3,
        [Description("平板")]
        Flat = 4,
        [Description("熟客卡主約")]
        PCPMain = 5,
        [Description("熟客卡異動")]
        PCPActivity = 6,
        [Description("團購附約異動")]
        PairActivity = 7,
        [Description("團購附約")]
        Pair = 8,
        [Description("團購主約(不含熟客卡)")]
        MainNonPCP = 9,
        [Description("熟客卡結案照片")]
        PCPClosedPic = 10,
        [Description("合約草案備存")]
        Draft = 11,
        [Description("策展專案商品合約書")]
        Curation = 12,
        [Description("宅配商品主約")]
        Delivery = 13,
        [Description("宅配入倉主約")]
        Warehousing = 14,
        [Description("在地團購主約")]
        CityGroupBuy = 15,
        [Description("旅遊好康主約")]
        PponTravel = 16,
        [Description("旅遊好康墨攻主約")]
        MohistTravel = 17,
        [Description("旅遊實體票券主約")]
        TravelTicket = 18,
        [Description("康朋團購主約")]
        ConPengGroupBuy = 19,
        [Description("展演票券主約")]
        PromotionTicket = 20,
        [Description("公益團購商品主約")]
        LoveGroupBuy = 21,
        [Description("公益捐款勸募主約")]
        LoveDonate = 22,
        [Description("在地廣告上架主約")]
        CityAD = 23,
        [Description("主約異動")]
        MainActivity = 24,
        [Description("其他：包含合約草稿或熟客卡照片等")]
        Other = 25,
    }

    public enum SellerGrade
    {
        [Description("開發名單")]
        Develop = 1,
        [Description("潛在客戶")]
        Potential = 2,
        [Description("準客戶")]
        Standard = 3,
        [Description("已簽約")]
        Signed = 4
    }

    /// <summary>
    /// 活動使用有效時間
    /// </summary>
    public enum ProposalStartUseUnitType
    {
        /// <summary>
        /// 日
        /// </summary>
        [Localization("StartUseUnitTypeDay")]
        Day = 0,
        /// <summary>
        /// 月
        /// </summary>
        [Localization("StartUseUnitTypeMonth")]
        Month = 1,
        /// <summary>
        /// 前
        /// </summary>
        [Localization("StartUseUnitTypeBeforeDay")]
        BeforeDay = 2,
    }

    /// <summary>
    /// 提案單特殊標記
    /// </summary>
    public enum ProposalSpecialFlag
    {
        [Localization("ProposalSpecialFlagNone")]
        None = 0,
        /// <summary>
        /// 免稅
        /// </summary>
        [Localization("ProposalSpecialFlagFreeTax")]
        FreeTax = 1,
        /// <summary>
        /// 平行輸入
        /// </summary>
        [Localization("ProposalSpecialFlagParallelImportation")]
        ParallelImportation = 2,
        /// <summary>
        /// 需拍攝
        /// </summary>
        [Localization("ProposalSpecialFlagPhotographers")]
        Photographers = 4,
        /// <summary>
        /// 年約
        /// </summary>
        [Localization("ProposalSpecialFlagYearContract")]
        YearContract = 8,
        /// <summary>
        /// 獨家
        /// </summary>
        [Localization("ProposalSpecialFlagExclusive")]
        Exclusive = 16,
        /// <summary>
        /// 美妝
        /// </summary>
        [Localization("ProposalSpecialFlagBeauty")]
        Beauty = 32,
        /// <summary>
        /// 即期品
        /// </summary>
        [Localization("ProposalSpecialFlagExpiringProduct")]
        ExpiringProduct = 64,
        /// <summary>
        /// 檢驗規定
        /// </summary>
        [Localization("ProposalSpecialFlagInspectRule")]
        InspectRule = 128,
        /// <summary>
        /// 發票開立人
        /// </summary>
        [Localization("ProposalSpecialFlagInvoiceFounder")]
        InvoiceFounder = 256,
        /// <summary>
        /// 首次合作
        /// </summary>
        [Localization("ProposalSpecialFlagFirstDeal")]
        FirstDeal = 512,
        /// <summary>
        /// 旅遊
        /// </summary>
        [Localization("ProposalSpecialFlagTravel")]
        Travel = 1024,
        /// <summary>
        /// 即買即用
        /// </summary>
        [Localization("ProposalSpecialFlagOnlineUse")]
        OnlineUse = 2048,
        /// <summary>
        /// 新商品
        /// </summary>
        [Localization("ProposalSpecialFlagNewItem")]
        NewItem = 4096,
        /// <summary>
        /// 快速上檔
        /// </summary>
        [Localization("ProposalSpecialFlagUrgent")]
        Urgent = 8192,
        /// <summary>
        /// 店家提供照片
        /// </summary>
        [Localization("ProposalSpecialFlagSellerPhoto")]
        SellerPhoto = 16384,
        /// <summary>
        /// FTP舊照片
        /// </summary>
        [Localization("ProposalSpecialFlagFTPPhoto")]
        FTPPhoto = 32768,
        /// <summary>
        /// FTP自選檔
        /// </summary>
        [Localization("ProposalSelfOptional")]
        SelfOptional = 65536,
        /// <summary>
        /// 成套票券
        /// </summary>
        [Localization("ProposalGroupCoupon")]
        GroupCoupon = 131072,
        /// <summary>
        /// 每日一物
        /// </summary>
        [Localization("EveryDayNewDeal")]
        EveryDayNewDeal = 262144,
        /// <summary>
        /// 本檔主打星
        /// </summary>
        [Localization("ProposalDealStar")]
        DealStar = 524288,
        /// <summary>
        /// 店家/品牌故事
        /// </summary>
        [Localization("ProposalStoreStory")]
        StoreStory = 1048576,
        /// <summary>
        /// 媒體/報導
        /// </summary>
        [Localization("ProposalMediaReport")]
        MediaReport = 2097152,
        /// <summary>
        /// 推薦菜色/食材
        /// </summary>
        [Localization("ProposalRecommendFood")]
        RecommendFood = 4194304,
        /// <summary>
        /// 環境介紹
        /// </summary>
        [Localization("ProposalEnvironmentIntroduction")]
        EnvironmentIntroduction = 8388608,
        /// <summary>
        /// 周邊景點
        /// </summary>
        [Localization("ProposalAroundScenery")]
        AroundScenery = 16777216,
        /// <summary>
        /// 季節/配合活動
        /// </summary>
        [Localization("ProposalSeasonActivity")]
        SeasonActivity = 33554432,
        /// <summary>
        /// 低毛利率可用折價券
        /// </summary>
        [Localization("LowGrossMarginAllowedDiscount")]
        LowGrossMarginAllowedDiscount = 67108864,
        /// <summary>
        /// 不可使用折價券
        /// </summary>
        [Localization("NotAllowedDiscount")]
        NotAllowedDiscount = 134217728,
        /// <summary>
        /// 通用券商品
        /// </summary>
        [Localization("NoRestrictedStore")]
        NoRestrictedStore = 268435456,
        /// <summary>
        /// 鑑賞期
        /// </summary>
        [Localization("TrialPeriod")]
        TrialPeriod = 536870912,
        /// <summary>
        /// 墨攻
        /// </summary>
        [Localization("Mohist")]
        Mohist = 1073741824,
    }

    public enum ProposalConsumerTime
    {
        None = 0,
        /// <summary>
        /// 平日
        /// </summary>
        [Localization("Weekdays")]
        Weekdays = 1,
        /// <summary>
        /// 假日
        /// </summary>
        [Localization("Holiday")]
        Holiday = 2,
        /// <summary>
        /// 特殊節日
        /// </summary>
        [Localization("SpecialHoliday")]
        SpecialHoliday = 4,
        /// <summary>
        /// 早餐
        /// </summary>
        [Localization("Breakfast")]
        Breakfast = 8,
        /// <summary>
        /// 午餐
        /// </summary>
        [Localization("Lunch")]
        Lunch = 16,
        /// <summary>
        /// 晚餐
        /// </summary>
        [Localization("Dinner")]
        Dinner = 32,
        /// <summary>
        /// 下午茶
        /// </summary>
        [Localization("AfternoonTea")]
        AfternoonTea = 64,
        /// <summary>
        /// 宵夜
        /// </summary>
        [Localization("NightSnack")]
        NightSnack = 128,
        /// <summary>
        /// 假日加價
        /// </summary>
        [Localization("HolidayFare")]
        HolidayFare = 256,
        /// <summary>
        /// 低消
        /// </summary>
        [Localization("MinimumCharge")]
        MinimumCharge = 512,
        /// <summary>
        /// 服務費
        /// </summary>
        [Localization("ServiceCharge")]
        ServiceCharge = 1024
    }

    /// <summary>
    /// 提案單複製類型
    /// </summary>
    public enum ProposalCopyType
    {
        /// <summary>
        /// 全新檔
        /// </summary>
        [Localization("ProposalCopyTypeNone")]
        None = 0,
        /// <summary>
        /// 複製提案單(優惠內容稍有不同)
        /// </summary>
        [Localization("ProposalCopyTypeSimilar")]
        Similar = 1,
        /// <summary>
        /// 完全複製檔(優惠內容完全相同)
        /// </summary>
        [Localization("ProposalCopyTypeSame")]
        Same = 2,
        /// <summary>
        /// 商家複製提案單
        /// </summary>
        [Localization("ProposalCopyTypeSeller")]
        Seller = 4,
    }

    public enum ProposalReturnPpon
    {
        None = 0,
        /// <summary>
        /// 抽成不符合
        /// </summary>
        [Localization("CommissionInCompatible")]
        CommissionInCompatible = 1,
        /// <summary>
        /// 折數不符合
        /// </summary>
        [Localization("FoldInCompatible")]
        FoldInCompatible = 2,
        /// <summary>
        /// 份數不符合
        /// </summary>
        [Localization("CopiesInCompatible")]
        CopiesInCompatible = 4,
        /// <summary>
        /// 過往營業額過低
        /// </summary>
        [Localization("BusinessPriceLower")]
        BusinessPriceLower = 8,
        /// <summary>
        /// 需優化方案內容
        /// </summary>
        [Localization("NeedOptimization")]
        NeedOptimization = 16,
        /// <summary>
        /// 比競業內容較差
        /// </summary>
        [Localization("PoorThanBusinessStrife")]
        PoorThanBusinessStrife = 32,
        /// <summary>
        /// 資料錯誤修正
        /// </summary>
        [Localization("DataModify")]
        DataModify = 64,
        /// <summary>
        /// 合約內容有誤
        /// </summary>
        [Localization("ContractError")]
        ContractError = 128,
        /// <summary>
        /// 拉檔
        /// </summary>
        [Localization("CancelOrderTime")]
        CancelOrderTime = 256,
        /// <summary>
        /// 資料錯誤修正/附約未回檢附不齊全(排檔前)
        /// </summary>
        [Localization("DataModifyContractInComplete")]
        DataModifyContractInComplete = 512,
        /// <summary>
        /// 權益修改及權益未編輯(排檔前)
        /// </summary>
        [Localization("CouponEventContentModify")]
        CouponEventContentModify = 1024,
        /// <summary>
        /// 檢核錯誤
        /// </summary>
        [Localization("CheckError")]
        CheckError = 2048,
        /// <summary>
        /// 其他
        /// </summary>
        [Localization("Other")]
        Other = 99
    }

    public enum ProposalReturnHouse
    {
        None = 0,
        /// <summary>
        /// 須優化價格或方案
        /// </summary>
        [Localization("OptimizationPrice")]
        OptimizationPrice = 1,
        /// <summary>
        /// 商品資訊錯誤、不齊全
        /// </summary>
        [Localization("InformationError")]
        InformationError = 2,
        /// <summary>
        /// 拆分比過低
        /// </summary>
        [Localization("PercentLower")]
        PercentLower = 4,
        /// <summary>
        /// 已有同商品提案
        /// </summary>
        [Localization("ExistProposal")]
        ExistProposal = 8,
        /// <summary>
        /// 業務提案跨線
        /// </summary>
        [Localization("SaleOverProposal")]
        SaleOverProposal = 16,
        /// <summary>
        /// 資料錯誤修正
        /// </summary>
        [Localization("DataModify")]
        DataModify = 32,
        /// <summary>
        /// 合約內容有誤
        /// </summary>
        [Localization("ContractError")]
        ContractError = 64,
        /// <summary>
        /// 拉檔
        /// </summary>
        [Localization("CancelOrderTime")]
        CancelOrderTime = 128,
        /// <summary>
        /// 檢核錯誤
        /// </summary>
        [Localization("CheckError")]
        CheckError = 256,
        /// <summary>
        /// 其他
        /// </summary>
        [Localization("Other")]
        Other = 99
    }

    /// <summary>
    /// 提案單申請檢核
    /// </summary>
    public enum ProposalApplyFlag
    {
        Initial = 0,
        /// <summary>
        /// 提案申請
        /// </summary>
        [Localization("ProposalApplyFlagProposalApply")]
        Apply = 1,
    }

    /// <summary>
    /// 案單申請核准
    /// </summary>
    public enum ProposalApproveFlag
    {
        Initial = 0,
        /// <summary>
        /// QC確認
        /// </summary>
        [Localization("ProposalApproveFlagQCcheck")]
        QCcheck = 1,
    }

    /// <summary>
    /// 提案單建檔檢核
    /// </summary>
    public enum ProposalBusinessCreateFlag
    {
        Initial = 0,
        /// <summary>
        /// 建檔確認
        /// </summary>
        [Localization("ProposalBusinessCreateFlagCreated")]
        Created = 1,
        /// <summary>
        /// 送出排檔
        /// </summary>
        [Localization("ProposalBusinessCreateFlagSettingCheck")]
        SettingCheck = 2,
    }
    /// <summary>
    /// 提案單檔次檢核
    /// </summary>
    public enum ProposalBusinessFlag
    {
        Initial = 0,
        /// <summary>
        /// 提案稽核
        /// </summary>
        [Localization("ProposalBusinessFlagProposalAudit")]
        ProposalAudit = 1,
        /// <summary>
        /// 排檔編輯
        /// </summary>
        [Localization("ProposalBusinessFlagScheduleCheck")]
        ScheduleCheck = 2,
        /// <summary>
        /// 攝影確認
        /// </summary>
        [Localization("ProposalBusinessFlagPhotographerCheck")]
        PhotographerCheck = 4,
    }

    public enum ProposalPhotographer
    {
        Initial = 0,
        /// <summary>
        /// 約拍中
        /// </summary>
        [Localization("ProposalBusinessFlagPhotographerAppoint")]
        PhotographerAppoint = 1,
        /// <summary>
        /// 已約拍
        /// </summary>
        [Localization("ProposalBusinessFlagPhotographerAppointCheck")]
        PhotographerAppointCheck = 2,
        /// <summary>
        /// 取消約拍
        /// </summary>
        [Localization("ProposalBusinessFlagPhotographerAppointCancel")]
        PhotographerAppointCancel = 4,
        /// <summary>
        /// 拍攝結案
        /// </summary>
        [Localization("ProposalBusinessFlagPhotographerAppointClose")]
        PhotographerAppointClose = 8
    }

    /// <summary>
    /// 提案單編輯檢核
    /// </summary>
    public enum ProposalEditorFlag
    {
        Initial = 0,
        /// <summary>
        /// 檔次設定
        /// </summary>
        [Localization("ProposalEditorFlagSetting")]
        Setting = 1,
        /// <summary>
        /// 文案編輯
        /// </summary>
        [Localization("ProposalEditorFlagCopyWriter")]
        CopyWriter = 2,
        /// <summary>
        /// 圖片設計
        /// </summary>
        [Localization("ProposalEditorFlagImageDesign")]
        ImageDesign = 4,
        /// <summary>
        /// 大圖ART
        /// </summary>
        [Localization("ProposalEditorFlagART")]
        ART = 8,
        /// <summary>
        /// 上架確認
        /// </summary>
        [Localization("ProposalEditorFlagListing")]
        Listing = 16,
    }

    /// <summary>
    /// 提案單上架檢核
    /// </summary>
    public enum ProposalListingFlag
    {
        Initial = 0,
        /// <summary>
        /// 頁面確認
        /// </summary>
        [Localization("ProposalListingFlagPageCheck")]
        PageCheck = 1,
        /// <summary>
        /// 附約已回
        /// </summary>
        [Localization("ProposalListingFlagPaperContractCheck")]
        PaperContractCheck = 2,
        /// <summary>
        /// 財務資料
        /// </summary>
        [Localization("ProposalListingFlagFinanceCheck")]
        FinanceCheck = 4,
        /// <summary>
        /// 合約寄出
        /// </summary>
        [Localization("ProposalListingFlagPaperContractSend")]
        PaperContractSend = 8,
        /// <summary>
        /// 圖檔確認
        /// </summary>
        [Localization("ProposalListingFlagPicCheck")]
        PicCheck = 16,
        /// <summary>
        /// 同意合約未回先上檔
        /// </summary>
        [Localization("ProposalListingFlagPaperContractNotCheck")]
        PaperContractNotCheck = 32,
        /// <summary>
        /// 合約檢核
        /// </summary>
        [Localization("ProposalListingFlagPaperContractInspect")]
        PaperContractInspect = 64,
    }

    public enum SellerProposalFlag
    {
        /// <summary>
        /// 初始
        /// </summary>
        [Localization("SellerProposalFlagInitial")]
        Initial = 0,
        /// <summary>
        /// 草稿
        /// </summary>
        [Localization("SellerProposalFlagApply")]
        Apply = 1,
        /// <summary>
        /// 商家儲存資料
        /// </summary>
        [Localization("SellerProposalFlagSaved")]
        Saved = 2,
        /// <summary>
        /// 提案送交
        /// </summary>
        [Localization("SellerProposalFlagSend")]
        Send = 4,
        /// <summary>
        /// 業務編輯中
        /// </summary>
        [Localization("SellerProposalFlagSaleEditor")]
        SaleEditor = 8,
        /// <summary>
        /// 提案退回給商家
        /// </summary>
        [Localization("SellerProposalFlagReturned")]
        Returned = 16,
        /// <summary>
        /// 等待商家覆核
        /// </summary>
        [Localization("SellerProposalFlagCheckWaitting")]
        CheckWaitting = 32,
        /// <summary>
        /// 商家編輯中
        /// </summary>
        [Localization("SellerProposalFlagSellerEditor")]
        SellerEditor = 64,
        /// <summary>
        /// 提案覆核
        /// </summary>
        [Localization("SellerProposalFlagCheck")]
        Check = 128,
        /// <summary>
        /// 提案處理中，(業務在提案單送件)
        /// </summary>
        [Localization("SellerProposalFlagSaleSend")]
        SaleSend = 256,
        /// <summary>
        /// 提案完成，(業務在提案單建檔)
        /// </summary>
        [Localization("SellerProposalFlagSaleCreated")]
        SaleCreated = 512,
        /// <summary>
        /// 退回給業務
        /// </summary>
        [Localization("SellerProposalFlagReturnToSales")]
        ReturnToSales = 1024
    }

    /// <summary>
    /// 提案單建立人員類型
    /// </summary>
    public enum ProposalCreatedType
    {
        /// <summary>
        /// 業務
        /// </summary>
        [Localization("ProposalCreatedTypeSales")]
        Sales = 1,
        /// <summary>
        /// 商家
        /// </summary>
        [Localization("ProposalCreatedTypeSeller")]
        Seller = 2
    }

    /// <summary>
    /// 提案單狀態
    /// </summary>
    public enum ProposalStatus
    {
        /// <summary>
        /// 商家提案單
        /// </summary>
        [Localization("ProposalStatusSellerCreate")]
        SellerCreate = -1,
        /// <summary>
        /// 未申請
        /// </summary>
        [Localization("ProposalStatusInitial")]
        Initial = 0,
        /// <summary>
        /// 提案申請
        /// </summary>
        [Localization("ProposalStatusApply")]
        Apply = 1,
        /// <summary>
        /// 申請核准
        /// </summary>
        [Localization("ProposalStatusApprove")]
        Approve = 2,
        /// <summary>
        /// 建檔檢核
        /// </summary>
        [Localization("ProposalStatusCreated")]
        Created = 3,
        /// <summary>
        /// 檔次檢核
        /// </summary>
        [Localization("ProposalStatusBusinessCheck")]
        BusinessCheck = 4,
        /// <summary>
        /// 編輯檢核
        /// </summary>
        [Localization("ProposalStatusEditor")]
        Editor = 5,
        /// <summary>
        /// 上架檢核
        /// </summary>
        [Localization("ProposalStatusListing")]
        Listing = 6,
    }

    public enum ProposalMultiDealStatus
    {
        /// <summary>
        /// 初始
        /// </summary>
        Init = 0,
        /// <summary>
        /// 商家刪除待確認
        /// </summary>
        DeleteEdit = 1,
        /// <summary>
        /// 業務確認刪除
        /// </summary>
        Delete = 2,
    }

    public enum ProposalLogType
    {
        Initial = 0,
        /// <summary>
        /// 指派
        /// </summary>
        Assign = 1,
        /// <summary>
        /// 商家提案單
        /// </summary>
        Seller = 2,
        /// <summary>
        /// 非業務
        /// </summary>
        DenySales = 3,
        /// <summary>
        /// 留言區
        /// </summary>
        Message = 4,
        /// <summary>
        /// HTML紀錄不顯示
        /// </summary>
        HTML = 5,
    }

    public enum ProposalFileStatus
    {
        /// <summary>
        /// 刪除
        /// </summary>
        Delete = -1,
        /// <summary>
        /// 正常
        /// </summary>
        Nomal  = 0,
        
    }

    public enum ProvisionDepartmentType
    {
        /// <summary>
        /// p好康
        /// </summary>
        Ppon = 0,
        /// <summary>
        /// p商品
        /// </summary>
        Product = 1,
        /// <summary>
        /// p旅遊
        /// </summary>
        Travel = 2
    }

    /// <summary>
    /// 優惠券活動分類
    /// </summary>
    public enum SellerSampleCategory
    {
        //美食
        [Localization("SellerCategoryCuisine")]
        [SystemCode("SellerSampleCategory", 0)]
        Cuisine,
        //旅遊
        [Localization("SellerCategoryTravel")]
        [SystemCode("SellerSampleCategory", 1)]
        Travel,
        //女性
        [Localization("SellerCategoryPeauty")]
        [SystemCode("SellerSampleCategory", 2)]
        Peauty,
        //其他
        [Localization("SellerCategoryOthers")]
        [SystemCode("SellerSampleCategory", 3)]
        Others
    }
    /// <summary>
    /// 優惠券店家平均消費
    /// </summary>
    public enum SellerConsumptionAvg
    {
        [Localization("SellerConsumptionAvgUnder150")]
        Under_150,
        [Localization("SellerConsumptionAvgBetween150And250")]
        Between_150_And_250,
        [Localization("SellerConsumptionAvgBetween250And500")]
        Between_250_And_500,
        [Localization("SellerConsumptionAvgBetween500And1000")]
        Between_500_And_1000,
        [Localization("SellerConsumptionAvgBetween1000And2000")]
        Between_1000_And_2000,
        [Localization("SellerConsumptionAvgBetween2000And3000")]
        Between_2000_And_3000,
        [Localization("SellerConsumptionAvgBetween3000And4000")]
        Between_3000_And_4000,
        [Localization("SellerConsumptionAvgBetween4000And5000")]
        Between_4000_And_5000,
        [Localization("SellerConsumptionAvgOver5000")]
        Over_5000
    }

    public enum FundTransferType
    {
        Unknown = 0,
        /// <summary>
        /// 中信ACH
        /// </summary>
        CtAch = 1,
        /// <summary>
        /// 財會部門處理 (網路銀行直接轉帳)
        /// </summary>
        FinDept = 2,
        /// <summary>
        /// 商家退款
        /// </summary>
        VendorRefund = 3

    }

    /// <summary>
    /// 賣家規模
    /// </summary>
    public enum SellerLevel
    {
        [Localization("SellerLevelBig")]
        Big,
        [Localization("SellerLevelMedium")]
        Medium,
        [Localization("SellerLevelSmall")]
        Small,
        [Localization("SellerLevelA")]
        A,
        [Localization("SellerLevelB")]
        B,
        [Localization("SellerLevelC")]
        C,
        [Localization("SellerLevelNone")]
        None,
        [Localization("SellerLevelDelivery")]
        Delivery,
        [Localization("SellerLevelSA")]
        SA,
        [Localization("SellerLevelD")]
        D,
    }

    public enum SellerMemberType
    {
        /// <summary>
        /// 不指定
        /// </summary>
        None = 0,
        /// <summary>
        /// 17Life 網站會員
        /// </summary>
        SiteMember = 1,
        /// <summary>
        /// 商家自建會員(部份可能會是17Life會員, 依 member.unique_id 連結資料)
        /// </summary>
        SellerMember = 2
    }

    public enum StoreCloseDate
    {
        /// <summary>
        /// 每週
        /// </summary>
        [Localization("EveryWeekly")]
        EveryWeekly = 1,
        /// <summary>
        /// 單週
        /// </summary>
        [Localization("SingleWeekly")]
        SingleWeekly = 2,
        /// <summary>
        /// 雙週
        /// </summary>
        [Localization("BiWeekly")]
        BiWeekly = 3,
        /// <summary>
        /// 第一週
        /// </summary>
        [Localization("FirstWeekly")]
        FirstWeekly = 4,
        /// <summary>
        /// 第二週
        /// </summary>
        [Localization("SecondWeekly")]
        SecondWeekly = 5,
        /// <summary>
        /// 第三週
        /// </summary>
        [Localization("ThirdWeekly")]
        ThirdWeekly = 6,
        /// <summary>
        /// 第四週
        /// </summary>
        [Localization("ForthWeekly")]
        ForthWeekly = 7,
        /// <summary>
        /// 第五週
        /// </summary>
        [Localization("FifthWeekly")]
        FifthWeekly = 8,
        /// <summary>
        /// 每月
        /// </summary>
        [Localization("EveryMonthly")]
        EveryMonthly = 9,
        /// <summary>
        /// 單月
        /// </summary>
        [Localization("SingleMonthly")]
        SingleMonthly = 10,
        /// <summary>
        /// 雙月
        /// </summary>
        [Localization("BiMonthly")]
        BiMonthly = 11,
        /// <summary>
        /// 特殊日期
        /// </summary>
        [Localization("SpecialDate")]
        SpecialDate = 12,
        /// <summary>
        /// 全年無休
        /// </summary>
        [Localization("AllYearRound")]
        AllYearRound = 13
    }

    public enum ProposalDealCharacter
    {
        /// <summary>
        /// 無
        /// </summary>
        [Localization("ProposalDealCharacterNone")]
        None = 0,
        /// <summary>
        /// 菜色
        /// </summary>
        [Localization("ProposalDealCharacterToShopFood")]
        ToShopFood = 1,
        /// <summary>
        /// 特殊食材
        /// </summary>
        [Localization("ProposalDealCharacterToShopSpecialFood")]
        ToShopSpecialFood = 2,
        /// <summary>
        /// 烹調方式
        /// </summary>
        [Localization("ProposalDealCharacterToShopCook")]
        ToShopCook = 4,
        /// <summary>
        /// 環境
        /// </summary>
        [Localization("ProposalDealCharacterToShopEnvironment")]
        ToShopEnvironment = 8,
        /// <summary>
        /// 一般憑證其他
        /// </summary>
        [Localization("ProposalDealCharacterToShopOther")]
        ToShopOther = 16,
        /// <summary>
        /// 設備/器材
        /// </summary>
        [Localization("ProposalDealCharacterPBeautyDevice")]
        PBeautyDevice = 32,
        /// <summary>
        /// 商品
        /// </summary>
        [Localization("ProposalDealCharacterPBeautyProduct")]
        PBeautyProduct = 64,
        /// <summary>
        /// 地點
        /// </summary>
        [Localization("ProposalDealCharacterPBeautyLocation")]
        PBeautyLocation = 128,
        /// <summary>
        /// 環境
        /// </summary>
        [Localization("ProposalDealCharacterPBeautyEnvironment")]
        PBeautyEnvironment = 256,
        /// <summary>
        /// 完美其他
        /// </summary>
        [Localization("ProposalDealCharacterPBeautyOther")]
        PBeautyOther = 512,
        /// <summary>
        /// 檔次特色1
        /// </summary>
        [Localization("ProposalDealCharacterToHouse1")]
        ToHouse1 = 1024,
        /// <summary>
        /// 檔次特色2
        /// </summary>
        [Localization("ProposalDealCharacterToHouse2")]
        ToHouse2 = 2048,
        /// <summary>
        /// 檔次特色3
        /// </summary>
        [Localization("ProposalDealCharacterToHouse3")]
        ToHouse3 = 4096,
        /// <summary>
        /// 檔次特色4
        /// </summary>
        [Localization("ProposalDealCharacterToHouse4")]
        ToHouse4 = 8192,
    }

    public enum ProposalMediaReport
    {
        /// <summary>
        /// 無
        /// </summary>
        [Localization("ProposalMediaReportNone")]
        None = 0,
        /// <summary>
        /// 報導圖檔
        /// </summary>
        [Localization("ProposalMediaReportPic")]
        Pic = 1,
        /// <summary>
        /// 連結
        /// </summary>
        [Localization("ProposalMediaReportLink")]
        Link = 2,
        /// <summary>
        /// 崁入
        /// </summary>
        [Localization("ProposalMediaReportLinkIn")]
        LinkIn = 4,
        /// <summary>
        /// 外連
        /// </summary>
        [Localization("ProposalMediaReportLinkOut")]
        LinkOut = 8,
    }

    public enum ProposalDeliveryMethod
    {
        /// <summary>
        /// 常溫
        /// </summary>
        [Localization("ProposalDeliveryMethodNormal")]
        Normal = 1,
        /// <summary>
        /// 冷藏
        /// </summary>
        [Localization("ProposalDeliveryMethodRefrigeration")]
        Refrigeration = 2,
        /// <summary>
        /// 冷凍
        /// </summary>
        [Localization("ProposalDeliveryMethodFreezer")]
        Freezer = 3,
    }

    /// <summary>
    /// 商品來源
    /// </summary>
    public enum ProposalDealSource
    {
        /// <summary>
        /// 原廠
        /// </summary>
        [Localization("ProposalSpecialFlagOriginal")]
        Original =1,
        /// <summary>
        /// 經銷商
        /// </summary>
        [Localization("ProposalSpecialFlagDistributor")]
        Distributor = 2,
        /// <summary>
        /// 平行輸入
        /// </summary>
        [Localization("ProposalSpecialFlagParallelImportation")]
        ParallelImportation = 3
    }

    /// <summary>
    /// 轉介人員類別
    /// </summary>
    public enum ReferralType
    {
        /// <summary>
        /// 未歸類
        /// </summary>
        None = 0,
        /// <summary>
        /// 業務人員
        /// </summary>
        [Localization("ReferralTypeSale")]
        Sale = 1,
        /// <summary>
        /// 17Life其他同仁
        /// </summary>
        [Localization("ReferralTypeCompanyOthers")]
        CompanyOthers = 2,
        /// <summary>
        /// 外部人員
        /// </summary>
        [Localization("ReferralTypeOuter")]
        Outer = 3
    }

    /// <summary>
    /// 店家拜訪類別
    /// </summary>
    public enum SellerEventType
    {
        /// <summary>
        /// 電訪
        /// </summary>
        [Localization("SellerEventTypePhone")]
        Phone = 10,
        /// <summary>
        /// 親訪
        /// </summary>
        [Localization("SellerEventTypeVisited")]
        Visited = 20,
        /// <summary>
        /// 開發
        /// </summary>
        [Localization("SellerEventTypeDevelop")]
        Develop = 30,
        /// <summary>
        /// 追蹤
        /// </summary>
        [Localization("SellerEventTypeTrack")]
        Track = 40,
        /// <summary>
        /// 陌開
        /// </summary>
        [Localization("SellerEventTypeStrangeDevelop")]
        StrangeDevelop = 50,
        /// <summary>
        /// 提案
        /// </summary>
        [Localization("SellerEventTypeProposal")]
        Proposal = 60,
        /// <summary>
        /// 簽約NEW
        /// </summary>
        [Localization("SellerEventTypeSignedNew")]
        SignedNew = 70,
        /// <summary>
        /// 簽約OLD
        /// </summary>
        [Localization("SellerEventTypeSignedOld")]
        SignedOld = 80,
        /// <summary>
        /// 其他
        /// </summary>
        [Localization("SellerEventTypeOther")]
        Other = 90
    }

    /// <summary>
    /// 行事曆類別
    /// </summary>
    public enum SalesCalendarType
    {
        /// <summary>
        /// 行程
        /// </summary>
        Schedlule = 0,
        /// <summary>
        /// 待辦
        /// </summary>
        Todo = 1
    }

    public enum ShoppingCartFreightStatus
    {
        /// <summary>
        /// 待審核
        /// </summary>
        [Localization("ShoppingCartFreightStatusInit")]
        Init = 0,
        /// <summary>
        /// 啟用
        /// </summary>
        [Localization("ShoppingCartFreightStatusApprove")]
        Approve = 1
    }
    public enum ShoppingCartFreightLogType
    {
        /// <summary>
        /// 使用者
        /// </summary>
        User = 0,
        /// <summary>
        /// 系統
        /// </summary>
        System = 1
    }

    public enum ProposalBatchBidStatus
    {
        Init = 0,
        Ready = 1,
        Queue = 2,
        Done = 3,
        Error = 99
    }

    public enum SellerSalesType
    {
        /// <summary>
        /// 開發業務
        /// </summary>
        Develope = 1,
        /// <summary>
        /// 經營業務
        /// </summary>
        Operation = 2
    }
    public enum ProposalHouseStatus
    {
        [Description("商家確認")]
        VbsApprove = 10,
        [Description("業務審核通過")]
        SaleApprove = 20,
        [Description("主管審核通過")]
        ManagerApprove = 30,
        [Description("製檔完成")]
        ProductionApprove = 40,
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace LunchKingSite.Core
{
    /// <summary>
    /// Table: famiport_barcode status
    /// </summary>
    public enum FamiposBarcodeStatus : byte
    {
        /// <summary>
        /// 產生交易序號/產生barcode code
        /// </summary>
        CreateTranNo = 0,
        /// <summary>
        /// POS機發出確認barcode是否正確 (第一次)
        /// </summary>
        ConfirmDeal = 1,
        /// <summary>
        /// POS機確認完成付款交易 (第二次)
        /// </summary>
        TranComplete = 2,
    }

    /// <summary>
    /// famiport 後台檔次申請狀態
    /// </summary>
    public enum FamiportStatus 
    {
        /// <summary>
        /// 草稿
        /// </summary>
        Draft,
        /// <summary>
        /// 製檔中
        /// </summary>
        OnTheMake,
        /// <summary>
        /// 等待確認
        /// </summary>
        WaitingConfirm,
        /// <summary>
        /// 已確認
        /// </summary>
        Confirmed,
        /// <summary>
        /// 已上檔
        /// </summary>
        OnProduct,
        /// <summary>
        /// 第一次產碼中
        /// </summary>
        OnFirstCreatingCode,
        /// <summary>
        /// 加碼產碼中
        /// </summary>
        OnExtraCreatingCode,
        
    }

    /// <summary>
    /// family 代金卷後台檔次狀態
    /// </summary>
    public enum FamilyNetEventStatus
    {
        /// <summary>
        /// 草稿
        /// </summary>
        Draft,
        /// <summary>
        /// 製檔中
        /// </summary>
        OnTheMake,
        /// <summary>
        /// 待確認
        /// </summary>
        WaitingConfirm,
        /// <summary>
        /// 已確認
        /// </summary>
        Confirmed,        
        /// <summary>
        /// 已下架
        /// </summary>
        Shelves
    }

    /// <summary>
    /// family 代金卷商品類型
    /// </summary>
    public enum FamilyNetEventIsComboDeal
    {
        /// <summary>
        /// 單檔
        /// </summary>
        Single =0,
        /// <summary>
        /// 多檔
        /// </summary>
        Multi = 1
    }

    /// <summary>
    /// family 代金卷商品類型
    /// </summary>
    public enum FamilyBarcodeVersion
    {
        /// <summary>
        /// 全家三段式條碼
        /// </summary>
        [Description("全家三段式條碼")]
        Pincode =0,
        /// <summary>
        /// 全家一段式條碼
        /// </summary>
        [Description("全家一段式條碼")]
        FamiSingleBarcode = 1
    }

    /// <summary>
    /// 全家檔商品類型
    /// </summary>
    public enum FamilyDealType
    {
        /// <summary>
        /// 一般全家檔
        /// </summary>
        General =0,
        /// <summary>
        /// 全家Beacon活動檔
        /// </summary>
        BeaconEvent =1
    }
    /// <summary>
    /// 全家商品活動周期
    /// </summary>
    public enum FamilyDisplayType
    {
        /// <summary>
        /// 使用者自訂
        /// </summary>
        UserSetting = 0,
        /// <summary>
        /// 單日
        /// </summary>
        Day=1,
        /// <summary>
        /// 週間
        /// </summary>
        Week=2,
    }

    public enum FamiChannel
    {
        /// <summary>
        /// 列印小白單
        /// </summary>
        Famiport = 0,
        /// <summary>
        /// 手機直秀 barcode
        /// </summary>
        FamiPos = 1
    }

    /// <summary>
    /// 全家(全網) Famiport Pincode API 交易狀態碼
    /// </summary>
    public enum FamilyNetTransStatus
    {
        /// <summary>
        /// 產生PIN碼及條碼
        /// </summary>
        Code01 = 1,
        /// <summary>
        /// 產生條碼
        /// </summary>
        Code02 = 2,
        /// <summary>
        /// 產生PIN碼
        /// </summary>
        Code03 = 3,
        /// <summary>
        /// PIN碼及條碼退貨
        /// </summary>
        Code04 = 4,
        /// <summary>
        /// 查詢活動資料
        /// </summary>
        Code11 = 11,
        /// <summary>
        /// 查詢商品資料
        /// </summary>
        Code12 = 12,
        /// <summary>
        /// PIN碼結帳通知
        /// </summary>
        Code20 = 20,
        /// <summary>
        /// 查詢PIN碼及條碼資料
        /// </summary>
        Code21 = 21,
        /// <summary>
        /// 查詢條碼資料
        /// </summary>
        Code22 = 22,
        /// <summary>
        /// 查詢PIN碼資料
        /// </summary>
        Code23 = 23,
        /// <summary>
        /// 查詢PIN碼及條碼退貨資料
        /// </summary>
        Code24 = 24
    }

    /// <summary>
    /// Famiport API 回覆代碼
    /// </summary>
    public enum FamilyNetReplyCode
    {
        Initial = -1,
        /// <summary>
        /// Success
        /// </summary>
        [Description("成功")]
        Code00 = 0,
        /// <summary>
        /// PIN碼錯誤
        /// </summary>
        [Description("PIN碼錯誤")]
        Code10 = 10,
        /// <summary>
        /// 活動已截止
        /// </summary>
        [Description("活動已截止")]
        Code11 = 11,
        /// <summary>
        /// 數量已兌換完畢
        /// </summary>
        [Description("數量已兌換完畢")]
        Code12 = 12,
        /// <summary>
        /// PIN碼已退貨
        /// </summary>
        [Description("PIN碼已退貨")]
        Code13 = 13,
        /// <summary>
        /// 此PIN碼請至全家FamiPort列印紙本兌換券
        /// </summary>
        [Description("此PIN碼請至全家FamiPort列印紙本兌換券")]
        Code14 = 14,
        /// <summary>
        /// PIN碼已結帳
        /// </summary>
        [Description("PIN碼已結帳")]
        Code15 = 15,
        /// <summary>
        /// 查無對應的活動
        /// </summary>
        [Description("查無對應的活動")]
        Code20 = 20,
        /// <summary>
        /// 查無對應的訂單編號
        /// </summary>
        [Description("查無對應的訂單編號")]
        Code22 = 22,
        /// <summary>
        /// 訂單編號重覆
        /// </summary>
        [Description("訂單編號重覆")]
        Code23 = 23,
        /// <summary>
        /// 找不到認證代號
        /// </summary>
        [Description("找不到認證代號")]
        Code30 = 30,
        /// <summary>
        /// 日期時間格式不符
        /// </summary>
        [Description("日期時間格式不符")]
        Code31 = 31,
        /// <summary>
        /// 交易模式其他錯誤
        /// </summary>
        [Description("交易模式其他錯誤")]
        Code32 = 32,
        /// <summary>
        /// 回覆代碼其他錯誤
        /// </summary>
        [Description("回覆代碼其他錯誤")]
        Code33 = 33,
        /// <summary>
        /// 傳入參數錯誤
        /// </summary>
        [Description("傳入參數錯誤")]
        Code81 = 81,
        /// <summary>
        /// SQL等錯誤
        /// </summary>
        [Description("SQL等錯誤")]
        Code90 = 90,
        /// <summary>
        /// run SQL 影響數錯誤
        /// </summary>
        [Description("run SQL 影響數錯誤")]
        Code95 = 95,
        /// <summary>
        /// 例外狀況
        /// </summary>
        [Description("例外狀況")]
        Code99 = 99
    }

    public enum FamilyNetPincodeStatus
    {
        /// <summary>
        /// 初始
        /// </summary>
        Init = 0,
        /// <summary>
        /// 已交易
        /// </summary>
        Completed = 1,
        /// <summary>
        /// 交易後失敗
        /// </summary>
        Fail = 2
    }

    public enum FamilyNetPincodeReturnStatus
    {
        /// <summary>
        /// 初始
        /// </summary>
        Init = 0,
        /// <summary>
        /// 退貨中
        /// </summary>
        Returning = 1,
        /// <summary>
        /// 退貨成功
        /// </summary>
        Completed = 2,
        /// <summary>
        /// 退貨失敗
        /// </summary>
        Fail = 3
    }

    public enum FamilyNetReturnType
    {
        /// <summary>
        /// 使用者退貨
        /// </summary>
        UserReturn = 0,
        /// <summary>
        /// 系統結檔
        /// </summary>
        DealClosed = 1,
        /// <summary>
        /// Job補做退貨
        /// </summary>
        JobRetry = 2
    }

    public enum FamilyNetVerifyLogStatus
    {
        /// <summary>
        /// 初始
        /// </summary>
        Init = 0,
        /// <summary>
        /// 等待被處理
        /// </summary>
        WaitForExecute = 1,
        /// <summary>
        /// 核銷處理中
        /// </summary>
        Executing = 2,
        /// <summary>
        /// 處理完成
        /// </summary>
        Completed = 3,
        /// <summary>
        /// 處理失敗
        /// </summary>
        Fail = 4,
        /// <summary>
        /// 卡紙補發通知 (非核銷)
        /// </summary>
        ReissuePin = 5
    }

    public enum FamilyNetApi
    {
        None = 0,
        /// <summary>
        /// 取Pin
        /// </summary>
        PinOrder = 1,
        /// <summary>
        /// Pin 換 Barcode
        /// </summary>
        OrderExg = 2,
        PinReturn = 3,
    }

    public enum FamilyNetPincodeDetailFlag
    {
        None = 0,
        /// <summary>
        /// 17Life Call FamilyNet API 取得原始Barcode
        /// </summary>
        OriginalBarcode = 1,
        /// <summary>
        /// 卡紙補發後的核銷Barcode
        /// </summary>
        ReissueBarcode = 2,
        /// <summary>
        /// 消費者Call全家客服進行卡紙補發，全家給我們的卡紙補發通知 (非核銷呼叫)
        /// 卡紙補發 = 作廢已取得的Barcode，原Pincode回到未領取Barcode狀態，消費者可至Famiport以Pin碼兌換新barcode
        /// </summary>
        ReissuePinCode = 3
    }
}

﻿using LunchKingSite.Core.Component;
using System;

namespace LunchKingSite.Core
{
    public enum PaymentType
    {
        [Localization("Creditcard")]
        Creditcard = 1,
        [Localization("PCash")]
        PCash = 2,
        [Localization("BonusPoint")]
        BonusPoint = 3,
        [Localization("ByPass")]
        ByPass = 4,
        [Localization("SCash")]
        SCash = 5,
        [Localization("ATM")]
        ATM = 6,
        [Localization("DiscountCode")]
        DiscountCode = 7,
        LCash = 8,
        Fami = 9,
        LinePay = 10,
        TaishinPay = 11,
        FamilyIsp = 12,
        SevenIsp = 13,
        /// <summary>
        /// 從Payeasy兌換來的購物金
        /// </summary>
        Pscash = 14,
    }

    /// <summary>
    /// 主要付款方式
    /// </summary>
    public enum ApiMainPaymentType
    {
        /// <summary>
        /// 未選擇 (全額以紅利金、購物金、折價券、PEZ點數等支付)
        /// Mapping to CashTrustLog.SCash/CashTrustLog.BCash/CashTrustLog.DiscountAmount/CashTrustLog.PCash
        /// </summary>
        None = 0,
        /// <summary>
        /// 信用卡
        /// Mapping to CashTrustLog.CreditCard
        /// </summary>
        CreditCard = 1,
        /// <summary>
        /// ATM轉帳
        /// Mapping to CashTrustLog.ATM
        /// </summary>
        ATM = 2,
        /// <summary>
        /// Line Pay
        /// Mapping to CashTrustLog.TCash
        /// </summary>
        LinePay = 3,
        /// <summary>
        /// 台新儲值支付
        /// Mapping to CashTrustLog.TCash
        /// </summary>
        TaishinPay = 4,
        /// <summary>
        /// Apple Pay
        /// Mapping to CashTrustLog.CreditCard
        /// </summary>
        ApplePay = 5,
        /// <summary>
        /// Masterpass
        /// Mapping to CashTrustLog.CreditCard
        /// </summary>
        Masterpass = 6,
        /// <summary>
        /// 全家超商付款
        /// Mapping to CashTrustLog.FamilyIsp
        /// </summary>
        FamilyIsp = 7,
        /// <summary>
        /// 7-11超商付款
        /// Mapping to CashTrustLog.SevenIsp
        /// </summary>
        SevenIsp = 8,
    }

    public enum PayTransType
    {
        [Localization("PayTransAuthorization")]
        Authorization = 0,

        [Localization("PayTransCharging")]
        Charging = 1,

        [Localization("PayTransRefund")]
        Refund = 2,
    }

    public enum PayTransResponseType
    {
        Initial = -1,
        OK = 0,
        GenericError = 1,
        InvalidMerchant = 2,
        DuplicateCharge = 3,
        IncompleteRequestData = 4,
        InvalidAccountNumber = 5,
        CreditCardFail = -9527
    }

    public enum PayTransPhase
    {
        [Localization("PayTransCreated")]
        Created = 0,

        [Localization("PayTransRequested")]
        Requested = 1,

        [Localization("PayTransCanceled")]
        Canceled = 2,

        [Localization("PayTransSuccessful")]
        Successful = 3,

        [Localization("PayTransFailed")]
        Failed = 4,

        [Localization("PayTransTransfered")]
        Transfered = 5,

        [Localization("Invoiced")]
        Invoiced = 6,

        [Localization("Allowanced")]
        Allowanced = 7,

        All = 99,
    }

    public enum PaymentResultPageType
    {
        [Localization("Initial")]
        Initial = 0,

        [Localization("CreditcardSuccess")]
        CreditcardSuccess = 1,

        [Localization("CreditcardFailed")]
        CreditcardFailed = 2,

        [Localization("PromotionSuccess")]
        PromotionSuccess = 3,

        [Localization("PcashSuccess")]
        PcashSuccess = 4,

        [Localization("PcashFailed")]
        PcashFailed = 5,

        [Localization("ZeroPriceSuccess")]
        ZeroPriceSuccess = 6,

        [Localization("ZeroPriceFailed")]
        ZeroPriceFailed = 7,

        [Localization("FamiBarcodeSuccess")]
        FamiBarcodeSuccess = 8,

        [Localization("SkmZeroPriceSuccess")]
        SkmZeroPriceSuccess = 9,

        VisaCardNumberError = 10,

        [Localization("ThirdPartyPaySuccess")]
        ThirdPartyPaySuccess = 11,
        [Localization("ThirdPartyPayCancel")]
        ThirdPartyPayCancel = 12,
        [Localization("ThirdPartyPayFail")]
        ThirdPartyPayFail = 13,
        [Localization("FamilyNetGetPincodeFail")]
        FamilyNetGetPincodeFail = 14,
        [Localization("HiLifeGetPincodeFail")]
        HiLifeGetPincodeFail = 15,
    }

    [Flags]
    public enum PayTransStatusFlag
    {
        [ExcludeBinding]
        None = 0,

        [ExcludeBinding]
        PhaseMask = 0x0000000F,

        [ExcludeBinding]
        DepartmentMask = 0x000000F0,

        [ExcludeBinding]
        PaymentAPIProviderMask = 0x00000F00,

        [ExcludeBinding]
        PoLogMask = 0x0000F000,
    }

    public enum PayTransStatusBitShift
    {
        Charge = 0,
        Department = 4,
        PaymentAPIProvider = 8,
        PoLog = 12,
    }

    public enum PaymentAPIProvider
    {
        /// <summary>
        /// 0 - NCCC
        /// </summary>
        NCCC = 0,
        /// <summary>
        /// 1 - 網際威信：好康
        /// </summary>
        HiTrust = 1,
        /// <summary>
        /// 2 - 網際威信：品生活
        /// </summary>
        HiTrustPiinLife = 2,
        /// <summary>
        /// 3
        /// </summary>
        ForgionCard = 3,
        /// <summary>
        /// 4 - 網際威信：康旅
        /// </summary>
        HiTrustPayeasyTravel = 4,
        /// <summary>
        /// 5 - Payeasy
        /// </summary>
        PezWebService = 5,
        /// <summary>
        /// 6 - 藍新：好康
        /// </summary>
        Neweb = 6,
        /// <summary>
        /// 7 - 藍新：品生活
        /// </summary>
        NewebPiinLife = 7,
        /// <summary>
        /// 8 - 網際威信：好康：銀聯
        /// </summary>
        HiTrustUnionPay = 8,
        /// <summary>
        /// 9 - 網際威信：品生活：銀聯
        /// </summary>
        HiTrustPiinLifeUnionPay = 9,
        /// <summary>
        /// 10 - 網際威信：需要後三碼
        /// </summary>
        HiTrustContactWithCVV2 = 10,
        /// <summary>
        /// 11 - 網際威信：無需後三碼
        /// </summary>
        HiTrustContactWithoutCVV2 = 11,
        /// <summary>
        /// 12 - 藍新：需要後三碼
        /// </summary>
        NewebContactWithCVV2 = 12,
        /// <summary>
        /// 13 - 藍新：無需後三碼
        /// </summary>
        NewebContactWithoutCVV2 = 13,
        /// <summary>
        /// 14 - 藍新：分期付款
        /// </summary>
        NewebContactInstallment = 14,
        
        /// <summary>
        /// for test.
        /// 測試時，如果環境沒有裝 HiTrust 元件，也不想用 BypassPaymentProcess 的方式來跳過刷卡頁(資料會沒卡號)
        /// </summary>
        Mock = 15,

        /// <summary>
        /// 31 - 網際威信：測試：：Com元件
        /// </summary>
        HiTrustComTest = 31,

        /// <summary>
        /// 32 - 藍新：測試
        /// </summary>
        NewebTest = 32,

        /// <summary>
        /// 33 - 網際威信：測試：：.Net元件
        /// </summary>
        HiTrustDotNetTest = 33,
        /// <summary>
        /// 34 - Line Pay
        /// </summary>
        LinePay = 34,
        /// <summary>
        /// 35 - 台新儲值支付
        /// </summary>
        TaishinPay = 35,
        /// <summary>
        /// 36 - 台新銀行
        /// </summary>
        TaishinPaymnetGateway = 36,
        /// <summary>
        /// 37 - 台新商城 - 使用其他的merchant ID
        /// </summary>
        TaishinMallPaymnetGateway = 37,
        /// <summary>
        /// 38 - ApplePay
        /// </summary>
        ApplePay = 38,
        /// <summary>
        /// 39 - OTP
        /// </summary>
        TaishinPaymnetOTPGateway = 39,
        /// <summary>
        /// 40 - 國泰世華
        /// </summary>
        CathayPaymnetGateway = 40,
        /// <summary>
        /// 41 - 國泰世華OTP
        /// </summary>
        CathayPaymnetOTPGateway = 41,
        /// <summary>
        /// 42 - 國泰世華分期(無OTP)
        /// </summary>
        CathayPaymnetInstallment = 42,
        /// <summary>
        /// 43 - 國泰世華分期(含OTP)
        /// </summary>
        CathayPaymnetInstallmentWithOtp = 43,
    }

   #region 新版購物流程 PaymentUtility

    /// <summary>
    /// 錯誤類型
    /// </summary>
    public enum PaymentUtilityErrorType
    {
        /// <summary>
        /// 檔次售完
        /// </summary>
        DealSoldOut,
        /// <summary>
        /// 檔次未在販售中
        /// </summary>
        DealNotOnSale,
        /// <summary>
        /// 會員Id錯誤
        /// </summary>
        InvalidUserId,
        /// <summary>
        /// 非PEZ會員(使用Pcash)
        /// </summary>
        NotPezMember,
        /// <summary>
        /// 公益檔次付款錯誤
        /// </summary>
        CharityPaymentIncorrect,
        /// <summary>
        /// 零元檔次付款金額錯誤
        /// </summary>
        ZeroPaymentIncorrect,
        /// <summary>
        /// Pcash輸入錯誤
        /// </summary>
        PCashIncorrect,
        /// <summary>
        /// Pcash餘額不足付款
        /// </summary>
        PCashInsufficient,
        /// <summary>
        /// 好康檔未選擇分店
        /// </summary>
        EmptyStoreGuid,
        /// <summary>
        /// 查無分店資訊
        /// </summary>
        EmptyStore,
        /// <summary>
        /// 分店商品數量不足
        /// </summary>
        StoreQuantityInsufficient,
        /// <summary>
        /// 未填入地址
        /// </summary>
        EmptyAddress,
        /// <summary>
        /// 選項錯誤
        /// </summary>
        OptionIncorrect,
        /// <summary>
        /// 紅利點數不足
        /// </summary>
        BCashInsufficient,
        /// <summary>
        /// 紅利點數輸入錯誤
        /// </summary>
        BCashIncorrect,
        /// <summary>
        /// 購物金不足
        /// </summary>
        SCashInsufficient,
        /// <summary>
        /// 購物金錯誤
        /// </summary>
        SCashIncorrect,
        /// <summary>
        /// 超過每筆訂單最大購買數量
        /// </summary>
        MaxItemCountExceed,
        /// <summary>
        /// 超過每人可訂購數量
        /// </summary>
        MaxDailyItemCountExceed,
        /// <summary>
        /// 折價券有誤
        /// </summary>
        DiscountCodeIncorrect,
        /// <summary>
        /// shoppingcart為空
        /// </summary>
        ShoppingCartEmpty,
        /// <summary>
        /// 宅配地址錯誤 未包含地段
        /// </summary>
        DeliveryAddressError,
        /// <summary>
        /// 信用卡資料錯誤
        /// </summary>
        CreditCardNumFormatError,
        /// <summary>
        /// 付款api provider錯誤
        /// </summary>
        PaymentApiProviderIncorrect,
        /// <summary>
        /// 建立order程序出錯
        /// </summary>
        MakeOrderProcessError,
        /// <summary>
        /// 傳入的刷卡金額和購物車計算金額不符
        /// </summary>
        CreditCardAmountError,
        /// <summary>
        /// MakePayment程序
        /// </summary>
        MakePaymentProcessError,
        /// <summary>
        /// 刷卡元件發生錯誤
        /// </summary>
        CreditCardUtilityError,
        /// <summary>
        /// PaymentTransaction失敗
        /// </summary>
        PaymentTransactionFail
    }

    public enum PaymentUtilityRefundType
    {
        Success,
        /// <summary>
        /// 查無訂單
        /// </summary>
        OrderGuidInvalid,
        /// <summary>
        /// 已申請退貨，無法再申請
        /// </summary>
        ProcessingReturnForm,
        /// <summary>
        /// 已申請換貨處理中
        /// </summary>
        ExchangeProcessing,
        /// <summary>
        /// 訂單已全數退貨，無法再申請
        /// </summary>
        NotReturnable,
        /// <summary>
        /// 未達門檻不需退貨
        /// </summary>
        DealStatusError,
        /// <summary>
        /// 發票二聯改三聯處理中
        /// </summary>
        Invoice2To3,
        /// <summary>
        /// 憑證已使用完畢，無法退貨
        /// </summary>
        CouponRemainError,
        /// <summary>
        /// 商品已過鑑賞期
        /// </summary>
        OverTrialPeriod,
        /// <summary>
        /// 尚未完成ATM付款或逾期未付款
        /// </summary>
        ATMStatusError,
        /// <summary>
        /// 訂單不接受退貨申請
        /// </summary>
        NoRefund,
        /// <summary>
        /// 訂單已預約，無法申請退貨
        /// </summary>
        ReservationLock,
        /// <summary>
        /// 退貨過程錯誤
        /// </summary>
        RefundProcessError,
        /// <summary>
        /// 憑證已使用
        /// </summary>
        CouponUsed,
        /// <summary>
        /// 查無憑證號碼
        /// </summary>
        CouponIdInvalid,
        /// <summary>
        /// 憑證已過期
        /// </summary>
        CouponExpired,
        /// <summary>
        /// 憑證未使用
        /// </summary>
        CouponUnUsed,
        /// <summary>
        /// 憑證狀態錯誤
        /// </summary>
        CouponStatusError,
        /// <summary>
        /// 退貨成功
        /// </summary>
        RefundSuccess,
        /// <summary>
        /// 憑證已過期且預約
        /// </summary>
        ExpiredAndReservationLock,
        /// <summary>
        /// 不接受部分退貨
        /// </summary>
        NoPartialReturns
    }
    #endregion
}
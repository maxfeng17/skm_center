﻿using LunchKingSite.Core.Component;
using System.ComponentModel;

namespace LunchKingSite.Core
{
    public enum SystemCodeGroup
    {
        /// <summary>
        /// 拋棄式Email
        /// </summary>
        DEAfilter,
        /// <summary>
        /// 檔次Icon與Tag
        /// </summary>
        DealLabel,
        /// <summary>
        /// 上檔分類
        /// </summary>
        DealType,
        /// <summary>
        /// 品生活上檔分類
        /// </summary>
        HiDealCatg,
        /// <summary>
        /// HiDeal交貨型態
        /// </summary>
        HiDealDeliveryType,
        /// <summary>
        /// Coupon類運費設定分類
        /// </summary>
        HiDealFreightType,
        /// <summary>
        /// HiDeal發票處理類型
        /// </summary>
        HiDealInvoiceType,
        /// <summary>
        /// HiDeal前台顯示訂單狀態
        /// </summary>
        HiDealOrderShowStatus,
        /// <summary>
        /// HiDeal訂單狀態
        /// </summary>
        HiDealOrderStatus,
        /// <summary>
        /// HiDeal商品資料型態
        /// </summary>
        HiDealProductType,
        /// <summary>
        /// HD活動區域
        /// </summary>
        HiDealRegion,
        /// <summary>
        /// 退貨單狀態
        /// </summary>
        HiDealReturnedStatus,
        /// <summary>
        /// HiDeal客服問題類別
        /// </summary>
        HiDealServiceCategory,
        /// <summary>
        /// 優惠類別
        /// </summary>
        HiDealSpecialDiscount,
        /// <summary>
        /// 優惠券活動分類
        /// </summary>
        SellerSampleCategory,
        /// <summary>
        /// 商家核銷方式
        /// </summary>
        SellerVerifyType,
        /// <summary>
        /// 單一JobLoadBalance HostName
        /// </summary>
        SingleJob,
        /// <summary>
        /// 提案單退件因素-資料修正細項(憑證)
        /// </summary>
        ProposalReturnDetailDataModifyOfPpon,
        /// <summary>
        /// 提案單退件因素-資料修正細項(宅配)
        /// </summary>
        ProposaReturnDetailDataModifyOfDelivery
    }
}

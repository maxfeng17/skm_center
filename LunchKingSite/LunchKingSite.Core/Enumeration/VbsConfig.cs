﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.Core.Enumeration
{
    public enum VbsMembershipPermissionScope
    {
        [Description("基本設定（可編輯）")]
        PcpBasicEdit = 1,
        [Description("基本設定（僅檢視）")]
        PcpBasicView = 2,
        [Description("會員優惠（可編輯）")]
        PcpMembershipCardEdit = 3,
        [Description("會員優惠（僅檢視）")]
        PcpMembershipCardView = 4,
        [Description("會員資訊（可編輯）")]
        PcpMemberInfoEdit = 5,
        [Description("會員資訊（僅檢視）")]
        PcpMemberInfoView = 6,
        [Description("點數功能（可編輯）")]
        PcpPointEdit = 7,
        [Description("點數功能（僅檢視）")]
        PcpPointView = 8,
        [Description("寄杯功能（可編輯）")]
        PcpDepositEdit = 9,
        [Description("寄杯功能（僅檢視）")]
        PcpDepositView = 10
    }

    public enum DocPageType
    {
        [Description("系統教學")]
        System = 1,
        [Description("申請/其他文件")]
        Other = 2
    }

    public enum VbsConfirmNoticeType
    {
        PchomeReturnRule2k19 = 1,
        DeliveryClose = 2
    }
}

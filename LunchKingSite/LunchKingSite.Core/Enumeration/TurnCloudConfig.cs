﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace LunchKingSite.Core
{
    /// <summary>
    /// 騰雲銷售上傳服務-交易型態列舉
    /// </summary>
    public enum TurnCloudSellTransType
    {
        /// <summary>
        /// 銷售
        /// </summary>
        Sell = 1,
        /// <summary>
        /// 退貨
        /// </summary>
        Refund = 2,
        /// <summary>
        /// 作廢
        /// </summary>
        Invalid = 3,
    }

    /// <summary>
    /// 騰雲銷售上傳服務-預售狀態列舉
    /// </summary>
    public enum TurnCloudPreDalesStatus
    {
        /// <summary>
        /// 一般銷售，停車預設寫0  因為在發票欄位上要填上空白
        /// </summary>
        Default = 0,
        /// <summary>
        /// 預售發生(銷售)
        /// </summary>
        Sale = 1,
        /// <summary>
        /// 預售結帳(核銷)
        /// </summary>
        WriteOff = 2
    }

    /// <summary> 騰雲銷售上傳服務-稅別列舉 </summary>
    public enum TurnCloudSellRateType
    {
        /// <summary> 免稅 </summary>
        Free = 0,
        /// <summary> 應稅 </summary>
        Taxable = 1,
        /// <summary> 零稅 </summary>
        ZeroTax = 2
    }

    /// <summary> 騰雲銷售上傳服務-折價方式列舉 </summary>
    public enum TurnCloudProductDiscountType
    {
        /// <summary>
        /// 無折讓或折扣
        /// </summary>
        [Description("")]
        None = 0,
        /// <summary> 折讓(10 元) </summary>
        [Description("1")]
        Amount = 1,
        /// <summary> 折扣(9折、8折) </summary>
        [Description("2")]
        Percentage = 2
    }

    /// <summary> 騰雲銷售上傳服務-電子發票載具類別編號值 </summary>
    public struct TurnCloudEuiVehicleTypeNo
    {
        /// <summary> 手機條碼 </summary>
        public static readonly string Mobile = "3J0002";
        /// <summary> 自然人憑證 </summary>
        public static readonly string PersonalCertificate = "CQ0001";
        /// <summary> 信用卡載具 </summary>
        public static readonly string CreditCard = "EK0002";
        /// <summary> 新光三越會員卡載具 </summary>
        public static readonly string SkmMember = "BG0002";
    }

    /// <summary> 騰雲銷售上傳服務-折價方式值 </summary>
    public struct TurnCloudSellTenderType
    {
        /// <summary> 信用卡聯合連線(目前沒用到 NCCC目前以8G為準)</summary>
        public const string NationalCreditCardCenter = "41";
        /// <summary> 信用卡聯合連線-退款(目前沒用到 NCCC目前以8G為準)</summary>
        public const string NationalCreditCardCenterRefund = "41";
        /// <summary> 信用卡台新連線 </summary>
        public const string TaishinCreditCard = "43";
        /// <summary> 信用卡台新連線-退款 </summary>
        public const string TaishinCreditCardRefund = "43";
        /// <summary> Skm Pay 台新</summary>
        public const string SkmPay = "8F";
        /// <summary> Skm Pay-退款 </summary>
        public const string SkmPayRefund = "8F";
        /// <summary> Skm Pay 聯合</summary>
        public const string SkmPayNCCC = "8G";
        /// <summary> Skm Pay 聯合-退款</summary>
        public const string SkmPayNCCCRefund = "8G";
        /// <summary> 會員點數扣點 </summary>
        public const string MemberPointsDeduct = "83";
        /// <summary> 會員點數扣點-退款 </summary>
        public const string MemberPointsDeductRefund = "83";
    }

    /// <summary> 騰雲銷售上傳服務-特賣型態 </summary>
    public struct TurnCloudSellPaymentType
    {
        /// <summary> 點數加價購 </summary>
        public static readonly string PointPlusCash = "20";
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.Core.Enumeration
{
    public enum HiLifeReturnType
    {
        /// <summary>
        /// 使用者退貨
        /// </summary>
        UserReturn = 0,

        /// <summary>
        /// 系統結檔
        /// </summary>
        DealClosed = 1,

        /// <summary>
        /// Job補做退貨
        /// </summary>
        JobRetry = 2
    }
    
    /// <summary>
    /// 因為萊爾富退貨  一次送10筆 10筆皆成功才算成功
    /// 只要有其中一筆失敗 其他9筆都不會被進行註銷
    /// 僅只回應可註銷(但不會被註銷)
    /// </summary>
    public enum HiLifeReturnStatus
    {
        Default = 0,
        Success = 1,
        InProcess = 2,
        Fail = 3,
    }

    public enum HiLifeVerifyType
    {
        UserTimely = 0,
        BatchFile=1
    }


    public enum HiLifeReturnReplyCode
    {
        Y = 0,
        N = 1
    }
}

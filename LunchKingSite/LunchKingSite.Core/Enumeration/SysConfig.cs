﻿using LunchKingSite.Core.Component;
using System.ComponentModel;

namespace LunchKingSite.Core
{
    public enum FaceBookPageType
    {
        /// <summary>
        /// 17Life好康的粉絲團
        /// </summary>
        PponMainPage,
        /// <summary>
        /// P玩美的粉絲團
        /// </summary>
        PeautyMainPage,
    }
    public enum FacebookPixelCode
    {
        /// <summary>
        /// JavaScript
        /// </summary>
        JsCode = 0,
        /// <summary>
        /// NoScript
        /// </summary>
        NsCode = 1,
    }
    /// <summary>
    /// DataManagerResettime的JobType欄位
    /// </summary>
    public enum DataManagerResettimeJobType
    {
        /// <summary>
        /// 總是執行，下一次執行時間預設為現在。
        /// </summary>
        Always = 0,
        /// <summary>
        /// 以時間間隔
        /// </summary>
        IntervalMinute = 1,
        /// <summary>
        /// 每小時
        /// </summary>
        EachHour = 2,
        /// <summary>
        /// 每天
        /// </summary>
        EachDay = 3,
        /// <summary>
        /// 每周
        /// </summary>
        EachWeek = 4,
        /// <summary>
        /// 每個月
        /// </summary>
        EachMonth = 5,
        /// <summary>
        /// 僅執行一次，透過後台強制執行。
        /// </summary>
        RunOnce = 6
    }

    public enum SmsSystem
    {
        Undefined = 0,
        Cht = 2,
        Sybase = 3,
        S2T = 4,
        iTe2 = 5,
    }

    /// <summary>
    /// 簡訊發送失敗的原因
    /// </summary>
    public enum SmsFailReason
    {
        None = 0,
        IllegalFormat = 1,
        Rejected = 2,
    }

    public enum SmsMultiple
    {
        Single = 0,
        Multiple = 1
    }

    /// <summary>
    /// 登入來源，用以區分公司的不同網站或服務
    /// </summary>
    public enum LoginFrom
    {
        Ppon = 0,
        PiinLife = 1,
    }

    public enum ApiImageSize
    {
        /// <summary>
        /// 很小 phone或3G環境建議採用
        /// </summary>
        XS = 1,
        /// <summary>
        /// 小 tabletPc建議採用
        /// </summary>
        S = 2,
        /// <summary>
        /// 一般 現有WEB使用的大小
        /// </summary>
        M = 3,
        /// <summary>
        /// 大 預留
        /// </summary>
        L = 4,
        /// <summary>
        /// 特大 預留
        /// </summary>
        XL = 5,

    }

    public enum JobLoadBalanceMode
    {
        Single = 1,
        Multiple = 2
    }

    /// <summary>
    /// 要壓縮的檔案類型
    /// </summary>
    public enum ZipFileType
    {
        Default,
        Xml,
        Byte,
    }

    public enum ApiReturnCode
    {
        /// <summary>
        /// 準備中
        /// </summary>
        Ready = -1,
        /// <summary>
        /// 完成作業
        /// </summary>
        Success = 0,
        /// <summary>
        /// ApiUserId不合法
        /// </summary>
        ApiUserIdError = 1,
        /// <summary>
        /// 輸入參數錯誤
        /// </summary>
        InputError = 2,
        /// <summary>
        /// 使用者尚未登入
        /// </summary>
        UserNoSignIn = 3,
        /// <summary>
        /// 結帳付款時發生錯誤
        /// </summary>
        PaymentError = 4,
        /// <summary>
        /// 傳入的裝置系統類型參數錯誤
        /// </summary>
        MobileOsTypeError = 5,
        /// <summary>
        /// 輸入參數不可用
        /// </summary>
        InputInvalid = 6,
        /// <summary>
        /// 資料不存在
        /// </summary>
        DataNotExist = 7,
        /// <summary>
        /// 未明確定義的錯誤
        /// </summary>
        Error = 8,
        /// <summary>
        /// 折價券更新狀態時發生錯誤
        /// </summary>
        DiscountCodeError = 9,
        /// <summary>
        /// 商品類型錯誤(商品或憑證)
        /// </summary>
        DealTypeError = 10,
        /// <summary>
        /// ATM可訂購商品已額滿
        /// </summary>
        ATMDealsFull,
        /// <summary>
        /// 信用卡資訊錯誤
        /// </summary>
        CreditcardInfoError,
        /// <summary>
        /// 已超過最大訂購數量
        /// </summary>
        OutofLimitOrderError,
        /// <summary>
        /// 團購好康已結束
        /// </summary>
        DealClosed,
        /// <summary>
        /// 折價券已使用過
        /// </summary>
        DiscountCodeIsUsed,
        /// <summary>
        /// 錯誤的使用者資訊
        /// </summary>
        BadUserInfo,
        /// <summary>
        /// 相容web api
        /// </summary>
        Success2 = 1000,
        /// <summary>
        /// 查無記憶信用卡
        /// </summary>
        CreditcardNotFound,

        #region 優惠券相關
        /// <summary>
        /// 無效憑證
        /// </summary>
        InvalidCoupon = 4000,
        /// <summary>
        /// 此憑證已退貨
        /// </summary>
        CouponReturned = 4010,
        /// <summary>
        /// 此憑證已使用
        /// </summary>
        CouponUsed = 4020,
        /// <summary>
        /// 此憑證不在使用期限內
        /// </summary>
        InvalidInterval = 4030,
        /// <summary>
        /// 此批憑證部分失敗
        /// </summary>
        CouponPartialFail = 4040,
        /// <summary>
        /// 此批憑證數量不足需核銷數
        /// </summary>
        CouponQtyNotEnough = 4050,

        #endregion 優惠券相關
    }

    /// <summary>
    /// 新版API採用的API處理狀況之回覆，所有的TYPE依據類型做大分類的區別如下
    /// 1000~1999:共用類型
    /// 2000~2999:會員相關
    /// 3000~3999:團購相關
    /// 4000~4999:優惠券相關
    /// 5000~5999:pcp相關
    /// 6000~6999:Skm相關
    /// 7000~7999:台新行動錢包支付相關
    /// 8000~8999:17Pay相關 
    /// </summary>
    public enum ApiResultCode
    {
        Ready = 0,

        #region (1000) 共用類型
        /// <summary>
        /// 呼叫成功，未發生錯誤
        /// </summary>
        [Localization("ApiResultCodeSuccess")]
        Success = 1000,
        /// <summary>
        /// 未明確定義之錯誤
        /// </summary>
        [Localization("ApiResultCodeError")]
        Error = 1010,
        /// <summary>
        /// 輸入參數錯誤
        /// </summary>
        [Localization("ApiResultCodeInputError")]
        InputError = 1020,
        /// <summary>
        /// Token已過期
        /// </summary>
        [Localization("ApiResultCodeOAuthTokenExpired")]
        OAuthTokenExpired = 1100,
        /// <summary>
        /// 查無此OAuthToken
        /// </summary>
        [Localization("ApiResultCodeOAuthTokerNotFound")]
        OAuthTokerNotFound = 1110,
        /// <summary>
        /// Token未授權
        /// </summary>
        [Localization("ApiResultCodeOAuthTokerNoAuth")]
        OAuthTokerNoAuth = 1120,
        /// <summary>
        /// 系統繁忙中，請30秒後再試一次。
        /// </summary>
        [Localization("ApiResultCodeSystemBusy")]
        SystemBusy = 1130,
        /// <summary>
        /// 查無資料
        /// </summary>
        [Localization("ApiResultCodeDataNotFound")]
        DataNotFound = 1140,
        /// <summary>
        /// 新增更新失敗
        /// </summary>
        [Localization("ApiResultCodeDataSaveFail")]
        SaveFail = 1150,
        /// <summary>
        /// 登入成功請修改密碼
        /// </summary>
        [Localization("ApiResultCodeLoginSuccessNeedChangePassword")]
        LoginSuccessNeedChangePassword = 1160,
        /// <summary>
        /// 資料已存在
        /// </summary>
        [Localization("ApiResultCodeDataIsExist")]
        DataIsExist = 1170,
        #endregion 共用類型

        #region (2000) 會員相關
        /// <summary>
        /// 使用者尚未登入
        /// </summary>
        [Localization("ApiResultCodeUserNoSignIn")]
        UserNoSignIn = 2010,
        /// <summary>
        /// 錯誤的使用者資訊
        /// </summary>
        [Localization("ApiResultCodeBadUserInfo")]
        BadUserInfo = 2020,
        /// <summary>
        /// 會員註冊失敗
        /// </summary>
        [Localization("ApiResultUserRegisterFail")]
        UserRegisterFail = 2030,
        /// <summary>
        /// 帳號停權
        /// </summary>
        [Localization("ApiResultAccountDisable")]
        AccountDisable = 2040,
        /// <summary>
        /// 會員資料不存在
        /// </summary>
        [Localization("AccountNotFound")]
        AccountNotFound = 2050,
        /// <summary>
        /// 手機認證傳送認證簡訊-成功
        /// </summary>
        [Localization("ApiResultMobileAuthSendSmsSuccess")]
        MobileAuthSendSmsSuccess = 2110,
        /// <summary>
        /// 手機認證傳送認證簡訊-號碼認證過了
        /// </summary>
        [Localization("ApiResultMobileAuthUsedByOtherMember")]
        MobileAuthUsedByOtherMember = 2120,
        /// <summary>
        /// 手機認證傳送認證簡訊-發送次數超過上限
        /// </summary>
        [Localization("ApiResultMobileAuthTooManySmsSent")]
        MobileAuthTooManySmsSent = 2130,
        /// <summary>
        /// 手機認證傳送認證簡訊-失敗
        /// </summary>
        [Localization("ApiResultMobileAuthSendSmsFail")]
        MobileAuthSendSmsFail = 2140,
        /// <summary>
        /// 手機認證驗證碼-成功
        /// </summary>
        [Localization("MobileValidateSuccess")]
        MobileValidateSuccess = 2210,
        /// <summary>
        /// 手機認證驗證碼-驗證碼輸入有誤
        /// </summary>
        [Localization("MobileValidateCodeError")]
        MobileValidateCodeError = 2220,
        /// <summary>
        /// 手機認證驗證碼-驗證碼已過有效時間
        /// </summary>
        [Localization("MobileValidateTimeError")]
        MobileValidateTimeError = 2230,
        /// <summary>
        /// 手機認證驗證碼-手機號碼己經被使用了
        /// </summary>
        [Localization("MobileValidateUsedByOtherMember")]
        MobileValidateUsedByOtherMember = 2240,
        /// <summary>
        /// 手機認證驗證碼-其他問題
        /// </summary>
        [Localization("MobileValidateError")]
        MobileValidateError = 2250,
        /// <summary>
        /// 手機認證設定密碼-成功
        /// </summary>
        [Localization("MobileSetPasswordSuccess")]
        MobileSetPasswordSuccess = 2310,
        /// <summary>
        /// 手機認證設定密碼-驗證碼輸入有誤
        /// </summary>
        [Localization("MobileSetPasswordEmptyError")]
        MobileSetPasswordEmptyError = 2320,
        /// <summary>
        /// 手機認證設定密碼-驗證碼已過有效時間
        /// </summary>
        [Localization("MobileSetPasswordDataError")]
        MobileSetPasswordDataError = 2330,
        /// <summary>
        /// 手機認證設定密碼-手機號碼己經被使用
        /// </summary>
        [Localization("MobileSetPasswordOtherError")]
        MobileSetPasswordOtherError = 2340,
        /// <summary>
        /// 手機認證設定密碼-密碼格式錯誤
        /// </summary>
        [Localization("MobileSetPasswordFormatError")]
        MobileSetPasswordFormatError = 2350,
        /// <summary>
        /// 沒有建立手機會員
        /// </summary>
        [Localization("MobileMemberNotFound")]
        MobileMemberNotFound = 2360,
        /// <summary>
        /// 只有會員在待設定密碼的狀態下，才能使用此API設定密碼
        /// </summary>
        [Localization("MobileDisallowSetPasswordByKey")]
        MobileDisallowSetPasswordByKey = 2370,
        /// <summary>
        /// 重設密碼的金鑰錯誤
        /// </summary>
        [Localization("MobileResetPasswordKeyError")]
        MobileResetPasswordKeyError = 2380,
        /// <summary>
        /// 到達每日重送認證信的上限
        /// </summary>
        ReachingReSendAuthMailLimit = 2390,
        /// <summary>
        /// email會員 已註冊但未開通
        /// </summary>
        MailRegisterInactive = 2340,

        #endregion 會員相關

        #region (3000) 核銷相關

        /// <summary>
        /// 不支援此核銷方式
        /// </summary>
        [Localization("ApiResultCodeNotSupportThisWayToVerify")]
        NotSupportThisWayToVerify = 3010,

        #endregion 核銷相關

        #region (4000) 優惠券相關
        /// <summary>
        /// 無效憑證
        /// </summary>
        [Localization("ApiResultCodeInvalidCoupon")]
        InvalidCoupon = 4000,
        /// <summary>
        /// 此憑證已退貨
        /// </summary>
        [Localization("ApiResultCodeCouponReturned")]
        CouponReturned = 4010,
        /// <summary>
        /// 此憑證已使用
        /// </summary>
        [Localization("ApiResultCodeCouponUsed")]
        CouponUsed = 4020,
        /// <summary>
        /// 此憑證不在使用期限內
        /// </summary>
        [Localization("ApiResultCodeInvalidInterval")]
        InvalidInterval = 4030,
        /// <summary>
        /// 此批憑證部分失敗
        /// </summary>
        [Localization("ApiResultCouponPartialFail")]
        CouponPartialFail = 4040,
        /// <summary>
        /// 此批憑證數量不足需核銷數
        /// </summary>
        [Localization("ApiResultCouponQtyNotEnough")]
        CouponQtyNotEnough = 4050,
        /// <summary>
        /// 憑證未使用
        /// </summary>
        [Localization("ApiResultCouponUnused")]
        CouponUnused = 4060,

        #endregion 優惠券相關

        #region (5000) pcp相關
        /// <summary>
        /// 無法取得身份識別碼
        /// </summary>
        [Localization("ApiResultCodeVerifyIdentityFail")]
        VerifyIdentityFail = 5000,
        /// <summary>
        /// 抵用券驗證失敗
        /// </summary>
        [Localization("ApiResultCodeDiscountCodeError")]
        DiscountCodeError = 5010,
        /// <summary>
        /// Pcp交易失敗
        /// </summary>
        [Localization("ApiResultCodePcpTransactionFail")]
        PcpTransactionFail = 5020,
        /// <summary>
        /// 消費者沒有足夠的紅利點數
        /// </summary>
        [Localization("ApiResultCodePcpNotEnoughBonus")]
        PcpNotEnoughBonus = 5030,
        /// <summary>
        /// 超級紅利點折抵不可大於結帳金額
        /// </summary>
        [Localization("ApiResultCodePcpSuperBonusOverAmount")]
        PcpSuperBonusOverAmount = 5031,
        /// <summary>
        /// 找不到會員卡資訊
        /// </summary>
        [Localization("ApiResultCodePcpMembershipCardNotFound")]
        MembershipCardNotFound = 5040,
        /// <summary>
        /// 會員卡星期六日不可使用
        /// </summary>
        [Localization("ApiResultCodePcpMembershipCardNotUsingSaturdayOrSunday")]
        MembershipCardNotUsingSaturdayOrSunday = 5041,
        /// <summary>
        /// 會員卡特殊假日無法使用
        /// </summary>
        [Localization("ApiResultCodePcpMembershipCardCanNotUsingExceptSpecialHoliday")]
        MembershipCardCanNotUsingExceptSpecialHoliday = 5042,
        /// <summary>
        /// 會員卡未啟用
        /// </summary>
        [Localization("ApiResultCodePcpMembershipCardUnable")]
        MembershipCardUnable = 5043,
        /// <summary>
        /// 會員卡未上架
        /// </summary>
        [Localization("ApiResultCodePcpMembershipCardUnopened")]
        MembershipCardUnopened = 5044,
        /// <summary>
        /// 會員卡不在上架期間
        /// </summary>
        [Localization("ApiResultCodePcpMembershipCardWithoutOpenTime")]
        MembershipCardWithoutOpenTime = 5045,
        /// <summary>
        /// 會員卡不在有效期間
        /// </summary>
        [Localization("ApiResultCodePcpMembershipCardWithoutTimeOfEffect")]
        MembershipCardWithoutTimeOfEffect = 5046,
        /// <summary>
        /// 會員卡不可在這間店使用
        /// </summary>
        [Localization("ApiResultCodePcpMembershipCardCanNotUsingThisStore")]
        MembershipCardCanNotUsingThisStore = 5047,
        /// <summary>
        /// Pcp identity code 已過期
        /// </summary>
        [Localization("ApiResultCodePcpPcpCodeIsExpired")]
        PcpCodeIsExpired = 5050,
        /// <summary>
        /// Pcp identity code 已使用過
        /// </summary>
        [Localization("ApiResultCodePcpPcpCodeIsUsed")]
        PcpCodeIsUsed = 5051,
        /// <summary>
        /// Pcp identity code 已作廢
        /// </summary>
        [Localization("ApiResultCodePcpPcpCodeIsCancel")]
        PcpCodeIsCancel = 5052,
        /// <summary>
        /// 熟客卡已發行
        /// </summary>
        [Localization("ApiResultCodePcpMembershipCardWasIssued")]
        MembershipCardWasIssued = 5060,
        /// <summary>
        /// 熟客卡已作廢
        /// </summary>
        [Localization("ApiResultCodePcpMembershipCardWasCancelled")]
        MembershipCardWasCancelled = 5061,
        /// <summary>
        /// 熟客卡已過有效期限
        /// </summary>
        [Localization("ApiResultCodePcpMembershipCardExpire")]
        MembershipCardExpire = 5062,
        /// <summary>
        /// 熟客卡分店寫入錯誤
        /// </summary>
        [Localization("ApiResultCodeMembershipCardStoreSaveError")]
        MembershipCardStoreSaveError = 5063,
        /// <summary>
        /// 商家沒有足夠的熟客點
        /// </summary>
        [Localization("ApiResultCodeStoreRegularsPointNotEnough")]
        StoreRegularsPointNotEnough = 5070,
        /// <summary>
        /// 商家沒有足夠的公關點
        /// </summary>
        [Localization("ApiResultCodeStoreFavorPointNotEnough")]
        StoreFavorPointNotEnough = 5071,
        /// <summary>
        /// 商家沒有足夠的Pcp點數
        /// </summary>
        [Localization("ApiResultCodeStorePcpPointNotEnough")]
        StorePcpPointNotEnough = 5072,
        /// <summary>
        /// 推播產生名單失敗
        /// </summary>
        [Localization("ApiResultCodePushPcpInsertUserListFail")]
        PushPcpInsertUserListFail = 5080,
        /// <summary>
        /// 目前停止推播功能
        /// </summary>
        [Localization("ApiResultCodePushPcpDisable")]
        PushPcpDisable = 5081,
        /// <summary>
        /// 推播失敗
        /// </summary>
        [Localization("ApiResultCodePushPcpFail")]
        PushPcpFail = 5082,
        /// <summary>
        /// 找不到核銷記錄
        /// </summary>
        [Localization("ApiResultCodePcpOrderNotFound")]
        PcpOrderNotFound = 5090,
        /// <summary>
        /// 已取消核銷過
        /// </summary>
        [Localization("ApiResultCodePcpVerifyCancelled")]
        PcpVerifyCancelled = 5091,
        /// <summary>
        /// 找不到賣家資訊
        /// </summary>
        [Localization("ApiResultCodePcpSellerNotFound")]
        PcpSellerNotFound = 5092,
        /// <summary>
        /// 找不到分店
        /// </summary>
        [Localization("ApiResultCodePcpStoreNotFound")]
        PcpStoreNotFound = 5093,
        /// <summary>
        /// 不是當初核銷的分店
        /// </summary>
        [Localization("ApiResultCodePcpStoreNotMatch")]
        PcpStoreNotMatch = 5094,
        /// <summary>
        /// 找不到熟客卡資訊
        /// </summary>
        [Localization("ApiResultCodePcpVbsSellerMemberNotFound")]
        PcpVbsSellerMemberNotFound = 5095,
        /// <summary>
        /// 找不到賣家階層關聯資料
        /// </summary>
        [Localization("ApiResultCodePcpSellerTreeNotFound")]
        PcpSellerTreeNotFound = 5096,
        /// <summary>
        ///不是廠商提供序號的檔次
        /// </summary>
        [Localization("ApiResultCodePcpPezEventNotFound")]
        PcpPezEventNotFound = 5097,
        /// <summary>
        ///找不到此序號
        /// </summary>
        [Localization("ApiResultCodePcpSerialNotFound")]
        PcpSerialNotFound = 5098,
        /// <summary>
        ///同意條款尚未同意
        /// </summary>
        [Localization("ApiResultCodePcpSerialNotFound")]
        PcpAgreementNotAccept = 5099,
        /// <summary>
        ///尚未領卡成功
        /// </summary>
        [Localization("PcpApplyCardFail")]
        PcpApplyCardFail = 5100,
        /// <summary>
        ///所有序號並非存在同一張訂單
        /// </summary>
        [Localization("PcpSerialsNotSameOrder")]
        PcpSerialsNotSameOrder = 5101,
        /// <summary>
        ///所有序號並非同個店家所核銷
        /// </summary>
        [Localization("PcpSerialsNotSameStore")]
        PcpSerialsNotSameStore = 5102,
        #endregion pcp相關

        #region (6000) skm return code 

        /// <summary>
        /// 驗證 skm 會員失敗
        /// </summary>
        [Localization("ApiResultSkmMemberVaildateFail")]
        SkmMemberVaildateFail = 6000,
        /// <summary>
        /// skm 總館不存在
        /// </summary>
        [Localization("ApiResultSkmSellerIsNull")]
        SkmSellerIsNull = 6010,
        /// <summary>
        /// 燒點失敗
        /// </summary>
        [Localization("ApiResultSkmBurningFail")]
        SkmBurningFail = 6020,
        /// <summary>
        /// 燒點失敗 - 未傳APP卡號
        /// </summary>
        [Localization("ApiResultSkmBurningFailCardNoIsNull")]
        SkmBurningFailCardNoIsNull = 6021,
        /// <summary>
        /// 燒點失敗 - 新增燒點Log失敗
        /// </summary>
        [Localization("ApiResultSkmBurningFailOrderLogError")]
        SkmBurningFailOrderLogError = 6022,
        /// <summary>
        /// 活動獎項抽完
        /// </summary>
        [Localization("ApiResultDrawEventDrawOut")]
        DrawEventDrawOut = 6030,
        /// <summary>
        /// 活動異常
        /// </summary>
        [Localization("ApiResultDrawEventError")]
        DrawEventError = 6031,
        /// <summary>
        /// 超過活動(單日)參與次數上限
        /// </summary>
        [Localization("ApiResultDrawEventOverDailyLimit")]
        DrawEventOverDailyLimit = 6032,
        /// <summary>
        /// 超過活動(總)參與次數上限
        /// </summary>
        [Localization("ApiResultDrawEventOverTotailLimit")]
        DrawEventOverTotailLimit = 6033,
        /// <summary>
        /// 抽獎建立零元檔失敗(MakeOrder)
        /// </summary>
        [Localization("ApiResultMakePrizeDrawOrderFail")]
        MakePrizeDrawOrderFail = 6034,
        /// <summary>
        /// 更新抽獎紀錄兌換館別失敗
        /// </summary>
        [Localization("ApiResultUpdatePrizeDrawExchangeShopCodeFail")]
        UpdatePrizeDrawExchangeShopCodeFail = 6035,
        /// <summary>
        /// 傳送中獎紀錄API失敗
        /// </summary>
        [Localization("ApiResultSendPrizeDataFail")]
        SendPrizeDataFail = 6036,
        /// <summary>
        /// 傳送中獎紀錄API失敗 - 參數有誤
        /// </summary>
        [Localization("ApiResultSendPrizeDataFailInputError")]
        SendPrizeDataFailInputError = 6037,
        /// <summary>
        /// SKM PAY 建立訂單異常
        /// </summary>
        [Localization("ApiResultSkmPayOrderAddError")]
        SkmPayOrderAddError = 6040,
        /// <summary>
        /// Wallet server 授權交易失敗
        /// </summary>
        [Localization("ApiResultSkmPayAuthPaymentError")]
        SkmPayAuthPaymentError = 6041,
        /// <summary>
        /// Wallet server 等待OTP回傳驗證狀態
        /// </summary>
        [Localization("ApiResultSkmPayWaitingOTPResult")]
        SkmPayWaitingOTPResult = 6050,
        /// <summary>
        /// Wallet server OTP驗證失敗
        /// </summary>
        [Localization("ApiResultSkmPayOTPError")]
        SkmPayOTPError = 6051,
        /// <summary>
        /// OTP驗證逾時(超過config設定的訪問次數上限)
        /// </summary>
        [Localization("ApiResultSkmPayOTPTimeout")]
        SkmPayOTPTimeout = 6052,
        
        /// <summary>
        /// 開立發票失敗
        /// </summary>
        [Localization("ApiResultSkmPayCreateInvoiceFail")]
        CreateInvoiceFail = 6101,
        /// <summary>
        /// SKM更新庫存失敗
        /// </summary>
        [Localization("ApiResultSkmUpdateGiftInventoryFail")]
        SkmUpdateGiftInventoryFail = 6051,
        #endregion

        #region (7000) 台新行動錢包支付 wallet pay 

        /// <summary>
        /// 查無資料
        /// </summary>
        [Localization("ApiResultCodeWpNotFound")]
        WpNotFound = 7000,
        /// <summary>
        /// 手機號碼格式錯誤
        /// </summary>
        [Localization("ApiResultCodeWpMobileError")]
        WpMobileError = 7100,
        /// <summary>
        /// 信用卡號輸入錯誤
        /// </summary>
        [Localization("ApiResultCodeWpCreditCardError")]
        WpCreditCardError = 7101,
        /// <summary>
        /// 信用卡到期年月輸入錯誤
        /// </summary>
        [Localization("ApiResultCodeWpCardExpireDateError")]
        WpCardExpireDateError = 7102,
        /// <summary>
        /// 信用卡後三碼輸入錯誤
        /// </summary>
        [Localization("ApiResultCodeWpCardCvvError")]
        WpCardCvvError = 7103,
        /// <summary>
        /// 信用卡名稱輸入錯誤
        /// </summary>
        [Localization("ApiResultCodeWpCardNameError")]
        WpCardNameError = 7104,
        /// <summary>
        /// CheckSum驗證失敗
        /// </summary>
        [Localization("ApiResultCodeWpCheckSumError")]
        WpCheckSumError = 7105,
        /// <summary>
        /// Token已過期
        /// </summary>
        [Localization("ApiResultCodeWpTokenExpired")]
        WpTokenExpired = 7106,
        /// <summary>
        /// 信用卡交易失敗
        /// </summary>
        [Localization("ApiResultCodeWpTradeFailure")]
        WpTradeFailure = 7200,
        /// <summary>
        /// 信用卡交易成功
        /// </summary>
        [Localization("ApiResultCodeWpTradeSuccess")]
        WpTradeSuccess = 7201,
        /// <summary>
        /// CardToken過期
        /// </summary>
        [Localization("ApiResultCodeWpCardTokenError")]
        WpCardTokenError = 7202,
        /// <summary>
        /// 條碼驗證失敗
        /// </summary>
        [Localization("ApiResultCodeWpBarcodeError")]
        WpBarcodeError = 7300,
        /// <summary>
        /// 條碼驗證失敗
        /// </summary>
        [Localization("ApiResultCodeWpTspgRtnError")]
        WpTspgRtnError = 7400,

        #endregion

        #region (8000) 17Life Payment Result Code

        /// <summary>
        /// 驗證訂單資料錯誤
        /// </summary>
        [Localization("ApiResultCodePaymentCheckOrderFail")]
        PaymentCheckOrderFail = 8000,
        /// <summary>
        /// 取得憑證失敗
        /// </summary>
        [Localization("ApiResultCodePaymentGetCouponFail")]
        PaymentGetCouponFail = 8010,
        /// <summary>
        /// 檔次尚未開賣
        /// </summary>
        [Localization("ApiResultCodePaymentCanNotOrderDeal")]
        PaymentCanNotOrderDeal = 8020,
        /// <summary>
        /// 信用卡資訊有誤
        /// </summary>
        [Localization("ApiResultCodePaymentCreditcardInfoError")]
        PaymentCreditcardInfoError = 8030,        
        /// <summary>
        /// Masterpass 資訊錯誤
        /// </summary>
        [Localization("ApiResultCodeMasterpassDataError")]
        MasterpassDataError = 8040,
        /// <summary>
        /// Get Masterpass Data Fail
        /// </summary>
        [Localization("ApiResultCodeMasterpassDataNotFound")]
        MasterpassDataNotFound = 8041,
        /// <summary>
        /// Masterpass 資料已使用
        /// </summary>
        [Localization("ApiResultCodeMasterpassDataIsUsed")]
        MasterpassDataIsUsed = 8042,
        /// <summary>
        /// 付款發生錯誤
        /// </summary>
        [Localization("ApiResultCodePaymentError")]
        PaymentError = 8050,
        /// <summary>
        /// 商品庫存不足
        /// </summary>
        [Localization("ApiResultCodeOutofLimitOrderError")]
        OutofLimitOrderError = 8060,
        /// <summary>
        /// 很抱歉，您所購買的好康已結束！
        /// </summary>
        [Localization("ApiResultCodeDealClosed")]
        DealClosed = 8070,
        /// <summary>
        /// 折價券已使用過
        /// </summary>
        [Localization("ApiResultCodeDiscountCodeIsUsed")]
        DiscountCodeIsUsed = 8080,
        /// <summary>
        /// 折價券更新狀態時發生錯誤
        /// </summary>
        [Localization("ApiResultCodeDiscountCodeUsingError")]
        DiscountCodeUsingError = 8090,
        /// <summary>
        /// 查無記憶信用卡
        /// </summary>
        [Localization("ApiResultCodeCreditcardNotFound")]
        CreditcardNotFound = 8100,
        /// <summary>
        /// ATM可訂購商品已額滿
        /// </summary>
        [Localization("ApiResultCodeATMDealsFull")]
        ATMDealsFull = 8110,
        /// <summary>
        /// 目前商品不供應
        /// </summary>
        [Localization("ApiResultCodeDealsNotSale")]
        DealsNotSale = 8120,

        #endregion

    }

    /// <summary>
    /// 回傳台新TSCB代碼，有跟全家做過定義，不可自行新增
    /// </summary>
    public enum ApiTSCBResultCode
    {
        Ready = 0,
        /// <summary>
        /// 成功
        /// </summary>
        [Localization("TSCBResultSuccess")]
        Success = 1000,
        /// <summary>
        /// 17Life 失敗 - TSPG 連線失敗
        /// </summary>
        [Localization("TSCBResultTSPGConnectionFailure")]
        TSPGConnectionFailure = 31001,
        /// <summary>
        /// 17Life 失敗 - 台新KEY驗證失敗
        /// </summary>
        [Localization("TSCBResultBankKeyFailure")]
        BankKeyFailure = 31002,
        /// <summary>
        /// 17Life 失敗 - TSPG Toolkit 載入失敗
        /// </summary>
        [Localization("TSCBResultTSPGToolkitFailure")]
        TSPGToolkitFailure = 31003,
        /// <summary>
        /// 17Life 失敗 - 解析資料失敗
        /// </summary>
        [Localization("TSCBResultDataError")]
        DataError = 32001,
        /// <summary>
        /// 17Life 失敗 - Barcode過期
        /// </summary>
        [Localization("TSCBResultBarcodeOutOfDate")]
        BarcodeOutOfDate = 32002,
        /// <summary>
        /// 17Life 失敗 - 特店資料無法交易
        /// </summary>
        [Localization("TSCBResultMerchantDataError")]
        MerchantDataError = 32003,
        /// <summary>
        /// 17Life 失敗 - TSPG 回傳錯誤
        /// </summary>
        [Localization("TSCBResultTSPGRetFailure")]
        TSPGRetFailure = 32004,
        /// <summary>
        /// 17Life 失敗  - 寫入資料失敗
        /// </summary>
        [Localization("TSCBResultDataBaseFailure")]
        DataBaseFailure = 32005,
        /// <summary>
        /// 17Life 失敗  - 重覆交易
        /// </summary>
        [Localization("TSCBResultRepeat")]
        Repeat = 32006,
        /// <summary>
        /// 17Life 失敗 - 未明確定義錯誤
        /// </summary>
        [Localization("TSCBResultUnDefined")]
        UnDefined = 39001
    }


    /// <summary>
    /// OAuth Token Check 回傳結果
    /// </summary>
    public enum TokenResult
    {
        [Localization("TokenResultOK")]
        OK,
        [Localization("TokenResultExpired")]
        Expired,
        [Localization("TokenResultError")]
        Error,
        [Localization("TokenResultNotFound")]
        NotFound,
        [Localization("TokenResultNoAuth")]
        NoAuth
    }


    /// <summary>
    /// 單一執行程式，抓取system_code的code_group_name 
    /// </summary>
    public enum SingleOperator
    {
        /// <summary>
        /// Job(單一Job指定Server Name)
        /// </summary>
        SingleJob,
        /// <summary>
        /// 圖片(指定圖片上傳的網芳路徑)
        /// </summary>
        ImageUNC,
        /// <summary>
        /// 網路芳鄰登入帳密
        /// </summary>
        ImpersonateAccount,
        //Ga Token
        GoogleAnalytics
    }

    /// <summary>
    /// 圖片上傳網芳資料夾類別
    /// </summary>
    public enum ImageLoadBalanceFolder
    {
        /// <summary>
        /// 經ImageUtility上傳
        /// </summary>
        Media,
        /// <summary>
        /// 經CkEditor上傳
        /// </summary>
        Images,
        /// <summary>
        /// 經CkEditor上傳(Images的子目錄)
        /// </summary>
        ImagesU
    }

    /// <summary>
    /// QR核銷憑證Api回傳資訊TYPE
    /// </summary>
    public enum VbsApiCouponReturnType
    {
        /// <summary>
        /// 無可核銷憑證
        /// </summary>
        NoneVaileCoupon = 0,
        /// <summary>
        /// 可核銷憑證單筆
        /// </summary>
        SingleAvailableCoupon = 1,
        /// <summary>
        /// 可核銷憑證多筆
        /// </summary>
        MultipleAvailableCoupon = 2
    }
    /// <summary>
    /// API_User (手機類型)區別
    /// </summary>
    public enum ApiUserDeviceType
    {
        WebService = 0,
        Android = 1,
        IOS = 2,
        WinPhone = 3
    }
    /// <summary>
    /// 系統權限設定類別
    /// </summary>
    public enum SystemFunctionType
    {
        /// <summary>
        /// 全部
        /// </summary>
        All,
        /// <summary>
        /// 建立
        /// </summary>
        Create,
        /// <summary>
        /// 瀏覽
        /// </summary>
        Read,
        /// <summary>
        /// 修改
        /// </summary>
        Update,
        /// <summary>
        /// 刪除
        /// </summary>
        Delete,
        /// <summary>
        /// 退貨
        /// </summary>
        Refund,
        /// <summary>
        /// 強制退貨
        /// </summary>
        ForceRefund,
        /// <summary>
        /// 強制核銷
        /// </summary>
        ForceVerify,
        /// <summary>
        /// 核銷
        /// </summary>
        Verify,
        /// <summary>
        /// 稽核
        /// </summary>
        Audit,
        /// <summary>
        /// 手動結檔
        /// </summary>
        ManualPponDealClose,
        /// <summary>
        /// 設定進貨價
        /// </summary>
        SetCost,
        /// <summary>
        /// 2天內復原結檔
        /// </summary>
        RecoverPponDealClose,
        /// <summary>
        /// 2天後復原結檔
        /// </summary>
        RecoverPponDealClose3Day,
        /// <summary>
        /// 修改好康名稱
        /// </summary>
        UpdateItemName,
        /// <summary>
        /// 設定營運（賣家或店舖設定停止營運）
        /// </summary>
        SetCloseDown,
        /// <summary>
        /// 調整有效期限截止日
        /// </summary>
        SetExpireDate,
        /// <summary>
        /// 調整有效期限截止日(無日期限制)
        /// </summary>
        SetExpireDateNoLimit,
        /// <summary>
        /// 補產擋款週結月對帳單
        /// </summary>
        TriggerWeeklyMonthBalanceSheet,
        /// <summary>
        /// 免稅通知
        /// </summary>
        TaxFreeNotice,
        /// <summary>
        /// 下載
        /// </summary>
        Download,
        /// <summary>
        /// 出帳方式通知
        /// </summary>
        RemittanceNotice,
        /// <summary>
        /// 審核
        /// </summary>
        Approve,
        /// <summary>
        /// 可維護退換貨完成日
        /// </summary>
        SetFinishReturnAndExchangeDate,
        /// <summary>
        /// 可調整帳務相關日期:如暫付七成付款日及可開立對帳單日 及 發票調整金額
        /// </summary>
        SetRelatedFinanceDateAndAllowanceAmount,
        /// <summary>
        /// 可維護廠商補扣款資訊(不包括發票調整金額)
        /// </summary>
        SetVendorPaymentChangeWithOutAllowanceAmount,
        /// <summary>
        /// 於開檔後 or 長度大於特定長度 可異動業務名稱
        /// </summary>
        UpdateSalesName,
        /// <summary>
        /// 可更新已確認對帳單單據資料
        /// </summary>
        UpdateConfirmedBsBill,
        /// <summary>
        /// 開檔後修改出帳方式
        /// </summary>
        UpdateRemittanceType,
        /// <summary>
        /// 排檔
        /// </summary>
        ArrangeDeals,
        /// <summary>
        /// 可異動商家帳號權限
        /// </summary>
        ManageVendorAccountAuthority,
        /// <summary>
        /// 可設定使用折價券
        /// </summary>
        UserDiscount,
        /// <summary>
        /// 儲存並排序
        /// </summary>
        SavingAndSorting,
        /// <summary>
        /// 可視及變更KPI
        /// </summary>
        KPISet,
        /// <summary>
        /// 可視及變更KPI(全國宅配)
        /// </summary>
        KPISetS010,
        /// <summary>
        /// 更新使用期限
        /// </summary>
        UpdateExpireDate,
        /// <summary>
        /// 修改結帳倍數
        /// </summary>
        SetComboPackCount,
        /// <summary>
        /// QC確認
        /// </summary>
        QCcheck,
        /// <summary>
        /// 文案編輯
        /// </summary>
        CopyWriter,
        /// <summary>
        /// 本區
        /// </summary>
        ReadDept,
        /// <summary>
        /// 申請核准
        /// </summary>
        Completed,
        /// <summary>
        /// 申請變更
        /// </summary>
        Applied,
        /// <summary>
        /// 全區
        /// </summary>
        ReadAll,
        /// <summary>
        /// 再次上檔/快速上檔
        /// </summary>
        ReCreateProposal,
        /// <summary>
        /// 合約列印
        /// </summary>
        ContractPrint,
        /// <summary>
        /// 批次核准店鋪
        /// </summary>
        StoreApprove,
        /// <summary>
        /// 建檔
        /// </summary>
        BusinessCreated,
        /// <summary>
        /// 建檔確認
        /// </summary>
        Created,
        /// <summary>
        /// 頁面確認
        /// </summary>
        PageCheck,
        /// <summary>
        /// 附約已回
        /// </summary>
        PaperContractCheck,
        /// <summary>
        /// 財務資料
        /// </summary>
        FinanceCheck,
        /// <summary>
        /// 排檔編輯
        /// </summary>
        ScheduleCheck,
        /// <summary>
        /// 提案申請
        /// </summary>
        Apply,
        /// <summary>
        /// 搬移商家
        /// </summary>
        SellerMove,
        /// <summary>
        /// 電子合約
        /// </summary>
        EContractCheck,
        /// <summary>
        /// 圖片設計
        /// </summary>
        ImageDesign,
        /// <summary>
        /// 轉件
        /// </summary>
        Referral,
        /// <summary>
        /// 攝影確認
        /// </summary>
        PhotographerCheck,
        /// <summary>
        /// 送出排檔
        /// </summary>
        SettingCheck,
        /// <summary>
        /// 提案稽核
        /// </summary>
        ProposalAudit,
        /// <summary>
        /// 指派
        /// </summary>
        Assign,
        /// <summary>
        /// 檔次設定
        /// </summary>
        Setting,
        /// <summary>
        /// 大圖ART
        /// </summary>
        ART,
        /// <summary>
        /// 上架確認
        /// </summary>
        Listing,
        /// <summary>
        /// 財務變更
        /// </summary>
        FinanceApplied,
        /// <summary>
        /// 修改買斷票券設定
        /// </summary>
        SetBuyoutCoupon,
        /// <summary>
        /// 提案單退件
        /// </summary>
        ProposalReturned,
        /// <summary>
        /// 策展
        /// </summary>
        Curation,
        /// <summary>
        /// 修改檔次資料
        /// </summary>
        MultiDealsUpdate,
        /// <summary>
        /// 排檔收件人
        /// </summary>
        ScheduleCheckMail,
        /// <summary>
        /// 好康客服
        /// </summary>
        CCSPpon,
        /// <summary>
        /// 宅配客服
        /// </summary>
        CCSHouse,
        /// <summary>
        /// 商審
        /// </summary>
        ProductApprove,
        /// <summary>
        /// 更新全區
        /// </summary>
        UpdateAll,
        /// <summary>
        /// 更新本區
        /// </summary>
        UpdateDept,
        /// <summary>
        /// 合約寄出
        /// </summary>
        PaperContractSend,
        /// <summary>
        /// 圖檔確認
        /// </summary>
        PicCheck,
        /// <summary>
        /// 完全複製檔
        /// </summary>
        ReCreateSameProposal,
        /// <summary>
        /// 提案諮詢
        /// </summary>
        ProposalConsultation,
        /// <summary>
        /// 同意合約未回先上檔
        /// </summary>
        PaperContractNotCheck,
        /// <summary>
        /// 合約檢核
        /// </summary>
        PaperContractInspect,
        /// <summary>
        /// 轉介人員設定(新增)
        /// </summary>
        ReferralAdd,
        /// <summary>
        /// 轉介人員設定(修改)
        /// </summary>
        ReferralUpdate,
        /// <summary>
        /// 轉介人員HR信箱設定
        /// </summary>
        ReferralHRMail,
        /// <summary>
        /// 備註
        /// </summary>
        Memo,
        /// <summary>
        /// 攝影師
        /// </summary>
        Photographer,
        /// <summary>
        /// 通用券檔次設定異動(開檔後)
        /// </summary>
        SetIsRestrictedStore,
        /// <summary>
        /// 請求商家覆核
        /// </summary>
        ProposalSellerEdit,
        /// <summary>
        /// 跨區/組
        /// </summary>
        CrossDeptTeam,
        /// <summary>
        /// 更新跨區/組
        /// </summary>
        UpdateCrossDeptTeam,
        /// <summary>
        /// 商家系統:模擬商家帳號登入
        /// </summary>
        VendorAccountSimulate,
        /// <summary>
        /// 商家系統:內部人員模擬商家帳號登入
        /// </summary>
        VendorAccountSimulateByEmployee,
        /// <summary>
        /// 提案單搬移賣家
        /// </summary>
        ProposalChangeSeller,
        /// <summary>
        /// 商家查詢結果匯出/匯入(批次變更業務)
        /// </summary>
        PatchChangeSeller,
        /// <summary>
        /// 舊出帳設定
        /// </summary>
        RemittanceTypeSet,
        /// <summary>
        /// 防盜刷管理:可查詢該會員所有訂單
        /// </summary>
        SetCreditcardOrderGetAllOrders,
        /// <summary>
        /// 提前頁確
        /// </summary>
        EarlierPageCheck,
        /// <summary>
        /// 批次移轉提案單
        /// </summary>
        TransferProposal,
        /// <summary>
        /// 批次移轉檔次
        /// </summary>
        TransferDeal,
        /// <summary>
        /// 商品寄倉檔次設定異動(開檔後)
        /// </summary>
        SetIsConsignment,
        /// <summary>
        /// 更新對帳單應付金額
        /// </summary>
        UpdateBalanceSheetEstAmount,
        /// <summary>
        /// 代銷訂單取消退貨
        /// </summary>
        CancelAgentOrderRefund,
        /// <summary>
        /// 匯出財會報表
        /// </summary>
        ExportAccountingReport,
        /// <summary>
        /// 增補宅配提案單
        /// </summary>
        BatchCreateProposal,
        /// <summary>
        /// 排序、鎖定排序、隱藏 (台北)
        /// </summary>
        SortLockHideTaipei,
        /// <summary>
        /// 排序、鎖定排序、隱藏 (桃園) 
        /// </summary>
        SortLockHideTaoyuan,
        /// <summary>
        /// 排序、鎖定排序、隱藏 (竹苗) 
        /// </summary>
        SortLockHideHsinchu,
        /// <summary>
        /// 排序、鎖定排序、隱藏 (中彰)  
        /// </summary>
        SortLockHideTaichung,
        /// <summary>
        /// 排序、鎖定排序、隱藏 (嘉南)  
        /// </summary>
        SortLockHideTainan,
        /// <summary>
        /// 排序、鎖定排序、隱藏 (高屏)  
        /// </summary>
        SortLockHideKaohsiung,
        /// <summary>
        /// 排序、鎖定排序、隱藏 (旅遊) 
        /// </summary>
        SortLockHideTravel,
        /// <summary>
        /// 排序、鎖定排序、隱藏 (玩美、休閒) 
        /// </summary>
        SortLockHidePeauty,
        /// <summary>
        /// 排序、鎖定排序、隱藏 (宅配) 
        /// </summary>
        SortLockHideAllCountry,
        /// <summary>
        /// 排序、鎖定排序、隱藏 (全家) 
        /// </summary>
        SortLockHideFamily,
        /// <summary>
        /// 排序、鎖定排序、隱藏 (品生活) 
        /// </summary>
        SortLockHidePiinlife,
    }

    public enum CheckAppVersionReply
    {
        /// <summary>
        /// 無法識別，或發生異常
        /// </summary>
        Error = 0,
        /// <summary>
        /// 通過，不需更新
        /// </summary>
        Pass = 1,
        /// <summary>
        /// 建議更新
        /// </summary>
        RecommendedUpgrade = 2,
        /// <summary>
        /// 強制更新
        /// </summary>
        ForceUpgrade = 3,
        /// <summary>
        /// 告知即將不支援 OsRceommandVer 以下之 OS
        /// </summary>
        InformRecommendUpdate = 4,
        /// <summary>
        /// 引導用 web 版更新
        /// </summary>
        RedirectToWebUpdate = 5
    }

    /// <summary>
    /// OAuth Token使用權限
    /// </summary>
    public enum TokenScope
    {
        Undefined = 0,
        /// <summary>
        /// 對內網站功能
        /// </summary>
        SystemManagement = 100,
        /// <summary>
        /// 訪客API存取權限
        /// </summary>
        QuestAccess = 200,
        User = 1000 /*user*/,
        UserProfile = 1010,
        UserCash = 1020 /*user*/,
        UserPublish = 1030 /*user*/,
        Pay = 1040 /*user*/,
        VourcherPublish = 1050, /*user*/
        Deal = 2000, /*app*/
        Vourcher = 2010, /*app*/
        Verification = 2020, /*app*/
        
        /// <summary>
        /// app 基本參數、共用參數
        /// </summary>
        Publish = 2030,
        /// <summary>
        /// 雄獅旅行合作
        /// </summary>
        LionTravel = 2040,
        /// <summary>
        /// 新光三越 APP
        /// </summary>
        Skm = 2050,
        /// <summary>
        /// 新光三越 Umall專用API
        /// </summary>
        SkmUmall = 2051,
        /// <summary>
        /// 新光三越 資訊部
        /// </summary>
        SkmIt = 2052,
        /// <summary>
        /// POS 核銷串接
        /// </summary>
        PosVerify = 2060,
        /// <summary>
        /// 導購商
        /// </summary>
        ShoppingGuide = 2070,
        /// <summary>
        /// 生活優會中台
        /// </summary>
        Entrepot = 2080,
        /// <summary>
        /// app 產生超長時間的 access_token (10年) ，方便我們自行產生 token 給合作廠商，
        /// 他們不需要顧慮token過期，也不用先用 client_id, client_secret 取得 token，簡化他們合作的流程
        /// </summary>
        LongLived = 3000,
        /// <summary>
        /// 產生時效一天的 access_token
        /// </summary>
        OnedayLived = 3010,
        /// <summary>
        /// 產生時效七天的 access_token
        /// </summary>
        OneWeekLived = 3020,
        /// <summary>
        /// 重取token
        /// </summary>
        RefreshToken = 3100
    }

    public enum OAuthTargetType
    {
        User, Client
    }

    public enum KeywordStatus
    {
        /// <summary>
        /// 初始
        /// </summary>
        Initial,
        /// <summary>
        /// 關閉
        /// </summary>
        Disabled,
    }

    public enum GaViewIDMap
    {
        [Description("17LIFE")]
        Lunchking_17LIFE = 6098270,
        [Description("17LIFE APP")]
        Zoo_17LIFE_APP = 69858944
    }

    public enum GaDealType
    {
        Summary,
        ByPath,
        BySite
    }

    public enum GaDealListType
    {
        BySchedule,
        ByUser
    }

    /// <summary>
    /// Code Type
    /// </summary>
    public enum CodeType
    {
        Webform,
        MVC
    }

    /// <summary>
    /// 17Life QRCode 類型
    /// </summary>
    public enum QrCodeActionType
    {
        /// <summary>
        /// 未定義
        /// </summary>
        Undefined = 0,

        /// <summary>
        /// 核銷-分店QRCode
        /// </summary>
        VerifyStore = 1,
    }
}

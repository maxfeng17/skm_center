﻿using System.ComponentModel;


namespace LunchKingSite.Core
{
    public enum PurchaseOrderStatus
    {
        [Description("初始")]
        Initial = 0,
        [Description("17Life審核中")]
        Apply = 1,
        [Description("17Life審核未過")]
        Fail = 2,
        [Description("17Life審核通過")]
        Verify = 3,
        [Description("倉儲：申請中")]
        Submit = 4,
        [Description("倉儲：申請失敗")]
        WmsFail = 5,
        [Description("倉儲：到貨待驗")]
        WmsArrived = 20,
        [Description("倉儲：進倉入庫")]
        WmsInventory = 21,
        [Description("倉儲：單據取消")]
        WmsCancel = 22,

    }

    /// <summary>
    /// Wms訂單(pchome)傳送狀態
    /// </summary>
    public enum WmsOrderSendStatus
    {
        /// <summary>
        /// 初始，待傳送
        /// </summary>
        [Description("初始，待傳送")]
        Init = 0,
        /// <summary>
        /// 傳送成功
        /// </summary>
        [Description("傳送成功")]
        Success = 1,
        /// <summary>
        /// 傳送失敗
        /// </summary>
        [Description("傳送失敗")]
        failure = 2,
    }

    public enum WarehouseType
    {
        [Description("一般宅配 (自出含快出與超取)")]
        Normal = 0,
        [Description("24H到貨 (倉儲+物流)")]
        Wms = 1,
    }

    public enum WmsDeliveryStatus
    {
        /// <summary>
        /// 訂單處理中
        /// </summary>
        [Description("訂單處理中")]
        Processing = 0,
        /// <summary>
        /// 收到訂單 
        /// </summary>
        [Description("收到訂單")]
        GotOrder = 1,
        /// <summary>
        /// 撿貨 
        /// </summary>
        [Description("撿貨")]
        Picking = 2,
        /// <summary>
        /// 理貨
        /// </summary>
        [Description("理貨")]
        Arranging = 3,
        /// <summary>
        /// 出貨 
        /// </summary>
        [Description("出貨")]
        Shipping = 6,
        /// <summary>
        /// 送達未遇 
        /// </summary>
        [Description("送達未遇")]
        Missed = 11,
        /// <summary>
        /// 已送達 
        /// </summary>
        [Description("已送達")]
        Arrived = 12,
    }

    public enum WmsOrderShipStatus
    {
        /// <summary>
        /// 初始 
        /// </summary>
        [Description("訂單處理中")]
        Init = 0,
        /// <summary>
        /// 出貨處理中 
        /// </summary>
        [Description("出貨處理中")]
        Progressing = 1,
        /// <summary>
        /// 已出貨 
        /// </summary>
        [Description("已出貨")]
        Shipped = 8,
        /// <summary>
        /// 已取消出貨 
        /// </summary>
        [Description("已取消出貨")]
        Cancel = 10,
    }

    public enum WmsReturnSource
    {
        /// <summary>
        /// 17Life
        /// </summary>
        [Description("17Life")]
        E7Life = 1,
        /// <summary>
        /// PChome
        /// </summary>
        [Description("PChome")]
        PChome = 2,
    }


    public enum WmsReturnOrderStatus
    {
        /// <summary>
        /// 初始
        /// </summary>
        [Description("初始")]
        Initial = 0,
        /// <summary>
        /// 17Life審核中
        /// </summary>
        [Description("17Life審核中")]
        Apply = 1,
        /// <summary>
        /// 17Life審核未過
        /// </summary>
        [Description("17Life審核未過")]
        Fail = 2,
        /// <summary>
        /// 
        /// </summary>
        [Description("17Life審核通過")]
        Verify = 3,
        /// <summary>
        /// 倉儲：申請中
        /// </summary>
        [Description("倉儲：申請中")]
        Submit = 4,
        /// <summary>
        /// 倉儲：申請失敗
        /// </summary>
        [Description("倉儲：申請失敗")]
        WmsFail = 5,
        /// <summary>
        /// 倉儲：已放行
        /// </summary>
        [Description("倉儲：已放行")]
        WmsPass = 23,
        /// <summary>
        /// 倉儲：已出庫
        /// </summary>
        [Description("倉儲：已出庫")]
        WmsOutBound = 24,
        /// <summary>
        /// 倉儲：刪單
        /// </summary>
        [Description("倉儲：刪單")]
        WmsCancel = 25,
        /// <summary>
        /// 倉儲：已出貨
        /// </summary>
        [Description("倉儲：已出貨")]
        WmsShip = 26,
    }

    public enum WmsIsSelfPick
    {
        /// <summary>
        /// 否
        /// </summary>
        [Description("否")]
        No = 0,
        /// <summary>
        /// 是
        /// </summary>
        [Description("是")]
        Yes = 1,
    }

    public enum WmsInvalidation
    {
        /// <summary>
        /// 否(作廢)
        /// </summary>
        [Description("否(作廢)")]
        No = 0,
        /// <summary>
        /// 是(作廢)
        /// </summary>
        [Description("是(作廢)")]
        Yes = 1,
    }

    /// <summary>
    /// PCHOME退貨-退貨物品所在位置
    /// </summary>
    public enum WmsRefundFrom
    {
        /// <summary>
        /// 未知
        /// </summary>
        [Description("未知")]
        None = 0,
        /// <summary>
        /// Pchome
        /// </summary>
        [Description("Pchome")]
        Pchome = 1,
        /// <summary>
        /// 消費者
        /// </summary>
        [Description("消費者")]
        Customer = 2,
    }

    public enum WmsRefundStatus
    {
        /// <summary>
        /// 初始
        /// </summary>
        [Description("初始")]
        Initial = 0,
        /// <summary>
        /// 退貨成立
        /// </summary>
        [Description("退貨成立")]
        Examine = 1,
        /// <summary>
        /// 退貨處理中
        /// </summary>
        [Description("退貨處理中")]
        Processing = 2,
        /// <summary>
        /// 取件配送中
        /// </summary>
        [Description("取件配送中")]
        Delivering = 3,
        /// <summary>
        /// 退貨配達(已退回)
        /// </summary>
        [Description("退貨配達(已退回)")]
        Arrived = 4,
        /// <summary>
        /// 取不到貨
        /// </summary>
        [Description("取不到貨")]
        Fail = 5,
        /// <summary>
        /// 客人不退貨
        /// </summary>
        [Description("客人不退貨")]
        NotRefund = 6,
    }

    public enum WmsRefundProgressStatus
    {
        /// <summary>
        /// 未委派
        /// </summary>
        [Description("未委派")]
        NonCall = 0,
        /// <summary>
        /// 傳至物流商
        /// </summary>
        [Description("傳至物流商")]
        FtpSent = 1,
        /// <summary>
        /// 資料接收
        /// </summary>
        [Description("資料接收")]
        Received = 2,
        /// <summary>
        /// 已取貨
        /// </summary>
        [Description("已取貨")]
        HasPickup = 3,
        /// <summary>
        /// 已配達
        /// </summary>
        [Description("已配達")]
        Arrival = 4,
        /// <summary>
        /// 其他(在取)
        /// </summary>
        [Description("其他(在取)")]
        PickupErr = 5,
        /// <summary>
        /// 已取消
        /// </summary>
        [Description("已取消")]
        Canceled = 6,

    }
}

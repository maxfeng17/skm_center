﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using LunchKingSite.Core.Component;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.Core.Enumeration
{

    public enum MatchType
    {
        Email = 0,
        Mobile = 1,
        Fb = 2,
        Line=3
    }


    public enum GiftStatus
    {
        [Description("建立禮物成功")]
        Success = 0,
        [Description("沒有可用的憑證")]
        NoneCopon = 1,
        [Description("沒有取得發送名單")]
        NoneMember = 2,
        [Description("已超出目前憑證數量")]
        OverCoponCount = 3

    }

    public enum GiftMessageType
    {
        [Description("熟客卡訊息")]
        PCP = 0,
        [Description("寄送訊息")]
        Send = 1,
        [Description("回覆訊息")]
        Reply = 2,
        [Description("禮物連結")]
        GiftCode = 3
    }

    public enum ContactStatus
    {
        [Description("建立朋友成功")]
        Success = 0,
        [Description("建立朋友失敗")]
        Fail = 1,
    }

    public enum MgmAcceptGiftType
    {
        /// <summary>
        /// 已送出(未拒絕、未歸戶)
        /// </summary>
        [Description("此禮物正在送禮中")]
        Unknown = 0,
        /// <summary>
        /// 接受送禮
        /// </summary>
        [Description("此禮物已被接受")]
        Accept = 1,
        /// <summary>
        /// 拒絕送禮
        /// </summary>
        [Description("此禮物已被拒絕")]
        Reject = 2,
        /// <summary>
        /// 取消送禮
        /// </summary>
        [Description("此禮物已被取消")]
        Cancel = 3,
        /// <summary>
        /// 此禮物已不存在
        /// </summary>
        [Description("此禮物已不存在")]
        None =4,
        /// <summary>
        /// 禮物過期
        /// </summary>
        [Description("此禮物已過期")]
        Expired =5,
        /// <summary>
        /// 快速領取
        /// </summary>
        [Description("此禮物已被快速領取")]
        Quick = 6
    }

    /// <summary>
    /// 禮物區首頁上線狀態
    /// </summary>
    public enum MgmGiftHeaderOnline
    {
        [Description("關閉")]
        Disabled = 0,
        [Description("開啟")]
        Initial = 1
 
    }

    /// <summary>
    /// 禮物區首頁顯示狀態
    /// </summary>
    public enum MgmGiftHeaderShowType
    {
        [Description("無禮物")]
        None = 0,
        [Description("有禮物")]
        One = 1

    }
}


using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Runtime.Serialization;

namespace LunchKingSite.Core
{
    public enum LkSiteCookie
    {
        CityLevel,
        MemberExtension,
        MemberDept,
        ReferenceId,
        CityId,
        ReferrerSourceId,
        CpaKey,
        TravelCategoryId,
        CookieVersion,
        LoginSource,
        FemaleCategoryId,
        PiinLifeSubscriptionCookie,
        SubRegionCategroyId,
        isPageLoaded,
        IndependentWindowsCookie,
        MemberDiscount,
        ToMobile,
        iChannelGid,
        PayWay,
        VisitorIdentity,
        HomeCategoryId,
        AppOsVersion,
        ChannelAgent,
        shopBackGid,
        PezChannelGid,
        LineShopGid,
        AffiliatesGid,
    }

    /// <summary>
    /// 提供一個較安全(比較不會衝名)的session 讀取機制
    /// </summary>
    public class SessionHelper
    {
        private static string GetName(VbsSession sessionName)
        {
            return string.Format("Vbs:{0}", sessionName);
        }
        private static string GetName(LkSiteSession sessionName)
        {
            return string.Format("LkSite:{0}", sessionName);
        }
        private static string GetName(HiDealSession sessionName)
        {
            return string.Format("HiDeal:{0}", sessionName);
        }

        public static void Set(VbsSession vbsSession, object obj)
        {
            System.Web.HttpContext.Current.Session[GetName(vbsSession)] = obj;
        }
        public static void Set(LkSiteSession lksiteSession, object obj)
        {
            System.Web.HttpContext.Current.Session[GetName(lksiteSession)] = obj;
        }
        public static void Set(HiDealSession hidealSession, object obj)
        {
            System.Web.HttpContext.Current.Session[GetName(hidealSession)] = obj;
        }

        public static object Get(VbsSession vbsSession)
        {
            return System.Web.HttpContext.Current.Session[GetName(vbsSession)];
        }
        public static object Get(LkSiteSession lksiteSession, object obj)
        {
            return System.Web.HttpContext.Current.Session[GetName(lksiteSession)];
        }
        public static object Get(HiDealSession hidealSession, object obj)
        {
            return System.Web.HttpContext.Current.Session[GetName(hidealSession)];
        }

        public static void Remove(VbsSession vbsSession)
        {
            System.Web.HttpContext.Current.Session.Remove(GetName(vbsSession));
        }
        public static void Remove(LkSiteSession lksiteSession)
        {
            System.Web.HttpContext.Current.Session.Remove(GetName(lksiteSession));
        }
        public static void Remove(HiDealSession hidealSession)
        {
            System.Web.HttpContext.Current.Session.Remove(GetName(hidealSession));
        }
    }

    public enum HiDealSession
    {
        City,
        Categroy,
        CityId,
        CategoryId,
        SubscribeEmail,
        /// <summary>
        /// 品生活-NowUrl
        /// </summary>
        PiinLifeNowUrl,
        OrderGuid,
        OrderId,
        CaptchaPiinlifeRegister,
        /// <summary>
        /// 圖形驗證-品生活忘記密碼
        /// </summary>
        CaptchaPiinlifeForgetPassword,
        /// <summary>
        /// 圖形驗證-品生活客服
        /// </summary>
        CaptchaPiinlifeService,
        /// <summary>
        /// HiDeal付款業交換資料用
        /// </summary>
        HiDealPayViewSessionData,
        /// <summary>
        /// HiDeal退貨訂單資料
        /// </summary>
        HiDealReturnData,
        HiDealRegionId,
    }

    public class VbsSession
    {
        /// <summary>
        /// 核銷對帳系統-使用者帳號
        /// </summary>
        public const string UserId = "VbsSession.UserId";
        /// <summary>
        /// 核銷對帳系統-使用者帳號名稱
        /// </summary>
        public const string UserName = "VbsSession.UserName";
        /// <summary>
        /// 核銷對帳系統-使用者Id (member.unique_id)
        /// </summary>
        public const string UniqueId = "VbsSession.UniqueId";
        /// <summary>
        /// 核銷對帳系統-是否為17Life員工
        /// </summary>
        public const string Is17LifeEmployee = "VbsSession.Is17LifeEmployee";
        /// <summary>
        /// 核銷對帳系統-是否使用查帳碼
        /// </summary>
        public const string UseInspectionCode = "VbsSession.UseInspectionCode";
        /// <summary>
        /// 核銷對帳系統-查帳碼是否已授權
        /// </summary>
        public const string InspectionRight = "VbsSession.InspectionRight";
        /// <summary>
        /// 核銷對帳系統-是否變更過查帳碼
        /// </summary>
        public const string IsInspectionCodeNeverChange = "VbsSession.IsInspectionCodeNeverChange";
        /// <summary>
        /// 核銷對帳系統-是否第一次登入
        /// </summary>
        public const string FirstLogin = "VbsSession.FirstLogin";
        /// <summary>
        /// 核銷對帳系統-檔次數量
        /// </summary>
        public const string DealCount = "VbsSession.DealCount";
        /// <summary>
        /// 核銷對帳系統-分店數量
        /// </summary>
        public const string StoreCount = "VbsSession.StoreCount";
        /// <summary>
        /// 核銷對帳系統-前個頁面Action Name
        /// </summary>
        public const string ParentActionName = "VbsSession.ParentActionName";
        /// <summary>
        /// 核銷對帳系統-超過密碼變更限制次數
        /// </summary>
        public const string IsPwdPassTimeLimit = "VbsSession.IsPwdPassTimeLimit";
        /// <summary>
        /// 核銷對帳系統-帳號類型
        /// </summary>
        public const string VbsAccountType = "VbsSession.VbsAccountType";
        /// <summary>
        /// 核銷對帳系統-Demo User Info
        /// </summary>
        public const string DemoUser = "VbsSession.DemoUser";
        /// <summary>
        /// 核銷對帳系統-Demo Data Info
        /// </summary>
        public const string DemoData = "VbsSession.DemoData";
        /// <summary>
        /// 核銷對帳系統-帳號檢視工具列權限類別
        /// </summary>
        public const string ViewVbsRight = "VbsSession.ViewVbsRight";
        public const string LoginSessionCheck = "VbsSession.LoginSessionCheck";
        public const string IsReadInfoProtectNeeded = "VbsSession.IsReadInfoProtectNeeded";
        public const string IsAgreePersonalInfoClause = "VbsSession.IsAgreePersonalInfoClause";
        public const string BalanceDealInfos = "VbsSession.BalanceDealInfos";
        /// <summary>
        /// 出貨管理查詢紀錄
        /// </summary>
        public const string VbsShipDealInfos = "VbsSession.VbsShipDealInfos";
        /// <summary>
        /// 商家帳號的熟客相關功能PK
        /// </summary>
        public const string MembershipCardGroupId = "VbsSession.MembershipCardGroupId";
        /// <summary>
        /// 商家核銷App呼叫API時，用來識別的token (oauth_token)
        /// </summary>
        public const string SellerAppApiToken = "VbsSession.SellerAppApiToken";
        /// <summary>
        /// 商家帳號擁有的熟客系統相關權限
        /// </summary>
        public const string VbsMembershipPermission = "VbsSession.VbsMembershipPermission";
        /// <summary>
        /// 商家帳號擁有的角色
        /// </summary>
        public const string VbsRole = "VbsSession.VbsRole";
        /// <summary>
        /// 用臨時密碼登入-暫時登入
        /// </summary>
        public const string IsTemporaryLogin = "VbsSession.IsTemporaryLogin";
        /// <summary>
        /// 用臨時密碼登入-暫時登入時間
        /// </summary>
        public const string TemporaryLoginDate = "VbsSession.TemporaryLoginDate";
        /// <summary>
        /// 是否已綁定帳號登入
        /// </summary>
        public const string IsLoginBy17LifeMemberAccount = "VbsSession.IsLoginBy17LifeMemberAccount";
        /// <summary>
        /// 實際登入帳號
        /// </summary>
        public const string RealLoginAccount = "VbsSession.RealLoginAccount";
        /// <summary>
        /// Delivery Type
        /// </summary>
        public const string DeliveryType = "VbsSession.DeliveryType";
        /// <summary>
        /// 超商取貨
        /// </summary>
        public const string IsInstorePickup = "VbsSession.IsInstorePickup";
        /// <summary>
        /// 模擬商家權限
        /// </summary>
        public const string LoginBy17LifeSimulate = "LoginBy17LifeSimulate";
        /// <summary>
        /// 模擬商家17life身分
        /// </summary>
        public const string LoginBy17LifeSimulateUserName = "LoginBy17LifeSimulateUserName";

    }

    public enum LkSiteSession
    {
        MemberEmail,
        ExternalMemberId,
        TransId,
        Amount,
        NowUrl,
        NonLoginBuildSelect,
        IsWelfareMember,
        ShareLinkText,
        /// <summary>
        /// FB、Line Access Token
        /// </summary>
        AccessToken,
        //新版CPA
        NewCpa,
        //紀錄Rsrc
        Rsrc,
        /// <summary>
        /// 圖形驗證-註冊
        /// </summary>
        CaptchaRegister,
        /// <summary>
        /// 圖形驗證-忘記密碼
        /// </summary>
        CaptchaForgetPassword,
        /// <summary>
        /// MasterPage套版(ex: taishin)
        /// </summary>
        MasterPageType,
        /// <summary>
        /// Hami大團購
        /// </summary>
        Hami,
        /// <summary>
        /// oauth client id
        /// </summary>
        OAuthAppKey,
        /// <summary>
        /// 行銷滿額贈活動
        /// </summary>
        SpecialConsumingDiscount,
        /// <summary>
        /// ppon default頁面，篩選條件
        /// </summary>
        PponDefaultPageFilter,
        /// <summary>
        /// 圖形驗證-登入
        /// </summary>
        CaptchaLogin,
        /// <summary>
        /// 後台-客服整合系統-TAB分頁
        /// </summary>
        ServiceIntegrate,
        /// <summary>
        /// 付款頁參數
        /// </summary>
        Carrier,
        /// <summary>
        /// 搜尋
        /// </summary>
        Search,
        /// <summary>
        /// 綁定17Life會員
        /// </summary>
        Bind17UserId,
        UserId,
        /// <summary>
        /// 記錄client在buy頁動作的交易唯一識別碼
        /// </summary>
        BuyTransactionGuid,
        /// <summary>
        /// 記錄client在buy頁動作的付款方式
        /// </summary>
        BuyTransPaymentMethod,
        /// <summary>
        /// 記錄購買的OrderClassification
        /// </summary>
        PayType,
        /// <summary>
        /// 使用者的最近瀏覽記錄
        /// </summary>
        ViewedDeals,
        /// <summary>
        /// 雙11遊戲 round guid
        /// </summary>
        RoundGuid,
    }

    public class LkSiteContextItem
    {
        public const string _API_USER_ID = "ApiUserId";
        public const string BuyTransaction = "BuyTransaction";
        public const string _DEVICE_TYPE = "deviceType";
        public const string Bid = "Bid";
        public const string _OAUTH_TOKEN = "OAuthToken";
        public const string _ORDER_LOG_ID = "orderLogId";
    }

    public enum EIPSiteSession{
        MenuTree,
        CNName,
        EmployeeId
    }

    public class LkSiteCacheExpiredMinutes
    {
        /// <summary>
        /// 單一檔次的推薦檔次列表，暫存 10 分鍾
        /// </summary>
        public static int RecommendDeal = 10;
    }

    public enum MediaType
    {
        SellerPhotoOriginal,
        SellerPhotoSmall,
        SellerPhotoLarge,
        ItemPhotoOriginal,
        ItemPhotoSmall,
        ItemPhotoLarge,
        SellerVideo,
        CityPhoto,
        PponDealPhoto,
        /// <summary>
        /// 大圖輪播(複數)
        /// </summary>
        HiDealBigPhotos,
        /// <summary>
        /// 大圖(單一)
        /// </summary>
        HiDealPrimaryBigPhoto,
        /// <summary>
        /// 大圖(複數，輪播)
        /// </summary>
        HiDealSecondaryBigPhoto,
        /// <summary>
        /// 小圖(單一)
        /// </summary>
        HiDealPrimarySmallPhoto,
        /// <summary>
        /// Yahoo大團購用圖
        /// </summary>
        YahooBigImage,
        /// <summary>
        /// EventPromo用圖片
        /// </summary>
        EventPromo,
        /// <summary>
        /// APP顯示之商品主題活動行銷BANNER
        /// </summary>
        EventPromoAppBanner,
        /// <summary>
        /// APP顯示之商品主題活動，主題圖片
        /// </summary>
        EventPromoAppMainImage,
        /// <summary>
        /// 商品主題活動上傳行銷活動訊息活動LOGO
        /// </summary>
        DealPromoImage,
        /// <summary>
        /// PCP客制化圖片
        /// </summary>
        PCPImage,
        /// <summary>
        /// 策展列表活動圖片
        /// </summary>
        ThemeImage,
    }

    public enum UploadFileType
    {
        CityPhoto,
        SellerPhoto,
        DelicaciesSellerPhoto,
        ItemPhoto,
        DelicaciesItemPhoto,
        Video,
        Menu,
        PponEvent,
        /// <summary>
        /// 精選店家
        /// </summary>
        SelectedStore,
        /// <summary>
        /// HiDeal首頁大圖
        /// </summary>
        HiDealPrimaryBigPhoto,
        /// <summary>
        /// HiDeal首頁小圖
        /// </summary>
        HiDealPrimarySmallPhoto,
        /// <summary>
        /// HiDeal輪播圖
        /// </summary>
        HiDealSecondaryPhoto,
        /// <summary>
        /// PEZ行銷用圖
        /// </summary>
        PezPromoImage,
        /// <summary>
        /// Yahoo大團購用圖
        /// </summary>
        YahooBigImage,
        /// <summary>
        /// APP顯示之商品主題活動行銷BANNER
        /// </summary>
        EventPromoAppBanner,
        /// <summary>
        /// APP顯示之商品主題活動，主題圖片
        /// </summary>
        EventPromoAppMainImage,
        /// <summary>
        /// 商品主題活動, 行銷活動訊息活動LOGO
        /// </summary>
        DealPromoImage,
        /// <summary>
        /// 商品主題活動頁投票圖
        /// </summary>
        EventPromoItemPic,
        /// <summary>
        /// PCP客制化圖片
        /// </summary>
        PCPImage,
        /// <summary>
        /// 策展2.0, 行銷活動web圖
        /// </summary>
        BrandWebImage,
        /// <summary>
        /// 策展2.0, 行銷活動app圖
        /// </summary>
        BrandAppImage,
        /// <summary>
        /// 策展2.0, 折價券活動前台 Web Banner圖
        /// </summary>
        BrandDiscountBannerImage,
        /// <summary>
        /// 策展列表頁小圖
        /// </summary>
        EventListImage,
        /// <summary>
        /// 策展頻道頁小圖
        /// </summary>
        ChannelListImage,
        /// <summary>
        /// WEB策展中繼頁圖片
        /// </summary>
        WebRelayImage,
        /// <summary>
        /// M版策展中繼頁圖片
        /// </summary>
        MobileRelayImage,
        /// <summary>
        /// 策展2.0 FB分享預覽圖
        /// </summary>
        FbShareImage,
        /// <summary>
        /// 策展列表活動圖片
        /// </summary>
        ThemeImage,
    }


    public enum AuditType
    {
        Member = 0,
        Seller = 1,
        Order = 2,
        OrderDetail = 3,
        OrderStatusLog = 4,
        Payment = 5,
        CashTrustLog = 6,
        Store = 7,
        Role = 8,
        /// <summary>
        /// 紀錄權限頁面
        /// </summary>
        RolePage = 9,

        /// <summary>
        /// 退貨
        /// </summary>
        Refund = 11,
        /// <summary>
        /// 會員作廢
        /// </summary>
        InvalidMember = 12,
        DealAccounting = 13,
        Api = 14,
        Controlroom = 15,
        /// <summary>
        /// 凍結
        /// </summary>
        Freeze = 16,
        /// <summary>
        /// 異動group_order紀錄
        /// </summary>
        GroupOrder = 17,
        /// <summary>
        /// 更新deal_sales_info紀錄
        /// </summary>
        DealSalesInfo = 18,
        /// <summary>
        /// 取得或取消Pcash轉購物金
        /// </summary>
        PayeasyPcashAPI = 19,
        /// <summary>
        /// 24到貨倉儲訂單
        /// </summary>
        WmsOrder = 20
    }

    public enum ShowListMode
    {
        All,
        Inclusive,
        Exclusive
    }

    public class ExternalLoginInfo
    {
        public static ExternalLoginInfo Empty;

        static ExternalLoginInfo()
        {
            Empty = new ExternalLoginInfo(SingleSignOnSource.Undefined, "");
        }

        public SingleSignOnSource Source;
        public string ExternalId;

        public ExternalLoginInfo(SingleSignOnSource source, string externalId)
        {
            this.Source = source;
            this.ExternalId = externalId;
        }

        public static string SourceToString(SingleSignOnSource source)
        {
            if (source == SingleSignOnSource.ContactDigitalIntegration)
            {
                return "17Life";
            }
            if (source == SingleSignOnSource.Mobile17Life)
            {
                return "手機號碼";
            }
            if (source == SingleSignOnSource.Undefined)
            {
                return string.Empty;
            }
            return source.ToString();
        }
    }

    [Serializable]
    public class ExternalMemberInfo : ISerializable, IEnumerable
    {
        private OrderedDictionary _idDict;

        public int? UserId { get; set; }

        public ExternalMemberInfo(int userId)
            : this()
        {
            this.UserId = userId;
        }

        public ExternalMemberInfo()
        {
            _idDict = new OrderedDictionary();
        }


        protected ExternalMemberInfo(SerializationInfo info, StreamingContext context)
        {
            _idDict = (OrderedDictionary)info.GetValue("iddict", typeof(OrderedDictionary));
        }

        public int Count
        {
            get { return _idDict.Count; }
        }

        public ExternalLoginInfo this[int index]
        {
            get
            {
                SingleSignOnSource key = (from SingleSignOnSource k in _idDict.Keys where _idDict[k] == _idDict[index] select k).FirstOrDefault();
                return new ExternalLoginInfo(key, (string)_idDict[index]);
            }
            set { _idDict[value.Source] = value.ExternalId; }
        }

        public string this[SingleSignOnSource source]
        {
            get { return (string)_idDict[source]; }
            set { _idDict[source] = value; }
        }

        public bool Add(SingleSignOnSource source, string memberId)
        {
            _idDict.Add(source, memberId);
            return true;
        }

        public bool Remove(SingleSignOnSource source)
        {
            _idDict.Remove(source);
            return true;
        }

        public bool Remove(int index)
        {
            _idDict.RemoveAt(index);
            return true;
        }

        public override string ToString()
        {
            string output = string.Empty;
            foreach (DictionaryEntry entry in _idDict)
                output += entry.Key + ", " + entry.Value + "\n";
            return output;
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("iddict", _idDict, typeof(OrderedDictionary));
        }

        public IEnumerator GetEnumerator()
        {
            return _idDict.GetEnumerator();
        }
    }

    public class WebStorage
    {
        public const string _IMAGE_TEMP= "ImageTemp";
    }
}

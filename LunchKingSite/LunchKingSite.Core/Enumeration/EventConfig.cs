﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace LunchKingSite.Core
{
    /// <summary>
    /// 商品主題活動頁(類別/子類別欄位)
    /// </summary>
    public enum EventPromoGroupByColumn
    {
        index,
        Category,
        SubCategory,
    }

    public enum GreetingCardSendStatus 
    {
        NotSendOut,
        Sending,
        SendOut,
    }

    /// <summary>
    /// SoloEDM 狀態
    /// </summary>
    public enum SoloEdmType
    {
        Default,
        Close,
    }

    public enum PrizeDrawRecordStatus
    {
        /// <summary>
        /// 起始
        /// </summary>
        Init = 0,
        /// <summary>
        /// 進行抽獎
        /// </summary>
        PrizeDrawing = 1,
        /// <summary>
        /// 抽獎燒點完成
        /// </summary>
        PrizeDrawCompleted = 2,
        /// <summary>
        /// 失敗
        /// </summary>
        Fail = 3,
        /// <summary>
        /// 獎項配發
        /// </summary>
        MakeOrderCompleted = 4
    }

    public enum SelectionDealStateType
    {
        [Description("預備中")]
        Preparing,
        [Description("上架銷售")]
        Online,
        [Description("已結檔")]
        Offline,
        [Description("暫停銷售")]
        Suspend
    }
}

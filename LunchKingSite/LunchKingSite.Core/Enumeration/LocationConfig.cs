﻿
using LunchKingSite.Core.Component;

namespace LunchKingSite.Core
{
    public enum CityStatus
    {
        Nothing = 0,
        Virtual = 1,
    }

    public enum AppendMode
    {
        None,
        All,
        DistrictOnly,
    }

    public enum ZoneType
    {
        Unknown,
        TopCity,
        TopCompany,
        District,
        Company,
    };

    public enum CityGroupType
    {
        /// <summary>
        /// 訂閱edm對應城市
        /// </summary>
        Subscription = 0,
        /// <summary>
        /// 優惠券城市對應分店地址城市
        /// </summary>
        Vourcher = 1,
    };

    public enum ChannelCategory
    {
        /// <summary>
        /// 美食
        /// </summary>
        Food = 87,
        /// <summary>
        /// 宅配
        /// </summary>
        Delivery = 88,
        /// <summary>
        /// 玩美‧休閒
        /// </summary>
        Beauty = 89,
        /// <summary>
        /// 旅遊
        /// </summary>
        Travel = 90,
        /// <summary>
        /// 全家專區
        /// </summary>
        Family = 91,
        /// <summary>
        /// 福利網
        /// </summary>
        Bonus = 92,
        /// <summary>
        /// 即買即用
        /// </summary>
        Immediately = 100001,


    }

}

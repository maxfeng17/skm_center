﻿using System.ComponentModel;

namespace LunchKingSite.Core.Enumeration
{
    /// <summary>
    /// Android Os 版本
    /// https://developer.android.com/guide/topics/manifest/uses-sdk-element.html#uses
    /// </summary>
    public enum AppOsVersion
    {
        None = 0,
        [Description("Android 1.0")]
        Android10 = 1,
        [Description("Android 1.1")]
        Android11 = 2,
        [Description("Android 1.5")]
        Android15 = 3,
        [Description("Android 1.6")]
        Android16 = 4,
        [Description("Android 2.0")]
        Android20 = 5,
        [Description("Android 2.0.1")]
        Android201 = 6,
        [Description("Android 2.1.x")]
        Android21X = 7,
        [Description("Android 2.2.x")]
        Android22X = 8,
        [Description("Android 2.3, 2.3.1, 2.3.2")]
        Android23 = 9,
        [Description("Android 2.3.3, 2.3.4")]
        Android233 = 10,
        [Description("Android 3.0.x")]
        Android30X = 11,
        [Description("Android 3.1.x")]
        Android31X = 12,
        [Description("Android 3.2")]
        Android32 = 13,
        [Description("Android 4.0, 4.0.1, 4.0.2")]
        Android40 = 14,
        [Description("Android 4.0.3, 4.0.4")]
        Android403 = 15,
        [Description("Android 4.1, 4.1.1")]
        Android41 = 16,
        [Description("Android 4.2, 4.2.2")]
        Android42 = 17,
        [Description("Android 4.3")]
        Android43 = 18,
        [Description("Android 4.4")]
        Android44 = 19,
        [Description("Android 4.4W")]
        Android44W = 20,
        [Description("Android 5.0")]
        Android50 = 21,
        [Description("Android 5.1")]
        Android51 = 22,
        [Description("Android 6.0")]
        Android60 = 23,
        [Description("Android 7.0")]
        Android70 = 24,
        [Description("Android 7.1")]
        Android71 = 25,
    }
}

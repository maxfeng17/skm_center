﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.Core.Enumeration
{
    public enum QuestionEventLoginMode
    {
        /// <summary>
        /// 匿名
        /// </summary>
        Guest = 0,
        /// <summary>
        /// 會員
        /// </summary>
        MemberOnly = 1
    }

    public enum QuestionType
    {
        /// <summary>
        /// 意見回覆型
        /// </summary>
        Default = 0,
        /// <summary>
        /// 單選
        /// </summary>
        Radio = 1,
        /// <summary>
        /// 多選
        /// </summary>
        Multiple = 2
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.Core.Enumeration
{
    public enum SkmRefreshDealSalesInfoMode
    {
        SkmTempTable = 1,
        SkmStoredProcedure = 2,
        PayEventsOnRefresh = 3
    }

    public enum SkmCashTrustLogNotVerifiedType
    {
        PreOrder = 1,
        ExpiringOrder = 2
    }

    public enum SkmPushRequestProgramType
    {
        [Description("")]
        Words = 1,
        [Description("ShowDealDetail")]
        ShowDealDetail = 2,
        [Description("FullscreenWebView")]
        FullscreenWebView = 3,
        [Description("Deeplink")]
        DeepLink = 4
    }

}

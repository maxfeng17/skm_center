﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.Core
{
    /// <summary>
    /// 代銷通路
    /// </summary>
    public enum AgentChannel
    {
        /// <summary>
        /// 預設沒意義
        /// </summary>
        [Description("")]
        NONE = 0,
        /// <summary>
        /// 雄獅
        /// </summary>
        [Description("雄獅")]
        LionTravelOrder = 7,
        /// <summary>
        /// Payeasy
        /// </summary>
        [Description("PayEasy")]
        PayEasyOrder = 9,
        /// <summary>
        /// 消創會
        /// </summary>
        [Description("消創會")]
        BuyerToBoss = 10,
        /// <summary>
        /// 易遊網
        /// </summary>
        [Description("易遊網")]
        EzTravel = 12,
        /// <summary>
        /// 寶獅
        /// </summary> 
        [Description("寶獅")]
        LionItri = 13,
        /// <summary>
        /// ASUS
        /// </summary>
        [Description("華碩")]
        ASUS = 14,
        /// <summary>
        /// 台新商城
        /// </summary>
        [Description("台新商城")]
        TaiShinMall = 15,
        /// <summary>
        /// C-Channel
        /// </summary>
        [Description("C_Channel")]
        CChannel = 16,
        /// <summary>
        /// PChome
        /// </summary>
        [Description("PChome")]
        PChome = 17,
        /// <summary>
        /// 台新生活中台
        /// </summary>
        [Description("LifeEntrepot")]
        LifeEntrepot = 18,
    }

    /// <summary>
    /// 代銷通路代理模式
    /// </summary>
    public enum DealAgentType
    {
        Default = 0
    }

    /// <summary>
    /// 特殊導購商
    /// </summary>
    public enum ChannelSource
    {
        Default = 0,
        iChannel = 1,
        ShopBack = 2,
        Payeasy = 3,
        Line = 4,
        Affiliates = 5,
    }

    public enum CommissionListType
    {
        /// <summary>
        /// 黑名單
        /// </summary>
        BlackList = 1,
        /// <summary>
        /// 白名單
        /// </summary>
        WhiteList = 2,
    }

    public enum CommissionRuleType
    {
        /// <summary>
        /// 憑證預設
        /// </summary>
        CouponDefault = 0,
        /// <summary>
        /// 宅配預設
        /// </summary>
        DeliveryDefault = 1,
        /// <summary>
        /// 業績歸屬分類 & 關鍵字
        /// </summary>
        NormalCondition = 2,
    }


    public enum ChannelOrderCheck
    {
        /// <summary>
        /// 預設,尚未確認
        /// </summary>
        Init = 0,
        /// <summary>
        /// 確認通路商有訂單
        /// </summary>
        Success = 1,
        /// <summary>
        /// 通路商找無訂單
        /// </summary>
        NotFound = 2,
        /// <summary>
        /// 訂單購買失敗,無須再檢查
        /// </summary>
        OrderNotComplete = 3,
    }

    public enum NotifyChannel
    {
        /// <summary>
        /// 預設,尚未確認
        /// </summary>
        Init = 0,
        /// <summary>
        /// 已執行通路商退貨
        /// </summary>
        Refund = 1,
        /// <summary>
        /// 通路商已自行取消
        /// </summary>
        Cancel = 2,
    }

    public enum PChomeIsShelf
    {
        /// <summary>
        /// 待上架
        /// </summary>
        Init = -1,
        /// <summary>
        /// 上架
        /// </summary>
        Online = 1,
        /// <summary>
        /// 下架
        /// </summary>
        Offline = 0,
    }

    public enum PChomeVoucherStatus
    {
        None = 0,
        Used = 1,
        Expired = 2,
        Refund = 3,
    }

    public enum PChomeRefundStatus
    {
        None = 0,
        RefundProcessing = 1,
        RefundSuccess = 2,
        RefundFail = 3,
        RefundCancel = 4
    }

    public enum PChomeRefund
    {
        Success = 1,
        Processing = 2
    }

    public enum NotifyVoucherStatus
    {
        None = 0,
        Verifing = 1,
        UndoVerified = 2
    }

    public enum ChannelCouponType 
    {
        [Description("預設一般憑證")]
        Default = 0,
        [Description("一般成套票券")]
        GroupCoupon = 1,
        /// <summary>
        /// 廠商自訂序號之成套票券 (ex:丹堤)
        /// </summary>
        [Description("廠商自訂序號成套票券")]
        PezGroupCoupon = 2,
        /// <summary>
        /// 全家寄杯(客製化)
        /// </summary>
        [Description("全家寄杯")]
        FamiGroupCoupon = 3,
        /// <summary>
        /// 萊爾富寄杯(客製化)
        /// </summary>
        [Description("萊爾富寄杯")]
        HiLifeGroupCoupon = 4
    }
}

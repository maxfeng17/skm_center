﻿using System.ComponentModel;

namespace LunchKingSite.Core
{
    public enum WareHouseType
    {
        [Description("一般宅配")]
        NormalDelivery = 0,
        [Description("Pchome24")]
        Pchome24 = 1,
    }

}

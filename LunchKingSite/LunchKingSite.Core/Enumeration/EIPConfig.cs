﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace LunchKingSite.Core.Enumeration
{
    /// <summary>
    /// Table: document_list
    /// </summary>
    public enum DocumentListStatus : byte
    {
        /// <summary>
        /// 正常
        /// </summary>
        Normal = 0,
        /// <summary>
        /// 刪除
        /// </summary>
        Delete = 1
    }

    public enum NetStatus : byte
    {
        Intranet = 0,
        Internet = 1
    }

    public enum EIPTaskStatus : byte
    {
        /// <summary>
        /// 申請
        /// </summary>
        [Description("申請")]
        Apply = 0,       
        /// <summary>
        /// 審核中
        /// </summary>       
        [Description("審核中")]
        Running = 1,        
        /// <summary>
        /// 退件
        /// </summary>
        [Description("退件")]
        Deny = 2,        
        /// <summary>
        /// 通知
        /// </summary>
        [Description("通知")]
        Notice = 3,        
        /// <summary>
        /// 完成
        /// </summary>
        [Description("完成")]
        Complete = 10,        
        /// <summary>
        /// 抽單
        /// </summary>
        [Description("抽單")]
        Cancel = 98,        
        /// <summary>
        /// 作廢
        /// </summary>
        [Description("作廢")]
        Dead = 99
    }

    public enum EIPTaskCategory
    {
        [Description("EIP權限申請")]
        SystemFunctionApply = 1
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.Core.Enumeration
{
    /// <summary>
    /// 捷運路網分類
    /// </summary>
    public enum MRTType
    {
        //None ,不使用
        None = 0 ,
        //台北捷運
        TaipeiMRT = 1,
        //高雄捷運
        KaohsiungMRT = 2,
    }

    /// <summary>
    /// 捷運路線分類
    /// </summary>
    public enum MRTLineType
    {
        //None ,不使用
        None = 0,
        //台北捷運-文湖線
        WenhuLine = 1,
        //台北捷運-淡水信義線
        TamshiXinyiLine = 2,
        //台北捷運-松山新店線
        SongShanXindianLine = 3,
        //台北捷運-中和新蘆線
        ZhongheXinluLine = 4,
        //台北捷運-板南線
        BannanLine = 5,
        //高雄捷運-紅線
        RedLine = 6,
        //高雄捷運-橘線
        OrangeLine =7,
    }

    /// <summary>
    /// 捷運附近店家關聯產生方式
    /// </summary>
    public enum MrtReleaseshipStoreSettingType
    {
        //系統產生
        System = 0 ,
        //使用者自訂
        User =1,
        //新光相關
        Skm = 2,
    }
}

﻿using System;
using System.ComponentModel;
using LunchKingSite.Core.Component;

namespace LunchKingSite.Core
{
    public enum AccountsPayableFormula
    {
        [Localization("AP_Formula_Default")]
        Default = 0,    //進貨價*份數+運費
        [Localization("AP_Formula_Payeasy")]
        Payeasy = 1,    //進貨價*份數+運費*0.8
    }

    public enum ReferrerActionType
    {
        /// <summary>
        /// 購買
        /// </summary>
        MakeOrder = 0,
        /// <summary>
        /// 註冊
        /// </summary>
        UserRegistration = 1,
        /// <summary>
        /// 訂閱
        /// </summary>
        UserSubscription = 2,
    }

    public enum AccountingFlag
    {
        /// <summary>
        /// 核銷中
        /// </summary>
        [Localization("DealVerifying")]
        Verifying = 0,

        /// <summary>
        /// 核銷鎖定
        /// </summary>
        [Localization("DealVerificationLocked")]
        VerificationLocked = 1,

        /// <summary>
        /// 核銷完成
        /// </summary>
        [Localization("DealVerificationFinished")]
        VerificationFinished = 2,

        /// <summary>
        /// 核銷結案
        /// </summary>
        [Localization("DealVerificationClosed")]
        VerificationClosed = 4,

        /// <summary>
        /// 清冊遺失
        /// </summary>
        [Localization("DealVerificationLost")]
        VerificationLost = 8,

        /// <summary>
        /// 已付首期款
        /// </summary>
        [Localization("DealDownPaymentPaid")]
        DownPaymentPaid = 16,

        /// <summary>
        /// 整檔請款凍結 無法請款
        /// </summary>
        Freeze = 32,
    }

    /// <summary>
    /// 會計的應收應付來源
    /// </summary>
    public enum AccountsClassification
    {
        /// <summary>
        /// 其他應收款需求
        /// </summary>
        Other = 0,
        /// <summary>
        /// 午餐王與P好康訂單
        /// </summary>
        LunchKingOrder = 1,
        /// <summary>
        /// 午餐王與P好康退貨單
        /// </summary>
        LunchKingReturn = 2,
        /// <summary>
        /// HiDeal訂單
        /// </summary>
        HiDealOrder = 3,
        /// <summary>
        /// HiDeal退貨單
        /// </summary>
        HiDealReturn = 4,
    }

    public enum ReceivableStatus
    {
        /// <summary>
        /// 未收款
        /// </summary>
        NotReceived = 1,
        /// <summary>
        /// 部分收款
        /// </summary>
        PartialReceived = 2,
        /// <summary>
        /// 收款完成
        /// </summary>
        CompleteReceived = 3,
    }

    public enum PayableStatus
    {
        /// <summary>
        /// 未付款
        /// </summary>
        NotPaid = 1,
        /// <summary>
        /// 部分付款
        /// </summary>
        PartialPaid = 2,
        /// <summary>
        /// 付款完成
        /// </summary>
        CompletePaid = 3,
    }

    public enum BusinessModel
    {
        Ppon = 0,
        PiinLife = 1,
    }

    public enum RemittanceType
    {
        /// <summary>
        /// 其他付款方式
        /// </summary>
        [Localization("RemittanceOthers")] 
        Others = 0,

        /// <summary>
        /// ACH每周付款
        /// </summary>
        [Localization("RemittanceAchWeekly")] 
        AchWeekly = 1,

        /// <summary>
        /// ACH每月付款
        /// </summary>
        [Localization("RemittanceAchMonthly")] 
        AchMonthly = 2,

        /// <summary>
        /// 部分預付
        /// </summary>
        [Localization("RemittanceManualPartially")]
        ManualPartially = 3,

        /// <summary>
        /// 人工每周付款
        /// </summary>
        [Localization("RemittanceManualWeekly")]
        ManualWeekly = 4,

        /// <summary>
        /// 人工每月付款
        /// </summary>
        [Localization("RemittanceManualMonthly")]
        ManualMonthly = 5,

        /// <summary>
        /// 彈性請款(人工付款):先收單據後付款
        /// </summary>
        [Localization("RemittanceFlexible")]
        Flexible = 6,

        /// <summary>
        /// 每週付款:先收單據後付款
        /// </summary>
        [Localization("RemittanceWeekly")]
        Weekly = 7,

        /// <summary>
        /// 每月付款:原來月結為先收單據後付款
        /// 因應新舊付款流程並行 只得增加月結新定義 
        /// </summary>
        [Localization("RemittanceMonthly")]
        Monthly = 8,

        /// <summary>
        /// 雙周付款
        /// 原每周的都改為雙周 
        /// </summary>
        [Localization("RemittanceFortnightly")]
        Fortnightly = 9,

    }

	/// <summary>
	/// 付款頻率
	/// </summary>
	public enum RemittanceFrequency
	{
		Unknown = 0,

		/// <summary>
		/// 每周付款
		/// </summary>
		Weekly = 1,

		/// <summary>
		/// 每月付款
		/// </summary>
        Monthly = 2,

        /// <summary>
        /// 一次性付款 或 預付
        /// 待預付需另產對帳單時 再另定義預付enum Partially
        /// </summary>
        LumpSum = 3,

        /// <summary>
        /// 彈性選擇請款時間
        /// </summary>
        Flexible = 4,
        /// <summary>
		/// 雙周付款
		/// </summary>
		Fortnightly = 5,

    }

    public enum VendorBillingModel
    {
        /// <summary>
        /// 會計以人工方式用報表處理.
        /// </summary>
        None,

        /// <summary>
        /// 對帳單模式
        /// </summary>
        BalanceSheetSystem,
     
      
        /// <summary>
        /// 以墨攻系統的資料為準
        /// </summary>
        MohistSystem
    }

    public enum BalanceSheetType
    {
        Unknown = 0,

        #region 憑證檔次
        
        /// <summary>
        /// 週結週對帳單 (ACH)
        /// </summary>
        WeeklyPayWeekBalanceSheet = 1,
        /// <summary>
        /// 週結月對帳單 (ACH)
        /// </summary>
        WeeklyPayMonthBalanceSheet = 2,
        /// <summary>
        /// 月結月對帳單 (ACH)
        /// </summary>
        MonthlyPayBalanceSheet = 3,
        /// <summary>
        /// 人為觸發的對帳單
        /// </summary>
        ManualTriggered = 4,
        /// <summary>
        /// 週結週對帳單 (手動匯款)
        /// </summary>
        ManualWeeklyPayWeekBalanceSheet = 5,
        /// <summary>
        /// 週結月對帳單 (手動匯款)
        /// </summary>
        ManualWeeklyPayMonthBalanceSheet = 6,
        /// <summary>
        /// 月結月對帳單 (手動匯款)
        /// </summary>
        ManualMonthlyPayBalanceSheet = 7,

        #endregion 憑證檔次

        /// <summary>
        /// 宅配檔次:一次性結算付款對帳單(手動匯款)
        /// 付款方式雖分 暫付七成後付尾款 及 一次全付 兩種
        /// 但一個檔次一律只產生一張對帳單(於尾款付清後)
        /// </summary>
        ManualLumpSumPayBalanceSheet = 8,

        /// <summary>
        /// 先收單據後付款:調整檔次設定出帳方式定義
        /// 由ACH或網銀匯款由財務匯款時決定(預設為ACH) 
        /// 取消綁定出帳方式與匯款方式之關聯 
        /// </summary>
        #region 先收單據後付款 
        
        #region 宅配檔次
        /// <summary>
        /// 彈性出帳宅配對帳單 
        /// </summary>
        FlexibleToHouseBalanceSheet = 9,
        /// <summary>
        /// 週結宅配對帳單
        /// </summary>
        WeeklyToHouseBalanceSheet = 10,
        /// <summary>
        /// 月結宅配對帳單
        /// </summary>
        MonthlyToHouseBalanceSheet = 11,

        #endregion 宅配檔次

        #region 憑證檔次

        /// <summary>
        /// 彈性請款對帳單:先收單據後付款
        /// </summary>
        FlexiblePayBalanceSheet = 12,        
        /// <summary>
        /// 月結對帳單:目前月結即為先收單據後付款
        /// 沿用目前ACH月結對帳單定義
        /// MonthlyPayBalanceSheet = 3
        /// </summary>
        /// <summary>
        /// 週結對帳單:先收單據後付款
        /// </summary>
        WeeklyPayBalanceSheet = 13,
        /// <summary>
        /// 雙周結宅配對帳單
        /// </summary>
        FortnightlyToHouseBalanceSheet = 14,

        #endregion 憑證檔次

        #endregion 先收單據後付款

    }

    public enum BalanceSheetGenerationFrequency
    {
        Unknown = 0,

        #region 憑證檔次

        /// <summary>
        /// 週對帳單
        /// </summary>
        WeekBalanceSheet = 1,

        /// <summary>
        /// 月對帳單
        /// </summary>
        MonthBalanceSheet = 2,

        /// <summary>
        /// 人工對帳單
        /// </summary>
        ManualTriggeredBalanceSheet = 3,

        #endregion 憑證檔次

        #region 宅配檔次

        /// <summary>
        /// 一次性(結算付款)對帳單
        /// 付款方式雖分 暫付七成後付尾款 及 一次全付 兩種
        /// 但一個檔次一律只產生一張對帳單(於尾款付清後)
        /// </summary>
        LumpSumBalanceSheet = 4,

        #endregion 宅配檔次

        /// <summary>
        /// 彈性選擇出帳對帳單
        /// </summary>
        FlexibleBalanceSheet = 5,
        /// <summary>
        /// 雙週對帳單
        /// </summary>
        FortnightlyBalanceSheet = 6,

    }

    /// <summary>
    /// [balance_sheet_detail].[status]
    /// </summary>
    public enum BalanceSheetDetailStatus
    {
        Abnormal = 0,

        /// <summary>
        /// 要付款的
        /// </summary>
        Normal = 1,

        /// <summary>
        /// 標註反核銷, 已付過款項但在未來的對帳單內要被扣除
        /// </summary>
        Undo = 2,

        /// <summary>
        /// 反核銷造成的扣除款項
        /// </summary>
        Deduction = 3,

        /// <summary>
        /// 在上架費份數內的無須付款
        /// </summary>
        NoPay = 4,

        /// <summary>
        /// 在上架費份數內的反核銷, 不用扣除
        /// </summary>
        UndoNoPay = 5
    }

    /// <summary>
    /// [balance_modification_log].[log_status]
    /// </summary>
    public enum BalanceModificationLogStatus
    {
        Unknown = 0,
        /// <summary>
        /// 反核銷
        /// </summary>
        Undo = 1,
        /// <summary>
        /// 反核銷, 因尚未付款, 從對帳單刪除
        /// </summary>
        UndoDelete = 2,
        /// <summary>
        /// 加入對帳單內一起付款
        /// </summary>
        AdditionalPay = 3,
        /// <summary>
        /// 加入對帳單內一起扣款
        /// </summary>
        AdditionalDeduction = 4,
        /// <summary>
        /// 取消退款, 因尚未付款, 從對帳單回復付款狀態(取消欲扣款狀態)
        /// </summary>
        UndoRecover = 5,
        /// <summary>
        /// 上架費份數異動:付款份數調整為上架費份數
        /// </summary>
        NormalToNoPay = 6,
        /// <summary>
        /// 上架費份數異動:上架費份數調整為付款份數
        /// </summary>
        NoPayToNormal = 7
    }

    public enum BalanceSheetGenerationResultStatus
    {
        Unknown,
        Success,
        BalanceSheetDataError,
        Duplication,
        Exception,
        PreconditionCheckFailed,
        StablizingCheckFailed,
		OutOfVerificationTimeRange,
        ItemsCheckFailed,
        StopPayment
    }

    /// <summary>
    /// 單據開立方式
    /// </summary>
    public enum VendorReceiptType
    {
        /// <summary>
        /// 其他
        /// </summary>
        [Localization("Other")]
        Other = 0,

        /// <summary>
        /// 免用統一發票 (開收據)
        /// </summary>
        [Localization("NoInvoiceReceipt")]
        Receipt = 1,

        /// <summary>
        /// 統一發票 (開發票)
        /// </summary> 
        [Localization("Invoice")]
        Invoice = 2,

        /// <summary>
        /// 免稅統一發票
        /// </summary> 
        [Localization("NoTaxInvoice")]
        NoTaxInvoice = 3,
    }

    public enum DealAccountingPayType
    {
        /// <summary>
        ///  匯至各分店匯款資料
        /// </summary>
        [Localization("DealAccountingPayToStore")]
        PayToStore = 0,
        /// <summary>
        /// 統一匯至賣家匯款資料
        /// </summary>
        [Localization("DealAccountingPayToSeller")]
        PayToCompany = 1,
    }

    /// <summary>
    /// 單據(發票或收據)買受人
    /// </summary>
    public enum BillBuyerType
    {
        /// <summary>
        ///  未知
        /// </summary>
        None = 0,
        /// <summary>
        ///  康迅(70553216)
        /// </summary>
        [Localization("PayEasy")]
        PayEasy = 1,
        /// <summary>
        ///  康太(24317014)
        /// </summary>
        [Localization("17Life")]
        Contact = 2,
        /// <summary>
        ///  康迅旅遊(27734293)
        /// </summary>
        [Localization("PayEasyTravel")]
        PayEasyTravel = 3,
        /// <summary>
        ///  康朋(24949807)
        /// </summary>
        [Localization("17LifeTravel")]
        ContactTravel = 4,
    }

    /// <summary>
    /// 對帳單明細成立扣款項目類型(原因)
    /// (更新對帳單明細Undo狀態時判別原因時使用)
    /// </summary>
    public enum UndoType
    {        
        /// <summary>
        ///  憑證訂單:取消核銷
        /// </summary>
        UnVerify,
        /// <summary>
        ///  宅配訂單:商品退貨
        /// </summary>
        Return,
    }

    /// <summary>
    /// 商品付款方式
    /// </summary>
    public enum LimitBankIdModel
    {
        /// <summary>
        /// 無限制(全部付款方式皆可使用)
        /// </summary>
        None=0,

        /// <summary>
        /// 限定台新銀行信用卡
        /// </summary>
        [Description("台新銀行信用卡")]
        TaiShinBank =1
    }

    /// <summary>
    /// 商家金額異動類型
    /// </summary>
    public enum VendorFineType
    {
        /// <summary>
        /// 異動金額
        /// </summary>
        [Description("異動金額")]
        Change = 1,
        /// <summary>
        /// 逾期出貨
        /// </summary>
        [Description("逾期出貨")]
        Overdue = 2
    }

    /// <summary>
    /// 對帳單類型
    /// </summary>
    public enum BalanceSheetUseType
    {
        /// <summary>
        /// 檔次對帳單
        /// </summary>
        [Description("檔次")]
        Deal = 1,
        /// <summary>
        /// 倉儲對帳單
        /// </summary>
        [Description("倉儲")]
        Wms = 2
    }
}

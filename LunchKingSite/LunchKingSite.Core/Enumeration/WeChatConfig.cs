﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.Core.Enumeration
{
    /// <summary>
    /// WeChat回傳或發送訊息類別
    /// </summary>
    public enum WeChatMsgType
    {
        /// <summary>
        /// 文本消息
        /// </summary>
        text,
        /// <summary>
        /// 圖片消息
        /// </summary>
        image,
        /// <summary>
        /// 語音消息
        /// </summary>
        voice,
        /// <summary>
        /// 視頻消息
        /// </summary>
        video,
        /// <summary>
        /// 連結消息
        /// </summary>
        link,
        /// <summary>
        /// 事件
        /// </summary>
        @event,
        /// <summary>
        /// 圖文消息
        /// </summary>
        news,
        /// <summary>
        /// 音樂消息
        /// </summary>
        music,
        /// <summary>
        /// 地理位置消息
        /// </summary>
        location
    }

    public enum WeChatEventType
    {
        /// <summary>
        /// 訂閱
        /// </summary>
        subscribe,
        /// <summary>
        /// 取消訂閱
        /// </summary>
        unsubscribe,
        /// <summary>
        /// 使用者已訂閱，重複掃描訂閱bar code
        /// </summary>
        scan,
        /// <summary>
        /// 回傳地理位置
        /// </summary>
        LOCATION,
        /// <summary>
        /// 自訂menu點擊事件
        /// </summary>
        click
    }
    /// <summary>
    /// WeChat Menu按鈕觸發key值
    /// </summary>
    public enum WeChatEventKey
    {
        //在地好康
        todaydeals_local,
        //宅配好物
        todaydeals_delivery,
        //旅遊渡假
        todaydeals_travel,
        //女性專區
        todaydeals_peauty,
        //全家專區
        todaydeals_family,
        //我的17Life
        enjoy_mine,
        //17享樂
        enjoy_promo,
        //優惠券
        vourcher,
    }
    /// <summary>
    /// WeChat一些會變動參數存在systemcode
    /// </summary>
    public enum WeChatSystemCodeType
    {
        /// <summary>
        /// 接口憑證
        /// </summary>
        WeChatAccessToken,
        /// <summary>
        /// 發送訊息
        /// </summary>
        WeChatPostMessage,
        /// <summary>
        /// 建立選單
        /// </summary>
        WeChatCreateMenu
    }
}

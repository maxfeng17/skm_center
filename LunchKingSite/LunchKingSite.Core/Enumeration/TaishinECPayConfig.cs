﻿using System;
using LunchKingSite.Core.Component;
using System.ComponentModel;

namespace LunchKingSite.Core
{
    public enum TaishinECPayCreditCardCheck
    {
        /// <summary>
        /// 不檢查
        /// </summary>
        None = -1,
        /// <summary>
        /// 台新
        /// </summary>
        Taishin = 1,
        /// <summary>
        /// 中國信託
        /// </summary>
        CTBC = 2,
        /// <summary>
        /// 國泰世華
        /// </summary>
        Cathay = 3
    }
}

﻿using System.ComponentModel;

namespace LunchKingSite.Core.Enumeration
{
    public enum SkmPayParkingNoticeUmallApi
    {
        CompleteParkingOTPOrder,
        RefundParkingPayment, //現在已不需要
        SkmPayParkingPayConfirmReTryJob,
    }

    public enum SkmPayParkingOrderApiStatus
    {
        //付款成功 : 1   (僅只有狀態為付款成功才可成功執行退貨)
        PaySuccess = 1,
        //退款成功 : 2  
        ReturnSuccess = 2,
        //退款失敗 : 3   (退款失敗，不可再退款)
        ReturnFail = 3
    }

    /// <summary>
    /// Skm Pay訂單狀態
    /// </summary>
    public enum SkmPayOrderStatus
    {
        /// <summary>
        /// Init
        /// </summary>
        [Description("交易處理中")]
        None = 0,
        /// <summary>
        /// 建立預約付款單
        /// </summary>
        [Description("交易處理中")]
        Create = 1,
        /// <summary>
        /// 取得預備交易編號
        /// </summary>
        [Description("交易處理中")]
        GetPreTransNo = 20,
        /// <summary>
        /// 產出Order編號
        /// </summary>
        [Description("交易處理中")]
        MakeOrderFinsh = 30,
        /// <summary>
        /// 授權交易成功 尚未開立發票
        /// </summary>
        [Description("交易處理中")]
        AuthPaymentSuccess = 40,
        /// <summary>
        /// 等待OTP完成
        /// </summary>
        [Description("交易處理中")]
        WaitingOTPFinished = 41,
        /// <summary>
        /// 退貨處理中
        /// </summary>
        [Description("退貨處理中")]
        WaitingOTPRefund = 42,
        /// <summary>
        /// 傳送發票交易成功
        /// </summary>
        [Description("購買成功 (未取貨)")]
        SentInvoice = 50,
        /// <summary>
        /// 核銷成功
        /// </summary>
        [Description("購買成功 (已取貨)")]
        WriteOff = 70,

        #region 逆流程類

        CancelAuthPayment = 200,
        /// <summary>
        /// 退貨成功
        /// </summary>
        [Description("退貨成功")]
        ReturnSuccess = 210,
        /// <summary>
        /// 取消退款成功
        /// </summary>
        [Description("購買成功")]
        CancelReturnSuccess = 220,

        #endregion 逆流程類

        #region Fail Code

        /// <summary>
        /// 取得預備交易編號失敗
        /// </summary>
        [Description("購買失敗")]
        GetPreTransFail = 1001,
        /// <summary>
        /// 授權交易失敗
        /// </summary>
        [Description("購買失敗 授權交易失敗")]
        AuthPaymentFail = 1002,
        /// <summary>
        /// 開立發票失敗 未退款 (需以Job做退款)
        /// </summary>
        [Description("購買失敗 退款處理中")]
        SentInvoiceFail = 1003,
        /// <summary>
        /// Wallet 退貨交易失敗
        /// </summary>
        [Description("退貨失敗")]
        RefundPaymentFail = 1004,
        /// <summary>
        /// 取消退貨交易失敗
        /// </summary>
        [Description("取消退貨失敗")]
        CancelRefundPaymentFail = 1005,
        /// <summary>
        /// 建立訂單失敗
        /// </summary>
        [Description("購買失敗")]
        MakeOrderFail = 1006,
        /// <summary>
        /// 開立發票失敗且退款成功
        /// </summary>
        [Description("購買失敗")]
        CreateInvoiceFail = 1007,

        /// <summary>
        /// 開立發票失敗且退款失敗 (需以Job做退款)
        /// </summary>
        [Description("購買失敗 退款處理中")]
        CreateInvoiceFailRefundFail = 1008,
        /// <summary>
        /// 核銷成功 通知發票商(藤雲)失敗(In DB) (需以Job再通知)
        /// </summary>
        [Description("購買成功 (已取貨)")]
        CreatedInvoiceWriteOffFail = 1011,
        /// <summary>
        /// 核銷成功 通知發票商(藤雲)失敗(API) (需以Job再通知)
        /// </summary>
        [Description("購買成功 (已取貨)")]
        WriteOffFail = 1012,
        /// <summary>
        /// 退貨 通知發票商(藤雲)失敗(API)
        /// </summary>
        [Description("退貨失敗")]
        ReturnInvoiceFail = 1013,
        /// <summary>
        /// OTP驗證逾時(超過config設定的訪問次數上限)
        /// </summary>
        [Description("交易逾時")]
        SkmPayOTPTimeout = 1014,

        /// <summary>
        /// 開立退貨發票失敗 已退款 (若想單獨退發票，將訂單狀態壓成1111)
        /// </summary>
        [Description("交易失敗")]
        RefundInvoiceFail = 1111,
        /// <summary>
        /// 購買失敗 單獨退發票成功
        /// </summary>
        [Description("交易失敗")]
        RefundInvoiceOnlySuccess = 1112,
        /// <summary>
        /// 購買失敗 單獨退發票失敗
        /// </summary>
        [Description("交易失敗")]
        RefundInvoiceOnlyFail = 1113

        #endregion Fail Code
    }

    /// <summary>
    /// 發票載具
    /// </summary>
    public enum SkmPayInvoiceCarrierType
    {
        [Description("")]
        None = 0,
        [Description("SKM 會員載具")]
        SkmMember = 1,
        [Description("自然人憑證")]
        PersonalCertificate = 2,
        [Description("手機條碼")]
        Mobile = 3,
        [Description("捐贈")]
        Donation = 4,
        [Description("電子發票證明聯")]
        ElectronicInvoiceCertificate = 5
    }
    
    public enum SkmPaymentMethod
    {
        None = 0,
        SkmPay = 1
    }

    /// <summary>
    /// 支付選項
    /// </summary>
    public enum SkmPaymentOption
    {
        /// <summary>
        /// 一次付清
        /// </summary>
        [Description("一次付清")]
        LumpSum = 0,
        /// <summary>
        /// 紅利折抵
        /// </summary>
        [Description("紅利折抵")]
        Redeem = 1,
        /// <summary>
        /// 分期付款
        /// </summary>
        [Description("分期付款")]
        Install = 2
    }

    /// <summary>
    /// Wallet API 回應代碼
    /// </summary>
    public enum WalletReturnCode
    {
        /// <summary>
        /// 成功
        /// </summary>
        [Description("Success")]
        Success = 1000,
        /// <summary>
        /// 取得訂單資訊失敗
        /// </summary>
        [Description("取得訂單資訊失敗")]
        GetPaymentResultfail = 1010,
        /// <summary>
        /// 輸入參數錯誤
        /// </summary>
        [Description("參數錯誤")]
        InputError = 1020,
        /// <summary>
        /// 呼叫API失敗
        /// </summary>
        [Description("呼叫API失敗")]
        InvokeError = 2000,
        /// <summary>
        /// OTP驗證中
        /// </summary>
        [Description("OTP驗證中")]
        WaitingOTP = 2070,
        /// <summary>
        /// 需要OTP驗證
        /// </summary>
        [Description("需要OTP驗證")]
        NeedOTP = 2222,
    }

    public enum SkmOrderFilterType
    {
        /// <summary>
        /// EC未取貨
        /// </summary>
        [Description("未取貨")] //←前台App顯示
        EcNotPickedUp = 1,
        /// <summary>
        /// EC已取貨
        /// </summary>
        [Description("已取貨")] //←前台App顯示
        EcPickedUp = 2,
        /// <summary>
        /// Ec取消
        /// </summary>
        [Description("取消")] //←前台App顯示
        EcCancel = 3,
    }

    public enum SkmSerialNumberLogTradeType
    {
        MakeOrderWallet = 10,
        MakeOrderInvoice = 20,
        ReturnInvoice = 30,
        ReturnWallet = 40
        
    }

    /// <summary>
    /// 從哪個程序去做退款
    /// </summary>
    public enum WalletRefundFromType
    {
        /// <summary>
        /// 非OTP時, 在MarkOrder就可能因開發票失敗直接退款
        /// </summary>
        FromSkmParkingMakeOrderApi,
        /// <summary>
        /// 為OTP時, 在CompleteOrder可能因開發票失敗直接退款
        /// </summary>
        FromCompleteParkingOTPOrderApi,
        /// <summary>
        /// 當OTP逾時, JOB自動做退款
        /// </summary>
        FromSkmPayOTPTimeoutRefundJob,
        /// <summary>
        /// 停管呼叫API做退貨
        /// </summary>
        FromSkmVerifyApiRefundParkingPayment,
        /// <summary>
        /// Skm Pay 正常退款失敗後重新退款Job
        /// </summary>
        FromSkmPayFailedRetryJob
    }

    /// <summary>
    /// 付款方式
    /// </summary>
    public enum WalletPaymentMethod
    {
        [Description("信用卡一次付清")]
        PayOff = 0,
        [Description("信用卡紅利折抵")]
        RewardPoints = 1,
        [Description("信用卡分期付款")]
        Installment = 2
    }
}

﻿using System;
using System.ComponentModel;
using LunchKingSite.Core.Component;

namespace LunchKingSite.Core
{

    /// <summary>
    /// 折價券設定
    /// </summary>
    [Flags]
    public enum DiscountCampaignUsedFlags
    {
        /// <summary>
        /// P好康
        /// </summary>
        Ppon = 1,
        /// <summary>
        /// 品生活
        /// </summary>
        Piinlife = 2,
        /// <summary>
        /// 預留館別1
        /// </summary>
        AdditionalDep_1 = 4,
        /// <summary>
        /// 合作夥伴
        /// </summary>
        Partner = 8,
        /// <summary>
        /// 即時產生折價券
        /// </summary>
        InstantGenerate = 16,
        /// <summary>
        /// 最低消費金額
        /// </summary>
        MinimumAmount = 32,
        /// <summary>
        ///統一序號 
        /// </summary>
        SingleSerialKey = 64,
        /// <summary>
        /// 自定短碼序號
        /// </summary>
        CustomSerialKey = 128,
        /// <summary>
        /// 消費贈等值折價券
        /// </summary>
        FreeGiftWithPurchase = 256,
        /// <summary>
        /// 限本人使用
        /// </summary>
        OwnerUseOnly = 512,
        /// <summary>
        /// 無條件進位
        /// </summary>
        TakeCeiling = 1024,
        /// <summary>
        /// 親友邀請購買
        /// </summary>
        ReferenceAndPay = 2048,
        /// <summary>
        /// 首購
        /// </summary>
        FirstBuy = 4096,
        /// <summary>
        /// VISA卡獨享
        /// </summary>
        VISA = 8192,
        /// <summary>
        /// 館別限制
        /// </summary>
        CategoryLimit = 16384,
        /// <summary>
        /// APP限定
        /// </summary>
        AppUseOnly = 32768,
        /// <summary>
        /// 消費自動發送
        /// </summary>
        AutoSend = 65536,
        /// <summary>
        /// 可重複使用
        /// </summary>
        CanReused = 131072
    }

    /// <summary>
    /// 折價券類型
    /// </summary>
    public enum DiscountCampaignType
    {
        None = 0,
        /// <summary>
        /// 17LIFE 折價券
        /// </summary>
        [Description("17LIFE 折價券")]
        PponDiscountTicket = 1,
        /// <summary>
        /// 熟客券
        /// </summary>
        [Description("熟客券")]
        RegularsTicket = 2,
        /// <summary>
        /// 公關券
        /// </summary>
        [Description("公關券")]
        FavorTicket = 3,
        /// <summary>
        /// 超級折價券
        /// </summary>
        [Description("超級折價券")]
        SuperDiscountTicket = 4
    }

    /// <summary>
    /// 折價券活動類型
    /// </summary>
    public enum DiscountEventType
    {
        /// <summary>
        /// 匯入名單
        /// </summary>
        [Localization("DiscountEventTypeImportUsers")]
        ImportUsers,
        /// <summary>
        /// 活動代碼
        /// </summary>
        [Localization("DiscountEventStatusEventCode")]
        EventCode,
    }

    public enum DiscountActivityType
    {
        /// <summary>
        /// 折價券活動
        /// </summary>
        [Localization("DiscountActivityTypeDiscountEvent")]
        DiscountEvent,
        /// <summary>
        /// 員工折價券
        /// </summary>
        [Localization("DiscountActivityTypeStaffEvent")]
        StaffEvent,
        /// <summary>
        /// 已刪除活動
        /// </summary>
        [Localization("DiscountActivityTypeDeleteEvent")]
        DeleteEvent,
    }

    /// <summary>
    /// 折價券活動狀態
    /// </summary>
    public enum DiscountEventStatus
    {
        /// <summary>
        /// 初始
        /// </summary>
        [Localization("DiscountEventStatusInitial")]
        Initial,
        /// <summary>
        /// 關閉
        /// </summary>
        [Localization("DiscountEventStatusDisabled")]
        Disabled,
    }

    /// <summary>
    /// 折價券名單狀態
    /// </summary>
    public enum DiscountUserStatus
    {
        None,
        /// <summary>
        /// 未發送
        /// </summary>
        Initial,
        /// <summary>
        /// 已發送
        /// </summary>
        Sent,
        /// <summary>
        /// 發送不足
        /// </summary>
        Shortage,
    }

    /// <summary>
    /// 折價券領取機制
    /// </summary>
    public enum DiscountUserCollectType
    {
        /// <summary>
        /// 無法由使用者自行領取
        /// </summary>
        None = 0,
        /// <summary>
        /// 使用者可手動點擊領取
        /// </summary>
        Manually = 1,
        /// <summary>
        /// 系統自動幫使用者領取(不須點擊)
        /// </summary>
        Auto = 2
    }

    public enum DiscountLimitType
    {
        /// <summary>
        /// 關閉
        /// </summary>
        Disabled = 0,
        /// <summary>
        /// 啟用
        /// </summary>
        Enabled = 1
    }

    public enum CampaignBlackDealType
    {
        /// <summary>
        /// 全部
        /// </summary>
        All = 0,
        /// <summary>
        /// 搶購中
        /// </summary>
        Sold = 1,
        /// <summary>
        /// 已結檔
        /// </summary>
        Expire = 2
    }

}

﻿using LunchKingSite.Core.Component;
using System;
using System.ComponentModel;

namespace LunchKingSite.Core
{
    /// <summary>
    /// skm system log type
    /// </summary>
    public enum SkmSystemLogType
    {
        /// <summary>
        /// 未分類
        /// </summary>
        System = 0,
        /// <summary>
        /// 燒點
        /// </summary>
        BurningEvent = 1,
        BurningOrder = 2,
        BurningReply = 3,
        BurningCostCenter = 4,
        /// <summary>
        /// 傳送中獎清冊
        /// </summary>
        SendPrizeCoupon = 5
    }

    /// <summary>
    /// SKM 檔次類型
    /// </summary>
    public enum ActiveType
    {
        /// <summary>
        /// 商品優惠
        /// </summary>
        [Description("商品優惠")]
        Product = 1,
        /// <summary>
        /// 全櫃位優惠
        /// </summary>
        [Description("全櫃位優惠")]
        Shoppe = 2,
        /// <summary>
        /// 相關商品(體驗)
        /// </summary>
        [Description("相關商品(體驗)")]
        Experience = 3,
        /// <summary>
        /// 買幾送幾
        /// </summary>
        [Description("買幾送幾")]
        BuyGetFree = 4,
        /// <summary>
        /// 電子禮券
        /// </summary>
        [Description("電子贈品禮券")]
        GiftCertificate = 5,
        /// <summary>
        /// 電子抵用金
        /// </summary>
        [Description("電子抵用金")]
        CashCoupon = 6
    }

    /// <summary>
    /// 優惠類型
    /// </summary>
    public enum DiscountType
    {
        None = 0,
        /// <summary>
        /// 優惠價格
        /// </summary>
        DiscountPrice = 1,
        /// <summary>
        /// 折抵現金
        /// </summary>
        DiscountCash = 2,
        /// <summary>
        /// 打折優惠
        /// </summary>
        DiscountPercent = 3,
        /// <summary>
        /// 消費送  
        /// </summary>
        DiscountFree = 4,
        /// <summary>
        /// 其他(免費送)
        /// </summary>
        DiscountOther = 5,
        /// <summary>
        /// 買幾送幾(當ActiveType=4, 則DiscountType=6)
        /// </summary>
        DiscountBuyGetFree = 6,
    }

    /// <summary>
    /// 後台檔次申請狀態
    /// </summary>
    public enum SKMDealStatus
    {
        /// <summary>
        /// 草稿
        /// </summary>
        Draft,
        /// <summary>
        /// 待製檔
        /// </summary>
        WaitingMake,
        /// <summary>
        /// 製檔中
        /// </summary>
        OnTheMake,
        /// <summary>
        /// 等待確認
        /// </summary>
        WaitingConfirm,
        /// <summary>
        /// 已確認
        /// </summary>
        Confirmed,
        /// <summary>
        /// 已上檔
        /// </summary>
        OnProduct,
        /// <summary>
        /// 失效
        /// </summary>
        Invalid
    }

    public enum PageFilterField
    {
        Desc = 1,
        [Description("BusinessHourOrderTimeS")]
        OrderS = 2,
        [Description("BusinessHourOrderTimeE")]
        OrderE = 4,
        [Description("Status")]
        Status = 8,
    }

    /// <summary>
    /// App Banner 活動狀態
    /// </summary>
    public enum MerchantEventType
    {
        /// <summary>
        /// 新光
        /// </summary>
        Skm = 1
    }

    public enum SkmAppStyleStatus
    {
        /// <summary>
        /// 刪除
        /// </summary>
        Delete = -1,
        /// <summary>
        /// 啟用
        /// </summary>
        Normal = 1,
        /// <summary>
        /// 隱藏
        /// </summary>
        Hide = 2,
    }

    /// <summary>
    /// 檔次類型
    /// </summary>
    public enum SkmDealTypes
    {
        /// <summary>
        /// 優惠活動
        /// </summary>
        [Localization("SkmPromotional")]
        SkmPromotional = 0,
        /// <summary>
        /// 熱賣商品
        /// </summary>
        [Localization("SkmHot")]
        SkmHot = 1,
        /// <summary>
        /// 注目商品
        /// </summary>
        [Localization("SkmProductInfo")]
        SkmProductInfo = 2,
    }

    public enum BeaconType
    {
        None = 0,
        Default = 1,
        Customer = 2,
    }

    public enum SkmQrDiscountType
    {
        /// <summary>
        /// 折抵現金
        /// </summary>
        DiscountCash = 0,
        /// <summary>
        /// 打折優惠
        /// </summary>
        DiscountPercent = 1,
        /// <summary>
        /// 指定金額
        /// </summary>
        DiscountPrice = 2
    }

    /// <summary>
    /// 新光館別
    /// </summary>
    public enum SkmShop
    {
        #region 台北地區

        [Description("台北南西店一館")]
        TaipeiNanxiShopOne = 101,
        [Description("台北南西店二館")]
        TaipeiNanxiShopTwo = 102,
        [Description("台北南西店三館")]
        TaipeiNanxiShopThree = 103,
        [Description("台北站前店")]
        TaipeiStation = 111,
        [Description("A11館")]
        TaipeiXinyiPlaceA11 = 121,
        [Description("A8館")]
        TaipeiXinyiPlaceA8 = 122,
        [Description("A9館")]
        TaipeiXinyiPlaceA9 = 123,
        [Description("A4館")]
        TaipeiXinyiPlaceA4 = 124,
        [Description("台北天母店一館")]
        TaipeiTianmuShopOne = 151,
        [Description("台北天母店二館")]
        TaipeiTianmuShopTwo = 152,

        #endregion 台北地區

        #region 桃園地區

        [Description("桃園大有店一館")]
        TaoyuanDayouShopOne = 161,
        [Description("桃園大有店二館")]
        TaoyuanDayouShopTwo = 162,
        [Description("桃園站前店")]
        TaoyuanStation = 171,

        #endregion 桃園地區

        #region 新竹地區

        [Description("新竹中華店")]
        HsinchuZhonghua = 181,

        #endregion 新竹地區

        #region 台中地區

        [Description("台中中港店")]
        TaichungZhonggang = 201,

        #endregion 台中地區

        #region 高雄地區

        [Description("高雄三多店")]
        KaohsiungSanduo = 301,
        [Description("高雄左營店一館")]
        KaohsiungZuoyingShopOne = 351,
        [Description("高雄左營店二館")]
        KaohsiungZuoyingShopTwo = 352,

        #endregion 高雄地區

        #region 台南地區

        [Description("台南中山店")]
        TainanZhongshan = 311,
        [Description("台南西門店一館")]
        TainanPlaceShopOne = 321,
        [Description("台南西門店二館")]
        TainanPlaceShopTwo = 322,

        #endregion 台南地區

        #region 嘉義地區

        [Description("嘉義垂楊店")]
        ChiayiChuiyang = 331,

        #endregion 嘉義地區
    }

    public enum SkmBeaconType
    {
        [Localization("SkmTop")]
        Top = 1,
        [Localization("SkmProject")]
        Project = 2,
        [Localization("SkmDeal")]
        Deal = 3,
        [Localization("SkmEvent_Manual")]
        Event_Manual = 4,
        [Localization("SkmEvent_Count")]
        Event_Count = 5
    }

    public enum SkmBeaconStatus
    {
        /// <summary>
        /// 啟用
        /// </summary>
        Normal = 1,
        /// <summary>
        /// 收藏通知
        /// </summary>
        Deal = 2,
        /// <summary>
        /// 停用
        /// </summary>
        Cancel = 3
    }

    public enum SkmItemNoApiResult
    {
        /// <summary>
        /// 查詢失敗
        /// </summary>
        Fail = 0,
        /// <summary>
        /// 查詢成功
        /// </summary>
        Success = 1,
        /// <summary>
        /// 多筆金額
        /// </summary>
        MultiPrice = 2,
    }

    public enum BeaconPowerLevel
    {
        /// <summary>
        /// 低電量，建議更換
        /// </summary>
        Low = 0,
        /// <summary>
        /// 一般電量
        /// </summary>
        Medium = 1,
        /// <summary>
        /// 充足電量
        /// </summary>
        Full = 2,
    }

    public enum SkmLogType
    {
        /// <summary>
        /// skm member.is_shared change log
        /// </summary>
        IsSharedChange = 0,
        /// <summary>
        /// Skm member check token api log
        /// </summary>
        CheckSkmTokenApi = 1,
        /// <summary>
        /// 新增燒點活動
        /// </summary>
        AddBurningEvent = 2,
        GetOrderCouponList = 3,
        PrizeDrawEventSave = 4,
        /// <summary>
        /// Umall 更新贈品庫存
        /// </summary>
        UmallUpdateGiftInventory = 5,
        /// <summary>
        /// 新光零元檔建立訂單
        /// </summary>
        ZeroPayMakeOrder = 6,
        /// <summary>
        /// 管理後台操作Log
        /// </summary>
        ManageBackgroundOperations = 7,
        /// <summary>
        /// 推播JobLog
        /// </summary>
        PostSkmPushData = 8,
        /// <summary>
        /// 取得商品條碼以及應稅/免稅資料
        /// </summary>
        GetProductCode = 9,
        /// <summary>
        /// 騰雲發票相關資料
        /// </summary>
        TurnCloudServer = 11
    }

    /// <summary>
    /// 新光WS交易代碼
    /// </summary>
    public enum SkmWebServiceTransCode
    {
        /// <summary>
        /// 新增燃點活動 (建立燒點活動)
        /// </summary>
        [Description("373273")]
        AddBurningPointEvent,
        /// <summary>
        /// 發放燃點活動券 (進行燒點)
        /// </summary>
        [Description("373271")]
        GetBurningPointCoupon,
        /// <summary>
        /// 折價券核銷資料更新 (核銷)
        /// </summary>
        [Description("373271")]
        VerifyBurningCoupon,
        /// <summary>
        /// 折價券發放資料更新 (傳送中獎清冊)
        /// </summary>
        [Description("373271")]
        SendPrizeCoupon
    }

    public enum SkmBurningEventStatus
    {
        /// <summary>
        /// 燒點活動建立初始, 尚未呼叫環友WS
        /// </summary>
        Init = 0,
        /// <summary>
        /// 已呼叫環友WS建立成功
        /// </summary>
        Create = 1,
        /// <summary>
        /// 已呼叫環友WS建立失敗
        /// </summary>
        Fail = 2,
        /// <summary>
        /// 初始更新
        /// </summary>
        UpdateInit = 3,
    }

    public enum SkmBurningOrderStatus
    {
        /// <summary>
        /// 建立Log
        /// </summary>
        Init = 0,
        /// <summary>
        /// 已呼叫WS
        /// </summary>
        Create = 1,
        /// <summary>
        /// 已呼叫環友WS建立失敗
        /// </summary>
        Fail = 2,
    }

    public enum ExternalDealVersion
    {
        /// <summary>
        /// 3.6以前的優惠
        /// </summary>
        Coupon = 0,
        /// <summary>
        /// 3.7(含)以後的優惠
        /// </summary>
        Burning = 1,
        /// <summary>
        /// Skm Pay EC
        /// </summary>
        SkmPay = 2
    }

    /// <summary>
    /// SKM檔次排序功能
    /// </summary>
    public enum SkmCategorySortType
    {
        /// <summary>
        /// 預設
        /// </summary>
        [Localization("CategorySortTypeDefault")]
        Default,
        /// <summary>
        /// 最新
        /// </summary>
        [Localization("CategorySortTypeTopNews")]
        TopNews,
        /// <summary>
        /// 銷量
        /// </summary>
        [Localization("CategorySortTypeTopOrderTotal")]
        TopOrderTotal,
        /// <summary>
        /// 折數(低到高)
        /// </summary>
        [Localization("CategorySortTypeDiscountAsc")]
        DiscountAsc,
        /// <summary>
        /// 價格(高到低)
        /// </summary>
        [Localization("CategorySortTypePriceDesc")]
        PriceDesc,
        /// <summary>
        /// 價格(低到高)
        /// </summary>
        [Localization("CategorySortTypePriceAsc")]
        PriceAsc,
        /// <summary>
        /// 收藏數量(高到低)
        /// </summary>
        MemberCollectDesc,
        /// <summary>
        /// 收藏數量(低到高)
        /// </summary>
        MemberCollectAsc,
        /// <summary>
        /// 活動結止日 早-晚
        /// </summary>
        BusinessHourOrderTimeEndAsc,
        /// <summary>
        /// 活動結止日 晚-早
        /// </summary>
        BusinessHourOrderTimeEndDesc,
        /// <summary>
        /// 兌換點數 高->低
        /// </summary>
        BurningPointDesc,
        /// <summary>
        /// 兌換點數 低->高
        /// </summary>
        BurningPointAsc
    }

    public enum SkmVerifyReplyStatus
    {
        /// <summary>
        /// create
        /// </summary>
        Init = 0,
        /// <summary>
        /// 預備送出回覆
        /// </summary>
        Ready = 1,
        /// <summary>
        /// 送出回覆後失敗
        /// </summary>
        ReplyFail = 2,
        /// <summary>
        /// 成功送出回覆
        /// </summary>
        ReplySuccess = 200
    }

    public enum SkmPrizeDrawTemple
    {
        [Description("紅包")]
        Red = 1,
    }

    public enum SkmPrizeDrawEventStatus
    {
        [Description("開啟")]
        Normal = 0,
        [Description("隱藏")]
        Hide = 1,
    }

    public enum SkmPrizeDrawEventDescription
    {
        [Description("上檔中")]
        Ineffective = 1,
        [Description("過期")]
        Expired = 2
    }

    public enum SkmPrizeDrawEventOrderField
    {
        Default = 0,
        Sd = 1,
        Ed = 2,
        Temple = 3,
        Status = 4,
        App = 5
    }

    public enum SkmPrizeDrawItemType
    {
        Bid = 0,
    }

    /// <summary> 策展狀態 </summary>
    public enum SkmExhibitionEventStatus : byte
    {
        /// <summary> 草稿 </summary>
        [Description("草稿")]
        Draft = 1,
        /// <summary> 上架 </summary>
        [Description("上架")]
        Published = 2,
        /// <summary> 下架 </summary>
        [Description("下架")]
        Unpublished = 3
    }

    /// <summary> 策展類型 </summary>
    public enum SkmExhibitionEventType : byte
    {
        /// <summary> 商品策展 </summary>
        [Description("商品策展")]
        DealExhibition = 1,
        /// <summary> 訊息策展 </summary>
        [Description("訊息策展")]
        MessageExhibition = 2,
        /// <summary> 單一活動 </summary>
        [Description("單一活動")]
        SingleEvent = 3,
        /// <summary> 多重活動 </summary>
        [Description("多重活動")]
        MultiEvent = 4,
    }

    /// <summary> 新光三越會員卡別 </summary>
    public enum SkmCardLevelType : byte
    {
        /// <summary>
        /// 靈級會員
        /// </summary>
        [Description("新光三越APP會員")]
        Zero = 0,
        /// <summary>
        /// 全客群
        /// </summary>
        [Description("全客群")]
        All = 1,
        /// <summary>
        /// 訪客
        /// </summary>
        [Description("訪客")]
        Guest = 2,
        /// <summary>
        /// 貴賓卡(紅卡)
        /// </summary>
        [Description("貴賓卡")]
        VIP = 3,
        /// <summary>
        /// 銀卡
        /// </summary>
        [Description("銀卡")]
        Silver = 4,
        /// <summary>
        /// 尊榮卡
        /// </summary>
        [Description("尊榮卡")]
        Honor = 5,
        /// <summary>
        /// 黑卡
        /// </summary>
        [Description("黑卡")]
        Centurion = 6
    }

    public enum DealOutputType
    {
        /// <summary>
        /// 非SKM PAY檔次
        /// </summary>
        Default = 0,
        /// <summary>
        /// 使用SKM PAY檔次
        /// </summary>
        SkmPay = 1,
        /// <summary>
        /// 不分
        /// </summary>
        Mixed = 2,
        /// <summary>
        /// 舊版
        /// </summary>
        OldSchool = 99
    }

    public enum SkmTriggerType
    {
        /// <summary>
        /// Beacon
        /// </summary>
        BeaconMessageRead = 1,
        /// <summary>
        /// push (skm未使用)
        /// </summary>
        MessageBoxRead = 2,
        /// <summary>
        /// in app message
        /// </summary>
        InAppMessageRead = 3
    }

    public enum SkmInAppMessageType
    {
        /// <summary>
        /// 一般訊息
        /// </summary>
        Normal = 0,
        /// <summary>
        /// 建立訂單成功
        /// </summary>
        MakeOrder = 1,
        /// <summary>
        /// 退貨成功
        /// </summary>
        ReturnOrder = 2,
        /// <summary>
        /// DeepLink
        /// </summary>
        DeepLink = 3,
        /// <summary>
        /// Beacon
        /// </summary>
        Beacon = 4,
    }

    /// <summary> 檔次兌換地點類型 </summary>
    public enum SkmDealExchangeType
    {
        /// <summary> 未知(DB中原始資料可能有無法定義的資料) </summary>
        Unknow = -1,
        /// <summary> 一般櫃 </summary>
        Normal = 1,
        /// <summary> 贈品處 </summary>
        Exchange = 2,
        /// <summary> 快取Bar </summary>
        Cache = 3,
    }

    /// <summary> 廣告活動連結類型 </summary>
    public enum SKMAdBoardLinkType
    {
        /// <summary> 無連結 </summary>
        None = 1,
        /// <summary> 分店策展列表DeepLink </summary>
        StoreExhibitionDeepLink = 2,
        /// <summary> 活動網址 </summary>
        EventLink = 3,
    }

    /// <summary> 檔次限制可購買次數類型 </summary>
    public enum SkmDealRepeatPurchaseType
    {
        /// <summary> 無限制 </summary>
        [Description("活動期間可重複兌換")]
        None = 0,
        /// <summary> 每人每日 </summary>
        [Description("每人每日限購數量")]
        Daily = 1,
        /// <summary> 每人本活動時間內 </summary>
        [Description("活動期間每人限購數量")]
        DealTime = 2,
    }

    /// <summary> 自訂版位連結類型 </summary>
    public enum SkmCustomizedBoardUrlTypeEnum
    {
        /// <summary> 美食訂候位 </summary>
        [Description("美食訂候位(預設)")]
        DefaultGoodEat = 1,
        /// <summary> 預設策展列表 </summary>
        [Description("策展列表(預設)")]
        DefaultExhibitionList = 2,
        /// <summary> 美麗台 </summary>
        [Description("美麗台(預設)")]
        DefaultBeautyStage = 3,
        /// <summary> 策展列表 </summary>
        [Description("策展列表頁")]
        CustomizedExhibitionList = 4,
        /// <summary> 策展主題 </summary>
        [Description("策展主題頁")]
        ExhibitionEvent = 5,
        /// <summary> 商品頁 </summary>
        [Description("商品頁")]
        ExternalDeal = 6,
        /// <summary> 網址 </summary>
        [Description("網址")]
        Url = 7,
        [Description("週週抽獎模組")]
        WeeklyDrawEvent = 8,
        [Description("抽紅包模組")]
        PrizeDrawEvent = 9
    }

    /// <summary> 自訂版位顯示位置類型 </summary>
    public enum SkmCustomizedBoardLayoutTypeEnum
    {
        /// <summary> 首頁三區域-左方 </summary>
        [Description("左方")]
        IndexZone1 = 1,
        /// <summary> 首頁三區域-右上 </summary>
        [Description("右上")]
        IndexZone2 = 2,
        /// <summary> 首頁三區域-右下 </summary>
        [Description("右下")]
        IndexZone3 = 3,
    }

    /// <summary> 自訂版位顯示位置類型 </summary>
    public enum SkmCustomizedBoardContentyTypeEnum
    {
        /// <summary> deeplink </summary>
        DeepLink = 1,
        /// <summary> inline </summary>
        Inline = 2,
        /// <summary> 網址 </summary>
        Weburl = 3,
    }

    /// <summary> 新光Deep Link 類型 </summary>
    public struct SkmDeepLinkFormat
    {
        /// <summary> 策展列表 </summary>
        public const string ExhibitionList = "skmapp://www.skm.com/exhibitionlist?store={0}&sname={1}";
        /// <summary> 策展 </summary>
        public const string ExhibitionEvent = "skmapp://www.skm.com/exhibitionpage?store={0}&sname={1}&exid={2}";
        /// <summary> 檔次 </summary>
        public const string ExternalDeal = "skmapp://www.skm.com/productpage?pbid={0}";

        public const string PrizeDrawEvent = "skmapp://www.skm.com/luckydraw?type=0&token={0}";

        public const string WeeklyDrawEvent = "skmapp://www.skm.com/luckydraw?type=1&token={0}";
    }

    public enum SkmInstallEventStatus
    {
        [Description("停用")]
        Disable = 0,
        [Description("使用中")]
        Avaliable = 1,
        [Description("已過期")]
        Expired = 2,
        [Description("待上架(使用中)")]
        Waiting = 3
    }

}

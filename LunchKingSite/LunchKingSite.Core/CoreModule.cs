﻿using Autofac;
using LunchKingSite.Core.Component;
using System.ComponentModel.Composition;
using System.Configuration;
using System.Web.Configuration;
using Vodka.Container;

namespace LunchKingSite.Core
{
    [Export(typeof(IModuleRegistrar))]
    public class CoreModule : IModuleRegistrar
    {
        public static IContainer Container { get; private set; }

        public void RegisterWithContainer(ContainerBuilder builder)
        {
            if (ConfigurationManager.ConnectionStrings != null)
                foreach (ConnectionStringSettings c in ConfigurationManager.ConnectionStrings)
                    builder.RegisterInstance(c.ConnectionString).Named<string>(c.Name + "ConnectionString");


            //如果哪天想給非web環境使用
            if (System.Web.Hosting.HostingEnvironment.ApplicationHost == null)
            {
                builder.RegisterType<AppConfSysConfProvider>().AsImplementedInterfaces().InstancePerLifetimeScope();
                builder.RegisterType<WebConfSysConfProvider>().AsImplementedInterfaces().InstancePerLifetimeScope();
            }
            else
            {
                builder.RegisterType<WebConfSysConfProvider>().AsImplementedInterfaces().InstancePerLifetimeScope();
            }
            
            builder.RegisterType<JsonSerializer>().AsImplementedInterfaces().InstancePerLifetimeScope();
            builder.RegisterType<MessageQueueProvider>().AsImplementedInterfaces().InstancePerLifetimeScope();

            if (WebConfigurationManager.AppSettings["CacheMode"] == "Redis")
            {
                builder.RegisterType<RedisCacheProvider>().As<ICacheProvider>().InstancePerLifetimeScope();
            }
            else
            {
                builder.RegisterType<MemoryCacheProvider>().As<ICacheProvider>().InstancePerLifetimeScope();
            }            
        }

        public void Initialize(IContainer container)
        {
            Container = container;
        }
    }
}
using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the CpaDetail class.
    /// </summary>
    [Serializable]
    public partial class CpaDetailCollection : RepositoryList<CpaDetail, CpaDetailCollection>
    {
        public CpaDetailCollection() { }

        /// <summary>
        /// Filters an existing collection based on the set criteria. This is an in-memory filter
        /// Thanks to developingchris for this!
        /// </summary>
        /// <returns>CpaDetailCollection</returns>
        public CpaDetailCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                CpaDetail o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }


    }
    /// <summary>
    /// This is an ActiveRecord class which wraps the cpa_detail table.
    /// </summary>
    [Serializable]
    public partial class CpaDetail : RepositoryRecord<CpaDetail>, IRecordBase
    {
        #region .ctors and Default Settings

        public CpaDetail()
        {
            SetSQLProps();
            InitSetDefaults();
            MarkNew();
        }

        private void InitSetDefaults() { SetDefaults(); }

        public CpaDetail(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
                ForceDefaults();
            MarkNew();
        }

        protected static void SetSQLProps() { GetTableSchema(); }

        #endregion

        #region Schema and Query Accessor
        public static Query CreateQuery() { return new Query(Schema); }
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                    SetSQLProps();
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("cpa_detail", TableType.Table, DataService.GetInstance("BigData"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = true;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = true;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                colvarId.DefaultSetting = @"";
                colvarId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarId);

                TableSchema.TableColumn colvarMainId = new TableSchema.TableColumn(schema);
                colvarMainId.ColumnName = "main_id";
                colvarMainId.DataType = DbType.Int32;
                colvarMainId.MaxLength = 0;
                colvarMainId.AutoIncrement = false;
                colvarMainId.IsNullable = false;
                colvarMainId.IsPrimaryKey = false;
                colvarMainId.IsForeignKey = false;
                colvarMainId.IsReadOnly = false;
                colvarMainId.DefaultSetting = @"";
                colvarMainId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarMainId);

                TableSchema.TableColumn colvarUserName = new TableSchema.TableColumn(schema);
                colvarUserName.ColumnName = "user_name";
                colvarUserName.DataType = DbType.AnsiString;
                colvarUserName.MaxLength = 250;
                colvarUserName.AutoIncrement = false;
                colvarUserName.IsNullable = true;
                colvarUserName.IsPrimaryKey = false;
                colvarUserName.IsForeignKey = false;
                colvarUserName.IsReadOnly = false;
                colvarUserName.DefaultSetting = @"";
                colvarUserName.ForeignKeyTableName = "";
                schema.Columns.Add(colvarUserName);

                TableSchema.TableColumn colvarSessionId = new TableSchema.TableColumn(schema);
                colvarSessionId.ColumnName = "session_id";
                colvarSessionId.DataType = DbType.AnsiString;
                colvarSessionId.MaxLength = 150;
                colvarSessionId.AutoIncrement = false;
                colvarSessionId.IsNullable = false;
                colvarSessionId.IsPrimaryKey = false;
                colvarSessionId.IsForeignKey = false;
                colvarSessionId.IsReadOnly = false;
                colvarSessionId.DefaultSetting = @"";
                colvarSessionId.ForeignKeyTableName = "";
                schema.Columns.Add(colvarSessionId);

                TableSchema.TableColumn colvarIp = new TableSchema.TableColumn(schema);
                colvarIp.ColumnName = "ip";
                colvarIp.DataType = DbType.AnsiString;
                colvarIp.MaxLength = 100;
                colvarIp.AutoIncrement = false;
                colvarIp.IsNullable = false;
                colvarIp.IsPrimaryKey = false;
                colvarIp.IsForeignKey = false;
                colvarIp.IsReadOnly = false;
                colvarIp.DefaultSetting = @"";
                colvarIp.ForeignKeyTableName = "";
                schema.Columns.Add(colvarIp);

                TableSchema.TableColumn colvarPageName = new TableSchema.TableColumn(schema);
                colvarPageName.ColumnName = "page_name";
                colvarPageName.DataType = DbType.AnsiString;
                colvarPageName.MaxLength = 300;
                colvarPageName.AutoIncrement = false;
                colvarPageName.IsNullable = false;
                colvarPageName.IsPrimaryKey = false;
                colvarPageName.IsForeignKey = false;
                colvarPageName.IsReadOnly = false;
                colvarPageName.DefaultSetting = @"";
                colvarPageName.ForeignKeyTableName = "";
                schema.Columns.Add(colvarPageName);

                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = false;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                colvarCreateTime.DefaultSetting = @"";
                colvarCreateTime.ForeignKeyTableName = "";
                schema.Columns.Add(colvarCreateTime);

                TableSchema.TableColumn colvarOrderGuid = new TableSchema.TableColumn(schema);
                colvarOrderGuid.ColumnName = "order_guid";
                colvarOrderGuid.DataType = DbType.Guid;
                colvarOrderGuid.MaxLength = 0;
                colvarOrderGuid.AutoIncrement = false;
                colvarOrderGuid.IsNullable = true;
                colvarOrderGuid.IsPrimaryKey = false;
                colvarOrderGuid.IsForeignKey = false;
                colvarOrderGuid.IsReadOnly = false;
                colvarOrderGuid.DefaultSetting = @"";
                colvarOrderGuid.ForeignKeyTableName = "";
                schema.Columns.Add(colvarOrderGuid);

                TableSchema.TableColumn colvarRsrc = new TableSchema.TableColumn(schema);
                colvarRsrc.ColumnName = "rsrc";
                colvarRsrc.DataType = DbType.AnsiString;
                colvarRsrc.MaxLength = 150;
                colvarRsrc.AutoIncrement = false;
                colvarRsrc.IsNullable = true;
                colvarRsrc.IsPrimaryKey = false;
                colvarRsrc.IsForeignKey = false;
                colvarRsrc.IsReadOnly = false;
                colvarRsrc.DefaultSetting = @"";
                colvarRsrc.ForeignKeyTableName = "";
                schema.Columns.Add(colvarRsrc);

                TableSchema.TableColumn colvarContent = new TableSchema.TableColumn(schema);
                colvarContent.ColumnName = "content";
                colvarContent.DataType = DbType.String;
                colvarContent.MaxLength = 500;
                colvarContent.AutoIncrement = false;
                colvarContent.IsNullable = true;
                colvarContent.IsPrimaryKey = false;
                colvarContent.IsForeignKey = false;
                colvarContent.IsReadOnly = false;
                colvarContent.DefaultSetting = @"";
                colvarContent.ForeignKeyTableName = "";
                schema.Columns.Add(colvarContent);

                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["BigData"].AddSchema("cpa_detail", schema);
            }
        }
        #endregion

        #region Props

        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id
        {
            get { return GetColumnValue<int>(Columns.Id); }
            set { SetColumnValue(Columns.Id, value); }
        }

        [XmlAttribute("MainId")]
        [Bindable(true)]
        public int MainId
        {
            get { return GetColumnValue<int>(Columns.MainId); }
            set { SetColumnValue(Columns.MainId, value); }
        }

        [XmlAttribute("UserName")]
        [Bindable(true)]
        public string UserName
        {
            get { return GetColumnValue<string>(Columns.UserName); }
            set { SetColumnValue(Columns.UserName, value); }
        }

        [XmlAttribute("SessionId")]
        [Bindable(true)]
        public string SessionId
        {
            get { return GetColumnValue<string>(Columns.SessionId); }
            set { SetColumnValue(Columns.SessionId, value); }
        }

        [XmlAttribute("Ip")]
        [Bindable(true)]
        public string Ip
        {
            get { return GetColumnValue<string>(Columns.Ip); }
            set { SetColumnValue(Columns.Ip, value); }
        }

        [XmlAttribute("PageName")]
        [Bindable(true)]
        public string PageName
        {
            get { return GetColumnValue<string>(Columns.PageName); }
            set { SetColumnValue(Columns.PageName, value); }
        }

        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime CreateTime
        {
            get { return GetColumnValue<DateTime>(Columns.CreateTime); }
            set { SetColumnValue(Columns.CreateTime, value); }
        }

        [XmlAttribute("OrderGuid")]
        [Bindable(true)]
        public Guid? OrderGuid
        {
            get { return GetColumnValue<Guid?>(Columns.OrderGuid); }
            set { SetColumnValue(Columns.OrderGuid, value); }
        }

        [XmlAttribute("Rsrc")]
        [Bindable(true)]
        public string Rsrc
        {
            get { return GetColumnValue<string>(Columns.Rsrc); }
            set { SetColumnValue(Columns.Rsrc, value); }
        }

        [XmlAttribute("Content")]
        [Bindable(true)]
        public string Content
        {
            get { return GetColumnValue<string>(Columns.Content); }
            set { SetColumnValue(Columns.Content, value); }
        }

        #endregion




        //no foreign key tables defined (0)



        //no ManyToMany tables defined (0)





        #region Typed Columns


        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }



        public static TableSchema.TableColumn MainIdColumn
        {
            get { return Schema.Columns[1]; }
        }



        public static TableSchema.TableColumn UserNameColumn
        {
            get { return Schema.Columns[2]; }
        }



        public static TableSchema.TableColumn SessionIdColumn
        {
            get { return Schema.Columns[3]; }
        }



        public static TableSchema.TableColumn IpColumn
        {
            get { return Schema.Columns[4]; }
        }



        public static TableSchema.TableColumn PageNameColumn
        {
            get { return Schema.Columns[5]; }
        }



        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[6]; }
        }



        public static TableSchema.TableColumn OrderGuidColumn
        {
            get { return Schema.Columns[7]; }
        }



        public static TableSchema.TableColumn RsrcColumn
        {
            get { return Schema.Columns[8]; }
        }



        public static TableSchema.TableColumn ContentColumn
        {
            get { return Schema.Columns[9]; }
        }



        #endregion
        #region Columns Struct
        public struct Columns
        {
            public static string Id = @"id";
            public static string MainId = @"main_id";
            public static string UserName = @"user_name";
            public static string SessionId = @"session_id";
            public static string Ip = @"ip";
            public static string PageName = @"page_name";
            public static string CreateTime = @"create_time";
            public static string OrderGuid = @"order_guid";
            public static string Rsrc = @"rsrc";
            public static string Content = @"content";

        }
        #endregion

        #region Update PK Collections

        #endregion

        #region Deep Save

        #endregion
    }
}

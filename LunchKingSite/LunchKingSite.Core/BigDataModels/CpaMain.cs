using System;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Xml;
using System.Xml.Serialization;
using SubSonic;
using SubSonic.Utilities;
namespace LunchKingSite.DataOrm
{
    /// <summary>
    /// Strongly-typed collection for the CpaMain class.
    /// </summary>
    [Serializable]
    public partial class CpaMainCollection : RepositoryList<CpaMain, CpaMainCollection>
    {
        public CpaMainCollection() { }

        /// <summary>
        /// Filters an existing collection based on the set criteria. This is an in-memory filter
        /// Thanks to developingchris for this!
        /// </summary>
        /// <returns>CpaMainCollection</returns>
        public CpaMainCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                CpaMain o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }


    }
    /// <summary>
    /// This is an ActiveRecord class which wraps the cpa_main table.
    /// </summary>
    [Serializable]
    public partial class CpaMain : RepositoryRecord<CpaMain>, IRecordBase
    {
        #region .ctors and Default Settings

        public CpaMain()
        {
            SetSQLProps();
            InitSetDefaults();
            MarkNew();
        }

        private void InitSetDefaults() { SetDefaults(); }

        public CpaMain(bool useDatabaseDefaults)
        {
            SetSQLProps();
            if (useDatabaseDefaults)
                ForceDefaults();
            MarkNew();
        }

        protected static void SetSQLProps() { GetTableSchema(); }

        #endregion

        #region Schema and Query Accessor

        public static Query CreateQuery()
        {
            return new Query(Schema);
        }
        public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }

        private static void GetTableSchema()
        {
            if (!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("cpa_main", TableType.Table, DataService.GetInstance("BigData"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns

                TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
                colvarId.ColumnName = "id";
                colvarId.DataType = DbType.Int32;
                colvarId.MaxLength = 0;
                colvarId.AutoIncrement = true;
                colvarId.IsNullable = false;
                colvarId.IsPrimaryKey = true;
                colvarId.IsForeignKey = false;
                colvarId.IsReadOnly = false;
                colvarId.DefaultSetting = string.Empty;
                colvarId.ForeignKeyTableName = string.Empty;
                schema.Columns.Add(colvarId);

                TableSchema.TableColumn colvarCpaName = new TableSchema.TableColumn(schema);
                colvarCpaName.ColumnName = "cpa_name";
                colvarCpaName.DataType = DbType.String;
                colvarCpaName.MaxLength = 250;
                colvarCpaName.AutoIncrement = false;
                colvarCpaName.IsNullable = false;
                colvarCpaName.IsPrimaryKey = false;
                colvarCpaName.IsForeignKey = false;
                colvarCpaName.IsReadOnly = false;
                colvarCpaName.DefaultSetting = string.Empty;
                colvarCpaName.ForeignKeyTableName = string.Empty;
                schema.Columns.Add(colvarCpaName);

                TableSchema.TableColumn colvarCpaCode = new TableSchema.TableColumn(schema);
                colvarCpaCode.ColumnName = "cpa_code";
                colvarCpaCode.DataType = DbType.AnsiString;
                colvarCpaCode.MaxLength = 100;
                colvarCpaCode.AutoIncrement = false;
                colvarCpaCode.IsNullable = false;
                colvarCpaCode.IsPrimaryKey = false;
                colvarCpaCode.IsForeignKey = false;
                colvarCpaCode.IsReadOnly = false;
                colvarCpaCode.DefaultSetting = string.Empty;
                colvarCpaCode.ForeignKeyTableName = string.Empty;
                schema.Columns.Add(colvarCpaCode);

                TableSchema.TableColumn colvarStartDate = new TableSchema.TableColumn(schema);
                colvarStartDate.ColumnName = "start_date";
                colvarStartDate.DataType = DbType.DateTime;
                colvarStartDate.MaxLength = 0;
                colvarStartDate.AutoIncrement = false;
                colvarStartDate.IsNullable = false;
                colvarStartDate.IsPrimaryKey = false;
                colvarStartDate.IsForeignKey = false;
                colvarStartDate.IsReadOnly = false;
                colvarStartDate.DefaultSetting = string.Empty;
                colvarStartDate.ForeignKeyTableName = string.Empty;
                schema.Columns.Add(colvarStartDate);

                TableSchema.TableColumn colvarEndDate = new TableSchema.TableColumn(schema);
                colvarEndDate.ColumnName = "end_date";
                colvarEndDate.DataType = DbType.DateTime;
                colvarEndDate.MaxLength = 0;
                colvarEndDate.AutoIncrement = false;
                colvarEndDate.IsNullable = false;
                colvarEndDate.IsPrimaryKey = false;
                colvarEndDate.IsForeignKey = false;
                colvarEndDate.IsReadOnly = false;
                colvarEndDate.DefaultSetting = string.Empty;
                colvarEndDate.ForeignKeyTableName = string.Empty;
                schema.Columns.Add(colvarEndDate);

                TableSchema.TableColumn colvarType = new TableSchema.TableColumn(schema);
                colvarType.ColumnName = "type";
                colvarType.DataType = DbType.Int32;
                colvarType.MaxLength = 0;
                colvarType.AutoIncrement = false;
                colvarType.IsNullable = false;
                colvarType.IsPrimaryKey = false;
                colvarType.IsForeignKey = false;
                colvarType.IsReadOnly = false;
                colvarType.DefaultSetting = @"((0))";
                colvarType.ForeignKeyTableName = string.Empty;
                schema.Columns.Add(colvarType);

                TableSchema.TableColumn colvarRatio = new TableSchema.TableColumn(schema);
                colvarRatio.ColumnName = "ratio";
                colvarRatio.DataType = DbType.Int32;
                colvarRatio.MaxLength = 0;
                colvarRatio.AutoIncrement = false;
                colvarRatio.IsNullable = true;
                colvarRatio.IsPrimaryKey = false;
                colvarRatio.IsForeignKey = false;
                colvarRatio.IsReadOnly = false;
                colvarRatio.DefaultSetting = @"((0))";
                colvarRatio.ForeignKeyTableName = string.Empty;
                schema.Columns.Add(colvarRatio);

                TableSchema.TableColumn colvarMaxCount = new TableSchema.TableColumn(schema);
                colvarMaxCount.ColumnName = "max_count";
                colvarMaxCount.DataType = DbType.Int32;
                colvarMaxCount.MaxLength = 0;
                colvarMaxCount.AutoIncrement = false;
                colvarMaxCount.IsNullable = true;
                colvarMaxCount.IsPrimaryKey = false;
                colvarMaxCount.IsForeignKey = false;
                colvarMaxCount.IsReadOnly = false;
                colvarMaxCount.DefaultSetting = @"((0))";
                colvarMaxCount.ForeignKeyTableName = string.Empty;
                schema.Columns.Add(colvarMaxCount);

                TableSchema.TableColumn colvarMaxTime = new TableSchema.TableColumn(schema);
                colvarMaxTime.ColumnName = "max_time";
                colvarMaxTime.DataType = DbType.Int32;
                colvarMaxTime.MaxLength = 0;
                colvarMaxTime.AutoIncrement = false;
                colvarMaxTime.IsNullable = true;
                colvarMaxTime.IsPrimaryKey = false;
                colvarMaxTime.IsForeignKey = false;
                colvarMaxTime.IsReadOnly = false;
                colvarMaxTime.DefaultSetting = @"((0))";
                colvarMaxTime.ForeignKeyTableName = string.Empty;
                schema.Columns.Add(colvarMaxTime);

                TableSchema.TableColumn colvarEnabled = new TableSchema.TableColumn(schema);
                colvarEnabled.ColumnName = "enabled";
                colvarEnabled.DataType = DbType.Boolean;
                colvarEnabled.MaxLength = 0;
                colvarEnabled.AutoIncrement = false;
                colvarEnabled.IsNullable = false;
                colvarEnabled.IsPrimaryKey = false;
                colvarEnabled.IsForeignKey = false;
                colvarEnabled.IsReadOnly = false;
                colvarEnabled.DefaultSetting = @"((1))";
                colvarEnabled.ForeignKeyTableName = string.Empty;
                schema.Columns.Add(colvarEnabled);

                TableSchema.TableColumn colvarCreator = new TableSchema.TableColumn(schema);
                colvarCreator.ColumnName = "creator";
                colvarCreator.DataType = DbType.AnsiString;
                colvarCreator.MaxLength = 200;
                colvarCreator.AutoIncrement = false;
                colvarCreator.IsNullable = false;
                colvarCreator.IsPrimaryKey = false;
                colvarCreator.IsForeignKey = false;
                colvarCreator.IsReadOnly = false;
                colvarCreator.DefaultSetting = string.Empty;
                colvarCreator.ForeignKeyTableName = string.Empty;
                schema.Columns.Add(colvarCreator);

                TableSchema.TableColumn colvarCreateTime = new TableSchema.TableColumn(schema);
                colvarCreateTime.ColumnName = "create_time";
                colvarCreateTime.DataType = DbType.DateTime;
                colvarCreateTime.MaxLength = 0;
                colvarCreateTime.AutoIncrement = false;
                colvarCreateTime.IsNullable = false;
                colvarCreateTime.IsPrimaryKey = false;
                colvarCreateTime.IsForeignKey = false;
                colvarCreateTime.IsReadOnly = false;
                colvarCreateTime.DefaultSetting = string.Empty;
                colvarCreateTime.ForeignKeyTableName = string.Empty;
                schema.Columns.Add(colvarCreateTime);

                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["BigData"].AddSchema("cpa_main", schema);
            }
        }

        #endregion

        #region Props

        [XmlAttribute("Id")]
        [Bindable(true)]
        public int Id
        {
            get { return GetColumnValue<int>(Columns.Id); }
            set { SetColumnValue(Columns.Id, value); }
        }

        [XmlAttribute("CpaName")]
        [Bindable(true)]
        public string CpaName
        {
            get { return GetColumnValue<string>(Columns.CpaName); }
            set { SetColumnValue(Columns.CpaName, value); }
        }

        [XmlAttribute("CpaCode")]
        [Bindable(true)]
        public string CpaCode
        {
            get { return GetColumnValue<string>(Columns.CpaCode); }
            set { SetColumnValue(Columns.CpaCode, value); }
        }

        [XmlAttribute("StartDate")]
        [Bindable(true)]
        public DateTime StartDate
        {
            get { return GetColumnValue<DateTime>(Columns.StartDate); }
            set { SetColumnValue(Columns.StartDate, value); }
        }

        [XmlAttribute("EndDate")]
        [Bindable(true)]
        public DateTime EndDate
        {
            get { return GetColumnValue<DateTime>(Columns.EndDate); }
            set { SetColumnValue(Columns.EndDate, value); }
        }

        [XmlAttribute("Type")]
        [Bindable(true)]
        public int Type
        {
            get { return GetColumnValue<int>(Columns.Type); }
            set { SetColumnValue(Columns.Type, value); }
        }

        [XmlAttribute("Ratio")]
        [Bindable(true)]
        public int? Ratio
        {
            get { return GetColumnValue<int?>(Columns.Ratio); }
            set { SetColumnValue(Columns.Ratio, value); }
        }

        [XmlAttribute("MaxCount")]
        [Bindable(true)]
        public int? MaxCount
        {
            get { return GetColumnValue<int?>(Columns.MaxCount); }
            set { SetColumnValue(Columns.MaxCount, value); }
        }

        [XmlAttribute("MaxTime")]
        [Bindable(true)]
        public int? MaxTime
        {
            get { return GetColumnValue<int?>(Columns.MaxTime); }
            set { SetColumnValue(Columns.MaxTime, value); }
        }

        [XmlAttribute("Enabled")]
        [Bindable(true)]
        public bool Enabled
        {
            get { return GetColumnValue<bool>(Columns.Enabled); }
            set { SetColumnValue(Columns.Enabled, value); }
        }

        [XmlAttribute("Creator")]
        [Bindable(true)]
        public string Creator
        {
            get { return GetColumnValue<string>(Columns.Creator); }
            set { SetColumnValue(Columns.Creator, value); }
        }

        [XmlAttribute("CreateTime")]
        [Bindable(true)]
        public DateTime CreateTime
        {
            get { return GetColumnValue<DateTime>(Columns.CreateTime); }
            set { SetColumnValue(Columns.CreateTime, value); }
        }

        #endregion
        
        #region Typed Columns


        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }



        public static TableSchema.TableColumn CpaNameColumn
        {
            get { return Schema.Columns[1]; }
        }



        public static TableSchema.TableColumn CpaCodeColumn
        {
            get { return Schema.Columns[2]; }
        }



        public static TableSchema.TableColumn StartDateColumn
        {
            get { return Schema.Columns[3]; }
        }



        public static TableSchema.TableColumn EndDateColumn
        {
            get { return Schema.Columns[4]; }
        }



        public static TableSchema.TableColumn TypeColumn
        {
            get { return Schema.Columns[5]; }
        }



        public static TableSchema.TableColumn RatioColumn
        {
            get { return Schema.Columns[6]; }
        }



        public static TableSchema.TableColumn MaxCountColumn
        {
            get { return Schema.Columns[7]; }
        }



        public static TableSchema.TableColumn MaxTimeColumn
        {
            get { return Schema.Columns[8]; }
        }



        public static TableSchema.TableColumn EnabledColumn
        {
            get { return Schema.Columns[9]; }
        }



        public static TableSchema.TableColumn CreatorColumn
        {
            get { return Schema.Columns[10]; }
        }



        public static TableSchema.TableColumn CreateTimeColumn
        {
            get { return Schema.Columns[11]; }
        }



        #endregion
        
        #region Columns Struct
        public struct Columns
        {
            public static string Id = @"id";
            public static string CpaName = @"cpa_name";
            public static string CpaCode = @"cpa_code";
            public static string StartDate = @"start_date";
            public static string EndDate = @"end_date";
            public static string Type = @"type";
            public static string Ratio = @"ratio";
            public static string MaxCount = @"max_count";
            public static string MaxTime = @"max_time";
            public static string Enabled = @"enabled";
            public static string Creator = @"creator";
            public static string CreateTime = @"create_time";

        }
        #endregion

        #region Update PK Collections

        #endregion

        #region Deep Save

        #endregion
    }
}

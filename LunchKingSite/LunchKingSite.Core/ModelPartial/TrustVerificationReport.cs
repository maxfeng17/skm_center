﻿using System;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using log4net;

namespace LunchKingSite.DataOrm
{
    public partial class TrustVerificationReport
    {
        ///// <summary>
        ///// 期初未兌換餘額
        ///// </summary>
        public int BeginningNonexchangeAmount { get; set; }
        /// <summary>
        /// 期末未兌換餘額
        /// </summary>
        public int ClosingNonexchangeAmount { get; set; }
        /// <summary>
        /// 上期滿一年以上的信託總額
        /// </summary>
        public int OverOneYearTotalOfLastMonth { get; set; }
        /// <summary>
        /// 本期淨增加滿一年以上信託總額,為減少總額加項
        /// </summary>
        public int OverOneYearAmount { get; set; }

        /// <summary>
        /// 憑證(未滿一年)總金額
        /// </summary>
        public int ScashLessYearAmount { get; set; }
        /// <summary>
        /// 憑證(未滿一年)總筆數
        /// </summary>
        public int ScashLessYearCount { get; set; }
        /// <summary>
        /// 憑證(滿一年)總金額
        /// </summary>
        public int ScashOverYearAmount { get; set; }
        /// <summary>
        /// 憑證(滿一年)總筆數
        /// </summary>
        public int ScashOverYearCount { get; set; }
        /// <summary>
        /// 購物金總金額
        /// </summary>
        public int UserAmount { get; set; }
        /// <summary>
        /// 購物金總筆數
        /// </summary>
        public int UserCount { get; set; }
        /// <summary>
        /// zip file
        /// </summary>
        public byte[] ZipFile { get; set; }
    }
}

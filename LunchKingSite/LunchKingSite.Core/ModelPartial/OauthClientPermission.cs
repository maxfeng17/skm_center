using System;
using LunchKingSite.Core;

namespace LunchKingSite.DataOrm
{
    public partial class OauthClientPermission
    {
        public TokenScope ScopeEnum
        {
            get { return (TokenScope)this.Scope; }
            set { this.Scope = (int)value; }
        }
    }
}

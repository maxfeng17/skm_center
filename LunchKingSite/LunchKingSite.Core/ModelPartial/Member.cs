using System;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;

namespace LunchKingSite.DataOrm
{
    public partial class Member
    {
        public const string _GUEST_MEMBER = "0級會員";
        /// <summary>
        /// 會員預設圖片，顯示在登入名字左邊
        /// </summary>
        public string DefaultPic = "../Themes/PCweb/images/17p_sn07_12.jpg";

        public string DisplayName
        {
            get
            {
                if (string.IsNullOrEmpty(this.LastName) == false && string.IsNullOrEmpty(this.FirstName) == false)
                {
                    if (Helper.IsChinese(FirstName[0]) == false || Helper.IsChinese(LastName[0]) == false)
                    {
                        return string.Format("{0} {1}", this.FirstName, this.LastName);
                    }
                    //中文姓名
                    return string.Format("{0}{1}", this.LastName, this.FirstName);
                }                
                if (string.IsNullOrEmpty(this.LastName) == false)
                {
                    return this.LastName;
                }
                if (string.IsNullOrEmpty(this.FirstName) == false)
                {
                    return this.FirstName;
                }
                return string.Empty;
            }
        }
        /// <summary>
        /// 盜刷嫌疑人
        /// </summary>
        public bool IsFraudSuspect
        {
            get
            {
                ISysConfProvider config = ProviderFactory.Instance().GetConfig();
                return config.AutoBlockCreditcardFraudSuspect && "盜刷嫌疑人" == this.Comment;
            }
        }
    }
}

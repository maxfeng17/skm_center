﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.DataOrm
{
    public class ShoppingCartConfirmModel
    {
        public int Id { get; set; }
        public Guid BusinessHourGuid { get; set; }
        public int UniqueId { get; set; }
        public string DealName { get; set; }
        public string ChangeLog { get; set; }
        public DateTime CreateTime { get; set; }
    }

    public class ShoppingCartManageList
    {
        public int Id { get; set; }
        public string UniqueId { get; set; }
        public string SellerName { get; set; }
        public int FreightsStatus { get; set; }
        public string FreightsStatusName { get; set; }
        public string FreightsName { get; set; }
        public int NoFreightLimit { get; set; }
        public int Freights { get; set; }
        public DateTime CreateTime { get; set; }
    }
}

﻿using System;

namespace LunchKingSite.DataOrm
{
    public interface IReadOnlyDealProperty
    {
        Guid BusinessHourGuid { get; }
        int? DealType { get; }
        int? SellerVerifyType { get; }
        int? DealAccBusinessGroupId { get; }
        string DealEmpName { get; }
        string CreateId { get; }
        DateTime? CreateTime { get; }
        string ModifyId { get; }
        DateTime? ModifyTime { get; }
        int UniqueId { get; }
        string CityList { get; }
        int? DeliveryType { get; }
        bool ShoppingCart { get; }
        int ComboPackCount { get; }
        Guid? AncestorBusinessHourGuid { get; }
        bool IsTravelcard { get; }
        int EntrustSell { get; }
        string TravelPlace { get; }
        bool MultipleBranch { get; }
        string EmpNo { get; }
        int? QuantityMultiplier { get; }
        bool IsDailyRestriction { get; }
        bool IsContinuedSequence { get; }
        bool IsContinuedQuantity { get; }
        int ContinuedQuantity { get; }
        string ActivityUrl { get; }
        string LabelTagList { get; }
        string LabelIconList { get; }
        int CouponCodeType { get; }
        int PinType { get; }
        int BookingSystemType { get; }
        int AdvanceReservationDays { get; }
        int CouponUsers { get; }
        bool IsReserveLock { get; }
        string CustomTag { get; }
        decimal TmallRmbPrice { get; }
        decimal TmallRmbExchangeRate { get; }
        decimal ExchangePrice { get; }
        bool IsQuantityMultiplier { get; }
        bool IsAveragePrice { get; }
        bool IsMergeCount { get; }
        bool IsZeroActivityShowCoupon { get; }
        bool IsTravelDeal { get; }
        Guid? AncestorSequenceBusinessHourGuid { get; }
        bool IsLongContract { get; }
        bool Installment3months { get; }
        bool Installment6months { get; }
        bool Installment12months { get; }
        bool DenyInstallment { get; }
        int? SaleMultipleBase { get; }
        int? ShipType { get; }
        int? ShippingdateType { get; }
        int? Shippingdate { get; }
        int? ProductUseDateStartSet { get; }
        int? ProductUseDateEndSet { get; }
        int PresentQuantity { get; }
        int DevelopeSalesId { get; }
        int GroupCouponAppStyle { get; }
        int? NewDealType { get; }
        int? VerifyType { get; }
        int DiscountType { get; }
        int Discount { get; }
        bool? IsExperience { get; }
        bool CompleteCopy { get; }
        int? CouponSeparateDigits { get; }

        bool IsLoaded { get; }
    }

    public partial class DealProperty : IReadOnlyDealProperty
    {
    }
}

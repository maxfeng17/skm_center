using System;

namespace LunchKingSite.DataOrm
{
    public partial class MemberAuthInfo
    {
        /// <summary>
        /// 上次的申請時間是否已過期
        /// </summary>
        public bool LinkExpired
        {
            get
            {
                return DateTime.Compare(DateTime.Now.Date, this.AuthDate.Value.Date.AddDays(7)) > 0;
            }
        }

        public bool HasAuthData
        {
            get
            {
                if (this.IsLoaded == false || this.AuthDate == null || string.IsNullOrEmpty(this.Email))
                {
                    return false;
                }
                return true;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.DataOrm
{
    public partial class ViewDiscountLimit
    {
        public List<int> ChannelIdList { get; set; }
        public string ChannelStr { get; set; }
    }
}

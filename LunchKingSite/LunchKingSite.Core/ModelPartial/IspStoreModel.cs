﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.Core.ModelPartial
{
    public class IspShipmentNumberModel
    {
        public string OrderKey { get; set; }
        public string ShipmentNo { get; set; }
    }

    public class IspShipmentNumberRespModel
    {
        public Guid StoreKey { get; set; }
        public List<IspShipmentNumberModel> ShipmentNumberItemList { get; set; }
    }

    public class ShippingDeliveryRtnModel
    {
        public string OrderKey { get; set; }
        public string Message { get; set; }
        public bool IsSuccess { get; set; }
    }

    public class IspShipmentPdfRespModel
    {
        public string pdfDownloadUrl { get; set; }
        public List<IspShipmentPdfListRespModel> SuccessData { get; set; }
        public List<IspShipmentPdfFailModel> FailData { get; set; }
    }

    public class IspShipmentPdfListRespModel
    {
        public List<IspShipmentNumberModel> ShipmentNumberItemList { get; set; }
    }

    public class IspShipmentPdfFailModel
    {
        public string OrderKey { get; set; }
        public string Message { get; set; }
    }

    public class ShipmentOrderModel
    {
        public Guid StoreKey { get; set; }
        public IspOrderType OrderType { get; set; }
        public List<string> OrderList { get; set; }
        public ServiceChannel channelType { get; set; }
        public bool isReTag { get; set; }
    }

}

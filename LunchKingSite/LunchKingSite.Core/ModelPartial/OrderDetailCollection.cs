﻿using System.Linq;
using LunchKingSite.Core;

namespace LunchKingSite.DataOrm
{
    public partial class OrderDetailCollection
    {
        /// <summary>
        /// 運費明細不計算在內
        /// </summary>
        public decimal OrderedTotal
        {
            get { return this.Where(t => t.Status != (int)OrderDetailStatus.SystemEntry).Sum(t => t.Total.GetValueOrDefault()); }
        }

        /// <summary>
        /// 運費明細不計算在內
        /// </summary>
        public int OrderedQuantity
        {
            get { return this.Where(t => t.Status != (int)OrderDetailStatus.SystemEntry).Sum(t => t.ItemQuantity); }
        }

        public string CreateTimeTicks
        {
            get 
            {
                if (this.FirstOrDefault() == null) 
                {
                    return string.Empty;
                } 
                else 
                {
                    return this.FirstOrDefault().CreateTime.Ticks.ToString();
                }
            }
        }
    }
}

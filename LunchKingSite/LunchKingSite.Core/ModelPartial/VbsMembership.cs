﻿using LunchKingSite.Core;
using System;

namespace LunchKingSite.DataOrm
{
    public partial class VbsMembership
    {
        public bool IsFirstLogin 
        {
            get { return !this.LastLoginDate.HasValue || !this.LastPasswordChangedDate.HasValue; }
        }
        
        public VbsMembershipAccountType VbsMembershipAccountType
        {
            get { return (VbsMembershipAccountType) this.AccountType; }
        }

        public bool IsPasswordResetNeeded
        {
            get
            {
                if(this.LastPasswordChangedDate.HasValue)
                {
                    // 樓上吱吱叫，改成6個月要改一次密碼
                    return DateTime.Now > this.LastPasswordChangedDate.Value.AddMonths(6);
                }
                else
                {
                    return true;
                }
            }
        }

        public bool IsReadInfoProtectNeeded
        {
            get
            {
                if (this.LastInfoProtectReadDate.HasValue)
                {
                    return DateTime.Now > this.LastInfoProtectReadDate.Value.AddMonths(3);
                }
                else
                {
                    return true;
                }
            }
        }
        public bool IsAgreePersonalInfoClause
        {
            get
            {
                return this.PersonalInfoClauseAgreeDate.HasValue;
            }
        }
    }
}

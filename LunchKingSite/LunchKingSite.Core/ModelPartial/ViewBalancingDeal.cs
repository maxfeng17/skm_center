﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.Core;

namespace LunchKingSite.DataOrm
{
    public partial class ViewBalancingDeal
    {
        public RemittanceFrequency RemittanceFrequency
        {
            get
            {
                switch ((RemittanceType)this.RemittanceType)
                {
                    case Core.RemittanceType.AchWeekly:
                    case Core.RemittanceType.ManualWeekly:
                        return RemittanceFrequency.Weekly;
                    case Core.RemittanceType.AchMonthly:
                    case Core.RemittanceType.ManualMonthly:
                        return RemittanceFrequency.Monthly;
                    default:
                        return RemittanceFrequency.Unknown;
                }
            }
        }
    }
}

﻿using System;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using log4net;

namespace LunchKingSite.DataOrm
{
    public partial class EinvoiceMain
    {
        private ILog logger = LogManager.GetLogger(typeof(EinvoiceMain));
        private ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        private IOrderProvider op = ProviderFactory.Instance().GetProvider<IOrderProvider>();

        /// <summary>
        /// 通用判斷新(載具)、舊版是否為三聯式發票
        /// </summary>
        public bool IsEinvoice3
        {
            get
            {
                if (this.InvoiceMode2 == (int)Core.InvoiceMode2.Triplicate)
                {
                    return true;
                }
                return false;
            }
        }

        public bool IsEinvoice2
        {
            get
            {
                if (this.InvoiceMode2 == (int)Core.InvoiceMode2.Duplicate)
                {
                    return true;
                }
                return false;
            }
        }

        public bool IsPrintMark
        {
            get
            {
                if (this.Version == (int)InvoiceVersion.Old || this.Version == (int)InvoiceVersion.CarrierEra)
                {
                    return this.InvoiceRequestTime.HasValue;
                }
                CheckVersionValid("IsPrintMark");
                return false;

            }
        }

        public bool IsDonateMark
        {
            get
            {
                if (this.Version == (int)InvoiceVersion.Old || this.Version == (int)InvoiceVersion.CarrierEra)
                {
                    return string.IsNullOrEmpty(this.LoveCode) == false;
                }
                CheckVersionValid("IsDonateMark");
                return false;
            }
        }

        /// <summary>
        /// 是否跨期
        /// </summary>
        public bool IsIntertemporal
        {
            get
            {
                if (this.InvoiceNumber != null && this.InvoiceNumber.Length == 10)
                {
                    return op.EinvoiceSerialCollectionGetByInvInfo(this.InvoiceNumber, this.InvoiceNumberTime.Value).EndDate < DateTime.Now;
                }
                return false;
            }
        }

        /// <summary>
        /// 是否跨期(加緩衝)
        /// </summary>
        public bool IsIntertemporalWithBuffer
        {
            get
            {
                if (this.InvoiceNumber != null && this.InvoiceNumber.Length == 10)
                {
                    return op.EinvoiceSerialCollectionGetByInvInfo(this.InvoiceNumber, this.InvoiceNumberTime.Value).EndDate.AddDays(config.InvoiceBufferDays) < DateTime.Now;
                }
                return false;
            }
        }

        public bool IsAllowPaperd {
            get
            {
                return InvoicePaperedTime == null &&
                       !IsIntertemporal && CarrierType == (int)Core.CarrierType.None ||
                       IsIntertemporal && InvoiceStatus == (int)EinvoiceType.C0401;
            }
        }

        public bool CopyPrint { get; set; }

        public bool SinglePrint { get; set; }

        private void CheckVersionValid(string pname)
        {
            logger.Warn("注意資料有問題 不預期 EinvoiceMain 的 Version 會出現 " + this.Version + " 引發來源:" + pname);
        }


        public string IssueCompanyName
        {
            get
            {
                if (this.InvoiceNumberTime == null)
                {
                    return string.Empty;
                }
                return (this.InvoiceNumberTime >= config.ContactToPayeasy &&
                        this.InvoiceNumberTime < config.PayeasyToContact)
                        ? "康迅"
                        : this.InvoiceNumberTime < config.ContactToPayeasy
                            ? "17Life康太"
                            : "康太";
            }
        }

        /// <summary>
        /// XXX稅捐稽徵處第 X 號函核准使用，用在發票明細的顯示
        /// </summary>
        public string GovernmentIssuedCode
        {
            get
            {
                if (this.InvoiceNumberTime == null)
                {
                    return string.Empty;
                }
                return (this.InvoiceNumberTime >= config.ContactToPayeasy &&
                        this.InvoiceNumberTime < config.PayeasyToContact)
                        ? "本發票─台北市稅捐稽徵處中南分處90年10月11日北市稽中南(甲)字第9063187700號函核准使用"
                        : this.InvoiceNumberTime < config.ContactToPayeasy
                            ? "本發票─台北市稅捐稽徵處中南分處90年10月11日北市稽中南(甲)字第8020535800號函核准使用"
                            : "本發票依財政部臺北市國稅局中南稽徵所99年8月20日財北國稅中南營業一字第0990017821號函核准使用";
            }
        }

        public string InvoicePassAsString
        {
            get
            {
                if (this.Version == (int)InvoiceVersion.Old)
                {
                    return "個人識別碼:" + InvoicePass;
                }
                if (this.Version == (int)InvoiceVersion.CarrierEra)
                {
                    return "電子發票隨機碼:" + InvoicePass;
                }
                throw new ArgumentOutOfRangeException();
            }
        }
    }
}

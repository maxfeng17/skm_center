﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.DataOrm
{
    public partial class ViewBrandItem
    {
        public decimal OrderedTotal { get; set; }
        public decimal GrossMargin { get; set; }
        public int GrossMarginStatus { get; set; }
        public int BusinessHourStatus { get; set; }
        public bool DiscountLimtType { get; set; }
    }
}

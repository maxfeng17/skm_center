﻿using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.Core.ModelPartial
{
    public class CreditcardBankInfoModel: CreditcardBankInfo
    {
        public string CardTypeName { get; set; }
    }
}

using System;

namespace LunchKingSite.DataOrm
{
    public partial class HiDealDeal
    {
        /// <summary>
        /// 是否開檔3日內，Visa優先檔3日後視為一般檔
        /// </summary>
        public bool WithinVisaPriorityDayLimit
        {
            get
            {
                return (DateTime.Now - DealStartTime.Value).Days < 3;
            }
        }
    }
}

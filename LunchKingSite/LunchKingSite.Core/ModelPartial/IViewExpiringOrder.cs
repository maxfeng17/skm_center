﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.DataOrm
{

    public interface IViewExpiringOrder
    {
        DateTime BusinessHourOrderTimeS { get; set; }
        DateTime BusinessHourOrderTimeE { get; set; }
        DateTime? LastShipDate { get; set; }
        int UniqueId { get; set; }
        string ItemName { get; set; }
        Guid BusinessHourGuid { get; set; }
        int? ShipType { get; set; }
        int? ShippingdateType { get; set; }
        int? Shippingdate { get; set; }
        int? ProductUseDateEndSet { get; set; }
        int? OrderCount { get; set; }
        Guid SellerGuid { get; set; }
        string OrderList { get; set; }
    }
}

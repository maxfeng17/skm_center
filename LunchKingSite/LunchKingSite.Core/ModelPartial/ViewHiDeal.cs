﻿using System;

namespace LunchKingSite.DataOrm
{
    public enum VisaDealType
    {
    /// <summary>
    /// 一般檔次
    /// </summary>
        NotForVisa, 
        /// <summary>
        /// Visa卡專屬
        /// </summary>
        VisaPrivate, 
        /// <summary>
        /// Visa卡優先
        /// </summary>
        VisaPriority
    }

    public partial class ViewHiDeal
    {
        public VisaDealType VisaType
        {
            get
            {
                if (SpecialCodeId == (int) VisaDealType.VisaPriority)
                {
                    if ((DateTime.Now - this.DealStartTime.Value).Days < 3)
                    {
                        return VisaDealType.VisaPriority;
                    } 
                    else
                    {
                        return VisaDealType.NotForVisa;
                    }
                }
                else if (SpecialCodeId == (int) VisaDealType.VisaPrivate)
                {
                    return VisaDealType.VisaPrivate;
                }
                return VisaDealType.NotForVisa;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.DataOrm
{
    public interface IDealTimeSlot
    {
        Guid BusinessHourGuid { get; }
        int CityId { get; }
        int Sequence { get; }
        DateTime EffectiveStart { get; }
        DateTime EffectiveEnd { get; }
        int Status { get; }
        bool IsInTurn { get; }
        bool IsLockSeq { get; }
        bool IsHotDeal { get; }
    }

    public class DealTimeSlotModel : IDealTimeSlot
    {
        public DealTimeSlotModel()
        {
        }

        public DealTimeSlotModel(DealTimeSlot dts)
        {
            this.BusinessHourGuid = dts.BusinessHourGuid;
            this.CityId = dts.CityId;
            this.Sequence = dts.Sequence;
            this.EffectiveStart = dts.EffectiveStart;
            this.EffectiveEnd = dts.EffectiveEnd;
            this.Status = dts.Status;
            this.IsInTurn = dts.IsInTurn;
            this.IsLockSeq = dts.IsLockSeq;
            this.IsHotDeal = dts.IsHotDeal;
        }

        public Guid BusinessHourGuid { get; set; }
        public int CityId { get; set; }
        public int Sequence { get; set; }
        public DateTime EffectiveStart { get; set; }
        public DateTime EffectiveEnd { get; set; }
        public int Status { get; set; }
        public bool IsInTurn { get; set; }
        public bool IsLockSeq { get; set; }
        public bool IsHotDeal { get; set; }

        public override int GetHashCode()
        {
            return string.Format("{0}:{1}:{2}:{3}:{4}:{5}:{6}:{7}"
                , BusinessHourGuid, CityId, EffectiveStart, Sequence, EffectiveEnd, Status, IsInTurn, IsLockSeq)
                .GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }

            DealTimeSlot castObj = obj as DealTimeSlot;
            if (castObj == null)
            {
                return false;
            }

            return GetHashCode() == castObj.GetHashCode();
        }
    }

    public partial class DealTimeSlot : IDealTimeSlot
    {
        public override int GetHashCode()
        {
            return string.Format("{0}:{1}:{2}:{3}:{4}:{5}:{6}:{7}"
                , BusinessHourGuid, CityId, EffectiveStart, Sequence, EffectiveEnd, Status, IsInTurn, IsLockSeq)
                .GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }

            DealTimeSlot castObj = obj as DealTimeSlot;
            if (castObj == null)
            {
                return false;
            }

            return GetHashCode() == castObj.GetHashCode();
        }
    }
}

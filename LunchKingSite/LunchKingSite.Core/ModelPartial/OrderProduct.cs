﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace LunchKingSite.DataOrm
{
    public partial class OrderProduct
    {
        public string DealName
        {     
            get
            {
                if (!string.IsNullOrEmpty(this.Name))
                {
                    string[] arrName = this.Name.Split("\n");
                    if (arrName.Count() > 1)
                    {
                        return arrName[0];
                    }
                    else
                    {
                        return string.Empty;
                    }
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        public string ProductName
        {
            get
            {
                if (!string.IsNullOrEmpty(this.Name))
                {
                    string[] arrName = this.Name.Split("\n");
                    if (arrName.Count() > 1)
                    {
                        return arrName[1];
                    }
                    else
                    {
                        return arrName[0];
                    }
                }
                else
                {
                    return string.Empty;
                }
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.DataOrm
{
    public partial class Seller
    {
        public class MultiContracts
        {
            public string Type { get; set; }
            public string ContactPersonName { get; set; }
            public string SellerTel { get; set; }
            public string SellerMobile { get; set; }
            public string SellerFax { get; set; }
            public string ContactPersonEmail { get; set; }
            public string Others { get; set; }
        }

        public class LevelDetail
        {
            /// <summary>
            /// 非在地餐飲類型店家規模
            /// </summary>
            public string NoCaterLv { get; set; }
            /// <summary>
            /// 在地餐飲類型-店家數
            /// </summary>
            public string Sellers { get; set; }
            /// <summary>
            /// 在地餐飲類型-平均客單價
            /// </summary>
            public string AvgPrice { get; set; }
            /// <summary>
            /// 在地餐飲類型-平均座位數
            /// </summary>
            public string AvgSeat { get; set; }
            /// <summary>
            /// 在地餐飲類型-滿桌率(%)
            /// </summary>
            public string TurnoverRate { get; set; }
            /// <summary>
            /// 在地餐飲類型-翻桌次
            /// </summary>
            public string TurnoverFreq { get; set; }
            /// <summary>
            /// 在地餐飲類型-人潮流量
            /// </summary>
            public string CrowdsFlow { get; set; }
        }
    }

    public class SellerProposal
    {
        /// <summary>
        /// 商家bid
        /// </summary>
        public Guid sellerGuid { get; set; }
        /// <summary>
        /// 提案單號
        /// </summary>
        public int Pid { get; set; }
        /// <summary>
        /// 提案狀態
        /// </summary>
        public string Status { get; set; }
        public string StatusName { get; set; }
        /// <summary>
        /// 檔期
        /// </summary>
        public string BusinessHourOrderTime { get; set; }
        public string BusinessHourOrderTimeS { get; set; }
        public string BusinessHourOrderTimeE { get; set; }

        /// <summary>
        /// 檔號
        /// </summary>
        public string UniqueId { get; set; }
        /// <summary>
        /// Bid
        /// </summary>
        public string BusinessHourId { get; set; }
        public bool BusinessCreated { get; set; }
        /// <summary>
        /// 負責業務
        /// </summary>
        public int DeSalesId { get; set; }
        public string DeSalesName { get; set; }
        public int OpSalesId { get; set; }
        public string OpSalesName { get; set; }
        /// <summary>
        /// 消費方式(好康/宅配)
        /// </summary>
        public string DeliveryType { get; set; }
        /// <summary>
        /// 品牌名稱
        /// </summary>
        public string BrandName { get; set; }
        /// <summary>
        /// 商品類型
        /// </summary>
        public int DealType { get; set; }
        public int DealSubType { get; set; }
        /// <summary>
        /// 店家單據開立方式
        /// </summary>
        public int VendorReceiptType { get; set; }
        /// <summary>
        /// 出貨類型
        /// </summary>
        public string ShipType { get; set; }
        public int ShippingdateType { get; set; }
        public string txtShipText1 { get; set; }
        public string txtShipText2 { get; set; }
        public string txtShipText3 { get; set; }
        public string ShipTypeOther { get; set; }
        /// <summary>
        /// 配送方式
        /// </summary>
        public int DeliveryMethod { get; set; }
        /// <summary>
        /// 平行輸入
        /// </summary>
        public bool IsParallelImportation { get; set; }
        /// <summary>
        /// 圖片來源
        /// </summary>
        public bool Photographers { get; set; }
        public bool SellerPhoto { get; set; }
        public bool FTPPhoto { get; set; }
        /// <summary>
        /// 規格功能/主要成份
        /// </summary>
        public string ProductSpec { get; set; }
        /// <summary>
        /// 醫療 / 妝廣字號
        /// </summary>
        public bool IsBeauty { get; set; }
        public string Beauty { get; set; }
        /// <summary>
        /// 即期品
        /// </summary>
        public bool IsExpiringProduct { get; set; }
        public string ExpiringProduct { get; set; }
        /// <summary>
        /// 檢驗規定
        /// </summary>
        public bool IsInspectRule { get; set; }
        public string InspectRule { get; set; }
        /// <summary>
        /// 備註說明
        /// </summary>
        public string ContractMemo { get; set; }
        /// <summary>
        /// 店家單據開立方式(其他)
        /// </summary>
        public string AccountingMessage { get; set; }
        /// <summary>
        /// 檔次資料
        /// </summary>
        public string MultiDeals { get; set; }
        /// <summary>
        /// 本檔主打星
        /// </summary>
        public string IsDealStar { get; set; }
        public string DealStar { get; set; }
        /// <summary>
        /// 檔次特色
        /// </summary>
        public string IsDealCharacterToHouse { get; set; }
        public string DealCharacterToHouse1 { get; set; }
        public string DealCharacterToHouse2 { get; set; }
        public string DealCharacterToHouse3 { get; set; }
        public string DealCharacterToHouse4 { get; set; }

        public string StoreStory { get; set; }

        public bool IsMediaReportPic { get; set; }
        public int MediaTextCount { get; set; }
        public int rdbMediaReport1 { get; set; }
        public int rdbMediaReport2 { get; set; }
        public int rdbMediaReport3 { get; set; }
        public int rdbMediaReport4 { get; set; }
        public int rdbMediaReport5 { get; set; }

        public string txtMediaReport1 { get; set; }
        public string txtMediaReport2 { get; set; }
        public string txtMediaReport3 { get; set; }
        public string txtMediaReport4 { get; set; }
        public string txtMediaReport5 { get; set; }

        public bool FileDone { get; set; }

        /// <summary>
        /// 媒體報導
        /// </summary>
        public bool IsMediaReportFlag { get; set; }

    }

    public class SellerProposalHouseModel
    {
        public SellerProposalHouseModel()
        {
            ProductionAssignList = new List<string>();
        }

        /// <summary>
        /// 商家guid
        /// </summary>
        public Guid SellerGuid { get; set; }
        /// <summary>
        /// 商家編號
        /// </summary>
        public string SellerId { get; set; }
        /// <summary>
        /// 商家名稱
        /// </summary>
        public string SellerName { get; set; }
        /// <summary>
        /// 提案單號
        /// </summary>
        public int ProId { get; set; }
        /// <summary>
        /// 檔號
        /// </summary>
        public string UniqueId { get; set; }
        /// <summary>
        /// Bid
        /// </summary>
        public string BusinessHourId { get; set; }
        /// <summary>
        /// 特殊標記
        /// </summary>
        public int SpecialFlag { get; set; }
        /// <summary>
        /// 商品類型
        /// </summary>
        public int? DealType1 { get; set; }
        public int? DealType2 { get; set; }
        public int OpSalesId { get; set; }
        public string OpSalesName { get; set; }
        /// <summary>
        /// 網站露出商品名稱
        /// </summary>
        public string AppTitle { get; set; }
        /// <summary>
        /// 品牌名稱
        /// </summary>
        public string BrandName { get; set; }
        /// <summary>
        /// 商品名稱
        /// </summary>
        public string DealName { get; set; }
        /// <summary>
        /// 店家單據開立方式
        /// </summary>
        public int? VendorReceiptType { get; set; }
        /// <summary>
        /// 其他合約開立方式
        /// </summary>
        public string AccountingMessage { get; set; }
        /// <summary>
        /// 出貨類型
        /// </summary>
        public int ProShipType { get; set; }
        public string ProShipDays { get; set; }
        public string ShipType { get; set; }
        public int ShippingdateType { get; set; }
        public string txtShipText1 { get; set; }
        public string txtShipText2 { get; set; }
        public string txtShipText3 { get; set; }
        public string ShipTypeOther { get; set; }
        /// <summary>
        /// 續接檔次數量
        /// </summary>
        public Guid? AncestorBid { get; set; }
        /// <summary>
        /// 配送方式
        /// </summary>
        public int? DeliveryMethod { get; set; }
        /// <summary>
        /// 不適用鑑賞期
        /// </summary>
        public bool TrialPeriod { get; set; }
        /// <summary>
        /// 商品來源
        /// </summary>
        public int? DealSource { get; set; }
        /// <summary>
        /// 銷售通路
        /// </summary>
        public string AgentChannels { get; set; }
        /// <summary>
        /// 檔次行銷文案
        /// </summary>
        public string DealContent { get; set; }
        /// <summary>
        /// 商品詳細介紹
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// 商品規格
        /// </summary>
        public string ProductSpec { get; set; }
        /// <summary>
        /// 檢驗字號
        /// </summary>
        public string InspectRule { get; set; }
        /// <summary>
        /// 一姬說好康(必買特色)
        /// </summary>
        public string Remark { get; set; }
        /// <summary>
        /// 權益說明
        /// </summary>
        public string Restrictions { get; set; }
        /// <summary>
        /// 多規格
        /// </summary>
        public string MultiDeals { get; set; }
        /// <summary>
        /// PC版圖片
        /// </summary>
        public string ImagePath { get; set; }
        /// <summary>
        /// App版圖片
        /// </summary>
        public string AppDealPic { get; set; }
        /// <summary>
        /// 去背圖片
        /// </summary>
        public string RemoveBGPic { get; set; }
        /// <summary>
        /// 去背圖片網址
        /// </summary>
        public string RemoveBGPicUrl { get; set; }
        ///// <summary>
        ///// 提案單狀態
        ///// </summary>
        //public int ProposalFlag { get; set; }
        /// <summary>
        /// 商家提案單狀態
        /// </summary>
        public int SellerProposalFlag { get; set; }
        /// <summary>
        /// 商家提案或業務提案
        /// </summary>
        public int ProposalCreatedType { get; set; }
        /// <summary>
        /// 建檔人員
        /// </summary>
        public string CreateId { get; set; }

        public int ApplyFlag { get; set; }
        public int ApproveFlag { get; set; }
        public int BusinessCreateFlag { get; set; }
        public int BusinessFlag { get; set; }
        public int EditFlag { get; set; }
        public int ListingFlag { get; set; }

        /// <summary>
        /// 是否已建檔
        /// </summary>
        public bool IsCreateBusinessHour { get; set; }
        /// <summary>
        /// 合約附註說明
        /// </summary>
        public string ContractMemo { get; set; }
        /// <summary>
        /// 寄倉檔次
        /// </summary>
        public bool Consignment { get; set; }
        /// <summary>
        /// 銀行合作
        /// </summary>
        public bool IsBankDeal { get; set; }
        /// <summary>
        /// 每日一物
        /// </summary>
        public bool EveryDayNewDeal { get; set; }
        /// <summary>
        /// 本檔不可使用折價券
        /// </summary>
        public bool NotAllowedDiscount { get; set; }
        /// <summary>
        /// 折數改為特選
        /// </summary>
        public bool NoDiscountShown { get; set; }
        /// <summary>
        /// 開放配送外島
        /// </summary>
        public bool DeliveryIslands { get; set; }
        /// <summary>
        /// 行銷策展
        /// </summary>
        public string MarketingResource { get; set; }
        /// <summary>
        /// 關鍵字 
        /// </summary>
        public string PicAlt { get; set; }
        /// <summary>
        /// 比價資訊
        /// </summary>
        public string SaleMarketAnalysis { get; set; }
        /// <summary>
        /// 開立免稅發票給消費者
        /// </summary>
        public bool NoTax { get; set; }
        /// <summary>
        /// 是否申請製檔
        /// </summary>
        public bool IsProduction { get; set; }
        /// <summary>
        /// 製檔說明
        /// </summary>
        public string ProductionDescription { get; set; }
        public List<string> ProductionAssignList { get; set; }
        /// <summary>
        /// 出帳方式
        /// </summary>
        public int RemittanceType { get; set; }
        /// <summary>
        /// 上檔頻道
        /// </summary>
        public string Category { get; set; }
        /// <summary>
        /// 上檔日
        /// </summary>
        public string BusinessHourOrderTimeS { get; set; }
        /// <summary>
        /// 結檔日
        /// </summary>
        public string BusinessHourOrderTimeE { get; set; }
        /// <summary>
        /// 配送開始日
        /// </summary>
        public string BusinessHourDeliveryTimeS { get; set; }
        /// <summary>
        /// 配送結束日
        /// </summary>
        public string BusinessHourDeliveryTimeE { get; set; }

        public string ImagePcCnt { get; set; }
        public string ImageAppCnt { get; set; }
        public string ImageRemoveBGCnt { get; set; }
        /// <summary>
        /// 檔次是否上架
        /// </summary>
        public bool DealOnline { get; set; }
        /// <summary>
        /// 是否有過到商家
        /// </summary>
        public bool PassSeller { get; set; }
        /// <summary>
        /// 檔次狀態
        /// </summary>
        public string StatusName { get; set; }
        /// <summary>
        /// CChannel
        /// </summary>
        public bool CChannel { get; set; }
        /// <summary>
        /// 影片網址
        /// </summary>
        public string CChannelLink { get; set; }
        /// <summary>
        /// 免稅商品通報
        /// </summary>
        public bool NoTaxNotification { get; set; }
        /// <summary>
        /// 流程跑完次數
        /// </summary>
        public int FlowCompleteTimes { get; set; }
        /// <summary>
        /// 不顯示折後價於前台
        /// </summary>
        public bool IsDealDiscountPriceBlacklist { get; set; }
        /// <summary>
        /// 本檔不支援ATM支付
        /// </summary>
        public bool NoATM { get; set; }
        /// <summary>
        /// 活動遊戲檔
        /// </summary>
        public bool IsGame { get; set; }
        /// <summary>
        /// 是否為24到貨
        /// </summary>
        public bool IsWms { get; set; }
    }

    public class ProposalMultiDealsSpec
    {
        public int Sort { get; set; }
        public List<ProposalMultiDealsItem> Items { get; set; }
    }
    public class ProposalMultiDealsItem
    {
        public Guid product_guid { get; set; }
        public Guid item_guid { get; set; }
        public string name { get; set; }
    }

    public class SellerProposalList
    {
        /// <summary>
        /// 檔次編號
        /// </summary>
        public int Pid { get; set; }
        /// <summary>
        /// 提案類型
        /// </summary>
        public string DealType { get; set; }
        /// <summary>
        /// 提案子類型
        /// </summary>
        public string DealSubType { get; set; }
        /// <summary>
        /// 優惠內容
        /// </summary>
        public string MultiDeals { get; set; }
        /// <summary>
        /// 配送時間
        /// </summary>
        public string BusinessHourOrderTime { get; set; }
        /// <summary>
        /// 檔號
        /// </summary>
        public string BusinessHourGuid { get; set; }
        public string UniqueId { get; set; }
        /// <summary>
        /// 業務姓名
        /// </summary>
        public string SaleName { get; set; }
        /// <summary>
        /// 狀態
        /// </summary>
        public string Status { get; set; }

        public int ProposalSourceType { get; set; }
        public bool IsWms { get; set; }
    }

    /// <summary>
    /// 商家批次處理錯誤資料
    /// </summary>
    [Serializable]
    public class SellerBatchErrorData
    {
        /// <summary>
        /// 處理模式
        /// </summary>
        public string ProcessMode { get; set; }
        /// <summary>
        /// 編號
        /// </summary>
        public string SellerID { get; set; }
        /// <summary>
        /// 品牌名稱
        /// </summary>
        public string SellerName { get; set; }
        /// <summary>
        /// 上層公司
        /// </summary>
        public string ParentSellerID { get; set; }
        /// <summary>
        /// 錯誤訊息
        /// </summary>
        public string ErrorMessage { get; set; }
    }

    public partial class AjaxResponseResult
    {
        public int Id { get; set; }
        public string Value { get; set; }
        public string Label { get; set; }
    }

    public class SalesGoogleCalendarModel
    {
        /// <summary>
        /// 概要
        /// </summary>
        public string summary { get; set; }
        /// <summary>
        /// 地點
        /// </summary>
        public string location { get; set; }
        /// <summary>
        /// 說明
        /// </summary>
        public string description { get; set; }
        /// <summary>
        /// 起始日
        /// </summary>
        public GoogleCalendarDate start { get; set; }
        /// <summary>
        /// 結束日
        /// </summary>
        public GoogleCalendarDate end { get; set; }
        /// <summary>
        /// 重複顯示
        /// </summary>
        public List<string> recurrence { get; set; }
        /// <summary>
        /// 會議參加對象
        /// </summary>
        public List<Dictionary<string, string>> attendees { get; set; }
        /// <summary>
        /// 提醒
        /// </summary>
        public GoogleCalendarReminder reminders { get; set; }

    }
    public class GoogleCalendarDate
    {
        public string dateTime { get; set; }
        public string timeZone { get; set; }
    }
    public class GoogleCalendarReminder
    {
        public bool useDefault { get; set; }
        public List<GoogleCalendarOverrides> overrides { get; set; }
    }
    public class GoogleCalendarOverrides
    {
        public string method { get; set; }
        public int minutes { get; set; }
    }

    public class ProposalRelatedDealModel
    {
        public int ProposalId { get; set; }
        public Guid BusinessHourGuid { get; set; }
        public string ItemName { get; set; }
        public string ShipTypeDesc { get; set; }
        public string DealStatusDesc { get; set; }
        public bool IsShow { get; set; }
    }
}

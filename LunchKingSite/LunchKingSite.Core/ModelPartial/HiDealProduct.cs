﻿namespace LunchKingSite.DataOrm
{
    public partial class HiDealProduct
    {
        /// <summary>
        /// 通常 PriceInPage 就是顯示 Price，但如果產品設定成套販售，那就會回傳 (Price * 每單限購數)
        /// </summary>
        public decimal PriceInPage
        {
            get
            {
                if (this.Price == null)
                {
                    return 0;
                }
                if (this.IsCombo)
                {
                    return this.OrderLimit.Value * this.Price.Value;
                }
                return this.Price.Value;
            }
        }

        /// <summary>
        /// 通常 OriginalPriceInPage 就是顯示 OriginalPrice，但如果產品設定成套販售，那就會回傳 (OriginalPrice * 每單限購數)
        /// </summary>
        public decimal OriginalPriceInPage
        {
            get
            {
                if (this.OriginalPrice == null)
                {
                    return 0;
                }
                if (this.IsCombo)
                {
                    return this.OrderLimit.Value * this.OriginalPrice.Value;
                }
                return this.OriginalPrice.Value;
            }
        }
    }
}
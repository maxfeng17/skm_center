﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.DataOrm
{
    public partial class ViewBrandCategory
    {
        public int BusinessHourStatus { get; set; }
        public decimal GrossMargin { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.Core.ModelPartial
{
    public class VbsDocumentCategoryEntity
    {
        public int Id { get; set; }
        public string CategoryName { get; set; }
        public int Sort { get; set; }
        public bool Status { get; set; }

        public string CreateName { get; set; }
        public string CreateDate { get; set; }

    }

    public class VbsDocumentFileEntity
    {
        public int Id { get; set; }
        public string DisplayName { get; set; }
        public int PageType { get; set; }
        public int CategoryId { get; set; }

        public bool IsHouse { get; set; }
        public bool IsShop { get; set; }
        public string CreateName { get; set; }
        public string CreateDate { get; set; }
        public bool Status { get; set; }
        public int Sort { get; set; }
        public string FilePath { get; set; }

    }
}

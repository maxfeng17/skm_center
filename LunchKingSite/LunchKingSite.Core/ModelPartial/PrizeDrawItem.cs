﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations.Schema;

namespace LunchKingSite.Core.Models.Entities
{
    public partial class PrizeDrawItem
    {
        /// <summary> 是否可以當抽獎項目 </summary>
        [JsonIgnore]
        [NotMapped]
        public bool CanDrawOut { get; set; }
    }
}

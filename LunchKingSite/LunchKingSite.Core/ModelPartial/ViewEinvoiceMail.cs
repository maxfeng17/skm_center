﻿using System;
using LunchKingSite.Core;
using log4net;

namespace LunchKingSite.DataOrm
{
    public partial class ViewEinvoiceMail
    {
        private ILog logger = LogManager.GetLogger(typeof(ViewEinvoiceMail));

        public bool IsEinvoice3
        {
            get
            {
                return this.InvoiceMode2 == (int)Core.InvoiceMode2.Triplicate;
            }
        }

        public bool IsEinvoice2
        {
            get
            {
                return this.InvoiceMode2 == (int)Core.InvoiceMode2.Duplicate;
            }
        }

        public bool IsPrintMark
        {
            get
            {
                return this.InvoiceRequestTime.HasValue;
            }
        }

        public bool IsDonateMark
        {
            get
            {
                return string.IsNullOrEmpty(this.LoveCode) == false;
            }
        }

        private void CheckVersionValid()
        {
            logger.Warn("注意資料有問題 不預期 EinvoiceMain 的 Version 會出現 " + this.Version);
        }

        public string InvoicePassAsString
        {
            get
            {
                if (this.Version == (int)InvoiceVersion.Old)
                {
                    return "個人識別碼:" + InvoicePass;
                }
                if (this.Version == (int)InvoiceVersion.CarrierEra)
                {
                    return "電子發票隨機碼:" + InvoicePass;
                }
                throw new ArgumentOutOfRangeException();
            }
        }
    }
}

﻿using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.Core.ModelPartial
{
    #region 阿里巴巴
    public class AlibabaItemResult
    {
        public string code { get; set; }
        public bool success { get; set; }
        public AlibabaItemInfo result { get; set; }
    }
    public class AlibabaItemInfo
    {
        public ItemInfo itemInfo { get; set; }
        public List<SkuInfo> skuInfoList { get; set; }
        public ItemDetailInfo itemDetailInfo { get; set; }
    }
    public class ItemInfo
    {
        public int tenantId { get; set; }
        public string tenantIdLong { get; set; }
        public ItemInfo_Extra extra { get; set; }
        public string name { get; set; }
        public int[] categoryIds { get; set; }
        public string spuId { get; set; }
        public string mainImage { get; set; }
        public int businessType { get; set; }
        public List<long> skuIdList { get; set; }
        public int status { get; set; }

    }
    public class SkuInfo
    {
        public int tenantId { get; set; }
        public string tenantIdLong { get; set; }
        public string barcode { get; set; }
        public long originalPrice { get; set; }
    }
    public class ItemDetailInfo
    {
        public List<itemDetailInfo_Detail> pcDetail { get; set; }
    }

    public class ItemInfo_Extra
    {
        public string categoryList { get; set; }
        public string salesStores { get; set; }
        public long onShelfTime { get; set; }
        public long offShelfTime { get; set; }
    }

    public class itemDetailInfo_Detail
    {
        public string title { get; set; }
        public string content { get; set; }
    }

    public class StockCenterDeal
    {
        public ExternalDeal deal { get; set; }
        public long skuId;
        public long spuId;
        public long itemId;
    }
    #endregion
}

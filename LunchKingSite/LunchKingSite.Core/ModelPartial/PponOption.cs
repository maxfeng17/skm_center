using System;
using System.Collections.Generic;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;

namespace LunchKingSite.DataOrm
{
    public partial class PponOption
    {
        private static IPponProvider _pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        
        private static readonly object findLock = new object();
        private static Dictionary<Guid, int> optIdByAcc = new Dictionary<Guid, int>();
        private static Dictionary<Guid, DateTime> noneOptIdByAccAndBid = new Dictionary<Guid, DateTime>();
        
        public static int? FindId(Guid acc_grp_mem_guid, Guid bid)
        {
            if (optIdByAcc.ContainsKey(acc_grp_mem_guid))
            {
                return optIdByAcc[acc_grp_mem_guid];
            }

            lock (findLock)
            {
                if (!optIdByAcc.ContainsKey(acc_grp_mem_guid))
                {
                    int? result = _pp.PponOptionGetIdByAccessory(acc_grp_mem_guid);
                    if (result.HasValue)
                    {
                        optIdByAcc.Add(acc_grp_mem_guid, result.Value);
                    }
                    else
                    {
                        if (!noneOptIdByAccAndBid.ContainsKey(acc_grp_mem_guid) || DateTime.Compare(DateTime.Now, noneOptIdByAccAndBid[acc_grp_mem_guid].AddHours(1)) > 0)
                        {
                            result = _pp.PponOptionGetIdByAccessoryAndBid(acc_grp_mem_guid, bid);
                            if (result.HasValue)
                            {
                                optIdByAcc.Add(acc_grp_mem_guid, result.Value);
                            }
                            else
                            {
                                noneOptIdByAccAndBid.Add(acc_grp_mem_guid, DateTime.Now);
                            }
                        }
                    }

                    return result;
                }

                return optIdByAcc[acc_grp_mem_guid];
            }
        }
    }

    public class PponOptionItems
    {
        public Guid BusinessHourGuid { get; set; }
        public bool multiDeals { get; set; }
        public List<ViewPponDeal> PponDeals { get; set; }
        public PponOptionCollection pponOption { get; set; }
        public List<Seller> store { get; set; }
        public PponStoreCollection pponStores { get; set; }
        public PponOptionLogCollection pponOptionLog { get; set; }
        /// <summary>
        ///核銷量
        /// </summary>
        public Dictionary<Guid,decimal> Verified { get; set; }
        /// <summary>
        ///分店核銷量
        /// </summary>
        public Dictionary<Guid, Dictionary<Guid, decimal>> StoreVerified { get; set; }
        /// <summary>
        ///銷售量
        /// </summary>
        public Dictionary<Guid, decimal> Sale { get; set; }
        /// <summary>
        ///分店銷售量
        /// </summary>
        public Dictionary<Guid, Dictionary<Guid, decimal>> StoreSale { get; set; }
        /// <summary>
        ///退貨量
        /// </summary>
        public Dictionary<Guid, decimal> Return { get; set; }
        /// <summary>
        /// 母檔的最大購買數
        /// </summary>
        public decimal MainOrderTotalLimit { get; set; }
        /// <summary>
        /// 子檔的最大購買數(選中的子檔)
        /// </summary>
        public decimal OrderTotalLimit { get; set; }
        /// <summary>
        /// 子檔的最大購買數加總
        /// </summary>
        public decimal SumOrderTotalLimit { get; set; }
        /// <summary>
        /// 建立檔次的部門
        /// </summary>
        public string DeptId { get; set; }
        /// <summary>
        /// 是否允許更新
        /// </summary>
        public bool IsUpdate { get; set; }
        /// <summary>
        /// 是否為上架費檔次
        /// </summary>
        public bool IsSlottingFeeQuantity { get; set; }
        /// <summary>
        /// 是否可輸入負數
        /// </summary>
        public bool IsNegative { get; set; }
        /// <summary>
        /// 檔次是否已過期
        /// </summary>
        public bool IsClosed { get; set; }
        /// <summary>
        ///是否自己的業務
        /// </summary>
        public bool IsSelf { get; set; }
        /// <summary>
        /// 是否有跨區權限
        /// </summary>
        public bool IsPrivilege { get; set; }
        public string NoPrivilege { get; set; }
        /// <summary>
        /// 是否為通用券檔次
        /// </summary>
        public bool IsNoRestrictedStore { get; set; }
        /// <summary>
        /// 是否為商品寄倉
        /// </summary>
        public bool IsConsignment { get; set; }
        
    }
    public class QuantityList
    {
        public string Id { get; set; }
        public string Bid { get; set; }
        public string Quantity { get; set; }
        public string ModifyCatgSeq { get; set; }
    }
    public class storeList
    {
        public string Bid { get; set; }
        public string Sid { get; set; }
        public string TotalQuantity { get; set; }
    }
    public class OrderTotalLimitList
    {
        public string Bid { get; set; }
        public string OrderTotalLimit { get; set; }
        public string ModifyOrderTotalLimit { get; set; }
    }
    public enum PponOptionEdotLevel
    {
        /// <summary>
        /// 客服部
        /// </summary>
        C000,
        /// <summary>
        /// 客服部(一線)
        /// </summary>
        C001,
        /// <summary>
        /// 客服部(二線)
        /// </summary>
        C002,
        /// <summary>
        /// 創意部
        /// </summary>
        P000,
        /// <summary>
        /// 創意部(設定)
        /// </summary>
        P001,
        /// <summary>
        /// 創意部(文案)
        /// </summary>
        P002,
        /// <summary>
        /// 創意部(圖片)
        /// </summary>
        P003,
        /// <summary>
        /// 創意部(ART)
        /// </summary>
        P004,
        /// <summary>
        /// 創意部(上架)
        /// </summary>
        P005,
        /// <summary>
        /// 創意部(攝影)
        /// </summary>
        P006
    }
}

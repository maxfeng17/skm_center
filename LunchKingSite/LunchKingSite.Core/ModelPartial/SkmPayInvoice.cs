﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.Core.Models.Entities
{
    public partial class SkmPayInvoice
    {
        /// <summary> 會員 APP 專屬密碼 </summary>
        [JsonIgnore]
        [NotMapped]
        public string VipAppPass { get; set; }
    }
}

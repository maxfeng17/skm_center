﻿namespace LunchKingSite.DataOrm
{
    public partial class Order
    {
        public void SetOrderStatusComplete()
        {
            this.OrderStatus |= (int)Core.OrderStatus.Complete;
        }

    }
}

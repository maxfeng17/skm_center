﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.Core.ModelPartial
{
    public class TaishinPlatFormEntity
    {
        public int PlatForm { get; set; }
        public string UserId { get; set; }
    }
    public class TaishinAppAuthEntity
    {
        public string Data { get; set; }
        public string Code { get; set; }
    }
    public class TaishinCreditCardAuthModel
    {
        public int Platform { get; set; }
        public string MemberId { get; set; }
        public string MobilePhone { get; set; }
        public string CardNumber { get; set; }
        public string ExpireDate { get; set; }
        public string Cvv2 { get; set; }
        public string CheckCardType { get; set; }
        public string CardName { get; set; }
        public string DeviceId { get; set; }
        public string UserId { get; set; }
        public string AuthKey { get; set; }
    }

    //public class TaishinCreditCardAuth2Model
    //{
    //    public string Platform { get; set; }
    //    public string MemberId { get; set; }
    //    public string MobilePhone { get; set; }
    //    public string CardNumber { get; set; }
    //    public string ExpireDate { get; set; }
    //    public string Cvv2 { get; set; }
    //    public string AuthKey { get; set; }
    //    public string UserId { get; set; }
    //}

    public class TaishinCreditCardResponseModel
    {
        public int Platform { get; set; }
        public string AuthKey { get; set; }
        public string Retcode { get; set; }
        public string MemberId { get; set; }
        public string DeviceId { get; set; }
        public string UserId { get; set; }
    }

    public class TaishinPaymentBarcodeModel
    {
        public int Platform { get; set; }
        public string CardToken { get; set; }
        public string MemberId { get; set; }
        public string Barcode { get; set; }
        public string DeviceId { get; set; }
        public string UserId { get; set; }
    }
    public class TaishinPaymentListModel
    {
        public string StartIdx { get; set; }

        public string MemberId { get; set; }
        public string BeginDate { get; set; }
        public string EndDate { get; set; }
        public string TradeStatus { get; set; }
        public string UserId { get; set; }
        public int Platform { get; set; }
    }
}

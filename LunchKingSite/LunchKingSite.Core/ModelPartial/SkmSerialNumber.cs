﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.Core.Models.Entities
{
    public partial class SkmSerialNumber
    {
        public string GetTradeNo()
        {
            return string.Format("{0}{1}", SkmTradeNo, SerialTime.ToString().PadLeft(2, '0'));
        }
    }
}

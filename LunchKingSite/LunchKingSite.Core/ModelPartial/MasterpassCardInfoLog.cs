﻿namespace LunchKingSite.DataOrm
{
    public partial class MasterpassCardInfoLog
    {
        /// <summary>
        /// 驗證VerifierCode是否正確
        /// </summary>
        public bool IsValid
        {
            get
            {
                return VerifierCode > 10000000;
            }
        }
    }
}

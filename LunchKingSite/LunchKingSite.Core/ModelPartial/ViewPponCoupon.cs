﻿using System;

namespace LunchKingSite.DataOrm
{
    public partial class ViewPponCoupon
    {
        /// <summary>
        /// 考量特例，如倒店日、載止日等。一般則回傳檔次設定裡的配送結束日期
        /// </summary>
        public DateTime CouponExpiredDate
        {
            get
            {
                if (CouponChangedExpiredDate != null)
                {
                    return CouponChangedExpiredDate.Value;
                }
                if (BusinessHourDeliverTimeE == null) {
                    throw new Exception("查詢 ViewPponCoupon 發生錯誤，因為檔次未設定結檔時間, bid=" + this.BusinessHourGuid);
                }
                return BusinessHourDeliverTimeE.Value;
            }
        }
        /// <summary>
        /// 四個日期中 (賣家倒店日, 店家倒店日, 賣家調整截止日, 店家調整截止日), 根據商業邏輯取出最小日期. 若四個日期都沒有設定, 則會回傳 null;
        /// </summary>
        public DateTime? CouponChangedExpiredDate
        {
            get
            {
                if (StoreChangedExpireDate == null && BhChangedExpireDate == null && StoreCloseDownDate == null &&
                    SellerCloseDownDate == null)
                {
                    return null;
                }
                // 截止日期: 分店優先賣家(檔次)
                DateTime changedExpireDate = StoreChangedExpireDate ?? BhChangedExpireDate ?? DateTime.MaxValue;

                // 結束營業日: 分店優先賣家
                DateTime closeDownDate = StoreCloseDownDate ?? SellerCloseDownDate ?? DateTime.MaxValue;

                // min(截止日期, 結束營業日)
                return changedExpireDate < closeDownDate ? changedExpireDate : closeDownDate;                
            }
        }

        /// <summary>
        /// 憑證的下載連結
        /// </summary>
        public string CouponGetUrl {
            get
            {
                if (this.CouponId == 0)
                {
                    return string.Empty;
                }
                return string.Format("/User/CouponGet.aspx?sequencenumber={0}&couponcode={1}&cid={2}&ct={3}",
                    this.SequenceNumber, this.CouponCode, this.CouponId, this.OrderDetailCreateTime.Ticks);
            }
        }
    }
}

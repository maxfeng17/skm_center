﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.DataOrm
{
    public partial class ViewEventPromoItem
    {
        public decimal OrderedTotal { get; set; }
        public decimal GrossMargin { get; set; }
        public int GrossMarginStatus { get; set; }
    }
}

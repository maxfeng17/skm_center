﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.DataOrm
{
    public partial class Employee
    {
        public double? KPI { get; set; }
        public int? DealTarget { get; set; }
        public string YearMon { get; set; }
    }
}

﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using Newtonsoft.Json;

namespace LunchKingSite.DataOrm
{

    public partial class ViewPponDealCollection
    {
        private static log4net.ILog logger = log4net.LogManager.GetLogger("speed");

        private ConcurrentDictionary<Guid, ViewPponDeal> indexedGuid = new
            ConcurrentDictionary<Guid, ViewPponDeal>();
        private ConcurrentDictionary<int, ViewPponDeal> indexedUniqueId = new
            ConcurrentDictionary<int, ViewPponDeal>();

        private bool isIndexed;

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void InitIndex()
        {
            if (isIndexed)
            {
                return;
            }
            isIndexed = true;

            this.ListChanged += delegate(object sender, ListChangedEventArgs e)
            {
                if (e.ListChangedType == ListChangedType.ItemAdded)
                {
                    ViewPponDeal vpd = this[e.NewIndex];
                    if (vpd.BusinessHourGuid == Guid.Empty || vpd.UniqueId == null)
                    {
                        return;
                    }
                    indexedGuid.TryAdd(vpd.BusinessHourGuid, vpd);
                    indexedUniqueId.TryAdd((int)vpd.UniqueId, vpd);
                }
            };

            foreach (var vpd in this)
            {
                if (vpd.BusinessHourGuid == Guid.Empty || vpd.UniqueId == null)
                {
                    continue;
                }
                indexedGuid.TryAdd(vpd.BusinessHourGuid, vpd);
                indexedUniqueId.TryAdd((int)vpd.UniqueId, vpd);
            }
        }

        public ViewPponDeal Get(Guid bid)
        {
            if (isIndexed)
            {
                ViewPponDeal deal;
                indexedGuid.TryGetValue(bid, out deal);

                //logger.DebugFormat("ViewPponDealCollection.Get {0}, {1}, {2}, {3}", bid, isIndexed, deal == null ? "Null" : "Not Null", Count);

                return deal;
            }
            var result = this.FirstOrDefault(x => x.BusinessHourGuid == bid);
            //logger.DebugFormat("ViewPponDealCollection.Get {0}, {1}, {2}, {3}", bid, isIndexed, result == null ? "Null" : "Not Null", Count);
            return result;
        }

        public ViewPponDeal Get(int uniqueId)
        {
            if (isIndexed)
            {
                ViewPponDeal deal;
                indexedUniqueId.TryGetValue(uniqueId, out deal);
                return deal;
            }
            return this.FirstOrDefault(x => x.UniqueId == uniqueId);
        }

        public new ViewPponDealCollection Clone()
        {
            ViewPponDealCollection newOne = new ViewPponDealCollection();
            this.ForEach((x) =>
            {
                newOne.Add(x.Clone());
            });
            return newOne;
        }
    }

    public interface IViewPponDeal
    {
        Guid BusinessHourGuid { get; }
        Guid SellerGuid { get; }
        DateTime BusinessHourOrderTimeS { get; }
        DateTime BusinessHourOrderTimeE { get; }
        DateTime? BusinessHourDeliverTimeS { get; }
        DateTime? BusinessHourDeliverTimeE { get; }
        decimal BusinessHourOrderMinimum { get; }
        int BusinessHourStatus { get; }
        decimal? OrderTotalLimit { get; }
        decimal BusinessHourDeliveryCharge { get; }
        int BusinessHourAtmMaximum { get; }
        string PageTitle { get; }
        string PageDesc { get; }
        string PicAlt { get; }
        DateTime? SettlementTime { get; }
        DateTime? ChangedExpireDate { get; }
        string SellerId { get; }
        string SellerName { get; }
        string SellerTel { get; }
        string SellerEmail { get; }
        string SellerAddress { get; }
        int Department { get; }
        int SellerCityId { get; }
        bool IsCloseDown { get; }
        DateTime? CloseDownDate { get; }
        string CompanyId { get; }
        string CompanyName { get; }
        Guid ItemGuid { get; }
        int? MaxItemCount { get; }
        string ItemName { get; }
        decimal ItemOrigPrice { get; }
        //DANGEROUS, not thread safe
        decimal ItemPrice { get; set; }
        int? ItemDefaultDailyAmount { get; }
        //DANGEROUS, not thread safe
        string EventName { get; set; }
        /// <summary>
        /// 橘標
        /// </summary>
        string EventTitle { get; }
        string EventImagePath { get; }
        string Introduction { get; }
        string Restrictions { get; }
        string Description { get; }
        string AppDescription { get; }
        string ReferenceText { get; }
        string CouponUsage { get; }
        string Remark { get; }
        string Reasons { get; }
        string SubjectName { get; }
        string EventSpecialImagePath { get; }
        string AppDealPic { get; }
        string AppTitle { get; }
        string Availability { get; }
        DateTime EventModifyTime { get; }
        int? DeliveryType { get; }
        bool? ShoppingCart { get; }
        int? ComboPackCount { get; }
        bool? IsTravelcard { get; }
        int? QuantityMultiplier { get; }
        int? UniqueId { get; }
        bool? IsDailyRestriction { get; }
        bool? IsContinuedQuantity { get; }
        int? ContinuedQuantity { get; }
        string CityList { get; }
        string ActivityUrl { get; }
        //DANGEROUS, not thread safe
        string LabelIconList { get; set; }
        string LabelTagList { get; }
        int? CouponCodeType { get; }
        int? PinType { get; }
        string CustomTag { get; }
        decimal? ExchangePrice { get; }
        bool? IsQuantityMultiplier { get; }
        bool? IsAveragePrice { get; }
        bool? IsZeroActivityShowCoupon { get; }
        int? DealAccBusinessGroupId { get; }
        int? DealType { get; }
        int? DealTypeDetail { get; }
        bool? IsTravelDeal { get; }
        string DealEmpName { get; }
        bool? Installment3months { get; }
        bool? Installment6months { get; }
        bool? Installment12months { get; }
        bool? DenyInstallment { get; }
        int? SaleMultipleBase { get; }
        int? ShipType { get; }
        int? ShippingdateType { get; }
        int? Shippingdate { get; }
        int? ProductUseDateEndSet { get; }
        int? PresentQuantity { get; }
        int? GroupCouponAppStyle { get; }
        int? NewDealType { get; }
        bool? IsExperience { get; }
        int? DiscountType { get; }
        int? DiscountValue { get; }
        string TravelPlace { get; }
        int? EntrustSell { get; }
        bool? MultipleBranch { get; }
        bool? IsLongContract { get; }
        bool? CompleteCopy { get; }
        int? DevelopeSalesId { get; }
        bool? IsFreightsDeal { get; }
        Guid? GroupOrderGuid { get; }
        Guid? OrderGuid { get; }
        DateTime? CreateTime { get; }
        DateTime? CloseTime { get; }
        int? GroupOrderStatus { get; }
        string Slug { get; }
        string Intro { get; }
        string PriceDesc { get; }
        bool? IsHideContent { get; }
        string ContentName { get; }
        bool? IsCloseMenu { get; }
        //DANGEROUS, not thread safe
        int? OrderedQuantity { get; set; }
        decimal? OrderedTotal { get; }
        int? OrderedIncludeRefundQuantity { get; }
        decimal? OrderedIncludeRefundTotal { get; }
        string DealPromoTitle { get; }
        string DealPromoDescription { get; }
        string DealPromoImage { get; }
        string PdfItemName { get; }
        Guid? MainBid { get; }
        int? ComboDealSeq { get; }
        int? GroupCouponDealType { get; }
        string CategoryList { get; }
        bool? Consignment { get; }
        bool? IsBankDeal { get; }
        bool? AllowGuestBuy { get; }

        bool IsLoaded { get; }
        //Logic Props
        bool IsVisaNow { get; }
        bool IsMultipleStores { get; }
        string HoverMessage { get; }
        string PromoImageHtml { get; }
        //DANGEROUS, not thread safe
        string DefaultDealImage
        {
            get;
            set;
        }
        IList<ViewComboDeal> ComboDelas { get; }
        HashSet<string> Tags { get; }
        List<int> CategoryIds { get; }
        /// <summary>
        /// 權益說明標籤，RightsAndInterestsTags，但是組好的html, 建議使用RightsAndInterestsTags
        /// </summary>
        string IconTags { get; }
        /// <summary>
        /// 權益說明標籤，與IconTags同，但是純陣列
        /// </summary>
        List<string> RestrictionTags { get; }
        bool IsHiddenInRecommendDeal { get; }
        bool IsSoldOut { get; }
        //method
        bool IsVisa(DateTime? effectiveTime = null);
        //method
        bool IsHouseDealNewVersion();
        int? FreightsId { get; }
        int? BankId { get; }
        string ProductSpec { get; }
        bool? DiscountUseType { get; }
        int? VerifyActionType { get; }
        bool? IsHouseNewVer { get; }
        decimal FreightAmount { get; }
        bool EnableDelivery { get; }
        bool EnableIsp { get; }
        int IspQuantityLimit { get; }
        bool? Cchannel { get; }
        string CchannelLink { get; }
        bool? IsExpiredDeal { get; }
        string RemoveBgPic { get; }
        decimal? DiscountPrice { get; }
        List<int> CityIds { get; }
        //是否為砍價遊戲的遊戲用檔次
        bool? IsGame {get;}
        int CouponSeparateDigits { get; }

        int BookingSystemType { get; }
        int? ExpireRedirectDisplay { get; }
        string ExpireRedirectUrl { get; }
        bool? UseExpireRedirectUrlAsCanonical { get; }
        bool IsWms {get;}

        string AgentChannels { get; }
        string EventOriImagePath { get; }
        bool IsChannelGift { get; }
        bool? IsChosen { get; }

        #region 邏輯產生的屬性

        List<DealLabelSystemCode> LabelIcons { get; }

        #endregion
    }

    public partial class ViewPponDeal : IViewPponDeal
    {
        public bool IsVisaNow
        {
            get
            {
                return IsVisa(DateTime.Now);
            }
        }

        private List<DealLabelSystemCode> _labelIcons;
        [JsonIgnore]
        public List<DealLabelSystemCode> LabelIcons
        {
            get
            {
                if (_labelIcons == null)
                {
                    if (string.IsNullOrEmpty(this.LabelIconList))
                    {
                        _labelIcons = new List<DealLabelSystemCode>();
                    }
                    else
                    {
                        _labelIcons = this.LabelIconList.Split(",").Select(t => (DealLabelSystemCode)Convert.ToInt32(t)).ToList();
                    }
                }
                return _labelIcons;
            }
        }

        /// <summary>
        /// 設定 visa 分類，且在上檔日期3天內，為 true
        /// </summary>
        /// <param name="effectiveTime">日期(不填則回傳是否為visa檔次)</param>
        /// <returns></returns>
        public bool IsVisa(DateTime? effectiveTime = null)
        {
            if (string.IsNullOrEmpty(this.LabelIconList))
            {
                return false;
            }
            string[] icons = this.LabelIconList.Split(',');
            foreach (string icon in icons)
            {
                if (int.Parse(icon) == (int)DealLabelSystemCode.Visa)
                {
                    if (effectiveTime != null)
                    {
                        return (effectiveTime.Value - this.BusinessHourOrderTimeS).Days < 3;
                    }
                    return true;
                }
            }
            return false;
        }

        public bool IsHouseDealNewVersion()
        {
            if(this.IsHouseNewVer != null && this.IsHouseNewVer.Value && this.DeliveryType == (int)Core.DeliveryType.ToHouse)
            {
                return true;
            }
            return false;
        }

        public string HoverMessage { get; set; }
        public bool IsMultipleStores { get; set; }

        public string DefaultDealImage { get; set; }
        public string IconTags { get; set; }
        /// <summary>
        /// 權益說明(陣列的版本)
        /// </summary>
        public List<string> RestrictionTags { get; set; }
        public string PromoImageHtml { get; set; }

        private List<int> _cityIds;
        [JsonIgnore]
        public List<int> CityIds
        {
            get {
                if (_cityIds == null)
                {
                    if (string.IsNullOrEmpty(this.CityList))
                    {
                        _cityIds = new List<int>();
                    }
                    else
                    {
                        _cityIds = ProviderFactory.Instance().GetSerializer().Deserialize<List<int>>(CityList);
                    }
                }
                return _cityIds;
            }
        }

        [JsonIgnore]
        private List<int> _categoryIds;
        public List<int> CategoryIds
        {
            get
            {
                if (_categoryIds != null)
                {
                    return _categoryIds;
                }
                if (string.IsNullOrEmpty(this.CategoryList))
                {
                    _categoryIds = new List<int>();
                }
                else
                {
                    _categoryIds = ProviderFactory.Instance().GetSerializer().Deserialize<List<int>>(CategoryList);
                }
                return _categoryIds;
            }
        }

        /// <summary>
        /// 判斷在APP的推薦檔次是否需要隱藏
        /// </summary>
        public bool IsHiddenInRecommendDeal { get; set; }

        public bool IsSoldOut { get; set; }

        public List<DealTimeSlot> DealTimeSlots { get; set; }

        private HashSet<string> _tags;
        [JsonIgnore]
        public HashSet<string> Tags
        {
            get
            {
                if (_tags == null)
                {
                    if (string.IsNullOrWhiteSpace(this.PicAlt))
                    {
                        _tags = new HashSet<string>();
                    }
                    else
                    {
                        _tags = new HashSet<string>(this.PicAlt.Split('/'));
                    }
                }
                return _tags;
            }
        }

        public IList<ViewComboDeal> ComboDelas { get; set; }

        public new ViewPponDeal Clone()
        {
            var newOne = base.Clone();
            newOne.HoverMessage = this.HoverMessage;
            newOne.IsMultipleStores = this.IsMultipleStores;
            newOne.DefaultDealImage = this.DefaultDealImage;
            newOne.IconTags = this.IconTags;
            newOne.RestrictionTags = this.RestrictionTags;
            newOne.PromoImageHtml = this.PromoImageHtml;
            newOne._tags = this._tags;
            newOne._cityIds = this._cityIds;
            newOne.IsHiddenInRecommendDeal = this.IsHiddenInRecommendDeal;
            newOne.IsSoldOut = this.IsSoldOut;

            if (this.ComboDelas != null)
            {
                newOne.ComboDelas = new List<ViewComboDeal>();
                foreach (var comboDeal in this.ComboDelas)
                {
                    newOne.ComboDelas.Add(comboDeal.Clone());
                }
            }
            return newOne;
        }

        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            ViewPponDeal castObj = obj as ViewPponDeal;
            if (castObj == null) return false;
            return this.GetHashCode() == castObj.GetHashCode();
        }

        public override int GetHashCode()
        {
            if (this.BusinessHourGuid == Guid.Empty)
            {
                return 0;
            }
            return this.BusinessHourGuid.GetHashCode();
        }

    }

    public class DealTimeSlotList : List<IDealTimeSlot>
    {
        private Dictionary<Guid, List<IDealTimeSlot>> indexedBid =
            new Dictionary<Guid, List<IDealTimeSlot>>();
        private bool IsIndexed;

        [MethodImpl(MethodImplOptions.Synchronized)]
        public void InitIndex()
        {
            if (IsIndexed)
            {
                return;
            }
            IsIndexed = true;

            foreach (var dts in this)
            {
                if (indexedBid.ContainsKey(dts.BusinessHourGuid) == false)
                {
                    indexedBid.Add(dts.BusinessHourGuid, new List<IDealTimeSlot>());
                }
                indexedBid[dts.BusinessHourGuid].Add(dts);
            }
        }

        /// <summary>
        /// 回傳null : 檔次資料不在 index data 裡
        /// 回傳 new DealTimeSlot(): 檔次資料符合，但其他條件不符合
        /// </summary>
        /// <param name="businessHourGuid"></param>
        /// <param name="cityId"></param>
        /// <param name="effectiveTime"></param>
        /// <returns></returns>
        public IDealTimeSlot GetByBidCityAndTime(Guid businessHourGuid, int cityId, DateTime effectiveTime)
        {
            if (IsIndexed)
            {
                List<IDealTimeSlot> dtsList;
                if (indexedBid.TryGetValue(businessHourGuid, out dtsList) == false)
                {
                    return null;
                }
                IDealTimeSlot result = dtsList.FirstOrDefault(
                    t => t.EffectiveStart <= effectiveTime && t.EffectiveEnd > effectiveTime && t.CityId == cityId);
                return result;
            }
            return this.FirstOrDefault(x => x.BusinessHourGuid == businessHourGuid && x.EffectiveStart <= effectiveTime &&
                                          x.EffectiveEnd > effectiveTime && x.CityId == cityId);
        }

        public List<IDealTimeSlot> GetByBidAndTime(Guid businessHourGuid, DateTime effectiveTime)
        {
            List<IDealTimeSlot> result = new List<IDealTimeSlot>();
            if (IsIndexed)
            {
                List<IDealTimeSlot> dtsList;
                if (indexedBid.TryGetValue(businessHourGuid, out dtsList))
                {
                    result = dtsList.Where(
                        t => t.EffectiveStart <= effectiveTime && t.EffectiveEnd > effectiveTime).ToList();
                    return result;
                }
                return result;
            }
            result.AddRange(this.Where(x => x.BusinessHourGuid == businessHourGuid &&
                x.EffectiveStart <= effectiveTime &&
                x.EffectiveEnd > effectiveTime).ToList()
            );
            return result;
        }
    }   
}

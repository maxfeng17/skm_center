﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Vodka.DataAccess
{
    public interface IRepository<T>
    {
        IQueryable<T> Table { get; }
        void Create(T entity);
        void Update(T entity);
        void Delete(T entity);

        T Get<T1>(T1 id);
        T Get(Expression<Func<T, bool>> predicate);

        int Count(Expression<Func<T, bool>> predicate);
        IEnumerable<T> Fetch(Expression<Func<T, bool>> predicate);
        IEnumerable<T> Fetch(Expression<Func<T, bool>> predicate, Action<Orderable<T>> order);
        IEnumerable<T> Fetch(Expression<Func<T, bool>> predicate, Action<Orderable<T>> order, int pageSize, int pageNum);

        void Transaction(Action action);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Vodka.DataAccess
{
    /*
        Basically the code here is pretty much ripped-off Sharp Architecture
    */

    public interface IEntity
    {
        object Id { get; set; }
        bool IsTransient();
        IEnumerable<PropertyInfo> GetSignatureProperties();
    }

    public interface IEntity<T> : IEntity
    {
        new T Id { get; set; }
    }

    [Serializable]
    public class DomainSignatureAttribute : Attribute { }
}

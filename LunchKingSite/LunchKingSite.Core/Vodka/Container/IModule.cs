﻿using System.Web.Routing;
using Autofac;

namespace Vodka.Container
{
    public interface IModuleRegistrar
    {
        void RegisterWithContainer(ContainerBuilder builder);
        void Initialize(IContainer container);
    }
}
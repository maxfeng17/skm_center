﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
//using FluentMongo.Linq;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using MongoDB.Driver.Linq;
using Vodka.DataAccess;

namespace Vodka.Mongo
{
    public class MongoRepository<T> : IMongoRepository<T> where T : IEntity
    {
        private ISessionLocator _session;
        private MongoCollection<T> _collection;
        private EntityMapper<T> _emapper;

        public MongoRepository(ISessionLocator sessionLocator, EntityMapper<T> entityMapper)
        {
            _session = sessionLocator;
            _collection = sessionLocator.For<T>();
            _emapper = entityMapper;
        }

        #region IRepository interface methods
        public IQueryable<T> Table
        {
            get { return _collection.AsQueryable(); }
        }

        public void Create(T entity)
        {
            _collection.Save(entity);
        }

        public void Update(T entity)
        {
            _collection.Save(entity);
        }

        public void Delete(T entity)
        {
            var cm = BsonClassMap.LookupClassMap(typeof (T));
            _collection.Remove(Query.EQ(cm.IdMemberMap.ElementName, BsonValue.Create(entity.Id)));
        }

        public T Get<T1>(T1 id)
        {
            return _collection.FindOneById(BsonValue.Create(id));
        }

        public T Get(Expression<Func<T, bool>> predicate)
        {
            return _collection.AsQueryable().Where(predicate).FirstOrDefault();
        }

        public int Count(Expression<Func<T, bool>> predicate)
        {
            return _collection.AsQueryable().Count(predicate);
        }

        IEnumerable<T> IRepository<T>.Fetch(Expression<Func<T, bool>> predicate)
        {
            return Fetch(predicate).AsEnumerable();
        }

        IEnumerable<T> IRepository<T>.Fetch(Expression<Func<T, bool>> predicate, Action<Orderable<T>> order)
        {
            return Fetch(predicate, order).AsEnumerable();
        }

        IEnumerable<T> IRepository<T>.Fetch(Expression<Func<T, bool>> predicate, Action<Orderable<T>> order, int skip, int count)
        {
            return Fetch(predicate, order, skip, count).AsEnumerable();
        }

        public void Transaction(Action action)
        {
            // not exactly same as transaction but it's the closest thing we can get
            using (_session.RequestStart())
            {
                action();
            }
        }
        #endregion

        #region utility methods for linq access
        public virtual IQueryable<T> Fetch(Expression<Func<T, bool>> predicate)
        {
            return _collection.AsQueryable().Where(predicate);
        }

        public virtual IQueryable<T> Fetch(Expression<Func<T, bool>> predicate, Action<Orderable<T>> order)
        {
            var orderable = new Orderable<T>(Fetch(predicate));
            if (order != null)
                order(orderable);
            return orderable.Queryable;
        }

        public virtual IQueryable<T> Fetch(Expression<Func<T, bool>> predicate, Action<Orderable<T>> order, int pageSize, int pageNum)
        {
            return Fetch(predicate, order).Skip(pageSize * (pageNum - 1)).Take(pageSize);
        }
        #endregion

        #region driver specific interface methods
        public void Drop()
        {
            _collection.Drop();
        }

        public void CreateIndex(IMongoIndexKeys keys)
        {
            _collection.CreateIndex(keys);
        }

        public void CreateGeoIndex(string fieldName)
        {
            _collection.CreateIndex(IndexKeys.GeoSpatial(fieldName));
        }

        public void CreateGeoIndex<LatLng>(Expression<Func<T, LatLng>> field)
        {
            _collection.CreateIndex(IndexKeys.GeoSpatial(_emapper.LookupFieldName(field)));
        }

        public IEnumerable<T> FindAll()
        {
            return _collection.FindAll();
        }

        public IEnumerable<T> Find(IMongoQuery query)
        {
            return _collection.Find(query);
        }

        public IEnumerable<T> Find(IMongoQuery query, IMongoSortBy sortBy, int pageSize, int pageNum)
        {
            if (pageSize > 0)
            {
                return _collection.Find(query).SetSortOrder(sortBy).SetSkip((pageNum - 1) * pageSize).SetLimit(pageSize);
            }
            else
            {
                return _collection.Find(query).SetSortOrder(sortBy);
            }
        }


        public int FindAndModify(IMongoQuery query, IMongoUpdate update)
        {
            return _collection.FindAndModify(query, SortBy.Ascending("Id"), update).ModifiedDocument.Count();
        }

        public int FindAndRemove(IMongoQuery query)
        {
            return _collection.FindAndRemove(query, SortBy.Ascending("Id")).ModifiedDocument.Count();
        }

        public GeoNearResult<T>.GeoNearHits GeoNear(double x, double y, double maxDistance, int limitRecord)
        {
            return GeoNear(null, x, y, maxDistance, limitRecord);
        }

        public GeoNearResult<T>.GeoNearHits GeoNear(IMongoQuery query, double x, double y, double maxDistance, int limitRecord)
        {
            return _collection.GeoNear(query, x, y, limitRecord, GeoNearOptions.SetMaxDistance(maxDistance)).Hits;
        }

        public GeoNearResult<T>.GeoNearHits GeoNear(LatLng target, double maxDistance, int limitRecord)
        {
            return _collection.GeoNear(null, target.Longitude, target.Latitude, limitRecord, GeoNearOptions.SetMaxDistance(maxDistance).SetSpherical(true)).Hits;
        }

        public GeoNearResult<T>.GeoNearHits GeoNear(IMongoQuery query, LatLng target, double maxDistance, int limitRecord)
        {
            return _collection.GeoNear(query, target.Longitude, target.Latitude, limitRecord, GeoNearOptions.SetMaxDistance(maxDistance).SetSpherical(true)).Hits;
        }

        public void Update(IMongoQuery query, IMongoUpdate update)
        {
            _collection.Update(query, update);
        }

        public void UpdateAll(IMongoQuery query, IMongoUpdate update)
        {
            _collection.Update(query, update , UpdateFlags.Multi);
        }

        private static U DeserializeToMapReduceResultEntity<U>(BsonDocument document)
        {
            BsonDocument newDoc = document["value"].AsBsonDocument;
            newDoc["_id"] = document["_id"];

            return BsonSerializer.Deserialize<U>(newDoc);
        }

        #endregion
    }
}

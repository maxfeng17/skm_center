﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MongoDB.Driver;

namespace Vodka.Mongo
{
    /// <summary>
    /// Session handler of Mongo official C# driver
    /// </summary>
    /// <remarks>
    /// Evaluate to see if we should handle class map here, <seealso cref="NHibernateSessionFactory"/>
    /// </remarks>
    public class MongoSessionLocator : ISessionLocator
    {
        private string _connStr;
        private MongoDatabase _database;

        /// <summary>
        /// Initializes a new instance of the <see cref="MongoSessionLocator"/> class.
        /// </summary>
        /// <param name="connString">The conn string.</param>
        public MongoSessionLocator(string connString)
        {
            _connStr = connString;
            _database = MongoServer.Create(connString).GetDatabase(GetDatabaseName(connString));
        }

        public MongoCollection<T> For<T>()
        {
            return _database.GetCollection<T>();
        }

        public IDisposable RequestStart()
        {
            return _database.RequestStart();
        }

        private static string GetDatabaseName(string connString)
        {
            // there will be a more sophisticated implementation
            return connString.Substring(connString.LastIndexOf('/') + 1);
        }
    }

    public static class MongoDriverExtension
    {
        public static MongoCollection<T> GetCollection<T>(this MongoDatabase db)
        {
            return db.GetCollection<T>(typeof(T).Name);
        }
    }
}

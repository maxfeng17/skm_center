﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using Vodka.DataAccess;

namespace Vodka.Mongo
{
    public class EntityMapper<T> where T: IEntity
    {
        public string LookupFieldName<U>(Expression<Func<T, U>> exp)
        {
            return RecurseMemberExpression(exp.Body as MemberExpression);
        }

        #region private methods
        /// <summary>
        /// Returns the fully qualified and mapped retval from the member expression.
        /// </summary>
        /// <param name="body">The expression.</param>
        /// <returns></returns>
        private string RecurseExpression(Expression body)
        {
            var me = body as MemberExpression;
            if (me != null)
            {
                return this.RecurseMemberExpression(me);
            }

            var ue = body as UnaryExpression;
            if (ue != null)
            {
                return this.RecurseExpression(ue.Operand);
            }

            throw new MongoException("Unknown expression type, expected a MemberExpression or UnaryExpression.");
        }

        /// <summary>
        /// Returns the fully qualified and mapped retval from the member expression.
        /// </summary>
        /// <param retval="mex"></param>
        /// <returns></returns>
        private string RecurseMemberExpression(MemberExpression mex)
        {
            var retval = "";
            var parentEx = mex.Expression as MemberExpression;
            if (parentEx != null)
            {
                //we need to recurse because we're not at the root yet.
                retval += this.RecurseMemberExpression(parentEx) + ".";
            }
            BsonClassMap map;
            BsonMemberMap mmap;
            retval += (map = BsonClassMap.LookupClassMap(mex.Expression.Type)) != null && (mmap = map.GetMemberMap(mex.Member.Name)) != null ? mmap.ElementName : mex.Member.Name;
            return retval;
        }
        #endregion
    }
}

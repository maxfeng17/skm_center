﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using MongoDB.Driver;
using Vodka.DataAccess;

namespace Vodka.Mongo
{
    public interface IMongoRepository<T> : IRepository<T> where T : IEntity
    {
        void Drop();
        void CreateIndex(IMongoIndexKeys keys);
        void CreateGeoIndex<LatLng>(Expression<Func<T, LatLng>> exp);
        IEnumerable<T> FindAll();
        IEnumerable<T> Find(IMongoQuery query);
        IEnumerable<T> Find(IMongoQuery query, IMongoSortBy sortBy,int pageSize,int pageNum);
        int FindAndModify(IMongoQuery query, IMongoUpdate update);
        int FindAndRemove(IMongoQuery query);

        GeoNearResult<T>.GeoNearHits GeoNear(IMongoQuery query, double x, double y, double maxDistance, int limitRecord);
        GeoNearResult<T>.GeoNearHits GeoNear(double x, double y, double maxDistance, int limitRecord);
        GeoNearResult<T>.GeoNearHits GeoNear(LatLng target, double maxDistance, int limitRecord);
        GeoNearResult<T>.GeoNearHits GeoNear(IMongoQuery query, LatLng target, double maxDistance, int limitRecord);
        void Update(IMongoQuery query, IMongoUpdate update);
        void UpdateAll(IMongoQuery query, IMongoUpdate update);        
    }
}

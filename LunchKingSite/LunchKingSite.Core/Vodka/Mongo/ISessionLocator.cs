﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MongoDB.Driver;
using Vodka.DataAccess;

namespace Vodka.Mongo
{
    public interface ISessionLocator
    {
        MongoCollection<T> For<T>();
        IDisposable RequestStart();
    }
}

﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Caching;

namespace LunchKingSite.Core.Component
{
    public class MemoryCacheProvider : ICacheProvider
    {
        public T Get<T>(string key, bool compress = false)
        {
            return (T)HttpRuntime.Cache[key];
        }

        public bool Set(string key, object value, TimeSpan? timeout, bool compress = false)
        {
            try
            {
                HttpRuntime.Cache.Insert(key, value, null, timeout == null ? DateTime.MaxValue : DateTime.Now.Add(timeout.Value),
                    Cache.NoSlidingExpiration,
                    CacheItemPriority.Normal, null);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Remove(string key)
        {
            HttpRuntime.Cache.Remove(key);
            return true;
        }

        public CacheServerStatus GetStatus()
        {
            return CacheServerStatus.Offline;
        }

        public string GetServer()
        {
            return "localhost";
        }

        public long Increment(string key, int incrNumber = 1)
        {
            long result = 0;
            string str = Get<string>(key, false);
            if (str != null && long.TryParse(str, out result))
            {
                result++;
            }
            else
            {
                result = 1;
            }
            Set(key, result.ToString(), TimeSpan.FromDays(30));
            return result;
        }

        public T HGet<T>(string key, string fieldName, bool compress = false) where T : class
        {
            var t = HttpRuntime.Cache.Get(key) as Dictionary<string, T>;
            if (t.ContainsKey(fieldName))
            {
                return t[fieldName];
            }
            return default(T);
        }

        public Dictionary<string, T> HGetAll<T>(string key, bool compress) where T : class
        {
            var t = HttpRuntime.Cache.Get(key) as Dictionary<string, T>;
            return t;
        }

        public void HSetAll<T>(string key, Dictionary<string, T> items, bool compress = false) where T :class
        {
            HttpRuntime.Cache.Insert(key, items, null,DateTime.MaxValue,
               Cache.NoSlidingExpiration,
               CacheItemPriority.Normal, null);
        }

        public bool HSet(string key, string fieldName, object value, bool compress = false)
        {
            var items = HGetAll<object>(key, compress);
            if (items.ContainsKey(key))
            {
                items[fieldName] = value;
            }
            else
            {
                items.Add(fieldName, value);
            }
            HttpRuntime.Cache.Insert(key, items, null, DateTime.MaxValue,
               Cache.NoSlidingExpiration,
               CacheItemPriority.Normal, null);
            return true;
        }
    }
}
﻿using log4net;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Constant;
using LunchKingSite.Core.Enumeration;
using LunchKingSite.DataOrm;
using System;
using System.Collections;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Xml;
using System.Collections.Generic;
using System.Net;
using System.ComponentModel;
using ICSharpCode.SharpZipLib.Zip;
using ICSharpCode.SharpZipLib.Core;
using System.IO;
using System.IO.Compression;
using System.Linq.Expressions;
using System.Security.Cryptography;
using System.Web.Hosting;
using System.Web.Routing;
using Microsoft.VisualBasic;

namespace LunchKingSite.Core
{
    /// <summary>
    /// Helper utitlities
    /// </summary>
    public class Helper
    {
        protected static string[] Folder = { "original", "small", "large" };
        private static ILog logger = LogManager.GetLogger(typeof(Helper));


        public static string mapCnumLettersString(string txt)
        {
            string result = string.Empty;
            int tmpParseInt = 0;
            int tmpParesInt2 = 0;
            int tmpC = 0;
            txt.ForEach(x =>
            {
                if (!int.TryParse(x.ToString(), out tmpParesInt2) && int.TryParse(mapCnumLetters(x).ToString(), out tmpParseInt))
                {
                    tmpC += tmpParseInt;
                }
                else
                {
                    if (x == '十')
                    {
                        tmpC = tmpC > 0 ? tmpC * 10 : 10;
                    }
                    else
                    {
                        if (tmpC > 0)
                        {
                            result += tmpC.ToString();
                            tmpC = 0;
                        }
                        result += x;
                    }
                }
            });
            return result;
        }

        private static char mapCnumLetters(char cnum)
        {
            switch (cnum)
            {
                case '零':
                    return '0';
                case '一':
                case '壹':
                    return '1';
                case '二':
                case '貳':
                    return '2';
                case '三':
                case '參':
                    return '3';
                case '四':
                case '肆':
                    return '4';
                case '五':
                case '伍':
                    return '5';
                case '六':
                case '陸':
                    return '6';
                case '七':
                case '柒':
                    return '7';
                case '八':
                case '捌':
                    return '8';
                case '九':
                case '玖':
                    return '9';
                case '十':
                    return ' ';
                default:
                    return cnum;
            }
        }

        /// <summary>
        /// 將文字轉為半形
        /// </summary>
        /// <param name="txt"></param>
        /// <returns></returns>
        public static string ConvertToHalf(string txt)
        {
            string ori = txt;
            txt = Strings.StrConv(txt, VbStrConv.Narrow, 0).ToLower().Trim();
            //部分難字處理
            foreach (var index in txt.Where(x => x == '?'))
            {
                var i = txt.IndexOf('?');
                txt = txt.Remove(i, 1).Insert(i, ori[i].ToString());
            }
            return txt;
        }

        public static Guid GetNewGuid(object obj)
        {
            /*
             * don't know if we ever will need to use different ways of generating guids for each class
             * still we leave room for that need.
             * ex:
             *  if (obj is DataOrm.Building)
             *  {
             *      do whatever necessary...
             *  }
            */

            return System.Guid.NewGuid();
        }

        /// <summary>
        /// checks the day against the flag to determine if it matches the flag type
        /// </summary>
        /// <param name="flag">to indicate the day type</param>
        /// <param name="day">day to check</param>
        /// <returns>true if the day matches the flag, otherwise false</returns>
        public static bool IsHoliday(int flag, DateTime day)
        {
            int convertedDay = (int)0;

            // check against weekdays
            switch (day.DayOfWeek)
            {
                case DayOfWeek.Monday:
                    convertedDay = (int)DayFlag.Monday;
                    break;

                case DayOfWeek.Tuesday:
                    convertedDay = (int)DayFlag.Tuesday;
                    break;

                case DayOfWeek.Wednesday:
                    convertedDay = (int)DayFlag.Wednesday;
                    break;

                case DayOfWeek.Thursday:
                    convertedDay = (int)DayFlag.Thursday;
                    break;

                case DayOfWeek.Friday:
                    convertedDay = (int)DayFlag.Friday;
                    break;

                case DayOfWeek.Saturday:
                    convertedDay = (int)DayFlag.Saturday;
                    break;

                case DayOfWeek.Sunday:
                    convertedDay = (int)DayFlag.Sunday;
                    break;

                default:
                    // not possible to be here, something messed up
                    ;
                    break;
            }

            // check against specific days
            if (day.Month == 1 && day.Day == 1)
                convertedDay += (int)DayFlag.NewYear;
            else if (day.Month == 12 && day.Day == 31)
                convertedDay += (int)DayFlag.NewYearEve;

            return (flag & convertedDay) != 0;
        }

        /// <summary>
        /// get the date in weeks between monday and sunday start with sunday
        /// </summary>
        /// <param name="target">date of target in week</param>
        /// <param name="dt">the week date</param>
        public static DateTime GetDateOfWeek(DateTime dt, DayOfWeek target)
        {
            CultureInfo myCI = new CultureInfo("zh-tw");
            DayOfWeek dow = myCI.Calendar.GetDayOfWeek(dt);
            int diff = target - dow;
            // Correct if it's negative.
            //if (diff < 0)
            //{
            //    diff += 7;
            //}
            DateTime dtMonday = dt.AddDays(diff).Date;

            return dtMonday;
        }

        /// <summary>
        /// checks today against the flag to determine if it matches the flag type
        /// </summary>
        /// <param name="flag">to indicate the day type</param>
        /// <returns>true if the day matches the flag, otherwise false</returns>
        public static bool IsHoliday(int flag)
        {
            return IsHoliday(flag, DateTime.Today);
        }

        /// <summary>
        /// return the value according to setting/unsetting a masked value
        /// </summary>
        /// <param name="mark">true if mask is to be set, otherwise mask value would be cleared</param>
        /// <param name="value">the original value</param>
        /// <param name="mask">the masked value (flag value)</param>
        /// <returns>final value</returns>
        public static long SetFlag(bool mark, long value, long mask)
        {
            if (mark)
                return value | mask;
            else
                return value & ~mask;
        }

        public static long SetFlag(bool mark, long value, Enum mask)
        {
            return SetFlag(mark, value, Convert.ToInt64(mask));
        }

        public static long SetFlag(bool mark, Enum value, Enum mask)
        {
            return SetFlag(mark, Convert.ToInt64(value), Convert.ToInt64(mask));
        }

        public static bool IsFlagSet(long value, Enum mask)
        {
            return IsFlagSet(Convert.ToInt64(value), Convert.ToInt64(mask));
        }

        public static bool IsFlagSet(Enum value, Enum mask)
        {
            return IsFlagSet(Convert.ToInt64(value), Convert.ToInt64(mask));
        }

        public static string GetDescription(object en)
        {
            Type type = en.GetType();

            if (type == null || !type.IsEnum)
            {
                return "";
            }

            var memInfo = type.GetMember(en.ToString());

            if (memInfo != null && memInfo.Length > 0)
            {
                object[] attrs = memInfo[0].GetCustomAttributes(typeof(System.ComponentModel.DescriptionAttribute), false);

                if (attrs != null && attrs.Length > 0)
                {
                    return ((System.ComponentModel.DescriptionAttribute)attrs[0]).Description;
                }
            }

            return en.ToString();
        }

        public static string GetSmartTimeSpanString(TimeSpan span)
        {
            if (span.TotalDays > 365)
            {
                return (int)(span.TotalDays / 365) + "年前";
            }
            if (span.TotalDays > 30)
            {
                return (int)(span.TotalDays / 30) + "個月前";
            }
            if (span.TotalDays > 14)
            {
                return "2週前";
            }
            if (span.TotalDays > 7)
            {
                return "1週前";
            }
            if (span.TotalDays > 1)
            {
                return String.Format("{0}天前", (int)Math.Floor(span.TotalDays));
            }
            if (span.TotalHours > 1)
            {
                return String.Format("{0}小時前", (int)Math.Floor(span.TotalHours));
            }
            if (span.TotalMinutes > 1)
            {
                return String.Format("{0}分鍾前", (int)Math.Floor(span.TotalMinutes));
            }
            if (span.TotalSeconds >= 1)
            {
                return String.Format("{0}秒前", (int)Math.Floor(span.TotalSeconds));
            }
            return "1秒前";
        }

        public static string GetDevelopStatus<T>(int value) where T : IConvertible
        {
            Type enumType = typeof(T);
            string data = "";

            T enumData = (T)Enum.ToObject(enumType, value);

            var name = Enum.GetNames(enumType)
                    .Where(f => f.Equals(enumData.ToString(), StringComparison.CurrentCultureIgnoreCase))
                    .Select(d => d).FirstOrDefault();
            data = Helper.GetDescription(enumData);

            return data;
        }

        /// <summary>
        /// check if a flag is set
        /// </summary>
        /// <param name="value">the value to be checked</param>
        /// <param name="mask">the flag (can "or" several flags together)</param>
        /// <returns>true if flag is set, otherwise false</returns>
        public static bool IsFlagSet(long value, long mask)
        {
            return ((value & mask) > 0);
        }

        /// <summary>
        /// serialize the object into string, for debugging purpose mainly
        /// </summary>
        /// <param name="obj">the object to be printed</param>
        /// <param name="lineBreaker">the line breaker value for each line</param>
        /// <returns>resulting string represents the object</returns>
        public static string Object2String(object obj, string lineBreaker)
        {
            return Object2String(obj, lineBreaker, 0, string.Empty);
        }

        public static string Object2String(object obj)
        {
            return Object2String(obj, "\n");
        }

        public static string Object2String(object obj, string lineBreaker, int level, string indention)
        {
            Type type;
            StringBuilder sb = new StringBuilder();
            if (obj == null)
                sb.Append("the input parameter is null");
            else
            {
                type = obj.GetType();
                foreach (PropertyInfo pi in type.GetProperties())
                {
                    for (int i = 0; i < level; i++)
                        sb.Append(indention);
                    sb.Append(pi.Name);
                    if (pi.GetValue(obj, null) == null)
                        sb.Append(" is NULL");
                    else
                        sb.Append(" = [" + pi.GetValue(obj, null) + "]");
                    sb.Append(lineBreaker);
                }
            }

            return sb.ToString();
        }

        /// <summary>
        /// check the business hour against time boundaries
        /// </summary>
        /// <param name="bht">the business hour type</param>
        /// <param name="startTime">lower time boundary, date is disgarded</param>
        /// <param name="endTime">higher time boundary, date is disgarded</param>
        /// <returns>true if business type is within the time boundary</returns>
        public static bool IsBusinessHourType(BusinessHourType bht, DateTime startTime, DateTime endTime)
        {
            return IsBusinessHourType(bht, startTime.TimeOfDay, endTime.TimeOfDay);
        }

        /// <summary>
        /// check the business hour against time boundaries
        /// </summary>
        /// <param name="bht">the business hour type</param>
        /// <param name="startTime">lower time boundary</param>
        /// <param name="endTime">higher time boundary</param>
        /// <returns>true if business type is within the time boundary</returns>
        public static bool IsBusinessHourType(BusinessHourType bht, TimeSpan start, TimeSpan end)
        {
            TimeSpan[] times = new TimeSpan[] {
                new TimeSpan(0, 0, 0),  // night snack 
                new TimeSpan(4, 0, 0),  // breakfast cutoff
                new TimeSpan(8, 0, 0), // lunch cutoff
                new TimeSpan(13, 0, 0), // afternoon tea cutoff
                new TimeSpan(17, 0, 0), // dinner cutoff
                new TimeSpan(20, 0, 0), // night snack cutoff
                new TimeSpan(1, 0, 0, 0)    // day cutoff
            };
            bool result = false;

            if (bht >= BusinessHourType.Breakfast && bht <= BusinessHourType.NightSnack)
            {
                if (bht != BusinessHourType.NightSnack)
                    result = (start < times[(int)bht + 1] && end > times[(int)bht]); // if timespan overlaps, it is the type
                else
                    result = (start < times[1] && end > times[0]) || (start < times[6] && end > times[5]);
            }

            return result;
        }

        /// <summary>
        /// get the business hour type according to time
        /// </summary>
        /// <param name="currentTime">time to check, date is disgarded</param>
        /// <returns>the business hour type for the time</returns>
        public static BusinessHourType GetBusinessHourTypeByTime(DateTime currentTime)
        {
            return GetBusinessHourTypeByTime(currentTime.TimeOfDay);
        }

        /// <summary>
        /// get the business hour type according to time
        /// </summary>
        /// <param name="currentTime">time to check</param>
        /// <returns>the business hour type for the time</returns>
        public static BusinessHourType GetBusinessHourTypeByTime(TimeSpan currentTime)
        {
            BusinessHourType type = BusinessHourType.Breakfast;
            for (; type <= BusinessHourType.NightSnack && !IsBusinessHourType(type, currentTime, currentTime); type++) ;

            return type;
        }

        public static TrustProvider GetBusinessHourTrustProvider(int status)
        {
            return (TrustProvider)((status & (int)BusinessHourStatus.TrustMask) >> (int)BusinessHourStatusBitShift.TrustMask);
        }

        public static object GetPropValue(object src, string propName)
        {
            return src.GetType().GetProperty(propName).GetValue(src, null);
        }

        public static int SetBusinessHourTrustProvider(int origStatus, TrustProvider sType)
        {
            return (origStatus & (int)~BusinessHourStatus.TrustMask) | (int)sType << (int)BusinessHourStatusBitShift.TrustMask;
        }

        public static PayTransPhase GetPaymentTransactionPhase(int status)
        {
            return (PayTransPhase)((status & (int)PayTransStatusFlag.PhaseMask) >> (int)PayTransStatusBitShift.Charge);
        }

        public static int SetPaymentTransactionPhase(int origStatus, PayTransPhase status)
        {
            return (origStatus & (int)~PayTransStatusFlag.PhaseMask) | (int)status << (int)PayTransStatusBitShift.Charge;
        }

        /// <summary>
        /// 查詢交易時所記錄的串接服務廠商
        /// </summary>
        /// <param name="apiProvider">數字代碼</param>
        /// <returns>廠商代碼</returns>
        /// <remarks>如果傳入的值未定義，則傳回測試用代碼 (Mock)</remarks>
        public static PaymentAPIProvider GetPaymentAPIProvider(int? apiProvider)
        {
            if (apiProvider.HasValue && Enum.IsDefined(typeof(PaymentAPIProvider), apiProvider.Value))
            {
                return (PaymentAPIProvider)(apiProvider.Value);
            }
            else
            {
                return PaymentAPIProvider.Mock;
            }
        }

        public static DepartmentTypes GetPaymentTransactionDepartmentTypes(int status)
        {
            return (DepartmentTypes)((status & (int)PayTransStatusFlag.DepartmentMask) >> (int)PayTransStatusBitShift.Department);
        }

        public static int SetPaymentTransactionDepartmentTypes(int origStatus, DepartmentTypes status)
        {
            return (origStatus & (int)~PayTransStatusFlag.DepartmentMask) | (int)status << (int)PayTransStatusBitShift.Department;
        }

        /// <summary>
        /// split a Chinese address line into segment of street name and the rest
        /// </summary>
        /// <param name="addrStr">the input address</param>
        /// <param name="streetName">name of the street for the address</param>
        /// <param name="addressNo">rest of the address value</param>
        public static void SplitAddress(string addrStr, out string streetName, out string addressNo)
        {
            // an easy way to split address into the number & street name
            Regex rex = new Regex(@"(\D+\d*路(.+段)*)(.*)$");
            Match m = rex.Match(addrStr);
            streetName = addressNo = null;
            if (m.Success)
            {
                streetName = m.Groups[1].Value;
                addressNo = m.Groups[3].Value;
            }
            else
                streetName = addrStr;
        }

        public static string GetLocalizedEnum(Enum value)
        {
            return GetLocalizedEnum(I18N.Phrase.ResourceManager, value);
        }

        public static string GetLocalizedEnum(MemberInfo info)
        {
            return GetLocalizedEnum(I18N.Phrase.ResourceManager, info);
        }

        /// <summary>
        /// translate the enum value to the localized presentation
        /// </summary>
        /// <param name="source">the .NET resource manager storing the translation info</param>
        /// <param name="value">enum value</param>
        /// <returns>localized string for the enum</returns>
        public static string GetLocalizedEnum(ResourceManager source, Enum value)
        {
            FieldInfo info = value.GetType().GetField(value.ToString());
            return GetLocalizedEnum(source, info);
        }

        public static string GetLocalizedEnum(ResourceManager source, MemberInfo info)
        {
            if (info == null) // threw in somehting that's out of range of the enum?
                return string.Empty;

            object[] fobjs = info.GetCustomAttributes(typeof(LocalizationAttribute), false);
            if (source != null && fobjs.Length > 0)
            {
                string txt = source.GetString(((LocalizationAttribute)fobjs[0]).Key);
                return txt ?? info.Name;
            }
            return info.Name;
        }
        /// <summary>
        /// 取得列舉裏，宣告Description屬性的值
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value"></param>
        /// <returns></returns>
        public static T GetEnumAttribue<T>(Enum value) where T : System.Attribute
        {
            object[] fobjs = value.GetType().GetField(value.ToString()).GetCustomAttributes(typeof(T), false);
            return fobjs.Length > 0 ? (T)fobjs[0] : null;
        }

        /// <summary>
        /// Extract delivery address from member record
        /// </summary>
        /// <param name="mem">The mem.</param>
        /// <returns></returns>
        public static string MemberDeliveryAddress(ViewMemberBuildingCity mem)
        {
            string delAddr = string.Empty;
            delAddr += mem.CompanyAddress;
            if (!string.IsNullOrEmpty(mem.DeliveryMethod))
                delAddr += string.Format("({0})", mem.DeliveryMethod);
            return delAddr;
        }

        public static string MemberName(ViewMemberBuildingCity mem)
        {
            return mem.LastName + " " + mem.FirstName;
        }

        public static string MemberPhone(ViewMemberBuildingCity mem)
        {
            string pn = mem.CompanyTel;
            if (!string.IsNullOrEmpty(mem.CompanyTelExt))
                pn += " #" + mem.CompanyTelExt;
            return pn;
        }

        public static bool IsFutureOrder(DateTime date)
        {
            return date.Date >= GlobalProperty.TimeBase.Date.AddDays(1);
        }

        public static bool IsFutureOrder(BusinessHour bh)
        {
            return IsFutureOrder(bh.BusinessHourOrderTimeS);
        }

        public static string GetExtensionByContentType(string typeString)
        {
            string ret = string.Empty;
            switch (typeString)
            {
                case "image/pjpeg":
                case "image/jpeg":
                    ret = "jpg";
                    break;
                case "image/png":
                case "image/x-png":
                    ret = "png";
                    break;

                case "image/gif":
                    ret = "gif";
                    break;

                default:
                    ret = "jpg";
                    break;
            }
            return ret;
        }

        public static DateTime PowerExtractDateTime(string dateTimeString)
        {
            string[] dtList = {
                            "yyyy/M/d tt hh:mm:ss",
                            "yyyy/MM/dd tt hh:mm:ss",
                            "yyyy/MM/dd HH:mm:ss",
                            "yyyy/M/d HH:mm:ss",
                            "yyyy/M/d",
                            "yyyy/MM/dd"
                        };
            return DateTime.ParseExact(dateTimeString, dtList, CultureInfo.InvariantCulture, DateTimeStyles.AllowWhiteSpaces);
        }

        public enum TimeRoundingDirection { Up, Down, Nearest };

        public static DateTime RoundDateTime(DateTime dt, int minutes, TimeRoundingDirection dir)
        {
            TimeSpan t;
            switch (dir)
            {
                case TimeRoundingDirection.Up:
                    t = (dt.Subtract(DateTime.MinValue)).Add(new TimeSpan(0, minutes, 0)); break;
                case TimeRoundingDirection.Down:
                    t = (dt.Subtract(DateTime.MinValue)); break;
                default:
                    t = (dt.Subtract(DateTime.MinValue)).Add(new TimeSpan(0, minutes / 2, 0)); break;
            }

            return DateTime.MinValue.Add(new TimeSpan(0,
                   (((int)t.TotalMinutes) / minutes) * minutes, 0));
        }

        public static ZoneType GetZoneType(int? parentId, CityStatus status)
        {
            if (parentId == null)
                return status == CityStatus.Virtual ? ZoneType.TopCompany : ZoneType.TopCity;

            return status == CityStatus.Virtual ? ZoneType.Company : ZoneType.District;
        }

        public static ZoneType GetZoneType(int cityId)
        {
            ILocationProvider lp = ProviderFactory.Instance().GetProvider<ILocationProvider>();
            City c = lp.CityGet(cityId);
            if (c == null)
                return ZoneType.Unknown;
            return GetZoneType(c.ParentId, c.Status == null ? CityStatus.Nothing : (CityStatus)c.Status);
        }

        public static string GenerateRawDataFromRawPaths(params string[] paths)
        {
            return string.Join("|", paths);
        }

        public static string AppendRawDataWithRawPath(string rawData, string pathToAppend)
        {
            return string.IsNullOrEmpty(rawData) ? pathToAppend : rawData + "|" + pathToAppend;
        }

        /// <summary>
        /// 傳遞圖檔路徑的字串進來，將資料已'|'分隔開來後，回傳string的array
        /// </summary>
        /// <param name="rawData"></param>
        /// <returns></returns>
        public static string[] GetRawPathsFromRawData(string rawData)
        {
            return string.IsNullOrEmpty(rawData) ? new string[0] : rawData.Split('|');
        }

        public static string GetTimeSpanString(ResourceManager localization, TimeSpan ts)
        {
            string result = string.Empty;
            if (ts.Days > 0)
                result += ts.Days + " " + localization.GetString("Day");
            if (ts.Hours > 0)
                result += ts.Hours + " " + localization.GetString("Hour");
            if (ts.Minutes > 0)
                result += ts.Minutes + " " + localization.GetString("Minute");

            return result;
        }

        public static int YieldOrderConfirmCode(string orderId, uint minValue, uint maxValue)
        {
            string seed = orderId.Remove(orderId.Length - 3);
            uint orderN = Convert.ToUInt32(orderId.Substring(orderId.Length - 3));
            MersenneTwister mt = new MersenneTwister((uint)Math.Abs(seed.GetHashCode()));

            return (int)mt.NextUIntAtOrderN(minValue, maxValue, orderN);
        }

        public static bool IsNumeric(object Expression)
        {
            bool isNum;
            double retNum;
            isNum = double.TryParse(Convert.ToString(Expression), System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.InvariantInfo, out retNum);

            return isNum;
        }

        public static DateTime ChineseDateTimeParse(string date)
        {
            return DateTime.Parse((Convert.ToInt32(date.Substring(0, 4)) + 1911).ToString() + "-" + date.Substring(4, 2) + "-" + date.Substring(6, 2));
        }

        public static string ToChineseDateTime(DateTime date)
        {
            return (date.Year - 1911).ToString().PadLeft(3, '0') + date.Month.ToString().PadLeft(2, '0') + date.Day.ToString().PadLeft(2, '0');
        }

        /// <summary>
        /// 取得該日的最後時間
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static DateTime GetFinalTime(DateTime date)
        {
            return new DateTime(date.Date.Year, date.Date.Month, date.Day, 23, 59, 59);
        }

        #region DataManagerResettime相關

        /// <summary>
        /// 依據傳入的lastRunningTime與設定，取得此次執行之後，下次的執行時間。
        /// </summary>
        /// <param name="lastRunningTime">最後一次執行的時間</param>
        /// <param name="data">排程紀錄的原始資料</param>
        /// <returns></returns>
        public static DateTime GetNextDataManagerResetRunningTime(DateTime lastRunningTime, DataManagerResettime data)
        {
            var rtnTime = new DateTime();
            switch ((DataManagerResettimeJobType)data.JobType)
            {
                case DataManagerResettimeJobType.Always:
                    rtnTime = DateTime.Now;
                    break;

                case DataManagerResettimeJobType.IntervalMinute:
                    rtnTime = lastRunningTime.AddMinutes(data.IntervalMinute);
                    break;

                case DataManagerResettimeJobType.EachHour:
                    //rtnTime = data.NextRunningTime.AddHours(1);
                    //rtnTime = lastRunningTime.AddHours(1);
                    rtnTime = lastRunningTime.Date.AddHours(lastRunningTime.Hour + 1).AddMinutes(data.IntervalMinute);
                    break;

                case DataManagerResettimeJobType.EachDay:
                    //rtnTime = data.NextRunningTime.AddDays(1);
                    //rtnTime = lastRunningTime.AddDays(1);
                    rtnTime = lastRunningTime.Date.AddDays(1).AddHours(data.IntervalMinute);
                    break;

                case DataManagerResettimeJobType.EachWeek:
                    //rtnTime = data.NextRunningTime.AddDays(7);
                    //rtnTime = lastRunningTime.AddDays(7);
                    //If cast to an integer, its value ranges from zero (which indicates DayOfWeek.Sunday) to six (which indicates DayOfWeek.Saturday).
                    rtnTime = lastRunningTime.GetNextWeekday(DayOfWeek.Sunday).Date.AddDays(data.IntervalMinute);
                    break;

                case DataManagerResettimeJobType.EachMonth:
                    //rtnTime = data.NextRunningTime.AddMonths(1);
                    var temp = lastRunningTime.AddMonths(1);
                    rtnTime = new DateTime(temp.Year, temp.Month, 1).Date.AddDays(data.IntervalMinute);

                    break;
                case DataManagerResettimeJobType.RunOnce:
                    rtnTime = data.NextRunningTime;
                    break;
            }
            return rtnTime;
        }

        #endregion DataManagerResettime相關

        public static XmlElement ObjectToXmlElement<T>(T data, XmlElement root, XmlDocument doc)
        {
            foreach (var field in data.GetType().GetFields())
            {
                if (field.GetCustomAttributes(typeof(XmlElementNameAttribute), true).Length <= 0) continue;

                Type valueType = field.FieldType;
                if (typeof(IList).IsAssignableFrom(field.FieldType))
                {
                    var nodeName = GetXmlElementAttrubuteName(field);
                    if (!string.IsNullOrWhiteSpace(nodeName))
                    {
                        var node = doc.CreateElement(nodeName);
                        if (field.GetValue(data) == null) continue;
                        foreach (var obj in (IEnumerable)field.GetValue(data))
                        {
                            node = ObjectToXmlElement(obj, node, doc);
                        }
                        root.AppendChild(node);
                    }
                }
                else
                {
                    var nodeName = GetXmlElementAttrubuteName(field);
                    if (!string.IsNullOrWhiteSpace(nodeName))
                    {
                        var node = doc.CreateElement(nodeName);
                        var value = field.GetValue(data);
                        if (value != null)
                        {
                            node.InnerText = field.GetValue(data).ToString();
                        }
                        root.AppendChild(node);
                    }
                }
            }
            return root;
        }

        public static string GetXmlElementAttrubuteName(FieldInfo info)
        {
            if (info == null) // threw in somehting that's out of range of the enum?
                return string.Empty;

            object[] fobjs = info.GetCustomAttributes(typeof(XmlElementNameAttribute), false);
            if (fobjs.Length > 0)
            {
                string txt = ((XmlElementNameAttribute)fobjs[0]).Name;
                return txt ?? info.Name;
            }
            return info.Name;
        }

        public static string BytesToHex(byte[] bytes)
        {
            StringBuilder hexString = new StringBuilder(bytes.Length);
            for (int i = 0; i < bytes.Length; i++)
            {
                hexString.Append(bytes[i].ToString("X2"));
            }
            return hexString.ToString();
        }

        public static int GetCurrentUnixTimestamp()
        {
            return GetUnixTimeStamp(DateTime.Now);
        }

        public static int GetUnixTimeStamp(DateTime dateTime)
        {
            DateTime UnixEpoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            return (int)(DateTime.Now.ToUniversalTime() - UnixEpoch).TotalSeconds;
        }

        public static string GetCurrentTimeStampHashCode()
        {
            string stamp = DateTime.Now.ToString();
            string hashCode = string.Format("{0:X}", stamp.GetHashCode());
            return hashCode;
        }

        public static string[] GetMediaPathsFromRawData(string rawData, MediaType type, string mediaBaseUrl)
        {
            string[] paths = Helper.GetRawPathsFromRawData(rawData);
            for (int i = 0; i < paths.Length; i++)
                paths[i] = GetMediaPath(paths[i], type, mediaBaseUrl);
            return paths;
        }

        /// <summary>
        /// "取消" sql的like查詢時，底線_ 會作單一字元的模糊搜尋
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static string GetIgnoreSqlLikeUnderineMatch(string s)
        {
            return s.Replace("[_]", "_").Replace("_", "[_]");
        }

        private static string GetMediaPath(string rawPath, MediaType type, string mediaBaseUrl)
        {
            if (!string.IsNullOrEmpty(rawPath))
            {
                // if path is an url, use it instead of parse it
                if (rawPath.IndexOf("http://", StringComparison.InvariantCultureIgnoreCase) >= 0)
                    return rawPath;

                string infix = string.Empty;
                switch (type)
                {
                    case MediaType.SellerPhotoOriginal:
                    case MediaType.ItemPhotoOriginal:
                        infix = Folder[0].ToString() + "/";
                        break;

                    case MediaType.SellerPhotoSmall:
                    case MediaType.ItemPhotoSmall:
                        infix = Folder[1].ToString() + "/";
                        break;

                    case MediaType.SellerPhotoLarge:
                    case MediaType.ItemPhotoLarge:
                        infix = Folder[2].ToString() + "/";
                        break;

                    default:
                        break;
                }

                switch (type)
                {
                    case MediaType.ItemPhotoLarge:
                    case MediaType.ItemPhotoOriginal:
                    case MediaType.ItemPhotoSmall:
                        infix += "item/";
                        break;

                    default:
                        break;
                }

                string[] fields = rawPath.Split(',');
                return mediaBaseUrl + (fields.Length > 0 ? fields[0] + "/" + infix + (fields.Length > 1 ? fields[1] : string.Empty) : string.Empty);
            }
            else
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// 可判斷中英數字最大長度
        /// </summary>
        /// <param name="input"></param>
        /// <param name="max"></param>
        /// <param name="suffix"></param>
        /// <returns></returns>
        public static string GetMaxString(string input, int max, string suffix)
        {
            string return_string = string.Empty;
            if (!string.IsNullOrEmpty(input) && (input.Length <= max || (Encoding.GetEncoding(950).GetBytes(input).Length <= max * 2)))
            {
                return_string = input;
            }
            else if (string.IsNullOrEmpty(input))
            {
                return_string = "";
            }
            else
            {
                for (int i = 0; i <= input.Length; i++)
                {
                    if (Encoding.GetEncoding(950).GetBytes(input.Substring(0, i)).Length >= max * 2 && i > 0)
                    {
                        return_string = input.Substring(0, i - 1) + suffix;
                        break;
                    }
                }
            }
            return return_string;
        }

        public static string GetImgUrl(string path)
        {
            ISysConfProvider config = ProviderFactory.Instance().GetConfig();
            return !string.IsNullOrWhiteSpace(path) ?
                Helper.GetMediaPathsFromRawData(path, MediaType.PponDealPhoto, config.MediaBaseUrl).DefaultIfEmpty(string.Empty).First() :
                config.SiteUrl + config.EDMDefaultImage;
        }

        public static string GetEnumDescription(Enum value)
        {
            FieldInfo fi = value.GetType().GetField(value.ToString());
            if (fi == null) return string.Empty;

            DescriptionAttribute[] attributes =
                (DescriptionAttribute[])fi.GetCustomAttributes(
                typeof(DescriptionAttribute),
                false);

            if (attributes != null &&
                attributes.Length > 0)
                return attributes[0].Description;
            else
                return value.ToString();
        }

        public static List<KeyValuePair<string, int>> GetEnumKeyValueList<T>()
        {
            List<KeyValuePair<string, int>> result = new List<KeyValuePair<string, int>>();
            Type type = typeof(T);

            string[] names = Enum.GetNames(type);
            foreach (var name in names)
            {
                var field = type.GetField(name);
                DescriptionAttribute fd = (DescriptionAttribute)
                    field.GetCustomAttributes(typeof(DescriptionAttribute), true).FirstOrDefault();
                string key = fd == null ? field.Name : fd.Description;
                int val = (int)Enum.Parse(type, name);
                result.Add(new KeyValuePair<string, int>(key, val));
            }
            return result;
        }

        public static string GetImgUrlWithCheck(string path)
        {
            ISysConfProvider config = ProviderFactory.Instance().GetConfig();
            try
            {
                string url = GetImgUrl(path);
                var request = WebRequest.Create(url);
                using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
                {
                    if (response != null && response.StatusCode == HttpStatusCode.OK)
                    {
                        return url;
                    }
                    else
                    {
                        return config.SiteUrl + config.EDMDefaultImage;
                    }
                }
            }
            catch (WebException)
            {
                return config.SiteUrl + config.EDMDefaultImage;
            }
        }

        public static bool CheckRemoteImageExists(string path)
        {
            try
            {
                Uri url = new Uri(path);
                var request = WebRequest.Create(url);
                request.Method = "HEAD";
                using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
                {
                    if (response != null && response.StatusCode == HttpStatusCode.OK)
                    {
                        return true;
                    }
                }
                return false;
            }
            catch (WebException ex)
            {
                return false;
            }
        }

        public static int GetQuarterByDate(DateTime date)
        {
            int[] quarters = new int[] { 1, 1, 1, 2, 2, 2, 3, 3, 3, 4, 4, 4 };
            return quarters[date.Month - 1];
        }

        public static bool IsTwoDateInSameQuarter(DateTime d1, DateTime d2)
        {
            if (d1.Year == d2.Year && Helper.GetQuarterByDate(d1) == Helper.GetQuarterByDate(d2))
            {
                return true;
            }

            return false;
        }

        public static string CombineUrl(string baseUrl, string relUrl)
        {
            if (string.IsNullOrEmpty(baseUrl) && string.IsNullOrEmpty(relUrl))
            {
                return "";
            }
            if (string.IsNullOrEmpty(baseUrl))
            {
                return relUrl;
            }
            if (string.IsNullOrEmpty(relUrl))
            {
                return baseUrl;
            }
            if (baseUrl.EndsWith("/") && relUrl.StartsWith("/"))
            {
                return string.Format("{0}{1}", baseUrl, relUrl.Substring(1));
            }
            if (baseUrl.EndsWith("/") && relUrl.StartsWith("/") == false)
            {
                return string.Format("{0}{1}", baseUrl, relUrl);
            }
            if (baseUrl.EndsWith("/") == false && relUrl.StartsWith("/"))
            {
                return string.Format("{0}{1}", baseUrl, relUrl);
            }
            return string.Format("{0}/{1}", baseUrl, relUrl);
        }

        public static string CombineUrl(string baseUrl, params string[] relUrls)
        {
            if (relUrls.Length == 0)
            {
                return baseUrl;
            }
            string result = baseUrl;
            foreach (string relUrl in relUrls)
            {
                result = CombineUrl(result, relUrl);
            }
            return result;
        }

        /// <summary>
        /// 取得Client端真實IP Address
        /// </summary>
        /// <returns></returns>
        public static string GetClientIP(bool simple = false)
        {
            if (HttpContext.Current == null)
            {
                return string.Empty;
            }
            if (simple)
            {
                return HttpContext.Current.Request.UserHostAddress;
            }
            string ip = string.Empty;
            if (HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"] == null)
            {
                ip = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
            }
            else
            {
                if (HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString(CultureInfo.InvariantCulture) == string.Empty || HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToUpper().IndexOf("UNKNOWN", System.StringComparison.Ordinal) > 0)
                {
                    ip = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
                }
                else if (HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"].IndexOf(",", System.StringComparison.Ordinal) > 0)
                {
                    ip = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"].Substring(1, HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"].IndexOf(",", System.StringComparison.Ordinal) - 1);
                }
                else if (HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"].IndexOf(";", System.StringComparison.Ordinal) > 0)
                {
                    ip = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"].Substring(1, HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"].IndexOf(";", System.StringComparison.Ordinal) - 1);
                }
                else
                {
                    ip = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
                }
            }

            ip = ip.Replace(" ", string.Empty);

            if (ip.Contains('\''))
            {
                logger.WarnFormat("偵測到 GetClientIp 有奇怪符號, ServerVariables= {0}",
                    HttpContext.Current.Request.ServerVariables);
            }
            return ip.Replace(" ", string.Empty);
        }


        /// <summary>
        /// service@17life.com => 17life.com
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public static string GetEmailDomain(string email)
        {
            if (string.IsNullOrEmpty(email))
            {
                return string.Empty;
            }
            int pos = email.IndexOf('@');
            if (pos == -1)
            {
                return string.Empty;
            }
            return email.Substring(pos + 1);
        }

        /// <summary>
        /// service@17life.com => service
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public static string GetEmailLocal(string email)
        {
            if (string.IsNullOrEmpty(email))
            {
                return string.Empty;
            }
            int pos = email.IndexOf('@');
            if (pos == -1)
            {
                return string.Empty;
            }
            return email.Substring(0, pos);
        }

        /// <summary>
        /// service@17life.com => s*****e@17life.com
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public static string MaskEmail(string email)
        {
            string local = GetEmailLocal(email);
            if (local.Length > 2)
            {
                return string.Format("{0}{1}{2}@{3}",
                    local.Substring(0, 1),
                    new string('*', local.Length - 2),
                    local.Substring(local.Length - 1, 1),
                    GetEmailDomain(email));
            }
            return email;
        }

        public static string MaskMobile(string mobile)
        {
            if (mobile.Length > 2)
            {
                return string.Format("{0}****{1}",
                    mobile.Substring(0, 4),
                    mobile.Substring(mobile.Length - 2, 2));
            }
            return mobile;
        }

        public static string MaskCreditCard(string cardNumber)
        {
            if (cardNumber != null && cardNumber.Length > 10)
            {
                string part1 = cardNumber.Substring(0, 6);
                string part3 = cardNumber.Substring(cardNumber.Length - 4, 4);
                string part2 = new string('*', cardNumber.Length - part1.Length - part3.Length);
                return part1 + part2 + part3;
            }
            return cardNumber;
        }

        public static string MaskCreditCardSecurityCode(string securityCode)
        {
            if (string.IsNullOrEmpty(securityCode) == false)
            {
                return new string('*', securityCode.Length);
            }
            return securityCode;
        }

        /// <summary>
        /// 壓縮資料檔加密
        /// </summary>
        /// <param name="base64String"></param>
        /// <param name="password"></param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public static string ZipFileConvert(string base64String, string fileName, string password)
        {
            string result = string.Empty;
            var btye = Convert.FromBase64String(base64String);
            MemoryStream ms = new MemoryStream(btye);


            MemoryStream output = new MemoryStream();
            ZipOutputStream zos = new ZipOutputStream(output);
            zos.Password = password;
            zos.SetLevel(3);
            ZipEntry newEntry = new ZipEntry(fileName);
            newEntry.DateTime = DateTime.Now;
            zos.PutNextEntry(newEntry);
            StreamUtils.Copy(ms, zos, new byte[4096]);
            zos.CloseEntry();
            zos.IsStreamOwner = false;
            zos.Close();
            output.Position = 0;

            var newbtye = output.ToArray();
            result = Convert.ToBase64String(newbtye);
            return result;
        }

        public static string MaskAnyWords(string words)
        {
            Regex re = new Regex("", RegexOptions.None);
            int wordslen = words.Length;
            switch (wordslen)
            {
                case 5:
                    re = new Regex("(\\w{3})(\\w{2})", RegexOptions.None);
                    words = re.Replace(words, "$1****");
                    break;
                case 6:
                    re = new Regex("(\\w{3})(\\d{3})", RegexOptions.None);
                    words = re.Replace(words, "$1****");
                    break;
                case 7:
                    re = new Regex("(\\w{3})(\\w{4})", RegexOptions.None);
                    words = re.Replace(words, "$1****");
                    break;
                case 8:
                    re = new Regex("(\\w{3})(\\w{4})(\\w{1})", RegexOptions.None);
                    words = re.Replace(words, "$1****$3");
                    break;
                case 9:
                    re = new Regex("(\\w{3})(\\w{4})(\\w{2})", RegexOptions.None);
                    words = re.Replace(words, "$1****$3");
                    break;
                case 10:
                    re = new Regex("(\\w{3})(\\w{4})(\\w{3})", RegexOptions.None);
                    words = re.Replace(words, "$1****$3");
                    break;
                case 11:
                    re = new Regex("(\\w{3})(\\w{4})(\\w{4})", RegexOptions.None);
                    words = re.Replace(words, "$1****$3");
                    break;
                case 12:
                    re = new Regex("(\\w{3})(\\w{4})(\\w{5})", RegexOptions.None);
                    words = re.Replace(words, "$1****$3");
                    break;
                default:
                    re = new Regex("(\\w{3})(\\w{4})(\\w)", RegexOptions.None);
                    words = re.Replace(words, "$1****$3");
                    break;
            }
            return words;
        }

        #region 複姓

        private static HashSet<string> compoundSurname = new HashSet<string>(
            new string[]
            {
                "歐陽",
                "太史",
                "端木",
                "上官",
                "司馬",
                "東方",
                "獨孤",
                "南宮",
                "万俟",
                "聞人",
                "夏侯",
                "諸葛",
                "尉遲",
                "公羊",
                "赫連",
                "澹台",
                "皇甫",
                "宗政",
                "濮陽",
                "公冶",
                "太叔",
                "申屠",
                "公孫",
                "慕容",
                "仲孫",
                "鍾離",
                "長孫",
                "宇文",
                "司徒",
                "鮮於",
                "司空",
                "閭丘",
                "子車",
                "亓官",
                "司寇",
                "巫馬",
                "公西",
                "顓孫",
                "壤駟",
                "公良",
                "漆雕",
                "樂正",
                "宰父",
                "穀梁",
                "拓跋",
                "夾谷",
                "軒轅",
                "令狐",
                "段幹",
                "百里",
                "呼延",
                "東郭",
                "南門",
                "羊舌",
                "微生",
                "公戶",
                "公玉",
                "公儀",
                "梁丘",
                "公仲",
                "公上",
                "公門",
                "公山",
                "公堅",
                "左丘",
                "公伯",
                "西門",
                "公祖",
                "第五",
                "公乘",
                "貫丘",
                "公皙",
                "南榮",
                "東裡",
                "東宮",
                "仲長",
                "子書",
                "子桑",
                "即墨",
                "達奚",
                "褚師",
                "吳銘",
            }
            );
        /// <summary>
        /// 123.png?ts=123
        /// 移掉?後的
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public static string GetFileNameWithoutCacheBusting(string fileName)
        {
            if (string.IsNullOrEmpty(fileName))
            {
                return fileName;
            }
            int index = fileName.LastIndexOf('?');
            if (index != -1)
            {
                fileName = fileName.Substring(0, index);
            }
            return Path.GetFileName(fileName);
        }
        #endregion

        #region 字元檢查排除
        /// <summary>
        /// 排除跳脫字元
        /// </summary>
        /// <param name="content">htmlcontent</param>
        /// <returns></returns>
        public static string RemoveControlChar(string content)
        {
            string output = "";
            if (!string.IsNullOrEmpty(content))
            {
                foreach (char c in content)//走訪字串的每個字元
                {
                    int asciiInt = Convert.ToInt32(c);
                    if ((asciiInt >= 0 && asciiInt <= 31) == false)
                    {//非控制字元
                        output += c;//就把合法的字元累加到output字串
                    }

                }//end foreach
            }

            return output;
        }

        #endregion 字元檢查排除

        /// <summary>
        /// 判斷字元是否為中文字
        /// </summary>
        /// <param name="ch"></param>
        /// <returns></returns>
        public static bool IsChinese(char ch)
        {
            return char.GetUnicodeCategory(ch) == UnicodeCategory.OtherLetter;
        }

        /// <summary>
        /// 判斷字元是否為繁體字
        /// </summary>
        /// <param name="word"></param>
        /// <returns></returns>
        public static bool IsBig5Code(char word)
        {
            byte[] bytes = Encoding.GetEncoding("Big5").GetBytes(word.ToString());
            if (bytes.Length <= 1) // if there is only one byte, it is ASCII code
            {
                return false;
            }
            else
            {
                byte byte1 = bytes[0];
                byte byte2 = bytes[1];
                if ((byte1 >= 129 && byte1 <= 254) && ((byte2 >= 64 && byte2 <= 126) || (byte2 >= 161 && byte2 <= 254))) //判断是否是Big5编码
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        /// <summary>
        /// 取得姓名裡的姓
        /// </summary>
        /// <param name="memberName"></param>
        /// <param name="givenName"></param>
        /// <returns></returns>
        public static string GetFamilyName(string memberName, out string givenName)
        {
            if (string.IsNullOrEmpty(memberName))
            {
                givenName = string.Empty;
                return string.Empty;
            }
            memberName = memberName.Trim();

            if (memberName.Length == 0)
            {
                givenName = string.Empty;
                return string.Empty;
            }

            if (memberName.Length == 1)
            {
                givenName = string.Empty;
                return memberName;
            }

            //以第1個字元來決定要當中文姓名來處理，還是英文姓名
            bool isChineseName = IsChinese(memberName[0]);

            if (isChineseName)
            {
                if (memberName.Length >= 2)
                {
                    string tmp = memberName.Substring(0, 2);
                    if (compoundSurname.Contains(tmp))
                    {
                        givenName = memberName.Substring(2).Trim();
                        return tmp;
                    }
                    givenName = memberName.Substring(1).Trim();
                    return memberName.Substring(0, 1);
                }
            }
            //當做英文姓名來處理
            int pos = memberName.LastIndexOf(',');
            if (pos != -1)
            {
                givenName = memberName.Substring(pos + 1).Trim();
                return memberName.Substring(0, pos).Trim();
            }
            pos = memberName.LastIndexOf(' ');
            if (pos != -1)
            {
                givenName = memberName.Substring(0, pos).Trim();
                return memberName.Substring(pos + 1).Trim();
            }

            givenName = string.Empty;
            return memberName;
        }

        public static OrderFromType GetOrderFromType()
        {
            //web servie, webapi 才可能設定 LkSiteContextItem._DEVICE_TYPE
            object deviceType = GetContextItem(LkSiteContextItem._DEVICE_TYPE);
            if (deviceType is int)
            {
                int deviceId = (int)deviceType;
                if (deviceId == (int)ApiUserDeviceType.WebService)
                {
                    return OrderFromType.ByWebService;
                }
                if (deviceId == (int)ApiUserDeviceType.Android)
                {
                    return OrderFromType.ByAndroid;
                }
                if (deviceId == (int)ApiUserDeviceType.IOS)
                {
                    return OrderFromType.ByIOS;
                }
            }

            if (HttpContext.Current == null)
            {
                return OrderFromType.Unknown;
            }

            string userAgent;
            try
            {
                userAgent = HttpContext.Current.Request.UserAgent;
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("無法取得UserAgent {0}", ex.Message);
                return OrderFromType.Unknown;
            }

            if (string.IsNullOrWhiteSpace(userAgent))
            {
                return OrderFromType.Unknown;
            }

            if (userAgent.StartsWith("App17userId/AND", StringComparison.OrdinalIgnoreCase))
            {
                return OrderFromType.ByAndroid;
            }
            if (userAgent.StartsWith("App17userId/IOS", StringComparison.OrdinalIgnoreCase))
            {
                return OrderFromType.ByIOS;
            }

            if (HttpContext.Current.Request.Browser.IsMobileDevice || userAgent.Contains(MobileOsType.Android.ToString(), StringComparison.OrdinalIgnoreCase))
            {
                // WinPhone 8.1 IE11 UserAgent包含Android, iphone等字串，需於第一個條件先排除。"Mozilla/5.0 (Mobile; Windows Phone 8.1; Android 4.0; ARM; Trident/7.0; Touch; rv:11.0; IEMobile/11.0; NOKIA; Lumia 520) like iPhone OS 7_0_3 Mac OS X AppleWebKit/537 (KHTML, like Gecko) Mobile Safari/537"
                if (userAgent.Contains("Windows Phone", StringComparison.OrdinalIgnoreCase))
                {
                    return OrderFromType.ByMweb;
                }
                if (userAgent.Contains(MobileOsType.Android.ToString(), StringComparison.OrdinalIgnoreCase))
                {
                    return OrderFromType.ByMwebAndroid;
                }
                if (userAgent.Contains("ipad", StringComparison.OrdinalIgnoreCase))
                {
                    return OrderFromType.ByMwebiPad;
                }
                if (userAgent.Contains("iphone", StringComparison.OrdinalIgnoreCase))
                {
                    return OrderFromType.ByMwebiPhone;
                }
                if (userAgent.Contains("ipod", StringComparison.OrdinalIgnoreCase))
                {
                    return OrderFromType.ByMwebiPod;
                }
                return OrderFromType.ByMweb;
            }
            return OrderFromType.ByWeb;
        }

        public static void SetContextItem(string key, object val)
        {
            if (HttpContext.Current == null)
            {
                return;
            }
            HttpContext.Current.Items[key] = val;
        }

        public static object GetContextItem(string key)
        {
            if (HttpContext.Current == null)
            {
                return null;
            }
            return HttpContext.Current.Items[key];
        }

        /// <summary>
        /// 略過靜態資源的Request不處理，如果哪天網站的 application pool 都用整合模式，這個判斷是不需要的，只要
        /// 掛載 module 時加上 preCondition="managedHandler"即可
        /// </summary>
        /// <returns></returns>
        public static bool IsManagedHandler()
        {
            int pos = HttpContext.Current.Request.Url.LocalPath.LastIndexOf('.');
            if (pos > -1)
            {
                string ext = HttpContext.Current.Request.Url.LocalPath.Substring(pos).ToLower();
                if (ext == ".jpg" || ext == ".js" || ext == ".gif" || ext == ".png" || ext == ".axd")
                {
                    return false;
                }
            }
            return true;
        }

        public static bool CheckSqlDateTime(DateTime dt)
        {
            if (dt > System.Data.SqlTypes.SqlDateTime.MaxValue.Value)
            {
                return false;
            }
            if (dt < System.Data.SqlTypes.SqlDateTime.MinValue.Value)
            {
                return false;
            }
            return true;
        }

        public static bool CheckSqlDateTime(DateTime? dt)
        {
            if (dt == null)
            {
                return false;
            }
            return CheckSqlDateTime(dt.Value);
        }

        public static DateTime GetNextYearLastDay(DateTime now)
        {
            return DateTime.Parse(now.Year + "/1/1").AddYears(2).AddDays(-1).SetTime(23, 59, 59);
        }

        /// <summary>
        /// 取得傳入時間的月初與月底
        /// item1=月初
        /// item2=月底
        /// </summary>
        /// <param name="now"></param>
        /// <returns>傳入時間的月初(item1)與月底(item2)</returns>
        public static Tuple<DateTime, DateTime> GetFirstAndLastDayOfMonth(DateTime now)
        {
            //1號
            DateTime firstDay = new DateTime(now.Year, now.Month, 1);
            //月底，取得那月有幾天然後變時間的日期
            DateTime lastDay = new DateTime(now.Year, now.Month, DateTime.DaysInMonth(now.Year, now.Month));

            return Tuple.Create(firstDay, lastDay);
        }

        public static T ParseToMyType<T>(object value)
        {
            try { return (T)System.ComponentModel.TypeDescriptor.GetConverter(typeof(T)).ConvertFrom(value.ToString()); }
            catch { return default(T); }
        }

        public static void GetQuarterDateRange(int year, Quarter q, out DateTime start, out DateTime end)
        {
            switch (q)
            {
                case Quarter.Quarter1:
                    start = DateTime.Parse(year + "/1/1 00:00:00");
                    end = DateTime.Parse(year + "/3/31 23:59:59");
                    break;
                case Quarter.Quarter2:
                    start = DateTime.Parse(year + "/4/1 00:00:00");
                    end = DateTime.Parse(year + "/6/30 23:59:59");
                    break;
                case Quarter.Quarter3:
                    start = DateTime.Parse(year + "/7/1 00:00:00");
                    end = DateTime.Parse(year + "/9/30 23:59:59");
                    break;
                case Quarter.Quarter4:
                    start = DateTime.Parse(year + "/10/1 00:00:00");
                    end = DateTime.Parse(year + "/12/31 23:59:59");
                    break;
                default:
                    start = DateTime.Now;
                    end = DateTime.Now;
                    break;
            }
        }

        public static string Encrypt(string data)
        {
            var sec = new Security(SymmetricCryptoServiceProvider.AES);
            return sec.Encrypt(data);
        }

        public static string Decrypt(string data)
        {
            var sec = new Security(SymmetricCryptoServiceProvider.AES);
            return sec.Decrypt(data);
        }

        static Security encrypeSecurity = new Security(SymmetricCryptoServiceProvider.AES, Encoding.UTF8.GetBytes("Google+Warnning-"));
        public static string EncryptEmail(string data)
        {
            return WebUtility.UrlEncode(Convert.ToBase64String(
                Encoding.UTF8.GetBytes(encrypeSecurity.Encrypt(data))));
        }

        public static string DecryptEmail(string data)
        {
            try
            {
                data = Encoding.UTF8.GetString(Convert.FromBase64String(WebUtility.UrlDecode(data)));
                var sec = new Security(SymmetricCryptoServiceProvider.AES, Encoding.UTF8.GetBytes("Google+Warnning-"));
                return sec.Decrypt(data);
            }
            catch
            {
                return null;
            }
        }

        ///<summary>
        ///Dictionary<key:file_name,value:filestream>
        ///</summary>
        ///<param name="SourceFiles"></param>
        ///<param name="type"></param>
        ///<returns></returns>
        public static byte[] ZipFile(Dictionary<string, byte[]> SourceFiles)
        {
            try
            {
                using (Stream memOutput = new MemoryStream())
                using (ZipOutputStream zipOutput = new ZipOutputStream(memOutput))
                {
                    zipOutput.SetLevel(3); //zip level
                    byte[] buffer = null;
                    foreach (KeyValuePair<string, byte[]> source in SourceFiles)
                    {

                        buffer = source.Value;
                        ZipEntry entry = new ZipEntry(source.Key); //set file name
                        entry.IsUnicodeText = true;
                        entry.DateTime = DateTime.Now;
                        zipOutput.PutNextEntry(entry);

                        zipOutput.Write(buffer, 0, buffer.Length);
                    }
                    zipOutput.Finish();
                    byte[] newBytes = new byte[memOutput.Length];
                    memOutput.Seek(0, SeekOrigin.Begin);
                    memOutput.Read(newBytes, 0, newBytes.Length);
                    zipOutput.Close();
                    return newBytes;
                }
            }
            catch
            {
                return null;
            }
        }

        public static string RemoveNewLineChar(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return string.Empty;
            }
            return value.Replace("\r", "").Replace("\n", "");
        }

        /// <summary>
        /// 字串中含有規則內的符號則轉為全形(包含 _ , ( ) ")
        /// </summary>
        /// <param name="txt"></param>
        /// <returns></returns>
        public static string CapText(string txt)
        {
            return txt.Replace("_", "－").Replace("(", "（").Replace(",", "，").Replace("\"", "＂").Replace(")", "）");
        }

        /// <summary>
        /// 用反射的方式，將src的值複製到dest
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="src"></param>
        /// <param name="dest"></param>
        public static List<ConfigChangedInfo> CopyObjectProperties<T>(T src, T dest)
        {
            PropertyInfo[] myObjectProps = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            var confChangedList = new List<ConfigChangedInfo>();

            foreach (PropertyInfo pi in myObjectProps)
            {
                #region Compare

                string fromValue;
                string toValue;

                Type pType = pi.PropertyType;

                if (pType == typeof(string[]))
                {
                    string[] pFrom = (string[])pi.GetValue(dest);
                    string[] pTo = (string[])pi.GetValue(src);
                    fromValue = string.Join(Environment.NewLine, pFrom);
                    toValue = string.Join(Environment.NewLine, pTo);
                }
                else if (pType == typeof(int[]))
                {
                    int[] pFrom = (int[])pi.GetValue(dest);
                    int[] pTo = (int[])pi.GetValue(src);
                    fromValue = string.Join(Environment.NewLine, pFrom);
                    toValue = string.Join(Environment.NewLine, pTo);
                }
                else
                {
                    fromValue = pi.GetValue(dest).ToString();
                    toValue = pi.GetValue(src).ToString();
                }

                if (!fromValue.Equals(toValue))
                {
                    var changedConf = new ConfigChangedInfo();
                    changedConf.Key = pi.Name;
                    changedConf.FromValue = fromValue;
                    changedConf.ToValue = toValue;
                    confChangedList.Add(changedConf);
                }

                #endregion

                pi.SetValue(dest, pi.GetValue(src));
            }
            return confChangedList;
        }

        /// <summary>
        /// 移除Description的html tag
        /// </summary>
        /// <param name="html"></param>
        /// <returns></returns>
        public static string RemoveHtmlTagButImg(string html)
        {
            if (!string.IsNullOrEmpty(html))
            {
                html = Regex.Replace(html, @"<(?!(img))[^>]*?>", string.Empty);
                html = Regex.Replace(html, @"\r\n", string.Empty);
                html = Regex.Replace(html, @"\t", string.Empty);
                html = Regex.Replace(html, @"\\r\\n", string.Empty);
                html = Regex.Replace(html, @"\\t", string.Empty);
                html = Regex.Replace(html, @"\\", string.Empty);
                html = Regex.Replace(html, @"&nbsp;", string.Empty);
                html = Regex.Replace(html, "style[^>]+\"", string.Empty);
            }
            return html;
        }

        #region 字串壓縮

        /// <summary>
        /// 壓縮字串，以Base64輸出結果
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string CompressString(string str)
        {
            byte[] compressBeforeByte = Encoding.GetEncoding("UTF-8").GetBytes(str);
            byte[] compressAfterByte = GzipCompress(compressBeforeByte);
            return Convert.ToBase64String(compressAfterByte);
        }

        /// <summary>
        /// 解壓縮Gzip的Base64文字
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string DecompressString(string str)
        {
            byte[] compressBeforeByte = Convert.FromBase64String(str);
            byte[] compressAfterByte = GzipDecompress(compressBeforeByte);
            return Encoding.GetEncoding("UTF-8").GetString(compressAfterByte);
        }


        #endregion

        #region Gzip

        /// <summary>
        /// GZip 壓縮
        /// </summary>
        /// <param name="data">欲壓縮檔</param>
        /// <returns>壓縮檔</returns>
        public static byte[] GzipCompress(byte[] data)
        {
            try
            {
                MemoryStream ms = new MemoryStream();
                using (GZipStream zip = new GZipStream(ms, CompressionMode.Compress, true))
                {
                    zip.Write(data, 0, data.Length);
                    zip.Close();
                }
                byte[] buffer = new byte[ms.Length];
                ms.Position = 0;
                ms.Read(buffer, 0, buffer.Length);
                ms.Close();
                ms.Dispose();
                return buffer;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        /// <summary>
        /// Gzip解壓縮
        /// </summary>
        /// <param name="data">zip檔</param>
        /// <returns>解壓檔</returns>
        public static byte[] GzipDecompress(byte[] data)
        {
            try
            {
                MemoryStream ms = new MemoryStream(data);
                GZipStream zip = new GZipStream(ms, CompressionMode.Decompress, true);
                MemoryStream msreader = new MemoryStream();
                byte[] buffer = new byte[0x1000];
                while (true)
                {
                    int reader = zip.Read(buffer, 0, buffer.Length);
                    if (reader <= 0)
                    {
                        break;
                    }
                    msreader.Write(buffer, 0, reader);
                }
                zip.Close();
                zip.Dispose();
                ms.Close();
                ms.Dispose();
                msreader.Position = 0;
                buffer = msreader.ToArray();
                msreader.Close();
                msreader.Dispose();
                return buffer;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        #endregion

        #region 字串加密

        /// <summary>
        /// 字串SHA256加密，輸出64個字元
        /// </summary>
        /// <param name="content"></param>
        /// <returns></returns>
        public static string ConvertSHA256EncodeTo64Char(string content)
        {
            ISysConfProvider config = ProviderFactory.Instance().GetConfig();

            SHA256 sha256 = new SHA256CryptoServiceProvider(); //建立一個SHA256
            byte[] source = Encoding.Default.GetBytes(content); //將字串轉為Byte[]
            byte[] crypto = sha256.ComputeHash(source); //進行SHA256加密
            string result = string.Empty;

            if (config.EnableSHA256Encode64To64Char)
            {
                result = BitConverter.ToString(crypto).Replace("-", string.Empty); //hex
            }
            else
            {
                for (int i = 0; i < crypto.Length; i++)
                {
                    result += crypto[i].ToString("X").ToLower();
                }
            }
            return result; //輸出結果

        }

        #endregion 字串加密

        /// <summary>
        /// /image/abcd/123.jpg  == > /image/abcd
        /// </summary>
        /// <param name="cdnUrl"></param>
        /// <returns></returns>
        public static string GetUriRequestFolder(Uri cdnUrl)
        {
            int pos = cdnUrl.LocalPath.LastIndexOf('/');
            if (pos > -1)
            {
                return cdnUrl.LocalPath.Substring(1, pos - 1);
            }
            return string.Empty;
        }

        /// <summary>
        /// /image/abcd/123.jpg  == > 123.jpg
        /// </summary>
        /// <param name="cdnUrl"></param>
        /// <returns></returns>
        public static string GetUriRequestFile(Uri cdnUrl)
        {
            int pos = cdnUrl.LocalPath.LastIndexOf('/');
            if (pos > -1)
            {
                return cdnUrl.LocalPath.Substring(pos + 1);
            }
            return string.Empty;
        }

        public static string MapPath(string virtualPath, bool allowPhysicalPath = false)
        {
            if (allowPhysicalPath && string.IsNullOrEmpty(virtualPath) == false
                && virtualPath[0] != '~' && virtualPath[0] != '/' && virtualPath[0] != '\\')
            {
                return virtualPath;
            }
            return HostingEnvironment.MapPath(virtualPath);
        }

        public static string GetFacebookAppId()
        {
            return ProviderFactory.Instance().GetConfig().FacebookApplicationId;
        }

        public static string GetWebUrl()
        {
            if (HttpContext.Current.Request.ApplicationPath == "/")
            {
                return HttpContext.Current.Request.ApplicationPath;
            }
            return HttpContext.Current.Request.ApplicationPath + "/";
        }

        public static string FixCookieVersionIssueForAndroid(string txt)
        {
            if (string.IsNullOrEmpty(txt))
            {
                return txt;
            }
            return txt.Replace(", $Version=0", "");
        }

        public static string NewLineToBr(string text)
        {
            if (!string.IsNullOrEmpty(text))
            {
                text = text.Replace("\r\n", "<br />");
            }
            return text;
        }
        /// <summary>
        /// routeDataValues 轉換成一般的 QueryString 即 bid=...&amp;cid=...
        /// </summary>
        /// <param name="routeDataValues"></param>
        /// <returns></returns>
        public static string ConvertDictionaryToQueryString(IDictionary<string, object> routeDataValues)
        {
            if (routeDataValues == null || routeDataValues.Count == 0)
            {
                return string.Empty;
            }
            StringBuilder sb = new StringBuilder();
            foreach (var pair in routeDataValues)
            {
                if (sb.Length > 0)
                {
                    sb.Append("&");
                }
                sb.Append(pair.Key);
                sb.Append('=');
                sb.Append(HttpUtility.UrlEncode(pair.Value.ToString()));
            }
            return sb.ToString();
        }
        /// <summary>
        /// 將多個整數以逗號分隔的字串，轉換成整數集合，譬如 "3,5,7"  =>  new List(3,5,6)
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static List<int> SplitToIntList(string str)
        {
            if (string.IsNullOrEmpty(str))
            {
                return new List<int>();
            }
            string[] tempStrArray = str.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            List<int> result = new List<int>();
            foreach (string tempStr in tempStrArray)
            {
                int tmp;
                if (int.TryParse(tempStr.Trim(), out tmp))
                {
                    result.Add(tmp);
                }
            }
            return result;
        }

        public static string RemoveQueryStringByKey(string url, string key)
        {
            var uri = new Uri(url);

            // this gets all the query string key value pairs as a collection
            var newQueryString = HttpUtility.ParseQueryString(uri.Query);

            // this removes the key if exists
            newQueryString.Remove(key);

            // this gets the page path from root without QueryString
            string pagePathWithoutQueryString = uri.GetLeftPart(UriPartial.Path);

            return newQueryString.Count > 0
                ? String.Format("{0}?{1}", pagePathWithoutQueryString, newQueryString)
                : pagePathWithoutQueryString;
        }

        public static bool IsInSiteUrl(Uri url)
        {
            if (url == null)
            {
                return false;
            }

            ISysConfProvider config = ProviderFactory.Instance().GetConfig();

            var checkHost = url.Host.ToLower();
            var sslSiteUrl = new Uri(config.SSLSiteUrl);
            if (checkHost.IndexOf(sslSiteUrl.Host.ToLower(), StringComparison.Ordinal) > -1) return true;

            var siteUrl = new Uri(config.SiteUrl);
            return checkHost.IndexOf(siteUrl.Host.ToLower(), StringComparison.Ordinal) > -1;
        }

        //public static string SmartGetPrefix(string[] lines, out string[] subLines)
        //{
        //    List<string> subLineList = new List<string>();
        //    subLines = null;
        //    if (lines.Length == 0)
        //    {
        //        return null;
        //    }
        //    string firstLine = lines[0];
        //    StringBuilder sb = new StringBuilder();

        //    bool finish = false;

        //    for (int i = 0; i < firstLine.Length; i++)
        //    {
        //        char ch = firstLine[i];
        //        for (int j = 1; j < lines.Length; j++)
        //        {
        //            if (lines[j].Length < j || ch != lines[j][i])
        //            {
        //                finish = true;
        //                break;
        //            }
        //        }
        //        if (finish)
        //        {
        //            break;
        //        }
        //        else
        //        {
        //            sb.Append(ch);
        //        }
        //    }
        //    string prefix = sb.ToString();
        //    if (prefix == string.Empty)
        //    {
        //        subLines = new string[lines.Length];
        //        //完全沒有共同點
        //        lines.CopyTo(subLines, 0);
        //        return prefix;
        //    }
        //    else
        //    {
        //        char lastChar = prefix[prefix.Length - 1];
        //        bool removeLastChar = false;
        //        if (lastChar == '(' || lastChar == '（')
        //        {                    
        //            prefix = prefix.Substring(0, prefix.Length - 1);
        //        }
        //        for (int j = 0; j < lines.Length; j++)
        //        {
        //            subLineList.Add(lines[j].Substring(prefix.Length));
        //        }
        //        subLines = subLineList.ToArray();
        //        return prefix;
        //    }
        //}
        ///// <summary>
        ///// 之後移到PponHelper
        ///// </summary>
        ///// <param name="subPart"></param>
        ///// <returns></returns>
        //public static string GetUnitTitle(string subPart)
        //{
        //    if (string.IsNullOrEmpty(subPart))
        //    {
        //        return string.Empty;
        //    }
        //    char lastChar = subPart.Last();
        //    if (lastChar == '盒') return "盒";
        //    if (lastChar == '包') return "包";
        //    if (lastChar == '箱') return "箱";
        //    if (lastChar == '份') return "份";

        //    if (subPart.Contains("盒")) return "盒";
        //    if (subPart.Contains("包")) return "包";
        //    if (subPart.Contains("箱")) return "箱";
        //    return "份";
        //}
        ///// <summary>
        ///// 之後移到PponHelper
        ///// </summary>
        ///// <param name="subPart"></param>
        ///// <param name="number"></param>
        ///// <returns></returns>
        //public static bool GetUnitNumber(string subPart, out int number)
        //{
        //    List<int> result = new List<int>();

        //    subPart = subPart.Trim();
        //    string part = null;
        //    for (int i = 0; i < subPart.Length; i++)
        //    {
        //        char ch = subPart[i];
        //        if (ch >= '0' && ch <= '9')
        //        {
        //            part += ch;
        //            continue;
        //        }
        //        if (string.IsNullOrEmpty(part) == false)
        //        {
        //            result.Add(int.Parse(part));
        //            part = null;
        //        }                
        //    }
        //    if (result.Count != 1)
        //    {
        //        number = 0;
        //        return false;
        //    }
        //    number = result[0];
        //    return true;
        //}
        /// <summary>
        /// 之後移到PponHelper
        /// </summary>
        /// <param name="prefix"></param>
        /// <returns></returns>
        //public static string StrongDashTitle(string prefix)
        //{
        //    StringBuilder sb = new StringBuilder();
        //    string[] parts = prefix.Split('-');
        //    if (parts.Length >= 2)
        //    {
        //        sb.Append('【');
        //        sb.Append(parts[0]);
        //        sb.Append('】');
        //        sb.Append(parts[1]);
        //        for (int i = 2; i < parts.Length; i++)
        //        {
        //            sb.Append('-');
        //            sb.Append(parts[i]);
        //        }
        //        return sb.ToString();
        //    }
        //    return prefix;
        //}
        public static string GetDealDeepLink(Guid dealId, string rsrc = null)
        {
            ISysConfProvider config = ProviderFactory.Instance().GetConfig();
            if (string.IsNullOrEmpty(rsrc))
            {
                return string.Format("{0}/deal/{1}", config.SiteUrl, dealId);
            }
            else
            {
                return string.Format("{0}/deal/{1}?rsrc={2}", config.SiteUrl, dealId, rsrc);
            }
        }

        public static string ReplaceToHttps(string str)
        {
            if (string.IsNullOrEmpty(str))
            {
                return string.Empty;
            }
            return str.Replace("http://", "https://");
        }

        /// <summary>
        /// 以正規表示式取出html屬性
        /// </summary>
        /// <param name="htmlText"></param>
        /// <param name="attribute"></param>
        /// <returns></returns>
        public static string RegularToGetHtmlAttribute(string htmlText, string attribute)
        {
            if (string.IsNullOrEmpty(htmlText)) return string.Empty;

            htmlText = htmlText.Replace("\t", " ").Replace(" ", "");
            htmlText = HttpUtility.HtmlDecode(htmlText);

            string pattern;
            switch (attribute)
            {
                case "src":
                    pattern = "<img.+?src=[\"'](.+?)[\"'].*?>";
                    break;
                case "href":
                    pattern = "<a [^>]*href=(?:'(?<href>.*?)')|(?:\"(?<href>.*?)\")";
                    break;
                default:
                    return string.Empty;
            }

            return Regex.Match(htmlText, pattern, RegexOptions.IgnoreCase).Groups[1].Value;
        }

        /// <summary>
        /// 地址正規化(段為國字其他為數字)
        /// </summary>
        /// <param name="address"></param>
        /// <returns></returns>
        public static string AddrConvertRegular(string address)
        {
            try
            {
                //修改請確認以下地址是否正確
                //103台北市大同區國強1街188號10樓之三
                //103台北市大同區華夏巷西4弄22號
                //103台北市大同區國強一街188號10樓之三
                //103台北市大同區華夏巷西四弄22號
                //103台北市大同區長安西路1段一百五十八巷三十弄五七之三號二樓之3
                //103台北市大同區長安西路一百號
                //103台北市大同區長安西路一百二十五號
                //103台北市大同區長安西路一百二十號
                //103台北市大同區長安西路二十號
                //103台北市大同區長安西路二十五號
                //103台北市大同區長安西路十號
                //103台北市大同區長安西路十五號
                //103台北市大同區長安西路三百號
                //103台北市大同區長安西路三百五十八號
                //103台北市大同區長安西路三百五十號
                //103台北市大同區長安西路五十號
                //103台北市大同區長安西路五十八號

                var pattern = @"(?<zipcode>(^\d{5}|^\d{3})?)(?<city>\D+[縣市])(?<district>\D+?(市區|鎮區|鎮市|[鄉鎮市區]))(?<village>\D+?[村里])?(?<neighbor>\S+[鄰])?(?<road>\S+?(村路|[路街道段]))?(?<section>\S+段)?(?<lane>\S+巷)?(?<alley>\S+弄)?(?<no>\S+號)?(?<floor>\S+樓)?(?<others>.*)";

                Match match = Regex.Match(address, pattern);
                if (match.Success)
                {
                    string ZipCode = match.Groups["zipcode"].ToString().Trim();
                    string City = match.Groups["city"].ToString().Trim();
                    string District = match.Groups["district"].ToString().Trim();
                    string Village = match.Groups["village"].ToString().Trim();
                    string Neighbor = match.Groups["neighbor"].ToString().Trim();
                    string Road = match.Groups["road"].ToString().Trim();
                    string Section = match.Groups["section"].ToString().Trim();
                    string Lane = match.Groups["lane"].ToString().Trim();
                    string Alley = match.Groups["alley"].ToString().Trim();
                    string No = match.Groups["no"].ToString().Trim();
                    string Floor = match.Groups["floor"].ToString().Trim();
                    string Others = match.Groups["others"].ToString().Trim();

                    Road = Road.Replace("1", "一").Replace("2", "二").Replace("3", "三").Replace("4", "四").Replace("5", "五").Replace("6", "六").Replace("7", "七").Replace("8", "八").Replace("9", "九");
                    Section = Section.Replace("1", "一").Replace("2", "二").Replace("3", "三").Replace("4", "四").Replace("5", "五").Replace("6", "六").Replace("7", "七").Replace("8", "八").Replace("9", "九");
                    Lane = AddrChineseToInt(Lane);
                    Alley = AddrChineseToInt(Alley);
                    No = AddrChineseToInt(No).Replace("之", "-");
                    Floor = AddrChineseToInt(Floor);
                    Others = Others.Replace("1", "一").Replace("2", "二").Replace("3", "三").Replace("4", "四").Replace("5", "五").Replace("6", "六").Replace("7", "七").Replace("8", "八").Replace("9", "九").Replace("-", "之");

                    address = (!string.IsNullOrEmpty(ZipCode) ? (ZipCode + " ") : string.Empty) + City + District + Village + Neighbor + Road + Section + Lane + Alley + No + Floor + Others;

                }
                return address;
            }
            catch (Exception ex)
            {
                logger.Error("address:" + address, ex);
                return address;
            }
        }

        /// <summary>
        /// 取得ZipCode
        /// </summary>
        /// <param name="prefixZipCodeOfAddress"></param>
        /// <returns></returns>
        public static string GetZipCode(string prefixZipCodeOfAddress)
        {
            string ZipCode = string.Empty;
            var pattern = @"(?<zipcode>(^\d{5}|^\d{3})?)(?<city>\D+[縣市])(?<district>\D+?(市區|鎮區|鎮市|[鄉鎮市區]))(?<village>\D+?[村里])?(?<neighbor>\S+[鄰])?(?<road>\S+?(村路|[路街道段]))?(?<section>\S+段)?(?<lane>\S+巷)?(?<alley>\S+弄)?(?<no>\S+號)?(?<floor>\S+樓)?(?<others>.*)";

            Match match = Regex.Match(prefixZipCodeOfAddress, pattern);
            if (match.Success)
            {
                ZipCode = match.Groups["zipcode"].ToString().Trim();
            }
            return ZipCode;
        }

        public static string AddrChineseToInt(string addr)
        {
            string newAddr = addr;

            #region 百位
            if (newAddr.IndexOf("一百") > -1)
            {
                if (newAddr.Length == 3)
                {
                    newAddr = newAddr.Replace("一百", "100");
                }
                else
                {
                    newAddr = newAddr.Replace("一百", "1");
                }
            }
            else if (newAddr.IndexOf("二百") > -1)
            {
                if (newAddr.Length == 3)
                {
                    newAddr = newAddr.Replace("二百", "200");
                }
                else
                {
                    newAddr = newAddr.Replace("二百", "2");
                }
            }
            else if (newAddr.IndexOf("三百") > -1)
            {
                if (newAddr.Length == 3)
                {
                    newAddr = newAddr.Replace("三百", "300");
                }
                else
                {
                    newAddr = newAddr.Replace("三百", "3");
                }
            }
            else if (newAddr.IndexOf("四百") > -1)
            {
                if (newAddr.Length == 3)
                {
                    newAddr = newAddr.Replace("四百", "400");
                }
                else
                {
                    newAddr = newAddr.Replace("四百", "4");
                }
            }
            else if (newAddr.IndexOf("五百") > -1)
            {
                if (newAddr.Length == 3)
                {
                    newAddr = newAddr.Replace("五百", "500");
                }
                else
                {
                    newAddr = newAddr.Replace("五百", "5");
                }
            }
            else if (newAddr.IndexOf("六百") > -1)
            {
                if (newAddr.Length == 3)
                {
                    newAddr = newAddr.Replace("六百", "600");
                }
                else
                {
                    newAddr = newAddr.Replace("六百", "6");
                }
            }
            else if (newAddr.IndexOf("七百") > -1)
            {
                if (newAddr.Length == 3)
                {
                    newAddr = newAddr.Replace("七百", "700");
                }
                else
                {
                    newAddr = newAddr.Replace("七百", "7");
                }
            }
            else if (newAddr.IndexOf("八百") > -1)
            {
                if (newAddr.Length == 3)
                {
                    newAddr = newAddr.Replace("八百", "800");
                }
                else
                {
                    newAddr = newAddr.Replace("八百", "8");
                }
            }
            else if (newAddr.IndexOf("九百") > -1)
            {
                if (newAddr.Length == 3)
                {
                    newAddr = newAddr.Replace("九百", "900");
                }
                else
                {
                    newAddr = newAddr.Replace("九百", "9");
                }
            }
            #endregion

            #region 十位
            if (newAddr.IndexOf("二十") > -1)
            {
                if (newAddr.Substring(newAddr.IndexOf("二十") + 2, 1) == "巷" || newAddr.Substring(newAddr.IndexOf("二十") + 2, 1) == "弄" || newAddr.Substring(newAddr.IndexOf("二十") + 2, 1) == "號" || newAddr.Substring(newAddr.IndexOf("二十") + 2, 1) == "樓")
                {
                    newAddr = newAddr.Replace("二十", "20");
                }
                else
                {
                    newAddr = newAddr.Replace("二十", "2");
                }

            }
            else if (newAddr.IndexOf("三十") > -1)
            {
                if (newAddr.Substring(newAddr.IndexOf("三十") + 2, 1) == "巷" || newAddr.Substring(newAddr.IndexOf("三十") + 2, 1) == "弄" || newAddr.Substring(newAddr.IndexOf("三十") + 2, 1) == "號" || newAddr.Substring(newAddr.IndexOf("三十") + 2, 1) == "樓")
                {
                    newAddr = newAddr.Replace("三十", "30");
                }
                else
                {
                    newAddr = newAddr.Replace("三十", "3");
                }
            }
            else if (newAddr.IndexOf("四十") > -1)
            {
                if (newAddr.Substring(newAddr.IndexOf("四十") + 2, 1) == "巷" || newAddr.Substring(newAddr.IndexOf("四十") + 2, 1) == "弄" || newAddr.Substring(newAddr.IndexOf("四十") + 2, 1) == "號" || newAddr.Substring(newAddr.IndexOf("四十") + 2, 1) == "樓")
                {
                    newAddr = newAddr.Replace("四十", "40");
                }
                else
                {
                    newAddr = newAddr.Replace("四十", "4");
                }
            }
            else if (newAddr.IndexOf("五十") > -1)
            {
                if (newAddr.Substring(newAddr.IndexOf("五十") + 2, 1) == "巷" || newAddr.Substring(newAddr.IndexOf("五十") + 2, 1) == "弄" || newAddr.Substring(newAddr.IndexOf("五十") + 2, 1) == "號" || newAddr.Substring(newAddr.IndexOf("五十") + 2, 1) == "樓")
                {
                    newAddr = newAddr.Replace("五十", "50");
                }
                else
                {
                    newAddr = newAddr.Replace("五十", "5");
                }
            }
            else if (newAddr.IndexOf("六十") > -1)
            {
                if (newAddr.Substring(newAddr.IndexOf("六十") + 2, 1) == "巷" || newAddr.Substring(newAddr.IndexOf("六十") + 2, 1) == "弄" || newAddr.Substring(newAddr.IndexOf("六十") + 2, 1) == "號" || newAddr.Substring(newAddr.IndexOf("六十") + 2, 1) == "樓")
                {
                    newAddr = newAddr.Replace("六十", "60");
                }
                else
                {
                    newAddr = newAddr.Replace("六十", "6");
                }
            }
            else if (newAddr.IndexOf("七十") > -1)
            {
                if (newAddr.Substring(newAddr.IndexOf("七十") + 2, 1) == "巷" || newAddr.Substring(newAddr.IndexOf("七十") + 2, 1) == "弄" || newAddr.Substring(newAddr.IndexOf("七十") + 2, 1) == "號" || newAddr.Substring(newAddr.IndexOf("七十") + 2, 1) == "樓")
                {
                    newAddr = newAddr.Replace("七十", "70");
                }
                else
                {
                    newAddr = newAddr.Replace("七十", "7");
                }
            }
            else if (newAddr.IndexOf("八十") > -1)
            {
                if (newAddr.Substring(newAddr.IndexOf("八十") + 2, 1) == "巷" || newAddr.Substring(newAddr.IndexOf("八十") + 2, 1) == "弄" || newAddr.Substring(newAddr.IndexOf("八十") + 2, 1) == "號" || newAddr.Substring(newAddr.IndexOf("八十") + 2, 1) == "樓")
                {
                    newAddr = newAddr.Replace("八十", "80");
                }
                else
                {
                    newAddr = newAddr.Replace("八十", "8");
                }
            }
            else if (newAddr.IndexOf("九十") > -1)
            {
                if (newAddr.Substring(newAddr.IndexOf("九十") + 2, 1) == "巷" || newAddr.Substring(newAddr.IndexOf("九十") + 2, 1) == "弄" || newAddr.Substring(newAddr.IndexOf("九十") + 2, 1) == "號" || newAddr.Substring(newAddr.IndexOf("九十") + 2, 1) == "樓")
                {
                    newAddr = newAddr.Replace("九十", "90");
                }
                else
                {
                    newAddr = newAddr.Replace("九十", "9");
                }
            }
            else if (newAddr.IndexOf("十") > -1)
            {
                if (newAddr.Substring(newAddr.IndexOf("十") + 1, 1) == "巷" || newAddr.Substring(newAddr.IndexOf("十") + 1, 1) == "弄" || newAddr.Substring(newAddr.IndexOf("十") + 1, 1) == "號" || newAddr.Substring(newAddr.IndexOf("十") + 1, 1) == "樓")
                {
                    newAddr = newAddr.Replace("十", "10");
                }
                else
                {
                    newAddr = newAddr.Replace("十", "1");
                }
            }
            #endregion

            //個位
            newAddr = newAddr.Replace("一", "1").Replace("二", "2").Replace("三", "3").Replace("四", "4").Replace("五", "5").Replace("六", "6").Replace("七", "7").Replace("八", "8").Replace("九", "9");

            int newInt = 0;
            if (int.TryParse(newAddr.Replace("巷", "").Replace("弄", "").Replace("號", "").Replace("樓", "").Replace("-", "").Replace("之", ""), out newInt))
            {
                //表示巷弄號樓都為數字
                return newAddr;
            }
            else
            {
                //表示巷弄號樓為非數字地址
                //將有數字的改成中文
                return addr.Replace("1", "一").Replace("2", "二").Replace("3", "三").Replace("4", "四").Replace("5", "五").Replace("6", "六").Replace("7", "七").Replace("8", "八").Replace("9", "九"); ;
            }
        }
        /// <summary>
        /// 依 property 的類型，將 "False" 轉成 (object)false
        /// </summary>
        /// <param name="pi"></param>
        /// <param name="src"></param>
        /// <param name="dest"></param>
        /// <returns></returns>
        public static bool ConvertStringToObjectValue(PropertyInfo pi, string src, out object dest)
        {
            bool ret = true;
            dest = null;

            if (pi.PropertyType == typeof(Guid))
            {
                dest = default(Guid);
                try
                {
                    dest = new Guid(src);
                }
                catch
                {
                    ret = false;
                }
            }
            else if (pi.PropertyType == typeof(DateTime))
            {
                dest = default(DateTime);
                try
                {
                    dest = DateTime.Parse(src);
                }
                catch
                {
                    ret = false;
                }
            }
            else if (pi.PropertyType == typeof(int))
            {
                int i;
                ret = int.TryParse(src, out i);
                dest = i;
            }
            else if (pi.PropertyType.IsEnum)
            {
                try
                {
                    dest = Enum.Parse(pi.PropertyType, src, true);
                }
                catch
                {
                    ret = false;
                }
            }
            else if (pi.PropertyType == typeof(bool))
            {
                bool x;
                ret = bool.TryParse(src, out x);
                dest = x;
            }
            else if (pi.PropertyType == typeof(decimal))
            {
                decimal x;
                ret = decimal.TryParse(src, out x);
                dest = x;
            }
            else if (pi.PropertyType == typeof(string[]))
            {
                dest = src.Split(new[]
                {
                    ';'
                }, StringSplitOptions.RemoveEmptyEntries);
            }
            else if (pi.PropertyType == typeof(int[]))
            {
                var rtnList = new List<int>();
                var srcArray = src.Split(new[]
                {
                    ';'
                }, StringSplitOptions.RemoveEmptyEntries);
                foreach (var item in srcArray)
                {
                    int rtn;
                    if (int.TryParse(item, out rtn))
                    {
                        rtnList.Add(rtn);
                    }
                }
                dest = rtnList.ToArray();
            }
            else
                dest = src;

            return ret;
        }

        public static bool SetPropertyValue(object theObject, string propertyName, string value)
        {
            var pi = theObject.GetType().GetProperty(propertyName, BindingFlags.GetProperty | BindingFlags.Instance | BindingFlags.Public);
            if (pi == null)
            {
                return false;
            }
            object propValue;
            if (ConvertStringToObjectValue(pi, value, out propValue))
            {
                pi.SetValue(theObject, propValue);
            }
            return false;
        }

        public static List<string> GetLinesFromStream(Stream inputStream)
        {
            return GetLinesFromStream(inputStream, Encoding.UTF8);
        }
        public static List<string> GetLinesFromStream(Stream inputStream, Encoding encoding)
        {
            List<string> result = new List<string>();
            using (var reader = new StreamReader(inputStream, encoding))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    result.Add(line);
                }
            }
            return result;
        }
        /// <summary>
        /// 從 zipped stream 解開檔案，將收集每一行文字回傳
        /// 預設編碼 utf-8
        /// </summary>
        /// <param name="inputStream"></param>
        /// <returns></returns>
        public static List<string> GetLinesFromZippedStream(Stream inputStream)
        {
            return GetLinesFromZippedStream(inputStream, Encoding.UTF8);
        }
        /// <summary>
        /// 從 zipped stream 解開檔案，將收集每一行文字回傳
        /// </summary>
        /// <param name="inputStream"></param>
        /// <param name="encoding">編碼方式</param>
        public static List<string> GetLinesFromZippedStream(Stream inputStream, Encoding encoding)
        {
            List<string> result = new List<string>();
            try
            {
                ZipEntry entry;
                using (ZipInputStream zis = new ZipInputStream(inputStream))
                {
                    do
                    {
                        entry = zis.GetNextEntry();
                        if (entry != null && entry.IsFile)
                        {

                            MemoryStream memStream = new MemoryStream();
                            int bufferSize = 2048;     //指定的緩衝區大小
                            int readCount = 0;           //回傳已讀取的位元組數目
                            byte[] buffer = new byte[bufferSize];

                            readCount = zis.Read(buffer, 0, bufferSize);
                            while (readCount > 0)
                            {
                                memStream.Write(buffer, 0, readCount);
                                readCount = zis.Read(buffer, 0, bufferSize);
                            }

                            //需指定預設的編碼，否則讀進資料可能會亂碼    
                            StreamReader reader = new StreamReader(memStream, encoding);

                            memStream.Seek(0, SeekOrigin.Begin);   // reset file pointer

                            string str;
                            while ((str = reader.ReadLine()) != null)
                            {
                                if (string.IsNullOrWhiteSpace(str) == false)
                                {
                                    result.Add(str);
                                }
                            }
                            reader.Close();
                            reader.Dispose();
                            memStream.Close();
                            memStream.Dispose();
                        }
                    } while (entry != null);
                }
                return result;
            }
            catch (Exception ex)
            {
                logger.Info(ex);
                return result;
            }
        }

        public static bool CheckUrlValid(string input)
        {
            Regex regex = new Regex("^http(s)?://([\\w-]+.)+[\\w-]+(/[\\w- ./?%&=])?$");
            return regex.IsMatch(input.ToLower());
        }
        /// <summary>
        /// 目前被管控的清單
        /// * /Themes/PCweb/css/ppon_item.min.css
        /// * /Themes/mobile/css/style.css
        /// </summary>
        /// <param name="cssFile"></param>
        /// <returns></returns>
        public static HtmlString RenderCss(string cssFile)
        {
            var config = ProviderFactory.Instance().GetConfig();
            return new HtmlString(string.Format("<link type=\"text/css\" rel=\"stylesheet\" href=\"{0}?{1}\" />",
                cssFile, config.CssAndJsVersion));
        }

        public static HtmlString RenderJs(string jsFile)
        {
            var config = ProviderFactory.Instance().GetConfig();
            return new HtmlString(string.Format("<script type=\"text/javascript\" src=\"{0}?{1}\"></script>\n",
                jsFile, config.CssAndJsVersion));
        }

        private static string imageVersionBase;
        private static int imageVersion = 1;
        public static void IncrImageVer()
        {
            System.Threading.Interlocked.Increment(ref imageVersion);
        }
        public static string GetImageVer()
        {
            if (imageVersionBase == null)
            {
                imageVersionBase = DateTime.Now.ToString("yyyyMMddHHmm");
            }
            return string.Format("{0}.{1}", imageVersionBase, imageVersion);
        }
        public static string GetFriendlyTimeDesc(DateTime modifyTime)
        {
            const int SECOND = 1;
            const int MINUTE = 60 * SECOND;
            const int HOUR = 60 * MINUTE;
            const int DAY = 24 * HOUR;
            const int MONTH = 30 * DAY;

            var ts = new TimeSpan(DateTime.Now.Ticks - modifyTime.Ticks);
            double delta = Math.Abs(ts.TotalSeconds);

            if (delta < 1 * MINUTE)
            {
                return ts.Seconds + "秒前";
            }
            if (delta < 1 * HOUR)
            {
                return ts.Minutes + "分鍾前";
            }

            if (delta < 24 * HOUR)
            {
                return ts.Hours + "小時前";
            }
            if (delta < 48 * HOUR)
            {
                return "昨天";
            }
            if (delta < 30 * DAY)
            {
                return ts.Days + "天前";
            }

            if (delta < 12 * MONTH)
            {
                int months = Convert.ToInt32(Math.Floor((double)ts.Days / 30));
                return months + "個月前";
            }
            else
            {
                int years = Convert.ToInt32(Math.Floor((double)ts.Days / 365));
                return years + "年前";
            }
        }
    }

    public static class DecimalExtension
    {
        public static string ToStringFractionOrNot(this decimal val)
        {
            // write this directly instead of calling the overload below to increase performance
            return val == decimal.Truncate(val) ? val.ToString("N0") : val.ToString("N1");
        }

        public static string ToStringFractionOrNot(this decimal val, int digitsBelowDecimal)
        {
            return val == decimal.Truncate(val) ? val.ToString("N0") : val.ToString("N" + digitsBelowDecimal);
        }

        public static decimal FractionFloor(this decimal val, int decimals)
        {
            var mthBase = (decimal)Math.Pow(10, decimals);
            if (val == Math.Floor(val * mthBase) / mthBase)
            {
                return val;
            }
            return (Math.Floor((val * mthBase + (decimal)0.5))) / mthBase;
        }
    }

    public static class StringExtension
    {
        public static string ReplaceIgnoreCase(this string source, string oldString, string newString)
        {
            bool match;
            int pos;
            do
            {
                pos = source.IndexOf(oldString, StringComparison.OrdinalIgnoreCase);
                // Did we match the word regardless of case
                match = pos >= 0;
                // perform the replace on the matched word
                if (match)
                {
                    source = source.Remove(pos, oldString.Length);
                    source = source.Insert(pos, newString);
                }
            } while (match);
            return source;
        }
        public static string TrimStart(this string source, string trimPart, StringComparison comparision)
        {
            if (source.StartsWith(trimPart, comparision))
            {
                return source.Substring(trimPart.Length);
            }
            return source;
        }
        public static string TrimEnd(this string source, string trimPart)
        {
            if (source.EndsWith(trimPart))
            {
                return source.Substring(0, source.Length - trimPart.Length);
            }
            return source;
        }
        static char[] allNeedTrimChars = new char[] {
            // SpaceSeparator category
            '\u0008', //bs
            '\u0020', '\u1680', '\u180E', '\u2000', '\u2001', '\u2002', '\u2003',
            '\u2004', '\u2005', '\u2006', '\u2007', '\u2008', '\u2009', '\u200A',
            '\u202F', '\u205F', '\u3000',
            // LineSeparator category
            '\u2028',
            // ParagraphSeparator category
            '\u2029',
            // Latin1 characters
            '\u0009', '\u000A', '\u000B', '\u000C', '\u000D', '\u0085', '\u00A0',
            // ZERO WIDTH SPACE (U+200B) & ZERO WIDTH NO-BREAK SPACE (U+FEFF)
            '\u200B', '\uFEFF'
        };
        public static string TrimStrict(this string source)
        {
            if (source == null)
            {
                return null;
            }
            return source.Trim(allNeedTrimChars);
        }

        public static string ToCamelCase(this string str)
        {
            if (string.IsNullOrEmpty(str))
            {
                return str;
            }
            if (str.Length == 1)
            {
                return char.ToLower(str[0]).ToString();
            }
            StringBuilder sb = new StringBuilder();
            bool firstChar = true;
            foreach (char ch in str)
            {
                if (ch == ' ' || ch == '-' || ch == '_')
                {
                    sb.Append(ch);
                    firstChar = true;
                }
                else if (ch == '/')
                {
                    sb.Append(ch);
                    firstChar = true;
                }
                else
                {
                    if (firstChar)
                    {
                        sb.Append(char.ToLower(ch));
                        firstChar = false;
                    }
                    else
                    {
                        sb.Append(ch);
                    }
                }
            }
            return sb.ToString();
        }
    }

    public static class DateTimeExtension
    {
        private static readonly long DatetimeMinTimeTicks =
            (new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).Ticks;

        public static long ToJavaScriptMilliseconds(this DateTime dt)
        {
            return (dt.ToUniversalTime().Ticks - DatetimeMinTimeTicks) / 10000;
        }

        public static long ToJavaScriptseconds(this DateTime dt)
        {
            return (dt.ToUniversalTime().Ticks - DatetimeMinTimeTicks) / 10000000;
        }
    }

    public static class TypeExtensions
    {
        public static bool HasAttribute(this Type t, Type customAttrType)
        {
            foreach (CustomAttributeData attr in t.CustomAttributes)
            {
                if (attr.AttributeType == customAttrType)
                {
                    return true;
                }
            }
            return false;
        }
    }


    public class ConfigChangedInfo
    {
        public string Key { get; set; }
        public string FromValue { get; set; }
        public string ToValue { get; set; }
    }

    public class ImgPoolExtensions
    {
        /// <summary>
        /// 使用正則式取得html內img
        /// </summary>
        public static List<string> GetImageLink(string strHtml, string strTagName, string strAttributeName)
        {
            if (string.IsNullOrWhiteSpace(strHtml))
            {
                return new List<string>();
            }

            strHtml = HttpUtility.HtmlDecode(strHtml);

            List<string> lstAttribute = new List<string>();
            string[] strTagNameArray = strTagName.Split(',');
            string[] strAttributeNameArray = strAttributeName.Split(',');

            if (strTagNameArray.Count() == strAttributeNameArray.Count())
            {
                for (int i = 0; i < strTagNameArray.Length; i++)
                {
                    string strPattern = string.Format(
                      "<\\s*{0}\\s+.*?(({1}\\s*=\\s*\"(?<attr>[^\"]+)\")|({1}\\s*=\\s*'(?<attr>[^']+)')|({1}\\s*=\\s*(?<attr>[^\\s]+)\\s*))[^>]*>"
                      , strTagNameArray[i]
                      , strAttributeNameArray[i]);
                    MatchCollection matchs = Regex.Matches(strHtml, strPattern, RegexOptions.IgnoreCase);
                    foreach (Match m in matchs)
                    {
                        if (m.Groups["attr"].Value.Contains("www.17life.com/Images/imagesU", StringComparison.OrdinalIgnoreCase))
                        {
                            lstAttribute.Add(m.Groups["attr"].Value.Trim());
                        }
                    }
                }
            }
            return lstAttribute.ToList();
        }
    }

    public static class NullableTypeExtensions
    {
        public static string ToString(this decimal? self, string format)
        {
            if (self == null)
            {
                return string.Empty;
            }
            return self.Value.ToString(format);
        }
    }

    public static class RouteDataExtensions
    {
        public static void MapPageRouteWithName(this RouteCollection routes, string routeName, string routeUrl, string physicalFile, bool checkPhysicalUrlAccess = true,
            RouteValueDictionary defaults = default(RouteValueDictionary), RouteValueDictionary constraints = default(RouteValueDictionary), RouteValueDictionary dataTokens = default(RouteValueDictionary))
        {
            if (dataTokens == null)
                dataTokens = new RouteValueDictionary();

            dataTokens.Add("route-name", routeName);
            routes.MapPageRoute(routeName, routeUrl, physicalFile, checkPhysicalUrlAccess, defaults, constraints, dataTokens);
        }

        public static string GetRouteName(this RouteData routeData)
        {
            if (routeData.DataTokens["route-name"] != null)
                return routeData.DataTokens["route-name"].ToString();
            else return String.Empty;
        }

        /// <summary>
        /// o
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="source"></param>
        /// <param name="orderByProperty"></param>
        /// <param name="desc"></param>
        /// <returns></returns>
        public static IEnumerable<TEntity> OrderBy<TEntity>(this IEnumerable<TEntity> source,
                                                    string orderByProperty, bool desc)
        {
            string command = desc ? "OrderByDescending" : "OrderBy";
            var type = typeof(TEntity);
            var property = type.GetProperty(orderByProperty);
            var parameter = Expression.Parameter(type, "p");
            var propertyAccess = Expression.MakeMemberAccess(parameter, property);
            var orderByExpression = Expression.Lambda(propertyAccess, parameter);
            var resultExpression = Expression.Call(typeof(Queryable), command,
                                                   new[] { type, property.PropertyType },
                                                   source.AsQueryable().Expression,
                                                   Expression.Quote(orderByExpression));
            return source.AsQueryable().Provider.CreateQuery<TEntity>(resultExpression);
        }

    }
}

﻿using log4net;
using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace LunchKingSite.Core.Component
{
    public class Security
    {
        #region keys

        /// <summary>
        /// 以下內容皆由 CryptoServiceProvider 產生，請勿更動任何內容，以免破壞資料的一致性
        /// </summary>

        private readonly byte[] defaultDesKey = new byte[] { 39, 71, 223, 29, 44, 171, 99, 63 };
        private readonly byte[] defaultDesIV = new byte[] { 148, 247, 61, 244, 76, 80, 16, 229 };
        private readonly byte[] defaultTripleDesKey = new byte[] { 65, 232, 99, 218, 201, 71, 51, 225, 115, 139, 176, 251, 218, 81, 120, 27, 61, 8, 6, 90, 47, 182, 138, 206 };
        private readonly byte[] defaultTripleDesIV = new byte[] { 140, 149, 206, 2, 146, 182, 89, 90 };
        private readonly byte[] defaultAesKey = new byte[] { 28, 23, 35, 7, 62, 166, 131, 195, 59, 145, 33, 2, 180, 209, 239, 149, 240, 229, 89, 31, 112, 8, 195, 182, 91, 101, 114, 88, 104, 228, 230, 38 };
        private readonly byte[] defaultAesIV = new byte[] { 154, 152, 185, 97, 251, 84, 189, 88, 67, 236, 212, 70, 174, 21, 196, 122 };
        private const string defaultMD5Prefix = "xYR!L'D(es$6+#:R;(%c}]S!|@$*!tj:";
        private const string defaultMD5Postfix = "*'o.#,7>q5(|>S94{)})3*/@5b1l7)3]";
        //private readonly string defaultSHA1Prefix = "5#%1+=S~k?|'2f+OJ,t}+ 8[i'/-@37d";
        //private readonly string defaultSHA1Postfix = "l'@;gk2Jy)]H:/Y!0tQ'6_*k{-!e-H=T";

        #endregion keys

        private byte[] symmetricKey;
        private byte[] symmetricIV;
        private CipherMode symmetricCipherMode = CipherMode.CBC;
        private PaddingMode symmetricPaddingMode = PaddingMode.PKCS7;
        private static ILog logger = LogManager.GetLogger("Security");
        private SymmetricCryptoServiceProvider provider = SymmetricCryptoServiceProvider.None;

        public EncryptedStringStyle StringStyle { get; set; }

        #region Service

        public Security(SymmetricCryptoServiceProvider serviceProvider, byte[] key = null, byte[] iv = null, CipherMode cipherMode = CipherMode.CBC, PaddingMode paddingMode = PaddingMode.PKCS7)
        {
            provider = serviceProvider;

            // set key and iv
            switch (provider)
            {
                case SymmetricCryptoServiceProvider.DES:
                    symmetricKey = key ?? defaultDesKey;
                    symmetricIV = iv ?? defaultDesIV;
                    break;

                case SymmetricCryptoServiceProvider.TripleDES:
                    symmetricKey = key ?? defaultTripleDesKey;
                    symmetricIV = iv ?? defaultTripleDesIV;
                    break;

                case SymmetricCryptoServiceProvider.AES:
                    symmetricKey = key ?? defaultAesKey;
                    symmetricIV = iv ?? defaultAesIV;
                    break;

                default:
                    break;
            }

            // set cipher mode and padding mode
            symmetricCipherMode = cipherMode;
            symmetricPaddingMode = paddingMode;
        }

        public string Encrypt(string plaintext)
        {
            switch (provider)
            {
                case SymmetricCryptoServiceProvider.DES:
                    return DESEncrypt(plaintext);

                case SymmetricCryptoServiceProvider.TripleDES:
                    return TripleDESEncrypt(plaintext);

                case SymmetricCryptoServiceProvider.AES:
                    return AESEncrypt(plaintext, StringStyle);

                case SymmetricCryptoServiceProvider.MD5:
                    return MD5Hash(plaintext, true);
                case SymmetricCryptoServiceProvider.DefaultMD5:
                    return MD5Hash(plaintext, false);                
                default:
                    return plaintext;
            }
        }

        public string Decrypt(string ciphertext)
        {
            switch (provider)
            {
                case SymmetricCryptoServiceProvider.DES:
                    return DESDecrypt(ciphertext);

                case SymmetricCryptoServiceProvider.TripleDES:
                    return TripleDESDecrypt(ciphertext);

                case SymmetricCryptoServiceProvider.AES:
                    return AESDecrypt(ciphertext, StringStyle);
                default:
                    return ciphertext;
            }
        }

        #endregion Service

        #region Crypto

        #region DES

        private string DESEncrypt(string plainttext)
        {
            using (DES des = new DESCryptoServiceProvider())
            {
                #region set up

                des.Key = symmetricKey;
                des.IV = symmetricIV;
                des.Mode = symmetricCipherMode;
                des.Padding = symmetricPaddingMode;

                #endregion set up

                byte[] pt = Encoding.UTF8.GetBytes(plainttext);

                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, des.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        try
                        {
                            cs.Write(pt, 0, pt.Length);
                            cs.FlushFinalBlock();

                            return Convert.ToBase64String(ms.ToArray());
                        }
                        catch (CryptographicException ex)
                        {
                            logger.Error("DES Encrypt error. plaintext = " + plainttext, ex);
                            return string.Empty;
                        }
                    }
                }
            }
        }

        private string DESDecrypt(string ciphertext)
        {
            using (DES des = new DESCryptoServiceProvider())
            {
                #region set up

                des.Key = symmetricKey;
                des.IV = symmetricIV;
                des.Mode = symmetricCipherMode;
                des.Padding = symmetricPaddingMode;

                #endregion set up

                byte[] ct = Convert.FromBase64String(ciphertext);

                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, des.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        try
                        {
                            cs.Write(ct, 0, ct.Length);
                            cs.FlushFinalBlock();
                            return Encoding.UTF8.GetString(ms.ToArray());
                        }
                        catch (CryptographicException ex)
                        {
                            logger.Error("DES Decrypt error. plaintext = " + ciphertext, ex);
                            return string.Empty;
                        }
                    }
                }
            }
        }

        #endregion DES

        #region TripleDES

        private string TripleDESEncrypt(string plainttext)
        {
            using (TripleDES tDes = new TripleDESCryptoServiceProvider())
            {
                #region set up

                tDes.Key = symmetricKey;
                tDes.IV = symmetricIV;
                tDes.Mode = symmetricCipherMode;
                tDes.Padding = symmetricPaddingMode;

                #endregion set up

                byte[] pt = Encoding.UTF8.GetBytes(plainttext);

                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, tDes.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        try
                        {
                            cs.Write(pt, 0, pt.Length);
                            cs.FlushFinalBlock();

                            return Convert.ToBase64String(ms.ToArray());
                        }
                        catch (CryptographicException ex)
                        {
                            logger.Error("TripleDES Encrypt error. plaintext = " + plainttext, ex);
                            return string.Empty;
                        }
                    }
                }
            }
        }

        private string TripleDESDecrypt(string ciphertext)
        {
            using (TripleDES tDes = new TripleDESCryptoServiceProvider())
            {
                #region set up

                tDes.Key = symmetricKey;
                tDes.IV = symmetricIV;
                tDes.Mode = symmetricCipherMode;
                tDes.Padding = symmetricPaddingMode;

                #endregion set up

                byte[] ct = Convert.FromBase64String(ciphertext);

                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, tDes.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        try
                        {
                            cs.Write(ct, 0, ct.Length);
                            cs.FlushFinalBlock();
                            return Encoding.UTF8.GetString(ms.ToArray());
                        }
                        catch (CryptographicException ex)
                        {
                            logger.Error("TripleDES Decrypt error. plaintext = " + ciphertext, ex);
                            return string.Empty;
                        }
                    }
                }
            }
        }

        #endregion TripleDES

        #region AES

        private string AESEncrypt(string plainttext, EncryptedStringStyle stringStyle)
        {
            using (Aes aes = new AesCryptoServiceProvider())
            {
                #region set up

                aes.Key = symmetricKey;
                aes.IV = symmetricIV;
                aes.Mode = symmetricCipherMode;
                aes.Padding = symmetricPaddingMode;

                #endregion set up

                byte[] pt = Encoding.UTF8.GetBytes(plainttext);

                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, aes.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        try
                        {
                            cs.Write(pt, 0, pt.Length);
                            cs.FlushFinalBlock();
                            if (stringStyle == EncryptedStringStyle.Base64)
                            {
                                return Convert.ToBase64String(ms.ToArray());
                            }
                            else
                            {
                                return BytesToHexString(ms.ToArray());
                            }
                        }
                        catch (CryptographicException ex)
                        {
                            logger.Error("AES Encrypt error. plaintext = " + plainttext, ex);
                            return string.Empty;
                        }
                    }
                }
            }
        }

        private string AESDecrypt(string ciphertext, EncryptedStringStyle stringStyle)
        {
            using (Aes aes = new AesCryptoServiceProvider())
            {
                #region set up

                aes.Key = symmetricKey;
                aes.IV = symmetricIV;
                aes.Mode = symmetricCipherMode;
                aes.Padding = symmetricPaddingMode;

                #endregion set up

                byte[] ct;
                if (stringStyle == EncryptedStringStyle.Base64)
                {
                    ct = Convert.FromBase64String(ciphertext);
                }
                else
                {
                    ct = HexToByte(ciphertext);
                }

                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, aes.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        try
                        {
                            cs.Write(ct, 0, ct.Length);
                            cs.FlushFinalBlock();
                            return Encoding.UTF8.GetString(ms.ToArray());
                        }
                        catch (CryptographicException ex)
                        {
                            logger.Error("AES Decrypt error. plaintext = " + ciphertext, ex);
                            return string.Empty;
                        }
                    }
                }
            }
        }

        public static string BytesToHexString(byte[] bitArray)
        {
            StringBuilder sb = new StringBuilder();
            foreach(var bit in bitArray)
            {
                sb.Append(bit.ToString("x2"));
            }
            return sb.ToString();
        }

        public static byte[] HexToByte(string hexString)
        {
            //運算後的位元組長度:16進位數字字串長/2
            byte[] byteOUT = new byte[hexString.Length / 2];
            for (int i = 0; i < hexString.Length; i = i + 2)
            {
                //每2位16進位數字轉換為一個10進位整數
                byteOUT[i / 2] = Convert.ToByte(hexString.Substring(i, 2), 16);
            }
            return byteOUT;
        }

        #endregion AES

        #endregion Crypto

        #region MD5 & SHA1

        public static string MD5Hash(string inputText, bool usingTextFix = true)
        {
            StringBuilder sBuilder = new StringBuilder();
            string encodeData = (usingTextFix) ? string.Concat(defaultMD5Prefix, inputText, defaultMD5Postfix) : inputText;

            byte[] buff = Encoding.UTF8.GetBytes(encodeData);
            byte[] data = MD5.Create().ComputeHash(buff);
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }
            return sBuilder.ToString();  
        }

        public static byte[] MD5HashData(string inputText, bool usingTextFix = true)
        {
            StringBuilder sBuilder = new StringBuilder();
            string encodeData = (usingTextFix) ? string.Concat(defaultMD5Prefix, inputText, defaultMD5Postfix) : inputText;

            byte[] buff = Encoding.UTF8.GetBytes(encodeData);
            var result = MD5.Create().ComputeHash(buff);

            for (int i = 0; i < result.Length; i++)
            {
                sBuilder.Append(result[i].ToString("x2"));
            }
            var ans = sBuilder.ToString();

            return result;
        }

        public static string MD5HashCSP(string inputText)
        {
            StringBuilder sBuilder = new StringBuilder();

            byte[] buff = Encoding.UTF8.GetBytes(inputText);
            buff = new MD5CryptoServiceProvider().ComputeHash(buff);
            
            for (int i = 0; i < buff.Length; i++)
            {
                sBuilder.Append(buff[i].ToString("x").PadLeft(2, '0'));
            }
            return sBuilder.ToString();
        }

        public static string GetBase64EncodedSHA1Hash(string plainText)
        {
            var sha1 = SHA1.Create();
            byte[] hashbytes = sha1.ComputeHash(Encoding.ASCII.GetBytes(plainText));
            return Convert.ToBase64String(hashbytes);
        }

        public static string GetSHA1Hash(string plainText)
        {
            var sha1 = SHA1.Create();
            byte[] hashbytes = sha1.ComputeHash(Encoding.UTF8.GetBytes(plainText));
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hashbytes.Length; i++)
            {
                sb.Append(hashbytes[i].ToString("x2"));
            }
            return sb.ToString();
        }

        #endregion

        #region SHA256
                
        public static byte[] SHA256(string input)
        {
            SHA256 sha256 = new SHA256CryptoServiceProvider();
            byte[] source = Encoding.Default.GetBytes(input);//將字串轉為Byte[]
            return sha256.ComputeHash(source);//進行SHA256加密
        }
        
        public static string SHA256Base64(string input)
        {
            SHA256 sha256 = new SHA256CryptoServiceProvider();
            byte[] source = Encoding.Default.GetBytes(input);//將字串轉為Byte[]
            var crypto = sha256.ComputeHash(source);//進行SHA256加密

            return Convert.ToBase64String(crypto);//把加密後的字串從Byte[]轉為字串
        }

        public static string SHA256Hex(string input)
        {
            SHA256 sha256 = new SHA256CryptoServiceProvider();
            byte[] source = Encoding.Default.GetBytes(input);//將字串轉為Byte[]
            var crypto = sha256.ComputeHash(source);//進行SHA256加密

            return BitConverter.ToString(crypto).Replace("-", "").ToLower();
        }

        public static byte[] HmacSHA256(string input, string key)
        {
            byte[] keyByte = Encoding.Default.GetBytes(key);
            byte[] inputByte = Encoding.Default.GetBytes(input);
            var hmacsha256 = new HMACSHA256(keyByte);

            return hmacsha256.ComputeHash(inputByte);            
        }

        public static string HmacSHA256Base64(string input, string key)
        {
            byte[] keyByte = Encoding.Default.GetBytes(key);
            byte[] inputByte = Encoding.Default.GetBytes(input);
            var hmacsha256 = new HMACSHA256(keyByte);
            var crypto = hmacsha256.ComputeHash(inputByte);

            return Convert.ToBase64String(crypto);//把加密後的字串從Byte[]轉為字串            
        }

        public static string HmacSHA256Hex(string input, string key)
        {
            byte[] keyByte = Encoding.Default.GetBytes(key);
            byte[] inputByte = Encoding.Default.GetBytes(input);
            var hmacsha256 = new HMACSHA256(keyByte);
            var crypto = hmacsha256.ComputeHash(inputByte);

            return BitConverter.ToString(crypto).Replace("-", "").ToLower();
        }

        /// <summary>
        /// HmacSHA256 
        /// </summary>
        /// <param name="input"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string HmacSHA256ToBit(string input, string key)
        {
            byte[] keyByte = Encoding.Default.GetBytes(key);
            byte[] inputByte = Encoding.Default.GetBytes(input);
            var hmacsha256 = new HMACSHA256(keyByte);
            byte[] hashmessage = hmacsha256.ComputeHash(inputByte);                        
            return BitConverter.ToString(hashmessage).Replace("-", "").ToLower();
        }

        #endregion
    }

    public enum SymmetricCryptoServiceProvider
    {
        None,
        /// <summary>
        /// 17Life 加鹽後的 MD5
        /// </summary>
        MD5,
        SHA1,
        DES,
        TripleDES,
        AES,
        /// <summary>
        /// 原始MD5
        /// </summary>
        DefaultMD5,
        MD5Hash,
        DefaultMD5Hash,
        SHA256
    }

    public enum EncryptedStringStyle
    {
        Base64, 
        Hex
    }
}
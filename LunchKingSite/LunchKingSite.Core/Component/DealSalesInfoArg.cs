﻿using System;

namespace LunchKingSite.Core.Component
{
    public class SalesInfoArg
    {
        public Guid BusinessHourGuid { get;set; }
        public int Quantity { get;set; }
        public decimal Total { get;set; }
    }
}

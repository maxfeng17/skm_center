﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Drawing;

namespace LunchKingSite.Core.Component
{
    public class BarCodeSettings : IDisposable
    {
        public int BarCodeHeight { get; set; }
        public bool DrawText { get; set; }
        public int LeftMargin { get; set; }
        public int RightMargin { get; set; }
        public int TopMargin { get; set; }
        public int BottomMargin { get; set; }
        public int InterCharacterGap { get; set; }
        public int WideWidth { get; set; }
        public int NarrowWidth { get; set; }
        public Font Font { get; set; }
        public int BarCodeToTextGapHeight { get; set; }

        public BarCodeSettings(bool isDrawText, bool mini = false)
        {
            BarCodeHeight = 50;
            DrawText = isDrawText;
            LeftMargin = 10;
            RightMargin = 10;
            TopMargin = 10;
            BottomMargin = 10;
            InterCharacterGap = 2;
            WideWidth = mini ? 2 : 4;
            NarrowWidth = mini ? 1 : 2;
            Font = new Font("Arial", 12f, FontStyle.Bold);
            BarCodeToTextGapHeight = 1;
        }

        #region IDisposable Members

        public void Dispose()
        {
            if (Font != null)
                Font.Dispose();
        }

        #endregion
    }

    public class BarCode39 : IDisposable
    {
        #region Static initialization

        static Dictionary<char, Pattern> codes;

        static BarCode39()
        {
            codes = new Dictionary<char, Pattern>();
            codes.Add('0', Pattern.Parse("000110100"));
            codes.Add('1', Pattern.Parse("100100001"));
            codes.Add('2', Pattern.Parse("001100001"));
            codes.Add('3', Pattern.Parse("101100000"));
            codes.Add('4', Pattern.Parse("000110001"));
            codes.Add('5', Pattern.Parse("100110000"));
            codes.Add('6', Pattern.Parse("001110000"));
            codes.Add('7', Pattern.Parse("000100101"));
            codes.Add('8', Pattern.Parse("100100100"));
            codes.Add('9', Pattern.Parse("001100100"));
            codes.Add('A', Pattern.Parse("100001001"));
            codes.Add('B', Pattern.Parse("001001001"));
            codes.Add('C', Pattern.Parse("101001000"));
            codes.Add('D', Pattern.Parse("000011001"));
            codes.Add('E', Pattern.Parse("100011000"));
            codes.Add('F', Pattern.Parse("001011000"));
            codes.Add('G', Pattern.Parse("000001101"));
            codes.Add('H', Pattern.Parse("100001100"));
            codes.Add('I', Pattern.Parse("001001100"));
            codes.Add('J', Pattern.Parse("000011100"));
            codes.Add('K', Pattern.Parse("100000011"));
            codes.Add('L', Pattern.Parse("001000011"));
            codes.Add('M', Pattern.Parse("101000010"));
            codes.Add('N', Pattern.Parse("000010011"));
            codes.Add('O', Pattern.Parse("100010010"));
            codes.Add('P', Pattern.Parse("001010010"));
            codes.Add('Q', Pattern.Parse("000000111"));
            codes.Add('R', Pattern.Parse("100000110"));
            codes.Add('S', Pattern.Parse("001000110"));
            codes.Add('T', Pattern.Parse("000010110"));
            codes.Add('U', Pattern.Parse("110000001"));
            codes.Add('V', Pattern.Parse("011000001"));
            codes.Add('W', Pattern.Parse("111000000"));
            codes.Add('X', Pattern.Parse("010010001"));
            codes.Add('Y', Pattern.Parse("110010000"));
            codes.Add('Z', Pattern.Parse("011010000"));
            codes.Add('-', Pattern.Parse("010000101"));
            codes.Add('.', Pattern.Parse("110000100"));
            codes.Add(' ', Pattern.Parse("011000100"));
            codes.Add('$', Pattern.Parse("010101000"));
            codes.Add('/', Pattern.Parse("010100010"));
            codes.Add('+', Pattern.Parse("010001010"));
            codes.Add('%', Pattern.Parse("000101010"));
            codes.Add('*', Pattern.Parse("010010100"));
        }

        #endregion

        private static Brush brush = Brushes.Black;

        private string _code;
        private BarCodeSettings _settings;

        public BarCode39(string code, bool mini)
            : this(code, new BarCodeSettings(true, mini))
        {
        }

        public BarCode39(string code, bool isDrawText, bool mini)
            : this(code, new BarCodeSettings(isDrawText, mini))
        {
        }

        public BarCode39(string code, BarCodeSettings settings)
        {
            foreach (char c in code.ToUpper())
                if (!codes.ContainsKey(c))
                    throw new ArgumentException("Invalid character encountered in specified code.");

            if (!code.StartsWith("*"))
                code = "*" + code;
            if (!code.EndsWith("*"))
                code = code + "*";

            this._code = code;
            this._settings = settings;
        }

        public Bitmap Paint()
        {
            string code = this._code.Trim('*');
            string textToDraw =string.Empty;
            SizeF sizeCodeText = SizeF.Empty;           

            int w = _settings.LeftMargin + _settings.RightMargin;
            foreach (char c in this._code)
                w += codes[c].GetWidth(_settings) + _settings.InterCharacterGap;
            w -= _settings.InterCharacterGap;

            int h = _settings.TopMargin + _settings.BottomMargin + _settings.BarCodeHeight;

            if (_settings.DrawText)
            {
                textToDraw = code.Substring(0, 1);
                for (int i = 1; i < code.Length; i++)
                    textToDraw += "  " + code.Substring(i, 1);
                sizeCodeText = Graphics.FromImage(new Bitmap(1, 1)).MeasureString(textToDraw, _settings.Font);

                h += _settings.BarCodeToTextGapHeight + (int) sizeCodeText.Height;
            }

            Bitmap bmp = new Bitmap(w, h, PixelFormat.Format32bppArgb);
            Graphics g = Graphics.FromImage(bmp);

            int left = _settings.LeftMargin;

            foreach (char c in this._code)
                left += codes[c].Paint(_settings, g, left) + _settings.InterCharacterGap;

            if (_settings.DrawText)
            {
                g.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAliasGridFit;
                int tX = _settings.LeftMargin + (w - _settings.LeftMargin - _settings.RightMargin - (int)sizeCodeText.Width) / 2;
                if (tX < 0)
                    tX = 0;
                int tY = _settings.TopMargin + _settings.BarCodeHeight + _settings.BarCodeToTextGapHeight;
                g.FillRectangle(Brushes.White, tX, tY, sizeCodeText.Width, sizeCodeText.Height);
                g.DrawString(textToDraw, _settings.Font, brush, tX, tY);
            }

            g.Dispose();
            return bmp;
        }

        private class Pattern
        {
            private bool[] nw = new bool[9];

            public static Pattern Parse(string s)
            {
                Debug.Assert(s != null);
                Debug.Assert(s.Length == 9);

                Pattern p = new Pattern();

                int i = 0;
                foreach (char c in s)
                    p.nw[i++] = c == '1';

                return p;
            }

            public int GetWidth(BarCodeSettings settings)
            {
                int width = 0;

                for (int i = 0; i < 9; i++)
                    width += (nw[i] ? settings.WideWidth : settings.NarrowWidth);

                return width;
            }

            public int Paint(BarCodeSettings settings, Graphics g, int left)
            {
                int x = left;

                int w = 0;
                for (int i = 0; i < 9; i++)
                {
                    int width = (nw[i] ? settings.WideWidth : settings.NarrowWidth);

                    if (i % 2 == 0)
                    {
                        Rectangle r = new Rectangle(x, settings.TopMargin, width, settings.BarCodeHeight);
                        g.FillRectangle(brush, r);
                    }

                    x += width;
                    w += width;
                }

                return w;
            }
        }

        #region IDisposable Members

        public void Dispose()
        {
            if (_settings != null)
                _settings.Dispose();
        }

        #endregion
    }

}

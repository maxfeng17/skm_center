﻿using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace LunchKingSite.Core
{
    /// <summary>
    /// 參考unit test 
    /// </summary>
    public class SmartGetDealTitlePrefix
    {
        public class SubDealUnitTitle
        {
            public string SubTitle { get; set; }
            public int UnitNumber { get; set; }
            public string UnitTitle { get; set; }
        }

        public SmartGetDealTitlePrefix()
        {
            SubItems = new List<SubDealUnitTitle>();
        }

        public string Prefix { get; set; }
        public List<SubDealUnitTitle> SubItems { get; set; }

        //private const string _UNIT_NAMES = "[盒|包|箱|組|件|入|雙|份]";

        public static SmartGetDealTitlePrefix Parse(List<string> lines)
        {
            if (lines == null || lines.Count == 0)
            {
                return null;
            }

            SmartGetDealTitlePrefix result = new SmartGetDealTitlePrefix();

            string firstLine = lines[0];
            StringBuilder sb = new StringBuilder();

            bool finish = false;

            for (int i = 0; i < firstLine.Length; i++)
            {
                char ch = firstLine[i];
                for (int j = 1; j < lines.Count; j++)
                {
                    if (lines[j].Length < j || ch != lines[j][i])
                    {
                        finish = true;
                        break;
                    }
                }
                if (finish)
                {
                    break;
                }
                else
                {
                    sb.Append(ch);
                }
            }
            result.Prefix = sb.ToString();
            if (result.Prefix == string.Empty)
            {
                List<SubDealUnitTitle> subItems;
                ParseUnitTitleNumber(lines, out subItems);
                result.SubItems = subItems;
            }
            else
            {
                char lastChar = result.Prefix[result.Prefix.Length - 1];
                if (lastChar == '(' || lastChar == '（')
                {
                    result.Prefix = result.Prefix.Substring(0, result.Prefix.Length - 1);
                }
                List<SubDealUnitTitle> subItems;

                List<string> subTitles = new List<string>();
                foreach (string line in lines)
                {
                    subTitles.Add(line.Substring(result.Prefix.Length));
                }

                ParseUnitTitleNumber(subTitles, out subItems);
                result.SubItems = subItems;
                result.Prefix = result.Prefix.Trim();
            }
            return result;
        }

        private static void ParseUnitTitleNumber(List<string> lines, out List<SubDealUnitTitle> subItems)
        {
            subItems = new List<SubDealUnitTitle>();

            List<MatchCollection> matchCols = new List<MatchCollection>();
            foreach (string line in lines)
            {
                //MatchCollection matchCol = Regex.Matches(line, @"(?<number>\d+)(?<title>" + _UNIT_NAMES + ")", RegexOptions.IgnoreCase | RegexOptions.Multiline);
                MatchCollection matchCol = Regex.Matches(line, "(?<number>\\d+)(?<title>[\\u4e00-\\u9fa5])", RegexOptions.IgnoreCase | RegexOptions.Multiline);
                matchCols.Add(matchCol);
            }

            HashSet<int> matchCountGroups = new HashSet<int>();
            foreach (var matchCol in matchCols)
            {
                if (matchCol.Count == 0)
                {
                    continue;
                }
                if (matchCountGroups.Contains(matchCol.Count) == false)
                {
                    matchCountGroups.Add(matchCol.Count);
                }
            }

            if (matchCountGroups.Count == 1)
            {
                for (int i = 0; i < lines.Count; i++)
                {
                    string line = lines[i];
                    var matchCol = matchCols[i];
                    subItems.Add(
                        new SubDealUnitTitle
                        {
                            SubTitle = line,
                            UnitNumber = int.Parse(matchCol[0].Groups[1].Value),
                            UnitTitle = matchCol[0].Groups[2].Value
                        }
                    );
                }
            }
            else
            {
                for (int i = 0; i < lines.Count; i++)
                {
                    string line = lines[i];
                    var matchCol = matchCols[i];
                    if (matchCol.Count == 1)
                    {
                        subItems.Add(
                            new SubDealUnitTitle
                            {
                                SubTitle = line,
                                UnitNumber = int.Parse(matchCol[0].Groups[1].Value),
                                UnitTitle = matchCol[0].Groups[2].Value
                            }
                        );
                    }
                    else if (matchCol.Count > 1)
                    {

                        subItems.Add(
                            new SubDealUnitTitle
                            {
                                SubTitle = line,
                                UnitNumber = 0,
                                UnitTitle = matchCol[0].Groups[2].Value
                            }
                        );
                    }
                    else
                    {
                        subItems.Add(
                            new SubDealUnitTitle
                            {
                                SubTitle = line,
                                UnitNumber = 0,
                                UnitTitle = string.Empty
                            }
                        );
                    }
                }
            }
        }

        public string GetUnitTitle()
        {
            if (this.SubItems.Count == 0)
            {
                return "份";
            }
            return this.SubItems[0].UnitTitle;
        }
        /// <summary>
        /// 主標-子標, 置換成【主標】子標
        /// </summary>
        /// <returns></returns>
        public string ToStrongPrefix()
        {
            return StrongDashTitle(this.Prefix);
        }

        public static string StrongDashTitle(string prefix)
        {
            StringBuilder sb = new StringBuilder();
            string[] parts = prefix.Split('-');
            if (parts.Length >= 2)
            {
                sb.Append('【');
                sb.Append(parts[0]);
                sb.Append('】');
                sb.Append(parts[1]);
                for (int i = 2; i < parts.Length; i++)
                {
                    sb.Append('-');
                    sb.Append(parts[i]);
                }
                return sb.ToString();
            }
            return prefix;
        }
    }
}
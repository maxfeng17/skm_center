﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SqlServer.Types;

namespace LunchKingSite.Core.Component
{
    public class GeographySearchParameter
    {
        public GeographySearchParameter(SqlGeography geography , int distance)
        {
            Geography = geography;
            Distance = distance;
        }

        /// <summary>
        /// sql地理資訊座標
        /// </summary>
        public SqlGeography Geography { get; set; }
        /// <summary>
        /// 要搜尋的距離
        /// </summary>
        public int Distance { get; set; }
    }
}

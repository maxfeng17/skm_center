using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web.Configuration;

namespace LunchKingSite.Core
{
    public class WebConfSysConfProvider : SysConfProviderBase
    {
        System.Configuration.Configuration config = null;

        protected override Dictionary<string, string> GetSettings()
        {
            config = WebConfigurationManager.OpenWebConfiguration(WebConfigurationManager.AppSettings["sysconf-path"]);
            KeyValueConfigurationCollection settings = config.AppSettings.Settings;

            Dictionary<string, string> dic = new Dictionary<string, string>(settings.Count, StringComparer.OrdinalIgnoreCase);
            foreach (string key in settings.AllKeys)
                dic.Add(key, settings[key].Value);

            return dic;
        }

        public override bool Persist()
        {
            try
            {
                config.Save(ConfigurationSaveMode.Minimal);
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}

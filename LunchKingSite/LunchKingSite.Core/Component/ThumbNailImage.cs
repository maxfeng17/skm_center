﻿using LunchKingSite.Core.Interface;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Net;

namespace LunchKingSite.Core.Component
{
    public class ThumbNailImage : IThumbNailImage
    {
        private Image thumbnail;

        public void TransferOriginalImage(string url)
        {
            int width = 227;
            int height = 126;
            Bitmap bm = new Bitmap(width, height);
            HttpWebRequest img_request = (HttpWebRequest)HttpWebRequest.Create(url);
            HttpWebResponse img_response = (HttpWebResponse)img_request.GetResponse();
            using (Image image = Image.FromStream(img_response.GetResponseStream()))
            {
                Graphics g = Graphics.FromImage(bm);
                g.CompositingQuality = CompositingQuality.HighQuality;
                g.SmoothingMode = SmoothingMode.HighQuality;
                g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                Rectangle rc = new Rectangle(0, 0, width, height);
                g.DrawImage(image, rc);
            }
            thumbnail = bm;
        }

        public void ShowThumbNailImage(Stream outputstream)
        {
            thumbnail.Save(outputstream, ImageFormat.Jpeg);
        }
    }
}
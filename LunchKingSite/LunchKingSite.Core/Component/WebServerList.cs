﻿using System.Configuration;

namespace LunchKingSite.Core.Component
{
    public class WebServerList : ConfigurationSection
    {
        [ConfigurationProperty("server", IsDefaultCollection = false)]
        [ConfigurationCollection(typeof(ServerCollection), AddItemName = "add", ClearItemsName = "clear", RemoveItemName = "remove")]
        public ServerCollection WebSites
        {
            get { return (ServerCollection)base["server"]; }
        }
    }

    public class Server : ConfigurationElement
    {
        public Server() { }

        [ConfigurationProperty("host", IsRequired = true, IsKey = true)]
        public string Host
        {
            get { return (string) this["host"]; }
            set { this["host"] = value; }
        }

        [ConfigurationProperty("ipAddress", IsKey = false)]
        public string IpAddress
        {
            get { return (string) this["ipAddress"]; }
            set { this["ipAddress"] = value; }
        }

        public Server(string host, string ipAddress)
        {
            Host = host;
            IpAddress = ipAddress;
        }
    }

    public class ServerCollection : ConfigurationElementCollection
    {
        public ServerCollection() { }

        public Server this[int index]
        {
            get { return (Server) BaseGet(index); }
            set
            {
                if (BaseGet(index) != null)
                {
                    BaseRemoveAt(index);
                }
                BaseAdd(index, value);
            }
        }

        public void Add(Server site)
        {
            BaseAdd(site);
        }

        public void Clear()
        {
            BaseClear();
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new Server();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((Server) element).Host;
        }

        public void Remove(Server site)
        {
            BaseRemove(site.Host);
        }

        public void RemoveAt(int index)
        {
            BaseRemoveAt(index);
        }
    }
}

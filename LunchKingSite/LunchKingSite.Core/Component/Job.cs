using LunchKingSite.Core.Configuration;
using Quartz;
using System;

namespace LunchKingSite.Core
{
    /// <summary>
    /// Configurable IJob descritpion
    /// </summary>
    public class Job : Quartz.IJob
    {
        private static readonly log4net.ILog logger = log4net.LogManager.GetLogger(typeof(Job));

        public virtual void Execute(JobExecutionContext context)
        {
            JobDataMap dataMap = context.JobDetail.JobDataMap;
            if (dataMap != null)
            {
                JobSetting js = dataMap.Get(Jobs.KEY_JOB_SETTING) as JobSetting;
                if (js != null)
                {
                    IJob theJob = new JobLoadBalance(this.CreateJobInstance(js.Worker), js);
                    if (theJob != null)
                    {
                        try
                        {
                            logger.Debug(js.Worker.Name + " is executing.");
                            theJob.Execute(js);
                            logger.Debug(js.Worker.Name + " was executed.");
                        }
                        catch (Exception e)
                        {
                            logger.Error(js.Worker.FullName + " job exception during execution", e);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Attempts to create an instance of the IJob. If the type
        /// can not be created, this Job will be disabled.
        /// </summary>
        /// <returns></returns>
        protected IJob CreateJobInstance(Type jobType)
        {
            IJob theJob = null;
            if (jobType != null)
            {
                System.Reflection.ConstructorInfo ctorInfo = jobType.GetConstructor(new Type[0]);
                if (ctorInfo == null) // could be a singleton object
                {
                    theJob = jobType.InvokeMember("Instance", System.Reflection.BindingFlags.InvokeMethod | System.Reflection.BindingFlags.GetProperty, null, jobType, new object[0]) as IJob;
                }
                else
                {
                    theJob = Activator.CreateInstance(jobType) as IJob;
                }
            }

            return theJob;
        }
    }
}
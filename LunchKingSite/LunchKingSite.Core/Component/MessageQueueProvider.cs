﻿using System;
using System.Collections.Generic;
using Apache.NMS;
using Apache.NMS.ActiveMQ;
using Apache.NMS.Util;

namespace LunchKingSite.Core.Component
{
    public class MessageQueueProvider : IMessageQueueProvider
    {
        private IConnection connection;        
        private ISysConfProvider config = ProviderFactory.Instance().GetConfig();

        public MessageQueueProvider()
        {
            ResetConnect();
        }

        public void Disconnect()
        {
            connection.Close();
            connection.Dispose();
        }

        public void ResetConnect()
        {
            Uri connecturi = new Uri(config.ActiveMQServerUri);
            IConnectionFactory factory = new ConnectionFactory(connecturi);
            connection = factory.CreateConnection("admin", "admin");
        }


        public IMessageConsumer GetConsumer(string queueName)
        {
            ISession session = connection.CreateSession();
            IDestination destination = SessionUtil.GetDestination(session, queueName);
            IMessageConsumer mc = session.CreateConsumer(destination);
            return mc;
        }

        public int GetMessageCount(string queueName)
        {
            int messageCount = 0;
            using (ISession session = connection.CreateSession())
            {
                IDestination destination = SessionUtil.GetDestination(session, queueName);
                IQueueBrowser queueBrowser = session.CreateBrowser((IQueue)destination);

                if (connection.IsStarted == false)
                {
                    connection.Start();
                }

                var messages = queueBrowser.GetEnumerator();
                while (messages.MoveNext())
                {
                    messageCount++;
                }
            }

            return messageCount;
        }

        /// <summary>
        /// queueName格式為 queue://your name
        /// 傳遞文字到 ActiveMQ Server
        /// </summary>
        /// <param name="queueName"></param>
        /// <param name="queueText"></param>
        /// <param name="categoryName"></param>
        /// <param name="priority"></param>
        public void Send(string queueName, string queueText, string categoryName = "", MsgPriority priority = MsgPriority.Normal)
        {
            using (ISession session = connection.CreateSession())
            {
                IDestination destination = SessionUtil.GetDestination(session, queueName);
                using (IMessageProducer producer = session.CreateProducer(destination))
                {
                    if (connection.IsStarted == false)
                    {
                        connection.Start();
                    }                    
                    ITextMessage message = session.CreateTextMessage(queueText);
                    message.NMSCorrelationID = categoryName;
                    producer.Priority = priority;
                    producer.RequestTimeout = new TimeSpan(0, 0, 0, 0, config.ActiveMQSendTimeoutMillSeconds);
                    producer.Send(message);
                }
            }
        }
        /// <summary>
        /// queueName格式為 queue://your name
        /// obj會序列化成json格式傳遞到 ActiveMQ Server
        /// </summary>
        /// <param name="queueName"></param>
        /// <param name="obj"></param>
        /// <param name="categoryName"></param>
        /// <param name="priority"></param>
        public void Send(string queueName, object obj, string categoryName = "", MsgPriority priority = MsgPriority.Normal)
        {
            string msg = ProviderFactory.Instance().GetSerializer().Serialize(obj);
            try
            {
                Send(queueName, msg, categoryName, priority);
            }
            catch (RequestTimedOutException ex)
            {
                ExceptionBalloon.GetOrCreate("ActiveMQ Send Error", 10, 100).BlowException(ex, typeof(MessageQueueProvider),
                    "ActiveMQ 連線太慢/或是溢出來了");
            }
        }
    }
}

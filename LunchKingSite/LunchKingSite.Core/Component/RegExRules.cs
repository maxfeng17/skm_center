﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using LunchKingSite.DataOrm;

namespace LunchKingSite.Core.Component
{
    public class RegExRules
    {
        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        private const string RegExEmail = @"^(?!\.)(""([^""\r\\]|\\[""\r\\])*""|"
                                           + @"([-a-z0-9!#$%&'*+/=?^_`{|}~]|(?<!\.)\.)*)(?<!\.)"
                                           + @"@[a-z0-9][\w\.-]*[a-z0-9]\.[a-z][a-z\.]*[a-z]$";

        private static Regex regexEmail = new Regex(RegExEmail, RegexOptions.Compiled | RegexOptions.IgnoreCase);
        private static Regex regexEng = new Regex(@"[a-zA-z]", RegexOptions.Compiled | RegexOptions.IgnorePatternWhitespace);
        private static Regex regexPass = new Regex(@"^[0-9a-zA-Z]{6,100}$", RegexOptions.Compiled);
        private static Regex rexgexNumber = new Regex(@"\D");

        //private static Regex regMobile = new Regex(@"[0-9]{10}", RegexOptions.Compiled);
        private static char[] commas = { ',', ';', '，', '；' };

        public static bool CheckMobile(string mobile) 
        {
            if (string.IsNullOrEmpty(mobile))
            {
                return false;
            }
            //return regMobile.IsMatch(mobile);
            int test;
            return mobile.StartsWith("09") && int.TryParse(mobile, out test) && mobile.Length.Equals(10);
        }

        public static bool CheckEmail(string sEmail)
        {
            if (string.IsNullOrWhiteSpace(sEmail))
            {
                return false;
            }
            if (sEmail[0] == '@') return false; // 補正則表示式的不足(其實是改不動)
            //檢查2個點dot不能重複相鄰的出現
            if (CheckEmailTwoDots(sEmail) == false)
            {
                return false;
            }
            if (sEmail.Length > Member.UserNameColumn.MaxLength)
            {
                return false;
            }
            return regexEmail.Match(sEmail).Success;
        }

        /// <summary>
        /// 檢查2個點dot不能重複相鄰的出現
        /// </summary>
        /// <param name="email"></param>
        /// <returns>false 表示這個狀態發生，此 email 不符合格式</returns>
        private static bool CheckEmailTwoDots(string email)
        {
            int pos = -1;
            for (int i=0;i<email.Length;i++)
            {
                char ch = email[i];
                if (ch == '.')
                {
                    if (pos >= 0 && i - pos == 1)
                    {
                        return false;
                    }
                    pos = i;  
                    
                }
            }
            return true;
        }

        /// <summary>
        /// 判斷傳入的Email是否有分隔字元
        /// </summary>
        /// <param name="email">傳入的Email字串</param>
        /// <returns>是否含有分隔字元</returns>
        public static bool IsEmailContainsSeperator(string email) 
        {
            bool result = false;
            foreach (char sep in commas) 
            {
                if (email.Contains(sep))
                    result = true;
            }
            return result;
        }

        /// <summary>
        /// 有些 Email String 是由多個 Email 加上逗號或分號組成，要檢核這些 Email 的格式，須先拆開並一個個檢核 
        /// </summary>
        /// <param name="sEmail"></param>
        /// <returns>合乎 Email 格式所組成的 string</returns>
        public static string CheckMultiEmail(string sEmail)
        {
            var mailLiet = CheckMultiEmailToList(sEmail);
            return string.Join(",", mailLiet);
        }

        /// <summary>
        /// 檢查信箱
        /// </summary>
        /// <param name="sEmail"></param>
        /// <returns></returns>
        public static List<string> CheckMultiEmailToList(string sEmail)
        {
            sEmail = sEmail.Replace(" ", ",").Replace("　", ",").Replace("／", ",").Replace("/", ",").Replace("\t", ",")
                .Replace("、", ",").Replace("`", ",").Replace(";", ",").Replace("；", ",").Replace("，", ",");


            List<string> result = new List<string>();

            if (!IsEmailContainsSeperator(sEmail))
            {
                //支援檢查單一信箱
                if (CheckEmail(sEmail))
                {
                    result.Add(sEmail);
                }
                else
                {
                    return new List<string>();
                }
            }
            else
            {
                string[] mails = sEmail.Split(commas, StringSplitOptions.RemoveEmptyEntries);
                foreach (string mail in mails)
                {
                    var trimEmail = mail.Trim();
                    if (CheckEmail(trimEmail))
                    {
                        result.Add(trimEmail);
                    }
                    else
                    {
                        return new List<string>();
                    }
                }
            }
            return result;
        }

        public static bool CheckPassword(string password)
        {
            if (password == null)
            {
                return false;
            }
            if (regexPass.Match(password).Success)
            {
                return true;
            }
            return false;
        }

        public static bool StringIsEng(string sPattern)
        {
            if (sPattern == null)
            {
                return false;
            }            
            return regexEng.Match(sPattern).Success;
        }

        /// <summary>
        /// HTML Code 最小化
        /// </summary>
        /// <param name="shtml"></param>
        /// <returns></returns>
        public static string StripHtml(string shtml)
        {
            var regexLineBreaks = new Regex(@"\n\s*", RegexOptions.Compiled);
            var regexLineSpace = new Regex(@"\n\s*\r", RegexOptions.Compiled);
            var regexSpace = new Regex(@"( )+", RegexOptions.Compiled);
            var regexLineNewline = new Regex(Environment.NewLine, RegexOptions.Compiled);
            var regexLineReturn = new Regex(@"\r", RegexOptions.Compiled);

            string sRetHtml = regexLineBreaks.Replace(shtml, "");
            sRetHtml = regexLineSpace.Replace(sRetHtml, "");
            sRetHtml = regexSpace.Replace(sRetHtml, " ");
            sRetHtml = regexLineNewline.Replace(sRetHtml, "");
            sRetHtml = regexLineReturn.Replace(sRetHtml, "");

            return sRetHtml;
        }
        /// <summary>
        /// 檢查是否為統編或身份證號碼
        /// </summary>
        /// <param name="identityOrTaxNumber"></param>
        /// <returns></returns>
        public static string CheckPersonIdOrCompanyNo(string identityOrTaxNumber)
        {
            if (string.IsNullOrEmpty(identityOrTaxNumber))
            {
                return "沒有輸入!";
            }
            if (identityOrTaxNumber.Length == 8)
            {
                return CompanyNoCheck(identityOrTaxNumber);
            }
            return PersonalIdCheck(identityOrTaxNumber);
        }

        /// <summary>
        /// 公司統一編號驗證
        /// </summary>
        /// <param name="companyNo"></param>
        /// <returns></returns>
        public static string CompanyNoCheck(string companyNo)
        {
            byte[] companyIdChkFac = { 1, 2, 1, 2, 1, 2, 4, 1 };

            string errMsg = string.Empty;
            if (string.IsNullOrEmpty(companyNo))
                return "沒有輸入統編!";
            if (companyNo == "00000000")
                return "請輸入正確統編";
            if (!Regex.IsMatch(companyNo, "\\d{8}"))
                errMsg = "統編應為8位數字!";
            else
            {
                int sum = 0;
                for (int i = 0; i < 8; i++)
                {
                    int d = (companyNo[i] - 48) * companyIdChkFac[i];
                    sum += d / 10 + d % 10;
                }
                if (
                    !(sum % 10 == 0 ||
                      companyNo[6] == '7' && (sum + 1) % 10 == 0)
                    )
                    errMsg = "統編檢查碼不符!";
            }
            return errMsg;
        }

        /// <summary>
        /// 統一證號ID檢查
        /// </summary>
        /// <param name="personalId">統一證號ID</param>
        /// <param name="checkWithForeign">是否檢查外來人口編碼原則</param>
        /// <returns></returns>
        public static string PersonalIdCheck(string personalId, bool checkWithForeign = true)
        {
            bool isValidate = false;
            string errMsg = string.Empty;
            personalId = personalId.ToUpper();

            if (personalId.Length != 10)
            {
                return "身份證字號應輸入10碼!!";
            }

            if (string.IsNullOrEmpty(personalId))
            {
                return "身份證字號沒有輸入!!";
            }

            // 外來人口統一證號編碼原則(https://www.immigration.gov.tw/ct_cert.asp?xItem=1106801&ctNode=32601&mp=1)
            // 第1碼為區域碼，同國民身分證[A-Z]；第2碼為性別碼[A-D]、3至10碼為阿拉伯數字[0-9]，其中第3至9碼為流水號、第10碼為檢查號碼
            var foreignRegex = new Regex("^[A-Z]{1}[A-D]{1}[0-9]{8}$");
            if (checkWithForeign && foreignRegex.IsMatch(personalId))
            {
                return string.Empty;
            }
            else
            {
                // 中華民國身份證字號編碼原則檢查
                var regex = new Regex("^[A-Z]{1}[0-9]{9}$");
                if (!regex.IsMatch(personalId))
                {
                    return "身份證字號驗證失敗!!";
                }

                // 檢查規則驗證
                if (personalId[0] >= 0x41 && personalId[0] <= 0x5A)
                {
                    var a = new[] { 10, 11, 12, 13, 14, 15, 16, 17, 34, 18, 19, 20, 21, 22, 35, 23, 24, 25, 26, 27, 28, 29, 32, 30, 31, 33 };
                    var b = new int[11];
                    b[1] = a[(personalId[0]) - 65] % 10;
                    var c = b[0] = a[(personalId[0]) - 65] / 10;
                    for (var i = 1; i <= 9; i++)
                    {
                        b[i + 1] = personalId[i] - 48;
                        c += b[i] * (10 - i);
                    }
                    if (((c % 10) + b[10]) % 10 == 0)
                    {
                        isValidate = true;
                    }
                }
            }
            return !isValidate ? "身份證字號檢查規則驗證失敗!!" : string.Empty;
        }

        /// <summary>
        /// 驗證信用卡規則
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static bool CheckLuhnForCreditCard(string s)
        {
            s = s.Reverse();
            int s1 = 0;
            for (int i = 0; i < s.Length; i = i + 2)
            {
                s1 += Convert.ToInt32(s[i].ToString());
            }
            int s2 = 0;
            for (int i = 1; i < s.Length; i = i + 2)
            {
                int tmp = Convert.ToInt32(s[i].ToString()) * 2;
                s2 = s2 + tmp / 10 + tmp % 10;
            }
            return (s1 + s2) % 10 == 0;
        }

        /// <summary>
        /// 驗證信用卡 (目前結帳是以此規則驗證)
        /// </summary>
        /// <param name="creditCardNo"></param>
        /// <param name="checkInLuhnFormat">卡號是否符合Luhn的驗證格式</param>
        /// <param name="errorMsg"></param>
        /// <returns></returns>
        public static bool CheckCreditCardNumber(string creditCardNo, bool checkInLuhnFormat, out string errorMsg)
        {
            errorMsg = string.Empty;
            if (!int.Equals(16, creditCardNo.Length) || rexgexNumber.IsMatch(creditCardNo))
            {
                errorMsg = "信用卡號驗證錯誤";
                return false;
            }
            if (checkInLuhnFormat)
            {
                if (!CheckLuhnForCreditCard(creditCardNo))
                {
                    errorMsg = "信用卡規則驗證失敗";
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// 驗證信用卡有效日期
        /// </summary>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <param name="errorMsg"></param>
        /// <returns></returns>
        public static bool CheckCreditCardValidDate(string year, string month, out string errorMsg)
        {
            errorMsg = string.Empty;
            if (!int.Equals(2, year.Length) || rexgexNumber.IsMatch(year))
            {
                errorMsg = "有效日期年錯誤";
                return false;
            }

            int num;
            int.TryParse(month, out num);
            if (!int.Equals(2, month.Length) || rexgexNumber.IsMatch(month) || num < 1 || num > 12)
            {
                errorMsg = "有效日期月錯誤";
                return false;
            }
            return true;
        }
    }
}

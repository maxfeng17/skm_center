﻿using System;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.XPath;
using System.Xml.Xsl;

namespace LunchKingSite.Core.Component
{
    /// <summary>
    /// 提供轉換的方式, 把 XSL 範本 與 XML 資料 轉為輸出文件. 若 XSL 範本有指定參數, 請設定 XslParameters.
    /// </summary>
    public class XslTransformer
    {
        /// <summary>
        /// XSL 範本檔案
        /// </summary>
        public IXPathNavigable XslStylesheet { get; set; }
        
        /// <summary>
        /// XML 輸入資料
        /// </summary>
        public IXPathNavigable XmlData { get; set; }
        
        /// <summary>
        /// XSL 參數值
        /// </summary>
        public XsltArgumentList XslParameters { get; set; }
        
        #region .ctors

        public XslTransformer(){}

        public XslTransformer(IXPathNavigable stylesheet, IXPathNavigable xmlData)
        {
            XslStylesheet = stylesheet;
            XmlData = xmlData;
        }

        #endregion

        /// <summary>
        /// 把資料 XmlData 和範本 XslStylesheet 及選擇性的 XslParameters 轉換成字串.
        /// 若 XmlData 或 XslStylesheet 沒設定的話, 會丟出 InvalidOperationException.
        /// </summary>
        /// <returns></returns>
        public string Transform()
        {
            if (XslStylesheet == null || XmlData == null)
                throw new InvalidOperationException("一定要有 XML資料和 XSL範本才能轉換成輸出.");
            
            XslCompiledTransform xslTransform = new XslCompiledTransform();
            xslTransform.Load(XslStylesheet);

            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Encoding = Encoding.UTF8;
            settings.ConformanceLevel = ConformanceLevel.Document;

            StringWriter sw = new StringWriter();
            using (sw)
            {
                XmlWriter xw = XmlWriter.Create(sw, settings);
                xslTransform.Transform(XmlData, XslParameters, xw);
                return sw.ToString();
            }
        }

        /// <summary>
        /// 把物件轉成 XmlDocument.
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static IXPathNavigable SerializeObjectToXml(object data)
        {
            XmlSerializer x = new XmlSerializer(data.GetType());
            MemoryStream memStream = new MemoryStream();
            x.Serialize(memStream, data);
            memStream.Seek(0, System.IO.SeekOrigin.Begin);
            XmlDocument doc = new XmlDocument();
            doc.Load(memStream);

            return doc;
        }
    }
}

﻿using System;
using System.IO;
using System.Text;
using System.Security.Cryptography;

namespace LunchKingSite.Core.Component
{
    public class Cryptography
    {
        private string _key;
        private string _iv;

        /// <summary>
        /// 初始宣告
        /// </summary>
        /// <param name="iKeyType">請依下列設明選擇金鑰設定，數字序號<br />
        /// 0. 用 MD5 不需要 Key & IV <br />
        /// 1. 17Life好康訂閱信 - 取消訂閱用 <br />
        /// 2. Piinlife訂閱信 - 取消訂閱用<br />
        /// 3. 信用卡號編碼用<br />
        /// </param>
        public Cryptography(int iKeyType)
        {
            SetKey(iKeyType);
        }

        private void SetKey(int iType)
        {
            switch (iType)
            {
                case 1: //17Life好康訂閱信 - 取消訂閱用
                    _key = "17Life_Ppon";
                    break;
                case 2: //Piinlife訂閱信 - 取消訂閱用
                    _key = "17Life_PiinLife";
                    break;
                case 3: //信用卡號用
                    _key = "17paykey";
                    _iv = "17payiv_";
                    break;
                default: //金鑰跟初始向量皆為空字串
                    _key = string.Empty;
                    _iv = string.Empty;
                    break;
            }

        }

        #region DES
        /// <summary>
        /// 取得DES編碼字串
        /// </summary>
        /// <param name="strOriValue">原始字串</param>
        /// <param name="iOffest">位移數，預設為0(已有預設值，可以不用給予此參數)</param>
        /// <returns></returns>
        public string Encrypt(string strOriValue, int iOffest = 0)
        {
            return Encrypt(strOriValue, _key, _iv, iOffest);
        }
        /// <summary>
        /// 取得DES解碼字串
        /// </summary>
        /// <param name="strEnValue">加密後字串</param>
        /// <param name="iOffest">位移數，預設為0(已有預設值，可以不用給予此參數)<br />請注意！請跟原資料設計者確認位移數誤隨意設定！</param>
        /// <returns></returns>
        public string Decrypt(string strEnValue, int iOffest = 0)
        {
            return Decrypt(strEnValue, _key, _iv, iOffest);
        }

        /// <summary>
        /// 驗證加密字串
        /// </summary>
        /// <param name="strOriValue">原始字串</param>
        /// <param name="strEnValue">加密後字串</param>
        /// <param name="iOffset">位移數，預設為0</param>
        /// <returns></returns>
        public bool ValidateCrypto(string strOriValue, string strEnValue, int iOffset = 0)
        {
            return (Decrypt(strEnValue, _key, _iv, iOffset) == strOriValue) ? true : false;
        }

        /// <summary>
        /// 加密演算法
        /// </summary>
        /// <param name="strOriValue">原始字串</param>
        /// <param name="strKey">加密用鍵，一定要8碼</param>
        /// <param name="strIv">加密用的初始向量，一定要8碼</param>
        /// <param name="iOffset">位移數，預設為0</param>
        /// <returns></returns>
        private string Encrypt(string strOriValue, string strKey, string strIv, int iOffset)
        {
            StringBuilder enValue = new StringBuilder();

            using (DESCryptoServiceProvider des = new DESCryptoServiceProvider())
            {
                byte[] inputArray = Encoding.Default.GetBytes(strOriValue);
                des.Key = ASCIIEncoding.UTF8.GetBytes(strKey);
                des.IV = ASCIIEncoding.UTF8.GetBytes(strIv);

                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, des.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(inputArray, iOffset, inputArray.Length);
                        cs.FlushFinalBlock();
                    }

                    foreach (byte b in ms.ToArray())
                    {
                        enValue.AppendFormat("{0:X2}", b);
                    }
                }
            }
            return enValue.ToString();
        }
        /// <summary>
        /// 解密
        /// </summary>
        /// <param name="strEnValue">加密後字串</param>
        /// <param name="strKey">加密用鍵</param>
        /// <param name="strIv">加密用的初始向量</param>
        /// <param name="iOffset">位移數，預設為0</param>
        /// <returns></returns>
        private string Decrypt(string strEnValue, string strKey, string strIv, int iOffset)
        {
            using (DESCryptoServiceProvider des = new DESCryptoServiceProvider())
            {
                byte[] inputArray = new byte[strEnValue.Length / 2];

                for (int i = 0; i < strEnValue.Length / 2; i++)
                {
                    int n = Convert.ToInt32(strEnValue.Substring(i * 2, 2), 16);
                    inputArray[i] = (byte)n;
                }

                des.Key = ASCIIEncoding.UTF8.GetBytes(strKey);
                des.IV = ASCIIEncoding.UTF8.GetBytes(strIv);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, des.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        try
                        {
                            cs.Write(inputArray, iOffset, inputArray.Length);
                            cs.FlushFinalBlock();
                            return Encoding.Default.GetString(ms.ToArray());
                        }
                        catch (CryptographicException)
                        {
                            return "error";
                        }
                    }
                }
            }
        }

        #endregion

        #region MD5
        /// <summary>
        /// 取得MD5編碼後的字串
        /// </summary>
        /// <param name="strAryOriValue">原始編碼字串陣列</param>
        /// <param name="strSplitSign">串接字串陣列用的符號，預設為空字串</param>
        /// <returns></returns>
        public string Md5(string[] strAryOriValue, string strSplitSign = "")
        {
            string oriString = string.Join(strSplitSign, strAryOriValue);
            return Md5(oriString + _key);   //將輸入的原始字串再加上鑰匙
        }
        /// <summary>
        /// 驗證編碼字串
        /// </summary>
        /// <param name="strAryOriValue">原始編碼字串陣列</param>
        /// <param name="strEnValue">編碼後之字串</param>
        /// <param name="strSplitSign">串接字串陣列用的符號，預設為空字串</param>
        /// <returns></returns>
        public bool ValidateMd5(string[] strAryOriValue, string strEnValue, string strSplitSign = "")
        {
            return (Md5(strAryOriValue) == strEnValue) ? true : false;
        }
        /// <summary>
        /// MD5編碼演算法
        /// </summary>
        /// <param name="strOriValue">原始編碼字串陣列</param>
        /// <returns></returns>
        private string Md5(string strOriValue)
        {
            MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
            byte[] b = md5.ComputeHash(Encoding.UTF8.GetBytes(strOriValue));
            return BitConverter.ToString(b).Replace("-", string.Empty);
        }
        #endregion

    }
}

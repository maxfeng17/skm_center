﻿using System;
using System.Collections.Generic;

namespace LunchKingSite.Core.Component
{
    public class RedisCacheProvider : ICacheProvider
    {
        #region hash

        public T HGet<T>(string key, string fieldName, bool compress = false) where T : class
        {
            return RedisCache.Currnet.HGet<T>(key, fieldName, compress);
        }

        public Dictionary<string, T> HGetAll<T>(string key, bool compress) where T : class
        {
            return RedisCache.Currnet.HGetAll<T>(key, compress);
        }
        public void HSetAll<T>(string key, Dictionary<string, T> items, bool compress = false) where T : class
        {
            RedisCache.Currnet.HSetAll(key, items, compress);
        }    

        public bool HSet(string key, string fieldName, object value, bool compress = false)
        {
            return RedisCache.Currnet.HSet(key, fieldName, value, compress);
        }

        #endregion

        public T Get<T>(string key, bool compress = false)
        {
            return RedisCache.Currnet.Get<T>(key, compress);
        }

        public bool Set(string key, object value, TimeSpan? timeout, bool compress = false)
        {
            return RedisCache.Currnet.Set(key, value, timeout, compress);
        }

        public bool Remove(string key)
        {
            return RedisCache.Currnet.Remove(key);
        }

        public CacheServerStatus GetStatus()
        {
            CacheServerStatus result = CacheServerStatus.Offline;
            var redisStatus = RedisCache.Currnet.GetStatus();
            if (redisStatus != null)
            {
                result = new CacheServerStatus
                {
                    Online = true,
                    ServerTime = ((DateTime)redisStatus[RedisCache._SERVER_TIME]),
                    Uptime = new TimeSpan(0, 0, (int)redisStatus[RedisCache._UPTIME_IN_SECONDS]),
                    TotalBytes = (long)redisStatus[RedisCache._MAX_MEMORY],
                    UsedBytes = (long)redisStatus[RedisCache._USED_MEMORY]
                };
            }
            return result;
        }

        public string GetServer()
        {
            return ProviderFactory.Instance().GetConfig().RedisServer;
        }

        public long Increment(string key, int incrNumber = 1)
        {
            return RedisCache.Currnet.Increment(key, incrNumber);
        }

 
    }
}
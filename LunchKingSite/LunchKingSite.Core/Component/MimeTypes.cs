﻿using System;
using System.Collections.Generic;

namespace LunchKingSite.Core.Component
{
    public static class MimeTypes
    {
        private static readonly Dictionary<string, string> extensionsByMimeType = new Dictionary<string, string>();
        private static readonly Dictionary<string, string> mimeTypesByExtension = new Dictionary<string, string>();
        public const string Binary = "application/octet-stream";
        public const string PlainText = "text/plain";
        public const string Xml = "text/xml";
        public const string Html = "text/html";
        public const string XHtml = "text/xhtml+xml";
        public const string Css = "text/css";
        public const string JavaScript = "text/javascript";
        public const string Png = "image/png";
        public const string Jpeg = "image/jpeg";
        public const string Gif = "image/gif";
        public const string MHtml = "multipart/related";
        public const string FlashVideo = "video/x-flv";

        public static IEnumerable<string> All
        {
            get
            {
                return (IEnumerable<string>)extensionsByMimeType.Keys;
            }
        }

        static MimeTypes()
        {
            RegisterMimeTypeExtensions("text/plain", ".txt");
            RegisterMimeTypeExtensions("text/xml", ".xml");
            RegisterMimeTypeExtensions("text/html", ".html", ".htm");
            RegisterMimeTypeExtensions("text/xhtml+xml", ".xhtml");
            RegisterMimeTypeExtensions("multipart/related", ".mht", ".mhtml");
            RegisterMimeTypeExtensions("text/css", ".css");
            RegisterMimeTypeExtensions("text/javascript", ".js");
            RegisterMimeTypeExtensions("image/png", ".png");
            RegisterMimeTypeExtensions("image/jpeg", ".jpg", ".jpeg");
            RegisterMimeTypeExtensions("image/gif", ".gif");
            RegisterMimeTypeExtensions("video/x-flv", ".flv");
        }

        private static void RegisterMimeTypeExtensions(string mimeType, params string[] extensions)
        {
            if (extensions.Length != 0)
                extensionsByMimeType.Add(mimeType, extensions[0]);
            foreach (string key in extensions)
                mimeTypesByExtension.Add(key, mimeType);
        }

        public static string GetMimeTypeByExtension(string extension)
        {
            if (extension == null)
                throw new ArgumentNullException("extension");
            string fromWindowsRegistry;
            mimeTypesByExtension.TryGetValue(extension, out fromWindowsRegistry);
            return fromWindowsRegistry;
        }

        public static string GetExtensionByMimeType(string mimeType)
        {
            if (mimeType == null)
                throw new ArgumentNullException("mimeType");
            string str;
            extensionsByMimeType.TryGetValue(mimeType, out str);
            return str;
        }
    }
}

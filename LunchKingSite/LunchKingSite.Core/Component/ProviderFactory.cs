using System;
using Autofac;
using log4net;

namespace LunchKingSite.Core.Component
{
    public class ProviderFactory : IDisposable
    {
        private static ILog logger = LogManager.GetLogger(typeof(ProviderFactory));
        public ILifetimeScope Container { get; private set; }

        private static ProviderFactory _theOne = new ProviderFactory();

        public static ProviderFactory Instance()
        {
            return _theOne;
        }

        private ProviderFactory()
        {
        }

        public static void ReInit(ILifetimeScope container)
        {
            logger.Info("was inited.");
            _theOne.Container = container;
        }

        #region public provider get property
        public T GetProvider<T>() where T : IProvider
        {
            return Container.Resolve<T>();
        }

        /// <summary>
        /// 建議改使用 GetProvider
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public T GetDefaultProvider<T>() where T : IProvider
        {
            return Container.Resolve<T>();
        }

        public T Get<T>() where T : class
        {
            return Container.Resolve<T>();
        }
        /// <summary>
        /// 取得 runtime config (小web.config)
        /// </summary>
        /// <returns></returns>
        public ISysConfProvider GetConfig()
        {
            return GetProvider<ISysConfProvider>();
        }
        /// <summary>
        /// 類似GetDefaultProvider&lt;T&gt;但更彈性，不受限於 Provider 類別。
        /// 依serverName取得對映的Singleton
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="servicename"></param>
        /// <returns></returns>
        public T Resolve<T>(string servicename)
        {
            return Container.ResolveNamed<T>(servicename);
        }
        /// <summary>
        /// 取得Json序列化轉換器
        /// </summary>
        /// <returns></returns>
        public ISerializer GetSerializer()
        {
            return Container.Resolve<ISerializer>();
        }
        /// <summary>
        /// 建議改使用 GetSerializer()
        /// </summary>
        /// <returns></returns>
        public ISerializer GetDefaultSerializer()
        {
            return Container.Resolve<ISerializer>();
        }
        #endregion

        public void Dispose()
        {
            Container.Dispose();
        }
    }
}

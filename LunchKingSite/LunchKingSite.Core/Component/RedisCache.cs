﻿using System;
using System.Collections.Generic;
using System.Linq;
using log4net;
using Newtonsoft.Json;
using StackExchange.Redis;

namespace LunchKingSite.Core.Component
{
    public class RedisCache : IDisposable
    {
        public const string _SERVER_TIME = "server_time";
        public const string _MAX_MEMORY = "max_memory";
        public const string _USED_MEMORY = "used_memory";
        public const string _REDIS_VERSION = "redis_version";
        public const string _UPTIME_IN_SECONDS = "uptime_in_seconds";

        private ILog logger = LogManager.GetLogger(typeof(RedisCache));

        private static RedisCache _instance = new RedisCache();        
        public static RedisCache Currnet
        {
            get { return _instance; }
        }

        private RedisCache()
        {
        }

        private Lazy<ConnectionMultiplexer> lazyConnection = new Lazy<ConnectionMultiplexer>(() =>
        {
            string redisServer = ProviderFactory.Instance().GetConfig().RedisServer;
            return ConnectionMultiplexer.Connect(redisServer);
        });

        protected ConnectionMultiplexer Connection
        {
            get
            {
                return lazyConnection.Value;
            }
        }
        private IDatabase Database
        {
            get
            {
                return lazyConnection.Value.GetDatabase();
            }
        }

        #region hash

        public T HGet<T>(string key, string fieldName, bool compress = false) where T : class
        {
            if (compress)
            {
                byte[] serializedObject = Database.HashGet(key, fieldName);
                if (serializedObject == null || serializedObject.Length == 0)
                {
                    return default(T);
                }
                try
                {
                    return CacheUtil.DecompressObject<T>(serializedObject);
                }
                catch (Exception ex)
                {
                    logger.Info("DecompressObject fail(c), key=" + key, ex);
                    return default(T);
                }
            }
            else
            {
                RedisValue serializedObject;
                try
                {
                    serializedObject = Database.HashGet(key, fieldName);
                }
                catch (Exception ex)
                {
                    logger.Info("Database.StringGet fail, key=" + key, ex);
                    return default(T);
                }
                if (serializedObject.IsNull)
                {
                    return default(T);
                }
                try
                {
                    return JsonConvert.DeserializeObject<T>(serializedObject);
                }
                catch (Exception ex)
                {
                    logger.Info("DecompressObject fail, key=" + key, ex);
                    return default(T);
                }
            }
        }

        public Dictionary<string, T> HGetAll<T>(string key, bool compress) where T : class
        {
            HashEntry[] entries = Database.HashGetAll(key);
            Dictionary<string, T> result = new Dictionary<string, T>();
            foreach (var entry in entries)
            {
                if (result.ContainsKey(entry.Name))
                {
                    continue;
                }

                if (compress)
                {
                    byte[] serializedObject = entry.Value;
                    if (serializedObject == null || serializedObject.Length == 0)
                    {
                        continue;
                    }
                    try
                    {
                        result.Add(entry.Name, CacheUtil.DecompressObject<T>(serializedObject));
                    }
                    catch (Exception ex)
                    {
                        logger.Info("DecompressObject fail(c), key=" + key, ex);
                        continue;
                    }
                }
                else
                {
                    RedisValue serializedObject;
                    try
                    {
                        serializedObject = entry.Value;
                    }
                    catch (Exception ex)
                    {
                        logger.Info("Database.StringGet fail, key=" + key, ex);
                        continue;
                    }
                    if (serializedObject.IsNull)
                    {
                        continue;
                    }
                    try
                    {
                        result.Add(entry.Name, JsonConvert.DeserializeObject<T>(serializedObject));
                    }
                    catch (Exception ex)
                    {
                        logger.Info("DecompressObject fail, key=" + key, ex);
                        continue;
                    }
                }         
            }
            return result;
        }
        public void HSetAll<T>(string key, Dictionary<string, T> items, bool compress = false) where T : class
        {
            List<HashEntry> entries = new List<HashEntry>();
            foreach(var item in items)
            {
                if (compress)
                {
                    var serializedObject = CacheUtil.CompressObject(item.Value);
                    entries.Add(new HashEntry(item.Key, serializedObject));
                }
                else
                {
                    var serializedObject = JsonConvert.SerializeObject(item.Value, Formatting.None);
                    entries.Add(new HashEntry(item.Key, serializedObject));
                }
            }
            Database.HashSet(key, entries.ToArray());
        }
        public bool HSet(string key, string fieldName, object value, bool compress = false)
        {            
            bool result;
            try
            {
                if (compress)
                {
                    var serializedObject = CacheUtil.CompressObject(value);
                    result = Database.HashSet(key, fieldName, serializedObject);
                }
                else
                {
                    var serializedObject = JsonConvert.SerializeObject(value, Formatting.None);
                    result = Database.HashSet(key, fieldName, serializedObject);
                }                
            }
            catch (Exception ex)
            {
                logger.Info("Database.StringSet fail(c), key=" + key, ex);
                return false;
            }
            return result;
        }

        #endregion

        public bool Set<T>(string key, T value, TimeSpan? timeout = null, bool compress = false)
        {
            bool result;
            try
            {
                if (compress)
                {
                    var serializedObject = CacheUtil.CompressObject(value);
                    result = Database.StringSet(key, serializedObject, timeout);
                }
                else
                {
                    var serializedObject = JsonConvert.SerializeObject(value, Formatting.None);
                    result = Database.StringSet(key, serializedObject, timeout);
                }
            }
            catch (Exception ex)
            {
                logger.Info("Database.StringSet fail(c), key=" + key, ex);
                return false;
            }
            return result;
        }

        public T Get<T>(string key, bool compress = false)
        {
            if (compress)
            {
                byte[] serializedObject = Database.StringGet(key);
                if (serializedObject == null || serializedObject.Length == 0)
                {
                    return default(T);
                }
                try
                {
                    return CacheUtil.DecompressObject<T>(serializedObject);
                }
                catch (Exception ex)
                {
                    logger.Info("DecompressObject fail(c), key=" + key, ex);
                    return default(T);
                }
            }
            else
            {
                RedisValue serializedObject;
                try
                {
                    serializedObject = Database.StringGet(key);
                }
                catch (Exception ex)
                {
                    logger.Info("Database.StringGet fail, key=" + key, ex);
                    return default(T);
                }
                if (serializedObject.IsNull)
                {
                    return default(T);
                }
                try
                {
                    return JsonConvert.DeserializeObject<T>(serializedObject);
                }
                catch (Exception ex)
                {
                    logger.Info("DecompressObject fail, key=" + key, ex);
                    return default(T);
                }
            }
        }

        public bool Remove(string key)
        {
            bool result;
            try
            {
                result = Database.KeyDelete(key);
            }
            catch (Exception ex)
            {
                logger.Info("Database.KeyDelete fail, key=" + key, ex);
                return false;
            }
            return result;
        }

        public long Increment(string key, int incrNumber)
        {
            long result;
            try
            {
                result = Database.StringIncrement(key, incrNumber);
            }
            catch (Exception ex)
            {
                logger.Info("Database.Incr fail, key=" + key, ex);
                return -1;
            }
            return result;
        }

        public bool Exists(string key)
        {
            bool result;
            try
            {
                result = Database.KeyExists(key);
            }
            catch (Exception ex)
            {
                logger.Info("Database.KeyExists fail, key=" + key, ex);
                return false;
            }
            return result;
        }

        public int GetTimeout()
        {
            return Connection.TimeoutMilliseconds;
        }

        public IDictionary<string, object> GetStatus()
        {
            try
            {
                string redisServer = ProviderFactory.Instance().GetConfig().RedisServer;
                int pos = redisServer.IndexOf(',');
                if (pos != -1)
                {
                    redisServer = redisServer.Substring(0, pos);
                }
                SortedDictionary<string, object> result = new SortedDictionary<string, object>();
                var info = Connection.GetServer(redisServer).Info();
                var serverTime = Connection.GetServer(redisServer).Time();
                var config = Connection.GetServer(redisServer).ConfigGet();

                result.Add(_SERVER_TIME, serverTime.ToLocalTime());
                result.Add(_MAX_MEMORY, long.Parse(config.First(t => t.Key == "maxmemory").Value));

                foreach (IGrouping<string, KeyValuePair<string, string>> grp in info)
                {
                    foreach (KeyValuePair<string, string> item in grp)
                    {
                        if (item.Key == _REDIS_VERSION)
                        {
                            result.Add(item.Key, item.Value);
                        }
                        else if (item.Key == _USED_MEMORY)
                        {
                            result.Add(item.Key, long.Parse(item.Value));
                        }
                        else if (item.Key == _UPTIME_IN_SECONDS)
                        {
                            result.Add(item.Key, int.Parse(item.Value));
                        }
                    }
                }
                return result;
            }
            catch
            {
                return null;
            }
        }

        public void Dispose()
        {
            if (Connection != null)
            {
                Connection.Dispose();
            }
        }
    }
}

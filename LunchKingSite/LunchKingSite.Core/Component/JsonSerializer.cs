﻿using System;
using System.Collections.Generic;
using System.Reflection;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;

namespace LunchKingSite.Core
{
    public class JsonSerializer : ISerializer
    {
        #region ISerializer Members

        public string Serialize(object input, bool indented = false)
        {
            return JsonConvert.SerializeObject(input, indented ? Formatting.Indented : Formatting.None);
        }

        public string Serialize(object input, bool indented, JsonIgnoreSetting ignoreSetting)
        {
            var jsonResolver = new PropertyRenameAndIgnoreSerializerContractResolver();
            foreach (string propName in ignoreSetting.PropNames)
            {
                jsonResolver.IgnoreProperty(ignoreSetting.IgnoreType, propName);
            }

            var serializerSettings = new JsonSerializerSettings();
            serializerSettings.ContractResolver = jsonResolver;

            var json = JsonConvert.SerializeObject(input, serializerSettings);
            return json;
        }

        public T Deserialize<T>(string input)
        {
            if (string.IsNullOrEmpty(input))
            {
                return default(T);
            }
            return JsonConvert.DeserializeObject<T>(input);
        }

        public object Deserialize(string input, Type objType)
        {
            if (string.IsNullOrEmpty(input))
            {
                return null;
            }
            return JsonConvert.DeserializeObject(input, objType);
        }

        public dynamic DeserializeDynamic(string input)
        {
            if (string.IsNullOrEmpty(input))
            {
                return null;
            }
            return JObject.Parse(input);
        }

        public T TryDeserialize<T>(string input)
        {
            try
            {
                return JsonConvert.DeserializeObject<T>(input);
            }
            catch
            {

            }
            return default(T);
        }
        #endregion

        public List<T> ConvertToList<T>(JToken jToken)
        {
            List<T> items = new List<T>();

            switch (jToken.Type)
            {
                case JTokenType.Array:
                    items = jToken.ToObject<List<T>>();
                    break;
                case JTokenType.Object:
                    items.Add(jToken.ToObject<T>());
                    break;
            }

            return items;
        }
    }

    /// <summary>
    /// https://blog.rsuter.com/advanced-newtonsoft-json-dynamically-rename-or-ignore-properties-without-changing-the-serialized-class/
    /// </summary>
    public class PropertyRenameAndIgnoreSerializerContractResolver : DefaultContractResolver
    {
        private readonly Dictionary<Type, HashSet<string>> _ignores;
        private readonly Dictionary<Type, Dictionary<string, string>> _renames;

        public PropertyRenameAndIgnoreSerializerContractResolver()
        {
            _ignores = new Dictionary<Type, HashSet<string>>();
            _renames = new Dictionary<Type, Dictionary<string, string>>();
        }

        public void IgnoreProperty(Type type, params string[] jsonPropertyNames)
        {
            if (!_ignores.ContainsKey(type))
                _ignores[type] = new HashSet<string>();

            foreach (var prop in jsonPropertyNames)
                _ignores[type].Add(prop);
        }

        public void RenameProperty(Type type, string propertyName, string newJsonPropertyName)
        {
            if (!_renames.ContainsKey(type))
                _renames[type] = new Dictionary<string, string>();

            _renames[type][propertyName] = newJsonPropertyName;
        }

        protected override JsonProperty CreateProperty(MemberInfo member, MemberSerialization memberSerialization)
        {
            var property = base.CreateProperty(member, memberSerialization);

            if (IsIgnored(property.DeclaringType, property.PropertyName))
            {
                property.ShouldSerialize = i => false;
            }


            string newJsonPropertyName;
            if (IsRenamed(property.DeclaringType, property.PropertyName, out newJsonPropertyName))
            {
                property.PropertyName = newJsonPropertyName;
            }

            return property;
        }

        private bool IsIgnored(Type type, string jsonPropertyName)
        {
            if (!_ignores.ContainsKey(type))
                return false;

            return _ignores[type].Contains(jsonPropertyName);
        }

        private bool IsRenamed(Type type, string jsonPropertyName, out string newJsonPropertyName)
        {
            Dictionary<string, string> renames;

            if (!_renames.TryGetValue(type, out renames) || !renames.TryGetValue(jsonPropertyName, out newJsonPropertyName))
            {
                newJsonPropertyName = null;
                return false;
            }

            return true;
        }
    }

    public class JsonIgnoreSetting
    {
        public Type IgnoreType { get; set; }
        public List<string> PropNames { get; set; }
        public JsonIgnoreSetting(Type type, string propName)
        {
            this.IgnoreType = type;
            this.PropNames = new List<string>();
            this.PropNames.Add(propName);
        }
    }
}

using System.Web.UI.WebControls;

namespace LunchKingSite.Core.Component
{
    public abstract class AddressSearchBase : IProvider
    {
        public abstract ListItemCollection SearchBuildingList(string phrase, bool zonePrefix);
        public abstract bool UpdateIndexer();
    }
}

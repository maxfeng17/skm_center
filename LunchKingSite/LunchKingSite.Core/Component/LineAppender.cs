﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using log4net.Core;
using Newtonsoft.Json;

namespace LunchKingSite.Core.Component
{
    /// <summary>
    /// 讓 logg4net 可以發送訊息到 Line
    /// LogManager.GetLogger(LineAppender._LINE).Info("edmv2");
    /// </summary>
    public class LineAppender : log4net.Appender.AppenderSkeleton
    {
        public string UserId { get; set; }
        public string Token { get; set; }

        public const string _LINE = "Line";

        protected override void Append(LoggingEvent log)
        {
            string msg = RenderLoggingEvent(log);

            HttpSend(Token, UserId, msg).Start();
        }

        public static Task HttpSend(string token, string userId, string message)
        {
            return new Task(delegate
            {
                //string url = "https://api.line.me/v2/bot/message/push";
                string url = "https://www.uni2.tw/api/line/broadcast";

                var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
                httpWebRequest.ContinueTimeout = 10000;
                httpWebRequest.Timeout = 10000;
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";
                httpWebRequest.Headers.Add(HttpRequestHeader.Authorization,
                    "Bearer " + token);


                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    var result = new LineMessage
                    {
                        To = userId,
                        Messages = new List<MessageBase>()
                    };
                    result.Messages.Add(new TextMessage() {Text = message});

                    string json = JsonConvert.SerializeObject(result);

                    streamWriter.Write(json);
                    streamWriter.Flush();
                    streamWriter.Close();
                }

                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    streamReader.ReadToEnd();
                }
            });
        }

        public class LineMessage
        {
            [JsonProperty("to")]
            public string To { get; set; }
            [JsonProperty("messages")]
            public List<MessageBase> Messages { get; set; }
        }

        public class MessageBase
        {
            [JsonProperty("type")]
            public string Type { get; set; }
        }

        public class TextMessage : MessageBase
        {
            public TextMessage()
            {
                this.Type = "text";
            }
            [JsonProperty("text")]
            public string Text { get; set; }
        }

    }
}

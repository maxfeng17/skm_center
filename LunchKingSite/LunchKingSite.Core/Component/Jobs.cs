using System;
using System.Collections.Generic;
using System.Web.Configuration;
using System.Linq;
using LunchKingSite.Core.Configuration;
using Quartz;
using Quartz.Impl;
using LunchKingSite.DataOrm;
using LunchKingSite.Core.Component;
using System.Net;
using System.Text;

namespace LunchKingSite.Core
{
    public enum JobStatus
    {
        Disabled = 0,
        Queueing = 1,
        Running = 2,
        Paused = 3,
        Elapsed = 4,
        Misfire = 5,
    }

    /// <summary>
    /// Summary description for Jobs.
    /// </summary>
    public class Jobs
    {
        #region const
        public const string KEY_JOB_SETTING = "JS";
        private const string KEY_JOB_RUN_INFO = "RS";
        private const string DEF_JOB_GRP_NAME = "DJG";
        private const string DEF_TRG_GRP_NAME = "DTG";
        #endregion

        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(Jobs));

        //Holds single instance of Jobs
        private static readonly Jobs _instance = null;

        private static IScheduler _scheduler = null;
        private static List<JobContext> _jobList = null;
        private static QuartzJobListener _jobListener = null;

        #region .ctor
        //Create single instance of Jobs 
        static Jobs()
        {
            _instance = new Jobs();
            _scheduler = (new StdSchedulerFactory()).GetScheduler();
            _jobListener = new QuartzJobListener("DEF");
            _scheduler.AddGlobalJobListener(_jobListener);
            _jobList = new List<JobContext>();
        }

        /// <summary>
        /// Do not allow direct creation
        /// </summary>
        private Jobs()
        {
        }
        #endregion

        #region properties
        /// <summary>
        /// Returns a reference to the current instance of Jobs
        /// </summary>
        /// <returns></returns>
        public static Jobs Instance
        {
            get { return _instance; }
        }

        public List<JobContext> JobList
        {
            get { return _jobList; }
        }
        #endregion

        #region public methods
        public void Start()
        {
            Start(WebConfigurationManager.OpenWebConfiguration(WebConfigurationManager.AppSettings["sysconf-path"]));
        }

        /// <summary>
        /// Finds and Starts all jobs. Any existing jobs are shutdown first
        /// </summary>
        public void Start(System.Configuration.Configuration conf)
        {
            // gotta stop it first
            if (_scheduler.IsStarted)
                return;

            // read jobs from configuration
            StringBuilder sbUpdateLog = new StringBuilder();
            LKSiteConfigSection config = (LKSiteConfigSection) conf.GetSection(Constant.ConfigSection.SECTION_NAME);
            LoadRecurringJobs(config);
            CleanAndLoadPersistedJob(DateTime.Now); // clean run-once job that's over a week old

            _scheduler.StartDelayed(new TimeSpan(0, config.Jobs.Delay, 0)); // delay X min to wait for system startup
        }

        /// <summary>
        /// 更新job
        /// 並回傳異動log
        /// </summary>
        /// <returns></returns>
        public string Reload()
        {
            var conf = WebConfigurationManager.OpenWebConfiguration(WebConfigurationManager.AppSettings["sysconf-path"]);
            LKSiteConfigSection config = (LKSiteConfigSection)conf.GetSection(Constant.ConfigSection.SECTION_NAME);
            return ReloadRecurringJobs(config);
        }

        /// <summary>
        /// Calls displose on all current jobs and clears the job list
        /// </summary>
        public void Stop()
        {
            if (_scheduler != null)
                _scheduler.Shutdown(true);
        }

        public bool QueueJob(JobSetting js)
        {
            bool ret = true;

            if (js.Worker != null)
            {
                JobContext jc;
                JobDetail jd = _scheduler.GetJobDetail(js.Name, DEF_JOB_GRP_NAME);
                if (jd != null) // remove persisted job & re-initialize
                {
                    jc = GetJobContext(jd);
                    if (jc == null) // try to queue a job with the same name that's not ours
                        return false;

                    lock (this)
                    {
                        // clear the entry in our list
                        int idx = FindJobIndex(js.Name);
                        if (idx != -1)
                            _jobList.RemoveAt(idx);

                        _scheduler.DeleteJob(js.Name, DEF_JOB_GRP_NAME);
                        jc = new JobContext(js);
                    }
                    jc.NextRun = null; // at this time we don't know what the future execution time will be
                }
                else
                    jc = new JobContext(js);

                jd = new JobDetail(js.Name, DEF_JOB_GRP_NAME, typeof(Job), false, true, false);
                jd.JobDataMap.Put(KEY_JOB_SETTING, js);

                lock (this)
                {
                    if (js.Enabled)
                    {
                        DateTime? start = null, end = null;
                        start = js.StartTime.HasValue ? js.StartTime.Value.ToUniversalTime() : DateTime.UtcNow;

                        if (js.EndTime.HasValue) end = js.EndTime.Value.ToUniversalTime();
                        Trigger trig;
                        if (string.IsNullOrEmpty(js.CronSchedule))
                        {
                            TimeSpan ts = new TimeSpan(0, 0, js.PeriodSchedule ?? 1);
                            if (js.Recurring) // delay the start of recurring job to prevent misfire during startup
                                start = start.Value.Add(ts);
                            trig = new SimpleTrigger(js.Name, DEF_TRG_GRP_NAME, jd.Name, DEF_JOB_GRP_NAME, start.Value,
                                                     end, js.Recurring ? SimpleTrigger.RepeatIndefinitely : 0, ts);
                        }
                        else
                            trig = new CronTrigger(js.Name, DEF_TRG_GRP_NAME, jd.Name, DEF_JOB_GRP_NAME, start.Value,
                                                   end, js.CronSchedule);
                        jc.Status = JobStatus.Queueing;
                        _scheduler.ScheduleJob(jd, trig);
                        jc.NextRun = QuartzJobListener.FromUtcToLocal(trig.GetNextFireTimeUtc()); // this line must be called after ScheduleJob for next fire time to be calculated
                    }
                    else
                    {
                        jc.Status = JobStatus.Disabled;
                        _scheduler.AddJob(jd, true);
                    }

                    _jobList.Add(jc);
                }
            }
            else
                ret = false;

            return ret;
        }

        public JobContext FindJob(string jobName)
        {
            int idx;
            lock (this)
            {
                idx = FindJobIndex(jobName);
            }
            return idx != -1 ? _jobList[idx] : null;
        }

        /// <summary>
        /// Checks to see whether the specified job is currently enabled or disabled.
        /// </summary>
        /// <param name="jobName">The name of the job</param>
        /// <returns>bool</returns>
        public bool IsJobEnabled(string jobName)
        {
            JobContext jc = FindJob(jobName);
            if (jc == null)
                return false;

            return jc.Status != JobStatus.Disabled;
        }

        public bool ControlJob(string jobName, bool pauseIt)
        {
            JobContext jc = FindJob(jobName);
            if (jc == null)
                return false;

            if (pauseIt)
            {
                _scheduler.PauseJob(jc.Setting.Name, DEF_JOB_GRP_NAME);
                jc.Status = JobStatus.Paused;
            }
            else
            {
                _scheduler.ResumeJob(jc.Setting.Name, DEF_JOB_GRP_NAME);
                jc.Status = JobStatus.Queueing;
            }

            return true;
        }

        public bool FireJob(string jobName)
        {
            JobContext jc = FindJob(jobName);
            if (jc == null)
                return false;

            _scheduler.TriggerJobWithVolatileTrigger(jc.Setting.Name, DEF_JOB_GRP_NAME);
            jc.Status = JobStatus.Running;
            return true;
        }

        public bool RemoveJob(string jobName)
        {
            JobContext jc = FindJob(jobName);
            if (jc == null)
                return false;
            lock (this)
            {
                _scheduler.DeleteJob(jobName, DEF_JOB_GRP_NAME);
                _jobList.Remove(jc);
            }

            return true;
        }

        internal void OnJobCompletion(string jobName, RunHistory exeInfo, DateTime? nextRun)
        {
            JobContext jc = FindJob(jobName);
            jc.LastExecution.RunTime = exeInfo.RunTime;
            jc.LastExecution.Duration = exeInfo.Duration;
            if (nextRun == null || nextRun > exeInfo.RunTime)
            {
                jc.NextRun = nextRun;
                jc.Status = nextRun != null ? JobStatus.Queueing : JobStatus.Elapsed;
            }
            else
                jc.Status = JobStatus.Queueing;

            // save last run info 
            JobDetail jd = _scheduler.GetJobDetail(jobName, DEF_JOB_GRP_NAME);
            jd.JobDataMap[KEY_JOB_RUN_INFO] = exeInfo;
            _scheduler.AddJob(jd, true);
        }
        #endregion

        #region private methods
        private int FindJobIndex(string jobName)
        {
            return (_jobList != null) ? _jobList.FindIndex(x => x.Setting.Name == jobName) : -1;
        }

        private JobContext GetJobContext(JobDetail jd)
        {
            if (jd == null)
                return null;

            JobSetting js = (JobSetting) jd.JobDataMap[KEY_JOB_SETTING];
            if (js == null) // something is wrong? is it our job?
                return null;

            JobContext jc = new JobContext(js);
            Trigger[] triggers = _scheduler.GetTriggersOfJob(jd.Name, jd.Group);

            DateTime? nextRun = null, lastRun = null;
            // currently we don't support multiple triggers associate with 1 job so let's not worry about it now..
            if (triggers != null && triggers.Length > 0)
            {
                nextRun = QuartzJobListener.FromUtcToLocal(triggers[0].GetNextFireTimeUtc());
                lastRun = QuartzJobListener.FromUtcToLocal(triggers[0].GetPreviousFireTimeUtc());
            }
            else
            {
                RunHistory lastHistory = (RunHistory)jd.JobDataMap[KEY_JOB_RUN_INFO];
                if (lastHistory != null)
                {
                    lastRun = lastHistory.RunTime;
                    jc.LastExecution.Duration = lastHistory.Duration;
                }
            }
            jc.NextRun = nextRun;
            jc.LastExecution.RunTime = lastRun;

            return jc;
        }

        private void LoadRecurringJobs(LKSiteConfigSection csec)
        {
            if (csec.Jobs != null && csec.Jobs.Enabled)
            {
                foreach (JobSettingConfiguration jsc in csec.Jobs)
                {
                    JobSetting js = jsc.ToJobSetting();
                    if (!QueueJob(js))
                    {
                        log.WarnFormat("請檢查小web.config的設定，job name=\"{0}\" 是否有錯誤。", js.Name);
                    }
                }
            }
        }

        private string ReloadRecurringJobs(LKSiteConfigSection csec)
        {
            StringBuilder sbLog = new StringBuilder();
            if (csec.Jobs != null && csec.Jobs.Enabled)
            {
                foreach (JobSettingConfiguration jsc in csec.Jobs)
                {
                    JobSetting js = jsc.ToJobSetting();
                    
                    var job = FindJob(js.Name);
                    if (job != null)
                    {
                        if (job.Setting.ToString() != js.ToString())
                        {
                            sbLog.AppendLine();
                            sbLog.AppendFormat("調整排程 {0}, 排程時間從 {1} 改為 {2}", js.Name, job.Setting, js);
                        }
                    }
                    else
                    {
                        sbLog.AppendLine();
                        sbLog.AppendFormat("新增排程 {0}, 排程時間為", js.Name, js);
                    }
                    if (!QueueJob(js))
                    {
                        log.WarnFormat("請檢查小web.config的設定，job name=\"{0}\" 是否有錯誤。", js.Name);
                    }
                }
            }            
            return sbLog.ToString();
        }

        private void CleanAndLoadPersistedJob(DateTime timeMark)
        {
            foreach (var jName in _scheduler.GetJobNames(DEF_JOB_GRP_NAME).Except(from x in _jobList select x.Setting.Name))
            {
                JobContext jc = GetJobContext(_scheduler.GetJobDetail(jName, DEF_JOB_GRP_NAME));
                if (!jc.Setting.Recurring && jc.LastExecution.RunTime != null && jc.LastExecution.RunTime.Value < timeMark.AddDays(-7)) // remove persisted run-once job that's too old
                    _scheduler.DeleteJob(jName, DEF_JOB_GRP_NAME);
                else
                {
                    if (jc.NextRun == null)
                        jc.Status = JobStatus.Elapsed;
                    else if (jc.NextRun < timeMark.AddMinutes(2)) // let's make a 2 min buffer zone
                        jc.Status = JobStatus.Misfire;
                    else
                        jc.Status = JobStatus.Queueing;
                    _jobList.Add(jc);
                }
            }
        }
        #endregion
    }

    #region QuartzJobListener
    /// <summary>
    /// Listens to Quartz job events
    /// </summary>
    internal class QuartzJobListener : IJobListener
    {
        #region IJobListener Members
        public void JobExecutionVetoed(JobExecutionContext context) { }

        public void JobToBeExecuted(JobExecutionContext context)
        {
        }

        public void JobWasExecuted(JobExecutionContext context, JobExecutionException jobException)
        {
            // treat volatile trigger as non-recurring & fire once trigger and not to contaminate with "next run" info generated by original trigger
            RunHistory exeInfo = new RunHistory(FromUtcToLocal(context.FireTimeUtc), context.JobRunTime);            
            Jobs.Instance.OnJobCompletion(context.JobDetail.Name, exeInfo, context.Trigger.Volatile ? FromUtcToLocal(context.FireTimeUtc) : FromUtcToLocal(context.NextFireTimeUtc));
        }

        public string Name { get; private set; }

        public QuartzJobListener(string name)
        {
            Name = name;
        }
        #endregion

        public static DateTime? FromUtcToLocal(DateTime? utcTime)
        {
            return utcTime.HasValue ? utcTime.Value.Add(TimeZone.CurrentTimeZone.GetUtcOffset(DateTime.Now)) : utcTime;
        }
    }
    #endregion

    #region JobContext
    public class JobContext
    {
        public JobSetting Setting { get; set; }
        public string Schedule
        {
            get
            {
                return string.IsNullOrEmpty(Setting.CronSchedule) ? (Setting.PeriodSchedule.HasValue ? "every " + Setting.PeriodSchedule + " seconds" : "fire once") : Setting.CronSchedule;
            }
        }
        public JobStatus Status { get; set; }
        public DateTime? NextRun { get; set; }
        public RunHistory LastExecution { get; private set; }

        public JobContext(JobSetting js)
        {
            if (js == null)
                throw new NullReferenceException("js cannot be null");
            Setting = js;
            Status = js.Enabled ? JobStatus.Paused : JobStatus.Disabled;
            LastExecution = new RunHistory();
        }

        public JobContext(string name, Type worker, string description)
        {
            Setting = new JobSetting(name, worker, description);
            LastExecution = new RunHistory();
        }
    }

    [Serializable]
    public class RunHistory
    {
        public DateTime? RunTime { get; set; }
        public TimeSpan? Duration
        {
            get { return _durationTicks >= 0 ? new TimeSpan?(new TimeSpan(_durationTicks)) : null; }
            set { _durationTicks = value.HasValue ? value.Value.Ticks : -1; }
        }
        private long _durationTicks = -1; // because TimeSpan can't be serialized, therefore we need to have this workaround

        public RunHistory() {}
        public RunHistory(DateTime? runTime, TimeSpan? duration)
        {
            RunTime = runTime;
            Duration = duration;
        }
    }
    #endregion
}
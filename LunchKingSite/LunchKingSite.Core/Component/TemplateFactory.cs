﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Component.Template;
using TemplateMaschine;

namespace LunchKingSite.Core.Component
{
    public abstract class TemplateBase
    {
        protected delegate void PrepareItemCallback();

        protected TemplateMaschine.Template _template = null;
        protected PrepareItemCallback _callback = null;

        public TemplateBase() : this(null, new Dictionary<string, object>()) { }

        public TemplateBase(TemplateMaschine.Template t) : this(t, new Dictionary<string, object>()) { }

        public TemplateBase(TemplateMaschine.Template t, Dictionary<string, object> it)
        {
            _template = t;
            _items = it;
            InitLayout();
            Initialize();
        }

        protected virtual void InitLayout()
        {
            ISysConfProvider config = ProviderFactory.Instance().GetConfig();

            _items["header"] = @"
    <table width='770' cellpadding='0' cellspacing='0' align='center' style='background:#F3F3EE; color:#333;'>
    
    <tr>
        <td width='20' height='80'></td>
        <td width='140' height='80'>
            <a href='https://www.17life.com' target='_blank'><img src='https://www.17life.com/Themes/default/images/17Life/EDM/EDMLOGO.png' width='132' height='80' alt='' border='0' /></a>
        </td>
        <td width='480' height='80' style='font-size:26px; font-weight:bold;'></td>
        <td width='110' height='80' style='text-align: right;'>
        <!-- JJ 2018.08.28 -->
            <ul style='list-style: none;'>
                <li>
                    <a href='https://line.me/R/ti/p/%40hlx4093o'>
                        <img src='https://www.17life.com/Themes/default/images/17Life/EDM/Line_beFriends.png' style='height: 2.4rem' alt='Add To Line APP'>
                    </a>
                
                </li>
                <li>
                    <p style='line-height:1rem;margin-top: .2rem;text-align: center;font-weight: 400;color:#888;'>超值優惠天天有</p>
                </li>
            </ul>
        </td>
        <td width='20' height='80'></td>
    </tr>
    </table>
";

            _items["footer"] = @"
                    <table width='770' cellpadding='0' cellspacing='0' align='center' style='background: #333;
                        color: #FFF;'>
                        <tr>
                            <td width='25'>
                            </td>
                            <td height='80'>
                                <br />
                                <a href='https://www.17life.com/user/service.aspx' target='_blank' style='color: #FFF'>
                                    客服中心</a> | <a href='https://www.17life.com/Ppon/WhiteListGuide.aspx' target='_blank'
                                        style='color: #FFF'>加入白名單</a> | 服務時間：平日9:00~18:00<br />
                                17Life 康太數位整合股份有限公司 版權所有 轉載必究 © 2012
                            </td>
                            <td>
                                <a href='https://itunes.apple.com/tw/app/id543439591?mt=8'>
                                    <img src='https://www.17life.com/images/17P/20150402-pcp-promo/images/ios-icon.png' alt='到蘋果 App Store' style='height:40px'>
                                </a>
                            </td>
                            <td>
                                <a href='https://play.google.com/store/apps/details?id=com.uranus.e7plife&feature=search_result#?t=W251bGwsMSwxLDEsImNvbS51cmFudXMuZTdwbGlmZSJd'>
                                    <img src='https://www.17life.com/images/17P/20150402-pcp-promo/images/android-icon.png' alt='到安卓 Google Play' style='height:40px'>
                                </a>
                            </td>
                        </tr>
                    </table>
";
        }

        protected virtual void Initialize()
        {
        
        }

        protected Dictionary<string, object> _items = null;
        public Dictionary<string, object> Items
        {
            get
            {
                return _items;
            }
            set
            {
                _items = value;
            }
        }

        internal TemplateMaschine.Template Template
        {
            get
            {
                return _template;
            }
            set
            {
                _template = value;
            }
        }

        public override string ToString()
        {
            string output = null;

            if (_callback != null)
            {
                _callback();
            }

            lock (_template)
            {
                output = _template.Generate(_items);
            }
            return output;
        }
    }
}

namespace LunchKingSite.Core
{
    public class TemplateFactory
    {
        protected struct TemplateInfo
        {
            public string Path;
            public TemplateMaschine.Template Template;

            public TemplateInfo(string path)
            {
                this.Path = path;
                this.Template = new Template(path);
            }
        }

        #region private members

        private static TemplateFactory _theOne;
        private string _tempDir;
        protected Dictionary<Type, TemplateInfo> _tempDic;

        #endregion

        #region .ctor

        static TemplateFactory()
        {
            _theOne = new TemplateFactory();
        }

        /// <summary>
        /// Returns a reference to the current template factory instance
        /// </summary>
        /// <returns></returns>
        public static TemplateFactory Instance()
        {
            return _theOne;
        }

        private TemplateFactory()
        {
        }

        #endregion

        private bool _debug = true;
        public bool DebugMode
        {
            get
            {
                return _debug;
            }
            set
            {
                _debug = value;
            }
        }

        public string DirectoryPath
        {
            get
            {
                return _tempDir;
            }
            set
            {
                _tempDir = value;
            }
        }

        public bool Init(string dirPath)
        {
            return Init(dirPath, false);
        }

        public bool Init(string dirPath, bool debugMode)
        {
            _tempDir = dirPath;
            _tempDic = new Dictionary<Type, TemplateInfo>();
            var templates = Assembly.GetExecutingAssembly().GetExportedTypes().Where(t => t.IsSubclassOf(typeof(TemplateBase)));
            foreach (var t in templates)
            {
                var attrs = t.GetCustomAttributes(typeof(TemplateFileAttribute), false);
                TemplateFileAttribute tfAttr = attrs.Length > 0 ? (TemplateFileAttribute)attrs[0] : null;
                // use template file attribute to specify file name, if attribute does not exist, use [class name] + .txt as convention
                _tempDic.Add(t, new TemplateInfo(Path.Combine(_tempDir, tfAttr != null ? tfAttr.FileName : t.Name.ToLowerInvariant() + ".txt")));
            }

            _debug = debugMode;
            return true;
        }

        public T GetTemplate<T>() where T : TemplateBase, new()
        {
            T x = null;
            if (_tempDic.ContainsKey(typeof(T)))
            {
                x = new T();
                if (_debug)
                {
                    x.Template = new Template(_tempDic[typeof(T)].Path, Path.Combine(_tempDir, "generated.cs"));
                }
                else
                {
                    x.Template = _tempDic[typeof(T)].Template;
                }
            }

            return x;
        }
    }
}

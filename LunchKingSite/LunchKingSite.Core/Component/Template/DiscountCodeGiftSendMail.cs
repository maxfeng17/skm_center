﻿using System;
using System.Linq;
using LunchKingSite.DataOrm;
using System.Collections.Generic;

namespace LunchKingSite.Core.Component.Template
{
    public class DiscountCodeGiftSendMail : TemplateBase
    {
        public ISysConfProvider ServerConfig { get; set; }
        ISysConfProvider cp = ProviderFactory.Instance().GetConfig();
        public DiscountCodeGiftSendMail()
            : base()
        {
        }

        protected string _memberName = null;
        public string MemberName
        {
            get { return _memberName; }
            set { _memberName = value; }
        }

        protected string _imgUrl = null;
        public string imgUrl
        {
            get { return _imgUrl; }
            set { _imgUrl = value; }
        }

        protected Dictionary<int, int> _discountCodeList = null;
        public Dictionary<int, int> DiscountCodeList
        {
            get { return _discountCodeList; }
            set { _discountCodeList = value; }
        }

        protected List<IViewPponDeal> _promoDeals = null;
        public List<IViewPponDeal> PromoDeals
        {
            get { return _promoDeals; }
            set { _promoDeals = value; }
        }

        protected override void Initialize()
        {
            _callback = new PrepareItemCallback(PrepareItem);
        }

        protected virtual void PrepareItem()
        {
            _items["site_url"] = cp.SiteUrl;
            _items["site_service_url"] = cp.SiteServiceUrl;
            _items["member_name"] = _memberName;
            _items["img_url"] = _imgUrl;

            PreparePromoDealsInfo();
            PrepareDiscountCodeInfo();
        }

        protected void PreparePromoDealsInfo()
        {
            // 先建立預設值
            int dealsCount = ServerConfig.PromoMailDealsCount;
            for (int i = 0; i < dealsCount; i++)
            {
                _items["deal" + (i + 1) + "url"] = string.Empty;
                _items["deal" + (i + 1) + "pic"] = string.Empty;
                _items["deal" + (i + 1) + "title"] = string.Empty;
                _items["deal" + (i + 1) + "price"] = string.Empty;
            }

            // 依序設定檔次資料
            for (int i = 0; i < _promoDeals.Count; i++)
            {
                _items["deal" + (i + 1) + "url"] = ServerConfig.SiteUrl + "/deal/" + _promoDeals[i].BusinessHourGuid.ToString() + "?rsrc=17_EDMpay";
                _items["deal" + (i + 1) + "pic"] = Helper.GetImgUrl(_promoDeals[i].EventImagePath);
                _items["deal" + (i + 1) + "title"] = (_promoDeals[i].ItemName.Length > 31) ? (_promoDeals[i].ItemName.Substring(0, 31) + "...") : _promoDeals[i].ItemName;
                _items["deal" + (i + 1) + "price"] = Convert.ToInt32(_promoDeals[i].ItemPrice).ToString("D") + (Helper.IsFlagSet(_promoDeals[i].BusinessHourStatus, BusinessHourStatus.ComboDealMain) ? "起" : string.Empty);
            }

            // 必須有檔次數才要顯示
            _items["show_promo"] = _promoDeals.Count() > 0;
        }

        protected void PrepareDiscountCodeInfo()
        {
            _items["discount_total_amount"] = DiscountCodeList.Select(x => x.Key * x.Value).Sum().ToString();
            _items["discount_desc"] = string.Join("、", DiscountCodeList.Select(x => string.Format("{0}元x{1}張", x.Key, x.Value)));
            _items["discount_notice"] = string.Join("；", DiscountCodeList.Select(x => string.Format("{0}元折價券，單筆消費滿{1}可折抵", x.Key, x.Key * 10)));
        }
    }
}
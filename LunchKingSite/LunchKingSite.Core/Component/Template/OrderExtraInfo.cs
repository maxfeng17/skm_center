﻿
namespace LunchKingSite.Core.Component.Template
{
    public class OrderExtraInfo : TemplateBase
    {
        protected override void Initialize()
        {
            _items.Add("name", null);
            _items.Add("mobile", null);
            _items.Add("dayphone", null);
            _items.Add("nightphone", null);
            _items.Add("address", null);
            _items.Add("email", null);
            _items.Add("donate_receipt", true);
            _items.Add("paper_receipt", false);
            _items.Add("company_id", null);
        }

        public string Name
        {
            get { return (string)_items["name"]; }
            set { _items["name"] = value; }
        }

        public string Mobile
        {
            get { return (string)_items["mobile"]; }
            set { _items["mobile"] = value; }
        }

        public string DayPhone
        {
            get { return (string)_items["dayphone"]; }
            set { _items["dayphone"] = value; }
        }

        public string NightPhone
        {
            get { return (string)_items["nightphone"]; }
            set { _items["nightphone"] = value; }
        }

        public string Address
        {
            get { return (string)_items["address"]; }
            set { _items["address"] = value; }
        }

        public string Email
        {
            get { return (string)_items["email"]; }
            set { _items["email"] = value; }
        }

        public bool DonateReceipt
        {
            get { return (bool)_items["donate_receipt"]; }
            set { _items["donate_receipt"] = value; }
        }

        public bool PaperReceipt
        {
            get { return (bool)_items["paper_receipt"]; }
            set { _items["paper_receipt"] = value; }
        }

        public string CompanyId
        {
            get { return (string)_items["company_id"]; }
            set { _items["company_id"] = value; }
        }
    }
}

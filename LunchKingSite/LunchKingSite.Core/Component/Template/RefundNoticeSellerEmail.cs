﻿using LunchKingSite.DataOrm;

namespace LunchKingSite.Core.Component.Template
{
    public class RefundNoticeSellerEmail : TemplateBase
    {
        public ISysConfProvider ServerConfig { get; set; }
        ISysConfProvider cp = ProviderFactory.Instance().GetConfig();

        public RefundNoticeSellerEmail()
            : base()
        {
        }

        protected override void Initialize()
        {
            _callback = new PrepareItemCallback(PrepareItem);
        }

        protected virtual void PrepareItem()
        {
            _items["site_url"] = cp.SiteUrl;
            _items["site_service_url"] = cp.SiteServiceUrl;
            _items["seller_name"] = seller_name;
            _items["deal_name"] = deal_name;
            _items["return_count"] = return_count;
        }

        protected string seller_name = null;
        public string Seller_Name
        {
            get { return seller_name; }
            set { seller_name = value; }
        }

        protected string deal_name = null;
        public string Deal_Name
        {
            get { return deal_name; }
            set { deal_name = value; }
        }

        protected string return_count = null;
        public string Return_Count
        {
            get { return return_count; }
            set { return_count = value; }
        }
    }
}
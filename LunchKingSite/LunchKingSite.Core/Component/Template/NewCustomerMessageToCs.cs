﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.Core.Component.Template
{
    [TemplateFile("NewCustomerMessageToCs.txt")]
    public class NewCustomerMessageToCs : TemplateBase
    {
        public NewCustomerMessageToCs() : base() { }

        protected override void Initialize()
        {
            _items.Add("service_no", null);
            _items.Add("service_no_url", null);
            _items.Add("service_name", null);
            
            _items.Add("site_url", null);
            _items.Add("https_site_url", null);
            _items.Add("site_service_url", null);

            _items.Add("outside_logs", new List<OutsideLog>());
        }
        
        /// <summary>
        /// 對話訊息
        /// </summary>
        public List<OutsideLog> OutsideLogs
        {
            get { return (List<OutsideLog>)_items["outside_logs"]; }
            set { _items["outside_logs"] = value; }
        }

        /// <summary>
        /// 案件編號
        /// </summary>
        public string ServiceNo
        {
            get { return (string)_items["service_no"]; }
            set { _items["service_no"] = value; }
        }
        /// <summary>
        /// 案件連結
        /// </summary>
        public string ServiceNoUrl
        {
            get { return (string)_items["service_no_url"]; }
            set { _items["service_no_url"] = value; }
        }
        
        /// <summary>
        /// 負責客服
        /// </summary>
        public string ServiceName
        {
            get { return (string)_items["service_name"]; }
            set { _items["service_name"] = value; }
        }
        

        public string SiteUrl
        {
            get { return (string)_items["site_url"]; }
            set { _items["site_url"] = value; }
        }
        public string HttpsSiteUrl
        {
            get { return (string)_items["https_site_url"]; }
            set { _items["https_site_url"] = value; }
        }
        public string SiteServiceUrl
        {
            get { return (string)_items["site_service_url"]; }
            set { _items["site_service_url"] = value; }
        }

        /// <summary>
        /// 轉單訊息
        /// </summary>
        public class OutsideLog
        {
            public string CustomerName { get; set; }
            public string CreateName { get; set; }
            public string CreateTime { get; set; }
            public string Content { get; set; }
            public int? Method { get; set; }
        }
    }
}

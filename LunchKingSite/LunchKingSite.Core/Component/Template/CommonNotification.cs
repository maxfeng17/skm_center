﻿namespace LunchKingSite.Core.Component.Template
{
    [TemplateFile("CommonNotification.txt")]
    public class CommonNotification : TemplateBase
    {
        public CommonNotification() : base() { }

        protected override void Initialize()
        {
            _items.Add("model", new CommonNotificationModel());
            _items.Add("site_url", string.Empty);
            _items.Add("https_site_url", string.Empty);
            _items.Add("site_service_url", string.Empty);
        }

        /// <summary>
        /// 一般通知信內容
        /// </summary>
        public CommonNotificationModel Model
        {
            get { return (CommonNotificationModel)_items["model"]; }
            set { _items["model"] = value; }
        }
        public string SiteUrl
        {
            get { return (string)_items["site_url"]; }
            set { _items["site_url"] = value; }
        }
        public string HttpsSiteUrl
        {
            get { return (string)_items["https_site_url"]; }
            set { _items["https_site_url"] = value; }
        }
        public string SiteServiceUrl
        {
            get { return (string)_items["site_service_url"]; }
            set { _items["site_service_url"] = value; }
        }

        /// <summary>
        /// 一般通知信內容
        /// </summary>
        public class CommonNotificationModel
        {
            /// <summary>
            /// 信件主旨
            /// </summary>
            public string Subject { get; set; }
            /// <summary>
            /// 信件開頭問候語
            /// </summary>
            public string Greeting { get; set; }
            /// <summary>
            /// 信件內文
            /// </summary>
            public string Message { get; set; }
        }
    }
}

﻿using System;
using System.Linq;
using LunchKingSite.DataOrm;

namespace LunchKingSite.Core.Component.Template
{
    public class HiDealSellerDeliverExceedInform : TemplateBase
    {
        public ISysConfProvider ServerConfig { get; set; }
        ISysConfProvider cp = ProviderFactory.Instance().GetConfig();

        public HiDealSellerDeliverExceedInform()
            : base()
        {

        }

        protected string _dealName = null;
        public string DealName
        {
            get { return _dealName; }
            set { _dealName = value; }
        }

        protected HiDealProduct _theProduct = null;
        public HiDealProduct TheProduct
        {
            get { return _theProduct; }
            set { _theProduct = value; }
        }

        public string MailSubject
        {
            get { return "超過出貨期限未完成出貨通知"; }
        }

        protected override void Initialize()
        {
            _callback = new PrepareItemCallback(PrepareItem);
        }

        protected virtual void PrepareItem()
        {
            _items["site_url"] = cp.SiteUrl;
            if (_dealName != null)
            {
                _items["deal_name"] = _dealName != null ? _dealName + " - " : string.Empty;
                _items["item_name"] = _theProduct.Name;
                _items["dateS"] = _theProduct.UseStartTime != null ? _theProduct.UseStartTime.Value.ToString("yyyy/MM/dd") : string.Empty;
                _items["dateE"] = _theProduct.UseEndTime != null ? Convert.ToDateTime(_theProduct.UseEndTime.Value).ToString("yyyy/MM/dd") : string.Empty;
                _items["mail_subject"] = this.MailSubject;
            }
        }
    }
}

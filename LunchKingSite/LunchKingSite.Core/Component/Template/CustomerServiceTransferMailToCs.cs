﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.Core.Component.Template
{
    [TemplateFile("CustomerServiceTransferMailToCS.txt")]
    public class CustomerServiceTransferMailToCs : TemplateBase
    {
        public CustomerServiceTransferMailToCs() : base() { }

        protected override void Initialize()
        {
            _items.Add("service_no", null);
            _items.Add("service_no_url", null);
            _items.Add("order_id", null);
            _items.Add("item_name", null);
            _items.Add("prioritydescription", null);
            _items.Add("problem_description", null);
            _items.Add("sub_problem_description", null);
            _items.Add("service_name", null);
            _items.Add("seller_name", null);
            _items.Add("content", null);
            _items.Add("site_url", null);
            _items.Add("https_site_url", null);
            _items.Add("site_service_url", null);
        }

        /// <summary>
        /// 案件編號
        /// </summary>
        public string ServiceNo
        {
            get { return (string)_items["service_no"]; }
            set { _items["service_no"] = value; }
        }
        /// <summary>
        /// 案件連結
        /// </summary>
        public string ServiceNoUrl
        {
            get { return (string)_items["service_no_url"]; }
            set { _items["service_no_url"] = value; }
        }
        /// <summary>
        /// 訂單編號
        /// </summary>
        public string OrderId
        {
            get { return (string)_items["order_id"]; }
            set { _items["order_id"] = value; }
        }
        /// <summary>
        /// 商品資訊
        /// </summary>
        public string ItemName
        {
            get { return (string)_items["item_name"]; }
            set { _items["item_name"] = value; }
        }
        /// <summary>
        /// 問題等級
        /// </summary>
        public string PriorityDescription
        {
            get { return (string)_items["prioritydescription"]; }
            set { _items["prioritydescription"] = value; }
        }
        /// <summary>
        /// 問題分類
        /// </summary>
        public string ProblemDescription
        {
            get { return (string)_items["problem_description"]; }
            set { _items["problem_description"] = value; }
        }
        /// <summary>
        /// 問題主因
        /// </summary>
        public string SubProblemDescription
        {
            get { return (string)_items["sub_problem_description"]; }
            set { _items["sub_problem_description"] = value; }
        }
        /// <summary>
        /// 負責客服
        /// </summary>
        public string ServiceName
        {
            get { return (string)_items["service_name"]; }
            set { _items["service_name"] = value; }
        }
        /// <summary>
        /// 商家名稱
        /// </summary>
        public string SellerName
        {
            get { return (string)_items["seller_name"]; }
            set { _items["seller_name"] = value; }
        }
        /// <summary>
        /// 轉單訊息
        /// </summary>
        public string Content
        {
            get { return (string)_items["content"]; }
            set { _items["content"] = value; }
        }

        public string SiteUrl
        {
            get { return (string)_items["site_url"]; }
            set { _items["site_url"] = value; }
        }
        public string HttpsSiteUrl
        {
            get { return (string)_items["https_site_url"]; }
            set { _items["https_site_url"] = value; }
        }
        public string SiteServiceUrl
        {
            get { return (string)_items["site_service_url"]; }
            set { _items["site_service_url"] = value; }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using LunchKingSite.Core.Models;
using LunchKingSite.DataOrm;
using LunchKingSite.I18N;


namespace LunchKingSite.Core.Component.Template
{
    public class SubscriptionPayeasy : TemplateBase
    {
        public SubscriptionPayeasy() : base() { }

        protected override void Initialize()
        {
            _callback = new PrepareItemCallback(PrepareItem);
        }

        protected virtual void PrepareItem()
        {
        }
    }
}

﻿using System.Collections.Generic;
using LunchKingSite.DataOrm;
using System;

namespace LunchKingSite.Core.Component.Template
{
    public class WmsReturnOrderNotificationToSeller : TemplateBase
    {
        ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        public WmsReturnOrderNotificationToSeller() : base() { }

        protected override void Initialize()
        {
            _items["seller_name"] = string.Empty;
            _items["site_url"] = string.Empty;
            _items["https_site_url"] = string.Empty;
            _items["site_service_url"] = string.Empty;
            _items.Add("return_order", new WmsReturnOrderListModel());
        }

        public string SellerName
        {
            get { return (string)_items["seller_name"]; }
            set { _items["seller_name"] = value; }
        }

        public string SiteUrl
        {
            get { return (string)_items["site_url"]; }
            set { _items["site_url"] = value; }
        }
        public string HttpSiteUrl
        {
            get { return (string)_items["https_site_url"]; }
            set { _items["https_site_url"] = value; }
        }

        public string SiteServiceUrl
        {
            get { return (string)_items["site_service_url"]; }
            set { _items["site_service_url"] = value; }
        }

        public WmsReturnOrderListModel ReturnOrder
        {
            get { return (WmsReturnOrderListModel)_items["return_order"]; }
            set { _items["return_order"] = value; }
        }

        public class WmsReturnOrderListModel
        {
            public WmsReturnOrderListModel()
            {
                ItemList = new List<WmsReturnOrderModel>();
            }

            public List<WmsReturnOrderModel> ItemList { get; set; }
        }

        public class WmsReturnOrderModel
        {
            /// <summary>
            /// 退貨申請日
            /// </summary>
            public string ReturnDate { get; set; }
            /// <summary>
            /// 訂單編號
            /// </summary>
            public string OrderId { get; set; }
            /// <summary>
            /// 檔號
            /// </summary>
            public string UniqueId { get; set; }
            /// <summary>
            /// 收件者姓名
            /// </summary>
            public string BuyerName { get; set; }
            /// <summary>
            /// 商品名稱
            /// </summary>
            public string ItemName { get; set; }
            /// <summary>
            /// 退貨規格數量
            /// </summary>
            public string ReturnSpec { get; set; }
            /// <summary>
            /// 退貨原因與備註
            /// </summary>
            public string ReturnReason { get; set; }
        }
    }


}

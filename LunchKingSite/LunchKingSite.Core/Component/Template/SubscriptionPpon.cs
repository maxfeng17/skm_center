﻿namespace LunchKingSite.Core.Component.Template
{
    public class SubscriptionPpon : TemplateBase
    {
        private ISysConfProvider cp = ProviderFactory.Instance().GetConfig();

        public SubscriptionPpon()
            : base()
        {
        }

        protected override void Initialize()
        {
            _callback = new PrepareItemCallback(PrepareItem);
        }

        protected virtual void PrepareItem()
        {
            _items["site_url"] = cp.SiteUrl;
            _items["site_service_url"] = cp.SiteServiceUrl;
        }

        protected void PrepareDealInfo()
        {
        }

        protected void PrepareGetCity()
        {
        }
    }
}
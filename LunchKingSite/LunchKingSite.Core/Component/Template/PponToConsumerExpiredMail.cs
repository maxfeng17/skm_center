﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using LunchKingSite.Core.Models;
using LunchKingSite.DataOrm;

namespace LunchKingSite.Core.Component.Template
{
    public class PponToConsumerExpiredMail : TemplateBase
    {
        public ISysConfProvider ServerConfig { get; set; }
        ISysConfProvider cp = ProviderFactory.Instance().GetConfig();

        public PponToConsumerExpiredMail()
            : base()
        {
        }

        public string TheMemberName { get; set; }

        public string ItemName { get; set; }

        public ViewPponDeal _theDeal = null;
        public ViewPponDeal TheDeal
        {
            get { return _theDeal; }
            set { _theDeal = value; }
        }

        protected List<IViewPponDeal> _promoDeals = null;
        public List<IViewPponDeal> PromoDeals
        {
            get { return _promoDeals; }
            set { _promoDeals = value; }
        }
        public DateTime EndTime { get; set; }

        protected override void Initialize()
        {
            _callback = new PrepareItemCallback(PrepareItem);
        }

        protected virtual void PrepareItem()
        {
            _items["site_url"] =cp.SiteUrl;
            _items["site_service_url"] = cp.SiteServiceUrl;
            PrepareDealInfo();
            PreparePromoDealsInfo();
        }

        protected void PreparePromoDealsInfo()
        {
            // 先建立預設值
            int dealsCount = ServerConfig.PromoMailDealsCount;
            for (int i = 0; i < dealsCount; i++)
            {
                _items["deal" + (i + 1) + "url"] = string.Empty;
                _items["deal" + (i + 1) + "pic"] = string.Empty;
                _items["deal" + (i + 1) + "title"] = string.Empty;
                _items["deal" + (i + 1) + "price"] = string.Empty;
            }

            // 依序設定檔次資料
            for (int i = 0; i < _promoDeals.Count; i++)
            {
                _items["deal" + (i + 1) + "url"] = ServerConfig.SiteUrl + "/deal/" + _promoDeals[i].BusinessHourGuid.ToString() + "?rsrc=17_EDMpay";
                _items["deal" + (i + 1) + "pic"] = Helper.GetImgUrl(_promoDeals[i].EventImagePath);
                _items["deal" + (i + 1) + "title"] = (_promoDeals[i].ItemName.Length > 31) ? (_promoDeals[i].ItemName.Substring(0, 31) + "...") : _promoDeals[i].ItemName;
                _items["deal" + (i + 1) + "price"] = Convert.ToInt32(_promoDeals[i].ItemPrice).ToString("D") + (Helper.IsFlagSet(_promoDeals[i].BusinessHourStatus, BusinessHourStatus.ComboDealMain) ? "起" : string.Empty);
            }

            // 必須有檔次數才要顯示
            _items["show_promo"] = _promoDeals.Count() > 0;
        }

        protected void PrepareDealInfo()
        {
            if (_theDeal != null)
            {
                _items["member_name"] = TheMemberName;
                _items["item_name"] = ItemName;
                _items["deliver_date_end"] = EndTime.ToString("yyyy/MM/dd");
            }
        }

        private string GetImgUrl(string path)
        {
            return ((string.IsNullOrWhiteSpace(path)) ? Helper.GetMediaPathsFromRawData(ServerConfig.EDMDefaultImage, MediaType.PponDealPhoto, ServerConfig.MediaBaseUrl).DefaultIfEmpty(string.Empty).First() : Helper.GetMediaPathsFromRawData(path, MediaType.PponDealPhoto, ServerConfig.MediaBaseUrl).DefaultIfEmpty(string.Empty).First());
        }
    }
}

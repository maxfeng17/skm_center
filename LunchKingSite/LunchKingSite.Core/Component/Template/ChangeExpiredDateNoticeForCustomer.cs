﻿using LunchKingSite.DataOrm;
using System;
using System.Linq;

namespace LunchKingSite.Core.Component.Template
{
    public class ChangeExpiredDateNoticeForCustomer : TemplateBase
    {
        public ChangeExpiredDateNoticeForCustomer()
        : base()
        {
        }

        protected string _content = null;
        public string Content
        {
            get
            {
                return _content;
            }
            set
            {
                _content = value;
            }
        }

        protected string _memberName = null;
        public string MemberName
        {
            get
            {
                return _memberName;
            }
            set
            {
                _memberName = value;
            }
        }

        protected DateTime _closeDate;
        public DateTime CloseDate 
        {
            get { return _closeDate; }
            set { _closeDate = value; }
        }

        protected string _site_url;
        public string SiteUrl
        {
            get
            {
                return this._site_url;
            }
            set
            {
                this._site_url = value;
            }
        }

        protected string _site_service_url;
        public string SiteServiceUrl
        {
            get
            {
                return this._site_service_url;
            }
            set
            {
                this._site_service_url = value;
            }
        }

        protected override void Initialize()
        {
            _callback = new PrepareItemCallback(PrepareItem);
        }

        protected virtual void PrepareItem()
        {
            _items["content"] = Content;
            _items["member_name"] = MemberName;
            _items["close_date"] = CloseDate.ToShortDateString();
            _items["site_url"] = _site_url;
            _items["site_service_url"] = _site_service_url;
        }
    }
}

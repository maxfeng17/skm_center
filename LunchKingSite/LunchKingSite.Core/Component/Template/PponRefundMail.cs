﻿using LunchKingSite.DataOrm;

namespace LunchKingSite.Core.Component.Template
{
    public class PponRefundMail : TemplateBase
    {
        private ISysConfProvider cp = ProviderFactory.Instance().GetConfig();

        public PponRefundMail()
            : base()
        {
        }

        protected string _orderName = string.Empty;

        public string OrderName
        {
            get
            {
                return _orderName;
            }
            set
            {
                _orderName = value;
            }
        }

        protected double _withdrawal_promotion_value = 0;

        public double WithdrawalPromotionValue
        {
            get
            {
                return _withdrawal_promotion_value;
            }
            set
            {
                _withdrawal_promotion_value = value;
            }
        }

        protected ViewPponDeal _theDeal = null;

        public ViewPponDeal TheDeal
        {
            get
            {
                return _theDeal;
            }
            set
            {
                _theDeal = value;
            }
        }

        protected ViewPponOrderDetail _theOrderDetail = null;

        public ViewPponOrderDetail TheOrderDetail
        {
            get
            {
                return _theOrderDetail;
            }
            set
            {
                _theOrderDetail = value;
            }
        }

        protected override void Initialize()
        {
            _callback = new PrepareItemCallback(PrepareItem);
        }

        protected virtual void PrepareItem()
        {
            _items["site_url"] = cp.SiteUrl;
            _items["withdrawal_promotion_value"] = _withdrawal_promotion_value.ToString();

            PrepareDealInfo();
            PrepareOrderDetailInfo();
        }

        protected void PrepareDealInfo()
        {
            if (_theDeal != null)
            {
                _items["event_name"] = _theDeal.EventName;
            }
        }

        protected void PrepareOrderDetailInfo()
        {
            if (_theOrderDetail != null)
            {
                _items["member_name"] = _orderName;
                _items["item_quantity"] = _theOrderDetail.ItemQuantity;
            }
        }
    }
}
﻿using LunchKingSite.DataOrm;
using System;
using System.Linq;
using System.Collections.Generic;

namespace LunchKingSite.Core.Component.Template
{
    public class PponAuthorizedMail : TemplateBase
    {
        public ISysConfProvider ServerConfig { get; set; }

        private ISysConfProvider cp = ProviderFactory.Instance().GetConfig();

        public PponAuthorizedMail()
            : base()
        {
        }

        private decimal bhOrderMinimum;
        private DateTime validTime;
        private DepartmentTypes department;

        protected MailContentType _mailType;

        public MailContentType MailType
        {
            get
            {
                return _mailType;
            }
            set
            {
                _mailType = value;
            }
        }

        protected decimal _freight = 0;

        public decimal Freight
        {
            get
            {
                return _freight;
            }
            set
            {
                _freight = value;
            }
        }

        protected bool _isRefundCreditcard = true;

        public bool IsRefundCreditcard
        {
            get
            {
                return _isRefundCreditcard;
            }
            set
            {
                _isRefundCreditcard = value;
            }
        }

        protected ViewPponDeal _theDeal = null;

        public ViewPponDeal TheDeal
        {
            get
            {
                return _theDeal;
            }
            set
            {
                _theDeal = value;
            }
        }

        protected List<IViewPponDeal> _promoDeals = null;
        public List<IViewPponDeal> PromoDeals
        {
            get { return _promoDeals; }
            set { _promoDeals = value; }
        }

        protected ViewPponOrderDetailCollection _theOrderDetailCol = null;

        public ViewPponOrderDetailCollection TheOrderDetailCol
        {
            get
            {
                return _theOrderDetailCol;
            }
            set
            {
                _theOrderDetailCol = value;
            }
        }

        protected override void Initialize()
        {
            _callback = new PrepareItemCallback(PrepareItem);
        }

        protected virtual void PrepareItem()
        {
            PrepareDealInfo();
            PrepareOrderDetailInfo();
            PreparePromoDealsInfo();

            _items["site_url"] = cp.SiteUrl;
            _items["freight"] = _items["statement"] = _items["warning"] = string.Empty;
            _items["authorize"] = string.Empty;
            string cpa = string.Empty;
            if (_mailType == MailContentType.Authorized)
            {
                _items["statement"] = "以下是您的購買明細--";
                _items["warning"] = "提醒您：若本檔次購買數量未達門檻" + bhOrderMinimum.ToString("N0") + "人，</font><br/>本次交易即自動取消，<br/>扣抵的17Life紅利、17Life購物金、PayEasy購物金也將全額退回，<br/>刷卡金額則刷退至您的刷卡帳戶，若您以ATM轉帳付款，費用將全額退於您提供的帳戶，請您放心！<br/>若訂單使用17Life折價券折抵，無論是否退貨皆不得返還。";
                _items["authorize"] = "<li>此信函為系統自動發出，請勿直接回覆，有任何問題請至 <a href=\"" + ServerConfig.SiteUrl + ServerConfig.SiteServiceUrl + "\">" + ServerConfig.SiteUrl + ServerConfig.SiteServiceUrl + "</a> 聯繫。</li><li>本公司保留接受或取消本訂購的權利。</li>";
            }
            else if (_mailType == MailContentType.RefundApply)
            {
                _items["statement"] = "17Life客服中心已收到您以下的取消訂單申請，將於三至五個工作天內回覆您處理結果。";
                _items["warning"] = "提醒您：當申請取消訂單處理完成後，<br/>17Life紅利、17Life購物金、PayEasy購物金都會分別退回至原本所屬帳戶，<br/>刷卡金額將退還至您的17Life購物金帳戶，<br/>請您放心！";
                _items["authorize"] = "<li>此信函為系統自動發出，請勿回覆此信，本公司保留接受或取消本訂購的權利。</li>";
                cpa = "/17_EDMreturn";
            }
            else if (_mailType == MailContentType.RefundResult)
            {
                _items["statement"] = "下列申請取消的訂單已為您處理完畢。";

                _items["warning"] = "提醒您：當客服中心為您取消訂單後，<br/>扣抵的17Life紅利、17Life購物金、PayEasy購物金將全額退回，<br/>刷卡金額將" +
                    ((_isRefundCreditcard) ? "刷退至您的刷卡銀行" : "退還至您的17Life購物金") +
                    "帳戶，<br/>請您放心！";
                _items["authorize"] = "<li>此信函為系統自動發出，請勿直接回覆，有任何問題請至 <a href=\"" + ServerConfig.SiteUrl + ServerConfig.SiteServiceUrl + "\">" + ServerConfig.SiteUrl + ServerConfig.SiteServiceUrl + "</a> 聯繫。</li>";
                cpa = "/17_EDMcancel";
            }
            else if (_mailType == MailContentType.RefundNoticeInvoice)
            {
                _items["statement"] = "我們已收到您的發票，<b>但尚未收到您的退貨申請書，還請您盡速補寄</b>。";
                _items["warning"] = "提醒您：當客服中心為您取消訂單後，<br/>扣抵的17Life紅利、17Life購物金、PayEasy購物金將全額退回，<br/>刷卡金額將" +
                    ((_isRefundCreditcard) ? "刷退至您的刷卡銀行" : "退還至您的17Life購物金") +
                    "帳戶，<br/>請您放心！";
                _items["authorize"] = "<li>此信函為系統自動發出，請勿直接回覆，有任何問題請至 <a href=\"" + ServerConfig.SiteUrl + ServerConfig.SiteServiceUrl + "\">" + ServerConfig.SiteUrl + ServerConfig.SiteServiceUrl + "</a> 聯繫。</li>";
                cpa = "";
            }
            else if (_mailType == MailContentType.RefundNoticePaper)
            {
                _items["statement"] = "我們已收到您的退貨申請書，<b>但尚未收到您的發票，還請您盡速補寄</b>。";
                _items["warning"] = "提醒您：當客服中心為您取消訂單後，<br/>扣抵的17Life紅利、17Life購物金、PayEasy購物金將全額退回，<br/>刷卡金額將" +
                    ((_isRefundCreditcard) ? "刷退至您的刷卡銀行" : "退還至您的17Life購物金") +
                    "帳戶，<br/>請您放心！";
                _items["authorize"] = "<li>此信函為系統自動發出，請勿直接回覆，有任何問題請至 <a href=\"" + ServerConfig.SiteUrl + ServerConfig.SiteServiceUrl + "\">" + ServerConfig.SiteUrl + ServerConfig.SiteServiceUrl + "</a> 聯繫。</li>";
                cpa = "";
            }
            if (_freight > 0)
                _items["freight"] = "運費：" + _freight.ToString("N0");
        }

        protected void PreparePromoDealsInfo()
        {
            // 先建立預設值
            int dealsCount = ServerConfig.PromoMailDealsCount;
            for (int i = 0; i < dealsCount; i++)
            {
                _items["deal" + (i + 1) + "url"] = string.Empty;
                _items["deal" + (i + 1) + "pic"] = string.Empty;
                _items["deal" + (i + 1) + "title"] = string.Empty;
                _items["deal" + (i + 1) + "price"] = string.Empty;
            }

            // 依序設定檔次資料
            for (int i = 0; i < _promoDeals.Count; i++)
            {
                _items["deal" + (i + 1) + "url"] = ServerConfig.SiteUrl + "/deal/" + _promoDeals[i].BusinessHourGuid.ToString() + "?rsrc=17_EDMpay";
                _items["deal" + (i + 1) + "pic"] = Helper.GetImgUrl(_promoDeals[i].EventImagePath);
                _items["deal" + (i + 1) + "title"] = (_promoDeals[i].ItemName.Length > 31) ? (_promoDeals[i].ItemName.Substring(0, 31) + "...") : _promoDeals[i].ItemName;
                _items["deal" + (i + 1) + "price"] = Convert.ToInt32(_promoDeals[i].ItemPrice).ToString("D") + (Helper.IsFlagSet(_promoDeals[i].BusinessHourStatus, BusinessHourStatus.ComboDealMain) ? "起" : string.Empty);
            }

            // 必須有檔次數才要顯示
            _items["show_promo"] = _promoDeals.Count() > 0;
        }

        protected void PrepareDealInfo()
        {
            if (_theDeal != null)
            {
                _items["event_name"] = _theDeal.EventName;
                bhOrderMinimum = _theDeal.BusinessHourOrderMinimum;

                _items["inquire"] = "若您想了解本次訂購的進行狀況，<br/>請至【<a href='" + cp.SiteUrl + "/ppon/'>17Life首頁</a>】點選【我的P好康 &gt; <a href='" + cp.SiteUrl + "/user/coupon_List.aspx'>我的好康憑證/訂單</a>】即可。";
                _items["department"] = "好康";
                _items["dLink"] = "ppon";
                _items["email_top"] = "17Life/G1-8/17PEmail_Top.jpg";
                _items["email_bottom"] = "17Life/G1-8/17PEmail_Bottom.jpg";

                validTime = _theDeal.BusinessHourDeliverTimeE.Value;
                department = (DepartmentTypes)_theDeal.Department;

                if (_theDeal.DeliveryType.HasValue && int.Equals((int)DeliveryType.ToHouse, _theDeal.DeliveryType.Value))  //如果是商品檔不顯示下載憑證 商品檔(PponItem=tohouse)
                {
                    _items["download_credit"] = "";
                }
                else
                {
                    _items["download_credit"] = "<tr > <td style=\"font-family: 'Arial';text-align:center\"> <br/>  好康成立後，請到這裡  <a href=\"" + cp.SiteUrl + "/user/coupon_List.aspx\">下載好康憑證>>></a>  </td>  </tr>";
                }
            }
        }

        protected void PrepareOrderDetailInfo()
        {
            if (_theOrderDetailCol.Count > 0)
            {
                int quantity = 0;
                decimal total = 0;
                string itemDetail = string.Empty;
                for (int i = 0; i < _theOrderDetailCol.Count; i++)
                {
                    quantity += _theOrderDetailCol[i].ItemQuantity;
                    total += (decimal)_theOrderDetailCol[i].OrderDetailTotal;
                    string itemName = _theOrderDetailCol[i].ItemName.Replace(_theDeal.EventName, string.Empty);
                    if (!string.IsNullOrEmpty(itemName))
                        itemDetail += "<li>" + itemName + " x " + _theOrderDetailCol[i].ItemQuantity.ToString() + "</li>";
                }
                if (_freight > 0) total += _freight;

                _items["member_name"] = _theOrderDetailCol[0].MemberName;
                _items["item_quantity"] = quantity;
                _items["order_detail_total"] = total;
                _items["item_detail"] = itemDetail;
            }
        }

        private string GetPaymentList(PaymentTransactionCollection payments)
        {
            string pList = string.Empty;
            string pType = string.Empty;

            foreach (var p in payments)
            {
                if (p != null)
                {
                    switch ((PaymentType)p.PaymentType)
                    {
                        case PaymentType.Creditcard:
                            pType = "刷卡";
                            break;

                        case PaymentType.PCash:
                            pType = "PayEasy購物金";
                            break;

                        case PaymentType.BonusPoint:
                            pType = "17Life紅利";
                            break;

                        case PaymentType.SCash:
                            pType = "17Life購物金";
                            break;

                        default:
                            pType = p.PaymentType.ToString();
                            break;
                    }
                    pList += "<li>" + pType + "：" + p.Amount.ToString("C0") + "</li>";
                }
            }

            return pList;
        }

        private string GetImgUrl(string path)
        {
            return ((string.IsNullOrWhiteSpace(path)) ? Helper.GetMediaPathsFromRawData(ServerConfig.EDMDefaultImage, MediaType.PponDealPhoto, ServerConfig.MediaBaseUrl).DefaultIfEmpty(string.Empty).First() : Helper.GetMediaPathsFromRawData(path, MediaType.PponDealPhoto, ServerConfig.MediaBaseUrl).DefaultIfEmpty(string.Empty).First());
        }
    }
}
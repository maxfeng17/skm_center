
namespace LunchKingSite.Core.Component.Template
{
    public class SimpGrpOrdCnt : TemplateBase
    {
        ISysConfProvider cp = ProviderFactory.Instance().GetConfig();

        public SimpGrpOrdCnt() : base() { }

        protected override void Initialize()
        {
            _items["site_url"] =cp.SiteUrl;
            _items["https_site_url"] = cp.SiteUrl.Replace("http", "https");

            _items.Add("user", null);
            _items.Add("sellername", null);
            _items.Add("link", null);
            _items.Add("deadline", null);
        }

        public string User
        {
            get { return (string)_items["user"]; }
            set { _items["user"] = value; }
        }

        public string SellerName
        {
            get { return (string)_items["sellername"]; }
            set { _items["sellername"] = value; }
        }

        public string Link
        {
            get { return (string)_items["link"]; }
            set { _items["link"] = value; }
        }

        public string Deadline
        {
            get { return (string)_items["deadline"]; }
            set { _items["deadline"] = value; }
        }
    }
}

﻿using System;
using System.Collections.Generic;

namespace LunchKingSite.Core.Component.Template
{
    [TemplateFile("greetingcardsenderdiscount.txt")]
    public class GreetingCardSenderDiscount : TemplateBase
    {
        ISysConfProvider cp = ProviderFactory.Instance().GetConfig();
        public GreetingCardSenderDiscount() : base() { }

        protected override void Initialize()
        {
            _items["discountName"] = string.Empty;
            _items["discountCode"] = string.Empty;
            _items["usingTimeLimit"] = string.Empty;
            _items["deal1title"] = string.Empty;
            _items["deal1pic"] = string.Empty;
            _items["deal1url"] = string.Empty;
            _items["deal2title"] = string.Empty;
            _items["deal2pic"] = string.Empty;
            _items["deal2url"] = string.Empty;
            _items["deal3title"] = string.Empty;
            _items["deal3pic"] = string.Empty;
            _items["deal3url"] = string.Empty;
            _items["site_url"] = cp.SiteUrl;
            _items["site_service_url"] = cp.SiteServiceUrl;
        }

        public string DiscountName 
        {
            get { return (string)_items["discountName"]; }
            set { _items["discountName"] = value; }
        }

        public string DiscountCode
        {
            get { return (string)_items["discountCode"]; }
            set { _items["discountCode"] = value; }
        }

        public string UsingTimeLimit
        {
            get { return (string)_items["usingTimeLimit"]; }
            set { _items["usingTimeLimit"] = value; }
        }

        public string Deal1Title
        {
            get { return (string)_items["deal1title"]; }
            set { _items["deal1title"] = value; }
        }

        public string Deal1Pic
        {
            get { return (string)_items["deal1pic"]; }
            set { _items["deal1pic"] = value; }
        }

        public string Deal1Url
        {
            get { return (string)_items["deal1url"]; }
            set { _items["deal1url"] = value; }
        }

        public string Deal2Title
        {
            get { return (string)_items["deal2title"]; }
            set { _items["deal2title"] = value; }
        }

        public string Deal2Pic
        {
            get { return (string)_items["deal2pic"]; }
            set { _items["deal2pic"] = value; }
        }

        public string Deal2Url
        {
            get { return (string)_items["deal2url"]; }
            set { _items["deal2url"] = value; }
        }

        public string Deal3Title
        {
            get { return (string)_items["deal3title"]; }
            set { _items["deal3title"] = value; }
        }

        public string Deal3Pic
        {
            get { return (string)_items["deal3pic"]; }
            set { _items["deal3pic"] = value; }
        }

        public string Deal3Url
        {
            get { return (string)_items["deal3url"]; }
            set { _items["deal3url"] = value; }
        }
    }
}



namespace LunchKingSite.Core.Component.Template
{
    [TemplateFile("forgetpassword.txt")]
    public class ForgetPassword : TemplateBase
    {
        public ForgetPassword()
            : base()
        {
        }

        protected override void Initialize()
        {
            _items.Add("username", null);
            _items.Add("password", null);
            _items.Add("site_url",null);
        }

        public string UserName
        {
            get { return (string)_items["username"]; }
            set { _items["username"] = value; }
        }

        public string Password
        {
            get { return (string)_items["password"]; }
            set { _items["password"] = value; }
        }

        public string SiteUrl
        {
            get { return (string)_items["site_url"]; }
            set { _items["site_url"] = value; }
        }
    }
}

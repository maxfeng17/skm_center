﻿using System;
using System.Linq;
using LunchKingSite.DataOrm;

namespace LunchKingSite.Core.Component.Template
{
    public class RefundCompleteNoticeMail : TemplateBase
    {
        public ISysConfProvider ServerConfig { get; set; }
        ISysConfProvider cp = ProviderFactory.Instance().GetConfig();

        public RefundCompleteNoticeMail()
            : base()
        {
        }

        protected string _memberName = string.Empty;
        public string MemberName
        {
            get { return _memberName; }
            set { _memberName = value; }
        }

        protected string _orderID = string.Empty;
        public string OrderID
        {
            get { return _orderID; }
            set { _orderID = value; }
        }
        protected string _orderGuid = string.Empty;
        public string OrderGuid
        {
            get { return _orderGuid; }
            set { _orderGuid = value; }
        }

        protected string _itemName = string.Empty;
        public string ItemName
        {
            get { return _itemName; }
            set { _itemName = value; }
        }

        protected int _returnedCounts = 0;
        public int ReturnedCounts
        {
            get { return _returnedCounts; }
            set { _returnedCounts = value; }
        }
        
        protected int _returnTotal = 0;
        public int ReturnTotal
        {
            get { return _returnTotal; }
            set { _returnTotal = value; }
        }

        protected int _returnPcash = 0;
        public int ReturnPcash
        {
            get { return _returnPcash; }
            set { _returnPcash = value; }
        }

        protected int _returnScash = 0;
        public int ReturnScash
        {
            get { return _returnScash; }
            set { _returnScash = value; }
        }

        protected int _returnPScash = 0;
        public int ReturnPScash
        {
            get { return _returnPScash; }
            set { _returnPScash = value; }
        }

        protected int _returnBcash = 0;
        public int ReturnBcash
        {
            get { return _returnBcash; }
            set { _returnBcash = value; }
        }

        protected int _returnCreditcard = 0;
        public int ReturnCreditcard
        {
            get { return _returnCreditcard; }
            set { _returnCreditcard = value; }
        }

        protected int _returnAtm = 0;
        public int ReturnAtm
        {
            get { return _returnAtm; }
            set { _returnAtm = value; }
        }

        protected int _returnTcash = 0;
        public int ReturnTcash
        {
            get { return _returnTcash; }
            set { _returnTcash = value; }
        }

        protected string _thirdPartyPayment = string.Empty;
        public string ThirdPartyPayment
        {
            get { return _thirdPartyPayment; }
            set { _thirdPartyPayment = value; }
        }
        public bool IsShowAllowance { get; set; }
        public bool IsInvoiced { get; set; }

        protected override void Initialize()
        {
            _callback = new PrepareItemCallback(PrepareItem);
        }

        protected virtual void PrepareItem()
        {
            _items["site_url"] = cp.SiteUrl;
            _items["site_service_url"] = cp.SiteServiceUrl;
            _items["member_name"] = _memberName;
            _items["order_id"] = _orderID;
            _items["order_guid"] = _orderGuid;
            _items["item_name"] = _itemName;
            _items["returned_counts"] = _returnedCounts;
            _items["return_total"] = _returnTotal;
            _items["return_pcash"] = _returnPcash;
            _items["return_scash"] = _returnScash;
            _items["return_pscash"] = _returnPScash;
            _items["return_bcash"] = _returnBcash;
            _items["return_creditcard"] = _returnCreditcard;
            _items["return_atm"] = _returnAtm;
            _items["return_tcash"] = _returnTcash;
            _items["thirdPartyPayment"] = _thirdPartyPayment;
            _items["is_show_allowance"] = this.IsShowAllowance;
            _items["is_invoiced"] = this.IsInvoiced;
        }
    }
}

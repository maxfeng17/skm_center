﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.Core.Component.Template.MGM
{
    [TemplateFile("MGM/sendGiftMail.txt")]
    public class SendGiftMail : TemplateBase
    {
        public SendGiftMail() : base()
        {
        }

        public string GiftCard
        {
            get { return (string)_items["gift_Card"]; }
            set { _items["gift_Card"] = value; }
        }
        public string FriendMessage
        {
            get { return (string)_items["friend_Message"]; }
            set { _items["friend_Message"] = value; }
        }
        public string DealBid
        {
            get { return (string)_items["deal_Bid"]; }
            set { _items["deal_Bid"] = value; }
        }
        public string DealItemName
        {
            get { return (string)_items["deal_ItemName"]; }
            set { _items["deal_ItemName"] = value; }
        }
        public string DealContent
        {
            get { return (string)_items["deal_Content"]; }
            set
            {
                if (value.Length > 70)
                {
                    _items["deal_Content"] = value.Substring(0, 60) + "...";
                }
                else
                {
                    _items["deal_Content"] = value;
                }
            }
        }
        public string DealTitle
        {
            get { return (string)_items["deal_Title"]; }
            set { _items["deal_Title"] = value; }
        }
        public string FriendName
        {
            get { return (string)_items["friend_Name"]; }
            set { _items["friend_Name"] = value; }
        }
        public string NowDate
        {
            get { return (string)_items["now_Date"]; }
            set { _items["now_Date"] = value; }
        }
        public string ExpireDate
        {
            get { return (string)_items["expire_Date"]; }
            set { _items["expire_Date"] = value; }
        }

        public string MainDealPic
        {
            get { return (string)_items["main_DealPic"]; }
            set { _items["main_DealPic"] = value; }
        }

        public string AccessLink
        {
            get { return (string)_items["access_Link"]; }
            set { _items["access_Link"] = value; }
        }

        public string Addr
        {
            get { return (string)_items["deal_Addr"]; }
            set { _items["deal_Addr"] = value; }
        }
        public string Tel
        {
            get { return (string)_items["deal_Tel"]; }
            set { _items["deal_Tel"] = value; }
        }
        public string SiteUrl
        {
            get { return (string)_items["siteUrl"]; }
            set { _items["siteUrl"] = value; }
        }
        public string SiteServiceUrl
        {
            get { return (string)_items["siteServiceUrl"]; }
            set { _items["siteServiceUrl"] = value; }
        }
        public string ReceiverName
        {
            get { return (string)_items["receiver_Name"]; }
            set { _items["receiver_Name"] = value; }
        }
        public string BackgroundColor
        {
            get { return (string)_items["background_color"]; }
            set { _items["background_color"] = value; }
        }
        protected override void Initialize()
        {
            _items["receiver_Name"] = string.Empty;
            _items["siteUrl"] = string.Empty;
            _items["siteServiceUrl"] = string.Empty;
            _items["gift_Card"] = string.Empty;
            _items["friend_Message"] = string.Empty;
            _items["deal_Bid"] = string.Empty;
            _items["deal_Tel"] = string.Empty;
            _items["deal_Addr"] = string.Empty;
            _items["deal_Title"] = string.Empty;
            _items["deal_Content"] = string.Empty;
            _items["deal_ItemName"] = string.Empty;
            _items["friend_Name"] = string.Empty;
            _items["now_Date"] = string.Empty;
            _items["expire_Date"] = string.Empty;
            _items["main_DealPic"] = string.Empty;
            _items["access_Link"] = string.Empty;
            _items["background_color"] = string.Empty;
        }
    }
}

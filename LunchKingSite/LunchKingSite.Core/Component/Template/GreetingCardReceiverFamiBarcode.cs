﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.Core.Component.Template
{
    [TemplateFile("greetingcardreceiverfamibarcode.txt")]
    public class GreetingCardReceiverFamiBarcode : TemplateBase
    {
        ISysConfProvider cp = ProviderFactory.Instance().GetConfig();
        public GreetingCardReceiverFamiBarcode() : base() { }

        protected override void Initialize()
        {
            _items["nickname"] = string.Empty;
            _items["card_pic"] = string.Empty;
            _items["mailcontent"] = string.Empty;
            _items["barcodePic"] = string.Empty;
            _items["dealName"] = string.Empty;
            _items["barcode"] = string.Empty;
            _items["deliverDateStart"] = string.Empty;
            _items["deliverDateEnd"] = string.Empty;
            _items["site_url"] = cp.SiteUrl;
            _items["site_service_url"] = cp.SiteServiceUrl;
        }

        public string Nickname
        {
            get { return (string)_items["nickname"]; }
            set { _items["nickname"] = value; }
        }

        public string CardPic
        {
            get { return (string)_items["card_pic"]; }
            set { _items["card_pic"] = value; }
        }

        public string MailContent
        {
            get { return (string)_items["mailcontent"]; }
            set { _items["mailcontent"] = value; }
        }

        public string BarcodePic
        {
            get { return (string)_items["barcodePic"]; }
            set { _items["barcodePic"] = value; }
        }

        public string DealName
        {
            get { return (string)_items["dealName"]; }
            set { _items["dealName"] = value; }
        }

        public string Barcode
        {
            get { return (string)_items["barcode"]; }
            set { _items["barcode"] = value; }
        }

        public string DeliverDateStart
        {
            get { return (string)_items["deliverDateStart"]; }
            set { _items["deliverDateStart"] = value; }
        }

        public string DeliverDateEnd
        {
            get { return (string)_items["deliverDateEnd"]; }
            set { _items["deliverDateEnd"] = value; }
        }
    }
}

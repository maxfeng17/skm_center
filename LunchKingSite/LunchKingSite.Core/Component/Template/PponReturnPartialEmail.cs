﻿using LunchKingSite.DataOrm;
using System;
using System.Linq;
using System.Collections.Generic;

namespace LunchKingSite.Core.Component.Template
{
    public class PponReturnPartialEmail : TemplateBase
    {
        public ISysConfProvider ServerConfig { get; set; }

        private ISysConfProvider cp = ProviderFactory.Instance().GetConfig();

        public PponReturnPartialEmail()
            : base()
        {
        }

        protected ViewPponDeal _theDeal = null;

        public ViewPponDeal TheDeal
        {
            get
            {
                return _theDeal;
            }
            set
            {
                _theDeal = value;
            }
        }

        protected ViewPponOrderDetailCollection _theOrderDetailCol = null;

        public ViewPponOrderDetailCollection TheOrderDetailCol
        {
            get
            {
                return _theOrderDetailCol;
            }
            set
            {
                _theOrderDetailCol = value;
            }
        }

        protected CashTrustLogCollection _theCashTrustLogCol = null;

        public CashTrustLogCollection TheCashTrustLogCol
        {
            get
            {
                return _theCashTrustLogCol;
            }
            set
            {
                _theCashTrustLogCol = value;
            }
        }

        protected List<IViewPponDeal> _promoDeals = null;
        public List<IViewPponDeal> PromoDeals
        {
            get { return _promoDeals; }
            set { _promoDeals = value; }
        }

        protected override void Initialize()
        {
            _callback = new PrepareItemCallback(PrepareItem);
        }

        protected virtual void PrepareItem()
        {
            _items["site_url"] = cp.SiteUrl;
            _items["site_service_url"] = cp.SiteServiceUrl;
            _items["credit"] = string.Empty;
            _items["atm"] = string.Empty;
            _items["pcash"] = string.Empty;
            _items["bcash"] = string.Empty;
            _items["scash"] = string.Empty;
            _items["freight"] = string.Empty;
            _items["total_price"] = 0;
            _items["return_total"] = 0;

            PrepareDealInfo();
            PrepareOrderDetailInfo();
            PreparePromoDealsInfo();
            SetOtherItem();
        }

        protected void PreparePromoDealsInfo()
        {
            // 先建立預設值
            int dealsCount = ServerConfig.PromoMailDealsCount;
            for (int i = 0; i < dealsCount; i++)
            {
                _items["deal" + (i + 1) + "url"] = string.Empty;
                _items["deal" + (i + 1) + "pic"] = string.Empty;
                _items["deal" + (i + 1) + "title"] = string.Empty;
                _items["deal" + (i + 1) + "price"] = string.Empty;
            }

            // 依序設定檔次資料
            for (int i = 0; i < _promoDeals.Count; i++)
            {
                _items["deal" + (i + 1) + "url"] = ServerConfig.SiteUrl + "/deal/" + _promoDeals[i].BusinessHourGuid.ToString() + "?rsrc=17_EDMpay";
                _items["deal" + (i + 1) + "pic"] = Helper.GetImgUrl(_promoDeals[i].EventImagePath);
                _items["deal" + (i + 1) + "title"] = (_promoDeals[i].ItemName.Length > 31) ? (_promoDeals[i].ItemName.Substring(0, 31) + "...") : _promoDeals[i].ItemName;
                _items["deal" + (i + 1) + "price"] = Convert.ToInt32(_promoDeals[i].ItemPrice).ToString("D") + (Helper.IsFlagSet(_promoDeals[i].BusinessHourStatus, BusinessHourStatus.ComboDealMain) ? "起" : string.Empty);
            }

            // 必須有檔次數才要顯示
            _items["show_promo"] = _promoDeals.Count() > 0;
        }

        protected void PrepareDealInfo()
        {
            if (_theDeal != null)
            {
                _items["item_name"] = _theDeal.ItemName;
                if (_theDeal.DeliveryType.HasValue && int.Equals((int)DeliveryType.ToShop, _theDeal.DeliveryType.Value))
                {
                    _items["product"] = "憑證";
                    _items["quantifier"] = "張";
                    _items["return_statement"] = "未使用憑證比例";
                }
                else
                {
                    _items["product"] = "商品";
                    _items["quantifier"] = "份";
                    _items["return_statement"] = "申請退貨商品數量";
                }
            }
        }

        protected void PrepareOrderDetailInfo()
        {
            if (_theOrderDetailCol.Count > 0)
            {
                _items["member_name"] = _theOrderDetailCol[0].MemberName;
                _items["return_no"] = _theOrderDetailCol.Where(x => x.OrderDetailStatus == (int)OrderDetailTypes.Regular).Sum(x => x.ItemQuantity);
            }
        }

        protected void SetOtherItem()
        {
            if (_theCashTrustLogCol.Count > 0)
            {
                int credit = _theCashTrustLogCol.Sum(x => x.CreditCard);
                int pcash = _theCashTrustLogCol.Sum(x => x.Pcash);
                int bonus = _theCashTrustLogCol.Sum(x => x.Bcash);
                int scash = _theCashTrustLogCol.Sum(x => x.Scash);
                int atm = _theCashTrustLogCol.Sum(x => x.Atm);
                int discount = _theCashTrustLogCol.Sum(x => x.DiscountAmount);
                int freight = 0;
                int returnTotal = _theCashTrustLogCol.Count();
                CashTrustLog ctFreight = (_theCashTrustLogCol.Where(x => x.SpecialStatus == (int)TrustSpecialStatus.Freight).Count() > 0) ? _theCashTrustLogCol.Where(x => x.SpecialStatus == (int)TrustSpecialStatus.Freight).First() : null;
                if (ctFreight != null)
                {
                    returnTotal -= 1;
                    freight = ctFreight.Amount;
                }

                string dollarSign = " NT$ ";
                if (scash > 0)
                    _items["scash"] =
                        @"<tr>" +
                        @"  <td>- 17Life購物金<span style=""color:#6d9345;font-family:Arial, Helvetica, sans-serif;"">" + dollarSign + scash.ToString("N0") + "</span></td>" +
                        @"</tr>";
                if (bonus > 0)
                    _items["bcash"] =
                        @"<tr>" +
                        @"  <td>- 17Life紅利<span style=""color:#6d9345;font-family:Arial, Helvetica, sans-serif;"">" + dollarSign + bonus.ToString("N0") + "</span></td>" +
                        @"</tr>";
                if (pcash > 0)
                    _items["pcash"] =
                        @"<tr>" +
                        @"  <td>- PayEasy購物金<span style=""color:#6d9345;font-family:Arial, Helvetica, sans-serif;"">" + dollarSign + pcash.ToString("N0") + "</span></td>" +
                        @"</tr>";
                if (atm > 0)
                    _items["atm"] =
                        @"<tr>" +
                        @"  <td>- ATM付款<span style=""color:#6d9345;font-family:Arial, Helvetica, sans-serif;"">" + dollarSign + atm.ToString("N0") + "</span></td>" +
                        @"</tr>";
                if (credit > 0)
                    _items["credit"] =
                        @"<tr>" +
                        @"  <td>- 刷卡<span style=""color:#6d9345;font-family:Arial, Helvetica, sans-serif;"">" + dollarSign + credit.ToString("N0") + "</span></td>" +
                        @"</tr>";
                if (freight > 0)
                    _items["freight"] = @"(含運費" + dollarSign + freight.ToString() + @")";

                _items["total_price"] = _theCashTrustLogCol.Sum(x => x.Amount) - discount;
                _items["return_total"] = returnTotal;
            }
        }

        private string GetImgUrl(string path)
        {
            return ((string.IsNullOrWhiteSpace(path)) ? Helper.GetMediaPathsFromRawData(ServerConfig.EDMDefaultImage, MediaType.PponDealPhoto, ServerConfig.MediaBaseUrl).DefaultIfEmpty(string.Empty).First() : Helper.GetMediaPathsFromRawData(path, MediaType.PponDealPhoto, ServerConfig.MediaBaseUrl).DefaultIfEmpty(string.Empty).First());
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.Core.Component.Template
{
    [TemplateFile("PremiumSuccessMail.txt")]
    public class PremiumSuccessMail : TemplateBase
    {
        public PremiumSuccessMail() : base() { }

        protected override void Initialize()
        {
            _items.Add("site_url", null);
            _items.Add("site_service_url", null);

            _items.Add("premium_name", string.Empty);
        }
        public string SiteUrl
        {
            get { return (string)_items["site_url"]; }
            set { _items["site_url"] = value; }
        }
        public string SiteServiceUrl
        {
            get { return (string)_items["site_service_url"]; }
            set { _items["site_service_url"] = value; }
        }

        public string PremiumName
        {
            get { return (string)_items["premium_name"]; }
            set { _items["premium_name"] = value; }
        }
    }
}

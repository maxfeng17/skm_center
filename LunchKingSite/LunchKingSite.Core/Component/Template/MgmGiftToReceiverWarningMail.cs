﻿using System.Data;
using LunchKingSite.Core.Models;
using LunchKingSite.DataOrm;
using System;

namespace LunchKingSite.Core.Component.Template
{
    public class MgmGiftToReceiverWarningMail : TemplateBase
    {
        ISysConfProvider cp = ProviderFactory.Instance().GetConfig();
        public MgmGiftToReceiverWarningMail()
            : base()
        {
        }

        public string TheMemberName { get; set; }

        public string ItemName { get; set; }

        public ViewPponDeal _theDeal = null;
        public ViewPponDeal TheDeal
        {
            get { return _theDeal; }
            set { _theDeal = value; }
        }
        public DateTime EndTime { get; set; }

        protected override void Initialize()
        {
            _callback = new PrepareItemCallback(PrepareItem);
        }

        protected virtual void PrepareItem()
        {
            _items["site_url"] = cp.SiteUrl;
            _items["site_service_url"] = cp.SiteServiceUrl;
            PrepareDealInfo();
        }

        protected void PrepareDealInfo()
        {
            if (_theDeal != null)
            {
                _items["member_name"] = TheMemberName;
                _items["item_name"] = ItemName;
                _items["deliver_date_end"] = EndTime.ToString("yyyy/MM/dd");
            }
        }
    }
}

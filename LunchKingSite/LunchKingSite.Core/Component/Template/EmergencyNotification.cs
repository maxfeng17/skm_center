﻿
using System;
using LunchKingSite.DataOrm;

namespace LunchKingSite.Core.Component.Template
{
    [TemplateFile("emergencynotify.txt")]
    public class EmergencyNotification : TemplateBase
    {
        
        private static ISysConfProvider SystemConfig
        {
            get { return ProviderFactory.Instance().GetConfig(); }
        }

        protected override void Initialize()
        {
            _items.Add("order_id", null);
            _items.Add("order_guid", null);
            _items.Add("seller_name", null);
            _items.Add("seller_id", null);
            _items.Add("reason", null);
            _items.Add("site_url",null);
            _items.Add("lk_SiteUrl",null);

            _callback = new PrepareItemCallback(GetOrderInfo);
        }

        private void GetOrderInfo()
        {
            _items["site_url"] = SystemConfig.SiteUrl;
            _items["lk_SiteUrl"] = SystemConfig.SiteUrl; ;

            if (OrderGuid == Guid.Empty)
                return;

            ViewOrderMemberBuildingSeller data =
                ProviderFactory.Instance().GetProvider<IOrderProvider>().ViewOrderMemberBuildingSellerGet(
                    OrderGuid);
            _items["order_id"] = data.OrderId;
            _items["order_guid"] = data.OrderGuid.ToString();
            _items["seller_name"] = data.SellerName;
            _items["seller_id"] = data.SellerId;

        }

        public string Reason
        {
            get { return (string) _items["reason"]; }
            set { _items["reason"] = value; }
        }

        public Guid OrderGuid { get; set; }
    }
}

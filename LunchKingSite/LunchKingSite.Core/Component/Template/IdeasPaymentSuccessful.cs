﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.DataOrm;

namespace LunchKingSite.Core.Component.Template
{
    public class IdeasPaymentSuccessful : TemplateBase
    {
        public ISysConfProvider ServerConfig { get; set; }
        ISysConfProvider cp = ProviderFactory.Instance().GetConfig();
        public IdeasPaymentSuccessful()
            : base()
        {
        }

        protected ViewPponDeal _theDeal = null;
        public ViewPponDeal TheDeal
        {
            get { return _theDeal; }
            set { _theDeal = value; }
        }

        protected ViewPponOrderDetailCollection _theOrderDetailCol = null;
        public ViewPponOrderDetailCollection TheOrderDetailCol
        {
            get { return _theOrderDetailCol; }
            set { _theOrderDetailCol = value; }
        }

        protected CashTrustLogCollection _theCashTrustLogCol = null;
        public CashTrustLogCollection TheCashTrustLogCol
        {
            get { return _theCashTrustLogCol; }
            set { _theCashTrustLogCol = value; }
        }

        protected string _itemName = null;
        public string ItemName
        {
            get { return _itemName; }
            set { _itemName = value; }
        }

        protected override void Initialize()
        {
            _callback = new PrepareItemCallback(PrepareItem);
        }

        protected virtual void PrepareItem()
        {
            _items["site_url"] = cp.SiteUrl;
            _items["site_service_url"] = cp.SiteServiceUrl;
            _items["https_site_url"] = cp.SiteUrl.Replace("http", "https");

            PrepareOrderDetailInfo();
            PreparePaymentTransInfo();
            _items["order_minimum"] = _theDeal.BusinessHourOrderMinimum.ToString("N0");
            SetOtherItem();
        }

        protected void PrepareOrderDetailInfo()
        {
            if (_theOrderDetailCol.Count > 0)
            {
                _items["member_name"] = _theOrderDetailCol[0].MemberName;
                if (!string.IsNullOrEmpty(_itemName))
                    _items["item_name"] = _itemName;
                else
                    _items["item_name"] = _theOrderDetailCol[0].ItemName.Replace("&nbsp", " ");
            }
        }

        protected void PreparePaymentTransInfo()
        {
            _items["credit"] = string.Empty;
            _items["order_date"] = _theCashTrustLogCol.First().CreateTime.ToString("yyyy/MM/dd");
            _items["amount"] = "NT$" + _theCashTrustLogCol.Sum(x => x.Amount).ToString("N0");
            _items["delivery_alert"] = (_theDeal.DeliveryType == (int)DeliveryType.ToHouse)
                                        ? "※商品將依照訂購順序，於" + _theDeal.BusinessHourDeliverTimeS.Value.ToString("yyyy/MM/dd") + "起開始陸續出貨，最晚將於" + _theDeal.BusinessHourDeliverTimeE.Value.ToString("yyyy/MM/dd") + "前出貨完畢；出貨後配送約2~3工作日，恕無法指定配達時間。<br/><br/>"
                                        : string.Empty;
        }

        protected void SetOtherItem()
        {
            _items["downloadcoupon"] = string.Empty;
            if (_theCashTrustLogCol.Count > 0)
            {
                int credit = _theCashTrustLogCol.Sum(x => x.CreditCard);
                if (credit > 0)
                    _items["credit"] =
                        @"<tr>" +
                        @"  <td>- 刷卡<span style=""color:#6d9345;font-family:Arial, Helvetica, sans-serif;""> NT$" + credit.ToString("N0") + "</span></td>" +
                        @"</tr>";
                _items["total_price"] = _theCashTrustLogCol.Sum(x => (x.Amount - x.DiscountAmount));
            }
        }
    }
}

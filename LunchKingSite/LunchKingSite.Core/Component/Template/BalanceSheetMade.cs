using LunchKingSite.DataOrm;

namespace LunchKingSite.Core.Component.Template
{
    public class BalanceSheetMade : TemplateBase
    {
        public ISysConfProvider ServerConfig { get; set; }
        ISysConfProvider cp = ProviderFactory.Instance().GetConfig();

        public BalanceSheetMade()
            : base()
        {
        }

        protected override void Initialize()
        {
            _callback = new PrepareItemCallback(PrepareItem);
        }

        protected virtual void PrepareItem()
        {
            _items["site_url"] = cp.SiteUrl;
            _items["seller_name"] = seller_name;
        }

        protected string seller_name = null;
        public string Seller_Name
        {
            get { return seller_name; }
            set { seller_name = value; }
        }

    }
}
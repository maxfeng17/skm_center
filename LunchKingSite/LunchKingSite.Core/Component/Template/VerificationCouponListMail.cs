﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.Core.Component.Template
{
    [TemplateFile("VerificationCouponListMail.txt")]
    public class VerificationCouponListMail : TemplateBase
    {
        public VerificationCouponListMail() : base() { }

        protected override void Initialize()
        {
            _items.Add("sellerName", string.Empty);
            _items.Add("exportTime", string.Empty);
        }

        public string sellerName
        {
            get { return (string)_items["sellerName"]; }
            set { _items["sellerName"] = value; }
        }
        public string exportTime
        {
            get { return (string)_items["exportTime"]; }
            set { _items["exportTime"] = value; }
        }
    }
}

﻿using System;
using System.Collections.Generic;

namespace LunchKingSite.Core.Component.Template
{
    [TemplateFile("dealcollectssoldend.txt")]
    public class DealCollectSoldEnd : TemplateBase
    {
        public DealCollectSoldEnd() : base() { }

        protected override void Initialize()
        {
            base.Initialize();

            _items["siteUrl"] = string.Empty;
            _items["siteServiceUrl"] = string.Empty;
            _items["memberName"] = string.Empty;
            _items["couponUsage"] = string.Empty;
            _items["eventTitle"] = string.Empty;
            _items["dealDiscount"] = string.Empty;
            _items["itemPrice"] = string.Empty;
            _items["origPrice"] = string.Empty;
            _items["dealUrl"] = string.Empty;
            _items["dealImg"] = string.Empty;
            _items["orderTimeEnd"] = string.Empty;
        }

        public string SiteUrl
        {
            get { return (string)_items["siteUrl"]; }
            set { _items["siteUrl"] = value; }
        }

        public string SiteServiceUrl
        {
            get { return (string)_items["siteServiceUrl"]; }
            set { _items["siteServiceUrl"] = value; }
        }

        public string MemberName
        {
            get { return (string)_items["memberName"]; }
            set { _items["memberName"] = value; }
        }

        public string CouponUsage
        {
            get { return (string)_items["couponUsage"]; }
            set { _items["couponUsage"] = value; }
        }

        public string EventTitle
        {
            get { return (string)_items["eventTitle"]; }
            set { _items["eventTitle"] = value; }
        }

        public string DealDiscount
        {
            get { return (string)_items["dealDiscount"]; }
            set { _items["dealDiscount"] = value; }
        }

        public string ItemPrice
        {
            get { return (string)_items["itemPrice"]; }
            set { _items["itemPrice"] = value; }
        }

        public string OrigPrice
        {
            get { return (string)_items["origPrice"]; }
            set { _items["origPrice"] = value; }
        }

        public string DealUrl
        {
            get { return (string)_items["dealUrl"]; }
            set { _items["dealUrl"] = value; }
        }

        public string DealImg
        {
            get { return (string)_items["dealImg"]; }
            set { _items["dealImg"] = value; }
        }

        public string OrderTimeEnd
        {
            get { return (string)_items["orderTimeEnd"]; }
            set { _items["orderTimeEnd"] = value; }
        }
    }
}

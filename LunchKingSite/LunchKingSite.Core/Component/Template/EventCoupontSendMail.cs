﻿using System;
using System.Linq;
using LunchKingSite.DataOrm;
using System.Collections.Generic;

namespace LunchKingSite.Core.Component.Template
{
    public class EventCoupontSendMail : TemplateBase
    {
        public ISysConfProvider ServerConfig { get; set; }
        public EventCoupontSendMail()
            : base()
        {
        }

        protected string _memberName = null;
        public string MemberName
        {
            get { return _memberName; }
            set { _memberName = value; }
        }

        protected string _eventName = null;
        public string EventName
        {
            get { return _eventName; }
            set { _eventName = value; }
        }

        protected string _code = null;
        public string Code
        {
            get { return _code; }
            set { _code = value; }
        }

        protected string _startdate = null;
        public string StartDate
        {
            get { return _startdate; }
            set { _startdate = value; }
        }

        protected string _enddate = null;
        public string EndDate
        {
            get { return _enddate; }
            set { _enddate = value; }
        }

        protected string _explanation = null;
        public string Explanation
        {
            get { return _explanation; }
            set { _explanation = value; }
        }
        
        protected override void Initialize()
        {
            _callback = new PrepareItemCallback(PrepareItem);
        }

        protected virtual void PrepareItem()
        {
            _items["site_url"] = ServerConfig.SiteUrl;
            _items["site_service_url"] = ServerConfig.SiteServiceUrl;

            PrepareDiscountCodeInfo();
        }

        protected void PrepareDiscountCodeInfo()
        {
            _items["member_name"] = _memberName;
            _items["event_name"] = _eventName;
            _items["code"] = _code;
            _items["start_date"] = _startdate;
            _items["end_date"] = _enddate;
            _items["explanation"] = _explanation;
        }
    }
}
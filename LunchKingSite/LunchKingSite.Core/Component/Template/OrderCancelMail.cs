﻿using System;
using System.Linq;
using LunchKingSite.DataOrm;

namespace LunchKingSite.Core.Component.Template
{
    public class OrderCancelMail : TemplateBase
    {
        public string MemberName { get; set; }

        public string OrderGuid { get; set; }

        public string OrderID { get; set; }

        public string ItemName { get; set; }

        public string CouponCounts { get; set; }

        public string ReturnCouponCounts { get; set; }
        public bool ApplyCancel { get; set; }
        public bool CancelSuccess { get; set; }

        public string SiteUrl { get; set; }

        public string SiteServiceUrl { get; set; }

        protected override void Initialize()
        {
            _callback = new PrepareItemCallback(PrepareItem);
        }

        protected virtual void PrepareItem()
        {
            _items["site_url"] = this.SiteUrl;
            _items["site_service_url"] = this.SiteServiceUrl;
            _items["member_name"] = this.MemberName;
            _items["order_id"] = this.OrderID;
            _items["order_guid"] = this.OrderGuid;
            _items["item_name"] = this.ItemName;
            _items["coupon_counts"] = this.CouponCounts;
            _items["return_coupon_counts"] = this.ReturnCouponCounts;
            _items["apply_cancel"] = this.ApplyCancel;
            _items["cancel_success"] = this.CancelSuccess;
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace LunchKingSite.Core.Component.Template
{
    [TemplateFile("GsaNotifyToSeller.txt")]
    public class GsaNotifyToSeller : TemplateBase
    {
        public GsaNotifyToSeller() : base() { }

        protected override void Initialize()
        {
            _items.Add("seller_name", string.Empty);
            _items.Add("site_url", string.Empty);
            _items.Add("https_site_url", string.Empty);
            _items.Add("site_service_url", string.Empty);
        }

        /// <summary>
        /// 商家名稱
        /// </summary>
        public string SellerName
        {
            get { return (string)_items["seller_name"]; }
            set { _items["seller_name"] = value; }
        }
       
        public string SiteUrl
        {
            get { return (string)_items["site_url"]; }
            set { _items["site_url"] = value; }
        }
        public string HttpsSiteUrl
        {
            get { return (string)_items["https_site_url"]; }
            set { _items["https_site_url"] = value; }
        }
        public string SiteServiceUrl
        {
            get { return (string)_items["site_service_url"]; }
            set { _items["site_service_url"] = value; }
        }
    }
}

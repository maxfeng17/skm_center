﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace LunchKingSite.Core.Component.Template
{
    [TemplateFile("NewReturnOrExchangeToSellerWmsMail.txt")]
    public class NewReturnOrExchangeToSellerWmsMail : TemplateBase
    {
        public NewReturnOrExchangeToSellerWmsMail() : base() { }

        protected override void Initialize()
        {
            _items.Add("return_info", new NewReturnOrExchangeToSellerMail.ProductRefundOrExchangeModel());
            
            _items.Add("site_url", string.Empty);
            _items.Add("https_site_url", string.Empty);
            _items.Add("site_service_url", string.Empty);
            _items.Add("ship_infos", string.Empty);
            _items.Add("ship_return_date", string.Empty);
        }
        
        /// <summary>
        /// 退/換貨資料
        /// </summary>
        public List<NewReturnOrExchangeToSellerMail.ProductRefundOrExchangeModel> ReturnInfo
        {
            get { return (List<NewReturnOrExchangeToSellerMail.ProductRefundOrExchangeModel>)_items["return_info"]; }
            set { _items["return_info"] = value; }
        }
        
        public string SiteUrl
        {
            get { return (string)_items["site_url"]; }
            set { _items["site_url"] = value; }
        }
        public string HttpsSiteUrl
        {
            get { return (string)_items["https_site_url"]; }
            set { _items["https_site_url"] = value; }
        }
        public string SiteServiceUrl
        {
            get { return (string)_items["site_service_url"]; }
            set { _items["site_service_url"] = value; }
        }
        public string VbsUrl
        {
            get { return (string)_items["vbs_url"]; }
            set { _items["vbs_url"] = value; }
        }

        public string OrderId
        {
            get { return (string)_items["order_id"]; }
            set { _items["order_id"] = value; }
        }

        public List<ShipInfo> ShipInfos
        {
            get { return (List<ShipInfo>)_items["ship_infos"]; }
            set { _items["ship_infos"] = value; }
        }

        public string ShipReturnDate
        {
            get { return (string)_items["ship_return_date"]; }
            set { _items["ship_return_date"] = value; }
        }

        public string CheckDeadLine
        {
            get { return (string)_items["check_deadline"]; }
            set { _items["check_deadline"] = value; }
        }

        /// <summary>
        /// 物流資訊
        /// </summary>
        public class ShipInfo
        {
            public string ShipId { get; set; }
            public string LogisticName { get; set; }
            public string ShipCompanySite { get; set; }
        }
    }
}

﻿using System;
using System.Collections.Generic;

namespace LunchKingSite.Core.Component.Template
{
    [TemplateFile("famidealdelivertimealert.txt")]
    public class FamiDealDeliverTimeAlert : TemplateBase
    {
        ISysConfProvider cp = ProviderFactory.Instance().GetConfig();
        public FamiDealDeliverTimeAlert() : base() { }

        protected override void Initialize()
        {
            _items["memberName"] = string.Empty;
            _items["deliverTime"] = string.Empty;
            _items["dealTag"] = string.Empty;
            _items["siteUrl"] = cp.SiteUrl;
            _items["siteServiceUrl"] = cp.SiteServiceUrl;
        }

        public string MemberName
        {
            get { return (string)_items["memberName"]; }
            set { _items["memberName"] = value; }
        }

        public string DeliverTime
        {
            get { return (string)_items["deliverTime"]; }
            set { _items["deliverTime"] = value; }
        }

        public string DealTag
        {
            get { return (string)_items["dealTag"]; }
            set { _items["dealTag"] = value; }
        }
    }
}

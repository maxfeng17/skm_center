﻿using LunchKingSite.DataOrm;
using System;
using System.Linq;
using System.Collections.Generic;

namespace LunchKingSite.Core.Component.Template
{
    public class DealNotify : TemplateBase
    {
        public ISysConfProvider ServerConfig { get; set; }

        public DealNotify()
            : base()
        {
        }

        private ISysConfProvider cp = ProviderFactory.Instance().GetConfig();
        protected ViewPponDeal _theDeal = null;

        public ViewPponDeal TheDeal
        {
            get
            {
                return _theDeal;
            }
            set
            {
                _theDeal = value;
            }
        }

        protected ViewPponCoupon _theOrderDetail = null;

        public ViewPponCoupon TheOrderDetail
        {
            get
            {
                return _theOrderDetail;
            }
            set
            {
                _theOrderDetail = value;
            }
        }

        protected List<ViewPponDeal> _promoDeals = null;
        public List<ViewPponDeal> PromoDeals
        {
            get { return _promoDeals; }
            set { _promoDeals = value; }
        }

        protected override void Initialize()
        {
            _callback = new PrepareItemCallback(PrepareItem);
        }

        protected virtual void PrepareItem()
        {
            _items["site_url"] = cp.SiteUrl;
            _items["site_service_url"] = cp.SiteServiceUrl;
            PrepareDealInfo();
            PrepareOrderDetailInfo();
            PreparePromoDealsInfo();
        }

        protected void PreparePromoDealsInfo()
        {
            // 先建立預設值
            int dealsCount = ServerConfig.PromoMailDealsCount;
            for (int i = 0; i < dealsCount; i++)
            {
                _items["deal" + (i + 1) + "url"] = string.Empty;
                _items["deal" + (i + 1) + "pic"] = string.Empty;
                _items["deal" + (i + 1) + "title"] = string.Empty;
                _items["deal" + (i + 1) + "price"] = string.Empty;
            }

            // 依序設定檔次資料
            for (int i = 0; i < _promoDeals.Count; i++)
            {
                _items["deal" + (i + 1) + "url"] = Helper.GetDealDeepLink(_promoDeals[i].BusinessHourGuid, "17_EDMpay");
                _items["deal" + (i + 1) + "pic"] = Helper.GetImgUrl(_promoDeals[i].EventImagePath);
                _items["deal" + (i + 1) + "title"] = (_promoDeals[i].ItemName.Length > 31) ? (_promoDeals[i].ItemName.Substring(0, 31) + "...") : _promoDeals[i].ItemName;
                _items["deal" + (i + 1) + "price"] = Convert.ToInt32(_promoDeals[i].ItemPrice).ToString("D") + (Helper.IsFlagSet(_promoDeals[i].BusinessHourStatus, BusinessHourStatus.ComboDealMain) ? "起" : string.Empty);
            }

            // 必須有檔次數才要顯示
            _items["show_promo"] = _promoDeals.Count() > 0;
        }

        protected void PrepareDealInfo()
        {
            if (_theDeal != null)
            {
                _items["item_name"] = _theDeal.ItemName;
                _items["date"] = _theDeal.BusinessHourDeliverTimeE.Value.ToString("yyyy/MM/dd");

                if (TheDeal.DeliveryType.HasValue && int.Equals((int)DeliveryType.ToHouse, TheDeal.DeliveryType.Value))
                {
                    _items["inquire"] = string.Empty;
                }
                else
                {
                    _items["inquire"] = @"<span style=""color:#F00; "">此Email無法做為憑證使用，請勿持此Email至店家消費</span><br/>" +
                                            @"好康成立後，<a href=""" + cp.SiteUrl + @"/user/coupon_List.aspx"" style=""text-decoration:underline; color:#BF0000"" target=""_blank"">請到此下載好康憑證>></a>";
                    string deliver_time = _theDeal.BusinessHourDeliverTimeS.GetValueOrDefault().ToString("yyyy/MM/dd") +
                                          "~" +
                                          _theDeal.BusinessHourDeliverTimeE.GetValueOrDefault().ToString("yyyy/MM/dd");
                    _items["download_credit"] =
                        @"使用期限：<span style=""color:#F60;font-family:Arial, Helvetica, sans-serif;"">" + deliver_time + @"</span><br/>" +
                        @"<a href=""" + cp.SiteUrl + @"/user/coupon_List.aspx"" style=""font-size:18px; color:#BF0000; margin-left:110px; text-decoration:underline; line-height:40px;"" target=""_blank"">下載好康憑證</a><br/>";
                }
            }
        }

        protected void PrepareOrderDetailInfo()
        {
            if (_theOrderDetail != null)
            {
                _items["member_name"] = _theOrderDetail.MemberName;
            }
        }

        private string GetImgUrl(string path)
        {
            return ((string.IsNullOrWhiteSpace(path)) ? Helper.GetMediaPathsFromRawData(ServerConfig.EDMDefaultImage, MediaType.PponDealPhoto, ServerConfig.MediaBaseUrl).DefaultIfEmpty(string.Empty).First() : Helper.GetMediaPathsFromRawData(path, MediaType.PponDealPhoto, ServerConfig.MediaBaseUrl).DefaultIfEmpty(string.Empty).First());
        }
    }
}
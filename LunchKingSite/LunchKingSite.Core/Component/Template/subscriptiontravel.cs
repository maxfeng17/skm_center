﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using LunchKingSite.Core.Models;
using LunchKingSite.DataOrm;
using LunchKingSite.I18N;


namespace LunchKingSite.Core.Component.Template
{
    public class SubscriptionTravel : TemplateBase
    {
        ISysConfProvider cp = ProviderFactory.Instance().GetConfig();
        public SubscriptionTravel() : base() { }

        protected override void Initialize()
        {
            _callback = new PrepareItemCallback(PrepareItem);
        }

        protected virtual void PrepareItem()
        {
            _items["site_url"] = cp.SiteUrl;
            _items["site_service_url"] = cp.SiteServiceUrl;
        }
    }
}

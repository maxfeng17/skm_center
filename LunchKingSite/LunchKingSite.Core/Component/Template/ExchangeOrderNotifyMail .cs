﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.Core.Component.Template
{

    [TemplateFile("ExchangeOrderNotifyMail.txt")]
    public class ExchangeOrderNotifyMail : TemplateBase
    {
        protected override void Initialize()
        {
            _items.Add("exchangeOrderList", null);
            _items.Add("seller", null);
            _items.Add("site_url", null);
            _items.Add("site_service_url", null);
        }

        public string ExchangeOrderList
        {
            get { return (string)_items["exchangeOrderList"]; }
            set { _items["exchangeOrderList"] = value; }
        }

        public string Seller
        {
            get { return (string)_items["seller"]; }
            set { _items["seller"] = value; }
        }

        public string SiteUrl
        {
            get { return (string)_items["site_url"]; }
            set { _items["site_url"] = value; }
        }

        public string SiteServiceUrl
        {
            get { return (string)_items["site_service_url"]; }
            set { _items["site_service_url"] = value; }
        }
    }
}

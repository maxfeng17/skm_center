﻿using System;
using System.Linq;
using LunchKingSite.DataOrm;
using System.Collections.Generic;

namespace LunchKingSite.Core.Component.Template
{
    public class DiscountCodeSendMail : TemplateBase
    {
        public ISysConfProvider ServerConfig { get; set; }
        public DiscountCodeSendMail()
            : base()
        {
        }

        protected List<IViewPponDeal> _cityDeal = null;
        public List<IViewPponDeal> CityDeal
        {
            get { return _cityDeal; }
            set { _cityDeal = value; }
        }

        protected string _memberName = null;
        public string MemberName
        {
            get { return _memberName; }
            set { _memberName = value; }
        }

        protected string _eventName = null;
        public string EventName
        {
            get { return _eventName; }
            set { _eventName = value; }
        }

        protected string _amount = null;
        public string Amount
        {
            get { return _amount; }
            set { _amount = value; }
        }

        protected string _code = null;
        public string Code
        {
            get { return _code; }
            set { _code = value; }
        }

        protected string _startdate = null;
        public string StartDate
        {
            get { return _startdate; }
            set { _startdate = value; }
        }

        protected string _enddate = null;
        public string EndDate
        {
            get { return _enddate; }
            set { _enddate = value; }
        }

        protected string _discountLimit = null;
        public string DiscountLimit
        {
            get { return _discountLimit; }
            set { _discountLimit = value; }
        }

        protected string _promoInfo = null;
        public string PromoInfo
        {
            get { return _promoInfo; }
            set { _promoInfo = value; }
        }

        protected override void Initialize()
        {
            _callback = new PrepareItemCallback(PrepareItem);
        }

        protected virtual void PrepareItem()
        {
            _items["site_url"] = ServerConfig.SiteUrl;
            _items["site_service_url"] = ServerConfig.SiteServiceUrl;

            PrepareDiscountCodeInfo();
            PreparePromoDealsInfo();
        }

        protected void PreparePromoDealsInfo()
        {
            #region 推薦好康
            // 檔次不夠時設定預設值
            int dealsCount = ServerConfig.PromoMailDealsCount;
            for (int i = ServerConfig.PromoMailDealsCount; i - _cityDeal.Count > 0; i--)
            {
                _items["deal" + i + "title"] = string.Empty;
                _items["deal" + i + "url"] = string.Empty;
                _items["deal" + i + "price"] = string.Empty;
                _items["deal" + i + "pic"] = string.Empty;
            }

            // 依序設定檔次資料
            for (int i = 0; i < _cityDeal.Count; i++)
            {
                _items["deal" + (i + 1) + "title"] = (_cityDeal[i].ItemName.Length > 31) ? (_cityDeal[i].ItemName.Substring(0, 31) + "...") : _cityDeal[i].ItemName;
                _items["deal" + (i + 1) + "url"] = ServerConfig.SiteUrl + "/deal/" + _cityDeal[i].BusinessHourGuid.ToString() + "?rsrc=17_EDMpay";
                _items["deal" + (i + 1) + "price"] = Convert.ToInt32(_cityDeal[i].ItemPrice).ToString("D") + (Helper.IsFlagSet(_cityDeal[i].BusinessHourStatus, BusinessHourStatus.ComboDealMain) ? "起" : string.Empty);
                _items["deal" + (i + 1) + "pic"] = Helper.GetImgUrl(_cityDeal[i].EventImagePath);
            }
            #endregion
        }

        protected void PrepareDiscountCodeInfo()
        {
            _items["member_name"] = _memberName;
            _items["event_name"] = _eventName;
            _items["amount"] = _amount;
            _items["code"] = _code;
            _items["start_date"] = _startdate;
            _items["end_date"] = _enddate;
            _items["discount_limit"] = _discountLimit;
            _items["promoInfo"] = _promoInfo;
        }
    }
}
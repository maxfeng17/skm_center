﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace LunchKingSite.Core.Component.Template
{
    [TemplateFile("ExpiringOrderToSellerMail.txt")]
    public class ExpiringOrderToSellerMail : TemplateBase
    {
        public ExpiringOrderToSellerMail() : base() { }

        protected override void Initialize()
        {
            _items.Add("seller_name", string.Empty);
            _items.Add("vbs_ship_url", string.Empty);
            _items.Add("expiring_orders", new List<ExpiringOrder>());
            
            _items.Add("site_url", string.Empty);
            _items.Add("https_site_url", string.Empty);
            _items.Add("site_service_url", string.Empty);
            _items.Add("total_fine_amount", 0);
        }

        /// <summary>
        /// 是否逾期
        /// </summary>
        public bool IsExpired
        {
            get { return (bool)_items["is_expired"]; }
            set { _items["is_expired"] = value; }
        }
        public string SellerName
        {
            get { return (string)_items["seller_name"]; }
            set { _items["seller_name"] = value; }
        }
        public string VbsShipUrl
        {
            get { return (string)_items["vbs_ship_url"]; }
            set { _items["vbs_ship_url"] = value; }
        }

        /// <summary>
        /// 逾期/即將逾期訂單
        /// </summary>
        public List<ExpiringOrder> ExpiringOrders
        {
            get { return (List<ExpiringOrder>)_items["expiring_orders"]; }
            set { _items["expiring_orders"] = value; }
        }
        
        public string SiteUrl
        {
            get { return (string)_items["site_url"]; }
            set { _items["site_url"] = value; }
        }
        public string HttpsSiteUrl
        {
            get { return (string)_items["https_site_url"]; }
            set { _items["https_site_url"] = value; }
        }
        public string SiteServiceUrl
        {
            get { return (string)_items["site_service_url"]; }
            set { _items["site_service_url"] = value; }
        }
        public int TotalFineAmount
        {
            get { return (int)_items["total_fine_amount"]; }
            set { _items["total_fine_amount"] = value; }
        }

        /// <summary>
        /// 逾期/即將逾期訂單
        /// </summary>
        public class ExpiringOrder
        {
            /// <summary>
            /// 序號
            /// </summary>
            public int SeqNo { get; set; }
            /// <summary>
            /// 上架日
            /// </summary>
            public string StartOrderDate { get; set; }
            /// <summary>
            /// 下架日
            /// </summary>
            public string EndOrderDate { get; set; }
            /// <summary>
            /// 出貨條件
            /// </summary>
            public string ShippingCondition { get; set; }
            /// <summary>
            /// 最晚出貨日
            /// </summary>
            public string LastShipDate { get; set; }
            /// <summary>
            /// 檔號
            /// </summary>
            public int DealUniqueId { get; set; }
            /// <summary>
            /// 商品名稱
            /// </summary>
            public string ItemName { get; set; }
            /// <summary>
            /// 逾期/即將逾期訂單數
            /// </summary>
            public int OrderQty { get; set; }
            /// <summary>
            /// 商家出貨連結
            /// </summary>
            public string VbsUrl { get; set; }
            /// <summary>
            /// 逾期/即將逾期罰款金額
            /// </summary>
            public int FineAmount { get; set; }
            /// <summary>
            /// 訂單編號
            /// </summary>
            public string OrderId { get; set; }
            /// <summary>
            /// 配送方式
            /// </summary>
            public string DeliveryTypeDesc { get; set; }
            /// <summary>
            /// 商家出貨連結
            /// </summary>
            public string VbsShipUrl { get; set; }
            
            public ExpiringOrder()
            {
                
            }
        }
    }
}

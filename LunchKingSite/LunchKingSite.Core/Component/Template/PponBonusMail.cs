﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.Core.Component.Template
{
    public class PponBonusMail : TemplateBase
    {
        ISysConfProvider cp = ProviderFactory.Instance().GetConfig();

        public PponBonusMail()
            : base()
        {
        }

        protected string _addressee = "";
        public string Addressee
        {
            get
            {
                return _addressee;
            }
            set
            {
                _addressee = value;
            }
        }

        protected string _inviteeName = "";
        public string InviteeName
        {
            get
            {
                return _inviteeName;
            }
            set
            {
                _inviteeName = value;
            }

        }

        protected double _promotionValue = 0;
        public double PromotionValue
        {
            get
            {
                return _promotionValue;
            }
            set
            {
                _promotionValue = value;
            }
        }





        protected override void Initialize()
        {
            _callback = new PrepareItemCallback(PrepareItem);
        }

        protected virtual void PrepareItem()
        {
            _items["mailto"] = Addressee;
            _items["Invitee"] = InviteeName;
            _items["PromotionValue"] = string.Format("{0:$###,##0}", (PromotionValue/10));
            _items["site_url"] =cp.SiteUrl;
            _items["site_service_url"] = cp.SiteServiceUrl;
        }
    }
}

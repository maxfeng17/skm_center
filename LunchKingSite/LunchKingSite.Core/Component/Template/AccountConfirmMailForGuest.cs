﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.Core.Component.Template
{
    [TemplateFile("accountconfirmmailforguest.txt")]
    public class AccountConfirmMailForGuest : TemplateBase
    {
        public AccountConfirmMailForGuest() : base() { }

        protected override void Initialize()
        {
            _items.Add("member_mail", null);
            _items.Add("member_uid", null);
            _items.Add("auth_code", null);
            _items.Add("auth_key", null);
            _items.Add("site_url", null);
            _items.Add("site_service_url", null);
            _items.Add("https_site_url", null);
        }

        public string MemberMail
        {
            get { return (string)_items["member_mail"]; }
            set { _items["member_mail"] = value; }
        }

        public string MemberUid
        {
            get { return (string)_items["member_uid"]; }
            set { _items["member_uid"] = value; }
        }

        public string AuthCode
        {
            get { return (string)_items["auth_code"]; }
            set { _items["auth_code"] = value; }
        }

        public string AuthKey
        {
            get { return (string)_items["auth_key"]; }
            set { _items["auth_key"] = value; }
        }

        public string SiteUrl
        {
            get { return (string)_items["site_url"]; }
            set { _items["site_url"] = value; }
        }

        public string SiteServiceUrl
        {
            get { return (string)_items["site_service_url"]; }
            set { _items["site_service_url"] = value; }
        }

        public string HttpsSiteUrl
        {
            get { return (string)_items["https_site_url"]; }
            set { _items["https_site_url"] = value; }
        }
    
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.Core.Component.Template
{
    [TemplateFile("ProductBalanceSheetCreate.txt")]
    public class ProductBalanceSheetCreate : TemplateBase
    {
        public ProductBalanceSheetCreate() : base() { }

        protected override void Initialize()
        {
            base.Initialize();

            _items["sellerName"] = string.Empty;
            _items["dealInfo"] = string.Empty;
            _items["siteUrl"] = string.Empty;
            _items["siteServiceUrl"] = string.Empty;
        }

        public string SellerName
        {
            get { return (string)_items["sellerName"]; }
            set { _items["sellerName"] = value; }
        }

        public string DealInfo
        {
            get { return (string)_items["dealInfo"]; }
            set { _items["dealInfo"] = value; }
        }

        public string SiteUrl
        {
            get { return (string)_items["siteUrl"]; }
            set { _items["siteUrl"] = value; }
        }

        public string SiteServiceUrl
        {
            get { return (string)_items["siteServiceUrl"]; }
            set { _items["siteServiceUrl"] = value; }
        }
    }
}

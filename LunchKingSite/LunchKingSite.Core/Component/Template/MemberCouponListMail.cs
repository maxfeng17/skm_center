﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.DataOrm;

namespace LunchKingSite.Core.Component.Template
{
    [TemplateFile("membercouponlistmail.txt")]
    public class MemberCouponListMail : TemplateBase
    {
        public MemberCouponListMail()
            : base()
        {
        }

        #region Property

        public string UserEmail
        {
            get { return (string)_items["user_email"]; }
            set { _items["user_email"] = value; }
        }

        public string UserName
        {
            get { return (string)_items["user_name"]; }
            set { _items["user_name"] = value; }
        }

        public string AuthLink
        {
            get { return (string) _items["auth_link"]; }
            set { _items["auth_link"] = value; }
        }

        public bool IsGuest
        {
            get { return (bool)_items["is_guest"]; }
            set { _items["is_guest"] = value; }
        }

        public string CashPoint
        {
            get { return (string)_items["cash_point"]; }
            set { _items["cash_point"] = value; }
        }

        public string BonusPoint
        {
            get { return (string)_items["bonus_point"]; }
            set { _items["bonus_point"] = value; }
        }

        public string PayeasyCash
        {
            get { return (string)_items["payeasy_cash"]; }
            set { _items["payeasy_cash"] = value; }
        }

        public string DuringString
        {
            get { return (string)_items["during_string"]; }
            set { _items["during_string"] = value; }
        }

        public string DuringDate
        {
            get { return (string)_items["during_date"]; }
            set { _items["during_date"] = value; }
        }

        public List<MailCouponListMain> MainList
        {
            get { return (List<MailCouponListMain>)_items["main_list"]; }
            set { _items["main_list"] = value; }
        }

        public List<MailCouponListMain> MainListNotUsed
        {
            get { return (List<MailCouponListMain>)_items["main_list_NotUsed"]; }
            set { _items["main_list_NotUsed"] = value; }
        }

        public List<MailCouponListMain> MainListUsed
        {
            get { return (List<MailCouponListMain>)_items["main_list_Used"]; }
            set { _items["main_list_Used"] = value; }
        }

        public List<MailCouponListMain> MainListExpired
        {
            get { return (List<MailCouponListMain>)_items["main_list_expired"]; }
            set { _items["main_list_expired"] = value; }
        }

        public List<MailCouponListMain> MainListRefund
        {
            get { return (List<MailCouponListMain>)_items["main_list_refund"]; }
            set { _items["main_list_refund"] = value; }
        }
        
        #endregion
        
        protected override void Initialize()
        {
            base.Initialize();

            _items.Add("column_cnt", 0);
            _items.Add("user_email", string.Empty);
            _items.Add("user_name", string.Empty);
            _items.Add("auth_link", string.Empty);
            _items.Add("is_guest", true);
            _items.Add("cash_point", string.Empty);
            _items.Add("bonus_point", string.Empty);
            _items.Add("payeasy_cash", string.Empty);
            _items.Add("during_string", string.Empty);
            _items.Add("during_date", string.Empty);
            _items.Add("main_list", new List<MailCouponListMain>());

            PrepareOrderInfo();

            _callback = new PrepareItemCallback(PrepareItem);
        }

        protected void PrepareOrderInfo()
        {
            _items["user_email"] = UserEmail;
            _items["user_name"] = UserName;
            _items["auth_link"] = AuthLink;
            _items["is_guest"] = IsGuest;
            _items["cash_point"] = CashPoint;
            _items["bonus_point"] = BonusPoint;
            _items["payeasy_cash"] = PayeasyCash;
            _items["during_string"] = DuringString;
            _items["during_date"] = DuringDate;
        }

        protected virtual void PrepareItem()
        {
            _items["main_list_NotUsed"] = MainList.Where(x => x.StatusType.Contains((int)MailCouponListStatusType.NotUsed)).OrderByDescending(x=>x.OrderDate).ToList();
            _items["main_list_Used"] = MainList.Where(x => x.StatusType.Contains((int)MailCouponListStatusType.Used)).OrderByDescending(x => x.OrderDate).ToList();
            _items["main_list_expired"] = MainList.Where(x => x.StatusType.Contains((int)MailCouponListStatusType.Expired)).OrderByDescending(x => x.OrderDate).ToList();
            _items["main_list_refund"] = MainList.Where(x => x.StatusType.Contains((int)MailCouponListStatusType.Refund)).OrderByDescending(x => x.OrderDate).ToList();
        }

    }

    #region class
    
    public class MailCouponListMain
    {
        /// <summary>
        /// 訂單GUID
        /// </summary>
        public Guid OrderGuid { set; get; }

        /// <summary>
        /// 訂購日期
        /// </summary>
        public DateTime OrderDate { set; get; }

        /// <summary>
        /// 訂單編號
        /// </summary>
        public string OrderId { set; get; }

        /// <summary>
        /// 好康名稱
        /// </summary>
        public string OrderName { set; get; }

        /// <summary>
        /// 好康連結
        /// </summary>
        public string OrderNameLink { set; get; }

        /// <summary>
        /// 好康說明
        /// </summary>
        public string OrderNameTag { set; get; }

        /// <summary>
        /// 使用期限
        /// </summary>
        public string OrderExp { set; get; }

        /// <summary>
        /// 訂單狀態
        /// </summary>
        public string OrderStatus { set; get; }

        /// <summary>
        /// 是否可下載憑證
        /// </summary>
        public bool IsDownloadCouponPdf { set; get; }

        /// <summary>
        /// 憑證明細
        /// </summary>
        public List<MailCouponListDetail> CouponListDetail { set; get; }

        /// <summary>
        /// 訂單狀態別(一張訂單內，其所有憑證明細狀態別的集合)
        /// </summary>
        public List<int> StatusType { set; get; }
    }

    public class MailCouponListDetail
    {
        /// <summary>
        /// 憑證建立日
        /// </summary>
        public DateTime OrderDetailCreatime { set; get; }

        /// <summary>
        /// 憑證編號
        /// </summary>
        public string OrderSn { set; get; }

        /// <summary>
        /// 確認碼
        /// </summary>
        public string OrderCode { set; get; }

        /// <summary>
        /// 憑證狀態
        /// </summary>
        public string CouponStatus { set; get; }

        /// <summary>
        /// 憑證狀態別
        /// </summary>
        public int StatusType { set; get; }

        /// <summary>
        /// 是否可下載憑證
        /// </summary>
        public bool IsEnabledDownLoad { set; get; }

        /// <summary>
        /// 下載憑證連結
        /// </summary>
        public string DownloadLink { set; get; }
    }
    
    #endregion
}

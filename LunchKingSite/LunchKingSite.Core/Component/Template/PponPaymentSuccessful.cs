﻿using System;
using System.Linq;
using LunchKingSite.DataOrm;
using System.Collections.Generic;
using System.Text;
using System.Web;
using LunchKingSite.Core;

namespace LunchKingSite.Core.Component.Template
{
    public class PponPaymentSuccessful : TemplateBase
    {
        public ISysConfProvider ServerConfig { get; set; }
        ISysConfProvider cp = ProviderFactory.Instance().GetConfig();
        IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        public PponPaymentSuccessful()
            : base()
        {

        }

        protected IViewPponDeal _theDeal = null;
        public IViewPponDeal TheDeal
        {
            get { return _theDeal; }
            set { _theDeal = value; }
        }

        protected CashTrustLogCollection _theCashTrustLogCol = null;
        public CashTrustLogCollection TheCashTrustLogCol
        {
            get { return _theCashTrustLogCol; }
            set { _theCashTrustLogCol = value; }
        }


        public ViewPponOrderDetailCollection OrderDetailCol { get; set; }

        protected List<IViewPponDeal> _promoDeals = null;
        public List<IViewPponDeal> PromoDeals
        {
            get { return _promoDeals; }
            set { _promoDeals = value; }
        }

        public string StoreName { get; set; }

        public string StoreDetails { get; set; }

        public string ShipInfo { get; set; }
        
        public string IspShipInfo { get; set; }

        public bool IsSkmZeroDeal { get; set; }

        public string Restrictions { get; set; }

        public bool IsPresentCoupon { get; set; }

        public bool IsFamiportGroupCoupon { get; set; }
        public bool IsHiLifeGroupCoupon { get; set; }

        public bool IsIspOrderPickup { get; set; }

        public ViewPponCouponCollection PponCoupons { get; set; }

        protected override void Initialize()
        {
            _callback = new PrepareItemCallback(PrepareItem);
        }

        protected virtual void PrepareItem()
        {
            _items["site_url"] = VirtualPathUtility.AppendTrailingSlash(cp.SiteUrl);
            _items["site_service_url"] = cp.SiteServiceUrl;
           
            //全家成套票券(咖啡寄杯)
            if (Helper.IsFlagSet(TheDeal.BusinessHourStatus, BusinessHourStatus.GroupCoupon) &&
                (TheDeal.GroupOrderStatus & (int) GroupOrderStatus.PEZevent) > 0)
            {
                if ((TheDeal.GroupOrderStatus & (int) GroupOrderStatus.FamiDeal) > 0)
                {
                    IsFamiportGroupCoupon = true;
                }
                else if ((TheDeal.GroupOrderStatus & (int) GroupOrderStatus.HiLifeDeal) > 0)
                {
                    IsHiLifeGroupCoupon = true;
                }
            }

            if (TheDeal.DeliveryType == (int)DeliveryType.ToShop)
            {
                PrepareCoupons();
            }
            else if (TheDeal.DeliveryType == (int)DeliveryType.ToHouse)
            {
                PrepareOrderDetails();
            }
            else
            {
                throw new Exception(string.Format("檔次設定有誤, bid= {0}, delivertType={1}",
                    TheDeal.BusinessHourGuid, TheDeal.DeliveryType));
            }
            PreparePaymentTransInfo();
            PreparePromoDealsInfo();

            _items["store_name"] = StoreName;
            _items["store_details"] = StoreDetails;
            _items["deal_type"] = TheDeal.DeliveryType.GetValueOrDefault();
            _items["is_skm_zero_deal"] = IsSkmZeroDeal;
            _items["deal_restrictions"] = Restrictions;
            _items["is_present_coupon"] = IsPresentCoupon;
            _items["is_famiport_group_coupon"] = IsFamiportGroupCoupon;
            _items["is_hilife_group_coupon"] = IsHiLifeGroupCoupon; 
             _items["is_isp_order_pickup"] = IsIspOrderPickup;//超取
            SetOtherItem();
        }

        private void PrepareOrderDetails()
        {
            List<CouponInfo> coupons = new List<CouponInfo>();
            foreach (ViewPponOrderDetail vpod in OrderDetailCol)
            {
                CouponInfo c = new CouponInfo();
                c.DealName = vpod.CouponUsage;
                c.Price = vpod.ItemUnitPrice.ToString("N0");
                c.OrderId = vpod.OrderId;
                c.DealUrl = Helper.CombineUrl(ServerConfig.SiteUrl, "/deal/" + TheDeal.BusinessHourGuid);
                c.ItemQuantity = vpod.ItemQuantity;
                c.ItemName = vpod.ItemName;
                c.ShipInfo = ShipInfo;
                c.IspShipInfo = IspShipInfo;
                coupons.Add(c);
                //if (IsIspOrderPickup)
                //{
                //    var vpcm = mp.GetCouponListMainByOid(vpod.OrderGuid);
                //    if (vpcm.IsLoaded)
                //    {
                //        switch (vpcm.ProductDeliveryType)
                //        {
                //            case (int)ProductDeliveryType.FamilyPickup:
                //                c.IspShipInfo = "<p> 全家 " + vpcm.FamilyStoreName + "<br />" + vpcm.FamilyStoreAddr + "</p>";
                //                break;
                //            case (int)ProductDeliveryType.SevenPickup:
                //                c.IspShipInfo = "<p> 7-11 " + vpcm.SevenStoreName + "<br />" + vpcm.SevenStoreAddr + "</p>";
                //                break;
                //        }
                //    }
                //}
            }

            _items["coupons"] = coupons;
            _items["member_name"] = OrderDetailCol[0].MemberName;

        }

        private void PrepareCoupons()
        {
            List<CouponInfo> coupons = new List<CouponInfo>();
            foreach (ViewPponCoupon vpc in PponCoupons)
            {
                coupons.Add(new CouponInfo
                {
                    DealName = vpc.CouponUsage,
                    Price = vpc.ItemUnitPrice.ToString("N0"),
                    OrderId = vpc.OrderId,
                    CouponCode = vpc.CouponStatus == (int)CouponStatus.Locked ? "".PadLeft(vpc.CouponCode.Length, '*') : vpc.CouponCode,
                    CouponSeq = vpc.SequenceNumber,
                    ExpiredDate = (IsFamiportGroupCoupon || IsHiLifeGroupCoupon) ? vpc.CouponExpiredDate.ToString("yyyy/MM/dd HH:mm") : vpc.CouponExpiredDate.ToString("yyyy/MM/dd"),
                    Link = Helper.CombineUrl(ServerConfig.SiteUrl, vpc.CouponGetUrl),
                    DealUrl = Helper.CombineUrl(ServerConfig.SiteUrl, "/deal/" + TheDeal.BusinessHourGuid),
                    ItemQuantity = vpc.ItemQuantity,
                    ItemName = (vpc.CouponUsage != "" ? vpc.ItemName.Replace(vpc.CouponUsage, "") : vpc.ItemName), //修正問題：String cannot be of zero length
                    CouponStatus = vpc.CouponStatus ?? 0
                });
            }

            _items["coupons"] = coupons;
            _items["member_name"] = PponCoupons[0].MemberName;
        }

        protected void PreparePromoDealsInfo()
        {
            // 依序設定檔次資料
            List<AdDeal> adDeals = new List<AdDeal>();
            for (int i = 0; i < _promoDeals.Count; i++)
            {
                adDeals.Add(new AdDeal
                {
                    Url = Helper.GetDealDeepLink(_promoDeals[i].BusinessHourGuid, "17_EDMpay"),
                    Pic = Helper.GetImgUrl(_promoDeals[i].EventImagePath),
                    Title = _promoDeals[i].ItemName.TrimToMaxLength(31, "..."),
                    Price = Convert.ToInt32(_promoDeals[i].ItemPrice).ToString("D") +
                            (Helper.IsFlagSet(_promoDeals[i].BusinessHourStatus, BusinessHourStatus.ComboDealMain) ? "起" : string.Empty)
                });
                if (adDeals.Count == ServerConfig.PromoMailDealsCount)
                {
                    break;
                }
            }
            _items["ad_deals"] = adDeals;

        }

        protected void PreparePaymentTransInfo()
        {
            _items["credit"] = string.Empty;
            _items["atm"] = string.Empty;
            _items["family_isp"] = string.Empty;
            _items["seven_isp"] = string.Empty;
            _items["pcash"] = string.Empty;
            _items["bcash"] = string.Empty;
            _items["scash"] = string.Empty;
            _items["pscash"] = string.Empty;
            _items["discount"] = string.Empty;
            _items["tcash"] = string.Empty;
            _items["scashModel"] = string.Empty;
            _items["pscashModel"] = string.Empty;
            _items["bcashModel"] = string.Empty;
            _items["pcashModel"] = string.Empty;
            _items["discountModel"] = string.Empty;
            _items["atmModel"] = string.Empty;
            _items["seven_ispModel"] = string.Empty;
            _items["family_ispModel"] = string.Empty;
            _items["creditModel"] = string.Empty;
            _items["tcashModel"] = string.Empty;
            _items["order_date"] = _theCashTrustLogCol.First().CreateTime.ToString("yyyy/MM/dd");
            _items["amount"] = "NT$" + _theCashTrustLogCol.Sum(x => x.Amount).ToString("N0");
        }

        protected void SetOtherItem()
        {
            if (_theCashTrustLogCol.Count > 0)
            {
                int credit = _theCashTrustLogCol.Sum(x => x.CreditCard);
                int pcash = _theCashTrustLogCol.Sum(x => x.Pcash);
                int bonus = _theCashTrustLogCol.Sum(x => x.Bcash);
                int scash = _theCashTrustLogCol.Sum(x => x.Scash);
                int pscash = _theCashTrustLogCol.Sum(x => x.Pscash);
                int atm = _theCashTrustLogCol.Sum(x => x.Atm);
                int familyIsp = _theCashTrustLogCol.Sum(x => x.FamilyIsp);
                int sevenIsp = _theCashTrustLogCol.Sum(x => x.SevenIsp);
                int discount = _theCashTrustLogCol.Sum(x => x.DiscountAmount);
                int tcash = _theCashTrustLogCol.Sum(x => x.Tcash);
                
                ThirdPartyPayment thirdPartyPaymentSystem = tcash > 0 ? (ThirdPartyPayment)_theCashTrustLogCol.First(x => x.Tcash > 0).ThirdPartyPayment
                    : ThirdPartyPayment.None;
                /*
                    <li>
                    <span style="color:#666">17Life購物金</span>
                    <span style="font-family:Arial,Helvetica,sans-serif"> NT$50</span>
                    </li>
                 */
                if (scash > 0)
                {
                    _items["scash"] = string.Format(@"
                    <li>
                    <span style='color:#666'>購物金</span>
                    <span style='font-family:Arial,Helvetica,sans-serif'> NT${0}</span>
                    </li>", scash.ToString("N0"));

                    _items["scashModel"] = "<span style='color:#666'>購物金</span>";
                }
                if (pscash > 0)
                {
                    _items["pscash"] = string.Format(@"
                    <li>
                    <span style='color:#666'>由Payeasy兌換</span>
                    <span style='font-family:Arial,Helvetica,sans-serif'> NT${0}</span>
                    </li>", pscash.ToString("N0"));

                    _items["pscashModel"] = "<span style='color:#666'>由Payeasy兌換</span>";
                }
                if (bonus > 0)
                {
                    _items["bcash"] = string.Format(@"
                    <li>
                    <span style='color:#666'>紅利金</span>
                    <span style='font-family:Arial,Helvetica,sans-serif'> NT${0}</span>
                    </li>", bonus.ToString("N0"));
                    _items["bcashModel"] = "<span style='color:#666'>紅利金</span>";
                }
                if (pcash > 0)
                {
                    _items["pcash"] = string.Format(@"
                    <li>
                    <span style='color:#666'>Payeasy購物金</span>
                    <span style='font-family:Arial,Helvetica,sans-serif'> NT${0}</span>
                    </li>", pcash.ToString("N0"));
                    _items["pcashModel"] = "<span style='color:#666'>Payeasy購物金</span>";
                }
                if (discount > 0)
                {
                    _items["discount"] = string.Format(@"
                    <li>
                    <span style='color:#666'>折價券</span>
                    <span style='font-family:Arial,Helvetica,sans-serif'> NT${0}</span>
                    </li>", discount.ToString("N0"));
                    _items["discountModel"] = "<span style='color:#666'>折價券</span>";
                }
                if (atm > 0)
                {
                    _items["atm"] = string.Format(@"
                    <li>
                    <span style='color:#666'>ATM付款</span>
                    <span style='font-family:Arial,Helvetica,sans-serif'> NT${0}</span>
                    </li>", atm.ToString("N0"));
                    _items["atmModel"] = "<span style='color:#666'>ATM付款</span>";
                }
                if (familyIsp > 0)
                {
                    _items["family_isp"] = string.Format(@"
                    <li>
                    <span style='color:#666'>全家超商付款</span>
                    <span style='font-family:Arial,Helvetica,sans-serif'> NT${0}</span>
                    </li>", familyIsp.ToString("N0"));
                    _items["family_ispModel"] = "<span style='color:#666'>全家超商付款</span>";
                }
                if (sevenIsp > 0)
                {
                    _items["seven_isp"] = string.Format(@"
                    <li>
                    <span style='color:#666'>7-11超商付款</span>
                    <span style='font-family:Arial,Helvetica,sans-serif'> NT${0}</span>
                    </li>", sevenIsp.ToString("N0"));
                    _items["seven_ispModel"] = "<span style='color:#666'>7-11超商付款</span>";
                }
                if (credit > 0)
                {
                    _items["credit"] = string.Format(@"
                    <li>
                    <span style='color:#666'>信用卡刷卡</span>
                    <span style='font-family:Arial,Helvetica,sans-serif'> NT${0}</span>
                    </li>", credit.ToString("N0"));
                    _items["creditModel"] = "<span style='color:#666'>信用卡刷卡</span>";
                }
                if (tcash > 0)
                {
                    _items["tcash"] = string.Format(@"
                    <li>
                    <span style='color:#666'>{0}</span>
                    <span style='font-family:Arial,Helvetica,sans-serif'> NT${1}</span>
                    </li>", Helper.GetEnumDescription(thirdPartyPaymentSystem), tcash.ToString("N0"));

                    _items["tcashModel"] = string.Format(@"<span style='color:#666'>{0}</span>", Helper.GetEnumDescription(thirdPartyPaymentSystem));
                }

            }
        }

        public class CouponInfo
        {
            public string DealName { get; set; }
            public string OrderId { get; set; }
            public string CouponSeq { get; set; }
            public string CouponCode { get; set; }
            public string ExpiredDate { get; set; }
            public string Price { get; set; }
            public string Link { get; set; }
            public string DealUrl { get; set; }
            public int ItemQuantity { get; set; }
            public string ItemName { get; set; }
            public string ShipInfo { get; set; }
            public string IspShipInfo { get; set; }
            public int CouponStatus { get; set; }
            public override string ToString()
            {
                return string.Format("{0} {1}", DealName, CouponSeq);
            }
        }

        public class AdDeal
        {
            public string Url { get; set; }
            public string Pic { get; set; }
            public string Title { get; set; }
            public string Price { get; set; }

            public AdDeal()
            {
                this.Url = string.Empty;
                this.Pic = string.Empty;
                this.Title = string.Empty;
                this.Price = string.Empty;
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.DataOrm;

namespace LunchKingSite.Core.Component.Template
{
    public class PponNoticeMail : TemplateBase
    {
        public ISysConfProvider ServerConfig { get; set; }
        ISysConfProvider cp = ProviderFactory.Instance().GetConfig();
        public PponNoticeMail()
            : base()
        {
        }

        protected MailContentType _mailType;
        public MailContentType MailType
        {
            get { return _mailType; }
            set { _mailType = value; }
        }

       

        protected ViewPponOrderDetailCollection _theOrderDetailCol = null;
        public ViewPponOrderDetailCollection TheOrderDetailCol
        {
            get { return _theOrderDetailCol; }
            set { _theOrderDetailCol = value; }
        }

        /// <summary>
        /// 好康名稱
        /// </summary>
        public string ItemName { get; set; }

        protected override void Initialize()
        {
            _callback = new PrepareItemCallback(PrepareItem);
        }

        protected virtual void PrepareItem()
        {
            _items["site_url"] =cp.SiteUrl;
            _items["site_service_url"] = cp.SiteServiceUrl;
            _items["https_site_url"] = cp.SiteUrl.Replace("http", "https");
            _items["download"] = _items["statement"] = _items["warning"] = string.Empty;
            _items["fax"] = "<br />傳真號碼：(02)2511-9110";
            _items["order_id"] = _theOrderDetailCol[0].OrderId;
            _items["item_name"] = this.ItemName;
            _items["member_name"] = _theOrderDetailCol[0].MemberName;
            string discountLink = cp.SiteUrl + "/service/returndiscount.ashx?oid=" + _theOrderDetailCol[0].OrderGuid;
            string paperLink = cp.SiteUrl + "/service/returnapplicationform.ashx?oid=" + _theOrderDetailCol[0].OrderGuid;
            
            if (_mailType == MailContentType.RefundNoticeDiscount)
            {
                _items["statement"] = "您的退貨申請尚缺<span style=\"font-weight:bold; font-size:13px;\">退回或折讓證明單</span>，故本筆訂單尚未完成退貨作業。<br />還請您儘速將下列連結的<span style=\"font-weight:bold; font-size:13px;\">退回或折讓證明單</span>列印出來，如開立為三聯式發票，請務必填寫「營利事業統一編號」並蓋上公司發票章，以郵局掛號或傳真方式寄回17Life退貨處理中心，待我們收到資料後會立即為您處理退款作業。";
                _items["download"] = "<a href=\"" + discountLink + "\" target=\"_blank\" style=\"color:#08C;text-decoration:underline; font-weight:bold;\">下載退回或折讓證明單</a>";
            }
            else if (_mailType == MailContentType.RefundNoticePaper)
            {
                _items["statement"] = "您的退貨申請文件尚缺<span style=\"font-weight:bold; font-size:13px;\">退貨申請書</span>，故本筆訂單尚未完成退貨作業。<br />還請您儘速將下列連結的<span style=\"font-weight:bold; font-size:13px;\">退貨申請書</span>列印出來，以正楷簽名或蓋章（需與買受人相符），若開立為三聯式發票，請務必填寫「營利事業統一編號」並蓋上公司發票章，以郵局掛號或傳真方式寄回17Life退貨處理中心，待我們收到資料後會立即為您處理退款作業。";
                _items["download"] = "<a href=\"" + paperLink + "\" target=\"_blank\" style=\"color:#08C;text-decoration:underline; font-weight:bold;\">下載退貨申請書</a>";
            }
            else if (_mailType == MailContentType.RefundNoticeInvoice)
            {
                _items["statement"] = "您的退貨申請文件尚缺<span style=\"font-weight:bold; font-size:13px;\">紙本發票</span>，故本筆訂單尚未完成退貨作業。<br />還請您儘速將紙本發票郵寄掛號至17Life退貨處理中心，待我們收到資料後會立即為您處理退款作業。";
                _items["fax"] = string.Empty;
            }
            else if (_mailType == MailContentType.RefundNoticeDiscountInvoice)
            {
                _items["statement"] = "您的退貨申請文件尚缺<span style=\"font-weight:bold; font-size:13px;\">退回或折讓證明單</span>及<span style=\"font-weight:bold; font-size:13px;\">紙本發票</span>，故本筆訂單尚未完成退貨作業。<br />還請您儘速將下列連結的<span style=\"font-weight:bold; font-size:13px;\">退回或折讓證明單</span>列印出來，如開立為三聯式發票，請務必填寫「營利事業統一編號」並蓋上公司發票章，連同紙本發票以郵局掛號或傳真方式寄回17Life退貨處理中心，待我們收到資料後會立即為您處理退款作業。";
                _items["download"] = "<a href=\"" + discountLink + "\" target=\"_blank\" style=\"color:#08C;text-decoration:underline; font-weight:bold;\">下載退回或折讓證明單</a>";
            }
            else if (_mailType == MailContentType.RefundNoticeDiscountPaper)
            {
                _items["statement"] = "您的退貨申請文件尚缺<span style=\"font-weight:bold; font-size:13px;\">退貨申請書</span>及<span style=\"font-weight:bold; font-size:13px;\">退回或折讓證明單</span>，故本筆訂單尚未完成退貨作業。<br />還請您儘速將下列連結的<span style=\"font-weight:bold; font-size:13px;\">退貨申請書</span>及<span style=\"font-weight:bold; font-size:13px;\">退回或折讓證明單</span>列印出來，退貨申請書僅需正楷簽名或蓋章，若開立為三聯式發票，均請務必填寫「營利事業統一編號」並蓋上公司發票章，以郵局掛號或傳真方式一併寄回17Life退貨處理中心，待我們收到資料後會立即為您處理退款作業。";
                _items["download"] = "<a href=\"" + paperLink + "\" target=\"_blank\" style=\"color:#08C;text-decoration:underline; font-weight:bold;\">下載退貨申請書</a>" + I18N.Phrase.HtmlNewLine +
                                    "<a href=\"" + discountLink + "\" target=\"_blank\" style=\"color:#08C;text-decoration:underline; font-weight:bold;\">下載退回或折讓證明單</a>";
            }
            else if (_mailType == MailContentType.RefundNoticeInvoicePaper)
            {
                _items["statement"] = "您的退貨申請文件尚缺<span style=\"font-weight:bold; font-size:13px;\">退貨申請書</span>及<span style=\"font-weight:bold; font-size:13px;\">紙本發票</span>，故本筆訂單尚未完成退貨作業。<br />還請您儘速將下列連結的<span style=\"font-weight:bold; font-size:13px;\">退貨申請書</span>列印出來，以正楷簽名或蓋章（需與買受人相符），若開立為三聯式發票，請務必填寫「營利事業統一編號」並蓋上公司發票章，與紙本發票以郵局掛號或傳真方式一併寄回17Life退貨處理中心，待我們收到資料後會立即為您處理退款作業。";
                _items["download"] = "<a href=\"" + paperLink + "\" target=\"_blank\" style=\"color:#08C;text-decoration:underline; font-weight:bold;\">退貨申請書</a>";
            }
            if (_mailType == MailContentType.ReturnApplicationformResend)
            {
                _items["statement"] = "請您將［退貨申請書］列印出來，以正楷簽名或蓋章（需與買受人相符），若開立為三聯式發票/代收轉付收據，請務必填寫「營利事業統一編號」並蓋上公司發票/代收轉付收據章，以郵局掛號或傳真方式寄回17Life退貨處理中心，待我們收到資料後會立即為您處理退款作業。";
                _items["download"] = "<a href=\"" + paperLink + "\" target=\"_blank\" style=\"color:#08C;text-decoration:underline; font-weight:bold;\">退貨申請書</a>";
            }
            if (_mailType == MailContentType.ReturnDiscountResend)
            {
                _items["statement"] = "請您將［退回或折讓證明單］列印出來，以正楷簽名或蓋章（需與買受人相符）；若開立為三聯式發票，請務必填寫「營利事業統一編號」並蓋上公司發票章，以郵局掛號或傳真方式寄回17Life退貨處理中心，待我們收到資料後會立即為您處理退款作業。";
                _items["download"] = "<a href=\"" + discountLink + "\" target=\"_blank\" style=\"color:#08C;text-decoration:underline; font-weight:bold;\">退回或折讓證明單</a>";
            }

            
        }

    }
}

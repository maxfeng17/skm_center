﻿using LunchKingSite.DataOrm;

namespace LunchKingSite.Core.Component.Template
{
    public class PponItemMail : TemplateBase
    {
        public PponItemMail()
            : base()
        {
        }

        protected string _orderName = string.Empty;
        public string OrderName
        {
            get { return _orderName; }
            set { _orderName = value; }
        }

        protected ViewPponOrderDetail _theOrderDetail = null;
        public ViewPponOrderDetail TheOrderDetail
        {
            get { return _theOrderDetail; }
            set { _theOrderDetail = value; }
        }

        protected override void Initialize()
        {
            _callback = new PrepareItemCallback(PrepareItem);
        }

        protected virtual void PrepareItem()
        {
            PrepareOrderDetailInfo();
        }

        protected void PrepareOrderDetailInfo()
        {
            if (_theOrderDetail != null)
            {
                if (!string.IsNullOrEmpty(_theOrderDetail.DeliveryAddress))
                {
                    string[] mt = _theOrderDetail.ItemName.Split(':');
                    if (mt.Length > 1)
                        _items["mailto"] = mt[1].Split('<')[0];
                    else _items["mailto"] = _orderName;
                }
                else _items["mailto"] = _orderName;

                _items["event_name"] = _theOrderDetail.ItemName;
                _items["order_id"] = _theOrderDetail.OrderId;
            }
        }
    }
}

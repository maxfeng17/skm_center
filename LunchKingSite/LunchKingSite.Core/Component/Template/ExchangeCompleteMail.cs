﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.Core.Component.Template
{
    public class ExchangeCompleteMail : TemplateBase
    {
        public string MemberName { get; set; }

        public string OrderID { get; set; }

        public string ItemName { get; set; }

        public string Reason { get; set; }

        public string SiteUrl { get; set; }

        public string SiteServiceUrl { get; set; }

        protected override void Initialize()
        {
            _callback = new PrepareItemCallback(PrepareItem);
        }

        protected virtual void PrepareItem()
        {
            _items["site_url"] = this.SiteUrl;
            _items["site_service_url"] = this.SiteServiceUrl;
            _items["member_name"] = this.MemberName;
            _items["order_id"] = this.OrderID;
            _items["item_name"] = this.ItemName;
            _items["reason"] = this.Reason;
        }
    }
}

﻿using LunchKingSite.DataOrm;
using System;
using System.Linq;

namespace LunchKingSite.Core.Component.Template
{
    public class CloseDownNoticeForCustomer : TemplateBase
    {
        public CloseDownNoticeForCustomer()
        : base()
        {
        }

        protected string _itemName = null;
        public string ItemName
        {
            get
            {
                return _itemName;
            }
            set
            {
                _itemName = value;
            }
        }

        protected string _memberName = null;
        public string MemberName
        {
            get
            {
                return _memberName;
            }
            set
            {
                _memberName = value;
            }
        }

        protected string _storeName = null;
        public string StoreName
        {
            get
            {
                return _storeName;
            }
            set
            {
                _storeName = value;
            }
        }

        protected DateTime _closeDate;
        public DateTime CloseDate 
        {
            get { return _closeDate; }
            set { _closeDate = value; }
        }

        private string _siteUrl = string.Empty;
        public string SiteUrl
        {
            get
            {
                return this._siteUrl;
            }
            set
            {
                this._siteUrl = value;
            }
        }

        private string _siteServiceUrl = string.Empty;
        public string SiteServiceUrl
        {
            get
            {
                return this._siteServiceUrl;
            }
            set
            {
                this._siteServiceUrl = value;
            }
        }

        protected override void Initialize()
        {
            _callback = new PrepareItemCallback(PrepareItem);
        }

        protected virtual void PrepareItem()
        {
            _items["item_name"] = ItemName;
            _items["member_name"] = MemberName;
            _items["store_name"] = StoreName;
            _items["close_date"] = CloseDate.ToShortDateString();
            _items["site_url"] = _siteUrl;
            _items["site_service_url"] = _siteServiceUrl;
        }
    }
}

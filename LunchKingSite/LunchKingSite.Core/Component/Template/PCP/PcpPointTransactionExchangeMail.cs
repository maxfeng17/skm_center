﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.Core.Component.Template
{

    [TemplateFile("PcpPointTransactionExchangeMail.txt")]
    public class PcpPointTransactionExchangeMail : TemplateBase
    {
        protected override void Initialize()
        {
            _items.Add("seller_name", string.Empty);
            _items.Add("exchange_amount", 0);
            _items.Add("site_url", null);
            _items.Add("site_service_url", null);
        }

        public string SellerName
        {
            get { return (string)_items["seller_name"]; }
            set { _items["seller_name"] = value; }
        }
        public int ExchangeAmount
        {
            get { return (int)_items["exchange_amount"]; }
            set { _items["exchange_amount"] = value; }
        }

        public string SiteUrl
        {
            get { return (string)_items["site_url"]; }
            set { _items["site_url"] = value; }
        }

        public string SiteServiceUrl
        {
            get { return (string)_items["site_service_url"]; }
            set { _items["site_service_url"] = value; }
        }
    }
}

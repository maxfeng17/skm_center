﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.Core.Component.Template
{

    [TemplateFile("PcpPointTransactionOrderMail.txt")]
    public class PcpPointTransactionOrderMail : TemplateBase
    {
        protected override void Initialize()
        {
            _items.Add("seller_name", string.Empty);
            _items.Add("show_regulars_content",false);
            _items.Add("regulars_point", 0);
            _items.Add("favor_point", 0);
            _items.Add("site_url", null);
            _items.Add("site_service_url", null);
        }


        public string SellerName
        {
            get { return (string)_items["seller_name"]; }
            set { _items["seller_name"] = value; }
        }
        public bool ShowRegularsContent
        {
            get { return (bool)_items["show_regulars_content"]; }
            set { _items["show_regulars_content"] = value; }
        }
        public int RegularsPoint
        {
            get { return (int)_items["regulars_point"]; }
            set { _items["regulars_point"] = value; }
        }
        public int FavorPoint
        {
            get { return (int)_items["favor_point"]; }
            set { _items["favor_point"] = value; }
        }


        public string SiteUrl
        {
            get { return (string)_items["site_url"]; }
            set { _items["site_url"] = value; }
        }
        
        public string SiteServiceUrl
        {
            get { return (string)_items["site_service_url"]; }
            set { _items["site_service_url"] = value; }
        }
    }
}

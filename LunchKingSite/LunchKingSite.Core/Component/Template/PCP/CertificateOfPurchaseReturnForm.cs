﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.Core.Component.Template
{
    [TemplateFile("pcp/CertificatePurchaseReturnForm.txt")]
    public class CertificateOfPurchaseReturnForm : TemplateBase
    {
        protected override void Initialize()
        {
            _items.Add("site_url", null);
            _items.Add("company_name", string.Empty);
            _items.Add("company_id", string.Empty);
            _items.Add("company_address", string.Empty);
            _items.Add("invoice_mode", default(int));
            _items.Add("envoice_date", DateTime.Now);
            _items.Add("einvoice_number", string.Empty);
            _items.Add("einvoice_item_name", string.Empty);
            _items.Add("item_amount", default(decimal));
            _items.Add("item_no_tax_amount", default(decimal));
            _items.Add("item_tax", default(decimal));
            _items.Add("total_amount", default(decimal));
            _items.Add("istax", false);           
        }

        public string SiteUrl
        {
            get { return (string)_items["site_url"]; }
            set { _items["site_url"] = value; }
        }

        public string CompanyName
        {
            get { return (string)_items["company_name"]; }
            set { _items["company_name"] = value; }            
        }

        public string CompanyId
        {
            get { return (string)_items["company_id"]; }
            set { _items["company_id"] = value; }
        }

        public string CompanyAddress
        {
            get { return (string)_items["company_address"]; }
            set { _items["company_address"] = value; }
        }

        public int InvoiceMode
        {
            get { return (int)_items["invoice_mode"]; }
            set { _items["invoice_mode"] = value; }
        }

        public DateTime EnvoiceDate
        {
            get { return (DateTime)_items["envoice_date"]; }
            set { _items["envoice_date"] = value; }
        }

        public string EinvoiceNumber
        {
            get { return (string)_items["einvoice_number"]; }
            set { _items["einvoice_number"] = value; }
        }

        public string EinvoiceItemName
        {
            get { return (string)_items["einvoice_item_name"]; }
            set { _items["einvoice_item_name"] = value; }
        }

        public decimal ItemAmount
        {
            get { return (decimal)_items["item_amount"]; }
            set { _items["item_amount"] = value; }
        }

        public decimal ItemNoTaxAmount
        {
            get { return (decimal)_items["item_no_tax_amount"]; }
            set { _items["item_no_tax_amount"] = value; }
        }

        public decimal ItemTax
        {
            get { return (decimal)_items["item_tax"]; }
            set { _items["item_tax"] = value; }
        }

        public decimal TotalAmount
        {
            get { return (decimal)_items["total_amount"]; }
            set { _items["total_amount"] = value; }
            
        }

        public bool IsTax
        {
            get { return (bool)_items["istax"]; }
            set { _items["istax"] = value; }
            
        }
    }
}

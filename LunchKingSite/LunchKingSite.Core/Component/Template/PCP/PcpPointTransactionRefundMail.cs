﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.Core.Component.Template
{

    [TemplateFile("PcpPointTransactionRefundMail.txt")]
    public class PcpPointTransactionRefundMail : TemplateBase
    {
        protected override void Initialize()
        {
            _items.Add("seller_name", string.Empty);
            _items.Add("refund_amount", 0);
            _items.Add("site_url", null);
            _items.Add("site_service_url", null);
        }

        public string SellerName
        {
            get { return (string)_items["seller_name"]; }
            set { _items["seller_name"] = value; }
        }
        public int RefundAmount
        {
            get { return (int)_items["refund_amount"]; }
            set { _items["refund_amount"] = value; }
        }


        public string SiteUrl
        {
            get { return (string)_items["site_url"]; }
            set { _items["site_url"] = value; }
        }

        public string SiteServiceUrl
        {
            get { return (string)_items["site_service_url"]; }
            set { _items["site_service_url"] = value; }
        }
    }
}

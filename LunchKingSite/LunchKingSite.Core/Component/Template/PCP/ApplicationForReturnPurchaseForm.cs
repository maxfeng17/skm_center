﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.Core.Component.Template
{
    [TemplateFile("pcp/ApplicationForReturnPurchaseForm.txt")]
    public class ApplicationForReturnPurchaseForm : TemplateBase
    {
        protected override void Initialize()
        {
            _items.Add("site_url", string.Empty);
            _items.Add("apply_return_purchase_date", DateTime.Now);
            _items.Add("return_purchase_item", string.Empty);
        }

        public string SiteUrl
        {
            get { return (string)_items["site_url"]; }
            set { _items["site_url"] = value; }
        }

        public DateTime ApplyReturnPurchaseDate
        {
            get { return (DateTime)_items["apply_return_purchase_date"]; }
            set { _items["apply_return_purchase_date"] = value; }
        }

        public string ReturnPurchaseItem
        {
            get { return (string)_items["return_purchase_item"]; }
            set { _items["return_purchase_item"] = value; }
        }
    }

}

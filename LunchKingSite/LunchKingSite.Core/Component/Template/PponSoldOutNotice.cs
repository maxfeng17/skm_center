﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.DataOrm;

namespace LunchKingSite.Core.Component.Template
{
    public class PponSoldOutNotice : TemplateBase
    {
        public PponSoldOutNotice()
            : base()
        {
        }

        protected ViewPponDeal _theDeal = null;
        public ViewPponDeal TheDeal
        {
            get { return _theDeal; }
            set { _theDeal = value; }
        }

        protected static ISysConfProvider _config = ProviderFactory.Instance().GetConfig();
        protected override void Initialize()
        {
            _callback = new PrepareItemCallback(PrepareItem);
        }

        protected virtual void PrepareItem()
        {
            _items["site_url"] =_config.SiteUrl;
            PrepareDealInfo();
        }

        protected void PrepareDealInfo()
        {
            if (_theDeal != null)
            {
                _items["event_name"] = _theDeal.EventName;
                _items["seller_name"] = _theDeal.SellerName;
                _items["order_time"] = _theDeal.BusinessHourOrderTimeS.ToString("yyyy/MM/dd") + "~" +
                                       _theDeal.BusinessHourOrderTimeE.ToString("yyyy/MM/dd");
                _items["deliver_time"] = _theDeal.BusinessHourDeliverTimeS.GetValueOrDefault().ToString("yyyy/MM/dd") + "~" +
                                         _theDeal.BusinessHourDeliverTimeE.GetValueOrDefault().ToString("yyyy/MM/dd");
                _items["limit"] = string.Format("{0:0.#}", _theDeal.OrderTotalLimit);
                _items["quantity"] = string.Format("{0:0.#}", _theDeal.OrderedQuantity);
                _items["unsell"] = string.Format("{0:0.#}", (_theDeal.OrderTotalLimit - _theDeal.OrderedQuantity));
                _items["link"] = _config.SiteUrl + "/deal/" + _theDeal.BusinessHourGuid.ToString();
                _items["email_top"] = "17Life/G1-8/17PEmail_Top.jpg";
                _items["email_bottom"] = "17Life/G1-8/17PEmail_Bottom.jpg";
            }
        }
    }
}

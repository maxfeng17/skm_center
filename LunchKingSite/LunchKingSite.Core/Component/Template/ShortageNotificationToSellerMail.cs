﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.Core.Component.Template
{
    [TemplateFile("ShortageNotificationToSellerMail.txt")]
    public class ShortageNotificationToSellerMail : TemplateBase
    {
        public ShortageNotificationToSellerMail() : base() { }

        protected override void Initialize()
        {
            _items.Add("product_info", new ProductShortageModel());

            _items.Add("site_url", string.Empty);
            _items.Add("https_site_url", string.Empty);
            _items.Add("site_service_url", string.Empty);
            _items.Add("is_wms", false);
        }

        /// <summary>
        /// 退/換貨資料
        /// </summary>
        public ProductShortageModel ProductInfo
        {
            get { return (ProductShortageModel)_items["product_info"]; }
            set { _items["product_info"] = value; }
        }

        public string SellerName
        {
            get { return (string)_items["seller_name"]; }
            set { _items["seller_name"] = value; }
        }
        public string SiteUrl
        {
            get { return (string)_items["site_url"]; }
            set { _items["site_url"] = value; }
        }
        public string HttpsSiteUrl
        {
            get { return (string)_items["https_site_url"]; }
            set { _items["https_site_url"] = value; }
        }
        public bool IsWms
        {
            get { return (bool)_items["is_wms"]; }
            set { _items["is_wms"] = value; }
        }

        /// <summary>
        /// 商品庫存不足資料
        /// </summary>
        public class ProductShortageModel
        {
            public List<ProductItemShortage> ItemList { get; set; }
        }

        public class ProductShortage
        {
            public Guid SellerGuid { get; set; }
            public List<ProductItemShortage> ItemList { get; set; }
        }

        public class ProductItemShortage
        {
            /// <summary>
            /// 商品名稱
            /// </summary>
            public string ProductName { get; set; }
            /// <summary>
            /// 17Life商品編號
            /// </summary>
            public int ProductNo { get; set; }
            /// <summary>
            /// 原廠貨號
            /// </summary>
            public string ProductCode { get; set; }
            /// <summary>
            /// 全球交易識別碼
            /// </summary>
            public string GTINs { get; set; }
            /// <summary>
            /// 庫存
            /// </summary>
            public int Stock { get; set; }

            /// <summary>
            /// 安全庫存
            /// </summary>
            public int SafetyStock { get; set; }
            /// <summary>
            /// 規格
            /// </summary>
            public string SpecName { get; set; }
            /// <summary>
            /// 業務信箱
            /// </summary>
            public List<string> SalesMail { get; set; }
        }
    }
}

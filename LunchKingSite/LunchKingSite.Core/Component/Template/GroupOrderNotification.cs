
namespace LunchKingSite.Core.Component.Template
{
    public class GroupOrderNotification : TemplateBase
    {
        public GroupOrderNotification() : base() { }

        protected override void Initialize()
        {
            _items.Add("user", null);
            _items.Add("sellername", null);
            _items.Add("link", null);
            _items.Add("creattime", null);
        }

        public string User
        {
            get { return (string)_items["user"]; }
            set { _items["user"] = value; }
        }

        public string SellerName
        {
            get { return (string)_items["sellername"]; }
            set { _items["sellername"] = value; }
        }

        public string CheckOutLink
        {
            get { return (string)_items["link"]; }
            set { _items["link"] = value; }
        }

        public string GOManageLink
        {
            get { return (string)_items["link2"]; }
            set { _items["link2"] = value; }
        }

        public string SimpGrpOrdLink
        {
            get { return (string)_items["link3"]; }
            set { _items["link3"] = value; }
        }

        public string CreatTime
        {
            get { return (string)_items["creattime"]; }
            set { _items["creattime"] = value; }
        }

        public int OrderMinimum
        {
            get { return (int)_items["order_minimum"]; }
            set { _items["order_minimum"] = value; }
        }

        public int OrderTotal
        {
            get { return (int)_items["order_total"]; }
            set { _items["order_total"] = value; }
        }
    }
}

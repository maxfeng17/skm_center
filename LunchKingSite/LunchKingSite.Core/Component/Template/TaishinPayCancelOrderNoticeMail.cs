﻿using System;
using System.Linq;
using LunchKingSite.DataOrm;
using System.Collections.Generic;
using System.Text;
using System.Web;
using LunchKingSite.Core;

namespace LunchKingSite.Core.Component.Template
{
    public class TaishinPayCancelOrderNoticeMail : TemplateBase
    {
        public ISysConfProvider ServerConfig { get; set; }
        ISysConfProvider cp = ProviderFactory.Instance().GetConfig();
        public TaishinPayCancelOrderNoticeMail()
            : base()
        {

        }

        protected PaymentTransactionCollection _paymentTransactionCol = null;
        public PaymentTransactionCollection PaymentTransactionCol
        {
            get { return _paymentTransactionCol; }
            set { _paymentTransactionCol = value; }
        }
        
        protected List<IViewPponDeal> _promoDeals = null;
        public List<IViewPponDeal> PromoDeals
        {
            get { return _promoDeals; }
            set { _promoDeals = value; }
        }

        public string MemberName { get; set; }

        public string ItemName { get; set; }

        public DateTime OrderDate { get; set; }

        public decimal OrderAmount { get; set; }

        protected override void Initialize()
        {
            _callback = new PrepareItemCallback(PrepareItem);
        }

        protected virtual void PrepareItem()
        {
            _items["site_url"] = VirtualPathUtility.AppendTrailingSlash(cp.SiteUrl);
            _items["site_service_url"] = cp.SiteServiceUrl;
            _items["member_name"] = MemberName;
            _items["item_name"] = ItemName;
            _items["order_date"] = OrderDate.ToString("yyyy/MM/dd");
            _items["amount"] = "NT$" + OrderAmount.ToString("N0"); ;
            PreparePaymentTransInfo();
            PreparePromoDealsInfo();
            SetOtherItem();
        }
        
        protected void PreparePromoDealsInfo()
        {
            if (_promoDeals.Any())
            {
                // 依序設定檔次資料
                List<AdDeal> adDeals = new List<AdDeal>();
                for (int i = 0; i < _promoDeals.Count; i++)
                {
                    adDeals.Add(new AdDeal
                    {
                        Url = ServerConfig.SiteUrl + "/deal/" + _promoDeals[i].BusinessHourGuid.ToString() + "?rsrc=17_EDMpay",
                        Pic = Helper.GetImgUrl(_promoDeals[i].EventImagePath),
                        Title = _promoDeals[i].ItemName.TrimToMaxLength(31, "..."),
                        Price = Convert.ToInt32(_promoDeals[i].ItemPrice).ToString("D") +
                                (Helper.IsFlagSet(_promoDeals[i].BusinessHourStatus, BusinessHourStatus.ComboDealMain) ? "起" : string.Empty)
                    });
                    if (adDeals.Count == ServerConfig.PromoMailDealsCount)
                    {
                        break;
                    }
                }
                _items["ad_deals"] = adDeals;
            }
        }

        protected void PreparePaymentTransInfo()
        {
            _items["credit"] = string.Empty;
            _items["atm"] = string.Empty;
            _items["pcash"] = string.Empty;
            _items["bcash"] = string.Empty;
            _items["scash"] = string.Empty;
            _items["discount"] = string.Empty;
            _items["tcash"] = string.Empty;
            _items["ptc_count"] = 0;
        }

        protected void SetOtherItem()
        {
            var ptc = _paymentTransactionCol;
            _items["ptc_count"] = ptc.Count();

            if (ptc.Count > 0)
            {
                var ptScash = ptc.Where(x => x.PaymentType == (int)PaymentType.SCash && Helper.GetPaymentTransactionPhase(x.Status) == PayTransPhase.Successful).FirstOrDefault();
                var ptBcash = ptc.Where(x => x.PaymentType == (int)PaymentType.BonusPoint && Helper.GetPaymentTransactionPhase(x.Status) == PayTransPhase.Successful).FirstOrDefault();
                var ptDcash = ptc.Where(x => x.PaymentType == (int)PaymentType.DiscountCode && Helper.GetPaymentTransactionPhase(x.Status) == PayTransPhase.Successful).FirstOrDefault();
                var ptTcash = ptc.Where(x => x.PaymentType == (int)PaymentType.TaishinPay && Helper.GetPaymentTransactionPhase(x.Status) == PayTransPhase.Successful).FirstOrDefault();
                
                if (ptScash != null && ptScash.Amount > 0)
                {
                    _items["scash"] = string.Format(@"
                    <li>
                    <span style='color:#666'>購物金</span>
                    <span style='font-family:Arial,Helvetica,sans-serif'> NT${0}</span>
                    </li>", ptScash.Amount.ToString("N0"));
                }
                if (ptBcash != null && ptBcash.Amount > 0)
                {
                    _items["bcash"] = string.Format(@"
                    <li>
                    <span style='color:#666'>紅利金</span>
                    <span style='font-family:Arial,Helvetica,sans-serif'> NT${0}</span>
                    </li>", ptBcash.Amount.ToString("N0"));
                }
                if (ptDcash != null && ptDcash.Amount > 0)
                {
                    _items["discount"] = string.Format(@"
                    <li>
                    <span style='color:#666'>折價券</span>
                    <span style='font-family:Arial,Helvetica,sans-serif'> NT${0}</span>
                    </li>", ptDcash.Amount.ToString("N0"));
                }
                if (ptTcash != null && ptTcash.Amount > 0)
                {
                    _items["tcash"] = string.Format(@"
                    <li>
                    <span style='color:#666'>{0}</span>
                    <span style='font-family:Arial,Helvetica,sans-serif'> NT${1}</span>
                    </li>", Helper.GetEnumDescription(ThirdPartyPayment.TaishinPay), ptTcash.Amount.ToString("N0"));
                }
            }
        }
        
        public class AdDeal
        {
            public string Url { get; set; }
            public string Pic { get; set; }
            public string Title { get; set; }
            public string Price { get; set; }

            public AdDeal()
            {
                this.Url = string.Empty;
                this.Pic = string.Empty;
                this.Title = string.Empty;
                this.Price = string.Empty;
            }
        }
    }
}
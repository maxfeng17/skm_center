﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.DataOrm;

namespace LunchKingSite.Core.Component.Template
{
    public class IDEASPponReturnAllMail : TemplateBase
    {
       public ISysConfProvider ServerConfig { get; set; }
        ISysConfProvider cp = ProviderFactory.Instance().GetConfig();

        public IDEASPponReturnAllMail()
            : base()
        {
        }

        protected ViewPponDeal _theDeal = null;
        public ViewPponDeal TheDeal
        {
            get { return _theDeal; }
            set { _theDeal = value; }
        }

        protected ViewPponOrderDetailCollection _theOrderDetailCol = null;
        public ViewPponOrderDetailCollection TheOrderDetailCol
        {
            get { return _theOrderDetailCol; }
            set { _theOrderDetailCol = value; }
        }

        protected CashTrustLogCollection _theCashTrustLogCol = null;
        public CashTrustLogCollection TheCashTrustLogCol
        {
            get { return _theCashTrustLogCol; }
            set { _theCashTrustLogCol = value; }
        }

        protected Order _order = null;
        public Order Order
        {
            get { return _order; }
            set { _order = value; }
        }

        protected override void Initialize()
        {
            _callback = new TemplateBase.PrepareItemCallback(PrepareItem);
        }

        protected virtual void PrepareItem()
        {
            _items["site_url"] =cp.SiteUrl;
            _items["site_service_url"] = cp.SiteServiceUrl;

            _items["credit"] = string.Empty;
            _items["freight"] = string.Empty;
            _items["total_price"] = 0;

            _items["r_credit"] = string.Empty;
            _items["r_freight"] = string.Empty;
            _items["r_total_price"] = 0;
            PrepareDealInfo();
            PrepareOrderDetailInfo();
            SetOtherItem();
        }

        protected void PrepareDealInfo()
        {
            if (_theDeal != null)
            {
                _items["item_name"] = _theDeal.ItemName;
            }
            if (_order != null)
            {
                _items["order_id"] = _order.OrderId;
            }
        }

        protected void PrepareOrderDetailInfo()
        {
            if (_theOrderDetailCol.Count > 0)
            {
                _items["member_name"] = _theOrderDetailCol[0].MemberName;
            }
        }

        protected void SetOtherItem()
        {
            if (_theCashTrustLogCol.Count > 0)
            {
                int credit = _theCashTrustLogCol.Sum(x => x.CreditCard);
                int freight = 0;
                CashTrustLog ctFreight = (_theCashTrustLogCol.Where(x => x.SpecialStatus == (int)TrustSpecialStatus.Freight).Count() > 0) ? _theCashTrustLogCol.Where(x => x.SpecialStatus == (int)TrustSpecialStatus.Freight).First() : null;
                if (ctFreight != null) freight = ctFreight.Amount;

                int rCredit = 0;

                if (_theCashTrustLogCol.First().Status != (int)TrustStatus.Returned) //刷退
                {
                    rCredit = credit;    
                }

                string dollarSign = " NT$ ";
                if (credit > 0)
                    _items["credit"] = @"<tr><td width=""200"">刷卡</td><td>" + dollarSign + credit.ToString("N0") + "元</td></tr>";
                if (freight > 0)
                    _items["freight"] = @"(含運費" + dollarSign + freight.ToString() + @")";

                _items["total_price"] = _theCashTrustLogCol.Sum(x => x.Amount);

                if (rCredit > 0)
                    _items["r_credit"] = @"<tr><td width=""200"">刷卡</td><td>" + dollarSign + rCredit.ToString("N0") + "元</td></tr>";
                if (freight > 0)
                    _items["r_freight"] = @"(含運費" + dollarSign + freight.ToString() + @")";

                _items["r_total_price"] = _theCashTrustLogCol.Sum(x => x.Amount);
            }
        }

        private string GetImgUrl(string path)
        {
            return ((string.IsNullOrWhiteSpace(path)) ? Helper.GetMediaPathsFromRawData(ServerConfig.EDMDefaultImage, MediaType.PponDealPhoto, ServerConfig.MediaBaseUrl).DefaultIfEmpty(string.Empty).First() : Helper.GetMediaPathsFromRawData(path, MediaType.PponDealPhoto, ServerConfig.MediaBaseUrl).DefaultIfEmpty(string.Empty).First());
        }
    }
}

﻿using System.Collections.Generic;
using LunchKingSite.DataOrm;
using System;
namespace LunchKingSite.Core.Component.Template
{
    [TemplateFile("famidealsuccessmail.txt")]
    public class FamiDealSuccessMail : TemplateBase
    {
        readonly ISysConfProvider _cp = ProviderFactory.Instance().GetConfig();

        public FamiDealSuccessMail() : base() { }

        protected override void Initialize()
        {
            _items["mailto"] = string.Empty;
            _items["dealname"] = string.Empty;
            _items["introduction"] = string.Empty;
            _items["sellername"] = string.Empty;
            _items["coupon"] = string.Empty;
            _items["siteUrl"] = _cp.SiteUrl;
            _items["siteServiceUrl"] = _cp.SiteServiceUrl;
            _items["enableFami3Barcode"] = false;
            _items["pinCode"] = string.Empty;
        }

        public string MailTo
        {
            get { return (string)_items["mailto"]; }
            set { _items["mailto"] = value; }
        }

        public string DealName
        {
            get { return (string)_items["dealname"]; }
            set { _items["dealname"] = value; }
        }
        public string Introduction
        {
            get { return (string)_items["Introduction"]; }
            set { _items["Introduction"] = value; }
        }
        public string SellerName
        {
            get { return (string)_items["sellername"]; }
            set { _items["sellername"] = value; }
        }
        public string Coupon
        {
            get { return (string)_items["coupon"]; }
            set { _items["coupon"] = value; }
        }
        public string PinCode
        {
            get { return (string)_items["pinCode"]; }
            set { _items["pinCode"] = value; }
        }
        public bool EnableFami3Barcode
        {
            get { return (bool)_items["enableFami3Barcode"]; }
            set { _items["enableFami3Barcode"] = value; }
        }
    }
}

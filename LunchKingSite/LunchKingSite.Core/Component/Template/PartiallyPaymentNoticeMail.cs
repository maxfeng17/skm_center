﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.Core.Component.Template
{

    [TemplateFile("PartiallyPaymentNoticeMail.txt")]
    public class PartiallyPaymentNoticeMail : TemplateBase
    {
        protected override void Initialize()
        {
            _items.Add("productList", null);
            _items.Add("seller", null);
            _items.Add("site_url", null);
            _items.Add("site_service_url", null);
        }

        public string ProdeuctList
        {
            get { return (string)_items["productList"]; }
            set { _items["productList"] = value; }
        }

        public string Seller
        {
            get { return (string)_items["seller"]; }
            set { _items["seller"] = value; }
        }

        public string SiteUrl
        {
            get { return (string)_items["site_url"]; }
            set { _items["site_url"] = value; }
        }

        public string SiteServiceUrl
        {
            get { return (string)_items["site_service_url"]; }
            set { _items["site_service_url"] = value; }
        }
    }
}

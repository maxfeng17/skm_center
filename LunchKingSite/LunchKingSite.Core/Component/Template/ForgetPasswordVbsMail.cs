﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.Core.Component.Template
{
    [TemplateFile("forgetpasswordvbsmail.txt")]
    public class ForgetPasswordVbsMail : TemplateBase
    {
        public ForgetPasswordVbsMail() : base() { }

        protected override void Initialize()
        {
            _items.Add("account_id", null);
            _items.Add("apply_password", null);
            _items.Add("site_url", null);
            _items.Add("hour_limit", null);
        }

        public string AccountId
        {
            get { return (string)_items["account_id"]; }
            set { _items["account_id"] = value; }
        }

        public string ApplyPassword
        {
            get { return (string)_items["apply_password"]; }
            set { _items["apply_password"] = value; }
        }

        public string SiteUrl
        {
            get { return (string)_items["site_url"]; }
            set { _items["site_url"] = value; }
        }

        public string HourLimit
        {
            get { return (string)_items["hour_limit"]; }
            set { _items["hour_limit"] = value; }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.Core.Component.Template
{
    [TemplateFile("onetimeeventmail.txt")]
    public class OneTimeEventMail : TemplateBase
    {
        public OneTimeEventMail() : base() { }

        protected override void Initialize()
        {
            _items.Add("member_name", null);
            _items.Add("site_url", null);
            _items.Add("site_service_url", null);
        }

        public string MemberName
        {
            get { return (string)_items["member_name"]; }
            set { _items["member_name"] = value; }
        }

        public string SiteUrl
        {
            get { return (string)_items["site_url"]; }
            set { _items["site_url"] = value; }
        }

        public string SiteServiceUrl
        {
            get { return (string)_items["site_service_url"]; }
            set { _items["site_service_url"] = value; }
        }
    }
}

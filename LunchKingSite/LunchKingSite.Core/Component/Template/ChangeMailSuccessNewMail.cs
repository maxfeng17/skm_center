﻿namespace LunchKingSite.Core.Component.Template
{
    [TemplateFile("changemailsuccessnewmail.txt")]
    public class ChangeMailSuccessNewMail : TemplateBase
    {
        protected override void Initialize()
        {
            this._callback = new PrepareItemCallback(() =>
            {
                _items["member_name"] = MemberName;
                _items["site_url"] = _siteUrl;
                _items["site_service_url"] = _siteServiceUrl;
            });
        }
        public string MemberName { get; set; }

        private string _siteUrl = string.Empty;
        public string SiteUrl
        {
            get
            {
                return this._siteUrl;
            }
            set
            {
                this._siteUrl = value;
            }
        }

        private string _siteServiceUrl = string.Empty;
        public string SiteServiceUrl
        {
            get
            {
                return this._siteServiceUrl;
            }
            set
            {
                this._siteServiceUrl = value;
            }
        }
    }
}

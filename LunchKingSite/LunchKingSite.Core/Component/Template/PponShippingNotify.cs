﻿using System;
using System.Collections.Generic;
using System.Linq;
using LunchKingSite.DataOrm;

namespace LunchKingSite.Core.Component.Template
{
    public class PponShippingNotify : TemplateBase
    {
        public ISysConfProvider ServerConfig { get; set; }
        ISysConfProvider cp = ProviderFactory.Instance().GetConfig();

        public PponShippingNotify()
            : base()
        {
        }

        protected ViewDealPropertyBusinessHourContent _vdpb = null;
        public ViewDealPropertyBusinessHourContent Vdpb
        {
            get { return _vdpb; }
            set { _vdpb = value; }
        }

        protected ViewPponCoupon _theOrderDetail = null;
        public ViewPponCoupon TheOrderDetail
        {
            get { return _theOrderDetail; }
            set { _theOrderDetail = value; }
        }

        protected List<ViewPponDeal> _promoDeals = null;
        public List<ViewPponDeal> PromoDeals
        {
            get { return _promoDeals; }
            set { _promoDeals = value; }
        }

        protected override void Initialize()
        {
            _callback = new PrepareItemCallback(PrepareItem);
        }

        protected virtual void PrepareItem()
        {
            _items["site_url"] = cp.SiteUrl;
            _items["site_service_url"] = cp.SiteServiceUrl;

            PrepareDealInfo();
            PrepareOrderDetailInfo();
            PreparePromoDealsInfo();
        }

        protected void PreparePromoDealsInfo()
        {
            // 先建立預設值
            int dealsCount = ServerConfig.PromoMailDealsCount;
            for (int i = 0; i < dealsCount; i++)
            {
                _items["deal" + (i + 1) + "url"] = string.Empty;
                _items["deal" + (i + 1) + "pic"] = string.Empty;
                _items["deal" + (i + 1) + "title"] = string.Empty;
                _items["deal" + (i + 1) + "price"] = string.Empty;
            }

            // 依序設定檔次資料
            for (int i = 0; i < _promoDeals.Count; i++)
            {
                _items["deal" + (i + 1) + "url"] = ServerConfig.SiteUrl + "/deal/" + _promoDeals[i].BusinessHourGuid.ToString() + "?rsrc=17_EDMpay";
                _items["deal" + (i + 1) + "pic"] = Helper.GetImgUrl(_promoDeals[i].EventImagePath);
                _items["deal" + (i + 1) + "title"] = (_promoDeals[i].ItemName.Length > 31) ? (_promoDeals[i].ItemName.Substring(0, 31) + "...") : _promoDeals[i].ItemName;
                _items["deal" + (i + 1) + "price"] = Convert.ToInt32(_promoDeals[i].ItemPrice).ToString("D") + (Helper.IsFlagSet(_promoDeals[i].BusinessHourStatus, BusinessHourStatus.ComboDealMain) ? "起" : string.Empty);
            }

            // 必須有檔次數才要顯示
            _items["show_promo"] = _promoDeals.Count() > 0;
        }

        protected void PrepareDealInfo()
        {
            if (_vdpb != null)
            {
                _items["item_name"] = _vdpb.DealName;
                if (_vdpb.BusinessHourDeliverTimeS != null)
                    _items["dateS"] = _vdpb.BusinessHourDeliverTimeS.Value.ToString("yyyy/MM/dd");
                if (_vdpb.BusinessHourDeliverTimeE != null)
                {
                    DateTime de = Convert.ToDateTime(_vdpb.BusinessHourDeliverTimeE.Value);
                    _items["dateE"] =de.ToString("yyyy/MM/dd");
                }
            }
        }

        protected void PrepareOrderDetailInfo()
        {
            if (_theOrderDetail != null)
            {
                _items["member_name"] = _theOrderDetail.MemberName;
            }
        }

        private string GetImgUrl(string path)
        {
            return ((string.IsNullOrWhiteSpace(path)) ? Helper.GetMediaPathsFromRawData(ServerConfig.EDMDefaultImage, MediaType.PponDealPhoto, ServerConfig.MediaBaseUrl).DefaultIfEmpty(string.Empty).First() : Helper.GetMediaPathsFromRawData(path, MediaType.PponDealPhoto, ServerConfig.MediaBaseUrl).DefaultIfEmpty(string.Empty).First());
        }
    }
}

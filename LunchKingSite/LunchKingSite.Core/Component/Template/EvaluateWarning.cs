﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.Core.Component.Template
{
    [TemplateFile("EvaluateWarning.txt")]
    public class EvaluateWarning : TemplateBase
    {
        protected override void Initialize()
        {
            _items.Add("DealName", null);
            _items.Add("UniqueId", null);
            _items.Add("OrderTimeS", null);
            _items.Add("OrderTimeE", null);
            _items.Add("DeliverTimeS", null);
            _items.Add("DeliverTimeE", null);
            _items.Add("AveragePointCollection", null);
            _items.Add("NegativeCommentsRateCollection", null);
        }


        public string DealName
        {
            get { return (string)_items["DealName"]; }
            set { _items["DealName"] = value; }
        }

        public string UniqueId
        {
            get { return (string)_items["UniqueId"]; }
            set { _items["UniqueId"] = value; }
        }

        public string OrderTimeS
        {
            get { return (string)_items["OrderTimeS"]; }
            set { _items["OrderTimeS"] = value; }
        }

        public string OrderTimeE
        {
            get { return (string)_items["OrderTimeE"]; }
            set { _items["OrderTimeE"] = value; }
        }
        public string DeliverTimeS
        {
            get { return (string)_items["DeliverTimeS"]; }
            set { _items["DeliverTimeS"] = value; }
        }
        public string DeliverTimeE
        {
            get { return (string)_items["DeliverTimeE"]; }
            set { _items["DeliverTimeE"] = value; }
        }
        public string AveragePointCollection
        {
            get { return (string)_items["AveragePointCollection"]; }
            set { _items["AveragePointCollection"] = value; }
        }
        public string NegativeCommentsRateCollection
        {
            get { return (string)_items["NegativeCommentsRateCollection"]; }
            set { _items["NegativeCommentsRateCollection"] = value; }
        }
    }
}

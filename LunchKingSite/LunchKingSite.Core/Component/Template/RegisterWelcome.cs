
namespace LunchKingSite.Core.Component.Template
{
    // since it's very similar to forget password mail, we inherit from that
    public class RegisterWelcome : ForgetPassword
    {
        public RegisterWelcome() : base() { }

        protected override void Initialize()
        {
            _items["authentication"] = string.Empty;
            _items["member_name"] = string.Empty;
            _items["hide_user_info"] = false;
        }

        public string MemberName
        {
            get { return (string)_items["member_name"]; }
            set { _items["member_name"] = value; }
        }

        public bool HideUserInfo    
        {
            get { return (bool)_items["hide_user_info"]; }
            set { _items["hide_user_info"] = value; }
        }

        public string UserId
        {
            get {   return (string)_items["user_id"];   }
            set {   _items["user_id"] = value;   }
        }
    }
}

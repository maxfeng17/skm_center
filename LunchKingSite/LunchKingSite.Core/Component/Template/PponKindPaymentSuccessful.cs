﻿using System;
using System.Linq;
using LunchKingSite.DataOrm;
using System.Collections.Generic;

namespace LunchKingSite.Core.Component.Template
{
    public class PponKindPaymentSuccessful : TemplateBase
    {
        public ISysConfProvider ServerConfig { get; set; }
        ISysConfProvider cp = ProviderFactory.Instance().GetConfig();
        public PponKindPaymentSuccessful()
            : base()
        {
        }

        protected ViewPponDeal _theDeal = null;
        public ViewPponDeal TheDeal
        {
            get { return _theDeal; }
            set { _theDeal = value; }
        }

        protected ViewPponOrderDetailCollection _theOrderDetailCol = null;
        public ViewPponOrderDetailCollection TheOrderDetailCol
        {
            get { return _theOrderDetailCol; }
            set { _theOrderDetailCol = value; }
        }

        protected CashTrustLogCollection _theCashTrustLogCol = null;
        public CashTrustLogCollection TheCashTrustLogCol
        {
            get { return _theCashTrustLogCol; }
            set { _theCashTrustLogCol = value; }
        }

        protected List<IViewPponDeal> _cityDeal = null;
        public List<IViewPponDeal> PromoDeals
        {
            get { return _cityDeal; }
            set { _cityDeal = value; }
        }

        protected string _itemName = null;
        public string ItemName
        {
            get { return _itemName; }
            set { _itemName = value; }
        }

        protected override void Initialize()
        {
            _callback = new PrepareItemCallback(PrepareItem);
        }

        protected virtual void PrepareItem()
        {
            _items["seller_name"] = _theDeal.SellerName;
            _items["site_url"] = cp.SiteUrl;
            _items["site_service_url"] = cp.SiteServiceUrl;
            _items["https_site_url"] = cp.SiteUrl.Replace("http", "https");

            PrepareOrderDetailInfo();
            PreparePaymentTransInfo();
            _items["order_minimum"] = _theDeal.BusinessHourOrderMinimum.ToString("N0");
            SetOtherItem();
        }

        protected void PrepareOrderDetailInfo()
        {
            if (_theOrderDetailCol.Count > 0)
            {
                _items["member_name"] = _theOrderDetailCol[0].MemberName;
                if (!string.IsNullOrEmpty(_itemName))
                    _items["item_name"] = _itemName;
                else
                    _items["item_name"] = _theOrderDetailCol[0].ItemName.Replace("&nbsp", " ");
            }

            #region 推薦好康
            // 檔次不夠時設定預設值
            int dealsCount = ServerConfig.PromoMailDealsCount;
            for (int i = ServerConfig.PromoMailDealsCount; i - _cityDeal.Count > 0; i--)
            {
                _items["deal" + i + "title"] = string.Empty;
                _items["deal" + i + "url"] = string.Empty;
                _items["deal" + i + "price"] = string.Empty;
                _items["deal" + i + "pic"] = string.Empty;
            }

            // 依序設定檔次資料
            for (int i = 0; i < _cityDeal.Count; i++)
            {
                _items["deal" + (i + 1) + "title"] = (_cityDeal[i].ItemName.Length > 31) ? (_cityDeal[i].ItemName.Substring(0, 31) + "...") : _cityDeal[i].ItemName;
                _items["deal" + (i + 1) + "url"] = ServerConfig.SiteUrl + "/deal/" + _cityDeal[i].BusinessHourGuid.ToString() + "?rsrc=17_EDMpay";
                _items["deal" + (i + 1) + "price"] = Convert.ToInt32(_cityDeal[i].ItemPrice).ToString("D") + (Helper.IsFlagSet(_cityDeal[i].BusinessHourStatus, BusinessHourStatus.ComboDealMain) ? "起" : string.Empty);
                _items["deal" + (i + 1) + "pic"] = Helper.GetImgUrl(_cityDeal[i].EventImagePath);
            }
            #endregion
        }

        protected void PreparePaymentTransInfo()
        {
            _items["credit"] = string.Empty;
            _items["atm"] = string.Empty;
            _items["pcash"] = string.Empty;
            _items["bcash"] = string.Empty;
            _items["scash"] = string.Empty;
            _items["discount"] = string.Empty;
            _items["order_date"] = _theCashTrustLogCol.First().CreateTime.ToString("yyyy/MM/dd");
            _items["amount"] = "NT$" + _theCashTrustLogCol.Sum(x => x.Amount).ToString("N0");
            _items["delivery_alert"] = (_theDeal.DeliveryType == (int)DeliveryType.ToHouse)
                                        ? "※商品將依照訂購順序，於" + _theDeal.BusinessHourDeliverTimeS.Value.ToString("yyyy/MM/dd") + "起開始陸續出貨，最晚將於" + _theDeal.BusinessHourDeliverTimeE.Value.ToString("yyyy/MM/dd") + "前出貨完畢；出貨後配送約2~3工作日，恕無法指定配達時間。<br/><br/>"
                                        : string.Empty;
        }

        protected void SetOtherItem()
        {
            _items["downloadcoupon"] = string.Empty;
            if (_theCashTrustLogCol.Count > 0)
            {
                int credit = _theCashTrustLogCol.Sum(x => x.CreditCard);
                int pcash = _theCashTrustLogCol.Sum(x => x.Pcash);
                int bonus = _theCashTrustLogCol.Sum(x => x.Bcash);
                int scash = _theCashTrustLogCol.Sum(x => x.Scash);
                int atm = _theCashTrustLogCol.Sum(x => x.Atm);
                int discount = _theCashTrustLogCol.Sum(x => x.DiscountAmount);

                if (scash > 0)
                    _items["scash"] =
                        @"<tr>" +
                        @"  <td>- 17Life購物金<span style=""color:#6d9345;font-family:Arial, Helvetica, sans-serif;""> NT$" + scash.ToString("N0") + "</span></td>" +
                        @"</tr>";
                if (bonus > 0)
                    _items["bcash"] =
                        @"<tr>" +
                        @"  <td>- 17Life紅利<span style=""color:#6d9345;font-family:Arial, Helvetica, sans-serif;""> NT$" + bonus.ToString("N0") + "</span></td>" +
                        @"</tr>";
                if (pcash > 0)
                    _items["pcash"] =
                        @"<tr>" +
                        @"  <td>- PayEasy購物金<span style=""color:#6d9345;font-family:Arial, Helvetica, sans-serif;""> NT$" + pcash.ToString("N0") + "</span></td>" +
                        @"</tr>";
                if (discount > 0)
                    _items["discount"] =
                        @"<tr>" +
                        @"  <td>- 17Life折價券<span style=""color:#6d9345;font-family:Arial, Helvetica, sans-serif;""> NT$" + discount.ToString("N0") + "</span></td>" +
                        @"</tr>";
                if (atm > 0)
                    _items["atm"] =
                        @"<tr>" +
                        @"  <td>- ATM付款<span style=""color:#6d9345;font-family:Arial, Helvetica, sans-serif;""> NT$" + atm.ToString("N0") + "</span></td>" +
                        @"</tr>";
                if (credit > 0)
                    _items["credit"] =
                        @"<tr>" +
                        @"  <td>- 刷卡<span style=""color:#6d9345;font-family:Arial, Helvetica, sans-serif;""> NT$" + credit.ToString("N0") + "</span></td>" +
                        @"</tr>";
                _items["total_price"] = _theCashTrustLogCol.Sum(x => (x.Amount - x.DiscountAmount));

                if (_theCashTrustLogCol.Any(x => x.CouponId != null))
                    _items["downloadcoupon"] = "<br/>好康成立後，<a href=\"" + cp.SiteUrl + "/user/coupon_List.aspx\" style=\"text-decoration:underline; color:#BF0000\" target=\"_blank\">請到此下載好康憑證>></a>";
            }
        }
    }
}
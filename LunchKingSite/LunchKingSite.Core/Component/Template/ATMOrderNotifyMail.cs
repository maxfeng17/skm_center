﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.DataOrm;

namespace LunchKingSite.Core.Component.Template
{
    [TemplateFile("atmordernotifymail.txt")]
    public class ATMOrderNotifyMail : TemplateBase
    {
        public ISysConfProvider ServerConfig { get; set; }
        public ATMOrderNotifyMail() : base() {            
        }

        protected List<IViewPponDeal> _promoDeals = null;
        public List<IViewPponDeal> PromoDeals
        {
            get { return _promoDeals; }
            set { _promoDeals = value; }
        }

        public List<ATMOrderInfo> OrderInfos
        {
            get
            {
                return (List<ATMOrderInfo>)_items["order_infos"];
            }
        }

        protected override void Initialize()
        {
            _items.Add("order_account", null);
            _items.Add("order_total", null);
            _items.Add("site_url", null);
            _items.Add("site_service_url", null);
            _items.Add("https_site_url", null);
            _items.Add("item_name", null);
            _items.Add("user_name", null);
            _items.Add("deadline", DateTime.Today.ToString("yyyy-MM-dd") + " 23:59:59");
            _items.Add("order_infos", new List<ATMOrderInfo>());

            _callback = new PrepareItemCallback(PrepareItem);

        }

        protected virtual void PrepareItem()
        {
            // 先建立預設值
            int dealsCount = ServerConfig.PromoMailDealsCount;
            for (int i = 0; i < dealsCount; i++)
            {
                _items["deal" + (i + 1) + "url"] = string.Empty;
                _items["deal" + (i + 1) + "pic"] = string.Empty;
                _items["deal" + (i + 1) + "title"] = string.Empty;
                _items["deal" + (i + 1) + "price"] = string.Empty;
            }

            // 依序設定檔次資料
            for (int i = 0; i < _promoDeals.Count; i++)
            {
                _items["deal" + (i + 1) + "url"] = ServerConfig.SiteUrl + "/deal/" + _promoDeals[i].BusinessHourGuid.ToString() + "?rsrc=17_EDMpay";
                _items["deal" + (i + 1) + "pic"] = Helper.GetImgUrl(_promoDeals[i].EventImagePath);
                _items["deal" + (i + 1) + "title"] = (_promoDeals[i].ItemName.Length > 31) ? (_promoDeals[i].ItemName.Substring(0, 31) + "...") : _promoDeals[i].ItemName;
                _items["deal" + (i + 1) + "price"] = Convert.ToInt32(_promoDeals[i].ItemPrice).ToString("D") + (Helper.IsFlagSet(_promoDeals[i].BusinessHourStatus, BusinessHourStatus.ComboDealMain) ? "起" : string.Empty);
            }

            // 必須有檔次數才要顯示
            _items["show_promo"] = _promoDeals.Count() > 0;
        }

        public string OrderAccount
        {
            get { return (string)_items["order_account"]; }
            set { _items["order_account"] = value; }
        }

        public string OrderTotal
        {
            get { return (string)_items["order_total"]; }
            set { _items["order_total"] = value; }
        }

        public string SiteUrl
        {
            get { return (string)_items["site_url"]; }
            set { _items["site_url"] = value; }
        }

        public string SiteServiceUrl
        {
            get { return (string)_items["site_service_url"]; }
            set { _items["site_service_url"] = value; }
        }

        public string HttpsSiteUrl
        {
            get { return (string)_items["https_site_url"]; }
            set { _items["https_site_url"] = value; }
        }

        public string UserName
        {
            get { return (string)_items["user_name"]; }
            set { _items["user_name"] = value; }
        }

        private string GetImgUrl(string path)
        {
            return ((string.IsNullOrWhiteSpace(path)) ? Helper.GetMediaPathsFromRawData(ServerConfig.EDMDefaultImage, MediaType.PponDealPhoto, ServerConfig.MediaBaseUrl).DefaultIfEmpty(string.Empty).First() : Helper.GetMediaPathsFromRawData(path, MediaType.PponDealPhoto, ServerConfig.MediaBaseUrl).DefaultIfEmpty(string.Empty).First());
        }

        public class ATMOrderInfo
        {
            public string OrderId { get; set; }
            public string DealName { get; set; }
            public string Qty { get; set; }
            public string Price { get; set; }
            public string DealLink { get; set; }
            public string OrderLink { get; set; }
        }
    }
}

﻿using LunchKingSite.DataOrm;

namespace LunchKingSite.Core.Component.Template
{
    public class ShipReferNoticeSellerEmail : TemplateBase
    {
        public ISysConfProvider ServerConfig { get; set; }
        ISysConfProvider cp = ProviderFactory.Instance().GetConfig();

        public ShipReferNoticeSellerEmail()
            : base()
        {
        }

        protected override void Initialize()
        {
            _callback = new PrepareItemCallback(PrepareItem);
        }

        protected virtual void PrepareItem()
        {
            _items["site_url"] = cp.SiteUrl;
            _items["site_service_url"] = cp.SiteServiceUrl;
            _items["seller_name"] = seller_name;
            _items["deal_name"] = deal_name;
            _items["refer_count"] = refer_count;
            _items["unique_id"] = unique_id;
            _items["business_hour_guid"] = business_hour_guid;
        }

        protected string seller_name = null;
        public string Seller_Name
        {
            get { return seller_name; }
            set { seller_name = value; }
        }

        protected string deal_name = null;
        public string Deal_Name
        {
            get { return deal_name; }
            set { deal_name = value; }
        }

        protected string refer_count = null;
        public string Refer_Count
        {
            get { return refer_count; }
            set { refer_count = value; }
        }

        protected string unique_id = null;
        public string Unique_id
        {
            get { return unique_id; }
            set { unique_id = value; }
        }

        protected string business_hour_guid = null;
        public string Business_hour_guid
        {
            get { return business_hour_guid; }
            set { business_hour_guid = value; }
        }
    }
}
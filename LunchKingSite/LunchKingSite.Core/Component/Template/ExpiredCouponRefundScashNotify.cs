﻿using System;
using System.Linq;
using LunchKingSite.DataOrm;
using System.Collections.Generic;
using System.Text;
using System.Web;


namespace LunchKingSite.Core.Component.Template
{
    [TemplateFile("ExpiredCouponRefundScashNotify.txt")]
    public class ExpiredCouponRefundScashNotify : TemplateBase
    {
        public ISysConfProvider ServerConfig { get; set; }
        ISysConfProvider cp = ProviderFactory.Instance().GetConfig();
        protected List<IViewPponDeal> _promoDeals = null;
        public List<IViewPponDeal> PromoDeals
        {
            get { return _promoDeals; }
            set { _promoDeals = value; }
        }
        public ExpiredCouponRefundScashNotify() : base() { }

        protected override void Initialize()
        {
            _items.Add("member_name", null);
            _items.Add("site_url", null);
            _items.Add("site_service_url", null);
            _items.Add("order_id", null);
            _items.Add("item_url", null);
            _items.Add("item_name", null);
            _items.Add("item_price", null);
            _items.Add("refund_scash", null);
            _items.Add("member_scash", null);
            _callback = new PrepareItemCallback(PrepareItem);
        }

        protected virtual void PrepareItem()
        {
            // 依序設定檔次資料
            List<AdDeal> adDeals = new List<AdDeal>();
            for (int i = 0; i < _promoDeals.Count; i++)
            {
                adDeals.Add(new AdDeal
                {
                    Url = cp.SiteUrl + "/deal/" + _promoDeals[i].BusinessHourGuid.ToString() + "?rsrc=17_EDMpay",
                    Pic = Helper.GetImgUrl(_promoDeals[i].EventImagePath),
                    Title = _promoDeals[i].ItemName.TrimToMaxLength(31, "..."),
                    Price = Convert.ToInt32(_promoDeals[i].ItemPrice).ToString("D") +
                            (Helper.IsFlagSet(_promoDeals[i].BusinessHourStatus, BusinessHourStatus.ComboDealMain) ? "起" : string.Empty)
                });
                if (adDeals.Count == cp.PromoMailDealsCount)
                {
                    break;
                }
            }
            _items["ad_deals"] = adDeals;
        }

        public string MemberName 
        {
            get { return (string)_items["member_name"]; }
            set { _items["member_name"] = value; }
        }

        public string SiteUrl
        {
            get { return (string)_items["site_url"]; }
            set { _items["site_url"] = value; }
        }

        public string SiteServiceUrl
        {
            get { return (string)_items["site_service_url"]; }
            set { _items["site_service_url"] = value; }
        }

        public string OrderId
        {
            get { return (string)_items["order_id"]; }
            set { _items["order_id"] = value; }
        }

        public string ItemUrl
        {
            get { return (string)_items["item_url"]; }
            set { _items["item_url"] = value; }
        }

        public string ItemName
        {
            get { return (string)_items["item_name"]; }
            set { _items["item_name"] = value; }
        }

        public string ItemPrice
        {
            get { return (string)_items["item_price"]; }
            set { _items["item_price"] = value; }
        }

        public string RefundScash
        {
            get { return (string)_items["refund_scash"]; }
            set { _items["refund_scash"] = value; }
        }

        public string MemberScash
        {
            get { return (string)_items["member_scash"]; }
            set { _items["member_scash"] = value; }
        }

        public class AdDeal
        {
            public string Url { get; set; }
            public string Pic { get; set; }
            public string Title { get; set; }
            public string Price { get; set; }

            public AdDeal()
            {
                this.Url = string.Empty;
                this.Pic = string.Empty;
                this.Title = string.Empty;
                this.Price = string.Empty;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.DataOrm;

namespace LunchKingSite.Core.Component.Template
{
    [TemplateFile("memberregistersuccessmail.txt")]
    public class MemberRegisterSuccessMail : TemplateBase
    {
        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();

        public override string ToString()
        {
            PreparePromoDealsInfo();
            return base.ToString();
        }

        public int UserId
        {
            set
            {
                _items["mobile_auth_url"] =
                    Helper.CombineUrl(config.SiteUrl, "NewMember/MobileAuth.aspx?skipIfAuthed=1");
            }
        }        

        protected List<IViewPponDeal> _promoDeals = null;
        public List<IViewPponDeal> PromoDeals
        {
            get { return _promoDeals; }
            set { _promoDeals = value; }
        }

        protected override void Initialize()
        {
            _items.Add("mobile_auth_url", string.Empty);
            _items.Add("site_url", config.SiteUrl);
            _items.Add("site_service_url", config.SiteServiceUrl);
        }
        
        protected void PreparePromoDealsInfo()
        {
            // 依序設定檔次資料
            List<AdDeal> adDeals = new List<AdDeal>();
            for (int i = 0; i < _promoDeals.Count; i++)
            {
                adDeals.Add(new AdDeal
                {
                    Url = config.SiteUrl + "/deal/" + _promoDeals[i].BusinessHourGuid.ToString() + "?rsrc=17_EDMpay",
                    Pic = Helper.GetImgUrl(_promoDeals[i].EventImagePath),
                    Title = _promoDeals[i].ItemName.TrimToMaxLength(31, "..."),
                    Price = Convert.ToInt32(_promoDeals[i].ItemPrice).ToString("D") +
                            (Helper.IsFlagSet(_promoDeals[i].BusinessHourStatus, BusinessHourStatus.ComboDealMain) ? "起" : string.Empty)
                });
                if (adDeals.Count == config.PromoMailDealsCount)
                {
                    break;
                }
            }
            _items["ad_deals"] = adDeals;

        }

        public class AdDeal
        {
            public string Url { get; set; }
            public string Pic { get; set; }
            public string Title { get; set; }
            public string Price { get; set; }

            public AdDeal()
            {
                this.Url = string.Empty;
                this.Pic = string.Empty;
                this.Title = string.Empty;
                this.Price = string.Empty;
            }
        }
    }
}

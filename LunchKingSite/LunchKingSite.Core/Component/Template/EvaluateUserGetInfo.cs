﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.Core.Component.Template
{
    [TemplateFile("EvaluateUserGetInfo.txt")]
    public class EvaluateUserGetInfo : TemplateBase
    {
        protected override void Initialize()
        {
            _items.Add("site_url", null);
            _items.Add("site_service_url", null);
            _items.Add("member_name", null);
            _items.Add("bid_list", null);
            _items.Add("bid_name", null);
            _items.Add("bid_short_title", null);
        }


        public string MemberName
        {
            get { return (string)_items["member_name"]; }
            set { _items["member_name"] = value; }
        }

        public string SiteUrl
        {
            get { return (string)_items["site_url"]; }
            set { _items["site_url"] = value; }
        }

        public string SiteServiceUrl
        {
            get { return (string)_items["site_service_url"]; }
            set { _items["site_service_url"] = value; }
        }

        public string Bid_List
        {
            get { return (string)_items["bid_list"]; }
            set { _items["bid_list"] = value; }
        }

        public string Bid_Name
        {
            get { return (string)_items["bid_name"]; }
            set { _items["bid_name"] = value; }
        }

        public string Bid_Short_Title
        {
            get { return (string)_items["bid_short_title"]; }
            set { _items["bid_short_title"] = value; }
        }
    }
}

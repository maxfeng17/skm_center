﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace LunchKingSite.Core.Component.Template
{
    [TemplateFile("NewShipOrderToSellerMail.txt")]
    public class NewShipOrderToSellerMail : TemplateBase
    {
        public NewShipOrderToSellerMail() : base() { }

        protected override void Initialize()
        {
            _items.Add("seller_name", string.Empty);
            _items.Add("vbs_ship_url", string.Empty);
            _items.Add("order_ship_list", new List<NewOrderShip>());
            
            _items.Add("site_url", string.Empty);
            _items.Add("https_site_url", string.Empty);
            _items.Add("site_service_url", string.Empty);
            _items.Add("is_wms", false);
        }

        /// <summary>
        /// 商家名稱
        /// </summary>
        public string SellerName
        {
            get { return (string)_items["seller_name"]; }
            set { _items["seller_name"] = value; }
        }
        /// <summary>
        /// 商家訂單管理url
        /// </summary>
        public string VbsShipUrl
        {
            get { return (string)_items["vbs_ship_url"]; }
            set { _items["vbs_ship_url"] = value; }
        }

        /// <summary>
        /// 出貨清單
        /// </summary>
        public List<NewOrderShip> OrderShipList
        {
            get { return (List<NewOrderShip>)_items["order_ship_list"]; }
            set { _items["order_ship_list"] = value; }
        }
        
        public string SiteUrl
        {
            get { return (string)_items["site_url"]; }
            set { _items["site_url"] = value; }
        }
        public string HttpsSiteUrl
        {
            get { return (string)_items["https_site_url"]; }
            set { _items["https_site_url"] = value; }
        }
        public string SiteServiceUrl
        {
            get { return (string)_items["site_service_url"]; }
            set { _items["site_service_url"] = value; }
        }
        public bool IsWms
        {
            get { return (bool)_items["is_wms"]; }
            set { _items["is_wms"] = value; }
        }


        public class NewOrderShip
        {
            public string OrderId { get; set; }
            public string ShipTypeDesc { get; set; }
            public string LastShipDate { get; set; }
            public string ItemName { get; set; }
            public int Count { get; set; }
            public string ReceiveName { get; set; }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.Core.Component.Template
{
   [TemplateFile("VerificationNoticeEmail.txt")]
    public class VerificationNoticeEmail : TemplateBase
    {
       public VerificationNoticeEmail() : base() { }

        protected override void Initialize()
        {
            _items.Add("siteUrl", null);
            
            _items.Add("sellerName",string.Empty);
            _items.Add("tbody_DealList", string.Empty);
        }

        public string siteUrl
        {
            get { return (string)_items["siteUrl"]; }
            set { _items["siteUrl"] = value; }
        }

        public string sellerName
        {
            get { return (string)_items["sellerName"]; }
            set { _items["sellerName"] = value; }
        }
        public string tbody_DealList
        {
            get { return (string)_items["tbody_DealList"]; }
            set { _items["tbody_DealList"] = value; }
        }
    }

}

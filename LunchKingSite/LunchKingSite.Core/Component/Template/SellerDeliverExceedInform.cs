﻿using System;
using System.Linq;
using System.Net;
using LunchKingSite.DataOrm;

namespace LunchKingSite.Core.Component.Template
{
    public class SellerDeliverExceedInform : TemplateBase
    {
        ISysConfProvider cp = ProviderFactory.Instance().GetConfig();

        public SellerDeliverExceedInform()
        : base()
        {
            
        }

        protected ViewDealPropertyBusinessHourContent _vdpb = null;
        public ViewDealPropertyBusinessHourContent Vdpb
        {
            get { return _vdpb; }
            set { _vdpb = value; }
        }

        protected ViewPponDeal _mainDeal = null;
        public ViewPponDeal MainDeal
        {
            get { return _mainDeal; }
            set { _mainDeal = value; }
        }

        public string MailSubject 
        {
            get { return "超過出貨期限未完成出貨通知" + "_［" + _vdpb.UniqueId + "］" + _vdpb.CouponUsage; }
        }

        protected override void Initialize()
        {
            _callback = new PrepareItemCallback(PrepareItem);
        }

        protected virtual void PrepareItem()
        {
            _items["site_url"] = cp.SiteUrl;
            if (_vdpb != null)
            {
                _items["item_name"] = "［" + _vdpb.UniqueId + "］" + _vdpb.CouponUsage;
                _items["dateS"] = _vdpb.BusinessHourDeliverTimeS != null ? _vdpb.BusinessHourDeliverTimeS.Value.ToString("yyyy/MM/dd") : string.Empty;
                _items["dateE"] = _vdpb.BusinessHourDeliverTimeE != null ? Convert.ToDateTime(_vdpb.BusinessHourDeliverTimeE.Value).ToString("yyyy/MM/dd") : string.Empty;
                TimeSpan dayDiff = Convert.ToDateTime(_vdpb.BusinessHourDeliverTimeE).Date - DateTime.Now.Date;
                _items["dateDiffstring"] = dayDiff.Days > 0 ? "於" + dayDiff.Days.ToString() + "天後" : "於今日";
                _items["mail_subject"] = this.MailSubject;
                _items["site_name"] = Dns.GetHostName();
            }
        }
    }
}

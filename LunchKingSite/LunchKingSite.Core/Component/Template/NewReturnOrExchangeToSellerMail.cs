﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace LunchKingSite.Core.Component.Template
{
    [TemplateFile("NewReturnOrExchangeToSellerMail.txt")]
    public class NewReturnOrExchangeToSellerMail : TemplateBase
    {
        public NewReturnOrExchangeToSellerMail() : base() { }

        protected override void Initialize()
        {
            _items.Add("return_info", new ProductRefundOrExchangeModel());
            
            _items.Add("site_url", string.Empty);
            _items.Add("https_site_url", string.Empty);
            _items.Add("site_service_url", string.Empty);
        }
        
        /// <summary>
        /// 退/換貨資料
        /// </summary>
        public List<ProductRefundOrExchangeModel> ReturnInfo
        {
            get { return (List<ProductRefundOrExchangeModel>)_items["return_info"]; }
            set { _items["return_info"] = value; }
        }
        
        public string SiteUrl
        {
            get { return (string)_items["site_url"]; }
            set { _items["site_url"] = value; }
        }
        public string HttpsSiteUrl
        {
            get { return (string)_items["https_site_url"]; }
            set { _items["https_site_url"] = value; }
        }
        public string SiteServiceUrl
        {
            get { return (string)_items["site_service_url"]; }
            set { _items["site_service_url"] = value; }
        }

        /// <summary>
        /// 退/換貨資料
        /// </summary>
        public class ProductRefundOrExchangeModel
        {
            /// <summary>
            /// 退/換貨類型
            /// </summary>
            public bool IsExchange { get; set; }
            /// <summary>
            /// 申請日
            /// </summary>
            public string CreateTime { get; set; }
            /// <summary>
            /// 訂單編號
            /// </summary>
            public string OrderId { get; set; }
            /// <summary>
            /// 檔號
            /// </summary>
            public int DealUniqueId { get; set; }
            /// <summary>
            /// 收件者
            /// </summary>
            public string ReciverName { get; set; }
            /// <summary>
            /// 商品名稱
            /// </summary>
            public string ItemName { get; set; }
            /// <summary>
            /// 退/換貨(規格)數量
            /// </summary>
            public string SpecAndQuanty { get; set; }
            /// <summary>
            /// 退貨原因與備註
            /// </summary>
            public string Reason { get; set; }
            /// <summary>
            /// 賣家Guid
            /// </summary>
            public Guid SellerGuid { get; set; }
            /// <summary>
            /// 賣家名稱
            /// </summary>
            public string SellerName { get; set; }
            /// <summary>
            /// 商家退/換貨頁面連結
            /// </summary>
            public string VbsUrl { get; set; }
            /// <summary>
            /// 檔號查詢連結
            /// </summary>
            public string VbsDealIdUrl { get; set; }
            /// <summary>
            /// 訂單查詢連結
            /// </summary>
            public string VbsOrderIdUrl { get; set; }
            /// <summary>
            /// 廠商退貨最晚完成日期
            /// </summary>
            public string LastReturnDate { get; set; }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.Core.Component.Template
{
    public class FlexibleBalanceSheetSystemCreate : TemplateBase
    {
        protected override void Initialize()
        {
            base.Initialize();

            _items["sellerName"] = string.Empty;
            _items["dealInfo"] = string.Empty;
            _items["siteUrl"] = string.Empty;
            _items["siteServiceUrl"] = string.Empty;
        }

        public string SellerName
        {
            get { return (string)_items["sellerName"]; }
            set { _items["sellerName"] = value; }
        }

        public string DealInfo
        {
            get { return (string)_items["dealInfo"]; }
            set { _items["dealInfo"] = value; }
        }

        public string SiteUrl
        {
            get { return (string)_items["siteUrl"]; }
            set { _items["siteUrl"] = value; }
        }

        public string SiteServiceUrl
        {
            get { return (string)_items["siteServiceUrl"]; }
            set { _items["siteServiceUrl"] = value; }
        }
    }
}

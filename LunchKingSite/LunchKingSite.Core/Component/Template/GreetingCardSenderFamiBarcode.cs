﻿using System;
using System.Collections.Generic;

namespace LunchKingSite.Core.Component.Template
{
    [TemplateFile("greetingcardsenderfamibarcode.txt")]
    public class GreetingCardSenderFamiBarcode : TemplateBase
    {
        ISysConfProvider cp = ProviderFactory.Instance().GetConfig();
        public GreetingCardSenderFamiBarcode() : base() { }

        protected override void Initialize()
        {
            _items["barcodePic"] = string.Empty;
            _items["dealName"] = string.Empty;
            _items["barcode"] = string.Empty;
            _items["deliverDateStart"] = string.Empty;
            _items["deliverDateEnd"] = string.Empty;
            _items["site_url"] = cp.SiteUrl;
            _items["site_service_url"] = cp.SiteServiceUrl;
        }

        public string BarcodePic
        {
            get { return (string)_items["barcodePic"]; }
            set { _items["barcodePic"] = value; }
        }

        public string DealName
        {
            get { return (string)_items["dealName"]; }
            set { _items["dealName"] = value; }
        }

        public string Barcode
        {
            get { return (string)_items["barcode"]; }
            set { _items["barcode"] = value; }
        }

        public string DeliverDateStart
        {
            get { return (string)_items["deliverDateStart"]; }
            set { _items["deliverDateStart"] = value; }
        }

        public string DeliverDateEnd
        {
            get { return (string)_items["deliverDateEnd"]; }
            set { _items["deliverDateEnd"] = value; }
        }
    }
}

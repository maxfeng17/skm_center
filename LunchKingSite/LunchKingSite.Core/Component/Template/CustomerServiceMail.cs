﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.Core.Component.Template
{
    [TemplateFile("customerservicemail.txt")]
    public class CustomerServiceMail : TemplateBase
    {
        public CustomerServiceMail() : base() { }

        protected override void Initialize()
        {
            _items.Add("member_name", null);
            _items.Add("content_des", null);
            _items.Add("content", null);
            _items.Add("site_service_redirect_url", null);
            _items.Add("site_url", null);
            _items.Add("site_service_url", null);
            _items.Add("https_site_url", null);
        }

        public string MemberName
        {
            get { return (string)_items["member_name"]; }
            set { _items["member_name"] = value; }
        }

        public string ContentDes
        {
            get { return (string)_items["content_des"]; }
            set { _items["content_des"] = value; }
        }

        public string Content
        {
            get { return (string)_items["content"]; }
            set { _items["content"] = value; }
        }

        public string SiteServiceRedirectUrl
        {
            get { return (string)_items["site_service_redirect_url"]; }
            set { _items["site_service_redirect_url"] = value; }
        }

        public string SiteUrl
        {
            get { return (string)_items["site_url"]; }
            set { _items["site_url"] = value; }
        }
        public string SiteServiceUrl
        {
            get { return (string)_items["site_service_url"]; }
            set { _items["site_service_url"] = value; }
        }

        public string ServiceName
        {
            get { return (string)_items["service_name"]; }
            set { _items["service_name"] = value; }
        }

        public string HttpsSiteUrl
        {
            get { return (string)_items["https_site_url"]; }
            set { _items["https_site_url"] = value; }
        }
    
    }
}

﻿using System.Collections.Generic;
using LunchKingSite.DataOrm;
using System;
namespace LunchKingSite.Core.Component.Template
{
    public class ZeroDealSuccessMail : TemplateBase
    {
        ISysConfProvider cp = ProviderFactory.Instance().GetConfig();

        public ZeroDealSuccessMail() : base() { }

        protected override void Initialize()
        {
            _items["mailto"] = string.Empty;
            _items["dealname"] = string.Empty;
            _items["Introduction"] = string.Empty;
            _items["sellername"] = string.Empty;
            _items["coupon"] = string.Empty;
            _items["PEZeventCouponDownload"] = string.Empty;
            _items["site_url"] = cp.SiteUrl;
            _items["site_service_url"] = cp.SiteServiceUrl;
            _items["isZeroActivityShowCoupon"] = false;
            _callback = new PrepareItemCallback(PrepareItem);
        }

        protected virtual void PrepareItem()
        {
            #region 推薦好康
            // 檔次不夠時設定預設值
            int dealsCount = cp.PromoMailDealsCount;
            for (int i = cp.PromoMailDealsCount; i - _cityDeal.Count > 0; i--)
            {
                _items["deal" + i + "title"] = string.Empty;
                _items["deal" + i + "url"] = string.Empty;
                _items["deal" + i + "price"] = string.Empty;
                _items["deal" + i + "pic"] = string.Empty;
            }

            // 依序設定檔次資料
            for (int i = 0; i < _cityDeal.Count; i++)
            {
                _items["deal" + (i + 1) + "title"] = (_cityDeal[i].ItemName.Length > 31) ? (_cityDeal[i].ItemName.Substring(0, 31) + "...") : _cityDeal[i].ItemName;
                _items["deal" + (i + 1) + "url"] = cp.SiteUrl + "/deal/" + _cityDeal[i].BusinessHourGuid.ToString() + "?rsrc=17_EDMpay";
                _items["deal" + (i + 1) + "price"] = Convert.ToInt32(_cityDeal[i].ItemPrice).ToString("D") + (Helper.IsFlagSet(_cityDeal[i].BusinessHourStatus, BusinessHourStatus.ComboDealMain) ? "起" : string.Empty);
                _items["deal" + (i + 1) + "pic"] = Helper.GetImgUrl(_cityDeal[i].EventImagePath);
            }
            #endregion
        }

        public string MailTo
        {
            get { return (string)_items["mailto"]; }
            set { _items["mailto"] = value; }
        }

        public string DealName
        {
            get { return (string)_items["dealname"]; }
            set { _items["dealname"] = value; }
        }
        public string Introduction
        {
            get { return (string)_items["Introduction"]; }
            set { _items["Introduction"] = value; }
        }
        public string SellerName
        {
            get { return (string)_items["sellername"]; }
            set { _items["sellername"] = value; }
        }
        public string Coupon
        {
            get { return (string)_items["coupon"]; }
            set { _items["coupon"] = value; }
        }
        public string SequenceNumber
        {
            get { return (string)_items["sequenceNumber"]; }
            set { _items["sequenceNumber"] = value; }
        }
        public string Code
        {
            get { return (string)_items["code"]; }
            set { _items["code"] = value; }
        }
        public string DeliveryDateStart
        {
            get { return (string)_items["deliveryDateStart"]; }
            set { _items["deliveryDateStart"] = value; }
        }
        public string DeliveryDateEnd
        {
            get { return (string)_items["deliveryDateEnd"]; }
            set { _items["deliveryDateEnd"] = value; }
        }
        public string StoreName
        {
            get { return (string)_items["storeName"]; }
            set { _items["storeName"] = value; }
        }
        public string ChangedExpireDate
        {
            get { return (string)_items["changedExpireDate"]; }
            set { _items["changedExpireDate"] = value; }
        }
        public string PEZeventCouponDownload
        {
            get { return (string)_items["PEZeventCouponDownload"]; }
            set { _items["PEZeventCouponDownload"] = value; }
        }

        protected List<IViewPponDeal> _cityDeal = null;
        public List<IViewPponDeal> CityDeal
        {
            get { return _cityDeal; }
            set { _cityDeal = value; }
        }

        public bool IsZeroActivityShowCoupon
        {
            get { return (bool)_items["isZeroActivityShowCoupon"]; }
            set { _items["isZeroActivityShowCoupon"] = value; }
        }

    }
}

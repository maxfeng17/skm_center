﻿using System;
using LunchKingSite.DataOrm;

namespace LunchKingSite.Core.Component.Template
{
    public class PponItemAuthMail : TemplateBase
    {
        public PponItemAuthMail()
            : base()
        {
        }

        protected string _orderName = string.Empty;
        public string OrderName
        {
            get { return _orderName; }
            set { _orderName = value; }
        }

        protected string _system_entries = string.Empty;
        public string SystemEntries
        {
            get { return _system_entries; }
            set { _system_entries = value; }
        }

        protected ViewPponDeal _theDeal = null;
        public ViewPponDeal TheDeal
        {
            get { return _theDeal; }
            set { _theDeal = value; }
        }

        protected ViewPponOrderDetail _theOrderDetail = null;
        public ViewPponOrderDetail TheOrderDetail
        {
            get { return _theOrderDetail; }
            set { _theOrderDetail = value; }
        }

        protected override void Initialize()
        {
            _callback = new PrepareItemCallback(PrepareItem);
        }

        protected virtual void PrepareItem()
        {
            _items["system_entries"] = _system_entries;

            PrepareDealInfo();
            PrepareOrderDetailInfo();

            if (_theOrderDetail.Amount == 0)
            {
                _items["charging_status"] = "以紅利全額扣抵，信用卡不會扣款";
                _items["refund"] = "交易即取消，紅利點數會全額退回";
                _items["creditcard_charging"] = string.Empty;
            }
            else
            {
                _items["charging_status"] = "尚未扣款";
                _items["refund"] = "刷卡授權即取消，您的信用卡不會扣款";
                _items["creditcard_charging"] = "<br /><p>17Life將於好康成立後<font color='#FF0000'>" + _theDeal.BusinessHourOrderTimeE.ToString("M/d") + " PM9:00</font>向信用卡中心請款。</p>";
            }
        }

        protected void PrepareDealInfo()
        {
            if (_theDeal != null)
            {
                _items["event_name"] = _theDeal.EventName;
                _items["business_hour_order_minimum"] = _theDeal.BusinessHourOrderMinimum;
                _items["business_hour_order_time_e"] = _theDeal.BusinessHourOrderTimeE.ToString("M/d tth:mm");
            }
        }

        protected void PrepareOrderDetailInfo()
        {
            if (_theOrderDetail != null)
            {
                _items["member_name"] = _orderName;
                _items["item_quantity"] = _theOrderDetail.ItemQuantity;
                _items["order_detail_total"] = _theOrderDetail.OrderDetailTotal;
                _items["amount"] = _theOrderDetail.Amount;

                _items["pcash_warning"] = string.Empty;
                if (_theOrderDetail.PaymentType == (int)PaymentType.PCash)
                    _items["pcash_warning"] = "或付費方式為PayEasy購物金且餘額足以折抵本次消費，<br/>";
            }
        }
    }
}
﻿using System.Collections.Generic;
using LunchKingSite.DataOrm;
using System;

namespace LunchKingSite.Core.Component.Template
{
    public class WmsPchomeAtmOrderFailMail : TemplateBase
    {
        ISysConfProvider cp = ProviderFactory.Instance().GetConfig();

        public WmsPchomeAtmOrderFailMail() : base() { }

        protected override void Initialize()
        {
            _items["member_name"] = string.Empty;
            _items["order_date"] = string.Empty;
            _items["order_id"] = string.Empty;
            _items["deal_name"] = string.Empty;
            _items["quantity"] = 0;
            _items["order_status"] = string.Empty;
            _items["atm"] = 0;
            _items["site_url"] = cp.SiteUrl;
            _items["site_service_url"] = cp.SiteServiceUrl;
        }

        public string MemberName
        {
            get { return (string)_items["member_name"]; }
            set { _items["member_name"] = value; }
        }

        public string OrderDate
        {
            get { return (string)_items["order_date"]; }
            set { _items["order_date"] = value; }
        }

        public string OrderId
        {
            get { return (string)_items["order_id"]; }
            set { _items["order_id"] = value; }
        }
        public string DealName
        {
            get { return (string)_items["deal_name"]; }
            set { _items["deal_name"] = value; }
        }
        public int Quantity
        {
            get { return (Int16)_items["quantity"]; }
            set { _items["quantity"] = value; }
        }
        public string OrderStatus
        {
            get { return (string)_items["order_status"]; }
            set { _items["order_status"] = value; }
        }
        public int Atm
        {
            get { return (Int32)_items["atm"]; }
            set { _items["atm"] = value; }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.Core.Component.Template
{
    [TemplateFile("ReservationCancelMail.txt")]
    public class ReservationCancelMail : TemplateBase
    {
        public ReservationCancelMail() : base() { }

        protected override void Initialize()
        {
            _items.Add("site_url", null);
            _items.Add("site_service_url", null);

            _items.Add("member_name", string.Empty);
            _items.Add("seller_name", string.Empty);
            _items.Add("store_name", string.Empty);
        }

        public string SiteUrl
        {
            get { return (string)_items["site_url"]; }
            set { _items["site_url"] = value; }
        }

        public string SiteServiceUrl
        {
            get { return (string)_items["site_service_url"]; }
            set { _items["site_service_url"] = value; }
        }

        public string MemberName
        {
            get { return (string)_items["member_name"]; }
            set { _items["member_name"] = value; }
        }
        public string SellerName
        {
            get { return (string)_items["seller_name"]; }
            set { _items["seller_name"] = value; }
        }
        public string StoreName
        {
            get { return (string)_items["store_name"]; }
            set { _items["store_name"] = value; }
        }

    }
}

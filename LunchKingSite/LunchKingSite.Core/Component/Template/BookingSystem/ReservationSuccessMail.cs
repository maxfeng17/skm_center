﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.Core.Component.Template
{
   [TemplateFile("ReservationSuccessMail.txt")]
    public class ReservationSuccessMail : TemplateBase
    {
        public ReservationSuccessMail() : base() { }

        protected override void Initialize()
        {
            _items.Add("site_url", null);
            _items.Add("site_service_url", null);

            _items.Add("member_name",string.Empty);
            _items.Add("reservation_date",string.Empty);
            _items.Add("reservation_date_of_week", string.Empty);
            _items.Add("reservation_timeslot",string.Empty);
            _items.Add("number_of_people",default(int));
            _items.Add("member_email", string.Empty);
            _items.Add("contact_name", string.Empty);
            _items.Add("contact_number", string.Empty);
            _items.Add("remark", string.Empty);
            _items.Add("coupon_sequence",string.Empty);
            _items.Add("coupon_info", string.Empty);
            _items.Add("coupon_link",string.Empty);
            _items.Add("seller_name", string.Empty);
            _items.Add("store_name", string.Empty);
            _items.Add("store_address", string.Empty);
            _items.Add("is_concur_coupon",default(bool));
        }

        public string SiteUrl
        {
            get { return (string)_items["site_url"]; }
            set { _items["site_url"] = value; }
        }

        public string SiteServiceUrl
        {
            get { return (string)_items["site_service_url"]; }
            set { _items["site_service_url"] = value; }
        }

        public string MemberName
        {
            get { return (string)_items["member_name"]; }
            set { _items["member_name"] = value; }
        }
        public string ReservationDate
        {
            get { return (string)_items["reservation_date"]; }
            set { _items["reservation_date"] = value; }
        }
        public string ReservationDateOfWeek
        {
            get { return (string)_items["reservation_date_of_week"]; }
            set { _items["reservation_date_of_week"] = value; }
        }
        public string ReservationTimeSlot
        {
            get { return (string)_items["reservation_timeslot"]; }
            set { _items["reservation_timeslot"] = value; }
        }
        public int NumberOfPeople
        {
            get { return (int)_items["number_of_people"]; }
            set { _items["number_of_people"] = value; }
        }
        public string MemberEmail
        {
            get { return (string)_items["member_email"]; }
            set { _items["member_email"] = value; }
        }
        public string ContactName
        {
            get { return (string)_items["contact_name"]; }
            set { _items["contact_name"] = value; }
        }
        public string ContactNumber
        {
            get { return (string)_items["contact_number"]; }
            set { _items["contact_number"] = value; }
        }
        public string Remark
        {
            get { return (string)_items["remark"]; }
            set { _items["remark"] = value; }
        }
        public string CouponSequence
        {
            get { return (string)_items["coupon_sequence"]; }
            set { _items["coupon_sequence"] = value; }
        }
       public string CouponInfo
        {
            get { return (string)_items["coupon_info"]; }
            set { _items["coupon_info"] = value; }
        }
       public string CouponLink
       {
           get { return (string)_items["coupon_link"]; }
           set { _items["coupon_link"] = value; }
       }
       public string SellerName
        {
            get { return (string)_items["seller_name"]; }
            set { _items["seller_name"] = value; }
        }
        public string StoreName
        {
            get { return (string)_items["store_name"]; }
            set { _items["store_name"] = value; }
        }
        public string StoreAddress
        {
            get { return (string)_items["store_address"]; }
            set { _items["store_address"] = value; }
        }
       public bool IsConcurCoupon
       {
           get { return (bool)_items["is_concur_coupon"]; }
           set { _items["is_concur_coupon"] = value; }
       }
    }

}

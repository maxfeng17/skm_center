﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.Core.Component.Template
{
    [TemplateFile("BookingSettingChangeLogMail.txt")]
    public class BookingSettingChangeLogMail : TemplateBase
    {
        public BookingSettingChangeLogMail(): base() { }

        protected override void Initialize()
        {
            _items.Add("site_url", null);
            _items.Add("site_service_url", null);

            _items.Add("vbs_account", string.Empty);
            _items.Add("seller_name", string.Empty);
            _items.Add("store_name", string.Empty);
            _items.Add("modify_time", string.Empty);
            _items.Add("change_setting_logs",string.Empty);
        }

        public string SiteUrl
        {
            get { return (string)_items["site_url"]; }
            set { _items["site_url"] = value; }
        }

        public string SiteServiceUrl
        {
            get { return (string)_items["site_service_url"]; }
            set { _items["site_service_url"] = value; }
        }

        public string VbsAccount
        {
            get { return (string)_items["vbs_account"]; }
            set { _items["vbs_account"] = value; }
        }

        public string SellerName
        {
            get { return (string)_items["seller_name"]; }
            set { _items["seller_name"] = value; }
        }

        public string StoreName
        {
            get { return (string)_items["store_name"]; }
            set { _items["store_name"] = value; }
        }

        public string ModifyTime
        {
            get { return (string)_items["modify_time"]; }
            set { _items["modify_time"] = value; }
        }

        public string ChangeSettingLogs
        {
            get { return (string)_items["change_setting_logs"]; }
            set { _items["change_setting_logs"] = value; }
        }


    }
}

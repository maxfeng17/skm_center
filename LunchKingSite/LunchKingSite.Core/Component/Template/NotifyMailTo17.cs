﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.Core.Component.Template
{

    [TemplateFile("NotifyMailTo17.txt")]
    public class NotifyMailTo17 : TemplateBase
    {
        protected override void Initialize()
        {
            _items.Add("mailContent", null);
            _items.Add("site_url", null);
        }

        public string MailContent
        {
            get { return (string)_items["mailContent"]; }
            set { _items["mailContent"] = value; }
        }        
    }
}

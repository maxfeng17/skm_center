﻿using System;
using System.Linq;
using LunchKingSite.DataOrm;
using System.Collections.Generic;

namespace LunchKingSite.Core.Component.Template
{
    public class DiscountCodeAutoSendMail : TemplateBase
    {
        public ISysConfProvider ServerConfig { get; set; }
        ISysConfProvider cp = ProviderFactory.Instance().GetConfig();
        public DiscountCodeAutoSendMail()
            : base()
        {
        }

        protected string _memberName = null;
        public string MemberName
        {
            get { return _memberName; }
            set { _memberName = value; }
        }

        protected string _eventName = null;
        public string eventName
        {
            get { return _eventName; }
            set { _eventName = value; }
        }

        protected string _discountAmount = null;
        public string discountAmount
        {
            get { return _discountAmount; }
            set { _discountAmount = value; }
        }
        protected string _discountLimit = null;
        public string discountLimit
        {
            get { return _discountLimit; }
            set { _discountLimit = value; }
        }

        protected string _discountName = null;
        public string discountName
        {
            get { return _discountName; }
            set { _discountName = value; }
        }
        protected List<ViewPponDeal> _promoDeals = null;
        public List<ViewPponDeal> PromoDeals
        {
            get { return _promoDeals; }
            set { _promoDeals = value; }
        }

        protected override void Initialize()
        {
            _callback = new PrepareItemCallback(PrepareItem);
        }

        protected virtual void PrepareItem()
        {
            _items["site_url"] = cp.SiteUrl;
            _items["site_service_url"] = cp.SiteServiceUrl;
            _items["member_name"] = _memberName;
            _items["event_name"] = eventName;

            PreparePromoDealsInfo();
            PrepareDiscountCodeInfo();
        }

        protected void PreparePromoDealsInfo()
        {
            // 先建立預設值
            int dealsCount = ServerConfig.PromoMailDealsCount;
            for (int i = 0; i < dealsCount; i++)
            {
                _items["deal" + (i + 1) + "url"] = string.Empty;
                _items["deal" + (i + 1) + "pic"] = string.Empty;
                _items["deal" + (i + 1) + "title"] = string.Empty;
                _items["deal" + (i + 1) + "price"] = string.Empty;
            }

            // 依序設定檔次資料
            //for (int i = 0; i < _promoDeals.Count; i++)
            //{
            //    _items["deal" + (i + 1) + "url"] = ServerConfig.SiteUrl + "/" + _promoDeals[i].BusinessHourGuid.ToString() + "/17_EDMpay";
            //    _items["deal" + (i + 1) + "pic"] = Helper.GetImgUrl(_promoDeals[i].EventImagePath);
            //    _items["deal" + (i + 1) + "title"] = (_promoDeals[i].ItemName.Length > 31) ? (_promoDeals[i].ItemName.Substring(0, 31) + "...") : _promoDeals[i].ItemName;
            //    _items["deal" + (i + 1) + "price"] = Convert.ToInt32(_promoDeals[i].ItemPrice).ToString("D") + (Helper.IsFlagSet(_promoDeals[i].BusinessHourStatus, BusinessHourStatus.ComboDealMain) ? "起" : string.Empty);
            //}

            // 必須有檔次數才要顯示
            _items["show_promo"] = 6;
        }

        protected void PrepareDiscountCodeInfo()
        {
            _items["discount_amount"] = discountAmount;
            _items["discount_limit"] = discountLimit;
            _items["discount_name"] = discountName;
        }
    }
}
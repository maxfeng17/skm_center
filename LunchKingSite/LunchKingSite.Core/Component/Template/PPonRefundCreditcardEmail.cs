﻿using System;
using System.Collections.Generic;
using System.Linq;
using LunchKingSite.DataOrm;

namespace LunchKingSite.Core.Component.Template
{
    public class PponRefundCreditcardEmail : TemplateBase
    {
        public ISysConfProvider ServerConfig { get; set; }
        ISysConfProvider cp = ProviderFactory.Instance().GetConfig();

        public PponRefundCreditcardEmail()
            : base()
        {
        }

        protected ViewPponDeal _theDeal = null;
        public ViewPponDeal TheDeal
        {
            get { return _theDeal; }
            set { _theDeal = value; }
        }

        protected ViewPponOrderDetailCollection _theOrderDetailCol = null;
        public ViewPponOrderDetailCollection TheOrderDetailCol
        {
            get { return _theOrderDetailCol; }
            set { _theOrderDetailCol = value; }
        }

        protected CashTrustLogCollection _theCashTrustLogCol = null;
        public CashTrustLogCollection TheCashTrustLogCol
        {
            get { return _theCashTrustLogCol; }
            set { _theCashTrustLogCol = value; }
        }

        protected List<IViewPponDeal> _promoDeals = null;
        public List<IViewPponDeal> PromoDeals
        {
            get { return _promoDeals; }
            set { _promoDeals = value; }
        }

        protected Order _order = null;
        public Order Order
        {
            get { return _order; }
            set { _order = value; }
        }

        protected override void Initialize()
        {
            _callback = new PrepareItemCallback(PrepareItem);
        }

        protected virtual void PrepareItem()
        {
            _items["site_url"] = cp.SiteUrl;
            _items["site_service_url"] = cp.SiteServiceUrl;
            _items["https_site_url"] = cp.SiteUrl.Replace("http", "https");

            _items["credit"] = string.Empty;
            _items["atm"] = string.Empty;
            _items["pcash"] = string.Empty;
            _items["bcash"] = string.Empty;
            _items["scash"] = string.Empty;
            _items["freight"] = string.Empty;
            _items["discount"] = string.Empty;
            _items["total_price"] = 0;

            _items["r_credit"] = string.Empty;
            _items["r_atm"] = string.Empty;
            _items["r_pcash"] = string.Empty;
            _items["r_bcash"] = string.Empty;
            _items["r_scash"] = string.Empty;
            _items["r_freight"] = string.Empty;
            _items["r_total_price"] = 0;
            PrepareDealInfo();
            PrepareOrderDetailInfo();
            PreparePromoDealsInfo();
            SetOtherItem();
        }

        protected void PreparePromoDealsInfo()
        {
            // 先建立預設值
            int dealsCount = ServerConfig.PromoMailDealsCount;
            for (int i = 0; i < dealsCount; i++)
            {
                _items["deal" + (i + 1) + "url"] = string.Empty;
                _items["deal" + (i + 1) + "pic"] = string.Empty;
                _items["deal" + (i + 1) + "title"] = string.Empty;
                _items["deal" + (i + 1) + "price"] = string.Empty;
            }

            // 依序設定檔次資料
            for (int i = 0; i < _promoDeals.Count; i++)
            {
                _items["deal" + (i + 1) + "url"] = ServerConfig.SiteUrl + "/deal/" + _promoDeals[i].BusinessHourGuid.ToString() + "?rsrc=17_EDMpay";
                _items["deal" + (i + 1) + "pic"] = Helper.GetImgUrl(_promoDeals[i].EventImagePath);
                _items["deal" + (i + 1) + "title"] = (_promoDeals[i].ItemName.Length > 31) ? (_promoDeals[i].ItemName.Substring(0, 31) + "...") : _promoDeals[i].ItemName;
                _items["deal" + (i + 1) + "price"] = Convert.ToInt32(_promoDeals[i].ItemPrice).ToString("D") + (Helper.IsFlagSet(_promoDeals[i].BusinessHourStatus, BusinessHourStatus.ComboDealMain) ? "起" : string.Empty);
            }

            // 必須有檔次數才要顯示
            _items["show_promo"] = _promoDeals.Count() > 0;
        }

        protected void PrepareDealInfo()
        {
            if (_theDeal != null)
            {
                _items["item_name"] = _theDeal.ItemName;
            }
            if (_order != null)
            {
                _items["order_id"] = _order.OrderId;
            }
        }

        protected void PrepareOrderDetailInfo()
        {
            if (_theOrderDetailCol.Count > 0)
            {
                _items["member_name"] = _theOrderDetailCol[0].MemberName;
            }
        }

        protected void SetOtherItem()
        {
            if (_theCashTrustLogCol.Count > 0)
            {
                int credit = _theCashTrustLogCol.Sum(x => x.CreditCard);
                int pcash = _theCashTrustLogCol.Sum(x => x.Pcash);
                int bonus = _theCashTrustLogCol.Sum(x => x.Bcash);
                int scash = _theCashTrustLogCol.Sum(x => x.Scash);
                int atm = _theCashTrustLogCol.Sum(x => x.Atm);
                int discount = _theCashTrustLogCol.Sum(x => x.DiscountAmount);
                int freight = 0;                
                CashTrustLog ctFreight = (_theCashTrustLogCol.Where(x => Helper.IsFlagSet((TrustSpecialStatus)x.SpecialStatus, TrustSpecialStatus.Freight)).Count() > 0) ? _theCashTrustLogCol.Where(x => Helper.IsFlagSet((TrustSpecialStatus)x.SpecialStatus, TrustSpecialStatus.Freight)).First() : null;
                if (ctFreight != null) freight = ctFreight.Amount;

                int rCredit = 0;
                int rScash = 0;
                int rAtm = 0;
                if (_theCashTrustLogCol.First().Status == (int)TrustStatus.Refunded)//購物金轉刷退或ATM
                {
                    scash = scash + credit + atm;
                    rCredit = credit;
                    rAtm = atm;
                    atm = credit = 0;
                }



                if (scash > 0)
                    _items["scash"] = @"<tr><td width=""200"">17Life購物金</td><td>" + scash.ToString("N0") + "元</td></tr>";
                if (bonus > 0)
                    _items["bcash"] = @"<tr><td width=""200"">17Life紅利</td><td>" + bonus.ToString("N0") + "元</td></tr>";
                if (pcash > 0)
                    _items["pcash"] = @"<tr><td width=""200"">PayEasy購物金</td><td>" + pcash.ToString("N0") + "元</td></tr>";
                if (atm > 0)
                    _items["atm"] = @"<tr><td width=""200"">ATM</td><td>" + atm.ToString("N0") + "元</td></tr>";
                if (credit > 0)
                    _items["credit"] = @"<tr><td width=""200"">刷卡</td><td>" + credit.ToString("N0") + "元</td></tr>";
                if (discount > 0)
                    _items["discount"] = @"<tr><td width=""200"">17Life折價券</td><td>" + discount.ToString("N0") + "元</td></tr>";
                if (freight > 0)
                    _items["freight"] = @" (含運費" + freight.ToString() + @"元)";

                _items["total_price"] = _theCashTrustLogCol.Sum(x => x.Amount);

                if (rScash > 0)
                    _items["r_scash"] = @"<tr><td width=""200"">17Life購物金</td><td>" + rScash.ToString("N0") + "元</td></tr>";
                if (bonus > 0)
                    _items["r_bcash"] = @"<tr><td width=""200"">17Life紅利</td><td>" + bonus.ToString("N0") + "元</td></tr>";
                if (pcash > 0)
                    _items["r_pcash"] = @"<tr><td width=""200"">PayEasy購物金</td><td>" + pcash.ToString("N0") + "元</td></tr>";
                if (rAtm > 0)
                    _items["r_atm"] = @"<tr><td width=""200"">ATM</td><td>" + rAtm.ToString("N0") + "元</td></tr>";
                if (rCredit > 0)
                    _items["r_credit"] = @"<tr><td width=""200"">刷卡</td><td>" + rCredit.ToString("N0") + "元</td></tr>";
                if (freight > 0)
                    _items["r_freight"] = @" (含運費" + freight.ToString() + @"元)";

                _items["r_total_price"] = _theCashTrustLogCol.Sum(x => x.Amount) - discount;
            }
        }

        private string GetImgUrl(string path)
        {
            return ((string.IsNullOrWhiteSpace(path)) ? Helper.GetMediaPathsFromRawData(ServerConfig.EDMDefaultImage, MediaType.PponDealPhoto, ServerConfig.MediaBaseUrl).DefaultIfEmpty(string.Empty).First() : Helper.GetMediaPathsFromRawData(path, MediaType.PponDealPhoto, ServerConfig.MediaBaseUrl).DefaultIfEmpty(string.Empty).First());
        }
    }
}
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using LunchKingSite.Core.Component;
using log4net;

namespace LunchKingSite.Core
{
    public abstract class MailAgentBase : IMailAgent
    {
        protected static ISysConfProvider config = ProviderFactory.Instance().GetConfig();

        //有嘗試用 SmtpClient.SendAsync(...) 但失敗，訊息顯示要將Page 的 Async 設成 true 才行。所以改用 Delegate 的方式做非同步
        protected void DoAsyncSend(MailMessage msg, Action successCallback, Action<Exception> failCallback)
        {
            try
            {
                SmtpClient smtpCli = GetSmtpClient();
                smtpCli.Send(msg);
                if (successCallback != null)
                {
                    successCallback();
                }
            }
            catch (Exception ex)
            {
                if (failCallback != null)
                {
                    failCallback(ex);
                }
            }
        }

        public virtual SmtpClient GetSmtpClient()
        {
            return new SmtpClient();
        }

        public abstract bool Send(MailMessage msg);
        public abstract void SendAsync(MailMessage msg, Action successCallback, Action<Exception> failCallback);
    }

    internal sealed class SiteSmtpMailAgent : MailAgentBase
    {
        private SmtpClient _smtpClient = null;
        public SiteSmtpMailAgent()
        {
            _smtpClient = GetSmtpClient();
        }

        public override bool Send(MailMessage msg)
        {
            lock (_smtpClient)
            {
                _smtpClient.Send(msg);
            }
            return true;
        }

        public override void SendAsync(MailMessage msg, Action successCallback, Action<Exception> failCallback)
        {
            new Action<MailMessage, Action, Action<Exception>>(DoAsyncSend)
                .BeginInvoke(msg, successCallback, failCallback, null, null);
        }
    }

    internal class SmtpMailLoadBalanceAgent : MailAgentBase
    {
        public override bool Send(MailMessage msg)
        {
            while (true)
            {
                SmtpClient smtpClient = GetSmtpClient();
                if (smtpClient == null)
                {
                    SmtpClientManager.Save(msg);
                    return false;
                }
                try
                {
                    smtpClient.Send(msg);
                    return true;
                }
                catch (SmtpException)
                {
                    SmtpClientManager.ReportError(smtpClient);
                }
            }
        }

        public override void SendAsync(MailMessage msg, Action successCallback, Action<Exception> failCallback)
        {
            new Action<MailMessage, Action, Action<Exception>>(DoAsyncSend)
                .BeginInvoke(msg, successCallback, failCallback, null, null);
        }

        public override SmtpClient GetSmtpClient()
        {
            return SmtpClientManager.GetSmtpClient();
        }
    }

    /// <summary>
    /// 當執行Send寄發信件時，如果mail的host字串與web.config中ServiceEmail的host字串不相同時，會忽略不寄發
    /// </summary>
    internal sealed class SiteSmtpMailAgentFilterHost : MailAgentBase
    {
        private SmtpClient _smtpClient;
        public SiteSmtpMailAgentFilterHost()
        {
            _smtpClient = GetSmtpClient();
        }

        public override bool Send(MailMessage msg)
        {

            MailAddress serviceEmail = new MailAddress(config.ServiceEmail);

            if (msg.To == null || msg.To.Count == 0)
            {
                return true;
            }
            FilterBySiteOrWrappedDebugGuyMail(msg, serviceEmail);
            if (msg.To.Count == 0)
            {
                return true;
            }
            lock (_smtpClient)
            {
                _smtpClient.Send(msg);
            }
            return true;
        }

        /// <summary>
        /// 否則就進行過瀘，只會寄給serviceEmail 相同 Host 的信箱
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="serviceEmail"></param>
        private static void FilterBySiteOrWrappedDebugGuyMail(MailMessage msg, MailAddress serviceEmail)
        {
            for (int i = msg.To.Count - 1; i >= 0; i--)
            {
                if (string.IsNullOrEmpty(config.DebugGuyEmail))
                {
                    if (msg.To[i].Host.Equals(serviceEmail.Host, StringComparison.OrdinalIgnoreCase) == false)
                    {
                        msg.To.Remove(msg.To[i]);
                    }
                }
                else
                {
                    MailAddress debugGuyMailAddr = new MailAddress(config.DebugGuyEmail);
                    string debugGuyAliasMailAddr = string.Format("{0}+{1}@{2}",
                        debugGuyMailAddr.User, msg.To[i].Address.Replace("@", "."), debugGuyMailAddr.Host);
                    msg.To[i] = new MailAddress(debugGuyAliasMailAddr, msg.To[i].Address);
                }
            }
        }

        public override void SendAsync(MailMessage msg, Action successCallback, Action<Exception> failCallback)
        {
            MailAddress serviceEmail = new MailAddress(config.ServiceEmail);
            if (msg.To == null || msg.To.Count == 0)
            {
                return;
            }
            FilterBySiteOrWrappedDebugGuyMail(msg, serviceEmail);
            if (msg.To.Count == 0)
            {
                return;
            }
            new Action<MailMessage, Action, Action<Exception>>(DoAsyncSend)
                .BeginInvoke(msg, successCallback, failCallback, null, null);
        }
    }



    // this class is supposed to be a singleton
    /// <summary>
    /// mail management singleton
    /// </summary>
    public sealed class PostMan : IJob
    {
        /// <summary>
        /// the mail queue element
        /// </summary>
        private struct MsgQElement
        {
            public MailMessage Message;
            public int AgentIndex;

            public MsgQElement(int index, MailMessage msg)
            {
                Message = msg;
                AgentIndex = index;
            }
        }

        private const int QUEUE_SIZE = 100;

        #region private members
        private static readonly ILog logger = LogManager.GetLogger(typeof(PostMan));
        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        /// <summary>
        /// 紀錄曾經發送過的信件
        /// </summary>
        private static readonly ILog mailLogger = LogManager.GetLogger("MailLog");
        private static PostMan _theMan;
        private Queue<MsgQElement> _mailQueue;
        private List<IMailAgent> _agents;
        private static MailAgent defaultAgent = MailAgent.SiteMailer;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="mm"></param>
        /// <param name="result"></param>
        /// <param name="category"></param>
        /// <param name="mailLogId">如果有值，重送模式，更新既有的MailLog資料</param>
        /// <param name="errorMessage"></param>
        public delegate void MailLogHandler(MailMessage mm, bool result, MailTemplateType category, int? mailLogId, string errorMessage);
        public static event MailLogHandler OnMailLog;
        #endregion

        #region .ctor
        static PostMan()
        {
            _theMan = new PostMan();
        }

        /// <summary>
        /// Returns a reference to the current postman instance
        /// </summary>
        /// <returns></returns>
        public static PostMan Instance()
        {
            return _theMan;
        }

        private PostMan()
        {
            ISysConfProvider config = ProviderFactory.Instance().GetConfig();

            _mailQueue = new Queue<MsgQElement>(QUEUE_SIZE);
            _agents = new List<IMailAgent>(Enum.GetValues(typeof(MailAgent)).Length);
            //_agents.Insert((int)MailAgent.SiteMailer, new SiteSmtpMailAgent());

            switch (config.SmtpMailAgent)
            {
                case SmtpMailServerAgent.SmtpMailAgent:
                    _agents.Insert((int)MailAgent.SiteMailer, new SiteSmtpMailAgent());
                    break;
                case SmtpMailServerAgent.SiteSmtpMailAgentFilterHost:
                    _agents.Insert((int)MailAgent.SiteMailer, new SiteSmtpMailAgentFilterHost());
                    break;
                case SmtpMailServerAgent.SmtpMailLoadBalanceAgent:
                    _agents.Insert((int)MailAgent.SiteMailer, new SmtpMailLoadBalanceAgent());
                    break;
            }
        }
        #endregion

        #region public methods

        public bool Send(MailMessage msg, SendPriorityType priority, MailTemplateType template = MailTemplateType.Other)
        {
            return Send(msg, priority, defaultAgent, template, null);
        }

        public bool Resend(MailMessage msg, MailTemplateType template, int mailLogId)
        {
            return Send(msg, SendPriorityType.Immediate, defaultAgent, template, mailLogId);
        }

        private bool Send(MailMessage msg, SendPriorityType priority, MailAgent agent, MailTemplateType template, int? mailLogId)
        {
            bool retVal = true;
            switch (priority)
            {
                case SendPriorityType.Normal:
                    lock (_mailQueue)
                    {
                        MsgQElement e = new MsgQElement((int)agent, msg);
                        _mailQueue.Enqueue(e);
                    }
                    break;
                case SendPriorityType.Immediate:
                    try
                    {
                        _agents[(int)agent].Send(msg);
                        string toAddrs = string.Join(",", msg.To.Select(t => t.Address).ToArray());
                        mailLogger.InfoFormat("1:{0}:{1}", toAddrs, msg.Subject);
                        if (config.MailLogToDb && OnMailLog != null)
                        {
                            OnMailLog.BeginInvoke(msg, true, template, mailLogId, null, null, null);
                        }
                    }
                    catch (Exception ex)
                    {
                        StringBuilder sbLog = new StringBuilder();
                        sbLog.Append("Something happened while sending mail.");
                        StackTrace st = new StackTrace(new StackFrame(1, true));
                        sbLog.Append(" Stacktrace: ");
                        sbLog.Append(st);
                        if (msg != null)
                        {
                            string toAddrs = string.Join(",", msg.To.Select(t => t.Address).ToArray());
                            sbLog.AppendFormat(
                                ", Maill address: {0}, Subject: {1}",
                                toAddrs,
                                msg.Subject);
                            mailLogger.InfoFormat("0:{0}:{1}", toAddrs, msg.Subject);
                        }
                        logger.Warn(sbLog.ToString(), ex);
                        if (config.MailLogToDb && OnMailLog != null)
                        {
                            OnMailLog.BeginInvoke(msg, false, template, mailLogId, ex.Message, null, null);
                        }
                        retVal = false;
                    }
                    break;
                case SendPriorityType.Async:
                    _agents[(int)agent].SendAsync(msg, delegate ()
                    {
                        string toAddrs = string.Join(",", msg.To.Select(t => t.Address).ToArray());
                        mailLogger.InfoFormat("1:{0}:{1}", toAddrs, msg.Subject);
                        if (config.MailLogToDb && OnMailLog != null)
                        {
                            OnMailLog.BeginInvoke(msg, true, template, mailLogId, null, null, null);
                        }
                    }, delegate (Exception ex)
                    {
                        string toAddrs = string.Join(",", msg.To.Select(t => t.Address).ToArray());
                        mailLogger.InfoFormat("0:{0}:{1}", toAddrs, msg.Subject);
                        logger.Warn("Something happened while sending mail", ex);
                        if (config.MailLogToDb && OnMailLog != null)
                        {
                            OnMailLog.BeginInvoke(msg, false, template, mailLogId, ex.Message, null, null);
                        }
                    });
                    break;
                default:
                    retVal = false;
                    break;
            }

            return retVal;
        }

        public void Execute(Configuration.JobSetting js)
        {
            while (_mailQueue.Count > 0)
            {
                MsgQElement e = _mailQueue.Dequeue();
                _agents[e.AgentIndex].Send(e.Message);
            }
        }

        /// <summary>
        /// this should only be used for unit-testing
        /// </summary>
        /// <param name="agent">the agent instance, can be a mocked object</param>
        /// <returns></returns>
        public bool RegisterDummyAgent(IMailAgent agent)
        {
            _agents[(int)MailAgent.Dummy] = agent;
            return true;
        }
        #endregion
    }

    public class SmtpClientManager
    {
        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        private static object thisLock = new object();
        private static List<SmtpClient> smtpClients = new List<SmtpClient>();
        /// <summary>
        /// how many seconds to retry error server
        /// </summary>
        private const int RetrySeconds = 600;
        private static Dictionary<string, DateTime> errorClients = new Dictionary<string, DateTime>();
        private static ILog logger = LogManager.GetLogger(typeof(SmtpClientManager));

        static SmtpClientManager()
        {
            Init();
        }

        public static void Init()
        {
            lock (thisLock)
            {
                smtpClients.Clear();
                errorClients.Clear();
                SmtpClient defaultClient = new SmtpClient();
                string[] hosts = string.IsNullOrEmpty(config.SmtpOtherHosts)
                    ? new string[] { }
                    : config.SmtpOtherHosts.Split(',');

                smtpClients.Add(defaultClient);
                foreach (string host in hosts)
                {
                    bool isExist = smtpClients.Any(t => t.Host == host);
                    if (isExist)
                    {
                        continue;
                    }
                    SmtpClient client = new SmtpClient();
                    if (host.Contains(':'))
                    {
                        string[] segs = host.Split(':');
                        client.Host = segs[0];
                        int port;
                        if (int.TryParse(segs[1], out port))
                        {
                            client.Port = port;
                        }
                    }
                    else
                    {
                        client.Host = host;
                    }
                    client.DeliveryMethod = SmtpDeliveryMethod.Network;
                    client.Credentials = defaultClient.Credentials;
                    smtpClients.Add(client);
                }
            }
        }
        private static int counter;
        public static int IncCounter()
        {
            //用到的地方要加 lock，所以也沒必要用 Interlocked
            return counter++;
            //return Interlocked.Increment(ref counter);
        }
        public static SmtpClient GetSmtpClient()
        {
            lock (thisLock)
            {
                return GetAliveSmtpClient();
            }
        }

        private static string GetClientKey(SmtpClient client)
        {
            return string.Format("{0}:{1}", client.Host, client.Port);
        }

        private static SmtpClient GetAliveSmtpClient()
        {
            SmtpClient client;
            int tryCount = 0;
            while (true)
            {
                int choice = IncCounter() % smtpClients.Count;
                client = smtpClients[choice];
                if (errorClients.ContainsKey(GetClientKey(client)) == false)
                {
                    break;
                }
                if ((DateTime.Now - errorClients[GetClientKey(client)]).TotalSeconds > RetrySeconds)
                {
                    errorClients.Remove(GetClientKey(client));
                    break;
                }
                tryCount++;
                //就是沒有可用的smtp clients
                if (tryCount >= smtpClients.Count)
                {
                    break;
                }
            }
            return client;
        }


        public static void ReportError(SmtpClient client)
        {
            if (client == null)
            {
                return;
            }
            lock (thisLock)
            {
                if (errorClients.ContainsKey(GetClientKey(client)) == false)
                {
                    errorClients.Add(GetClientKey(client), DateTime.Now);
                    logger.ErrorFormat("SMTP {0} 發生問題，請儘速維修，目前還有 {1} 臺信件伺服器服務中.", GetClientKey(client), smtpClients.Count - errorClients.Count);
                }
                else
                {
                    errorClients[GetClientKey(client)] = DateTime.Now;
                }
            }
        }

        public static void Save(MailMessage msg)
        {
            using (var client = new SmtpClient())
            {
                string folder = Path.Combine(config.TemporaryStoragePath, "mail-storages");
                if (Directory.Exists(folder) == false)
                {
                    Directory.CreateDirectory(folder);
                }
                client.UseDefaultCredentials = true;
                client.DeliveryMethod = SmtpDeliveryMethod.SpecifiedPickupDirectory;
                client.PickupDirectoryLocation = folder;
                client.Send(msg);
            }
        }
    }
}

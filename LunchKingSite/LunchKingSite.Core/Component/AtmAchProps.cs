﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.Core.Component
{
    public class AtmAchProps
    {
        //定義中信ACH 匯款結果代碼
        public struct AtmAchResultCode
        {
            //交易成功
            public const string Success = "00";
            //存款不足
            public const string DepositNotEnough = "01";
            //非委託用戶
            public const string UnauthorizedUser = "02";
            //已終止委託用戶
            public const string TerminatedUser = "03";
            //無此帳號
            public const string NoSuchAccount = "04";
            //收受者統編錯誤
            public const string WrongVATNumber = "05";
            //無此用戶號碼
            public const string WrongUserNumber = "06";
            //用戶號碼不符
            public const string UserNumberNotMatch = "07";
            //帳戶已結清
            public const string AccountCleared = "22";
            //靜止戶
            public const string SilentAccount = "23";
            //凍結戶
            public const string FrozenAccount = "24";
            //帳戶存款遭法院強制執行
            public const string CompulsoryEnforcement = "25";
            //未交易或匯入失敗資料
            public const string UntradeOrWrongImportData = "91";
            //其它
            public const string Others = "99";
        }

        //定義中信ACH 匯款結果描述
        public struct AtmAchResultDescription
        {
            //交易成功
            public const string Success = "交易成功";
            //存款不足
            public const string DepositNotEnough = "存款不足";
            //非委託用戶
            public const string UnauthorizedUser = "非委託用戶";
            //已終止委託用戶
            public const string TerminatedUser = "已終止委託用戶";
            //無此帳號
            public const string NoSuchAccount = "無此帳號";
            //收受者統編錯誤
            public const string WrongVATNumber = "收受者統編錯誤";
            //無此用戶號碼
            public const string WrongUserNumber = "無此用戶號碼";
            //用戶號碼不符
            public const string UserNumberNotMatch = "用戶號碼不符";
            //帳戶已結清
            public const string AccountCleared = "帳戶已結清";
            //靜止戶
            public const string SilentAccount = "靜止戶";
            //凍結戶
            public const string FrozenAccount = "凍結戶";
            //帳戶存款遭法院強制執行
            public const string CompulsoryEnforcement = "帳戶存款遭法院強制執行";
            //未交易或匯入失敗資料
            public const string UntradeOrWrongImportData = "未交易或匯入失敗資料";
            //其它
            public const string Others = "其它";
        }

        //定義中信ACH 匯款列的頭碼
        public struct TradeMode { 
            //提示
            public const string Inform = "N";
            //退件
            public const string Reject = "R";
        }

        //定義中信ACH 匯款列的次碼
        public struct TradeType { 
            //代付
            public const string Pay = "SC";
            //代收
            public const string Receive = "SD";
        }

        //首錄
        public struct FirstRow {
            public const string FirstRowCode = "BOF";
        }

        //尾錄
        public struct LastRow {
            public const string LastRowCode = "EOF";
        }
    }
}

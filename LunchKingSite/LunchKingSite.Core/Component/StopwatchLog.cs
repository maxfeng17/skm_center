﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using log4net;

namespace LunchKingSite.Core.Component
{
    public class StopwatchLog : IDisposable
    {
        private Stopwatch _watch = new Stopwatch();
        private DateTime _logTime;
        private StringBuilder _sbLog = new StringBuilder();
        private string _pattern;
        private string _name;
        public List<LogFilter> Filters = new List<LogFilter>();
        private Func<StopwatchLog, bool> _enableFilter;
        private ILogWriter LogWriter;


        public class LogLevel
        {
            public static log4net.Core.Level Debug = log4net.Core.Level.Debug;
            public static log4net.Core.Level Info = log4net.Core.Level.Info;
            public static log4net.Core.Level Warn = log4net.Core.Level.Warn;
            public static log4net.Core.Level Error = log4net.Core.Level.Error;
            public static log4net.Core.Level Fatal = log4net.Core.Level.Fatal;
        }

        public class LogFilter
        {
            public LogFilter(log4net.Core.Level level)
                : this(level, t => true)
            {

            }

            public LogFilter(log4net.Core.Level level, Func<StopwatchLog, bool> filter)
            {
                this.Level = level;
                this.Filter = filter;
            }

            public log4net.Core.Level Level { get; set; }
            public Func<StopwatchLog, bool> Filter { get; set; }
        }

        public TimeSpan Elapsed
        {
            get { return _watch.Elapsed; }
        }

        private string _logName = typeof(StopwatchLog).Name;
        private log4net.Core.Level _logLevel = LogLevel.Debug;

        public StopwatchLog()
            : this("費時 {0} 秒.", true)
        {
        }

        public StopwatchLog(string pattern)
            : this(pattern, true)
        {
        }

        public StopwatchLog(string pattern, bool autoStart)
        {
            this._pattern = pattern;
            if (autoStart)
            {
                Start();
            }
        }

        public StopwatchLog(log4net.Core.Level logLevel)
            : this("費時 {0} 秒.",  true, logLevel)
        {
        }

        public StopwatchLog(log4net.Core.Level logLevel, string logName)
            : this("費時 {0} 秒.", true, logLevel)
        {
            this._logName = logName;
        }

        public StopwatchLog(string pattern, bool autoStrat, log4net.Core.Level logLevel)
        {
            this._pattern = pattern;
            this._logLevel = logLevel;
            if (autoStrat)
            {
                Start();
            }
        }

        public StopwatchLog(string logName, string pattern, bool autoStrat, log4net.Core.Level logLevel)
        {
            this._logName = logName;
            this._pattern = pattern;
            this._logLevel = logLevel;
            if (autoStrat)
            {
                Start();
            }
        }


        public StopwatchLog(string logName, string pattern)
            : this(logName, pattern, true, log4net.Core.Level.Debug)
        {
        }

        public void Dispose()
        {
            Stop();
            if (_name != null)
            {
                StopwatchLog tmp;
                data.TryRemove(GetUniqueWatchName(_name), out tmp);
            }
        }

        public StopwatchLog For(Func<object, bool> func)
        {
            _enableFilter = func;
            return this;
        }

        public StopwatchLog SetLog(ILogWriter logWriter)
        {
            this.LogWriter = logWriter;
            return this;
        }

        public StopwatchLog SetLog(log4net.Core.Level level, Func<StopwatchLog, bool> filter)
        {
            Filters.Remove(Filters.Find(t => t.Level == this._logLevel));
            Filters.Add(new LogFilter(level, filter));
            return this;
        }

        public StopwatchLog SetLog(string logName, log4net.Core.Level logLevel, Func<StopwatchLog, bool> filter)
        {
            this._logName = logName;
            Filters.Remove(Filters.Find(t => t.Level == this._logLevel));
            Filters.Add(new LogFilter(logLevel, filter));
            return this;
        }

        public virtual void Flush()
        {
            if (_enableFilter == null || _enableFilter(this))
            {
                if (LogWriter == null)
                {
                    LogWriter = new Log4NetWriter();
                }

                if (Filters.Count > 0)
                {
                    var sortedFilters = (from t in Filters select t).OrderByDescending(t => t.Level);
                    foreach (LogFilter log in sortedFilters)
                    {
                        if (log.Filter(this))
                        {
                            LogWriter.Log(_logName, log.Level, _sbLog.ToString());
                            break;
                        }
                    }
                }
                else
                {
                    LogWriter.Log(_logName, _logLevel, _sbLog.ToString());
                }
            }
            _sbLog.Clear();
        }

        public void Stop()
        {
            Stop(null);
        }

        public void Stop(string actionName)
        {
            if (_watch.IsRunning == false)
            {
                return;
            }
            _watch.Stop();
            LogEnding(actionName);
            LogGroupData();
            Flush();
            _logTime = DateTime.MinValue;
            //
        }

        private void LogGroupData()
        {
            if (groupStopwatchs.Count == 0)
            {
                return;
            }
            _sbLog.AppendLine("\r\n群組費時: ");
            var keys = new List<string>(groupStopwatchs.Keys);
            keys.Sort();
            foreach (string key in keys)
            {
                _sbLog.AppendFormat("  [{0}] {1}秒 {2}次.\r\n",
                    key, groupStopwatchs[key].Watch.Elapsed.TotalSeconds.ToString("0.##"),
                    groupStopwatchs[key].Count
                    );
            }
        }

        public void Start()
        {
            if (_watch.IsRunning)
            {
                return;
            }
            _logTime = DateTime.Now;
            _watch.Reset();
            _watch.Start();
        }

        public void Log()
        {
            Log(null);
        }

        /// <summary>
        /// %t
        /// </summary>
        /// <param name="actionName"></param>
        /// <param name="logTime"></param>
        public void LogFormat(bool logTime, string pattern, params object[] args)
        {
            _sbLog.AppendLine();
            if (logTime)
            {
                List<object> objs = new List<object>(args);
                objs.Add(logTime);
                _sbLog.AppendFormat(pattern, objs.ToArray());
                _logTime = DateTime.Now;
            }
            else
            {
                _sbLog.AppendFormat(pattern, args);
            }
        }

        public void Log(string actionName)
        {
            _sbLog.AppendLine();
            if (actionName != null)
            {
                _sbLog.Append(actionName);
                _sbLog.Append(": ");
            }
            _sbLog.AppendFormat(_pattern,
                new TimeSpan(DateTime.Now.Ticks - _logTime.Ticks).TotalSeconds.ToString("0.##"));
            _logTime = DateTime.Now;
        }

        public void LogEnding(string actionName)
        {
            if (_sbLog.Length > 0)
            {
                _sbLog.AppendLine();
            }
            if (actionName != null)
            {
                _sbLog.Append(actionName);
                _sbLog.Append(": ");
            }
            _sbLog.AppendFormat("共 {0} 秒.",
                _watch.Elapsed.TotalSeconds.ToString("0.##"));
            _logTime = DateTime.Now;
        }

        private ConcurrentDictionary<string, GroupStopwatch> groupStopwatchs =
            new ConcurrentDictionary<string, GroupStopwatch>();
        public void GroupStart(string key)
        {
            var grpwatch = groupStopwatchs.GetOrAdd(key, delegate
            {
                return new GroupStopwatch();
            });
            grpwatch.Start();
        }

        public void GroupEnd(string key)
        {
            var grpwatch = groupStopwatchs.GetOrAdd(key, delegate
            {
                return new GroupStopwatch();
            });
            grpwatch.Stop();
        }

        private static ConcurrentDictionary<string, StopwatchLog> data =
            new ConcurrentDictionary<string, StopwatchLog>();

        public static StopwatchLog Get(string watchName)
        {
            return Get(watchName, log4net.Core.Level.Debug);
        }
        public static StopwatchLog Get(string watchName, log4net.Core.Level level)
        {
            var watch = Get(watchName, delegate
            {
                return new StopwatchLog(level);
            });
            watch.Start();
            return watch;
        }

        public static StopwatchLog Get(string watchName, Func<StopwatchLog> createAction)
        {
            return data.GetOrAdd(GetUniqueWatchName(watchName), delegate
            {
                var tmp = createAction();
                tmp._name = watchName;
                tmp._logName = watchName;
                return tmp;
            });
        }

        private static string GetUniqueWatchName(string watchName)
        {
            return string.Format("{0}::{1}", watchName, Thread.CurrentThread.ManagedThreadId);
        }

        internal class GroupStopwatch
        {
            public int Count { get; private set; }
            public Stopwatch Watch = new Stopwatch();
            public void Start()
            {
                Watch.Start();
                Count++;
            }

            public void Stop()
            {
                Watch.Stop();
            }
        }

        public interface ILogWriter
        {
            void Log(string logName, log4net.Core.Level level, string log);
        }

        public class Log4NetWriter :ILogWriter
        {
            public void Log(string logName, log4net.Core.Level level, string log)
            {
                LogManager.GetLogger(logName).Logger.Log(null, level, log, null);

            }
        }

        public class ConsoleLogWriter :ILogWriter
        {
            public void Log(string logName, log4net.Core.Level level, string log)
            {
                Console.Write("{0}:{1}: {2}\r\n",logName, level, log);
            }
        }
    }
}
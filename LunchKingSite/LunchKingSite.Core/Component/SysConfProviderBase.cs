using LunchKingSite.Core.Component;
using LunchKingSite.Core.Enumeration;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace LunchKingSite.Core
{
    public abstract class SysConfProviderBase : ISysConfProvider
    {
        protected SysConfProviderBase()
        {
            Initialize(GetSettings());
        }

        protected abstract Dictionary<string, string> GetSettings();

        public abstract bool Persist();

        #region individual parameters

        public string DefaultTitle { set; get; }

        public string DefaultMetaDescription { set; get; }

        public string DefaultPiinlifeTitle { get; set; }

        public string DefaultPiinlifeMetaDescription { get; set; }

        public string DefaultPiinlifeKeywords { get; set; }

        public string DefaultMetaOgTitle { set; get; }

        public string DefaultMetaOgImage { set; get; }

        public string DefaultMetaOgDescription { set; get; }

        private string _title;

        public string Title
        {
            get { return _title; }
            set { _title = value; }
        }

        public string ServiceTel { get; set; }

        private string _serviceFax;

        public string ServiceFax
        {
            get { return _serviceFax; }
            set { _serviceFax = value; }
        }

        private string _serviceName;

        public string ServiceName
        {
            get { return _serviceName; }
            set { _serviceName = value; }
        }
        public string EvaluateServiceName { get; set; }

        public string BusinessServiceToHouseAccountTel { get; set; }

        public string BusinessServiceToHouseTel { get; set; }

        public string BusinessServicePponAccountTel { get; set; }

        public string BusinessServicePponTel { get; set; }

        public string BusinessServiceTravelPponAccountTel { get; set; }


        public string BusinessServiceOnDutyTel { get; set; }

        private string _hidealCustomerServiceName;

        public string HidealCustomerServiceName
        {
            get { return _hidealCustomerServiceName; }
            set { _hidealCustomerServiceName = value; }
        }

        public string HiDealServiceName { get; set; }

        public string HiDealServiceCenterUrl { get; set; }

        private string _mediaBaseUrl;

        public string MediaBaseUrl
        {
            get { return _mediaBaseUrl; }
            set { _mediaBaseUrl = value; }
        }

        private string _mediaCDNUrl;

        public string CDNBaseUrl
        {
            get { return _mediaCDNUrl; }
            set { _mediaCDNUrl = value; }
        }

        private int _shopCartRsvDay;

        public int ShoppingCartReserveDays
        {
            get { return _shopCartRsvDay; }
            set { _shopCartRsvDay = value; }
        }

        private int _groupOrdRsvDay;

        public int GroupOrderReserveDays
        {
            get { return _groupOrdRsvDay; }
            set { _groupOrdRsvDay = value; }
        }

        public bool BypassSsoVerify { get; set; }

        public string TrackerExcludedIp { get; set; }

        public string TemporaryStoragePath { get; set; }

        public string WebTempPath { get; set; }

        public string WebAppDataPath { get; set; }

        public string FullTextIndexPath { get; set; }

        public string GoogleMapsAPIKey { get; set; }

        public string FacebookApplicationSecret { get; set; }

        public string FacebookGiftObjectId { get; set; }

        public string FacebookShareAppId { get; set; }

        public string FacebookApplicationId { get; set; }

        public bool EnableBetaGraphAPI { get; set; }

        public int ReturnWaitingDays { get; set; }

        public int NotifyVendorProcessReturnDays { get; set; }

        public int SysRefundScashExpriedDays { get; set; }

        public int ExhibitionDealExpiredVerifiedExpriedDays { get; set; }

        public bool IsReturnByPaymentTypeAmount { get; set; }

        public string PponOnTime { get; set; }
        public string PponEndTime { get; set; }
        public decimal GrossMargin { get; set; }
        public string ImagePoolUrl { get; set; }
        public bool EnableImagesCDN { get; set; }
        public string SiteUrl { get; set; }
        public string SSLSiteUrl { get; set; }
        public string EIPSiteUrl { get; set; }
        public string SiteServiceUrl { get; set; }
        public string CustomerSiteService { get; set; }

        public string EmailTemplateDirectory { get; set; }

        public string InvoiceMailFolder { get; set; }

        public string InvoiceKeyPointUrl { get; set; }

        public int InvoiceDelayDays { get; set; }

        public string EInvoiceAppId { get; set; }

        public string EInvoiceApiUrl { get; set; }

        public int InvoiceBufferDays { get; set; }

        public int VendorInvoiceNumberCount { get; set; }

        public bool EnabledInvoicePaperRequest { get; set; }

        public string PcashBatchFtp { get; set; }

        public string PcashXchAESKey1 { get; set; }
        public string PcashXchAESKey2 { get; set; }
        public string PcashXchClientId { get; set; }
        public string PcashXchClientSecret { get; set; }
        public string EnablePcashXchUser { get; set; }
        public string PezAPIClientId { get; set; }
        public string PezAPIClientSecret { get; set; }
        public string PezAPIUsrNum { get; set; }
        public string PezAPIVenNum { get; set; }

        public SmtpMailServerAgent SmtpMailAgent { get; set; }

        public bool EnableCpa { get; set; }

        public bool EnableIChannel { get; set; }

        public bool EnableShopBack { get; set; }

        public bool EnableAffiliates { get; set; }

        public int IChannelCookieExpires { get; set; }

        public string IChannelApiUrl { get; set; }

        public string IChannelMcode { get; set; }

        public string IChannelItemToShop { get; set; }

        public string IChannelItemToHouse { get; set; }

        public int IChannelRunDay { get; set; }

        public bool EnableCommissionRule { get; set; }

        public string PezSsoVerifyUrl { get; set; }

        public int LowQuantity { get; set; }

        public bool TwentyFourHours { get; set; }

        public string EmailTempPath { get; set; }

        public string EmailServerFTPHost { get; set; }

        public int EmailServerFTPPort { get; set; }

        public string EmailServerFTPUser { get; set; }

        public string EmailServerFTPPw { get; set; }

        public string EmailServerFTPSavePath { get; set; }

        public MailServerSendType MailServiceType { get; set; }

        public string EDMSenderName { get; set; }

        public string EDMPrePath { get; set; }

        public string EDMDefaultImage { get; set; }

        public string NewEdmPath { get; set; }

        public bool EnableNewEdmUploadFtp { get; set; }

        public bool EnableNewEdmSendRules { get; set; }

        public int RemoveMonthsCount { get; set; }

        public string TrustReportFtpPath { get; set; }

        public string PiinlifeEDMTempPath { get; set; }

        public HiDealMailConfig.PiinlifeEDMMailServerType PiinlifeEDMServiceType { get; set; }

        public PiinlifeMailSendCity PiinlifeEDMSendCity { get; set; }

        public string HiDealNotificationTemplatesPath { get; set; }

        public bool DataManagerUsed { get; set; }

        public bool MultipleMainDeal { get; set; }

        public SmsSystem SmsSystem { get; set; }

        public string SmsRatio { get; set; }

        public int SmsPhoneNumberLimit { get; set; }

        public DateTime NewInvoiceDate { get; set; }

        public string IDEASUserEmail { get; set; }

        public string PponDefaultPageUrl { get; set; }

        public string PiinLifeDefaultPageUrl { get; set; }

        public string HamiHMACSHA1Key { get; set; }

        public string BalanceRatio { get; set; }

        public string PponDefaultMobilePageUrl { get; set; }

        public Guid DefaultBuildingGuid { get; set; }

        public int HiDealOrderCountlimit { get; set; }

        public int HiDealLastNDays { get; set; }

        public int GoodsAppreciationPeriod { get; set; }

        public int ProductRefundDays { get; set; }

        public int PponNoRefundBeforeDays { get; set; }

        public string EventEmailCookieDays { get; set; }

        public string SiteMapCreatePath { get; set; }

        public string SiteMapBaseUrl { get; set; }

        public int WeeklyPayPageSize { get; set; }

        public string EdmPopUpCacheName { get; set; }

        public bool IsEnabledSpecialConsumingDiscount { get; set; }

        public string SpecialConsumingDiscountPeriod { get; set; }

        public int SpecialConsumingDiscountCid { get; set; }

        public int SpecialConsumingDiscountAmount { get; set; }

        public string SpecialConsumingDiscountUrl { get; set; }

        public int DiscountCodeMinGrossMarginSet { get; set; }

        public int ReferrerAndFirstBuyDiscountMinAmountSet { get; set; }

        public string ReferrerDiscountCodeSet { get; set; }

        public string FirstBuyDiscountCodeSet { get; set; }

        public decimal DiscountReferrerExpireMonths { get; set; }

        public decimal FirstBuyDiscountExpireMonths { get; set; }

        public bool FirstBuyDiscountCodeEnabled { get; set; }

        public bool NewUserSendDiscountCodeEnabled { get; set; }

        public bool AutoSendDiscountCodeEnabled { get; set; }

        public bool ShoppingCartEnabled { get; set; }
        public bool ShoppingCartV2Enabled { get; set; }

        public bool SimulateReadOnlyEnabled { get; set; }

        public string MasterPassSendBonusPeriod { get; set; }

        public string MasterPassSendBonusSetting { get; set; }

        public int BusinessOrderUpdateTimerInterval { get; set; }

        public bool EnableMobileEventPromoPage { set; get; }

        public bool EnableThemeCurationChannel { set; get; }

        public bool EnableNewCurationSetupPage { get; set; }

        public int DisciuntLimitedCount { get; set; }

        public bool EnableCurationTwoInApp { get; set; }

        public bool EnableCurationLimitCountInApp { get; set; }

        public int CurationCountInApp { get; set; }

        public bool DefaultPageOpenNewWindow { get; set; }

        public bool GoogleMapInDefaultPage { get; set; }

        public string ParticipateBtnBid1 { get; set; }

        public string ParticipateBtnBid2 { get; set; }

        public string ParticipateBtnBid3 { get; set; }

        public string MohistId { get; set; }

        public string MohistId2 { get; set; }

        public string MohistVerifyFtpPath { get; set; }

        public string MohistVerifyFtpPath2 { get; set; }

        public string MohistVerifyFtpAccountId { get; set; }

        public string MohistVerifyFtpAccountId2 { get; set; }

        public string MohistVerifyFtpPassWord { get; set; }

        public string MohistVerifyFtpPassWord2 { get; set; }

        public bool EnabledMohistUpload { get; set; }

        public string SybaseUrl { get; set; }

        public string SybaseSecondUrl { get; set; }

        public string S2tServerPriority { get; set; }

        public bool PponServiceButtonVisible { get; set; }

        public bool PiinlifeServiceButtonVisible { get; set; }

        public bool NewPezSso { get; set; }

        public bool PezSyncLogout { get; set; }

        public string PayeasyIdCheckWebServiceUrl { get; set; }

        public bool EnableGrossMarginRestrictions { get; set; }

        public bool EnableBuyMemberDiscountList { get; set; }

        public bool NewCpaEnable { get; set; }

        public bool IsImageLoadBalance { get; set; }

        public bool MediaCDNEnabled { get; set; }

        public bool ImagesCDNEnabled { get; set; }

        public string MASCDNAPIUrl { get; set; }

        public string MASCDNSlug { get; set; }

        public string MASCDNAPIKey { get; set; }

        public bool AWSAutoSync { get; set; }

        public bool AWSEnabled { get; set; }

        public string AWSBucketName { get; set; }

        public string AWSAccessKey { get; set; }

        public string AWSSecretKey { get; set; }

        public int AppDefaultPageInfo { get; set; }

        public bool IsDefaultPageSettingByCache { get; set; }

        public bool AppDealNameIncludeLocation { get; set; }

        #region AppNoticfication 推播相關

        public bool AwsPush { get; set; }

        public string AwsSnsAccessKey { get; set; }

        public string AwsSnsSecretKey { get; set; }

        public int NotificationDescriptionLimit { get; set; }

        public bool AppNotificationJob { get; set; }

        public bool AppNotificationByDevice { get; set; }

        public string AppPushServiceUri { get; set; }

        public bool AppIOSNotification { get; set; }

        public string IOSPushP12FilePath { get; set; }

        public string IOSPushP12FileName { get; set; }

        public string IOSPushP12FilePassword { get; set; }

        public int IOSPushChannelNumber { get; set; }

        public bool IOSOfficialApsHost { get; set; }

        public int AndroidPushChannelNumber { get; set; }

        public bool AppAndroidNotification { get; set; }

        public string AndroidPushSenderID { get; set; }

        public string AndroidPushAPIAccessAPIKey { get; set; }

        public string AndroidAppPackageName { get; set; }

        public int AndroidRegistrationIdRange { get; set; }

        public int SubscriptionNoticeLimitHours { get; set; }

        public string SubscriptionNoticeSingleMsgFormat { get; set; }

        public string SubscriptionNoticeMultiMsgFormat { get; set; }

        public int MemberCollectionDealAfterCloseMonths { get; set; }

        public int RefreshTokenExtendExpiredDays { get; set; }

        public int OAuthTokenExpiredSeconds { get; set; }

        public string PokeBallBannerImage { get; set; }

        public string PokeBallBannerUrl { get; set; }

        public DateTime PokeBallBannerStart { get; set; }

        public DateTime PokeBallBannerEnd { get; set; }

        public int PushMessageDefaultExpirationDays { get; set; }

        public bool EnableControlroomPushBrand { get; set; }

        #endregion AppNoticfication

        #endregion individual parameters

        #region private methods

        private void Initialize(Dictionary<string, string> settingWeak)
        {
            foreach (PropertyInfo pi in typeof(ISysConfProvider).GetProperties())
            {
                try
                {
                    object[] cpAttrs = pi.GetCustomAttributes(typeof(ConfigParameterAttribute), true);
                    if (cpAttrs.Length > 0)
                    {
                        ConfigParameterAttribute cpAttr = cpAttrs[0] as ConfigParameterAttribute;
                        object val = null;
                        bool assignDefault = true;
                        //settingWeak 的值是來自小web.config
                        //如果小web.config沒設定，就從 attribute 裡取預設值
                        if (settingWeak.ContainsKey(cpAttr.Key))
                        {
                            if (ConvertWeak2Strong(pi, settingWeak[cpAttr.Key], out val))
                            {
                                assignDefault = false;
                            }
                        }

                        if (assignDefault)
                        {
                            val = cpAttr.DefaultValue;
                            ConvertWeak2Strong(pi, val.ToString(), out val);
                        }

                        pi.SetValue(this, val, null);
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(string.Format("小 web.config 錯誤，請確認 {0} 設定, {1}", pi.Name, ex.Message));
                }
            }
        }

        private bool ConvertWeak2Strong(PropertyInfo pi, string src, out object dest)
        {
            return Helper.ConvertStringToObjectValue(pi, src, out dest);
        }

        #endregion private methods

        public bool EnableLowestComboDealPriceForGoogleShopping { get; set; }

        public bool EnableGSASubstituteDiscountPriceForPrice { get; set; }

        public string CowellFtpHost { get; set; }

        public string CowellFtpUserName { get; set; }

        public string CowellFtpPassword { get; set; }

        public string CowellFtpRemoteUploadFolder { get; set; }

        public string CowellFtpRemoteCompletedFolder { get; set; }

        public string CowellDataBackupPath { get; set; }

        public int VerificationBuffer { get; set; }

        public int UnverificationBuffer { get; set; }

        public int ShippingBuffer { get; set; }

        public int SevenShippingBuffer { get; set; }

        public bool NewCouponPdfWriter { get; set; }

        public int ThousandSelling { get; set; }

        public int NoneCreditRefundPeriod { get; set; }

        public bool EnabledValidateBuyerAddress { get; set; }

        public string GAConsolePath { get; set; }

        public bool IsEnabledNewDeliveryTemplate { get; set; }

        public int DeliveryTemplateRandomDealsCount { get; set; }

        public bool EnablePageAccessControl { get; set; }

        public string PayEasyCasServerUrl { get; set; }

        public bool FamiOrderExgGate { get; set; }

        public string Famiport17LifeCode { get; set; }

        public string FamiportAesKey { get; set; }

        public string FamiportAuthFormat { get; set; }

        public string FamiportApiUrl { get; set; }

        public string FamiportSingleBarcodeApiUrl { get; set; }

        public int FamiportPinOrderExgRetryTimes { set; get; }

        public int FamiportPinOrderExgRetryTimeSpan { set; get; }

        public int FamiportPinVerifyRetryTimeSpan { get; set; }

        public int FamiportPinVerifyRetryPerDay { get; set; }

        public int FamiportPinOrderExgRetryPerDay { set; get; }

        public int FamiportPinVerifyRetryTimes { get; set; }

        public int FamilyNetUnlockPincodeSec { get; set; }

        public string FamilyMariOpenDatetime { get; set; }

        public Guid FamilyNetSellerGuid { get; set; }

        public int FamilyCategordId { get; set; }

        public int FamilyExpiredReturnDay { get; set; }

        public bool FamilyNetExpiredPinAutoReturn { get; set; }

        public DateTime FamilyNetReturnApiStopTimeS { get; set; }

        public DateTime FamilyNetReturnApiStopTimeE { get; set; }

        public string FamilyCityButtonString { get; set; }

        public bool EnableFamilyBarcodeVersion { get; set; }

        public bool IsVisa2013 { get; set; }

        public DateTime NewPiinlifeDate { get; set; }

        public string FamiDailyPath { get; set; }

        public int ReceiptReceivedThresholdDay { get; set; }

        public decimal BusinessTax { get; set; }

        public int PromoMailDealsCount { get; set; }

        public decimal PromoMailDealsProportion { get; set; }

        public string AndroidAppDownloadUrl { get; set; }

        public string AndroidUserAgent { get; set; }

        public string iOSUserAgent { get; set; }

        public string iOSAppId { get; set; }

        public string AndroidAppId { get; set; }

        public int CloseAppBlockHiddenDays { get; set; }

        public int ViewAppBlockHiddenDays { get; set; }

        public int AppDownloadSMSCheckDays { get; set; }

        public int AppDownloadSMSCheckSendTimes { get; set; }

        public bool AppHtmlIncludeIFrame { get; set; }

        public bool AppNewVersion { get; set; }

        public int RecommendDealsLogicType { get; set; }

        public int MemoryCache2Mode { get; set; }

        public bool AppDescEnableIframe { get; set; }

        public bool EanbleABTestRecommendDealsByViewHistory { get; set; }

        public string ShipBatchImport { get; set; }

        public string DEAfilterApiKey { get; set; }

        public bool IsSmsSend { get; set; }

        public bool IsUsePponTinyUrl { get; set; }

        public int ShipTimeDayDiffLimit { get; set; }

        public bool IsPiinlifeBuy { get; set; }

        public bool IsSendWarnEmail { get; set; }

        public bool SetApiLog { get; set; }

        public bool IsSetBuyTransaction { get; set; }

        public bool EnableChangeEmail { get; set; }

        public bool ReferralToApp { get; set; }

        public bool ShowFamiDiscount { get; set; }

        public string VourcherLotteryEvent { get; set; }

        public string[] SyncWebServerUrl { get; set; }

        public int[] FamiteaDealId { get; set; }

        public string FamiteaCouponDesc { get; set; }

        public string VendorBillingSystemManual { get; set; }

        public string OAuthLoginUrl { get; set; }

        public string GoogleApiKey { get; set; }

        public string GoogleApiSecret { get; set; }

        public bool EnableGoogleMapApiV2 { get; set; }

        public bool EnableOpenStreetMap { get; set; }

        public bool EnableBingMapAPI { get; set; }

        public string BingMapApiKey { get; set; }

        public string IDEASServiceToken { get; set; }

        public string IDEASServiceUrl { get; set; }

        public int NegativeCommentsRate { get; set; }

        public string DefaultTestCreditcard { get; set; }
        public string DefaultTestCreditcardExpireDate { get; set; }
        public string DefaultTestCreditcardSecurityCode { get; set; }

        public int OrderFilterVerion { get; set; }

        public bool MDealEnabled { get; set; }
        public string MDealTester { get; set; }
        public bool PersonalPushMessageEnabled { get; set; }
        public bool FacebookSuspendLogin { get; set; }
        public bool LineSuspendLogin { get; set; }

        #region MessageQueue相關
        public string ActiveMQServerUri { get; set; }

        public string ActiveMQSSQueueName { set; get; }

        public bool ActiveMQEnable { get; set; }

        public bool OopsEnabled { get; set; }
        public string OopsHost { get; set; }

        public string OopsTopic { get; set; }

        #endregion

        #region Email Configuration

        #region 客服系統信

        public bool EnableTransferMailNotify { get; set; }
        public string CsTransferEmailAddress { get; set; }
        public string CsTransferEmailDisplayName { get; set; }
        public string CsMasterEmailAddress { get; set; }

        public bool EnableNewOrderShipMailNotify { get; set; }
        public string NewOrderShipEmailAddress { get; set; }
        public string NewOrderShipEmailDisplayName { get; set; }

        public bool EnableNewReturnOrExchangeMailNotify { get; set; }
        public string NewReturnOrExchangepEmailAddress { get; set; }
        public string NewReturnOrExchangeEmailDisplayName { get; set; }

        public int TransferNoReplyWithinHours { get; set; }

        #endregion

        public int SendVerificationNoticeEmailBeforeDays { get; set; }

        public bool EnableUserTrackingModule { get; set; }

        public bool EnableUserTrackingMQSend { get; set; }

        public bool EnableUserTrackingLogOutput { get; set; }

        public int ActiveMQSendTimeoutMillSeconds { get; set; }

        public string MQ2AWSQueueName { set; get; }

        public bool RsrcDetailError { get; set; }

        public string ServerIPs { get; set; }

        public int DefenseModuleEnabledLevel { get; set; }

        public bool DefenseModuleShareBlackList { get; set; }

        public bool CheckDiscountStringNewOldVersionsMatch { get; set; }

        public bool SwitchableHttpsEnabled { get; set; }

        public string CssAndJsVersion { get; set; }

        public string SystemEmail { get; set; }

        public string AdminEmail { get; set; }

        public string ItTesterEmail { get; set; }

        public string ItPAD { get; set; }

        public string TrustReportEmail { get; set; }

        public string EinvoiceMediaEmail { get; set; }

        public string ServiceEmail { get; set; }

        public string CsReturnEmail { get; set; }

        public string CsdEmail { get; set; }

        public string Cs02Email { get; set; }

        public string CodShipEmail { get; set; }

        public string CsManagerEmail { get; set; }

        public string CsMailDealerEmail { get; set; }

        public string PlanningEmail { get; set; }

        public string FinanceEmail { get; set; }

        public string FinanceAccountsEmail { get; set; }

        public string BizEmail { get; set; }

        public string PponEmail { get; set; }

        public string PeautyEmail { get; set; }

        public string PiinlifeServiceEmail { get; set; }

        public string PponServiceEmail { get; set; }

        public string ExpirationChangeEmail { get; set; }

        public string EdmEmail { get; set; }

        public string PiinlifeEdmEmail { get; set; }

        public string BookingSystemChangeNoticeEmail { get; set; }

        public string SolicitBusinessEmail { get; set; }

        public string SolicitMarketEmail { get; set; }

        public string SolicitKindEmail { get; set; }

        public string SalesManagerEmail { get; set; }

        public string SalesAssistantEmail { get; set; }

        public string SalesSpecialAssistantEmail { get; set; }

        public string SalesSpecialAssistantShippingEmail { get; set; }

        public string CodEmail { get; set; }

        public string CodLocalEmail { get; set; }
        public string LocalEmail { get; set; }
        public string TravelEmail { get; set; }

        public string PaperAllowanceSerivceEmail { get; set; }

        public string PaperAllowanceSerivceCCEmail { get; set; }

        public string FamiDealBuildEmail { get; set; }

        public string DebugGuyEmail { get; set; }

        public string SupportingLeaderEmail { get; set; }

        public string MhdEmail { get; set; }

        public string OadMpShipEmail { get; set; }

        public string OadEmail { get; set; }

        public string OpdEmail { get; set; }

        public string ProposalPhotographerAppointEmail { get; set; }

        public string ProposalPhotographerAppointCCEmail { get; set; }

        public string PponCreditcardFraudEmail { get; set; }

        public bool MailLogToDb { get; set; }

        #endregion Email Configuration

        #region Payment Configuration

        public string CreditCardAPIProviderRatio { get; set; }

        public bool BypassPaymentProcess { get; set; }

        public bool EnableAntiCreditcardFraud { get; set; }

        public bool DiscountCodeUsed { get; set; }

        public bool TurnATMOn { get; set; }

        public string PCashAPI { get; set; }

        public string CreditCardAPIProvider { get; set; }

        public string UnionPayBinPatterns { get; set; }

        public string NotOkOrderQueryDays { get; set; }

        public string CreditcardFailLineToken { get; set; }

        public bool AutoBlockCreditcardFraudSuspect { get; set; }

        public bool EnableOTP { get; set; }

        public string EnableOTPUser { get; set; }
        public string ForceOTPDeal { get; set; }

        public int ChargingDays { get; set; }

        public int PayWayLimitExpiresMinute { set; get; }

        public string PayWayLimitPaymentType { get; set; }

        public bool PayWayLimitEnable { set; get; }

        public bool UsingPreventingOversellProcedure { get; set; }

        #region NCCC

        public string MerchantID { get; set; }

        public string TerminalID { get; set; }

        public string CardPath { get; set; }

        public string NcccRequestUrl { get; set; }

        public string NcccFileName { get; set; }

        #endregion NCCC

        #region HiTrust

        public string HiTrustMerchantID { get; set; }

        public string HiTrustPiinLifeMerchantID { get; set; }

        public string HiTrustPayeasyTravelMerchantID { get; set; }

        public string HiTrustUnionPayMerchantID { get; set; }

        public string HiTrustPiinLifeUnionPayMerchantID { get; set; }

        public string HiTrustContactWithCVV2MerchantID { get; set; }

        public string HiTrustContactWithoutCVV2MerchantID { get; set; }

        public string HiTrustTestMerchantID { get; set; }

        public string HiTrustConfigPath { get; set; }

        public string HiTrustBatchFtpUri { get; set; }

        #endregion HiTrust

        #region Neweb

        public string NewebMerchantID { get; set; }

        public string NewebUserID { get; set; }

        public string NewebUserPassword { get; set; }

        public string NewebPiinLifeMerchantID { get; set; }

        public string NewebAcceptServer { get; set; }

        public string NewebAdminServer { get; set; }

        public string NewebTestMerchantID { get; set; }

        public string NewebTestUserID { get; set; }

        public string NewebTestUserPassword { get; set; }

        public string NewebTestAcceptServer { get; set; }

        public string NewebTestAdminServer { get; set; }

        public string NewebContactMerchantID { get; set; }

        public string NewebContactPiinLifeMerchantID { get; set; }

        public string NewebContactInstallmentMerchantID { get; set; }

        public string NewebContactUserID { get; set; }

        public string NewebContactUserPassword { get; set; }

        #endregion Neweb

        #region ATM

        public string AtmTestPostIp { get; set; }

        public string AtmAccountBankName { get; set; }

        public string AtmAccountBankCode { get; set; }

        public int AtmRemitDeadline { get; set; }

        public string ChinaTrustAtmId { get; set; }

        #endregion ATM

        public bool PponDescriptionTestEnabled { get; set; }

        public bool EnableNewAppPponDescriptionVersion { get; set; }

        public int CreditcardReferAmount { get; set; }

        public bool EnableCreditcardRefer { get; set; }

        public string TaiShinPaymentBankAndBranchNo { get; set; }

        public bool EnabledTaiShinPaymnetGateway { get; set; }

        public string TaiShinPaymnetGatewayApi { get; set; }

        public string TaiShinMallMerchantID { get; set; }

        public string TaiShinMallToken { get; set; }

        public string TaiShinMerchantID { get; set; }

        public string TaiShinTerminalID { get; set; }

        public string TaiShin3DTerminalID { get; set; }

        public bool EnabledApplePay { get; set; }

        public bool IsApplePayEMVManualProcess { get; set; }

        public int ApplePaySessionVersion { get; set; }

        public string ApplePayMerchantCerThumbprint { get; set; }

        public string ApplePayPaymentProcessingThumbprint { get; set; }

        public string ApplePayPrivateKey { get; set; }
        #endregion Payment Configuration

        #region Tmall

        public string TmallDefaultUserName { get; set; }

        #endregion Tmall

        #region WeChat

        public string WeChatToken { get; set; }

        public string WeChatAppId { get; set; }

        public string WeChatAppSecret { get; set; }

        public int WeChatArticleCount { get; set; }

        #endregion WeChat

        public bool UseNewRefund { get; set; }

        public bool EnableVendorReturnManagement { get; set; }

        public int CacheServerMode { get; set; }

        public bool CacheItemCompress { get; set; }

        public string RedisServer { get; set; }

        public bool ViewPponDealManagerLogEnabled { get; set; }

        public int FamiDealDeliverTimeAlertDay { get; set; }

        public string FamiPosMMKId { set; get; }

        public string FamiportMMKId { set; get; }

        public bool FamiSendDiscountCodeAtVerified { set; get; }

        public bool FamiLogSendDiscountEvent { get; set; }

        public int FamiVerifiedDiscountCampaignId { set; get; }

        public DateTime FamiVerifiedDiscountStartDate { set; get; }

        public DateTime FamiVerifiedDiscountEndDate { set; get; }

        public string EventPromoBindCategoryList { get; set; }

        public int FamiAccumulatedVerifiedCount { get; set; }
        public int FamiAccumulatedVerifiedDiscountCampaignId { get; set; }
        public DateTime FamiAccumulatedVerifiedDiscountStartDate { get; set; }
        public DateTime FamiAccumulatedVerifiedDiscountEndDate { get; set; }

        #region Event

        public int GreetingCardPponDiscountCampaignId { set; get; }

        public int GreetingCardHiDealDiscountCampaignId { set; get; }

        #endregion Event

        public bool IsPrivilegeLogged { get; set; }

        public bool EnableMemberLinkBind { get; set; }

        public bool JobRepeatCheckEnabled { get; set; }

        public int JobRepeatTimeoutSec { get; set; }

        public bool DataManagerResetImmediately { get; set; }

        public string CkFinderBaseDir { get; set; }

        public DateTime ContactToPayeasy { get; set; }

        public DateTime PayeasyToContact { get; set; }

        public DateTime EinvoiceMIGv312 { get; set; }

        public DateTime EinvAllowanceStartDate { get; set; }

        public string PayeasyCompanyId { get; set; }

        public string PayeasyTravelCompanyId { get; set; }

        public string ContactCompanyId { get; set; }

        #region App Links

        public string AppLinksiOSId { get; set; }

        public string AppLinksiOSName { get; set; }

        public string AppLinksAndroidPackage { get; set; }

        public string AppLinksAndroidAppName { get; set; }

        #endregion App Links

        public bool EnableFami3Barcode { get; set; }

        public int RelatedDealCount { set; get; }

        public string TodayDealsAllowIp { set; get; }

        public int RandomParagraphCacheMinuteSet { get; set; }

        public int ResponseWriteType { get; set; }

        public bool IsGroupCouponOn { get; set; }

        public bool EnableGroupCouponNewMakeOrderApi { set; get; }

        public DateTime NewGroupCouponOn { get; set; }

        public bool EnabledGroupCouponTypeB { get; set; }
        public bool EnableEvaluateCache { get; set; }

        public bool EnableAppEvaluateSystem { get; set; }

        public bool EnableSHA256Encode64To64Char { get; set; }

        public DateTime ZiWeiPromoStartDate { get; set; }

        public DateTime ZiWeiPromoEndDate { get; set; }

        public string ZiWeiPromoUrl { get; set; }

        public bool IsReturnsBtnEnabled { get; set; }

        #region PponSetup
        public string OperationPath { get; set; }
        public string ReconciliationPath { get; set; }
        public string WriteOffPath { get; set; }
        #endregion

        #region MasterPass

        public string ConsumerKey { get; set; }

        public string CheckoutIdentifier { get; set; }

        public string CertPath { get; set; }

        public string CertPassword { get; set; }

        public string ApiPrefix { get; set; }

        public string CallbackPath { get; set; }

        public bool EnableMasterPass { get; set; }

        public string LightboxJsUrl { get; set; }

        public int EncryptCount { get; set; }

        public bool IsMasterPassLocalTest { get; set; }

        public bool EnableMasterpassPreCheckout { get; set; }

        public bool EnableMasterpassExpressCheckout { get; set; }

        public bool EnableMasterpassStandardCheckout { get; set; }


        #endregion MasterPass

        #region LionTravel

        public string LionTravelNotifyAPI { get; set; }

        public string LionTravelNofityAPIToken { get; set; }

        public string LionTravelChannelCheckBillBeginDate { get; set; }

        public string LionTravelServiceEmail { set; get; }

        #endregion LionTravel


        public string UnUsed17PayMsg { get; set; }
        public string Used17PayMsg { get; set; }
        public bool IsPCPApiTestMode { get; set; }

        public bool IsMobileWindowSizeSession { get; set; }

        public bool EnableHotDealSetting { get; set; }

        public bool IsSortDealTimeSlotSequence { get; set; }

        public bool IsLogOperationOfDealTimeSlot { get; set; }

        public int DealTimeSlotDays { get; set; }

        public int TakeViewPponDealOperatingInfoCount { get; set; }

        public bool NewShareLinkEnabled { get; set; }

        public int SystemUserId { get; set; }

        public int SessionStateTimeOut { set; get; }

        public string Web17LifeAccessToken { set; get; }

        public int MobileMaxWidth { set; get; }

        public bool IsNewMobileSetting { set; get; }

        public int GenToHouseBalanceSheetBuffer { get; set; }

        public bool RemovePeztempOrderGuid { get; set; }

        public bool CouponSmsEnabled { get; set; }

        public int ProductReturnProcessExpiredDay { get; set; }

        public bool EnableProductAutoReturnProcess { get; set; }

        #region ACH

        public string TriggerBankAndBranchNo { get; set; }

        public string TriggerAccountNo { get; set; }

        public string TriggerCompanyId { get; set; }

        public string SendUnitNo { get; set; }

        public string ReceiveUnitNo { get; set; }

        /* ACH匯費機制已開發一部分 但決議暫觀察一陣子 再決定是否施行
        public int AchRemittanceFee { get; set; }

        public int AchRemittanceTimeLimit { get; set; }
         */

        #endregion ACH

        #region PCP

        public int ExchangeSuperBounsAmount { get; set; }

        public decimal ExchangeSuperBounsRatio { get; set; }

        public bool PCPApiDemo { get; set; }

        public string PcpCompressImageTypeList { set; get; }

        public int PcpGenTestIdentityCodeUserId { get; set; }

        public string PcpIdentityCodeGroupIdList { get; set; }

        #endregion PCP

        public bool IsSubLocationListInspection { get; set; }

        public bool GuestBuyEnabled { set; get; }

        public bool EnableInspectDealCategorises { get; set; }

        public bool ShowEmptyDealCategory { get; set; }
        public int NumberOfDealInOnePage { set; get; }
        public bool CheckCreditCardEnabled { get; set; }
        public DiscountUserCollectType DiscountUserCollectType { get; set; }
        public DateTime BeaconEventPushValidStartTime { get; set; }
        public DateTime BeaconEventPushValidEndTime { get; set; }
        public int BeaconEventPushIntervalMinute { get; set; }
        public int BeaconPushDailyUpperLimit { get; set; }
        public bool BeaconEventAdEnabled { get; set; }
        public bool FamiChannelBannerEnabled { get; set; }
        public int PponSearchLogLimitCount { get; set; }
        public bool EnableComboDealNewOrderTotalLimitSum { get; set; }

        public int BusinessOrderTimeSMinute { get; set; }
        public int BusinessOrderTimeEMinute { get; set; }

        public int VisitorIdentityCookieExpires { get; set; }

        #region Line Pay

        public bool EnableLinePay { get; set; }
        public bool LinePayCaptureWithConfirm { get; set; }
        public string LinePayChannelId { get; set; }
        public string LinePayChannelSecretKey { get; set; }
        public string LinePayApiReserve { set; get; }
        public string LinePayApiConfirm { set; get; }
        public string LinePayApiCapture { set; get; }
        public string LinePayApiVoid { set; get; }
        public string LinePayApiRefund { set; get; }
        public string LinePayUsingLogo { set; get; }
        public string LinePayLangCd { set; get; }
        public int LinePayApiRetryTimes { set; get; }
        public int LinePayRefundPeriodLimit { get; set; }

        #endregion

        #region 台新銀行

        public bool EnableTaishinCreditCardOnly { get; set; }
        public string TSBankAppRsrc { get; set; }
        public bool EnableTaishinThirdPartyPay { get; set; }
        public string TaishinThirdPartyPayApiUrl { get; set; }
        public string TaishinThirdPartyPayUserUrl { get; set; }
        public string TaishinThirdPartyPayRegisterUrl { get; set; }
        public string TaishinThirdPartyPayIndexUrl { get; set; }
        public string TaishinThirdPartyPayClientId { get; set; }
        public string TaishinThirdPartyPayClientPassword { get; set; }
        public string TaishinThirdPartyPayVirtualAcctStart { get; set; }
        public string TaishinNavName { get; set; }
        public string TaishinNavUrl { get; set; }
        public string TaishinNavImgUrl { get; set; }
        public int TaishinCreditCardBankId { get; set; }

        #endregion

        public bool InstallmentPayEnabled { get; set; }
        public DateTime EnableInstallment12TimeS { get; set; }
        public DateTime EnableInstallment12TimeE { get; set; }
        public decimal EnableInstallment12GrossMargin { get; set; }
        public int PicSetupUploadImageSleepTime { get; set; }

        public string SellerContractFtpUri { get; set; }

        public bool SellerContractIsFtp { get; set; }

        public string SellerContractVersionHouse { get; set; }

        public string SellerContractVersionPpon { get; set; }



        public string SaleGoogleCalendarAPIKey { get; set; }
        public bool IsProposalOnMemory { get; set; }
        public string VbsShortageMail { get; set; }

        public bool IsConsignment { get; set; }

        public string SearchApiUrl { get; set; }
        public int SearchEngine { get; set; }

        public string SmtpOtherHosts { get; set; }

        public bool StarRatingEnabled { get; set; }

        public PponDealType GoogleProductFeedSet { get; set; }

        public PponDealType FBProductFeedSet { get; set; }

        #region SKM

        public bool IsEnableDealCategory { get; set; }
        public bool IsEnableCashAndGiftCouponOnStore { get; set; }
        public bool IsEnableCostByHq { get; set; }
        public bool IsEnableCRMUserGroup { get; set; }
        public string SkmPayTradeNoErrorAlertEmail { get; set; }
        public int SkmEcExpiredDataMonth { get; set; }
        public string SkmWalletSiteUrl { get; set; }
        public string SkmWalletAuthToken { get; set; }
        public bool SkmWalletUseStaticClient { get; set; }
        public string SkmTurnCloudSiteUrl { get; set; }
        public bool EnableSkmPay { get; set; }
        public int SkmDealCurrentVerion { get; set; }
        public string SkmRsvData { get; set; }
        public string SkmRsvUrl { get; set; }
        public string SkmImages { get; set; }
        public int MaxCrmDataCount { get; set; }
        public int MaxCrmExhibitionCount { get; set; }
        public bool CheckPrizeOverdraw { get; set; }
        public string SkmAppId { get; set; }
        public string SkmWebserviceUrl { set; get; }
        public string SkmWsNamespace { set; get; }
        public string SkmWsClassName { set; get; }
        public string SkmWsMethod { set; get; }
        public bool IsValidateSkmMember { set; get; }
        public bool MakeSkmMemberValidateFail { set; get; }
        public int SkmCategordId { set; get; }
        public int SkmDefaultDealCategordId { set; get; }
        public int SkmFayaqueCategoryId { get; set; }
        public string SkmBeaconDefaultMsg { get; set; }
        public int SkmBeaconNotificationDailyLimit { get; set; }
        public int SkmBeaconMsgBoxDailyLimit { get; set; }
        public int SkmBeaconNotificationTimeSpanMinutes { get; set; }
        public int SkmBeaconMsgBoxTimeSpanMinutes { get; set; }
        public bool SkmBeaconChecked { get; set; }

        public int SkmCityId { set; get; }
        public int SkmAgreementOfCmsContentId { set; get; }
        public string SkmPublicImag { get; set; }
        public int SkmQrCodeExpireTime { get; set; }
        public int SkmQrCodeAppExpireTime { get; set; }
        public string SkmDealSales { get; set; }
        public bool IsEnableCouponSettlement { get; set; }
        public string SkmDealBuildEmail { get; set; }
        public DateTime SkmOldDeal { get; set; }
        public string SkmBackendOauth { get; set; }
        public int SkmDeliveryCategoryId { get; set; }
        public int SkmDealPageSize { get; set; }
        public Guid SkmRootSellerGuid { get; set; }
        public Guid SkmDefaultSellerGuid { get; set; }
        public bool SkmBuyGetFreeIsShow { get; set; }
        public bool SkmNoInvoicesIsShow { get; set; }
        public string SkmHQCategories { get; set; }
        public string SkmShoppeDisplayNameCheckDate { get; set; }
        public bool NotShowSkmDealsInWeb { get; set; }
        public bool UsingLowLoadingBeaconMethod { get; set; }
        public int SkmDealDisableCategory { get; set; }
        public string SkmAppHomeDisableCategory { get; set; }
        public string SkmRootPowerSellerGuid { get; set; }
        public bool EnableNewSKMAppDealStyle { get; set; }
        public bool EnableSKMNewBeacon { set; get; }
        public bool EnableSKMBeaconLimit { get; set; }
        public bool EnableSkmBurning { get; set; }
        public int SKMBeaconMessageDueDays { get; set; }
        public int SkmLatestActivityTakeCnt { get; set; }
        public int SkmExpiredDataMonth { get; set; }
        public bool EnableSKMActivityStyleNewImgSize { get; set; }
        public bool EnableCallBurningWebService { get; set; }
        public bool EnableSkmPrizeDraw { get; set; }
        public bool EnableQueryProductCode { get; set; }
        public bool SkmEnablePrizeDealCallBurningWs { get; set; }
        public bool SkmEnablePrizeEventTestMode { get; set; }
        public bool SkmEnableSendPrizeData { get; set; }
        public bool SkmGiftGetFromUmall { get; set; }

        public string ApposPauseRefundStartTime { get; set; }
        public string ApposPauseRefundEndTime { get; set; }
        public string SkmPayPreDayClearPosExecDate { get; set; }
        public DateTime SKMDeepLinkStartDate { get; set; }
        public DateTime SKMDeepLinkEndDate { get; set; }
        public string SKMDeepLinkUrl { get; set; }
        public string SKMDeepLinkImage { get; set; }
        public int SkmGetPaymentResultAccseeLimite { get; set; }
        public int SkmPayOTPTimeoutSeconds { get; set; }
        public int SKMPrizeItemSafetyStock { get; set; }
        public int SkmRefreshDealSalesInfoMode { get; set; }
        public bool EnableSKMListLikeQuery { get; set; }
        public int ResendPrizeCouponEventId { get; set; }
        public bool IsCheckSkmDealTimeSlotStatusWhenMakeOrder { get; set; }
        public bool EnableAdList { get; set; }
        public string SkmCashTrustLogExpectSendTime { get; set; }
        public int SkmCashTrustLogStartDays { get; set; }
        public int SkmCashTrustLogEndDays { get; set; }
        public bool EnableSkmDealRepeatPurchase { get; set; }
        public bool DisplaySkmPayDealInfoOriPrice { get; set; }
        public bool EnableSkmCreditCardCategory { get; set; }
        public string SkmBeautyStageUrl { get; set; }
        public bool EnableCopyDeepLink { get; set; }
        public string SkmPushSiteUrl { get; set; }
        public string SkmPushRoute { get; set; }
        public string SkmPushXSignature { get; set; }
        public bool EnableCustomizedBoardList { get; set; }
        public bool ShowCRMZeroOption { get; set; }
        public bool ShowCutomizedBoardPrizeDrawOption { get; set; }
        public bool EnableCheckDealImageLength { get; set; }
        public string EnableCheckDealImageLengthDate { get; set; }
        public bool EnableInstallment { get; set; }
        public bool EnableCheckDealInstallProperty { get; set; }

        public string NavigationShops { get; set; }
        public int SkmCacheDataMinute { get; set; }
        public bool IsEnableForceCoverAd { get; set; }
        public DateTime SkmPrizeEventCustomEndDate { get; set; }
        #endregion

        #region HiLife

        public bool EnableHiLifeDealSetup { get; set; }
        public bool EnableHiLifeApiTransfer { get; set; }

        public string HiLifeFixedFunctionPincode { get; set; }

        public string HiLifeTransferServerDomain { get; set; }

        #endregion

        #region Mobile

        public bool IsMobileShowCanBeUsedImmediatelyChannel { set; get; }
        public string IsMobileShowCanBeUsedImmediatelyChannelAndroidVersion { set; get; }
        public string IsMobileShowCanBeUsedImmediatelyChanneliOSVersion { set; get; }
        public string MobileChannel { set; get; }
        public string MobileChannelSKMDefaultDealTypes { set; get; }
        public bool IsShowSKMFilter { set; get; }
        public bool EnableDepositCoffee { get; set; }
        public bool EnableFamiCoffee { get; set; }
        public bool EnableFamiCoffeeIcon { get; set; }
        public bool EnableKindDealInApp { get; set; }
        public string iOS17LifeAppOAuthClientId { get; set; }
        public string Android17LifeAppOauthClientId { get; set; }
        public string Vbs17LifeOauthClientId { get; set; }
        public string LoginUrl { get; set; }
        public int PushOrderCpaWithInHour { get; set; }

        #endregion

        #region Lucene Search
        public int dealCountLimit { get; set; }

        public bool EnabledSearchExcludeSKMChannel { get; set; }

        public bool MGMEnabled { get; set; }

        public int MGMGiftCount { get; set; }

        public int MGMGiftLockDay { get; set; }

        public string MGMGiftGetUrl { get; set; }

        #endregion

        #region Debug用(錯誤修正後會移除)
        public bool EnableElmahLogUriIsEmpty { get; set; }
        #endregion

        #region 網站自動化測試
        public string ExecuteAutomatedTestingAPI { get; set; }
        public string AccountForAutoTest { get; set; }
        public string GeneralForgotPasswordCaptcha { get; set; }
        #endregion

        #region 會員 Member

        public bool EnableCaptcha { get; set; }

        #endregion

        #region 新光業務中台
        public string SKMCenterAppKey { get; set; }

        public string SKMCenterAppSecret { get; set; }

        public string SKMCenterApiUrl { get; set; }

        public string SKMCenterRocketMQTopic { get; set; }

        public string SKMCenterRocketMQAccessKey { get; set; }

        public string SKMCenterRocketMQSecretKey { get; set; }

        public string SKMCenterRocketMQGroupId { get; set; }

        public string SKMCenterRocketMQNameSrv { get; set; }
        #endregion

        public bool EnabledRefundOrExchangeEditReceiverInfo { get; set; }

        public bool EnabledExchangeProcess { get; set; }

        public string MobileIndexEventIconName { get; set; }

        public DateTime StopRefundQuantityCalculateDate { get; set; }

        public int FamiportCityId { get; set; }

        public string FamilyNetDealSales { get; set; }

        public bool IsEveryDayNewDeals { get; set; }

        public int EveryDayNewDealsCategoryId { get; set; }

        public bool FilterLowGrossMarginDealForShoppingGuideApi { get; set; }

        public bool EnableTaisihinPayCashPoint { get; set; }

        public int PreventingOversellTimeoutSec { get; set; }

        public bool PaymentChargeAndRefundUseCartVersion { get; set; }

        public bool PaymentChargeAndRefundDebugMode { get; set; }

        public bool LogPaymentDTO { get; set; }

        public bool EnableWebUseAPIBuy { get; set; }
        public string CathayMerchantID { get; set; }
        public string CathayCubKey { get; set; }
        public string CathayInstallmentMerchantID { get; set; }
        public string CathayInstallmentCubKey { get; set; }
        public bool EnabledCathayPaymnetGateway { get; set; }
        public string CathayPaymnetGatewayApi { get; set; }
        public bool EnabledCathayPaymnetGatewayTest { get; set; }
        public string CathayPaymnetTestBid { get; set; }

        public int ShoppingGuidePageSize { get; set; }

        #region 商家系統
        public bool IsEnableVbsBindAccount { get; set; }

        public int VbsTemporaryPasswordHourLimit { get; set; }
        public bool VbsShipRule2k18 { get; set; }

        public string ForcedVerificationAccount { get; set; }

        public bool IsEnableVbsNewShipFile { get; set; }

        public bool IsEnableVbsNewUi { get; set; }

        public string VbsGsaNotifyStartDate { get; set; }

        public string VbsGsaNotifyEndDate { get; set; }
        public bool DeliveryClosePop { get; set; }
        #endregion

        public bool IsProvisionSwitchToSql { get; set; }
        public bool EnableParallelTestGetDeductibleMemberPromotionDepositBetaVersion { get; set; }
        public bool IsVbsProposalNewVersion { get; set; }

        public bool IsRemittanceFortnightly { get; set; }
        public string CreditcardFraudSeller { get; set; }

        public string ProposalImageAllowDomain { get; set; }
        public bool CheckPhoneCarrierValid { get; set; }

        #region CustomerSerivce
        public string InSideUrl { get; set; }
        public string OutSideUrl { get; set; }
        public bool OrderDetailV2LinkVisable { get; set; }
        #endregion

        #region 超取設定
        public string ISPOAuthToken { get; set; }
        public string ISPApiUrl { get; set; }
        public string ISPFamilyFreightsChangeDate { get; set; }
        public string ISPFamilyBeforeFreights { get; set; }
        public int ISPFamilyNowFreights { get; set; }
        public string ISPSevenFreightsChangeDate { get; set; }
        public string ISPSevenBeforeFreights { get; set; }
        public int ISPSevenNowFreights { get; set; }

        public string FamilyIspPromoTips { get; set; }
        public string SevenIspPromoTips { get; set; }

        public int FamilyIspShipCompanyId { get; set; }
        public string FamilyIspWebSide { get; set; }

        public int SevenIspShipCompanyId { get; set; }
        public string SevenIspWebSide { get; set; }

        public string AllowBidsForOnlineTest { get; set; }
        public bool EnableFamiMap { get; set; }
        public bool EnableFamiMapTestMode { get; set; }
        public bool EnableSevenMap { get; set; }
        public bool EnableSevenIsp { get; set; }

        public bool EnableCancelPaidByIspOrder { get; set; }
        public bool AllowsIspReturnForShipping { get; set; }
        public string SevenEmapUrl { get; set; }
        public bool RedirectSevenMap { get; set; }
        public int FamilyParentId { get; set; }
        public int SevenParentId { get; set; }
        public bool ApplyPopout { get; set; }


        #endregion

        #region 連結設定

        public string RedirectUrlToEvent1111 { get; set; }
        public bool EnableDiscountPrice { get; set; }
        public int DealPagePriceMode { get; set; }

        #endregion

        #region Marketing 行銷設定

        public string GoogleGtagSendToData { get; set; }
        public string GoogleGtagConversionSendToData { get; set; }
        public string UrADGoogleGtagConversionSendToData { get; set; }
        public string YahooProjectId { get; set; }
        public string YahooPixelId { get; set; }
        public bool EnabledOneAD { get; set; }

        #endregion Marketing 行銷設定

        #region ShopBack

        public string ShopbackApiUrl { get; set; }
        public string ShopbackOrderAPIOfferId { get; set; }
        public string ShopbackValidationAPIOfferId { get; set; }
        public int ShopbackCommissionElectronic { get; set; }
        public int ShopbackCommission3C { get; set; }
        public string ShopbackAffId { get; set; }
        public int ShopBackCookieExpires { get; set; }
        public int ShopBackEffectiveShipDays { get; set; }
        public int ShopBackEffectiveOrderDays { get; set; }

        #endregion

        #region 騰雲發票相關(TurnCloud)
        /// <summary> 騰雲發票服務Url </summary>
        public string TurnCloudServerSiteUrl { get; set; }
        /// <summary> 是否真實呼叫騰雲發票服務 </summary>
        public bool IsExecuteTurnCloudServer { get; set; }
        /// <summary> 是否使用測試的會員編號與館號呼叫騰雲發票服務 </summary>
        public bool IsUseTestVipNoAndShopCode { get; set; }
        /// <summary> 騰雲發票服務HashKey精選優惠 </summary>
        public string TurnCloudServerHashKey { get; set; }
        /// <summary> 騰雲發票服務HashKey停車支付 </summary>
        public string TurnCloudServerParkingHashKey { get; set; }
        /// <summary> 騰雲發票啟用6X櫃號 </summary>
        public bool EnableTurnCloudInvoice6X { get; set; }
        /// <summary> 聯信銀行代碼 </summary>
        public string NationalCreditCardCenterBankNo { get; set; }

        #endregion

        #region 雙11 Game

        public string GameFacebookApplicationSecret { get; set; }
        public string GameFacebookApplicationId { get; set; }
        public string GameFbAuthRedirectUri { get; set; }
        public string GameLineAuthRedirectUri { get; set; }

        public string GameLineApplicationSecret { get; set; }
        public string GameLineId { get; set; }

        #endregion

        #region Jobs

        public bool EnableAutoFixLostDealTimeSlotWorker { get; set; }

        #endregion Jobs

        public bool EnableRushBuy { get; set; }
        public bool DealTimeSlotEnableSortLockHide { get; set; }

        #region PChome 代銷
        public bool EnablePChomeChannel { get; set; }
        public string PChomeChannelVendorId { get; set; }
        public string PChomeChannelVendorNo { get; set; }
        public string PChomeChannelCouponQRCODE { get; set; }
        public string PChomeChannelCouponBarCode { get; set; }

        public string PChomeChannelAESKey { get; set; }

        public string PChomeChannelAESIV { get; set; }
        public string PChomeChannelTestBid { get; set; }
        public string PChomeChannelShortTimeUpdateStockBid { get; set; }
        public string PChomeChannelNoUpdateStockBid { get; set; }

        #endregion

        #region Pchome Wms

        public bool EnableWms { get; set; }
        public string WmsVendorId { get; set; }
        public string PchomeWmsAESKey { get; set; }

        public string PchomeWmsAESIV { get; set; }

        public string WmsContractVersion { get; set; }

        public bool EnableP24HPersonalNotification { get; set; }
        public int SyncInventoryBatchSize { get; set; }

        public string MerchantReturnAccount { get; set; }

        public bool MerchantReturnEnable { get; set; }
        public bool EnablePchomeDeductingInventory { get; set; }
        public string PchomeDeductingInventoryBids { get; set; }
        public bool WmsRefundEnable { get; set; }
        public string WmsRefundAccount { get; set; }
        public string WmsRefundVbsAccount { get; set; }
        public string WmsOrderAPI { get; set; }
        public string WmsProgressStatusAPI { get; set; }
        public string WmsRefundAPI { get; set; }
        public string WmsRefundLogisticAPI { get; set; }

        #endregion

        #region Payeasy 導購

        public string PezChannelOrderApiUrl { get; set; }
        public string PezChannelVenId { get; set; }
        public bool EnablePezChannel { get; set; }
        public int PezChannelCookieExpires { get; set; }
        public int PezChannelRunDay { get; set; }

        #endregion

        #region Line導購

        public string LineRsrc { get; set; }
        public decimal LineMinGrossMargin { get; set; }
        public string LineShopOrderFirstApiUrl { get; set; }
        public string LineShopOrderFinishApiUrl { get; set; }
        public int LineShopSite { get; set; }
        public int LineShopId { get; set; }
        public string LineShopAuthKey { get; set; }
        public bool EnableLineShop { get; set; }
        public int LineShopCookieExpires { get; set; }
        public int LineShopEffectiveOrderDays { get; set; }
        public int LineShopEffectiveShipDays { get; set; }
        public bool LineShopForTest { get; set; }
        #endregion

        public string LineAuthRedirectUri { get; set; }
        public string LineApplicationSecret { get; set; }
        public string LineApplicationId { get; set; }

        #region 富利核銷

        public string PizzaHutFtpUri { get; set; }
        public string KfcFtpUri { get; set; }
        public string PizzaHutFtpPath { get; set; }
        public string KfcFtpPath { get; set; }
        public string JfMailTo { get; set; }
        public string JfMailSubject { get; set; }

        #endregion

        #region Affiliates 導購

        public int AffiliatesCookieExpires { get; set; }
        public string AffiliatesApiUrl { get; set; }

        #endregion

        #region 台新中台

        public bool EnbaledTaishinEcPayApi { get; set; }

        #endregion

        #region entrepot 生活優惠中台

        public string LifeEntrepotSiteUrl { get; set; }

        #endregion

        #region Alibaba skm千人千面
        public int skmFirstShowLimit { get; set; }
        public string SkmFtpHost { get; set; }
        public string SkmFtpUserId { get; set; }
        public string SkmFPassword { get; set; }
        public string LocalFilePath { get; set; }
        public string RemoteWorkPath { get; set; }
        public string RemoteFinishPath { get; set; }
        public string RemoteFailPath { get; set; }
        #endregion

        #region Alibaba商品頁面控制
        public bool CreateItemBySelf { get; set; }

        public bool EnableCreateCategory { get; set; }
        #endregion

        #region Alibaba api
        public string AlibabaAPIWhiteIps { get; set; }
        #endregion
    }
}
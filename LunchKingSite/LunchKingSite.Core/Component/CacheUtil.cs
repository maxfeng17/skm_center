using System.IO;
using System.IO.Compression;
using System.Text;
using Newtonsoft.Json;

namespace LunchKingSite.Core.Component
{
    public class CacheUtil
    {
        public static byte[] CompressObject(object obj)
        {
            string jsonData = JsonConvert.SerializeObject(obj, Formatting.None);
            byte[] srcBuffer;
            byte[] descBuffer;
            srcBuffer = Encoding.UTF8.GetBytes(jsonData);
            using (MemoryStream ms = new MemoryStream())
            {
                using (GZipStream gz = new GZipStream(ms, CompressionMode.Compress))
                {
                    gz.Write(srcBuffer, 0, srcBuffer.Length);
                    gz.Close();
                    descBuffer = ms.ToArray();
                }
            }

            return descBuffer;
        }

        public static T DecompressObject<T>(byte[] buff)
        {
            if (buff == null || buff.Length == 0)
            {
                return default(T);
            }
            byte[] plainBuff = Decompress(buff);
            string jsonStr = Encoding.UTF8.GetString(plainBuff);

            return JsonConvert.DeserializeObject<T>(jsonStr);
        }

        private static byte[] Decompress(byte[] gzip)
        {
            // Create a GZIP stream with decompression mode.
            // ... Then create a buffer and write into while reading from the GZIP stream.
            using (GZipStream stream = new GZipStream(new MemoryStream(gzip), CompressionMode.Decompress))
            {
                const int size = 4096;
                byte[] buffer = new byte[size];
                using (MemoryStream memory = new MemoryStream())
                {
                    int count;
                    do
                    {
                        count = stream.Read(buffer, 0, size);
                        if (count > 0)
                        {
                            memory.Write(buffer, 0, count);
                        }
                    } while (count > 0);
                    return memory.ToArray();
                }
            }
        }
    }
}
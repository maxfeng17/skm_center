﻿using System;
using System.Collections.Generic;

namespace LunchKingSite.Core.Component
{
    public class EmailUtil
    {
        public static string ConvertToTrueEmail(string email)
        {
            if (RegExRules.CheckEmail(email) == false)
            {
                return string.Empty;
            }
            string domain = Helper.GetEmailDomain(email);
            if (IsGmailDomain(domain) || IsMicrosoftDomain(domain))
            {
                email = RemoveEmailNameAfterPlusSign(email);
            }
            if (IsGmailDomain(domain))
            {
                email = RemoveDotFromEmailName(email);
            }
            return email;
        }

        public static bool IsGmailDomain(string domain)
        {
            return string.Equals(domain, "gmail.com", StringComparison.OrdinalIgnoreCase);
        }

        private static HashSet<string> microsoftDomains = new HashSet<string>(
            new string[]
            {
                "hotmail.com", "hotmail.com.tw", "outlook.com", "livemail.tw"
            });

        public static bool IsMicrosoftDomain(string domain)
        {
            if (string.IsNullOrEmpty(domain))
            {
                return false;
            }
            return microsoftDomains.Contains(domain.ToLower());
        }

        public static string RemoveDotFromEmailName(string emailAddress)
        {
            int pos = emailAddress.IndexOf('@');
            if (pos == -1)
            {
                return emailAddress;
            }
            string[] emailParts = emailAddress.Split('@');
            if (emailParts.Length != 2)
            {
                return emailAddress;
            }
            return string.Format("{0}@{1}", emailParts[0].Replace(".", ""), emailParts[1]);
        }

        public static string RemoveEmailNameAfterPlusSign(string emailAddress)
        {
            string[] emailParts = emailAddress.Split('@');
            if (emailParts.Length != 2)
            {
                return emailAddress;
            }
            int pos = emailParts[0].IndexOf('+');
            if (pos == -1)
            {
                return emailAddress;
            }
            return string.Format("{0}@{1}", emailParts[0].Substring(0, pos), emailParts[1]);
        }
    }

}

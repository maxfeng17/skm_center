﻿using log4net;
using System;
using System.Collections.Generic;
using System.Data;
using System.Dynamic;
using System.Web;
using System.Web.UI;
using NPOI.SS.UserModel;
using NPOI.HSSF.UserModel;
using NPOI.HSSF.Util;
using System.Linq.Expressions;


namespace LunchKingSite.Core.Component
{
    public static class Extensions
    {
        public static DataTable ToDataTable<T>(this IEnumerable<T> collection, string tableName)
        {
            DataTable tbl = ToDataTable(collection);
            tbl.TableName = tableName;
            return tbl;
        }

        /// <summary>
        /// 將 IEnumerable 轉為 DataTable
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="collection">The collection.</param>
        /// <returns></returns>
        public static DataTable ToDataTable<T>(this IEnumerable<T> collection)
        {
            var dtReturn = new DataTable();

            // column names
            var oProps = typeof(T).GetProperties();
            foreach (var pi in oProps)
            {
                var colType = pi.PropertyType;
                if ((colType.IsGenericType) && (colType.GetGenericTypeDefinition() == typeof(Nullable<>)))
                {
                    colType = colType.GetGenericArguments()[0];
                }
                dtReturn.Columns.Add(new DataColumn(pi.Name, colType));
            }

            // Could add a check to verify that there is an element 0
            foreach (var rec in collection)
            {
                var dr = dtReturn.NewRow();
                foreach (var pi in oProps)
                {
                    dr[pi.Name] = pi.GetValue(rec, null) ?? DBNull.Value;
                }
                dtReturn.Rows.Add(dr);
            }
            return (dtReturn);
        }

        public static ExpandoObject ToExpando(this object obj)
        {
            IDictionary<string, object> result = new ExpandoObject();

            Type t = obj.GetType();
            var props = t.GetProperties();
            foreach (var prop in props)
            {
                result.Add(prop.Name, prop.GetValue(obj, null));
            }

            return result as ExpandoObject;
        }

        /// <summary>
        /// 讀取列舉Attribute
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="enumVal"></param>
        /// <returns></returns>
        public static T GetAttributeOfType<T>(this Enum enumVal) where T : System.Attribute
        {
            var type = enumVal.GetType();
            var memInfo = type.GetMember(enumVal.ToString());
            var attributes = memInfo[0].GetCustomAttributes(typeof(T), false);
            return (attributes.Length > 0) ? (T)attributes[0] : null;
        }


    }

    #region log4net

    public enum LogWith
    {
        None = 0,
        Url = 1,
        LoginId = 2,
        LoginIdAndUrl = 3
        //, Variable
    }

    public static class Log4netExtension
    {
        public static void Warn(this ILog logger, object message, LogWith logWith)
        {
            if (HttpContext.Current == null)
            {
                logger.Warn(message);
                return;
            }
            logger.Warn(GetLogWithMessage(message.ToString(), logWith));
        }

        public static void Warn(this ILog logger, object message, Exception ex, LogWith logWith)
        {
            if (HttpContext.Current == null)
            {
                logger.Warn(message, ex);
                return;
            }
            logger.Warn(GetLogWithMessage(message.ToString(), logWith), ex);
        }

        private static string GetLogWithMessage(string message, LogWith logWith)
        {
            Page page = HttpContext.Current.Handler as Page;
            bool hasPage = page != null;
            if (hasPage)
            {
                if ((logWith & LogWith.LoginId) == LogWith.LoginId)
                {
                    message += ",登入者: " + page.User.Identity.Name;
                }
                if ((logWith & LogWith.Url) == LogWith.Url)
                {
                    message += ",頁面: " + page.Request.Url;
                }
            }
            return message;
        }
    }

    #endregion log4net

    #region NPOI

    public static class NpoiExtension
    {
        #region SetValue

        public static Cell SetValue(this Cell cell, string value)
        {
            cell.SetCellValue(value);
            return cell;
        }

        public static Cell SetValue(this Cell cell, DateTime value)
        {
            cell.SetCellValue(value);
            return cell;
        }

        public static Cell SetValue(this Cell cell, DateTime? value)
        {
            if (value.HasValue)
            {
                cell.SetCellValue(value.Value);
            }
            else
            {
                cell.SetType(CellType.BLANK);
            }
            return cell;
        }

        public static Cell SetValue(this Cell cell, bool value)
        {            
            cell.SetCellValue(value);
            return cell;
        }

        public static Cell SetValue(this Cell cell, RichTextString value)
        {
            cell.SetCellValue(value);
            return cell;
        }

        public static Cell SetValue(this Cell cell, double value)
        {
            cell.SetCellValue(value);
            return cell;
        }

        public static Cell SetValue(this Cell cell, int value)
        {
            cell.SetCellValue(value);
            return cell;
        }

        public static Cell SetValue(this Cell cell, int? value)
        {
            if (value.HasValue)
            {
                cell.SetCellValue(value.Value);
            }
            else
            {
                cell.SetType(CellType.BLANK);
            }
            return cell;
        }

        #endregion

        public static T GetCellValue<T>(this Row row, Enum em)
        {
            Cell c = row.GetCell(Convert.ToInt32(em));

            if (c == null)      //在 excel 中剪下 (ctrl+x) 時, 該格就會是null
            {
                return default(T);
            }

            switch (c.CellType)
            {
                case CellType.BLANK:
                    return default(T);
                case CellType.BOOLEAN:
                    return (T)Convert.ChangeType(c.BooleanCellValue, typeof(T));
                case CellType.NUMERIC:
                    if (typeof(T) == typeof(DateTime))
                    {
                        return (T)Convert.ChangeType(c.DateCellValue, typeof(T));
                    }

                    if(typeof(T) == typeof(DateTime?))
                    {
                        DateTime? result = c.DateCellValue;
                        return (T)(object)result;
                    }

                    Type t = typeof(T);
                    if (t.IsGenericType && t.GetGenericTypeDefinition() == typeof(Nullable<>))
                    {
                        return (T)(object)Convert.ChangeType(c.NumericCellValue, t.GetGenericArguments()[0]);
                    }

                    return (T)Convert.ChangeType(c.NumericCellValue, typeof(T));
                case CellType.STRING:
                    return (T)Convert.ChangeType(c.StringCellValue, typeof(T));
                case CellType.FORMULA:
                    return (T)Convert.ChangeType(GetFormulaText(c), typeof(T));
                default:
                    break;
            }

            throw new Exception(string.Format("Cannot get cell value at row: {0}, column: {1}", c.RowIndex, c.ColumnIndex));
        }
        
        private static string GetFormulaText(Cell cell)
        {
            string value = null;
            switch (cell.CachedFormulaResultType)
            {                
                case CellType.NUMERIC:
                    value = cell.NumericCellValue.ToString();
                    break;
                case CellType.STRING:
                    value = cell.StringCellValue;
                    break;
                default:
                    value = cell.ToString();
                    break;
            }
            return value;
        }
        
        public static Cell SetType(this Cell cell, CellType type)
        {
            cell.SetCellType(type);
            return cell;
        }

        public static Cell SetStyle(this Cell cell, CellStyle style)
        {            
            cell.CellStyle = style;
            return cell;
        }

        public static void SetColWidth(this Sheet sheet, int colIndex, int approximateChars)
        {
            sheet.SetColumnWidth(colIndex, 256 * approximateChars);
        }

        public static void SetColWidth(this Sheet sheet, Enum e, int approximateChars)
        {
            sheet.SetColumnWidth(Convert.ToInt32(e), 256 * approximateChars);
        }

        public static void BackgroundColor(this CellStyle style, short hssfColorIndex)
        {
            style.FillForegroundColor = hssfColorIndex;
            style.FillPattern = FillPatternType.SOLID_FOREGROUND;
        }

        public static Cell CreateCell(this Row row, Enum e)
        {
            return row.CreateCell(Convert.ToInt32(e));
        }

        public static Cell CreateCell(this Row row, Enum e, CellType type)
        {
            return row.CreateCell(Convert.ToInt32(e), type);
        }
        
    }

    #endregion

    public static class ExpressionExtension
    {
        public static Expression<Func<T, K>> And<T, K>(this Expression<Func<T, K>> left_exp, Expression<Func<T, K>> right_exp)
        {
            ParameterExpression parameter = Expression.Parameter(typeof(T));
            return Expression.Lambda<Func<T, K>>(Expression.AndAlso(left_exp.Body, right_exp.Body), parameter);
        }
    }

    public static class ShopBackExtension
    {
        /// <summary>
        /// 推算該日期減去工作天數後的日期
        /// </summary>
        /// <param name="date">日期</param>
        /// <param name="workDays">工作天數</param>
        /// <returns></returns>
        public static DateTime ReduceWorkDays(this DateTime date, int workDays)
        {
            if (workDays < 0)
            {
                throw new ArgumentException("日期不能為負", "workDays");
            }

            if (workDays == 0) return date;

            if (date.DayOfWeek == DayOfWeek.Saturday)
            {
                date = date.AddDays(-1);
                workDays -= 1;
            }
            else if (date.DayOfWeek == DayOfWeek.Sunday)
            {
                date = date.AddDays(-2);
                workDays -= 1;
            }

            date = date.AddDays(-(workDays / 5 * 7));
            int extraDays = workDays % 5;

            if ((int)date.DayOfWeek - extraDays < 0)
            {
                extraDays += 2;
            }

            return date.AddDays(-extraDays);
        }
    }

}
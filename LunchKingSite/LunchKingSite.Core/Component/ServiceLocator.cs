﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using Autofac;
using Autofac.Integration.Web;

namespace LunchKingSite.Core.Component
{
    public class ServiceLocator
    {
        public static IContainer Container { get; private set; }

        #region .ctor
        static ServiceLocator()
        {
            Container = ((IContainerProviderAccessor) HttpContext.Current.ApplicationInstance).ContainerProvider.ApplicationContainer;
        }

        private ServiceLocator() {}
        #endregion
    }
}

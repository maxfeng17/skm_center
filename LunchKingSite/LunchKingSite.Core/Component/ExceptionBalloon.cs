using System;
using System.Collections.Concurrent;
using System.Runtime.CompilerServices;
using System.Text;
using log4net;

namespace LunchKingSite.Core.Component
{
    /// <summary>
    /// 一段時間累積到一定的錯誤量，才會發出錯誤訊息
    /// </summary>
    public class ExceptionBalloon
    {
        private static ConcurrentDictionary<string, ExceptionBalloon> handlers = new ConcurrentDictionary<string, ExceptionBalloon>();
        public static ExceptionBalloon GetOrCreate(string groupName, int seconds, int occurs)
        {
            return handlers.GetOrAdd(groupName, delegate(string gn)
            {
                return new ExceptionBalloon()
                {
                    GroupName = gn,
                    TriggerSeconds = seconds,
                    TriggerOccurs = occurs
                };
            });
        }

        public string GroupName { get; set; }
        public DateTime StartTime { get; set; }
        public int TriggerSeconds { get; set; }
        public int TriggerOccurs { get; set; }
        public ConcurrentQueue<Tuple<string, DateTime>> ExceptionItems = new ConcurrentQueue<Tuple<string, DateTime>>();

        public void BlowException(Exception ex, Type targetType, string msg)
        {
            lock (GroupName)
            {
                ConcurrentQueue<Tuple<string, DateTime>> newOne = new ConcurrentQueue<Tuple<string, DateTime>>();
                Tuple<string, DateTime> item;
                while (ExceptionItems.TryDequeue(out item))
                {
                    //移掉過期的
                    if ((DateTime.Now - item.Item2).TotalSeconds > TriggerSeconds)
                    {
                        continue;
                    }
                    newOne.Enqueue(item);
                }
                //加新的
                string message = string.Format("{0} - {1} - {2} - {3}",
                    DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"),
                    targetType, msg, ex);
                newOne.Enqueue(new Tuple<string, DateTime>(message, DateTime.Now));
                //結算
                if (newOne.Count >= TriggerOccurs)
                {
                    StringBuilder sbLog = new StringBuilder();
                    sbLog.AppendFormat("此錯誤於 {0} 秒鍾內發生 {1} 次.\r\n\r\n", TriggerSeconds, newOne.Count);
                    foreach (Tuple<string, DateTime> temp in newOne)
                    {
                        sbLog.AppendLine(temp.Item1);
                    }
                    string totalMessage = sbLog.ToString().Substring(0, 2000);
                    LogManager.GetLogger(targetType).Warn(totalMessage);
                    ExceptionItems = new ConcurrentQueue<Tuple<string, DateTime>>();
                }
                else
                {
                    ExceptionItems = newOne;
                }
            }
        }
    }
}
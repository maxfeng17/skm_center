﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace LunchKingSite.Core.Component
{
    public class BarCode128 : IDisposable
    {
        public enum BarCode128Types
        {
            A = 0,
            B,
            C
        }

        private readonly static List<string> CODES;
        private const int STOP = 106;

        #region Static initialization
        static BarCode128()
        {
            CODES = new List<string>(107);
            CODES.Add("212222");
            CODES.Add("222122");
            CODES.Add("222221");
            CODES.Add("121223");
            CODES.Add("121322");
            CODES.Add("131222");
            CODES.Add("122213");
            CODES.Add("122312");
            CODES.Add("132212");
            CODES.Add("221213");
            CODES.Add("221312");
            CODES.Add("231212");
            CODES.Add("112232");
            CODES.Add("122132");
            CODES.Add("122231");
            CODES.Add("113222");
            CODES.Add("123122");
            CODES.Add("123221");
            CODES.Add("223211");
            CODES.Add("221132");
            CODES.Add("221231");
            CODES.Add("213212");
            CODES.Add("223112");
            CODES.Add("312131");
            CODES.Add("311222");
            CODES.Add("321122");
            CODES.Add("321221");
            CODES.Add("312212");
            CODES.Add("322112");
            CODES.Add("322211");
            CODES.Add("212123");
            CODES.Add("212321");
            CODES.Add("232121");
            CODES.Add("111323");
            CODES.Add("131123");
            CODES.Add("131321");
            CODES.Add("112313");
            CODES.Add("132113");
            CODES.Add("132311");
            CODES.Add("211313");
            CODES.Add("231113");
            CODES.Add("231311");
            CODES.Add("112133");
            CODES.Add("112331");
            CODES.Add("132131");
            CODES.Add("113123");
            CODES.Add("113321");
            CODES.Add("133121");
            CODES.Add("313121");
            CODES.Add("211331");
            CODES.Add("231131");
            CODES.Add("213113");
            CODES.Add("213311");
            CODES.Add("213131");
            CODES.Add("311123");
            CODES.Add("311321");
            CODES.Add("331121");
            CODES.Add("312113");
            CODES.Add("312311");
            CODES.Add("332111");
            CODES.Add("314111");
            CODES.Add("221411");
            CODES.Add("431111");
            CODES.Add("111224");
            CODES.Add("111422");
            CODES.Add("121124");
            CODES.Add("121421");
            CODES.Add("141122");
            CODES.Add("141221");
            CODES.Add("112214");
            CODES.Add("112412");
            CODES.Add("122114");
            CODES.Add("122411");
            CODES.Add("142112");
            CODES.Add("142211");
            CODES.Add("241211");
            CODES.Add("221114");
            CODES.Add("413111");
            CODES.Add("241112");
            CODES.Add("134111");
            CODES.Add("111242");
            CODES.Add("121142");
            CODES.Add("121241");
            CODES.Add("114212");
            CODES.Add("124112");
            CODES.Add("124211");
            CODES.Add("411212");
            CODES.Add("421112");
            CODES.Add("421211");
            CODES.Add("212141");
            CODES.Add("214121");
            CODES.Add("412121");
            CODES.Add("111143");
            CODES.Add("111341");
            CODES.Add("131141");
            CODES.Add("114113");
            CODES.Add("114311");
            CODES.Add("411113");
            CODES.Add("411311");
            CODES.Add("113141");
            CODES.Add("114131");
            CODES.Add("311141");
            CODES.Add("411131");
            CODES.Add("211412");
            CODES.Add("211214");
            CODES.Add("211232");
            CODES.Add("23311120");
        }
        #endregion

        private Brush _brush = Brushes.Black;

        private string _code;
        private BarCodeSettings _settings;
        private BarCode128Types _type;

        public BarCode128(string code)
        {
            Init(code, DetectType(code), new BarCodeSettings(true));
        }

        public BarCode128(string code, BarCode128Types type)
            : this(code, type, new BarCodeSettings(true))
        {
        }

        public BarCode128(string code, BarCode128Types type, BarCodeSettings settings)
        {
            Init(code, type, settings);
        }

        public Bitmap Paint()
        {
            _settings.NarrowWidth = 3;
            _settings.Font = new Font("Arial", 12f, FontStyle.Regular);
            string code = _code;
            List<bool> barMarker = GetBarMarker(code);
            string textToDraw =string.Empty;
            SizeF sizeCodeText = SizeF.Empty;           

            int w = _settings.LeftMargin + _settings.RightMargin + barMarker.Count * _settings.NarrowWidth;
            int h = _settings.TopMargin + _settings.BottomMargin + _settings.BarCodeHeight;

            if (_settings.DrawText)
            {
                textToDraw = code.Substring(0, 1);
                for (int i = 1; i < code.Length; i++)
                    textToDraw += "  " + code.Substring(i, 1);
                sizeCodeText = Graphics.FromImage(new Bitmap(1, 1)).MeasureString(textToDraw, _settings.Font);

                h += _settings.BarCodeToTextGapHeight + (int) sizeCodeText.Height;
            }

            Bitmap bmp = new Bitmap(w, h, PixelFormat.Format32bppArgb);
            Graphics g = Graphics.FromImage(bmp);

            int left = _settings.LeftMargin;

            foreach (bool mark in barMarker)
                left += PaintBar(mark, _settings, g, left);

            if (_settings.DrawText)
            {
                g.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAliasGridFit;
                int tX = _settings.LeftMargin + (w - _settings.LeftMargin - _settings.RightMargin - (int)sizeCodeText.Width) / 2;
                if (tX < 0)
                    tX = 0;
                int tY = _settings.TopMargin + _settings.BarCodeHeight + _settings.BarCodeToTextGapHeight;
                g.FillRectangle(Brushes.White, tX, tY, sizeCodeText.Width, sizeCodeText.Height);
                g.DrawString(textToDraw, _settings.Font, _brush, tX, tY);
            }

            g.Dispose();
            return bmp;
        }

        private int PaintBar(bool mark, BarCodeSettings settings, Graphics g, int left)
        {
            if (mark)
            {
                Rectangle r = new Rectangle(left, settings.TopMargin, settings.NarrowWidth, settings.BarCodeHeight);
                g.FillRectangle(_brush, r);
            }

            return settings.NarrowWidth;
        }

        private List<bool> GetBarMarker(string code)
        {
            int length = 11 + 11 * (_type == BarCode128Types.C ? code.Length / 2 : code.Length) + 11 + 13; // start + 11 * code_length + check + stop
            List<bool> marker = new List<bool>(length);
            BarCode128Types currentType = _type;
            int check = InsertChar(marker, (char)(103 + (int)_type)); // 103 is start code A
            for (int i = 0; i < code.Length; i++)
            {
                char c = ConvertChar(_type, _type != BarCode128Types.C ? code[i] : (char)int.Parse(code.Substring(i++, 2)));
                check += InsertChar(marker, c);
            }

            InsertChar(marker, (char) (check%103));
            InsertChar(marker, (char)STOP);
            return marker;
        }

        private char ConvertChar(BarCode128Types type, char c)
        {
            int charCode = (int)c;
            switch (type)
            {
                case BarCode128Types.A:
                    if (charCode >= 0 && charCode < 32)  charCode += 64;
                    if (charCode >= 32 && charCode < 96) charCode -= 32;
                    break;

                case BarCode128Types.B:
                    if (charCode >= 32 && charCode < 128) charCode -= 32;
                    break;

                default:
                    break;
            }

            return (char) charCode;
        }

        protected int InsertChar(List<bool> marker, char c)
        {
            int check = marker.Count < 11 ? c : marker.Count / 11 * c;
            string weight = CODES[c];
            bool black = true;
            foreach (char x in weight)
            {
                int times = x - '0';
                for (int i = 0; i < times; i++)
                    marker.Add(black);
                black = !black;
            }

            return check;
        }

        private void Init(string code, BarCode128Types type, BarCodeSettings settings)
        {
            // TODO: if it's type C we should be able to switch to another type, instead of prefix it to make it even digits
            if (type == BarCode128Types.C && code.Length % 2 == 1)
                code = "0" + code;
            _code = code;
            _settings = settings;
            _type = type;
        }


        private BarCode128Types DetectType(string input)
        {
            if (new Regex(@"^[0-9]+$").IsMatch(input))
                return BarCode128Types.C;

            if (new Regex(@"[a-z]").IsMatch(input))
                return BarCode128Types.B;

            return BarCode128Types.A;
        }

        #region IDisposable Members

        public void Dispose()
        {
            if (_settings != null)
                _settings.Dispose();
        }

        #endregion
    }
}

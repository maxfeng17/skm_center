using System;
using System.Collections.Generic;
using System.Configuration;

namespace LunchKingSite.Core
{
    public class AppConfSysConfProvider : SysConfProviderBase
    {        
        protected override Dictionary<string, string> GetSettings()
        {
            var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            KeyValueConfigurationCollection settings = config.AppSettings.Settings;

            Dictionary<string, string> dic = new Dictionary<string, string>(settings.Count, StringComparer.OrdinalIgnoreCase);
            foreach (string key in settings.AllKeys)
                dic.Add(key, settings[key].Value);

            return dic;

        }

        public override bool Persist()
        {
            return false;
        }
    }
}
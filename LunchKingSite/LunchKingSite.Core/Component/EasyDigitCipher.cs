﻿using System;

namespace LunchKingSite.Core.Component
{
    public class EasyDigitCipher
    {
        private const string SHUFFLED_CHARACTER_MAP = @"gi4pmjbdaqz38w1kru0l7fs2n5h6xe";
        private readonly int DIGIT_SPACE = SHUFFLED_CHARACTER_MAP.Length / 10;

        public string Encrypt(int seed, string digitString)
        {
            long number;
            if (!long.TryParse(digitString, out number))
            {
                throw new ArgumentException("Input is not a valid digit string", "digitString");
            }

            string result = string.Empty;
            seed %= SHUFFLED_CHARACTER_MAP.Length; // make seed (offset) to be less than number of characters available in the map
            digitString = GetDigest(number) + digitString; // prepend digest to the beginning
            Random rnd = new Random((int)DateTime.Now.Ticks);
            foreach (char c in digitString)
            {
                // random number is an additional offset (each digit occupies 3 characters)
                result += SHUFFLED_CHARACTER_MAP[(seed + (rnd.Next() % DIGIT_SPACE) + (c - '0') * DIGIT_SPACE) % SHUFFLED_CHARACTER_MAP.Length];
            }

            return string.Format("{0}{1}", SHUFFLED_CHARACTER_MAP[seed], result);
        }

        public string Encrypt(string digitString)
        {
            Random rnd = new Random((int)DateTime.Now.Ticks);
            return Encrypt(rnd.Next(99), digitString);
        }

        public bool Decrypt(string input, out string result)
        {
            result = null;
            if (string.IsNullOrEmpty(input) || input.Length < 4) // 1 seed, 2 digests, and 1 input
            {
                throw new ArgumentException("invalid input", "input");
            }

            long offset = SHUFFLED_CHARACTER_MAP.IndexOf(input[0]);
            if (offset < 0)
            {
                return false;
            }

            string digestString;
            if (!DecodeString(input.Substring(1, 2), offset, out digestString))
            {
                return false;
            }

            if (!DecodeString(input.Substring(3), offset, out result))
            {
                return false;
            }

            if (digestString.CompareTo(GetDigest(long.Parse(result))) != 0)
            {
                return false;
            }

            return true;
        }

        public bool Decrypt(string input, string number, out string result)
        {
            result = null;
            if (string.IsNullOrEmpty(input) || input.Length < 4) // 1 seed, 2 digests, and 1 input
            {
                throw new ArgumentException("invalid input:" + input + ",original number:" + number, "input");
            }

            int offset = SHUFFLED_CHARACTER_MAP.IndexOf(input[0]);
            if (offset < 0)
            {
                return false;
            }

            string digestString;
            if (!DecodeString(input.Substring(1, 2), offset, out digestString))
            {
                return false;
            }

            if (!DecodeString(input.Substring(3), offset, out result))
            {
                return false;
            }

            if (digestString.CompareTo(GetDigest(long.Parse(result))) != 0)
            {
                return false;
            }

            return true;
        }

        private string GetDigest(long number)
        {
            return ((number * 37) % 100).ToString("00");
        }

        private bool DecodeCharacter(char crypted, long offset, out char result)
        {
            result = '0';
            long index = SHUFFLED_CHARACTER_MAP.IndexOf(crypted);
            if (index < 0)
            {
                return false;
            }

            index += (index < offset ? SHUFFLED_CHARACTER_MAP.Length : 0) - offset;
            result += (char)(index / DIGIT_SPACE);
            return true;
        }

        private bool DecodeString(string crypted, long offset, out string result)
        {
            result = string.Empty;
            foreach (char c in crypted)
            {
                char x;
                if (DecodeCharacter(c, offset, out x))
                {
                    result += x;
                }
                else
                {
                    return false;
                }
            }

            return true;
        }
    }
}
﻿using System.IO;
using System.Web;

namespace LunchKingSite.Core.Component
{
    public interface IHttpPostedFileAdapter
    {
        int ContentLength { get; }
        string ContentType { get; }
        string FileName { get; }
        Stream InputStream { get; }
        void SaveAs(string filename);
    }

    public class AspNetHttpPostedFileAdapter : IHttpPostedFileAdapter
    {
        private HttpPostedFile _postFile;
        public AspNetHttpPostedFileAdapter(HttpPostedFile postFile)
        {
            this._postFile = postFile;
        }

        public virtual int ContentLength
        {
            get { return _postFile.ContentLength; }
        }

        public virtual string ContentType
        {
            get { return _postFile.ContentType; }
        }

        public virtual string FileName
        {
            get { return _postFile.FileName; }
        }

        public virtual Stream InputStream
        {
            get { return _postFile.InputStream; }
        }

        public virtual void SaveAs(string filename)
        {
            this._postFile.SaveAs(filename);
        }
    }

    public class MvcHttpPostedFileAdapter : IHttpPostedFileAdapter
    {
        private HttpPostedFileBase _postFile;
        public MvcHttpPostedFileAdapter(HttpPostedFileBase postFile)
        {
            this._postFile = postFile;
        }

        public virtual int ContentLength
        {
            get { return _postFile.ContentLength; }
        }

        public virtual string ContentType
        {
            get { return _postFile.ContentType; }
        }

        public virtual string FileName
        {
            get { return _postFile.FileName; }
        }

        public virtual Stream InputStream
        {
            get { return _postFile.InputStream; }
        }

        public virtual void SaveAs(string filename)
        {
            this._postFile.SaveAs(filename);
        }
    }

    public class PhysicalPostedFileAdapter : IHttpPostedFileAdapter
    {
        private FileInfo _fileInfo;
        public PhysicalPostedFileAdapter(string fileName)
        {
            this._fileInfo = new FileInfo(fileName);
        }

        public int ContentLength
        {
            get { return (int)_fileInfo.Length; }
        }

        public string ContentType
        {
            get
            {
                return MimeTypes.GetMimeTypeByExtension(_fileInfo.Extension);
            }
        }

        public string FileName
        {
            get { return _fileInfo.Name; }
        }

        public Stream InputStream
        {
            get { return _fileInfo.OpenRead(); }
        }

        public void SaveAs(string filename)
        {
            File.Copy(_fileInfo.FullName, filename);
        }
    }

    public static class HttpPostedFileExtension
    {
        public static IHttpPostedFileAdapter ToAdapter(this HttpPostedFile postedFile)
        {
            return new AspNetHttpPostedFileAdapter(postedFile);
        }

        public static IHttpPostedFileAdapter ToAdapter(this HttpPostedFileBase postedFile)
        {
            return new MvcHttpPostedFileAdapter(postedFile);
        }
    }
}

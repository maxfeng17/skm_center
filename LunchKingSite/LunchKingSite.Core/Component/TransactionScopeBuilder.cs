﻿using System;
using System.Transactions;

namespace LunchKingSite.Core.Component
{
    public class TransactionScopeBuilder
    {
        public static TransactionScope CreateReadCommitted()
        {
            return CreateReadCommitted(TimeSpan.FromSeconds(600));
        }

        public static TransactionScope CreateReadCommitted(TimeSpan timeSpan)
        {
            var transactionOptions = new TransactionOptions();
            transactionOptions.IsolationLevel = IsolationLevel.ReadCommitted;
            transactionOptions.Timeout = timeSpan;
            return new TransactionScope(TransactionScopeOption.Required, transactionOptions);
        }

        public static TransactionScope CreateReadUncommitted()
        {
            return CreateReadCommitted(TimeSpan.FromSeconds(600));
        }

        public static TransactionScope CreateReadUncommitted(TimeSpan timeSpan)
        {
            var transactionOptions = new TransactionOptions();
            transactionOptions.IsolationLevel = IsolationLevel.ReadUncommitted;
            transactionOptions.Timeout = timeSpan;
            return new TransactionScope(TransactionScopeOption.Required, transactionOptions);
        }
    }
}

﻿using System;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
namespace LunchKingSite.Web
{
    public partial class AboutUs : System.Web.UI.Page
    {
        ISysConfProvider cp = ProviderFactory.Instance().GetConfig();
        public ISysConfProvider SystemConfig
        {
            get
            {
                return cp;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            Response.RedirectPermanent("/about/us.html");
        }
    }
}

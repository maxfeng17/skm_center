﻿using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LunchKingSite.Web.Vourcher
{
    public partial class vourcherredirect : System.Web.UI.Page
    {
        private static ISysConfProvider cp = ProviderFactory.Instance().GetConfig();
        public string WebRoot
        {
            get
            {
                return cp.SiteUrl;
            }
        }

        public int VourcherId
        {
            get
            {
                int vourcher_id;
                if (!string.IsNullOrEmpty(Request.QueryString["VourcherId"]) && int.TryParse(Request.QueryString["VourcherId"], out vourcher_id))
                {
                    return vourcher_id;
                }
                else
                {
                    return 0;
                }
            }
        }
        public string AndroidUserAgent
        {
            get
            {
                return "/" + cp.AndroidUserAgent + "/i";
            }
        }

        public string iOSUserAgent
        {
            get
            {
                return "/" + cp.iOSUserAgent + "/i";
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {

        }
    }
}

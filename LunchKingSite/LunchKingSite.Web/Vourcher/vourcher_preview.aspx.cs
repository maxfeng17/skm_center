﻿using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using Microsoft.SqlServer.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Security;
using System.Web.UI.WebControls;

namespace LunchKingSite.Web.Vourcher
{
    public partial class vourcher_preview : RolePage
    {
        public int VourcherEventId
        {
            get
            {
                int id;
                if (!string.IsNullOrEmpty(Request.QueryString["eventid"].ToString()) && int.TryParse(Request.QueryString["eventid"].ToString(), out id))
                {
                    return id;
                }
                else
                {
                    return 0;
                }
            }
        }

        public bool IsOnlyOnce
        {
            get
            {
                bool onlyonce;
                if (bool.TryParse(hid_OnlyOnce.Value, out onlyonce))
                {
                    return onlyonce;
                }
                else
                {
                    return false; ;
                }
            }
            set
            {
                hid_OnlyOnce.Value = value.ToString();
            }
        }

        public bool IsOnlyOneStore
        {
            get
            {
                bool onlyonestore;
                if (bool.TryParse(hid_OnlyOneStore.Value, out onlyonestore))
                {
                    return onlyonestore;
                }
                else
                {
                    return false; ;
                }
            }
            set
            {
                hid_OnlyOneStore.Value = value.ToString();
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!User.Identity.IsAuthenticated)
            {
                FormsAuthentication.RedirectToLoginPage();
                return;
            }

            //權限尚未建立，暫時以公司domain作判別
            if (!User.Identity.Name.ToLower().Contains("17life.com"))
            {
                return;
            }

            if (!IsPostBack)
            {
                Panel1.Visible = Panel2.Visible = Panel3.Visible = Panel4.Visible = false;
                ViewVourcherSeller view_vourcher_seller = GetViewVourcherSeller();
                lab_SellerName_1.Text = lab_SellerName_2.Text = lab_SellerName_3.Text = lab_SellerName_4.Text = view_vourcher_seller.SellerName;
                lab_PageCount_1.Text = lab_PageCount_2.Text = view_vourcher_seller.PageCount.ToString();
                lab_VourcherSellerDescription_1.Text = lab_VourcherSellerDescription_2.Text = view_vourcher_seller.SellerDescription;
                lab_VourcherContents_1.Text = lab_VourcherContents_2.Text = view_vourcher_seller.Contents;
                lab_VourcherInstruction_1.Text = lab_VourcherInstruction_2.Text = EmptyStringCheck(view_vourcher_seller.Instruction, "注意事項:") + EmptyStringCheck(view_vourcher_seller.Restriction, "不適用:") + EmptyStringCheck(view_vourcher_seller.Others, "其他:", true);
                lab_VourcherEndDate_1.Text = lab_VourcherEndDate_2.Text = view_vourcher_seller.EndDate.HasValue ? view_vourcher_seller.EndDate.Value.ToString("yyyy/MM/dd") : "yyyy/MM/dd";
                lab_SellerConsumptionAvg_4.Text =
                    Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, (SellerConsumptionAvg)(view_vourcher_seller.SellerConsumptionAvg ?? 0));

                if (view_vourcher_seller.Mode == (int)VourcherEventMode.EventLimitQuantityOnlyOneTime || view_vourcher_seller.Mode == (int)VourcherEventMode.NoEventLimitQuantyOnlyOneTime)
                {
                    Panel1.Visible = true;
                    IsOnlyOnce = true;
                }
                else
                {
                    Panel2.Visible = true;
                    IsOnlyOnce = false;
                }
            }
        }

        protected void SelectStores(object sender, EventArgs e)
        {
            Panel1.Visible = Panel2.Visible = Panel3.Visible = Panel4.Visible = false;
            ViewVourcherStoreCollection view_vourcher_stores = GetViewVourcherStores();
            if (view_vourcher_stores.Count > 0)
            {
                ViewVourcherStore view_vourcher_store = view_vourcher_stores.First();
                SetVourcherStoreDetail(view_vourcher_store);
            }
            rpt_Stores_3.DataSource = view_vourcher_stores;
            rpt_Stores_3.DataBind();
            if (view_vourcher_stores.Count > 1)
            {
                Panel3.Visible = true;
                IsOnlyOneStore = false;
            }
            else
            {
                Panel4.Visible = true;
                IsOnlyOneStore = true;
            }
        }

        protected void RptStoreBind(object sender, RepeaterItemEventArgs e)
        {
            SqlGeography office_geo = LocationFacade.GetGeographicPoint("25.046388", "121.519695");
            if (e.Item.DataItem is ViewVourcherStore)
            {
                Label clab_Rpt_Distince_3 = (Label)e.Item.FindControl("lab_Rpt_Distince_3");
                ViewVourcherStore store = (ViewVourcherStore)e.Item.DataItem;
                string[] s = store.Coordinate.Split(' ');
                string latitide = string.Empty;
                string longitude = string.Empty;
                if (s.Length > 2)
                {
                    longitude = s[1].Replace("(", "");
                    latitide = s[2].Replace(")", "");
                    SqlGeography store_geo = LocationFacade.GetGeographicPoint(latitide, longitude);
                    clab_Rpt_Distince_3.Text = (office_geo.STDistance(store_geo).Value / 1000).ToString("F2");
                }
                else
                {
                    clab_Rpt_Distince_3.Text = "??";
                }
            }
        }

        protected void SetVourcherStoreDetail(ViewVourcherStore view_vourcher_store)
        {
            lab_StoreName_1.Text = lab_StoreName_2.Text = lab_StoreName_4.Text = view_vourcher_store.StoreName;
            lab_StoreAdd_4.Text = view_vourcher_store.City + view_vourcher_store.Town + view_vourcher_store.AddressString;
            lab_StorePhone_4.Text = view_vourcher_store.Phone;
            List<KeyValuePair<string, string>> details = new List<KeyValuePair<string, string>>();
            if (!string.IsNullOrEmpty(view_vourcher_store.OpenTime))
            {
                details.Add(new KeyValuePair<string, string>("營業時間", view_vourcher_store.OpenTime));
            }

            if (!string.IsNullOrEmpty(view_vourcher_store.CloseDate))
            {
                details.Add(new KeyValuePair<string, string>("公休時間", view_vourcher_store.CloseDate));
            }

            if (!string.IsNullOrEmpty(view_vourcher_store.WebUrl))
            {
                details.Add(new KeyValuePair<string, string>("官網", view_vourcher_store.WebUrl));
            }

            List<string> transpost = new List<string>() { view_vourcher_store.Mrt, view_vourcher_store.Car, view_vourcher_store.Bus, view_vourcher_store.OtherVehicles };
            if (transpost.Any(x => !string.IsNullOrEmpty(x)))
            {
                details.Add(new KeyValuePair<string, string>("大眾運輸", transpost.Aggregate((current, next) => current + "<br/>" + next)));
            }

            if (view_vourcher_store.CreditcardAvailable)
            {
                details.Add(new KeyValuePair<string, string>("可刷卡", string.Empty));
            }

            rpt_Details_4.DataSource = details;
            rpt_Details_4.DataBind();
        }

        protected void GetDetail(object sender, RepeaterCommandEventArgs e)
        {
            int id;
            if (e.CommandName == "GoDetail" && int.TryParse(e.CommandArgument.ToString(), out id))
            {
                ViewVourcherStore view_vourcher_store = GetViewVourcherStore(id);
                SetVourcherStoreDetail(view_vourcher_store);
                Panel1.Visible = Panel2.Visible = Panel3.Visible = Panel4.Visible = false;
                Panel4.Visible = true;
            }
        }

        protected void GoHome(object sender, EventArgs e)
        {
            Panel1.Visible = Panel2.Visible = Panel3.Visible = Panel4.Visible = false;
            if (IsOnlyOnce)
            {
                Panel1.Visible = true;
            }
            else
            {
                Panel2.Visible = true;
            }
        }

        protected void GoBack(object sender, EventArgs e)
        {
            if (Panel3.Visible)
            {
                Panel1.Visible = Panel2.Visible = Panel3.Visible = Panel4.Visible = false;
                if (IsOnlyOnce)
                {
                    Panel1.Visible = true;
                }
                else
                {
                    Panel2.Visible = true;
                }
            }
            else if (Panel4.Visible)
            {
                Panel1.Visible = Panel2.Visible = Panel3.Visible = Panel4.Visible = false;
                if (IsOnlyOneStore)
                {
                    if (IsOnlyOnce)
                    {
                        Panel1.Visible = true;
                    }
                    else
                    {
                        Panel2.Visible = true;
                    }
                }
                else
                {
                    Panel3.Visible = true;
                }
            }
        }

        private ViewVourcherSeller GetViewVourcherSeller()
        {
            ViewVourcherSeller vourcherseller = VourcherFacade.ViewVourcherSellerGetById(VourcherEventId);
            // 關注數加1
            VourcherFacade.UpdateVourcherEventPageCountAddCount(vourcherseller.Id, 1);
            return vourcherseller;
        }

        private ViewVourcherStoreCollection GetViewVourcherStores()
        {
            return VourcherFacade.ViewVourcherStoreCollectionGetByEventId(-1, 10, VourcherEventId, string.Empty);
        }

        private ViewVourcherStore GetViewVourcherStore(int id)
        {
            return VourcherFacade.ViewVourcherStoreGetById(id);
        }

        private string EmptyStringCheck(string input, string title, bool withmark = false)
        {
            if (string.IsNullOrEmpty(input))
            {
                return string.Empty;
            }
            else
            {
                return "<br/>" + title + "<br/>" + (withmark ? "■" : string.Empty) + input;
            }
        }
    }
}
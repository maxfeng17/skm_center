﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.Core.UI;
using LunchKingSite.WebLib.Views;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.DataOrm;
using System.Web.Services;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Component;
using System.Web.Security;
using System.IO;
using System.Text;
using Winnovative.WnvHtmlConvert;
using Winnovative.WnvHtmlConvert.PdfDocument;

namespace LunchKingSite.Web.Vourcher
{
    public partial class vourcher_print : RolePage, IVourcherEventPrintView
    {
        #region property
        private VourcherEventPrintPresenter _presenter;
        public VourcherEventPrintPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }
        /// <summary>
        /// 賣家guid
        /// </summary>
        public Guid SellerGuid
        {
            get
            {
                Guid seller_guid;
                if (Request["sid"] != null && Guid.TryParse(Request["sid"].ToString(), out seller_guid))
                    return seller_guid;
                else
                    return Guid.Empty;
            }
        }
        /// <summary>
        /// 優惠券id
        /// </summary>
        public int EventId
        {
            get
            {
                int event_id;
                if (Request["eid"] != null && int.TryParse(Request["eid"].ToString(), out event_id))
                    return event_id;
                else
                    return 0;
            }
        }
        /// <summary>
        /// 使用者名稱
        /// </summary>
        public string LoginUser
        {
            get { return Page.User.Identity.Name; }
        }

        public int UserId
        {
            get { return MemberFacade.GetUniqueId(Page.User.Identity.Name); }
        }
        #endregion
        #region event
        /// <summary>
        /// 依賣家guid搜尋優惠券
        /// </summary>
        public event EventHandler GetVourcherEventBySellerGuid;
        /// <summary>
        /// 依優惠券id搜尋
        /// </summary>
        public event EventHandler GetVourcherEventById;
        /// <summary>
        /// 印出優惠券合約
        /// </summary>
        public event EventHandler<DataEventArgs<List<int>>> GetPrintVourcherEvent;
        #endregion
        #region page
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!User.Identity.IsAuthenticated)
            {
                FormsAuthentication.RedirectToLoginPage();
                return;
            }
            //權限尚未建立，暫時以公司domain作判別
            if (!User.Identity.Name.ToLower().Contains("17life.com"))
            {
                return;
            }
            if (!IsPostBack)
            {
                _presenter.OnViewInitialized();
            }
            _presenter.OnViewLoaded();
            if (!IsPostBack)
            {
                if (EventId != 0 && GetVourcherEventById != null)
                {
                    lab_SearchMode.Text = "依優惠券搜尋";
                    GetVourcherEventById(sender, e);
                }
                else if (SellerGuid != Guid.Empty && GetVourcherEventBySellerGuid != null)
                {
                    lab_SearchMode.Text = "依賣家搜尋";
                    GetVourcherEventBySellerGuid(sender, e);
                }
                else
                {
                    lab_SearchMode.Text = string.Empty;
                    ClientScript.RegisterStartupScript(GetType(), "alert", "alert('尚未指定賣家或優惠券');", true);
                }
            }
        }
        /// <summary>
        /// 取出被勾選的優惠券id
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void PrintVourcherEvent(object sender, EventArgs e)
        {
            List<int> EventIdList = new List<int>();
            for (int i = 0; i < rpt_VourcherEvent.Items.Count; i++)
            {
                int event_id;
                CheckBox cbx = (CheckBox)rpt_VourcherEvent.Items[i].FindControl("cbx_Event");
                HiddenField hif = (HiddenField)rpt_VourcherEvent.Items[i].FindControl("hif_EventId");
                if (cbx.Checked && int.TryParse(hif.Value, out event_id))
                    EventIdList.Add(event_id);
            }
            if (EventIdList.Count > 0 && GetPrintVourcherEvent != null)
                GetPrintVourcherEvent(sender, new DataEventArgs<List<int>>(EventIdList));
        }
        /// <summary>
        /// 顯示優惠券的分店資料
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void repStore_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (!(e.Item.DataItem is ViewVourcherStore)) return;

            ViewVourcherStore data = (ViewVourcherStore)e.Item.DataItem;
            Label lblStoreName = (Label)e.Item.FindControl("lblStoreName");
            Label lblStoreAddress = (Label)e.Item.FindControl("lblStoreAddress");
            Label lblStoreTel = (Label)e.Item.FindControl("lblStoreTel");
            Label lblStoreOpeningTime = (Label)e.Item.FindControl("lblStoreOpeningTime");
            Label lblStoreCloseDate = (Label)e.Item.FindControl("lblStoreCloseDate");
            Label lblIsEDC = (Label)e.Item.FindControl("lblIsEDC");
            Label lblStoreVehicleInfo = (Label)e.Item.FindControl("lblStoreVehicleInfo");
            Label lblStoreMemo = (Label)e.Item.FindControl("lblStoreMemo");

            lblStoreName.Text = data.StoreName;
            if (data.TownshipId != null)
                lblStoreAddress.Text = CityManager.CityTownShopStringGet(data.TownshipId.Value) + data.AddressString;
            lblStoreTel.Text = data.Phone;
            lblStoreOpeningTime.Text = data.OpenTime;
            lblStoreCloseDate.Text = data.CloseDate;
            lblIsEDC.Text = data.CreditcardAvailable ? "是" : "否";
            string vihiclesInfo = string.Empty;
            if (!string.IsNullOrWhiteSpace(data.Mrt))
                vihiclesInfo += string.Format("捷運：{0}；", data.Mrt);
            if (!string.IsNullOrWhiteSpace(data.Car))
                vihiclesInfo += string.Format("開車：{0}；", data.Car);
            if (!string.IsNullOrWhiteSpace(data.Bus))
                vihiclesInfo += string.Format("公車：{0}；", data.Bus);
            if (!string.IsNullOrWhiteSpace(data.OtherVehicles))
                vihiclesInfo += string.Format("其他：{0}；", data.OtherVehicles);
            lblStoreVehicleInfo.Text = vihiclesInfo.TrimEnd('；');
            lblStoreMemo.Text = data.Remarks;
        }
        /// <summary>
        /// 顯示優惠券列表
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void repVourcher_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (!(e.Item.DataItem is VourcherEventStoreInfo)) return;

            VourcherEventStoreInfo data = (VourcherEventStoreInfo)e.Item.DataItem;
            Label lblVourcherType = (Label)e.Item.FindControl("lblVourcherType");
            Label lblVourcherContent = (Label)e.Item.FindControl("lblVourcherContent");
            Label lblVourcherInstruction = (Label)e.Item.FindControl("lblVourcherInstruction");
            Label lbllVourcherRestriction = (Label)e.Item.FindControl("lbllVourcherRestriction");
            Label lblMaxQuantity = (Label)e.Item.FindControl("lblMaxQuantity");
            Repeater repVourcherStoreList = (Repeater)e.Item.FindControl("repVourcherStoreList");
            Label lblOthers = (Label)e.Item.FindControl("lblOthers");

            Dictionary<int, string> eventTypeList = VourcherFacade.GetVourcherEventTypeDictionary();
            if (eventTypeList.ContainsKey(data.Vourcher_Event.Type))
                lblVourcherType.Text = eventTypeList[data.Vourcher_Event.Type];
            lblVourcherContent.Text = data.Vourcher_Event.Contents;
            lblVourcherInstruction.Text = data.Vourcher_Event.Instruction;
            Dictionary<int, string> eventModeList = VourcherFacade.GetVourcherEventModeDictionary();
            if (eventModeList.ContainsKey(data.Vourcher_Event.Mode))
                lblMaxQuantity.Text = eventModeList[data.Vourcher_Event.Mode];
            if (data.Vourcher_Event.Mode.EqualsAny((int)VourcherEventMode.EventLimitQuantityOnlyOneTime, (int)VourcherEventMode.EventLimitQuantityNoUsingTimes))
                lblMaxQuantity.Text += "；數量：" + data.Vourcher_Event.MaxQuantity;
            lbllVourcherRestriction.Text = data.Vourcher_Event.Restriction;
            repVourcherStoreList.DataSource = data.View_Vourcher_Stores;
            repVourcherStoreList.DataBind();
            lblOthers.Text = data.Vourcher_Event.Others;
        }
        /// <summary>
        /// 顯示優惠券分店
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void repVourcherStoreList_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (!(e.Item.DataItem is ViewVourcherStore)) return;

            ViewVourcherStore data = (ViewVourcherStore)e.Item.DataItem;
            Label lblStoreName = (Label)e.Item.FindControl("lblStoreName");
            Label lblStoreAddress = (Label)e.Item.FindControl("lblStoreAddress");
            lblStoreName.Text = data.StoreName;
        }

        #endregion
        #region method
        /// <summary>
        /// 顯示賣家資訊和所屬優惠券
        /// </summary>
        /// <param name="seller"></param>
        /// <param name="vourcherevents"></param>
        public void SetSellerVourcherEventCollection(Seller seller, ViewVourcherSellerCollection vourcherevents)
        {
            lab_ComapnyName.Text = seller.CompanyName;
            lab_CompanyId.Text = seller.SignCompanyID;
            lab_CompanyBossName.Text = seller.CompanyBossName;
            lab_Seller_Mobile.Text = seller.SellerMobile;
            lab_Seller_Boss_Name.Text = seller.SellerBossName;
            lab_Seller_Fax.Text = seller.SellerFax;
            lab_Seller_Tel.Text = seller.SellerTel;
            lab_Seller_Email.Text = seller.SellerEmail;
            lab_Seller_Address.Text = CityManager.CityTownShopStringGet(seller.CityId) + seller.SellerAddress;
            rpt_VourcherEvent.DataSource = vourcherevents.OrderByDescending(x => x.Id);
            rpt_VourcherEvent.DataBind();
        }
        /// <summary>
        /// 顯示賣家和優惠券
        /// </summary>
        /// <param name="seller"></param>
        /// <param name="vourcherevent"></param>
        public void SetSellerVourcherEvent(Seller seller, ViewVourcherSeller vourcherevent)
        {
            lab_ComapnyName.Text = seller.CompanyName;
            lab_CompanyId.Text = seller.SignCompanyID;
            lab_CompanyBossName.Text = seller.CompanyBossName;
            lab_Seller_Mobile.Text = seller.SellerMobile;
            lab_Seller_Boss_Name.Text = seller.SellerBossName;
            lab_Seller_Fax.Text = seller.SellerFax;
            lab_Seller_Tel.Text = seller.SellerTel;
            lab_Seller_Email.Text = seller.SellerEmail;
            lab_Seller_Address.Text = CityManager.CityTownShopStringGet(seller.CityId) + seller.SellerAddress;
            rpt_VourcherEvent.DataSource = vourcherevent;
            rpt_VourcherEvent.DataBind();
        }
        public void SetSellerVourcherEventSotres(Seller seller, List<VourcherEventStoreInfo> info, string salesName)
        {
            lblBasicInfo.Text = salesName + "/" + DateTime.Now.ToString("yyyy.MM.dd");
            lblSellerName.Text = seller.SellerName;
            lblCompanyName.Text = seller.CompanyName;
            lblCompanyAddress.Text = CityManager.CityTownShopStringGet(seller.CityId) + seller.SellerAddress;
            lblCompanyId.Text = seller.SignCompanyID;
            lblBossName.Text = seller.SellerBossName;
            lblContactPerson.Text = seller.SellerContactPerson;
            lblMobile.Text = seller.SellerMobile;
            lblSellerTel.Text = seller.SellerTel;
            lblSellerFax.Text = seller.SellerFax;
            lblSellerEmail.Text = seller.SellerEmail;
            lblWebUrl.Text = seller.SellerBlog;
            if (seller.SellerCategory != null)
            {
                Dictionary<int, string> categoryList = VourcherFacade.GetSellerSampleCategoryDictionary();
                if (categoryList.ContainsKey(seller.SellerCategory.Value))
                    lblSellerCategory.Text = categoryList[seller.SellerCategory.Value];
            }
            if (seller.SellerConsumptionAvg.HasValue)
            {
                Dictionary<int, string> consumptionAvgList = VourcherFacade.GetSellerConsumptionAvgDictionary();
                if (consumptionAvgList.ContainsKey(seller.SellerConsumptionAvg.Value))
                    lblPerCustomerTrans.Text = consumptionAvgList[seller.SellerConsumptionAvg.Value];
            }
            else
            {
                lblPerCustomerTrans.Text = "未選取平均消費金額";
            }
            lblSellerDescription.Text = seller.SellerDescription;

            List<ViewVourcherStore> vourcherStoreList = new List<ViewVourcherStore>();
            foreach (ViewVourcherStoreCollection storeList in info.Select(x => x.View_Vourcher_Stores))
            {
                foreach (ViewVourcherStore store in storeList)
                {
                    if (!vourcherStoreList.Select(x => x.StoreGuid).Contains(store.StoreGuid))
                        vourcherStoreList.Add(store);
                }
            }
            repStore.DataSource = vourcherStoreList;
            repStore.DataBind();

            repVourcher.DataSource = info;
            repVourcher.DataBind();

            PrintPdf();
        }
        #endregion

        private void PrintPdf()
        {
            ISysConfProvider config = ProviderFactory.Instance().GetConfig();
            string fileName = string.Format("{0}_APP優惠券_{1}_{2}", lblSellerName.Text, DateTime.Now.ToString("yyyy_MM_dd"), config.ResponseWriteType);
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment; filename=\"" + fileName + ".pdf\"");
            PdfConverter pdfc = new PdfConverter();
            pdfc.LicenseKey = @"YklTQlNCVFZCVExSQlFTTFNQTFtbW1s=";
            pdfc.PdfDocumentOptions.PdfPageSize = PdfPageSize.A4;
            pdfc.PdfDocumentOptions.TopMargin = 40;
            pdfc.PdfDocumentOptions.LeftMargin = 50;
            pdfc.PdfDocumentOptions.BottomMargin = 81;
            pdfc.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.Normal;
            pdfc.PdfDocumentOptions.PdfPageOrientation = PDFPageOrientation.Portrait;
            StringWriter sr = new StringWriter();
            HtmlTextWriter htmlWriter = new HtmlTextWriter(sr);
            divVourcherPrint.RenderControl(htmlWriter);
            if (config.ResponseWriteType == 0)
            {
                Response.BinaryWrite(pdfc.GetPdfBytesFromHtmlString(sr.ToString()));
                Response.End();
            }
            else if (config.ResponseWriteType == 1)
            {
                Response.BinaryWrite(pdfc.GetPdfBytesFromHtmlString(sr.ToString()));
                Response.Flush();
            }
            else if (config.ResponseWriteType == 2)
            {
                Response.Buffer = false;
                Response.BinaryWrite(pdfc.GetPdfBytesFromHtmlString(sr.ToString()));
                Response.End();
            }
            else if (config.ResponseWriteType == 3)
            {
                string path = Path.Combine(Path.GetTempPath(), fileName + ".pdf");
                pdfc.SavePdfFromHtmlStringToFile(sr.ToString(), path);
                Response.TransmitFile(path);
                Response.End();
            }
        }
    }
}
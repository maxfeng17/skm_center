﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="vourcher_preview.aspx.cs"
    Inherits="LunchKingSite.Web.Vourcher.vourcher_preview" %>

<%@ Import Namespace="LunchKingSite.DataOrm" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>優惠券預覽</title>
    <link href="../Themes/Coupon_Preview/CSS/Coupon_Preivew.css" rel="stylesheet" type="text/css" />
    <script src="../Tools/js/jquery-1.7.2.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#maintable').css({ 'margin-left': (($(window).width() - $('#maintable').width()) / 2) + 'px' });
            $(window).resize(function () {
                $('#maintable').css({ 'margin-left': (($(window).width() - $('#maintable').width()) / 2) + 'px' });
            });
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:HiddenField ID="hid_OnlyOnce" runat="server" />
        <asp:HiddenField ID="hid_OnlyOneStore" runat="server" />
        <table id="maintable">
            <tr>
                <td style="float: left">
                    <asp:Button ID="btn_Home" runat="server" Text="Home" OnClick="GoHome" />
                </td>
                <td style="float: right">
                    <asp:Button ID="btn_Back" runat="server" Text="Back" OnClick="GoBack" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Panel ID="Panel1" runat="server">
                        <div class="main">
                            <div class="title">
                                <asp:Label ID="lab_SellerName_1" runat="server"></asp:Label></div>
                            <div class="focus_count">
                                關注次數：<asp:Label ID="lab_PageCount_1" runat="server"></asp:Label>次</div>
                            <div class="coupon_box">
                                <div class="coupon_tag">
                                    優<br />
                                    惠<br />
                                    券</div>
                                <div class="coupon_content">
                                    <h2>
                                        <asp:Label ID="lab_StoreName_1" runat="server"></asp:Label></h2>
                                    <h1>
                                        <asp:Label ID="lab_VourcherContents_1" runat="server"></asp:Label></h1>
                                    <hr />
                                    <p>
                                        <asp:Label ID="lab_VourcherInstruction_1" runat="server"></asp:Label></p>
                                    <p>
                                        <asp:Label ID="lab_VourcherEndDate_1" runat="server"></asp:Label>截止</p>
                                </div>
                                <div class="coupon_logo">
                                </div>
                            </div>
                            <div class="use_state">
                                <div class="button BTNL_red">
                                    點我使用</div>
                                <p class="red_note">
                                    限每人使用一次，點選後視同使用。</p>
                            </div>
                            <div class="function_area">
                                <asp:Button ID="btn_SelectVourcherStores_1" runat="server" Text="適用店家" CssClass="button_2 BT_gray"
                                    OnClick="SelectStores" />
                                <div class="function_box BT_gray">
                                    <div class="function">
                                        <img src="../Themes/Coupon_Preview/Images/cu_share_icon_1.png" width="80" height="80"
                                            alt="" /><br />
                                        +收藏</div>
                                    <div class="function">
                                        <img src="../Themes/Coupon_Preview/Images/cu_share_icon_2.png" width="80" height="80"
                                            alt="" /><br />
                                        PassBook</div>
                                    <div class="function">
                                        <img src="../Themes/Coupon_Preview/Images/cu_share_icon_3.png" width="80" height="80"
                                            alt="" /><br />
                                        分享</div>
                                </div>
                            </div>
                            <div class="intro_bar">
                                特色介紹</div>
                            <asp:Label ID="lab_VourcherSellerDescription_1" runat="server"></asp:Label>
                        </div>
                    </asp:Panel>
                    <asp:Panel ID="Panel2" runat="server">
                        <div class="main">
                            <div class="title">
                                <asp:Label ID="lab_SellerName_2" runat="server"></asp:Label></div>
                            <div class="focus_count">
                                關注次數：<asp:Label ID="lab_PageCount_2" runat="server"></asp:Label>次</div>
                            <div class="coupon_box">
                                <div class="coupon_tag">
                                    優<br />
                                    惠<br />
                                    券</div>
                                <div class="coupon_content">
                                    <h2>
                                        <asp:Label ID="lab_StoreName_2" runat="server"></asp:Label></h2>
                                    <h1>
                                        <asp:Label ID="lab_VourcherContents_2" runat="server"></asp:Label></h1>
                                    <hr />
                                    <p>
                                        <asp:Label ID="lab_VourcherInstruction_2" runat="server"></asp:Label></p>
                                    <p>
                                        <asp:Label ID="lab_VourcherEndDate_2" runat="server"></asp:Label>截止</p>
                                </div>
                                <div class="coupon_logo">
                                </div>
                            </div>
                            <div class="use_state">
                                <p class="red_note">
                                    <span class="pass_img">&nbsp;</span>直接出示優惠券即可享用優惠</p>
                            </div>
                            <div class="function_area">
                                <asp:Button ID="btn_SelectVourcherStores_2" runat="server" Text="適用店家" CssClass="button_2 BT_gray"
                                    OnClick="SelectStores" />
                                <div class="function_box BT_gray">
                                    <div class="function">
                                        <img src="../Themes/Coupon_Preview/Images/cu_share_icon_1.png" width="80" height="80"
                                            alt="" /><br />
                                        +收藏</div>
                                    <div class="function">
                                        <img src="../Themes/Coupon_Preview/Images/cu_share_icon_2.png" width="80" height="80"
                                            alt="" /><br />
                                        PassBook</div>
                                    <div class="function">
                                        <img src="../Themes/Coupon_Preview/Images/cu_share_icon_3.png" width="80" height="80"
                                            alt="" /><br />
                                        分享</div>
                                </div>
                            </div>
                            <div class="intro_bar">
                                特色介紹</div>
                            <asp:Label ID="lab_VourcherSellerDescription_2" runat="server"></asp:Label>
                        </div>
                    </asp:Panel>
                    <asp:Panel ID="Panel3" runat="server" Visible="false">
                        <div class="main">
                            <div class="title">
                                <asp:Label ID="lab_SellerName_3" runat="server"></asp:Label></div>
                            <asp:Repeater ID="rpt_Stores_3" runat="server" OnItemCommand="GetDetail" OnItemDataBound="RptStoreBind">
                                <ItemTemplate>
                                    <div class="Multi_shop_box">
                                        <div class="Multi_shop_info_left">
                                            <div class="shop_name short">
                                                <%# ((ViewVourcherStore)(Container.DataItem)).StoreName %></div>
                                            <div class="Multi_shop_info">
                                                <%# ((ViewVourcherStore)(Container.DataItem)).City+((ViewVourcherStore)(Container.DataItem)).Town+((ViewVourcherStore)(Container.DataItem)).AddressString %><br />
                                                <%# ((ViewVourcherStore)(Container.DataItem)).Phone %>
                                            </div>
                                        </div>
                                        <div class="Multi_shop_info_right">
                                            <div class="kilometer">
                                                &lt;<asp:Label ID="lab_Rpt_Distince_3" runat="server"></asp:Label>km</div>
                                            <asp:LinkButton ID="lbt_Rpt_Store_3" runat="server" CommandName="GoDetail" CommandArgument="<%# ((ViewVourcherStore)(Container.DataItem)).Id %>"><div class="button_right_arrow"></div></asp:LinkButton>
                                        </div>
                                        <div class="clear">
                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                    </asp:Panel>
                    <asp:Panel ID="Panel4" runat="server" Visible="false">
                        <div class="main">
                            <div class="title">
                                <asp:Label ID="lab_SellerName_4" runat="server"></asp:Label></div>
                            <div class="shop_name">
                                <asp:Label ID="lab_StoreName_4" runat="server"></asp:Label></div>
                            <div class="avg_consumption">
                                平均消費：<asp:Label ID="lab_SellerConsumptionAvg_4" runat="server"></asp:Label></div>
                            <div class="shop_info">
                                <div class="info_bar">
                                    <div class="content">
                                        <asp:Label ID="lab_StoreAdd_4" runat="server"></asp:Label></div>
                                    <div class="button_arrow">
                                    </div>
                                </div>
                                <div class="info_bar">
                                    <div class="content">
                                        <asp:Label ID="lab_StorePhone_4" runat="server"></asp:Label></div>
                                    <div class="button_arrow">
                                    </div>
                                </div>
                                <div class="content">
                                    <asp:Repeater ID="rpt_Details_4" runat="server">
                                        <HeaderTemplate>
                                            <ul>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <li>
                                                <%# "<div style='font-weight:bold'>"+((KeyValuePair<string, string>)(Container.DataItem)).Key +"<div/>"+ ((KeyValuePair<string, string>)(Container.DataItem)).Value%></li>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </ul>
                                        </FooterTemplate>
                                    </asp:Repeater>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>

﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Security;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.BizLogic.Model.API;
using Microsoft.SqlServer.Types;

namespace LunchKingSite.Web.Vourcher
{
    public partial class vourcherseller : RolePage, IVourcherSellerView
    {
        public ISysConfProvider Config = ProviderFactory.Instance().GetConfig();

        #region property

        #region view property

        private VourcherSellerPresenter _presenter;

        public VourcherSellerPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        /// <summary>
        /// 使用者名稱
        /// </summary>
        public string UserName
        {
            get
            {
                return User.Identity.Name;
            }
        }

        /// <summary>
        /// 賣家Guid
        /// </summary>
        public Guid SellerGuid
        {
            get
            {
                Guid seller_guid;
                if (Guid.TryParse(hif_TempSellerGuid.Value, out seller_guid))
                {
                    return seller_guid;
                }
                else
                {
                    return Guid.Empty;
                }
            }
            set
            {
                hif_TempSellerGuid.Value = value.ToString();
            }
        }

        /// <summary>
        /// 賣家Id
        /// </summary>
        public string SellerId
        {
            get
            {
                return hif_TempSellerId.Value;
            }
            set
            {
                hif_TempSellerId.Value = lab_Seller_SellerId.Text = lab_Store_SellerId.Text = string.IsNullOrEmpty(value) ? "新賣家" : value.ToString();
            }
        }

        /// <summary>
        /// 分店Guid
        /// </summary>
        public Guid StoreGuid
        {
            get
            {
                Guid store_guid;
                if (Guid.TryParse(hif_TempStoreGuid.Value, out store_guid))
                {
                    return store_guid;
                }
                else
                {
                    return Guid.Empty;
                }
            }
            set
            {
                hif_TempStoreGuid.Value = lab_Store_StoreGuid.Text = value.ToString();
                lab_Store_StoreGuid.Text = value == Guid.Empty ? "新分店" : value.ToString();
            }
        }

        /// <summary>
        /// 分頁的每頁大小
        /// </summary>
        public int PageSize
        {
            get
            {
                return gridPager.PageSize;
            }
        }

        /// <summary>
        /// 分頁的總資料量大小
        /// </summary>
        public int PageCount
        {
            get
            {
                int pagecount;
                return int.TryParse((ViewState["pagecount"] == null ? string.Empty : ViewState["pagecount"].ToString()), out pagecount) ? pagecount : 0;
            }
            set
            {
                ViewState["pagecount"] = value;
            }
        }

        /// <summary>
        /// 搜尋賣家的欄位和輸入資料
        /// </summary>
        public KeyValuePair<string, string> SearchKeys
        {
            get
            {
                KeyValuePair<string, string> searchkey = new KeyValuePair<string, string>(ddl_Search.SelectedValue, tbx_Search.Text.Trim());
                return searchkey;
            }
        }

        /// <summary>
        /// 暫存的狀態
        /// </summary>
        public SellerTempStatus TempStatus
        {
            get
            {
                SellerTempStatus tempstatus;
                if (Enum.TryParse<SellerTempStatus>(ddl_TempStatus.SelectedValue, out tempstatus))
                {
                    return tempstatus;
                }
                else
                {
                    return SellerTempStatus.Applied;
                }
            }
        }

        #endregion view property

        /// <summary>
        /// 城市Id
        /// </summary>
        public int CityId
        {
            get
            {
                int city_id;
                if (int.TryParse(hif_Seller_City.Value, out city_id))
                {
                    return city_id;
                }
                else
                {
                    return 0;
                }
            }
            set
            {
                hif_Seller_City.Value = value.ToString();
            }
        }

        /// <summary>
        /// 區域Id
        /// </summary>
        public int TownShipId
        {
            get
            {
                int township_id;
                if (int.TryParse(hif_Seller_Township.Value, out township_id))
                {
                    return township_id;
                }
                else
                {
                    return 0;
                }
            }
            set
            {
                hif_Seller_Township.Value = value.ToString();
            }
        }

        /// <summary>
        /// 銀行代碼
        /// </summary>
        public string BankCode
        {
            get
            {
                return hif_BankCode.Value;
            }
            set
            {
                hif_BankCode.Value = value;
            }
        }

        /// <summary>
        /// 分行代碼
        /// </summary>
        public string BranchCode
        {
            get
            {
                return hif_BranchCode.Value;
            }
            set
            {
                hif_BranchCode.Value = value;
            }
        }

        /// <summary>
        /// 賣家分類
        /// </summary>
        public int? SellerCategory
        {
            get
            {
                int seller_category;
                if (int.TryParse(ddl_Seller_Category.SelectedValue, out seller_category))
                {
                    if (seller_category == -1)
                    {
                        return null;
                    }
                    else
                    {
                        return seller_category;
                    }
                }
                else
                {
                    return null;
                }
            }
            set
            {
                if (value == null)
                {
                    ddl_Seller_Category.SelectedIndex = -1;
                }
                else
                {
                    ddl_Seller_Category.SelectedValue = value.Value.ToString();
                }
            }
        }

        /// <summary>
        /// 賣家平均消費
        /// </summary>
        public int? SellerConsumptionAvg
        {
            get
            {
                int seller_consumptionavg;
                if (int.TryParse(rbl_Seller_ConsumptionAvg.SelectedValue, out seller_consumptionavg))
                {
                    return seller_consumptionavg;
                }
                else
                {
                    return null;
                }
            }
            set
            {
                if (value == null)
                {
                    rbl_Seller_ConsumptionAvg.SelectedIndex = -1;
                }
                else
                {
                    rbl_Seller_ConsumptionAvg.SelectedValue = value.Value.ToString();
                }
            }
        }

        /// <summary>
        /// 前台web control和 Id，用來控制隱藏顯示'
        /// </summary>
        public Dictionary<string, WebControl> FrontPanels
        {
            get
            {
                Dictionary<string, WebControl> panels = new Dictionary<string, WebControl>();
                panels.Add(pan_SearchSeller.ID, pan_SearchSeller);
                panels.Add(pan_VourcherTempSeller.ID, pan_VourcherTempSeller);
                panels.Add(pan_VourcherTempStore.ID, pan_VourcherTempStore);
                panels.Add(pan_Account.ID, pan_Account);
                panels.Add(pan_VourcherTempSeller_Stores.ID, pan_VourcherTempSeller_Stores);
                panels.Add(btn_AddNewSeller.ID, btn_AddNewSeller);
                panels.Add(btn_AddNewStore.ID, btn_AddNewStore);
                panels.Add(pan_ChangeLog.ID, pan_ChangeLog);
                panels.Add(hyp_AddVourcher.ID, hyp_AddVourcher);
                return panels;
            }
        }

        /// <summary>
        /// 賣家異動的欄位名稱和用來顯示Literal
        /// </summary>
        public Dictionary<string, Literal> SellerChangeItemLiteral
        {
            get
            {
                Dictionary<string, Literal> change_items_literal = new Dictionary<string, Literal>();
                change_items_literal.Add(Seller.SellerNameColumn.ColumnName, lit_Seller_SellerName);
                change_items_literal.Add(Seller.SellerBossNameColumn.ColumnName, lit_Seller_BossName);
                change_items_literal.Add(Seller.SellerTelColumn.ColumnName, lit_Seller_Tel);
                change_items_literal.Add(Seller.SellerFaxColumn.ColumnName, lit_Seller_Fax);
                change_items_literal.Add(Seller.SellerMobileColumn.ColumnName, lit_Seller_Mobile);
                change_items_literal.Add(Seller.SellerAddressColumn.ColumnName, lit_Seller_Address);
                change_items_literal.Add(Seller.SellerEmailColumn.ColumnName, lit_Seller_Email);
                change_items_literal.Add(Seller.CompanyNameColumn.ColumnName, lit_Seller_ComapnyName);
                change_items_literal.Add(Seller.SignCompanyIDColumn.ColumnName, lit_Seller_CompanySignId);
                change_items_literal.Add(Seller.CompanyBossNameColumn.ColumnName, lit_Seller_CompanyBossName);
                change_items_literal.Add(Seller.SellerContactPersonColumn.ColumnName, lit_Seller_Contact_Person);
                change_items_literal.Add(Seller.SellerBlogColumn.ColumnName, lit_Seller_Blog);
                change_items_literal.Add(Seller.SellerDescriptionColumn.ColumnName, lit_Seller_Description);
                change_items_literal.Add(Seller.SellerCategoryColumn.ColumnName, lit_Seller_Category);
                change_items_literal.Add(Seller.SellerConsumptionAvgColumn.ColumnName, lit_Seller_ConsumptionAvg);

                return change_items_literal;
            }
        }

        /// <summary>
        /// 賣家異動的欄位名稱和用來輸入的TextBox
        /// </summary>
        public Dictionary<string, TextBox> SellerChangeItemTextBox
        {
            get
            {
                Dictionary<string, TextBox> change_items_textbox = new Dictionary<string, TextBox>();
                change_items_textbox.Add(Seller.SellerNameColumn.ColumnName, tbx_Seller_SellerName);
                change_items_textbox.Add(Seller.SellerBossNameColumn.ColumnName, tbx_Seller_BossName);
                change_items_textbox.Add(Seller.SellerTelColumn.ColumnName, tbx_Seller_Tel);
                change_items_textbox.Add(Seller.SellerFaxColumn.ColumnName, tbx_Seller_Fax);
                change_items_textbox.Add(Seller.SellerMobileColumn.ColumnName, tbx_Seller_Mobile);
                change_items_textbox.Add(Seller.SellerAddressColumn.ColumnName, tbx_Seller_Address);
                change_items_textbox.Add(Seller.CoordinateColumn.ColumnName, txtLatitude);
                change_items_textbox.Add(Seller.SellerEmailColumn.ColumnName, tbx_Seller_Email);
                change_items_textbox.Add(Seller.CompanyNameColumn.ColumnName, tbx_Seller_ComapnyName);
                change_items_textbox.Add(Seller.SignCompanyIDColumn.ColumnName, tbx_Seller_CompanySignId);
                change_items_textbox.Add(Seller.CompanyBossNameColumn.ColumnName, tbx_Seller_CompanyBossName);
                change_items_textbox.Add(Seller.SellerContactPersonColumn.ColumnName, tbx_Seller_Contact_Person);
                change_items_textbox.Add(Seller.SellerBlogColumn.ColumnName, tbx_Seller_Blog);
                change_items_textbox.Add(Seller.SellerDescriptionColumn.ColumnName, tbx_Seller_Description);

                return change_items_textbox;
            }
        }

        /// <summary>
        /// 分店異動的欄位名稱和用來顯示Literal
        /// </summary>
        public Dictionary<string, Literal> StoreChangeItemLiteral
        {
            get
            {
                Dictionary<string, Literal> change_items_literal = new Dictionary<string, Literal>();
                change_items_literal.Add(Store.StoreNameColumn.ColumnName, lit_Store_StoreName);
                change_items_literal.Add(Store.CompanyBossNameColumn.ColumnName, lit_Seller_CompanyBossName);
                change_items_literal.Add(Store.PhoneColumn.ColumnName, lit_Store_Phone);
                change_items_literal.Add(Store.AddressStringColumn.ColumnName, lit_Seller_Address);
                change_items_literal.Add(Store.OpenTimeColumn.ColumnName, lit_Store_OpenTime);
                change_items_literal.Add(Store.CloseDateColumn.ColumnName, lit_Store_CloseDate);
                change_items_literal.Add(Store.RemarksColumn.ColumnName, lit_Store_Remark);
                change_items_literal.Add(Store.MrtColumn.ColumnName, lit_Store_MRT);
                change_items_literal.Add(Store.CarColumn.ColumnName, lit_Store_Car);
                change_items_literal.Add(Store.BusColumn.ColumnName, lit_Store_Bus);
                change_items_literal.Add(Store.OtherVehiclesColumn.ColumnName, lit_Store_OtherVehicle);
                change_items_literal.Add(Store.WebUrlColumn.ColumnName, lit_Store_WebUrl);
                change_items_literal.Add(Store.FacebookUrlColumn.ColumnName, lit_Store_FaceBookUrl);
                change_items_literal.Add(Store.PlurkUrlColumn.ColumnName, lit_Store_PlurkUrl);
                change_items_literal.Add(Store.BlogUrlColumn.ColumnName, lit_Store_BlogUrl);
                change_items_literal.Add(Store.OtherUrlColumn.ColumnName, lit_Store_OtherUrl);
                change_items_literal.Add(Store.SignCompanyIDColumn.ColumnName, lit_Seller_CompanySignId);
                change_items_literal.Add(Store.MessageColumn.ColumnName, lit_Store_Message);
                change_items_literal.Add(Store.CompanyNameColumn.ColumnName, lit_Seller_ComapnyName);

                return change_items_literal;
            }
        }

        /// <summary>
        /// 賣家異動的欄位名稱和用來輸入的TextBox
        /// </summary>
        public Dictionary<string, TextBox> StoreChangeItemTextBox
        {
            get
            {
                Dictionary<string, TextBox> change_items_textbox = new Dictionary<string, TextBox>();
                change_items_textbox.Add(Store.StoreNameColumn.ColumnName, tbx_Store_StoreName);
                change_items_textbox.Add(Store.CompanyBossNameColumn.ColumnName, tbx_Seller_CompanyBossName);
                change_items_textbox.Add(Store.PhoneColumn.ColumnName, tbx_Store_Phone);
                change_items_textbox.Add(Store.AddressStringColumn.ColumnName, tbx_Seller_Address);
                change_items_textbox.Add(Store.CoordinateColumn.ColumnName, txtLatitude);
                change_items_textbox.Add(Store.OpenTimeColumn.ColumnName, tbx_Store_OpenTime);
                change_items_textbox.Add(Store.CloseDateColumn.ColumnName, tbx_Store_CloseDate);
                change_items_textbox.Add(Store.RemarksColumn.ColumnName, tbx_Store_Remark);
                change_items_textbox.Add(Store.MrtColumn.ColumnName, tbx_Store_MRT);
                change_items_textbox.Add(Store.CarColumn.ColumnName, tbx_Store_Car);
                change_items_textbox.Add(Store.BusColumn.ColumnName, tbx_Store_Bus);
                change_items_textbox.Add(Store.OtherVehiclesColumn.ColumnName, tbx_Store_OtherVehicle);
                change_items_textbox.Add(Store.WebUrlColumn.ColumnName, tbx_Store_WebUrl);
                change_items_textbox.Add(Store.FacebookUrlColumn.ColumnName, tbx_Store_FaceBookUrl);
                change_items_textbox.Add(Store.PlurkUrlColumn.ColumnName, tbx_Store_PlurkUrl);
                change_items_textbox.Add(Store.BlogUrlColumn.ColumnName, tbx_Store_BlogUrl);
                change_items_textbox.Add(Store.OtherUrlColumn.ColumnName, tbx_Store_OtherUrl);
                change_items_textbox.Add(Store.SignCompanyIDColumn.ColumnName, tbx_Seller_CompanySignId);
                change_items_textbox.Add(Store.MessageColumn.ColumnName, tbx_Seller_Message);
                change_items_textbox.Add(Store.CompanyNameColumn.ColumnName, tbx_Seller_ComapnyName);

                return change_items_textbox;
            }
        }

        /// <summary>
        /// 外部搜尋seller 或是store
        /// </summary>
        public string Mode
        {
            get
            {
                if (Request["mode"] != null)
                {
                    return Request["mode"].ToString();
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        /// <summary>
        /// 外部搜尋參數seller_guid或store_guid
        /// </summary>
        public Guid SearchGuid
        {
            get
            {
                Guid guid;
                if (Request["guid"] != null && Guid.TryParse(Request["guid"].ToString(), out guid))
                {
                    return guid;
                }
                else
                {
                    return Guid.Empty;
                }
            }
        }

        #endregion property

        #region event

        //搜尋賣家
        public event EventHandler SearchSellerInfo;

        //儲存賣家
        public event EventHandler<DataEventArgs<KeyValuePair<Seller, List<PhotoInfo>>>> SaveTempSeller;

        //儲存分店
        public event EventHandler<DataEventArgs<Store>> SaveTempStore;

        //撈取賣家資料
        public event EventHandler<DataEventArgs<Guid>> GetSellerByGuid;

        //撈取分店資料
        public event EventHandler<DataEventArgs<Guid>> GetStoreByGuid;

        //搜尋賣家資料分頁
        public event EventHandler<DataEventArgs<int>> PageChanged;

        //搜尋賣家資料計算總筆數
        public event EventHandler GetDataCount;

        public event EventHandler GetTempStatusSellerStore;

        #endregion event

        #region page

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!User.Identity.IsAuthenticated)
            {
                FormsAuthentication.RedirectToLoginPage();
                return;
            }

            //權限尚未建立，暫時以公司domain作判別
            if (!User.Identity.Name.ToLower().Contains("17life.com"))
            {
                return;
            }

            if (!Page.IsPostBack)
            {
                ddl_TempStatus.Items.Add(new ListItem("申請建立", ((int)SellerTempStatus.Applied).ToString()));
                ddl_TempStatus.Items.Add(new ListItem("退回申請", ((int)SellerTempStatus.Returned).ToString()));

                ddl_Search.Items.Add(new ListItem("統編/ID", Seller.SignCompanyIDColumn.ColumnName));
                ddl_Search.Items.Add(new ListItem("品牌名稱", Seller.SellerNameColumn.ColumnName));
                ddl_Search.Items.Add(new ListItem("負責人", Seller.CompanyBossNameColumn.ColumnName));
                ddl_Search.Items.Add(new ListItem("簽約公司名稱", Seller.CompanyNameColumn.ColumnName));
                ddl_Seller_Category.Items.Add(new ListItem("請選擇", "-1"));

                ddl_Seller_Category.DataSource = VourcherFacade.GetSellerSampleCategoryDictionary();
                ddl_Seller_Category.DataTextField = "Value";
                ddl_Seller_Category.DataValueField = "Key";
                ddl_Seller_Category.DataBind();
                rbl_Seller_ConsumptionAvg.DataSource = VourcherFacade.GetSellerConsumptionAvgDictionary();
                rbl_Seller_ConsumptionAvg.DataTextField = "Value";
                rbl_Seller_ConsumptionAvg.DataValueField = "Key";
                rbl_Seller_ConsumptionAvg.DataBind();

                _presenter.OnViewInitialized();
            }
            _presenter.OnViewLoaded();
            if (!IsPostBack)
            {
                if (!string.IsNullOrEmpty(Mode) && SearchGuid != Guid.Empty)
                {
                    if (Mode == "seller")
                    {
                        //選取單一賣家顯示資料
                        if (GetSellerByGuid != null)
                        {
                            GetSellerByGuid(sender, new DataEventArgs<Guid>(SearchGuid));
                        }
                        btn_AddNewSeller.Text = "更新賣家";
                        lit_Address.Text = "簽約";
                    }
                    else if (Mode == "store")
                    {
                        //選取分店顯示
                        if (GetStoreByGuid != null)
                        {
                            GetStoreByGuid(sender, new DataEventArgs<Guid>(SearchGuid));
                        }
                        btn_AddNewStore.Text = "更新分店";
                        lit_Address.Text = "分店";
                    }
                }
            }

            if (Config.EnableOpenStreetMap)
            {
                ljs.Text = @"<script type=""text/javascript"" src=""" + ResolveUrl("~/Tools/js/osm/OpenLayers.js") + @"""></script>";
            }
            else
            {
                if (Config.EnableGoogleMapApiV2)
                {
                    ljs.Text = @"<script src=""https://maps.google.com/maps?file=api&v=2&key=" + LocationFacade.GetGoogleMapsAPIKey() + @""" type=""text/javascript""></script>";
                }
                else
                {
                    ljs.Text = @"<script src=""https://maps.googleapis.com/maps/api/js?v=3&sensor=false&libraries=drawing"" type=""text/javascript""></script>";
                }
            }
        }

        /// <summary>
        /// 搜尋賣家
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SearchSeller(object sender, EventArgs e)
        {
            SellerGuid = StoreGuid = Guid.Empty;
            SellerId = string.Empty;
            ShowPanel(pan_SearchSeller.ID);
            //是否有搜尋條件
            if (SearchSellerInfo != null)
            {
                gridPager.ResolvePagerView(1, true);
                SearchSellerInfo(sender, e);
            }
        }

        /// <summary>
        /// 儲存賣家資料
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SaveSeller(object sender, EventArgs e)
        {
            if (SaveTempSeller != null)
            {
                bool check = true;
                string message = string.Empty;

                if (SellerGuid != Guid.Empty)
                {
                    Seller seller_original = VourcherFacade.SellerGetByGuid(SellerGuid);
                    if (!(seller_original.NewCreated && seller_original.CreateId == UserName))
                    {
                        check = false;
                        message += "僅可新增賣家。";
                    }
                }

                Seller seller = GetTempSellerByInput();
                PhotoInfo photo_1 = new PhotoInfo();
                PhotoInfo photo_2 = new PhotoInfo();
                if (check)
                {
                    if (!String.IsNullOrEmpty(fu_LogoImg_1.PostedFile.FileName))
                    {
                        if (fu_LogoImg_1.PostedFile.ContentLength > 3698688)
                        {
                            check = false;
                            message += "Logo圖檔過大。";
                        }
                        else
                        {
                            string tempFile = Path.GetTempFileName();
                            HttpPostedFile pFile = fu_LogoImg_1.PostedFile;
                            string destFileName = "logo-" + DateTime.Now.ToString("yyMMddHHmmss");
                            if (pFile.ContentType == "image/pjpeg" || pFile.ContentType == "image/jpeg" || pFile.ContentType == "image/x-png" || pFile.ContentType == "image/gif")
                            {
                                photo_1.PFile = pFile;
                                photo_1.Type = UploadFileType.SellerPhoto;
                                photo_1.DestFileName = destFileName;
                            }
                        }
                    }

                    if (!String.IsNullOrEmpty(fu_LogoImg_2.PostedFile.FileName))
                    {
                        if (fu_LogoImg_2.PostedFile.ContentLength > 3698688)
                        {
                            check = false;
                            message += "店家照圖檔過大。";
                        }
                        else
                        {
                            string tempFile = Path.GetTempFileName();
                            HttpPostedFile pFile = fu_LogoImg_2.PostedFile;
                            string destFileName = "logo-" + DateTime.Now.AddSeconds(3).ToString("yyMMddHHmmss");
                            if (pFile.ContentType == "image/pjpeg" || pFile.ContentType == "image/jpeg" || pFile.ContentType == "image/x-png" || pFile.ContentType == "image/gif")
                            {
                                photo_2.PFile = pFile;
                                photo_2.Type = UploadFileType.SellerPhoto;
                                photo_2.DestFileName = destFileName;
                            }
                        }
                    }
                }

                if (check)
                {
                    SaveTempSeller(sender, new DataEventArgs<KeyValuePair<Seller, List<PhotoInfo>>>(new KeyValuePair<Seller, List<PhotoInfo>>(seller, new List<PhotoInfo>() { photo_1, photo_2 })));
                }
                else
                {
                    ShowMessage(message);
                }
            }
        }

        /// <summary>
        /// 儲存分店資料
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SaveStore(object sender, EventArgs e)
        {
            if (SaveTempStore != null)
            {
                string message = string.Empty;
                bool check = true;
                if (StoreGuid != Guid.Empty)
                {
                    Store store_original = VourcherFacade.StoreGetByGuid(StoreGuid);
                    if (!(store_original.CreateId == UserName && store_original.NewCreated))
                    {
                        check = false;
                        message = "僅可新增分店。";
                    }
                }

                if (check)
                {
                    bool reliable_coordinate;
                    Store store = GetTempStoreByInput(out reliable_coordinate);
                    if (reliable_coordinate)
                    {
                        SaveTempStore(sender, new DataEventArgs<Store>(store));
                    }
                    else
                    {
                        ShowMessage("請填入分店地址和經緯度");
                    }
                }
                else
                {
                    ShowMessage(message);
                }
            }
        }

        private void ShowMessage(string message)
        {
            ScriptManager.RegisterStartupScript(this, GetType(), "alert", "alert('" + message + "')", true);
        }

        /// <summary>
        /// 直接新增分店資料
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void AddTempStore(object sender, EventArgs e)
        {
            StoreGuid = Guid.Empty;
            lit_Address.Text = "分店";
            ShowPanel(pan_VourcherTempStore.ID, pan_Account.ID, btn_AddNewStore.ID);
        }

        /// <summary>
        /// 直接新增賣家資料
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void AddTempSeller(object sender, EventArgs e)
        {
            StoreGuid = SellerGuid = Guid.Empty;
            SellerId = string.Empty;
            ShowPanel(pan_VourcherTempSeller.ID, pan_Account.ID, btn_AddNewSeller.ID);
        }

        /// <summary>
        /// 取消返回至上依階段頁面
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CancelReturn(object sender, EventArgs e)
        {
            ClearText();
            //在分店資料頁轉回賣家資料頁
            if (pan_VourcherTempStore.Visible)
            {
                GetSellerByGuid(sender, new DataEventArgs<Guid>(SellerGuid));
            }
            else if (pan_VourcherTempSeller.Visible)
            {
                //賣家資料頁轉回搜尋賣家頁
                ShowPanel(pan_SearchSeller.ID);
                StoreGuid = SellerGuid = Guid.Empty;
                BankCode = BranchCode = string.Empty;
                CityId = PponCityGroup.DefaultPponCityGroup.TaipeiCity.CityId;
                btn_AddNewSeller.Text = "新增賣家";
                btn_AddNewStore.Text = "新增分店";
            }
        }

        protected void ChangeTempStatus(object sender, EventArgs e)
        {
            if (GetTempStatusSellerStore != null)
            {
                GetTempStatusSellerStore(sender, e);
            }
        }

        /// <summary>
        /// 清除輸入和顯示的字串
        /// </summary>
        private void ClearText()
        {
            foreach (var item in SellerChangeItemLiteral)
            {
                item.Value.Text = string.Empty;
            }

            foreach (var item in StoreChangeItemLiteral)
            {
                item.Value.Text = string.Empty;
            }

            foreach (var item in SellerChangeItemTextBox)
            {
                item.Value.Text = string.Empty;
            }

            foreach (var item in StoreChangeItemTextBox)
            {
                item.Value.Text = string.Empty;
            }
            txtLatitude.Text = txtLongitude.Text = lit_Seller_CityId.Text = lit_Seller_CompanyBankCode.Text = lit_Seller_CompanyBranchCode.Text = string.Empty;
        }

        /// <summary>
        /// Repeater的按鈕動作
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void TempSellerInfoItemCommand(object sender, RepeaterCommandEventArgs e)
        {
            //賣家列表
            if (e.CommandName == "EditTempSeller")
            {
                //選取單一賣家顯示資料
                Guid seller_guid;
                if (Guid.TryParse(e.CommandArgument.ToString(), out seller_guid) && GetSellerByGuid != null)
                {
                    GetSellerByGuid(sender, new DataEventArgs<Guid>(seller_guid));
                }
                btn_AddNewSeller.Text = "更新賣家";
                lit_Address.Text = "簽約";
            }
            else if (e.CommandName == "EditTempStore")
            {
                //分店列表
                Guid store_guid;
                //選取分店顯示
                if (Guid.TryParse(e.CommandArgument.ToString(), out store_guid) && GetStoreByGuid != null)
                {
                    GetStoreByGuid(sender, new DataEventArgs<Guid>(store_guid));
                }
                btn_AddNewStore.Text = "更新分店";
                lit_Address.Text = "分店";
            }
        }

        /// <summary>
        /// 搜尋賣家分頁
        /// </summary>
        /// <param name="pageNumber"></param>
        protected void UpdateHandler(int pageNumber)
        {
            DataEventArgs<int> e = new DataEventArgs<int>(pageNumber);
            if (this.PageChanged != null)
            {
                this.PageChanged(this, e);
            }
        }

        /// <summary>
        /// 搜尋賣家計算資料量
        /// </summary>
        /// <returns></returns>
        protected int RetrieveDataCount()
        {
            EventArgs e = new EventArgs();
            if (GetDataCount != null && !string.IsNullOrEmpty(SearchKeys.Value))
            {
                GetDataCount(this, e);
            }

            return PageCount;
        }

        #region local page method

        /// <summary>
        /// 顯示隱藏web control
        /// </summary>
        /// <param name="controls"></param>
        private void ShowPanel(params string[] controls)
        {
            foreach (var item in FrontPanels)
            {
                item.Value.Visible = false;
            }

            foreach (string item in controls)
            {
                FrontPanels[item].Visible = true;
            }
        }

        /// <summary>
        /// 依輸入的資料回傳Seller
        /// </summary>
        /// <returns></returns>
        private Seller GetTempSellerByInput()
        {
            Seller seller = new Seller();
            seller.SellerName = tbx_Seller_SellerName.Text;
            seller.SellerBossName = tbx_Seller_BossName.Text;
            seller.SellerMobile = tbx_Seller_Mobile.Text;
            seller.SellerTel = tbx_Seller_Tel.Text;
            seller.SellerFax = tbx_Seller_Fax.Text;
            seller.CityId = TownShipId;
            seller.SellerAddress = tbx_Seller_Address.Text;
            seller.SellerEmail = tbx_Seller_Email.Text;
            seller.CompanyName = tbx_Seller_ComapnyName.Text;
            seller.SignCompanyID = tbx_Seller_CompanySignId.Text;
            seller.CompanyBossName = tbx_Seller_CompanyBossName.Text;
            seller.SellerContactPerson = tbx_Seller_Contact_Person.Text;
            seller.SellerBlog = websiteformat(tbx_Seller_Blog.Text);
            seller.SellerDescription = tbx_Seller_Description.Text;

            #region coordinate

            string latitude = string.Empty;
            string longitude = string.Empty;
            if (txtLatitude.Text != "0" && !string.IsNullOrEmpty(txtLatitude.Text) &&
                txtLongitude.Text != "0" && !string.IsNullOrEmpty(txtLongitude.Text))
            {
                latitude = txtLatitude.Text;
                longitude = txtLongitude.Text;
            }
            else
            {
                KeyValuePair<string, string> latitude_longitude = LocationFacade.GetArea(CityManager.CityTownShopStringGet(TownShipId) + tbx_Seller_Address.Text);
                latitude = latitude_longitude.Key;
                longitude = latitude_longitude.Value;
            }
            seller.Coordinate = LocationFacade.GetGeographyWKT(latitude, longitude);

            #endregion coordinate

            seller.SellerCategory = SellerCategory;
            seller.SellerConsumptionAvg = SellerConsumptionAvg;

            return seller;
        }

        /// <summary>
        /// 輸入賣家資料和異動資料紀錄做前台顯示
        /// </summary>
        /// <param name="seller">賣家資料</param>
        /// <param name="changeitems">異動資料</param>
        private void SetTempSeller(Seller seller, string changeitems)
        {
            ClearText();
            lab_Seller_SellerId.Text = seller.SellerId;
            //將賣家資料填入對應textbox
            foreach (var item in SellerChangeItemTextBox)
            {
                if (item.Key == Seller.CoordinateColumn.ColumnName)
                {
                    if (seller.GetColumnValue(item.Key) != null)
                    {
                        SqlGeography latlong = (SqlGeography)seller.GetColumnValue(item.Key);
                        txtLatitude.Text = latlong.Lat.ToString();
                        txtLongitude.Text = latlong.Long.ToString();
                    }
                }
                else
                {
                    item.Value.Text = seller.GetColumnValue(item.Key) != null ? seller.GetColumnValue(item.Key).ToString() : string.Empty;
                }
            }
            SellerCategory = seller.SellerCategory;
            SellerConsumptionAvg = seller.SellerConsumptionAvg;
            City city = CityManager.TownShipGetById(seller.CityId);
            CityId = city != null ? city.ParentId ?? 0 : 0;
            TownShipId = seller.CityId;

            if (!string.IsNullOrEmpty(seller.SellerLogoimgPath))
            {
                img_Logo_1.ImageUrl = ImageFacade.GetMediaPathsFromRawData(seller.SellerLogoimgPath, MediaType.SellerPhotoLarge).DefaultIfEmpty(string.Empty).First();
                img_Logo_1.DataBind();
                img_Logo_2.ImageUrl = ImageFacade.GetMediaPathsFromRawData(seller.SellerLogoimgPath, MediaType.SellerPhotoLarge).Skip(1).DefaultIfEmpty(string.Empty).First();
                img_Logo_2.DataBind();
            }

            #region coorinate

            string[] s = seller.Coordinate.Split(' ');
            string latitide = "0";
            string longitude = "0";
            if (s.Length > 2)
            {
                longitude = s[1].Replace("(", "");
                latitide = s[2].Replace(")", "");
            }
            ShowMessage("GetMap(" + latitide + "," + longitude + ",'" + seller.SellerName + "');");
            #endregion coorinate

            int city_id;
            //如有異動資料紀錄
            if (!string.IsNullOrEmpty(changeitems))
            {
                JObject json = JObject.Parse(changeitems);
                //將原資料填入對應literal，異動資料填入相對應textbox
                foreach (var item in SellerChangeItemTextBox)
                {
                    if (!string.IsNullOrEmpty(CheckJTokenEmpty(json[item.Key])))
                    {
                        SellerChangeItemLiteral[item.Key].Text = string.IsNullOrEmpty(item.Value.Text) ? "空字串" : item.Value.Text;
                        item.Value.Text = CheckJTokenEmpty(json[item.Key]);
                    }
                }
                //對無法直接變更的欄位做處理(城市區域、銀行分行代碼)
                string city_id_jtoken = CheckJTokenEmpty(json[Seller.CityIdColumn.ColumnName]);
                if (int.TryParse(city_id_jtoken, out city_id) && CityManager.TownShipGetById(city_id) != null)
                {
                    lit_Seller_CityId.Text = CityManager.CityGetById(CityManager.TownShipGetById(seller.CityId).ParentId ?? 0).CityName + CityManager.TownShipGetById(seller.CityId).CityName;
                    CityId = CityManager.TownShipGetById(city_id).ParentId ?? 0;
                    TownShipId = city_id;
                }
                string seller_category_jtoken = CheckJTokenEmpty(json[Seller.SellerCategoryColumn.ColumnName]);
                SellerSampleCategory seller_category;
                if (!string.IsNullOrEmpty(seller_category_jtoken) && Enum.TryParse<SellerSampleCategory>(seller_category_jtoken, out seller_category))
                {
                    if (seller.SellerCategory != null)
                    {
                        lit_Seller_Category.Text = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, (SellerSampleCategory)seller.SellerCategory.Value);
                    }
                    else
                    {
                        lit_Seller_Category.Text = "未選擇";
                    }

                    SellerCategory = (int)seller_category;
                }
                string seller_consumptionavg_jtoken = CheckJTokenEmpty(json[Seller.SellerConsumptionAvgColumn.ColumnName]);
                SellerConsumptionAvg seller_consumption;
                if (!string.IsNullOrEmpty(seller_consumptionavg_jtoken) && Enum.TryParse<SellerConsumptionAvg>(seller_consumptionavg_jtoken, out seller_consumption))
                {
                    if (seller.SellerConsumptionAvg != null)
                    {
                        lit_Seller_ConsumptionAvg.Text = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, (SellerConsumptionAvg)seller.SellerConsumptionAvg.Value);
                    }
                    else
                    {
                        lit_Seller_ConsumptionAvg.Text = "未選擇";
                    }

                    SellerConsumptionAvg = (int)seller_consumption;
                }
            }
        }

        /// <summary>
        /// 依輸入傳回分店資料
        /// </summary>
        /// <returns></returns>
        private Store GetTempStoreByInput(out bool reliable_coordinate)
        {
            Store store = new Store();
            store.StoreName = tbx_Store_StoreName.Text;
            store.CompanyBossName = tbx_Seller_CompanyBossName.Text;
            store.Phone = tbx_Store_Phone.Text;
            store.TownshipId = TownShipId;
            store.CityId = CityId;
            store.AddressString = tbx_Seller_Address.Text;
            store.OpenTime = HttpUtility.HtmlDecode(tbx_Store_OpenTime.Text);
            store.CloseDate = tbx_Store_CloseDate.Text;
            store.Remarks = tbx_Store_Remark.Text;
            store.Mrt = tbx_Store_MRT.Text;
            store.Car = tbx_Store_Car.Text;
            store.Bus = tbx_Store_Bus.Text;
            store.OtherVehicles = tbx_Store_OtherVehicle.Text;
            store.WebUrl = websiteformat(tbx_Store_WebUrl.Text);
            store.FacebookUrl = websiteformat(tbx_Store_FaceBookUrl.Text);
            store.PlurkUrl = websiteformat(tbx_Store_PlurkUrl.Text);
            store.BlogUrl = websiteformat(tbx_Store_BlogUrl.Text);
            store.OtherUrl = websiteformat(tbx_Store_OtherUrl.Text);
            store.CreditcardAvailable = cbx_Store_CreditCardAvailable.Checked;
            store.CompanyName = tbx_Seller_ComapnyName.Text;
            store.SignCompanyID = tbx_Seller_CompanySignId.Text;
            store.Message = tbx_Seller_Message.Text;



            #region coordinate

            string latitude = string.Empty;
            string longitude = string.Empty;
            if (txtLatitude.Text != "0" && !string.IsNullOrEmpty(txtLatitude.Text) &&
                txtLongitude.Text != "0" && !string.IsNullOrEmpty(txtLongitude.Text))
            {
                latitude = txtLatitude.Text;
                longitude = txtLongitude.Text;
            }
            else
            {
                KeyValuePair<string, string> latitude_longitude = LocationFacade.GetArea(CityManager.CityTownShopStringGet(TownShipId) + tbx_Seller_Address.Text);
                latitude = latitude_longitude.Key;
                longitude = latitude_longitude.Value;
            }
            double check_double;
            if (double.TryParse(latitude, out check_double) && double.TryParse(longitude, out check_double))
            {
                store.Coordinate = LocationFacade.GetGeographyWKT(latitude, longitude);
                reliable_coordinate = true;
            }
            else
            {
                reliable_coordinate = false;
            }
            #endregion coordinate

            return store;
        }

        private string websiteformat(string input)
        {
            string trim_input = input.Trim();
            if (trim_input.StartsWith("www"))
            {
                return "http://" + trim_input;
            }
            else
            {
                return trim_input;
            }
        }

        /// <summary>
        /// 傳入分店資料和異動資料紀錄
        /// </summary>
        /// <param name="store">分店資料</param>
        /// <param name="changeitems">異動紀錄</param>
        private void SetTempStore(Store store, string changeitems)
        {
            ClearText();

            //將分店資料傳入對應的textbox
            foreach (var item in StoreChangeItemTextBox)
            {
                if (item.Key == Store.CoordinateColumn.ColumnName)
                {
                    if (store.GetColumnValue(item.Key) != null)
                    {
                        SqlGeography latlong = (SqlGeography)store.GetColumnValue(item.Key);
                        txtLatitude.Text = latlong.Lat.ToString();
                        txtLongitude.Text = latlong.Long.ToString();
                    }
                }
                else
                {
                    item.Value.Text = store.GetColumnValue(item.Key) != null ? store.GetColumnValue(item.Key).ToString() : string.Empty;
                }
            }
            if (store.TownshipId == null)
            {
                lit_Seller_CityId.Text = "空字串";
            }

            City city = CityManager.TownShipGetById(store.TownshipId ?? 0);
            CityId = city != null ? city.ParentId ?? 0 : 0;
            TownShipId = store.TownshipId ?? 0;
            cbx_Store_CreditCardAvailable.Checked = store.CreditcardAvailable;

            #region coorinate

            string[] s = store.Coordinate.Split(' ');
            string latitide = "0";
            string longitude = "0";
            if (s.Length > 2)
            {
                longitude = s[1].Replace("(", "");
                latitide = s[2].Replace(")", "");
            }
            ShowMessage("GetMap(" + latitide + "," + longitude + ",'" + store.StoreName + "');");
            #endregion coorinate

            int city_id;
            bool creditcard_available;
            //是否有異動資料
            if (!string.IsNullOrEmpty(changeitems))
            {
                JObject json = JObject.Parse(changeitems);
                //將異動資料填入textbox，原資料填入literal
                foreach (var item in StoreChangeItemTextBox)
                {
                    if (!string.IsNullOrEmpty(CheckJTokenEmpty(json[item.Key])))
                    {
                        StoreChangeItemLiteral[item.Key].Text = string.IsNullOrEmpty(item.Value.Text) ? "空字串" : item.Value.Text;
                        item.Value.Text = CheckJTokenEmpty(json[item.Key]);
                    }
                }

                //無法枝節處理的欄位(城市區域、銀行分行代碼和刷卡服務)
                string city_id_jtoken = CheckJTokenEmpty(json[Store.TownshipIdColumn.ColumnName]);
                if (int.TryParse(city_id_jtoken, out city_id) && CityManager.TownShipGetById(city_id) != null)
                {
                    if (store.TownshipId != null)
                    {
                        lit_Seller_CityId.Text = CityManager.CityGetById(CityManager.TownShipGetById(store.TownshipId ?? 0).ParentId ?? 0).CityName + CityManager.TownShipGetById(store.TownshipId ?? 0).CityName;
                    }
                    else
                    {
                        lit_Seller_CityId.Text = "空字串";
                    }

                    CityId = CityManager.TownShipGetById(city_id).ParentId ?? 0;
                    TownShipId = city_id;
                }
                string creditcard_available_string = CheckJTokenEmpty(json[Store.CreditcardAvailableColumn.ColumnName]);
                if (!string.IsNullOrEmpty(creditcard_available_string))
                {
                    lit_Store_CreditCardAvailable.Text = store.CreditcardAvailable.ToString();
                    cbx_Store_CreditCardAvailable.Checked = bool.TryParse(creditcard_available_string, out creditcard_available) ? creditcard_available : false;
                }
            }
            tbx_Store_OpenTime.Text = HttpUtility.HtmlEncode(tbx_Store_OpenTime.Text);
        }

        /// <summary>
        /// 判斷json的欄位是否為null，回傳空字串或其值
        /// </summary>
        /// <param name="jtoken"></param>
        /// <returns></returns>
        private string CheckJTokenEmpty(JToken jtoken)
        {
            if (jtoken != null)
            {
                return jtoken.ToString().Replace("\"", string.Empty);
            }
            else
            {
                return string.Empty;
            }
        }

        #endregion local page method

        #endregion page

        #region view method

        /// <summary>
        /// 設定賣家和分店資料，及賣家的異動資料
        /// </summary>
        /// <param name="seller">賣家資料</param>
        /// <param name="stores">分店資料</param>
        /// <param name="changeitems">賣家異動資料</param>
        public void SetSellerStores(Seller seller, StoreCollection stores, ChangeLogCollection changelogs)
        {
            lab_SellerStatus.Text = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, (SellerTempStatus)seller.TempStatus);
            rpt_TempStores.DataSource = stores;
            rpt_TempStores.DataBind();
            rpt_ChangeLog.DataSource = changelogs;
            rpt_ChangeLog.DataBind();
            SetTempSeller(seller, changelogs.Where(x => x.Enabled).OrderByDescending(x => x.Id).Select(x => x.Content).DefaultIfEmpty(string.Empty).First());
            btn_AddNewSeller.Text = "更新賣家";
            lit_Address.Text = "簽約";
            hyp_AddVourcher.NavigateUrl = "~/Vourcher/vourcher.aspx?mode=newadd&sid=" + seller.SellerId;
            ShowPanel(pan_Account.ID, pan_VourcherTempSeller.ID, pan_VourcherTempSeller_Stores.ID, btn_AddNewSeller.ID, pan_ChangeLog.ID, hyp_AddVourcher.ID);
        }

        /// <summary>
        /// 設定搜尋賣家資料
        /// </summary>
        /// <param name="sellers">賣家列表</param>
        public void SetSellerCollection(SellerCollection sellers)
        {
            rpt_TempSellers.DataSource = sellers;
            rpt_TempSellers.DataBind();
        }

        /// <summary>
        /// 設定分店和其異動資料
        /// </summary>
        /// <param name="store">分店資料</param>
        /// <param name="changeitems">分店異動資料</param>
        public void SetStore(Store store, ChangeLogCollection changelogs)
        {
            lab_StoreStatus.Text = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, (SellerTempStatus)store.TempStatus);
            rpt_ChangeLog.DataSource = changelogs;
            rpt_ChangeLog.DataBind();
            SetTempStore(store, changelogs.Where(x => x.Enabled).OrderByDescending(x => x.Id).Select(x => x.Content).DefaultIfEmpty(string.Empty).First());
            lit_Address.Text = "分店";
            ShowPanel(pan_Account.ID, pan_VourcherTempStore.ID, btn_AddNewStore.ID, pan_ChangeLog.ID);
        }

        /// <summary>
        /// 回傳選定狀態賣家和分店資料
        /// </summary>
        /// <param name="temp_seller">選定狀態的賣家資料</param>
        /// <param name="temp_stores">選定狀態的分店資料</param>
        public void GetReturnCase(SellerCollection temp_seller, StoreCollection temp_stores)
        {
            List<ReturnCase> returncases = new List<ReturnCase>();
            foreach (var item in temp_seller)
            {
                returncases.Add(new ReturnCase(item.Guid, item.SellerName, item.SignCompanyID, item.CompanyBossName, item.CompanyName, item.TempStatus, item.Message, item.ReturnTime, item.ApplyTime, ReturnCaseType.Seller));
            }
            foreach (var item in temp_stores)
            {
                returncases.Add(new ReturnCase(item.Guid, item.StoreName, item.SignCompanyID, item.CompanyBossName, item.CompanyName, item.TempStatus, item.Message, item.ReturnTime, item.ApplyTime, ReturnCaseType.Store));
            }
            rpt_ReturnCase.DataSource = returncases;
            rpt_ReturnCase.DataBind();
        }

        /// <summary>
        /// 顯示重複的賣家
        /// </summary>
        /// <param name="repeat_sellers">重複的賣家</param>
        public void ShowRepeaterSeller(SellerCollection repeat_sellers)
        {
            string sellerids = repeat_sellers.Select(x => x.SellerId).Aggregate((current, next) => current + "," + next);
            ShowMessage("存檔失敗賣家重複: " + sellerids);
        }

        #endregion view method

        #region webmethod

        /// <summary>
        /// 依銀行Id回傳分銀資料
        /// </summary>
        /// <param name="id">銀行Id</param>
        /// <returns></returns>
        [WebMethod]
        public static BankInfoCollection BankInfoGetBranchList(string id)
        {
            int bankcode;
            if (int.TryParse(id, out bankcode))
            {
                return VourcherFacade.BankInfoGetBranchList(id);
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        ///回傳銀行資料
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public static BankInfoCollection BankInfoGetMainList()
        {
            return VourcherFacade.BankInfoGetMainList();
        }

        /// <summary>
        /// 回傳城市資料
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public static CityCollection CityGetList()
        {
            return VourcherFacade.CityGetList();
        }

        /// <summary>
        /// 依城市Id回傳區域資料
        /// </summary>
        /// <param name="city_id">城市Id</param>
        /// <returns></returns>
        [WebMethod]
        public static List<City> TownShipGetListByCityId(string city_id)
        {
            int cityid;
            if (int.TryParse(city_id, out cityid))
            {
                return VourcherFacade.TownShipGetListByCityId(cityid);
            }
            return null;
        }

        /// <summary>
        /// 驗證統編或身分證
        /// </summary>
        /// <param name="id">統編或身分證</param>
        /// <returns></returns>
        [WebMethod]
        public static bool CheckCompanyUniqueId(string id)
        {
            bool is_valid = false;
            string companyno_message = RegExRules.CompanyNoCheck(id);
            string personid_message = RegExRules.PersonalIdCheck(id);
            if (string.IsNullOrEmpty(companyno_message) || string.IsNullOrEmpty(personid_message))
            {
                is_valid = true;
            }
            return is_valid;
        }

        /// <summary>
        /// 驗證email
        /// </summary>
        /// <param name="email">email</param>
        /// <returns></returns>
        [WebMethod]
        public static bool CheckEmail(string email)
        {
            return RegExRules.CheckEmail(email);
        }

        [WebMethod]
        public static KeyValuePair<string, string> CalculateCoordinates(int townshipId, string address)
        {
            return LocationFacade.GetArea(CityManager.CityTownShopStringGet(townshipId) + address);
        }

        #endregion webmethod
    }

    public enum ReturnCaseType
    {
        Seller,
        Store
    }

    public class ReturnCase
    {
        public Guid KeyGuid { get; set; }

        public Guid SubKeyGuid { get; set; }

        public string Name { get; set; }

        public string SignComapnyId { get; set; }

        public string CompanyBossName { get; set; }

        public string CompanyName { get; set; }

        public int Status { get; set; }

        public string Message { get; set; }

        public DateTime? ReturnTime { get; set; }

        public DateTime? ApplyTime { get; set; }

        public ReturnCaseType Type { get; set; }

        public ReturnCase(Guid key, string name, string signcomapnyid, string companybossname, string companyname,
            int status, string message, DateTime? returntime, DateTime? applytime, ReturnCaseType type)
        {
            KeyGuid = key;
            Name = name;
            SignComapnyId = signcomapnyid;
            CompanyBossName = companybossname;
            CompanyName = companyname;
            Status = status;
            Message = message;
            ReturnTime = returntime;
            ApplyTime = applytime;
            Type = type;
        }

        public ReturnCase(Guid key, Guid subkey, string name, string signcomapnyid, string companybossname, string companyname,
           int status, string message, DateTime? returntime, DateTime? applytime, ReturnCaseType type)
            : this(key, name, signcomapnyid, companybossname, companyname,
                status, message, returntime, applytime, type)
        {
            SubKeyGuid = subkey;
        }
    }
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Ppon/Ppon.Master" AutoEventWireup="true" CodeBehind="wechat_coupon.aspx.cs" Inherits="LunchKingSite.Web.Vourcher.wechat_coupon" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <meta name="viewport" content="width=device-width initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <link rel="stylesheet" type="text/css" href="<%=ResolveUrl("~/Themes/default/images/17Life/G2/A3-event_coupons.css")%>" />
    <link href="<%=ResolveUrl("~/themes/PCweb/css/RDL-L.css")%>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/themes/PCweb/css/RDL-M.css")%>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/themes/PCweb/css/RDL-S.css")%>" rel="stylesheet" type="text/css" />

    <script type='text/javascript'>
        function vourchercollect(btn, id) {
            var user='<%=UserName%>';
            if(user=='')
            {
                if (confirm("請先登入會員")) {
                    location.href = '<%=ResolveUrl(FormsAuthentication.LoginUrl) %>';
                }
            }
            else{
                if ($(btn).val() == '已收藏') {
                    return false;
                }
                else
                {
                    $.ajax({
                        type: "POST",
                        url: "wechat_coupon.aspx/SetVourcherCollect",
                        data: "{'eventId': '" + id + "','username':'"+user+"' }",
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        success: function (msg) {
                            $(btn).css({'cursor':'default','background-color':'gray'}).val('已收藏');
                            alert("收藏成功！您可在有效期間內依使用規則享有此優惠。\n\n請先下載 17Life App，登入即可在「我的」→「優惠券收藏」內開啟此優惠券，於消費時出示即可使用");
                        },
                        error: function (msg) {
                            alert(msg);
                            if (confirm("請先登入會員")) {
                                location.href = '<%=ResolveUrl(FormsAuthentication.LoginUrl) %>';
                            }
                        }
                    });
                }
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MC" runat="server">
    <asp:Panel ID="pan_Vourcher" runat="server">
        <div class="evn-copcafarm eve-center clearfix">
            <a>
                <asp:Image ID="img_Pic" runat="server" /></a>
            <div class="evn-copcadealname">
                <asp:Literal ID="lit_SellerName" runat="server"></asp:Literal>
            </div>
            <div class="evn-coptextinfo">
                <p class="evn-cop-ptext">
                    <asp:Literal ID="lit_Content" runat="server"></asp:Literal>
                </p>
                <asp:Panel ID="pan_Expired" runat="server" Visible="false">
                    <span class="st-evncopca-col-btn" style="background-color: gray">已過期
                    </span>
                </asp:Panel>
                <asp:Panel ID="pan_Available" runat="server">
                    <input type="button" style='<%=AlreadyCollected?"background-color:gray;": "background-color:#D00000;cursor:pointer;" %>'
                        class="st-evncopca-col-btn"
                        value='<%=AlreadyCollected?"已收藏" : "收藏" %>'
                        onclick="return vourchercollect(this,<%=VourcherEventId %>);">
                </asp:Panel>
            </div>
        </div>
        <div class="mc-content rd-mg10">
            <h1 class="rd-smll">優惠卷使用說明</h1>
            <hr class="header_hr">
            <p>請先下載17Life App，登入即可在「我的」→「優惠卷收藏」內開啟此優惠卷，於消費時出示即可使用。</p>
            <a href="../ppon/promo.aspx?cn=app" target="_blank">下載17Life App</a>
            <div class="clearfix"></div>
        </div>
    </asp:Panel>
    <asp:Panel ID="pan_Fail" runat="server" Visible="false">
        <div class="evn-copcafarm eve-center clearfix">
            <a>
                <img src="../Themes/PCweb/images/ppon-M1-AppQR-Co.png" alt="" /></a>
            <div class="evn-copcadealname">
                活動已結束
            </div>
            <div class="evn-coptextinfo">
                <p class="evn-cop-ptext">
                    下載17Life ，隨時隨地買好康
                </p>
                <a href="../ppon/promo.aspx?cn=app" class="st-evncopca-col-btn" style="color: white">下載17Life App</a>
            </div>
        </div>
        <div class="mc-content rd-mg10">
            <h1 class="rd-smll">優惠卷使用說明</h1>
            <hr class="header_hr">
            <p>請先下載17Life App，登入即可在「我的」→「優惠卷收藏」內開啟此優惠卷，於消費時出示即可使用。</p>
        </div>
    </asp:Panel>
</asp:Content>

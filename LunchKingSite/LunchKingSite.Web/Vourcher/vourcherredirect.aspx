﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="vourcherredirect.aspx.cs" Inherits="LunchKingSite.Web.Vourcher.vourcherredirect" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <script type="text/javascript" src="../Tools/js/jquery-1.6.2.min.js"></script>
    <script type="text/javascript" language="javascript">
        function openAtAPP(d, url) {
            if (d == "ios") {
                var start = new Date();
                window.setTimeout(function () {
                    if (new Date() - start > 2000) {
                        return;
                    }
                    document.location = "https://itunes.apple.com/tw/app/17life/id543439591";
                }, 1000);
                document.location = "open17life://www.17life.com?vourcherid=<%=VourcherId%>";
            }
            else if (d == "android") {
                document.location = "<%=WebRoot%>/ppon/OpenApp.aspx?vourcherid=<%=VourcherId%>";
            }


        window.setTimeout(function () {
            document.location = url;
        }, 1500);
    }

    function appPromo(url) {
        // 行動裝置(不含iPad)出現建議下載app
        var device = '';
        if (navigator.userAgent.match(<%= AndroidUserAgent%>)) {
            device = 'android';
        } else if (navigator.userAgent.match(<%= iOSUserAgent%>)) {
                device = 'ios';
        }

        if (device != '') {
            if (confirm("是否於17Life APP開啟連結。")) {
                //嘗試開始APP
                openAtAPP(device, url);
            } else {
                document.location = url;
            }
        } else {
            document.location = url;
        }
    };

    $(document).ready(function () {
        appPromo("http://yahoo.17life.com/coupon/<%=VourcherId%>");
    });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div>
        </div>
    </form>
</body>
</html>

﻿using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LunchKingSite.Web.Vourcher
{
    public partial class wechat_coupon : System.Web.UI.Page
    {
        public int VourcherEventId
        {
            get
            {
                int event_id;
                if (!string.IsNullOrEmpty(Request.QueryString["eid"]) && int.TryParse(Request.QueryString["eid"], out event_id))
                {
                    return event_id;
                }
                else
                {
                    return -1;
                }
            }
        }
        public bool AlreadyCollected { get; set; }
        public string UserName
        {
            get
            {
                return User.Identity.Name;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!VourcherEventId.Equals(-1))
                {
                    ViewVourcherSeller vvs = VourcherFacade.ViewVourcherSellerGetEnabledById(VourcherEventId);
                    if (vvs != null && vvs.EndDate.HasValue)
                    {
                        lit_SellerName.Text = vvs.SellerName;
                        lit_Content.Text = vvs.Contents;
                        img_Pic.ImageUrl = ImageFacade.GetMediaPathsFromRawData(vvs.PicUrl, MediaType.PponDealPhoto).DefaultIfEmpty("../Themes/PCweb/images/ppon-M1_pic.jpg").First();
                        if (!string.IsNullOrEmpty(UserName))
                        {
                            int userId = MemberFacade.GetUniqueId(UserName);
                            if (userId != 0)
                            {
                                VourcherCollect vc = VourcherFacade.GetVourcherCollect(userId, VourcherEventId);
                                AlreadyCollected = vc.IsLoaded;
                            }
                        }
                        if (vvs.EndDate.Value < DateTime.Now && !AlreadyCollected)
                        {
                            pan_Expired.Visible = true;
                            pan_Available.Visible = false;
                        }
                    }
                    else
                    {
                        pan_Fail.Visible = true;
                        pan_Vourcher.Visible = false;
                    }
                }
                else
                {
                    pan_Fail.Visible = true;
                    pan_Vourcher.Visible = false;
                }
            }
        }
        [WebMethod]
        public static void SetVourcherCollect(int eventId, string username)
        {
            int userId = MemberFacade.GetUniqueId(username);
            if (userId != 0)
            {
                VourcherFacade.SetVourcherCollect(userId, eventId, VourcherCollectStatus.Initial);
            }
        }
    }
}
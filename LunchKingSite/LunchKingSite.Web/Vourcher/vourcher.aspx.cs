﻿using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI.WebControls;

namespace LunchKingSite.Web.Vourcher
{
    public partial class vourcher : RolePage, IVourcherEventView
    {
        #region property

        private VourcherEventPresenter _presenter;

        public VourcherEventPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        /// <summary>
        /// 使用者名稱
        /// </summary>
        public string UserName
        {
            get
            {
                return User.Identity.Name;
            }
        }

        /// <summary>
        /// 分頁的總資料量大小
        /// </summary>
        public int PageCount
        {
            get
            {
                int pagecount;
                return int.TryParse((ViewState["pagecount"] == null ? string.Empty : ViewState["pagecount"].ToString()), out pagecount) ? pagecount : 0;
            }
            set
            {
                ViewState["pagecount"] = value;
            }
        }

        public int PageReturnCaseCount
        {
            get
            {
                int pagecount;
                return int.TryParse((ViewState["pagereturncasecount"] == null ? string.Empty : ViewState["pagereturncasecount"].ToString()), out pagecount) ? pagecount : 0;
            }
            set
            {
                ViewState["pagereturncasecount"] = value;
            }
        }

        /// <summary>
        /// 分頁的每頁大小
        /// </summary>
        public int PageSize
        {
            get
            {
                return gridPager.PageSize;
            }
        }

        public int PageReturnCaseSize
        {
            get
            {
                return 1;
            }
        }

        /// <summary>
        /// 搜尋優惠的欄位和輸入資料
        /// </summary>
        public KeyValuePair<string, string> SearchKeys
        {
            get
            {
                KeyValuePair<string, string> search_keys = new KeyValuePair<string, string>(ddl_Search.SelectedValue, tbx_Search.Text.Trim());
                return search_keys;
            }
        }

        /// <summary>
        /// 優惠券活動Id
        /// </summary>
        public int VourcherEventId
        {
            get
            {
                int vourcherevent_id;
                if (int.TryParse(hif_VourcherEventId.Value, out vourcherevent_id))
                {
                    return vourcherevent_id;
                }
                else
                {
                    return 0;
                }
            }
            set
            {
                hif_VourcherEventId.Value = value.ToString();
                if (value != 0)
                {
                    lab_EventID.Text = value.ToString();
                }
                else
                {
                    lab_EventID.Text = "新優惠券";
                }
            }
        }

        /// <summary>
        /// 賣家Guid
        /// </summary>
        public Guid SellerGuid
        {
            get
            {
                Guid seller_guid;
                if (Guid.TryParse(hif_SellerGuid.Value, out seller_guid))
                {
                    return seller_guid;
                }
                else
                {
                    return Guid.Empty;
                }
            }
            set
            {
                hif_SellerGuid.Value = value.ToString();
            }
        }

        /// <summary>
        /// 賣家ID
        /// </summary>
        public string SellerId
        {
            get
            {
                return hif_SellerId.Value;
            }
            set
            {
                hif_SellerId.Value = value;
            }
        }

        /// <summary>
        /// 使否具編審身分(後台為true)
        /// </summary>
        public bool IsAdmin
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// 另開新頁新增優惠券 newadd
        /// 查詢賣家優惠券 search
        /// </summary>
        public string Mode
        {
            get
            {
                if (Request["mode"] != null)
                {
                    return Request["mode"].ToString();
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        /// <summary>
        /// 外部導向搜尋賣家優惠券
        /// </summary>
        public string SearchSellerId
        {
            get
            {
                if (Request["sid"] != null)
                {
                    return Request["sid"].ToString();
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        /// <summary>
        /// 優惠券類別
        /// </summary>
        public VourcherEventType EventType
        {
            get
            {
                VourcherEventType type;
                if (Enum.TryParse<VourcherEventType>(ddl_Vourcher_Type.SelectedValue, out type))
                {
                    return type;
                }
                else
                {
                    return VourcherEventType.DirectDiscount;
                }
            }
            set
            {
                ddl_Vourcher_Type.SelectedValue = ((int)value).ToString();
            }
        }

        /// <summary>
        /// 優惠券使用數量限制
        /// </summary>
        public VourcherEventMode EventMode
        {
            get
            {
                VourcherEventMode mode;
                if (Enum.TryParse<VourcherEventMode>(ddl_Vourcher_Mode.SelectedValue, out mode))
                {
                    return mode;
                }
                else
                {
                    return VourcherEventMode.None;
                }
            }
            set
            {
                ddl_Vourcher_Mode.SelectedValue = ((int)value).ToString();
            }
        }

        /// <summary>
        /// 優惠券狀態
        /// </summary>
        public VourcherEventStatus EventStatus
        {
            get
            {
                VourcherEventStatus event_status;
                if (Enum.TryParse<VourcherEventStatus>(ddl_EventStatus.SelectedValue, out event_status))
                {
                    return event_status;
                }
                else
                {
                    return VourcherEventStatus.ApplyEvent;
                }
            }
        }

        private string _salesEmailArray;
        public string SalesEmailArray
        {
            get { return _salesEmailArray; }
            set { _salesEmailArray = value; }
        }

        #region

        /// <summary>
        /// 優惠券類型輸入區塊
        /// </summary>
        public Dictionary<VourcherEventType, Panel> TypePanels
        {
            get
            {
                Dictionary<VourcherEventType, Panel> panels = new Dictionary<VourcherEventType, Panel>();
                panels[VourcherEventType.DirectDiscount] = pan_DirectDiscount;
                panels[VourcherEventType.DirectOffset] = pan_DirectOffset;
                panels[VourcherEventType.DiscountSecondOne] = pan_DiscountSecondOne;
                panels[VourcherEventType.ParticularProduct] = pan_ParticularProduct;
                panels[VourcherEventType.Upgrade] = pan_Upgrade;
                panels[VourcherEventType.CustomersGetOneFree] = pan_CustomersGetOneFree;
                panels[VourcherEventType.ConsumptionGetOnFree] = pan_ConsumptionGetOnFree;
                panels[VourcherEventType.BirthDay] = pan_BirthDay;
                panels[VourcherEventType.Other] = pan_Other;
                panels[VourcherEventType.Lottery] = pan_Lottery;
                panels[VourcherEventType.Gift] = pan_Gift;
                return panels;
            }
        }

        /// <summary>
        /// 圖片區塊
        /// </summary>
        public List<Image> PicImages
        {
            get
            {
                return new List<Image>() { img_VourcherEvent_1, img_VourcherEvent_2, img_VourcherEvent_3, img_VourcherEvent_4, img_VourcherEvent_5, img_VourcherEvent_6 };
            }
        }

        public List<FileUpload> PicFileUploads
        {
            get
            {
                return new List<FileUpload>() { fu_Vourcher_Img_1, fu_Vourcher_Img_2, fu_Vourcher_Img_3, fu_Vourcher_Img_4, fu_Vourcher_Img_5, fu_Vourcher_Img_6 };
            }
        }

        public List<CheckBox> PicDeleteImgs
        {
            get
            {
                return new List<CheckBox>() { cbx_Delete_Img_1, cbx_Delete_Img_2, cbx_Delete_Img_3, cbx_Delete_Img_4, cbx_Delete_Img_5, cbx_Delete_Img_6 };
            }
        }

        public Dictionary<int, int[]> SelectedCommercialCategoryId
        {
            get
            {
                Dictionary<int, int[]> selected = new Dictionary<int, int[]>();
                for (int i = 0; i < rptCommercialCategory.Items.Count; i++)
                {
                    HiddenField hidCommercialCategoryId = (HiddenField)rptCommercialCategory.Items[i].FindControl("hidCommercialCategoryId");
                    CheckBoxList chklCommercialCategory = (CheckBoxList)rptCommercialCategory.Items[i].FindControl("chklCommercialCategory");
                    int cid = int.TryParse(hidCommercialCategoryId.Value, out cid) ? cid : 0;
                    int[] categorys = chklCommercialCategory.Items.Cast<ListItem>().Where(item => item.Selected).Select(item => Convert.ToInt32(item.Value)).ToArray();
                    if (!cid.Equals(0) && categorys.Length > 0)
                        selected.Add(cid, categorys);
                }
                return selected;
            }
            set
            {
                if (value.Count > 0)
                {
                    for (int i = 0; i < rptCommercialCategory.Items.Count; i++)
                    {
                        HiddenField hidCommercialCategoryId = (HiddenField)rptCommercialCategory.Items[i].FindControl("hidCommercialCategoryId");
                        CheckBoxList chklCommercialCategory = (CheckBoxList)rptCommercialCategory.Items[i].FindControl("chklCommercialCategory");
                        int cid = int.TryParse(hidCommercialCategoryId.Value, out cid) ? cid : 0;
                        if (value.ContainsKey(cid))
                        {
                            for (int j = 0; j < chklCommercialCategory.Items.Count; j++)
                            {
                                if (value[cid] != null)
                                    chklCommercialCategory.Items[j].Selected = value[cid].Contains(Convert.ToInt32(chklCommercialCategory.Items[j].Value));
                            }
                        }
                    }
                }
            }
        }

        public List<int> SelectedCategory
        {
            get
            {
                return cbx_Category.Items.Cast<ListItem>().Where(x => x.Selected).Select(x => int.Parse(x.Value)).ToList();
            }
            set
            {
                foreach (var item in cbx_Category.Items.Cast<ListItem>())
                {
                    item.Selected = value.Any(x => x.ToString() == item.Value);
                }
            }
        }

        #endregion property

        #endregion

        #region event

        //搜尋賣家資料分頁
        public event EventHandler<DataEventArgs<int>> PageChanged;

        public event EventHandler<DataEventArgs<int>> PageReturnCaseChanged;

        //搜尋賣家資料計算總筆數
        public event EventHandler GetDataCount;

        public event EventHandler GetReturnCaseDataCount;

        //依賣家Id搜尋賣家資料
        public event EventHandler<DataEventArgs<string>> GetSellerById;

        //儲存優惠券內容和對應分店
        public event EventHandler<DataEventArgs<VourcherEventInfo>> SaveVourcherEventStores;

        //撈取優惠券
        public event EventHandler<DataEventArgs<int>> GetVourcherEvent;

        //搜尋優惠券內容
        public event EventHandler SearchVourcherEvent;

        /// <summary>
        /// 依優惠券狀態搜尋
        /// </summary>
        public event EventHandler SearchVourcherByStatus;

        /// <summary>
        /// 退回申請
        /// </summary>
        public event EventHandler<DataEventArgs<string>> ReturnApply;

        /// <summary>
        /// 賣家審核通過
        /// </summary>
        public event EventHandler SellerPass;

        /// <summary>
        /// 分店審核通過
        /// </summary>
        public event EventHandler StorePass;

        /// <summary>
        /// 通過申請
        /// </summary>
        public event EventHandler ApproveApply;
        //隱藏或顯示活動
        public event EventHandler<DataEventArgs<bool>> EnableEvent;
        #endregion
        #region page

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!User.Identity.IsAuthenticated)
            {
                FormsAuthentication.RedirectToLoginPage();
                return;
            }

            //權限尚未建立，暫時以公司domain作判別
            if (!User.Identity.Name.ToLower().Contains("17life.com"))
            {
                return;
            }

            if (!IsPostBack)
            {
                #region 下拉選單綁定資料

                ddlContractSend.DataSource = VourcherFacade.GetVourcherContractTypeDictionary();
                ddl_Vourcher_Mode.DataSource = VourcherFacade.GetVourcherEventModeDictionary();
                ddl_Vourcher_Type.DataSource = VourcherFacade.GetVourcherEventTypeDictionary();
                ddl_Vourcher_Type.DataTextField = ddl_Vourcher_Mode.DataTextField = ddlContractSend.DataTextField = "Value";
                ddl_Vourcher_Type.DataValueField = ddl_Vourcher_Mode.DataValueField = ddlContractSend.DataValueField = "Key";
                ddlContractSend.DataBind();
                ddl_Vourcher_Mode.DataBind();
                ddl_Vourcher_Type.DataBind();

                ddl_EventStatus.Items.Add(new ListItem(Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, VourcherEventStatus.ApplyEvent), ((int)VourcherEventStatus.ApplyEvent).ToString()));
                ddl_EventStatus.Items.Add(new ListItem(Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, VourcherEventStatus.ReturnApply), ((int)VourcherEventStatus.ReturnApply).ToString()));

                ddl_Search.Items.Add(new ListItem("品牌名稱", ViewVourcherSeller.Columns.SellerName));
                ddl_Search.Items.Add(new ListItem("賣家編號", ViewVourcherSeller.Columns.SellerId));
                #endregion
                if (Page.User.IsInRole(MemberRoles.SalesAssistant.ToString()))
                {
                    ddlContractSend.Enabled = txtNoContractReason.Enabled = true;
                }
                _presenter.OnViewInitialized();
            }
            _presenter.OnViewLoaded();
            if (!IsPostBack && !string.IsNullOrEmpty(Mode) && !string.IsNullOrEmpty(SearchSellerId))
            {
                if (Mode.Equals("newadd", StringComparison.OrdinalIgnoreCase))
                {
                    tbx_SellerId.Text = SearchSellerId;
                    OpenAddNewwVourcher(sender, e);
                }
                else if (Mode.Equals("search", StringComparison.OrdinalIgnoreCase) && !string.IsNullOrEmpty(SearchSellerId))
                {
                    tbx_Search.Text = SearchSellerId;
                    ddl_Search.SelectedValue = ViewVourcherSeller.Columns.SellerId;
                    SearchVourche(sender, e);
                }
            }

            bool onsenseforwarnning = (SellerPass == null && StorePass == null);
        }

        //避免未使用的event出現在build warning
        protected void NoUseEvent()
        {
            bool check = ReturnApply != null && ApproveApply != null && EnableEvent != null;
        }

        /// <summary>
        /// 選擇優惠券撈取資料
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void VourcherEventListItemCommand(object sender, RepeaterCommandEventArgs e)
        {
            int event_id;
            if (int.TryParse(e.CommandArgument.ToString(), out event_id) && GetVourcherEvent != null)
            {
                btn_AddNewVourcherAndOn.Visible = false;
                GetVourcherEvent(sender, new DataEventArgs<int>(event_id));
            }
        }

        /// <summary>
        /// 直接新增優惠券內容(須提供seller_id)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void OpenAddNewwVourcher(object sender, EventArgs e)
        {
            VourcherEventId = 0;
            SellerGuid = Guid.Empty;
            SellerId = string.Empty;
            for (int i = 0; i < PicImages.Count; i++)
            {
                PicImages[i].Visible = false;
            }
            if (!string.IsNullOrEmpty(tbx_SellerId.Text.Trim()) && GetSellerById != null)
            {
                GetSellerById(sender, new DataEventArgs<string>(tbx_SellerId.Text.Trim()));
            }
        }

        /// <summary>
        /// 儲存優惠券
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SaveVourcherEvent(object sender, EventArgs e)
        {
            bool check_valid = true;
            string message = lab_Vourcher_SetUpMessage.Text = string.Empty;
            VourcherEvent vourcher_event = new VourcherEvent();
            VourcherStoreCollection vourcher_stores = new VourcherStoreCollection();
            int max_quantity;
            if (EventMode == VourcherEventMode.EventLimitQuantityNoUsingTimes || EventMode == VourcherEventMode.EventLimitQuantityOnlyOneTime)
            {
                if (int.TryParse(tbx_MaxQuantity.Text, out max_quantity))
                {
                    vourcher_event.MaxQuantity = max_quantity;
                }
                else
                {
                    check_valid = false;
                    message += "請填入優惠券數量。";
                }
            }

            //檢查是否有選擇分店
            for (int i = 0; i < rpt_TempStores.Items.Count; i++)
            {
                Guid store_guid;
                CheckBox cbx = (CheckBox)rpt_TempStores.Items[i].FindControl("cbx_Store");
                HiddenField hif = (HiddenField)rpt_TempStores.Items[i].FindControl("hif_StoreGuid");
                TextBox tbx = (TextBox)rpt_TempStores.Items[i].FindControl("tbx_StoreMaxQuantity");
                if (cbx.Checked && Guid.TryParse(hif.Value, out store_guid))
                {
                    VourcherStore vourcher_store = new VourcherStore();
                    vourcher_store.SellerGuid = SellerGuid;
                    vourcher_store.StoreGuid = store_guid;
                    vourcher_store.CreateId = UserName;
                    vourcher_store.CreateTime = DateTime.Now;
                    vourcher_stores.Add(vourcher_store);
                }
            }

            if (vourcher_stores.Count == 0)
            {
                check_valid = false;
                message += "請選擇分店。";
            }

            //圖片大小檢查
            if (PicFileUploads.Any(x => !String.IsNullOrEmpty(x.PostedFile.FileName) && x.PostedFile.ContentLength > 3698688))
            {
                check_valid = false;
                message += "圖片限制3Mb以下。";
            }

            if (check_valid && SaveVourcherEventStores != null)
            {
                vourcher_event.EventName = string.Empty;
                vourcher_event.SellerGuid = SellerGuid;
                vourcher_event.Type = (int)EventType;
                vourcher_event.Mode = (int)EventMode;
                int status = int.TryParse(ddlContractSend.SelectedValue, out status) ? status : 0;
                vourcher_event.ContractStatus = status;
                vourcher_event.NoContractReason = txtNoContractReason.Text;
                vourcher_event.SalesName = txtSalesName.Text;
                vourcher_event.Magazine = !cbx_Magazine.Checked;
                vourcher_event.Others = tbx_Others.Text.Trim();

                decimal discount;
                int min_consumption, discount_price, original_price, gift_quantity, min_persons;
                btn_AddNewVourcher.ValidationGroup = EventType.ToString();
                StringBuilder sb_instruction = new StringBuilder();
                StringBuilder sb_restriction = new StringBuilder();
                switch (EventType)
                {
                    case VourcherEventType.DirectDiscount:
                        vourcher_event.Discount = decimal.TryParse(tbx_DirectDiscount_Discount.Text, out discount) ? discount : 10;
                        vourcher_event.AllTable = cbx_DirectDiscount_AllTable.Checked;
                        GetNoticeText(cbx_DirectDiscount_AllTable, sb_instruction);
                        vourcher_event.OneMeal = cbx_DirectDiscount_OneMeal.Checked;
                        GetNoticeText(cbx_DirectDiscount_OneMeal, sb_instruction);
                        vourcher_event.ServiceFee = !cbx_DirectDiscount_ServiceFee.Checked;
                        GetNoticeText(cbx_DirectDiscount_ServiceFee, sb_instruction);
                        vourcher_event.ServiceFeeAdditional = cbx_DirectDiscount_ServiceFeeAdditional.Checked;
                        GetNoticeText(cbx_DirectDiscount_ServiceFeeAdditional, sb_instruction);
                        vourcher_event.ConsumptionApplied = cbx_DirectDiscount_ConsumptionApplied.Checked;
                        GetNoticeText(cbx_DirectDiscount_ConsumptionApplied, sb_instruction);
                        if (cbx_DirectDiscount_MinConsumption.Checked && int.TryParse(tbx_DirectDiscount_MinConsumption.Text, out min_consumption))
                        {
                            vourcher_event.MinConsumption = min_consumption;
                            GetNoticeText(tbx_DirectDiscount_MinConsumption, sb_instruction);
                        }
                        else
                        {
                            vourcher_event.MinConsumption = null;
                        }
                        vourcher_event.HolidayApplied = !cbx_DirectDiscount_HolidayApplied.Checked;
                        GetNoticeText(cbx_DirectDiscount_HolidayApplied, sb_restriction);
                        vourcher_event.WeekendApplied = !cbx_DirectDiscount_WeekendApplied.Checked;
                        GetNoticeText(cbx_DirectDiscount_WeekendApplied, sb_restriction);
                        if (cbx_DirectDiscount_OtherTimes.Checked && !string.IsNullOrEmpty(tbx_DirectDiscount_OtherTimes.Text.Trim()))
                        {
                            vourcher_event.OtherTimes = tbx_DirectDiscount_OtherTimes.Text.Trim();
                            GetNoticeText(tbx_DirectDiscount_OtherTimes, sb_restriction);
                        }
                        else
                        {
                            vourcher_event.OtherTimes = null;
                        }
                        vourcher_event.Contents = string.Format(LunchKingSite.I18N.Phrase.VourcherEventTypeDirectDiscountMessage, vourcher_event.Discount.Value.ToString("G0"));
                        break;

                    case VourcherEventType.DirectOffset:
                        vourcher_event.DiscountPrice = int.TryParse(tbx_DirectOffset_DiscountPrice.Text, out discount_price) ? discount_price : 9999;
                        vourcher_event.ServiceFee = !cbx_DirectOffset_ServiceFee.Checked;
                        GetNoticeText(cbx_DirectOffset_ServiceFee, sb_instruction);
                        vourcher_event.ServiceFeeAdditional = cbx_DirectOffset_ServiceFeeAdditional.Checked;
                        GetNoticeText(cbx_DirectOffset_ServiceFeeAdditional, sb_instruction);
                        vourcher_event.ConsumptionApplied = cbx_DirectOffset_ConsumptionApplied.Checked;
                        GetNoticeText(cbx_DirectOffset_ConsumptionApplied, sb_instruction);
                        if (cbx_DirectOffset_MinConsumption.Checked && int.TryParse(tbx_DirectOffset_MinConsumption.Text, out min_consumption))
                        {
                            vourcher_event.MinConsumption = min_consumption;
                            GetNoticeText(tbx_DirectDiscount_MinConsumption, sb_instruction);
                        }
                        else
                        {
                            vourcher_event.MinConsumption = null;
                        }
                        vourcher_event.HolidayApplied = !cbx_DirectOffset_HolidayApplied.Checked;
                        GetNoticeText(cbx_DirectOffset_HolidayApplied, sb_restriction);
                        vourcher_event.WeekendApplied = !cbx_DirectOffset_WeekendApplied.Checked;
                        GetNoticeText(cbx_DirectOffset_WeekendApplied, sb_restriction);
                        if (cbx_DirectOffset_OtherTimes.Checked && !string.IsNullOrEmpty(tbx_DirectOffset_OtherTimes.Text.Trim()))
                        {
                            vourcher_event.OtherTimes = tbx_DirectOffset_OtherTimes.Text.Trim();
                            GetNoticeText(tbx_DirectOffset_OtherTimes, sb_restriction);
                        }
                        else
                        {
                            vourcher_event.OtherTimes = null;
                        }
                        vourcher_event.Contents = string.Format(LunchKingSite.I18N.Phrase.VourcherEventTypeDirectOffsetMessage, vourcher_event.DiscountPrice);
                        break;

                    case VourcherEventType.DiscountSecondOne:
                        vourcher_event.Discount = decimal.TryParse(tbx_DiscountSecondOne_Discount.Text, out discount) ? discount : 10;
                        vourcher_event.OneMeal = cbx_DiscountSecondOne_OneMeal.Checked;
                        GetNoticeText(cbx_DiscountSecondOne_OneMeal, sb_instruction);
                        vourcher_event.TheFourthApplied = cbx_DiscountSecondOne_TheFourthApplied.Checked;
                        GetNoticeText(cbx_DiscountSecondOne_TheFourthApplied, sb_instruction);
                        vourcher_event.ServiceFee = !cbx_DiscountSecondOne_ServiceFee.Checked;
                        GetNoticeText(cbx_DiscountSecondOne_ServiceFee, sb_instruction);
                        vourcher_event.ServiceFeeAdditional = cbx_DiscountSecondOne_ServiceFeeAdditional.Checked;
                        GetNoticeText(cbx_DiscountSecondOne_ServiceFeeAdditional, sb_instruction);
                        vourcher_event.HolidayApplied = !cbx_DiscountSecondOne_HolidayApplied.Checked;
                        GetNoticeText(cbx_DiscountSecondOne_HolidayApplied, sb_restriction);
                        vourcher_event.WeekendApplied = !cbx_DiscountSecondOne_WeekendApplied.Checked;
                        GetNoticeText(cbx_DiscountSecondOne_WeekendApplied, sb_restriction);
                        if (cbx_DiscountSecondOne_OtherTimes.Checked && !string.IsNullOrEmpty(tbx_DiscountSecondOne_OtherTimes.Text.Trim()))
                        {
                            vourcher_event.OtherTimes = tbx_DiscountSecondOne_OtherTimes.Text.Trim();
                            GetNoticeText(tbx_DiscountSecondOne_OtherTimes, sb_restriction);
                        }
                        else
                        {
                            vourcher_event.OtherTimes = null;
                        }
                        vourcher_event.Contents = string.Format(LunchKingSite.I18N.Phrase.VourcherEventTypeDiscountSecondOneMessage, vourcher_event.Discount.Value.ToString("G0"));
                        break;

                    case VourcherEventType.ParticularProduct:
                        vourcher_event.Message1 = tbx_ParticularProduct_Message_1.Text;
                        vourcher_event.DiscountPrice = int.TryParse(tbx_ParticularProduct_DiscountPrice.Text, out discount_price) ? discount_price : 9999;
                        vourcher_event.OriginalPrice = int.TryParse(tbx_ParticularProduct_OriginalPrice.Text, out original_price) ? original_price : 9999;
                        vourcher_event.OneMeal = cbx_ParticularProduct_OneMeal.Checked;
                        GetNoticeText(cbx_ParticularProduct_OneMeal, sb_instruction);
                        vourcher_event.MultipleMeals = cbx_ParticularProduct_MultipleMeals.Checked;
                        GetNoticeText(cbx_ParticularProduct_MultipleMeals, sb_instruction);
                        vourcher_event.ServiceFee = !cbx_ParticularProduct_ServiceFee.Checked;
                        GetNoticeText(cbx_ParticularProduct_ServiceFee, sb_instruction);
                        vourcher_event.ServiceFeeAdditional = cbx_ParticularProduct_ServiceFeeAdditional.Checked;
                        GetNoticeText(cbx_ParticularProduct_ServiceFeeAdditional, sb_instruction);
                        vourcher_event.HolidayApplied = !cbx_ParticularProduct_HolidayApplied.Checked;
                        GetNoticeText(cbx_ParticularProduct_HolidayApplied, sb_restriction);
                        vourcher_event.WeekendApplied = !cbx_ParticularProduct_WeekendApplied.Checked;
                        GetNoticeText(cbx_ParticularProduct_WeekendApplied, sb_restriction);
                        if (cbx_ParticularProduct_OtherTimes.Checked && !string.IsNullOrEmpty(tbx_ParticularProduct_OtherTimes.Text.Trim()))
                        {
                            vourcher_event.OtherTimes = tbx_ParticularProduct_OtherTimes.Text.Trim();
                            GetNoticeText(tbx_ParticularProduct_OtherTimes, sb_restriction);
                        }
                        else
                        {
                            vourcher_event.OtherTimes = null;
                        }
                        vourcher_event.Contents = string.Format(LunchKingSite.I18N.Phrase.VourcherEventTypeParticularProductMessage, vourcher_event.Message1, vourcher_event.DiscountPrice, vourcher_event.OriginalPrice);
                        break;

                    case VourcherEventType.Upgrade:
                        vourcher_event.Message1 = tbx_Upgrade_Message_1.Text;
                        vourcher_event.Message2 = tbx_Upgrade_Message_2.Text;
                        vourcher_event.OriginalPrice = int.TryParse(tbx_Upgrade_OriginalPrice.Text, out original_price) ? original_price : 9999;
                        vourcher_event.OneMeal = cbx_Upgrade_OneMeal.Checked;
                        GetNoticeText(cbx_Upgrade_OneMeal, sb_instruction);
                        vourcher_event.MultipleMeals = cbx_Upgrade_MultipleMeals.Checked;
                        GetNoticeText(cbx_Upgrade_MultipleMeals, sb_instruction);
                        vourcher_event.ServiceFee = !cbx_Upgrade_ServiceFee.Checked;
                        GetNoticeText(cbx_Upgrade_ServiceFee, sb_instruction);
                        vourcher_event.ServiceFeeAdditional = cbx_Upgrade_ServiceFeeAdditional.Checked;
                        GetNoticeText(cbx_Upgrade_ServiceFeeAdditional, sb_instruction);
                        vourcher_event.HolidayApplied = !cbx_Upgrade_HolidayApplied.Checked;
                        GetNoticeText(cbx_Upgrade_HolidayApplied, sb_restriction);
                        vourcher_event.WeekendApplied = !cbx_Upgrade_WeekendApplied.Checked;
                        GetNoticeText(cbx_Upgrade_WeekendApplied, sb_restriction);
                        if (cbx_Upgrade_OtherTimes.Checked && !string.IsNullOrEmpty(tbx_Upgrade_OtherTimes.Text.Trim()))
                        {
                            vourcher_event.OtherTimes = tbx_Upgrade_OtherTimes.Text.Trim();
                            GetNoticeText(tbx_Upgrade_OtherTimes, sb_restriction);
                        }
                        else
                        {
                            vourcher_event.OtherTimes = null;
                        }
                        vourcher_event.Contents = string.Format(LunchKingSite.I18N.Phrase.VourcherEventTypeUpgradeMessage, vourcher_event.Message1, vourcher_event.Message2, vourcher_event.OriginalPrice);
                        break;

                    case VourcherEventType.Gift:
                        vourcher_event.Message1 = tbx_Gift_Message_1.Text;
                        vourcher_event.OriginalPrice = int.TryParse(tbx_Gift_OriginalPrice.Text, out original_price) ? original_price : 9999;
                        vourcher_event.OneMeal = cbx_Gift_OneMeal.Checked;
                        GetNoticeText(cbx_Gift_OneMeal, sb_instruction);
                        vourcher_event.AllTable = cbx_Gift_AllTable.Checked;
                        GetNoticeText(cbx_Gift_AllTable, sb_instruction);
                        vourcher_event.ServiceFee = !cbx_Gift_ServiceFee.Checked;
                        GetNoticeText(cbx_Gift_ServiceFee, sb_instruction);
                        vourcher_event.ServiceFeeAdditional = cbx_Gift_ServiceFeeAdditional.Checked;
                        GetNoticeText(cbx_Gift_ServiceFeeAdditional, sb_instruction);
                        vourcher_event.GiftAttending = cbx_Gift_GiftAttending.Checked;
                        GetNoticeText(cbx_Gift_GiftAttending, sb_instruction);
                        vourcher_event.GiftConsumption = cbx_Gift_GiftConsumption.Checked;
                        GetNoticeText(cbx_Gift_GiftConsumption, sb_instruction);
                        vourcher_event.GiftLimited = !cbx_Gift_GiftLimited.Checked;
                        GetNoticeText(cbx_Gift_GiftLimited, sb_instruction);
                        if (cbx_Gife_GiftQuantity.Checked && int.TryParse(tbx_Gife_GiftQuantity.Text, out gift_quantity))
                        {
                            vourcher_event.GiftQuantity = gift_quantity;
                            GetNoticeText(tbx_Gife_GiftQuantity, sb_instruction);
                        }
                        else
                        {
                            vourcher_event.GiftQuantity = null;
                        }
                        vourcher_event.GiftReplacable = cbx_Gift_GiftReplacabe.Checked;
                        GetNoticeText(cbx_Gift_GiftReplacabe, sb_instruction);
                        if (cbx_Gift_GiftMinConsumption.Checked && int.TryParse(tbx_Gift_GiftMinConsumption.Text, out min_consumption))
                        {
                            vourcher_event.MinConsumption = min_consumption;
                            GetNoticeText(tbx_Gift_GiftMinConsumption, sb_instruction);
                        }
                        else
                        {
                            vourcher_event.MinConsumption = null;
                        }
                        vourcher_event.Contents = string.Format(LunchKingSite.I18N.Phrase.VourcherEventTypeGiftMessage, vourcher_event.Message1, vourcher_event.OriginalPrice);
                        break;

                    case VourcherEventType.CustomersGetOneFree:
                        vourcher_event.OriginalPrice = int.TryParse(tbx_CustomersGetOneFree_OriginalPrice.Text, out original_price) ? original_price : 0;
                        vourcher_event.DiscountPrice = int.TryParse(tbx_CustomersGetOneFree_DiscountPrice.Text, out discount_price) ? discount_price : 0;
                        vourcher_event.OneMeal = cbx_CustomersGetOneFree_OneMeal.Checked;
                        GetNoticeText(cbx_CustomersGetOneFree_OneMeal, sb_instruction);
                        vourcher_event.AllTable = cbx_CustomersGetOneFree_AllTable.Checked;
                        GetNoticeText(cbx_CustomersGetOneFree_AllTable, sb_instruction);
                        vourcher_event.ServiceFee = !cbx_CustomersGetOneFree_ServiceFee.Checked;
                        GetNoticeText(cbx_CustomersGetOneFree_ServiceFee, sb_instruction);
                        vourcher_event.ServiceFeeAdditional = cbx_CustomersGetOneFree_ServiceFeeAdditional.Checked;
                        GetNoticeText(cbx_CustomersGetOneFree_ServiceFeeAdditional, sb_instruction);
                        vourcher_event.Contents = string.Format(LunchKingSite.I18N.Phrase.VourcherEventTypeCustomersGetOneFreeMessage, vourcher_event.OriginalPrice, vourcher_event.DiscountPrice);
                        break;

                    case VourcherEventType.ConsumptionGetOnFree:
                        vourcher_event.Message1 = tbx_ConsumptionGetOnFree_Message_1.Text;
                        vourcher_event.OriginalPrice = int.TryParse(tbx_ConsumptionGetOnFree_OriginalPrice.Text, out original_price) ? original_price : 9999;
                        vourcher_event.DiscountPrice = int.TryParse(tbx_ConsumptionGetOnFree_DiscountPrice.Text, out discount_price) ? discount_price : 9999;
                        vourcher_event.OneMeal = cbx_ConsumptionGetOnFree_OneMeal.Checked;
                        GetNoticeText(cbx_ConsumptionGetOnFree_OneMeal, sb_instruction);
                        vourcher_event.AllTable = cbx_ConsumptionGetOnFree_AllTable.Checked;
                        GetNoticeText(cbx_ConsumptionGetOnFree_AllTable, sb_instruction);
                        vourcher_event.ServiceFee = !cbx_ConsumptionGetOnFree_ServiceFee.Checked;
                        GetNoticeText(cbx_ConsumptionGetOnFree_ServiceFee, sb_instruction);
                        vourcher_event.ServiceFeeAdditional = cbx_ConsumptionGetOnFree_ServiceFeeAdditional.Checked;
                        GetNoticeText(cbx_ConsumptionGetOnFree_ServiceFeeAdditional, sb_instruction);
                        vourcher_event.Contents = string.Format(LunchKingSite.I18N.Phrase.VourcherEventTypeConsumptionGetOnFreeMessage, vourcher_event.Message1, vourcher_event.DiscountPrice, vourcher_event.OriginalPrice);
                        break;

                    case VourcherEventType.BirthDay:
                        vourcher_event.Message1 = tbx_BirthDay_Message_1.Text;
                        vourcher_event.OneMeal = cbx_BirthDay_OneMeal.Checked;
                        GetNoticeText(cbx_BirthDay_OneMeal, sb_instruction);
                        vourcher_event.AllTable = cbx_BirthDay_AllTable.Checked;
                        GetNoticeText(cbx_BirthDay_AllTable, sb_instruction);
                        vourcher_event.MultipleMeals = cbx_BirthDay_MultipleMeals.Checked;
                        GetNoticeText(cbx_BirthDay_MultipleMeals, sb_instruction);
                        vourcher_event.ServiceFee = !cbx_BirthDay_ServiceFee.Checked;
                        GetNoticeText(cbx_BirthDay_ServiceFee, sb_instruction);
                        vourcher_event.ServiceFeeAdditional = cbx_BirthDay_ServiceFeeAdditional.Checked;
                        GetNoticeText(cbx_BirthDay_ServiceFeeAdditional, sb_instruction);
                        if (cbx_BirthDay_MinPersons.Checked && int.TryParse(tbx_BirthDay_MinPersons.Text, out min_persons))
                        {
                            vourcher_event.MinPersons = min_persons;
                            GetNoticeText(tbx_BirthDay_MinPersons, sb_instruction);
                        }
                        else
                        {
                            vourcher_event.MinPersons = null;
                        }
                        vourcher_event.Contents = string.Format(LunchKingSite.I18N.Phrase.VourcherEventTypeBirthDayMessage, vourcher_event.Message1);
                        break;

                    case VourcherEventType.Other:
                        vourcher_event.Message1 = tbx_Other_Message_1.Text;
                        vourcher_event.ServiceFee = !cbx_Other_ServiceFee.Checked;
                        GetNoticeText(cbx_Other_ServiceFee, sb_instruction);
                        vourcher_event.ServiceFeeAdditional = cbx_Other_ServiceFeeAdditional.Checked;
                        GetNoticeText(cbx_Other_ServiceFeeAdditional, sb_instruction);
                        vourcher_event.Contents = string.Format(LunchKingSite.I18N.Phrase.VourcherEventTypeOtherMessage, vourcher_event.Message1);
                        break;

                    case VourcherEventType.Lottery:
                        vourcher_event.Contents = tbx_Lottery_Message_1.Text;
                        break;
                }
                vourcher_event.Instruction = sb_instruction.ToString();
                vourcher_event.Restriction = sb_restriction.ToString();
                bool addnext = (sender is Button) ? (((Button)sender).ID == "btn_AddNewVourcherAndOn") : false;
                List<PhotoInfo> photoinfos = new List<PhotoInfo>();

                int i = 1;
                foreach (var file in PicFileUploads)
                {
                    PhotoInfo photo = new PhotoInfo();
                    if (!String.IsNullOrEmpty(file.PostedFile.FileName) && file.PostedFile.ContentLength < 3698688)
                    {
                        string tempFile = Path.GetTempFileName();
                        HttpPostedFile pFile = file.PostedFile;
                        if (pFile.ContentType == "image/pjpeg" || pFile.ContentType == "image/jpeg" || pFile.ContentType == "image/x-png" || pFile.ContentType == "image/gif")
                        {
                            photo.PFile = pFile;
                            photo.Type = UploadFileType.PponEvent;
                            photo.DestFileName = i.ToString().PadLeft(2, '0') + DateTime.Now.ToString("MMddss");
                        }
                    }
                    photoinfos.Add(photo);
                    i++;
                }
                List<int> delete_imgs = new List<int>();
                for (int j = 0; j < PicDeleteImgs.Count; j++)
                {
                    if (PicDeleteImgs[j].Checked)
                    {
                        delete_imgs.Add(j);
                    }
                }
                SaveVourcherEventStores(sender, new DataEventArgs<VourcherEventInfo>(new VourcherEventInfo(vourcher_event, vourcher_stores, photoinfos, delete_imgs, addnext)));
            }
            else
            {
                lab_Vourcher_SetUpMessage.Text = message + "<br/>";
                ClientScript.RegisterStartupScript(GetType(), "alert", "alert('" + message + "');", true);
            }
        }

        /// <summary>
        /// 返回搜尋
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ReturnCancel(object sender, EventArgs e)
        {
            btn_AddNewVourcher.Text = "新增優惠券";
            ClearText();
            pan_SearchVourcherEvent.Visible = true;
            pan_VourcherEvent.Visible = false;
        }

        /// <summary>
        /// 變更輸入區塊
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ChangeInputPanel(object sender, EventArgs e)
        {
            foreach (var item in TypePanels)
            {
                item.Value.Visible = false;
            }
            TypePanels[EventType].Visible = true;

            // 設定預設值
            if (EventType == VourcherEventType.Lottery)
            {
                tbx_Lottery_Message_1.Text = ProviderFactory.Instance().GetConfig().VourcherLotteryEvent;
            }
        }

        /// <summary>
        /// 搜尋優惠券列表
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SearchVourche(object sender, EventArgs e)
        {
            if (SearchVourcherEvent != null)
            {
                gridPager.ResolvePagerView(1, true);
                SearchVourcherEvent(sender, e);
            }
        }

        /// <summary>
        /// 變更優惠券狀態並重新搜尋
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ChangeEventStatus(object sender, EventArgs e)
        {
            if (SearchVourcherByStatus != null)
            {
                SearchVourcherByStatus(sender, e);
            }
        }

        /// <summary>
        /// 搜尋優惠券分頁
        /// </summary>
        /// <param name="pageNumber"></param>
        protected void UpdateHandler(int pageNumber)
        {
            DataEventArgs<int> e = new DataEventArgs<int>(pageNumber);
            if (this.PageChanged != null)
            {
                this.PageChanged(this, e);
            }
        }

        /// <summary>
        /// 搜尋優惠券計算資料量
        /// </summary>
        /// <returns></returns>
        protected int RetrieveDataCount()
        {
            EventArgs e = new EventArgs();
            if (GetDataCount != null)
            {
                GetDataCount(this, e);
            }
            return PageCount;
        }

        protected bool AvoidWarning()
        {
            if (PageReturnCaseChanged != null && GetReturnCaseDataCount != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        protected void rptCommercialCategory_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is KeyValuePair<Category, List<Category>>)
            {
                KeyValuePair<Category, List<Category>> dataItem = (KeyValuePair<Category, List<Category>>)e.Item.DataItem;

                HiddenField hidCommercialCategoryId = (HiddenField)e.Item.FindControl("hidCommercialCategoryId");
                CheckBox chkCommercialArea = (CheckBox)e.Item.FindControl("chkCommercialArea");
                CheckBoxList chklCommercialCategory = (CheckBoxList)e.Item.FindControl("chklCommercialCategory");

                hidCommercialCategoryId.Value = dataItem.Key.Id.ToString();
                chkCommercialArea.Text = dataItem.Key.Name;
                chklCommercialCategory.DataSource = dataItem.Value;
                chklCommercialCategory.DataValueField = "Id";
                chklCommercialCategory.DataTextField = "Name";
                chklCommercialCategory.DataBind();
            }
        }

        #region local method

        /// <summary>
        /// 清除輸入資料
        /// </summary>
        private void ClearText()
        {
            tbx_Vourcher_EventName.Text = lab_Vourcher_SetUpMessage.Text = tbx_MaxQuantity.Text = tbx_Others.Text = string.Empty;
            foreach (var item in TypePanels)
            {
                foreach (var innitem in item.Value.Controls)
                {
                    if (innitem is CheckBox)
                    {
                        ((CheckBox)innitem).Checked = false;
                    }
                    else if (innitem is TextBox)
                    {
                        ((TextBox)innitem).Text = string.Empty;
                    }
                }
            }
            cbx_Delete_Img_1.Checked = cbx_Delete_Img_2.Checked = cbx_Delete_Img_3.Checked = cbx_Delete_Img_4.Checked = cbx_Delete_Img_5.Checked = cbx_Delete_Img_6.Checked = false;
        }

        /// <summary>
        /// 抓取checkbox，textbox字串
        /// </summary>
        /// <param name="cbx"></param>
        /// <param name="sb"></param>
        private void GetNoticeText(CheckBox cbx, StringBuilder sb)
        {
            if (cbx.Checked)
            {
                sb.Append("■" + cbx.Text);
            }
        }

        /// <summary>
        /// 組成使用提醒字串
        /// </summary>
        /// <param name="tbx"></param>
        /// <param name="sb"></param>
        private void GetNoticeText(TextBox tbx, StringBuilder sb)
        {
            if (!string.IsNullOrEmpty(tbx.Text))
            {
                sb.Append("■" + string.Format(tbx.ToolTip, tbx.Text));
            }
        }

        #endregion
        #endregion
        #region method

        /// <summary>
        /// 設定賣家和其分店及賣家的修改資料
        /// </summary>
        /// <param name="seller">賣家資料</param>
        /// <param name="stores">分店資料</param>
        public void SetSellerStores(Seller seller, StoreCollection stores)
        {
            if (seller.Guid == Guid.Empty)
            {
                ClientScript.RegisterStartupScript(GetType(), "alert", "alert('無此賣家');", true);
                return;
            }
            else if (stores.Count == 0 && !seller.IsCloseDown)
            {
                ClientScript.RegisterStartupScript(GetType(), "alert", "alert('此賣家無分店資訊，請先建立分店');", true);
                return;
            }
            else
            {
                if (seller.IsCloseDown)
                {
                    ClientScript.RegisterStartupScript(GetType(), "alert", "alert('此賣家已結束營業');", true);
                }
                ClearText();
                pan_SearchVourcherEvent.Visible = false;
                pan_VourcherEvent.Visible = true;
                rpt_TempStores.DataSource = stores;
                rpt_TempStores.DataBind();
                lab_SellerId.Text = seller.SellerId;
                lab_SellerName.Text = seller.SellerName;
                lab_ComapnyName.Text = seller.CompanyName;
                lab_CompanyId.Text = seller.SignCompanyID;
            }
        }

        /// <summary>
        /// 設定優惠券資料和其對應分店資料
        /// </summary>
        /// <param name="vourcher_event">優惠券資料</param>
        /// <param name="vourcher_stores">對應分店資料</param>
        public void SetVourcherEventStores(Seller seller, StoreCollection stores, VourcherEvent vourcher_event, VourcherStoreCollection vourcher_stores)
        {
            SetSellerStores(seller, stores);
            btn_AddNewVourcherAndOn.Visible = false;
            lab_CurrentStatus.Text = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, (VourcherEventStatus)vourcher_event.Status);
            pan_Return.Visible = (VourcherEventStatus)vourcher_event.Status == VourcherEventStatus.ReturnApply;
            lab_ReturnMessage.Text = vourcher_event.Message;
            cbx_Magazine.Checked = !(vourcher_event.Magazine ?? true);
            tbx_Others.Text = vourcher_event.Others;
            btn_AddNewVourcher.Text = "更新優惠券";

            for (int i = 0; i < rpt_TempStores.Items.Count; i++)
            {
                Guid store_guid;
                CheckBox cbx = (CheckBox)rpt_TempStores.Items[i].FindControl("cbx_Store");
                HiddenField hif = (HiddenField)rpt_TempStores.Items[i].FindControl("hif_StoreGuid");
                TextBox tbx = (TextBox)rpt_TempStores.Items[i].FindControl("tbx_StoreMaxQuantity");
                if (Guid.TryParse(hif.Value, out store_guid) && vourcher_stores.Any(x => x.StoreGuid == store_guid))
                {
                    cbx.Checked = true;
                }
                else
                {
                    cbx.Checked = false;
                }
            }
            tbx_MaxQuantity.Text = vourcher_event.MaxQuantity.ToString();
            tbx_Vourcher_EventName.Text = vourcher_event.EventName;
            ddl_Vourcher_Type.SelectedValue = vourcher_event.Type.ToString();
            EventType = (VourcherEventType)vourcher_event.Type;
            ddlContractSend.SelectedValue = vourcher_event.ContractStatus.ToString();
            txtNoContractReason.Text = vourcher_event.NoContractReason;
            txtSalesName.Text = vourcher_event.SalesName;
            ChangeInputPanel(this, new EventArgs());
            EventMode = (VourcherEventMode)vourcher_event.Mode;
            switch (EventType)
            {
                case VourcherEventType.DirectDiscount:
                    tbx_DirectDiscount_Discount.Text = (vourcher_event.Discount ?? 10).ToString("G0");
                    cbx_DirectDiscount_AllTable.Checked = vourcher_event.AllTable ?? false;
                    cbx_DirectDiscount_OneMeal.Checked = vourcher_event.OneMeal ?? false;
                    cbx_DirectDiscount_ServiceFee.Checked = !(vourcher_event.ServiceFee ?? true);
                    cbx_DirectDiscount_ServiceFeeAdditional.Checked = vourcher_event.ServiceFeeAdditional ?? false;
                    cbx_DirectDiscount_ConsumptionApplied.Checked = vourcher_event.ConsumptionApplied ?? false;
                    cbx_DirectDiscount_MinConsumption.Checked = vourcher_event.MinConsumption.HasValue;
                    tbx_DirectDiscount_MinConsumption.Text = vourcher_event.MinConsumption.ToString();
                    cbx_DirectDiscount_HolidayApplied.Checked = !(vourcher_event.HolidayApplied ?? true);
                    cbx_DirectDiscount_WeekendApplied.Checked = !(vourcher_event.WeekendApplied ?? false);
                    cbx_DirectDiscount_OtherTimes.Checked = !string.IsNullOrEmpty(vourcher_event.OtherTimes);
                    tbx_DirectDiscount_OtherTimes.Text = vourcher_event.OtherTimes;
                    break;

                case VourcherEventType.DirectOffset:
                    tbx_DirectOffset_DiscountPrice.Text = vourcher_event.DiscountPrice.ToString();
                    cbx_DirectOffset_ServiceFee.Checked = !(vourcher_event.ServiceFee ?? true);
                    cbx_DirectOffset_ServiceFeeAdditional.Checked = vourcher_event.ServiceFeeAdditional ?? false;
                    cbx_DirectOffset_ConsumptionApplied.Checked = vourcher_event.ConsumptionApplied ?? false;
                    cbx_DirectOffset_MinConsumption.Checked = vourcher_event.MinConsumption.HasValue;
                    tbx_DirectOffset_MinConsumption.Text = vourcher_event.MinConsumption.ToString();
                    cbx_DirectOffset_HolidayApplied.Checked = !(vourcher_event.HolidayApplied ?? true);
                    cbx_DirectOffset_WeekendApplied.Checked = !(vourcher_event.WeekendApplied ?? true);
                    cbx_DirectOffset_OtherTimes.Checked = !string.IsNullOrEmpty(vourcher_event.OtherTimes);
                    tbx_DirectOffset_OtherTimes.Text = vourcher_event.OtherTimes;
                    break;

                case VourcherEventType.DiscountSecondOne:
                    tbx_DiscountSecondOne_Discount.Text = (vourcher_event.Discount ?? 10).ToString("G0");
                    cbx_DiscountSecondOne_OneMeal.Checked = vourcher_event.OneMeal ?? false;
                    cbx_DiscountSecondOne_TheFourthApplied.Checked = vourcher_event.TheFourthApplied ?? false;
                    cbx_DiscountSecondOne_ServiceFee.Checked = !(vourcher_event.ServiceFee ?? true);
                    cbx_DiscountSecondOne_ServiceFeeAdditional.Checked = vourcher_event.ServiceFeeAdditional ?? false;
                    cbx_DiscountSecondOne_HolidayApplied.Checked = !(vourcher_event.HolidayApplied ?? true);
                    cbx_DiscountSecondOne_WeekendApplied.Checked = !(vourcher_event.WeekendApplied ?? true);
                    cbx_DiscountSecondOne_OtherTimes.Checked = !string.IsNullOrEmpty(vourcher_event.OtherTimes);
                    tbx_DiscountSecondOne_OtherTimes.Text = vourcher_event.OtherTimes;
                    break;

                case VourcherEventType.ParticularProduct:
                    tbx_ParticularProduct_Message_1.Text = vourcher_event.Message1;
                    tbx_ParticularProduct_DiscountPrice.Text = vourcher_event.DiscountPrice.ToString();
                    tbx_ParticularProduct_OriginalPrice.Text = vourcher_event.OriginalPrice.ToString();
                    cbx_ParticularProduct_OneMeal.Checked = vourcher_event.OneMeal ?? false;
                    cbx_ParticularProduct_MultipleMeals.Checked = vourcher_event.MultipleMeals ?? false;
                    cbx_ParticularProduct_ServiceFee.Checked = !(vourcher_event.ServiceFee ?? true);
                    cbx_ParticularProduct_ServiceFeeAdditional.Checked = vourcher_event.ServiceFeeAdditional ?? false;
                    cbx_ParticularProduct_HolidayApplied.Checked = !(vourcher_event.HolidayApplied ?? true);
                    cbx_ParticularProduct_WeekendApplied.Checked = !(vourcher_event.WeekendApplied ?? true);
                    cbx_ParticularProduct_OtherTimes.Checked = !string.IsNullOrEmpty(vourcher_event.OtherTimes);
                    tbx_ParticularProduct_OtherTimes.Text = vourcher_event.OtherTimes;
                    break;

                case VourcherEventType.Upgrade:
                    tbx_Upgrade_Message_1.Text = vourcher_event.Message1;
                    tbx_Upgrade_Message_2.Text = vourcher_event.Message2;
                    tbx_Upgrade_OriginalPrice.Text = vourcher_event.OriginalPrice.ToString();
                    cbx_Upgrade_OneMeal.Checked = vourcher_event.OneMeal ?? false;
                    cbx_Upgrade_MultipleMeals.Checked = vourcher_event.MultipleMeals ?? false;
                    cbx_Upgrade_ServiceFee.Checked = !(vourcher_event.ServiceFee ?? true);
                    cbx_Upgrade_ServiceFeeAdditional.Checked = vourcher_event.ServiceFeeAdditional ?? false;
                    cbx_Upgrade_HolidayApplied.Checked = !(vourcher_event.HolidayApplied ?? true);
                    cbx_Upgrade_WeekendApplied.Checked = !(vourcher_event.WeekendApplied ?? true);
                    cbx_Upgrade_OtherTimes.Checked = !string.IsNullOrEmpty(vourcher_event.OtherTimes);
                    tbx_Upgrade_OtherTimes.Text = vourcher_event.OtherTimes;
                    break;

                case VourcherEventType.Gift:
                    tbx_Gift_Message_1.Text = vourcher_event.Message1;
                    tbx_Gift_OriginalPrice.Text = vourcher_event.OriginalPrice.ToString();
                    cbx_Gift_OneMeal.Checked = vourcher_event.OneMeal ?? false;
                    cbx_Gift_AllTable.Checked = vourcher_event.AllTable ?? false;
                    cbx_Gift_ServiceFee.Checked = !(vourcher_event.ServiceFee ?? true);
                    cbx_Gift_ServiceFeeAdditional.Checked = vourcher_event.ServiceFeeAdditional ?? false;
                    cbx_Gift_GiftAttending.Checked = vourcher_event.GiftAttending ?? false;
                    cbx_Gift_GiftConsumption.Checked = vourcher_event.GiftConsumption ?? false;
                    cbx_Gift_GiftLimited.Checked = !(vourcher_event.GiftLimited ?? true);
                    cbx_Gife_GiftQuantity.Checked = vourcher_event.GiftQuantity.HasValue;
                    tbx_Gife_GiftQuantity.Text = vourcher_event.GiftQuantity.ToString();
                    cbx_Gift_GiftReplacabe.Checked = vourcher_event.GiftReplacable ?? false;
                    cbx_Gift_GiftMinConsumption.Checked = vourcher_event.MinConsumption.HasValue;
                    tbx_Gift_GiftMinConsumption.Text = vourcher_event.MinConsumption.ToString();
                    break;

                case VourcherEventType.CustomersGetOneFree:
                    tbx_CustomersGetOneFree_OriginalPrice.Text = vourcher_event.OriginalPrice.ToString();
                    tbx_CustomersGetOneFree_DiscountPrice.Text = vourcher_event.DiscountPrice.ToString();
                    cbx_CustomersGetOneFree_OneMeal.Checked = vourcher_event.OneMeal ?? false;
                    cbx_CustomersGetOneFree_AllTable.Checked = vourcher_event.AllTable ?? false;
                    cbx_CustomersGetOneFree_ServiceFee.Checked = !(vourcher_event.ServiceFee ?? true);
                    cbx_CustomersGetOneFree_ServiceFeeAdditional.Checked = vourcher_event.ServiceFeeAdditional ?? false;
                    break;

                case VourcherEventType.ConsumptionGetOnFree:
                    tbx_ConsumptionGetOnFree_Message_1.Text = vourcher_event.Message1;
                    tbx_ConsumptionGetOnFree_OriginalPrice.Text = vourcher_event.OriginalPrice.ToString();
                    tbx_ConsumptionGetOnFree_DiscountPrice.Text = vourcher_event.DiscountPrice.ToString();
                    cbx_ConsumptionGetOnFree_OneMeal.Checked = vourcher_event.OneMeal ?? false;
                    cbx_ConsumptionGetOnFree_AllTable.Checked = vourcher_event.AllTable ?? false;
                    cbx_ConsumptionGetOnFree_ServiceFee.Checked = !(vourcher_event.ServiceFee ?? true);
                    cbx_ConsumptionGetOnFree_ServiceFeeAdditional.Checked = vourcher_event.ServiceFeeAdditional ?? false;
                    break;

                case VourcherEventType.BirthDay:
                    tbx_BirthDay_Message_1.Text = vourcher_event.Message1;
                    cbx_BirthDay_OneMeal.Checked = vourcher_event.OneMeal ?? false;
                    cbx_BirthDay_AllTable.Checked = vourcher_event.AllTable ?? false;
                    cbx_BirthDay_MultipleMeals.Checked = vourcher_event.MultipleMeals ?? false;
                    cbx_BirthDay_ServiceFee.Checked = !(vourcher_event.ServiceFee ?? true);
                    cbx_BirthDay_ServiceFeeAdditional.Checked = vourcher_event.ServiceFeeAdditional ?? false;
                    cbx_BirthDay_MinPersons.Checked = vourcher_event.MinPersons.HasValue;
                    tbx_BirthDay_MinPersons.Text = vourcher_event.MinPersons.ToString();
                    break;

                case VourcherEventType.Other:
                    tbx_Other_Message_1.Text = vourcher_event.Message1;
                    cbx_Other_ServiceFee.Checked = !(vourcher_event.ServiceFee ?? true);
                    cbx_Other_ServiceFeeAdditional.Checked = vourcher_event.ServiceFeeAdditional ?? false;
                    break;

                case VourcherEventType.Lottery:
                    tbx_Lottery_Message_1.Text = vourcher_event.Contents;
                    break;
            }

            string[] vourcher_pic_array = ImageFacade.GetMediaPathsFromRawData(vourcher_event.PicUrl, MediaType.PponDealPhoto);
            for (int i = 0; i < PicImages.Count; i++)
            {
                PicImages[i].ImageUrl = vourcher_pic_array.Skip(i).DefaultIfEmpty(string.Empty).First();
                PicImages[i].DataBind();
                PicImages[i].Visible = !string.IsNullOrEmpty(PicImages[i].ImageUrl);
            }

            if (vourcher_event.CreateId == UserName)
            {
                if (vourcher_event.Status == (int)VourcherEventStatus.ApplyEvent || vourcher_event.Status == (int)VourcherEventStatus.ReturnApply)
                {
                    btn_AddNewVourcher.Enabled = true;
                }
                else
                {
                    btn_AddNewVourcher.Enabled = false;
                    lab_Vourcher_SetUpMessage.Text = "目前狀態為" + Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, (VourcherEventStatus)(vourcher_event.Status)) + ",無法更新";
                }
            }
            else
            {
                btn_AddNewVourcher.Enabled = false;
            }
        }

        /// <summary>
        /// 回傳搜尋的優惠券內容
        /// </summary>
        /// <param name="vourcher_events">優惠券列表</param>
        public void SetVourcherEventSellerCollection(ViewVourcherSellerCollection vourcher_sellers)
        {
            rpt_VourcherEventList.DataSource = vourcher_sellers;
            rpt_VourcherEventList.DataBind();
            if (vourcher_sellers.Count == 0)
            {
                ClientScript.RegisterStartupScript(GetType(), "alert", "alert('查無資料');", true);
            }
        }

        /// <summary>
        /// 回傳退件的優惠券
        /// </summary>
        /// <param name="vourcher_events">退件的優惠券</param>
        public void SetReturnCaseVourcherEvent(ViewVourcherSellerCollection vourcher_sellers)
        {
            rpt_ReturnCase.DataSource = vourcher_sellers;
            rpt_ReturnCase.DataBind();
        }

        /// <summary>
        /// 設定生活商圈資料
        /// </summary>
        /// <param name="categories"></param>
        public void SetSelectableCommercialCategory(Dictionary<Category, List<Category>> categories, Dictionary<int, string> category_list)
        {
            cbx_Category.DataSource = category_list;
            cbx_Category.DataTextField = "Value";
            cbx_Category.DataValueField = "Key";
            cbx_Category.DataBind();
            rptCommercialCategory.DataSource = categories;
            rptCommercialCategory.DataBind();
        }

        #endregion
    }
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Ppon/Ppon.Master" AutoEventWireup="true"
    CodeBehind="vourcher.aspx.cs" Inherits="LunchKingSite.Web.Vourcher.vourcher" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/User/Pager.ascx" TagName="Pager" TagPrefix="uc1" %>
<%@ Import Namespace="LunchKingSite.Core" %>
<%@ Import Namespace="LunchKingSite.DataOrm" %>
<%@ Import Namespace="LunchKingSite.BizLogic.Component" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <link type="text/css" href="../Tools/js/css/ui-lightness/jquery-ui-1.8rc3.custom.css"
        rel="stylesheet" />
    <script src="../Tools/js/effects/jquery.flexipage.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#navi').attr('style', 'display:none;');

            $('.tablecontent tr:odd').addClass('oddtr');

            $('fieldset li').hover(function () { $(this).addClass('highlight_tr'); }, function () { $(this).removeClass('highlight_tr'); });

            $('#<%= ddl_Vourcher_Mode.ClientID %>').live('change', function () {
                maxquantity_check($(this).val());
            });

            $('#<%= ddl_Vourcher_Type.ClientID %>').live('change', function () {
                Page_BlockSubmit = false;
            });

            maxquantity_check($('#<%= ddl_Vourcher_Mode.ClientID %> option:selected').val());
            $('#aa').live('input', function () {
                chineseCount($(this).val(), $('#s_title'), 6);
            });
            $('#pagerstores').flexipage({ perpage: 10, element: 'tr', visible_css: { display: "table-row" } });

            // 生活商圈
            $('.divCommercial').each(function () {
                var selected = $(this).find('[id*=chklCommercialCategory]').filter(function () { return $(this).is(':checked') }).length;
                if (selected == 0) {
                    $(this).find('[id*=chkCommercialArea]').attr('checked', false);
                    $(this).find('#CommercialCategoryList').css('display', 'none');
                } else {
                    $(this).find('[id*=chkCommercialArea]').attr('checked', 'checked');
                    $(this).find('#CommercialCategoryList').css('display', '');
                }
            })

            // 合約送交狀態
            $('[id*=ddlContractSend]').bind("change", function () {
                CheckContractStatus();
                if ($('[id*=ddlContractSend]').find(":checked").val() == '<%= (int)ContractSendStatus.Others %>') {
                    $('[id*=txtNoContractReason]').val('');
                }
            });
            CheckContractStatus();

            var availableEmailTags = [<%=SalesEmailArray%>];
            $('#<%=txtSalesName.ClientID %>').autocomplete({
                source: availableEmailTags
            });
        });
        function CheckContractStatus() {
            var $reason = $('[id*=txtNoContractReason]');
            if ($('[id*=ddlContractSend]').find(":checked").val() == '<%= (int)ContractSendStatus.Others %>') {
                $reason.show();
            } else {
                $reason.val($('[id*=ddlContractSend]').find(":checked").text());
                $reason.hide();
            }
        }
        function maxquantity_check(item) {
            //            if (item == '2') {
            //                $('.storemaxquantity').attr('style', 'display:inline;');
            //                $('.maxquantity').attr('style', 'display:none;');
            //            }
            if (item == '2' || item == '3') {
                $('.maxquantity').attr('style', 'display:inline;');
                $('.storemaxquantity').attr('style', 'display:none;');
            }
            else {
                $('.storemaxquantity').attr('style', 'display:none;');
                $('.maxquantity').attr('style', 'display:none;');
            }
        }
        function chineseCount(word, d, max) {
            v = 0
            for (cc = 0; cc < word.length; cc++) {
                c = word.charCodeAt(cc);
                if (!(c >= 32 && c <= 126)) v++;
            }
            total = Math.ceil((word.length - v) / 2);
            $(d).text((v + total) + '/' + max);
            if ((v + total) > max) {
                $(d).addClass('redcolor');
            }
            else {
                $(d).removeClass('redcolor');
            }
        }
        function Check_data() {
            // 合約未回檢查
            if ($('[id*=ddlContractSend]').val() == '<%= (int)ContractSendStatus.None %>') {
                alert('請選擇合約送交狀態!!');
                $('[id*=ddlContractSend]').focus();
                return false;
            } else if ($('[id*=ddlContractSend]').val() == '<%= (int)ContractSendStatus.Others %>' && $.trim($('[id*=txtNoContractReason]').val()) == '') {
                alert('請填寫合約未回原因!!');
                $('[id*=txtNoContractReason]').focus();
                return false;
            }
            if ($.trim($('[id*=txtSalesName]').val()) == '') {
                alert('請填寫業務名稱!!');
                $('[id*=txtSalesName]').focus();
                return false;
            }
            return check_validgroup();
        }
        function check_validgroup() {
            var type = $('#<%= ddl_Vourcher_Type.ClientID %> option:selected').val();
            var is_valid = false;
            if (type == 0) {
                is_valid = window.Page_ClientValidate('DirectDiscount');
            }
            else if (type == 1) {
                is_valid = window.Page_ClientValidate('DirectOffset');
            }
            else if (type == 2) {
                is_valid = window.Page_ClientValidate('DiscountSecondOne');
            }
            else if (type == 3) {
                is_valid = window.Page_ClientValidate('ParticularProduct');
            }
            else if (type == 4) {
                is_valid = window.Page_ClientValidate('Upgrade');
            }
            else if (type == 5) {
                is_valid = window.Page_ClientValidate('Gift');
            }
            else if (type == 6) {
                is_valid = window.Page_ClientValidate('CustomersGetOneFree');
            }
            else if (type == 7) {
                is_valid = window.Page_ClientValidate('ConsumptionGetOnFree');
            }
            else if (type == 8) {
                is_valid = window.Page_ClientValidate('BirthDay');
            }
            else if (type == 9) {
                is_valid = window.Page_ClientValidate('Other');
            }
            else if (type == 10) {
                is_valid = window.Page_ClientValidate('Lottery');
            }
            return is_valid;
        }
        function NaviToNew() {
            var is_valid = false;
            is_valid = window.Page_ClientValidate('OpenNew');
            if (is_valid) {
                window.open('vourcher.aspx?mode=newadd&sid=' + $('#<%=tbx_SellerId.ClientID %>').val());
            }
            else {
                return is_valid;
            }
        }
        function select_all_stores() {
            if ($('#cbx_SelectAll').attr('checked')) {
                $('#pagerstores tr').find('input:checkbox').attr('checked', true);
            }
            else {
                $('#pagerstores tr').find('input:checkbox').attr('checked', false);
            }
        }
        function ShowCommercialList(obj) {
            if ($(obj).is(':checked')) {
                $(obj).next().next('#CommercialCategoryList').css('display', '');
            } else {
                $(obj).next().next('#CommercialCategoryList').css('display', 'none');
                $(obj).next().next('#CommercialCategoryList').find(':checked').each(function () {
                    $(this).attr('checked', false);
                });
            }
        }
    </script>
    <link type="text/css" href="../Themes/default/images/17Life/Gactivities/vourcher.css"
        rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MC" runat="server">
    <div style="margin-bottom: 100px;">
        <asp:HiddenField ID="hif_VourcherEventId" runat="server" />
        <asp:HiddenField ID="hif_SellerGuid" runat="server" />
        <asp:HiddenField ID="hif_SellerId" runat="server" />
        <asp:Panel ID="pan_SearchVourcherEvent" runat="server">
            <span class="floattitle"><span style="font-size: medium; color: #4169E1">優惠券列表</span>>編輯優惠券</span>
            <fieldset>
                <legend>優惠券活動</legend>
                <ul>
                    <li><span class="spantitle">搜尋條件</span>
                        <asp:DropDownList ID="ddl_Search" runat="server">
                        </asp:DropDownList>
                        <asp:TextBox ID="tbx_Search" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfv_Search" runat="server" ErrorMessage="*" ForeColor="Red"
                            Display="Dynamic" ControlToValidate="tbx_Search" ValidationGroup="search" SetFocusOnError="true"></asp:RequiredFieldValidator>
                        <asp:Button ID="btn_Search" runat="server" Text="搜尋" OnClick="SearchVourche" ValidationGroup="search" /></li>
                    <li style="display:none;"><span class="spantitle">賣家編號:</span><asp:TextBox ID="tbx_SellerId" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfv_SellerId" runat="server" ErrorMessage="*" ForeColor="Red"
                            Display="Dynamic" ControlToValidate="tbx_SellerId" ValidationGroup="OpenNew"></asp:RequiredFieldValidator>
                        <input id="btn_NewPage" type="button" value="新增活動" onclick="NaviToNew();" />
                    </li>
                </ul>
                <asp:Repeater ID="rpt_VourcherEventList" runat="server" OnItemCommand="VourcherEventListItemCommand">
                    <HeaderTemplate>
                        <table class="tablecontent">
                            <tr>
                                <td class="tablehead">預覽
                                </td>
                                <td class="tablehead">賣家編號
                                </td>
                                <td class="tablehead">品牌名稱
                                </td>
                                <td class="tablehead">優惠內容
                                </td>
                                <td class="tablehead">數量限制
                                </td>
                                <td class="tablehead">活動期間
                                </td>
                                <td class="tablehead">合約狀態
                                </td>
                                <td class="tablehead">狀態
                                </td>
                                <td class="tablehead">合約
                                </td>
                            </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td>
                                <asp:HyperLink ID="hyp_PreView" runat="server" Target="_blank" NavigateUrl='<%# "~/Vourcher/vourcher_preview.aspx?eventid="+((ViewVourcherSeller)(Container.DataItem)).Id%>'><%# ((ViewVourcherSeller)(Container.DataItem)).Id%></asp:HyperLink>
                            </td>
                            <td>
                                <%# ((ViewVourcherSeller)(Container.DataItem)).SellerId%>
                            </td>
                            <td>
                                <asp:HyperLink ID="hyp_Seller" runat="server" Target="_blank" Enabled="false" NavigateUrl='<%# "~/Vourcher/vourcherseller.aspx?mode=seller&guid="+((ViewVourcherSeller)(Container.DataItem)).SellerGuid%>'
                                    ForeColor="#CD5C5C"><%# ((ViewVourcherSeller)(Container.DataItem)).SellerName%></asp:HyperLink>
                            </td>
                            <td>
                                <asp:LinkButton ID="lbt_Vourcher_Edit" runat="server" CommandName="EditVourcherEvent"
                                    CommandArgument="<%# ((ViewVourcherSeller)(Container.DataItem)).Id %>"><%# ((ViewVourcherSeller)(Container.DataItem)).Contents%></asp:LinkButton>
                            </td>
                            <td>
                                <%# Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, (VourcherEventMode)(((ViewVourcherSeller)(Container.DataItem)).Mode))%>
                            </td>
                            <td>
                                <%# ((ViewVourcherSeller)(Container.DataItem)).StartDate.HasValue?((ViewVourcherSeller)(Container.DataItem)).StartDate.Value.ToString("yyyy/MM/dd"):string.Empty%>~<br />
                                <%# ((ViewVourcherSeller)(Container.DataItem)).EndDate.HasValue ? ((ViewVourcherSeller)(Container.DataItem)).EndDate.Value.ToString("yyyy/MM/dd") : string.Empty%>
                            </td>
                            <td>
                                <%# ((ViewVourcherSeller)(Container.DataItem)).NoContractReason %>
                            </td>
                            <td>
                                <%#Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, (VourcherEventStatus)(((ViewVourcherSeller)(Container.DataItem)).Status))%>
                            </td>
                            <td>
                                <asp:HyperLink ID="hyp_Print1" runat="server" Target="_blank" NavigateUrl='<%# "~/Vourcher/vourcher_print.aspx?eid="+((ViewVourcherSeller)(Container.DataItem)).Id+"&sid="+((ViewVourcherSeller)(Container.DataItem)).SellerGuid%>'
                                    ForeColor="#CD5C5C" Enabled=' <%# (VourcherEventStatus)(((ViewVourcherSeller)(Container.DataItem)).Status) != VourcherEventStatus.ReturnApply  %>'>優惠券</asp:HyperLink><br />
                                <asp:HyperLink ID="hyp_Print2" runat="server" Target="_blank" NavigateUrl='<%# "~/Vourcher/vourcher_print.aspx?sid="+((ViewVourcherSeller)(Container.DataItem)).SellerGuid%>'
                                    ForeColor="#CD5C5C">賣家</asp:HyperLink>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
                <uc1:Pager ID="gridPager" runat="server" PageSize="15" OnGetCount="RetrieveDataCount"
                    OnUpdate="UpdateHandler" />
            </fieldset>
            <fieldset>
                <legend>訊息通知</legend>
                <ul style="display: none">
                    <li><span class="spantitle">狀態</span>
                        <asp:DropDownList ID="ddl_EventStatus" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ChangeEventStatus">
                        </asp:DropDownList>
                    </li>
                </ul>
                <asp:Repeater ID="rpt_ReturnCase" runat="server" OnItemCommand="VourcherEventListItemCommand">
                    <HeaderTemplate>
                        <table class="tablecontent">
                            <tr>
                                <td class="tablehead">編號
                                </td>
                                <td class="tablehead">賣家編號
                                </td>
                                <td class="tablehead">品牌名稱
                                </td>
                                <td class="tablehead">優惠內容
                                </td>
                                <td class="tablehead">合約狀態
                                </td>
                                <td class="tablehead">狀態
                                </td>
                                <td class="tablehead">訊息
                                </td>
                                <td class="tablehead">合約
                                </td>
                            </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td>
                                <asp:HyperLink ID="hyp_PreView" runat="server" Target="_blank" NavigateUrl='<%# "~/Vourcher/vourcher_preview.aspx?eventid="+((ViewVourcherSeller)(Container.DataItem)).Id%>'><%# ((ViewVourcherSeller)(Container.DataItem)).Id%></asp:HyperLink>
                            </td>
                            <td>
                                <%# ((ViewVourcherSeller)(Container.DataItem)).SellerId%>
                            </td>
                            <td>
                                <asp:HyperLink ID="hyp_Seller" runat="server" Target="_blank" Enabled="false" NavigateUrl='<%# "~/Vourcher/vourcherseller.aspx?mode=seller&guid="+((ViewVourcherSeller)(Container.DataItem)).SellerGuid%>'
                                    ForeColor="#CD5C5C"><%# ((ViewVourcherSeller)(Container.DataItem)).SellerName%></asp:HyperLink>
                            </td>
                            <td>
                                <asp:LinkButton ID="lbt_Vourcher_Edit" runat="server" CommandName="EditVourcherEvent"
                                    CommandArgument="<%# ((ViewVourcherSeller)(Container.DataItem)).Id %>"><%# ((ViewVourcherSeller)(Container.DataItem)).Contents%></asp:LinkButton>
                            </td>
                            <td>
                                <%# ((ViewVourcherSeller)(Container.DataItem)).NoContractReason %>
                            </td>
                            <td>
                                <%#
                               ( (VourcherEventStatus)(((ViewVourcherSeller)(Container.DataItem)).Status)==VourcherEventStatus.ReturnApply?"<span style='color:red'>":"<span>")+
                                Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, (VourcherEventStatus)(((ViewVourcherSeller)(Container.DataItem)).Status))+"</span>"%>
                                <%# (VourcherEventStatus)(((ViewVourcherSeller)(Container.DataItem)).Status) == VourcherEventStatus.ApplyEvent ? 
                                ((ViewVourcherSeller)(Container.DataItem)).CreateTime.Value.ToString("MM/dd hh:mm") : ((ViewVourcherSeller)(Container.DataItem)).ReturnTime.Value.ToString("MM/dd hh:mm")%>
                            </td>
                            <td>
                                <%#((ViewVourcherSeller)(Container.DataItem)).Message%>
                            </td>
                            <td>
                                <asp:HyperLink ID="hyp_Print" runat="server" Target="_blank" NavigateUrl='<%# "~/Vourcher/vourcher_print.aspx?eid="+((ViewVourcherSeller)(Container.DataItem)).Id+"&sid="+((ViewVourcherSeller)(Container.DataItem)).SellerGuid%>'
                                    ForeColor="#CD5C5C" Enabled=' <%# (VourcherEventStatus)(((ViewVourcherSeller)(Container.DataItem)).Status) != VourcherEventStatus.ReturnApply  %>'>優惠券</asp:HyperLink><br />
                                <asp:HyperLink ID="hyp_Print2" runat="server" Target="_blank" NavigateUrl='<%# "~/Vourcher/vourcher_print.aspx?sid="+((ViewVourcherSeller)(Container.DataItem)).SellerGuid%>'
                                    ForeColor="#CD5C5C">賣家</asp:HyperLink>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
            </fieldset>
        </asp:Panel>
        <asp:Panel ID="pan_VourcherEvent" runat="server" Visible="false">
            <span class="floattitle">優惠券列表><span style="font-size: medium; color: #4169E1">編輯優惠券(<asp:Label
                ID="lab_CurrentStatus" runat="server"></asp:Label>)</span></span>
            <fieldset>
                <legend>優惠券活動</legend><span style="font-weight: bold; color: Red;">■行銷審閱後若無疑義，將於 5 個工作天內上線，優惠時間為上線後起算六個月。</span>
                <ul>
                    <li><span class="spantitle">優惠券編號:</span>
                        <asp:Label ID="lab_EventID" runat="server" Font-Bold="true" ForeColor="Brown"></asp:Label>
                    </li>
                    <li>
                        <span class="spantitle">合約狀態:</span>
                        <asp:DropDownList ID="ddlContractSend" runat="server" Enabled="false">
                        </asp:DropDownList>
                        <asp:TextBox ID="txtNoContractReason" Text="" runat="server" style="display: none;" Enabled="false"></asp:TextBox>
                    </li>
                    <li>
                        <span class="spantitle">負責業務:</span>
                        <asp:TextBox ID="txtSalesName" runat="server" Width="80px"></asp:TextBox>
                    </li>
                    <li style="display: none"><span class="spantitle">活動名稱:</span><asp:TextBox ID="tbx_Vourcher_EventName"
                        runat="server"></asp:TextBox></li>
                    <li><span class="spantitle">活動類型:</span><asp:DropDownList ID="ddl_Vourcher_Type"
                        OnSelectedIndexChanged="ChangeInputPanel" runat="server" AutoPostBack="true"
                        CausesValidation="false">
                    </asp:DropDownList>
                    </li>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="ddl_Vourcher_Type" EventName="SelectedIndexChanged" />
                        </Triggers>
                        <ContentTemplate>
                            <asp:Panel ID="pan_DirectDiscount" runat="server">
                                <li><span class="spantitle">優惠內容:</span> 憑券，消費享<asp:TextBox ID="tbx_DirectDiscount_Discount"
                                    runat="server" Width="50"></asp:TextBox>折優惠
                                    <asp:RequiredFieldValidator ID="rfv_DirectDiscount_Discount" runat="server" ErrorMessage="*"
                                        ForeColor="Red" Display="Dynamic" ControlToValidate="tbx_DirectDiscount_Discount"
                                        ValidationGroup="DirectDiscount" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="rev_DirectDiscount_Discount" runat="server" ErrorMessage="*"
                                        ForeColor="Red" Display="Dynamic" ControlToValidate="tbx_DirectDiscount_Discount"
                                        ValidationGroup="DirectDiscount" SetFocusOnError="true" ValidationExpression="^[1-9]+(\.[0-9]{1,2})?$"></asp:RegularExpressionValidator>
                                </li>
                                <li style="height: 160px"><span class="spantitle">注意事項:</span> <span class="cbx_font">
                                    <asp:CheckBox ID="cbx_DirectDiscount_AllTable" runat="server" Text="本券優惠全桌適用" /><br />
                                    <asp:CheckBox ID="cbx_DirectDiscount_OneMeal" runat="server" Text="本券限優惠乙份餐點(乙位客人)，以價低者計"
                                        CssClass="cbx_margin" /><br />
                                    <asp:CheckBox ID="cbx_DirectDiscount_ServiceFee" runat="server" Text="無服務費" CssClass="cbx_margin" /><br />
                                    <asp:CheckBox ID="cbx_DirectDiscount_ServiceFeeAdditional" runat="server" Text="服務費另計"
                                        CssClass="cbx_margin" /><br />
                                    <asp:CheckBox ID="cbx_DirectDiscount_ConsumptionApplied" runat="server" Text="消費即享優惠"
                                        CssClass="cbx_margin" /><br />
                                    <label class="cbx_margin">
                                        <asp:CheckBox ID="cbx_DirectDiscount_MinConsumption" runat="server" />單筆消費需滿NT$</label>
                                    <asp:TextBox ID="tbx_DirectDiscount_MinConsumption" runat="server" Width="50" ToolTip="單筆消費需滿NT${0}"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="rev_DirectDiscount_MinConsumption" runat="server"
                                        ErrorMessage="*" ForeColor="Red" Display="Dynamic" ControlToValidate="tbx_DirectDiscount_MinConsumption"
                                        ValidationGroup="DirectDiscount" SetFocusOnError="true" ValidationExpression="\d*"></asp:RegularExpressionValidator>
                                </span></li>
                                <li style="height: 80px"><span class="spantitle">不適用:</span><span class="cbx_font">
                                    <asp:CheckBox ID="cbx_DirectDiscount_HolidayApplied" runat="server" Text="特殊假日" /><br />
                                    <asp:CheckBox ID="cbx_DirectDiscount_WeekendApplied" runat="server" Text="週六、週日"
                                        CssClass="cbx_margin" /><br />
                                    <label class="cbx_margin">
                                        <asp:CheckBox ID="cbx_DirectDiscount_OtherTimes" runat="server" />其他時段</label>
                                    <asp:TextBox ID="tbx_DirectDiscount_OtherTimes" runat="server" ToolTip="其他時段:{0}"></asp:TextBox>
                                </span></li>
                            </asp:Panel>
                            <asp:Panel ID="pan_DirectOffset" runat="server" Visible="false">
                                <li><span class="spantitle">優惠內容:</span> 憑券，消費可折抵<asp:TextBox ID="tbx_DirectOffset_DiscountPrice"
                                    runat="server" Width="50"></asp:TextBox>元
                                    <asp:RequiredFieldValidator ID="rfv_DirectOffset" runat="server" ErrorMessage="*"
                                        ForeColor="Red" Display="Dynamic" ControlToValidate="tbx_DirectOffset_DiscountPrice"
                                        ValidationGroup="DirectOffset" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="rev_DirectOffset" runat="server" ErrorMessage="*"
                                        ForeColor="Red" Display="Dynamic" ControlToValidate="tbx_DirectOffset_DiscountPrice"
                                        ValidationGroup="DirectOffset" SetFocusOnError="true" ValidationExpression="\d*"></asp:RegularExpressionValidator>
                                </li>
                                <li style="height: 120px"><span class="spantitle">注意事項:</span> <span class="cbx_font">
                                    <asp:CheckBox ID="cbx_DirectOffset_ServiceFee" runat="server" Text="無服務費" /><br />
                                    <asp:CheckBox ID="cbx_DirectOffset_ServiceFeeAdditional" runat="server" Text="服務費另計"
                                        CssClass="cbx_margin" /><br />
                                    <asp:CheckBox ID="cbx_DirectOffset_ConsumptionApplied" runat="server" Text="消費即享優惠"
                                        CssClass="cbx_margin" /><br />
                                    <label class="cbx_margin">
                                        <asp:CheckBox ID="cbx_DirectOffset_MinConsumption" runat="server" />單筆消費需滿NT$</label><asp:TextBox
                                            ID="tbx_DirectOffset_MinConsumption" runat="server" Width="50" ToolTip="單筆消費需滿NT${0}"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="rev_DirectOffset_MinConsumption" runat="server"
                                        ErrorMessage="*" ForeColor="Red" Display="Dynamic" ControlToValidate="tbx_DirectOffset_MinConsumption"
                                        ValidationGroup="DirectOffset" SetFocusOnError="true" ValidationExpression="\d*"></asp:RegularExpressionValidator></span></li>
                                <li style="height: 80px"><span class="spantitle">不適用:</span> <span class="cbx_font">
                                    <asp:CheckBox ID="cbx_DirectOffset_HolidayApplied" runat="server" Text="特殊假日" /><br />
                                    <asp:CheckBox ID="cbx_DirectOffset_WeekendApplied" runat="server" Text="週六、週日" CssClass="cbx_margin" /><br />
                                    <label class="cbx_margin">
                                        <asp:CheckBox ID="cbx_DirectOffset_OtherTimes" runat="server" />其他時段</label>
                                    <asp:TextBox ID="tbx_DirectOffset_OtherTimes" runat="server" ToolTip="其他時段:{0}"></asp:TextBox></span>
                                </li>
                            </asp:Panel>
                            <asp:Panel ID="pan_DiscountSecondOne" runat="server" Visible="false">
                                <li><span class="spantitle">優惠內容:</span> 憑券，消費享第二件<asp:TextBox ID="tbx_DiscountSecondOne_Discount"
                                    runat="server" Width="50"></asp:TextBox>折優惠
                                    <asp:RequiredFieldValidator ID="rfv_DiscountSecondOne" runat="server" ErrorMessage="*"
                                        ForeColor="Red" Display="Dynamic" ControlToValidate="tbx_DiscountSecondOne_Discount"
                                        ValidationGroup="DiscountSecondOne" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="rev_DiscountSecondOne_Discount" runat="server"
                                        ErrorMessage="*" ForeColor="Red" Display="Dynamic" ControlToValidate="tbx_DiscountSecondOne_Discount"
                                        ValidationGroup="DiscountSecondOne" SetFocusOnError="true" ValidationExpression="^[1-9]+(\.[0-9]{1,2})?$"></asp:RegularExpressionValidator>
                                </li>
                                <li style="height: 110px"><span class="spantitle">注意事項:</span> <span class="cbx_font">
                                    <asp:CheckBox ID="cbx_DiscountSecondOne_OneMeal" runat="server" Text="本券限優惠乙組，以價低者計" /><br />
                                    <asp:CheckBox ID="cbx_DiscountSecondOne_TheFourthApplied" runat="server" Text="使用本券第四件同享折扣，以價低者計，以此類推"
                                        CssClass="cbx_margin" /><br />
                                    <asp:CheckBox ID="cbx_DiscountSecondOne_ServiceFee" runat="server" Text="無服務費" CssClass="cbx_margin" /><br />
                                    <asp:CheckBox ID="cbx_DiscountSecondOne_ServiceFeeAdditional" runat="server" Text="服務費另計"
                                        CssClass="cbx_margin" />
                                </span></li>
                                <li style="height: 80px"><span class="spantitle">不適用:</span><span class="cbx_font">
                                    <asp:CheckBox ID="cbx_DiscountSecondOne_HolidayApplied" runat="server" Text="特殊假日" /><br />
                                    <asp:CheckBox ID="cbx_DiscountSecondOne_WeekendApplied" runat="server" Text="週六、週日"
                                        CssClass="cbx_margin" /><br />
                                    <label class="cbx_margin">
                                        <asp:CheckBox ID="cbx_DiscountSecondOne_OtherTimes" runat="server" />其他時段</label>
                                    <asp:TextBox ID="tbx_DiscountSecondOne_OtherTimes" runat="server" ToolTip="其他時段:{0}"></asp:TextBox></span>
                                </li>
                            </asp:Panel>
                            <asp:Panel ID="pan_ParticularProduct" runat="server" Visible="false">
                                <li><span class="spantitle">優惠內容:</span> 憑券，消費
                                    <asp:TextBox ID="tbx_ParticularProduct_Message_1" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfv_ParticularProduct_Message_1" runat="server" ErrorMessage="*"
                                        ForeColor="Red" Display="Dynamic" ControlToValidate="tbx_ParticularProduct_Message_1"
                                        ValidationGroup="ParticularProduct" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                    享優惠價NT$<asp:TextBox ID="tbx_ParticularProduct_DiscountPrice" runat="server" Width="50"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfv_ParticularProduct_DiscountPrice" runat="server"
                                        ErrorMessage="*" ForeColor="Red" Display="Dynamic" ControlToValidate="tbx_ParticularProduct_DiscountPrice"
                                        ValidationGroup="ParticularProduct" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="rev_ParticularProduct_DiscountPrice" runat="server"
                                        ErrorMessage="*" ForeColor="Red" Display="Dynamic" ControlToValidate="tbx_ParticularProduct_DiscountPrice"
                                        ValidationGroup="ParticularProduct" SetFocusOnError="true" ValidationExpression="\d*"></asp:RegularExpressionValidator>
                                    (原價NT$<asp:TextBox ID="tbx_ParticularProduct_OriginalPrice" runat="server" Width="50"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfv_ParticularProduct_OriginalPrice" runat="server"
                                        ErrorMessage="*" ForeColor="Red" Display="Dynamic" ControlToValidate="tbx_ParticularProduct_OriginalPrice"
                                        ValidationGroup="ParticularProduct" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="rev_ParticularProduct_OriginalPrice" runat="server"
                                        ErrorMessage="*" ForeColor="Red" Display="Dynamic" ControlToValidate="tbx_ParticularProduct_OriginalPrice"
                                        ValidationGroup="ParticularProduct" SetFocusOnError="true" ValidationExpression="\d*"></asp:RegularExpressionValidator>
                                    )</li>
                                <li style="height: 100px"><span class="spantitle">注意事項:</span> <span class="cbx_font">
                                    <asp:CheckBox ID="cbx_ParticularProduct_OneMeal" runat="server" Text="本券限優惠乙份餐點(產品)" /><br />
                                    <asp:CheckBox ID="cbx_ParticularProduct_MultipleMeals" runat="server" Text="本券不限優惠份數"
                                        CssClass="cbx_margin" /><br />
                                    <asp:CheckBox ID="cbx_ParticularProduct_ServiceFee" runat="server" Text="無服務費" CssClass="cbx_margin" /><br />
                                    <asp:CheckBox ID="cbx_ParticularProduct_ServiceFeeAdditional" runat="server" Text="服務費另計"
                                        CssClass="cbx_margin" />
                                </span></li>
                                <li style="height: 80px"><span class="spantitle">不適用:</span><span class="cbx_font">
                                    <asp:CheckBox ID="cbx_ParticularProduct_HolidayApplied" runat="server" Text="特殊假日" /><br />
                                    <asp:CheckBox ID="cbx_ParticularProduct_WeekendApplied" runat="server" Text="週六、週日"
                                        CssClass="cbx_margin" /><br />
                                    <label class="cbx_margin">
                                        <asp:CheckBox ID="cbx_ParticularProduct_OtherTimes" runat="server" />其他時段</label>
                                    <asp:TextBox ID="tbx_ParticularProduct_OtherTimes" runat="server" ToolTip="其他時段:{0}"></asp:TextBox></span>
                                </li>
                            </asp:Panel>
                            <asp:Panel ID="pan_Upgrade" runat="server" Visible="false">
                                <li><span class="spantitle">優惠內容:</span> 憑券，消費
                                    <asp:TextBox ID="tbx_Upgrade_Message_1" runat="server" Width="100"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfv_Upgrade_Message_1" runat="server" ErrorMessage="*"
                                        ForeColor="Red" Display="Dynamic" ControlToValidate="tbx_Upgrade_Message_1" ValidationGroup="Upgrade"
                                        SetFocusOnError="true"></asp:RequiredFieldValidator>
                                    享升等<asp:TextBox ID="tbx_Upgrade_Message_2" runat="server" Width="100"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfv_Upgrade_Message_2" runat="server" ErrorMessage="*"
                                        ForeColor="Red" Display="Dynamic" ControlToValidate="tbx_Upgrade_Message_2" ValidationGroup="Upgrade"
                                        SetFocusOnError="true"></asp:RequiredFieldValidator>
                                    ( 原升等價NT$<asp:TextBox ID="tbx_Upgrade_OriginalPrice" runat="server" Width="50"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfv_Upgrade_OriginalPrice" runat="server" ErrorMessage="*"
                                        ForeColor="Red" Display="Dynamic" ControlToValidate="tbx_Upgrade_OriginalPrice"
                                        ValidationGroup="Upgrade" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="rev_Upgrade_OriginalPrice" runat="server" ErrorMessage="*"
                                        ForeColor="Red" Display="Dynamic" ControlToValidate="tbx_Upgrade_OriginalPrice"
                                        ValidationGroup="Upgrade" SetFocusOnError="true" ValidationExpression="\d*"></asp:RegularExpressionValidator>
                                    )</li>
                                <li style="height: 110px"><span class="spantitle">注意事項:</span> <span class="cbx_font">
                                    <asp:CheckBox ID="cbx_Upgrade_OneMeal" runat="server" Text="本券限優惠乙份餐點(產品)" /><br />
                                    <asp:CheckBox ID="cbx_Upgrade_MultipleMeals" runat="server" Text="本券不限優惠份數" CssClass="cbx_margin" /><br />
                                    <asp:CheckBox ID="cbx_Upgrade_ServiceFee" runat="server" Text="無服務費" CssClass="cbx_margin" /><br />
                                    <asp:CheckBox ID="cbx_Upgrade_ServiceFeeAdditional" runat="server" Text="服務費另計" CssClass="cbx_margin" />
                                </span></li>
                                <li style="height: 80px"><span class="spantitle">不適用:</span><span class="cbx_font">
                                    <asp:CheckBox ID="cbx_Upgrade_HolidayApplied" runat="server" Text="特殊假日" /><br />
                                    <asp:CheckBox ID="cbx_Upgrade_WeekendApplied" runat="server" Text="週六、週日" CssClass="cbx_margin" /><br />
                                    <label class="cbx_margin">
                                        <asp:CheckBox ID="cbx_Upgrade_OtherTimes" runat="server" />其他時段</label>
                                    <asp:TextBox ID="tbx_Upgrade_OtherTimes" runat="server" ToolTip="其他時段:{0}"></asp:TextBox></span>
                                </li>
                            </asp:Panel>
                            <asp:Panel ID="pan_Gift" runat="server" Visible="false">
                                <li><span class="spantitle">優惠內容:</span> 憑券，消費贈<asp:TextBox ID="tbx_Gift_Message_1"
                                    runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfv_Gift_Message_1" runat="server" ErrorMessage="*"
                                        ForeColor="Red" Display="Dynamic" ControlToValidate="tbx_Gift_Message_1" ValidationGroup="Gift"
                                        SetFocusOnError="true"></asp:RequiredFieldValidator>
                                    乙份 ( 原價NT$<asp:TextBox ID="tbx_Gift_OriginalPrice" runat="server" Width="50"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfv_Gift_OriginalPrice" runat="server" ErrorMessage="*"
                                        ForeColor="Red" Display="Dynamic" ControlToValidate="tbx_Gift_OriginalPrice"
                                        ValidationGroup="Gift" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="rev_Gift_OriginalPrice" runat="server" ErrorMessage="*"
                                        ForeColor="Red" Display="Dynamic" ControlToValidate="tbx_Gift_OriginalPrice"
                                        ValidationGroup="Gift" SetFocusOnError="true" ValidationExpression="\d*"></asp:RegularExpressionValidator>
                                    )</li>
                                <li style="height: 260px"><span class="spantitle">注意事項:</span> <span class="cbx_font">
                                    <asp:CheckBox ID="cbx_Gift_OneMeal" runat="server" Text="本券限優惠乙份餐點(產品)" /><br />
                                    <asp:CheckBox ID="cbx_Gift_AllTable" runat="server" Text="本券優惠全桌適用，依消費人數贈送" CssClass="cbx_margin" /><br />
                                    <asp:CheckBox ID="cbx_Gift_ServiceFee" runat="server" Text="無服務費" CssClass="cbx_margin" /><br />
                                    <asp:CheckBox ID="cbx_Gift_ServiceFeeAdditional" runat="server" Text="服務費另計" CssClass="cbx_margin" /><br />
                                    <asp:CheckBox ID="cbx_Gift_GiftAttending" runat="server" Text="到店直接送" CssClass="cbx_margin" /><br />
                                    <asp:CheckBox ID="cbx_Gift_GiftConsumption" runat="server" Text="消費就送" CssClass="cbx_margin" /><br />
                                    <label class="cbx_margin">
                                        <asp:CheckBox ID="cbx_Gift_GiftMinConsumption" runat="server" />消費滿NT$</label><asp:TextBox
                                            ID="tbx_Gift_GiftMinConsumption" runat="server" Width="50" ToolTip="消費滿NT${0}就送"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="rev_Gift_MinConsumption" runat="server" ErrorMessage="*"
                                        ForeColor="Red" Display="Dynamic" ControlToValidate="tbx_Gift_GiftMinConsumption"
                                        ValidationGroup="Gift" SetFocusOnError="true" ValidationExpression="\d*"></asp:RegularExpressionValidator>
                                    就送<br />
                                    <asp:CheckBox ID="cbx_Gift_GiftLimited" runat="server" Text="數量無限" CssClass="cbx_margin" /><br />
                                    <label class="cbx_margin">
                                        <asp:CheckBox ID="cbx_Gife_GiftQuantity" runat="server" />數量</label><asp:TextBox
                                            ID="tbx_Gife_GiftQuantity" runat="server" Width="50" ToolTip="數量{0}份送完為止"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="rev_GiftQuantity" runat="server" ErrorMessage="*"
                                        ForeColor="Red" Display="Dynamic" ControlToValidate="tbx_Gife_GiftQuantity" ValidationGroup="Gift"
                                        SetFocusOnError="true" ValidationExpression="\d*"></asp:RegularExpressionValidator>
                                    份送完為止<br />
                                    <asp:CheckBox ID="cbx_Gift_GiftReplacabe" runat="server" Text="送完將以等值商品替代" CssClass="cbx_margin" />
                                </span></li>
                            </asp:Panel>
                            <asp:Panel ID="pan_CustomersGetOneFree" runat="server" Visible="false">
                                <li><span class="spantitle">優惠內容:</span> 憑券，<asp:TextBox ID="tbx_CustomersGetOneFree_OriginalPrice"
                                    runat="server" Width="50"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfv_CustomersGetOneFree_OriginalPrice" runat="server"
                                        ErrorMessage="*" ForeColor="Red" Display="Dynamic" ControlToValidate="tbx_CustomersGetOneFree_OriginalPrice"
                                        ValidationGroup="CustomersGetOneFree" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="rev_CustomersGetOneFree_OriginalPrice" runat="server"
                                        ErrorMessage="*" ForeColor="Red" Display="Dynamic" ControlToValidate="tbx_CustomersGetOneFree_OriginalPrice"
                                        ValidationGroup="CustomersGetOneFree" SetFocusOnError="true" ValidationExpression="\d*"></asp:RegularExpressionValidator>
                                    人同行<asp:TextBox ID="tbx_CustomersGetOneFree_DiscountPrice" runat="server" Width="50"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfv_CustomersGetOneFree_DiscountPrice" runat="server"
                                        ErrorMessage="*" ForeColor="Red" Display="Dynamic" ControlToValidate="tbx_CustomersGetOneFree_DiscountPrice"
                                        ValidationGroup="CustomersGetOneFree" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="rev_CustomersGetOneFree_DiscountPrice" runat="server"
                                        ErrorMessage="*" ForeColor="Red" Display="Dynamic" ControlToValidate="tbx_CustomersGetOneFree_DiscountPrice"
                                        ValidationGroup="CustomersGetOneFree" SetFocusOnError="true" ValidationExpression="\d*"></asp:RegularExpressionValidator>
                                    人免費</li>
                                <li style="height: 100px"><span class="spantitle">注意事項:</span> <span class="cbx_font">
                                    <asp:CheckBox ID="cbx_CustomersGetOneFree_OneMeal" runat="server" Text="本券限優惠乙份餐點(乙位客人)，以價低者計" /><br />
                                    <asp:CheckBox ID="cbx_CustomersGetOneFree_AllTable" runat="server" Text="本券優惠全桌適用，依消費人數贈送"
                                        CssClass="cbx_margin" /><br />
                                    <asp:CheckBox ID="cbx_CustomersGetOneFree_ServiceFee" runat="server" Text="無服務費"
                                        CssClass="cbx_margin" /><br />
                                    <asp:CheckBox ID="cbx_CustomersGetOneFree_ServiceFeeAdditional" runat="server" Text="服務費另計"
                                        CssClass="cbx_margin" />
                                </span></li>
                            </asp:Panel>
                            <asp:Panel ID="pan_ConsumptionGetOnFree" runat="server" Visible="false">
                                <li><span class="spantitle">優惠內容:</span> 憑券，
                                    <asp:TextBox ID="tbx_ConsumptionGetOnFree_Message_1" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfv_ConsumptionGetOnFree_Message_1" runat="server"
                                        ErrorMessage="*" ForeColor="Red" Display="Dynamic" ControlToValidate="tbx_ConsumptionGetOnFree_Message_1"
                                        ValidationGroup="ConsumptionGetOnFree" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                    買<asp:TextBox ID="tbx_ConsumptionGetOnFree_DiscountPrice" runat="server" Width="50"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfv_ConsumptionGetOnFree_DiscountPrice" runat="server"
                                        ErrorMessage="*" ForeColor="Red" Display="Dynamic" ControlToValidate="tbx_ConsumptionGetOnFree_DiscountPrice"
                                        ValidationGroup="ConsumptionGetOnFree" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="rev_ConsumptionGetOnFree_DiscountPrice" runat="server"
                                        ErrorMessage="*" ForeColor="Red" Display="Dynamic" ControlToValidate="tbx_ConsumptionGetOnFree_DiscountPrice"
                                        ValidationGroup="ConsumptionGetOnFree" SetFocusOnError="true" ValidationExpression="\d*"></asp:RegularExpressionValidator>
                                    送1( 原價NT$
                                    <asp:TextBox ID="tbx_ConsumptionGetOnFree_OriginalPrice" runat="server" Width="50"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfv_ConsumptionGetOnFree_OriginalPrice" runat="server"
                                        ErrorMessage="*" ForeColor="Red" Display="Dynamic" ControlToValidate="tbx_ConsumptionGetOnFree_OriginalPrice"
                                        ValidationGroup="ConsumptionGetOnFree" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="rev_tConsumptionGetOnFree_OriginalPrice" runat="server"
                                        ErrorMessage="*" ForeColor="Red" Display="Dynamic" ControlToValidate="tbx_ConsumptionGetOnFree_OriginalPrice"
                                        ValidationGroup="ConsumptionGetOnFree" SetFocusOnError="true" ValidationExpression="\d*"></asp:RegularExpressionValidator>
                                    )</li>
                                <li style="height: 100px"><span class="spantitle">注意事項:</span> <span class="cbx_font">
                                    <asp:CheckBox ID="cbx_ConsumptionGetOnFree_OneMeal" runat="server" Text="本券限贈乙份，以價低者計" /><br />
                                    <asp:CheckBox ID="cbx_ConsumptionGetOnFree_AllTable" runat="server" Text="本券優惠全桌適用，依消費人數贈送"
                                        CssClass="cbx_margin" /><br />
                                    <asp:CheckBox ID="cbx_ConsumptionGetOnFree_ServiceFee" runat="server" Text="無服務費"
                                        CssClass="cbx_margin" /><br />
                                    <asp:CheckBox ID="cbx_ConsumptionGetOnFree_ServiceFeeAdditional" runat="server" Text="服務費另計"
                                        CssClass="cbx_margin" />
                                </span></li>
                            </asp:Panel>
                            <asp:Panel ID="pan_BirthDay" runat="server" Visible="false">
                                <li><span class="spantitle">優惠內容:</span> 憑券+當月壽星證明，享<asp:TextBox ID="tbx_BirthDay_Message_1"
                                    runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfv_BirthDay_Message_1" runat="server" ErrorMessage="*"
                                        ForeColor="Red" Display="Dynamic" ControlToValidate="tbx_BirthDay_Message_1"
                                        ValidationGroup="BirthDay" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                </li>
                                <li style="height: 160px"><span class="spantitle">注意事項:</span> <span class="cbx_font">
                                    <asp:CheckBox ID="cbx_BirthDay_OneMeal" runat="server" Text="本券限優惠乙份餐點(乙位客人)，以價低者計" /><br />
                                    <asp:CheckBox ID="cbx_BirthDay_AllTable" runat="server" Text="本券優惠全桌適用" CssClass="cbx_margin" /><br />
                                    <asp:CheckBox ID="cbx_BirthDay_MultipleMeals" runat="server" Text="不限人數" CssClass="cbx_margin" /><br />
                                    <label class="cbx_margin">
                                        <asp:CheckBox ID="cbx_BirthDay_MinPersons" runat="server" />需<asp:TextBox ID="tbx_BirthDay_MinPersons"
                                            runat="server" Width="50" ToolTip="需{0}人以上同行"></asp:TextBox>
                                        <asp:RegularExpressionValidator ID="rev_BirthDay_MinPersons" runat="server" ErrorMessage="*"
                                            ForeColor="Red" Display="Dynamic" ControlToValidate="tbx_BirthDay_MinPersons"
                                            ValidationGroup="BirthDay" SetFocusOnError="true" ValidationExpression="\d*"></asp:RegularExpressionValidator>
                                        人以上同行</label>
                                    <br />
                                    <asp:CheckBox ID="cbx_BirthDay_ServiceFee" runat="server" Text="無服務費" CssClass="cbx_margin" /><br />
                                    <asp:CheckBox ID="cbx_BirthDay_ServiceFeeAdditional" runat="server" Text="服務費另計"
                                        CssClass="cbx_margin" />
                                </span></li>
                            </asp:Panel>
                            <asp:Panel ID="pan_Other" runat="server" Visible="false">
                                <li><span class="spantitle">優惠內容:</span>憑券，<asp:TextBox ID="tbx_Other_Message_1"
                                    runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfv_Other_Message_1" runat="server" ErrorMessage="*"
                                        ForeColor="Red" Display="Dynamic" ControlToValidate="tbx_Other_Message_1" ValidationGroup="Other"
                                        SetFocusOnError="true"></asp:RequiredFieldValidator>
                                </li>
                                <li style="height: 60px"><span class="spantitle">注意事項:</span> <span class="cbx_font">
                                    <asp:CheckBox ID="cbx_Other_ServiceFee" runat="server" Text="無服務費" /><br />
                                    <asp:CheckBox ID="cbx_Other_ServiceFeeAdditional" runat="server" Text="服務費另計" CssClass="cbx_margin" />
                                </span></li>
                            </asp:Panel>
                            <asp:Panel ID="pan_Lottery" runat="server" Visible="false">
                                <li><span class="spantitle">優惠內容:</span><asp:TextBox ID="tbx_Lottery_Message_1"
                                    runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfv_Lottery_Message_1" runat="server" ErrorMessage="*"
                                        ForeColor="Red" Display="Dynamic" ControlToValidate="tbx_Lottery_Message_1" ValidationGroup="Lottery"
                                        SetFocusOnError="true"></asp:RequiredFieldValidator>
                                </li>
                            </asp:Panel>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <li style="height: 100px"><span class="spantitle">其他:</span>
                        <asp:TextBox ID="tbx_Others" runat="server" TextMode="MultiLine" Rows="3" Width="500"></asp:TextBox>
                    </li>
                    <li><span class="spantitle">數量限制:</span><asp:DropDownList ID="ddl_Vourcher_Mode"
                        runat="server">
                    </asp:DropDownList>
                        <span class="maxquantity" style="display: none;">數量:
                            <asp:TextBox ID="tbx_MaxQuantity" runat="server" Width="50"></asp:TextBox></span>
                    </li>
                    <li><span class="spantitle">雜誌合作:</span><span class="cbx_font"><asp:CheckBox ID="cbx_Magazine"
                        runat="server" Text="不願意同時刊登於17Life雜誌" /></span> </li>
                    <li style="height: auto;"><span class="spantitle">生活商圈:</span>
                        <asp:Repeater ID="rptCommercialCategory" runat="server" OnItemDataBound="rptCommercialCategory_ItemDataBound">
                            <HeaderTemplate>
                                <table style="margin-left: 120px;">
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td>
                                        <span class="cbx_font divCommercial">
                                            <asp:HiddenField ID="hidCommercialCategoryId" runat="server"></asp:HiddenField>
                                            <asp:CheckBox ID="chkCommercialArea" runat="server" onclick="ShowCommercialList(this);" />
                                            <span id="CommercialCategoryList" class="cbx_font" style="font-size: small; display: none;">
                                                <asp:CheckBoxList ID="chklCommercialCategory" runat="server" RepeatDirection="Horizontal" RepeatColumns="7" RepeatLayout="Table">
                                                </asp:CheckBoxList></span>
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="line-height: 10px;">&nbsp;</td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                        </asp:Repeater>
                    </li>
                    <li style="height: 100px;"><span class="spantitle">分類:</span>
                        <table style="margin-left: 120px;">
                            <tr>
                                <td>
                                    <span id="CommercialCategoryList" class="cbx_font" style="font-size: small;">
                                        <asp:CheckBoxList ID="cbx_Category" runat="server" RepeatDirection="Horizontal" RepeatColumns="6" RepeatLayout="Table"></asp:CheckBoxList>
                                    </span>
                                </td>
                            </tr>
                        </table>
                    </li>
                    <li style="height: 320px;"><span class="spantitle">圖檔上傳:</span><span class="cbx_font"><label>僅支援jpg或gif檔3Mb以內</label></span>
                        <table style="margin-left: 100px">
                            <tr>
                                <td>1.<asp:FileUpload ID="fu_Vourcher_Img_1" runat="server" />
                                </td>
                                <td>2.<asp:FileUpload ID="fu_Vourcher_Img_2" runat="server" />
                                </td>
                                <td>3.<asp:FileUpload ID="fu_Vourcher_Img_3" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 90px;">
                                    <asp:Image ID="img_VourcherEvent_1" runat="server" AlternateText="無上傳照片" Height="90" /><asp:CheckBox
                                        ID="cbx_Delete_Img_1" runat="server" Text="刪除" Font-Size="Smaller" />
                                </td>
                                <td>
                                    <asp:Image ID="img_VourcherEvent_2" runat="server" AlternateText="無上傳照片" Height="90" />
                                    <asp:CheckBox ID="cbx_Delete_Img_2" runat="server" Text="刪除" Font-Size="Smaller" />
                                </td>
                                <td>
                                    <asp:Image ID="img_VourcherEvent_3" runat="server" AlternateText="無上傳照片" Height="90" />
                                    <asp:CheckBox ID="cbx_Delete_Img_3" runat="server" Text="刪除" Font-Size="Smaller" />
                                </td>
                            </tr>
                            <tr>
                                <td>4.<asp:FileUpload ID="fu_Vourcher_Img_4" runat="server" />
                                </td>
                                <td>5.<asp:FileUpload ID="fu_Vourcher_Img_5" runat="server" />
                                </td>
                                <td>6.<asp:FileUpload ID="fu_Vourcher_Img_6" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 90px;">
                                    <asp:Image ID="img_VourcherEvent_4" runat="server" AlternateText="無上傳照片" Height="90" /><asp:CheckBox
                                        ID="cbx_Delete_Img_4" runat="server" Text="刪除" Font-Size="Smaller" />
                                </td>
                                <td>
                                    <asp:Image ID="img_VourcherEvent_5" runat="server" AlternateText="無上傳照片" Height="90" /><asp:CheckBox
                                        ID="cbx_Delete_Img_5" runat="server" Text="刪除" Font-Size="Smaller" />
                                </td>
                                <td>
                                    <asp:Image ID="img_VourcherEvent_6" runat="server" AlternateText="無上傳照片" Height="90" /><asp:CheckBox
                                        ID="cbx_Delete_Img_6" runat="server" Text="刪除" Font-Size="Smaller" />
                                </td>
                            </tr>
                        </table>
                    </li>
                </ul>
            </fieldset>
            <fieldset>
                <legend>分店選取</legend>
                <table style="width: 100%">
                    <tr>
                        <td style="width: 50%">
                            <ul>
                                <li><span class="spantitle">品牌名稱</span><asp:Label ID="lab_SellerName" runat="server"></asp:Label></li>
                                <li><span class="spantitle">簽約公司名稱</span><asp:Label ID="lab_ComapnyName" runat="server"></asp:Label></li>
                            </ul>
                        </td>
                        <td>
                            <ul>
                                <li><span class="spantitle">賣家編號</span><asp:Label ID="lab_SellerId" runat="server"></asp:Label></li>
                                <li><span class="spantitle">統一編號/ID</span><asp:Label ID="lab_CompanyId" runat="server"></asp:Label></li>
                            </ul>
                        </td>
                    </tr>
                </table>
                <table class="tablecontent">
                    <tr>
                        <td class="tablehead" style="width: 20%;">店名
                        </td>
                        <td class="tablehead" style="width: 15%;">店家電話
                        </td>
                        <td class="tablehead" style="width: 15%;">統一編號
                        </td>
                        <td class="tablehead" style="width: 30%;">地址
                        </td>
                        <td class="tablehead" style="width: 20%;">選取 (全選<input id="cbx_SelectAll" type="checkbox" onclick="select_all_stores();" />)
                        </td>
                    </tr>
                </table>
                <asp:Repeater ID="rpt_TempStores" runat="server">
                    <HeaderTemplate>
                        <table id="pagerstores" class="tablecontent" style="width: 100%">
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr style="display: table-row;">
                            <td style="width: 20%;">
                                <%# ((Store)(Container.DataItem)).StoreName%>
                            </td>
                            <td style="width: 15%;">
                                <%# ((Store)(Container.DataItem)).Phone%>
                            </td>
                            <td style="width: 15%;">
                                <%# ((Store)(Container.DataItem)).SignCompanyID%>
                            </td>
                            <td style="width: 30%;">
                                <%# CityManager.CityTownShopStringGet(((Store)(Container.DataItem)).TownshipId ?? 0) + ((Store)(Container.DataItem)).AddressString%>
                            </td>
                            <td style="width: 20%;">
                                <asp:HiddenField ID="hif_StoreGuid" runat="server" Value="<%# ((Store)(Container.DataItem)).Guid%>" />
                                <asp:CheckBox ID="cbx_Store" runat="server" />
                                <span class="storemaxquantity" style="display: none;"><span style="margin-left: 10px;">數量:</span><asp:TextBox ID="tbx_StoreMaxQuantity" runat="server" Width="50"></asp:TextBox></sapn>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
                <ul>
                    <li style="text-align: center">
                        <asp:Label ID="lab_Vourcher_SetUpMessage" runat="server" ForeColor="Red" Font-Size="Small"></asp:Label>
                    </li>
                    <asp:Panel ID="pan_Return" runat="server" Visible="false">
                        <li><span class="spantitle" style="color: Blue;">退件訊息:</span>
                            <asp:Label ID="lab_ReturnMessage" runat="server"></asp:Label>
                        </li>
                    </asp:Panel>
                    <li style="text-align: center">
                        <asp:Button ID="btn_AddNewVourcher" runat="server" Text="儲存" OnClick="SaveVourcherEvent"
                            OnClientClick="return Check_data();" Visible="false" />&nbsp;&nbsp;&nbsp;
                        <asp:Button ID="btn_AddNewVourcherAndOn" runat="server" Text="儲存，並建立下一筆" OnClick="SaveVourcherEvent"
                            OnClientClick="return Check_data();" Visible="false" />&nbsp;&nbsp;&nbsp;
                        <asp:Button ID="btn_Cancel" runat="server" Text="取消" OnClick="ReturnCancel" OnClientClick="return confirm('確定取消動作?');" />
                    </li>
                </ul>
            </fieldset>
        </asp:Panel>
        <a href="https://www.17life.com/images/doc/17Life_APP.pdf" style="color: Blue" target="_blank">列印空白合約</a>&nbsp;&nbsp;
        <a href="vourcherseller.aspx" style="color: Blue; display: none;" target="_blank">優惠券賣家</a>&nbsp;&nbsp;
        <asp:HyperLink ID="hyp_GoogleDoc" runat="server" Target="_blank" ForeColor="Blue" Visible="false"
            NavigateUrl="https://docs.google.com/a/17life.com/spreadsheet/viewform?formkey=dE52Q0tVQkhkMzdNS2NVcjU5Z256Vmc6MQ">申請修改/異動優惠券</asp:HyperLink>
    </div>
</asp:Content>

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Ppon/Ppon.Master" AutoEventWireup="true"
    CodeBehind="vourcher_print.aspx.cs" Inherits="LunchKingSite.Web.Vourcher.vourcher_print" %>

<%@ Import Namespace="LunchKingSite.Core" %>
<%@ Import Namespace="LunchKingSite.DataOrm" %>
<%@ Import Namespace="LunchKingSite.BizLogic.Component" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <script src="../Tools/js/effects/jquery.flexipage.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.tablecontent tr:odd').addClass('oddtr');
            $('#pagerstores').flexipage({ perpage: 10, element: 'tr' });
            $('.tablecontent tr').attr('style', 'display:table-row;');
            $('fieldset li').hover(function () { $(this).addClass('highlight_tr'); }, function () { $(this).removeClass('highlight_tr'); });
        });
    </script>
    <link type="text/css" href="../Themes/default/images/17Life/Gactivities/vourcher.css"
        rel="stylesheet" />
    <style type="text/css">
        li
        {
            height: 30px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MC" runat="server">
    <fieldset>
        <legend>紙本合約列印</legend>
        <table style="width: 100%">
            <tr>
                <td style="width: 50%;">
                    <ul>
                        <li><span class="spantitle">簽約公司名稱</span><asp:Label ID="lab_ComapnyName" runat="server"></asp:Label></li>
                        <li><span class="spantitle">負責人</span><asp:Label ID="lab_CompanyBossName" runat="server"></asp:Label></li>
                        <li><span class="spantitle">連絡人</span><asp:Label ID="lab_Seller_Boss_Name" runat="server"></asp:Label></li>
                        <li><span class="spantitle">連絡電話</span><asp:Label ID="lab_Seller_Tel" runat="server"></asp:Label></li>
                        <li><span class="spantitle">簽約公司地址</span><asp:Label ID="lab_Seller_Address" runat="server"></asp:Label></li>
                    </ul>
                </td>
                <td style="vertical-align: top;">
                    <ul>
                        <li><span class="spantitle">統一編號/ID</span><asp:Label ID="lab_CompanyId" runat="server"></asp:Label></li>
                        <li><span class="spantitle">行動電話</span><asp:Label ID="lab_Seller_Mobile" runat="server"></asp:Label></li>
                        <li><span class="spantitle">傳真電話</span><asp:Label ID="lab_Seller_Fax" runat="server"></asp:Label></li>
                        <li><span class="spantitle">電子信箱</span><asp:Label ID="lab_Seller_Email" runat="server"></asp:Label></li>
                    </ul>
                </td>
            </tr>
        </table>
        <span class="title">優惠券列表(<asp:Label ID="lab_SearchMode" runat="server"></asp:Label>)</span>
        <br />
        <table class="tablecontent">
            <tr>
                <td class="tablehead" style="width: 10%;">
                    優惠券編號
                </td>
                <td class="tablehead" style="width: 30%;">
                    優惠內容
                </td>
                <td class="tablehead" style="width: 25%;">
                    數量限制
                </td>
                <td class="tablehead" style="width: 15%;">
                    活動期間
                </td>
                <td class="tablehead" style="width: 10%;">
                    狀態
                </td>
                <td class="tablehead" style="width: 10%;">
                    列印
                </td>
            </tr>
        </table>
        <asp:Repeater ID="rpt_VourcherEvent" runat="server">
            <HeaderTemplate>
                <table id="pagerstores" class="tablecontent" style="width: 100%">
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td style="width: 10%;">
                        <%# ((ViewVourcherSeller)(Container.DataItem)).Id%>
                    </td>
                    <td style="width: 30%;">
                        <%# ((ViewVourcherSeller)(Container.DataItem)).Contents%>
                    </td>
                    <td style="width: 25%;">
                        <%#Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, (VourcherEventMode)(((ViewVourcherSeller)(Container.DataItem)).Mode))%>
                    </td>
                    <td style="width: 15%;">
                        <%# ((ViewVourcherSeller)(Container.DataItem)).StartDate.HasValue?((ViewVourcherSeller)(Container.DataItem)).StartDate.Value.ToString("yyyy/MM/dd"):string.Empty%>~
                        <%# ((ViewVourcherSeller)(Container.DataItem)).EndDate.HasValue ? ((ViewVourcherSeller)(Container.DataItem)).EndDate.Value.ToString("yyyy/MM/dd") : string.Empty%>
                    </td>
                    <td style="width: 10%;">
                        <%#Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, (VourcherEventStatus)(((ViewVourcherSeller)(Container.DataItem)).Status))%>
                    </td>
                    <td style="width: 10%;">
                        <asp:HiddenField ID="hif_EventId" runat="server" Value="<%# ((ViewVourcherSeller)(Container.DataItem)).Id%>" />
                        <asp:CheckBox ID="cbx_Event" runat="server" Visible='<%#(VourcherEventStatus)((ViewVourcherSeller)(Container.DataItem)).Status!=VourcherEventStatus.ReturnApply %>' />
                    </td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
        <ul>
            <li style="text-align: center;">
                <asp:Button ID="btn_Confirm" runat="server" Text="確定" OnClick="PrintVourcherEvent" />&nbsp;&nbsp;&nbsp;
                <asp:Button ID="btn_Cancel" runat="server" Text="取消" ForeColor="Red" />
            </li>
        </ul>
    </fieldset>
    <br />
    <div style="width: 900px; background-color: White; border: 2px groove black; padding: 28px 28px 28px 28px;
        display: none;">
        <asp:Panel ID="divVourcherPrint" runat="server">
            <style>
                .tdTitle
                {
                    width: 160px;
                    border-width: 2px;
                }
                .tdContent2
                {
                    width: 740px;
                    border-width: 2px;
                }
                .tdContent4
                {
                    width: 290px;
                    border-width: 2px;
                }
                .pMain
                {
                    text-align: center;
                    font-weight: bold;
                    padding-bottom: 36px;
                    font-size: 34px;
                }
                .tdMainTitle
                {
                    text-align: left;
                    font-weight: bold;
                    font-size: 24px;
                    border-width: 2px;
                }
                .tbContent
                {
                    border-collapse: collapse;
                    width: 900px;
                    font-size: 19px;
                }
                .tbContent tr td
                {
                    border-width: 2px;
                    border-style: double;
                    border-color: Black;
                    line-height: 40px;
                    padding-left: 3px;
                    font-size: 19px;
                    border-width: 2px;
                }
                .tdSubContent
                {
                    font-size: 15px;
                }
            </style>
            <p class="pMain">
                17Life APP優惠券合作契約書附件</p>
            <%--header--%>
            <table style="width: 900px">
                <tr>
                    <td class="tdMainTitle">
                        一、店家資訊：
                    </td>
                    <td style="text-align: right;">
                        <span class="tdSubContent">業務/填寫日期：
                            <asp:Label ID="lblBasicInfo" runat="server"></asp:Label></span>
                    </td>
                </tr>
            </table>
            <%--seller--%>
            <table class="tbContent">
                <tr>
                    <td colspan="4" style="background-color: LightGray;">
                        【基本資訊】
                    </td>
                </tr>
                <tr>
                    <td class="tdTitle">
                        品(招)牌名稱
                    </td>
                    <td class="tdContent2" colspan="3">
                        <asp:Label ID="lblSellerName" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="tdTitle">
                        簽約公司名稱
                    </td>
                    <td class="tdContent2" colspan="3">
                        <asp:Label ID="lblCompanyName" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="tdTitle">
                        簽約地址
                    </td>
                    <td class="tdContent2" colspan="3">
                        <asp:Label ID="lblCompanyAddress" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="tdTitle">
                        統編/身分證字號
                    </td>
                    <td class="tdContent4">
                        <asp:Label ID="lblCompanyId" runat="server"></asp:Label>
                    </td>
                    <td class="tdTitle">
                        負責人
                    </td>
                    <td class="tdContent4">
                        <asp:Label ID="lblBossName" runat="server"></asp:Label>
                        &nbsp;<span class="tdSubContent">(需同公司登記)</span>
                    </td>
                </tr>
                <tr>
                    <td class="tdTitle">
                        連絡人
                    </td>
                    <td class="tdContent4">
                        <asp:Label ID="lblContactPerson" runat="server"></asp:Label>
                    </td>
                    <td class="tdTitle">
                        行動電話
                    </td>
                    <td class="tdContent4">
                        <asp:Label ID="lblMobile" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="tdTitle">
                        聯絡電話
                    </td>
                    <td class="tdContent4">
                        <asp:Label ID="lblSellerTel" runat="server"></asp:Label>
                    </td>
                    <td class="tdTitle">
                        傳真電話
                    </td>
                    <td class="tdContent4">
                        <asp:Label ID="lblSellerFax" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="tdTitle">
                        電子信箱
                    </td>
                    <td class="tdContent2" colspan="3">
                        <asp:Label ID="lblSellerEmail" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="tdTitle">
                        官網
                    </td>
                    <td class="tdContent2" colspan="3">
                        <asp:Label ID="lblWebUrl" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="tdTitle">
                        賣家分類
                    </td>
                    <td class="tdContent2" colspan="3">
                        <asp:Label ID="lblSellerCategory" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="tdTitle">
                        平均消費
                    </td>
                    <td class="tdContent2" colspan="3">
                        <asp:Label ID="lblPerCustomerTrans" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="tdTitle">
                        店家介紹
                    </td>
                    <td class="tdContent2" colspan="3">
                        <asp:Label ID="lblSellerDescription" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="tdTitle">
                        店家圖片
                    </td>
                    <td class="tdContent2" colspan="3">
                        提醒您！請務必記得提供 LOGO/店家門面/環境照片/優惠餐點/服務項目照片 素材<br />
                        <sapn class="tdSubContent">註：一起生活.玩樂誌額外露出，會同時篩選優惠內容及店家圖文素材是否符合印刷需求。</sapn>
                    </td>
                </tr>
            </table>
            <br />
            <%--store--%>
            <asp:Repeater ID="repStore" runat="server" OnItemDataBound="repStore_ItemDataBound">
                <ItemTemplate>
                    <table class="tbContent">
                        <tr>
                            <td colspan="4" style="background-color: LightGray;">
                                【<asp:Label ID="lblStoreName" runat="server"></asp:Label>】
                            </td>
                        </tr>
                        <tr>
                            <td class="tdTitle">
                                地址
                            </td>
                            <td class="tdContent4" colspan="3">
                                <asp:Label ID="lblStoreAddress" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdTitle">
                                電話
                            </td>
                            <td class="tdContent4" style="width: 350px">
                                <asp:Label ID="lblStoreTel" runat="server"></asp:Label>
                            </td>
                            <td class="tdTitle">
                                刷卡
                            </td>
                            <td class="tdContent2" style="width: 230px">
                                <asp:Label ID="lblIsEDC" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdTitle">
                                營業時間
                            </td>
                            <td class="tdContent4" style="width: 350px">
                                <asp:Label ID="lblStoreOpeningTime" runat="server"></asp:Label>
                            </td>
                            <td class="tdTitle">
                                公休日
                            </td>
                            <td class="tdContent4" style="width: 230px">
                                <asp:Label ID="lblStoreCloseDate" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdTitle">
                                交通資訊
                            </td>
                            <td class="tdContent2" colspan="3">
                                <asp:Label ID="lblStoreVehicleInfo" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdTitle">
                                備註
                            </td>
                            <td class="tdContent2" colspan="3">
                                <asp:Label ID="lblStoreMemo" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </ItemTemplate>
            </asp:Repeater>
            <%--vourcher--%>
            <br />
            <span style="page-break-before: always;"></span>
            <table>
                <tr>
                    <td class="tdMainTitle">
                        二、優惠券內容：
                    </td>
                </tr>
            </table>
            <table class="tbContent">
                <tr>
                    <td class="tdTitle">
                        優惠期間
                    </td>
                    <td class="tdContent2">
                        簽訂協議書後若無疑義，於5個工作天內上線，優惠時間為上線後起算六個月。<br />
                        例：民國101/12/14簽約，預計民國101/12/21上線，優惠期間為民國101/12/21~101/06/21止
                    </td>
                </tr>
            </table>
            <asp:Repeater ID="repVourcher" runat="server" OnItemDataBound="repVourcher_ItemDataBound">
                <ItemTemplate>
                    <table class="tbContent">
                        <tr>
                            <td colspan="2" style="background-color: LightGray;">
                                【<asp:Label ID="lblVourcherType" runat="server"></asp:Label>】
                            </td>
                        </tr>
                        <tr>
                            <td class="tdTitle">
                                優惠內容
                            </td>
                            <td class="tdContent2">
                                <asp:Label ID="lblVourcherContent" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdTitle">
                                注意事項
                            </td>
                            <td class="tdContent2">
                                <asp:Label ID="lblVourcherInstruction" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdTitle">
                                不適用
                            </td>
                            <td class="tdContent2">
                                <asp:Label ID="lbllVourcherRestriction" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdTitle">
                                數量類型
                            </td>
                            <td class="tdContent2">
                                <asp:Label ID="lblMaxQuantity" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdTitle">
                                適用分店
                            </td>
                            <td class="tdContent2">
                                <ul>
                                    <asp:Repeater ID="repVourcherStoreList" runat="server">
                                        <ItemTemplate>
                                            <li>
                                                <%# ((ViewVourcherStore)(Container.DataItem)).StoreName %>&nbsp;/&nbsp;
                                                <%# ((ViewVourcherStore)(Container.DataItem)).Phone %>
                                            </li>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdTitle">
                                其他
                            </td>
                            <td class="tdContent2">
                                <asp:Label ID="lblOthers" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </ItemTemplate>
            </asp:Repeater>
            <br />
            <span style="font-size: 19px">註：<br />
                1. 酒類商品請在優惠券注意事項上加註「未成年請勿飲酒，請勿飲酒過量」。<br />
                2. 請確認可使用的時段/日期，如特殊假日或週末等不適用之限制。</span>
        </asp:Panel>
    </div>
    <br />
</asp:Content>

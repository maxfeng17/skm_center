﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Ppon/Ppon.Master" AutoEventWireup="true"
    CodeBehind="vourcherseller.aspx.cs" Inherits="LunchKingSite.Web.Vourcher.vourcherseller" %>

<%@ Register Src="~/User/Pager.ascx" TagName="Pager" TagPrefix="uc1" %>
<%@ Import Namespace="LunchKingSite.Core" %>
<%@ Import Namespace="LunchKingSite.DataOrm" %>
<%@ Import Namespace="LunchKingSite.Web.Vourcher" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <script src="../Tools/js/effects/jquery.flexipage.min.js" type="text/javascript"></script>
    <asp:Literal ID="ljs" runat="server"></asp:Literal>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#navi').attr('style', 'display:none;');

            $('.tablecontent tr:odd').addClass('oddtr');

            $('.tablecontent tr').attr('style', 'display:table-row;');

            loadbank();

            $('#<%= ddl_Seller_CompanyBankCode.ClientID %>').live('change', function () {
                if ($(this).val() != '') {
                    $('#<%=hif_BranchCode.ClientID%>').val('');
                    loadbankbranch($(this).val());
                }
            });

            $('#<%= ddl_Seller_CompanyBranchCode.ClientID %>').live('change', function () {
                $('#<%=hif_BranchCode.ClientID%>').val($(this).val());
            });

            loadcity();

            $('#<%= ddl_Seller_City.ClientID %>').live('change', function () {
                if ($(this).val() != '') {
                    $('#<%=hif_Seller_Township.ClientID%>').val('');
                    loadtownship($(this).val());
                }
            });

            $('#<%= ddl_Seller_Township.ClientID %>').live('change', function () {
                $('#<%=hif_Seller_Township.ClientID%>').val($(this).val());
            });

            $('fieldset li').hover(function () { $(this).addClass('highlight_tr'); }, function () { $(this).removeClass('highlight_tr'); });

            $('#pagerstores').flexipage({ perpage: 5, element: 'tr', visible_css: { display: 'table-row' } });
            $('#pagerchangelog').flexipage({ perpage: 10, element: 'tr', visible_css: { display: 'table-row' } });
        });

        function loadbank() {
            $.ajax({
                type: "POST",
                url: "vourcherseller.aspx/BankInfoGetMainList",
                data: "",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    $("#<%= ddl_Seller_CompanyBankCode.ClientID %> >option").remove();
                    $.each(msg.d, function (index, item) {
                        $('#<%= ddl_Seller_CompanyBankCode.ClientID %>').append(new Option($.strPad(item.BankNo, 3) + ' ' + item.BankName, item.BankNo));
                    });
                    if ($('#<%=hif_BankCode.ClientID %>').val() != '') {
                        $("#<%= ddl_Seller_CompanyBankCode.ClientID %> option[value='" + $('#<%=hif_BankCode.ClientID %>').val() + "']").attr('selected', 'selected');
                        loadbankbranch($('#<%=hif_BankCode.ClientID %>').val());
                    }
                    else {
                        loadbankbranch($('#<%= ddl_Seller_CompanyBankCode.ClientID %> option:first').val());
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(xhr.responseText);
                }
            });
        }

        function loadbankbranch(selecteditem) {
            $.ajax({
                type: "POST",
                url: "vourcherseller.aspx/BankInfoGetBranchList",
                data: "{id:'" + selecteditem + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    $("#<%= ddl_Seller_CompanyBranchCode.ClientID %> >option").remove();
                    $.each(msg.d, function (index, item) {
                        $('#<%= ddl_Seller_CompanyBranchCode.ClientID %>').append(new Option(item.BranchNo + ' ' + item.BranchName, item.BranchNo));
                    });
                    $('#<%=hif_BankCode.ClientID %>').val(selecteditem);
                    if ($('#<%=hif_BranchCode.ClientID %>').val() != '') {
                        $("#<%= ddl_Seller_CompanyBranchCode.ClientID %> option[value='" + $('#<%=hif_BranchCode.ClientID %>').val() + "']").attr('selected', 'selected');
                    }
                    else {
                        $('#<%=hif_BranchCode.ClientID %>').val($('#<%= ddl_Seller_CompanyBranchCode.ClientID %> option:first').val());
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(xhr.responseText);
                }
            });
        }

        function loadcity() {
            $.ajax({
                type: "POST",
                url: "vourcherseller.aspx/CityGetList",
                data: "",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    $("#<%= ddl_Seller_City.ClientID %> >option").remove();
                    $.each(msg.d, function (index, item) {
                        $('#<%= ddl_Seller_City.ClientID %>').append(new Option(item.CityName, item.Id));
                    });
                    if ($('#<%=hif_Seller_City.ClientID %>').val() != '' && $('#<%=hif_Seller_City.ClientID %>').val() != '0') {
                        loadtownship($('#<%=hif_Seller_City.ClientID %>').val());
                        $("#<%= ddl_Seller_City.ClientID %> option[value='" + $('#<%=hif_Seller_City.ClientID %>').val() + "']").attr('selected', 'selected');
                    }
                    else {
                        loadtownship($('#<%= ddl_Seller_City.ClientID %> option:first').val());
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(xhr.responseText);
                }
            });
        }

        function loadtownship(selecteditem) {
            $.ajax({
                type: "POST",
                url: "vourcherseller.aspx/TownShipGetListByCityId",
                data: "{city_id:'" + selecteditem + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    $("#<%= ddl_Seller_Township.ClientID %> >option").remove();
                    $.each(msg.d, function (index, item) {
                        $('#<%= ddl_Seller_Township.ClientID %>').append(new Option(item.CityName, item.Id));
                    });

                    if ($('#<%=hif_Seller_Township.ClientID%>').val() != '' && $('#<%=hif_Seller_Township.ClientID%>').val() != '0') {
                        $("#<%= ddl_Seller_Township.ClientID %> option[value='" + $('#<%=hif_Seller_Township.ClientID %>').val() + "']").attr('selected', 'selected');
                    }
                    else {
                        $('#<%=hif_Seller_Township.ClientID%>').val($('#<%= ddl_Seller_Township.ClientID %> option:first').val());
                    }
                    $('#<%=hif_Seller_City.ClientID%>').val(selecteditem);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(xhr.responseText);
                }
            });
        }


        function changetab(i) {
            $('#tabs').tabs({ selected: i });
            return false;
        }

        $.strPad = function (i, l, s) {
            var o = i.toString();
            if (!s) { s = '0'; }
            while (o.length < l) {
                o = s + o;
            }
            return o;
        };

        function checkcompanyuniqueid(source, args) {
            var cntrl_id = source.controltovalidate;
            var cntrl = $("#" + cntrl_id);
            $.ajax({
                type: "POST",
                url: "vourcherseller.aspx/CheckCompanyUniqueId",
                data: "{id:'" + cntrl.val() + "'}",
                contentType: "application/json; charset=utf-8",
                async: false,
                success: function (msg) {
                    if (msg.d == false) {
                        args.IsValid = false;
                        $(source).attr('style', 'display:inline;color:red;');
                    }
                    else {
                        args.IsValid = true;
                        $(source).attr('style', 'display:none;');
                    }
                }
                ,
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(thrownError);
                }
            });
        }

        function checkemail(source, args) {
            var cntrl_id = source.controltovalidate;
            var cntrl = $("#" + cntrl_id);
            var is_valid = false;

            $.ajax({
                type: "POST",
                url: "vourcherseller.aspx/CheckEmail",
                data: "{email:'" + cntrl.val() + "'}",
                contentType: "application/json; charset=utf-8",
                async: false,
                success: function (msg) {
                    if (msg.d == false) {
                        args.IsValid = false;
                        $(source).attr('style', 'display:inline;color:red;');
                    }
                    else {
                        args.IsValid = true;
                        $(source).attr('style', 'display:none;');
                    }
                }
                ,
                error: function (xhr, ajaxOptions, thrownError) {
                    alert(thrownError);
                }
            });
        }

        function checknewseller_validgroup() {
            var is_valid = window.Page_ClientValidate('NewSeller');
            if (is_valid) {
                is_valid = window.Page_ClientValidate('NewAccount');
            }
            return is_valid;
        }

        function Calculate() {
            $.ajax({
                type: "POST",
                url: "vourcherseller.aspx/CalculateCoordinates",
                data: "{'townshipId':" + $('#<%= ddl_Seller_Township.ClientID %>').val() + ",'address':'" + $('#<%= tbx_Seller_Address.ClientID %>').val() + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    if (response.d != '' && response.d.Key != null) {
                        $('[id*=txtLongitude]').val(response.d.Value);
                        $('[id*=txtLatitude]').val(response.d.Key);
                        DrawMap();
                    }
                    else {
                        alert('沒有座標資料!!');
                    }
                }
            });
        }

        function DrawMap() {

            var latitude = $('[id*=txtLatitude]');
            var longitude = $('[id*=txtLongitude]');

            if ('<%= Config.EnableOpenStreetMap %>'.toLowerCase() == 'true') {
                DrawOpenStreetMap(latitude.val(), longitude.val());
            } else {
                if ('<%= Config.EnableGoogleMapApiV2 %>'.toLowerCase() == 'true') {
                    DrawGoogleMapV2(latitude.val(), longitude.val());
                } else {
                    DrawGoogleMapV3(latitude, longitude);
                }
            }
        }

        function DrawOpenStreetMap(latitude, longitude) {

            $('#map').empty(); // 因OpenStreetMap會一直append新的地圖，必須清空
            map = new OpenLayers.Map("map");
            map.addLayer(new OpenLayers.Layer.OSM());

            var lonLat = new OpenLayers.LonLat(longitude, latitude)
                  .transform(
                    new OpenLayers.Projection("EPSG:4326"), // transform from WGS 1984
                    map.getProjectionObject() // to Spherical Mercator Projection
                  );
            var zoom = 16;

            var markers = new OpenLayers.Layer.Markers("Markers");
            map.addLayer(markers);
            markers.addMarker(new OpenLayers.Marker(lonLat));
            map.setCenter(lonLat, zoom);
            $('#map').css('display', '');
        }

        function DrawGoogleMapV2(latitude, longitude) {

            if (GBrowserIsCompatible()) {
                if (document.getElementById("map") != null) {
                    $('#map').css('display', '');
                    var map = new GMap2(document.getElementById("map"));
                    var geocoder = new GClientGeocoder();
                    map.addControl(new GSmallMapControl());

                    geocoder.getLocations(new GLatLng(latitude, longitude), function (point) {
                        if (point) {
                            map.setCenter(new GLatLng(latitude, longitude), 16);
                            var marker = new GMarker(new GLatLng(latitude, longitude));
                            map.addControl(new GSmallMapControl()); // 小型地圖控制項
                            map.addOverlay(marker);
                            AddMoveCoordinateListener(map, marker);
                        }
                    });
                }
            }
        }

        function AddMoveCoordinateListener(map, marker) {
            GEvent.addListener(map, "click", function (overlay, point) {
                if (point) {
                    if (confirm('確定要移動座標位置嗎?')) {
                        //設定標註座標
                        marker.setLatLng(point);
                        $('[id*=txtLongitude]').val(point.x.toString());
                        $('[id*=txtLatitude]').val(point.y.toString());
                    }
                }
            });
        }

        function DrawGoogleMapV3(latitude, longitude) {

            var myLatlng = new google.maps.LatLng(latitude.val(), longitude.val());
            var mapOptions = {
                zoom: 16,
                center: myLatlng,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            }

            var map = new google.maps.Map(document.getElementById('map'),
              mapOptions);

            var marker = new google.maps.Marker({
                position: myLatlng,
                draggable: true,
                map: map
            });

            google.maps.event.addListener(
                marker,
                'drag',
                function () {
                    latitude.val(marker.position.lat());
                    longitude.val(marker.position.lng());
                }
            );
            $('#map').css('display', '');
        }
    </script>
    <link type="text/css" href="../Themes/default/images/17Life/Gactivities/vourcher.css"
        rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MC" runat="server">
    <asp:HiddenField ID="hif_Seller_Township" runat="server" />
    <asp:HiddenField ID="hif_Seller_City" runat="server" />
    <asp:HiddenField ID="hif_BankCode" runat="server" />
    <asp:HiddenField ID="hif_BranchCode" runat="server" />
    <asp:HiddenField ID="hif_TempSellerGuid" runat="server" />
    <asp:HiddenField ID="hif_TempSellerId" runat="server" />
    <asp:HiddenField ID="hif_TempStoreGuid" runat="server" />
    <div style="margin-bottom: 100px;">
        <asp:Panel ID="pan_SearchSeller" runat="server">
            <span class="floattitle"><span style="font-size: medium; color: #4169E1">賣家列表</span>>編輯賣家>編輯分店</span>
            <fieldset>
                <legend>搜尋賣家</legend>
                <ul>
                    <li><span class="spantitle">搜尋條件</span><asp:DropDownList ID="ddl_Search" runat="server">
                    </asp:DropDownList>
                        <asp:TextBox ID="tbx_Search" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfv_Search" runat="server" ErrorMessage="*" ForeColor="Red"
                            Display="Dynamic" ControlToValidate="tbx_Search" ValidationGroup="search" SetFocusOnError="true"></asp:RequiredFieldValidator>
                        <asp:Button ID="btn_Search" runat="server" Text="搜尋" OnClick="SearchSeller" ValidationGroup="search" />&nbsp;&nbsp;&nbsp;&nbsp;<asp:Button
                            ID="btn_AddVourcherTempSeller" runat="server" Text="新增賣家" OnClick="AddTempSeller"
                            Visible="false" ForeColor="Blue" />
                    </li>
                </ul>
                <br />
                <span class="title" style="margin-top: 10px">賣家列表</span>
                <asp:Repeater ID="rpt_TempSellers" runat="server" OnItemCommand="TempSellerInfoItemCommand">
                    <HeaderTemplate>
                        <table class="tablecontent">
                            <tr>
                                <td class="tablehead">賣家編號
                                </td>
                                <td class="tablehead">品牌名稱
                                </td>
                                <td class="tablehead">負責人
                                </td>
                                <td class="tablehead">簽約公司名稱
                                </td>
                                <td class="tablehead">統一編號
                                </td>
                                <td class="tablehead">優惠券
                                </td>
                                <td class="tablehead">賣家狀態
                                </td>
                                <td class="tablehead">最後異動
                                </td>
                                <td class="tablehead">合約
                                </td>
                            </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td>
                                <asp:LinkButton ID="lbt_Seller_Edit" runat="server" CommandName="EditTempSeller"
                                    CommandArgument="<%# ((Seller)(Container.DataItem)).Guid %>"><%# string.IsNullOrEmpty(((Seller)(Container.DataItem)).SellerId) ? "新建" : ((Seller)(Container.DataItem)).SellerId%></asp:LinkButton>
                            </td>
                            <td>
                                <%# ((Seller)(Container.DataItem)).SellerName %>
                            </td>
                            <td>
                                <%# ((Seller)(Container.DataItem)).SellerBossName%>
                            </td>
                            <td>
                                <%# ((Seller)(Container.DataItem)).CompanyName%>
                            </td>
                            <td>
                                <%# ((Seller)(Container.DataItem)).SignCompanyID%>
                            </td>
                            <td align="center">
                                <asp:HyperLink ID="hyp_Seller_VourcherEventSearch" runat="server" NavigateUrl='<%# "~/Vourcher/vourcher.aspx?mode=search&sid="+((Seller)(Container.DataItem)).SellerId %>'
                                    Target="_blank">查詢</asp:HyperLink>
                                <asp:HyperLink ID="hyp_Seller_VourcherEvent" runat="server" NavigateUrl='<%# "~/Vourcher/vourcher.aspx?mode=newadd&sid="+((Seller)(Container.DataItem)).SellerId %>'
                                    Target="_blank" Visible="false">新增</asp:HyperLink>
                            </td>
                            <td>
                                <%#Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, (SellerTempStatus)(((Seller)(Container.DataItem)).TempStatus))%>
                            </td>
                            <td>
                                <%# (((Seller)(Container.DataItem)).ApplyId??string.Empty).Replace("@17life.com",string.Empty)%>
                            </td>
                            <td>
                                <asp:HyperLink ID="hyp_Print2" runat="server" Target="_blank" NavigateUrl='<%# "~/Vourcher/vourcher_print.aspx?sid="+ ((Seller)(Container.DataItem)).Guid%>'
                                    ForeColor="#CD5C5C">列印</asp:HyperLink>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
                <uc1:Pager ID="gridPager" runat="server" PageSize="15" ongetcount="RetrieveDataCount"
                    onupdate="UpdateHandler" />
            </fieldset>
            <fieldset>
                <legend>訊息通知</legend>
                <ul style="display: none">
                    <li><span class="spantitle">狀態</span>
                        <asp:DropDownList ID="ddl_TempStatus" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ChangeTempStatus">
                        </asp:DropDownList>
                    </li>
                </ul>
                <asp:Repeater ID="rpt_ReturnCase" runat="server" OnItemCommand="TempSellerInfoItemCommand">
                    <HeaderTemplate>
                        <table class="tablecontent">
                            <tr>
                                <td class="tablehead">品牌名稱
                                </td>
                                <td class="tablehead">類別
                                </td>
                                <td class="tablehead">負責人
                                </td>
                                <td class="tablehead">簽約公司名稱
                                </td>
                                <td class="tablehead">統一編號
                                </td>
                                <td class="tablehead">賣家狀態
                                </td>
                                <td class="tablehead">訊息
                                </td>
                            </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td>
                                <asp:LinkButton ID="lbt_ReturnCase" runat="server" CommandName='<%# ((ReturnCase)(Container.DataItem)).Type == ReturnCaseType.Seller ? "EditTempSeller" : "EditTempStore"%>'
                                    CommandArgument="<%# ((ReturnCase)(Container.DataItem)).KeyGuid%>"><%# ((ReturnCase)(Container.DataItem)).Name%></asp:LinkButton>
                            </td>
                            <td>
                                <%# ((ReturnCase)(Container.DataItem)).Type == ReturnCaseType.Seller ? "賣家" : "分店"%>
                            </td>
                            <td>
                                <%# ((ReturnCase)(Container.DataItem)).CompanyBossName%>
                            </td>
                            <td>
                                <%# ((ReturnCase)(Container.DataItem)).CompanyName%>
                            </td>
                            <td>
                                <%# ((ReturnCase)(Container.DataItem)).SignComapnyId%>
                            </td>
                            <td>
                                <%#
                                ((SellerTempStatus)(((ReturnCase)(Container.DataItem)).Status)==SellerTempStatus.Returned?"<span style='color:red;'>":"<span>")+
                                Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, (SellerTempStatus)(((ReturnCase)(Container.DataItem)).Status))+"</span>"%><%# ((SellerTempStatus)(((ReturnCase)(Container.DataItem)).Status)==SellerTempStatus.Applied)?(((ReturnCase)(Container.DataItem)).ApplyTime.HasValue?((ReturnCase)(Container.DataItem)).ApplyTime.Value.ToString("MM/dd hh:mm"):string.Empty):
                                   (((ReturnCase)(Container.DataItem)).ReturnTime.HasValue?((ReturnCase)(Container.DataItem)).ReturnTime.Value.ToString("MM/dd hh:mm"):string.Empty)%>
                            </td>
                            <td>
                                <%# ((ReturnCase)(Container.DataItem)).Message%>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
            </fieldset>
        </asp:Panel>
        <asp:Panel ID="pan_VourcherTempSeller" runat="server" Visible="false">
            <span class="floattitle">賣家列表><span style="font-size: medium; color: #4169E1">編輯賣家(<asp:Label
                ID="lab_SellerStatus" runat="server"></asp:Label>)</span>>編輯分店</span>
            <fieldset>
                <legend>編輯賣家</legend>
                <ul>
                    <li><span class="spantitle">賣家編號:</span><asp:Label ID="lab_Seller_SellerId" runat="server"
                        Font-Bold="true" ForeColor="Brown"></asp:Label></li>
                    <li><span class="spantitle">品牌名稱:</span><asp:TextBox ID="tbx_Seller_SellerName" runat="server"></asp:TextBox><asp:RequiredFieldValidator
                        ID="rfv_Seller_SellerName" runat="server" ErrorMessage="*" ForeColor="Red" Display="Dynamic"
                        ControlToValidate="tbx_Seller_SellerName" ValidationGroup="NewSeller" SetFocusOnError="true"></asp:RequiredFieldValidator>
                        <span style="color: #4AA02C;">
                            <asp:Literal ID="lit_Seller_SellerName" runat="server"></asp:Literal></span>
                        <span style="color: Gray; font-size: small;">範例：COCO都可，不需填寫分店名稱</span> </li>
                    <li><span class="spantitle">負責人:</span><asp:TextBox ID="tbx_Seller_BossName" runat="server"></asp:TextBox><asp:RequiredFieldValidator
                        ID="rfv_Seller_BossName" runat="server" ErrorMessage="*" ForeColor="Red" Display="Dynamic"
                        ControlToValidate="tbx_Seller_BossName" ValidationGroup="NewAccount" SetFocusOnError="true"></asp:RequiredFieldValidator>
                        <span style="color: #4AA02C;">
                            <asp:Literal ID="lit_Seller_BossName" runat="server"></asp:Literal></span>
                    </li>
                    <li><span class="spantitle">聯絡人:</span><asp:TextBox ID="tbx_Seller_Contact_Person"
                        runat="server"></asp:TextBox><asp:RequiredFieldValidator ID="rfv_Seller_Contact_Person"
                            runat="server" ErrorMessage="*" ForeColor="Red" Display="Dynamic" ControlToValidate="tbx_Seller_Contact_Person"
                            ValidationGroup="NewAccount" SetFocusOnError="true"></asp:RequiredFieldValidator>
                        <span style="color: #4AA02C;">
                            <asp:Literal ID="lit_Seller_Contact_Person" runat="server"></asp:Literal></span>
                    </li>
                    <li><span class="spantitle">連絡電話:</span><asp:TextBox ID="tbx_Seller_Tel" runat="server"></asp:TextBox><asp:RequiredFieldValidator
                        ID="rfv_Seller_Tel" runat="server" ErrorMessage="*" ForeColor="Red" Display="Dynamic"
                        ControlToValidate="tbx_Seller_Tel" ValidationGroup="NewSeller" SetFocusOnError="true"></asp:RequiredFieldValidator>
                        <span style="color: #4AA02C;">
                            <asp:Literal ID="lit_Seller_Tel" runat="server"></asp:Literal></span> <span style="color: Gray;
                                font-size: small;">範例：(02)25588817;(02)25219131</span> </li>
                    <li><span class="spantitle">行動電話:</span><asp:TextBox ID="tbx_Seller_Mobile" runat="server"></asp:TextBox><asp:RequiredFieldValidator
                        ID="rfv_Seller_Mobile" runat="server" ErrorMessage="*" ForeColor="Red" Display="Dynamic"
                        ControlToValidate="tbx_Seller_Mobile" ValidationGroup="NewSeller" SetFocusOnError="true"></asp:RequiredFieldValidator>
                        <span style="color: #4AA02C;">
                            <asp:Literal ID="lit_Seller_Mobile" runat="server"></asp:Literal>
                        </span><span style="color: Gray; font-size: small;">範例：0912345678;0987654321</span>
                    </li>
                    <li><span class="spantitle">傳真電話:</span><asp:TextBox ID="tbx_Seller_Fax" runat="server"></asp:TextBox>
                        <span style="color: #4AA02C;">
                            <asp:Literal ID="lit_Seller_Fax" runat="server"></asp:Literal></span> <span style="color: Gray;
                                font-size: small;">範例：(02)25588817;(02)25219131</span> </li>
                    <li><span class="spantitle">電子信箱:</span><asp:TextBox ID="tbx_Seller_Email" runat="server"></asp:TextBox><asp:RequiredFieldValidator
                        ID="rfv_Seller_Email" runat="server" ErrorMessage="*" ForeColor="Red" Display="Dynamic"
                        ControlToValidate="tbx_Seller_Email" ValidationGroup="NewSeller" Enabled="false"></asp:RequiredFieldValidator>
                        <span style="color: #4AA02C;">
                            <asp:Literal ID="lit_Seller_Email" runat="server"></asp:Literal></span> <span style="color: Gray;
                                font-size: small;">範例：test1@17life.com;test2@17life.com</span> </li>
                    <li><span class="spantitle">部落格網址:</span><asp:TextBox ID="tbx_Seller_Blog" runat="server"></asp:TextBox>
                        <span style="color: #4AA02C;">
                            <asp:Literal ID="lit_Seller_Blog" runat="server"></asp:Literal></span><span style="color: Gray;
                                font-size: small;">範例：https://www.17life.com/ppon </span></li>
                    <li style="height: 150px"><span class="spantitle">店家介紹:</span> <span style="font-size: small;
                        color: Gray;">■ 可由店家提供、17Life販售頁、17Life特選店家頁面截取(約200個文字，不可直接放連結)</span>
                        <br />
                        <span class="spantitle"></span>
                        <asp:TextBox ID="tbx_Seller_Description" runat="server" TextMode="MultiLine"></asp:TextBox><br />
                        <span class="spantitle"></span><span style="color: #4AA02C;">
                            <asp:Literal ID="lit_Seller_Description" runat="server"></asp:Literal></span>
                    </li>
                    <li style="height: 100px"><span class="spantitle">賣家分類:</span>
                        <asp:DropDownList ID="ddl_Seller_Category" runat="server">
                        </asp:DropDownList>
                        <span style="color: #4AA02C;">
                            <asp:Literal ID="lit_Seller_Category" runat="server"></asp:Literal></span>
                        <br />
                        <span class="spantitle"></span><span style="color: Gray; font-size: small;">■ 美食：餐廳、咖啡廳等
                            ■ 旅遊：飯店、民宿、遊樂園等</span><br />
                        <span class="spantitle"></span><span style="color: Gray; font-size: small;">■ 美體美髮：美容、美髮、SPA等
                            ■ 其它：非美食、旅遊、美體美髮店家</span> </li>
                    <li style="height: 100px"><span class="spantitle">平均消費:</span>
                        <asp:RadioButtonList ID="rbl_Seller_ConsumptionAvg" runat="server" RepeatDirection="Horizontal"
                            RepeatColumns="5" Style="font-size: small; margin-left: 100px; width: 650px;">
                        </asp:RadioButtonList>
                        <span style="color: #4AA02C; float: right; margin-right: 250px; margin-top: -20px;">
                            <asp:Literal ID="lit_Seller_ConsumptionAvg" runat="server"></asp:Literal></span>
                    </li>
                    <li style="height: 165px"><span class="spantitle">上傳LOGO:</span><asp:FileUpload ID="fu_LogoImg_1"
                        runat="server" />
                        <span style="font-size: small; color: Gray;">限上傳一張，且僅支援jpg或gif檔3Mb以內</span><br />
                        <span class="spantitle"></span>
                        <asp:Image ID="img_Logo_1" runat="server" AlternateText="無上傳照片" Height="112" Width="151" />
                    </li>
                    <li style="height: 165px"><span class="spantitle">上傳店家照:</span><asp:FileUpload ID="fu_LogoImg_2"
                        runat="server" />
                        <span style="font-size: small; color: Gray;">限上傳一張，且僅支援jpg或gif檔3Mb以內</span><br />
                        <span class="spantitle"></span>
                        <asp:Image ID="img_Logo_2" runat="server" AlternateText="無上傳照片" Height="112" Width="151" />
                    </li>
                </ul>
            </fieldset>
        </asp:Panel>
        <asp:Panel ID="pan_VourcherTempStore" runat="server" Visible="false">
            <span class="floattitle">賣家列表>編輯賣家><span style="font-size: medium; color: #4169E1">編輯分店(<asp:Label
                ID="lab_StoreStatus" runat="server"></asp:Label>)</span></span>
            <fieldset>
                <legend>編輯分店</legend>
                <ul>
                    <li><span class="spantitle">賣家編號:</span><asp:Label ID="lab_Store_SellerId" runat="server"
                        Font-Bold="true" ForeColor="Brown"></asp:Label><span class="spantitle">分店ID:</span><asp:Label
                            ID="lab_Store_StoreGuid" runat="server" Font-Bold="true" ForeColor="Brown"></asp:Label>
                    </li>
                    <li><span class="spantitle">分店名稱:</span><asp:TextBox ID="tbx_Store_StoreName" runat="server"></asp:TextBox><asp:RequiredFieldValidator
                        ID="rfv_Store_StoreName" runat="server" ErrorMessage="*" ForeColor="Red" Display="Dynamic"
                        ControlToValidate="tbx_Store_StoreName" ValidationGroup="NewStore" SetFocusOnError="true"></asp:RequiredFieldValidator><span
                            style="color: #4AA02C;">
                            <asp:Literal ID="lit_Store_StoreName" runat="server"></asp:Literal></span><span style="color: Gray;
                                font-size: small"> 範例：中山店，不需重打品牌名稱 </span></li>
                    <li><span class="spantitle">店家電話:</span><asp:TextBox ID="tbx_Store_Phone" runat="server"></asp:TextBox><asp:RequiredFieldValidator
                        ID="rfv_Store_Phone" runat="server" ErrorMessage="*" ForeColor="Red" Display="Dynamic"
                        ControlToValidate="tbx_Store_Phone" ValidationGroup="NewStore" SetFocusOnError="true"></asp:RequiredFieldValidator><span
                            style="color: #4AA02C;">
                            <asp:Literal ID="lit_Store_Phone" runat="server"></asp:Literal></span><span style="color: Gray;
                                font-size: small">範例：(02)25588817;(02)25219131</span></li>
                    <li><span class="spantitle">營業時間:</span><asp:TextBox ID="tbx_Store_OpenTime" runat="server"></asp:TextBox><asp:RequiredFieldValidator
                        ID="rfv_Store_OpenTime" runat="server" ErrorMessage="*" ForeColor="Red" Display="Dynamic"
                        ControlToValidate="tbx_Store_OpenTime" ValidationGroup="NewStore" SetFocusOnError="true"></asp:RequiredFieldValidator><span
                            style="color: #4AA02C;">
                            <asp:Literal ID="lit_Store_OpenTime" runat="server"></asp:Literal></span><span style="color: Gray;
                                font-size: small">範例：週一~週五 9:00~22:00;週六 9:00~12:00</span></li>
                    <li><span class="spantitle">公休日:</span><asp:TextBox ID="tbx_Store_CloseDate" runat="server"></asp:TextBox><span
                        style="color: #4AA02C;">
                        <asp:Literal ID="lit_Store_CloseDate" runat="server"></asp:Literal></span><span style="color: Gray;
                            font-size: small">範例：週六 13:00~22:00;週日</span></li>
                    <li><span class="spantitle">刷卡服務:</span><asp:CheckBox ID="cbx_Store_CreditCardAvailable"
                        runat="server" /><span style="color: #4AA02C;">
                            <asp:Literal ID="lit_Store_CreditCardAvailable" runat="server"></asp:Literal></span>
                    </li>
                    <li><span class="spantitle">捷運:</span><asp:TextBox ID="tbx_Store_MRT" runat="server"></asp:TextBox><span
                        style="color: #4AA02C;">
                        <asp:Literal ID="lit_Store_MRT" runat="server"></asp:Literal></span> </li>
                    <li><span class="spantitle">開車:</span><asp:TextBox ID="tbx_Store_Car" runat="server"></asp:TextBox><span
                        style="color: #4AA02C;">
                        <asp:Literal ID="lit_Store_Car" runat="server"></asp:Literal></span></li>
                    <li><span class="spantitle">公車:</span><asp:TextBox ID="tbx_Store_Bus" runat="server"></asp:TextBox><span
                        style="color: #4AA02C;">
                        <asp:Literal ID="lit_Store_Bus" runat="server"></asp:Literal></span></li>
                    <li><span class="spantitle">其他:</span><asp:TextBox ID="tbx_Store_OtherVehicle" runat="server"></asp:TextBox><span
                        style="color: #4AA02C;">
                        <asp:Literal ID="lit_Store_OtherVehicle" runat="server"></asp:Literal></span>
                    </li>
                    <li><span class="spantitle">官網:</span><asp:TextBox ID="tbx_Store_WebUrl" runat="server"></asp:TextBox><span
                        style="color: #4AA02C;">
                        <asp:Literal ID="lit_Store_WebUrl" runat="server"></asp:Literal></span><span style="color: Gray;
                            font-size: small">範例：https://www.17life.com/ppon </span></li>
                    <li><span class="spantitle">FaceBook:</span><asp:TextBox ID="tbx_Store_FaceBookUrl"
                        runat="server">FaceBook</asp:TextBox><span style="color: #4AA02C;">
                            <asp:Literal ID="lit_Store_FaceBookUrl" runat="server"></asp:Literal></span>
                    </li>
                    <li><span class="spantitle">Plurk:</span><asp:TextBox ID="tbx_Store_PlurkUrl" runat="server"></asp:TextBox><span
                        style="color: #4AA02C;">
                        <asp:Literal ID="lit_Store_PlurkUrl" runat="server"></asp:Literal></span></li>
                    <li><span class="spantitle">Blog:</span><asp:TextBox ID="tbx_Store_BlogUrl" runat="server"></asp:TextBox><span
                        style="color: #4AA02C;">
                        <asp:Literal ID="lit_Store_BlogUrl" runat="server"></asp:Literal></span></li>
                    <li><span class="spantitle">其他網址:</span><asp:TextBox ID="tbx_Store_OtherUrl" runat="server"></asp:TextBox><span
                        style="color: #4AA02C;">
                        <asp:Literal ID="lit_Store_OtherUrl" runat="server"></asp:Literal></span></li>
                    <li><span class="spantitle">備註:</span><asp:TextBox ID="tbx_Store_Remark" runat="server"></asp:TextBox><span
                        style="color: #4AA02C;">
                        <asp:Literal ID="lit_Store_Remark" runat="server"></asp:Literal></span></li>
                </ul>
            </fieldset>
        </asp:Panel>
        <asp:Panel ID="pan_Account" runat="server" Visible="false">
            <fieldset>
                <legend>公司設定</legend>
                <ul>
                    <li><span class="spantitle" style="width: 160px;">簽約公司名稱:</span><asp:TextBox ID="tbx_Seller_ComapnyName"
                        runat="server"></asp:TextBox>
                        <span style="color: #4AA02C;">
                            <asp:Literal ID="lit_Seller_ComapnyName" runat="server"></asp:Literal></span>
                        <asp:RequiredFieldValidator ID="rfv_Seller_ComapnyName" runat="server" ErrorMessage="*"
                            ForeColor="Red" Display="Dynamic" ControlToValidate="tbx_Seller_ComapnyName"
                            ValidationGroup="NewAccount" SetFocusOnError="true"></asp:RequiredFieldValidator>
                    </li>
                    <li><span class="spantitle" style="width: 160px;">簽約公司ID/統一編號:</span><asp:TextBox
                        ID="tbx_Seller_CompanySignId" runat="server"></asp:TextBox>
                        <span style="color: #4AA02C;">
                            <asp:Literal ID="lit_Seller_CompanySignId" runat="server"></asp:Literal></span>
                        <span style="color: Gray; font-size: small">私人約請填寫ID，公司約請填寫統一編號</span>
                        <asp:RequiredFieldValidator ID="rfv_Seller_CompanySignId" runat="server" ErrorMessage="*"
                            ForeColor="Red" Display="Dynamic" ControlToValidate="tbx_Seller_CompanySignId"
                            ValidationGroup="NewAccount" SetFocusOnError="true"></asp:RequiredFieldValidator>
                        <asp:CustomValidator ID="cv_Seller_CompanySignId" runat="server" ErrorMessage="格式錯誤"
                            ForeColor="Red" Display="Dynamic" ControlToValidate="tbx_Seller_CompanySignId"
                            ValidationGroup="NewAccount" ClientValidationFunction="checkcompanyuniqueid"
                            SetFocusOnError="true"></asp:CustomValidator></li>
                    <li><span class="spantitle" style="width: 160px;">簽約負責人:</span>
                        <asp:TextBox ID="tbx_Seller_CompanyBossName" runat="server"></asp:TextBox><span style="color: #4AA02C;">
                            <asp:Literal ID="lit_Seller_CompanyBossName" runat="server"></asp:Literal></span>
                        <asp:RequiredFieldValidator ID="rfv_Seller_CompanyBossName" runat="server" ErrorMessage="*"
                            ForeColor="Red" Display="Dynamic" ControlToValidate="tbx_Seller_CompanyBossName"
                            ValidationGroup="NewAccount" SetFocusOnError="true"></asp:RequiredFieldValidator>
                    </li>
                    <li style="height: 230px"><span class="spantitle">
                        <asp:Literal ID="lit_Address" runat="server" Text="簽約"></asp:Literal>地址:</span>
                        <asp:DropDownList ID="ddl_Seller_City" runat="server">
                        </asp:DropDownList>
                        <asp:DropDownList ID="ddl_Seller_Township" runat="server">
                        </asp:DropDownList>
                        <asp:TextBox ID="tbx_Seller_Address" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfv_Seller_Address" runat="server" ErrorMessage="*"
                            ForeColor="Red" Display="Dynamic" ControlToValidate="tbx_Seller_Address" ValidationGroup="NewAccount"
                            SetFocusOnError="true"></asp:RequiredFieldValidator>
                        <span style="color: Gray; font-size: small;">巷弄號樓，請填寫阿拉伯數字。</span>
                        <br />
                        <span style="color: #4AA02C; margin-left: 150px;">
                            <asp:Literal ID="lit_Seller_CityId" runat="server"></asp:Literal></span> <span style="color: #4AA02C;">
                                <asp:Literal ID="lit_Seller_Address" runat="server"></asp:Literal></span>
                        緯度:<asp:TextBox ID="txtLatitude" runat="server" Width="115px"></asp:TextBox>&nbsp;
                        經度:<asp:TextBox ID="txtLongitude" runat="server" Width="115px"></asp:TextBox>
                        <input type="button" value="計算經緯度" onclick="Calculate();" />
                        <input type="button" value="畫出地圖" onclick="DrawMap();" />
                        <div style="margin-left: 150px">
                            <div id="map" style="width: 450px; height: 150px; display: none;">
                            </div>
                        </div>
                    </li>
                    <li style="text-align: center">
                        <asp:Button ID="btn_AddNewSeller" runat="server" Text="新增賣家" OnClick="SaveSeller"
                            OnClientClick="return checknewseller_validgroup();" Visible="false" />&nbsp;
                        <asp:Button ID="btn_AddNewStore" runat="server" Text="新增分店" ValidationGroup="NewStore"
                            OnClick="SaveStore" Visible="false" />&nbsp;
                        <asp:Button ID="btn_CancelReturnToSearch" runat="server" Text="取消" OnClick="CancelReturn" />&nbsp;
                        <asp:HyperLink ID="hyp_AddVourcher" runat="server" Target="_blank" Visible="false">新增優惠券</asp:HyperLink>
                    </li>
                </ul>
            </fieldset>
        </asp:Panel>
        <asp:Panel ID="pan_VourcherTempSeller_Stores" runat="server" Visible="false">
            <fieldset>
                <legend>分店列表 </legend>
                <table class="tablecontent">
                    <tr>
                        <td class="tablehead" style="width: 35%">品牌名稱
                        </td>
                        <td class="tablehead" style="width: 25%">店家電話
                        </td>
                        <td class="tablehead" style="width: 25%">統一編號
                        </td>
                        <td class="tablehead" style="width: 15%">分店狀態
                        </td>
                    </tr>
                </table>
                <asp:Repeater ID="rpt_TempStores" runat="server" OnItemCommand="TempSellerInfoItemCommand">
                    <HeaderTemplate>
                        <table id="pagerstores" class="tablecontent" style="width: 100%">
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td style="width: 35%">
                                <asp:LinkButton ID="lbt_Store_Edit" runat="server" CommandName="EditTempStore" CommandArgument="<%# ((Store)(Container.DataItem)).Guid %>"><%# ((Store)(Container.DataItem)).StoreName%></asp:LinkButton>
                            </td>
                            <td style="width: 25%">
                                <%# ((Store)(Container.DataItem)).Phone%>
                            </td>
                            <td style="width: 25%">
                                <%# ((Store)(Container.DataItem)).SignCompanyID%>
                            </td>
                            <td style="width: 15%">
                                <%#Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, (SellerTempStatus)(((Store)(Container.DataItem)).TempStatus))%>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
                <br />
                <ul>
                    <li style="text-align: center">
                        <asp:Button ID="btn_AddTempStore" runat="server" Text="新增分店" OnClick="AddTempStore" />
                    </li>
                </ul>
            </fieldset>
        </asp:Panel>
        <asp:Panel ID="pan_ChangeLog" runat="server" Visible="false">
            <fieldset>
                <legend>異動紀錄</legend>
                <table class="tablecontent">
                    <tr>
                        <td class="tablehead" style="width: 25%">日期
                        </td>
                        <td class="tablehead" style="width: 60%">異動資料
                        </td>
                        <td class="tablehead" style="width: 15%">建立者
                        </td>
                    </tr>
                </table>
                <asp:Repeater ID="rpt_ChangeLog" runat="server">
                    <HeaderTemplate>
                        <table id="pagerchangelog" class="tablecontent" style="width: 100%">
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td style="width: 25%">
                                <%# ((ChangeLog)(Container.DataItem)).ModifyTime.ToString("MM/dd hh:mm")%>
                            </td>
                            <td style="width: 60%">
                                <%#  Regex.Replace(((ChangeLog)(Container.DataItem)).Content,"[{}\"]",string.Empty).Replace(",","<br/>")%>
                            </td>
                            <td style="width: 15%">
                                <%# ((ChangeLog)(Container.DataItem)).ModifyUser.Replace("@17life.com",string.Empty)%>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
            </fieldset>
        </asp:Panel>
        <asp:Panel ID="pan_DisplayNone" runat="server" Visible="false">
            <ul>
                <li><span class="spantitle">銀行代號:</span>
                    <asp:DropDownList ID="ddl_Seller_CompanyBankCode" runat="server">
                    </asp:DropDownList>
                    <span style="color: #4AA02C;">
                        <asp:Literal ID="lit_Seller_CompanyBankCode" runat="server"></asp:Literal></span>
                </li>
                <li><span class="spantitle">分行代號:</span>
                    <asp:DropDownList ID="ddl_Seller_CompanyBranchCode" runat="server">
                    </asp:DropDownList>
                    <span style="color: #4AA02C;">
                        <asp:Literal ID="lit_Seller_CompanyBranchCode" runat="server"></asp:Literal></span>
                </li>
                <li><span class="spantitle">帳號:</span><asp:TextBox ID="tbx_Seller_CompanyAccount"
                    runat="server"></asp:TextBox>
                    <span style="color: #4AA02C;">
                        <asp:Literal ID="lit_Seller_CompanyAccount" runat="server"></asp:Literal></span><asp:RequiredFieldValidator
                            ID="rfv_Seller_CompanyAccount" runat="server" ErrorMessage="*" ForeColor="Red"
                            Display="Dynamic" ControlToValidate="tbx_Seller_CompanyAccount" ValidationGroup="NewAccount"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="rev_Seller_CompanyAccount" runat="server" ErrorMessage="僅數字"
                        ForeColor="Red" Display="Dynamic" ControlToValidate="tbx_Seller_CompanyAccount"
                        ValidationGroup="NewAccount" ValidationExpression="\d*" SetFocusOnError="true"
                        Enabled="false"></asp:RegularExpressionValidator>
                </li>
                <li><span class="spantitle">戶名:</span><asp:TextBox ID="tbx_Seller_CompanyAccountName"
                    runat="server">戶名</asp:TextBox>
                    <span style="color: #4AA02C;">
                        <asp:Literal ID="lit_Seller_CompanyAccountName" runat="server"></asp:Literal></span><asp:RequiredFieldValidator
                            ID="efv_Seller_CompanyAccountName" runat="server" ErrorMessage="*" ForeColor="Red"
                            Display="Dynamic" ControlToValidate="tbx_Seller_CompanyAccountName" ValidationGroup="NewAccount"
                            SetFocusOnError="true" Enabled="false"></asp:RequiredFieldValidator>
                </li>
                <li><span class="spantitle" style="width: 160px;">受款人ID/統一編號:</span><asp:TextBox
                    ID="tbx_Seller_CompanyId" runat="server"></asp:TextBox>
                    <span style="color: #4AA02C;">
                        <asp:Literal ID="lit_Seller_CompanyId" runat="server"></asp:Literal></span><asp:RequiredFieldValidator
                            ID="rfv_Seller_CompanyId" runat="server" ErrorMessage="*" ForeColor="Red" Display="Dynamic"
                            ControlToValidate="tbx_Seller_CompanyId" ValidationGroup="NewAccount"></asp:RequiredFieldValidator><asp:CustomValidator
                                ID="cv_Seller_CompanyId" runat="server" ErrorMessage="格式錯誤" ForeColor="Red" Display="Dynamic"
                                ControlToValidate="tbx_Seller_CompanyId" ValidationGroup="NewAccount" ClientValidationFunction="checkcompanyuniqueid"
                                SetFocusOnError="true" Enabled="false"></asp:CustomValidator></li>
                <li><span class="spantitle">帳務聯絡人:</span><asp:TextBox ID="tbx_Seller_AccountantName"
                    runat="server"></asp:TextBox>
                    <span style="color: #4AA02C;">
                        <asp:Literal ID="lit_Seller_AccountantName" runat="server"></asp:Literal></span><asp:RequiredFieldValidator
                            ID="rfv_Seller_AccountantName" runat="server" ErrorMessage="*" ForeColor="Red"
                            Display="Dynamic" ControlToValidate="tbx_Seller_AccountantName" ValidationGroup="NewAccount"
                            SetFocusOnError="true" Enabled="false"></asp:RequiredFieldValidator>
                </li>
                <li><span class="spantitle" style="width: 160px;">帳務聯絡電話:</span><asp:TextBox ID="tbx_Seller_AccountantTel"
                    runat="server"></asp:TextBox>
                    <span style="color: #4AA02C;">
                        <asp:Literal ID="lit_Seller_AccountantTel" runat="server"></asp:Literal></span><asp:RequiredFieldValidator
                            ID="rfv_Seller_AccountantTel" runat="server" ErrorMessage="*" ForeColor="Red"
                            Display="Dynamic" ControlToValidate="tbx_Seller_AccountantTel" ValidationGroup="NewAccount"
                            SetFocusOnError="true" Enabled="false"></asp:RequiredFieldValidator>
                </li>
                <li><span class="spantitle" style="width: 160px;">帳務連絡人電子信箱:</span><asp:TextBox ID="tbx_Seller_CompanyEmail"
                    runat="server"></asp:TextBox>
                    <span style="color: #4AA02C;">
                        <asp:Literal ID="lit_Seller_CompanyEmail" runat="server"></asp:Literal></span><asp:RequiredFieldValidator
                            ID="rfv_Seller_CompanyEmail" runat="server" ErrorMessage="*" ForeColor="Red"
                            Display="Dynamic" ControlToValidate="tbx_Seller_CompanyEmail" ValidationGroup="NewAccount"
                            SetFocusOnError="true" Enabled="false"></asp:RequiredFieldValidator>
                </li>
                <li><span class="spantitle">備 註:</span><asp:TextBox ID="tbx_Seller_CompanyNotice"
                    runat="server"></asp:TextBox>
                    <span style="color: #4AA02C;">
                        <asp:Literal ID="lit_Seller_CompanyNotice" runat="server"></asp:Literal></span>
                </li>
                <li><span class="spantitle">後台備註:</span><asp:TextBox ID="tbx_Seller_Message" runat="server"></asp:TextBox>
                    <span style="color: #4AA02C;">
                        <asp:Literal ID="lit_Seller_Message" runat="server"></asp:Literal></span>
                </li>
                <li><span class="spantitle">後台備註:</span><asp:TextBox ID="tbx_Store_Message" runat="server"></asp:TextBox><span
                    style="color: #4AA02C;">
                    <asp:Literal ID="lit_Store_Message" runat="server"></asp:Literal></span></li>
            </ul>
        </asp:Panel>
        <asp:HyperLink ID="hyp_GoogleDoc" runat="server" Target="_blank" ForeColor="Blue"
            NavigateUrl="https://docs.google.com/a/17life.com/spreadsheet/viewform?formkey=dE52Q0tVQkhkMzdNS2NVcjU5Z256Vmc6MQ">申請修改/異動優惠券</asp:HyperLink>(含優惠券店家倒閉/優惠券內容調整...等回報)
    </div>
</asp:Content>

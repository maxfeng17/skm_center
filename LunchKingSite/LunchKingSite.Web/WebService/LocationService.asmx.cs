﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Component.API;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib;
using LunchKingSite.SsBLL;

namespace LunchKingSite.Web.WebService
{
    /// <summary>
    /// Summary description for LocationService
    /// </summary>
    [WebService(Namespace = "http://www.17life.com/WebService")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class LocationService : BaseService
    {
        public static ISysConfProvider config;
        public static ILocationProvider lp;
        public static IMemberProvider mp;
        static LocationService()
        {
            config = ProviderFactory.Instance().GetConfig();
            lp = ProviderFactory.Instance().GetProvider<ILocationProvider>();
            mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        }

        [WebMethod]
        public string GetTownships(string cid)
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(cid))
                {
                    var cities = CityManager.TownShipGetListByCityId(int.Parse(cid));
                    var jsonCol = from x in cities select new {id = x.Id, name = x.CityName};

                    return new JsonSerializer().Serialize(jsonCol);
                }
            }
            catch (Exception e)
            {
                WebUtility.LogExceptionAnyway(e);
            }

            return "[]";
        }

        [WebMethod]
        public string GetNotDeliveryIslandsTownships(string cid)
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(cid))
                {
                    int parentId = int.TryParse(cid, out parentId) ? parentId : PponCityGroup.DefaultPponCityGroup.AllCountry.CityId;
                    var cities = CityManager.TownShipGetListByParentCityIdWithoutIsland(parentId);
                    var jsonCol = from x in cities select new { id = x.Id, name = x.CityName };

                    return new JsonSerializer().Serialize(jsonCol);
                }
            }
            catch (Exception e)
            {
                WebUtility.LogExceptionAnyway(e);
            }

            return "[]";
        }

        [WebMethod]
        public string GetLocations(string cid)
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(cid))
                {
                    var buildings = CityManager.BuildingListGetByTownshipId(int.Parse(cid));
                    var jsonCol = from x in buildings select new { id = x.Guid, name = x.BuildingName };

                    return new JsonSerializer().Serialize(jsonCol);
                }
            }
            catch (Exception e)
            {
                WebUtility.LogExceptionAnyway(e);
            }

            return "[]";
        }

        [WebMethod]
        public void GetCitysJSon(bool withOutLands)
        {
            //設定輸出格式為json格式
            this.Context.Response.ContentType = "application/json";
            this.Context.Response.Charset = Encoding.UTF8.WebName;

            var jsonS = new JsonSerializer();
            var jsonCol = from x in CityManager.Citys.Where(x => x.Code != "SYS") select new { Id = x.Id, Name = x.CityName };

            this.Context.Response.Write(jsonS.Serialize(jsonCol));
        }
        [WebMethod]
        public void GetTownshipsJSon(string cid , bool withOutLands)
        {
            //設定輸出格式為json格式
            this.Context.Response.ContentType = "application/json";
            this.Context.Response.Charset = Encoding.UTF8.WebName;

            var jsonS = new JsonSerializer();
            var cities = CityManager.TownShipGetListByCityId(int.Parse(cid));
            var jsonCol = from x in cities select new { Id = x.Id, Name = x.CityName };

            this.Context.Response.Write(jsonS.Serialize(jsonCol));
        }

        [WebMethod]
        public void GetLocationsJSon(string cid)
        {
            //設定輸出格式為json格式
            this.Context.Response.ContentType = "application/json";
            this.Context.Response.Charset = Encoding.UTF8.WebName;

            var jsonS = new JsonSerializer();
            var buildings = CityManager.BuildingListGetByTownshipId(int.Parse(cid));
            var jsonCol = from x in buildings select new { Id = x.Guid, Name = x.BuildingName };
            this.Context.Response.Write(jsonS.Serialize(jsonCol));
        }

        [WebMethod]
        public void GetCityListWithTownshipJSon()
        {
            //設定輸出格式為json格式
            this.Context.Response.ContentType = "application/json";
            this.Context.Response.Charset = Encoding.UTF8.WebName;

            var jsonS = new JsonSerializer();
            var citys = LocationApiManager.GetApiCityList();

            this.Context.Response.Write(jsonS.Serialize(citys));
        }

        [WebMethod]
        public void GetFamilyMartStoreList(string userId)
        {
            var sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();

            const string methodName = "FamilyMartStoreList";
            //設定輸出格式為json格式
            this.Context.Response.ContentType = "application/json";
            this.Context.Response.Charset = Encoding.UTF8.WebName;

            var jsonS = new JsonSerializer();

            ApiReturnObject rtnObject;
            //驗證使用者
            if (!CheckApiUser(userId, out rtnObject))
            {
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
                return;
            }

            var actionEventList = mp.ActionEventInfoListGet().ToList();

            var famiActionEvent = actionEventList.FirstOrDefault();
            if (famiActionEvent != null)
            {
                Guid famiSellerGuid = famiActionEvent.SellerGuid.Value;
                var stores = new SellerCollection();
                foreach (var store in SellerFacade.GetChildrenSellers(famiSellerGuid))
                {
                    if (store.StoreStatus == (int)StoreStatus.Available && store.StoreTownshipId.HasValue)
                    {
                        stores.Add(store);
                    }
                }            

                List<FamiStore> famistoreList = new List<FamiStore>();

                foreach (var store in stores)
                {
                        KeyValuePair<double, double> storeLatLongitude =
                            LocationFacade.GetLatLongitudeByGeography(store.Coordinate);
                    famistoreList.Add(new FamiStore
                    {
                        StoreName = store.SellerName,
                            Address =
                                CityManager.CityTownShopStringGet(store.StoreTownshipId == null ? -1 : store.StoreTownshipId.Value) +
                                store.StoreAddress,
                            Coordinates =
                                new FamiStoreCoordinates
                                {
                                    Longitude = storeLatLongitude.Value,
                                    Latitude = storeLatLongitude.Key
                                }
                    });
                
                }

                rtnObject.Data = famistoreList;
            }
            this.Context.Response.Write(jsonS.Serialize(rtnObject));

            SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
        }


        public class FamiStore
        {
            public String StoreName { get; set; }
            public String Address { get; set; }
            public Object Coordinates { get; set; }
        }
        public class FamiStoreCoordinates
        {
            public double Longitude { get; set; }
            public double Latitude { get; set; }

        }


    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using log4net;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Component.API;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.BizLogic.Models;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Enumeration;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Component;
using LunchKingSite.WebLib;
using JsonSerializer = LunchKingSite.Core.JsonSerializer;
using System.Dynamic;
using LunchKingSite.BizLogic.Models.API;

namespace LunchKingSite.Web.WebService.sso
{
    /// <summary>
    /// MemberService 的摘要描述
    /// </summary>
    [WebService(Namespace = "http://www.17life.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // 若要允許使用 ASP.NET AJAX 從指令碼呼叫此 Web 服務，請取消註解下一行。
    [System.Web.Script.Services.ScriptService]
    public class MemberService : BaseService
    {
        private static IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        private static ILocationProvider lp = ProviderFactory.Instance().GetProvider<ILocationProvider>();
        private static IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        private static IOrderProvider op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
        private static ILog logger = LogManager.GetLogger(typeof(MemberService));
        private static ILog logger2 = LogManager.GetLogger(typeof(NotificationFacade));

        /// <summary>
        /// Facebook登入
        /// </summary>
        /// <param name="token">FB登入識別的TOKEN</param>
        /// <param name="userId">API的使用者編號</param>
        /// <param name="driverId">裝置識別碼</param>
        [WebMethod]
        public void FbLogin(string token, string userId, string driverId)
        {
            var jsonS = new JsonSerializer();
            ApiReturnObject rtnObject = new ApiReturnObject() { Code = ApiReturnCode.Success };
            //設定輸出格式為json格式
            this.Context.Response.ContentType = "application/json";
            this.Context.Response.Charset = Encoding.UTF8.WebName;
            //驗證使用者
            var apiUser = ApiUserManager.ApiUserGetByUserId(userId);
            if (apiUser == null)
            {
                rtnObject.Code = ApiReturnCode.ApiUserIdError;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeApiUserIdError;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }
            string fbpic;
            //登入
            SignInReplyData data = MemberUtility.FBLoginIn(token, out fbpic);

            //依據登入結果產生回傳資料
            ApiSignInReplyData replyData = UserSignInManager.ApiSignInReplyDataGet(data, Helper.GetClientIP(), driverId, null, (ApiUserDeviceType)apiUser.UserDeviceType);

            #region 判斷是否已經手機認證
            if (replyData.SignInMember != null)
            {
                //預設
                replyData.SignInMember.IsMobileAuthed = false;
                replyData.SignInMember.MobileAuthNumber = null;

                MobileMember mm = MemberFacade.GetMobileMember(replyData.SignInMember.Id);

                if (mm != null)
                {
                    if (mm.UserId != 0)
                    {
                        replyData.SignInMember.MobileAuthNumber = mm.MobileNumber;

                        if (mm.Status == 2) //完成手機認證
                        {
                            replyData.SignInMember.IsMobileAuthed = true;
                        }
                    }
                }
            }
            #endregion

            rtnObject.Data = replyData;

            this.Context.Response.Write(jsonS.Serialize(rtnObject));
            SystemFacade.SetApiLog("sso/MemberService/FbLogin", userId,
                new { token = token, userId = userId, driverId = driverId }, rtnObject, Helper.GetClientIP());
        }

        [WebMethod]
        public void LineLogin(string token, string userId, string driverId)
        {
            logger.InfoFormat("LineLogin info: {0},{1},{2}", token, userId, driverId);
            var jsonS = new JsonSerializer();
            ApiReturnObject rtnObject = new ApiReturnObject() { Code = ApiReturnCode.Success };
            //設定輸出格式為json格式
            this.Context.Response.ContentType = "application/json";
            this.Context.Response.Charset = Encoding.UTF8.WebName;
            //驗證使用者
            var apiUser = ApiUserManager.ApiUserGetByUserId(userId);
            if (apiUser == null)
            {
                rtnObject.Code = ApiReturnCode.ApiUserIdError;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeApiUserIdError;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }

            //登入
            SignInReplyData data = MemberUtility.LineLogin(token);

            //依據登入結果產生回傳資料
            ApiSignInReplyData replyData = UserSignInManager.ApiSignInReplyDataGet(
                data, Helper.GetClientIP(), driverId, null, (ApiUserDeviceType)apiUser.UserDeviceType);

            #region 判斷是否已經手機認證
            if (replyData.SignInMember != null)
            {
                //預設
                replyData.SignInMember.IsMobileAuthed = false;
                replyData.SignInMember.MobileAuthNumber = null;

                MobileMember mm = MemberFacade.GetMobileMember(replyData.SignInMember.Id);

                if (mm != null)
                {
                    if (mm.UserId != 0)
                    {
                        replyData.SignInMember.MobileAuthNumber = mm.MobileNumber;

                        if (mm.Status == 2) //完成手機認證
                        {
                            replyData.SignInMember.IsMobileAuthed = true;
                        }
                    }
                }
            }
            #endregion

            rtnObject.Data = replyData;

            this.Context.Response.Write(jsonS.Serialize(rtnObject));
            SystemFacade.SetApiLog("sso/MemberService/LineLogin", userId,
                new { token = token, userId = userId, driverId = driverId }, rtnObject, Helper.GetClientIP());
        }

        /// <summary>
        /// 17life帳號登入
        /// </summary>
        /// <param name="userName">使用者帳號</param>
        /// <param name="password">密碼</param>
        /// <param name="userId">API的使用者編號</param>
        /// <param name="driverId">裝置識別碼</param>
        [WebMethod]
        public void ContactLogin(string userName, string password, string userId, string driverId)
        {
            var jsonS = new JsonSerializer();
            ApiReturnObject rtnObject = new ApiReturnObject() { Code = ApiReturnCode.Success };
            //設定輸出格式為json格式
            this.Context.Response.ContentType = "application/json";
            this.Context.Response.Charset = Encoding.UTF8.WebName;
            //驗證使用者
            var apiUser = ApiUserManager.ApiUserGetByUserId(userId);
            if (apiUser == null)
            {
                rtnObject.Code = ApiReturnCode.ApiUserIdError;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeApiUserIdError;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }
            Helper.SetContextItem(LkSiteContextItem._DEVICE_TYPE, apiUser.UserDeviceType);

            //登入
            SignInReplyData data = MemberUtility.ContactLogin(userName, password);
            //依據登入結果產生回傳資料
            ApiSignInReplyData replyData = UserSignInManager.ApiSignInReplyDataGet(data, Helper.GetClientIP(), driverId, null, (ApiUserDeviceType)apiUser.UserDeviceType);

            if (replyData.ReplyType != SignInReply.Success)
            {
                ApiSignInReplyData errorReply = new ApiSignInReplyData
                {
                    ReplyType = replyData.ReplyType,
                    ReplyMessage = replyData.ReplyMessage
                };
                rtnObject.Data = errorReply;
                rtnObject.Message = replyData.ReplyMessage;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }

            #region 判斷是否已經手機認證
            if (replyData.SignInMember != null)
            {
                //預設
                replyData.SignInMember.IsMobileAuthed = false;
                replyData.SignInMember.MobileAuthNumber = null;

                MobileMember mm = MemberFacade.GetMobileMember(replyData.SignInMember.Id);

                if (mm != null)
                {
                    if (mm.UserId != 0)
                    {
                        replyData.SignInMember.MobileAuthNumber = mm.MobileNumber;

                        if (mm.Status == 2) //完成手機認證
                        {
                            replyData.SignInMember.IsMobileAuthed = true;
                        }
                    }
                }
            }
            #endregion

            rtnObject.Data = replyData;

            //log new ticket for debug
            if (data.Reply == SignInReply.Success)
            {
                OrderFromType fromType = OrderFromType.Unknown;
                if (apiUser.UserDeviceType == (int)ApiUserDeviceType.Android)
                {
                    fromType = OrderFromType.ByAndroid;
                }
                else if (apiUser.UserDeviceType == (int)ApiUserDeviceType.IOS)
                {
                    fromType = OrderFromType.ByIOS;
                }
                mp.AccountAuditSet(data.SignInMember.UniqueId, data.SignInMember.UserName, LunchKingSite.Core.Helper.GetClientIP(),
                    AccountAuditAction.LogNewTicket, true, replyData.LoginTicket, fromType);
            }

            this.Context.Response.Write(jsonS.Serialize(rtnObject));
            SystemFacade.SetApiLog("sso/MemberService/FbLogin", userId,
                 new { userId = userId, driverId = driverId }, rtnObject, Helper.GetClientIP());
        }

        /// <summary>
        /// 利用ticket進行登入
        /// </summary>
        /// <param name="ticket"></param>
        /// <param name="id">Unique Id</param>
        /// <param name="userId"></param>
        /// <param name="driverId">裝置識別碼</param>
        [WebMethod]
        public void ContactLoginByTicket(string ticket, int id, string userId, string driverId)
        {
            var jsonS = new JsonSerializer();
            ApiReturnObject rtnObject = new ApiReturnObject() { Code = ApiReturnCode.Success };
            //設定輸出格式為json格式
            this.Context.Response.ContentType = "application/json";
            this.Context.Response.Charset = Encoding.UTF8.WebName;
            //驗證使用者
            var apiUser = ApiUserManager.ApiUserGetByUserId(userId);
            if (apiUser == null)
            {
                rtnObject.Code = ApiReturnCode.ApiUserIdError;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeApiUserIdError;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }
            Helper.SetContextItem(LkSiteContextItem._DEVICE_TYPE, apiUser.UserDeviceType);

            //登入
            SignInReplyData data = MemberUtility.ContactLoginByTicket(ticket, id, driverId);
            //依據登入結果產生回傳資料
            ApiSignInReplyData replyData = UserSignInManager.ApiSignInReplyDataGet(data, Helper.GetClientIP(), driverId, ticket, (ApiUserDeviceType)apiUser.UserDeviceType);

            //log new ticket for debug
            if (data.Reply == SignInReply.Success)
            {
                OrderFromType fromType = OrderFromType.Unknown;
                if (apiUser.UserDeviceType == (int)ApiUserDeviceType.Android)
                {
                    fromType = OrderFromType.ByAndroid;
                }
                else if (apiUser.UserDeviceType == (int)ApiUserDeviceType.IOS)
                {
                    fromType = OrderFromType.ByIOS;
                }
                mp.AccountAuditSet(data.SignInMember.UniqueId, data.SignInMember.UserName, LunchKingSite.Core.Helper.GetClientIP(),
                    AccountAuditAction.LogNewTicket, true, replyData.LoginTicket, fromType);
            }

            //APP端如果沒有Ticket設定為錯誤
            if (string.IsNullOrWhiteSpace(replyData.LoginTicket))
            {
                var errorReply = new ApiSignInReplyData
                {
                    ReplyType = SignInReply.TicketSignInError,
                    ReplyMessage = I18N.Phrase.SignInReplyTicketSignInError
                };
                rtnObject.Data = errorReply;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }

            #region 判斷是否已經手機認證
            if (replyData.SignInMember != null)
            {
                //預設
                replyData.SignInMember.IsMobileAuthed = false;
                replyData.SignInMember.MobileAuthNumber = null;

                MobileMember mm = MemberFacade.GetMobileMember(replyData.SignInMember.Id);

                if (mm != null)
                {
                    if (mm.UserId != 0)
                    {
                        replyData.SignInMember.MobileAuthNumber = mm.MobileNumber;

                        if (mm.Status == 2) //完成手機認證
                        {
                            replyData.SignInMember.IsMobileAuthed = true;
                        }
                    }
                }
            }
            #endregion

            rtnObject.Data = replyData;

            this.Context.Response.Write(jsonS.Serialize(rtnObject));
            SystemFacade.SetApiLog("sso/MemberService/ContactLoginByTicket", userId,
                new { ticket = ticket, Id = id, userId = userId, driverId = driverId }, rtnObject, Helper.GetClientIP());
        }

        /// <summary>
        /// Payeasy登入
        /// </summary>
        /// <param name="memId">使用者帳號</param>
        /// <param name="password">密碼</param>
        /// <param name="userId">API使用者編號</param>
        /// <param name="driverId">裝置識別碼</param>
        [WebMethod]
        public void PayeasyLogin(string memId, string password, string userId, string driverId)
        {
            var jsonS = new JsonSerializer();
            ApiReturnObject rtnObject = new ApiReturnObject() { Code = ApiReturnCode.Success };
            //設定輸出格式為json格式
            this.Context.Response.ContentType = "application/json";
            this.Context.Response.Charset = Encoding.UTF8.WebName;
            //驗證使用者
            var apiUser = ApiUserManager.ApiUserGetByUserId(userId);
            if (apiUser == null)
            {
                rtnObject.Code = ApiReturnCode.ApiUserIdError;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeApiUserIdError;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }
            Helper.SetContextItem(LkSiteContextItem._DEVICE_TYPE, apiUser.UserDeviceType);
            //登入
            //SignInReplyData data = MemberUtility.PayeasyLogin(memId,password);

            ApiSignInReplyData replyData = new ApiSignInReplyData();

            //登入
            SignInReplyData data = MemberUtility.PayeasyLogin(memId, password);

            //依據登入結果產生回傳資料
            replyData = UserSignInManager.ApiSignInReplyDataGet(data, Helper.GetClientIP(), driverId, null, (ApiUserDeviceType)apiUser.UserDeviceType);

            #region 判斷是否已經手機認證
            if (replyData.SignInMember != null)
            {
                //預設
                replyData.SignInMember.IsMobileAuthed = false;
                replyData.SignInMember.MobileAuthNumber = null;

                MobileMember mm = MemberFacade.GetMobileMember(replyData.SignInMember.Id);

                if (mm != null)
                {
                    if (mm.UserId != 0)
                    {
                        replyData.SignInMember.MobileAuthNumber = mm.MobileNumber;

                        if (mm.Status == 2) //完成手機認證
                        {
                            replyData.SignInMember.IsMobileAuthed = true;
                        }
                    }
                }
            }
            #endregion

            rtnObject.Data = replyData;
            this.Context.Response.Write(jsonS.Serialize(rtnObject));
            SystemFacade.SetApiLog("sso/MemberService/PayeasyLogin", userId,
                new { userId = userId, driverId = driverId }, rtnObject, Helper.GetClientIP());
        }

        /// <summary>
        /// 外部網站帳號進行串接建立會員資料，並同時完成登入作業
        /// </summary>
        /// <param name="email">登記的email</param>
        /// <param name="externalUserId">串接的外部帳號編號</param>
        /// <param name="signType">登入類型代號PAYEASY或FACEBOOK等</param>
        /// <param name="cityId">登記的會員所在城市</param>
        /// <param name="userId">使用者編號</param>
        /// <param name="driverId">裝置識別碼</param>
        [WebMethod]
        public void RegisterExternalMember(string email, string externalUserId, string signType, int cityId, string userId, string driverId)
        {

            var jsonS = new JsonSerializer();
            ApiReturnObject rtnObject = new ApiReturnObject() { Code = ApiReturnCode.Success };
            //設定輸出格式為json格式
            this.Context.Response.ContentType = "application/json";
            this.Context.Response.Charset = Encoding.UTF8.WebName;
            //驗證使用者
            var apiUser = ApiUserManager.ApiUserGetByUserId(userId);
            if (apiUser == null)
            {
                rtnObject.Code = ApiReturnCode.ApiUserIdError;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeApiUserIdError;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }
            Helper.SetContextItem(LkSiteContextItem._DEVICE_TYPE, apiUser.UserDeviceType);
            //驗證傳入的資料
            SingleSignOnSource signOnSource;
            if (!SingleSignOnSource.TryParse(signType, out signOnSource))
            {
                rtnObject.Code = ApiReturnCode.InputError;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeInputError;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }
            //登入(一律訂閱EDM)
            SignInReplyData data = MemberUtility.RegisterExternalMember(email, externalUserId, signOnSource, cityId, true);

            //依據登入結果產生回傳資料
            ApiSignInReplyData replyData = UserSignInManager.ApiSignInReplyDataGet(data, Helper.GetClientIP(), driverId, null, (ApiUserDeviceType)apiUser.UserDeviceType);
            rtnObject.Data = replyData;

            logger.InfoFormat("RegisterNewMember: {0}, {1}, {2}, {3}, driverId={4}",
                email, userId, apiUser.UserDeviceType, replyData.ReplyType, driverId);

            this.Context.Response.Write(jsonS.Serialize(rtnObject));

            ApiUserManager.ApiUsedLogSet(apiUser, "sso/MemberService/RegisterExternalMember",
                jsonS.Serialize(new { email = email, externalUserId = externalUserId, signType = signType, cityId = cityId, userId = userId, driverId = driverId }));
        }

        /// <summary>
        /// 註冊康太會員帳號
        /// </summary>
        /// <param name="email">會員EMAIL</param>
        /// <param name="password">密碼</param>
        /// <param name="cityId">註冊的城市編號</param>
        /// <param name="userId">API USER編號</param>
        [WebMethod]
        public void RegisterNewMember(string email, string password, int cityId, string userId)
        {
            var jsonS = new JsonSerializer();
            ApiReturnObject rtnObject = new ApiReturnObject() { Code = ApiReturnCode.Success };
            //設定輸出格式為json格式
            this.Context.Response.ContentType = "application/json";
            this.Context.Response.Charset = Encoding.UTF8.WebName;
            //驗證使用者
            var apiUser = ApiUserManager.ApiUserGetByUserId(userId);
            if (apiUser == null)
            {
                rtnObject.Code = ApiReturnCode.ApiUserIdError;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeApiUserIdError;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }

            Helper.SetContextItem(LkSiteContextItem._DEVICE_TYPE, apiUser.UserDeviceType);

            //登入(一律訂閱EDM)
            RegisterContactMemberReplyData replyData = MemberUtility.RegisterContactMember(email, password, cityId, true);

            logger.InfoFormat("RegisterNewMember: {0}, {1}, {2}, {3}", email, userId, apiUser.UserDeviceType, replyData.Reply);

            rtnObject.Data = replyData;

            this.Context.Response.Write(jsonS.Serialize(rtnObject));

            ApiUserManager.ApiUsedLogSet(apiUser, "sso/MemberService/RegisterNewMember",
                jsonS.Serialize(new { email = email, password = password, cityId = cityId, userId = userId }));
        }

        /// <summary>
        /// 回傳供使用者註冊時選取的城市選單，內容為 id、name 組成的json陣列
        /// </summary>
        [WebMethod]
        public void RegisterCityList(string userId)
        {
            ApiReturnObject rtnObject = new ApiReturnObject() { Code = ApiReturnCode.Success };
            var jsonS = new JsonSerializer();
            //設定輸出格式為json格式
            this.Context.Response.ContentType = "application/json";
            this.Context.Response.Charset = Encoding.UTF8.WebName;
            //驗證使用者
            var apiUser = ApiUserManager.ApiUserGetByUserId(userId);
            if (apiUser == null)
            {
                rtnObject.Code = ApiReturnCode.ApiUserIdError;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeApiUserIdError;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }


            var cities = CityManager.Citys.Where(x => x.Code != "SYS");
            var jsonCol = from x in cities select new { id = x.Id, name = x.CityName };
            rtnObject.Data = jsonCol;

            this.Context.Response.Write(jsonS.Serialize(rtnObject));

            ApiUserManager.ApiUsedLogSet(apiUser, "sso/MemberService/RegisterCityList", "");
        }

        /// <summary>
        /// 寄發會員email認證信
        /// </summary>
        /// <param name="userName">使用者登記的帳號</param>
        /// <param name="userId">API USER帳號</param>
        [WebMethod]
        public void SendAccountConfirmMail(string userName, string userId)
        {
            ApiReturnObject rtnObject = new ApiReturnObject() { Code = ApiReturnCode.Success };

            var jsonS = new JsonSerializer();
            //設定輸出格式為json格式
            this.Context.Response.ContentType = "application/json";
            this.Context.Response.Charset = Encoding.UTF8.WebName;
            //驗證使用者
            var apiUser = ApiUserManager.ApiUserGetByUserId(userId);
            if (apiUser == null)
            {
                rtnObject.Code = ApiReturnCode.ApiUserIdError;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeApiUserIdError;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }
            if (string.IsNullOrWhiteSpace(userName))
            {
                rtnObject.Code = ApiReturnCode.InputError;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeInputError;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }

            MemberSendConfirmMailReplyType replyType = MemberUtility.SendAccountConfirmMail(userName);
            rtnObject.Data = new { ReplyType = replyType };

            this.Context.Response.Write(jsonS.Serialize(rtnObject));

            ApiUserManager.ApiUsedLogSet(apiUser, "sso/MemberService/SendAccountConfirmMail", jsonS.Serialize(new { userName = userName, userId = userId }));
        }

        [WebMethod]
        public void GetMemberDetailData(string userId)
        {
            ApiReturnObject rtnObject = new ApiReturnObject() { Code = ApiReturnCode.Success };

            var jsonS = new JsonSerializer();
            //設定輸出格式為json格式
            this.Context.Response.ContentType = "application/json";
            this.Context.Response.Charset = Encoding.UTF8.WebName;
            //驗證使用者
            var apiUser = ApiUserManager.ApiUserGetByUserId(userId);
            if (apiUser == null)
            {
                rtnObject.Code = ApiReturnCode.ApiUserIdError;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeApiUserIdError;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }
            //取得會員名稱
            string userName = HttpContext.Current.User.Identity.Name;
            if (string.IsNullOrWhiteSpace(userName))
            {
                rtnObject.Code = ApiReturnCode.UserNoSignIn;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeUserNoSignIn;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }
            string message;
            ApiMemberDetail rtn = PponDealApiManager.ApiMemberDetailGet(userName, out message, ApiUserManager.IsOldAppUserId(userId));
            rtnObject.Data = rtn;

            this.Context.Response.Write(jsonS.Serialize(rtnObject));

            ApiUserManager.ApiUsedLogSet(apiUser, "sso/MemberService/GetMemberDeliveryData", jsonS.Serialize(new { userId = userId }));
        }

        /// <summary>
        /// 修改會員資料
        /// </summary>
        /// <param name="data"></param>
        /// <param name="userId"></param>
        [WebMethod]
        public void SaveMemberDetailData(string data, string userId)
        {
            ApiReturnObject rtnObject = new ApiReturnObject();

            var jsonS = new JsonSerializer();
            //設定輸出格式為json格式
            this.Context.Response.ContentType = "application/json";
            this.Context.Response.Charset = Encoding.UTF8.WebName;
            //驗證使用者
            var apiUser = ApiUserManager.ApiUserGetByUserId(userId);
            if (apiUser == null)
            {
                rtnObject.Code = ApiReturnCode.ApiUserIdError;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeApiUserIdError;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }

            ApiMemberDetail delivery = new ApiMemberDetail();
            try
            {
                delivery = jsonS.Deserialize<ApiMemberDetail>(data);
            }
            catch
            {
                rtnObject.Code = ApiReturnCode.InputError;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeInputError;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }

            //取得會員名稱
            string userName = HttpContext.Current.User.Identity.Name;
            if (string.IsNullOrWhiteSpace(userName))
            {
                rtnObject.Code = ApiReturnCode.UserNoSignIn;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeUserNoSignIn;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }


            string message;
            if (PponDealApiManager.ApiMemberDetailSet(delivery, userName, out message))
            {
                rtnObject.Code = ApiReturnCode.Success;
            }
            else
            {
                rtnObject.Code = ApiReturnCode.InputError;
                rtnObject.Message = message;
            }

            this.Context.Response.Write(jsonS.Serialize(rtnObject));

            //ApiUserManager.ApiUsedLogSet(apiUser, "sso/MemberService/ModifyMemberDetailData", jsonS.Serialize(new { data=data , userId = userId }));
        }

        /// <summary>
        /// 取得收件人資料(排除memberDelivery中 isdefault的紀錄)
        /// </summary>
        /// <param name="userId"></param>
        [WebMethod]
        public void GetMemberAddresseeList(string userId)
        {
            ApiReturnObject rtnObject = new ApiReturnObject() { Code = ApiReturnCode.Success };

            var jsonS = new JsonSerializer();
            //設定輸出格式為json格式
            this.Context.Response.ContentType = "application/json";
            this.Context.Response.Charset = Encoding.UTF8.WebName;
            //驗證使用者
            var apiUser = ApiUserManager.ApiUserGetByUserId(userId);
            if (apiUser == null)
            {
                rtnObject.Code = ApiReturnCode.ApiUserIdError;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeApiUserIdError;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }
            //取得會員名稱
            string userName = HttpContext.Current.User.Identity.Name;
            if (string.IsNullOrWhiteSpace(userName))
            {
                rtnObject.Code = ApiReturnCode.UserNoSignIn;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeUserNoSignIn;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }
            string message;
            List<ApiMemberDelivery> rtn = PponDealApiManager.ApiMemberDeliveryGetByAddressee(userName, out message, ApiUserManager.IsOldAppUserId(userId));
            rtnObject.Data = rtn;
            rtnObject.Message = message;

            this.Context.Response.Write(jsonS.Serialize(rtnObject));

            ApiUserManager.ApiUsedLogSet(apiUser, "sso/MemberService/GetMemberAddresseeList", jsonS.Serialize(new { userId = userId }));
        }

        /// <summary>
        /// 刪除某筆收件人資料
        /// </summary>
        /// <param name="userId"></param>
        [WebMethod]
        public void DeleteMemberAddressee(string deliveryId, string userId)
        {
            ApiReturnObject rtnObject = new ApiReturnObject();

            var jsonS = new JsonSerializer();
            //設定輸出格式為json格式
            this.Context.Response.ContentType = "application/json";
            this.Context.Response.Charset = Encoding.UTF8.WebName;
            //驗證使用者
            var apiUser = ApiUserManager.ApiUserGetByUserId(userId);
            if (apiUser == null)
            {
                rtnObject.Code = ApiReturnCode.ApiUserIdError;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeApiUserIdError;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }
            //取得會員名稱
            string userName = HttpContext.Current.User.Identity.Name;
            if (string.IsNullOrWhiteSpace(userName))
            {
                rtnObject.Code = ApiReturnCode.UserNoSignIn;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeUserNoSignIn;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }
            int delId;
            if (!int.TryParse(deliveryId, out delId))
            {
                rtnObject.Code = ApiReturnCode.InputError;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeInputError;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }

            string message;
            if (PponDealApiManager.deleteMemberAddressee(userName, delId, out message))
            {
                rtnObject.Code = ApiReturnCode.Success;
            }
            else
            {
                rtnObject.Code = ApiReturnCode.InputError;
            }
            rtnObject.Message = message;

            this.Context.Response.Write(jsonS.Serialize(rtnObject));

            ApiUserManager.ApiUsedLogSet(apiUser, "sso/MemberService/DeleteMemberAddresseeList", jsonS.Serialize(new { deliveryId = deliveryId, userId = userId }));
        }

        /// <summary>
        /// 新增會員收件人資料
        /// </summary>
        /// <param name="data"></param>
        /// <param name="userId"></param>
        [WebMethod]
        public void InsertMemberAddressee(string data, string userId)
        {
            ApiReturnObject rtnObject = new ApiReturnObject();

            var jsonS = new JsonSerializer();
            //設定輸出格式為json格式
            this.Context.Response.ContentType = "application/json";
            this.Context.Response.Charset = Encoding.UTF8.WebName;
            //驗證使用者
            var apiUser = ApiUserManager.ApiUserGetByUserId(userId);
            if (apiUser == null)
            {
                rtnObject.Code = ApiReturnCode.ApiUserIdError;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeApiUserIdError;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }

            ApiMemberDelivery delivery = new ApiMemberDelivery();
            try
            {
                delivery = jsonS.Deserialize<ApiMemberDelivery>(data);
            }
            catch
            {
                rtnObject.Code = ApiReturnCode.InputError;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeInputError;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }

            //取得會員名稱
            string userName = HttpContext.Current.User.Identity.Name;
            if (string.IsNullOrWhiteSpace(userName))
            {
                rtnObject.Code = ApiReturnCode.UserNoSignIn;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeUserNoSignIn;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }

            try
            {
                var memberDelivery = PponDealApiManager.InsertMemberAddressee(userName, delivery);
                rtnObject.Data = new { DeliveryId = memberDelivery.Id };
                rtnObject.Code = ApiReturnCode.Success;
            }
            catch (Exception)
            {
                rtnObject.Code = ApiReturnCode.InputError;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeInputError;
            }

            this.Context.Response.Write(jsonS.Serialize(rtnObject));
        }

        /// <summary>
        /// 取得會員各項紅利與購物金的餘額
        /// </summary>
        /// <param name="userId"></param>
        [WebMethod]
        public void GetMemberCashData(string userId)
        {
            ApiReturnObject rtnObject = new ApiReturnObject() { Code = ApiReturnCode.Success };

            var jsonS = new JsonSerializer();
            //設定輸出格式為json格式
            this.Context.Response.ContentType = "application/json";
            this.Context.Response.Charset = Encoding.UTF8.WebName;
            //驗證使用者
            var apiUser = ApiUserManager.ApiUserGetByUserId(userId);
            if (apiUser == null)
            {
                rtnObject.Code = ApiReturnCode.ApiUserIdError;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeApiUserIdError;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }
            //取得會員名稱
            string userName = HttpContext.Current.User.Identity.Name;
            if (string.IsNullOrWhiteSpace(userName))
            {
                rtnObject.Code = ApiReturnCode.UserNoSignIn;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeUserNoSignIn;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }

            ApiUserCashDetail rtn = PponDealApiManager.ApiUserCashDetailGetByUserName(userName);
            rtnObject.Data = rtn;

            this.Context.Response.Write(jsonS.Serialize(rtnObject));

            ApiUserManager.ApiUsedLogSet(apiUser, "sso/MemberService/GetMemberCashData", jsonS.Serialize(new { userId = userId }));
        }

        /// <summary>
        /// 檢查會員是否登入中
        /// </summary>
        /// <param name="userId"></param>
        [WebMethod]
        public void UserIsSignIn(string userId)
        {
            ApiReturnObject rtnObject = new ApiReturnObject() { Code = ApiReturnCode.Success };

            var jsonS = new JsonSerializer();
            //設定輸出格式為json格式
            this.Context.Response.ContentType = "application/json";
            this.Context.Response.Charset = Encoding.UTF8.WebName;
            //驗證使用者
            var apiUser = ApiUserManager.ApiUserGetByUserId(userId);
            if (apiUser == null)
            {
                rtnObject.Code = ApiReturnCode.ApiUserIdError;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeApiUserIdError;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }
            //取得會員名稱
            string userName = HttpContext.Current.User.Identity.Name;
            if (string.IsNullOrWhiteSpace(userName))
            {
                rtnObject.Code = ApiReturnCode.UserNoSignIn;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeUserNoSignIn;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }

            this.Context.Response.Write(jsonS.Serialize(rtnObject));

            ApiUserManager.ApiUsedLogSet(apiUser, "sso/MemberService/UserIsSignIn", jsonS.Serialize(new { userId = userId }));
        }

        [WebMethod]
        public void ForgotPassword(string userName, string userId)
        {
            ApiReturnObject rtnObject = new ApiReturnObject();

            var jsonS = new JsonSerializer();
            //設定輸出格式為json格式
            this.Context.Response.ContentType = "application/json";
            this.Context.Response.Charset = Encoding.UTF8.WebName;
            //驗證使用者
            var apiUser = ApiUserManager.ApiUserGetByUserId(userId);
            if (apiUser == null)
            {
                rtnObject.Code = ApiReturnCode.ApiUserIdError;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeApiUserIdError;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }
            if (string.IsNullOrWhiteSpace(userName))
            {
                rtnObject.Code = ApiReturnCode.InputError;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeInputError;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }

            //檢查有無該會員
            Member mem = mp.MemberGet(userName);
            if (mem != null && mem.IsLoaded)
            {
                MemberAuthInfo mai = mp.MemberAuthInfoGet(mem.UniqueId);
                if (mai != null && mai.IsLoaded)
                {
                    string[] authPair = NewMemberUtility.GenEmailAuthCode();
                    if (mai.ForgetPasswordDate.HasValue && int.Equals(0, DateTime.Compare(DateTime.Now.Date, mai.ForgetPasswordDate.Value.Date)))
                    {
                        mai.PasswordQueryTime = mai.PasswordQueryTime.HasValue ? mai.PasswordQueryTime.Value + 1 : 1;
                    }
                    else
                    {
                        mai.PasswordQueryTime = 1;
                    }
                    mai.ForgetPasswordDate = DateTime.Now;
                    mai.ForgetPasswordKey = authPair[0];
                    mai.ForgetPasswordCode = authPair[1];
                    mp.MemberAuthInfoSet(mai);

                    // sent forget password mail
                    MemberFacade.SendForgetPasswordMail(mem.UserEmail, mem.UniqueId.ToString(), mai.ForgetPasswordKey, mai.ForgetPasswordCode);
                    rtnObject.Code = ApiReturnCode.Success;
                }
                else
                {
                    rtnObject.Code = ApiReturnCode.InputError;
                    rtnObject.Message = "查無此會員";
                }
            }
            else
            {
                rtnObject.Code = ApiReturnCode.InputError;
                rtnObject.Message = "查無此會員";
            }




            this.Context.Response.Write(jsonS.Serialize(rtnObject));

            ApiUserManager.ApiUsedLogSet(apiUser, "sso/MemberService/ForgotPassword", jsonS.Serialize(new { userName = userName, userId = userId }));
        }

        #region 好康檔次收藏功能相關
        [WebMethod]
        public void SetMemberCollectNotice(string token, bool enabled, int mobileType, string deviceId, string userId)
        {
            ApiReturnObject rtnObject = new ApiReturnObject();

            var jsonS = new JsonSerializer();
            //設定輸出格式為json格式
            this.Context.Response.ContentType = "application/json";
            this.Context.Response.Charset = Encoding.UTF8.WebName;
            //驗證使用者
            var apiUser = ApiUserManager.ApiUserGetByUserId(userId);
            if (apiUser == null)
            {
                rtnObject.Code = ApiReturnCode.ApiUserIdError;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeApiUserIdError;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }
            //取得會員名稱
            string userName = HttpContext.Current.User.Identity.Name;
            if (string.IsNullOrWhiteSpace(userName))
            {
                rtnObject.Code = ApiReturnCode.UserNoSignIn;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeUserNoSignIn;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }
            int memberUniqueId = MemberFacade.GetUniqueId(userName);

            MobileOsType type;

            if (Enum.IsDefined(typeof(MobileOsType), mobileType))
            {
                type = (MobileOsType)mobileType;
            }
            else
            {
                rtnObject.Code = ApiReturnCode.MobileOsTypeError;
                rtnObject.Message = I18N.Phrase.MobileOsTypeError;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }

            try
            {
                NotificationFacade.SetMemberCollectNotice(token, enabled, type, deviceId, memberUniqueId);

                rtnObject.Code = ApiReturnCode.Success;
                rtnObject.Data = null;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
            }
            catch (Exception)
            {
                rtnObject.Code = ApiReturnCode.Error;
                rtnObject.Data = null;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
            }
        }
        
        /// <summary>
        /// 紀錄收藏的資料
        /// </summary>
        /// <param name="bid"></param>
        /// <param name="collect"></param>
        /// <param name="notice"></param>
        /// <param name="userId"></param>
        [WebMethod]
        public void SetMemberCollectDeal(string bid, bool collect, bool notice, string userId)
        {
            ApiReturnObject rtnObject = new ApiReturnObject();

            var jsonS = new JsonSerializer();
            //設定輸出格式為json格式
            this.Context.Response.ContentType = "application/json";
            this.Context.Response.Charset = Encoding.UTF8.WebName;
            //驗證使用者
            var apiUser = ApiUserManager.ApiUserGetByUserId(userId);
            if (apiUser == null)
            {
                rtnObject.Code = ApiReturnCode.ApiUserIdError;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeApiUserIdError;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }

            Guid bGuid;
            if (!Guid.TryParse(bid, out bGuid))
            {
                rtnObject.Code = ApiReturnCode.InputError;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeInputError;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }

            //取得會員名稱
            string userName = HttpContext.Current.User.Identity.Name;
            if (string.IsNullOrWhiteSpace(userName))
            {
                rtnObject.Code = ApiReturnCode.UserNoSignIn;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeUserNoSignIn;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }
            int memberUniqueId = MemberFacade.GetUniqueId(userName);

            if (MemberFacade.MemberCollectDealSetForApp(memberUniqueId, bGuid, collect, notice))
            {
                rtnObject.Code = ApiReturnCode.Success;
                rtnObject.Data = null;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
            }
            else
            {
                rtnObject.Code = ApiReturnCode.Error;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeError;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
            }
        }
        
        /// <summary>
        /// 取消登入會員所有已過期檔次的收藏紀錄
        /// </summary>
        /// <param name="userId"></param>
        [WebMethod]
        public void RemoveMemberCollectDealOutOfDate(string userId)
        {
            ApiReturnObject rtnObject = new ApiReturnObject();

            var jsonS = new JsonSerializer();
            //設定輸出格式為json格式
            this.Context.Response.ContentType = "application/json";
            this.Context.Response.Charset = Encoding.UTF8.WebName;
            //驗證使用者
            var apiUser = ApiUserManager.ApiUserGetByUserId(userId);
            if (apiUser == null)
            {
                rtnObject.Code = ApiReturnCode.ApiUserIdError;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeApiUserIdError;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }

            //取得會員名稱
            string userName = HttpContext.Current.User.Identity.Name;
            if (string.IsNullOrWhiteSpace(userName))
            {
                rtnObject.Code = ApiReturnCode.UserNoSignIn;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeUserNoSignIn;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }
            int memberUniqueId = MemberFacade.GetUniqueId(userName);

            try
            {
                mp.MemberCollectDealRemoveOutOfDateDeal(memberUniqueId);

                rtnObject.Code = ApiReturnCode.Success;
                rtnObject.Data = null;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
            }
            catch
            {
                rtnObject.Code = ApiReturnCode.Error;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeError;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
            }
        }
        /// <summary>
        /// 取消登入會員的特定收藏檔次的收藏紀錄，指定方式為傳入檔次的BID陣列
        /// </summary>
        /// <param name="bidList"></param>
        /// <param name="userId"></param>
        [WebMethod]
        public void RemoveMemberCollectDeal(string bidList, string userId)
        {
            ApiReturnObject rtnObject = new ApiReturnObject();

            var jsonS = new JsonSerializer();
            //設定輸出格式為json格式
            this.Context.Response.ContentType = "application/json";
            this.Context.Response.Charset = Encoding.UTF8.WebName;
            //驗證使用者
            var apiUser = ApiUserManager.ApiUserGetByUserId(userId);
            if (apiUser == null)
            {
                rtnObject.Code = ApiReturnCode.ApiUserIdError;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeApiUserIdError;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }
            //驗證傳入的bidList是否正確
            List<Guid> bids;
            try
            {
                bids = jsonS.Deserialize<List<Guid>>(bidList);
            }
            catch
            {
                rtnObject.Code = ApiReturnCode.InputError;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeInputError;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }

            //取得會員名稱
            string userName = HttpContext.Current.User.Identity.Name;
            if (string.IsNullOrWhiteSpace(userName))
            {
                rtnObject.Code = ApiReturnCode.UserNoSignIn;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeUserNoSignIn;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }
            int memberUniqueId = MemberFacade.GetUniqueId(userName);

            try
            {
                mp.MemberCollectDealRemove(memberUniqueId, bids);

                rtnObject.Code = ApiReturnCode.Success;
                rtnObject.Data = null;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
            }
            catch
            {
                rtnObject.Code = ApiReturnCode.Error;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeError;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
            }
        }

        /// <summary>
        /// 取得會員收藏的檔次資料
        /// </summary>
        /// <param name="userId"></param>
        [WebMethod]
        public void GetMemberCollectDealList(string userId)
        {
            ApiReturnObject rtnObject = new ApiReturnObject();

            var jsonS = new JsonSerializer();
            //設定輸出格式為json格式
            this.Context.Response.ContentType = "application/json";
            this.Context.Response.Charset = Encoding.UTF8.WebName;
            //驗證使用者
            var apiUser = ApiUserManager.ApiUserGetByUserId(userId);
            if (apiUser == null)
            {
                rtnObject.Code = ApiReturnCode.ApiUserIdError;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeApiUserIdError;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }
            //取得會員名稱
            string userName = HttpContext.Current.User.Identity.Name;
            if (string.IsNullOrWhiteSpace(userName))
            {
                rtnObject.Code = ApiReturnCode.UserNoSignIn;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeUserNoSignIn;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }
            int memberUniqueId = MemberFacade.GetUniqueId(userName);

            try
            {
                rtnObject.Code = ApiReturnCode.Success;
                rtnObject.Data = ApiMemberManager.GetApiMemberCollectDealList(memberUniqueId);
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
            }
            catch
            {
                rtnObject.Code = ApiReturnCode.Error;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeError;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
            }
        }

        #endregion 好康檔次收藏功能相關

        #region 會員訂單與優惠券

        /// <summary>
        /// [新版]回傳會員的各訂單類別所擁有的訂單數量
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="isLatest">只顯示近4個月的訂單</param>
        [Obsolete("2018/9/18時查過，5天內未使用，暫訂2018/10/18後刪除")]
        [WebMethod]
        public void CouponListMainGroupGetListByNewFilterType(string userId, bool isLatest)
        {
            ApiReturnObject rtnObject = new ApiReturnObject() { Code = ApiReturnCode.Success };

            var jsonS = new JsonSerializer();
            //設定輸出格式為json格式
            this.Context.Response.ContentType = "application/json";
            this.Context.Response.Charset = Encoding.UTF8.WebName;

            #region 驗證使用者

            var apiUser = ApiUserManager.ApiUserGetByUserId(userId);
            if (apiUser == null)
            {
                rtnObject.Code = ApiReturnCode.ApiUserIdError;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeApiUserIdError;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }

            string userName = HttpContext.Current.User.Identity.Name;
            if (string.IsNullOrWhiteSpace(userName))
            {
                rtnObject.Code = ApiReturnCode.UserNoSignIn;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeUserNoSignIn;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }

            #endregion

            var uid = MemberFacade.GetUniqueId(userName);
            List<dynamic> data = PponDealApiManager.ApiCouponListMainGroupGetListByUser(uid, isLatest);

            //寫入列舉的說明文字
            foreach (var item in data)
            {
                NewCouponListFilterType filterType = (NewCouponListFilterType)((object)item).GetField<int>("FilterType");
                ((object)item).SetField<string>("Text", WebUtility.GetTextFromEnum(I18N.Phrase.ResourceManager, filterType));
            }

            rtnObject.Data = data;

            this.Context.Response.Write(jsonS.Serialize(rtnObject));

            ApiUserManager.ApiUsedLogSet(apiUser, "sso/MemberService/CouponListMainGroupGetList", jsonS.Serialize(new { userId = userId, isLatest = isLatest }));
        }

        /// <summary>
        /// [新版]回傳會員的各訂單類別所擁有的訂單數量 (支援成套商品)
        /// 原 CouponListMainGroupGetListByNewFilterType 加入分群(一般Coupon/成套商品/再來券)
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="isLatest">只顯示近4個月的訂單</param>
        [WebMethod]
        public void CouponListMainGroupCountByGrouping(string userId, bool isLatest)
        {
            ApiReturnObject rtnObject = new ApiReturnObject() { Code = ApiReturnCode.Success };

            var jsonS = new JsonSerializer();
            //設定輸出格式為json格式
            this.Context.Response.ContentType = "application/json";
            this.Context.Response.Charset = Encoding.UTF8.WebName;

            #region 驗證使用者

            var apiUser = ApiUserManager.ApiUserGetByUserId(userId);
            if (apiUser == null)
            {
                rtnObject.Code = ApiReturnCode.ApiUserIdError;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeApiUserIdError;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }

            string userName = HttpContext.Current.User.Identity.Name;
            if (string.IsNullOrWhiteSpace(userName))
            {
                rtnObject.Code = ApiReturnCode.UserNoSignIn;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeUserNoSignIn;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }

            #endregion

            Member mem = MemberFacade.GetMember(userName);
            int uid = mem.UniqueId;

            if (config.EnableFamiCoffee)
            {
                //考慮建新API
                #region 新版組訂單Group
                var data = PponDealApiManager.GetApiOrderGroupListMain(uid, isLatest, true);

                #region 防盜刷

                if (mem.IsFraudSuspect)
                {
                    ApiOrderGrouptMain orderGroups = data.FirstOrDefault(t => t.GroupType == (int)CouponListGroupType.Coupon);
                    if (orderGroups != null)
                    {
                        var allCoupons = orderGroups.CouponList.FirstOrDefault(t => t.FilterType == NewCouponListFilterType.None);
                        var notUsedCoupons = orderGroups.CouponList.FirstOrDefault(t => t.FilterType == NewCouponListFilterType.NotUsed);
                        allCoupons.Count = allCoupons.Count - notUsedCoupons.Count;
                        notUsedCoupons.Count = 0;
                    }
                }

                #endregion

                rtnObject.Data = data;
                #endregion
            }
            else
            {
                #region 舊版組訂單Group
                
                List<dynamic> data = PponDealApiManager.ApiCouponListMainGroupGetListByUser(uid, isLatest, true);

                //寫入列舉的說明文字
                if (config.IsGroupCouponOn)
                {
                    foreach (var gt in data)
                    {
                        foreach (var item in ((object)gt).GetField<List<object>>("CouponList"))
                        {
                            NewCouponListFilterType filterType =
                                (NewCouponListFilterType)((object)item).GetField<int>("FilterType");
                            ((object)item).SetField<string>("Text",
                                WebUtility.GetTextFromEnum(I18N.Phrase.ResourceManager, filterType));
                        }
                    }
                }
                else
                {
                    foreach (var item in data)
                    {
                        NewCouponListFilterType filterType = (NewCouponListFilterType)((object)item).GetField<int>("FilterType");
                        ((object)item).SetField<string>("Text", WebUtility.GetTextFromEnum(I18N.Phrase.ResourceManager, filterType));
                    }
                }

                rtnObject.Data = data;

                #endregion
            }

            this.Context.Response.Write(jsonS.Serialize(rtnObject));

            ApiUserManager.ApiUsedLogSet(apiUser, "sso/MemberService/CouponListMainGroupGetList", jsonS.Serialize(new { userId = userId, isLatest = isLatest }));
        }

        /// <summary>
        /// [新版]依據filterType查詢會員訂單資料
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="filterType"></param>
        /// <param name="isLatest"></param>
        [WebMethod]
        public void CouponListMainGetListByNewFilterType(string userId, string filterType, bool isLatest)
        {
            ApiReturnObject rtnObject = new ApiReturnObject() { Code = ApiReturnCode.Success };

            var jsonS = new JsonSerializer();
            //設定輸出格式為json格式
            this.Context.Response.ContentType = "application/json";
            this.Context.Response.Charset = Encoding.UTF8.WebName;
            //驗證使用者
            var apiUser = ApiUserManager.ApiUserGetByUserId(userId);
            if (apiUser == null)
            {
                rtnObject.Code = ApiReturnCode.ApiUserIdError;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeApiUserIdError;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }

            NewCouponListFilterType theType = NewCouponListFilterType.None;

            if (!Enum.TryParse(filterType, out theType))
            {
                rtnObject.Code = ApiReturnCode.InputError;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeInputError;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }

            string userName = HttpContext.Current.User.Identity.Name;
            if (string.IsNullOrWhiteSpace(userName))
            {
                rtnObject.Code = ApiReturnCode.UserNoSignIn;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeUserNoSignIn;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }

            Member mem = MemberFacade.GetMember(userName); 
            var data = PponDealApiManager.ApiPponOrderListItemGetListByFilterType(mem.UniqueId, theType, isLatest);

            #region 防盜刷

            if (mem.IsFraudSuspect)
            {
                data = data.Where(t => t.CountType != NewCouponListFilterType.NotUsed).ToList();
            }

            #endregion

            rtnObject.Data = data;

            this.Context.Response.Write(jsonS.Serialize(rtnObject));

            ApiUserManager.ApiUsedLogSet(apiUser, "sso/MemberService/GetMemberOrderListByFilterType", jsonS.Serialize(new { filterType, userId }));
        }

        /// <summary>
        /// [舊版]回傳會員的各訂單類別所擁有的訂單數量
        /// </summary>
        /// <param name="userId"></param>
        [Obsolete("2018/9/18時查過，5天內未使用，暫訂2018/10/18後刪除")]
        [WebMethod]
        public void CouponListMainGroupGetList(string userId)
        {

            ApiReturnObject rtnObject = new ApiReturnObject() { Code = ApiReturnCode.Success };

            var jsonS = new JsonSerializer();
            //設定輸出格式為json格式
            this.Context.Response.ContentType = "application/json";
            this.Context.Response.Charset = Encoding.UTF8.WebName;
            //驗證使用者
            var apiUser = ApiUserManager.ApiUserGetByUserId(userId);
            if (apiUser == null)
            {
                rtnObject.Code = ApiReturnCode.ApiUserIdError;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeApiUserIdError;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }

            string userName = HttpContext.Current.User.Identity.Name;
            if (string.IsNullOrWhiteSpace(userName))
            {
                rtnObject.Code = ApiReturnCode.UserNoSignIn;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeUserNoSignIn;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }

            var uid = MemberFacade.GetUniqueId(userName);
            rtnObject.Data = PponDealApiManager.ApiCouponListMainGroupGetListByUser(uid);

            this.Context.Response.Write(jsonS.Serialize(rtnObject));

            ApiUserManager.ApiUsedLogSet(apiUser, "sso/MemberService/CouponListMainGroupGetList", jsonS.Serialize(new { userId = userId }));
        }

        /// <summary>
        /// [舊版]依據filterType查詢會員訂單資料
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="filterType"></param>
        [WebMethod]
        public void CouponListMainGetList(string userId, string filterType)
        {
            ApiReturnObject rtnObject = new ApiReturnObject() { Code = ApiReturnCode.Success };

            var jsonS = new JsonSerializer();
            //設定輸出格式為json格式
            this.Context.Response.ContentType = "application/json";
            this.Context.Response.Charset = Encoding.UTF8.WebName;
            //驗證使用者
            var apiUser = ApiUserManager.ApiUserGetByUserId(userId);
            if (apiUser == null)
            {
                rtnObject.Code = ApiReturnCode.ApiUserIdError;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeApiUserIdError;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }

            CouponListFilterType theType = CouponListFilterType.None;

            if (!Enum.TryParse(filterType, out theType))
            {
                rtnObject.Code = ApiReturnCode.InputError;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeInputError;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }

            string userName = HttpContext.Current.User.Identity.Name;
            if (string.IsNullOrWhiteSpace(userName))
            {
                rtnObject.Code = ApiReturnCode.UserNoSignIn;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeUserNoSignIn;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }

            var uid = MemberFacade.GetUniqueId(userName);
            var rtn = PponDealApiManager.ApiPponOrderListItemGetListByFilterType(uid, theType);

            rtnObject.Data = rtn;

            this.Context.Response.Write(jsonS.Serialize(rtnObject));

            ApiUserManager.ApiUsedLogSet(apiUser, "sso/MemberService/GetMemberOrderListByFilterType", jsonS.Serialize(new { filterType = filterType, userId = userId }));
        }

        /// <summary>
        /// 依據訂單GUID取得訂單相關資料
        /// </summary>
        /// <param name="guid"></param>
        /// <param name="userId"></param>
        [WebMethod]
        public void GetMemberOrderByOrderGuid(string guid, string userId)
        {
            ApiReturnObject rtnObject = new ApiReturnObject() { Code = ApiReturnCode.Success };

            var jsonS = new JsonSerializer();
            //設定輸出格式為json格式
            this.Context.Response.ContentType = "application/json";
            this.Context.Response.Charset = Encoding.UTF8.WebName;
            //驗證使用者
            var apiUser = ApiUserManager.ApiUserGetByUserId(userId);
            if (apiUser == null)
            {
                rtnObject.Code = ApiReturnCode.ApiUserIdError;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeApiUserIdError;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }

            Guid orderGuid;
            if (!Guid.TryParse(guid, out orderGuid))
            {
                rtnObject.Code = ApiReturnCode.InputError;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeInputError;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }

            string userName = HttpContext.Current.User.Identity.Name;
            if (string.IsNullOrWhiteSpace(userName))
            {
                rtnObject.Code = ApiReturnCode.UserNoSignIn;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeUserNoSignIn;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }

            ApiUserOrder rtn = PponDealApiManager.ApiUserOrderGetByOrderGuid(orderGuid, MemberFacade.GetUniqueId(userName), false);
            
            #region SKM
            
            if (rtn.BehaviorType.Equals((int)BehaviorType.Skm) == true)
            {
                foreach (var item in rtn.SkmPponDeal.SkmAvailabilities)
                {
                    item.BrandCounterName = SkmFacade.PrefixStoreName(item.BrandCounterName);
                }

                foreach (ApiCouponData coupon in rtn.CouponList)
                {
                    coupon.CouponUsedType = (coupon.CouponUsedType == ApiCouponUsedType.FailDeal ||
                                                coupon.CouponUsedType == ApiCouponUsedType.CouponRefund ||
                                                coupon.CouponUsedType == ApiCouponUsedType.ATMRefund ||
                                                coupon.CouponUsedType == ApiCouponUsedType.WaitForRefund ||
                                                ApiSystemManager.DateTimeFromDateTimeString(rtn.UseEndDate) < DateTime.Now)
                                                ? ApiCouponUsedType.CouponExpired
                                                : coupon.CouponUsedType;

                    coupon.CouponUsedTypeDesc = (coupon.CouponUsedType == ApiCouponUsedType.FailDeal ||
                                                coupon.CouponUsedType == ApiCouponUsedType.CouponRefund ||
                                                coupon.CouponUsedType == ApiCouponUsedType.ATMRefund ||
                                                coupon.CouponUsedType == ApiCouponUsedType.WaitForRefund ||
                                                ApiSystemManager.DateTimeFromDateTimeString(rtn.UseEndDate) < DateTime.Now)
                                                ? "憑證過期"
                                                : coupon.CouponUsedTypeDesc;
                }
            }

            #endregion

            #region APP版本
            
            string appVersion = HttpContext.Current.Request.QueryString["appVersion"];
            //擴充OrderStatus，於下次版本才使用
            if ((int)rtn.OrderStatus >= (int)ApiOrderStatus.Unreturnable)
            {
                if (!ApiSystemManager.IsNewAppVersion(appVersion, "6.1.0", "6.1.0", userId))
                {   //當有重要功能或大改版時，app team 會給一個中版號 (這時android與ios版號可能就相同)
                    rtn.OrderStatus = ApiOrderStatus.ToHouse;
                }
            }

            //舊版不支援B型
            if (rtn.GroupCouponDealType == (int)GroupCouponDealType.CostAssign)
            {
                rtnObject.Code = ApiReturnCode.Error;
                rtnObject.Message = "此憑證類型不支援舊版APP，請更新至最新版";
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }

            #endregion

            rtnObject.Data = rtn;

            this.Context.Response.Write(jsonS.Serialize(rtnObject));

            ApiUserManager.ApiUsedLogSet(apiUser, "sso/MemberService/GetMemberOrderByOrderGuid", jsonS.Serialize(new { guid = guid, userId = userId }));
        }

        /// <summary>
        /// 依據訂單GUID取得訂單相關資料FOR新版成套票券
        /// </summary>
        /// <param name="guid"></param>
        /// <param name="userId"></param>
        [WebMethod]
        public void GetMemberOrderByOrderGuidV2(string guid, string userId)
        {
            ApiReturnObject rtnObject = new ApiReturnObject() { Code = ApiReturnCode.Success };

            var jsonS = new JsonSerializer();
            //設定輸出格式為json格式
            this.Context.Response.ContentType = "application/json";
            this.Context.Response.Charset = Encoding.UTF8.WebName;
            //驗證使用者
            var apiUser = ApiUserManager.ApiUserGetByUserId(userId);
            if (apiUser == null)
            {
                rtnObject.Code = ApiReturnCode.ApiUserIdError;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeApiUserIdError;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }

            Guid orderGuid;
            if (!Guid.TryParse(guid, out orderGuid))
            {
                rtnObject.Code = ApiReturnCode.InputError;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeInputError;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }

            string userName = HttpContext.Current.User.Identity.Name;
            if (string.IsNullOrWhiteSpace(userName))
            {
                rtnObject.Code = ApiReturnCode.UserNoSignIn;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeUserNoSignIn;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }

            //取得訂單明細內容
            ApiUserOrder rtn = PponDealApiManager.ApiUserOrderGetByOrderGuid(orderGuid, MemberFacade.GetUniqueId(userName));

            if (rtn == null)
            {
                rtnObject.Code = ApiReturnCode.DataNotExist;
                rtnObject.Message = I18N.Phrase.ApiResultCodeWpNotFound;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }

            if (rtn.CouponList.Any() 
                && (rtn.CouponType == DealCouponType.FamilyNetPincode || rtn.CouponType == DealCouponType.HiLifePincode 
                || rtn.CouponType == DealCouponType.FamiSingleBarcode))
            {
                rtn.CouponList = rtn.CouponList.OrderByDescending(x => x.CouponUsedType).ThenByDescending(x => x.UsageVerifiedTime).ToList();
            }

            #region SKM

            if (rtn.BehaviorType.Equals((int)BehaviorType.Skm) == true)
            {
                foreach (var item in rtn.SkmPponDeal.SkmAvailabilities)
                {
                    item.BrandCounterName = SkmFacade.PrefixStoreName(item.BrandCounterName);
                }

                foreach (ApiCouponData coupon in rtn.CouponList)
                {
                    coupon.CouponUsedType = (coupon.CouponUsedType == ApiCouponUsedType.FailDeal ||
                                                coupon.CouponUsedType == ApiCouponUsedType.CouponRefund ||
                                                coupon.CouponUsedType == ApiCouponUsedType.ATMRefund ||
                                                coupon.CouponUsedType == ApiCouponUsedType.WaitForRefund ||
                                                ApiSystemManager.DateTimeFromDateTimeString(rtn.UseEndDate) < DateTime.Now)
                                                ? ApiCouponUsedType.CouponExpired
                                                : coupon.CouponUsedType;

                    coupon.CouponUsedTypeDesc = (coupon.CouponUsedType == ApiCouponUsedType.FailDeal ||
                                                coupon.CouponUsedType == ApiCouponUsedType.CouponRefund ||
                                                coupon.CouponUsedType == ApiCouponUsedType.ATMRefund ||
                                                coupon.CouponUsedType == ApiCouponUsedType.WaitForRefund ||
                                                ApiSystemManager.DateTimeFromDateTimeString(rtn.UseEndDate) < DateTime.Now)
                                                ? "憑證過期"
                                                : coupon.CouponUsedTypeDesc;
                }
            }

            #endregion

            var returnClass = new ExpandoObject() as IDictionary<string, object>;
            var t = rtn.GetType();
            foreach (var pr in t.GetProperties())
            {
                //新版廢棄GroupProductUnitPrice
                //新增GroupCouponDealType，目前僅在server判斷用
                if (pr.Name != "GroupProductUnitPrice" && pr.Name != "GroupCouponDealType")
                {
                    returnClass.Add(pr.Name, pr.GetValue(rtn));
                }
            }

            rtnObject.Data = returnClass;

            this.Context.Response.Write(jsonS.Serialize(rtnObject));

            ApiUserManager.ApiUsedLogSet(apiUser, "sso/MemberService/GetMemberOrderByOrderGuidV2", jsonS.Serialize(new { guid = guid, userId = userId }));
        }

        /// <summary>
        /// 分別回傳會員收藏的優惠券，未過期與已過期的數量
        /// </summary>
        /// <param name="userId"></param>
        [WebMethod]
        public void GetMemberVourcherGroupCount(string userId)
        {
            ApiReturnObject rtnObject = new ApiReturnObject() { Code = ApiReturnCode.Success };

            var jsonS = new JsonSerializer();
            //設定輸出格式為json格式
            this.Context.Response.ContentType = "application/json";
            this.Context.Response.Charset = Encoding.UTF8.WebName;
            //驗證使用者
            var apiUser = ApiUserManager.ApiUserGetByUserId(userId);
            if (apiUser == null)
            {
                rtnObject.Code = ApiReturnCode.ApiUserIdError;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeApiUserIdError;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }

            string userName = HttpContext.Current.User.Identity.Name;
            if (string.IsNullOrWhiteSpace(userName))
            {
                rtnObject.Code = ApiReturnCode.UserNoSignIn;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeUserNoSignIn;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }

            int uId = MemberFacade.GetUniqueId(userName);

            rtnObject.Data = ApiVourcherManager.ApiUserVourcherGroupMainGetList(uId);

            this.Context.Response.Write(jsonS.Serialize(rtnObject));

            ApiUserManager.ApiUsedLogSet(apiUser, "sso/MemberService/GetMemberOrderByOrderGuid", jsonS.Serialize(new { userId = userId }));
        }

        /// <summary>
        /// 依據 傳入的type 回傳會員收藏的優惠券資料
        /// </summary>
        /// <param name="type">已過期與未過期的type,VourcherGroupMainType型別的列舉</param>
        /// <param name="userId"></param>
        [WebMethod]
        public void GetMemberVourcherListByGroupType(int type, string userId)
        {

            ApiReturnObject rtnObject = new ApiReturnObject() { Code = ApiReturnCode.Success };

            var jsonS = new JsonSerializer();
            //設定輸出格式為json格式
            this.Context.Response.ContentType = "application/json";
            this.Context.Response.Charset = Encoding.UTF8.WebName;
            //驗證使用者
            var apiUser = ApiUserManager.ApiUserGetByUserId(userId);
            if (apiUser == null)
            {
                rtnObject.Code = ApiReturnCode.ApiUserIdError;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeApiUserIdError;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }

            string userName = HttpContext.Current.User.Identity.Name;
            if (string.IsNullOrWhiteSpace(userName))
            {
                rtnObject.Code = ApiReturnCode.UserNoSignIn;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeUserNoSignIn;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }

            if (!Enum.IsDefined(typeof(VourcherGroupMainType), type))
            {
                rtnObject.Code = ApiReturnCode.InputError;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeInputError;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }

            int uId = MemberFacade.GetUniqueId(userName);

            rtnObject.Data = ApiVourcherManager.ApiVourcherEventSynopsesGetListUserAndByGroupType(0, 0, uId, (VourcherGroupMainType)type);

            this.Context.Response.Write(jsonS.Serialize(rtnObject));

            ApiUserManager.ApiUsedLogSet(apiUser, "sso/MemberService/GetMemberOrderByOrderGuid", jsonS.Serialize(new { userId = userId }));
        }

        /// <summary>
        /// 取消收藏會員收藏的優惠券中，已過期的優惠券
        /// </summary>
        /// <param name="userId"></param>
        [WebMethod]
        public void VourcherCollectSetDisabledForOverdueEvent(string userId)
        {
            ApiReturnObject rtnObject = new ApiReturnObject() { Code = ApiReturnCode.Success };

            var jsonS = new JsonSerializer();
            //設定輸出格式為json格式
            this.Context.Response.ContentType = "application/json";
            this.Context.Response.Charset = Encoding.UTF8.WebName;
            //驗證使用者
            var apiUser = ApiUserManager.ApiUserGetByUserId(userId);
            if (apiUser == null)
            {
                rtnObject.Code = ApiReturnCode.ApiUserIdError;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeApiUserIdError;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }

            string userName = HttpContext.Current.User.Identity.Name;
            if (string.IsNullOrWhiteSpace(userName))
            {
                rtnObject.Code = ApiReturnCode.UserNoSignIn;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeUserNoSignIn;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }

            int uId = MemberFacade.GetUniqueId(userName);

            int updateCount = ApiVourcherManager.VourcherCollectSetDisabledForOverdueEvent(uId);
            rtnObject.Data = new { count = updateCount };

            this.Context.Response.Write(jsonS.Serialize(rtnObject));

            ApiUserManager.ApiUsedLogSet(apiUser, "sso/MemberService/VourcherCollectSetDisabledForOverdueEvent", jsonS.Serialize(new { userId = userId }));
        }

        [WebMethod]
        public void GetPponOrderAndVourcherOverview(string userId)
        {
            ApiReturnObject rtnObject = new ApiReturnObject() { Code = ApiReturnCode.Success };

            var jsonS = new JsonSerializer();
            //設定輸出格式為json格式
            this.Context.Response.ContentType = "application/json";
            this.Context.Response.Charset = Encoding.UTF8.WebName;
            //驗證使用者
            var apiUser = ApiUserManager.ApiUserGetByUserId(userId);
            if (apiUser == null)
            {
                rtnObject.Code = ApiReturnCode.ApiUserIdError;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeApiUserIdError;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }

            string userName = HttpContext.Current.User.Identity.Name;
            if (string.IsNullOrWhiteSpace(userName))
            {
                rtnObject.Code = ApiReturnCode.UserNoSignIn;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeUserNoSignIn;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }

            int uId = MemberFacade.GetUniqueId(userName);
            var couponList = PponDealApiManager.ApiCouponListMainGroupGetListByUser(uId);
            var vourcherList = ApiVourcherManager.ApiUserVourcherGroupMainGetList(uId);

            rtnObject.Data = new { CouponList = couponList, VourcherList = vourcherList };

            this.Context.Response.Write(jsonS.Serialize(rtnObject));

            ApiUserManager.ApiUsedLogSet(apiUser, "sso/MemberService/VourcherCollectSetDisabledForOverdueEvent", jsonS.Serialize(new { userId = userId }));
        }

        /// <summary>
        /// 傳送憑證簡訊到會員登記的手機，會員需登入
        /// </summary>
        /// <param name="orderGuid">訂單編號</param>
        /// <param name="sequenceNumList">憑證編號</param>
        /// <param name="mobile">手機號碼</param>
        /// <param name="changePhone">是否同時修改會員登記手機號碼</param>
        /// <param name="userId">apiUser</param>
        [WebMethod]
        public void SendPponCouponSMS(string orderGuid, string sequenceNumList, string mobile, bool changePhone, string userId)
        {
            ApiReturnObject rtnObject = new ApiReturnObject() { Code = ApiReturnCode.Success };

            var jsonS = new JsonSerializer();
            //設定輸出格式為json格式
            this.Context.Response.ContentType = "application/json";
            this.Context.Response.Charset = Encoding.UTF8.WebName;
            //驗證使用者
            var apiUser = ApiUserManager.ApiUserGetByUserId(userId);
            if (apiUser == null)
            {
                rtnObject.Code = ApiReturnCode.ApiUserIdError;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeApiUserIdError;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }

            string userName = HttpContext.Current.User.Identity.Name;
            if (string.IsNullOrWhiteSpace(userName))
            {
                rtnObject.Code = ApiReturnCode.UserNoSignIn;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeUserNoSignIn;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }

            int uId = MemberFacade.GetUniqueId(userName);

            Guid oGuid;
            if (!Guid.TryParse(orderGuid, out oGuid))
            {
                rtnObject.Code = ApiReturnCode.InputError;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeInputError;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }
            List<string> sequenctNumList = jsonS.Deserialize<List<string>>(sequenceNumList);
            if (sequenceNumList == null || sequenceNumList.Count() == 0)
            {
                rtnObject.Code = ApiReturnCode.InputError;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeInputError;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }
            int sendCount = OrderUtility.SendOrderPponCouponSMS(oGuid, sequenctNumList, mobile, uId, changePhone);
            rtnObject.Data = new { count = sendCount };

            this.Context.Response.Write(jsonS.Serialize(rtnObject));

            ApiUserManager.ApiUsedLogSet(apiUser, "sso/MemberService/SendPponCouponSMS", jsonS.Serialize(new
            {
                orderGuid = orderGuid,
                sequenceNumList = sequenceNumList,
                mobile = mobile,
                userId = userId,
                ch
                    = changePhone
            }));
        }

        /// <summary>
        /// 傳送憑證擋到指定mail，會員需登入
        /// </summary>
        /// <param name="orderGuid"></param>
        /// <param name="sequenceNumList">要取得的couponList列表</param>
        /// <param name="email"></param>
        /// <param name="userId"></param>
        [WebMethod]
        public void SendPponCouponEMail(string orderGuid, string sequenceNumList, string email, string userId)
        {
            ApiReturnObject rtnObject = new ApiReturnObject() { Code = ApiReturnCode.Success };

            var jsonS = new JsonSerializer();
            //設定輸出格式為json格式
            this.Context.Response.ContentType = "application/json";
            this.Context.Response.Charset = Encoding.UTF8.WebName;
            //驗證使用者
            var apiUser = ApiUserManager.ApiUserGetByUserId(userId);
            if (apiUser == null)
            {
                rtnObject.Code = ApiReturnCode.ApiUserIdError;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeApiUserIdError;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }

            string userName = HttpContext.Current.User.Identity.Name;
            if (string.IsNullOrWhiteSpace(userName))
            {
                rtnObject.Code = ApiReturnCode.UserNoSignIn;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeUserNoSignIn;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }

            int uId = MemberFacade.GetUniqueId(userName);

            Guid oGuid;
            if (!Guid.TryParse(orderGuid, out oGuid))
            {
                rtnObject.Code = ApiReturnCode.InputError;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeInputError;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }
            List<string> sequenctNumList = jsonS.Deserialize<List<string>>(sequenceNumList);
            if (sequenceNumList == null || sequenceNumList.Count() == 0)
            {
                rtnObject.Code = ApiReturnCode.InputError;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeInputError;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }
            int sendCount = MemberFacade.SendOrderPponCouponToEMail(oGuid, sequenctNumList, email, uId);
            rtnObject.Data = new { count = sendCount };

            this.Context.Response.Write(jsonS.Serialize(rtnObject));

            ApiUserManager.ApiUsedLogSet(apiUser, "sso/MemberService/SendPponCouponEMail", jsonS.Serialize(new { orderGuid = orderGuid, sequenceNumList = sequenceNumList, email = email, userId = userId }));
        }

        #endregion 會員訂單與優惠券

        #region 訊息中心 API

        [WebMethod]
        public void MessageCollection(string userId, string deviceId)
        {
            const string methodName = "MessageCollection";
            //設定輸出格式為json格式
            ContextBaseSetting();

            var jsonS = new JsonSerializer();
            ApiReturnObject rtnObject;

            #region 身份驗證

            var apiUser = ApiUserManager.ApiUserGetByUserId(userId);
            if (apiUser == null)
            {
                this.Context.Response.Write("{\"error\":\"錯誤的帳號\"}");
                return;
            }
            //驗證APP UserId(非會員UserId)
            if (!CheckApiUser(userId, out rtnObject))
            {
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
                return;
            }            
            //檢查會員是否已登入
            Member mem = null;
            if (User.Identity.IsAuthenticated)
            {
                mem = MemberFacade.GetMember(User.Identity.Name);
            }

            #endregion

            #region Device Info

            //建立手機裝置與登入會員的關聯
            DeviceIdentyfierInfo device = lp.DeviceIdentyfierInfoListGet(deviceId)
                .OrderByDescending(t => t.Id).FirstOrDefault();
            if (device != null)
            {
                device.ModifyTime = DateTime.Now;
                if (mem != null && mem.IsLoaded && device.MemberUniqueId != mem.UniqueId)
                {
                    device.MemberUniqueId = mem.UniqueId;
                    lp.DeviceIdentyfierInfoSet(device);
                }
            }
            else
            {
                device = new DeviceIdentyfierInfo();
                device.IdentifierCode = deviceId;
                device.CreateTime = DateTime.Now;
                if (mem != null && mem.IsLoaded)
                {
                    device.MemberUniqueId = mem.UniqueId;
                }
                lp.DeviceIdentyfierInfoSet(device);
            }

            #endregion

            var messages = BuildMessageCollections(device, mem);

            rtnObject.Code = ApiReturnCode.Success;
            rtnObject.Data = new { Messages = messages };
            this.Context.Response.Write(jsonS.Serialize(rtnObject));

            SetApiLog(GetMethodName(methodName), userId,
                new { userId, deviceId, userName = User.Identity.Name }, rtnObject);
        }

        
        private static List<MessageCollectionModel> BuildMessageCollections(DeviceIdentyfierInfo device, Member mem)
        {
            //TODO 應該要針對不同類型的推播(廣告型、個人訊息型)，給予不同的生命週期
            List<ViewPushRecordMessage> records = new List<ViewPushRecordMessage>();

            if (mem != null && mem.IsLoaded)
            {
                //將屬於該device，但還沒有歸在哪位會員的訊息，
                //在會員登入後綁定給該會員
                lp.DevicePushRecordSetMemberIfNoOwner(device.Id, mem.UniqueId);

                ViewPushRecordMessageCollection memberPushRecords = lp.ViewPushRecordMessageGetListByMember(mem.UniqueId,
                    config.PushMessageDefaultExpirationDays);

                //舊資料可能重複，重複的不重複加入
                HashSet<int> messageIdSet = new HashSet<int>();
                foreach (var memberPushRecord in memberPushRecords)
                {
                    int messageId = memberPushRecord.Id;
                    if (messageIdSet.Contains(messageId) == false)
                    {
                        messageIdSet.Add(messageId);
                        records.Add(memberPushRecord);
                    }
                    else
                    {
                        //重複的就標記移除
                        logger2.Info("set devicepushrcord is_remove: " + memberPushRecord.PushRecordId);
                        lp.DevicePushRecordSetRemove(memberPushRecord.PushRecordId);
                    }
                }
            }
            else
            {
                ViewPushRecordMessageCollection noOwnerRecords = lp.ViewPushRecordMessageGetListByDeviceAndNoOwner(device.Id,
                    config.PushMessageDefaultExpirationDays);
                records.AddRange(noOwnerRecords);
            }

            records = records.OrderByDescending(t => t.PushTime).ToList();

            #region 取Push Message，套用pokeball資料字典

            var messages = new List<MessageCollectionModel>();
            foreach (var record in records)
            {
                var model = new MessageCollectionModel();
                model.Id = record.PushRecordId;
                model.IsRead = record.IsRead ? "True" : "False";
                model.SendTime = ApiSystemManager.DateTimeToDateTimeString(record.PushTime);                
                model.Subject = record.Subject;
                model.Content = record.Content;

                var pokeball = NotificationFacade.GetPokeball(record);
                if (pokeball != null)
                {
                    model.PokeballList = new List<object> { pokeball };
                    messages.Add(model);
                }
            }

            #endregion

            return messages;
        }

        [WebMethod]
        public void ReadMessage(string userId, string deviceId, int messageId, int triggertype)
        {
            const string methodName = "ReadMessage";
            //設定輸出格式為json格式
            ContextBaseSetting();

            var jsonS = new JsonSerializer();

            ApiReturnObject rtnObject;


            var apiUser = ApiUserManager.ApiUserGetByUserId(userId);
            if (apiUser == null)
            {
                this.Context.Response.Write("{\"error\":\"錯誤的帳號\"}");
                return;
            }

            //驗證使用者
            if (!CheckApiUser(userId, out rtnObject))
            {
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
                return;
            }

            //檢查會員是否已登入
            string userName;
            CheckMemberSingin(out userName, out rtnObject);
            rtnObject.Code = ApiReturnCode.Success;
            rtnObject.Message = string.Empty;

            ////檢查會員Id是否存在
            int memberUserId = ApiPCPManager.GetMemberUserId(userName);
            if (string.IsNullOrEmpty(userName))
            {
                memberUserId = 0;
            }
            //getDeviceId
            DeviceIdentyfierInfo device;
            var deviceCols = lp.DeviceIdentyfierInfoListGet(deviceId);
            if (deviceCols.Any())
            {
                device = deviceCols.ToList().OrderByDescending(x => x.Id).First();
                device.ModifyTime = DateTime.Now;
            }
            else
            {
                device = new DeviceIdentyfierInfo();
                device.IdentifierCode = deviceId;
                device.CreateTime = DateTime.Now;
            }

            if (memberUserId > 0)
            {
                device.MemberUniqueId = memberUserId;
            }

            lp.DeviceIdentyfierInfoSet(device);

            var deviceRecord = lp.DevicePushRecordGet(messageId);

            deviceRecord.IsRead = true;
            deviceRecord.TriggerType = triggertype;
            deviceRecord.ReadTime = DateTime.Now;
            if (memberUserId > 0 && deviceRecord.MemberId == 0)
            {
                deviceRecord.MemberId = memberUserId;
            }
            lp.DevicePushRecordSet(deviceRecord);

            this.Context.Response.Write(jsonS.Serialize(rtnObject));

            SetApiLog(GetMethodName(methodName), userId, 
                new { userId, deviceId, messageId, triggertype }, rtnObject);
        }

        [WebMethod]
        public void RemoveMessage(string userId, string deviceId, string messageIds)
        {
            const string methodName = "RemoveMessage";
            //設定輸出格式為json格式
            ContextBaseSetting();

            var jsonS = new JsonSerializer();

            ApiReturnObject rtnObject;

            var apiUser = ApiUserManager.ApiUserGetByUserId(userId);
            if (apiUser == null)
            {
                this.Context.Response.Write("{\"error\":\"錯誤的帳號\"}");
                return;
            }

            //驗證使用者
            if (!CheckApiUser(userId, out rtnObject))
            {
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
                return;
            }

            //檢查會員是否已登入
            string userName;
            CheckMemberSingin(out userName, out rtnObject);
            rtnObject.Code = ApiReturnCode.Success;
            rtnObject.Message = string.Empty;

            ////檢查會員Id是否存在
            int memberUserId = ApiPCPManager.GetMemberUserId(userName);
            if (string.IsNullOrEmpty(userName))
            {
                memberUserId = 0;
            }
            DeviceIdentyfierInfo device;
            //確認裝置資訊
            var deviceCols = lp.DeviceIdentyfierInfoListGet(deviceId);
            if (deviceCols.Any())
            {
                device = deviceCols.ToList().OrderByDescending(x => x.Id).First();
                device.ModifyTime = DateTime.Now;
            }
            else
            {
                device = new DeviceIdentyfierInfo();
                device.IdentifierCode = deviceId;
                device.CreateTime = DateTime.Now;
            }

            if (memberUserId > 0)
            {
                device.MemberUniqueId = memberUserId;
                DevicePushRecordCollection dRecords = lp.DevicePushRecordListGet(device.Id, 0);
                foreach (var record in dRecords)
                {
                    record.MemberId = memberUserId;
                    lp.DevicePushRecordSet(record);
                }
            }

            lp.DeviceIdentyfierInfoSet(device);

            string theArray = messageIds.Replace("[", "").Replace("]", "");
            List<int> theList = (from string s in theArray.Split(',') select Convert.ToInt32(s)).ToList<int>();

            DevicePushRecordCollection devicePushRecords = lp.DevicePushRecordListGet(device.Id, memberUserId);

            List<DevicePushRecord> records;
            if (memberUserId > 0)
            {
                records = devicePushRecords.Where(x => !x.IsRemove).OrderByDescending(z => z.CreateTime).ToList();
            }
            else
            {
                records = devicePushRecords.Where(x => !x.IsRemove && !x.MemberId.HasValue).OrderByDescending(z => z.CreateTime).ToList();
            }

            foreach (var recordId in theList)
            {
                var record = records.FirstOrDefault(x => x.Id == recordId);
                if (record != null && record.IsLoaded)
                {
                    record.IsRemove = true;
                    record.RemoveTime = DateTime.Now;
                    lp.DevicePushRecordSet(record);
                }
            }

            var result = @"{""Code"":0,""Data"":{""messageIds"":" + jsonS.Serialize(theList) + @"},""Message"":""""}";

            this.Context.Response.Write(result);

            SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
        }

        [WebMethod]
        public void RemoveReadMessage(string userId, string deviceId)
        {
            const string methodName = "RemoveReadMessage";
            //設定輸出格式為json格式
            ContextBaseSetting();

            var jsonS = new JsonSerializer();

            ApiReturnObject rtnObject;


            var apiUser = ApiUserManager.ApiUserGetByUserId(userId);
            if (apiUser == null)
            {
                this.Context.Response.Write("{\"error\":\"錯誤的帳號\"}");
                return;
            }

            //驗證使用者
            if (!CheckApiUser(userId, out rtnObject))
            {
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
                return;
            }

            //檢查會員是否已登入
            string userName;
            CheckMemberSingin(out userName, out rtnObject);
            rtnObject.Code = ApiReturnCode.Success;
            rtnObject.Message = string.Empty;

            ////檢查會員Id是否存在
            int memberUserId = ApiPCPManager.GetMemberUserId(userName);
            if (string.IsNullOrEmpty(userName))
            {
                memberUserId = 0;
            }
            DeviceIdentyfierInfo device;
            //確認裝置資訊
            var deviceCols = lp.DeviceIdentyfierInfoListGet(deviceId);
            if (deviceCols.Any())
            {
                device = deviceCols.ToList().OrderByDescending(x => x.Id).First();
                device.ModifyTime = DateTime.Now;
            }
            else
            {
                device = new DeviceIdentyfierInfo();
                device.IdentifierCode = deviceId;
                device.CreateTime = DateTime.Now;
            }
            if (memberUserId > 0)
            {
                device.MemberUniqueId = memberUserId;
            }

            lp.DeviceIdentyfierInfoSet(device);

            DevicePushRecordCollection devicePushRecords = lp.DevicePushRecordListGet(device.Id, memberUserId);
            foreach (var record in devicePushRecords)
            {
                if (record != null && record.IsLoaded && record.IsRead && !record.IsRemove)
                {
                    record.IsRemove = true;
                    record.RemoveTime = DateTime.Now;
                    lp.DevicePushRecordSet(record);
                }
            }

            this.Context.Response.Write(jsonS.Serialize(rtnObject));

            SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
        }


        #endregion 訊息中心 API

        #region MasterPassBindingStatus

        [WebMethod]
        public void MasterPassBindingStatus(string userId)
        {
            ApiReturnObject rtnObject = new ApiReturnObject() { Code = ApiReturnCode.Success };
            var jsonS = new JsonSerializer();
            //設定輸出格式為json格式
            this.Context.Response.ContentType = "application/json";
            this.Context.Response.Charset = Encoding.UTF8.WebName;
            //驗證使用者
            var apiUser = ApiUserManager.ApiUserGetByUserId(userId);
            if (apiUser == null)
            {
                rtnObject.Code = ApiReturnCode.ApiUserIdError;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeApiUserIdError;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }

            //取得會員名稱
            string userName = HttpContext.Current.User.Identity.Name;
            if (string.IsNullOrWhiteSpace(userName))
            {
                rtnObject.Code = ApiReturnCode.UserNoSignIn;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeUserNoSignIn;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }

            int memberUniqueId = MemberFacade.GetUniqueId(userName);
            MasterpassPreCheckoutToken masterPassPreCheckoutToken = op.MasterPassPreCheckoutTokenGet(memberUniqueId);

            if (String.IsNullOrEmpty(masterPassPreCheckoutToken.AccessToken) || masterPassPreCheckoutToken.IsUsed)
            {
                rtnObject.Data = new { isBinding = false };
            }
            else
            {
                rtnObject.Data = new { isBinding = true };
            }

            this.Context.Response.Write(jsonS.Serialize(rtnObject));

            ApiUserManager.ApiUsedLogSet(apiUser, "sso/MemberService/MasterPassBindingStatus", jsonS.Serialize(new { userId = userId }));
        }

        #endregion MasterPassBindingStatus

        #region MasterPassBindingCancel

        [WebMethod]
        public void MasterPassBindingCancel(string userId)
        {
            ApiReturnObject rtnObject = new ApiReturnObject() { Code = ApiReturnCode.Success };
            var jsonS = new JsonSerializer();
            //設定輸出格式為json格式
            this.Context.Response.ContentType = "application/json";
            this.Context.Response.Charset = Encoding.UTF8.WebName;
            //驗證使用者
            var apiUser = ApiUserManager.ApiUserGetByUserId(userId);
            if (apiUser == null)
            {
                rtnObject.Code = ApiReturnCode.ApiUserIdError;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeApiUserIdError;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }

            //取得會員名稱
            string userName = HttpContext.Current.User.Identity.Name;
            if (string.IsNullOrWhiteSpace(userName))
            {
                rtnObject.Code = ApiReturnCode.UserNoSignIn;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeUserNoSignIn;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }
            
            int memberUniqueId = MemberFacade.GetUniqueId(userName);

            //取消綁定結果
            if (!op.MasterPassPreCheckoutTokenUpdateUsed(memberUniqueId))
            {
                rtnObject.Code = ApiReturnCode.InputError;
                rtnObject.Message = I18N.Phrase.UpdateFail;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }

            this.Context.Response.Write(jsonS.Serialize(rtnObject));

            ApiUserManager.ApiUsedLogSet(apiUser, "sso/MemberService/MasterPassBindingCancel", jsonS.Serialize(new { userId = userId }));
        }

        #endregion MasterPassBindingCancel

        /// <summary>
        /// 依據訂單GUID取得pchome 24H 物流配送資訊
        /// </summary>
        /// <param name="guid"></param>
        /// <param name="userId"></param>
        /// <param name="shipNo"></param>
        [WebMethod]
        public void GetWmsDeliveryHistory(string guid,string shipNo, string userId)
        {
            const string methodName = "GetWmsDeliveryHistory";
            ApiReturnObject rtnObject = new ApiReturnObject() { Code = ApiReturnCode.Success };

            var jsonS = new JsonSerializer();
            //設定輸出格式為json格式
            this.Context.Response.ContentType = "application/json";
            this.Context.Response.Charset = Encoding.UTF8.WebName;
            //驗證使用者
            var apiUser = ApiUserManager.ApiUserGetByUserId(userId);
            if (apiUser == null)
            {
                rtnObject.Code = ApiReturnCode.ApiUserIdError;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeApiUserIdError;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }

            Guid orderGuid;
            if (!Guid.TryParse(guid, out orderGuid))
            {
                rtnObject.Code = ApiReturnCode.InputError;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeInputError;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }

            //取得配送資訊
            var wmsHistory = WmsFacade.GetWmsOrderHistory(orderGuid, shipNo);

            rtnObject.Data = wmsHistory;

            this.Context.Response.Write(jsonS.Serialize(rtnObject));

            SetApiLog(GetMethodName(methodName), userId, 
                new { guid, userId}, rtnObject);
        }
    }

}

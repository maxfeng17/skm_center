﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.DataOrm;
using LunchKingSite.BizLogic.Model.Verification;
using LunchKingSite.BizLogic.Component.Verification;

namespace LunchKingSite.Web.WebService.Verification
{
    /// <summary>
    /// Summary description for MohistVerifyService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class MohistVerifyService : System.Web.Services.WebService
    {
        VerifyAccount va = new VerifyAccount();
        VerifyCoupon vc = new VerifyCoupon();

        [WebMethod]
        public string VerifyCoupon(string account, string passWord, string mohistDealsId, string barcode)
        {
            string rtnInfo = string.Empty;

            // 帳號登入檢查
            VerificationAccountStatusDetail acctStatus = va.ChkIsLogin(account, passWord);
            if (acctStatus.Equals(VerificationAccountStatusDetail.Logined))
            {
                if (!string.IsNullOrEmpty(barcode))
                {
                    // 查找墨攻核銷資訊
                    string couponCode = barcode.Split('-')[2];
                    string sequenceNumber = barcode.Replace(couponCode, string.Empty).TrimEnd('-');
                    MohistVerifyInfo mohistInfo = va.GetMohistVerifyInfo(mohistDealsId, sequenceNumber, couponCode);
                    if (mohistInfo.IsLoaded)
                    {
                        int couponId;
                        int.TryParse(mohistInfo.CouponId, out couponId);

                        // 判斷是否在可用期限內
                        VerificationStatus status = vc.CheckVerifyCouponStatus(couponId);
                        switch (status)
                        {
                            case VerificationStatus.CouponInit:
                            case VerificationStatus.DuInTime:
                                // 合法憑證
                                rtnInfo = OrderFacade.VerifyCouponByMohist(
                                    mohistInfo, couponId, HttpContext.Current.Request.UserHostAddress, account);
                                break;
                            case VerificationStatus.CouponOver:
                                rtnInfo = "此憑證不在兌換期限";
                                break;
                            default:
                                rtnInfo = "系統錯誤！請再試一次，或聯絡17life技術人員。造成不便，敬請見諒！";
                                break;
                        }
                    }
                    else
                        rtnInfo = "憑證資訊錯誤!!";
                }
                else
                    rtnInfo = "憑證資訊錯誤!!";
            }
            else
                rtnInfo = "帳號登入錯誤!!";

            // api user log
            var apiUser = ApiUserManager.ApiUserGetByUserId(passWord);
            if (apiUser == null)
                rtnInfo = "錯誤的API帳號";
            else
            {
                var jsonS = new JsonSerializer();
                ApiUserManager.ApiUsedLogSet(apiUser, "Verification/MohistVerifyService", jsonS.Serialize(new { account = account, passWord = passWord, mohistDealsId = mohistDealsId, barcode = barcode }), rtnInfo);
            }

            return rtnInfo;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.Services;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.DataOrm;
using System.Security.Cryptography;
using System.Text;
using System.IO;
using LunchKingSite.Core;
namespace LunchKingSite.Web.WebService.Verification
{
    /// <summary>
    /// Summary description for MobileApp
    /// </summary>
    [WebService(Namespace = "http://www.17life.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class MobileApp : System.Web.Services.WebService
    {
        VerifyAccount va = new VerifyAccount();
        [WebMethod]
        public string VerifyCouponPhone(string acctcode, string acctno, string password, string sequencenumber_code, string phonenumber)
        {
            string result = string.Empty;
            string sequencenumber = string.Empty;
            string code = string.Empty;
            string[] codes = sequencenumber_code.Split(new string[] { "-" }, StringSplitOptions.RemoveEmptyEntries);

            if (codes.Length == 3)
            {
                sequencenumber = codes.DefaultIfEmpty(string.Empty).FirstOrDefault() + "-" + codes.Skip(1).DefaultIfEmpty(string.Empty).FirstOrDefault();
                code = codes.Skip(2).DefaultIfEmpty(string.Empty).FirstOrDefault();
            }
            else if(codes.Length==1)
            {
                sequencenumber = code = sequencenumber_code;
            }
            StoreAcctInfoCollection info = va.GetAcctInfoByAcctForWPF(acctcode, acctno, password);
            if (info.Count > 0)
            {
                var data = va.ViewCouponStoreCollectionGet(info.First().SellerGuid, info.First().StoreGuid ?? Guid.Empty, sequencenumber, code);
                if (data.Count() > 0)
                {
                    if (data.Any(x => x.Status == (int)TrustStatus.Verified))
                        result = Resources.Localization.TrustStatus_Verified;
                    else if (data.Any(x => x.Status == (int)TrustStatus.Refunded))
                        result = Resources.Localization.TrustStatus_Refunded;
                    else if (data.Any(x => x.Status == (int)TrustStatus.Returned))
                        result = Resources.Localization.TrustStatus_Returned;
                    else if (data.Any(x => x.Status == (int)TrustStatus.ATM))
                        result = Resources.Localization.TrustStatus_ATM;
                    else
                    {
                        VerificationLog log = new VerificationLog();
                        log.Id = new Guid();
                        log.SellerGuid = info.First().SellerGuid;
                        log.CouponId = data.First().CouponId;
                        log.SequenceNumber = sequencenumber;
                        log.CouponCode = code;
                        log.Ip = phonenumber;
                        log.CreateTime = DateTime.Now;
                        log.Description = "phone verify";
                        log.Status = (int)VerificationStatus.CouponOk;
                        va.VerifyCouponByWpf(data.First().TrustId, log);
                        result = "核銷成功";
                    }
                }
                else
                    result = "查無資料";
            }
            else
                result = "帳密錯誤";
            return result;
        }
    }
}

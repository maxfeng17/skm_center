﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.Services;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.DataOrm;
using System.Security.Cryptography;
using System.Text;
using System.IO;
using LunchKingSite.Core;

namespace LunchKingSite.Web.WebService.Verification
{
    /// <summary>
    /// Summary description for WPF
    /// </summary>
    [WebService(Namespace = "http://www.17life.com")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WPFApp : System.Web.Services.WebService
    {
        VerifyAccount va = new VerifyAccount();
        [WebMethod]
        public AccountDealInfo Login(string acctcode, string acctno, string password)
        {
            StoreAcctInfoCollection info = va.GetAcctInfoByAcctForWPF(acctcode, acctno, password);
            if (info.Count > 0)
            {
                StoreAcctInfo store = info.First();
                ViewPponDealCollection deal_collection = va.GetDealsBySellerGuidForWPF(store.SellerGuid);
                List<DealWpf> deals = new List<DealWpf>();
                //結檔後10天內
                foreach (ViewPponDeal item in deal_collection.Where(x => x.BusinessHourDeliverTimeE.HasValue && x.BusinessHourDeliverTimeE.Value.AddDays(10) >= DateTime.Now))
                {
                    deals.Add(new DealWpf(item.BusinessHourGuid, item.EventName, item.BusinessHourDeliverTimeS.Value, item.BusinessHourDeliverTimeE.Value));
                }
                return new AccountDealInfo(true, store.SellerGuid, store.StoreGuid ?? Guid.Empty, acctcode, acctno, deals);
            }
            else
            {
                return new AccountDealInfo(false);
            }
        }
        [WebMethod]
        public bool LogInAdmin(string acctcode, string password)
        {
            return va.GetAcctSpecialInfoForWPF(acctcode, password);
        }
        [WebMethod]
        public CouponWpf[] GetCouponWpfList(Guid bid, Guid seller_guid, Guid store_guid, string acctcode, string acctno, string password)
        {
            StoreAcctInfoCollection info = va.GetAcctInfoByAcctForWPF(acctcode, acctno, password);
            if (info.Count > 0)
            {
                ViewCouponStoreCollection data = va.ViewCouponStoreCollectionGet(bid, seller_guid, store_guid);
                CouponWpf[] list = new CouponWpf[data.Count];
                int i = 0;
                foreach (ViewCouponStore item in data)
                {
                    list[i] = (new CouponWpf(item.CouponSequenceNumber, item.Status, item.ModifyTime));
                    i++;
                } return list;
            }
            else
                return new CouponWpf[0];

        }
        [WebMethod]
        public bool VerifyCouponWpf(Guid bid, Guid seller_guid, Guid store_guid, string acctcode, string acctno, string password, string sequencenumber, string code, string pc)
        {
            bool result = false;
            StoreAcctInfoCollection info = va.GetAcctInfoByAcctForWPF(acctcode, acctno, password);
            if (info.Count > 0)
            {
                var dataall = va.ViewCouponStoreCollectionGet(bid, seller_guid, store_guid, sequencenumber, code);
                var data = dataall.Where(x => x.Status < (int)TrustStatus.Verified);
                VerificationLog log = new VerificationLog();
                log.Id = new Guid();
                log.SellerGuid = seller_guid;
                log.SequenceNumber = sequencenumber;
                log.CouponCode = code;
                log.Ip = pc;
                log.CreateTime = DateTime.Now;
                if (data.Count() > 0)
                {
                    log.CouponId = data.First().CouponId;
                    log.Description = "wpf verify";
                    log.Status = (int)VerificationStatus.CouponOk;
                    va.VerifyCouponByWpf(data.First().TrustId, log);
                    result = true;
                }
                else
                {
                    log.Description = "wpf verify fail status:" + (dataall.Count() > 0 ? dataall.First().Status.ToString() : "no exist");
                    log.Status = (int)VerificationStatus.CouponUsed;
                    va.LogByWpf(log);
                    result = false;
                }
            }
            return result;
        }
    }
    [Serializable]
    public class AccountDealInfo
    {
        public bool IsLegal { get; set; }
        public Guid SellerGuid { get; set; }
        public Guid StoreGuid { get; set; }
        public string AcctCode { get; set; }
        public string AcctNo { get; set; }
        public string PassWord { get; set; }
        public List<DealWpf> Deals { get; set; }
        public AccountDealInfo(bool islegal, Guid sellerguid, Guid storeguid, string acctcode, string acctno, List<DealWpf> deals)
        {
            IsLegal = islegal;
            SellerGuid = sellerguid;
            StoreGuid = storeguid;
            AcctCode = acctcode;
            AcctNo = acctno;
            PassWord = string.Empty;
            Deals = deals;
        }
        public AccountDealInfo(bool islegal)
        {
            IsLegal = islegal;
        }
        public AccountDealInfo()
        { }
    }
    [Serializable]
    public class DealWpf
    {
        public Guid Bid { get; set; }
        public string EventName { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DealWpf() { }
        public DealWpf(Guid bid, string eventname, DateTime startdate, DateTime enddate)
        {
            Bid = bid;
            EventName = eventname;
            StartDate = startdate;
            EndDate = enddate;
        }
    }
    [Serializable]
    public class CouponWpf
    {
        public string SequenceNumber { get; set; }
        public string Code { get; set; }
        public int Status { get; set; }
        public DateTime VerifyTime { get; set; }
        public DateTime ModifyTime { get; set; }
        public string PC { get; set; }
        public bool IsUpload { get; set; }
        public bool Enable { get; set; }
        public CouponWpf(string sequencenumber, int status, DateTime modifytime)
        {
            SequenceNumber = sequencenumber;
            Status = status;
            ModifyTime = modifytime;
            IsUpload = false;
            PC = string.Empty;
            Code = string.Empty;
            Enable = true;
        }
        public CouponWpf() { }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core;

namespace LunchKingSite.Web.WebService.Verification
{
    /// <summary>
    /// Summary description for StoreAcct
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class StoreAcct : System.Web.Services.WebService
    {
        private string _reg_user_info = "{\"a\":\"$loginAcct\", \"s\":\"$loginStatus\", \"n\":\"$loginName\", \"e\":\"$loginEmail\"}";
        private string _msg_ori = "{\"msg\":\"$msg\", \"mt\":\"$time\"}";
        private string _now = string.Format("{0:yyyy/MM/dd HH:mm:ss}", DateTime.Now);
        VerifyAccount _va = new VerifyAccount();

        #region 有管理權限者登入
        [WebMethod] 
        public string Login(string a, string p)
        {
            string userInfo = string.Empty;
            Dictionary<string, string> loginInfo = new Dictionary<string, string>();
            loginInfo.Add("status", ((int)VerificationAccountStatusDetail.LoginError).ToString());
            try
            {
                loginInfo = _va.ActRegLogin(a, p);
            } finally
            {
                userInfo = _reg_user_info.Replace("$loginAcct", loginInfo["acct"])
                                        .Replace("$loginStatus", loginInfo["status"])
                                        .Replace("$loginName", loginInfo["name"])
                                        .Replace("$loginEmail", loginInfo["email"]);
            }
            return userInfo;
        }
        #endregion

        #region 註冊
        [WebMethod]
        public string actRegist(string a, string p, string ra, string rc, string e, string s)
        {//傳入值 a: account, p: password, ra: 欲註冊的帳號, rc: 註冊碼, e: IMEI, s:IMSI
            string rtnMsg = string.Empty;
            VerifyAccount va = new VerifyAccount();
            Dictionary<string, string> rtnInfo = new Dictionary<string, string>();

            VerificationAccountStatusDetail acctStatus = va.ChkIsLogin(a, p);
            try
            {
                if (acctStatus == VerificationAccountStatusDetail.Logined)
                {
                    rtnInfo = _va.ActRegist(ra, e, s, rc, a, _now);
                } else
                {
                    rtnInfo["status"] = VerificationAccountStatusDetail.LoginError.ToString();
                }
            }catch(Exception ex)
            {
                rtnInfo["status"] = VerificationAccountStatusDetail.RegistError.ToString();
                _now = ex.ToString();
            }
            finally
            {
                rtnMsg = _msg_ori.Replace("$msg", rtnInfo["status"]).Replace("$time", _now);
            }
            return rtnMsg;
        }
        #endregion

        #region 重新註冊
        [WebMethod]
        public string actReRegist(string a, string p, string ra, string roc, string rnc, string e, string s)
        {//傳入值 a: account, p: password, ra: 欲註冊的帳號, roc: 舊註冊碼, rnc: 新註冊碼, e: IMEI, s:IMSI
            string rtnMsg = string.Empty;
            VerifyAccount va = new VerifyAccount();
            Dictionary<string, string> rtnInfo = new Dictionary<string, string>();

            VerificationAccountStatusDetail acctStatus = va.ChkIsLogin(a, p);
            rtnInfo.Add("status", acctStatus.ToString());
            try
            {
                if (acctStatus == VerificationAccountStatusDetail.Logined)
                {
                    rtnInfo = _va.ActResetRegist(ra, e, s, roc, rnc, a, _now);
                }
            }
            finally
            {
                rtnMsg = _msg_ori.Replace("$msg", rtnInfo["status"]).Replace("$time", _now);
            }

            return rtnMsg;
        }
        #endregion

        #region 取消註冊
        [WebMethod]
        public string actUndoRegist(string a, string p, string ra, string rc, string e, string s)
        {//傳入值 a: account, p: password, ra: 欲註冊的帳號, rc: 註冊碼, e: IMEI, s:IMSI
            string rtnMsg = string.Empty;
            VerifyAccount va = new VerifyAccount();
            Dictionary<string, string> rtnInfo = new Dictionary<string, string>();

            VerificationAccountStatusDetail acctStatus = va.ChkIsLogin(a, p);
            rtnInfo.Add("status", acctStatus.ToString());
            try
            {
                if (acctStatus == VerificationAccountStatusDetail.Logined)
                {
                    rtnInfo = _va.ActUndoRegist(ra, e, s, rc, a, _now);
                }
            }finally
            {
                rtnMsg = _msg_ori.Replace("$msg", rtnInfo["status"]).Replace("$time", _now);
            }
            
            return rtnMsg;
        }
        #endregion

        
        
    }
}

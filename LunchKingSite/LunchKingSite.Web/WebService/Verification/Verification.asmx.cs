﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;

namespace LunchKingSite.Web.WebService.Verification
{
    /// <summary>
    /// Summary description for Verification
    /// </summary>
    [WebService(Namespace = "http://www.17life.com/WebService/Verification")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class Verification : System.Web.Services.WebService
    {
        private string _msg_ori = "{\"msg\":\"$msg\", \"mt\":\"$time\"}";
        private string _now = string.Format("{0:yyyy/MM/dd HH:mm:ss}", DateTime.Now);
        /// <summary>
        /// 登入
        /// </summary>
        /// <param name="strAcct"></param>
        /// <param name="strPw"></param>
        /// <param name="strImei"></param>
        /// <param name="strImsi"></param>
        /// <returns></returns>
        [WebMethod]
        public string Login(string strAcct, string strPw, string strImei, string strImsi)
        {
            string userInfo = "{ \"session_id\":\"$sessionId\" " +
                              ", \"login_id\":\"$loginId\", \"is_login\":\"$isLogin\" " +
                              ", \"seller_guid\":\"$seller_guid\", \"store_guid\": \"$store_guid\" " +
                              ", \"seller_name\":\"$seller_name\", \"store_name\": \"$store_name\" " +
                              ", \"is_hideal\":\"$isHideal\"}";

            VerifyAccount va = new VerifyAccount();
            Dictionary<string, string> loginInfo = va.ActLogin(strAcct, strPw, strImei, strImsi);
            userInfo =
                userInfo.Replace("$sessionId", loginInfo["session_id"])
                    .Replace("$loginId", loginInfo["login_id"])
                    .Replace("$isLogin", loginInfo["is_login"])
                    .Replace("$seller_guid", loginInfo["seller_guid"])
                    .Replace("$store_guid", loginInfo["store_guid"])
                    .Replace("$seller_name", loginInfo["seller_name"])
                    .Replace("$store_name", loginInfo["store_name"])
                    .Replace("$isHideal", loginInfo["is_hideal"]);
            return userInfo;
        }

        /// <summary>
        /// 取得檔次列表
        /// </summary>
        /// <param name="a">帳號</param>
        /// <param name="p">密碼</param>
        /// <param name="e">IMEI</param>
        /// <param name="s">IMSI</param>
        /// <param name="lt">登入時間</param>
        /// <param name="seg">SellerGuid</param>
        /// <param name="stg">StoreGuid</param>
        /// <returns></returns>
        [WebMethod]
        public string GetDeals(string a, string p, string e, string s, string lt, string seg, string stg)
        {//傳入值 a: account, p: password, e: IMEI, s:IMSI, seg: seller guid, stg: store guid
            string dealInfo = "{ \"id\":\"$bid\", \"pid\":\"$pid\", \"ina\":\"$item_name\", \"isT\":\"$is_deliver_time\" " +
                              ", \"dts\":\"$deliver_time_s\", \"dte\":\"$deliver_time_e\" "+
                              ", \"aq\":\"$all_quantity\", \"vq\":\"$verification_quantity\", \"uvq\":\"$unverification_quantity\" " +
                              ", \"vp\":\"$verification_persent\", \"sen\":\"$seller_name\", \"stn\":\"$store_name\", \"stn\":\"$store_name\"}";
            string rtnMsg = "";
            string rtnInfo = "";
            VerifyAccount va = new VerifyAccount();
            VerificationAccountStatusDetail acctStatus = va.ChkIsLogin(a, p, e, s, "", lt);
            if (acctStatus != VerificationAccountStatusDetail.Logined)
            {
                //rtnMsg = "{\"msg\":\"" + (int)acctStatus +
                //         "\",\"mt\":\"" + string.Format("{0:yyyy/MM/dd HH:mm:ss}", DateTime.Now) + "\"}";
                rtnMsg = _msg_ori.Replace("$msg", ((int) acctStatus).ToString())
                                .Replace("$time", _now);
            } else
            {
                List<Dictionary<string, string>> lDealsInfo = va.GetDeals(a, p, e, s, lt, seg, stg);
                List<Dictionary<string, string>> lHiDealsInfo = va.GetHiDeals(a, p, e, s, lt, seg, stg);
                if(lHiDealsInfo != null)
                {
                    if(lDealsInfo == null) { lDealsInfo = new List<Dictionary<string, string>>(); }
                    lDealsInfo.AddRange(lHiDealsInfo);
                }
                if (lDealsInfo == null || lDealsInfo.Count == 0)
                {
                    rtnMsg = _msg_ori.Replace("$msg", ((int)VerificationAccountStatusDetail.DealNone).ToString())
                                .Replace("$time", _now);
                        //"{\"msg\":\"" + (int) VerificationAccountStatusDetail.DealNone +
                        //     "\",\"mt\":\"" + string.Format("{0:yyyy/MM/dd HH:mm:ss}", DateTime.Now) + "\"}";
                }
                else
                {
                    int i = 0;
                    foreach (Dictionary<string, string> deal in lDealsInfo)
                    {
                        if(i > 0)
                        {
                            rtnInfo += ",";
                        }
                        rtnInfo += dealInfo.Replace("$bid", deal["deal_bid"]).Replace("$pid", deal["deal_pid"])
                            .Replace("$item_name", deal["deal_item_name"])
                            .Replace("$is_deliver_time", deal["deal_is_deliver_time"])
                            .Replace("$deliver_time_s", deal["deal_order_deliver_time_s"]).Replace("$deliver_time_e", deal["deal_order_deliver_time_e"])
                            .Replace("$all_quantity", deal["deal_a_quantity"])
                            .Replace("$verification_quantity", deal["deal_v_quantity"]).Replace("$unverification_quantity", deal["deal_uv_quantity"])
                            .Replace("$verification_persent", deal["deal_persent"])
                            .Replace("$seller_name", deal["deal_seller_name"]).Replace("$store_name", deal["deal_store_name"]);
                        i++;
                    }
                }
            }
            return (rtnMsg != "") ? rtnMsg : "[" + rtnInfo + "]";
        }

        /// <summary>
        /// 取得店家列表，核銷app用
        /// </summary>
        /// <param name="a">帳號</param>
        /// <param name="p">密碼</param>
        /// <param name="e">IMEI</param>
        /// <param name="s">IMSI</param>
        /// <param name="lt">登入時間</param>
        /// <param name="seg">SellerGuid</param>
        /// <returns></returns>
        [WebMethod]
        public string GetStores(string a, string p, string e, string s, string lt, string seg)
        {
            string dealInfo = "{ \"sen\":\"$seller_name\", \"stn\":\"$store_name\"" +
                              ", \"seg\":\"$seller_guid\", \"stg\":\"$store_guid\" }";
            string rtnMsg = "";
            string rtnInfo = "";
            VerifyAccount va = new VerifyAccount();
            VerificationAccountStatusDetail acctStatus = va.ChkIsLogin(a, p, e, s, "", lt);
            if (acctStatus != VerificationAccountStatusDetail.Logined)
            {
                rtnMsg = _msg_ori.Replace("$msg", ((int)acctStatus).ToString())
                                .Replace("$time", _now);
                //rtnMsg = "{\"msg\":\"" + (int) acctStatus +
                //         "\",\"mt\":\"" + string.Format("{0:yyyy/MM/dd HH:mm:ss}", DateTime.Now) + "\"}";
            }
            else
            {
                List<Dictionary<string, string>> lStoresInfo = va.GetStores(a, p, e, s, lt, seg);
                if (lStoresInfo == null || lStoresInfo.Count == 0)
                {
                    rtnMsg = "{\"msg\":\"" + (int) VerificationAccountStatusDetail.DealNone +
                             "\",\"mt\":\"" + string.Format("{0:yyyy/MM/dd HH:mm:ss}", DateTime.Now) + "\"}";
                }
                else
                {
                    int i = 0;
                    foreach (Dictionary<string, string> deal in lStoresInfo)
                    {
                        if (i > 0)
                        {
                            rtnInfo += ",";
                        }
                        rtnInfo += dealInfo.Replace("$seller_name", deal["deal_seller_name"]).Replace("$store_name", deal["deal_store_name"])
                            .Replace("$seller_guid", deal["deal_seller_guid"]).Replace("$store_guid", deal["deal_store_guid"]);
                        i++;
                    }
                }
            }
            return (rtnMsg != "") ? rtnMsg : "[" + rtnInfo + "]";
        }

        #region 核銷
        [WebMethod]
        public string GetCouponInfo(string a, string p, string e, string s, string lt, string seg, string stg, string cs, string cc, string ih)
        {//傳入值 a: account, p: password, e: IMEI, s:IMSI, seg: seller guid, stg: store guid, cs: sequence_code(last 4 word), cc: coupon_code, ih: is_hideal
            string rtnMsg = "";
            string rtnInfo = "";
            VerifyAccount va = new VerifyAccount();
            VerificationAccountStatusDetail acctStatus = va.ChkIsLogin(a, p, e, s, "", lt);
            if (acctStatus != VerificationAccountStatusDetail.Logined)
            {
                rtnMsg = _msg_ori.Replace("$msg", ((int)acctStatus).ToString())
                                .Replace("$time", _now);
            }
            else
            {
                VerifyCoupon vc = new VerifyCoupon();
                rtnInfo = vc.GetCouponInfo(e, s, seg, stg, cs, cc, ih);
            }
            return (rtnMsg != "") ? rtnMsg : rtnInfo;
        }
        [WebMethod]
        public string ActVerify(string a, string p, string e, string s, string lt, string seg, string stg, string cs, string cc)
        {//傳入值 a: account, p: password, e: IMEI, s:IMSI, seg: seller guid, stg: store guid, cs: sequence_code(last 4 word), cc: coupon_code
            string rtnMsg = "";
            string rtnInfo = "";
            VerifyAccount va = new VerifyAccount();
            VerificationAccountStatusDetail acctStatus = va.ChkIsLogin(a, p, e, s, "", lt);
            if (acctStatus != VerificationAccountStatusDetail.Logined)
            {
                rtnMsg = _msg_ori.Replace("$msg", ((int)acctStatus).ToString())
                                .Replace("$time", _now);
            }
            else
            {
                VerificationStatus flag = va.ActVerify(cs, cc, seg, stg, e, s, a);
                rtnInfo = "{\"msg\":\"" + (int)flag +
                          "\",\"mt\":\"" + string.Format("{0:yyyy/MM/dd HH:mm:ss}", DateTime.Now) + "\"}";
            }
            return (rtnMsg != "") ? rtnMsg : rtnInfo;
        }
        /// <summary>
        /// 取得核銷列表資料
        /// </summary>
        /// <param name="dd">DealId</param>
        /// <param name="pd">ProductId</param>
        /// <param name="sty">Start Time - Year</param>
        /// <param name="std">Start Time - Day</param>
        /// <param name="stm">Start Time - Month</param>
        /// <param name="ety">End Time - Year</param>
        /// <param name="etd">End Time - Day</param>
        /// <param name="etm">End Time - Month</param>
        /// <param name="stg">Store Guid</param>
        /// <param name="st">Query Status</param>
        /// <returns></returns>
        [WebMethod]
        public string GetVerificationListInfo(string dd, string pd, string sty, string stm, string std,
            string ety, string etm, string etd, string seg, string stg, string st)
        {
            VerifyCoupon vc = new VerifyCoupon();

            DateTime startTime, endTime;
            DateTime.TryParse(sty + "/" + stm + "/" + std + " 00:00", out startTime);
            DateTime.TryParse(ety + "/" + etm + "/" + etd + " 00:00", out endTime);
            string bid = string.Empty;
            Guid Bid = Guid.Empty;

            OrderClassification dealType = OrderClassification.LkSite;
            int status = int.Parse(st);
            if (!Guid.TryParse(dd, out Bid))
            {
                dealType = OrderClassification.HiDeal;
            }
            else
            {
                bid = Bid.ToString();
            }
            List<VerificationListInfo> list = vc.GetVerificationList(dealType, bid, dd, pd, startTime, endTime, seg, stg, int.Parse(st));

            var jsonCol = from x in list
                          select new
                          {
                              cid = x.SequenceCode,
                              time = x.Time,
                              status = (
                                status == (int)VerificationQueryStatus.Init ? "未核銷" :
                                status == (int)VerificationQueryStatus.Verified ? "已核銷" :
                                status == (int)VerificationQueryStatus.Refund ? "退貨" : ""
                              )
                          };
            return new JsonSerializer().Serialize(jsonCol);
        }

        #endregion
        

    }
}

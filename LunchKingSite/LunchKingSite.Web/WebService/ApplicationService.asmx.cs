﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using LunchKingSite.BizLogic.Component.API;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.Core;
using System.Web.UI.WebControls;
using System.Reflection;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Component;
using log4net;

namespace LunchKingSite.Web.WebService
{
    /// <summary>
    /// Summary description for ApplicationService
    /// </summary>
    [WebService(Namespace = "http://www.17life.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class ApplicationService : System.Web.Services.WebService
    {
        /// <summary>
        /// 己查app版本是否需要更新
        /// </summary>
        /// <param name="appname">APP名稱</param>
        /// <param name="version">目前版本</param>
        /// <param name="userId">api使用者編號</param>
        [WebMethod]
        public void CheckAppVersion(string appname, string version, string userId)
        {
            this.Context.Response.ContentType = "application/json";
            this.Context.Response.Charset = Encoding.UTF8.WebName;            
            var jsonS = new JsonSerializer();

            ApiReturnObject rtnObject = new ApiReturnObject() { Code = ApiReturnCode.Success };
            if (userId == null)
            {
                rtnObject.Code = ApiReturnCode.ApiUserIdError;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeApiUserIdError;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }
            Version inputVersion;
            Version inputOsVersion;
            if (!Version.TryParse(version, out inputVersion))
            {
                //轉型失敗，回傳錯誤
                rtnObject.Code = ApiReturnCode.InputError;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeInputError;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }
            else
            {
                //option osversion
                var osversion = this.Context.Request.QueryString["osversion"];
                if (!string.IsNullOrEmpty(osversion))
                {
                    //version format:major.minor[.build[.revision]]
                    if (osversion.IndexOf('.') == -1)
                    {
                        osversion = osversion + ".0";
                    }
                    if (Version.TryParse(osversion, out inputOsVersion))
                    {
                        rtnObject.Data = new { type = ApiSystemManager.CheckAppVersion(appname, inputVersion, inputOsVersion) };
                    }
                    else
                    {
                        rtnObject.Data = new { type = ApiSystemManager.CheckAppVersion(appname, inputVersion) };
                    }
                }
                else
                {
                    rtnObject.Data = new { type = ApiSystemManager.CheckAppVersion(appname, inputVersion) };
                }
            }

            this.Context.Response.Write(jsonS.Serialize(rtnObject));
        }

        [WebMethod]
        public void RefreshConfig(string key, string value)
        {

            var log = LogManager.GetLogger(typeof(ApplicationService));

            this.Context.Response.ContentType = "application/json";
            this.Context.Response.Charset = Encoding.UTF8.WebName;

            var jsonS = new JsonSerializer();

            ApiReturnObject rtnObject = new ApiReturnObject() { Code = ApiReturnCode.Success };

            var ip = this.Context.Request.UserHostAddress.ToString();

            var whiteIpList = CommonFacade.GetWebServerIpList();
            if (!whiteIpList.Contains(ip))
            {
                log.Info(ip + " not in " + whiteIpList);
                rtnObject.Code = ApiReturnCode.Error;
                rtnObject.Message = string.Format("Entry prohibited");
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }
            
            //檢查是否已登入
            string memberName = User.Identity.Name;

            if (string.IsNullOrWhiteSpace(memberName))
            {
                HttpCookie appCookie = Context.Request.Cookies[System.Web.Security.FormsAuthentication.FormsCookieName];
                var ticket = System.Web.Security.FormsAuthentication.Decrypt(appCookie.Value);
                memberName = ticket.Name;
            }

            if (string.IsNullOrWhiteSpace(memberName))
            {
                rtnObject.Code = ApiReturnCode.UserNoSignIn;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeUserNoSignIn;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }

            var isItstaff = System.Web.Security.Roles.GetRolesForUser(memberName).Any(x =>
                string.Equals(x, "itstaff", StringComparison.CurrentCultureIgnoreCase));

            if (!isItstaff)
            {
                rtnObject.Code = ApiReturnCode.Error;
                rtnObject.Message = "User has no ItStaff role.";
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }

            ISysConfProvider conf = LunchKingSite.Core.Component.ProviderFactory.Instance().GetConfig();
            WebControl ctrl = null;
            PropertyInfo pi = conf.GetType().GetProperty(key);
            if (pi == null)
            {
                rtnObject.Code = ApiReturnCode.Error;
                rtnObject.Message = "Error key";
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }

            switch (Type.GetTypeCode(pi.PropertyType))
            {
                case TypeCode.Boolean:
                    bool tmp;
                    Boolean.TryParse(value, out tmp);
                    ctrl = new CheckBox();
                    ((CheckBox)ctrl).Checked = tmp;
                    break;
                case TypeCode.Object:
                    //CheckBoxList
                    break;
                default:
                    //Enumeration
                    if (pi.PropertyType.IsEnum)
                    {
                        ctrl = new DropDownList();
                        ((DropDownList)ctrl).Items.Add(new ListItem("", value));
                        ((DropDownList)ctrl).Items.FindByValue(value).Selected = true;
                    }
                    else
                    {
                        ctrl = new TextBox();
                        ((TextBox)ctrl).Text = value;
                    }

                    break;
            }

            ctrl.ID = key;
            try
            {
                log.Info(memberName + " changed sys config. From " + ip);
                LunchKingSite.WebLib.WebUtility.UpdateInstance(conf, pi, ctrl);
            }
            catch (Exception ex)
            {
                log.Error("RefreshConfig error.", ex);
                rtnObject.Code = ApiReturnCode.Error;
                rtnObject.Message = string.Format("error:{0};{1}", ex.Message, ex.StackTrace);
            }

            this.Context.Response.Write(jsonS.Serialize(rtnObject));
        }
    }
}

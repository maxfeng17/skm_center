﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Xml;
using log4net;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Component.API;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib;
using LunchKingSite.WebLib.Views;

namespace LunchKingSite.Web.WebService
{
    /// <summary>
    /// Summary description for PponDealService
    /// </summary>
    //[WebService(Namespace = "http://tempuri.org/")]
    [WebService(Namespace = "http://www.17life.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class PponDealService : System.Web.Services.WebService
    {
        private ILog logger = LogManager.GetLogger(typeof(PponDealService));
        private static IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();

        /// <summary>
        /// 導購API
        /// </summary>
        /// <param name="UserId"></param>
        [WebMethod]
        public void PponDealDataJSon(string UserId)
        {
            //設定輸出格式為json格式
            this.Context.Response.ContentType = "application/json";
            this.Context.Response.Charset = Encoding.UTF8.WebName;

            //輸出json格式
            var apiUser = ApiUserManager.ApiUserGetByUserId(UserId);
            if (apiUser == null)
            {
                this.Context.Response.Write("{\"error\":\"錯誤的帳號\"}");
                return;
            }
            var l = PponFacade.DealInfoTranslateGetList(apiUser.CpaCode, DateTime.Today, apiUser.GrossMarginLimit, apiUser.CouponGrossMarginLimit, apiUser.DeliveryGrossMarginLimit);
            var jsonS = new JsonSerializer();
            this.Context.Response.Write(jsonS.Serialize(l));

            ApiUserManager.ApiUsedLogSet(apiUser, "PponDealService/PponDealDataJSon", jsonS.Serialize(new { UserId = UserId }));

        }

        /// <summary>
        /// 導購API
        /// 團購+品生活檔次
        /// </summary>
        /// <param name="UserId"></param>
        [WebMethod]
        public void PponPiinLifeDealDataJSon(string UserId)
        {
            //設定輸出格式為json格式
            this.Context.Response.ContentType = "application/json";
            this.Context.Response.Charset = Encoding.UTF8.WebName;

            //輸出json格式
            var apiUser = ApiUserManager.ApiUserGetByUserId(UserId);
            if (apiUser == null)
            {
                this.Context.Response.Write("{\"error\":\"錯誤的帳號\"}");
                return;
            }
            var l = PponFacade.DealInfoTranslateGetList(apiUser.CpaCode, DateTime.Today, apiUser.GrossMarginLimit, apiUser.CouponGrossMarginLimit, apiUser.DeliveryGrossMarginLimit, PponDealSpecialCityType.PponAndPiinlife);
            var jsonS = new JsonSerializer();
            Context.Response.Write(jsonS.Serialize(l));

            ApiUserManager.ApiUsedLogSet(apiUser, "PponDealService/PponDealDataJSon", jsonS.Serialize(new { UserId }));

        }

        /// <summary>
        /// 莫名不能刪，刪了webmethod無法呼叫，原因不明
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
        [WebMethod]
        public string PponActivityListJSonString(string UserId)
        {
            //驗證使用者
            var apiUser = ApiUserManager.ApiUserGetByUserId(UserId);
            if (apiUser == null)
            {
                return "{\"error\":\"錯誤的帳號\"}";
            }

            var areaList = PponCityGroup.DefaultPponCityGroup.GetPponcityForPublic().Select(pponCity => new DealAreaInfo { Id = pponCity.CityId, Name = pponCity.CityName }).ToList();

            //儲存使用紀錄
            var jsonS = new JsonSerializer();
            ApiUserManager.ApiUsedLogSet(apiUser, "PponDealService/PponActivityListJSonString", jsonS.Serialize(new { UserId = UserId }));

            return jsonS.Serialize(areaList);
        }

        /// <summary>
        /// 以多檔次代表檔的BID或子檔BID取得多檔次子檔資料的列表
        /// </summary>
        /// <param name="bid"></param>
        /// <param name="userId"></param>
        [WebMethod]
        public void ComboDealMainGetByMainDealId(string bid, string userId)
        {
            var jsonS = new JsonSerializer();
            ApiReturnObject rtnObject = new ApiReturnObject() { Code = ApiReturnCode.Success };

            //設定輸出格式為json格式
            this.Context.Response.ContentType = "application/json";
            this.Context.Response.Charset = Encoding.UTF8.WebName;

            //驗證使用者
            var apiUser = ApiUserManager.ApiUserGetByUserId(userId);
            if (apiUser == null)
            {
                rtnObject.Code = ApiReturnCode.ApiUserIdError;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeApiUserIdError;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }

            Guid bGuid;

            if (!Guid.TryParse(bid, out bGuid))
            {
                rtnObject.Code = ApiReturnCode.InputError;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeInputError;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }
            
            var comboDealMain = PponDealApiManager.ApiComboDealMainGetByMainDealGuid(bGuid);

            if (comboDealMain != null)
            {
                foreach (var deal in comboDealMain.ComboDeals)
                {
                    if (string.IsNullOrEmpty(deal.DiscountString))
                    {
                        deal.DiscountString = "特選";
                    }
                    else
                    {
                        deal.DiscountString += "折";
                    }
                }
                rtnObject.Data = comboDealMain;
            }

            this.Context.Response.Write(jsonS.Serialize(rtnObject));
            //使用紀錄
            ApiUserManager.ApiUsedLogSet(apiUser, "PponDealService/ComboDealMainGetByMainDealId", jsonS.Serialize(new { Bid = bid, UserId = userId }));
        }
        
        /// <summary>
        /// 取得付款時需要的檔次資料
        /// </summary>
        /// <param name="bid">檔次的businessHourGuid</param>
        /// <param name="userId"></param>
        [WebMethod]
        public void DealDataForCheckoutGetByBid(string bid, string userId)
        {
            var jsonS = new JsonSerializer();
            ApiReturnObject rtnObject = new ApiReturnObject() { Code = ApiReturnCode.Success };

            //設定輸出格式為json格式
            this.Context.Response.ContentType = "application/json";
            this.Context.Response.Charset = Encoding.UTF8.WebName;

            //驗證使用者
            var apiUser = ApiUserManager.ApiUserGetByUserId(userId);
            if (apiUser == null)
            {
                rtnObject.Code = ApiReturnCode.ApiUserIdError;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeApiUserIdError;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }

            Guid bGuid;

            if (!Guid.TryParse(bid, out bGuid))
            {
                rtnObject.Code = ApiReturnCode.InputError;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeInputError;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }

            string userName = HttpContext.Current.User.Identity.Name;
            if (string.IsNullOrWhiteSpace(userName))
            {
                rtnObject.Code = ApiReturnCode.UserNoSignIn;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeUserNoSignIn;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }

            string message;
            var pponDeal = PponDealApiManager.ApiPponDealCheckoutDataGet(bGuid, userName, null, false, out message, ApiUserManager.IsOldAppUserId(userId));
            rtnObject.Data = pponDeal;
            if (!string.IsNullOrWhiteSpace(message))
            {
                rtnObject.Message = message;
            }

            this.Context.Response.Write(jsonS.Serialize(rtnObject));
            //使用紀錄
            ApiUserManager.ApiUsedLogSet(apiUser, "PponDealService/DealDataForCheckoutGetByBid", jsonS.Serialize(new { Bid = bid, UserId = userId }));
        }

        /// <summary>
        /// 依據檔次bid回傳檔次的推薦分享連結，有登入情況下回傳分享送紅利的連結。
        /// </summary>
        /// <param name="bid"></param>
        /// <param name="userId"></param>
        [WebMethod]
        public void GetDealPromoUrl(string bid, string userId)
        {
            var jsonS = new JsonSerializer();
            ApiReturnObject rtnObject = new ApiReturnObject() { Code = ApiReturnCode.Success };
            //設定輸出格式為json格式
            this.Context.Response.ContentType = "application/json";
            this.Context.Response.Charset = Encoding.UTF8.WebName;

            try
            {
                //驗證使用者
                var apiUser = ApiUserManager.ApiUserGetByUserId(userId);
                if (apiUser == null)
                {
                    rtnObject.Code = ApiReturnCode.ApiUserIdError;
                    rtnObject.Message = I18N.Phrase.ApiReturnCodeApiUserIdError;
                    this.Context.Response.Write(jsonS.Serialize(rtnObject));
                    throw new Exception("ApiReturnCode.ApiUserIdError, apiUserId=" + userId);
                }

                Guid bGuid;

                if (!Guid.TryParse(bid, out bGuid))
                {
                    rtnObject.Code = ApiReturnCode.InputError;
                    rtnObject.Message = I18N.Phrase.ApiReturnCodeInputError;
                    this.Context.Response.Write(jsonS.Serialize(rtnObject));
                    throw new Exception("ApiReturnCode.InputError, bid=" + bid);
                }

                //檢查使用者身分
                int? uId = null;
                string userName = HttpContext.Current.User.Identity.Name;
                if (!string.IsNullOrWhiteSpace(userName))
                {
                    //已登入，取得登入會員編號
                    uId = MemberFacade.GetUniqueId(userName);
                }



                string url;

                ApiDealPromoUrl promoUrl = new ApiDealPromoUrl();
                promoUrl.UrlType = PponDealApiManager.TryGetPponDealPromoUrl(bGuid, uId, out url);

                string origUrl = url;
                if (promoUrl.UrlType == PponDealPromoUrlType.BonusFeedback)
                {
                    //分享送紅利連結，轉換為短網址
                    url = WebUtility.RequestShortUrl(url);
                    if (uId != null)
                    {
                        PromotionFacade.SetDiscountReferrerUrl(origUrl, url, uId.Value, userName, bGuid, 0);
                    }
                }
                promoUrl.Url = url;
                //填入回傳直
                rtnObject.Data = promoUrl;

                //使用紀錄
                SystemFacade.SetApiLog("PponDealService.GetDealPromoUrl", userId,
                    jsonS.Serialize(new {Bid = bid}),
                    jsonS.Serialize(new {Result = true, UserId = uId, OrigUrl = origUrl, PromoUrl = promoUrl}),
                    Helper.GetClientIP());
            }
            catch (Exception ex)
            {
                //使用紀錄
                SystemFacade.SetApiLog("PponDealService.GetDealPromoUrl", userId,
                    jsonS.Serialize(new { Bid = bid }),
                    jsonS.Serialize(new { Result = false, userName = HttpContext.Current.User.Identity.Name }),
                    Helper.GetClientIP());
            }

            this.Context.Response.Write(jsonS.Serialize(rtnObject));
        }

        /// <summary>
        /// 取得發票捐贈愛心碼
        /// </summary>
        /// <param name="userId"></param>
        [WebMethod]
        public void GetInvoiceLoveCode(string userId)
        {
            var jsonS = new JsonSerializer();
            ApiReturnObject rtnObject = new ApiReturnObject() { Code = ApiReturnCode.Success };

            //設定輸出格式為json格式
            this.Context.Response.ContentType = "application/json";
            this.Context.Response.Charset = Encoding.UTF8.WebName;

            //驗證使用者
            var apiUser = ApiUserManager.ApiUserGetByUserId(userId);
            if (apiUser == null)
            {
                rtnObject.Code = ApiReturnCode.ApiUserIdError;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeApiUserIdError;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }
            //填入回傳直
            rtnObject.Data = LoveCodeManager.Instance.LoveCodes;

            this.Context.Response.Write(jsonS.Serialize(rtnObject));
            //使用紀錄
            ApiUserManager.ApiUsedLogSet(apiUser, "PponDealService/GetInvoiceLoveCode", jsonS.Serialize(new { UserId = userId }));
        }

        /// <summary>
        /// 查詢檔次店家列表，用於地圖顯示
        /// </summary>
        /// <param name="category">類型</param>
        /// <param name="latitude">緯度座標</param>
        /// <param name="longitude">經度座標</param>
        /// <param name="distance">查詢距離經緯度的距離</param>
        /// <param name="userId"></param>
        [WebMethod]
        public void GetStoreListByDistanceForMap(int channel, int area, int category, string latitude, string longitude, string distance, string userId)
        {
            var jsonS = new JsonSerializer();
            ApiReturnObject rtnObject = new ApiReturnObject() { Code = ApiReturnCode.Success };

            //設定輸出格式為json格式
            this.Context.Response.ContentType = "application/json";
            this.Context.Response.Charset = Encoding.UTF8.WebName;

            //驗證使用者
            var apiUser = ApiUserManager.ApiUserGetByUserId(userId);
            if (apiUser == null)
            {
                rtnObject.Code = ApiReturnCode.ApiUserIdError;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeApiUserIdError;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }

            int? categoryId = null;
            if (category > 0)
            {
                categoryId = category;
            }
            else
            {
                categoryId = 0;
            }

            int distanceParse = 0;
            if (!int.TryParse(distance, out distanceParse))
            {
                rtnObject.Code = ApiReturnCode.InputError;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeInputError;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }

            List<ApiStoreMap> stores = PponDealApiManager.ApiStoreMapGetListForShow(channel, area, categoryId, latitude, longitude, distanceParse);

            rtnObject.Data = stores;
            rtnObject.Message = I18N.Phrase.ApiResultCodeSuccess;
            this.Context.Response.Write(jsonS.Serialize(rtnObject));

            //使用紀錄
            ApiUserManager.ApiUsedLogSet(apiUser, "PponDealService/GetStoreListByDistanceForMap", jsonS.Serialize(new { Channel = channel, Area = area, Category = category, Latitude = latitude, Longitude = longitude, Distance = distance, UserId = userId }));
        }

        /// <summary>
        /// 後臺檔次設定使用，需要保留
        /// </summary>
        /// <param name="bid"></param>
        /// <returns></returns>
        [WebMethod]
        public string GetDraftForDescriptionByComboDealSpecs(Guid bid)
        {            
            //這邊要給後臺使用，不傾向從cache讀取資料
            var comboDeals = pp.GetViewComboDealAllByBid(bid);
            StringBuilder sb = new StringBuilder();
            List<string> titles = new List<string>();
            List<ViewPponDeal> deals = new List<ViewPponDeal>();
            List<List<PponSetupViewFreight>> dealFreightList = new List<List<PponSetupViewFreight>>();
            //子檔回傳空字串
            Guid mainBid = comboDeals.First(t => t.MainBusinessHourGuid == t.BusinessHourGuid).BusinessHourGuid;
            if (mainBid != bid)
            {
                return string.Empty;
            }
            foreach (var comboDeal in comboDeals.Where(t => t.MainBusinessHourGuid != t.BusinessHourGuid))
            {
                titles.Add(comboDeal.Title);
                deals.Add(pp.ViewPponDealGetByBusinessHourGuid(comboDeal.BusinessHourGuid));

                var freightCollection = pp.CouponFreightGetList(comboDeal.BusinessHourGuid);
                List<PponSetupViewFreight> freightIncomes = freightCollection
                    .Where(x => x.FreightType == (int)CouponFreightType.Income)
                    .OrderBy(x => x.StartAmount)
                    .Select(freight => new PponSetupViewFreight() { FreightAmount = freight.FreightAmount, StartAmount = freight.StartAmount }).ToList();
                dealFreightList.Add(freightIncomes);
            }

            SmartGetDealTitlePrefix titlePrefix = SmartGetDealTitlePrefix.Parse(titles);
            
            if (titlePrefix == null || titlePrefix.SubItems.Count == 0)
            {
                return string.Empty;
            }

            sb.AppendLine("<div style=\"font-size:18px;font-weight:bold\">");
            sb.AppendLine(titlePrefix.ToStrongPrefix());
            sb.AppendLine("</div>");
            sb.AppendLine("<br />");
            sb.AppendLine("<br />");
            sb.AppendLine("<div style=\"font-size:18px;color:#696969;\">");
            sb.AppendFormat("規格：______/{0}", titlePrefix.GetUnitTitle());
            sb.AppendLine("</div>");
            sb.AppendLine("<br />");
            sb.AppendLine("<br />");

            for (int i = 0; i < titlePrefix.SubItems.Count; i++)
            {
                string unitTitle = titlePrefix.SubItems[i].UnitTitle;
                var deal = deals[i];                
                sb.AppendLine("<div id=\"cke_pastebin\"><span style=\"color:#696969;\"><span style=\"font-size:18px;\">");
                sb.Append(titlePrefix.SubItems[i].SubTitle);
                sb.AppendFormat(" NT${0} <strike>原價${1}</strike>", (int)deal.ItemPrice, (int)deal.ItemOrigPrice);
                if (deal.DeliveryType == (int)DeliveryType.ToHouse)
                {
                    var freights = dealFreightList[i];
                    if (freights.Count > 0)
                    {
                        sb.AppendFormat(" 運費${0}", (int)freights[0].FreightAmount);
                        if (freights.Last().FreightAmount == 0)
                        {
                            var amountForFree = freights.Last().StartAmount;
                            int qtyForFree = (int)Math.Ceiling(amountForFree / deal.ItemPrice);
                            sb.AppendFormat("，{0}{1}免運", qtyForFree, unitTitle);
                        }
                    }
                    else
                    {
                        if (deal.ComboPackCount.GetValueOrDefault() > 1)
                        {
                            sb.AppendFormat(" 含運 平均${0}/{1}", (int)Math.Ceiling(deal.ItemPrice / deal.ComboPackCount.Value), unitTitle);
                        }
                        else
                        {
                            if (titlePrefix.SubItems[i].UnitNumber != 0)
                            {
                                sb.AppendFormat(" 含運 平均${0}/{1}", (int)Math.Ceiling(deal.ItemPrice / titlePrefix.SubItems[i].UnitNumber), unitTitle);
                            }
                        }
                    }
                }
                sb.AppendLine("</span></span></div>");
            }



            return sb.ToString();
        }
    }

    public class DealSpecUnit
    {
        public int Number;
        public string Title;
    }
}

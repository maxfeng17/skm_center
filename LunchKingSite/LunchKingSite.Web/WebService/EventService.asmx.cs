﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Services;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Component.API;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.BizLogic.Model;

namespace LunchKingSite.Web.WebService
{
    /// <summary>
    /// Summary description for EventService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class EventService : BaseService
    {
        public static IMemberProvider mp;
        public static ISysConfProvider config;

        private static string baseVerion = "5.9.0"; //App 大改版起始版號

        static EventService()
        {
            mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
            config = ProviderFactory.Instance().GetConfig();
        }

        [WebMethod]
        public void GetPromoEventList(string userId)
        {
            const string methodName = "GetPromoEventList";
            //設定輸出格式為json格式
            ContextBaseSetting();

            var jsonS = new JsonSerializer();

            List<ApiPromoEvent> eventList = new List<ApiPromoEvent>();

            ApiReturnObject rtnObject;
            //驗證使用者
            if (!CheckApiUser(userId, out rtnObject))
            {
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
                return;
            }

            var tmpData = ApiPromoEventManager.ApiPromoEventGetList();

            foreach (var Data in tmpData)
            {
                DateTime tmpEndDate;
                if (ApiSystemManager.TryDateTimeStringToDateTime(Data.EndDate, out tmpEndDate))
                {
                    if (DateTime.Now < tmpEndDate)
                    {
                        eventList.Add(Data);
                    }
                }
            }

            rtnObject.Data = eventList;

            this.Context.Response.Write(jsonS.Serialize(rtnObject));

            SetApiLog(GetMethodName(methodName), userId, null, new ApiReturnObject {Code = rtnObject.Code, Message = rtnObject.Message});
        }

        [WebMethod]
        public void GetPromoEventListByChannel(string userId, int channelId, string DeviceId)
        {
            const string methodName = "GetPromoEventListByChannel";
            //設定輸出格式為json格式
            ContextBaseSetting();

            var jsonS = new JsonSerializer();

            ApiReturnObject rtnObject;
            //驗證使用者
            if (!CheckApiUser(userId, out rtnObject))
            {
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
                return;
            }

            var actionEventList = mp.ActionEventInfoListGet().Where(x=>x.ActionStartTime<DateTime.Now&& DateTime.Now<=x.ActionEndTime).ToList();

            if (actionEventList.Count > 0)
            {
                var famiAction = actionEventList.FirstOrDefault(x => x.ActionName.Equals("2015FamilyMartBeaconEvent", StringComparison.OrdinalIgnoreCase));

                if (famiAction != null)
                {

            //rtnObject.Data = new[] { 
                    //    new { PromoBannerImage = @"https://www.17life.com/images/famibeacon/Family_590x80.gif", 
            //         PokeballList = new[] { 
            //            new { Type = "2015FamilyMartBeaconEvent", Url = @"https://www.17life.com/images/famibeacon/FamiBeacon.html" } } } 
            //};//ApiPromoEventManager.ApiPromoEventGetList();
                    if (config.FamiChannelBannerEnabled)
                    {
                        rtnObject.Data = new[] { new { PromoBannerImage = @"https://www.17life.com/images/famibeacon/Family_590x80.gif", 
                                                       PokeballList = new[] { new { Type = "FullscreenWebView", 
                                                                                    Url = @"http://www.17life.com/event/1111fami" 
                                                                                  } 
                                                                            } 
                                                     } 
                                               };//ApiPromoEventManager.ApiPromoEventGetList();

                    }
                    else
                    {
                    rtnObject.Data = new List<object>();//ApiPromoEventManager.ApiPromoEventGetList();
                }

                    
                }
            }

            this.Context.Response.Write(jsonS.Serialize(rtnObject));

            //SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
        }

        /// <summary>
        /// 依據傳入的活動編號，回傳此活動的基本資料與參加此活動的團購及優惠券
        /// </summary>
        /// <param name="eventId"></param>
        /// <param name="userId"></param>
        [WebMethod]
        public void GetEventPromoItemList(int eventId, string userId)
        {
            const string methodName = "GetEventPromoItemList";
            //設定輸出格式為json格式
            ContextBaseSetting();

            var jsonS = new JsonSerializer();

            ApiReturnObject rtnObject;
            //驗證使用者
            if (!CheckApiUser(userId, out rtnObject))
            {
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
                return;
            }

            rtnObject.Data = ApiPromoEventManager.ApiPromoEventDataGet(eventId);

            this.Context.Response.Write(jsonS.Serialize(rtnObject));

            SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
        }

        /// <summary>
        /// 查詢地圖模式時商品主題活動要顯示的資訊
        /// </summary>
        /// <param name="eventId">活動編號</param>
        /// <param name="longitude">經度，無法取得時回傳空值</param>
        /// <param name="latitude">緯度，無法取得時回傳空值</param>
        /// <param name="userId"></param>
        /// <returns></returns>
        [WebMethod]
        public void GetEventPromoItemListForMap(int eventId, string longitude, string latitude, string userId)
        {
            const string methodName = "GetEventPromoItemListForMap";
            //設定輸出格式為json格式
            ContextBaseSetting();

            var jsonS = new JsonSerializer();

            ApiReturnObject rtnObject;
            //驗證使用者
            if (!CheckApiUser(userId, out rtnObject))
            {
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
                return;
            }
            //處理傳入的座標
            ApiCoordinates coordinates;
            ApiCoordinates.TryParse(longitude, latitude, out coordinates);

            rtnObject.Data = ApiPromoEventManager.ApiPromoEventMapDataGet(eventId, coordinates);

            this.Context.Response.Write(jsonS.Serialize(rtnObject));

            SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
        }


        /// <summary>
        /// 依據活動編號回傳商品主題活動要顯示的團購檔次資訊
        /// </summary>
        /// <param name="eventId"></param>
        /// <param name="userId"></param>
        [WebMethod]
        public void GetEventPromoDealList(int eventId, string userId)
        {
            const string methodName = "GetEventPromoDealList";
            //設定輸出格式為json格式
            ContextBaseSetting();

            var jsonS = new JsonSerializer();

            ApiReturnObject rtnObject;
            //驗證使用者
            if (!CheckApiUser(userId, out rtnObject))
            {
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
                return;
            }

            string appVersion = HttpContext.Current.Request.QueryString["appVersion"];
            bool isNewPrototype = ApiSystemManager.IsNewAppVersion(appVersion, baseVerion, baseVerion, userId); 
            
            List<PponDealSynopsis> tempDealList = ApiPromoEventManager.PponDealSynopsisGetListByEventId(eventId);
            rtnObject.Data = ApiPromoEventManager.PponDealSynopsisGetListByEventId(eventId);

            if (isNewPrototype == true)
            {
                List<PponDealSynopsisBasic> dealList = new List<PponDealSynopsisBasic>();
                PponDealSynopsisBasic pponDealSynopsisBasic = new PponDealSynopsisBasic();

                foreach (var dealtmp in tempDealList)
                {
                    pponDealSynopsisBasic = PponDealApiManager.PponDealSynopsisToBasic(dealtmp);

                    dealList.Add(pponDealSynopsisBasic);
                }

                rtnObject.Data = dealList;
            }

            this.Context.Response.Write(jsonS.Serialize(rtnObject));

            SetApiLog(GetMethodName(methodName), userId, new {eventId, userId},
                new ApiReturnObject {Code = rtnObject.Code, Message = rtnObject.Message});
        }

        /// <summary>
        /// 依據活動編號回傳商品主題活動要顯示的優惠券檔次資訊
        /// </summary>
        /// <param name="eventId"></param>
        /// <param name="userId"></param>
        [WebMethod]
        public void GetEventPromoVourcherList(int eventId, string userId)
        {
            const string methodName = "GetEventPromoVourcherList";
            //設定輸出格式為json格式
            ContextBaseSetting();

            var jsonS = new JsonSerializer();

            ApiReturnObject rtnObject;
            //驗證使用者
            if (!CheckApiUser(userId, out rtnObject))
            {
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
                return;
            }

            rtnObject.Data = ApiPromoEventManager.ApiVourcherStoreGetListByEventId(eventId);

            this.Context.Response.Write(jsonS.Serialize(rtnObject));

            SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
        }

        [WebMethod]
        public void GetSkmTaishinCreditCardRules(string userId)
        {
            const string methodName = "GetSkmCreditCardValidationRules";
            //設定輸出格式為json格式
            ContextBaseSetting();

            var jsonS = new JsonSerializer();

            ApiReturnObject rtnObject;
            //驗證使用者
            if (!CheckApiUser(userId, out rtnObject))
            {
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
                return;
            }

            dynamic resultList = new List<ExpandoObject>();

            CreditCardPremiumCollection taishinCardRules = CreditCardPremiumManager.GetTaishinCreditCardsNumberRules();

            foreach (var taishinCardRule in taishinCardRules)
            {
                dynamic result = new ExpandoObject();
                result.CardNumber = taishinCardRule.CardNum;
                resultList.Add(result);
            }

            rtnObject.Data = resultList;

            this.Context.Response.Write(jsonS.Serialize(rtnObject));

            SetApiLog(GetMethodName(methodName), userId, null, rtnObject);


        }

        [WebMethod]
        public void GetSkmInvoiceVerification(string invoiceNumber, string userId)
        {
            const string methodName = "GetSkmInvoiceVerification";
            //設定輸出格式為json格式
            ContextBaseSetting();

            var jsonS = new JsonSerializer();

            ApiReturnObject rtnObject;
            //驗證使用者
            if (!CheckApiUser(userId, out rtnObject))
            {
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
                return;
            }

            //檢查會員是否已登入
            string userName;
            if (!CheckMemberSingin(out userName, out rtnObject))
            {
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
                return;
            }

            dynamic result = new ExpandoObject();
            string errorMessage = string.Empty;

            Regex regInvoice = new Regex(@"^[a-zA-Z][a-zA-Z]\d{8}$", RegexOptions.Compiled);
            if (regInvoice.IsMatch(invoiceNumber))
            {

                var skmInvoices =
                    PromotionFacade.GetSkmEventPromoInvoiceContentCollection()
                                   .Where(x => x.InvoiceNumber.ToLower() == invoiceNumber.ToLower());
                errorMessage = skmInvoices.Any() ? "抱歉，此發票號碼已登錄過" : "";
            }
            else
            {
                errorMessage = "抱歉您的發票號碼有誤，請重新輸入";
            }

            result.InvoiceVerification = errorMessage == string.Empty ? "Success" : "Failure";
            result.ErrorMessage = errorMessage;

            rtnObject.Code = ApiReturnCode.Success;
            rtnObject.Data = result;


            //轉JSON
            this.Context.Response.Write(jsonS.Serialize(rtnObject));

            SetApiLog(GetMethodName(methodName), userId, null, rtnObject);


        }

        [WebMethod]
        public void SetSkmEventPromoInvoiceContent(string cardNumber, string storeName, string invoiceNumber, string contactUserName, string contactAddress, string contactMobile, string userId)
        {
            const string methodName = "SetSkmEventPromoInvoiceContent";
            //設定輸出格式為json格式
            ContextBaseSetting();

            var jsonS = new JsonSerializer();

            ApiReturnObject rtnObject;
            //驗證使用者
            if (!CheckApiUser(userId, out rtnObject))
            {
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
                return;
            }

            //檢查會員是否已登入
            string userName;
            if (!CheckMemberSingin(out userName, out rtnObject))
            {
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
                return;
            }

            dynamic result = new ExpandoObject();
            string errorMessage = string.Empty;

            #region 檢查台新卡號

            Regex regCreditCardNumber = new Regex(@"^\d{8}$", RegexOptions.Compiled);
            if (regCreditCardNumber.IsMatch(cardNumber))
            {
                CreditCardPremiumCollection _taishinCardRules = CreditCardPremiumManager.GetTaishinCreditCardsNumberRules();

                var item = _taishinCardRules.Where(t => cardNumber.Length >= t.CardNum.Length &&
                          t.CardNum == cardNumber.Substring(0, t.CardNum.Length)).FirstOrDefault();

                errorMessage = (item == null) ? "抱歉，您所使用的非台新銀行信用卡" : "";
            }
            else
            {
                errorMessage = "抱歉您的卡號有誤，請重新輸入";
            }

            if (errorMessage != string.Empty)
            {
                result.InsertLottoryInvoice = "Failure";
                result.ErrorMessage = errorMessage;

                rtnObject.Code = ApiReturnCode.Success;
                rtnObject.Data = result;

                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
                return;
            }


            #endregion 檢查台新卡號

            #region 檢查發票號碼

            Regex regInvoice = new Regex(@"^[a-zA-Z][a-zA-Z]\d{8}$", RegexOptions.Compiled);
            if (regInvoice.IsMatch(invoiceNumber))
            {

                var skmInvoices =
                    PromotionFacade.GetSkmEventPromoInvoiceContentCollection()
                                   .Where(x => x.InvoiceNumber.ToLower() == invoiceNumber.ToLower());
                errorMessage = skmInvoices.Any() ? "抱歉，此發票號碼已登錄過" : "";
            }
            else
            {
                errorMessage = "抱歉您的發票號碼有誤，請重新輸入";
            }

            if (errorMessage != string.Empty)
            {
                result.InsertLottoryInvoice = "Failure";
                result.ErrorMessage = errorMessage;

                rtnObject.Code = ApiReturnCode.Success;
                rtnObject.Data = result;

                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
                return;
            }

            #endregion 檢查發票號碼

            #region  insert SkmEventPromoInvoiceContent

            PromotionFacade.SetSkmEventPromoInvoiceContent(cardNumber, storeName, invoiceNumber, contactUserName, contactAddress, contactMobile, userName);

            #endregion insert SkmEventPromoInvoiceContent


            result.InsertLottoryInvoice = "Success";
            result.ErrorMessage = errorMessage;

            rtnObject.Code = ApiReturnCode.Success;
            rtnObject.Data = result;


            //轉JSON
            this.Context.Response.Write(jsonS.Serialize(rtnObject));

            SetApiLog(GetMethodName(methodName), userId, null, rtnObject);

        }
    }
}

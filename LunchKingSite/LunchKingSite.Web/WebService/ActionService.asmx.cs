﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Xml;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Component.API;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.BizLogic.Model.EInvoice;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.Core.Enumeration;

namespace LunchKingSite.Web.WebService
{
    /// <summary>
    ///ActionService 的摘要描述
    /// </summary>
    //[WebService(Namespace = "http://tempuri.org/")]
    [WebService(Namespace = "http://www.17life.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class ActionService : BaseService
    {
        public static ISysConfProvider config ;
        public static ILocationProvider lp  ;
        public static IMemberProvider mp ;

        static ActionService()
        {
            config = ProviderFactory.Instance().GetConfig();
            lp = ProviderFactory.Instance().GetProvider<ILocationProvider>();
            mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        }


        [WebMethod]
        public void DevicePromoFilter(string userId, string deviceId, string trigger, string manufacturer, string model, string os, string osVer, string appVer, double longitude, double latitude)
        {
            CookieManager.SetAppOsVersion(os, osVer);

            //設定輸出格式為json格式
            ContextBaseSetting();

            ////檢查會員Id是否存在
            var memberName = HttpContext.Current.User.Identity.Name;
            int memberUserId = ApiPCPManager.GetMemberUserId(memberName);
            if (string.IsNullOrEmpty(memberName))
            {
                memberUserId = 0;
            }

            DeviceIdentyfierInfo device;

            //確認裝置資訊
            var deviceCols = lp.DeviceIdentyfierInfoListGet(deviceId);
            DateTime lastVisitTime;
            if (deviceCols.Any())
            {
                device = deviceCols.ToList().OrderByDescending(x => x.Id).First();
                lastVisitTime = (device.ModifyTime.HasValue) ? device.ModifyTime.Value : device.CreateTime;
                device.ModifyTime = DateTime.Now;
            }
            else
            {
                device = new DeviceIdentyfierInfo();
                device.IdentifierCode = deviceId;
                device.CreateTime = DateTime.Now;
                lastVisitTime = device.CreateTime;
            }
            device.ActionTrigger = trigger;
            device.Manufacturer = manufacturer;
            device.Model = model;
            device.Os = os;
            device.OsVer = osVer;
            device.AppVer = appVer;
            device.Longitude = longitude;
            device.Latitude = latitude;

            if (memberUserId > 0)
            {
                device.MemberUniqueId = memberUserId;

                //有開啟自動領取機制的話，領取[未領取]或[未領足]的折價券
                if (config.DiscountUserCollectType == DiscountUserCollectType.Auto)
                {
                    OrderFacade.SendUncollectedDiscountList(memberUserId, memberName);
                }
                
            }

            if(!string.Equals(trigger,String.Empty))
            { 
                lp.DeviceIdentyfierInfoSet(device);
            }

            if (config.EnableSKMNewBeacon)
            {
                var jsonSerialzer = new JsonSerializer();
                var result = new
                {
                    Code = 0,
                    Data = new
                    {
                        PokeballList = new List<object>()
                            {
                                new
                                {
                                    Type="BeaconListener",
                                    UUIDS=BeaconFacade.GetBeaconTriggerAppUuidList((int)BeaconTriggerAppType.Life17Beacon)
                                }
                            }
                    },
                    Message = ""
                };
                this.Context.Response.Write(jsonSerialzer.Serialize(result));
            }
            else
            {
                var actionEventList = mp.ActionEventInfoListGet().Where(x => !x.ActionName.Equals("SkmBeaconEvent", StringComparison.OrdinalIgnoreCase) && x.ActionStartTime < DateTime.Now && DateTime.Now <= x.ActionEndTime).ToList();
                if (actionEventList.Count > 0)
                {
                    if (trigger.Equals("onAppLaunch", StringComparison.OrdinalIgnoreCase))
                    {
                        var pokeballList = @"";
                        foreach (var actionEvent in actionEventList)
                        {
                            pokeballList += @"{""Type"":""BeaconListener"",""UUIDS"":[""" + actionEvent.SellerUuid + @""",""" + actionEvent.ElectricUuid + @"""]},";


                            if (config.BeaconEventAdEnabled && ((!device.ModifyTime.HasValue) || lastVisitTime.Date < DateTime.Now.Date)) //每日首次登入跳出訊息
                            {
                                //pokeballList += @"{""Type"":""" + actionEvent.ActionName + @"""},";
                                pokeballList += @"{""Type"":""FullscreenWebView"",""Url"":""https://www.17life.com/images/famibeacon/FamiBeacon.html""},";
                            }
                        }

                        if (pokeballList[pokeballList.Length - 1].ToString() == ",")
                        {
                            pokeballList = pokeballList.Remove(pokeballList.Length - 1);
                        }

                        this.Context.Response.Write(
                                @"{""Code"":0,""Data"":{""PokeballList"":[" + pokeballList + @"]},""Message"":""""}"
                                );
                    }
                    else
                    {
                        var action = actionEventList.FirstOrDefault(x => x.ActionName.Equals(trigger, StringComparison.OrdinalIgnoreCase));
                        var pokeballList = @"";

                        if (action != null)
                        {
                            pokeballList = @"{""Type"":""BeaconListener"",""UUIDS"":[""" + action.SellerUuid + @""",""" + action.ElectricUuid + @"""]}";
                        }
                        else
                        {
                            pokeballList = @"{""Type"":""BeaconListener"",""UUIDS"":[]}";
                        }

                        this.Context.Response.Write(
                            @"{""Code"":0,""Data"":{""PokeballList"":[" + pokeballList + @"]},""Message"":""""}"
                            );
                    }
                }
                else
                {
                    this.Context.Response.Write(
                        @"{""Code"":0,""Data"":{""PokeballList"":[{""Type"":""BeaconListener"",""UUIDS"":[]}]},""Message"":""""}"
                        );
                }
            }
        }

        [WebMethod]
        public void BeaconEvent(string userId, string deviceId, int memberId, string uuId, int major, int minor)
        {
            BeaconTriggerLog("BeaconEvent", string.Format("userId={0}/deviceId={1}/memberId={2}/uuId={3}/major={4}/minor={5}: ",userId,deviceId,memberId,uuId,major,minor));
            //設定輸出格式為json格式
            ContextBaseSetting();

            var jsonSerialzer = new JsonSerializer();
            var rtnObject = new ApiReturnObject();
            var isVaildSend = true;
           
            #region checkApiUser

            //驗證使用者
            if (!CheckApiUser(userId, out rtnObject))
            {
                this.Context.Response.Write(jsonSerialzer.Serialize(rtnObject));
                return;
            }

            #endregion checkApiUser

            #region 會員是否已登入

            ////檢查會員Id是否存在
            var memberName = HttpContext.Current.User.Identity.Name;
            int memberUserId = ApiPCPManager.GetMemberUserId(memberName);
            if (string.IsNullOrEmpty(memberName))
            {
                memberUserId = 0;
            }

            #endregion 會員是否已登入

            if (config.EnableSKMNewBeacon)
            {
                BeaconManager.IBeaconBase factory = new BeaconManager.BeaconFatory().CreateFatoryBy17Life(
                  new LunchKingSite.Core.ModelCustom.BeaconTriggerModel { major = major, minor = minor, uuId = uuId },
                   memberUserId,
                   deviceId);
                
                rtnObject.Message = string.IsNullOrEmpty(factory.ErrorMessage) ? rtnObject.Message : factory.ErrorMessage;
                rtnObject.Data = new { pokeballList = factory.PokeballList };
                rtnObject.Code = ApiReturnCode.Success;
                
                this.Context.Response.Write(jsonSerialzer.Serialize(rtnObject));
                return;
            }
            else
            {
                var actionEventList = mp.ActionEventInfoListGet().Where(x => x.ActionName.Equals("TaishinBeaconEvent", StringComparison.OrdinalIgnoreCase) && x.ActionStartTime < DateTime.Now && DateTime.Now <= x.ActionEndTime).ToList();

                if (actionEventList.Count > 0)
                {
                    var taishinAction = actionEventList.FirstOrDefault(x => x.ActionName.Equals("TaishinBeaconEvent", StringComparison.OrdinalIgnoreCase));

                    if (taishinAction != null && taishinAction.ElectricUuid.HasValue && taishinAction.SellerUuid.HasValue)
                    {
                        var taiShinDevices = mp.ActionEventDeviceInfoListGet(taishinAction.Id);
                        if (string.Equals(uuId, taishinAction.ElectricUuid.Value.ToString(), StringComparison.OrdinalIgnoreCase))
                        {
                            #region 電量檢查UUID

                            if (major > 0 && minor > 0 && taiShinDevices.Any(x => x.Major.Equals(major) && x.Minor.Equals(minor)))
                            {
                                var beaconBatteryCapacity = ApiActionFilterManager.DecodeBatteryCapacityOfBeacon(minor);
                                var beaconMacAddress = ApiActionFilterManager.DecodeMacAddressOfBeacon(major, minor);

                                var taiShinDevice = taiShinDevices.FirstOrDefault(x => x.ElectricMacAddress.Substring(10, 7).Equals(beaconMacAddress, StringComparison.OrdinalIgnoreCase));
                                if (taiShinDevice != null && taiShinDevice.IsLoaded)
                                {
                                    taiShinDevice.ElectricPower = beaconBatteryCapacity;
                                    taiShinDevice.LastUpdateTime = DateTime.Now;
                                    mp.ActionEventDeviceInfoSet(taiShinDevice);

                                    #region Beacon_Log 

                                    BeaconTriggerLog("TaiShinBeaconElectricLog", string.Format("deviceId={0},uuId={1}, major={2}, minor={3},electricMacAddress={4},electricPower={5}", deviceId, uuId, major, minor, taiShinDevice.ElectricMacAddress, taiShinDevice.ElectricPower));

                                    #endregion Beacon_Log
                                }
                            }

                            #endregion 電量檢查UUID
                        }
                        else if (string.Equals(uuId, taishinAction.SellerUuid.Value.ToString(), StringComparison.OrdinalIgnoreCase))
                        {
                            DeviceIdentyfierInfo device;
                            ActionEventPushMessage pushActionEventPushMsg;

                            #region Beacon訊息推送UUID

                            if (major > 0 && minor > 0 && taiShinDevices.Any(x => x.Major.Equals(major) && x.Minor.Equals(minor)))
                            {
                                #region  確認裝置資訊

                                var deviceCols = lp.DeviceIdentyfierInfoListGet(deviceId);
                                if (deviceCols.Any())
                                {
                                    device = deviceCols.ToList().OrderByDescending(x => x.Id).First();
                                    device.ModifyTime = DateTime.Now;
                                }
                                else
                                {
                                    device = new DeviceIdentyfierInfo();
                                    device.IdentifierCode = deviceId;
                                    device.CreateTime = DateTime.Now;
                                    device.ActionTrigger = "BeaconEvent";
                                }

                                if (memberUserId > 0)
                                {
                                    device.MemberUniqueId = memberUserId;
                                }

                                lp.DeviceIdentyfierInfoSet(device);

                                #endregion  確認裝置資訊

                                ActionEventPushMessageCollection actionEventPushMessageCollection = mp.ActionEventPushMessageOfNowListGet(DateTime.Now);

                                List<ActionEventPushMessage> actionEventPushMessageList = actionEventPushMessageCollection
                                    .Where(x => x.EventType == (int)ActionEventType.RemotePushCustomUrl && x.Status)
                                    .OrderBy(z => z.SendStartTime)
                                    .ThenByDescending(z => z.Id)
                                    .ToList();

                                if (actionEventPushMessageList.Any())
                                {
                                    DevicePushRecordCollection devicePushRecords = lp.DevicePushRecordListGet(device.Id, memberUserId);

                                    //每日推播有效時間(起)  => config.BeaconEventPushValidStartTime
                                    //每日推播有效時間(迄)  => config.BeaconEventPushValidEndTime
                                    //每次推播間隔  => config.BeaconEventPushIntervalMinute
                                    //每日推播上限  => config.BeaconPushDailyUpperLimit

                                    if (config.BeaconEventPushValidStartTime.TimeOfDay > DateTime.Now.TimeOfDay || DateTime.Now.TimeOfDay > config.BeaconEventPushValidEndTime.TimeOfDay)
                                    {
                                        //每日可推播時間
                                        isVaildSend = false;
                                    }

                                    if (devicePushRecords.Any())
                                    {
                                        #region 有訊息紀錄

                                        var recordList = devicePushRecords.ToList().OrderByDescending(x => x.CreateTime);

                                        isVaildSend = actionEventPushMessageList.Any(x => !devicePushRecords.Select(z => z.ActionId).Contains(x.Id));


                                        if (new TimeSpan(DateTime.Now.Ticks - recordList.First().CreateTime.Ticks).TotalMinutes < config.BeaconEventPushIntervalMinute)
                                        {
                                            //每次推播間隔
                                            isVaildSend = false;
                                        }
                                        else if (recordList.Count(x => x.CreateTime.Date.Equals(DateTime.Now.Date)) > config.BeaconPushDailyUpperLimit)
                                        {
                                            //每日推播上限
                                            isVaildSend = false;
                                        }

                                        #endregion 有訊息紀錄
                                    }
                                    else
                                    {
                                        if (config.BeaconPushDailyUpperLimit <= 0)
                                        {
                                            //每日推播上限
                                            isVaildSend = false;
                                        }
                                    }

                                    if (isVaildSend)
                                    {
                                        var vaildActionEventPushId = actionEventPushMessageList.First(x => !devicePushRecords.Select(z => z.ActionId).Contains(x.Id)).Id;
                                        pushActionEventPushMsg = mp.ActionEventPushMessageGetByActionEventPushMessageId(vaildActionEventPushId);

                                        #region deviceRecordset

                                        DevicePushRecord deviceRecord = new DevicePushRecord();
                                        deviceRecord.ActionId = pushActionEventPushMsg.Id;
                                        deviceRecord.DeviceId = device.Id;
                                        deviceRecord.CreateTime = DateTime.Now;
                                        if (memberUserId > 0)
                                        {
                                            deviceRecord.MemberId = memberUserId;
                                            deviceRecord.IdentifierType = 2;
                                        }
                                        else
                                        {
                                            deviceRecord.IdentifierType = 1;
                                        }
                                        lp.DevicePushRecordSet(deviceRecord);

                                        #endregion deviceRecordset

                                        #region Beacon_Log 

                                        BeaconTriggerLog("TaiShinBeaconTriggerLog",
                                            string.Format(
                                                "deviceId={0},uuId={1}, major={2}, minor={3},pushActionEventPushMsgId={4},recordId={5}",
                                                deviceId, uuId, major, minor, pushActionEventPushMsg.Id, deviceRecord.Id));

                                        #endregion Beacon_Log 

                                        this.Context.Response.Write(
                                            @"{""Code"":0,""Data"":{""PokeballList"":[{""Type"":""LocalPush"",""Message"":""" +
                                            pushActionEventPushMsg.Subject +
                                            @""",""PokeballList"":[{""Type"":""FullscreenWebView"",""Url"":""" +
                                            pushActionEventPushMsg.CustomUrl.ToString() +
                                            @"""},{""Type"":""MarkMessageRead"",""MessageId"":" +
                                            deviceRecord.Id
                                            + @",""TriggerType"":1}]}"
                                            + @"]},""Message"":""""}"
                                            );

                                        return;
                                    }
                                    else
                                    {
                                        //觸發紀錄無回傳訊息

                                        #region Beacon_Log 

                                        BeaconTriggerLog("TaiShinBeaconTriggerLog",
                                            string.Format(
                                                "deviceId={0},uuId={1}, major={2}, minor={3},pushActionEventPushMsgId={4}",
                                                deviceId, uuId, major, minor, -1));

                                        #endregion Beacon_Log 
                                    }
                                }
                            }
                            #endregion Beacon訊息推送UUID
                        }
                    }
                }
            }
           

            rtnObject.Data = new { pokeballList = new List<object>() };
            rtnObject.Code = ApiReturnCode.Success;
            rtnObject.Message = string.Empty;
            
            //this.Context.Response.Write(@"{""Code"":0,""Data"":null,""Message"":""""}");
            this.Context.Response.Write(jsonSerialzer.Serialize(rtnObject));
        }

        private void BeaconTriggerLog(string logType, string logInfo)
        {
            try
            {
                PromoItem item = new PromoItem();
                item.Code = logType;
                item.UseTime = DateTime.Now;
                item.Memo = logInfo;
                ProviderFactory.Instance().GetProvider<IEventProvider>().PromoItemSet(item);
            }
            catch
            {
                // ignored
            }
        }
    }
}

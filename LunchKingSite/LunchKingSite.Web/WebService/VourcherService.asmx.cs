﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using LunchKingSite.BizLogic.Component.API;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.Core;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Enumeration;
using LunchKingSite.DataOrm;

namespace LunchKingSite.Web.WebService
{
    /// <summary>
    /// Summary description for VourcherService
    /// </summary>
    [WebService(Namespace = "http://www.17life.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class VourcherService : BaseService//System.Web.Services.WebService
    {
        /// <summary>
        /// 取得優惠券相關的 搜尋選單，包含 區域、商家類型、排序條件
        /// </summary>
        /// <param name="cityId"></param>
        /// <param name="userId"></param>
        [WebMethod]
        public void GetVourcherQueryData(int cityId, string userId)
        {
            const string methodName = "GetVourcherQueryData";
            //設定輸出格式為json格式
            ContextBaseSetting();

            var jsonS = new JsonSerializer();

            ApiReturnObject rtnObject;
            //驗證使用者
            if (!CheckApiUser(userId, out rtnObject))
            {
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
                return;
            }

            var categories = ApiVourcherManager.ApiSellerSampleCategoryGetListByCityId(cityId);
            var regions = ApiVourcherManager.ApiVourcherRegionGetList();
            var sortTypes = ApiVourcherManager.ApiVourcherSortTypeGetList();

            rtnObject.Data = new { Categories = categories, Regions = regions, SortTypes = sortTypes };

            this.Context.Response.Write(jsonS.Serialize(rtnObject));

            SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
        }

        /// <summary>
        /// 取得城市區域清單
        /// </summary>
        /// <param name="userId">呼叫API的client端裝置或合作機關的帳號</param>
        [WebMethod]
        public void GetCitysJSon(string userId)
        {
            const string methodName = "GetCitysJSon";

            var jsonS = new JsonSerializer();
            //設定輸出格式為json格式
            ContextBaseSetting();

            ApiReturnObject rtnObject;
            //驗證使用者
            if (!CheckApiUser(userId, out rtnObject))
            {
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
                return;
            }


            var jsonCol = ApiVourcherManager.ApiVourcherRegionGetList();
                
            rtnObject.Data = jsonCol;

            this.Context.Response.Write(jsonS.Serialize(rtnObject));
            //紀錄呼叫的log
            SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
        }

        /// <summary>
        /// 回傳賣家類別(列舉 SellerSampleCategory)
        /// </summary>
        /// <param name="userId">呼叫API的client端裝置或合作機關的帳號</param>
        [WebMethod]
        public void GetSellerSampleCategory(string userId)
        {

            const string methodName = "GetSellerSampleCategory";
            //設定輸出格式為json格式
            ContextBaseSetting();

            var jsonS = new JsonSerializer();

            ApiReturnObject rtnObject;
            //驗證使用者
            if (!CheckApiUser(userId, out rtnObject))
            {
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
                return;
            }

            var jsonCol = ApiVourcherManager.ApiSellerSampleCategoryGetList();

            rtnObject.Data = jsonCol;
            //紀錄LOG
            this.Context.Response.Write(jsonS.Serialize(rtnObject));
            SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
        }

        /// <summary>
        /// 依據傳遞城市編號回傳賣家類別(列舉 SellerSampleCategory)
        /// </summary>
        /// <param name="cityId">城市</param>
        /// <param name="userId">呼叫API的client端裝置或合作機關的帳號</param>
        [WebMethod]
        public void GetSellerSampleCategoryByCity(int cityId, string userId)
        {
            const string methodName = "GetSellerSampleCategoryByCity";
            //設定輸出格式為json格式
            ContextBaseSetting();

            var jsonS = new JsonSerializer();

            ApiReturnObject rtnObject;
            //驗證使用者
            if (!CheckApiUser(userId, out rtnObject))
            {
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
                return;
            }

            var jsonCol = ApiVourcherManager.ApiSellerSampleCategoryGetListByCityId(cityId);

            rtnObject.Data = jsonCol;
            //紀錄LOG
            this.Context.Response.Write(jsonS.Serialize(rtnObject));
        }

        /// <summary>
        /// 取得排序條件的選項
        /// </summary>
        /// <param name="userId"></param>
        [WebMethod]
        public void GetVourcherSortTypeList(string userId)
        {
            const string methodName = "GetVourcherSortTypeList";
            //設定輸出格式為json格式
            ContextBaseSetting();

            var jsonS = new JsonSerializer();

            ApiReturnObject rtnObject;
            //驗證使用者
            if (!CheckApiUser(userId, out rtnObject))
            {
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
                return;
            }

            //轉成類似ListItem格式(Text,Value)
            var jsonCol = ApiVourcherManager.ApiVourcherSortTypeGetList();

            rtnObject.Data = jsonCol;

            this.Context.Response.Write(jsonS.Serialize(rtnObject));
            //紀錄LOG
            SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="pageStart"></param>
        /// <param name="pageLength"></param>
        /// <param name="regionId">城市編號</param>
        /// <param name="category">類型</param>
        /// <param name="orderByType">排序方式</param>
        /// <param name="latitude">緯度座標</param>
        /// <param name="longitude">經度座標</param>
        /// <param name="distance">查詢距離經緯度的距離</param>
        /// <param name="userId">呼叫API的client端裝置或合作機關的帳號</param>
        [WebMethod]
        public void GetVourcherStoreListByRegionIdAndCategoryWithOrderBy(string pageStart , string pageLength , string regionId
            , string category, string orderByType, string latitude, string longitude, string distance, string userId)
        {
            const string methodName = "GetVourcherStoreListByRegionIdAndCategoryWithOrderBy";
            //設定輸出格式為json格式
            ContextBaseSetting();

            var jsonS = new JsonSerializer();

            ApiReturnObject rtnObject;
            //驗證使用者
            if (!CheckApiUser(userId, out rtnObject))
            {
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
                return;
            }


            int pagestart = int.TryParse(pageStart, out pagestart) ? pagestart : -1;
            int pagelength = int.TryParse(pageLength, out pagelength) ? pagelength : 10;
            int cityId = int.TryParse(regionId, out cityId) ? cityId : 0;


            SellerSampleCategory? sellerCategory = null;
            if(category.Trim() != "-1")
            {
                SellerSampleCategory paresCategory;
                if(Enum.TryParse(category,out paresCategory ))
                {
                    sellerCategory = paresCategory;
                }
                else
                {
                    rtnObject.Code = ApiReturnCode.InputError;
                    rtnObject.Message = I18N.Phrase.ApiReturnCodeInputError;
                    this.Context.Response.Write(jsonS.Serialize(rtnObject));
                    SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
                    return;                
                }
            }
            VourcherSortType sortType;
            if(!Enum.TryParse(orderByType,out sortType))
            {
                rtnObject.Code = ApiReturnCode.InputError;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeInputError;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
                return;                
            }
            int distanceParse;
            int? distanceLength = null;
            if(!string.IsNullOrWhiteSpace(distance))
            {
                if(!int.TryParse(distance,out distanceParse))
                {
                    rtnObject.Code = ApiReturnCode.InputError;
                    rtnObject.Message = I18N.Phrase.ApiReturnCodeInputError;
                    this.Context.Response.Write(jsonS.Serialize(rtnObject));
                    SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
                    return;
                }
                distanceLength = distanceParse;
            }
            //將傳入的經緯度轉型，若失敗，設為null。
            ApiCoordinates coordinates;
            if (!ApiCoordinates.TryParse(longitude, latitude, out coordinates))
            {
                coordinates = null;
            }


            List<ApiVourcherStore> stores = ApiVourcherManager.ApiVourcherStoreGetList(pagestart, pagelength, cityId,
                                                                                       sellerCategory, sortType,
                                                                                       coordinates,
                                                                                       distanceLength);

            rtnObject.Data = stores;
            this.Context.Response.Write(jsonS.Serialize(rtnObject));

            //紀錄LOG
            Object parameter = new
                {
                    pageStart = pageStart,
                    pageLength = pageLength,
                    regionId = regionId,
                    category = category,
                    orderByType = orderByType,
                    latitude = latitude,
                    longitude = longitude,
                    distance = distance,
                    userId = userId
                };
            Object rtnValue = new { count = (stores == null ? 0 : stores.Count), message = "回傳資料筆數" };
            SetApiLog(GetMethodName(methodName), userId, parameter, rtnValue);
        }


        /// <summary>
        /// 查詢優惠店家列表，用於地圖顯示 作廢
        /// </summary>
        /// <param name="regionId">城市編號</param>
        /// <param name="category">類型</param>
        /// <param name="latitude">緯度座標</param>
        /// <param name="longitude">經度座標</param>
        /// <param name="distance">查詢距離經緯度的距離</param>
        /// <param name="userId">呼叫API的client端裝置或合作機關的帳號</param>
        [WebMethod]
        public void GetVourcherStoreListByRegionIdAndCategoryForMap(string regionId , string category
            , string latitude, string longitude, string distance, string userId)
        {

            const string methodName = "GetVourcherStoreListByRegionIdAndCategoryForMap";
            //設定輸出格式為json格式
            ContextBaseSetting();

            var jsonS = new JsonSerializer();

            ApiReturnObject rtnObject;
            //驗證使用者
            if (!CheckApiUser(userId, out rtnObject))
            {
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
                return;
            }

            int cityId = int.TryParse(regionId, out cityId) ? cityId : 0;


            SellerSampleCategory? sellerCategory = null;
            if (category.Trim() != "-1")
            {
                SellerSampleCategory paresCategory;
                if (Enum.TryParse(category, out paresCategory))
                {
                    sellerCategory = paresCategory;
                }
                else
                {
                    rtnObject.Code = ApiReturnCode.InputError;
                    rtnObject.Message = I18N.Phrase.ApiReturnCodeInputError;
                    this.Context.Response.Write(jsonS.Serialize(rtnObject));
                    SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
                    return;
                }
            }

            int distanceParse = 0;
            if (!int.TryParse(distance, out distanceParse))
            {
                rtnObject.Code = ApiReturnCode.InputError;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeInputError;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }
            List<ApiVourcherStore> stores = ApiVourcherManager.ApiVourcherStoreGetListForMapShow( cityId,
                                                                                       sellerCategory,
                                                                                       latitude, longitude,
                                                                                       distanceParse);

            rtnObject.Data = stores;
            this.Context.Response.Write(jsonS.Serialize(rtnObject));

            //紀錄LOG
            Object parameter = new
            {
                regionId = regionId,
                category = category,
                latitude = latitude,
                longitude = longitude,
                distance = distance,
                userId = userId
            };
            Object rtnValue = new { count = (stores==null?0:stores.Count), message = "回傳資料筆數" };
            SetApiLog(GetMethodName(methodName), userId, parameter, rtnValue);
        }

        /// <summary>
        /// 查詢優惠店家列表，用於地圖顯示
        /// </summary>
        /// <param name="category">類型</param>
        /// <param name="latitude">緯度座標</param>
        /// <param name="longitude">經度座標</param>
        /// <param name="distance">查詢距離經緯度的距離</param>
        /// <param name="userId">呼叫API的client端裝置或合作機關的帳號</param>
        [WebMethod]
        public void GetVourcherStoreListByCategoryForMap(string category
            , string latitude, string longitude, string distance, string userId)
        {
            var jsonS = new JsonSerializer();

            const string methodName = "GetVourcherStoreListByCategoryForMap";
            //設定輸出格式為json格式
            ContextBaseSetting();

            ApiReturnObject rtnObject;
            //驗證使用者
            if (!CheckApiUser(userId, out rtnObject))
            {
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
                return;
            }


            SellerSampleCategory? sellerCategory = null;
            if (category.Trim() != "-1")
            {
                SellerSampleCategory paresCategory;
                if (Enum.TryParse(category, out paresCategory))
                {
                    sellerCategory = paresCategory;
                }
                else
                {
                    rtnObject.Code = ApiReturnCode.InputError;
                    rtnObject.Message = I18N.Phrase.ApiReturnCodeInputError;
                    SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
                    return;
                }
            }

            int distanceParse = 0;
            if (!int.TryParse(distance, out distanceParse))
            {
                rtnObject.Code = ApiReturnCode.InputError;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeInputError;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }
            List<ApiVourcherStore> stores = ApiVourcherManager.ApiVourcherStoreGetListForMapShow(sellerCategory,
                                                                                       latitude, longitude,
                                                                                       distanceParse);

            rtnObject.Data = stores;
            this.Context.Response.Write(jsonS.Serialize(rtnObject));

            //紀錄LOG
            Object parameter = new
            {
                category = category,
                latitude = latitude,
                longitude = longitude,
                distance = distance,
                userId = userId
            };
            Object rtnValue = new { count = (stores == null ? 0 : stores.Count), message = "回傳資料筆數" };
            SetApiLog(GetMethodName(methodName), userId, parameter, rtnValue);
        }




        /// <summary>
        /// 依城市搜尋賣家及優惠券資訊列表 預定作廢
        /// </summary>
        /// <param name="pageStart"></param>
        /// <param name="pageLength"></param>
        /// <param name="regionId">城市區域id</param>
        /// <param name="userId">呼叫API的client端裝置或合作機關的帳號</param>
        [WebMethod]
        public void GetViewVourcherStoreFacadeCollectionByRegionId(string pageStart, string pageLength, string regionId, string userId)
        {
            const string methodName = "GetViewVourcherStoreFacadeCollectionByRegionId";
            //設定輸出格式為json格式
            ContextBaseSetting();

            //即將作廢，所以紀錄看看有誰呼叫他，不記錄細節
            SetApiLog(GetMethodName(methodName), userId, null , null );


            int pagestart = int.TryParse(pageStart, out pagestart) ? pagestart : -1;
            int pagelength = int.TryParse(pageLength, out pagelength) ? pagelength : 10;
            int id = int.TryParse(regionId, out id) ? id : 0;

            var jsonS = new JsonSerializer();

            ApiReturnObject rtnObject = new ApiReturnObject() { Code = ApiReturnCode.Success };
            if (userId == null)
            {
                rtnObject.Code = ApiReturnCode.ApiUserIdError;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeApiUserIdError;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }
            
            var jsonCol = VourcherFacade.ViewVourcherStoreFacadeCollectionGetByRegionId(pagestart, pagelength, id, string.Empty).GroupBy(x => new { x.SellerId, x.SellerName, x.SellerLogoimgPath })
                .Select(x => new
                {
                    SellerId = x.Key.SellerId,
                    SellerName = x.Key.SellerName,
                    SellerLogoimgPath = x.Key.SellerLogoimgPath,
                    AttentionCount = x.Sum(s => s.VourcherPageCount),
                    Distance = string.Empty,
                    VourcherContent = x.FirstOrDefault() != null ? x.FirstOrDefault().Contents : string.Empty,
                    StartDate = x.FirstOrDefault() != null ? x.FirstOrDefault().StartDate : DateTime.Now,
                    VourcherCount = x.GroupBy(y => y.VourcherEventId).Count(),
                }).OrderByDescending(g => g.StartDate);

            rtnObject.Data = jsonCol;

            this.Context.Response.Write(jsonS.Serialize(rtnObject));
        }

        /// <summary>
        /// 依經緯度搜尋賣家及優惠券資訊列表 預定作廢
        /// </summary>
        /// <param name="pageStart"></param>
        /// <param name="pageLength"></param>
        /// <param name="latitude">緯度座標</param>
        /// <param name="longitude">經度座標</param>
        /// <param name="distance">查詢距離經緯度的距離</param>
        /// <param name="userId">呼叫API的client端裝置或合作機關的帳號</param>
        [WebMethod]
        public void GetViewVourcherStoreFacadeCollectionByCoordinate(string pageStart, string pageLength, string latitude, string longitude, string distance, string userId)
        {
            const string methodName = "GetViewVourcherStoreFacadeCollectionByCoordinate";
            //設定輸出格式為json格式
            ContextBaseSetting();

            //即將作廢，所以紀錄看看有誰呼叫他，不記錄細節
            SetApiLog(GetMethodName(methodName), userId, null, null);

            int pagestart = int.TryParse(pageStart, out pagestart) ? pagestart : -1;
            int pagelength = int.TryParse(pageLength, out pagelength) ? pagelength : 10;
            int storeDistance = int.TryParse(distance, out storeDistance) ? storeDistance : 0;

            var jsonS = new JsonSerializer();

            ApiReturnObject rtnObject = new ApiReturnObject() { Code = ApiReturnCode.Success };
            if (userId == null)
            {
                rtnObject.Code = ApiReturnCode.ApiUserIdError;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeApiUserIdError;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }

            var jsonCol = VourcherFacade.ViewVourcherStoreFacadeCollectionGetByCoordinate(pagestart, pagelength, storeDistance, latitude, longitude, string.Empty).GroupBy(x => new { x.SellerId, x.SellerName, x.SellerLogoimgPath })
                .Select(x => new
                {
                    SellerId = x.Key.SellerId,
                    SellerName = x.Key.SellerName,
                    SellerLogoimgPath = x.Key.SellerLogoimgPath,
                    AttentionCount = x.Sum(s => s.PageCount),
                    Distance = string.Empty,
                    VourcherContent = x.FirstOrDefault() != null ? x.FirstOrDefault().Contents : string.Empty,
                    StartDate = x.FirstOrDefault() != null ? x.FirstOrDefault().StartDate : DateTime.Now,
                    VourcherCount = x.GroupBy(y => y.VourcherEventId).Count(),
                }).OrderByDescending(g => g.StartDate);

            rtnObject.Data = jsonCol;

            this.Context.Response.Write(jsonS.Serialize(rtnObject));
        }


        /// <summary>
        /// 回傳賣家平均消費(列舉 SellerConsumptionAvg)  預定作廢
        /// </summary>
        /// <param name="userId">呼叫API的client端裝置或合作機關的帳號</param>
        [WebMethod]
        public void GetSellerConsumptionAvg(string userId)
        {
            const string methodName = "GetSellerConsumptionAvg";
            //設定輸出格式為json格式
            ContextBaseSetting();

            //即將作廢，所以紀錄看看有誰呼叫他，不記錄細節
            SetApiLog(GetMethodName(methodName), userId, null, null);

            var jsonS = new JsonSerializer();

            ApiReturnObject rtnObject = new ApiReturnObject() { Code = ApiReturnCode.Success };
            if (userId == null)
            {
                rtnObject.Code = ApiReturnCode.ApiUserIdError;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeApiUserIdError;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }

            var jsonCol = VourcherFacade.GetSellerConsumptionAvgDictionary().Select(x => new { Text = x.Value, Value = x.Key });

            rtnObject.Data = jsonCol;

            this.Context.Response.Write(jsonS.Serialize(rtnObject));
        }

        /// <summary>
        /// 依賣家id搜尋優惠券內容
        /// </summary>
        /// <param name="pageStart">起頁(-1全選)</param>
        /// <param name="pageLength">每頁大小</param>
        /// <param name="sellerId">賣家id</param>
        /// <param name="userId">呼叫API的client端裝置或合作機關的帳號</param>
        [WebMethod]
        public void GetViewVourcherSellerBySellerId(string pageStart, string pageLength, string sellerId, string userId)
        {
            const string methodName = "GetViewVourcherSellerBySellerId";
            //設定輸出格式為json格式
            ContextBaseSetting();
            
            int pagestart = int.TryParse(pageStart, out pagestart) ? pagestart : -1;
            int pagelength = int.TryParse(pageLength, out pagelength) ? pagelength : 10;

            var jsonS = new JsonSerializer();

            ApiReturnObject rtnObject;
            //驗證使用者
            if (!CheckApiUser(userId, out rtnObject))
            {
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
                return;
            }

            var jsonCol = ApiVourcherManager.ApiVourcherEventSynopsesGetList(pagestart, pagelength, sellerId);
            rtnObject.Data = jsonCol;

            this.Context.Response.Write(jsonS.Serialize(rtnObject));


            //紀錄LOG
            Object parameter = new
                {
                    pageStart = pageStart,
                    pageLength = pageLength,
                    sellerId = sellerId,
                    userId = userId
                };
            Object rtnValue = new { count = (jsonCol == null ? 0 : jsonCol.Count), message = "回傳資料筆數" };
            SetApiLog(GetMethodName(methodName), userId, parameter, rtnValue);
        }

        /// <summary>
        /// 依優惠券id搜尋優惠券內容(查取優惠券內容時，關注數會加1)
        /// </summary>
        /// <param name="eventId">優惠券id</param>
        /// <param name="latitude"></param>
        /// <param name="longitude"></param>
        /// <param name="userId">呼叫API的client端裝置或合作機關的帳號</param>
        [WebMethod]
        public void GetViewVourcherSellerByEventId(string eventId, string latitude, string longitude, string userId)
        {
            const string methodName = "GetViewVourcherSellerByEventId";
            //設定輸出格式為json格式
            ContextBaseSetting();

            int eventid = int.TryParse(eventId, out eventid) ? eventid : 0;

            var jsonS = new JsonSerializer();

            ApiReturnObject rtnObject;
            //驗證使用者
            if (!CheckApiUser(userId, out rtnObject))
            {
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
                return;
            }

            //取得會員名稱
            int? uniqueId = null;
            string userName = HttpContext.Current.User.Identity.Name;
            if (!string.IsNullOrWhiteSpace(userName))
            {
                uniqueId = MemberFacade.GetUniqueId(userName);
            }

            var jsonData = ApiVourcherManager.ApiVourcherEventGet(eventid, latitude, longitude, uniqueId);

            if (userId == "AND2012250752")//針對andiord舊版app調整。
            {
                if (!string.IsNullOrWhiteSpace(jsonData.Restriction))
                {
                    jsonData.Instruction = jsonData.Instruction + "\n" + "不適用：" + jsonData.Restriction;
                }
                if (!string.IsNullOrWhiteSpace(jsonData.Others))
                {
                    jsonData.Instruction = jsonData.Instruction + "\n" + jsonData.Others;
                }
            }
            rtnObject.Data = jsonData;

            this.Context.Response.Write(jsonS.Serialize(rtnObject));

            //紀錄LOG
            Object parameter = new
                {
                    eventId = eventId,
                    latitude = latitude,
                    longitude = longitude,
                    userId = userId
                };
            SetApiLog(GetMethodName(methodName), userId, parameter, rtnObject);
        }

        /// <summary>
        /// 依優惠券id搜尋所有分店 預定作廢
        /// </summary>
        /// <param name="pageStart">起頁(-1全選)</param>
        /// <param name="pageLength">每頁大小</param>
        /// <param name="eventId">優惠券id</param>
        /// <param name="userId">呼叫API的client端裝置或合作機關的帳號</param>
        [WebMethod]
        public void GetViewVourcherStoreCollectionGetByEventId(string pageStart, string pageLength, string eventId, string userId)
        {
            const string methodName = "GetViewVourcherStoreCollectionGetByEventId";
            //設定輸出格式為json格式
            ContextBaseSetting();

            //即將作廢，所以紀錄看看有誰呼叫他，不記錄細節
            SetApiLog(GetMethodName(methodName), userId, null, null);

            int pagestart = int.TryParse(pageStart, out pagestart) ? pagestart : -1;
            int pagelength = int.TryParse(pageLength, out pagelength) ? pagelength : 10;
            int eventid = int.TryParse(eventId, out eventid) ? eventid : 0;

            var jsonS = new JsonSerializer();

            ApiReturnObject rtnObject = new ApiReturnObject() { Code = ApiReturnCode.Success };
            if (userId == null)
            {
                rtnObject.Code = ApiReturnCode.ApiUserIdError;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeApiUserIdError;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }

            var jsonCol = VourcherFacade.ViewVourcherStoreCollectionGetByEventId(pagestart, pagelength, eventid, string.Empty)
                .Select(x => new
                {
                    Id = x.Id,
                    EventId = x.VourcherEventId,
                    StoreName = x.StoreName,
                    Address = x.City + x.Town + x.AddressString,
                    Tel = x.Phone,
                    SellerGuid = x.SellerGuid
                });

            rtnObject.Data = jsonCol;

            this.Context.Response.Write(jsonS.Serialize(rtnObject));
        }

        /// <summary>
        /// 依分店guid搜尋分店資訊 預定作廢
        /// </summary>
        /// <param name="store_guid">分店guid</param>
        /// <param name="userId">呼叫API的client端裝置或合作機關的帳號</param>
        [WebMethod]
        public void GetStoreByGuid(string storeGuid, string userId)
        {
            const string methodName = "GetViewVourcherStoreCollectionGetByEventId";
            //設定輸出格式為json格式
            ContextBaseSetting();

            //即將作廢，所以紀錄看看有誰呼叫他，不記錄細節
            SetApiLog(GetMethodName(methodName), userId, null, null);

            Guid storeguid = Guid.TryParse(storeGuid, out storeguid) ? storeguid : Guid.Empty;
            Store store = VourcherFacade.StoreGetByGuid(storeguid);

            var jsonS = new JsonSerializer();

            ApiReturnObject rtnObject = new ApiReturnObject() { Code = ApiReturnCode.Success };
            if (userId == null)
            {
                rtnObject.Code = ApiReturnCode.ApiUserIdError;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeApiUserIdError;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }

            var jsonCol = new
                {
                    StoreGuid = store.Guid,
                    StoreName = store.StoreName,
                    Tel = store.Phone,
                    OpenTime = store.OpenTime,
                    CloseDate = store.CloseDate,
                    WebUrl = store.WebUrl,
                    BlogUrl = store.BlogUrl,
                    CreditCard = store.CreditcardAvailable,
                    Mrt = store.Mrt,
                    Car = store.Car,
                    Bus = store.Bus,
                    OtherVehicles = store.OtherVehicles
                };

            rtnObject.Data = jsonCol;

            this.Context.Response.Write(jsonS.Serialize(rtnObject));
        }

        /// <summary>
        /// 收藏/取消收藏 優惠券
        /// </summary>
        /// <param name="eventId">優惠券id</param>
        /// <param name="status">收藏狀態</param>
        /// <param name="userId">呼叫API的client端裝置或合作機關的帳號</param>
        [WebMethod]
        public void SetVourcherCollect(int eventId, int status, string userId)
        {
            const string methodName = "SetVourcherCollect";
            //設定輸出格式為json格式
            ContextBaseSetting();

            var jsonS = new JsonSerializer();

            ApiReturnObject rtnObject;
            //驗證使用者
            if (!CheckApiUser(userId, out rtnObject))
            {
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
                return;
            }

            //檢查會員是否已登入
            string userName;
            if (!CheckMemberSingin(out userName,out rtnObject))
            {
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
                return;
            }


            int uId = MemberFacade.GetUniqueId(userName);

            VourcherFacade.SetVourcherCollect(uId, eventId, (VourcherCollectStatus)status);
            rtnObject.Code = ApiReturnCode.Success;
            rtnObject.Data = "";

            this.Context.Response.Write(jsonS.Serialize(rtnObject));


            //紀錄LOG
            Object parameter = new
                {
                    eventId = eventId,
                    status = status,
                    userId = userId
                };
            SetApiLog(GetMethodName(methodName), userId, parameter, rtnObject);
        }

        /// <summary>
        /// 使用優惠券
        /// </summary>
        /// <param name="eventId">優惠券id</param>
        /// <param name="status">使用狀態</param>
        [WebMethod]
        public void SetVourcherOrder(string userId , int eventId , int status)
        {
            const string methodName = "SetVourcherOrder";
            //設定輸出格式為json格式
            ContextBaseSetting();

            var jsonS = new JsonSerializer();

            ApiReturnObject rtnObject;
            //驗證使用者
            if (!CheckApiUser(userId, out rtnObject))
            {
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
                return;
            }

            //檢查會員是否已登入
            string userName;
            if (!CheckMemberSingin(out userName, out rtnObject))
            {
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
                return;
            }

            int uId = MemberFacade.GetUniqueId(userName);

            VourcherFacade.SetVourcherOrder(uId , eventId, (VourcherOrderStatus)status);

            rtnObject.Data = "";

            this.Context.Response.Write(jsonS.Serialize(rtnObject));

            //紀錄LOG
            Object parameter = new
            {
                eventId = eventId,
                status = status,
                userId = userId
            };
            SetApiLog(GetMethodName(methodName), userId, parameter, rtnObject);
            
        }

        /// <summary>
        /// 依使用者id搜尋收藏的優惠券
        /// </summary>
        /// <param name="pageStart">起頁(-1全選)</param>
        /// <param name="pageLength">每頁大小</param>
        /// <param name="userId">呼叫API的client端裝置或合作機關的帳號</param>
        [WebMethod]
        public void ViewVourcherSellerCollectGetList(string pageStart, string pageLength , string userId)
        {
            const string methodName = "ViewVourcherSellerCollectGetList";
            //設定輸出格式為json格式
            ContextBaseSetting();

            int pagestart = int.TryParse(pageStart, out pagestart) ? pagestart : -1;
            int pagelength = int.TryParse(pageLength, out pagelength) ? pagelength : 10;

            var jsonS = new JsonSerializer();

            ApiReturnObject rtnObject;
            //驗證使用者
            if (!CheckApiUser(userId, out rtnObject))
            {
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
                return;
            }

            //檢查會員是否已登入
            string userName;
            if (!CheckMemberSingin(out userName, out rtnObject))
            {
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
                return;
            }

            int uId = MemberFacade.GetUniqueId(userName);

            var jsonCol = ApiVourcherManager.ApiVourcherEventSynopsesGetUserCollectListByUserId(pagestart,pagelength,uId);

            rtnObject.Data = jsonCol;

            this.Context.Response.Write(jsonS.Serialize(rtnObject));

            //紀錄LOG
            Object parameter = new
            {
                pageStart = pageStart, pageLength = pageLength , userId = userId
            };
            SetApiLog(GetMethodName(methodName), userId, parameter, rtnObject);
        }

        /// <summary>
        /// 依使用者id搜尋使用的優惠券 預定作廢
        /// </summary>
        /// <param name="page_start">起頁(-1全選)</param>
        /// <param name="page_length">每頁大小</param>
        /// <param name="seller_id">賣家id</param>
        /// <param name="userId">呼叫API的client端裝置或合作機關的帳號</param>
        [WebMethod]
        public void ViewVourcherSellerOrderGetList(string pageStart, string pageLength , string userId)
        {
            const string methodName = "ViewVourcherSellerOrderGetList";
            //設定輸出格式為json格式
            ContextBaseSetting();

            int pagestart = int.TryParse(pageStart, out pagestart) ? pagestart : -1;
            int pagelength = int.TryParse(pageLength, out pagelength) ? pagelength : 10;

            var jsonS = new JsonSerializer();

            ApiReturnObject rtnObject;
            //驗證使用者
            if (!CheckApiUser(userId, out rtnObject))
            {
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
                return;
            }

            //檢查會員是否已登入
            string userName;
            if (!CheckMemberSingin(out userName, out rtnObject))
            {
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
                return;
            }

            int uId = MemberFacade.GetUniqueId(userName);

            var jsonCol = VourcherFacade.ViewVourcherSellerOrderGetList(pagestart, pagelength, uId, string.Empty)
                .Select(x => new
                {
                    Id = x.Id,
                    Contents = x.Contents,
                    Instruction = x.Instruction,
                    Restriction = x.Restriction,
                    EndDate = DateTimeToDateTimeString(x.EndDate.Value),
                    AttentionCount = x.PageCount,
                    Mode = x.Mode,
                    MaxQuantity = x.MaxQuantity,
                    CurrentQuantity = x.CurrentQuantity,
                    PicUrl = ImageFacade.GetMediaPathsFromRawData(x.PicUrl,
                                                    MediaType.PponDealPhoto).ToList(),
                    SellerName = x.SellerName
                });

            rtnObject.Data = jsonCol;

            this.Context.Response.Write(jsonS.Serialize(rtnObject));

            //紀錄LOG
            Object parameter = new
            {
                pageStart = pageStart,
                pageLength = pageLength,
                userId = userId
            };
            SetApiLog(GetMethodName(methodName), userId, parameter, rtnObject);
        }
        /// <summary>
        /// 行銷Banner
        /// Type=(int)VourcherPromoType.Seller=0;以賣家sellerid作為連結(帶出所有該賣家的優惠券)
        /// Type=(int)VourcherPromoType.Vourcher=1;直接帶出該優惠券連結
        /// Banner圖片為賣家logo
        /// </summary>
        /// <param name="userId">呼叫API的client端裝置或合作機關的帳號</param>
        [WebMethod]
        public void VourcherPromoDataGetList(string userId)
        {
            const string methodName = "ViewVourcherSellerOrderGetList";
            //設定輸出格式為json格式
            ContextBaseSetting();

            var jsonS = new JsonSerializer();

            ApiReturnObject rtnObject;
            //驗證使用者
            if (!CheckApiUser(userId, out rtnObject))
            {
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
                return;
            }

            var jsonCol = ApiVourcherManager.ApiVourcherPromoDataGetList();

            rtnObject.Data = jsonCol;

            this.Context.Response.Write(jsonS.Serialize(rtnObject));

            //紀錄LOG
            Object rtnValue = new { count = (jsonCol == null ? 0 : jsonCol.Count), message = "回傳資料筆數" };
            SetApiLog(GetMethodName(methodName), userId, null, rtnValue);
        }

        [WebMethod]
        public void TodayVourchers()
        {
            //設定輸出格式為json格式
            ContextBaseSetting();

            List<ApiToyotaVourcherEvent> apiToyotaVourcherEventCol = new List<ApiToyotaVourcherEvent>();
            
            ViewVourcherSellerCollection viewVourcherSellerCollection =
                VourcherFacade.TodayVourcherEventCollectionGet(DateTime.Now.Date);

            foreach (var item in viewVourcherSellerCollection)
            {
                var apiToyotaVourcherEvent = GetApiToyotaVourcherEvent(item);
                if (apiToyotaVourcherEvent != null)
                {
                    apiToyotaVourcherEventCol.Add(apiToyotaVourcherEvent);
                }
            }

            var jsonS = new JsonSerializer();
            this.Context.Response.Write(jsonS.Serialize(apiToyotaVourcherEventCol));
        }

        private ApiToyotaVourcherEvent GetApiToyotaVourcherEvent(ViewVourcherSeller vourcherSeller)
        {
            //商家說明
            Seller seller = ProviderFactory.Instance().GetProvider<ISellerProvider>().SellerGet(vourcherSeller.SellerGuid);
            string sellerDescription = string.Empty;
            if (seller.IsLoaded)
            {
                sellerDescription = seller.SellerDescription;
            }

            ApiToyotaVourcherEvent apiVourcherEvent = new ApiToyotaVourcherEvent();
            apiVourcherEvent.Id = vourcherSeller.Id;
            apiVourcherEvent.Contents = vourcherSeller.Contents;
            apiVourcherEvent.Instruction = vourcherSeller.Instruction;
            apiVourcherEvent.Restriction = vourcherSeller.Restriction;
            apiVourcherEvent.Others = vourcherSeller.Others;
            apiVourcherEvent.EndDate = ApiSystemManager.DateTimeToDateTimeString(vourcherSeller.EndDate.Value);
            apiVourcherEvent.Mode = (VourcherEventMode)vourcherSeller.Mode;
            apiVourcherEvent.MaxQuantity = vourcherSeller.MaxQuantity;
            apiVourcherEvent.CurrentQuantity = vourcherSeller.CurrentQuantity;
            apiVourcherEvent.PicUrl = ImageFacade.GetMediaPathsFromRawData(vourcherSeller.PicUrl, MediaType.PponDealPhoto).ToList();
            apiVourcherEvent.SellerName = vourcherSeller.SellerName;
            apiVourcherEvent.SellerGuid = vourcherSeller.SellerGuid;
            apiVourcherEvent.SellerCategory = (vourcherSeller.SellerCategory.HasValue) ? vourcherSeller.SellerCategory.Value : (int)SellerSampleCategory.Others;
            apiVourcherEvent.SellerDescription = sellerDescription;
            apiVourcherEvent.ConsumptionAvg = (SellerConsumptionAvg?)vourcherSeller.SellerConsumptionAvg; //平均店消費

            //分店
            ViewVourcherStoreCollection stores = VourcherFacade.ViewVourcherStoreCollectionGetByEventId(0, 0, vourcherSeller.Id, string.Empty);
            apiVourcherEvent.EventStores = new List<ApiToyotaVourcherEventStore>();
            foreach (ViewVourcherStore store in stores)
            {
                ApiToyotaVourcherEventStore apiStore = new ApiToyotaVourcherEventStore();

                apiStore.Id = store.Id;
                apiStore.StoreGuid = store.StoreGuid;
                apiStore.StoreName = store.StoreName;
                apiStore.Address = store.City + store.Town + store.AddressString;
                apiStore.Phone = store.Phone;
                apiStore.OpenTime = store.OpenTime;
                apiStore.CloseDate = store.CloseDate;
                apiStore.Remarks = store.Remarks;
                Microsoft.SqlServer.Types.SqlGeography storeGeo = LocationFacade.GetGeographyByCoordinate(store.Coordinate);
                if (storeGeo != null && storeGeo != Microsoft.SqlServer.Types.SqlGeography.Null)
                {
                    double geoLong;
                    double geoLat;
                    apiStore.Longitude = (double.TryParse(storeGeo.Long.ToString(), out geoLong)) ? geoLong : default(double);
                    apiStore.Latitude = (double.TryParse(storeGeo.Lat.ToString(), out geoLat)) ? geoLat : default(double);
                }
                else
                {
                    apiStore.Longitude = default(double);
                    apiStore.Latitude = default(double);
                }

                apiVourcherEvent.EventStores.Add(apiStore);
            }
            apiVourcherEvent.AttentionCount = vourcherSeller.PageCount;

            return apiVourcherEvent;
        }

        /// <summary>
        /// 將日期欄位轉換成API用的字串
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        private static string DateTimeToDateTimeString(DateTime date)
        {
            return ApiSystemManager.DateTimeToDateTimeString(date);
            //return date.ToString("yyyyMMdd HHmmss zz00");
        }
    }
}

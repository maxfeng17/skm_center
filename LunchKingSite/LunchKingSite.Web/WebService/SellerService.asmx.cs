﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Models;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Component;
using LunchKingSite.WebLib.Views;

namespace LunchKingSite.Web.WebService
{
    /// <summary>
    /// Summary description for SellerService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class SellerService : System.Web.Services.WebService
    {

        [WebMethod]
        public string GetSellerPponList(string sellerGuid)
        {
            var vpdc = SellerUtility.GetSellerViewPponDealOnShow(new Guid(sellerGuid));
            var pponList = new List<SellerPponModel>();
            foreach (ViewPponDeal deal in vpdc)
            {
                var model = new SellerPponModel();
                model.EventName = deal.EventName;
                model.BusinessHourGuid = deal.BusinessHourGuid;
                model.EventImagePath = deal.EventImagePath;
                model.BusinessHourOrderTimeS = deal.BusinessHourOrderTimeS;
                model.BusinessHourOrderTimeE = deal.BusinessHourOrderTimeE;
                QuantityAdjustment qa = deal.GetAdjustedOrderedQuantity();
                model.OrderedQuantity = qa.Quantity;
                QuantityAdjustment qs = deal.GetAdjustedSlug();
                model.Slug = qs.Quantity.ToString();
                model.UnitName = qa.IsAdjusted ? "份" : "人";
                model.ItemPrice = deal.ItemPrice;
                model.ItemOrigPrice = deal.ItemOrigPrice;
                pponList.Add(model);
            }
            return new JsonSerializer().Serialize(pponList);
        }
    }

    public class SellerPponModel
    {
        public string EventName { get; set; }
        public Guid BusinessHourGuid { get; set; }
        public string EventImagePath { get; set; }
        public DateTime BusinessHourOrderTimeS { get; set; }
        public DateTime BusinessHourOrderTimeE { get; set; }
        public string Slug { get; set; }
        public int? OrderedQuantity { get; set; }
        public decimal ItemPrice { get; set; }
        public decimal ItemOrigPrice { get; set; }
        public string UnitName { get; set; }
    }

}

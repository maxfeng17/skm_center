﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using LunchKingSite.Mongo.Services;
using LunchKingSite.Mongo.Facade;
using LunchKingSite.BizLogic.Model.Sales;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using MongoDB.Bson;
using System.Xml;
using System.Data;
using System.Net;
using System.Text.RegularExpressions;
using System.Globalization;

namespace LunchKingSite.Web.WebService
{
    /// <summary>
    /// Summary description for PponDealService
    /// </summary>
    //[WebService(Namespace = "http://tempuri.org/")]
    [WebService(Namespace = "http://www.17life.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class ReportingService : System.Web.Services.WebService
    {
        private const string allowedAddress = "10.15.0.3";

        [WebMethod]
        public XmlDocument RemittanceTypeCheck()
        {
            if (allowedAddress != HttpContext.Current.Request.UserHostAddress)
                return null;

            var rtnXml = new XmlDocument();
            DateTime d1 = (this.Context.Request.QueryString["d1"] != null) ? DateTime.ParseExact(this.Context.Request.QueryString["d1"], "yyyyMMdd", CultureInfo.InvariantCulture) : new DateTime(DateTime.Now.AddDays(1).Year, DateTime.Now.AddDays(1).Month, DateTime.Now.AddDays(1).Day, 0, 0, 0);
            DateTime d2 = (d1.DayOfWeek == DayOfWeek.Saturday) ?
                new DateTime(DateTime.Now.AddDays(3).Year, DateTime.Now.AddDays(3).Month, DateTime.Now.AddDays(3).Day, 23, 59, 59) :
                new DateTime(DateTime.Now.AddDays(1).Year, DateTime.Now.AddDays(1).Month, DateTime.Now.AddDays(1).Day, 23, 59, 59);

            List<BusinessOrder> list = SalesFacade.GetBusinessOrderList(d1, d2);
            DataTable dt = SalesFacade.GetRemittanceTypeCheckReport();

            //建立根節點
            var rootNode = rtnXml.CreateElement("Results");
            rtnXml.AppendChild(rootNode);
            //設定XML標頭
            var xmldecl = rtnXml.CreateXmlDeclaration("1.0", null, null);
            xmldecl.Encoding = "UTF-8";
            rtnXml.InsertBefore(xmldecl, rootNode);
            int i = dt.Rows.Count;
            foreach (DataRow r in dt.AsEnumerable())
            {
                //建立父節點
                var parentNode = rtnXml.CreateElement("Data");
                rootNode.AppendChild(parentNode);

                var _IsMainDeal = rtnXml.CreateElement("IsMainDeal");
                _IsMainDeal.InnerText = r.Field<string>("IsMainDeal");
                parentNode.AppendChild(_IsMainDeal);

                var _SellerId = rtnXml.CreateElement("SellerId");
                _SellerId.InnerText = r.Field<string>("seller_id");
                parentNode.AppendChild(_SellerId);

                var _BusinessHourOrderTimeS_Date = rtnXml.CreateElement("BusinessHourOrderTimeS_Date");
                _BusinessHourOrderTimeS_Date.InnerText = r.Field<DateTime>("business_hour_order_time_s").ToString("yyyy/MM/dd");
                parentNode.AppendChild(_BusinessHourOrderTimeS_Date);

                var _BusinessHourOrderTimeS_Time = rtnXml.CreateElement("BusinessHourOrderTimeS_Time");
                _BusinessHourOrderTimeS_Time.InnerText = r.Field<DateTime>("business_hour_order_time_s").ToString("HH:mm:ss");
                parentNode.AppendChild(_BusinessHourOrderTimeS_Time);

                var _BusinessHourGuid = rtnXml.CreateElement("BusinessHourGuid");
                _BusinessHourGuid.InnerText = r.Field<Guid>("business_hour_guid").ToString();
                parentNode.AppendChild(_BusinessHourGuid);

                var _UniqueId = rtnXml.CreateElement("UniqueId");
                _UniqueId.InnerText = r.Field<int>("unique_id").ToString();
                parentNode.AppendChild(_UniqueId);

                var _BusinessOrderId = rtnXml.CreateElement("BusinessOrderId");
                _BusinessOrderId.InnerText = list.Any(x => x.BusinessHourGuid == r.Field<Guid?>("business_hour_guid")) ? list.Where(x => x.BusinessHourGuid == r.Field<Guid?>("business_hour_guid")).FirstOrDefault().BusinessOrderId : list.Any(x => x.BusinessHourGuid == r.Field<Guid?>("MainBid")) ? list.Where(x => x.BusinessHourGuid == r.Field<Guid?>("MainBid")).FirstOrDefault().BusinessOrderId : string.Empty;
                parentNode.AppendChild(_BusinessOrderId);

                var _SalesId = rtnXml.CreateElement("SalesId");
                _SalesId.InnerText = r.Field<string>("sales_id");
                parentNode.AppendChild(_SalesId);

                var _DeliveryType = rtnXml.CreateElement("DeliveryType");
                _DeliveryType.InnerText = r.Field<string>("DeliveryType");
                parentNode.AppendChild(_DeliveryType);

                var _AccBusinessGroupName = rtnXml.CreateElement("AccBusinessGroupName");
                _AccBusinessGroupName.InnerText = r.Field<string>("acc_business_group_name");
                parentNode.AppendChild(_AccBusinessGroupName);

                var _AuthorizationGroup = rtnXml.CreateElement("AuthorizationGroup");
                _AuthorizationGroup.InnerText = r.Field<string>("AuthorizationGroup");
                parentNode.AppendChild(_AuthorizationGroup);

                var _PayToCompany = rtnXml.CreateElement("PayToCompany");
                _PayToCompany.InnerText = r.Field<string>("paytocompany");
                parentNode.AppendChild(_PayToCompany);

                var _VendorBillingModel = rtnXml.CreateElement("VendorBillingModel");
                _VendorBillingModel.InnerText = r.Field<string>("VendorBillingModel");
                parentNode.AppendChild(_VendorBillingModel);

                var _RemittanceType = rtnXml.CreateElement("RemittanceType");
                _RemittanceType.InnerText = r.Field<string>("RemittanceType");
                parentNode.AppendChild(_RemittanceType);

                var _VendorReceiptType = rtnXml.CreateElement("VendorReceiptType");
                _VendorReceiptType.InnerText = r.Field<string>("VendorReceiptType");
                parentNode.AppendChild(_VendorReceiptType);

                var _ItemName = rtnXml.CreateElement("ItemName");
                _ItemName.InnerText = r.Field<string>("item_name");
                parentNode.AppendChild(_ItemName);

                var _ItemOrigPrice = rtnXml.CreateElement("ItemOrigPrice");
                _ItemOrigPrice.InnerText = r.Field<decimal>("item_orig_price").ToString();
                parentNode.AppendChild(_ItemOrigPrice);

                var _ItemPrice = rtnXml.CreateElement("ItemPrice");
                _ItemPrice.InnerText = r.Field<decimal>("item_price").ToString();
                parentNode.AppendChild(_ItemPrice);

                var _SlottingFeeQuantity = rtnXml.CreateElement("SlottingFeeQuantity");
                _SlottingFeeQuantity.InnerText = r.Field<int>("quantity").ToString();
                parentNode.AppendChild(_SlottingFeeQuantity);

                var _Cost = rtnXml.CreateElement("cost");
                _Cost.InnerText = r.Field<decimal>("cost").ToString();
                parentNode.AppendChild(_Cost);

                var _GrossMargin = rtnXml.CreateElement("GrossMargin");
                _GrossMargin.InnerText = r.Field<string>("GrossMargin");
                parentNode.AppendChild(_GrossMargin);

                var _OrderTotalLimit = rtnXml.CreateElement("OrderTotalLimit");
                _OrderTotalLimit.InnerText = r.Field<decimal>("order_total_limit").ToString();
                parentNode.AppendChild(_OrderTotalLimit);

                var _MaxItemCount = rtnXml.CreateElement("MaxItemCount");
                _MaxItemCount.InnerText = r.Field<int>("max_item_count").ToString();
                parentNode.AppendChild(_MaxItemCount);

                var _BusinessHourAtmMaximum = rtnXml.CreateElement("BusinessHourAtmMaximum");
                _BusinessHourAtmMaximum.InnerText = r.Field<int>("business_hour_atm_maximum").ToString();
                parentNode.AppendChild(_BusinessHourAtmMaximum);
            }
            return rtnXml;
        }


        [WebMethod]
        public XmlDocument BusinessOrder()
        {
            if (allowedAddress != HttpContext.Current.Request.UserHostAddress)
                return null;

            var rtnXml = new XmlDocument();
            DateTime d1 = (this.Context.Request.QueryString["d1"] != null) ? DateTime.ParseExact(this.Context.Request.QueryString["d1"], "yyyyMMdd", CultureInfo.InvariantCulture) : new DateTime(DateTime.Now.AddDays(1).Year, DateTime.Now.AddDays(1).Month, DateTime.Now.AddDays(1).Day, 0, 0, 0);
            DateTime d2 = (d1.DayOfWeek == DayOfWeek.Saturday) ?
                new DateTime(DateTime.Now.AddDays(3).Year, DateTime.Now.AddDays(3).Month, DateTime.Now.AddDays(3).Day, 23, 59, 59) :
                new DateTime(DateTime.Now.AddDays(1).Year, DateTime.Now.AddDays(1).Month, DateTime.Now.AddDays(1).Day, 23, 59, 59);

            List<BusinessOrder> list = SalesFacade.GetBusinessOrderList(d1, d2);
            AccBusinessGroupCollection group = SalesFacade.AccBusinessGroupGetList();
            DepartmentCollection deprt = SalesFacade.DepartmentGetList();
            
            //建立根節點
            var rootNode = rtnXml.CreateElement("BusinessOrders");
            rtnXml.AppendChild(rootNode);
            //設定XML標頭
            var xmldecl = rtnXml.CreateXmlDeclaration("1.0", null, null);
            xmldecl.Encoding = "UTF-8";
            rtnXml.InsertBefore(xmldecl, rootNode);
            foreach (BusinessOrder b in list.OrderBy(x => x.BusinessHourTimeS))
            {
                for (int i = 0; i <= b.Deals.Count - 1; i++)
                {
                    //建立父節點
                    var parentNode = rtnXml.CreateElement("BusinessOrder");
                    rootNode.AppendChild(parentNode);

                    var _SellerId = rtnXml.CreateElement("SellerId");
                    _SellerId.InnerText = b.SellerId;
                    parentNode.AppendChild(_SellerId);

                    var _BusinessHourTimeS_Date = rtnXml.CreateElement("BusinessHourTimeS_Date");
                    _BusinessHourTimeS_Date.InnerText = b.BusinessHourTimeS != null ? b.BusinessHourTimeS.Value.ToLocalTime().ToString("yyyy/MM/dd") : string.Empty;
                    parentNode.AppendChild(_BusinessHourTimeS_Date);

                    var _BusinessOrderId = rtnXml.CreateElement("BusinessOrderId");
                    _BusinessOrderId.InnerText = b.BusinessOrderId;
                    parentNode.AppendChild(_BusinessOrderId);

                    var _BusinessHourGuid = rtnXml.CreateElement("BusinessHourGuid");
                    _BusinessHourGuid.InnerText = b.BusinessHourGuid.ToString();
                    parentNode.AppendChild(_BusinessHourGuid);

                    var _SalesName = rtnXml.CreateElement("SalesName");
                    _SalesName.InnerText = b.SalesName;
                    parentNode.AppendChild(_SalesName);

                    var _AccBusinessGroupId = rtnXml.CreateElement("AccBusinessGroupId");
                    _AccBusinessGroupId.InnerText = !string.IsNullOrEmpty(b.AccBusinessGroupId) ? group.Where(x => x.AccBusinessGroupId ==  Convert.ToInt32(b.AccBusinessGroupId)).FirstOrDefault().AccBusinessGroupName : string.Empty;
                    parentNode.AppendChild(_AccBusinessGroupId);

                    var _AuthorizationGroup = rtnXml.CreateElement("AuthorizationGroup");
                    _AuthorizationGroup.InnerText = !string.IsNullOrEmpty(b.AuthorizationGroup) ? deprt.Where(x => x.DeptId == b.AuthorizationGroup).FirstOrDefault().DeptName : string.Empty;
                    parentNode.AppendChild(_AuthorizationGroup);

                    var _PayToCompany = rtnXml.CreateElement("PayToCompany");
                    switch (b.PayToCompany)
                    {
                        case DealAccountingPayType.PayToStore:
                            _PayToCompany.InnerText = "匯至各分店匯款資料";
                            break;
                        case DealAccountingPayType.PayToCompany:
                            _PayToCompany.InnerText = "統一匯至賣家匯款資料";
                            break;
                    }
                    parentNode.AppendChild(_PayToCompany);

                    var _RemittancePaidType = rtnXml.CreateElement("RemittancePaidType");
                    switch (b.RemittancePaidType)
                    {
                        case RemittanceType.AchMonthly:
                            _RemittancePaidType.InnerText = "ACH每月付款";
                            break;
                        case RemittanceType.AchWeekly:
                            _RemittancePaidType.InnerText = "ACH每周付款";
                            break;
                        case RemittanceType.ManualMonthly:
                            _RemittancePaidType.InnerText = "人工每月付款";
                            break;
                        case RemittanceType.ManualPartially:
                            _RemittancePaidType.InnerText = "暫付七成";
                            break;
                        case RemittanceType.ManualWeekly:
                            _RemittancePaidType.InnerText = "人工每周付款";
                            break;
                        case RemittanceType.Others:
                            _RemittancePaidType.InnerText = "其他付款方式";
                            break;
                        case RemittanceType.Flexible:
                            _RemittancePaidType.InnerText = "彈性選擇出帳";
                            break;
                        case RemittanceType.Monthly:
                            _RemittancePaidType.InnerText = "新每月出帳";
                            break;
                        case RemittanceType.Weekly:
                            _RemittancePaidType.InnerText = "新每週出帳";
                            break;
                    }
                    parentNode.AppendChild(_RemittancePaidType);

                    var _InvoiceFounderType = rtnXml.CreateElement("InvoiceFounderType");
                    switch (b.Invoice.Type)
                    {
                        case InvoiceType.DuplicateUniformInvoice:
                            switch ((BusinessOrderInvoiceTax)Convert.ToInt32(b.Invoice.InTax))
                            {
                                case BusinessOrderInvoiceTax.None:
                                    _InvoiceFounderType.InnerText = "三聯式統一發票(免稅)";
                                    break;
                                case BusinessOrderInvoiceTax.In5Tax:
                                    _InvoiceFounderType.InnerText = "三聯式統一發票(應稅(5%營業稅))";
                                    break;
                                case BusinessOrderInvoiceTax.In18Tax:
                                    _InvoiceFounderType.InnerText = "三聯式統一發票(18%娛樂稅)";
                                    break;
                            }
                            break;
                        case InvoiceType.General:
                            _InvoiceFounderType.InnerText = "收銀機統一發票";
                            break;
                        case InvoiceType.NonInvoice:
                            _InvoiceFounderType.InnerText = "免用統一發票收據";
                            break;
                        case InvoiceType.Other:
                            _InvoiceFounderType.InnerText = "其他";
                            break;
                    }
                    parentNode.AppendChild(_InvoiceFounderType);

                    var _OrignPrice = rtnXml.CreateElement("OrignPrice");
                    _OrignPrice.InnerText = b.Deals.Count > 0 ? b.Deals[i].OrignPrice : string.Empty;
                    parentNode.AppendChild(_OrignPrice);

                    var _ItemPrice = rtnXml.CreateElement("ItemPrice");
                    _ItemPrice.InnerText = b.Deals.Count > 0 ? b.Deals[i].ItemPrice : string.Empty;
                    parentNode.AppendChild(_ItemPrice);

                    var _SlottingFeeQuantity = rtnXml.CreateElement("SlottingFeeQuantity");
                    _SlottingFeeQuantity.InnerText = b.Deals.Count > 0 ? b.Deals[i].SlottingFeeQuantity : string.Empty;
                    parentNode.AppendChild(_SlottingFeeQuantity);

                    var _PurchasePrice = rtnXml.CreateElement("PurchasePrice");
                    _PurchasePrice.InnerText = b.Deals.Count > 0 ? b.Deals[i].PurchasePrice : string.Empty;
                    parentNode.AppendChild(_PurchasePrice);

                    var _HasFreight = rtnXml.CreateElement("HasFreight");
                    _HasFreight.InnerText = b.Deals.Count > 0 ? (b.BusinessType == BusinessType.Product && !b.Deals[i].IsIncludeFreight && !string.IsNullOrWhiteSpace(b.Deals[i].Freight) && b.Deals[i].Freight.Trim() != "0" ? "＊" : string.Empty) : string.Empty;
                    parentNode.AppendChild(_HasFreight);

                    var _MaxQuentity = rtnXml.CreateElement("MaxQuentity");
                    _MaxQuentity.InnerText = b.Deals.Count > 0 ? b.Deals[i].MaxQuentity : string.Empty;
                    parentNode.AppendChild(_MaxQuentity);

                    var _LimitPerQuentity = rtnXml.CreateElement("LimitPerQuentity");
                    _LimitPerQuentity.InnerText = b.Deals.Count > 0 ? b.Deals[i].LimitPerQuentity : string.Empty;
                    parentNode.AppendChild(_LimitPerQuentity);

                    var _AtmQuantity = rtnXml.CreateElement("AtmQuantity");
                    _AtmQuantity.InnerText = b.Deals.Count > 0 ? b.Deals[i].AtmQuantity : string.Empty;
                    parentNode.AppendChild(_AtmQuantity);
                }
            }

            return rtnXml; //.InnerXml;
        }
    }
}

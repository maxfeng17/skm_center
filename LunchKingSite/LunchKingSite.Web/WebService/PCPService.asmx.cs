﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.Core;
using LunchKingSite.BizLogic.Component.API;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.Web.ControlRoom;
using LunchKingSite.WebApi.Core.OAuth;
using System.Collections;
using System.IO;


namespace LunchKingSite.Web.WebService
{
    /// <summary>
    /// Summary description for PCPService
    /// </summary>
    //[WebService(Namespace = "http://tempuri.org/")]
    [WebService(Namespace = "http://www.17life.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]

    public class PCPService : BaseService
    {
        private static string baseVerion = "5.9.0"; //App 大改版起始版號

        [WebMethod]
        public void MembershipCardQueryData(string userId)
        {
            const string methodName = "MembershipCardQueryData";
            //設定輸出格式為json格式
            ContextBaseSetting();

            var jsonS = new JsonSerializer();

            ApiReturnObject rtnObject;
            //驗證使用者
            if (!CheckApiUser(userId, out rtnObject))
            {
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
                return;
            }
            //區域的部分
            object regionData = new
            {
                Version = ApiMembershipCardRegionManager.MembershipCardRegionVersion,
                RegionList = ApiMembershipCardRegionManager.RegionsCatchData.RegionList
            };
            //分類的部分
            List<Category> categories = CategoryManager.CategoryGetListByType(CategoryType.MembershipCard);

            List<ApiMembershipCardCategory> categoryList = new List<ApiMembershipCardCategory>();
            foreach (var category in categories)
            {
                categoryList.Add(new ApiMembershipCardCategory { CategoryId = category.Id, CategoryName = category.Name, ShortName = category.Name });
            }
            categoryList.Insert(0, new ApiMembershipCardCategory { CategoryId = 0, CategoryName = "全部類別", ShortName = "全部類別" });

            Object categoryData =
                new
                {
                    Version = CategoryManager.MembershipCardCategoryVersion,
                    CategoryList = categoryList
                };


            rtnObject.Data = new { Region = regionData, Category = categoryData };

            this.Context.Response.Write(jsonS.Serialize(rtnObject));

            SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
        }
        [WebMethod]
        public void MembershipCardRegionList(string userId)
        {
            const string methodName = "MembershipCardRegionList";
            //設定輸出格式為json格式
            ContextBaseSetting();

            var jsonS = new JsonSerializer();

            ApiReturnObject rtnObject;
            //驗證使用者
            if (!CheckApiUser(userId, out rtnObject))
            {
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
                return;
            }
            //包含identityCode的網址路徑
            rtnObject.Data =
                new
                {
                    Version = ApiMembershipCardRegionManager.MembershipCardRegionVersion,
                    RegionList = ApiMembershipCardRegionManager.RegionsCatchData.RegionList
                };

            this.Context.Response.Write(jsonS.Serialize(rtnObject));

            SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
        }

        [WebMethod]
        public void MembershipCardCategoryList(string userId)
        {
            const string methodName = "MembershipCardCategoryList";
            //設定輸出格式為json格式
            ContextBaseSetting();

            var jsonS = new JsonSerializer();

            ApiReturnObject rtnObject;
            //驗證使用者
            if (!CheckApiUser(userId, out rtnObject))
            {
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
                return;
            }
            List<Category> categories = CategoryManager.CategoryGetListByType(CategoryType.MembershipCard);

            List<ApiMembershipCardCategory> categoryList = new List<ApiMembershipCardCategory>();
            foreach (var category in categories)
            {
                categoryList.Add(new ApiMembershipCardCategory { CategoryId = category.Id, CategoryName = category.Name, ShortName = category.Name });
            }
            categoryList.Insert(0, new ApiMembershipCardCategory { CategoryId = 0, CategoryName = "全部", ShortName = "全部" });

            rtnObject.Data =
                new
                {
                    Version = CategoryManager.MembershipCardCategoryVersion,
                    CategoryList = categoryList
                };

            this.Context.Response.Write(jsonS.Serialize(rtnObject));

            SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
        }

        [WebMethod]
        public void MembershipCardGroupList(int regionId, int category, int orderBy, double longitude, double latitude, int imageSize, string userId)
        {
            const string methodName = "MembershipCardGroupList";
            //設定輸出格式為json格式
            ContextBaseSetting();

            var jsonS = new JsonSerializer();

            ApiReturnObject rtnObject;
            //驗證使用者
            if (!CheckApiUser(userId, out rtnObject))
            {
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
                return;
            }

            //檢查會員是否已登入(登入非必須)
            string userName;
            CheckMemberSingin(out userName, out rtnObject);
            rtnObject.Code = ApiReturnCode.Success;
            rtnObject.Message = string.Empty;

            var query = new MembershipCardGroupListQueryModel(regionId, category, userName, orderBy, latitude, longitude, imageSize);

            if (!query.IsValid())
            {
                rtnObject.Code = ApiReturnCode.InputInvalid;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeInputError;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
                return;
            }

            rtnObject.Data = ApiPCPManager.GetMembershipCardGroupList(query);

            this.Context.Response.Write(jsonS.Serialize(rtnObject));
            SetApiLog(GetMethodName(methodName), userId, new
            {
                regionId,
                category,
                orderBy,
                longitude,
                latitude,
                imageSize,
                userId
            }, new ApiReturnObject { Code = rtnObject.Code, Message = rtnObject.Message });
        }

        [WebMethod]
        public void MembershipCardGroup(int groupId, double longitude, double latitude, int imageSize, string userId)
        {
            const string methodName = "MembershipCardGroup";
            //設定輸出格式為json格式
            ContextBaseSetting();

            var jsonS = new JsonSerializer();

            ApiReturnObject rtnObject;
            //驗證使用者
            if (!CheckApiUser(userId, out rtnObject))
            {
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
                return;
            }

            //檢查會員是否已登入(登入非必須)
            string userName;
            CheckMemberSingin(out userName, out rtnObject);
            rtnObject.Code = ApiReturnCode.Success;
            rtnObject.Message = string.Empty;

            var query = new MembershipCardGroupListQueryModel(groupId, userName, latitude, longitude, imageSize);

            if (!query.IsValid())
            {
                rtnObject.Code = ApiReturnCode.InputInvalid;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeInputError;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
                return;
            }

            string appVersion = HttpContext.Current.Request.QueryString["appVersion"];
            bool isNewPrototype = ApiSystemManager.IsNewAppVersion(appVersion, baseVerion, baseVerion, userId);

            rtnObject.Data = ApiPCPManager.GetMembershipCardGroup(query, isNewPrototype);
            if (!((MembershipCardAboutGroupResultModel)rtnObject.Data).IsLoaded)
            {
                rtnObject.Code = ApiReturnCode.DataNotExist;
                rtnObject.Data = null;
                rtnObject.Message = "找不到熟客卡資料";
            }
            this.Context.Response.Write(jsonS.Serialize(rtnObject));
            SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
        }

        [WebMethod]
        public void MembershipCardGroupBySales(int groupId, string vbsName, string userId)
        {
            const string methodName = "MembershipCardGroupBySales";
            //設定輸出格式為json格式
            ContextBaseSetting();

            var jsonS = new JsonSerializer();

            ApiReturnObject rtnObject;
            //驗證使用者
            if (!CheckApiUser(userId, out rtnObject))
            {
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
                return;
            }

            //檢查會員是否已登入(登入非必須)
            string userName;
            if (!CheckMemberSingin(out userName, out rtnObject))
            {
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
                return;
            }
            rtnObject.Code = ApiReturnCode.Success;
            rtnObject.Message = string.Empty;

            var query = new MembershipCardGroupListQueryModel(groupId, vbsName, 0.0, 0.0, 0);

            if (!query.IsValid())
            {
                rtnObject.Code = ApiReturnCode.InputInvalid;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeInputError;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
                return;
            }

            rtnObject.Data = ApiPCPManager.GetMembershipCardGroup(query);
            if (!((MembershipCardAboutGroupResultModel)rtnObject.Data).IsLoaded)
            {
                rtnObject.Code = ApiReturnCode.DataNotExist;
                rtnObject.Data = null;
                rtnObject.Message = "找不到熟客卡資料";
            }
            this.Context.Response.Write(jsonS.Serialize(rtnObject));
            SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string GetMembershipCardByGroup(int groupId, int imageSize)
        {
            const string methodName = "GetMembershipCardByGroup";
            //設定輸出格式為json格式
            //ContextBaseSetting();

            var jsonS = new JsonSerializer();

            ApiReturnObject rtnObject;
            //驗證使用者
            //if (!CheckApiUser(userId, out rtnObject))
            //{
            //    //this.Context.Response.Write(jsonS.Serialize(rtnObject));
            //    SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
            //    return jsonS.Serialize(rtnObject);
            //}

            //檢查會員是否已登入(登入非必須)
            string userName;
            CheckMemberSingin(out userName, out rtnObject);
            rtnObject.Code = ApiReturnCode.Success;
            rtnObject.Message = string.Empty;

            var query = new MembershipCardGroupListQueryModel(groupId, userName, 0.0, 0.0, imageSize);

            var data = ApiPCPManager.GetMembershipCardByGroup(query);

            if (data == null) { rtnObject.Code = ApiReturnCode.DataNotExist; }

            rtnObject.Data = data;

            SetApiLog(GetMethodName(methodName), "2431701417", null, rtnObject);
            return jsonS.Serialize(rtnObject);
        }

        [WebMethod]
        public void ApplyCardByGroupId(int groupId, string userId)
        {
            const string methodName = "ApplyCardByGroupId";
            //設定輸出格式為json格式
            ContextBaseSetting();

            var jsonS = new JsonSerializer();

            ApiReturnObject rtnObject;
            //驗證使用者
            if (!CheckApiUser(userId, out rtnObject))
            {
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
                return;
            }

            //檢查會員是否已登入
            string userName;
            if (!CheckMemberSingin(out userName, out rtnObject))
            {
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
                return;
            }

            ApiPCPManager.ApplyCardByGroupId(groupId, userName, out rtnObject);

            this.Context.Response.Write(jsonS.Serialize(rtnObject));
            SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
        }


        [WebMethod]
        public void ApplyCardByGroupIdV2(string groupId, string userId)
        {
            const string methodName = "ApplyCardByGroupIdV2";
            //設定輸出格式為json格式
            ContextBaseSetting();

            var jsonS = new JsonSerializer();

            ApiReturnObject rtnObject;
            //驗證使用者
            if (!CheckApiUser(userId, out rtnObject))
            {
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
                return;
            }

            //檢查會員是否已登入
            string userName;
            if (!CheckMemberSingin(out userName, out rtnObject))
            {
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
                return;
            }

            //檢查會員是否通過一次性同意條款
            if (ApiPCPManager.CheckOnceAgreement(userName) == false)
            {
                this.Context.Response.Write(jsonS.Serialize(new ApiResult()
                {
                    Code = ApiResultCode.PcpAgreementNotAccept,
                    Message = "熟客卡條款尚未同意"
                }));
                SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
                return;
            }

            List<int> groupIds;
            try
            {
                groupIds = jsonS.Deserialize<int[]>(groupId).ToList(); // ["1048","1031"]
            }
            catch
            {
                rtnObject.Code = ApiReturnCode.InputInvalid;
                rtnObject.Message = I18N.Phrase.ApiResultCodeInputError;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
                return;
            }

            ApiResult apiResult;
            ApiPCPManager.ApplyCardByGroupIdV2(groupIds, userName, out apiResult);

            this.Context.Response.Write(jsonS.Serialize(apiResult));
            SetApiLog(GetMethodName(methodName), userId, null, apiResult);
        }

        /// <summary>
        /// 同意一次性條款
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="isAgree"></param>
        [WebMethod]
        public void AgreeMembershipCard(bool isAgree, string userId)
        {
            const string methodName = "AgreeMemberShipCard";
            //設定輸出格式為json格式
            ContextBaseSetting();

            var jsonS = new JsonSerializer();

            ApiReturnObject rtnObject;
            //驗證使用者
            if (!CheckApiUser(userId, out rtnObject))
            {
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
                return;
            }

            //檢查會員是否已登入
            string userName;
            if (!CheckMemberSingin(out userName, out rtnObject))
            {
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
                return;
            }

            ApiPCPManager.AgreeMembershipCard(userName, isAgree);
            this.Context.Response.Write(jsonS.Serialize(rtnObject));
            SetApiLog(GetMethodName(methodName), userId, null, rtnObject);

        }

        [WebMethod]
        public void SRecomdsApplyingCard(int groupId, string refereeId, string userId)
        {
            const string methodName = "SRecomdsApplyingCard";
            //設定輸出格式為json格式
            ContextBaseSetting();

            var jsonS = new JsonSerializer();

            ApiReturnObject rtnObject;
            //驗證使用者
            if (!CheckApiUser(userId, out rtnObject))
            {
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
                return;
            }

            //檢查會員是否已登入
            string userName;
            if (!CheckMemberSingin(out userName, out rtnObject))
            {
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
                return;
            }

            ApiPCPManager.ApplyCardByGroupId(groupId, userName, out rtnObject, refereeId);

            this.Context.Response.Write(jsonS.Serialize(rtnObject));
            SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
        }


        [WebMethod]
        public void GetIdentityCodeUrl(int cardId, int discountCodeId, string userId)
        {
            const string methodName = "GetIdentityCodeUrl";
            //設定輸出格式為json格式
            ContextBaseSetting();

            var jsonS = new JsonSerializer();

            ApiReturnObject rtnObject;
            //驗證使用者
            if (!CheckApiUser(userId, out rtnObject))
            {
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
                return;
            }

            //檢查會員是否已登入
            string userName;
            if (!CheckMemberSingin(out userName, out rtnObject))
            {
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
                return;
            }

            //檢查會員Id是否存在
            int memberUserId = ApiPCPManager.GetMemberUserId(userName);
            if (memberUserId <= 0)
            {
                //無17life會員Id
                rtnObject.Code = ApiReturnCode.UserNoSignIn;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeUserNoSignIn;
                return;
            }

            #region Func

            rtnObject.Data = ApiPCPManager.CreateIdentityCodeUrl(cardId, discountCodeId, memberUserId);

            #endregion Func

            this.Context.Response.Write(jsonS.Serialize(rtnObject));

            SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
        }

        [WebMethod]
        public void SetUserCardsSeq(string userCardIds, string userId)
        {
            const string methodName = "SetUserCardsSeq";
            //設定輸出格式為json格式
            ContextBaseSetting();

            var jsonS = new JsonSerializer();

            ApiReturnObject rtnObject;
            //驗證使用者
            if (!CheckApiUser(userId, out rtnObject))
            {
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
                return;
            }

            //檢查會員是否已登入
            string userName;
            if (!CheckMemberSingin(out userName, out rtnObject))
            {
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
                return;
            }

            //檢查會員Id是否存在
            int memberUserId = ApiPCPManager.GetMemberUserId(userName);
            if (memberUserId <= 0)
            {
                //無17life會員Id
                rtnObject.Code = ApiReturnCode.UserNoSignIn;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeUserNoSignIn;
                return;
            }

            #region Func

            List<int> userMembershipCardIds = new List<int>();

            try
            {
                userMembershipCardIds = jsonS.Deserialize<int[]>(userCardIds).ToList(); // "[10000086,10000087]"
            }
            catch
            {
                rtnObject.Code = ApiReturnCode.InputInvalid;
                rtnObject.Message = I18N.Phrase.ApiResultCodeInvalidInterval;
                SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
                return;
            }


            bool result = ApiPCPManager.SetMembershipCardListSeq(memberUserId, userMembershipCardIds);

            if (!result)
            {
                rtnObject.Code = ApiReturnCode.DataNotExist;
                rtnObject.Message = I18N.Phrase.ApiResultCodeDataNotFound;
                SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
                return;
            }

            #endregion Func

            this.Context.Response.Write(jsonS.Serialize(rtnObject));
            SetApiLog(GetMethodName(methodName), userId, null, rtnObject);

        }

        [WebMethod]
        public void UserMembershipCardList(int orderBy, double longitude, double latitude, int imageSize, string userId)
        {
            const string methodName = "UserMembershipCardList";
            //設定輸出格式為json格式
            ContextBaseSetting();

            var jsonS = new JsonSerializer();

            ApiReturnObject rtnObject;
            //驗證使用者
            if (!CheckApiUser(userId, out rtnObject))
            {
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
                return;
            }

            //檢查會員是否已登入
            //檢查會員是否已登入(登入非必須)
            string userName;
            CheckMemberSingin(out userName, out rtnObject);
            rtnObject.Code = ApiReturnCode.Success;
            rtnObject.Message = string.Empty;

            ////檢查會員Id是否存在
            int memberUserId = ApiPCPManager.GetMemberUserId(userName);

            //if (memberUserId <= 0)
            //{
            //    //無17life會員Id
            //    rtnObject.Code = ApiReturnCode.UserNoSignIn;
            //    rtnObject.Message = I18N.Phrase.ApiReturnCodeUserNoSignIn;
            //    return;
            //}

            #region Func

            ////包含identityCode的網址路徑
            //rtnObject.Data = "http://www.17life.com/vbs_r/verifycode?code=TESTCODE";


            rtnObject.Data = ApiPCPManager.GetUserMembershipCardList(orderBy, longitude, latitude, imageSize, userName);

            #endregion Func

            this.Context.Response.Write(jsonS.Serialize(rtnObject));

            SetApiLog(GetMethodName(methodName), userId, new
            {
                orderBy,
                longitude,
                latitude,
                imageSize,
                userId
            }, new ApiReturnObject { Code = rtnObject.Code, Message = rtnObject.Message });
        }

        [WebMethod]
        public void UserMembershipCardListV2(int orderBy, double longitude, double latitude, int imageSize, string userId)
        {
            const string methodName = "UserMembershipCardListV2";
            //設定輸出格式為json格式
            ContextBaseSetting();

            var jsonS = new JsonSerializer();

            ApiReturnObject rtnObject;
            //驗證使用者
            if (!CheckApiUser(userId, out rtnObject))
            {
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
                return;
            }

            //檢查會員是否已登入(登入非必須)
            string userName;
            CheckMemberSingin(out userName, out rtnObject);
            rtnObject.Code = ApiReturnCode.Success;
            rtnObject.Message = string.Empty;

            rtnObject.Data = ApiPCPManager.GetUserMembershipCardListV2(orderBy, longitude, latitude, imageSize, userName);
            this.Context.Response.Write(jsonS.Serialize(rtnObject));

            SetApiLog(GetMethodName(methodName), userId, new
            {
                orderBy,
                longitude,
                latitude,
                imageSize,
                userId
            }, new ApiReturnObject { Code = rtnObject.Code, Message = rtnObject.Message });
        }

        /// <summary>
        /// 顯示會員卡介紹
        /// </summary>
        /// <param name="userCardId"></param>
        /// <param name="longitude"></param>
        /// <param name="latitude"></param>
        /// <param name="imageSize"></param>
        /// <param name="userId"></param>
        [WebMethod]
        public void UserMembershipCard(int userCardId, double longitude, double latitude, int imageSize, string userId)
        {
            const string methodName = "UserMembershipCard";
            //設定輸出格式為json格式
            ContextBaseSetting();

            var jsonS = new JsonSerializer();

            ApiReturnObject rtnObject;
            //驗證使用者
            if (!CheckApiUser(userId, out rtnObject))
            {
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
                return;
            }

            //檢查會員是否已登入
            string userName;
            if (!CheckMemberSingin(out userName, out rtnObject))
            {
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
                return;
            }

            //檢查會員Id是否存在
            int memberUserId = ApiPCPManager.GetMemberUserId(userName);
            if (memberUserId <= 0)
            {
                //無17life會員Id
                rtnObject.Code = ApiReturnCode.UserNoSignIn;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeUserNoSignIn;
                return;
            }

            #region Func

            var userMembershipCardModel = ApiPCPManager.GetUserMembershipCardModel(userCardId, longitude, latitude, imageSize, memberUserId);

            if (userMembershipCardModel == null)
            {
                rtnObject.Code = ApiReturnCode.DataNotExist;
                rtnObject.Message = I18N.Phrase.ApiResultCodeDataNotFound;
                return;
            }


            rtnObject.Data = userMembershipCardModel;

            #endregion Func

            this.Context.Response.Write(jsonS.Serialize(rtnObject));

            SetApiLog(GetMethodName(methodName), userId, null, rtnObject);

        }

        [WebMethod]
        public void RemoveUserMembershipCard(string userCardIds, string userId)
        {
            const string methodName = "RemoveUserMembershipCard";
            //設定輸出格式為json格式
            ContextBaseSetting();

            var jsonS = new JsonSerializer();

            ApiReturnObject rtnObject;
            //驗證使用者
            if (!CheckApiUser(userId, out rtnObject))
            {
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
                return;
            }

            //檢查會員是否已登入
            string userName;
            if (!CheckMemberSingin(out userName, out rtnObject))
            {
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
                return;
            }

            //檢查會員Id是否存在
            int memberUserId = ApiPCPManager.GetMemberUserId(userName);
            if (memberUserId <= 0)
            {
                //無17life會員Id
                rtnObject.Code = ApiReturnCode.UserNoSignIn;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeUserNoSignIn;
                return;
            }

            #region Func

            List<int> userMembershipCardIds;

            try
            {
                userMembershipCardIds = jsonS.Deserialize<int[]>(userCardIds).ToList(); // "[10000086,10000087]"
            }
            catch
            {
                rtnObject.Code = ApiReturnCode.InputInvalid;
                rtnObject.Message = I18N.Phrase.ApiResultCodeInvalidInterval;
                SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
                return;
            }


            bool result = ApiPCPManager.RemoveMembershipCardList(memberUserId, userMembershipCardIds);

            if (!result)
            {
                rtnObject.Code = ApiReturnCode.DataNotExist;
                rtnObject.Message = I18N.Phrase.ApiResultCodeDataNotFound;
                SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
                return;
            }

            #endregion Func

            this.Context.Response.Write(jsonS.Serialize(rtnObject));
            SetApiLog(GetMethodName(methodName), userId, null, rtnObject);

        }

        [WebMethod]
        public void PersonalDataCollectMatterFormat(string userId)
        {
            const string methodName = "RemoveUserMembershipCard";
            //設定輸出格式為json格式
            ContextBaseSetting();

            var jsonS = new JsonSerializer();

            ApiReturnObject rtnObject;
            //驗證使用者
            if (!CheckApiUser(userId, out rtnObject))
            {
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
                return;
            }

            #region Func

            SystemData systemdata = SystemCodeManager.GetSystemDataByName("PersonalDataCollectMatterContent");
            rtnObject.Data = new
            {
                Version = ApiPCPManager.PersonalDataCollectMatterFormatVersion,
                MatterContent = systemdata.Data
            };


            #endregion Func

            this.Context.Response.Write(jsonS.Serialize(rtnObject));
            SetApiLog(GetMethodName(methodName), userId, null, rtnObject);

        }

        [WebMethod]
        public void MembershipCardGroupQuery(string querySellerName)
        {
            const string methodName = "MembershipCardGroupQuery";
            //設定輸出格式為json格式
            ContextBaseSetting();

            var jsonS = new JsonSerializer();

            ApiReturnObject rtnObject;

            //檢查會員是否已登入
            string userName;
            if (!CheckMemberSingin(out userName, out rtnObject))
            {
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                SetApiLog(GetMethodName(methodName), "2431701417", null, rtnObject);
                return;
            }

            #region Func

            var userMembershipCardModel = ApiPCPManager.GetMembershipCardGroupForSal(querySellerName);

            if (userMembershipCardModel == null)
            {
                rtnObject.Code = ApiReturnCode.DataNotExist;
                rtnObject.Message = I18N.Phrase.ApiResultCodeDataNotFound;
                return;
            }


            if (userMembershipCardModel.Count == 1)
            {
                rtnObject.Message = userMembershipCardModel.First().GroupId == null ?
                    "未開卡" : "";
            }

            rtnObject.Data = userMembershipCardModel;

            #endregion Func

            this.Context.Response.Write(jsonS.Serialize(rtnObject));

            SetApiLog(GetMethodName(methodName), "2431701417", null, rtnObject);

        }

        [WebMethod]
        public void PromoCards(string userId)
        {
            const string methodName = "PromoCards";
            //設定輸出格式為json格式
            ContextBaseSetting();

            var jsonS = new JsonSerializer();

            ApiReturnObject rtnObject;
            //驗證使用者
            if (!CheckApiUser(userId, out rtnObject))
            {
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
                return;
            }

            string userName = "";
            if (!CheckMemberSingin(out userName, out rtnObject)) //若有登入需取userId出來
            {
                rtnObject.Code = ApiReturnCode.Success;  //此功能不需要登入，若未登入也要回傳success
                rtnObject.Message = "";
            }
            rtnObject.Data = ApiPCPManager.GetPromoCardsList(userName);

            this.Context.Response.Write(jsonS.Serialize(rtnObject));
            SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
        }

        [WebMethod]
        public void GetMembershipCardQrCode(string GroupId, string SellerName)
        {
            this.Context.Response.AddHeader("content-disposition", "attachment;filename=" + GroupId + ".png");
            this.Context.Response.ContentType = "image/png";
            byte[] b = ApiPCPManager.GetMembershipCardGroup(GroupId, SellerName);
            this.Context.Response.OutputStream.Write(b, 0, b.Length);
            this.Context.Response.Flush();
        }

        [WebMethod]
        public void ShowCardQrCode(string content)
        {
            //this.Context.Response.AddHeader("content-disposition", "attachment;filename=test.png");
            this.Context.Response.ContentType = "image/png";
            byte[] b = ApiPCPManager.GetMembershipCardGroupQRCdoe(content);
            this.Context.Response.OutputStream.Write(b, 0, b.Length);
            this.Context.Response.Flush();
        }

        [WebMethod]
        public void ImageCompress()
        {
            LunchKingSite.WebLib.Component.BackendUtility.CompressImage();
        }


        #region 新版熟客卡首頁

        /// <summary>
        /// 連鎖品牌商家
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="isAll"></param>
        [WebMethod]
        public void MembershipCardBrandList(string userId, bool isAll = false)
        {
            const string methodName = "MemberShipCardBrandList";
            //設定輸出格式為json格式
            ContextBaseSetting();

            var jsonS = new JsonSerializer();

            ApiReturnObject rtnObject;
            //驗證使用者
            if (!CheckApiUser(userId, out rtnObject))
            {
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
                return;
            }

            rtnObject.Data = ApiPCPManager.GetMembershipCardBrandList(isAll);

            this.Context.Response.Write(jsonS.Serialize(rtnObject));
            SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
        }

        [WebMethod]
        public void MembershipCardGroupListV2(int regionId, int category, int orderBy,
            double longitude, double latitude, int imageSize, string userId, bool getAll = false, int startIndex = 0)
        {
            const string methodName = "MembershipCardGroupListV2";
            //設定輸出格式為json格式
            ContextBaseSetting();

            var jsonS = new JsonSerializer();

            ApiReturnObject rtnObject;
            //驗證使用者
            if (!CheckApiUser(userId, out rtnObject))
            {
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
                return;
            }

            //檢查會員是否已登入(登入非必須)
            string userName;
            CheckMemberSingin(out userName, out rtnObject);
            rtnObject.Code = ApiReturnCode.Success;
            rtnObject.Message = string.Empty;

            var query = new MembershipCardGroupListQueryModel(regionId, category, userName, orderBy, latitude, longitude, imageSize, getAll, startIndex);

            if (!query.IsValid())
            {
                rtnObject.Code = ApiReturnCode.InputInvalid;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeInputError;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
                return;
            }

            rtnObject.Data = ApiPCPManager.GetMembershipCardGroupListV2(query);

            this.Context.Response.Write(jsonS.Serialize(rtnObject));
            SetApiLog(GetMethodName(methodName), userId, new
            {
                regionId,
                category,
                orderBy,
                longitude,
                latitude,
                imageSize,
                userId
            }, new ApiReturnObject { Code = rtnObject.Code, Message = rtnObject.Message });
        }

        [WebMethod]
        public void MembershipCardRecommendList(int regionId, int orderBy, double longitude, double latitude,
            int imageSize, string userId)
        {
            const string methodName = "MembershipCardRecommendList";
            //設定輸出格式為json格式
            ContextBaseSetting();

            var jsonS = new JsonSerializer();

            ApiReturnObject rtnObject;
            //驗證使用者
            if (!CheckApiUser(userId, out rtnObject))
            {
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
                return;
            }

            //檢查會員是否已登入(登入非必須)
            string userName;
            CheckMemberSingin(out userName, out rtnObject);
            rtnObject.Code = ApiReturnCode.Success;
            rtnObject.Message = string.Empty;

            var query = new MembershipCardGroupListQueryModel(regionId, userName, orderBy, latitude, longitude, imageSize);

            if (!query.IsValid())
            {
                rtnObject.Code = ApiReturnCode.InputInvalid;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeInputError;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
                return;
            }

            rtnObject.Data = new { CategoryList = ApiPCPManager.GetMembershipRecommendList(query) };

            this.Context.Response.Write(jsonS.Serialize(rtnObject));
            SetApiLog(GetMethodName(methodName), userId, new
            {
                regionId,
                orderBy,
                longitude,
                latitude,
                imageSize,
                userId
            }, new ApiReturnObject { Code = rtnObject.Code, Message = rtnObject.Message });
        }

        [WebMethod]
        public void MembershipCardStoreInfo(int groupId, string userId, int imageSize, double longitude, double latitude)
        {
            const string methodName = "MemberShipCardStoreInfo";
            //設定輸出格式為json格式
            ContextBaseSetting();

            var jsonS = new JsonSerializer();

            ApiReturnObject rtnObject;
            //驗證使用者
            if (!CheckApiUser(userId, out rtnObject))
            {
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
                return;
            }

            //檢查會員是否已登入(登入非必須)
            string userName;
            CheckMemberSingin(out userName, out rtnObject);
            rtnObject.Code = ApiReturnCode.Success;
            rtnObject.Message = string.Empty;

            var query = new MembershipCardGroupListQueryModel(groupId, userName, latitude, longitude, imageSize);
            rtnObject.Data = new { StoreInfoList = ApiPCPManager.GetMembershipCardStoreInfoList(query) };

            this.Context.Response.Write(jsonS.Serialize(rtnObject));
            SetApiLog(GetMethodName(methodName), userId, new
            {
                imageSize,
                userId
            }, new ApiReturnObject { Code = rtnObject.Code, Message = rtnObject.Message });
        }

        #endregion

        #region 新版熟客卡商家頁

        /// <summary>
        /// 新版熟客卡頁，整合MembershipCardGroup、UserMembershipCard
        /// </summary>
        /// <param name="groupId"></param>
        /// <param name="userId"></param>
        /// <param name="imageSize"></param>
        /// <param name="longitude"></param>
        /// <param name="latitude"></param>
        [WebMethod]
        public void MembershipCardDetail(int groupId, string userId, int imageSize, double longitude, double latitude)
        {
            const string methodName = "MemberShipCardDetail";
            //設定輸出格式為json格式
            ContextBaseSetting();

            var jsonS = new JsonSerializer();

            ApiReturnObject rtnObject;
            //驗證使用者
            if (!CheckApiUser(userId, out rtnObject))
            {
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
                return;
            }

            //檢查會員是否已登入(登入非必須)
            string userName;
            CheckMemberSingin(out userName, out rtnObject);
            rtnObject.Code = ApiReturnCode.Success;
            rtnObject.Message = string.Empty;

            var query = new MembershipCardGroupListQueryModel(groupId, userName, latitude, longitude, imageSize);
            string appVersion = HttpContext.Current.Request.QueryString["appVersion"];
            bool isNewPrototype = ApiSystemManager.IsNewAppVersion(appVersion, baseVerion, baseVerion, userId);

            rtnObject.Data = ApiPCPManager.GetMembershipCardDetail(query, isNewPrototype);

            this.Context.Response.Write(jsonS.Serialize(rtnObject));
            SetApiLog(GetMethodName(methodName), userId, new
            {
                imageSize,
                userId
            }, new ApiReturnObject { Code = rtnObject.Code, Message = rtnObject.Message });

        }

        [WebMethod]
        public void MembershipCardOffer(string userId, int groupId)
        {
            const string methodName = "MembershipCardOffer";
            //設定輸出格式為json格式
            ContextBaseSetting();

            var jsonS = new JsonSerializer();

            ApiReturnObject rtnObject;
            //驗證使用者
            if (!CheckApiUser(userId, out rtnObject))
            {
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
                return;
            }

            //檢查會員是否已登入(登入非必須)
            string userName;
            CheckMemberSingin(out userName, out rtnObject);
            rtnObject.Code = ApiReturnCode.Success;
            rtnObject.Message = string.Empty;
            rtnObject.Data = ApiPCPManager.GetMembershipCardOffer(userName, groupId);

            this.Context.Response.Write(jsonS.Serialize(rtnObject));
            SetApiLog(GetMethodName(methodName), userId, new
            {
                userId
            }, new ApiReturnObject { Code = rtnObject.Code, Message = rtnObject.Message });
        }

        [WebMethod]
        public void MembershipCardDeposit(string userId, int groupId)
        {
            const string methodName = "MembershipCardDeposit";
            //設定輸出格式為json格式
            ContextBaseSetting();

            var jsonS = new JsonSerializer();

            ApiReturnObject rtnObject;
            //驗證使用者
            if (!CheckApiUser(userId, out rtnObject))
            {
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
                return;
            }

            //檢查會員是否已登入(登入非必須)
            string userName;
            CheckMemberSingin(out userName, out rtnObject);
            rtnObject.Code = ApiReturnCode.Success;
            rtnObject.Message = string.Empty;
            rtnObject.Data = ApiPCPManager.GetMembershipCardDeposit(userName, groupId);

            this.Context.Response.Write(jsonS.Serialize(rtnObject));
            SetApiLog(GetMethodName(methodName), userId, new
            {
                userId
            }, new ApiReturnObject { Code = rtnObject.Code, Message = rtnObject.Message });


        }


        [WebMethod]
        public void MembershipCardPoint(string userId, int groupId)
        {

            const string methodName = "MembershipCardPoint";
            //設定輸出格式為json格式
            ContextBaseSetting();

            var jsonS = new JsonSerializer();

            ApiReturnObject rtnObject;
            //驗證使用者
            if (!CheckApiUser(userId, out rtnObject))
            {
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
                return;
            }

            //檢查會員是否已登入(登入非必須)
            string userName;
            CheckMemberSingin(out userName, out rtnObject);
            rtnObject.Code = ApiReturnCode.Success;
            rtnObject.Message = string.Empty;
            rtnObject.Data = ApiPCPManager.GetMembershipCardPoint(userName, groupId);

            this.Context.Response.Write(jsonS.Serialize(rtnObject));
            SetApiLog(GetMethodName(methodName), userId, new
            {
                userId
            }, new ApiReturnObject { Code = rtnObject.Code, Message = rtnObject.Message });
        }



        #endregion


    }
}

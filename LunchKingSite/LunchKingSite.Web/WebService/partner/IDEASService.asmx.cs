﻿using log4net;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Component.API;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Models;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Component;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Transactions;
using System.Web.Services;
using PaymentType = LunchKingSite.Core.PaymentType;

namespace LunchKingSite.Web.WebService.partner
{
    /// <summary>
    /// 提供部分區域限定的檔次資料
    /// 比如雙北APP需要的針對各捷運站所需要的檔次資料
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
    [System.Web.Script.Services.ScriptService]
    public class IDEASService : BaseService
    {
        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        private static IOrderProvider op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
        private static IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        private static ILocationProvider lp = ProviderFactory.Instance().GetProvider<ILocationProvider>();
        private static IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        private static IItemProvider ip = ProviderFactory.Instance().GetProvider<IItemProvider>();
        private static ILog logger = LogManager.GetLogger("OrderFacade");
        private static Object CouponLock = new Object();

        /// <summary>
        /// 依據檔次或優惠的起始時間查詢資策會雙北APP需要的資訊
        /// </summary>
        /// <param name="userId">會員編號</param>
        [WebMethod]
        public void GetIDEASEventBaseData(string userId)
        {
            const string methodName = "GetIDEASEventBaseData";
            //設定輸出格式為json格式
            ContextBaseSetting();

            ApiIDEASReturnObject rtnObject = new ApiIDEASReturnObject();
            ApiReturnObject apiRtnObject;
            //驗證使用者
            if (!CheckApiUser(userId, out apiRtnObject))
            {
                rtnObject = new ApiIDEASReturnObject(apiRtnObject);
                this.Context.Response.Write(jsonSerializer.Serialize(rtnObject));
                SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
                return;
            }

            DateTime itemStartDate = DateTime.Now.Date;
            DateTime itemEndDate = itemStartDate.AddDays(2);
            rtnObject.data = ApiLocationDealManager.ApiIDEASEventBaseDataGet(itemStartDate, itemEndDate);


            var result = jsonSerializer.Serialize(rtnObject);
            this.Context.Response.ContentType = "application/json";
            this.Context.Response.Write(result);
            #region 不加入此region內的code的話，回傳的json後面會多出 {"d":null} 造成錯誤
            this.Context.Response.Flush(); // Sends all currently buffered output to the client.
            this.Context.Response.SuppressContent = true;  // Gets or sets a value indicating whether to send HTTP content to the client.
            this.Context.ApplicationInstance.CompleteRequest(); // Causes ASP.NET to bypass all events and filtering in the HTTP pipeline chain of execution and directly execute the EndRequest event.
            #endregion

            SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
        }

        [WebMethod]
        public void GetIDEASEventBaseDataByDate(string date, string userId)
        {
            const string methodName = "GetIDEASEventBaseDataByDate";
            //設定輸出格式為json格式
            ContextBaseSetting();

            ApiIDEASReturnObject rtnObject = new ApiIDEASReturnObject();
            ApiReturnObject apiRtnObject;
            //驗證使用者
            if (!CheckApiUser(userId, out apiRtnObject))
            {
                rtnObject = new ApiIDEASReturnObject(apiRtnObject);
                this.Context.Response.Write(jsonSerializer.Serialize(rtnObject));
                SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
                return;
            }

            DateTime inputDate;
            if (!ApiSystemManager.TryDateStringToDateTime(date, out inputDate))
            {
                rtnObject.code = ApiReturnCode.InputError;
                rtnObject.message = I18N.Phrase.ApiReturnCodeInputError;
                this.Context.Response.Write(jsonSerializer.Serialize(rtnObject));
                SetApiLog(GetMethodName(methodName), userId, new { date = date }, rtnObject);
                return;
            }

            DateTime itemEndDate = inputDate.AddDays(2);

            rtnObject.data = ApiLocationDealManager.ApiIDEASEventBaseDataGet(inputDate, itemEndDate);

            this.Context.Response.Write(jsonSerializer.Serialize(rtnObject));

            SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
        }

        /// <summary>
        /// 取得各機台輪播內容
        /// </summary>
        /// <param name="date">查詢輪播日期</param>
        /// <param name="userId"></param>
        [WebMethod]
        public void GetIDEASEventData(string date, string userId)
        {
            const string methodName = "GetIDEASEventData";
            //設定輸出格式為json格式
            ContextBaseSetting();

            ApiIDEASReturnObject rtnObject = new ApiIDEASReturnObject();
            ApiReturnObject apiRtnObject;
            //驗證使用者
            if (!CheckApiUser(userId, out apiRtnObject))
            {
                rtnObject = new ApiIDEASReturnObject(apiRtnObject);
                this.Context.Response.Write(jsonSerializer.Serialize(rtnObject));
                SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
                return;
            }

            DateTime inputDate;
            if (!ApiSystemManager.TryDateStringToDateTime(date, out inputDate))
            {
                rtnObject.code = ApiReturnCode.InputError;
                rtnObject.message = I18N.Phrase.ApiReturnCodeInputError;
                this.Context.Response.Write(jsonSerializer.Serialize(rtnObject));
                SetApiLog(GetMethodName(methodName), userId, new { date = date }, rtnObject);
                return;
            }

            rtnObject.data = ApiLocationDealManager.ApiIDEASEventDataGetList(inputDate);

            this.Context.Response.Write(jsonSerializer.Serialize(rtnObject));

            SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
        }

        /// <summary>
        /// 回傳目前銷售中商品的數量
        /// </summary>
        /// <param name="userId"></param>
        [WebMethod]
        public void GetIDEASSurplusQuantity(string userId)
        {
            const string methodName = "GetIDEASSurplusQuantity";
            //設定輸出格式為json格式
            ContextBaseSetting();

            ApiIDEASReturnObject rtnObject = new ApiIDEASReturnObject();
            ApiReturnObject apiRtnObject;
            //驗證使用者
            if (!CheckApiUser(userId, out apiRtnObject))
            {
                rtnObject = new ApiIDEASReturnObject(apiRtnObject);
                this.Context.Response.Write(jsonSerializer.Serialize(rtnObject));
                SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
                return;
            }

            rtnObject.data = ApiLocationDealManager.ApiIDEASSurplusQuantityDataGet();

            this.Context.Response.Write(jsonSerializer.Serialize(rtnObject));

            SetApiLog(GetMethodName(methodName), userId, null, new ApiReturnObject {Code = rtnObject.code, Message = rtnObject.message});
        }

        [WebMethod]
        public void SendSMS(string orderId, string mobile, string userId)
        {
            const string methodName = "SendSMS";
            //設定輸出格式為json格式
            ContextBaseSetting();

            ApiIDEASReturnObject rtnObject = new ApiIDEASReturnObject();
            ApiReturnObject apiRtnObject;
            //驗證使用者
            if (!CheckApiUser(userId, out apiRtnObject))
            {
                rtnObject = new ApiIDEASReturnObject(apiRtnObject);
                this.Context.Response.Write(jsonSerializer.Serialize(rtnObject));
                SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
                return;
            }

            rtnObject.data = SendSMSWork(orderId, mobile);

            this.Context.Response.Write(jsonSerializer.Serialize(rtnObject));

            SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
        }

        [WebMethod]
        public void IDEASPayment(string userId, string dtoString)
        {
            const string methodName = "IDEASPayment";
            //設定輸出格式為json格式
            ContextBaseSetting();

            ApiIDEASReturnObject rtnObject;
            ApiReturnObject apiRtnObject;
            //驗證使用者
            if (!CheckApiUser(userId, out apiRtnObject))
            {
                rtnObject = new ApiIDEASReturnObject(apiRtnObject);
                this.Context.Response.Write(jsonSerializer.Serialize(rtnObject));
                SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
                return;
            }

            ApiIDEASPaymentData paymentData = jsonSerializer.Deserialize<ApiIDEASPaymentData>(dtoString);
            PaymentDTO paymentDTO = MakePaymentDTOByIDEASPaymentData(paymentData);
            apiRtnObject = MakeOrder(paymentDTO, config.IDEASUserEmail, paymentData, ApiUserDeviceType.WebService);
            rtnObject = new ApiIDEASReturnObject(apiRtnObject);

            this.Context.Response.Write(jsonSerializer.Serialize(rtnObject));
        }

        [WebMethod]
        public void GetPromoPaymentDataJSon(string userId)
        {
            const string methodName = "GetPromoPaymentDataJSon";
            //設定輸出格式為json格式
            ContextBaseSetting();

            ApiReturnObject rtnObject;
            //驗證使用者
            if (!CheckApiUser(userId, out rtnObject))
            {
                this.Context.Response.Write(jsonSerializer.Serialize(rtnObject));
                SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
                return;
            }
            ApiIDEASPaymentData data = new ApiIDEASPaymentData();
            data.dealId = 10000688;
            data.quantity = 1;
            data.selectedStoreGuid = "c6f5b18d-b6ab-4d5f-87a8-7fb1fed4a852";
            data.itemOptions = new List<ApiIDEASPaymentOptionCategory>();
            var category = new ApiIDEASPaymentOptionCategory();
            category.itemQuantity = 1;
            var option = new ApiIDEASPaymentOptionItem();
            option.optionGuid = Guid.Empty.ToString();
            option.optionName = @"紅色";

            category.options.Add(option);

            data.itemOptions.Add(category);

            data.donationReceiptsType = DonationReceiptsType.DoNotContribute;
            data.creditcardNumber = "8888880000000006";
            data.creditcardSecurityCode = "036";
            data.creditcardExpireYear = "16";
            data.creditcardExpireMonth = "04";
            data.purchaserName = "tunintsai";
            data.purchaserAddress = "新北市三芝區錫板里14-17號";
            data.purchaserPhone = "0937901419";
            data.purchaserEMail = "tunintsai@gmail.com";
            data.writeAddress = "";
            data.writeZipCode = "";
            data.freight = 0;
            data.writeName = "";
            data.writePhone = "";
            data.invoiceType = ApiIDEASInvoiceType.DuplexInvoice;
            data.companyTitle = "";
            data.unifiedSerialNumber = "";
            data.invoiceBuyerName = "";
            data.invoiceBuyerAddress = "";

            rtnObject.Data = data;

            this.Context.Response.Write(jsonSerializer.Serialize(rtnObject));
        }

        [WebMethod]
        public void InvoiceRequestPaper(string userId, string orderId, string invoiceNumber, string writeName, string writeAddress)
        {
            const string methodName = "InvoiceRequestPaper";
            //設定輸出格式為json格式
            ContextBaseSetting();

            ApiReturnObject rtnObject;
            //驗證使用者
            if (!CheckApiUser(userId, out rtnObject))
            {
                this.Context.Response.Write(jsonSerializer.Serialize(rtnObject));
                SetApiLog(GetMethodName(methodName), userId, null, rtnObject);
                return;
            }
            rtnObject.Data = ApiLocationDealManager.EinvoiceRequestPaper(orderId, invoiceNumber, writeName, writeAddress);

            this.Context.Response.Write(jsonSerializer.Serialize(rtnObject));
        }

        private PaymentDTO MakePaymentDTOByIDEASPaymentData(ApiIDEASPaymentData paymentData)
        {
            IViewPponDeal deal = ViewPponDealManager.DefaultManager.ViewPponDealGetByUniqueId(paymentData.dealId);

            Guid selectStoreGuid;//選取的分店
            if (!Guid.TryParse(paymentData.selectedStoreGuid, out selectStoreGuid))
            {
                selectStoreGuid = Guid.Empty;
            }
            //先整理DeliveryInfo的資料
            PponDeliveryInfo pponDelivery = new PponDeliveryInfo();
            pponDelivery.WriteAddress = paymentData.writeAddress;
            pponDelivery.WriteZipCode = paymentData.writeZipCode;
            pponDelivery.Freight = paymentData.freight;
            pponDelivery.WriteName = paymentData.writeName;
            pponDelivery.Phone = paymentData.writePhone;
            pponDelivery.PayType = Core.PaymentType.Creditcard;//IDEAS 這邊一律為信用卡付款
            pponDelivery.Quantity = paymentData.quantity;
            pponDelivery.BonusPoints = 0;
            pponDelivery.PayEasyCash = 0;
            pponDelivery.SCash = 0;
            pponDelivery.Notes = string.Empty;
            pponDelivery.SellerType = (DepartmentTypes)deal.Department;
            pponDelivery.IsForFriend = false;
            pponDelivery.FriendName = string.Empty;
            pponDelivery.FriendEMail = string.Empty;
            pponDelivery.ToFriendNote = string.Empty;
            pponDelivery.DeliveryDateString = string.Empty;
            pponDelivery.DeliveryTimeString = string.Empty;
            pponDelivery.Genus = string.Empty;
            pponDelivery.MemberDeliveryID = -1;
            pponDelivery.WriteBuildingGuid = Guid.Empty;
            pponDelivery.WriteShortAddress = string.Empty;
            pponDelivery.DeliveryType = (DeliveryType)deal.DeliveryType;
            //pponDelivery.ReceiptsType = (DonationReceiptsType)paymentData.donationReceiptsType;
            #region 發票處理
            //依據目前設定判斷發票該如何處理
            InvoiceVersion version = InvoiceVersion.CarrierEra;
            if (version == InvoiceVersion.Old)
            {
                //舊版發票
                if (paymentData.invoiceType == ApiIDEASInvoiceType.DuplexInvoice)
                {
                    //二聯式-一律為電子發票
                    pponDelivery.InvoiceType = "2";
                    pponDelivery.ReceiptsType = DonationReceiptsType.None;
                    pponDelivery.CarrierType = CarrierType.None;//固定為紙本
                    pponDelivery.CarrierId = string.Empty;
                    pponDelivery.LoveCode = string.Empty;
                    pponDelivery.IsEinvoice = true; //電子發票
                }
                else
                {
                    //三聯式
                    pponDelivery.InvoiceType = "3";
                    pponDelivery.ReceiptsType = DonationReceiptsType.DoNotContribute;
                    pponDelivery.CarrierId = string.Empty;
                    pponDelivery.LoveCode = string.Empty;
                    //三聯式設定為紙本
                    pponDelivery.IsEinvoice = false;
                    //公司抬頭、統編、購買人與地址
                    pponDelivery.CompanyTitle = paymentData.companyTitle;
                    pponDelivery.UnifiedSerialNumber = paymentData.unifiedSerialNumber;
                    pponDelivery.InvoiceBuyerName = paymentData.invoiceBuyerName;
                    pponDelivery.InvoiceBuyerAddress = paymentData.invoiceBuyerAddress;

                }
            }
            else
            {
                //系統已轉換到新版發票
                if (paymentData.invoiceType == ApiIDEASInvoiceType.DuplexInvoice)
                {
                    //二聯式發票，判斷資料是否足夠
                    if (string.IsNullOrWhiteSpace(paymentData.invoiceBuyerName) ||
                        string.IsNullOrWhiteSpace(paymentData.invoiceBuyerAddress))
                    {
                        //沒有填購買人與地址
                        //採用會員載具
                        pponDelivery.InvoiceType = "2";
                        pponDelivery.ReceiptsType = DonationReceiptsType.None;
                        pponDelivery.CarrierType = CarrierType.Member; //固定為紙本
                        pponDelivery.CarrierId = string.Empty;
                        pponDelivery.LoveCode = string.Empty;
                        pponDelivery.IsEinvoice = true; //電子發票
                    }
                    else
                    {
                        //有足夠的資料，採紙本發票
                        pponDelivery.InvoiceType = "2";
                        pponDelivery.ReceiptsType = DonationReceiptsType.None;
                        pponDelivery.CarrierType = CarrierType.None;//固定為紙本
                        pponDelivery.CarrierId = string.Empty;
                        pponDelivery.LoveCode = string.Empty;
                        pponDelivery.IsEinvoice = false; //電子發票
                    }
                }
                else
                {
                    //三聯式
                    pponDelivery.InvoiceType = "3";
                    pponDelivery.ReceiptsType = DonationReceiptsType.DoNotContribute;
                    pponDelivery.CarrierId = string.Empty;
                    pponDelivery.LoveCode = string.Empty;
                    //三聯式設定為紙本
                    pponDelivery.IsEinvoice = false;
                    //公司抬頭、統編、購買人與地址
                    pponDelivery.CompanyTitle = paymentData.companyTitle;
                    pponDelivery.UnifiedSerialNumber = paymentData.unifiedSerialNumber;
                    pponDelivery.InvoiceBuyerName = paymentData.invoiceBuyerName;
                    pponDelivery.InvoiceBuyerAddress = paymentData.invoiceBuyerAddress;
                }
            }

            pponDelivery.Version = version;
            #endregion 發票處理

            pponDelivery.StoreGuid = selectStoreGuid;
            pponDelivery.DiscountCode = paymentData.discountCode;
            pponDelivery.EntrustSell = EntrustSellType.No;

            PaymentDTO rtn = new PaymentDTO();
            rtn.TicketId = string.Empty;
            rtn.TransactionId = string.Empty;
            rtn.OrderGuid = Guid.Empty;
            rtn.SessionId = string.Empty;
            rtn.PezId = string.Empty;
            rtn.BusinessHourGuid = deal.BusinessHourGuid;
            rtn.DeliveryInfo = pponDelivery;
            rtn.DeliveryCharge = Convert.ToInt32(paymentData.freight);
            rtn.Quantity = paymentData.quantity;
            rtn.Amount = 0;//由後續程式計算
            rtn.BCash = 0;
            rtn.SCash = 0;
            rtn.PCash = 0;
            rtn.DiscountCode = paymentData.discountCode;
            rtn.SelectedStoreGuid = selectStoreGuid;

            #region 整頓選項內容產生傳入paymentDTO的字串

            string itemOptionString = string.Empty;
            List<string> rtnCateList = new List<string>();
            foreach (var cate in paymentData.itemOptions)
            {
                List<string> rtnOptionList = new List<string>();
                foreach (var option in cate.options)
                {
                    rtnOptionList.Add(string.Format("{0}|^|{1}", option.optionName, option.optionGuid));
                }
                rtnCateList.Add(string.Format("{0}|#|{1}", string.Join(",", rtnOptionList), cate.itemQuantity));
            }
            itemOptionString = string.Join("||", rtnCateList);
            if (string.IsNullOrWhiteSpace(itemOptionString))
            {
                itemOptionString = "|#|" + paymentData.quantity;
            }

            rtn.ItemOptions = itemOptionString;

            #endregion 整頓選項內容產生傳入paymentDTO的字串

            rtn.DonationReceiptsType = pponDelivery.ReceiptsType;//DonationReceiptsType.DoNotContribute;
            rtn.APIProvider = 0;

            rtn.IsPaidByATM = false;
            rtn.CreditcardNumber = paymentData.creditcardNumber;
            rtn.CreditcardSecurityCode = paymentData.creditcardSecurityCode;
            rtn.CreditcardExpireYear = paymentData.creditcardExpireYear;
            rtn.CreditcardExpireMonth = paymentData.creditcardExpireMonth;
            rtn.ATMAccount = string.Empty;
            //rtn.ResultPageType = PaymentResultPageType.Initial;  //不設定
            //購買人資訊
            rtn.AddressInfo = string.Empty;
            rtn.ReferenceId = string.Empty;

            return rtn;
        }

        #region private methods

        /// <summary>
        /// 發送簡訊憑證
        /// </summary>
        /// <param name="orderId">要發送的訂單</param>
        /// <param name="mobile">目標手機號碼</param>
        /// <returns>回傳發送狀況的物件</returns>
        private ApiIDEASSendSMSReply SendSMSWork(string orderId, string mobile)
        {
            ApiIDEASSendSMSReply reply = new ApiIDEASSendSMSReply();

            Order order = op.OrderGet(Order.Columns.OrderId, orderId);
            if (!order.IsLoaded)
            {
                reply.Status = ApiIDEASSendSMSReplyStatus.OrderNotExists;
                reply.Message = "訂單不存在。";
                return reply;
            }
            //檢查訂單是否為資策會訂單
            if (order.UserId != ApiLocationDealManager.IDEASMember.UniqueId)
            {
                //非資策會訂單
                reply.Status = ApiIDEASSendSMSReplyStatus.OrderNotExists;
                reply.Message = "訂單不存在。";
                return reply;
            }
            if ((order.OrderStatus & (int)OrderStatus.Cancel) > 0)
            {
                //已退貨
                reply.Status = ApiIDEASSendSMSReplyStatus.NoCouponCanSend;
                reply.Message = "此訂單已退貨。";
                return reply;
            }

            //查詢所有未核銷憑證

            #region 整理憑證資料

            List<string> sequenceNumList = new List<string>();
            ViewCouponListSequenceCollection couponList = mp.GetCouponListSequenceListByOid(order.Guid, OrderClassification.LkSite);

            CashTrustLogCollection cashTrustLogList = mp.CashTrustLogGetListByOrderGuid(order.Guid, OrderClassification.LkSite);
            Guid bid = Guid.Empty;
            if (cashTrustLogList.Count > 0)
            {
                var firstCoupon = cashTrustLogList.First();
                if (firstCoupon.BusinessHourGuid == null)
                {
                    reply.Status = ApiIDEASSendSMSReplyStatus.OrderNotExists;
                    reply.Message = "訂單資料異常。";
                    return reply;
                }
                else
                {
                    bid = firstCoupon.BusinessHourGuid.Value;
                }
            }
            else
            {
                reply.Status = ApiIDEASSendSMSReplyStatus.NoCouponCanSend;
                reply.Message = "無可發送的憑證資料。";
                return reply;
            }
            ViewDealPropertyBusinessHourContent viewDealProperty = pp.ViewDealPropertyBusinessHourContentGetByBid(bid);
            //結檔時的已售出份數
            int slug;
            if (!int.TryParse(viewDealProperty.Slug, out slug))
            {
                slug = 0;
            }

            //沒有cashTrustLog資料(status == -1)，或 信託準備中、已信託，則可以傳送簡訊(其他的type為退貨或已核銷等)
            List<int> effectStatus = new List<int> { -1, (int)TrustStatus.Initial, (int)TrustStatus.Trusted };

            DateTime now = DateTime.Now;
            foreach (ViewCouponListSequence coupon in couponList)
            {
                //已之前宣告的 effectStatus 檢查status是否可以進行簡訊發送
                if (effectStatus.Any(x => x == coupon.Status))
                {
                    if (viewDealProperty.BusinessHourOrderTimeE < now)
                    {
                        //已結檔，判斷是否達門檻且憑證未過期
                        if ((slug >= viewDealProperty.BusinessHourOrderMinimum) &&
                            (!PponFacade.IsPastFinalExpireDate(coupon)))
                        {
                            //憑證未過期
                            sequenceNumList.Add(coupon.SequenceNumber);
                            continue;
                        }
                    }
                    else
                    {
                        sequenceNumList.Add(coupon.SequenceNumber);
                        continue;
                    }
                }
            }

            #endregion 整理憑證資料

            if (sequenceNumList.Count == 0)
            {
                reply.Status = ApiIDEASSendSMSReplyStatus.NoCouponCanSend;
                reply.Message = "無可發送的憑證資料。";
                return reply;
            }

            int sendCount = OrderUtility.SendOrderPponCouponSMS(order.Guid, sequenceNumList, mobile, ApiLocationDealManager.IDEASMember.UniqueId, false);

            reply.Status = ApiIDEASSendSMSReplyStatus.Success;
            reply.Message = "";
            reply.Count = sendCount;
            return reply;
        }

        private ApiReturnObject MakeOrder(PaymentDTO paymentDTO, String userName, ApiIDEASPaymentData ideasData, ApiUserDeviceType apiUserDeviceType = ApiUserDeviceType.WebService)
        {
            //付款回傳用的資料
            ApiIDEASPaymentReply paymentReply = new ApiIDEASPaymentReply();
            ApiReturnObject rtnObject = new ApiReturnObject() { Code = ApiReturnCode.Success };
            rtnObject.Data = paymentReply;

            int userId = MemberFacade.GetUniqueId(userName);
            if (userId == 0)
            {
                logger.Error(String.Format("user {0} not found", userName));

                paymentReply.paymentStatus = ApiIDEASPaymentReplyStatus.Error;
                paymentReply.message = "查無會員資料。";

                return rtnObject;
            }

            #region Check Order

            if (paymentDTO.PCash > 0)
            {
                if (!CheckRemaining(paymentDTO.PezId, paymentDTO.PCash))
                {
                    paymentReply.paymentStatus = ApiIDEASPaymentReplyStatus.PCashError;
                    paymentReply.message = "Payeasy購物金不足";

                    return rtnObject;
                }
            }
            IViewPponDeal theDeal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(paymentDTO.BusinessHourGuid);

            // check branch 非0元檔次，檢查分店編號是否正確            
            // 通用券檔次無須檢查分店
            bool noRestrictedStore = Helper.IsFlagSet(theDeal.BusinessHourStatus, BusinessHourStatus.NoRestrictedStore);
            bool isItem = theDeal.DeliveryType.GetValueOrDefault() == (int)DeliveryType.ToHouse;
            if ((theDeal.ItemPrice != 0) && (!CheckBranch(paymentDTO.BusinessHourGuid, isItem, paymentDTO.SelectedStoreGuid, paymentDTO.Quantity, noRestrictedStore,theDeal.SaleMultipleBase)))
            {
                paymentReply.message = "請選擇還有數量的分店";
                paymentReply.paymentStatus = ApiIDEASPaymentReplyStatus.SoldOut;

                return rtnObject;
            }

            // check option;
            if (!checkOptions(paymentDTO.ItemOptions))
            {
                paymentReply.message = "請選擇還有數量的選項";
                paymentReply.paymentStatus = ApiIDEASPaymentReplyStatus.SoldOut;

                return rtnObject;
            }

            // add for empty address
            if (isItem)
            {
                if (string.IsNullOrWhiteSpace(paymentDTO.DeliveryInfo.WriteAddress) || paymentDTO.DeliveryInfo.WriteAddress.IndexOf("請選擇") >= 0)
                {
                    paymentReply.message = "請選擇收件人地址";
                    paymentReply.paymentStatus = ApiIDEASPaymentReplyStatus.Error;

                    return rtnObject;
                }
            }

            //使用17Life購物金付全額仍需紀錄invoice_mode (invoice_mode不為0)
            int sc_price = Convert.ToInt32(theDeal.ItemPrice) + paymentDTO.SCash;

            if (sc_price == 0)
            {
                paymentDTO.DonationReceiptsType = DonationReceiptsType.None;//0
            }

            #endregion Check Order

            #region 發票問題應急修正

            //針對APP傳入參數的錯誤，先增加的修改，相容於正確判斷後傳來的參數。
            if ((DonationReceiptsType)paymentDTO.DeliveryInfo.ReceiptsType == DonationReceiptsType.None)
            {
                paymentDTO.DeliveryInfo.ReceiptsType = DonationReceiptsType.DoNotContribute;
                if (string.IsNullOrWhiteSpace(paymentDTO.DeliveryInfo.CompanyTitle))
                {
                    paymentDTO.DeliveryInfo.InvoiceType = "2";
                    paymentDTO.DeliveryInfo.IsEinvoice = true;
                }
                else
                {
                    paymentDTO.DeliveryInfo.InvoiceType = "3";
                    paymentDTO.DeliveryInfo.IsEinvoice = false;
                }
            }

            #endregion 發票問題應急修正

            #region CheckOut

            paymentDTO.TicketId = OrderFacade.MakeRegularTicketId(paymentDTO.SessionId, paymentDTO.BusinessHourGuid) + "_" + Path.GetRandomFileName().Replace(".", "");
            paymentDTO.APIProvider = (PaymentAPIProvider)Enum.Parse(typeof(PaymentAPIProvider), config.CreditCardAPIProvider);

            string strResult = CheckData(theDeal, (int)(Math.Ceiling((double)(paymentDTO.Quantity) / (theDeal.ComboPackCount ?? 1))), paymentDTO.BCash, paymentDTO.SCash, paymentDTO.DiscountCode, userName, paymentDTO.DeliveryInfo.Freight
                , (bool)theDeal.IsDailyRestriction);
            if (string.IsNullOrWhiteSpace(strResult))
            {
                string infoStr = jsonSerializer.Serialize(paymentDTO.DeliveryInfo);

                List<ItemEntry> ies = PaymentFacade.ProcessSubItemsAndAccessoriesWithComboPack(
                    paymentDTO.ItemOptions, paymentDTO.TicketId, theDeal, paymentDTO.SelectedStoreGuid, infoStr);
                
                try
                {
                    OrderFacade.PutItemToCart(paymentDTO.TicketId, ies);

                    #region 把資料放到暫存Session中

                    TempSession ts = new TempSession();
                    ts.SessionId = paymentDTO.TicketId;
                    ts.Name = "pponContent";
                    ts.ValueX = infoStr;
                    pp.TempSessionSetForUpdate(ts);

                    #endregion 把資料放到暫存Session中
                }
                catch (Exception ex)
                {
                    logger.Error(String.Format("Something wrong while put items to cart : ticketId={0}, bid={1}", paymentDTO.TicketId,
                            paymentDTO.BusinessHourGuid), ex);

                    paymentReply.message = @"交易失敗";
                    paymentReply.paymentStatus = ApiIDEASPaymentReplyStatus.Error;
                    return rtnObject;
                }
            }
            else
            {
                paymentReply.message = strResult;
                paymentReply.paymentStatus = ApiIDEASPaymentReplyStatus.Error;
                return rtnObject;
            }

            #endregion CheckOut

            #region Make Order

            TempSessionCollection tsc = pp.TempSessionGetList(paymentDTO.TicketId);

            if (tsc.Count <= 0)
            {
                logger.Error("Temp Session Missing.");

                paymentReply.message = "交易失敗！";
                paymentReply.paymentStatus = ApiIDEASPaymentReplyStatus.Error;
                return rtnObject;
            }

            paymentDTO.TransactionId = PaymentFacade.GetTransactionId();

            #region order amount

            var discountCodeObject = new DiscountCode();
            //discountCode實際扣抵金額
            var discountAmount = 0;
            if (!string.IsNullOrWhiteSpace(paymentDTO.DeliveryInfo.DiscountCode))
            {
                discountCodeObject = op.DiscountCodeGetByCodeWithoutUseTime(paymentDTO.DeliveryInfo.DiscountCode);

                if (discountCodeObject.IsLoaded)
                {
                    discountAmount = discountCodeObject.Amount == null ? 0 : (int) discountCodeObject.Amount.Value;
                    }
                    else
                    {
                    paymentReply.message = "折價券已達使用數量上限！";
                    paymentReply.paymentStatus = ApiIDEASPaymentReplyStatus.Error;
                    return rtnObject;
                }
            }

            //訂單總金額
            var orderTotalPay = 0;
            paymentDTO.Amount = 0;
            ShoppingCartCollection scc = op.ShoppingCartGetList(ShoppingCart.Columns.TicketId, paymentDTO.TicketId);
            foreach (ShoppingCart sc in scc)
            {
                int x = (int)Math.Round(sc.ItemUnitPrice * sc.ItemQuantity);
                paymentDTO.Amount += x;
                orderTotalPay += x;
            }

            paymentDTO.DeliveryCharge = SetDeliveryCharge(paymentDTO.Amount, theDeal);
            paymentDTO.Amount += paymentDTO.DeliveryCharge - paymentDTO.DeliveryInfo.BonusPoints - paymentDTO.DeliveryInfo.PayEasyCash - (int)paymentDTO.DeliveryInfo.SCash - discountAmount;
            orderTotalPay += paymentDTO.DeliveryCharge;
            //若扣除付款項目後，金額小於0，則視為0且調整discount實際扣抵金額
            if (paymentDTO.Amount < 0)
            {
                discountAmount = discountAmount + paymentDTO.Amount;
                paymentDTO.Amount = 0;
            }

            #endregion order amount

            #region New Version Order Start

            DeliveryInfo di = new DeliveryInfo();
            di.DeliveryCharge = paymentDTO.DeliveryCharge;
            OrderFromType orderfromtype;
            switch (apiUserDeviceType)
            {
                case ApiUserDeviceType.WebService:
                    orderfromtype = OrderFromType.ByWebService;
                    break;

                case ApiUserDeviceType.Android:
                    orderfromtype = OrderFromType.ByAndroid;
                    break;

                case ApiUserDeviceType.IOS:
                    orderfromtype = OrderFromType.ByIOS;
                    break;

                case ApiUserDeviceType.WinPhone:
                    orderfromtype = OrderFromType.ByWinPhone;
                    break;

                default:
                    orderfromtype = OrderFromType.ByWebService;
                    break;
            }

            string userMemo = paymentDTO.DeliveryInfo.DeliveryDateString + paymentDTO.DeliveryInfo.DeliveryTimeString + paymentDTO.DeliveryInfo.Notes;

            if (theDeal.DeliveryType.HasValue && int.Equals((int)DeliveryType.ToHouse, theDeal.DeliveryType.Value)) //好康到店到府消費方式判斷；PponItem=ToHouse
            {
                string deliveryAddress = paymentDTO.DeliveryInfo.WriteAddress;
                if (string.IsNullOrWhiteSpace(paymentDTO.DeliveryInfo.WriteZipCode))
                {
                    if (paymentDTO.DeliveryInfo.WriteBuildingGuid != Guid.Empty)
                    {
                        ViewBuildingCity buildingCity = lp.ViewBuildingCityGet(paymentDTO.DeliveryInfo.WriteBuildingGuid);
                        if (buildingCity.IsLoaded)
                        {
                            deliveryAddress = buildingCity.ZipCode + " " + deliveryAddress;
                        }
                    }
                }
                else
                {
                    deliveryAddress = paymentDTO.DeliveryInfo.WriteZipCode + " " + deliveryAddress;
                }

                di.CustomerName = paymentDTO.DeliveryInfo.WriteName;
                di.CustomerMobile = paymentDTO.DeliveryInfo.Phone;
                di.DeliveryAddress = deliveryAddress;
            }
            else
            {
                di.DeliveryAddress = PponBuyFacade.GetOrderDesc(paymentDTO.DeliveryInfo);
            }

            // make order
            paymentDTO.OrderGuid = OrderFacade.MakeOrder(userName, paymentDTO.TicketId, DateTime.Now, userMemo, di,
                theDeal, orderfromtype);

            Order o = op.OrderGet(paymentDTO.OrderGuid);

            o.OrderMemo = ((int)paymentDTO.DeliveryInfo.ReceiptsType.GetValueOrDefault()).ToString() + "|" + paymentDTO.DeliveryInfo.InvoiceType + "|" + paymentDTO.DeliveryInfo.UnifiedSerialNumber + "|" + paymentDTO.DeliveryInfo.CompanyTitle;
            op.OrderSet(o);

            #region 紀錄CompanyUserOrder

            CompanyUserOrder userOrder = new CompanyUserOrder();
            userOrder.CompanyUserId = userId;
            userOrder.OrderGuid = o.Guid;
            userOrder.PurchaserEmail = ideasData.purchaserEMail;
            userOrder.PurchaserName = ideasData.purchaserName;
            userOrder.PurchaserPhone = ideasData.purchaserPhone;
            userOrder.PurchaserAddress = ideasData.purchaserAddress;
            op.CompanyUserOrderSet(userOrder);

            #endregion 紀錄CompanyUserOrder

            #endregion New Version Order Start

            #region Paying by 17Life bonus cash

            if (paymentDTO.DeliveryInfo.BonusPoints > 0)
            {
                using (var tx = TransactionScopeBuilder.CreateReadCommitted())
                {
                    decimal bonusCash = paymentDTO.DeliveryInfo.BonusPoints;
                    string action = "折抵:" + (theDeal.EventName.Length <= 30 ? theDeal.EventName : (theDeal.EventName.Substring(0, 30) + "..."));
                    //pt 2.3 bcash
                    MemberFacade.DeductibleMemberPromotionDeposit(o.UserId, Convert.ToDouble(bonusCash * 10), action,
                        BonusTransactionType.OrderAmtRedeeming, o.Guid, MemberFacade.GetUserName(o.UserId), false, false);
                    //pt 1.3 bcash
                    OrderFacade.MakeTransaction(paymentDTO.TransactionId, PaymentType.BonusPoint, paymentDTO.DeliveryInfo.BonusPoints,
                        (DepartmentTypes)theDeal.Department, userName, o.Guid, paymentDTO.PezId);
                    tx.Complete();
                }
                //means paid all by bonus cash
                if (paymentDTO.Amount + paymentDTO.DeliveryInfo.PayEasyCash + paymentDTO.DeliveryInfo.SCash + discountAmount == 0)
                {
                    return MakePayment(paymentDTO, userName);
                }
            }

            #endregion Paying by 17Life bonus cash

            #region Check PCash

            if (paymentDTO.DeliveryInfo.PayEasyCash > 0)
            {
                //check if one pez member do have enough PCash or redirect to Pay.aspx
                if (PCashWorker.CheckUserRemaining(paymentDTO.PezId) >= paymentDTO.DeliveryInfo.PayEasyCash)
                {
                    //僅建立transation 不預先扣款
                    int status = Helper.SetPaymentTransactionPhase(Helper.SetPaymentTransactionDepartmentTypes(0, (DepartmentTypes)theDeal.Department), PayTransPhase.Created);
                    PaymentFacade.NewTransaction(paymentDTO.TransactionId, o.Guid, PaymentType.PCash, paymentDTO.DeliveryInfo.PayEasyCash, string.Empty, PayTransType.Authorization, DateTime.Now, userName, string.Empty, status, PayTransResponseType.OK);

                    if (paymentDTO.Amount + paymentDTO.DeliveryInfo.BonusPoints + discountAmount == 0) //means paid all by PCash
                    {
                        return MakePayment(paymentDTO, userName);
                    }
                }
                else
                {
                    paymentReply.message = "Payeasy購物金不足";
                    paymentReply.paymentStatus = ApiIDEASPaymentReplyStatus.PCashError;

                    return rtnObject;
                }
            }

            #endregion Check PCash

            #region 17購物金折抵

            if (paymentDTO.DeliveryInfo.SCash > 0)
            {
                OrderFacade.MakeTransaction(paymentDTO.TransactionId, PaymentType.SCash, int.Parse(paymentDTO.DeliveryInfo.SCash.ToString("F0")),
                    (DepartmentTypes)theDeal.Department, userName, o.Guid, paymentDTO.PezId);
            }

            #endregion 17購物金折抵

            #region DiscountCode

            if (!string.IsNullOrWhiteSpace(paymentDTO.DeliveryInfo.DiscountCode))
            {
                if ((discountCodeObject.IsLoaded) && (discountCodeObject.Amount != null) && (discountAmount > 0))
                {
                    OrderFacade.MakeTransaction(paymentDTO.TransactionId, PaymentType.DiscountCode, discountAmount, (DepartmentTypes)theDeal.Department,
                        userName, o.Guid, paymentDTO.PezId);
                    try
                    {
                        var updateCount = op.DiscountCodeUpdateStatusSet((int)discountCodeObject.CampaignId, discountCodeObject.Code, discountAmount, userId, o.Guid, orderTotalPay, paymentDTO.Amount);
                        
                        if (updateCount == 0)
                        {
                            paymentReply.message = "折價券已達使用數量上限";
                            paymentReply.paymentStatus = ApiIDEASPaymentReplyStatus.DiscountCodeError;
                            return rtnObject;
                        }
                        else if (updateCount > 1)
                        {
                            //找無折價券或是已使用完畢
                            logger.Error(string.Format("折價券更新使用狀態ERROR：=>IDEASService.asmx.cs，更新{0}筆異常，UseTime:{1},UseAmount:{2},UseId:{3},OrderGuid:{4},OrderAmount{5},OrderCost:{6}", updateCount, discountCodeObject.Code, discountAmount, userId, o.Guid, orderTotalPay, paymentDTO.Amount));
                        }
                    }
                    catch (Exception ex)
                    {
                        logger.Error(string.Format("折價券更新使用狀態ERROR：IDEASService.asmx.cs=>UseTime:{0},UseAmount:{1},UseId:{2},OrderGuid:{3},OrderAmount{4},OrderCost:{5}", discountCodeObject.Code, discountAmount, userId, o.Guid, orderTotalPay, paymentDTO.Amount), ex);
                    }
                }
            }

            #endregion DiscountCode

            //有金額或好康設定為0元好康
            if ((paymentDTO.Amount > 0) || (theDeal.ItemPrice == 0))
            {
                if ((config.BypassPaymentProcess) || (theDeal.ItemPrice == 0))
                {
                    #region For test purpose

                    OrderFacade.MakeTransaction(paymentDTO.TransactionId, PaymentType.ByPass, paymentDTO.Amount, (DepartmentTypes)theDeal.Department,
                        userName, o.Guid, paymentDTO.PezId);

                    #endregion For test purpose
                }
                else
                {
                    if (paymentDTO.IsPaidByATM)
                    {
                        PaymentTransaction ptAtm = OrderFacade.MakeTransaction(paymentDTO.TransactionId, PaymentType.ATM,
                            paymentDTO.Amount, (DepartmentTypes)theDeal.Department, userName, o.Guid, paymentDTO.PezId);
                        if (!string.IsNullOrWhiteSpace(paymentDTO.ATMAccount))
                        {
                            CtAtm ca = op.CtAtmGetByVirtualAccount(paymentDTO.ATMAccount);
                            ca.Status = (int)AtmStatus.Expired;
                            op.CtAtmSet(ca);
                        }

                        paymentDTO.ATMAccount = PaymentFacade.GetAtmAccount(ptAtm.TransId, userId, paymentDTO.Amount, theDeal.BusinessHourGuid,
                            o.OrderId, o.Guid);
                    }
                    else
                    {
                        OrderFacade.MakeTransaction(paymentDTO.TransactionId, PaymentType.Creditcard,
                            paymentDTO.Amount, (DepartmentTypes)theDeal.Department, userName, o.Guid, paymentDTO.PezId);
                    }
                }

                return MakePayment(paymentDTO, userName);
            }
            else
            {
                //means paid by the combination of PCash and bonus cash
                return MakePayment(paymentDTO, userName);
            }

            #endregion Make Order
        }

        private ApiReturnObject MakePayment(PaymentDTO paymentDTO, string userName)
        {
            ApiIDEASPaymentReply paymentReply = new ApiIDEASPaymentReply();

            ApiReturnObject rtnObject = new ApiReturnObject() { Code = ApiReturnCode.Success };
            rtnObject.Data = paymentReply;

            //string userName = HttpContext.Current.User.Identity.Name;
            int userId = MemberFacade.GetUniqueId(userName);

            if (string.IsNullOrWhiteSpace(userName))
            {
                rtnObject.Code = ApiReturnCode.UserNoSignIn;
                paymentReply.paymentStatus = ApiIDEASPaymentReplyStatus.ParameterError;
                return rtnObject;
            }
            else
            {
                // 避免使用者在交易過程中改變自己的 email，導致之後在用 email 反查 unique id 時查不到資料填 0
                if (int.Equals(0, userId))
                {
                    logger.Error("Can't find unique id. email = " + userName);

                    paymentReply.message = "交易失敗！";
                    paymentReply.paymentStatus = ApiIDEASPaymentReplyStatus.ParameterError;
                    return rtnObject;
                }

                if (!string.IsNullOrWhiteSpace(paymentDTO.TransactionId))
                {
                    if (paymentDTO.Amount > 0)
                    {
                        #region pay by credit card

                        if (!paymentDTO.IsPaidByATM)
                        {
                            Regex rex = new Regex(@"\D");
                            if (!int.Equals(16, paymentDTO.CreditcardNumber.Length) ||
                                !int.Equals(2, paymentDTO.CreditcardExpireYear.Length) ||
                                !int.Equals(2, paymentDTO.CreditcardExpireMonth.Length) ||
                                rex.IsMatch(paymentDTO.CreditcardNumber) ||
                                rex.IsMatch(paymentDTO.CreditcardExpireYear) ||
                                rex.IsMatch(paymentDTO.CreditcardExpireMonth))
                            {
                                paymentReply.message = "卡號錯誤!!";
                                paymentReply.paymentStatus = ApiIDEASPaymentReplyStatus.ParameterError;
                                return rtnObject;
                            }
                            else
                            {
                                CreditCardAuthResult result = CreditCardUtility.Authenticate(
                                    new CreditCardAuthObject()
                                    {
                                        TransactionId = paymentDTO.TransactionId,
                                        Amount = paymentDTO.Amount,
                                        CardNumber = paymentDTO.CreditcardNumber,
                                        ExpireYear = paymentDTO.CreditcardExpireYear,
                                        ExpireMonth = paymentDTO.CreditcardExpireMonth,
                                        SecurityCode = paymentDTO.CreditcardSecurityCode,
                                        Description = paymentDTO.TicketId.Split('_')[1]
                                    }, paymentDTO.OrderGuid, userName, OrderClassification.LkSite, CreditCardOrderSource.Ppon);

                                CheckResult(paymentDTO, result);
                            }
                        }

                        #endregion pay by credit card
                    }

                    PaymentTransactionCollection ptc = op.PaymentTransactionGetListByTransIdAndTransType(paymentDTO.TransactionId, PayTransType.Authorization);
                    IEnumerable<PaymentTransaction> ptCreditcardCol = from i in ptc where (i.PaymentType == (int)PaymentType.Creditcard) select i;
                    PaymentTransaction ptCreditcard = (ptCreditcardCol.Count() > 0) ? ptCreditcardCol.First() : null;
                    IEnumerable<PaymentTransaction> ptScashCol = from i in ptc where (i.PaymentType == (int)PaymentType.SCash) select i;
                    PaymentTransaction ptScash = (ptScashCol.Count() > 0) ? ptScashCol.First() : null;
                    IEnumerable<PaymentTransaction> ptPcashCol = from i in ptc where (i.PaymentType == (int)PaymentType.PCash) select i;
                    PaymentTransaction ptPcash = (ptPcashCol.Count() > 0) ? ptPcashCol.First() : null;
                    IEnumerable<PaymentTransaction> ptDiscountCol = from i in ptc where (i.PaymentType == (int)PaymentType.DiscountCode) select i;
                    PaymentTransaction ptDiscount = (ptDiscountCol.Count() > 0) ? ptDiscountCol.First() : null;
                    IEnumerable<PaymentTransaction> ptATMCol = from i in ptc where (i.PaymentType == (int)PaymentType.ATM) select i;
                    PaymentTransaction ptATM = (ptATMCol.Count() > 0) ? ptATMCol.First() : null;

                    #region paying by pcash

                    if (paymentDTO.DeliveryInfo.PayEasyCash > 0 &&
                        (ptCreditcard == null || (ptCreditcard != null &&
                        Enum.Equals(PayTransResponseType.OK, (PayTransResponseType)ptCreditcard.Result))))
                    {
                        //check if one pez member do have enough PCash
                        if (PCashWorker.CheckUserRemaining(paymentDTO.PezId) >= paymentDTO.DeliveryInfo.PayEasyCash)
                        {
                            ptc.Add(ptPcash);
                        }
                        else
                        {
                            paymentReply.message = "卡號錯誤!!";
                            paymentReply.paymentStatus = ApiIDEASPaymentReplyStatus.PCashError;
                            return rtnObject;
                        }
                    }

                    #endregion paying by pcash

                    PayTransResponseType theResponseType = PayTransResponseType.OK;
                    PaymentType errorPaymentType = PaymentType.PCash;
                    List<PaymentType> paymentTypes = new List<PaymentType>();

                    foreach (var pt in ptc)
                    {
                        paymentTypes.Add((PaymentType)pt.PaymentType);

                        if (pt.Result != (int)PayTransResponseType.OK)
                        {
                            theResponseType = PayTransResponseType.GenericError;
                            errorPaymentType = (PaymentType)pt.PaymentType;
                        }
                    }

                    if (theResponseType == PayTransResponseType.OK)
                    {
                        IViewPponDeal theDeal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(paymentDTO.BusinessHourGuid);
                        if (theDeal.DealIsOrderable())
                        {
                            #region 檢查pcash transation

                            string authCode = string.Empty;
                            if (ptPcash != null)
                            {
                                //如扣款不成功
                                if (!PponBuyFacade.GetPcashAuthCode(paymentDTO.PezId, paymentDTO.TransactionId, ptPcash.Amount, (DepartmentTypes)theDeal.Department, out authCode))
                                {
                                    //更新transation紀錄 並刪除購物車和session 顯示pcash扣款失敗頁面
                                    PaymentFacade.UpdateTransaction(ptPcash.TransId, Guid.Empty, PaymentType.PCash, ptPcash.Amount, ptPcash.AuthCode,
                                        PayTransType.Authorization, DateTime.Now, ptPcash.Message,
                                        Helper.SetPaymentTransactionPhase(Helper.SetPaymentTransactionDepartmentTypes(0, (DepartmentTypes)theDeal.Department), PayTransPhase.Failed),
                                        (int)PayTransResponseType.GenericError);

                                    OrderFacade.DeleteItemInCart(paymentDTO.TicketId);

                                    pp.TempSessionDelete(paymentDTO.TicketId);
                                    paymentDTO.ResultPageType = (errorPaymentType == PaymentType.PCash) ? PaymentResultPageType.PcashFailed : PaymentResultPageType.CreditcardFailed;

                                    paymentReply.message = "扣款失敗。";
                                    paymentReply.paymentStatus = ApiIDEASPaymentReplyStatus.PCashError;

                                    return rtnObject;
                                }
                                else
                                {
                                    PaymentFacade.UpdateTransaction(ptPcash.TransId, Guid.Empty, PaymentType.PCash, ptPcash.Amount, authCode,
                                        PayTransType.Authorization, DateTime.Now, ptPcash.Message,
                                        Helper.SetPaymentTransactionPhase(Helper.SetPaymentTransactionDepartmentTypes(0, (DepartmentTypes)theDeal.Department),
                                        PayTransPhase.Successful), (int)PayTransResponseType.OK);
                                }
                            }

                            #endregion 檢查pcash transation

                            #region Transaction Scope

                            bool orderOK = false;
                            Order o = op.OrderGet(ptc.First().OrderGuid.Value);
                            OrderDetailCollection odCol = op.OrderDetailGetList(1, theDeal.MaxItemCount ?? 10, string.Empty, o.Guid, OrderDetailTypes.Regular);
                            int atmAmount = ptATM == null ? 0 : (int)ptATM.Amount;
                            int discountAmt = ptDiscount == null ? 0 : (int)ptDiscount.Amount;
                            int trustCreditCardAmount = ptCreditcard == null ? 0 : (int)ptCreditcard.Amount;

                            #region 檢查付款金額與份數是否相符

                            int quantityTotal = (int)odCol.Sum(x => x.ItemQuantity * x.ItemUnitPrice) + paymentDTO.DeliveryCharge;
                            int paymentTotal = trustCreditCardAmount + (int)paymentDTO.DeliveryInfo.SCash + paymentDTO.DeliveryInfo.PayEasyCash +
                                paymentDTO.DeliveryInfo.BonusPoints + discountAmt + atmAmount;

                            if (!int.Equals(quantityTotal, paymentTotal))
                            {
                                paymentReply.message = "交易失敗！";
                                paymentReply.paymentStatus = ApiIDEASPaymentReplyStatus.CreditCardError;

                                logger.Error("付款金額與份數不符");
                                paymentDTO.ResultPageType = PaymentResultPageType.CreditcardFailed;

                                return rtnObject;
                            }

                            #endregion 檢查付款金額與份數是否相符

                            int quantity = odCol.Sum(x => x.ItemQuantity);

                            using (TransactionScope transScope = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted }))
                            {
                                #region pcash扣款成功後 更新transation

                                if (ptPcash != null)
                                {
                                    PaymentFacade.UpdateTransaction(ptPcash.TransId, o.Guid, PaymentType.PCash, ptPcash.Amount, authCode,
                                        PayTransType.Authorization, DateTime.Now, ptPcash.Message,
                                        Helper.SetPaymentTransactionPhase(Helper.SetPaymentTransactionDepartmentTypes(0, (DepartmentTypes)theDeal.Department), PayTransPhase.Successful),
                                        (int)PayTransResponseType.OK);
                                }

                                #endregion pcash扣款成功後 更新transation

                                pp.TempSessionDelete(paymentDTO.TicketId);

                                #region Scash

                                if (paymentDTO.DeliveryInfo.SCash > 0 &&
                                    (ptCreditcard == null || (ptCreditcard != null &&
                                    Enum.Equals(PayTransResponseType.OK, (PayTransResponseType)ptCreditcard.Result))))
                                {
                                    OrderFacade.ScashUsed(paymentDTO.DeliveryInfo.SCash, userId, o.Guid, "購物金折抵:" + theDeal.SellerName,
                                        userName, OrderClassification.CashPointOrder);
                                }

                                #endregion Scash

                                #region 扣除選項

                                TempSessionCollection tsc = pp.TempSessionGetList("Accessory" + paymentDTO.TicketId);
                                foreach (TempSession ts in tsc)
                                {
                                    int forOut = 0;
                                    AccessoryGroupMember agm = ip.AccessoryGroupMemberGet(new Guid(ts.Name));
                                    if (!agm.IsLoaded)
                                        continue;

                                    if (agm.Quantity != null && int.TryParse(ts.ValueX, out forOut))
                                    {
                                        agm.Quantity = agm.Quantity - int.Parse(ts.ValueX);

                                        int? optId = PponOption.FindId(agm.Guid, theDeal.BusinessHourGuid);
                                        if (optId.HasValue)
                                        {
                                            PponOption opt = pp.PponOptionGet(optId.Value);
                                            if (opt != null && opt.IsLoaded)
                                            {
                                                opt.Quantity = agm.Quantity;
                                                pp.PponOptionSet(opt);
                                            }
                                        }
                                    }

                                    ip.AccessoryGroupMemberSet(agm);
                                }
                                pp.TempSessionDelete("Accessory" + paymentDTO.TicketId);

                                #endregion 扣除選項

                                #region 分店銷售數量

                                if (paymentDTO.DeliveryInfo.StoreGuid != Guid.Empty)
                                {
                                    int k = (int)(Math.Ceiling((double)paymentDTO.DeliveryInfo.Quantity / (theDeal.ComboPackCount ?? 1)));
                                    k = (theDeal.SaleMultipleBase ?? 0) > 0 ? theDeal.SaleMultipleBase.Value : k;
                                    pp.PponStoreUpdateOrderQuantity(theDeal.BusinessHourGuid, paymentDTO.DeliveryInfo.StoreGuid, k);
                                }

                                #endregion 分店銷售數量

                                #region 信用卡轉購物金

                                if (ptCreditcard != null)
                                {
                                    Guid sOrderGuid = OrderFacade.MakeSCachOrder(ptCreditcard.Amount, ptCreditcard.Amount, "17Life購物金購買(系統)",
                                        CashPointStatus.Approve, userId, theDeal.ItemName, CashPointListType.Income, userName, o.Guid);

                                    //Scash will not be invoiced initially after the NewInvoiceDate.
                                    bool invoiced = (DateTime.Now < config.NewInvoiceDate);
                                    int depositId = OrderFacade.NewScashDeposit(sOrderGuid, ptCreditcard.Amount, o.UserId, o.Guid,
                                        "購物金購買(系統):" + theDeal.SellerName, userName, invoiced, OrderClassification.CashPointOrder);

                                    OrderFacade.NewScashWithdrawal(depositId, ptCreditcard.Amount, o.Guid, "購物金折抵(系統):" + theDeal.SellerName,
                                        userName, OrderClassification.CashPointOrder);

                                    ptCreditcard.OrderGuid = sOrderGuid;
                                    ptCreditcard.Message += "|" + o.Guid.ToString();
                                    op.PaymentTransactionSet(ptCreditcard);

                                    if (ptScash == null)
                                    {
                                        OrderFacade.MakeTransaction(paymentDTO.TransactionId, PaymentType.SCash, int.Parse(ptCreditcard.Amount.ToString("F0")),
                                            (DepartmentTypes)theDeal.Department, userName, o.Guid, paymentDTO.PezId);
                                    }
                                    else
                                    {
                                        PaymentFacade.UpdateTransaction(ptScash.TransId, ptScash.OrderGuid, PaymentType.SCash, ptScash.Amount + ptCreditcard.Amount,
                                            ptScash.AuthCode, PayTransType.Authorization, DateTime.Now, ptScash.Message, ptScash.Status, ptScash.Result.Value);
                                    }
                                }

                                #endregion 信用卡轉購物金

                                transScope.Complete();
                                orderOK = true;
                            }

                            #endregion Transaction Scope

                            if (!orderOK)
                            {
                                CreditCardUtility.AuthenticationReverse(paymentDTO.TransactionId, ptCreditcard.AuthCode);
                                if (ptPcash != null)
                                {
                                    PCashWorker.DeCheckOut(paymentDTO.TransactionId, authCode, ptPcash.Amount);
                                }
                                logger.Error("Order update failed.");

                                paymentDTO.ResultPageType = PaymentResultPageType.CreditcardFailed;

                                paymentReply.message = "交易失敗！";
                                paymentReply.paymentStatus = ApiIDEASPaymentReplyStatus.CreditCardError;

                                return rtnObject;
                            }
                            else
                            {
                                //if weeklypay deal
                                int trust_checkout_type = 0;
                                if (((theDeal.BusinessHourStatus) & (int)BusinessHourStatus.WeeklyPay) > 0)
                                {
                                    trust_checkout_type = (int)TrustCheckOutType.WeeklyPay;
                                }

                                #region make cash trust log

                                #region payments
                                Dictionary<Core.PaymentType, int> payments = new Dictionary<Core.PaymentType, int>();
                                if ((int)paymentDTO.DeliveryInfo.SCash > 0)
                                {
                                    payments.Add(Core.PaymentType.SCash, (int)paymentDTO.DeliveryInfo.SCash);
                                }
                                if ((int)paymentDTO.DeliveryInfo.PayEasyCash > 0)
                                {
                                    payments.Add(Core.PaymentType.PCash, (int)paymentDTO.DeliveryInfo.PayEasyCash);
                                }
                                if ((int)paymentDTO.DeliveryInfo.BonusPoints > 0)
                                {
                                    payments.Add(Core.PaymentType.BonusPoint, (int)paymentDTO.DeliveryInfo.BonusPoints);
                                }
                                if ((int)trustCreditCardAmount > 0)
                                {
                                    payments.Add(Core.PaymentType.Creditcard, (int)trustCreditCardAmount);
                                }
                                if ((int)discountAmt > 0)
                                {
                                    payments.Add(Core.PaymentType.DiscountCode, (int)discountAmt);
                                }
                                if ((int)atmAmount > 0)
                                {
                                    payments.Add(Core.PaymentType.ATM, (int)atmAmount);
                                }
                                #endregion payments

                                int saleMultipleBase = Helper.IsFlagSet(theDeal.BusinessHourStatus, BusinessHourStatus.GroupCoupon) ? theDeal.SaleMultipleBase ?? 0 : 0;

                                CashTrustInfo cti = new CashTrustInfo
                                {
                                    OrderGuid = o.Guid,
                                    OrderDetails = odCol,
                                    CreditCardAmount = trustCreditCardAmount,
                                    SCashAmount = (int)paymentDTO.DeliveryInfo.SCash,
                                    PCashAmount = paymentDTO.DeliveryInfo.PayEasyCash,
                                    BCashAmount = paymentDTO.DeliveryInfo.BonusPoints,
                                    Quantity = saleMultipleBase > 0 ? saleMultipleBase : quantity,
                                    DeliveryCharge = paymentDTO.DeliveryCharge,
                                    DeliveryType = theDeal.DeliveryType == null ? DeliveryType.ToShop : (DeliveryType)theDeal.DeliveryType.Value,
                                    DiscountAmount = discountAmt,
                                    AtmAmount = atmAmount,
                                    BusinessHourGuid = theDeal.BusinessHourGuid,
                                    CheckoutType = trust_checkout_type,
                                    TrustProvider = Helper.GetBusinessHourTrustProvider(theDeal.BusinessHourStatus),
                                    ItemName = theDeal.ItemName,
                                    ItemPrice = (int)theDeal.ItemPrice,
                                    ItemOriPrice = (int)theDeal.ItemOrigPrice,
                                    User = mp.MemberGet(userName),
                                    Payments = payments,
                                    PresentQuantity = theDeal.PresentQuantity ?? 0,
                                    IsGroupCoupon = Helper.IsFlagSet(theDeal.BusinessHourStatus, BusinessHourStatus.GroupCoupon),
                                    GroupCouponType = (GroupCouponDealType)(theDeal.GroupCouponDealType ?? 0)
                                };

                                CashTrustLogCollection ctCol = OrderFacade.MakeCashTrust(cti);

                                #endregion make cash trust log

                                if (paymentDTO.DeliveryInfo.EntrustSell == EntrustSellType.No)
                                {
                                    if (DateTime.Now >= config.NewInvoiceDate)
                                    {
                                        #region 新制憑證發票

                                        string creditCardTail = !string.IsNullOrEmpty(paymentDTO.CreditcardNumber) ? paymentDTO.CreditcardNumber.Substring(paymentDTO.CreditcardNumber.Length - 4) : string.Empty;
                                        if (ctCol.Count() > 0 && paymentDTO.DeliveryInfo.DeliveryType == DeliveryType.ToShop)
                                        {
                                            foreach (var ct in ctCol)
                                            {
                                                int uninvoicedAmount = saleMultipleBase > 0 ? ((ctCol.Count() > 0) ? ctCol.Sum(x => x.UninvoicedAmount) : 0) : ct.UninvoicedAmount;
                                                if (paymentDTO.IsPaidByATM)
                                                {
                                                    EinvoiceFacade.SetEinvoiceMain(o, paymentDTO.DeliveryInfo, paymentDTO.TransactionId, 1,
                                                        theDeal.ItemName, uninvoicedAmount,
                                                        Helper.IsFlagSet(theDeal.GroupOrderStatus ?? 0, GroupOrderStatus.NoTax),
                                                        userName, false, OrderClassification.LkSite, creditCardTail, (int)EinvoiceType.NotComplete, InvoiceVersion.Old, Helper.IsFlagSet(theDeal.BusinessHourStatus, BusinessHourStatus.GroupCoupon));
                                                }
                                                else if (uninvoicedAmount > 0)
                                                {
                                                    EinvoiceFacade.SetEinvoiceMain(o, paymentDTO.DeliveryInfo, paymentDTO.TransactionId, 1,
                                                        theDeal.ItemName, uninvoicedAmount,
                                                        Helper.IsFlagSet(theDeal.GroupOrderStatus ?? 0, GroupOrderStatus.NoTax),
                                                        userName, false, OrderClassification.LkSite, creditCardTail, (int)EinvoiceType.Initial, InvoiceVersion.Old, Helper.IsFlagSet(theDeal.BusinessHourStatus, BusinessHourStatus.GroupCoupon));
                                                }
                                                if (Helper.IsFlagSet(theDeal.BusinessHourStatus, BusinessHourStatus.GroupCoupon))
                                                    break;
                                            }
                                        }

                                        #endregion 新制憑證發票

                                        #region 新制宅配發票

                                        int uninvoicedTotal = (ctCol.Count() > 0) ? ctCol.Sum(x => x.UninvoicedAmount) : 0;
                                        if (paymentDTO.DeliveryInfo.DeliveryType == DeliveryType.ToHouse)
                                        {
                                            if (paymentDTO.IsPaidByATM)
                                            {
                                                EinvoiceFacade.SetEinvoiceMain(o, paymentDTO.DeliveryInfo, paymentDTO.TransactionId, quantity,
                                                    theDeal.ItemName, uninvoicedTotal,
                                                    Helper.IsFlagSet(theDeal.GroupOrderStatus ?? 0, GroupOrderStatus.NoTax),
                                                    userName, true, OrderClassification.LkSite, creditCardTail, (int)EinvoiceType.NotComplete);
                                            }
                                            else if (uninvoicedTotal > 0)
                                            {
                                                EinvoiceFacade.SetEinvoiceMain(o, paymentDTO.DeliveryInfo, paymentDTO.TransactionId, quantity,
                                                    theDeal.ItemName, uninvoicedTotal,
                                                    Helper.IsFlagSet(theDeal.GroupOrderStatus ?? 0, GroupOrderStatus.NoTax),
                                                    userName, true, OrderClassification.LkSite, creditCardTail);
                                            }
                                        }

                                        #endregion 新制宅配發票
                                    }
                                }
                                else
                                {
                                    //代收轉付以收據代替發票。
                                    OrderFacade.EntrustSellReceiptSet(paymentDTO.DeliveryInfo, o, odCol, ctCol, theDeal.ItemName, userName);
                                }

                                // save delivery info
                                saveDeliveryInfo(paymentDTO, o, userName, theDeal.DeliveryType);

                                // set deal close time
                                OrderFacade.PponSetCloseTimeIfJustReachMinimum(theDeal, quantity);

                                // send deal-on mail  //付款成功
                                if (paymentDTO.IsPaidByATM)
                                {
                                    //加註訂單為ATM付款
                                    OrderFacade.SetOrderStatus(o.Guid, userName, OrderStatus.ATMOrder, true, new SalesInfoArg
                                    {
                                        BusinessHourGuid = theDeal.BusinessHourGuid,
                                        Quantity = theDeal.OrderedQuantity.GetValueOrDefault(),
                                        Total = theDeal.OrderedTotal.GetValueOrDefault()
                                    });
                                    Member m = mp.MemberGet(userName);
                                    OrderFacade.SendATMOrderNotifyMail(m.DisplayName, m.UserEmail, theDeal.ItemName,
                                        paymentDTO.ATMAccount, atmAmount.ToString(), theDeal, o, quantity);
                                }
                                else
                                {
                                    OrderFacade.SetOrderStatus(o.Guid, userName, OrderStatus.Complete, true,
                                        new SalesInfoArg
                                        {
                                            BusinessHourGuid = theDeal.BusinessHourGuid,
                                            Quantity = odCol.OrderedQuantity,
                                            Total = odCol.OrderedTotal
                                        });

                                    //如果付款金而為0表示該檔次為0元好康，不發信
                                    if (paymentDTO.Amount != 0)
                                    {
                                        //寄發資策會的購買完成通知信
                                        OrderFacade.SendIDEASMail(IDEASMailContentType.PaymentCompleted, o, null);
                                    }
                                }

                                #region 推薦送折價券

                                //有推薦人資料，發放推薦折價券
                                if (!string.IsNullOrWhiteSpace(paymentDTO.ReferenceId))
                                {
                                    PromotionFacade.ReferenceDiscountCheckAndPay(paymentDTO.ReferenceId, o, theDeal, MemberFacade.GetUserName(o.UserId));
                                }

                                #endregion 推薦送折價券

                                #region 首購送折價券

                                PromotionFacade.FirstBuyDiscountCheckAndPay(o, theDeal);

                                #endregion

                                #region Coupon

                                /*產生coupon並寄信 start*/
                                lock (CouponLock)
                                {
                                    Order theOrder = op.OrderGet(odCol[0].OrderGuid);
                                    CouponFacade.GenCouponWithSmsWithGeneratePeztemp(theOrder, theDeal, false);
                                }
                                /*產生coupon並寄信 end*/

                                #endregion Coupon
                            }

                            paymentDTO.ResultPageType = (paymentTypes.Contains(PaymentType.Creditcard) || paymentTypes.Contains(PaymentType.ATM)) ?
                                PaymentResultPageType.CreditcardSuccess : PaymentResultPageType.PcashSuccess;

                            #region 抓序號(宅配檔就不抓)

                            List<ApiIDEASCoupon> apiCoupons = new List<ApiIDEASCoupon>();

                            CouponCollection couponList = new CouponCollection();
                            if (theDeal.DeliveryType != (int)DeliveryType.ToHouse)
                            {
                                #region 第一次停半秒鐘抓序號

                                System.Threading.Thread.Sleep(500);

                                if (odCol.Count > 0)
                                {
                                    couponList = pp.CouponGetList(odCol[0].Guid);
                                }

                                #endregion 第一次停半秒鐘抓序號

                                #region 如果沒抓到每半秒再抓一次，最多抓20次沒抓到後結束

                                int iCodeCount = 0;
                                do
                                {
                                    System.Threading.Thread.Sleep(500);

                                    if (odCol.Count > 0)
                                    {
                                        couponList = pp.CouponGetList(odCol[0].Guid);
                                    }

                                    iCodeCount++;
                                } while (int.Equals(0, couponList.Count) && iCodeCount < 20);

                                #endregion 如果沒抓到每半秒再抓一次，最多抓20次沒抓到後結束

                                #region 取不到序號則不成立訂單

                                if (int.Equals(0, couponList.Count))
                                {
                                    //作廢訂單
                                    OrderFacade.SetOrderStatus(o.Guid, userName, OrderStatus.Complete, false);

                                    rtnObject.Code = ApiReturnCode.PaymentError;
                                    paymentDTO.ResultPageType = PaymentResultPageType.CreditcardFailed;
                                    rtnObject.Message = "很抱歉，這個序號無法被取得！";

                                    paymentReply.paymentStatus = ApiIDEASPaymentReplyStatus.CouponCreateError;
                                    return rtnObject;
                                }
                                else
                                {
                                    foreach (var coupon in couponList)
                                    {
                                        ApiIDEASCoupon apiCoupon = new ApiIDEASCoupon();
                                        apiCoupon.seq = coupon.StoreSequence == null ? 0 : coupon.StoreSequence.Value;
                                        apiCoupon.couponId = coupon.Id;
                                        apiCoupon.sequenceNumber = coupon.SequenceNumber;
                                        apiCoupon.couponCode = coupon.Code;
                                        apiCoupons.Add(apiCoupon);
                                    }
                                }

                                #endregion 取不到序號則不成立訂單
                            }

                            #endregion 抓序號(宅配檔就不抓)

                            paymentReply.orderId = o.OrderId;
                            paymentReply.paymentStatus = ApiIDEASPaymentReplyStatus.Success;
                            paymentReply.coupons = apiCoupons;
                            paymentReply.amount = ptCreditcard.Amount;

                            rtnObject.Code = ApiReturnCode.Success;
                            return rtnObject;
                        }
                        else
                        {
                            if (ptc.Count > 0)
                            {
                                PaymentFacade.CancelPaymentTransactionByTransId(ptc[0].TransId, "sys", "Unorderable. ");
                            }

                            if (errorPaymentType == PaymentType.PCash)
                            {
                                paymentDTO.ResultPageType = PaymentResultPageType.PcashFailed;

                                paymentReply.message = "Payeasy購物金扣款失敗。";
                                paymentReply.paymentStatus = ApiIDEASPaymentReplyStatus.PCashError;
                            }
                            else
                            {
                                paymentDTO.ResultPageType = PaymentResultPageType.CreditcardFailed;

                                paymentReply.message = "信用卡刷卡失敗。";
                                paymentReply.paymentStatus = ApiIDEASPaymentReplyStatus.CreditCardError;
                            }
                            return rtnObject;
                        }
                    }
                    else
                    {
                        OrderFacade.DeleteItemInCart(paymentDTO.TicketId);
                        pp.TempSessionDelete(paymentDTO.TicketId);

                        paymentDTO.TransactionId = null;

                        if (errorPaymentType == PaymentType.PCash)
                        {
                            paymentDTO.ResultPageType = PaymentResultPageType.PcashFailed;

                            paymentReply.message = "Payeasy購物金扣款失敗。";
                            paymentReply.paymentStatus = ApiIDEASPaymentReplyStatus.PCashError;
                        }
                        else
                        {
                            paymentDTO.ResultPageType = PaymentResultPageType.CreditcardFailed;

                            paymentReply.message = "信用卡刷卡失敗。";
                            paymentReply.paymentStatus = ApiIDEASPaymentReplyStatus.CreditCardError;
                        }
                        return rtnObject;
                    }
                }
                else
                {
                    logger.Error("TransactionID is missing.");

                    paymentReply.message = "交易失敗！";
                    paymentReply.paymentStatus = ApiIDEASPaymentReplyStatus.Error;

                    return rtnObject;
                }
            }
        }

        private bool CheckRemaining(string pezid, decimal charge)
        {
            if (string.IsNullOrEmpty(pezid))
            {
                if (charge > 0)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            else
            {
                decimal remaining = PCashWorker.CheckUserRemaining(pezid);
                if (remaining == -1)
                {
                    return false;
                }

                if (remaining < charge)
                {
                    return false;
                }

                return true;
            }
        }

        private bool CheckBranch(Guid bid, bool isItem, Guid storeId, int itemQuantity, bool noRestrictedStore, int? saleMultipleBase)
        {
            //商品檔，回傳true
            if (isItem)
            {
                return true;
            }

            //通用券檔次 無須檢查分店 return true
            if (noRestrictedStore)
            {
                return true;
            }

            //非商品檔，分店編號錯誤回傳false
            if (Guid.Empty == storeId)
            {
                return false;
            }
            //有分店編號，檢查是否還有數量
            PponStore pponStore = pp.PponStoreGet(bid, storeId);
            //查無分店設定，回傳false
            if (!pponStore.IsLoaded)
            {
                return false;
            }
            //分店設定有設定數量控制，檢查是否超過上限，若是則不允許購買
            itemQuantity = (saleMultipleBase ?? 0) > 0 ? saleMultipleBase.Value : itemQuantity;
            if ((pponStore.TotalQuantity != null) && (pponStore.TotalQuantity.Value < pponStore.OrderedQuantity + itemQuantity))
            {
                return false;
            }

            return true;
        }

        private bool checkOptions(string itemOptions)
        {
            if (itemOptions.IndexOf("請選擇") > -1 || itemOptions.IndexOf("已售完") > -1)
            {
                return false;
            }

            return true;
        }

        private string CheckData(IViewPponDeal viewPponDeal, int quantity, int bonusCash, decimal scash, string discountCode, string userName, decimal freight, bool isDailyRestriction)
        {
            if (quantity > (viewPponDeal.MaxItemCount ?? int.MaxValue))
            {
                return "每單訂購數量最多" + viewPponDeal.MaxItemCount + "份，請重新輸入數量";
            }

            if (bonusCash > Math.Floor(MemberFacade.GetAvailableMemberPromotionValue(userName) / 10))
            {
                return "您的可用紅利點數換算可抵用金額最多為" + Math.Floor(MemberFacade.GetAvailableMemberPromotionValue(userName) / 10) + "元，請重新輸入金額";
            }

            if (bonusCash < 0)
            {
                return "紅利點數金額不可為負，請重新輸入金額";
            }

            if (scash > OrderFacade.GetSCashSum(userName))
            {
                return "您的可用17購物金可抵用金額最多為" + OrderFacade.GetSCashSum(userName).ToString() + "元，請重新輸入金額";
            }

            if (PponFacade.CheckExceedOrderedQuantityLimit(viewPponDeal, quantity))
            {
                return "很抱歉!已超過最大訂購數量";
            }

            if (DateTime.Now > viewPponDeal.BusinessHourOrderTimeE)
            {
                return "很抱歉~此好康已結束";
            }

            if (!string.IsNullOrWhiteSpace(discountCode))
            {
                int minimum_amount;
                decimal amount = viewPponDeal.ItemPrice * quantity + freight;
                var discountCodeStatus = PromotionFacade.GetDiscountCodeStatus(discountCode, DiscountCampaignUsedFlags.Ppon, amount, userName, out minimum_amount, viewPponDeal.BusinessHourGuid.ToString());
                if (discountCodeStatus == DiscountCodeStatus.None)
                {
                    return I18N.Phrase.DiscountCodeNone;
                }
                else if (discountCodeStatus == DiscountCodeStatus.UpToQuantity)
                {
                    return I18N.Phrase.DiscountCodeUptoQuantity;
                }
                else if (discountCodeStatus == DiscountCodeStatus.IsUsed)
                {
                    return I18N.Phrase.DiscountCodeIsUsed;
                }
                else if (discountCodeStatus == DiscountCodeStatus.Disabled)
                {
                    return I18N.Phrase.DiscountCodeDisabled;
                }
                else if (discountCodeStatus == DiscountCodeStatus.OverTime)
                {
                    return I18N.Phrase.DiscountCodeOverTime;
                }
                else if (discountCodeStatus == DiscountCodeStatus.Restricted)
                {
                    return string.Format(I18N.Phrase.DiscountCodeRestricted, "品生活");
                }
                else if (discountCodeStatus == DiscountCodeStatus.UnderMinimumAmount)
                {
                    return string.Format(I18N.Phrase.DiscountCodeUnderMinimumAmount, minimum_amount);
                }
                else if (discountCodeStatus == DiscountCodeStatus.DiscountCodeCategoryWrong)
                {
                    return I18N.Phrase.DiscountCodeCategoryWrong;
                }
                else if (discountCodeStatus == DiscountCodeStatus.OwnerUseOnlyWrong)
                {
                    return I18N.Phrase.DiscountCodeOwnerUseOnlyWrong;
                }
                else if (discountCodeStatus == DiscountCodeStatus.CategoryUseLimit)
                {
                    return string.Format("僅限" + PromotionFacade.GetDiscountCategoryString(discountCode) + "使用");
                }
                else if (discountCodeStatus != DiscountCodeStatus.CanUse)
                {
                    return I18N.Phrase.DiscountCodeCanNotUse;
                }
            }

            return string.Empty;
        }

        private decimal CheckDeliveryCharge(Decimal orderedAmount, IViewPponDeal deal)
        {
            return deal.BusinessHourDeliveryCharge;
        }

        private int SetDeliveryCharge(Decimal orderedAmount, IViewPponDeal deal)
        {
            CouponFreightCollection cfc = pp.CouponFreightGetList(deal.BusinessHourGuid, CouponFreightType.Income);

            if (null != cfc && cfc.Count > 0)
            {
                foreach (CouponFreight cf in cfc)
                {
                    if (orderedAmount >= cf.StartAmount && cf.EndAmount > orderedAmount)
                    {
                        return (int)(cf.FreightAmount);
                    }
                }
            }
            else
            {
                return (int)CheckDeliveryCharge(orderedAmount, deal);
            }

            return 0;
        }

        private void CheckResult(PaymentDTO paymentDTO, CreditCardAuthResult result)
        {
            PaymentTransaction pt = op.PaymentTransactionGet(paymentDTO.TransactionId, PaymentType.Creditcard, PayTransType.Authorization);
            int status = Helper.SetPaymentTransactionPhase(pt.Status, result.TransPhase);
            int rlt = 0;
            try
            {
                rlt = int.Parse(result.ReturnCode);
            }
            catch
            {
                rlt = -9527;
            }

            PaymentFacade.UpdateTransaction(paymentDTO.TransactionId, pt.OrderGuid, PaymentType.Creditcard, Convert.ToDecimal(paymentDTO.Amount), result.AuthenticationCode,
                result.TransType, result.OrderDate, result.TransMessage + " " + result.CardNumber, status, rlt, result.APIProvider);
        }

        private void saveDeliveryInfo(PaymentDTO paymentDTO, Order order, string userName, int? deliveryType)
        {
            if (!string.IsNullOrWhiteSpace(paymentDTO.AddressInfo))
            {
                string[] yy = paymentDTO.AddressInfo.Split('^');
                if (!string.IsNullOrWhiteSpace(yy[0]))
                {
                    string[] badd = yy[0].Split('|');

                    Member m = mp.MemberGet(userName);
                    if (null != m && m.IsLoaded)
                    {
                        if (!string.IsNullOrWhiteSpace(badd[0]))
                        {
                            m.LastName = badd[0];
                        }

                        if (!string.IsNullOrWhiteSpace(badd[1]))
                        {
                            m.FirstName = badd[1];
                        }

                        if (!string.IsNullOrWhiteSpace(badd[2]))
                        {
                            m.Mobile = badd[2];
                        }

                        Guid buid = Guid.Empty;
                        if (!string.IsNullOrWhiteSpace(badd[6]) && Guid.TryParse(badd[5], out buid))
                        {
                            m.BuildingGuid = buid;
                            m.CompanyAddress = lp.CityGet(int.Parse(badd[3])).CityName + lp.CityGet(int.Parse(badd[4])).CityName + lp.BuildingGet(buid).BuildingStreetName + badd[6];
                        }

                        mp.MemberSet(m);

                        // fix order's buyer name
                        if (deliveryType.HasValue && int.Equals((int)DeliveryType.ToShop, deliveryType.Value))
                        {
                            order.MemberName = m.DisplayName;

                            op.OrderSet(order);
                        }
                    }
                }

                // receiver delivery
                if (!string.IsNullOrWhiteSpace(yy[1]))
                {
                    string[] radd = yy[1].Split('|');
                    if (bool.Parse(radd[6]))
                    {
                        if (!string.IsNullOrWhiteSpace(radd[5]))
                        {
                            MemberDelivery md = new MemberDelivery();
                            md.CreateTime = DateTime.Now;
                            md.UserId = MemberFacade.GetUniqueId(userName);
                            Guid buid = Guid.Empty;
                            if (!string.IsNullOrWhiteSpace(radd[5]) && Guid.TryParse(radd[4], out buid))
                            {
                                md.BuildingGuid = Guid.Parse(radd[4]);
                                md.Address = lp.CityGet(int.Parse(radd[2])).CityName + lp.CityGet(int.Parse(radd[3])).CityName + lp.BuildingGet(md.BuildingGuid).BuildingStreetName + radd[5];
                            }
                            md.ContactName = radd[0];
                            md.AddressAlias = radd[0];
                            md.Mobile = radd[1];

                            mp.MemberDeliverySet(md);
                        }
                    }
                }
            }
        }

        #endregion private methods
    }
}
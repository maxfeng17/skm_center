﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Component.API;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.Core;

namespace LunchKingSite.Web.WebService
{
    /// <summary>
    /// AppSystemService 的摘要描述
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // 若要允許使用 ASP.NET AJAX 從指令碼呼叫此 Web 服務，請取消註解下一行。
    // [System.Web.Script.Services.ScriptService]
    public class AppSystemService : System.Web.Services.WebService
    {

        [WebMethod]
        public void CheckAppVersion(string userId)
        {
            //設定輸出格式為json格式
            this.Context.Response.ContentType = "application/json";
            this.Context.Response.Charset = Encoding.UTF8.WebName;

            var jsonS = new JsonSerializer();
            this.Context.Response.Write(jsonS.Serialize(new {needUpdate = false}));
        }

        [WebMethod]
        public void GetFamilyMarkOpenDate(string userId)
        {
            var jsonS = new JsonSerializer();
            ApiReturnObject rtnObject = new ApiReturnObject() { Code = ApiReturnCode.Success };

            //設定輸出格式為json格式
            this.Context.Response.ContentType = "application/json";
            this.Context.Response.Charset = Encoding.UTF8.WebName;

            //驗證使用者
            var apiUser = ApiUserManager.ApiUserGetByUserId(userId);
            if (apiUser == null)
            {
                rtnObject.Code = ApiReturnCode.ApiUserIdError;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeApiUserIdError;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }
            string openTime = ApiSystemManager.GetFamilyMarkOpenTimeString();
            rtnObject.Data = new { OpenTime = openTime };

            this.Context.Response.Write(jsonS.Serialize(rtnObject));
            //使用紀錄
            ApiUserManager.ApiUsedLogSet(apiUser, "AppSystemService/GetFamilyMarkOpenDate", jsonS.Serialize(new { userId = userId}));
        }
    }
}

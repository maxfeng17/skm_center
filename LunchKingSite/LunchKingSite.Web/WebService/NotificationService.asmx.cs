﻿using System;
using System.Text;
using System.Web.Services;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;

namespace LunchKingSite.Web.WebService
{
    /// <summary>
    /// NotificationService 的摘要描述
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // 若要允許使用 ASP.NET AJAX 從指令碼呼叫此 Web 服務，請取消註解下一行。
    // [System.Web.Script.Services.ScriptService]
    public class NotificationService : BaseService
    {
        /// <summary>
        /// 存入推播用的 token/registId(iOS/android)
        /// </summary>
        /// <param name="token"></param>
        /// <param name="cityId"></param>
        /// <param name="enabled"></param>
        /// <param name="userId"></param>
        [WebMethod]
        public void SetIDeviceToken(string token, int cityId, bool enabled, string userId)
        {
            SetIDeviceTokenByType(token, cityId, enabled, userId, (int)MobileOsType.iOS, "");
        }

        /// <summary>
        /// 存入推播用的 token/registId(iOS/android)
        /// </summary>
        /// <param name="token">推播id(iOS是token, Android是RegistId)</param>
        /// <param name="cityId">要推播的城市</param>
        /// <param name="enabled">true:註冊/false:取消註冊</param>
        /// <param name="userId">Web API 使用的 User Id</param>
        /// <param name="mobileType">Mobile OS 的類型，請見列舉：MobileConfig.MobileOsType </param>
        /// <param name="deviceId"> </param>
        [WebMethod]
        public void SetIDeviceTokenByType(string token, int cityId, bool enabled, string userId, int mobileType, string deviceId)
        {
            ApiReturnObject rtnObject = new ApiReturnObject();

            var jsonS = new JsonSerializer();
            //設定輸出格式為json格式
            this.Context.Response.ContentType = "application/json";
            this.Context.Response.Charset = Encoding.UTF8.WebName;
            //驗證使用者
            var apiUser = ApiUserManager.ApiUserGetByUserId(userId);
            if (apiUser == null)
            {
                rtnObject.Code = ApiReturnCode.ApiUserIdError;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeApiUserIdError;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }

            MobileOsType type;

            if (Enum.IsDefined(typeof(MobileOsType), mobileType))
            {
                type = (MobileOsType)mobileType;
            }
            else
            {
                rtnObject.Code = ApiReturnCode.MobileOsTypeError;
                rtnObject.Message = I18N.Phrase.MobileOsTypeError;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }

            PponCity city = PponCityGroup.DefaultPponCityGroup.GetPponCityByCityId(cityId);
            if (city == null)
            {
                rtnObject.Code = ApiReturnCode.InputError;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeInputError;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }

            //設定推播訂閱
            if (NotificationFacade.SetSubscriptionNotice(city.CategoryId, token, enabled, type, deviceId))
            {
                rtnObject.Code = ApiReturnCode.Success;
                rtnObject.Data = new { CategoryId = city.CategoryId, Enabled = enabled };
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
            }
            else
            {
                rtnObject.Code = ApiReturnCode.Error;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeError;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }

            ApiUserManager.ApiUsedLogSet(apiUser, "NotificationService/SetIDeviceTokenByType",
                jsonS.Serialize(new { token = token, categoryId = city.CategoryId, enabled = enabled, userId = userId, mobileType = mobileType, deviceId = deviceId }));
        }

        /// <summary>
        /// 存入推播用的 token/registId(iOS/android)，並依據傳入的城市名稱，決定使用者紀錄的好康區域編號。
        /// </summary>
        /// <param name="token"></param>
        /// <param name="cityName"></param>
        /// <param name="enabled"></param>
        /// <param name="userId"></param>
        /// <param name="mobileType"></param>
        /// <param name="deviceId"></param>
        [WebMethod]
        public void SetIDeviceTokenByCityName(string token, string cityName, bool enabled, string userId, int mobileType, string deviceId)
        {
            ApiReturnObject rtnObject = new ApiReturnObject();

            var jsonS = new JsonSerializer();
            //設定輸出格式為json格式
            this.Context.Response.ContentType = "application/json";
            this.Context.Response.Charset = Encoding.UTF8.WebName;
            //驗證使用者
            var apiUser = ApiUserManager.ApiUserGetByUserId(userId);
            if (apiUser == null)
            {
                rtnObject.Code = ApiReturnCode.ApiUserIdError;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeApiUserIdError;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }

            MobileOsType type;

            if (Enum.IsDefined(typeof(MobileOsType), mobileType))
            {
                type = (MobileOsType)mobileType;
            }
            else
            {
                rtnObject.Code = ApiReturnCode.MobileOsTypeError;
                rtnObject.Message = I18N.Phrase.MobileOsTypeError;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }

            if (string.IsNullOrWhiteSpace(cityName))
            {
                rtnObject.Code = ApiReturnCode.InputError;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeInputError;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }
            //依據cityName查詢符合的cityGroup資料，好得到在地好康的區域城市的編號
            CityGroup cityGroup = CityManager.CityGroupGetByCityName(cityName, CityGroupType.Subscription);

            if (cityGroup == null)
            {
                rtnObject.Code = ApiReturnCode.InputError;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeInputError;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }

            PponCity city = PponCityGroup.DefaultPponCityGroup.GetPponCityByCityId(cityGroup.RegionId);
            if (city == null)
            {
                rtnObject.Code = ApiReturnCode.InputError;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeInputError;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }

            //設定推播訂閱
            if (NotificationFacade.SetSubscriptionNotice(city.CategoryId, token, enabled, type, deviceId))
            {
                rtnObject.Code = ApiReturnCode.Success;
                rtnObject.Data = new { CategoryId = city.CategoryId, Enabled = enabled };
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
            }
            else
            {
                rtnObject.Code = ApiReturnCode.Error;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeError;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }

            ApiUserManager.ApiUsedLogSet(apiUser, "NotificationService/SetIDeviceTokenByCityName",
                jsonS.Serialize(new { token = token, categoryId = city.CategoryId, enabled = enabled, userId = userId, mobileType = mobileType, deviceId = deviceId }));
        }

        [WebMethod]
        public void SetNoticeTokenByCategoryId(string token, int categoryId, bool enabled, string userId, int mobileType, string deviceId)
        {
            ApiReturnObject rtnObject = new ApiReturnObject();

            var jsonS = new JsonSerializer();
            //設定輸出格式為json格式
            this.Context.Response.ContentType = "application/json";
            this.Context.Response.Charset = Encoding.UTF8.WebName;
            //驗證使用者
            var apiUser = ApiUserManager.ApiUserGetByUserId(userId);
            if (apiUser == null)
            {
                rtnObject.Code = ApiReturnCode.ApiUserIdError;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeApiUserIdError;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }

            //裝置識別改由apiUser的值判定
            MobileOsType type;
            switch ((ApiUserDeviceType)apiUser.UserDeviceType)
            {
                case ApiUserDeviceType.WebService:
                    type = MobileOsType.Universal;
                    break;
                case ApiUserDeviceType.Android:
                    type = MobileOsType.Android;
                    break;
                case ApiUserDeviceType.IOS:
                    type = MobileOsType.iOS;
                    break;
                case ApiUserDeviceType.WinPhone:
                    type = MobileOsType.WinPhone;
                    break;
                default:
                    rtnObject.Code = ApiReturnCode.MobileOsTypeError;
                    rtnObject.Message = I18N.Phrase.MobileOsTypeError;
                    this.Context.Response.Write(jsonS.Serialize(rtnObject));
                    return;

            }

            if (NotificationFacade.SetSubscriptionNotice(categoryId, token, enabled, type, deviceId))
            {
                rtnObject.Code = ApiReturnCode.Success;
                rtnObject.Data = new { CategoryId = categoryId, Enabled = enabled };
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
            }
            else
            {
                rtnObject.Code = ApiReturnCode.Error;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeError;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }

            ApiUserManager.ApiUsedLogSet(apiUser, "NotificationService/SetDeviceTokenByCategoryId"
                , jsonS.Serialize(new { token = token, categoryId = categoryId, enabled = enabled, userId = userId, mobileType = mobileType, deviceId = deviceId }));
        }

        [WebMethod]
        public void SetNoticeTokenByCityName(string token, string cityName, bool enabled, string userId, int mobileType,
                                             string deviceId)
        {
            ApiReturnObject rtnObject = new ApiReturnObject();

            var jsonS = new JsonSerializer();
            //設定輸出格式為json格式
            this.Context.Response.ContentType = "application/json";
            this.Context.Response.Charset = Encoding.UTF8.WebName;
            //驗證使用者
            var apiUser = ApiUserManager.ApiUserGetByUserId(userId);
            if (apiUser == null)
            {
                rtnObject.Code = ApiReturnCode.ApiUserIdError;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeApiUserIdError;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }

            //裝置識別改由apiUser的值判定
            MobileOsType type;
            switch ((ApiUserDeviceType)apiUser.UserDeviceType)
            {
                case ApiUserDeviceType.WebService:
                    type = MobileOsType.Universal;
                    break;
                case ApiUserDeviceType.Android:
                    type = MobileOsType.Android;
                    break;
                case ApiUserDeviceType.IOS:
                    type = MobileOsType.iOS;
                    break;
                case ApiUserDeviceType.WinPhone:
                    type = MobileOsType.WinPhone;
                    break;
                default:
                    rtnObject.Code = ApiReturnCode.MobileOsTypeError;
                    rtnObject.Message = I18N.Phrase.MobileOsTypeError;
                    this.Context.Response.Write(jsonS.Serialize(rtnObject));
                    return;
            }

            //驗證cityName並轉換成CityGroup物件
            if (string.IsNullOrWhiteSpace(cityName))
            {
                rtnObject.Code = ApiReturnCode.InputError;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeInputError;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }
            //依據cityName查詢符合的cityGroup資料，好得到在地好康的區域城市的編號
            CityGroup cityGroup = CityManager.CityGroupGetByCityName(cityName, CityGroupType.Subscription);

            if (cityGroup == null)
            {
                rtnObject.Code = ApiReturnCode.InputError;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeInputError;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }

            PponCity city = PponCityGroup.DefaultPponCityGroup.GetPponCityByCityId(cityGroup.RegionId);
            if (city == null)
            {
                rtnObject.Code = ApiReturnCode.InputError;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeInputError;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }

            //設定推播訂閱
            if (NotificationFacade.SetSubscriptionNotice(city.CategoryId, token, enabled, type, deviceId))
            {
                rtnObject.Code = ApiReturnCode.Success;
                rtnObject.Data = new { CategoryId = city.CategoryId, Enabled = enabled };
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
            }
            else
            {
                rtnObject.Code = ApiReturnCode.Error;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeError;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }

            ApiUserManager.ApiUsedLogSet(apiUser, "NotificationService/SetNoticeTokenByCityName"
                , jsonS.Serialize(new { token = token, categoryId = city.CategoryId, enabled = enabled, userId = userId, mobileType = mobileType, deviceId = deviceId }));
        }
        /// <summary>
        /// 計算推播點擊率
        /// </summary>
        /// <param name="pushId">推播編號</param>
        /// <param name="deviceId">裝置編號</param>
        /// <param name="userId"></param>
        [WebMethod]
        public void ClickNotification(int pushId, string deviceId, string userId)
        {
            ApiReturnObject rtnObject = new ApiReturnObject();
            //驗證使用者
            var jsonS = new JsonSerializer();
            //設定輸出格式為json格式
            this.Context.Response.ContentType = "application/json";
            this.Context.Response.Charset = Encoding.UTF8.WebName;
            //驗證使用者
            var apiUser = ApiUserManager.ApiUserGetByUserId(userId);
            if (apiUser == null)
            {
                rtnObject.Code = ApiReturnCode.ApiUserIdError;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeApiUserIdError;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                SetApiLog(GetMethodName("ClickNotification"), userId, new { pushId, deviceId, userId }, rtnObject);
                return;
            }

            int updateCount = NotificationFacade.PushAppViewCountAddOne(pushId, (ApiUserDeviceType)apiUser.UserDeviceType);

            rtnObject.Code = ApiReturnCode.Success;
            rtnObject.Data = updateCount;
            SetApiLog(GetMethodName("ClickNotification"), userId, new { pushId, deviceId, userId }, rtnObject);
            this.Context.Response.Write(jsonS.Serialize(rtnObject));
        }
    }
}

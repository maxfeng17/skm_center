﻿using log4net;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.Core;
using System.Collections.Generic;
using System.Text;
using System.Web.Services;
using System.Linq;
using LunchKingSite.BizLogic.Component;
using Newtonsoft.Json.Linq;

namespace LunchKingSite.Web.WebService
{
    /// <summary>
    ///EinvoiceService 的摘要描述
    /// </summary>
    [WebService(Namespace = "http://www.17life.com/WebService")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // 若要允許使用 ASP.NET AJAX 從指令碼呼叫此 Web 服務，請取消註解下列一行。
    [System.Web.Script.Services.ScriptService]
    public class EinvoiceService : System.Web.Services.WebService
    {
        [WebMethod]
        //[ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public string CheckCarrid(string carrid)
        {
            try
            {
                string TxID = System.DateTime.Now.ToString("yyyyMMddHHmmss"); //暫無用途
                string targetUrl = string.Format(@"https://www-vc.einvoice.nat.gov.tw/BIZAPIVAN/biz?version=1.0&action=bcv&barCode={0}&TxID={1}&appId=EINV1201410300635", System.Web.HttpUtility.UrlEncode(carrid), TxID);

                System.Net.HttpWebRequest request = (System.Net.HttpWebRequest)System.Net.HttpWebRequest.Create(targetUrl);
                request.Method = "POST";
                request.ContentType = "application/x-www-form-urlencoded";
                request.Timeout = 10000;

                string result = "";
                // 取得回應資料
                using (System.Net.HttpWebResponse response = (System.Net.HttpWebResponse)request.GetResponse())
                {
                    using (System.IO.StreamReader sr = new System.IO.StreamReader(response.GetResponseStream()))
                    {
                        result = sr.ReadToEnd();
                    }
                }
                JObject jb = Newtonsoft.Json.JsonConvert.DeserializeObject<JObject>(result);
                return jb.Property("code").Value.ToString() == "200" ? jb.Property("isExist").Value.ToString() : string.Empty;
                //return "N";
            }
            catch
            {
                //發生不明錯誤則PASS手機載具驗證
                return "P";
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using LunchKingSite.Core.Component;
using LunchKingSite.Core;

namespace LunchKingSite.Web.WebService
{
    /// <summary>
    /// Summary description for BannerService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class PartnerBannerService : System.Web.Services.WebService
    {
        [WebMethod]
        public string[] GetVBOBanners()
        {
            ICmsProvider cmsProv = ProviderFactory.Instance().GetProvider<ICmsProvider>();
            string[] result = cmsProv.GetViewCmsRandomCollection("", RandomCmsType.VBO)
                .OrderBy(t => t.StartTime).Select(t=>t.Body).ToArray();
            return result;
        }
    }
}

﻿using System;
using System.Text;
using System.Web;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.Core;

namespace LunchKingSite.Web.WebService
{
    public class BaseService : System.Web.Services.WebService
    {
        protected static JsonSerializer jsonSerializer = new JsonSerializer();


        protected void ContextBaseSetting()
        {
            this.Context.Response.ContentType = "application/json";
            this.Context.Response.Charset = Encoding.UTF8.WebName;
        }


        protected string GetMethodName(string methodName)
        {
            Type type = this.GetType();
            return string.Format("{0}.{1}", type.Name, methodName);
        }
        /// <summary>
        /// 儲存LOG紀錄
        /// </summary>
        /// <param name="methodName">函式名稱</param>
        /// <param name="apiUserId">呼叫函式的apiUserId</param>
        /// <param name="parameter">物件，紀錄傳入參數</param>
        /// <param name="returnValue">物件，紀錄回傳參數</param>
        /// <returns></returns>
        protected bool SetApiLog(string methodName, string apiUserId, object parameter, object returnValue)
        {
            return SystemFacade.SetApiLog(methodName, apiUserId, parameter, returnValue);
        }
        /// <summary>
        /// 檢查apiUser是否存在
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="rtnObject"></param>
        /// <returns></returns>
        protected bool CheckApiUser(string userId, out ApiReturnObject rtnObject)
        {
            rtnObject = new ApiReturnObject() { Code = ApiReturnCode.Success };

            //驗證使用者
            var apiUser = ApiUserManager.ApiUserGetByUserId(userId);
            if (apiUser == null)
            {
                rtnObject.Code = ApiReturnCode.ApiUserIdError;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeApiUserIdError;
                return false;
            }
            return true;
        }
        /// <summary>
        /// 檢查會員是否已登入
        /// </summary>
        /// <param name="memberName"></param>
        /// <param name="rtnObject"></param>
        /// <returns></returns>
        protected bool CheckMemberSingin(out string memberName, out ApiReturnObject rtnObject)
        {
            memberName = string.Empty;
            rtnObject = new ApiReturnObject() { Code = ApiReturnCode.Success };

            if (HttpContext.Current.User.Identity.IsAuthenticated == false)
            {
                rtnObject.Code = ApiReturnCode.UserNoSignIn;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeUserNoSignIn;
                return false;
            }

            memberName = HttpContext.Current.User.Identity.Name;
            if (string.IsNullOrWhiteSpace(memberName))
            {
                rtnObject.Code = ApiReturnCode.UserNoSignIn;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeUserNoSignIn;
                return false;
            }
            return true;
        }
    }
}
﻿using System.Security.Principal;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using LunchKingSite.BizLogic.Facade;

namespace LunchKingSite.Web.WebService
{
    /// <summary>
    /// Summary description for MarketingService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [ScriptService]
    public class MarketingService : System.Web.Services.WebService
    {
        /// <summary>
        /// 品生活訂閱電子報且為visa特別卡的會員
        /// 給登入會員，且是visa特定卡的用戶，在購買品生活的visa特定檔次後，加入成為 visa 專門會員(在訂閱電子報加註記)
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        [ScriptMethod]
        public bool SubscribeHiDealVisaMember(int cardType)
        {
            IIdentity user = HttpContext.Current.User.Identity;
            if (user.IsAuthenticated == false)
            {
                HttpContext.Current.Response.StatusCode = 500;
                HttpContext.Current.Response.StatusDescription = "使用者未登入。";
                return false;
            }
            try
            {
                int userId = MemberFacade.GetUniqueId(user.Name);
                HiDealMailFacade.SubscribeVisaMember(userId, cardType);
                return true;
            }
            catch
            {
                HttpContext.Current.Response.StatusCode = 500;
                HttpContext.Current.Response.StatusDescription = "系統繁忙中，請稍後再試。";
                return false;
            }
        }

        /// <summary>
        /// 品生活訂閱電子報
        /// 給登入會員
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        [ScriptMethod]
        public bool SubscribeHiDeal()
        {
            IIdentity user = HttpContext.Current.User.Identity;
            if (user.IsAuthenticated == false)
            {
                HttpContext.Current.Response.StatusCode = 500;
                HttpContext.Current.Response.StatusDescription = "使用者未登入。";
                return false;
            }
            try
            {
                int userId = MemberFacade.GetUniqueId(user.Name);
                HiDealMailFacade.SubscribeEdm(userId);
                return true;
            }
            catch
            {
                HttpContext.Current.Response.StatusCode = 500;
                HttpContext.Current.Response.StatusDescription = "系統繁忙中，請稍後再試。";
                return false;
            }
        }
    }
}

﻿using log4net;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using System;
using System.Text;
using System.Web;
using System.Web.Services;

namespace LunchKingSite.Web.WebService
{
    /// <summary>
    /// Summary description for ZeroPayService
    /// </summary>
    // [WebService(Namespace = "http://tempuri.org/")]
    [WebService(Namespace = "http://www.17life.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
    [System.Web.Script.Services.ScriptService]
    public class ZeroPayService : System.Web.Services.WebService
    {
        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        private static IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        private static IOrderProvider op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
        private static IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        protected static ILog Logger = LogManager.GetLogger("ZeroPayService");
        private static JsonSerializer jsonSerializer = new JsonSerializer();
        private static Object CouponLock = new Object();

        [WebMethod]
        public void GetUse17PayMsg(string userId)
        {
            ApiReturnObject rtnObject = new ApiReturnObject() { Code = ApiReturnCode.Success };
            //設定輸出格式為json格式
            this.Context.Response.ContentType = "application/json";
            this.Context.Response.Charset = Encoding.UTF8.WebName;
            //驗證使用者
            var apiUser = ApiUserManager.ApiUserGetByUserId(userId);
            if (apiUser == null)
            {
                rtnObject.Code = ApiReturnCode.ApiUserIdError;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeApiUserIdError;
                this.Context.Response.Write(jsonSerializer.Serialize(rtnObject));
                return;
            }

            rtnObject.Data = new { UnUsed17PayMsg = config.UnUsed17PayMsg, Used17PayMsg = config.Used17PayMsg };
            rtnObject.Message = "";
            rtnObject.Code = ApiReturnCode.Success;
            Context.Response.Write(jsonSerializer.Serialize(rtnObject));
        }

        [WebMethod]
        public void MakeOrderByApiUser(string dtoString, string userId)
        {
            ApiReturnObject rtnObject = new ApiReturnObject() { Code = ApiReturnCode.Success };
            //設定輸出格式為json格式
            this.Context.Response.ContentType = "application/json";
            this.Context.Response.Charset = Encoding.UTF8.WebName;
            //驗證使用者
            var apiUser = ApiUserManager.ApiUserGetByUserId(userId);
            if (apiUser == null)
            {
                rtnObject.Code = ApiReturnCode.ApiUserIdError;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeApiUserIdError;
                this.Context.Response.Write(jsonSerializer.Serialize(rtnObject));
                return;
            }

            string userName = HttpContext.Current.User.Identity.Name;

            if (string.IsNullOrWhiteSpace(userName))
            {
                rtnObject.Code = ApiReturnCode.UserNoSignIn;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeUserNoSignIn;
                Context.Response.Write(jsonSerializer.Serialize(rtnObject));
                return;
            }
            PaymentDTO paymentDTO = null;
            try
            {
                paymentDTO = jsonSerializer.Deserialize<PaymentDTO>(dtoString);
                var payResult = PaymentFacade.ZeroPayMakeOrder(paymentDTO, userName,
                    (ApiUserDeviceType) apiUser.UserDeviceType);

                if (payResult.ReturnCode == ApiReturnCode.BadUserInfo)
                {
                    throw new Exception(payResult.Message);
                }

                if (payResult.ReturnCode == ApiReturnCode.Success)
                {
                    if (payResult.Data != null)
                    {
                        payResult.Data.UnUsed17PayMsg = config.UnUsed17PayMsg;
                        payResult.Data.Used17PayMsg = config.Used17PayMsg;
                        if (payResult.Data.IsSkm == true)
                        {
                            if (payResult.Data.SkmAvailabilities != null)
                            {
                                foreach (var item in payResult.Data.SkmAvailabilities)
                                {
                                    item.BrandCounterName = SkmFacade.PrefixStoreName(item.BrandCounterName);
                                }
                            }
                        }
                    }
                }

                rtnObject.Data = payResult.Data;
                rtnObject.Message = payResult.Message;
                rtnObject.Code = payResult.ReturnCode;
                Context.Response.Write(jsonSerializer.Serialize(rtnObject));
            }
            catch (Exception ex)
            {
                if (paymentDTO == null)
                {
                    Logger.Error(string.Format("{0}////{1}",
                        ex.StackTrace, ex.Message));
                }
                else
                {
                    Logger.Error(string.Format("{0}////{1}////{2}////{3}",
                        ex.StackTrace, ex.Message, paymentDTO.BusinessHourGuid, paymentDTO.DeliveryInfo.BuyerUserId));
                }
                this.Context.Response.Write(jsonSerializer.Serialize(rtnObject));
                return;
            }

            ApiUserManager.ApiUsedLogSet(apiUser, "/WebService/ZeroPayService.asmx/MakeOrderByApiUser",
                jsonSerializer.Serialize(new { dtoString = dtoString, userId = userId }));
        }
    }
}
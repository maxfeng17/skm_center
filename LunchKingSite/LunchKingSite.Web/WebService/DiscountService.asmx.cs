﻿using System.Web.Helpers;
using log4net;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.Core;
using System.Collections.Generic;
using System.Text;
using System.Web.Services;
using System.Linq;
using LunchKingSite.BizLogic.Component;

namespace LunchKingSite.Web.WebService
{
    /// <summary>
    /// Summary description for DiscountService
    /// </summary>
    [WebService(Namespace = "http://www.17life.com/WebService")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
    [System.Web.Script.Services.ScriptService]
    public class DiscountService : System.Web.Services.WebService
    {
        [WebMethod]
        public string GetDiscountAmount(string code)
        {
            int minimum_amount;
            var amount = PromotionFacade.GetAmountFromDiscountCode(code.ToUpper(), out minimum_amount);
            if (amount != -1)
            {
                return amount.ToString();
            }
            else
            {
                return string.Empty;
            }
        }

        [WebMethod]
        public string GetDiscountAmountByUser(string code, string user_name)
        {
            KeyValuePair<decimal, decimal> kv = PromotionFacade.GetAmountFromDiscountCode(code.ToUpper(), user_name);
            var jsonCol = new { Discount = kv.Key, Minimum = kv.Value };
            return new JsonSerializer().Serialize(jsonCol);
        }

        [WebMethod]
        public string GetDiscountAmountByUserAndDep(string code, string user_name, int dep, string bid)
        {
            int flag;
            string msg;
            KeyValuePair<decimal, decimal> kv = PromotionFacade.GetAmountCheckDepFromDiscountCode(code.ToUpper(), user_name, dep, bid, out flag, out msg, false);
            var jsonCol = new { Discount = kv.Key, Minimum = kv.Value, Flag = flag, Message = msg };
            return new JsonSerializer().Serialize(jsonCol);
        }

        [WebMethod]
        public string GetDiscountAmountByUserAndDepForWeb(string code, string user_name, int dep, string bid)
        {
            int flag;
            string msg;
            KeyValuePair<decimal, decimal> kv = PromotionFacade.GetAmountCheckDepFromDiscountCode(code.ToUpper(), user_name, dep, bid, out flag, out msg, true);
            var jsonCol = new { Discount = kv.Key, Minimum = kv.Value, Flag = flag, Message = msg };
            return new JsonSerializer().Serialize(jsonCol);
        }

        [WebMethod]
        public string GetDiscountAmountJSon(string code)
        {
            var jsonS = new JsonSerializer();
            int minimum_amount;
            var amount = PromotionFacade.GetAmountFromDiscountCode(code.ToUpper(), out minimum_amount);

            ApiReturnObject rtnObject = new ApiReturnObject();
            if (amount != -1)
            {
                rtnObject.Code = ApiReturnCode.Success;
                rtnObject.Data = new { Amount = amount };
                rtnObject.Message = string.Empty;
            }
            else
            {
                rtnObject.Code = ApiReturnCode.InputError;
                rtnObject.Data = new { Amount = 0 };
                rtnObject.Message = I18N.Phrase.DiscountAmountJSonInputError;
            }

            return jsonS.Serialize(rtnObject);
        }

    }
}
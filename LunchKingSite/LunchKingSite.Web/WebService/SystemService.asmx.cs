﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Models;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Component;
using LunchKingSite.WebLib.Views;

namespace LunchKingSite.Web.WebService
{
    /// <summary>
    /// Summary description for SellerService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class SystemService : System.Web.Services.WebService
    {

        [WebMethod(true)]
        public void Mgm(int enable)
        {
            MGMFacade.EnableModuleTemporarily(enable == 1);
            var result =  new
            {
                enabled = MGMFacade.IsEnabled
            };
            this.Context.Response.Write(new JsonSerializer().Serialize(result));
        }
    }
}

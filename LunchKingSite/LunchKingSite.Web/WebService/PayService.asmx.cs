﻿using log4net;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Models;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Component;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Transactions;
using System.Web;
using System.Web.Services;
using LunchKingSite.Core.ModelCustom;
using PaymentType = LunchKingSite.Core.PaymentType;

namespace LunchKingSite.Web.WebService
{
    /// <summary>
    /// Summary description for PayService
    /// </summary>
    // [WebService(Namespace = "http://tempuri.org/")]
    [WebService(Namespace = "http://www.17life.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
    [System.Web.Script.Services.ScriptService]
    public class PayService : System.Web.Services.WebService
    {
        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        private static IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        private static ISellerProvider sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
        private static IOrderProvider op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
        private static IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        private static IItemProvider ip = ProviderFactory.Instance().GetProvider<IItemProvider>();
        private static ILocationProvider lp = ProviderFactory.Instance().GetProvider<ILocationProvider>();
        private static ILog logger = LogManager.GetLogger("PayService");
        private static JsonSerializer jsonSerializer = new JsonSerializer();

        /// <summary>
        /// 支援新版成套商品
        /// </summary>
        /// <param name="dtoString"></param>
        /// <param name="userId"></param>
        [WebMethod]
        public void MakeOrderByApiUserV2(string dtoString, string userId)
        {
            string methodName = "LunchKingSite.Web.WebService.MakeOrderByApiUserV2";
            var jsonS = new JsonSerializer();
            ApiReturnObject rtnObject = new ApiReturnObject() { Code = ApiReturnCode.Success };
            //設定輸出格式為json格式
            this.Context.Response.ContentType = "application/json";
            this.Context.Response.Charset = Encoding.UTF8.WebName;
            //驗證使用者
            ApiUser apiUser = ApiUserManager.ApiUserGetByUserId(userId);
            if (apiUser == null)
            {
                rtnObject.Code = ApiReturnCode.ApiUserIdError;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeApiUserIdError;
                this.Context.Response.Write(jsonS.Serialize(rtnObject));
                return;
            }

            HttpContext.Current.Items[LkSiteContextItem._API_USER_ID] = apiUser.UserId;
            
            string userName = HttpContext.Current.User.Identity.Name;

            if (string.IsNullOrWhiteSpace(userName))
            {
                rtnObject.Code = ApiReturnCode.UserNoSignIn;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeUserNoSignIn;
                this.Context.Response.Write(jsonSerializer.Serialize(rtnObject));
                return;
            }

            Helper.SetContextItem(LkSiteContextItem._DEVICE_TYPE, apiUser.UserDeviceType);

            PaymentDTO paymentDTO = null;
            try
            {
                int memberId = MemberFacade.GetUniqueId(userName);
                if (config.LogPaymentDTO)
                {
                    OrderFacade.AddOrderLog(memberId, dtoString, Helper.GetOrderFromType());
                }
                paymentDTO = jsonSerializer.Deserialize<PaymentDTO>(dtoString);
                AdjustPaymentDTOExtraInfo(paymentDTO);

                Helper.SetContextItem(LkSiteContextItem.Bid, paymentDTO.BusinessHourGuid);

                if (paymentDTO.ApplePayTokenData != null)
                {
                    logger.Info("apple pay token Data:" + paymentDTO.ApplePayTokenData.Data);
                    logger.Info("apple pay token Signature:" + paymentDTO.ApplePayTokenData.Signature);
                    logger.Info("apple pay token Version:" + paymentDTO.ApplePayTokenData.Version);
                    logger.Info("apple pay token EphemeralPublicKey:" + paymentDTO.ApplePayTokenData.Header.EphemeralPublicKey);
                    logger.Info("apple pay token PublicKeyHash:" + paymentDTO.ApplePayTokenData.Header.PublicKeyHash);
                    logger.Info("apple pay token TransactionId:" + paymentDTO.ApplePayTokenData.Header.TransactionId);
                }

                var result = PaymentFacade.PaymentForApp(paymentDTO, userName, true);
                rtnObject.Code = result.ReturnCode;
                rtnObject.Message = result.Message;
                rtnObject.Data = result.Data;

                Member mem = MemberFacade.GetMember(memberId);
                if (mem.IsLoaded && mem.IsFraudSuspect)
                {
                    rtnObject.Code = ApiReturnCode.CreditcardInfoError;
                    rtnObject.Message = I18N.Message.CreditcardInfoError;
                }

                if (rtnObject.Code != ApiReturnCode.Success)
                {
                    Order o = op.OrderGet(paymentDTO.OrderGuid);
                    if (o.IsLoaded)
                        PaymentFacade.CancelPaymentTransactionByOrderForIncompleteOrder(o, "sys"); //自動退貨
                }
                
                this.Context.Response.Write(jsonSerializer.Serialize(rtnObject));

                if (!(paymentDTO.IsOTP && !paymentDTO.IsFinishOTP) && paymentDTO.MainPaymentType != ApiMainPaymentType.LinePay)
                {
                    //OTP但還沒過先不做
                    
                    #region PushRecardOrder

                    PaymentFacade.PushRecardOrder(paymentDTO, memberId, result);

                    #endregion

                    paymentDTO.CreditcardNumber = Helper.MaskCreditCard(paymentDTO.CreditcardNumber);
                    paymentDTO.CreditcardSecurityCode = Helper.MaskCreditCardSecurityCode(paymentDTO.CreditcardSecurityCode);
                    SystemFacade.SetApiLog(methodName, userId, paymentDTO, null);
                }
            }
            catch (Exception ex)
            {
                try
                {
                    Order o = op.OrderGet(paymentDTO.OrderGuid);
                    if (o.IsLoaded)
                    {
                        PaymentFacade.CancelPaymentTransactionByOrderForIncompleteOrder(o, "sys"); //自動退貨
                    } 
                    else
                    {
                        logger.Error(string.Format("找到 Order 但 transId  未設定 OrderGuid, transId=:{0}", paymentDTO == null ? "" : paymentDTO.TransactionId), ex);
                    }

                    logger.Error(string.Format("an error msg at transId:{0}", paymentDTO == null ? "" : paymentDTO.TransactionId), ex);


                    paymentDTO = jsonSerializer.Deserialize<PaymentDTO>(dtoString);
                    paymentDTO.CreditcardNumber = Helper.MaskCreditCard(paymentDTO.CreditcardNumber);
                    paymentDTO.CreditcardSecurityCode = Helper.MaskCreditCardSecurityCode(paymentDTO.CreditcardSecurityCode);
                    SystemFacade.SetApiLog(methodName, userId, paymentDTO, new { Result = false, Error = ex });
                }
                catch (Exception ex2)
                {
                    SystemFacade.SetApiLog(methodName, userId, paymentDTO, new { Result = false, Error = ex, Error2 = ex2 });
                }

                
            }
        }

        

        #region private methods

        private void AdjustPaymentDTOExtraInfo(PaymentDTO paymentDTO)
        {
            //PaymentMethod
            if (paymentDTO.DeliveryInfo.PaymentMethod == BuyTransPaymentMethod.Init)
            {
                if (paymentDTO.IsPaidByATM)
                {
                    paymentDTO.DeliveryInfo.PaymentMethod = BuyTransPaymentMethod.Atm;
                }
                else if (paymentDTO.IsMasterPass)
                {
                    paymentDTO.DeliveryInfo.PaymentMethod = BuyTransPaymentMethod.MasterPass;
                }
                else if (paymentDTO.CreditCardGuid != Guid.Empty || string.IsNullOrEmpty(paymentDTO.CreditcardNumber) == false)
                {
                    paymentDTO.DeliveryInfo.PaymentMethod = BuyTransPaymentMethod.CreaditCardPayOff;
                }
                else if (paymentDTO.Amount == 0)
                {
                    paymentDTO.DeliveryInfo.PaymentMethod = BuyTransPaymentMethod.BonusOrScashPayOff;
                }
                else if (paymentDTO.ApplePayTokenData != null)
                {
                    paymentDTO.DeliveryInfo.PaymentMethod = BuyTransPaymentMethod.ApplePay;
                }
                else if (paymentDTO.Amount > 0 && paymentDTO.DeliveryInfo.ProductDeliveryType == ProductDeliveryType.FamilyPickup)
                {
                    paymentDTO.DeliveryInfo.PaymentMethod = BuyTransPaymentMethod.FamilyIsp;
                }
                else if (paymentDTO.Amount > 0 && paymentDTO.DeliveryInfo.ProductDeliveryType == ProductDeliveryType.SevenPickup)
                {
                    paymentDTO.DeliveryInfo.PaymentMethod = BuyTransPaymentMethod.SevenIsp;
                }
                else if (paymentDTO.Amount > 0 && paymentDTO.MainPaymentType == ApiMainPaymentType.LinePay)
                {
                    paymentDTO.DeliveryInfo.PaymentMethod = BuyTransPaymentMethod.LinePay;
                }
            }

            //PayType
            switch (paymentDTO.DeliveryInfo.PaymentMethod)
            {
                case BuyTransPaymentMethod.CreaditCardInstallment:
                case BuyTransPaymentMethod.CreaditCardPayOff:
                case BuyTransPaymentMethod.ApplePay:
                case BuyTransPaymentMethod.MasterPass:
                    paymentDTO.DeliveryInfo.PayType = PaymentType.Creditcard;
                    break;
                case BuyTransPaymentMethod.Atm:
                    paymentDTO.DeliveryInfo.PayType = PaymentType.ATM;
                    break;
                case BuyTransPaymentMethod.TaishinPay:
                    paymentDTO.DeliveryInfo.PayType = PaymentType.TaishinPay;
                    break;
                case BuyTransPaymentMethod.LinePay:
                    paymentDTO.DeliveryInfo.PayType = PaymentType.LinePay;
                    break;
                case BuyTransPaymentMethod.FamilyIsp:
                    paymentDTO.DeliveryInfo.PayType = PaymentType.FamilyIsp;
                    break;
                case BuyTransPaymentMethod.SevenIsp:
                    paymentDTO.DeliveryInfo.PayType = PaymentType.SevenIsp;
                    break;
            }
        }
        
        #endregion private methods
    }
}
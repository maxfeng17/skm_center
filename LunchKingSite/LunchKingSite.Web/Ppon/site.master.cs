﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib;
using System;
using System.Web;
using System.Web.UI;
using LunchKingSite.WebLib.Models.Mobile;
using LunchKingSite.WebLib.Component;

public partial class site : System.Web.UI.MasterPage
{
    protected const string imgPath = "~/Themes/default/images/ui/";

    private ISysConfProvider cp = ProviderFactory.Instance().GetConfig();

    public ISysConfProvider SystemConfig
    {
        get
        {
            return cp;
        }
    }

    public bool BypassSetNowUrl { get; set; }

    /// <summary>
    /// 新版CPA
    /// </summary>
    public string Cpa
    {
        get
        {
            if (Page.RouteData.Values["cpa"] != null)
            {
                return Page.RouteData.Values["cpa"].ToString().Replace("cpa-", "").Replace("CPA-", "");
            }
            else
            {
                return HttpContext.Current.Request.QueryString["cpa"] == null ? string.Empty : HttpContext.Current.Request.QueryString["cpa"].ToString().Replace("cpa-", "").Replace("CPA-", "");
            }
        }
        
    }

    public string Rsrc
    {
        get
        {
            string Qrsrc = HttpContext.Current.Request.QueryString["rsrc"];
            if (Page.RouteData.Values["rsrc"] != null) ///處理 {bid}/YH_BMC?rsrc=G_search 情況
            {
                if (!string.IsNullOrEmpty(Qrsrc))
                {
                    if (Qrsrc.ToLower()  == "G_search".ToLower())
                    {
                        return "G_search";
                    }
                }
                return Page.RouteData.Values["rsrc"].ToString();
            }
            else if (!string.IsNullOrEmpty(Qrsrc))
            {
                if (Qrsrc.IndexOf(',') > 0) //處理 rsrc=YH_BMC,G_search 情況
                {
                   Boolean IsFirst=true;
                    string rightrsrc=string.Empty;
                    foreach (string temprsrc in Qrsrc.Split(','))
                    {
                        if (IsFirst)
                        {
                            rightrsrc = temprsrc;
                        }
                        
                        if (temprsrc.ToLower() == "G_search".ToLower())
                        {
                            return "G_search";
                        }
                        IsFirst=false;
                    }
                    return rightrsrc;
                }
                else
                {
                    if (Qrsrc.IndexOf('?') > 0) //處理?rsrc=YH_BMC?rsrc=G_search 情況
                    {                        
                        string[] SPrsrc = Qrsrc.Split('?');
                        if (SPrsrc[1].Replace("rsrc=", "").ToLower() == "G_search".ToLower())
                        {
                            return "G_search";
                        }
                        else
                        {
                            return SPrsrc[0];
                        }
                    }
                    else //一般情況 rsrc=G_search
                        return Qrsrc;
                }

            }
            else
            {
                return string.Empty;
            }
        }
    }

    protected static ISysConfProvider config = ProviderFactory.Instance().GetConfig();

    /// <summary>
    /// 搜尋
    /// </summary>
    public string Search
    {
        get
        {
            string search = string.Empty;
            if (HttpContext.Current.Request.QueryString["search"] != null)
            {
                search = HttpContext.Current.Request.QueryString["search"].ToString();
                Session[LkSiteSession.Search.ToString()] = search;
            }
            else if (Session[LkSiteSession.Search.ToString()] != null)
            {
                search = Session[LkSiteSession.Search.ToString()].ToString();
            }
            return search;
        }
    }

    public string FacebookAppId
    {
        get
        {
            return ProviderFactory.Instance().GetConfig().FacebookApplicationId;
        }
    }

    public string WebUrl
    {
        get
        {
            return Request.ApplicationPath == "/" ? Request.ApplicationPath : (Request.ApplicationPath + "/");
        }
    }

    public bool ViewPortEnabled
    {
        set
        {
            viewport.Visible = value; 
        }
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        if (!BypassSetNowUrl)
        {
            SetNowUrlIntoSession();
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        string currentPage = Page.AppRelativeVirtualPath.ToLower();
        if (currentPage.IndexOf("/default.aspx") < 0 && currentPage.IndexOf("/delivery.aspx") < 0 && currentPage.IndexOf("/todaydeals.aspx") < 0 && currentPage.IndexOf("/piinlife") < 0)
        {
            bool isCustomizedPageTitle = false;

            string sbid = null;
            if (Page.RouteData.Values["bid"] != null)
            {
                sbid = Page.RouteData.Values["bid"].ToString();
            }
            else if (this.Page.Request.QueryString["bid"] != null)
            {
                sbid = this.Page.Request.QueryString["bid"];
            }
            Guid bid;
            
            if (string.IsNullOrEmpty(sbid) == false && Guid.TryParse(sbid, out bid))
            {
                IViewPponDeal deal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(bid, true);
                if (string.IsNullOrWhiteSpace(deal.PageTitle) == false)
                {
                    isCustomizedPageTitle = true;
                }
            }

            if (string.IsNullOrEmpty(Page.Title))
            {
                Page.Title = SystemConfig.DefaultTitle;
            }
            else if (isCustomizedPageTitle == false)
            {
                Page.Title = string.Format("{0} - {1}", Page.Title, SystemConfig.Title);
            }

            Page.MetaDescription = (string.IsNullOrEmpty(Page.MetaDescription)) ? SystemConfig.DefaultMetaDescription : Page.MetaDescription;
        }

        if (!IsPostBack && SystemConfig.NewCpaEnable)
        {
            #region NewCpa

            try
            {
                CpaClientMain<CpaClientEvent<CpaClientDeail>> client_cpa_data;
                if (PponFacade.CpaCheck(Rsrc, Cpa, Search, Request.Url.AbsoluteUri, Session[LkSiteSession.NewCpa.ToString()], out client_cpa_data))
                {
                    Session[LkSiteSession.NewCpa.ToString()] = client_cpa_data;
                }
            }
            catch
            (Exception ex)
            {
                log4net.LogManager.GetLogger("LogToDb").Error(
                    string.Format("NewCpa Expection =>site.master=>PponFacade.CpaCheck({0},{1},{2},{3},) ,Exception={4}",
                    Rsrc,
                    Cpa,
                    Request.Url.AbsoluteUri,
                    Session[LkSiteSession.NewCpa.ToString()],
                    ex
                    ));
            }

            #endregion NewCpa
        }
    }

    protected void SM1_AsyncPostBackError(object sender, AsyncPostBackErrorEventArgs e)
    {
        WebUtility.LogExceptionAnyway(e.Exception);
    }

    /// <summary>
    /// 將目前網址寫到Session中
    /// </summary>
    protected void SetNowUrlIntoSession()
    {
        string url = Request.Url.AbsoluteUri;
        if (url.ToLower().IndexOf("newmember/") < 0)
        {
            Session[LkSiteSession.NowUrl.ToString()] = url;
        }
    }

    public bool isSearchPage(string name)
    {
        return name == "ASP.ppon_pponsearch_aspx";
    }
}
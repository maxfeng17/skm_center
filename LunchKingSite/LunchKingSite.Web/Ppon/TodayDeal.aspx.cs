﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Models;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.I18N;
using LunchKingSite.WebLib;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using LunchKingSite.WebLib.Component;
using LunchKingSite.WebLib.Models.Ppon;
namespace LunchKingSite.Web.Ppon
{
    public partial class TodayDeal : BasePage, ITodayDealPponView
    {
        #region event

        public event EventHandler CheckEventMail;

        #endregion event

        #region Props

        public const string CPA_HOT_DEALS = "cpa-17_hotdeals";

        public int Bonus = 50;

        private TodayDealPponPresenter _presenter;

        public TodayDealPponPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        /// <summary>
        /// 主要檔次
        /// </summary>
        private List<ViewMultipleMainDeal> _viewMultipleMainDeals;

        public List<ViewMultipleMainDeal> ViewMultipleMainDeals
        {
            get { return _viewMultipleMainDeals; }
        }

        /// <summary>
        /// 今日熱銷
        /// </summary>
        private List<ViewMultipleSideDeal> _viewMultipleHotDeals;

        public List<ViewMultipleSideDeal> ViewMultipleHotDeals
        {
            get { return _viewMultipleHotDeals; }
        }

        /// <summary>
        /// 由master page取得的cityid
        /// </summary>
        private int _cityId;

        public int CityId
        {
            get
            {
                return Master.SelectedCityId;
            }
            set
            {
                Master.SelectedPponCity = PponCityGroup.DefaultPponCityGroup.GetPponCityByCityId(value);
            }
        }

        private int _dealCityId;
        public int DealCityId
        {
            set
            {
                _dealCityId = value;
            }
            get
            {
                if (_dealCityId == 0)
                {
                    return Master.SelectedCityId;
                }
                else
                {
                    return _dealCityId;
                }
            }
        }

        public int PponCategory
        {
            get
            {
                if (PponCityGroup.DefaultPponCityGroup.GetPponCityByCityId(CityId) != null)
                {
                    return PponCityGroup.DefaultPponCityGroup.GetPponCityByCityId(CityId).CategoryId;
                }
                else
                {
                    return PponCityGroup.DefaultPponCityGroup.GetPponCityByCityId(PponCityGroup.DefaultPponCityGroup.TaipeiCity.CityId).CategoryId;
                }
            }
        }

        public int TravelCategoryId
        {
            get
            {
                HttpCookie cookie = Request.Cookies.Get(LkSiteCookie.TravelCategoryId.ToString("g"));

                if (cookie == null)
                {
                    cookie = CookieManager.NewCookie(
                        LkSiteCookie.TravelCategoryId.ToString("g"),
                        CategoryManager.GetDefaultCategoryNode().CategoryId.ToString(),
                        DateTime.Now.AddDays(1)
                    );
                    Response.Cookies.Add(cookie);
                }

                int cid = int.TryParse(cookie.Value, out cid) ? cid : CategoryManager.GetDefaultCategoryNode().CategoryId;
                return cid == 0 ? CategoryManager.GetDefaultCategoryNode().CategoryId : cid;
            }
        }

        public int FemaleCategoryId
        {
            get
            {
                HttpCookie cookie = Request.Cookies.Get(LkSiteCookie.FemaleCategoryId.ToString("g"));

                if (cookie == null)
                {
                    cookie = CookieManager.NewCookie(
                        LkSiteCookie.FemaleCategoryId.ToString("g"),
                        CategoryManager.Default.FemaleTaipei.CategoryId.ToString(),
                        DateTime.Now.AddDays(1)
                    );
                    Response.Cookies.Add(cookie);
                }

                int cid = int.TryParse(cookie.Value, out cid) ? cid : CategoryManager.Default.FemaleTaipei.CategoryId;
                return cid == 0 ? CategoryManager.Default.FemaleTaipei.CategoryId : cid;
            }
        }

        public int SubRegionCategoryId
        {
            get
            {


                string subcategoryString = string.Empty;
                if (Request.QueryString["subc"] != null)
                {
                    subcategoryString = Request.QueryString["subc"];
                }
                HttpCookie cookie = Request.Cookies.Get(LkSiteCookie.SubRegionCategroyId.ToString("g"));
                if (cookie == null)
                {
                    cookie = CookieManager.NewCookie(
                        LkSiteCookie.SubRegionCategroyId.ToString("g"),
                        subcategoryString,
                        DateTime.Now.AddDays(1)
                    );
                    Response.Cookies.Add(cookie);
                }
                else
                {
                    cookie.Value = subcategoryString;
                }
                int cid = int.TryParse(cookie.Value, out cid) ? cid : CategoryManager.GetDefaultCategoryNode().CategoryId;
                return cid == 0 ? CategoryManager.GetDefaultCategoryNode().CategoryId : cid;
            }
        }

        public List<int> FilterCategoryIdList
        {
            get
            {
                List<int> data = new List<int>();

                if (Request.QueryString["filter"] != null)
                {
                    string filterString = Request.QueryString["filter"];
                    string[] filterList = filterString.Split(",");
                    foreach (var categoryID in filterList)
                    {
                        int i;
                        if (int.TryParse(categoryID, out i))
                        {
                            data.Add(i);
                        }
                    }
                }
                return data;
            }
        }

        public List<CategoryNode> DealChannelList { get; set; }

        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();

        public static ISysConfProvider SystemConfig
        {
            get
            {
                return config;
            }
        }

        public Guid BusinessHourId
        {
            get
            {
                string bid_querystring = string.Empty;
                Guid bGuid = Guid.Empty;
                if (Page.RouteData.Values["bid"] != null)
                {
                    bid_querystring = Page.RouteData.Values["bid"].ToString();
                }
                else if (!string.IsNullOrWhiteSpace(Request.QueryString["bid"]))
                {
                    bid_querystring = Request.QueryString["bid"];
                }

                if (!string.IsNullOrWhiteSpace(bid_querystring))
                {
                    if (bid_querystring.Split(',').Length > 1)
                    {
                        bid_querystring = bid_querystring.Split(',')[0];
                    }

                    if (!Guid.TryParse(bid_querystring, out bGuid))
                    {
                        bGuid = Guid.Empty;
                    }
                }
                else if (ViewState["bid"] != null)
                {
                    bGuid = new Guid(ViewState["bid"].ToString());
                }

                return bGuid;
            }

            set
            {
                ViewState["bid"] = value;
            }
        }

        /// <summary>
        /// 檔號，目前先隱藏檔號判斷檔次，直接回傳-1
        /// </summary>
        public int BusinessHourUniqueId
        {
            get
            {
                return -1;
            }
            set
            {
                ViewState["uid"] = value;
            }
        }

        /// <summary>
        /// rsrc參數
        /// </summary>
        public string Rsrc
        {
            get
            {
                if (Page.RouteData.Values["rsrc"] != null)
                {
                    return Page.RouteData.Values["rsrc"].ToString();
                }
                else if (!string.IsNullOrWhiteSpace(Request.QueryString["rsrc"]))
                {
                    return Request.QueryString["rsrc"];
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        /// <summary>
        /// Hami
        /// </summary>
        public string Hami
        {
            get
            {
                if (Page.RouteData.Values["rsrc"] != null && Page.RouteData.Values["rsrc"].ToString().ToLower() == "hinet_hammi")
                {
                    Session[LkSiteSession.Hami.ToString()] = Page.RouteData.Values["rsrc"].ToString().ToLower();
                }
                else if (!string.IsNullOrWhiteSpace(Request.QueryString["rsrc"]) && Request.QueryString["rsrc"].ToLower() == "hinet_hammi")
                {
                    Session[LkSiteSession.Hami.ToString()] = Request.QueryString["rsrc"].ToLower();
                }
                return Session[LkSiteSession.Hami.ToString()] != null ? Session[LkSiteSession.Hami.ToString()].ToString() : string.Empty;
            }
        }

        public CategorySortType SortType
        {
            get
            {
                if (Session["SortType"] == null)
                {
                    Session["SortType"] = CategorySortType.Default;
                }

                return (CategorySortType)Session["SortType"];
            }
            set
            {
                Session["SortType"] = value;
            }
        }

        public string GoogleApiKey
        {
            get
            {
                return ProviderFactory.Instance().GetConfig().GoogleMapsAPIKey;
            }
        }

        public string FB_AppId
        {
            get
            {
                return ProviderFactory.Instance().GetConfig().FacebookApplicationId;
            }
        }
        /// <summary>
        /// App Title or CouponUsage
        /// </summary>
        public string AppTitle { set; get; }
        /// <summary>
        /// meta og:title content
        /// </summary>
        public string OgTitle { set; get; }
        /// <summary>
        /// meta og:url content
        /// </summary>
        public string OgUrl { set; get; }
        /// <summary>
        /// meta og:description content
        /// </summary>
        public string OgDescription { set; get; }
        /// <summary>
        /// meta og:image content
        /// </summary>
        public string OgImage { set; get; }
        /// <summary>
        /// link rel=canonical href
        /// </summary>
        public string LinkCanonicalUrl { set; get; }
        /// <summary>
        /// link rel=image_src href
        /// </summary>
        public string LinkImageSrc { set; get; }

        public int Encores { get; set; }

        public string UserName
        {
            get
            {
                return Page.User.Identity.Name;
            }
        }

        public int UserId
        {
            get
            {
                return MemberFacade.GetUniqueId(Page.User.Identity.Name);
            }
        }

        //前端資料以商品的憑證宅配撈取20120314
        public DeliveryType[] RepresentedDeliverytype
        {
            get
            {
                return new DeliveryType[2] { DeliveryType.ToHouse, DeliveryType.ToShop };
            }
        }

        private bool isDeliverDeal = false;
        public bool IsDeliveryDeal
        {
            set
            {
                this.isDeliverDeal = value;
            }
            get
            {
                return this.isDeliverDeal;
            }
        }

        public bool TwentyFourHours { get; set; }

        public bool ShowEventEmail { get; set; }

        public string HamiTicketId
        {
            get
            {
                if (!string.IsNullOrWhiteSpace(Request.QueryString["TicketNo"]))
                {
                    return Request.QueryString["TicketNo"];
                }
                return null;
            }
        }

        public int? DealType { get; set; }

        //public bool EntrustSell
        //{
        //    set
        //    {
        //        liEntrustSell.Visible = value;
        //    }
        //}

        public string PicAlt { get; set; }

        public bool isUseNewMember { get; set; }

        public int CategoryID
        {
            get
            {
                string cat_querystring = string.Empty;
                int cat;
                if (Page.RouteData.Values["cat"] != null)
                {
                    cat_querystring = Page.RouteData.Values["cat"].ToString();
                }
                else if (!string.IsNullOrWhiteSpace(Request.QueryString["cat"]))
                {
                    cat_querystring = Request.QueryString["cat"];
                }

                if (!string.IsNullOrWhiteSpace(cat_querystring) && int.TryParse(cat_querystring, out cat))
                {
                    return cat;
                }
                else
                {
                    return CategoryManager.GetDefaultCategoryNode().CategoryId;
                }
            }
        }

        public string CategoryName
        {
            get
            {
                Category cate = CategoryManager.CategoryGetById(CategoryID);
                return cate != null ? cate.Name : string.Empty;
            }
        }

        public DateTime Now
        {
            get
            {
                return DateTime.Now;
            }
        }

        public bool isMultipleMainDeals { get; set; }

        public bool BypassSsoVerify { get; set; }

        //public IExpireNotice ExpireNotice
        //{
        //    get
        //    {
        //        return PponExpireNotice;
        //    }
        //}
        public bool IsNewMobileSetting
        {
            get { return config.IsNewMobileSetting; }
        }

        public string EdmPopUpCacheName { get; set; }

        //public string CityName
        //{
        //    set
        //    {
        //        liCityNamem.Text = liCityName.Text = value;
        //    }
        //}

        public bool IsOpenNewWindow { get; set; }

        public bool IsMobileBroswer
        {
            get
            {
                return CommonFacade.ToMobileVersion();
            }
        }

        private bool _isKindDeal;

        public bool IsKindDeal
        {
            get
            {
                return _isKindDeal;
            }
            set
            {
                _isKindDeal = value;
            }
        }

        public string iOSAppId
        {
            get
            {
                return config.iOSAppId;
            }
        }

        public string AndroidAppId
        {
            get
            {
                return config.AndroidAppId;
            }
        }

        public string AndroidUserAgent
        {
            get
            {
                return config.AndroidUserAgent;
            }
        }

        public string iOSUserAgent
        {
            get
            {
                return config.iOSUserAgent;
            }
        }

        public int CloseAppBlockHiddenDays
        {
            get
            {
                return config.CloseAppBlockHiddenDays;
            }
        }

        public int ViewAppBlockHiddenDays
        {
            get
            {
                return config.ViewAppBlockHiddenDays;
            }
        }

        public decimal MinGross
        {
            get
            {
                return config.GrossMargin;
            }
        }

        public bool EnableGrossMarginRestrictions
        {
            get
            {
                return config.EnableGrossMarginRestrictions;
            }
        }

        public string DealArgs { set; get; }

        private string SessionId
        {
            get { return Session.SessionID; }
        }
        private CpaClientMain<CpaClientEvent<CpaClientDeail>> NewCpa
        {
            get
            {
                if (Session[LkSiteSession.NewCpa.ToString()] != null)
                {
                    return (CpaClientMain<CpaClientEvent<CpaClientDeail>>)Session[LkSiteSession.NewCpa.ToString()];
                }
                return null;
            }
            set { Session[LkSiteSession.NewCpa.ToString()] = value; }
        }


        #endregion Props

        #region interface methods        
        public void RedirectToPiinlife()
        {
            Response.Redirect(string.Format("{0}/piinlife/{1}", SystemConfig.SiteUrl, BusinessHourId.ToString())
                + (!string.IsNullOrWhiteSpace(Rsrc) ? string.Format("/{0}", Rsrc) : string.Empty)
                + (Request.QueryString["token"] != null && !string.IsNullOrWhiteSpace(Request.QueryString["token"].ToString()) ? string.Format("?token={0}", Request.QueryString["token"].ToString()) : string.Empty));
        }

        public void RedirectToDefault()
        {
            Response.Redirect(string.Format("{0}/{1}", SystemConfig.SiteUrl, "ppon/default.aspx"));
        }

        /// <summary>
        /// 設定一組Cookie到client端
        /// </summary>
        /// <param name="aCookie"></param>
        public bool SetCookie(String value, DateTime expireTime, LkSiteCookie cookieField)
        {
            HttpCookie referenceCookie = CookieManager.NewCookie(cookieField.ToString("g"));

            referenceCookie.Value = value;
            referenceCookie.Expires = expireTime;
            //設定cookie值於使用者電腦
            try
            {
                Response.Cookies.Add(referenceCookie);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public void ShowMessage(string message)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('" + message + "');", true);
        }

        public void NoRobots()
        {
            phNorobots.Visible = true;
        }

        public void SeoSetting(ViewPponDeal d, string appendTitle = "")
        {
            if (String.IsNullOrWhiteSpace(d.PageDesc))
            {
                this.Page.MetaDescription = d.CouponUsage + "-" + d.EventTitle; //好康券+橘標
            }
            else
            {
                this.Page.MetaDescription = d.PageDesc;
            }

            if (String.IsNullOrWhiteSpace(d.PageTitle))
            {
                if (!string.IsNullOrEmpty(appendTitle))
                {
                    this.Page.Title = string.Format("{0}，{1}", d.CouponUsage, appendTitle);
                }
                else
                {
                    this.Page.Title = string.Format("{0}-{1}，{2}", d.CouponUsage, d.EventTitle, SystemConfig.Title);
                }
            }
            else
            {
                this.Page.Title = d.PageTitle;
            }

            PicAlt = string.IsNullOrWhiteSpace(d.PicAlt) ? (string.IsNullOrEmpty(d.AppTitle) ? d.CouponUsage : d.AppTitle) : d.PicAlt;
            this.Page.MetaKeywords = string.Format("{0},{1}", string.IsNullOrEmpty(PicAlt) ? string.Empty : PicAlt.Replace("/", ","), this.Page.MetaKeywords);
        }
        
        /// <summary>
        /// 熱銷檔次
        /// </summary>
        /// <param name="hotdeals"></param>
        public void SetHotSaleMutilpMainDeals(List<MultipleMainDealPreview> hotdeals)
        {
            this._viewMultipleHotDeals = SetMultipleSideDeal(hotdeals);
        }

        private List<ViewMultipleSideDeal> SetMultipleSideDeal(List<MultipleMainDealPreview> deals)
        {
            List<ViewMultipleSideDeal> viewMultipleSideDeals = new List<ViewMultipleSideDeal>();
            foreach (var deal in deals)
            {
                ViewMultipleSideDeal viewSidePreview = new ViewMultipleSideDeal();
                PponCity pponcity = PponCityGroup.DefaultPponCityGroup.GetPponCityByCityId(deal.CityID);

                viewSidePreview.deal = deal;

                viewSidePreview.clit_CityName2 = ViewPponDealManager.DefaultManager.GetCityNameOrTravelPlace(
                pponcity, deal.PponDeal.BusinessHourGuid);

                if (_isKindDeal)
                {
                    viewSidePreview.discount_1 = "公益";
                }
                else if (Helper.IsFlagSet(deal.PponDeal.BusinessHourStatus, BusinessHourStatus.NoDiscountShown)
                      || Helper.IsFlagSet(deal.PponDeal.BusinessHourStatus, BusinessHourStatus.PriceZeorShowOriginalPrice))
                {
                    viewSidePreview.discount_1 = "特選";
                }
                else if (deal.PponDeal.ItemPrice == 0)
                {
                    if (CityId.Equals(PponCityGroup.DefaultPponCityGroup.Family.CityId) && deal.PponDeal.ExchangePrice.HasValue && deal.PponDeal.ExchangePrice.Value > default(decimal))
                    {
                        if (Helper.IsFlagSet(deal.PponDeal.BusinessHourStatus, BusinessHourStatus.NoDiscountShown))
                        {
                            viewSidePreview.discount_1 = "特選";
                        }
                        else
                        {
                            string discount_2 = ViewPponDealManager.GetDealDiscountString(Helper.IsFlagSet(deal.PponDeal.BusinessHourStatus, BusinessHourStatus.NoDiscountShown), CheckZeroPriceToShowPrice(deal.PponDeal), deal.PponDeal.ItemOrigPrice);
                            viewSidePreview.discount_1 = discount_2 + "折";
                        }
                    }
                    else
                    {
                        viewSidePreview.discount_1 = "優惠";
                    }
                }
                else
                {
                    string discount = ViewPponDealManager.GetDealDiscountString(Helper.IsFlagSet(deal.PponDeal.BusinessHourStatus, BusinessHourStatus.NoDiscountShown), CheckZeroPriceToShowPrice(deal.PponDeal), deal.PponDeal.ItemOrigPrice);
                    int length = discount.Length;
                    if (length > 0)
                    {
                        string discount_2 = discount.Substring(0, 1);
                        if (discount.IndexOf('.') != -1 && length > 2)
                        {
                            discount_2 += discount.Substring(1, length - 1);
                        }
                        viewSidePreview.discount_1 = discount_2 + "折";
                    }
                }

                viewSidePreview.EveryDayNewDeal = PponFacade.GetEveryDayNewDealFlag(deal.PponDeal.BusinessHourGuid);
                viewMultipleSideDeals.Add(viewSidePreview);
            }

            return viewMultipleSideDeals;
        }

        public void SetMutilpMainDeals(List<MultipleMainDealPreview> multiplemaindeals)
        {
            pan_EmptyZone.Visible = !multiplemaindeals.Any();
            List<MultipleMainDealPreview> deals = ViewPponDealManager.DefaultManager.SortMultipleMainDealPreview(multiplemaindeals, SortType).ToList();

            SetMultipleMainDeal(deals);

            this.Page.Title = "今日開賣";
            this.Page.MetaDescription = "識貨的你，知道17life擁有多項價格實惠熱門商品，好逛又好買。現在就來看看【今日開賣專區】，有什麼欠買好貨色吧！";

            //The Open Graph protocol (facebook 分享參數)
            this.OgUrl = this.LinkCanonicalUrl = new Uri(HttpContext.Current.Request.Url.ToString()).AbsoluteUri;
            this.OgTitle = SystemConfig.DefaultMetaOgTitle;
            this.OgImage = SystemConfig.DefaultMetaOgImage;
            this.OgDescription = SystemConfig.DefaultMetaOgDescription;
        }

        private void SetMultipleMainDeal(List<MultipleMainDealPreview> deals)
        {
            List<ViewMultipleMainDeal> viewMultipleMainDeals = new List<ViewMultipleMainDeal>();

            foreach (var deal in deals)
            {
                ViewMultipleMainDeal viewDealPreview = new ViewMultipleMainDeal();
                viewDealPreview.deal = deal;

                PponCity pponcity = PponCityGroup.DefaultPponCityGroup.GetPponCityByCityId(deal.CityID);
                _isKindDeal = Helper.IsFlagSet(deal.PponDeal.GroupOrderStatus ?? 0, GroupOrderStatus.KindDeal);

                //貼上檔次 Icon 標籤
                string icon_string = PponFacade.GetDealIconHtmlContentList(deal.PponDeal, 2);

                viewDealPreview.deal_Label_1 = icon_string;
                viewDealPreview.deal_Label_2 = icon_string;
                viewDealPreview.clit_CityName = deal.PponDeal.IsMultipleStores ?
                   "<div class='hover_place'><span class='hover_place_text'><i class='fa fa-map-marker'></i>" + deal.PponDeal.HoverMessage + "</span></div>" : string.Empty;

                viewDealPreview.clit_CityName2 = ViewPponDealManager.DefaultManager.GetCityNameOrTravelPlace(
                pponcity, deal.PponDeal.BusinessHourGuid);
                //= deal.PponDeal.DeliveryType.HasValue && (deal.PponDeal.DeliveryType.Value == (int)DeliveryType.ToHouse) ? "宅配" :
                //(deal.PponDeal.IsMultipleStores ? "多分店" : deal.PponDeal.HoverMessage);

                switch (deal.PponDeal.GetDealStage())
                {
                    case PponDealStage.Created:
                    case PponDealStage.Ready:
                        viewDealPreview.litCountdownTime = "即將開賣";
                        break;

                    case PponDealStage.ClosedAndFail:
                    case PponDealStage.ClosedAndOn:
                    case PponDealStage.CouponGenerated:
                        viewDealPreview.litCountdownTime = "已結束販售";
                        break;
                    case PponDealStage.Running:
                    case PponDealStage.RunningAndFull:
                    case PponDealStage.RunningAndOn:
                    default:
                        int daysToClose = (int)(deal.PponDeal.BusinessHourOrderTimeE - Now).TotalDays;
                        if (daysToClose > 3)
                        {
                            viewDealPreview.litCountdownTime = "限時優惠中!";
                        }
                        else
                        {
                            viewDealPreview.litDays = daysToClose + Phrase.Day;
                            viewDealPreview.litCountdownTime = DealTimer.SetCountdown(deal.PponDeal.BusinessHourOrderTimeE);

                            viewDealPreview.timerTitleField_dataStart = deal.PponDeal.BusinessHourOrderTimeS.ToJavaScriptMilliseconds();
                            viewDealPreview.timerTitleField_dataEnd = deal.PponDeal.BusinessHourOrderTimeE.ToJavaScriptMilliseconds();
                        }
                        break;
                }

                if (_isKindDeal)
                {
                    viewDealPreview.discount_1 = viewDealPreview.discount_2 = "公益";
                }
                else if (Helper.IsFlagSet(deal.PponDeal.BusinessHourStatus, BusinessHourStatus.NoDiscountShown)
                      || Helper.IsFlagSet(deal.PponDeal.BusinessHourStatus, BusinessHourStatus.PriceZeorShowOriginalPrice))
                {
                    viewDealPreview.discount_1 = viewDealPreview.discount_2 = "特選";
                }
                else if (deal.PponDeal.ItemPrice == 0)
                {
                    if (CityId.Equals(PponCityGroup.DefaultPponCityGroup.Family.CityId) && deal.PponDeal.ExchangePrice.HasValue && deal.PponDeal.ExchangePrice.Value > default(decimal))
                    {
                        if (Helper.IsFlagSet(deal.PponDeal.BusinessHourStatus, BusinessHourStatus.NoDiscountShown))
                        {
                            viewDealPreview.discount_1 = viewDealPreview.discount_2 = "特選";
                        }
                        else
                        {
                            viewDealPreview.discount_2 = ViewPponDealManager.GetDealDiscountString(Helper.IsFlagSet(deal.PponDeal.BusinessHourStatus, BusinessHourStatus.NoDiscountShown), CheckZeroPriceToShowPrice(deal.PponDeal), deal.PponDeal.ItemOrigPrice);
                            viewDealPreview.discount_1 = viewDealPreview.discount_2 + "折";
                            viewDealPreview.discount_2 += "<span class=\"smalltext\">折</span>";
                        }
                    }
                    else
                    {
                        viewDealPreview.discount_1 = viewDealPreview.discount_2 = "優惠";
                    }
                }
                else
                {
                    string discount = ViewPponDealManager.GetDealDiscountString(Helper.IsFlagSet(deal.PponDeal.BusinessHourStatus, BusinessHourStatus.NoDiscountShown), CheckZeroPriceToShowPrice(deal.PponDeal), deal.PponDeal.ItemOrigPrice);
                    int length = discount.Length;
                    if (length > 0)
                    {
                        viewDealPreview.discount_2 = discount.Substring(0, 1);
                        if (discount.IndexOf('.') != -1 && length > 2)
                        {
                            viewDealPreview.discount_2 += discount.Substring(1, length - 1);
                        }

                        viewDealPreview.discount_1 = viewDealPreview.discount_2 + "折";
                        viewDealPreview.discount_2 += "<span class=\"smalltext\">折</span>";
                    }
                }

                if (config.InstallmentPayEnabled == false)
                {
                    viewDealPreview.litInstallmentPriceLabel = "<p>好康價</p>";
                }
                else if (deal.PponDeal.Installment3months.GetValueOrDefault() == false)
                {
                    viewDealPreview.litInstallmentPriceLabel = "<p>好康價</p>";
                }
                else
                {
                    viewDealPreview.litInstallmentPriceLabel = "<p class='price_installment'>分期0利率</p>";
                }

                viewDealPreview.EveryDayNewDeal = PponFacade.GetEveryDayNewDealFlag(deal.PponDeal.BusinessHourGuid);
                viewMultipleMainDeals.Add(viewDealPreview);
            }
            this._viewMultipleMainDeals = viewMultipleMainDeals;
        }


        public void SetEventMainActivities(KeyValuePair<EventActivity, CmsRandomCityCollection> pair)
        {
            Cache.Remove(EdmPopUpCacheName);
            //修正跳窗EDM暫存誤差
            double CacheMin = Math.Floor((pair.Key.EndDate - DateTime.Now).TotalMinutes);
            CacheMin = (CacheMin > 60 ? 60 : CacheMin);
            Cache.Insert(EdmPopUpCacheName, pair, null, DateTime.Now.AddMinutes(CacheMin), TimeSpan.Zero, System.Web.Caching.CacheItemPriority.NotRemovable, null);
            if (!string.IsNullOrWhiteSpace(Rsrc) && pair.Key.ExcludeCpaList.Contains(Rsrc))
            {
                ShowEventEmail = false;
            }
            else if (!pair.Value.Any(x => x.CityId == CityId))
            {
                ShowEventEmail = false;
            }
            else
            {
                ShowEventEmail = true;
            }
        }

        #endregion interface methods

        #region page

        protected void Page_Init(object sender, EventArgs e)
        {
            string redirectUrl = SystemConfig.SiteUrl.TrimEnd('/');

            if (Request.QueryString["bid"] != null)
            {
                string queryValue = HttpContext.Current.Request.Url.Query;
                redirectUrl += @"/ppon/detail.aspx" + queryValue;
                Response.Redirect(redirectUrl);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                this.Presenter.OnViewInitialized();
                if (Session[LkSiteSession.ShareLinkText.ToString()] != null && User.Identity.IsAuthenticated)
                {
                    Session.Remove(LkSiteSession.ShareLinkText.ToString());
                    if (string.IsNullOrWhiteSpace(tbx_ReferralShare.Text))
                    {
                        tbx_ReferralShare.Text = GetShareText(BusinessHourId);
                    }
                    ScriptManager.RegisterStartupScript(this, typeof(Button), "showshare", "showshare('" + tbx_ReferralShare.Text + "');", true);
                }

                #region New Cpa

                if (config.NewCpaEnable)
                {
                    try
                    {
                        if (NewCpa != null && NewCpa.Count > 0)
                        {
                            PponFacade.CpaSetDetailToDB(UserName, SessionId, Helper.GetClientIP(), NewCpa);
                        }
                    }
                    catch
                    {
                        //Logger.Error(string.Format("CpaSetDetailToDB error(SubscribeEvent): UserName:{0}, SessionId:{1}, Cpa:{2}, Exception:{3}", UserName, SessionId, NewCpa, ex));
                    }
                }

                #endregion New Cpa


            }
            this.Presenter.OnViewLoaded();
            this.Master.IsShowBanner = true;

            if (_cityId == 0)
            {
                _cityId = CityId;
            }

            //App Links
            string bid = BusinessHourId == Guid.Empty ? string.Empty : BusinessHourId.ToString();
            HttpCookie cookie = Request.Cookies.Get(LkSiteCookie.ReferenceId.ToString("g"));
            string extId = cookie != null ? cookie.Value : string.Empty;

            if (!string.IsNullOrWhiteSpace(bid))
            {
                AddiOSMeta(bid, extId);
            }
            else
            {
                lit_PadCss.Text = "<link href='" + ResolveUrl("../themes/PCweb/css/RDL-Pad.css") + "' rel='stylesheet' type='text/css' />";
            }
        }

        public void SetMemberCollection(List<Guid> collectBids)
        {
            string jsonStr = ProviderFactory.Instance().GetSerializer()
                .Serialize(collectBids);
            hdMemberCollectDealGuidJson.Value = jsonStr;
        }

        protected void RandomPponNewsInit(object sender, EventArgs e)
        {
            int tempCity = _cityId == 0 ? CityId : _cityId;
            saleB.CityId = news.CityId = Master.PponNews.CityId = outsideAD.CityId = insideAD.CityId = active.CityId = tempCity;
            saleB.Type = news.Type = Master.PponNews.Type = outsideAD.Type = insideAD.Type = active.Type = RandomCmsType.PponRandomCms;
        }

        protected void rpt_SubCategory_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is KeyValuePair<CategoryDealCount, List<CategoryDealCount>>)
            {
                var item = (KeyValuePair<CategoryDealCount, List<CategoryDealCount>>)e.Item.DataItem;

                Repeater rptSecCategory = (Repeater)e.Item.FindControl("rpt_SecCategory");
                rptSecCategory.Visible = item.Value.Count(x => x.DealCount > 0) > 0;
                rptSecCategory.DataSource = item.Value.Where(x => x.DealCount > 0);
                rptSecCategory.DataBind();
            }
        }

        protected void rpt_Category_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is KeyValuePair<CategoryDealCount, List<CategoryDealCount>>)
            {
                var item = (KeyValuePair<CategoryDealCount, List<CategoryDealCount>>)e.Item.DataItem;
                Repeater rptSecCategory = (Repeater)e.Item.FindControl("rpt_SecCategory");
                rptSecCategory.Visible = item.Value.Count(x => x.DealCount > 0) > 0;
                rptSecCategory.DataSource = item.Value.Where(x => x.DealCount > 0);
                rptSecCategory.DataBind();
            }
        }

        #endregion page

        #region other methods

        protected void GetShareLink(object sender, EventArgs e)
        {
            if (Page.User == null || !Page.User.Identity.IsAuthenticated)
            {
                Session[LkSiteSession.ShareLinkText.ToString()] = "share";
                ScriptManager.RegisterStartupScript(this, typeof(Button), "showshare", "bi();", true);
            }
            else
            {
                if (string.IsNullOrWhiteSpace(tbx_ReferralShare.Text))
                {
                    tbx_ReferralShare.Text = GetShareText(BusinessHourId);
                }

                ScriptManager.RegisterStartupScript(this, typeof(Button), "showshare", "showshare('" + tbx_ReferralShare.Text + "');", true);
            }
        }

        protected static string GetShareText(Guid bid)
        {
            if (HttpContext.Current.Session[LkSiteSession.ExternalMemberId.ToString()] == null)
            {
                MemberUtility.SetUserSsoInfoReady(MemberFacade.GetUniqueId(HttpContext.Current.User.Identity.Name));
            }

            ExternalMemberInfo emi = HttpContext.Current.Session[LkSiteSession.ExternalMemberId.ToString()] as ExternalMemberInfo;

            if (emi != null)
            {
                string origUrl = WebUtility.GetSiteRoot() + "/ppon/PponReferralRedirection.aspx?bid=" + bid + "," + (int)emi[0].Source + "|" + emi[0].ExternalId;
                string shortUrl = WebUtility.RequestShortUrl(origUrl);
                PromotionFacade.SetDiscountReferrerUrl(origUrl, shortUrl, MemberFacade.GetUniqueId(HttpContext.Current.User.Identity.Name), HttpContext.Current.User.Identity.Name, bid, 0);
                return shortUrl;
            }
            else
            {
                return string.Empty;
            }
        }

        public decimal CheckZeroPriceToShowPrice(IViewPponDeal deal)
        {
            if (deal.CategoryIds.Contains(CategoryManager.Default.Family.CategoryId))
            {
                return PponFacade.CheckZeroPriceToShowPrice(deal, PponCityGroup.DefaultPponCityGroup.Family.CityId);
            }
            else
            {
                return PponFacade.CheckZeroPriceToShowPrice(deal, CityId);
            }
        }

        public decimal CheckZeroPriceToShowPrice(ViewComboDeal deal)
        {
            if (decimal.Equals(0, deal.ItemPrice))
            {
                if ((deal.BusinessHourStatus & (int)BusinessHourStatus.PriceZeorShowOriginalPrice) > 0)
                {
                    return deal.ItemOrigPrice;
                }
                else
                {
                    return deal.ItemPrice;
                }
            }
            else
            {
                return deal.ItemPrice;
            }
        }

        public string AveragePriceTag(ViewComboDeal deal)
        {
            return IsShowAveragePriceInfo(deal) ? "均價" : string.Empty;
        }

        public string AveragePriceLab(ViewComboDeal deal)
        {
            var averagePrice = CheckZeroPriceToShowAveragePrice(deal);

            return (IsShowAveragePriceInfo(deal) && averagePrice > 0) ? "$" + averagePrice.ToString("F0") : string.Empty;
        }



        public bool IsShowAveragePriceInfo(ViewComboDeal deal)
        {
            return (deal.IsAveragePrice && deal.QuantityMultiplier != null && deal.QuantityMultiplier >= 1);
        }

        public decimal CheckZeroPriceToShowAveragePrice(ViewComboDeal deal)
        {
            if (decimal.Equals(0, deal.ItemPrice))
            {
                return deal.ItemPrice;
            }
            else
            {
                if (IsShowAveragePriceInfo(deal))
                {
                    return Math.Ceiling(deal.ItemPrice / deal.QuantityMultiplier.Value);
                }
                else
                {
                    return deal.ItemPrice;
                }
            }
        }

        public string GetDealShortTitle(ViewPponDeal deal)
        {
            string shortTitle = (string.IsNullOrEmpty(deal.AppTitle)) ? deal.ItemName : deal.AppTitle;
            return string.Format("<span class='tag-location' style='font-size:14px;'>{0}</span>{1}",
                ViewPponDealManager.DefaultManager.GetCityNameOrTravelPlace(
                    PponCityGroup.DefaultPponCityGroup.GetPponCityByCityId(CityId),
                    deal.BusinessHourGuid)
                , shortTitle);
        }

        public int GetCategoryID()
        {
            if (CityId == PponCityGroup.DefaultPponCityGroup.Travel.CityId && TravelCategoryId == CategoryID)
            {
                return CategoryManager.GetDefaultCategoryNode().CategoryId;
            }

            if (CityId == PponCityGroup.DefaultPponCityGroup.PBeautyLocation.CityId && FemaleCategoryId == CategoryID)
            {
                return CategoryManager.GetDefaultCategoryNode().CategoryId;
            }


            return CategoryID;
        }

        public string GetCategoryUrl(int cityId, int categoryId, bool isDelivery = false)
        {
            const string CPA_CATEGORY = "17_category";

            string categoryUrl = ResolveUrl(string.Format((isDelivery) ? "~/delivery/{0}/{1}?cpa={2}" : "~/{0}/{1}?cpa={2}", cityId, categoryId, CPA_CATEGORY));

            if (cityId == PponCityGroup.DefaultPponCityGroup.Travel.CityId && TravelCategoryId != CategoryManager.GetDefaultCategoryNode().CategoryId)
            {
                categoryUrl = ResolveUrl(string.Format((isDelivery) ? "~/delivery/{0}/{1}/{2}?cpa={3}" : "~/{0}/{1}/{2}?cpa={3}", cityId, TravelCategoryId, categoryId, CPA_CATEGORY));
            }
            if (cityId == PponCityGroup.DefaultPponCityGroup.PBeautyLocation.CityId)
            {
                categoryUrl = ResolveUrl(string.Format((isDelivery) ? "~/delivery/{0}/{1}/{2}?cpa={3}" : "~/{0}/{1}/{2}?cpa={3}", cityId, FemaleCategoryId, categoryId, CPA_CATEGORY));
            }

            if (SubRegionCategoryId > 0)
            {
                categoryUrl += "?subc=" + SubRegionCategoryId;
            }

            return categoryUrl;
        }

        #region WebMethod

        [WebMethod]
        public static string GetShareLink(string businessHourId)
        {
            if (HttpContext.Current.User == null || !HttpContext.Current.User.Identity.IsAuthenticated)
            {
                return string.Empty;
            }
            else
            {
                Guid bid = Guid.TryParse(businessHourId, out bid) ? bid : Guid.Empty;
                return GetShareText(bid);
            }
        }

        [WebMethod]
        public static string PostToFaceBook(string businessHourId)
        {
            if ((HttpContext.Current.User == null || !HttpContext.Current.User.Identity.IsAuthenticated) ||
                !(new DefaultPponPresenter()).AddUserTrack("", "", "/ppon/default.aspx", "", "FaceBook"))
            {
                return string.Empty;
            }
            else
            {
                Guid bid = Guid.TryParse(businessHourId, out bid) ? bid : Guid.Empty;
                GetShareText(bid);
                return "Y";
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="city"></param>
        /// <returns></returns>
        [WebMethod]
        public static string GetUrl(string city)
        {
            string url = config.SiteUrl;
            int cityId = int.TryParse(city, out cityId) ? cityId : PponCityGroup.DefaultPponCityGroup.TaipeiCity.CityId;
            PponCity pponCity = PponCityGroup.DefaultPponCityGroup.GetPponCityByCityId(cityId);
            switch (pponCity.CityCode)
            {
                case "ALL":
                    url += "/Event/Exhibition.aspx?u=PDelivery&rsrc=17_eventDL";
                    break;

                case "TRA":
                    url += "/Event/Exhibition.aspx?u=PTravel&rsrc=17_eventtravel";
                    break;

                case "PBU":
                    url += "/Event/Exhibition.aspx?u=PBeauty&rsrc=17_eventpeauty";
                    break;

                default:
                    url += "/" + pponCity.CityId.ToString();
                    break;
            }
            var referenceCookie = CookieManager.NewCookie(
                "CityId",
                pponCity.CityId.ToString(),
                DateTime.Now.AddMonths(3));
            HttpContext.Current.Response.Cookies.Add(referenceCookie);
            return url;
        }

        [WebMethod]
        public static void SetSortCategory(int type)
        {
            CategorySortType nowSortType = CategorySortType.Default;
            if (HttpContext.Current.Session["SortType"] != null)
            {
                nowSortType = (CategorySortType)HttpContext.Current.Session["SortType"];
            }
            CategorySortType ctype = (CategorySortType)type;
            CategorySortType sortType = ctype;
            if (ctype == CategorySortType.PriceAsc)
            {
                if (nowSortType == CategorySortType.PriceAsc)
                {
                    sortType = CategorySortType.PriceDesc;
                }
                else if (nowSortType == CategorySortType.PriceDesc)
                {
                    sortType = CategorySortType.PriceAsc;
                }
            }
            HttpContext.Current.Session["SortType"] = sortType;
        }

        [WebMethod]
        public static bool CheckMemberCollectDealStatus(Guid businessHourGuid)
        {
            //check login, if not login return false.
            if (!HttpContext.Current.User.Identity.IsAuthenticated)
            {
                return false;
            }

            int memberUnique = MemberFacade.GetUniqueId(HttpContext.Current.User.Identity.Name);
            return MemberFacade.MemberCollectDealIsCollected(memberUnique, businessHourGuid);
        }

        [WebMethod]
        public static string SetMemberCollectDeal(Guid businessHourGuid, int cityId, bool isCollecting)
        {
            //check login.
            if (HttpContext.Current.User.Identity.IsAuthenticated == false)
            {
                HttpContext.Current.Response.StatusCode = 500;
                HttpContext.Current.Response.StatusDescription = "userNotLogin";
                return new LunchKingSite.Core.JsonSerializer().Serialize(new { Success = false, IsCollecting = false });
            }

            int memberUnique = MemberFacade.GetUniqueId(HttpContext.Current.User.Identity.Name);
            bool result = false;

            if (isCollecting && businessHourGuid != Guid.Empty)
            {
                MemberCollectDeal mcd = new MemberCollectDeal();
                mcd.MemberUniqueId = memberUnique;
                mcd.BusinessHourGuid = businessHourGuid;
                mcd.CollectStatus = (Byte)MemberCollectDealStatus.Collected;
                mcd.CollectTime = System.DateTime.Now;
                mcd.LastAccessTime = System.DateTime.Now;
                mcd.CityId = cityId;
                result = MemberFacade.MemberCollectDealSave(mcd);
            }
            else
            {
                result = MemberFacade.MemberCollectDealRemove(memberUnique, businessHourGuid);
            }

            return new LunchKingSite.Core.JsonSerializer().Serialize(new { Success = result, IsCollecting = isCollecting });
        }

        [WebMethod]
        public static string GetSideDeals(List<Guid> bids, int workCityId, int takeCount)
        {
            try
            {
                workCityId = workCityId == PponCityGroup.DefaultPponCityGroup.NewTaipeiCity.CityId
                               ? PponCityGroup.DefaultPponCityGroup.TaipeiCity.CityId
                               : workCityId;
                return new LunchKingSite.Core.JsonSerializer().Serialize(new { Success = true, Data = PponFacade.GetSideDealJson(bids, workCityId, takeCount) });
            }
            catch (Exception ex)
            {
                WebUtility.LogExceptionAnyway(ex);
                return new LunchKingSite.Core.JsonSerializer().Serialize(new { Success = false, Data = string.Empty });
            }
        }

        [WebMethod]
        public static string GetCityNameByGEOLocation(string latitude, string longitude)
        {
            double _latitude = (double.TryParse(latitude, out _latitude)) ? _latitude : default(double);
            double _longitude = (double.TryParse(longitude, out _longitude)) ? _longitude : default(double);
            City city;
            PponCity pponcity = PponCityGroup.DefaultPponCityGroup.TaipeiCity;

            if (_latitude <= 0 || _longitude <= 0)
            {
                return pponcity.CityName;
            }
            try
            {
                pponcity = LocationFacade.GetAreaCityWithGoogleMap(_latitude, _longitude, out city);
            }
            catch
            {
                return PponCityGroup.DefaultPponCityGroup.TaipeiCity.CityName;
            }
            //return "";
            return pponcity.CityName;
        }






        #endregion WebMethod

        #endregion other methods

        #region AppLinks Meta
        /// <summary>
        /// Add iOS App Links Meta Tags
        /// </summary>
        /// <param name="bid"></param>
        /// <param name="extId"></param>
        protected void AddiOSMeta(string bid, string extId)
        {
            string ios_url = string.IsNullOrWhiteSpace(extId) ? string.Format("open17life://www.17life.com/deal/{0}", bid) : string.Format("open17life://www.17life.com/deal/{0}?extId={1}", bid, extId);

            AddMetaProperty("al:ios:url", ios_url);
            AddMetaProperty("al:ios:app_store_id", config.AppLinksiOSId);
            AddMetaProperty("al:ios:app_name", config.AppLinksiOSName);

        }

        /// <summary>
        /// Add Andriod App Links Meta Tags
        /// </summary>
        /// <param name="bid"></param>
        /// <param name="extId"></param>
        protected void AddAndroidMeta(string bid, string extId)
        {
            string android_url = string.IsNullOrWhiteSpace(extId) ? string.Format("{0}/ppon/OpenApp.aspx?bid={1}", config.SiteUrl, bid) : string.Format("{0}/ppon/OpenApp.aspx?bid={1}&extId={2}", config.SiteUrl, bid, extId);

            AddMetaProperty("al:android:url", android_url);
            AddMetaProperty("al:android:package", config.AppLinksAndroidPackage);
            AddMetaProperty("al:android:app_name", config.AppLinksAndroidAppName);
        }

        /// <summary>
        /// Add Meta Tag Property
        /// </summary>
        /// <param name="property"></param>
        /// <param name="content"></param>
        protected void AddMetaProperty(string property, string content)
        {
            HtmlMeta metaObject = new HtmlMeta();
            metaObject.Attributes.Add("property", property);
            metaObject.Attributes.Add("content", content);

            this.phAppLinks.Controls.Add(metaObject);
            this.phAppLinks.Controls.Add(new LiteralControl(Environment.NewLine));
        }
        #endregion

    }

    public class GeoLoc
    {
        public string Latitude { get; set; }
        public string Lognitude { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string Host { get; set; }
        public string Ip { get; set; }
        public string Code { get; set; }
    }
}
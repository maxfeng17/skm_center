﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using log4net;
using LunchKingSite.Core;
using LunchKingSite.Core.UI;
using LunchKingSite.WebLib;
using LunchKingSite.I18N;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core.Component;
using LunchKingSite.WebLib.Component;

namespace LunchKingSite.Web.Ppon
{
    public partial class fbshare_proxy : BasePage
    {
        ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        ILog logger = LogManager.GetLogger(typeof(fbshare_proxy));

        public string Type
        {
            get
            {
                return Request.QueryString["type"];
            }
        }

        public string ItemName
        {
            get
            {
                return Request.QueryString["itemname"];
            }
        }

        public string GoodWord
        {
            get
            {
                var goodWords = new List<string> { Phrase.LooksGood, Phrase.IHadHeardThat, Phrase.LetUsGoTogether, Phrase.PeopleSayThatStoreClerksAreHot, Phrase.RealBargain };
                return goodWords.ElementAt(new Random().Next(goodWords.Count));
            }
        }

        public string Name
        {
            get
            {
                return Request.QueryString["name"];
            }
        }

        public string Link
        {
            get
            {
                return Request.QueryString["link"];
            }
        }
        public string FBLink
        {
            get
            {
                return Request.QueryString["fblink"];
            }
        }

        public Guid BlogId
        {
            get
            {
                Guid blogid = Guid.Empty;
                Guid.TryParse(Request.QueryString["blogid"], out blogid);
                return blogid;
            }
        }

        public string Picture
        {
            get
            {
                return Request.QueryString["picture"];
            }
        }

        public string Description
        {
            get
            {
                return Request.QueryString["description"];
            }
        }

        public string Redirect_uri
        {
            get
            {
                return Request.QueryString["redirect_uri"];
            }
        }

        public int EventActivityId
        {
            get
            {
                if (Request.QueryString["eid"] != null)
                {
                    int refType;
                    if (int.TryParse(Request.QueryString["eid"], out refType))
                    {
                        return refType;
                    }
                }
                return 0;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            string origUrl = string.Empty;
            string shortenedUrl = string.Empty;
            if (!string.IsNullOrWhiteSpace(Request["bid"]))
            {
                if (HttpContext.Current.Session[LkSiteSession.ExternalMemberId.ToString()] == null)
                {
                    if (User.Identity.IsAuthenticated)
                    {
                        MemberUtility.SetUserSsoInfoReady(MemberFacade.GetUniqueId(User.Identity.Name));
                    }
                }

                ExternalMemberInfo emi = Session[LkSiteSession.ExternalMemberId.ToString()] as ExternalMemberInfo;
                if (emi == null || emi.Count == 0)
                {
                    Response.Redirect(FormsAuthentication.LoginUrl, true);
                    return;
                }
                try
                {
                    origUrl = WebUtility.GetSiteRoot() + "/ppon/PponReferralRedirection.aspx?bid=" +
                              Request["bid"] + "," + (int)emi[0].Source + "|" + emi[0].ExternalId;
                    shortenedUrl = WebUtility.RequestShortUrl(origUrl);

                    Guid bzid = Guid.TryParse(Request["bid"].ToString(), out bzid) ? bzid : Guid.Empty;
                    PromotionFacade.SetDiscountReferrerUrl(origUrl, shortenedUrl, MemberFacade.GetUniqueId(HttpContext.Current.User.Identity.Name), HttpContext.Current.User.Identity.Name, bzid, 0);

                }
                catch (Exception ex)
                {
                    var jsonS = new JsonSerializer();
                    log4net.LogManager.GetLogger("LogToDb").Error(
                        string.Format("fbshare_proxy.aspx.cs missing sessiong or cookie,error:Exception:{0},ExternalMemberInfo emi:{1},UserId:{2},toString:{3},login name:{4}",
                            ex,
                            jsonS.Serialize(emi),
                            emi.UserId,
                            emi.ToString(),
                            HttpContext.Current.User.Identity.Name));
                }
            }
            else if (!string.IsNullOrWhiteSpace(Link))
            {
                var eventActivity = PromotionFacade.GetEventActivity(EventActivityId);
                origUrl = string.Format("{0}?ruid={1}&eid={2}"
                    , Link
                    , MemberFacade.GetUniqueId(Page.User.Identity.Name)
                    , (eventActivity.IsLoaded) ? eventActivity.Id : 0);
                shortenedUrl = WebUtility.RequestShortUrl(origUrl);
                PromotionFacade.SetDiscountReferrerUrl(origUrl, shortenedUrl, MemberFacade.GetUniqueId(HttpContext.Current.User.Identity.Name), HttpContext.Current.User.Identity.Name, null, eventActivity.Id, DiscountReferrerUrlType.Event);
            }
            else if (!string.IsNullOrWhiteSpace(FBLink))
            {
                origUrl = FBLink;
                shortenedUrl = WebUtility.RequestShortUrl(origUrl);
                if (string.IsNullOrEmpty(shortenedUrl))
                {
                    shortenedUrl = origUrl;
                }
                OrderFacade.SetBlogReferrerUrl(origUrl, shortenedUrl, MemberFacade.GetUniqueId(HttpContext.Current.User.Identity.Name), BlogId);
            }

            if (!string.IsNullOrWhiteSpace(shortenedUrl))
            {
                if (!string.IsNullOrWhiteSpace(Type))
                {
                    if (Type == "plurk")
                    {
                        SharePlurk(shortenedUrl, GoodWord, ItemName);
                    }
                    else if (Type == "line")
                    {
                        ShareLine(shortenedUrl, ItemName);
                    }
                    else if (Type == "fbm")
                    {
                        ShareFBM(shortenedUrl);
                    }
                    else// if (Type == "facebook")
                    {
                        ShareFB(shortenedUrl);
                    }
                }
            }
            else
            {
                Response.Redirect(FormsAuthentication.LoginUrl, true);
                return;
            }
        }

        private void ShareFB(string url)
        {
            if (!string.IsNullOrWhiteSpace(Link))
            {
                Response.Redirect(string.Format(@"http://www.facebook.com/dialog/feed?app_id={0}&link={1}&caption=www.17life.com&picture={2}&name={3}&description={4}&redirect_uri={5}"
                    , config.FacebookApplicationId, url, Picture, Name, Description, Redirect_uri));
            }
            else
            {
                Response.Redirect(@"http://www.facebook.com/share.php?u=" + url, true);
            }
        }
        /// <summary>
        /// 以FB訊息方式分享連結
        /// </summary>
        /// <param name="url"></param>
        private void ShareFBM(string url)
        {
            string shareUrl = string.Format("fb-messenger://share/?link={0}&app_id={1}",
                HttpUtility.UrlEncode(url), config.FacebookApplicationId);
            logger.Debug("ShareFBM: " + shareUrl);
            Response.Redirect(shareUrl);
        }

        private void SharePlurk(string url, string goodWord, string itemname)
        {
            Response.Redirect(@"http://www.plurk.com/?qualifier=shares&status=" + HttpUtility.UrlEncode(url) + "+(" + HttpUtility.UrlEncode(goodWord) + "+ - + " + HttpUtility.UrlEncode(itemname) + ")", true);
        }

        private void ShareLine(string url, string itemname)
        {
            Response.Redirect(string.Format(@"http://line.me/R/msg/text/?{0} {1}", HttpUtility.UrlEncode(itemname), HttpUtility.UrlEncode(url)), true);
        }

    }
}
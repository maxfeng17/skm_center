﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.Core.Component;
using LunchKingSite.Core;

namespace LunchKingSite.Web.Ppon
{
    public partial class FbActivities : System.Web.UI.Page
    {
        public string FacebookAppId { get { return ProviderFactory.Instance().GetConfig().FacebookApplicationId; } }
        public string WebUrl { get { return Request.Url.Host + Request.ApplicationPath; } }

        protected void Page_Load(object sender, EventArgs e)
        {
        }
    }
}
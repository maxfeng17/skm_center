﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Ppon/Ppon.Master" AutoEventWireup="true" CodeBehind="Store.aspx.cs" Inherits="LunchKingSite.Web.Ppon.Store" EnableViewState="false" %>

<%@ Import Namespace="LunchKingSite.BizLogic.Component" %>
<%@ Import Namespace="LunchKingSite.DataOrm" %>
<%@ Import Namespace="LunchKingSite.Core" %>
<%@ MasterType VirtualPath="~/PPon/PPon.Master" %>
<%@ Register Src="~/UserControls/RandomParagraph.ascx" TagName="RandomParagraph"
    TagPrefix="ucR" %>
<%@ Register Src="~/UserControls/Paragraph.ascx" TagName="Paragraph"
    TagPrefix="ucP" %>
<%@ Register Src="AvailabilityCtrl.ascx" TagName="AvailabilityCtrl" TagPrefix="uc1" %>
<%@ Register Src="~/PPon/DealTimer.ascx" TagName="DealTimer" TagPrefix="uc3" %>

<asp:Content ID="cDefaultMeta" ContentPlaceHolderID="cphPponMeta" runat="server">
    <meta property="fb:app_id" content="<%= FB_AppId %>">
    <meta property="og:type" content="website" />
    <meta property="og:title" content="<%=OgTitle%>" />
    <meta property="og:url" content="<%=OgUrl%>" />
    <meta property="og:site_name" content="17Life" />
    <meta property="og:description" content="<%=OgDescription%>" />
    <meta property="og:image" content="<%=OgImage%>" />
    <meta name="apple-itunes-app" content="app-id=<%= iOSAppId%>">
    <meta name="google-play-app" content="app-id=<%= AndroidAppId%>">
    <asp:PlaceHolder ID="phNorobots" runat="server" Visible="false">
        <meta name="robots" content="noindex,nofollow,noarchive" />
    </asp:PlaceHolder>
    <link rel="image_src" href="<%=LinkImageSrc%>" />
    <link rel="canonical" href="<%=LinkCanonicalUrl%>" />
    <asp:PlaceHolder ID="phAppLinks" runat="server"></asp:PlaceHolder>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <link href='<%= ResolveUrl("../themes/PCweb/css/Rightside.css")%>' rel="stylesheet" type="text/css" />
    <link href='<%= ResolveUrl("../themes/PCweb/css/RDL-L.css") %>' rel="stylesheet" type="text/css" />
    <link href='<%= ResolveUrl("../themes/PCweb/css/RDL-M.css") %>' rel="stylesheet" type="text/css" />
    <link href='<%= ResolveUrl("../themes/PCweb/css/RDL-S.css") %>' rel="stylesheet" type="text/css" />
    <%=LunchKingSite.Core.Helper.RenderCss("/Themes/PCweb/css/ppon_item.min.css") %>
        <style type="text/css">            
        #Shopareatitle li + li {
            margin-top: 10px;
        }
        </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MC" runat="server">
    <div id="Default">
        <asp:PlaceHolder ID="p1" runat="server">
            <input id="HfShowEventEmail" type="hidden" value="<%= ShowEventEmail%>" />
            <div id="ousideAd" class="outside_AD" style="display: none;">
                <ucR:RandomParagraph ID="outsideAD" runat="server" OnInit="RandomPponNewsInit" />
            </div>

            <div class="LeftRight_wrap">
                <div id="Left">
                    <div id="maincontent" class="clearfix">
                            <!--outside-top-Opt-->
                            <div id="dealAnchorPoint" class="outside-topOptBox">
                                <div id="divLeftCollectDeal"></div>
                                <p class="outTOPT goTop2"><a href="#otTOP2">詳細介紹</a></p>
                                <p class="outTOPT goTop3"><a href="#otTOP3">店家資訊</a></p>
                            </div>
                            <!--outside-top-Opt-->
                        <!--好康資訊區塊Web版 Start-->
                        <asp:Repeater ID="rpt_Store_Deals" runat="server" OnItemDataBound="rpt_CategoryDealsBound">
                            <ItemTemplate>
                                <div class="forsdeal store-info" id="divppon1" runat="server">
                                    <a class="open_new_window" href='<%#ResolveUrl(string.Format("~/{0}", ((ViewPponDeal)(Container.DataItem)).BusinessHourGuid))%>'>
                                        <div id="MainPic" class="picforsdeal">
                                            <span>
                                                <img class="multipleDealLazy" src="<%# ((ViewPponDeal)(Container.DataItem)).DefaultDealImage %>" alt="<%# (Container.ItemIndex > 5) ? string.IsNullOrEmpty(((ViewPponDeal)(Container.DataItem)).PicAlt) ? (string.IsNullOrEmpty(((ViewPponDeal)(Container.DataItem)).AppTitle) ? ((ViewPponDeal)(Container.DataItem)).CouponUsage : ((ViewPponDeal)(Container.DataItem)).AppTitle) : ((ViewPponDeal)(Container.DataItem)).PicAlt : string.Empty%>" />
                                            </span>
                                            <div style='<%# ((((ViewPponDeal)(Container.DataItem)).OrderedQuantity >= ((ViewPponDeal)(Container.DataItem)).OrderTotalLimit) ? "": "display: none")%>' class="SoldOut_Bar_480"></div>
                                            <div class="activeLogo alo-l">
                                                <%# ((ViewPponDeal)(Container.DataItem)).PromoImageHtml %>
                                            </div>
                                        </div>
                                        <div class="DealPriceInfo">
                                            <span class="tag_place" id="divppon2" runat="server">
                                                <asp:Literal ID="lit_CityName2" runat="server"></asp:Literal></span>
                                    <div class="clear"></div>
                                            <div class="tag_title"><%# ((ViewPponDeal)(Container.DataItem)).ItemName%></div>
                                            <p class="tag_subtitle"><%# ((ViewPponDeal)(Container.DataItem)).ContentName ?? (((ViewPponDeal)(Container.DataItem)).EventName + ((ViewPponDeal)(Container.DataItem)).DealPromoTitle)  %></p>
                                            <div id="divppon3" runat="server">
                                            <div class="discount" id='<%# Container.ItemIndex + "discount_min"%>'>
                                                <asp:Literal ID="lit_Discount_2" runat="server"></asp:Literal>
                                            </div>  
                                            <p class="dealprice">
                                                $<%# CheckZeroPriceToShowPrice((ViewPponDeal)(Container.DataItem)).ToString("F0")%>元
                                            </p>
                                            <p class="dealsaletext fordeallist">
                                                原價<span class="oriprice">$<%# ((ViewPponDeal)(Container.DataItem)).ItemOrigPrice.ToString("F0")%></span>  | 節省$<%# (((ViewPponDeal)(Container.DataItem)).ItemOrigPrice - CheckZeroPriceToShowPrice((ViewPponDeal)(Container.DataItem))).ToString("F0")%>
                                            </p>
                                                </div>
                                            <uc3:DealTimer ID="timer" runat="server" />
                                            <%# OrderedQuantityHelper.Show(((ViewPponDeal)(Container.DataItem)), OrderedQuantityHelper.ShowType.BetaInPortal)%>
                                        </div>
                                    </a>
                                    <div class="clear"></div>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                        <!--好康資訊區塊Web版 End-->
                    </div>
                    <!--好康資訊區塊End-->

                    <div id="Entry" class="clearfix">
                        <div id="EntryContent">
                            <div id="EntryContentLeft">
                                <div class="EntryTitle PublicwelfareTBk">
                                    <a name="otTOP2" id="otTOP2"></a><a name="StoreDetail"></a>
                                    詳細介紹－<%=SellerName %>
                                </div>
                                <div class="EntryZone">
                                    <div id="Detailinner">
                                        <div id="detailDesc">
                                            <asp:Literal ID="liSellerIntroduct" runat="server"></asp:Literal>
                                        </div>
                                    </div>
                                </div>
                                <div id="SellerAvailability" runat="server" class="EntryTitle PublicwelfareTBk StoreInfo">
                                    <a name="otTOP3" id="otTOP3"></a>
                                    <asp:Literal ID="liSellerAvailabilityTitle" runat="server" Text="店家資訊"></asp:Literal>
                                </div>
                                <div id="storeInfoZone" class="EntryZone StoreInfo">
                                    <uc1:AvailabilityCtrl ID="avc" runat="server" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--Entry-->
                </div>

                <div id="Rightarea">
                    <div id="Stickers">
                        <ucP:Paragraph ID="paragraph1" SetContentName="/ppon/default.aspx_paragraph1" runat="server" />
                    </div>
                    <div class="inside_AD" style="display: none;">
                        <ucR:RandomParagraph ID="insideAD" runat="server" OnInit="RandomPponNewsInit" />
                    </div>
                    <div id="active_block" style="margin-bottom: 10px">
                        <ucR:RandomParagraph ID="active" runat="server" OnInit="RandomPponNewsInit" />
                    </div>
                    <div id="news_block">
                        <ucR:RandomParagraph ID="news" runat="server" OnInit="RandomPponNewsInit" />
                    </div>

                    <%=LunchKingSite.WebLib.Component.WebHelper.PageBlock("新好友限定好康") %>

                    <div id="saleB_block">
                        <ucR:RandomParagraph ID="saleB" runat="server" OnInit="RandomPponNewsInit" />
                    </div>

                    <div id="Sidedeal" class="side_block">
                        <div class="side_block_title">
                            <i class="fa fa-star fa-fw"></i>今日熱銷
                        </div>
                        <div class="side_item_wrap">
                            <asp:Repeater ID="rpt_SideDeals" runat="server" OnItemDataBound="rpt_CategoryDealsBound">
                                <ItemTemplate>
                                    <div class="side_item">
                                        <a href='<%#ResolveUrl(string.Format("~/{0}/{1}", ((MultipleMainDealPreview)(Container.DataItem)).CityID, ((MultipleMainDealPreview)(Container.DataItem)).PponDeal.BusinessHourGuid))%>'>
                                            <div class="activeLogo alo-s">
                                                <%# ((MultipleMainDealPreview)(Container.DataItem)).PponDeal.PromoImageHtml%>
                                            </div>
                                            <div class="side_item_pic">
                                                <span class="num_hot"><%# Container.ItemIndex+1 %></span>
                                                <img src="<%# ((MultipleMainDealPreview)(Container.DataItem)).PponDeal.DefaultDealImage %>" alt="<%# (Container.ItemIndex > 5) ? string.IsNullOrEmpty(((MultipleMainDealPreview)(Container.DataItem)).PponDeal.PicAlt) ? (string.IsNullOrEmpty(((MultipleMainDealPreview)(Container.DataItem)).PponDeal.AppTitle) ? ((MultipleMainDealPreview)(Container.DataItem)).PponDeal.CouponUsage : ((MultipleMainDealPreview)(Container.DataItem)).PponDeal.AppTitle) : ((MultipleMainDealPreview)(Container.DataItem)).PponDeal.PicAlt : string.Empty%>" />
                                                <span class="side_item_soldout_wrap" style='<%# ((((MultipleMainDealPreview)(Container.DataItem)).PponDeal.OrderedQuantity >= ((MultipleMainDealPreview)(Container.DataItem)).PponDeal.OrderTotalLimit) ? "": "display: none")%>'>
                                                    <div class="item_soldout_220">
                                                    </div>
                                                </span>
                                            </div>
                                            <div class="side_item_deal">
                                                <span class="tag_place">
                                                    <asp:Literal ID="lit_CityName2" runat="server"></asp:Literal></span>
                                                <div class="tag_subtitle"><%# ((MultipleMainDealPreview)(Container.DataItem)).PponDeal.ItemName%></div>
                                            </div>
                                            <div class="side_item_price">
                                                <span class="discount" style="color: rgb(153, 153, 153);">
                                                    <asp:Literal ID="lit_Discount_1" runat="server"></asp:Literal></span>
                                                <span class="price" style="color: rgb(191, 0, 0);">$<%# CheckZeroPriceToShowPrice(((MultipleMainDealPreview)(Container.DataItem)).PponDeal).ToString("F0")%>元<%# (((MultipleMainDealPreview)(Container.DataItem)).PponDeal.BusinessHourStatus & (int)(BusinessHourStatus.ComboDealMain)) > 0 ? "起" : string.Empty%></span>
                                                <span class="tag_buycounter" style="color: rgb(153, 153, 153);"><%# OrderedQuantityHelper.Show(((MultipleMainDealPreview)(Container.DataItem)).PponDeal, OrderedQuantityHelper.ShowType.MStyleDeal)%></span>

                                                <span class="btn_buy btn-primary">馬上看</span>
                                            </div>
                                        </a>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                    </div>

                    <div id="SideTodaydeal" class="side_block">
                        <div class="side_block_title">
                            <i class="fa fa-star fa-fw"></i>最新上檔
                        </div>
                        <div class="side_item_wrap">
                            <asp:Repeater ID="rpt_SideTodayDeals" runat="server" OnItemDataBound="rpt_CategoryDealsBound">
                                <ItemTemplate>
                                    <div class="side_item">
                                        <a href='<%#ResolveUrl(string.Format("~/{0}/{1}", ((MultipleMainDealPreview)(Container.DataItem)).CityID, ((MultipleMainDealPreview)(Container.DataItem)).PponDeal.BusinessHourGuid))%>'>
                                            <div class="activeLogo alo-s">
                                                <%# ((MultipleMainDealPreview)(Container.DataItem)).PponDeal.PromoImageHtml%>
                                            </div>
                                            <div class="side_item_pic">
                                                <img src="<%# ((MultipleMainDealPreview)(Container.DataItem)).PponDeal.DefaultDealImage %>" alt="<%# (Container.ItemIndex > 5) ? string.IsNullOrEmpty(((MultipleMainDealPreview)(Container.DataItem)).PponDeal.PicAlt) ? (string.IsNullOrEmpty(((MultipleMainDealPreview)(Container.DataItem)).PponDeal.AppTitle) ? ((MultipleMainDealPreview)(Container.DataItem)).PponDeal.CouponUsage : ((MultipleMainDealPreview)(Container.DataItem)).PponDeal.AppTitle) : ((MultipleMainDealPreview)(Container.DataItem)).PponDeal.PicAlt : string.Empty%>" />
                                                <span class="side_item_soldout_wrap" style='<%# ((((MultipleMainDealPreview)(Container.DataItem)).PponDeal.OrderedQuantity >= ((MultipleMainDealPreview)(Container.DataItem)).PponDeal.OrderTotalLimit) ? "": "display: none")%>'>
                                                    <div class="item_soldout_220">
                                                    </div>
                                                </span>
                                            </div>
                                            <div class="side_item_deal">
                                                <span class="tag_place">
                                                    <asp:Literal ID="lit_CityName2" runat="server"></asp:Literal></span>
                                                <div class="tag_subtitle"><%# ((MultipleMainDealPreview)(Container.DataItem)).PponDeal.ItemName%></div>
                                            </div>
                                            <div class="side_item_price">
                                                <span class="discount" style="color: rgb(153, 153, 153);">
                                                    <asp:Literal ID="lit_Discount_1" runat="server"></asp:Literal></span>
                                                <span class="price" style="color: rgb(191, 0, 0);">$<%# CheckZeroPriceToShowPrice(((MultipleMainDealPreview)(Container.DataItem)).PponDeal).ToString("F0")%>元<%# (((MultipleMainDealPreview)(Container.DataItem)).PponDeal.BusinessHourStatus & (int)(BusinessHourStatus.ComboDealMain)) > 0 ? "起" : string.Empty%></span>
                                                <span class="tag_buycounter" style="color: rgb(153, 153, 153);"><%# OrderedQuantityHelper.Show(((MultipleMainDealPreview)(Container.DataItem)).PponDeal, OrderedQuantityHelper.ShowType.MStyleDeal)%></span>
                                                <span class="btn_buy btn-primary">馬上看</span>
                                            </div>
                                        </a>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                    </div>

                    <div id="SideLastDaydeal" class="side_block">
                        <div class="side_block_title">
                            <i class="fa fa-star fa-fw"></i>最後倒數
                        </div>
                        <div class="side_item_wrap">
                            <asp:Repeater ID="rpt_SideLastDayDeals" runat="server" OnItemDataBound="rpt_CategoryDealsBound">
                                <ItemTemplate>
                                    <div class="side_item">
                                        <a href='<%#ResolveUrl(string.Format("~/{0}/{1}", ((MultipleMainDealPreview)(Container.DataItem)).CityID, ((MultipleMainDealPreview)(Container.DataItem)).PponDeal.BusinessHourGuid))%>'>
                                            <div class="activeLogo alo-s">
                                                <%# ((MultipleMainDealPreview)(Container.DataItem)).PponDeal.PromoImageHtml%>
                                            </div>
                                            <div class="side_item_pic">
                                                <img src="<%# ((MultipleMainDealPreview)(Container.DataItem)).PponDeal.DefaultDealImage %>" alt="<%# (Container.ItemIndex > 5) ? string.IsNullOrEmpty(((MultipleMainDealPreview)(Container.DataItem)).PponDeal.PicAlt) ? (string.IsNullOrEmpty(((MultipleMainDealPreview)(Container.DataItem)).PponDeal.AppTitle) ? ((MultipleMainDealPreview)(Container.DataItem)).PponDeal.CouponUsage : ((MultipleMainDealPreview)(Container.DataItem)).PponDeal.AppTitle) : ((MultipleMainDealPreview)(Container.DataItem)).PponDeal.PicAlt : string.Empty%>" />
                                                <span class="side_item_soldout_wrap" style='<%# ((((MultipleMainDealPreview)(Container.DataItem)).PponDeal.OrderedQuantity >= ((MultipleMainDealPreview)(Container.DataItem)).PponDeal.OrderTotalLimit) ? "": "display: none")%>'>
                                                    <div class="item_soldout_220">
                                                    </div>
                                                </span>
                                            </div>
                                            <div class="side_item_deal">
                                                <span class="tag_place">
                                                    <asp:Literal ID="lit_CityName2" runat="server"></asp:Literal></span>
                                                <div class="tag_subtitle"><%# ((MultipleMainDealPreview)(Container.DataItem)).PponDeal.ItemName%></div>
                                            </div>
                                            <div class="side_item_price">
                                                <span class="discount" style="color: rgb(153, 153, 153);">
                                                    <asp:Literal ID="lit_Discount_1" runat="server"></asp:Literal></span>
                                                <span class="price" style="color: rgb(191, 0, 0);">$<%# CheckZeroPriceToShowPrice(((MultipleMainDealPreview)(Container.DataItem)).PponDeal).ToString("F0")%>元<%# (((MultipleMainDealPreview)(Container.DataItem)).PponDeal.BusinessHourStatus & (int)(BusinessHourStatus.ComboDealMain)) > 0 ? "起" : string.Empty%></span>
                                                <span class="tag_buycounter" style="color: rgb(153, 153, 153);"><%# OrderedQuantityHelper.Show(((MultipleMainDealPreview)(Container.DataItem)).PponDeal, OrderedQuantityHelper.ShowType.MStyleDeal)%></span>
                                                <span class="btn_buy btn-primary">馬上看</span>
                                            </div>
                                        </a>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                    </div>

                    <div class="side_block">
                        <div class="side_block_title">
                            <i class="fa fa-facebook-square fa-fw"></i>17Life粉絲團
                        </div>
                        <div class="side_block_ad">
                            <div id="FacebookareaContent" style="padding-bottom: 0px">
                                <iframe id="fbfans" data-src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2F17life.com.tw&amp;send=false&amp;layout=standard&amp;width=230&amp;show_faces=true&amp;action=like&amp;colorscheme=light&amp;font=arial&amp;height=80"
                                    scrolling="no" frameborder="0" style="border: none; overflow: hidden; width: 230px; padding-bottom: 0px; padding-left: 2px; height: 70px;"
                                    allowtransparency="true"></iframe>
                                <br />
                                <a href="https://www.facebook.com/17life.com.tw" target="_blank">
                                    <img src="<%= ResolveUrl("~/Themes/PCweb/images/goto_fbpage.png")%>" width="202" height="28"
                                        border="0" alt="" />
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="side_block">
                        <div class="side_block_title">
                            <i class="fa fa-users fa-fw"></i>合作提案
                        </div>
                        <div class="side_block_ad">
                            <div id="Shopareatitle">
                                <ul>
                                    <li style="font-size:18px;font-weight:bold;">手牽手，力量比較大</li>
                                    <li>17life 竭力在尋找各種優質、有趣的店家及產品，為我們的平台帶來更多生命力。</li>
                                    <li>若您有任何有趣的合作方案，我們都非常歡迎並誠摯的期待來自你的提案。</li> 
                                    <li>如對廣告行銷、公益合作有興趣者，也歡迎跟我們提案噢。</li> 
                                </ul>
                            </div>
                            <div style="width:100%;text-align:center;margin-top:15px;margin-bottom:5px;">
                                <a class="btn btn-primary btn-primary-flat " style="font-size:18px;" href="<%=SystemConfig.SiteUrl+"/Ppon/ContactUs"%>">填寫提案表</a>
                            </div>
                        </div>
<%--                        <div class="side_block_ad">
                            <div id="Div4">
                                歡迎各類企業及店家與我們合作，您只要準備最優質的產品/服務，以及消費者至上的誠意，就可以在17Life的平台上，精準行銷，一起賣到翻~
                        <br />
                                <br />
                                如對廣告行銷、公益合作有興趣者，也歡迎跟我們提案。
                            </div>
                            <a class="ShopBtn" href="<%=SystemConfig.SiteUrl%>/Ppon/ContactUs.aspx"></a>
                            <p id="ShopinnerIMG">
                                將一卡車的客戶送到您面前
                            </p>
                        </div>--%>
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>
        </asp:PlaceHolder>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="LastLoadJs" runat="server">
    <script>
        var x;
        $(document).ready(function () {
            resizefun();
        });
        $(window).resize(function () {
            resizefun();
        });

        function resizefun() {
            x = $(window).width();
            if (x > 763) { //寬版      
                $("#target2").css('display', 'none');
                $(".NaviCityArea").css("display", "block");
                $(".center").css("display", "block");

            } else { //手機板  
                //手機版內容展開收合 
                $(function () {
                    var icons = {
                        header: "ui-icon-circle-arrow-e",
                        activeHeader: "ui-icon-circle-arrow-s"
                    };
                    $("#EntryContent").accordion({
                        header: '.EntryTitle',
                        collapsible: true,
                        autoHeight: false,
                        active: 0,
                        change: function (event, ui) {
                            if ($(ui.newContent).offset() != null) {
                                $('html, body').animate({
                                    scrollTop: $(ui.newContent).offset().top - 65
                                }, 500);
                            }
                        }
                    });
                });
            }
        }
    </script>
</asp:Content>

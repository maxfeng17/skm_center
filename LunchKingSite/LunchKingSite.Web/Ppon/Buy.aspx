﻿<%@ Page Language="C#" MasterPageFile="~/Ppon/Ppon.Master" AutoEventWireup="true" 
    CodeBehind="Buy.aspx.cs" Inherits="LunchKingSite.Web.Ppon.Buy" ClientIDMode="Static"
    Title="17Life交易" %>

<%@ Import Namespace="LunchKingSite.Core" %>
<%@ Import Namespace="LunchKingSite.WebLib" %>
<%@ Import Namespace="LunchKingSite.BizLogic.Facade" %>
<%@ Register Src="~/UserControls/Paragraph.ascx" TagName="Paragraph"
    TagPrefix="uc3" %>
<%@ Register Src="~/UserControls/RandomParagraph.ascx" TagName="RandomParagraph"
    TagPrefix="ucR" %>

<asp:Content ContentPlaceHolderID="SSC" runat="server">
    <script type="text/javascript" src="../Tools/js/jquery.blockUI.js"></script>
    <script type="text/javascript" src="../Tools/js/jquery.cascadeAddress.js?ver=0.0.0.2"></script>
    <script type="text/javascript" src="../Tools/js/completer.js"></script>
    <script type="text/javascript" src="<%=config.LightboxJsUrl%>"></script>
    <script type="text/javascript" src="../Tools/js/jquery.bpopup.min.js"></script>
    <link href="../Themes/default/images/17Life/G3/PPD.css" rel="stylesheet" type="text/css" />
    <link href="../Themes/default/images/17Life/G3/D2-5-Checkout.css" rel="stylesheet" type="text/css" />
    <asp:Literal ID="liPiinlifeMasterPageCss" runat="server"></asp:Literal>
    <asp:Literal ID="liPiinlifePponCss" runat="server"></asp:Literal>
    <link href="../themes/PCweb/css/RDL-L.css" rel="stylesheet" type="text/css" />
    <link href="../themes/PCweb/css/RDL-M.css" rel="stylesheet" type="text/css" />
    <link href="../themes/PCweb/css/RDL-S.css" rel="stylesheet" type="text/css" />
    <link href="../Tools/js/css/completer.css" rel="stylesheet" type="text/css" />
    <asp:Literal ID="liPiinlifeRDLCss" runat="server"></asp:Literal>
    <script src="../Tools/js/json2.js" type="text/javascript"></script>
    <script src="../Tools/js/vue.min.js" type="text/javascript"></script>
    <script src="../Tools/js/buy.js?v=1.19" type="text/javascript"></script>
    <script type="text/javascript" src="https://www.payeasy.com.tw/static/gateway/js/ui.js"></script>



    <style type="text/css">
        .memberLogin, .memberLoginToo {
            cursor: pointer;
        }
        .installmentPanel label {
            margin-bottom:0px !important;
        }
        .ui-autocomplete {
            max-height: 250px;
            position: absolute;
            width: 380px;
            font-size: 13px;
            background: #fff;
            border: 1px solid #cccccc;
            margin-top: -1px;
        }

        .ui-autocomplete .code {
            width: 60px;
            display: inline-block;
            padding-left: 3px;
        }

        .ui-state-default, .ui-widget-content .ui-state-default, .ui-widget-header .ui-state-default {
            border: 1px solid #d3d3d3;
            background: #e6e6e6 url(images/ui-bg_glass_75_e6e6e6_1x400.png) 50% 50% repeat-x;
            font-weight: normal;
            color: #555555;
        }

        .ui-state-default a, .ui-state-default a:link, .ui-state-default a:visited {
            color: #555555;
            text-decoration: none;
        }

        .ui-state-hover, .ui-widget-content .ui-state-hover, .ui-widget-header .ui-state-hover, .ui-state-focus, .ui-widget-content .ui-state-focus, .ui-widget-header .ui-state-focus {
            border: 1px solid #999999;
            background: #dadada url(images/ui-bg_glass_75_dadada_1x400.png) 50% 50% repeat-x;
            font-weight: normal;
            color: #212121;
        }

        .ui-state-hover a, .ui-state-hover a:hover {
            color: #212121;
            text-decoration: none;
        }

        .ui-state-active, .ui-widget-content .ui-state-active, .ui-widget-header .ui-state-active {
            border: 1px solid #aaaaaa;
            background: #ffffff url(images/ui-bg_glass_65_ffffff_1x400.png) 50% 50% repeat-x;
            font-weight: normal;
            color: #212121;
        }

        .ui-state-active a, .ui-state-active a:link, .ui-state-active a:visited {
            color: #212121;
            text-decoration: none;
        }

        .ui-widget :active {
            outline: none;
        }

        .ui-menu .ui-menu-item a {
            text-decoration: none;
            display: block;
            padding: .2em .4em;
            line-height: 1.5;
            zoom: 1;
            cursor: default;
        }

        #middle-mc-menu-btn_HD {
            display: none;
        }

        .LinePayContent,
        .phInstallmentContent,
        .MasterPassContent,
        .TaishinContent,
        .AtmPayContent {
            display:none;
        }

        .disabled {
            pointer-events:none;
            /*opacity:0.6;*/
        }

        .grui-form .form-unit .data-input p.invalidDiscount {
            color:#bf0000;
        }
        .message_box{
            z-index: 1;
        }
        .ispHint {
            display:none;
        }
        .tempHidden {
            display: none !important;
        }
        

        #pan_Success_ATM .grui-form .form-unit {
            min-height: 24px !important;
            margin:0px;
            display:flex;
            padding: 3px;
        }
        #maincontent .content-group.payment .info {
            background-color: rgb(220, 223, 228);
			padding: 10px 20px 10px 20px;
        }

        #maincontent .content-group.payment .info.btn-center {
            background-color: #ffffff;
        }

        #pan_Success_ATM .success {
            text-align:center;
        }
        #pan_Success_ATM .grui-form .form-unit .data-input {
             margin-left: 0px;
        }
    	#pan_Success_CreditCard .info {
			padding-top:10px;
    	}
    </style>
    <script type="text/javascript">
        <%=GtagDataJson%>
        <%=GtagConversionJson%>
        <%=UrADGtagConversionJson%>
    </script>
    <script type="application/javascript">
        <%=YahooDataJson%>
    </script>
     <script type="application/javascript">
        <%=ScupioJson%>
    </script>
     <script type="application/javascript">
         function scupioCheckoutPage() {
             var itemQty = parseInt($('#itemQty').val());
             <% if (ScupioCheckoutPageJson != null && ScupioCheckoutPageJson.Length > 10) { %>
               <%=ScupioCheckoutPageJson.Remove(ScupioCheckoutPageJson.Length - 5, 5) + ",\"quantity\":" + "itemQty" + "}]});"%>
             <% } %>
         }
    </script>
    <script type="text/javascript">

        $(document).ready(function () {
            if ($('#hidInvoiceCity').val() != '' && $('#hidInvoiceArea').val() != '') {
                $.cascadeAddress.setup('<%=WebUtility.GetSiteRoot()%>', function() {
                    $.cascadeAddress.bind($('#ddlInvoiceCity'), $('#ddlInvoiceArea'), $('#InvoiceAddress'), $('#hidInvoiceCity').val(), $('#hidInvoiceArea').val(), $('#InvoiceAddress').val(), window.deliveryToIsland);
                });
            }
            else {
                $.cascadeAddress.setup('<%=WebUtility.GetSiteRoot()%>', function() {
                    $.cascadeAddress.bind($('#ddlInvoiceCity'), $('#ddlInvoiceArea'), $('#InvoiceAddress'), -1, -1, $('#InvoiceAddress').val(), window.deliveryToIsland);
                });
            }
        })

        //避免text按enter而postback
        document.onkeydown = function() {  
            if (window.event)  
                if (event.keyCode == 13 && event.srcElement.nodeName == "INPUT" && event.srcElement.type == "text" && event.srcElement.id != "SecurityCode") 
                    return false;
        }  

        window.registerUrl = "<%=LunchKingSite.BizLogic.Component.MemberActions.MemberUtilityCore.RegisterUrl%>";
        function Share(bid, userId) {
            $.dnajax(ServerLocation + 'service/ppon/GetDealPromoUrl',
                "{'bid':'" + bid + "','userId': '" + userId + "'}", function(res) {
                    if (res.Code == window.ApiSuccessCode) {
                        showshare(res.Data);
                    } else {                    
                        bi();
                    }
                });
        }

        function BuyShareFB() {
            $.ajax({
                type: "POST",
                url: ServerLocation + 'user/service.aspx/PostToFaceBook',
                data: "{businessHourId:'" + buyData.dealId + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg.d != '') {
                        showshareFB(buyData.dealId);
                    } else {
                        fbShareWindow.close();
                        bi();
                    }
                }
            });
        }

        function showOrHidePaywayByTotal () {
            var total = buyData.pendingAmount;
            if (total <= 0) {
                if (getQtyAtCart() > 0) {
                    $('.SelectedPayWay').addClass('tempHidden');
                }
                $('#divConfirmPayWay').hide();
                if ($(':radio[name="ctl00$ctl00$ML$MC$PayWay"]:checked').val() === undefined) {
                    $('#rbPayWayByCreditCard').attr('checked', 'checked');
                }
                if (total < 0) {
                    //銀行檔次仍不顯示
                    if ($('#HfIsBankDeal').val() !== 'True') {
                        $('.SelectedPayWay').removeClass('tempHidden');
                        $('#divConfirmPayWay').show();
                        $("#totalErr").show();
                    }
                }
            } else {
                //銀行檔次仍不顯示
                if ($('#HfIsBankDeal').val() !== 'True') {
                    $('.SelectedPayWay').removeClass('tempHidden');
                    $('#divConfirmPayWay').show();
                }
            }
        }

        function calculateTotal() {
            var bcash = buyData.bcash;
            var scash = buyData.scash;
            if (bcash == null || bcash === '') bcash = 0;
            if (scash == null || scash === '') scash = 0;
            var Number = /^\d*$/;

            if (!Number.test(bcash)) {
                $("#bcashNaN").show();
                $("#bcashErr").hide();
                $("#txtbcash").focus();
                return;
            } else {
                $("#bcashNaN").hide();
                if ("" === bcash) {
                    bcash = 0;
                } else {
                    bcash = parseInt(bcash, 10);
                }
            }

            if (!Number.test(scash)) {
                $("#scashNaN").show();
                $("#scashErr").hide();
                $("#txtscash").focus();
                return;
            } else {
                $("#scashNaN").hide();
                if ("" === scash) {
                    scash = 0;
                } else {
                    scash = parseInt(scash, 10);
                }
            }

            var dAmount = buyData.discountAmount;

            var otherAmount = bcash + scash;
            var total = buyData.totalAmount - otherAmount;

            if (dAmount != 0) {
                total = total - dAmount;
                if ((total < 0) && (otherAmount == 0)) {
                    total = 0;
                }
            }
            
            buyData.pendingAmount = total;
            $("#totalErr").hide();

            //使用17Life購物金付全額仍需顯示發票資訊
            if (parseInt(total) + parseInt(scash) <= 0){
                $("#hInvoiceEditTitle").hide();
                $("#divInvoiceEditContent").hide();
                $("#divInvoiceConfirm").hide();
            }
            else{
                //銀行檔次仍不顯示
                if ($('#HfIsBankDeal').val() != 'True') {
                    $("#hInvoiceEditTitle").show();
                    $("#divInvoiceEditContent").show();
                    $("#divInvoiceConfirm").show();
                }
            }

            var orderAmount =buyData.totalAmount;
            if (buyData.deliveryType === 2) {
                if (orderAmount < 3000) {
                    //$('#rbInstallment12Months').closest('div').hide();
                    //if ($('#rbInstallment12Months').is(':checked')) {
                    //    $('#rbInstallment12Months').removeAttr('checked');
                    //    $('#rbPayWayByCreditCard').attr('checked', true);
                    //}
                } else {
                    $('#rbInstallment12Months').closest('div').show();
                }
                if (orderAmount < 2000) {
                    $('#rbInstallment6Months').closest('div').hide();
                    if ($('#rbInstallment6Months').is(':checked')) {
                        $('#rbInstallment6Months,#rbInstallmentDefault').removeAttr('checked');
                        $('#rbPayWayByCreditCard').attr('checked', true);
                    }
                } else {
                    $('#rbInstallment6Months').closest('div').show();
                }
            }
            console.log('total: ' + total +' / ' + orderAmount);
            $('#rbInstallment3Months').next('p').html('NT$' + Math.floor(total / 3) + ' x 3期 0利率');
            $('#rbInstallment6Months').next('p').html('NT$' + Math.floor(total / 6) + ' x 6期 0利率');
            $('#rbInstallment12Months').next('p').html('NT$' + Math.floor(total / 12) + ' x 12期 0利率');

            showOrHidePaywayByTotal();
        }

        function changeFreight(subTotal, combopack, oprice) {
            var freight = $("#hidFreight").val();
            var zeroFreightAmt = "0";
            var unit = "份";
            if (combopack > 1) {
                unit = "組";
            }

            if ("" != freight) {
                var freights = jQuery.parseJSON(freight);
                var currenFreight = undefined;
                var currentEndAmt = 0;
                $.each(freights, function(key, val) {
                    if (subTotal >= val.startAmt && subTotal < val.endAmt) {
                        currenFreight = val.freightAmt;
                        buyData.freightCharge = parseInt(val.freightAmt, 10) | 0;
                        currentEndAmt = val.endAmt;
                    }

                    if (subTotal < val.startAmt && val.freightAmt == 0 && ("0" == zeroFreightAmt || parseInt(zeroFreightAmt, 10) > parseInt(val.startAmt, 10))) {
                        zeroFreightAmt = val.startAmt;
                    }
                });

                if ("0" == zeroFreightAmt) {
                    var nextFreight = 0;
                    $.each(freights, function(key, val) {
                        if (currentEndAmt == val.startAmt) {
                            nextFreight = val.freightAmt;
                        }
                    });
                    if (0 != nextFreight && nextFreight < parseInt(currenFreight, 10)) {
                        $("#<%= lbldc.ClientID %>").html("<span class='hint'>-還差" + Math.ceil((currentEndAmt - subTotal) / oprice) + unit + "，運費將降為" + nextFreight + "元喔!</span>");
                    } else {
                        $("#<%= lbldc.ClientID %>").html("");
                    }
                } else {
                    if (currenFreight == "0") {
                        $("#<%= lbldc.ClientID %>").html("");
                    } else {
                        $("#<%= lbldc.ClientID %>").html("<span class='hint'>-還差" + Math.ceil((parseInt(zeroFreightAmt, 10) - subTotal) / oprice) + unit + "就可以免運費喔!</span>");
                    }
                }
            }
            setLTotal(subTotal + buyData.freightCharge );
        }

        function checkopt() {
            var totalCheck = true;
            var pendingAmount = buyData.pendingAmount;
            var bcash = buyData.bcash;
            var scash = buyData.scash;;
            if (bcash == null || bcash === '') bcash = 0;
            if (scash == null || scash === '') scash = 0;

            if (pendingAmount < 0) {
                totalCheck = false;
            }

            var Number = /^\d*$/;
            var bc = $("#txtbcash");
            if (!Number.test(bcash)) {
                $("#bcashNaN").show();
                $("#bcashErr").hide();
                $(bc).focus();
                totalCheck = false;
            } else if (parseInt(bcash, 10) < 0) {
                $("#bcashNaN").hide();
                $("#bcashErr").show();
                $(bc).focus();
                totalCheck = false;
            } else if (buyData.bcashMaxinum < bcash) {
                $("#bcashNaN").hide();
                $("#bcashErr").show();
                $(bc).focus();
                totalCheck = false;
            }

            var sc = $("#txtscash");
            if (!Number.test(scash)) {
                $("#scashNaN").show();
                $("#scashErr").hide();
                $(sc).focus();
                totalCheck = false;
            } else if (parseInt(scash, 10) < 0) {
                $("#scashNaN").hide();
                $("#scashErr").show();
                $(sc).focus();
                totalCheck = false;
            } else if (buyData.scashMaxinum < scash) {
                $("#scashNaN").hide();
                $("#scashErr").show();
                $(sc).focus();
                totalCheck = false;
            }

            if (!checkBuyerInfo(true)) {
                totalCheck = false;
            }

            if (!checkReceiverDelivery(true)) {
                totalCheck = false;
            }

            if (!checkBranch()) {
                totalCheck = false;
            }

            if (!CheckInvoiceInfo()) {
                totalCheck = false;
            }

            return totalCheck;
        }

        function checkBranch() {
            var rlt = true;
            $('option:selected', '#ddlStore').each(function() {
                if (this.text == '請選擇') {
                    $(this).parent().focus();
                    scroll(0, 250);
                    rlt = false;
                } else {
                    buyData.StoreInfo = $("#<%= ddlStoreCity.ClientID %> > option:selected").text() +
                        $("#<%= ddlStoreArea.ClientID %> > option:selected").text() + this.text;
                    $('.form-unit').has($(this)).removeClass("error");
                }
                if (this.text.indexOf('已售完') > -1) {
                    alert('請勿選擇已售完的選項，謝謝');
                    rlt = false;
                }
            });

            if (0 == $("#ddlStore").length || rlt) {
                $("#branchErr").hide();
                $('.form-unit').has($("#ddlStore")).removeClass("error");
                $("#tdStore").removeClass("error");
            } else {
                $("#branchErr").show();
                $("#tdStore").addClass("error");
            }

            return rlt;
        }

        function GetCreditCardName()
        {
            if($("#CardNo1").val().length == 4 && $("#CardNo2").val().length == 4 && $("#CardNo3").val().length == 4 && $("#CardNo4").val().length == 4)
            {
                var CardNo=$("#CardNo1").val()+$("#CardNo2").val()+$("#CardNo3").val()+$("#CardNo4").val();
                
                $.ajax({
                    type: "POST",
                    url: window.ServerLocation + "service/ppon/GetCreditCardName",
                    data: "{cardno:'" + CardNo + "',bankId:'" + <%=BankId %> + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        if(data.Code==window.ApiSuccessCode)
                        {
                            $("#txtCreditCardName").val(data.Data.BankName);
                        }
                        else {
                            alert(data.Data.BankName);
                            $("#CardNo1").val("");
                            $("#CardNo2").val("");
                            $("#CardNo3").val("");
                            $("#CardNo4").val("");
                            $("#CardNo1").focus();
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert("(" + xhr.status + ", " + thrownError + " " + ajaxOptions + ")");
                    }
                });
            }
        }

        function checkCardNo(tb) {
            $("#CardNo").removeClass("error");
            $("#errMsg").hide();
            var maxLength = $(tb).attr('maxlength');
            var reg = new RegExp("\\D");
            if (reg.test($(tb).val())) {
                $(tb).val($(tb).val().replace(reg, ''));
            }

            if ($(tb).val().length == maxLength) {
                var cc = $(".cc");
                cc.eq((cc.index($(tb))) + 1).focus();
            }
        }

        function checkSecurityCode(tb) {
            $('#SecurityCode').closest('.data-input').find('.if-error').hide();
            $('#securityCodeRow').removeClass('error');
            var reg = new RegExp("\\D");
            if (reg.test($(tb).val())) {
                $(tb).val($(tb).val().replace(reg, ''));
            }
        }

        function checkBuyerInfo(ck) {
            var result = true;

            if (checkMobile($("#BuyerMobile").val()) == false) {
                $("#buyerMobileErr").show();
                $("#buyerMobileNotice").hide();
                $("#tdBuyerMobile").addClass("error");
                $("#BuyerMobile").focus();
                result = false;
            } else {
                $("#buyerMobileErr").hide();
                $("#buyerMobileNotice").show();
                $("#tdBuyerMobile").removeClass("error");
            }

            if ($("#BuyerName").val() == "") {
                $("#buyerNameErr").show();
                $("#tdBuyerName").addClass("error");
                $("#BuyerName").focus();
                result = false;
            } else {
                $("#buyerNameErr").hide();
                $("#tdBuyerName").removeClass("error");
            }

            if ($('#BuyerEmail').is('[readonly]') == false && checkAccount($("#BuyerEmail").val()) == false) {
                $("#buyerEmailErr").show();
                $("#tdBuyerEmail").addClass("error");
                $("#BuyerEmail").focus();
                result = false;
            } else {
                $("#buyerEmailErr").hide();
                $("#tdBuyerEmail").removeClass("error");
            }

            return result;
        }

        function checkAddr() {
            var selectedcity = $("#ddlReceiverCity").children(":selected").html();
            var selectedArea = $("#ddlReceiverArea").children(":selected").html();
            var addr = $("#ReceiverAddress").val();
            if (addr == '' || selectedcity == "請選擇" || (selectedArea == "請選擇" || selectedArea == "")) {
                $("#receiverAddrErr").show();
                return false;
            }
            var s = addr.trim();
            if (s.length == 0) {
                $("#receiverAddrErr").show();
                return false;
            }

            var chineseregx = new RegExp(/[\u4e00-\u9fa5]/gi);
            if (!s.charAt(0).match(chineseregx)) {
                $("#receiverLostAddrErr").show();
                return false;
            }

            return true;
        }

        function checkReceiverDelivery(ck) {
            if ($("#ReceiverInfoPanel:visible").length > 0) {

                var recCheck = true;
                if ($("#ReceiverName").val() === "") {
                    $("#receiverNameErr").show();
                    $("#tdRecName").addClass("error");
                    $("#ReceiverName").focus();
                    recCheck = false;
                } else {
                    $("#receiverNameErr").hide();
                    $("#tdRecName").removeClass("error");
                }

                var productDeliveryType = window.getCurrentProductDeliveryType();
                if (productDeliveryType === window.ProductDeliveryType.Normal) {
                    if ($("#ReceiverMobile").val() === "") {
                        $("#receiverMobileErr").show();
                        $("#tdRecMobile").addClass("error");
                        $("#ReceiverMobile").focus();
                        recCheck = false;
                    } else {
                        $("#receiverMobileErr").hide();
                        $("#tdRecMobile").removeClass("error");
                    }

                    if (checkAddr() === false) {
                        if ($("#hiNotDeliveryIslands").val() === "True") {
                            $("#notDeliveryIslandsAddrErr").show();
                        }
                        $("#tdRecAddr").addClass("error");
                        $("#ReceiverAddress").focus();
                        recCheck = false;
                    } else {
                        var drb = $("#ddlReceiverArea option:selected").text();
                        if (drb === "請選擇" || drb === "") {
                            $("#receiverAddrErr").show();
                            if ($("#hiNotDeliveryIslands").val() === "True") {
                                $("#notDeliveryIslandsAddrErr").show();
                            }
                            $("#tdRecAddr").addClass("error");
                            $("#ddlReceiverArea").focus();
                            recCheck = false;
                        } else {
                            if ($("#ddlReceiverCity option:selected").text() === "請選擇") {
                                $("#receiverAddrErr").show();
                                if ($("#hiNotDeliveryIslands").val() === "True") {
                                    $("#notDeliveryIslandsAddrErr").show();
                                }
                                $("#tdRecAddr").addClass("error");
                                $("#ddlReceiverCity").focus();
                                recCheck = false;
                            } else {
                                $("#notDeliveryIslandsAddrErr").hide();
                                $("#tdRecAddr").removeClass("error");
                            }
                        }
                    }

                    if (recCheck) {
                        var receiverAddr = $("#ddlReceiverCity").children(":selected").html() +
                            $("#ddlReceiverArea").children(":selected").html() +
                            $("#ReceiverAddress").val();
                        $("#hidReceiverAddress").val(receiverAddr);
                    }

                } else if (productDeliveryType === window.ProductDeliveryType.FamilyIsp || 
                    productDeliveryType === window.ProductDeliveryType.SevenIsp) {
                    if ($("#ReceiverMobile").val() === "" || window.checkMobile($("#ReceiverMobile").val()) === false) {
                        $("#receiverMobileErr").show();
                        $("#tdRecMobile").addClass("error");
                        $("#ReceiverMobile").focus();
                        recCheck = false;
                    } else {
                        $("#receiverMobileErr").hide();
                        $("#tdRecMobile").removeClass("error");
                    }
                    if (productDeliveryType === window.ProductDeliveryType.FamilyIsp) {
                        if ($('#hidFamilyPickupStoreId').val() === "") {
                            $('#pickupStoreErr').show();
                            $('#tdStorePickup').addClass('error');
                            $('#btnSelectCVS').focus();
                            recCheck = false;
                        } else {
                            $('#pickupStoreErr').hide();
                            $('#tdStorePickup').removeClass('error');
                        }
                    } else if (productDeliveryType === window.ProductDeliveryType.SevenIsp) {
                        if ($('#hidSevenPickupStoreId').val() === "") {
                            $('#pickupStoreErr').show();
                            $('#tdStorePickup').addClass('error');
                            $('#btnSelectCVS').focus();
                            recCheck = false;
                        } else {
                            $('#pickupStoreErr').hide();
                            $('#tdStorePickup').removeClass('error');
                        }
                    }
                }

                return recCheck;
            } else {
                return true;
            }
        }

        function checkReceiptAddress() {
            var isEntrustSell = <%=EntrustSell != EntrustSellType.No ? "true" : "false" %>;

            if (isEntrustSell==false || $('input[id$=rbReceiptType2]').is(':checked')==false) {
                return true;;
            }

            var result = true;
            if($("#ddlReceiptCity option:selected").val() == "0") {
                $("#ddlReceiptCity").focus();
                result = false;
            }
            if($("#ddlReceiptArea option:selected").val() == "0") {
                $("#ddlReceiptArea").focus();
                result = false;
            }
            if($.trim($("#txtReceiptAddress").val()) == "") {
                $("#txtReceiptAddress").focus();
                result = false;
            }
            if (result == false) {
                $("#tdReceiptAddress").addClass("error").find('.message').show();
            } else {
                $("#tdReceiptAddress").removeClass("error").find('.message').hide();
            }

            if ($('#chkReceiptForBusiness').is(':checked') == true) {
                if ($.trim($('#txtReceiptTitle').val()) == '') {
                    $('#txtReceiptTitle').parent().parent().addClass("error");
                    $('#txtReceiptTitle').next('.message').show();
                    result = false;
                } else {
                    $('#txtReceiptTitle').parent().parent().removeClass("error");
                    $('#txtReceiptTitle').next('.message').hide();
                }
                if ($('#txtReceiptNumber').val().length  != 8) {
                    $('#txtReceiptNumber').parent().parent().addClass("error");
                    $('#txtReceiptNumber').next('.message').show();
                    result = false;
                } else {
                    $('#txtReceiptNumber').parent().parent().removeClass("error");
                    $('#txtReceiptNumber').next('.message').hide();
                }
            } else {
                $('#txtReceiptTitle').parent().parent().removeClass("error");
                $('#txtReceiptTitle').next('.message').hide();
                $('#txtReceiptNumber').parent().parent().removeClass("error");
                $('#txtReceiptNumber').next('.message').hide();
            }
            return result;
        }

        function checkAllowCardNum(cards, allowNums) {
            var isPass = false;
            var testCard = (cards.eq(0).val() + cards.eq(1).val()).substr(0, 6);
            console.log('testCard: ' + testCard);
            for (var key in allowNums) {
                if (allowNums[key] == testCard) {
                    isPass = true;
                    break;
                }
            }
            return isPass;
        }

        function errorInitial()
        {
            $("#CardNo").removeClass("error");
            $("#ExpDateRow").removeClass("error");
            $("#securityCodeRow").removeClass("error");
            $(".if-error").hide();
        }

        function checkAPIopt() {
            errorInitial();

            var reg = new RegExp("\\D");
            var cards = $(":text.cc");
            
            var isPass = true;

            if ($('#ddlSelectCreditCard').val() == '') {
                var isFocus = false;
                $.each(cards,function() {
                    if (reg.test($(this).val()) || $(this).val().length != $(this).attr('maxlength')) {
                        isPass = false;
                        if (isFocus == false) {
                            $(this).focus();
                        }
                        isFocus = true;
                    }
                });
                if (!isPass) {
                    $("#CardNo").addClass("error");
                    $("#errMsg").text('請輸入您的信用卡資訊');
                    $("#errMsg").show();
                    return false;
                }
            }

            if($('#ExpMonth').val() == '' || $('#ExpYear').val() == '')
            {
                $('#ExpDateRow').addClass('error');
                $('#ExpDate').closest('.data-input').find('.if-error').show();
                if($('#ExpMonth').val() == '')
                {
                    $('#ExpMonth').focus();
                }
                else
                {
                    $('#ExpYear').focus();
                }
                return false;
            }

            if(document.getElementById("SecurityCode"))
            {
                if ($('#SecurityCode').val().length != 3) {
                    $('#securityCodeRow').addClass('error');
                    $('#SecurityCode').closest('.data-input').find('.if-error').show();
                    $('#SecurityCode').focus();
                    return false;
                }
            }

            var checkEnabled = "<%= config.CheckCreditCardEnabled %>";
            if (checkEnabled == "True") {
                var creditCardNo = "";
                $.each(cards, function() {
                    creditCardNo += $(this).val();
                });
                // Luhn algorithm.
                if (!checkCreditCardWithLuhnAlgorithm(creditCardNo)) {
                    $("#CardNo").css("background-color", "#feb3b3").css("width", "95%");
                    $("#errMsg").text('信用卡卡號錯誤');
                    $("#errMsg").show();
                    return false;
                }
            }

            if ($('#hidIsTaishinCreditCardOnly').val() == 'True') {
                var taishinBankAllowNums = $('#hidTaishinCreditcards').val().split(',');
                var mmCardArray = $.grep(window.mmcards,function(t) {
                    return t.CardGuid == $('#ddlSelectCreditCard').val();
                });
                var mmcard = mmCardArray.length > 0 ? mmCardArray[0] : null;
                var isTaishinCreditCard;
                if (mmcard == null) {
                    isTaishinCreditCard = checkAllowCardNum(cards, taishinBankAllowNums);
                } else {
                    isTaishinCreditCard = mmcard.BankId == 1;
                }
                if (isTaishinCreditCard == false) {
                    $("#CardNo").css("background-color", "#feb3b3").css("width", "95%");
                    $("#errMsg").text('※僅限使用台新或國泰信用卡');
                    $("#errMsg").show();
                    return false;
                }
            }

            if ($('#hidInstallment').val() != '0') {
                var allowNums = $('#hidCreditcardsAllowInstallment').val().split(',');
                var mmCardArray = $.grep(window.mmcards,function(t) {
                    return t.CardGuid == $('#ddlSelectCreditCard').val();
                });
                var mmcard = mmCardArray.length > 0 ? mmCardArray[0] : null;
                var isInstallment;
                if (mmcard == null) {
                    isInstallment = checkAllowCardNum(cards, allowNums);
                } else {
                    isInstallment = mmcard.IsInstallment;
                }
                if(isInstallment == false)
                {
                    $.blockUI({ message: $('.card-check-warning'), css: { border: '0px' } });
                    return false;
                }
            }
            return true;
        }
        
        function checkDiscountCode() {
            var userName = $('#buyerUserName').val();
            var code = $("#tbDiscount").val();
            $("#hidValidDiscountCode").val(true);
            if (code != undefined) {
                if (code.length == 0) {
                    $("#couponMessage").text("");
                    buyData.discountAmount = 0;
                    calculateTotal();
                } else if (code.length > 1) {
                    var regex = new RegExp(/[\W_]+/);
                    if (regex.test(code)) {
                        $("#couponMessage").text("無效的序號").addClass('invalidDiscount');
                        buyData.discountAmount = 0;
                        $("#hidValidDiscountCode").val(false);
                        calculateTotal();
                    } else {
                        var data = {};
                        data.code = code;
                        data.user_name = userName;
                        data.dep = '<%=(int)DiscountCampaignUsedFlags.Ppon %>';
                        data.bid = buyData.dealId;
                        var dataJsonFormat = JSON.stringify(data);

                        $.ajax({
                            type: "POST",
                            url: "../WebService/DiscountService.asmx/GetDiscountAmountByUserAndDepForWeb",
                            data: dataJsonFormat,
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function(response) {
                                $("#hidIsVisaDiscountCode").val(false);
                                var obj = $.parseJSON(response.d);
                                
                                if (parseInt(obj.Discount) < 0) {
                                    switch(obj.Discount){
                                    case -2:
                                        $("#couponMessage").text("很抱歉！此筆 17Life 折價券已使用過，無法重複使用。").addClass('invalidDiscount');
                                        buyData.discountAmount = 0;
                                        break;
                                    case -1:
                                        $("#couponMessage").text("無效的序號").addClass('invalidDiscount');
                                        buyData.discountAmount = 0;
                                        break;
                                    case -3:
                                        $("#couponMessage").text("很抱歉！此筆 17Life 折價券無法在此館使用。").addClass('invalidDiscount');
                                        buyData.discountAmount = 0;
                                        break;
                                    case -4:
                                        $("#couponMessage").text("很抱歉！此筆 17Life 折價券尚未開放使用。").addClass('invalidDiscount');
                                        buyData.discountAmount = 0;
                                        break;
                                    case -5:
                                        $("#couponMessage").text("此折價券不適用此檔。請參考折價券來源的使用規則說明。").addClass('invalidDiscount');
                                        buyData.discountAmount = 0;
                                        break;
                                    case -6:
                                        $("#couponMessage").text("很抱歉，此活動折價券已達使用上限。").addClass('invalidDiscount');
                                        buyData.discountAmount = 0;
                                        break;
                                    case -7:
                                        $("#couponMessage").text("很抱歉，此活動折價券僅限本人使用。").addClass('invalidDiscount');
                                        buyData.discountAmount = 0;
                                        break;
                                    case -8:
                                        $("#couponMessage").text("很抱歉，此活動折價券僅限 " + obj.Message + " 使用。").addClass('invalidDiscount');
                                        buyData.discountAmount = 0;
                                        break;
                                    case -9:
                                        $("#couponMessage").html("此筆折價券限於App購買使用。" + GetAppBuyUrl(navigator.userAgent.toString())).addClass('invalidDiscount');
                                        buyData.discountAmount = 0;
                                        break;
                                    default:
                                        break;
                                        $("#hidValidDiscountCode").val(false);
                                    }
                                } else {
                                    if (buyData.totalAmount < parseInt(obj.Minimum)) {
                                        $("#couponMessage").text("很抱歉！此筆 17Life 折價券需訂單滿 NT$" + obj.Minimum + " 方可使用。").addClass('invalidDiscount');
                                        buyData.discountAmount = 0;
                                        $("#hidValidDiscountCode").val(false);
                                    } else if (buyData.totalAmount < parseInt(obj.Discount)) {
                                        $("#couponMessage").text("很抱歉！此筆 17Life 折價券需訂單滿 NT$" + obj.Discount + " 方可使用。").addClass('invalidDiscount');
                                        buyData.discountAmount = 0;
                                        $("#hidValidDiscountCode").val(false);
                                    } else {
                                        $("#couponMessage").text("可抵用金額" + obj.Discount).removeClass('invalidDiscount');
                                        buyData.discountAmount = obj.Discount | 0;
                                        if ((obj.Flag & <%=(int)DiscountCampaignUsedFlags.VISA %>) > 0) {
                                            $("#hidIsVisaDiscountCode").val(true);
                                        }
                                    }
                                }
                                calculateTotal();
                            }
                        });//end of ajax
                    }
                }
            }
        }
        function UpdateDiscountCodeList(totalAmountBeforeUseDiscount)
        {
            var ul = $('#discountCodeList'),
                li = ul.children('li'),
                ulM = $('#discountCodeMList'),
                liM =  ulM.children('li');

            li.detach().each(function (i, el) {
                var $el = $(el);
                if(totalAmountBeforeUseDiscount >= $el.data('amount')){
                    $el.removeClass('disabled');
                    $el.find('.cover').hide();
                }
                else{
                    $el.addClass('disabled');
                    $el.find('.cover').show();
                }
            });

            liM.detach().each(function (i, el){
                var $el = $(el);
                if(totalAmountBeforeUseDiscount >= $el.data('amount')){
                    $el.removeClass('disabled');
                    $el.find('.cover_m').hide();
                }
                else{
                    $el.addClass('disabled');
                    $el.find('.cover_m').show();
                }
            });
    
            ulM.append(liM);
            ul.append(li);
        }

        function GetAppBuyUrl(userAgent) {
            var appBuyUrl;
            //android
            if (userAgent.toLowerCase().indexOf("android") > -1) {
                appBuyUrl = "<%=string.Format("<a href='{0}/Ppon/OpenApp.aspx?bid={1}'>點此開啟App購買</a>", config.SiteUrl, BusinessHourGuid)%>";
            }
            //IOS
            else if ((userAgent.toLowerCase().indexOf("ipad") > -1)
                || (userAgent.toLowerCase().indexOf("iphone") > -1)
                || (userAgent.toLowerCase().indexOf("ipod") > -1)) {
                appBuyUrl = "<%=string.Format("<a href='open17life://www.17life.com?bid={0}'>點此開啟App購買</a>",  BusinessHourGuid)%>";
            } else //其他
            {
                appBuyUrl = "<%=string.Format("<a target='_blank' href='{0}/ppon/promo.aspx?cn=17APP&rsrc=APP_WebBN'>點此開啟App購買</a>", config.SiteUrl)%>";
            }
            return appBuyUrl;
        }

        function SelectDiscount(obj) {
            $('.discount_box li').each(function() {
                $(this).find('a').removeClass('on');
            });

            $(obj).find('a').addClass('on');
            $('#tbDiscount').val($(obj).attr('id'));

            CloseDiscount();
            checkDiscountCode();
        }

        function ShowDiscount() {
            if ($(window).width() <= 480) {
                $('#discountMList').show();
                //$('#discountCodeMList li .cover_m').each(function(){
                //    $(this).css('line-height', $(this).parent().height()+'px');
                //});
                $('html,body').animate({ scrollTop: 0 }, 0);
                $('#divFillPurchaseInfo').hide();
            } else {
                $('#discountList').fadeIn(500);
                //$('#discountCodeList li .cover').each(function(){
                //    $(this).height($(this).parent().height());
                //    $(this).css('line-height', $(this).parent().height()+'px');
                //});
            }
        }

        function CloseDiscount() {
            if ($(window).width() <= 480) {
                $('#discountMList').hide();
                $('#divFillPurchaseInfo').show();
                $('#tbDiscount').focus();
            } else {
                $('#discountList').fadeOut(500);
            }
        }

        function CheckInvoiceInfo() {
            var rlt = true;
            if ($("#rbInvoiceDuplicate").is(":checked")) {
                if ($("#chkPersonalCarrier").is(":checked")) {
                    var carrier = $('#ddlCarrierType').val();
                    if (carrier == '<%= (int)CarrierType.Phone %>') {
                        var phoneCarrier = $.trim($('#txtPhoneCarrier').val());
                        if (phoneCarrier == '') {
                            $('#tdPhoneCarrier').addClass("error");
                            $('#phoneCarrierErr').show();
                            rlt = false;
                        } else {
                            var phone_code39 = /^\/[0-9A-Z\-.$/\+%\*\s]*$/;
                            if (!phone_code39.test(phoneCarrier) || phoneCarrier.length != 8 || $('#hidCarrIdVerify').val() == 'false') {
                                $('#phoneCarrierInvalid').show();
                                $('#tdPhoneCarrier').addClass("error");
                                rlt = false;
                            } else {
                                $.ajax({
                                    type: "POST",
                                    contentType: "application/json; charset=utf-8",
                                    url: "Buy.aspx/IsValidPhoneCarrier",
                                    data: '{carrid: "' + $('#txtPhoneCarrier').val() + '" }',
                                    async: false,
                                    dataType: "json",
                                    success: function (data) {
                                        if (data.d == true) {
                                            $('#phoneCarrierInvalid').hide();
                                            $('#tdPhoneCarrier').removeClass("error");
                                        }
                                        else {
                                            $('#phoneCarrierInvalid').show();
                                            $('#tdPhoneCarrier').addClass("error");
                                            rlt = false;
                                        }
                                    },
                                    complete: function () {
                                    },
                                    error: function (data) {
                                        console.log(data);
                                    }
                                });
                            }
                        }
                    } else if (carrier == '<%= (int)CarrierType.PersonalCertificate %>') {
                        var PersonalCertificateCarrier = $.trim($('#txtPersonalCertificateCarrier').val());
                        if (PersonalCertificateCarrier == '') {
                            $('#tdPersonalCertificateCarrier').addClass("error");
                            $('#personalCertificateCarrierErr').show();
                            rlt = false;
                        } else {
                            var PersonalCertificate = /^[A-Z][A-Z]\d{14}$/;
                            if (!PersonalCertificate.test(PersonalCertificateCarrier)) {
                                $('#tdPersonalCertificateCarrier').addClass("error");
                                $('#personalCertificateCarrierInvalid').show();
                                rlt = false;
                            } else {
                                $('#tdPersonalCertificateCarrier').removeClass("error");
                                $('#personalCertificateCarrierInvalid').hide();
                            }
                        }
                    } else if (carrier == '<%= (int)CarrierType.None %>') {

                        if ($.trim($('#InvoiceBuyerName').val()) == "") {
                            $('#invoNameErr').show();
                            $("#tdInvoName").addClass("error");
                            $('#InvoiceBuyerName').focus();
                            rlt = false;
                        } else {
                            $('#invoNameErr').hide();
                            $("#tdInvoName").removeClass("error");
                        }

                        if ($.trim($('#InvoiceAddress').val()) == "" || $.trim($('#ddlInvoiceCity').val())=="-1" || $.trim($('#ddlInvoiceArea').val())=="-1") {
                            $('#invoAddrErr').show();
                            $("#tdInvoAddr").addClass("error");
                            $('#InvoiceAddress').focus();
                            rlt = false;
                        } else {
                            $('#invoAddrErr').hide();
                            $("#tdInvoAddr").removeClass("error");
                        }
                    }
                    return rlt;
                } else {
                    $('#ddlCarrierType').val( '<%= (int)CarrierType.Member %>');
                    return true;
                }
            } else if ($("#rbInvoiceDonation").is(":checked")) {
                var lcVal = $.trim($("#txtLoveCode").val());
                var loveCodeExist = $.grep(window.availableLoveCodes, function(lc) {
                    return lc.value == lcVal;
                }).length > 0;
                if (lcVal == '' || loveCodeExist == false) {
                    $('#tdLoveCode').addClass('error');
                    $('#loveCodeErr').show();
                    $("#LoveCodeDesc").html('請於上方輸入正確的愛心碼。');
                    rlt = false;
                } else {
                    $('#tdLoveCode').removeClass('error');
                    $('#loveCodeErr').hide();
                }
                return rlt;
            } else if ($("#rbInvoiceTriplicate").is(":checked")) {
                if ($.trim($('#InvoiceTitle').val()) == "") {
                    $('#invoTitleErr').show();
                    $("#tdInvoTitle").addClass("error");
                    $("#InvoiceTitle").focus();
                    rlt = false;
                } else {
                    $('#invoTitleErr').hide();
                    $("#tdInvoTitle").removeClass("error");
                }

                if ($.trim($("#InvoiceBuyerName").val()) == "") {
                    $('#invoNameErr').show();
                    $("#tdInvoName").addClass("error");
                    $("#InvoiceBuyerName").focus();
                    rlt = false;
                } else {
                    $('#invoNameErr').hide();
                    $("#tdInvoName").removeClass("error");
                }

                if ($.trim($("#InvoiceNumber").val()) == "") {
                    $('#invoNumberErr').show();
                    $("#tdInvoNumber").addClass("error");
                    $("#InvoiceNumber").focus();
                    rlt = false;
                } else {
                    if (checkCompanyTaxNumber($("#InvoiceNumber").val()) === false) {
                        $('#invoNumberErr').show();
                        $("#tdInvoNumber").addClass("error");
                        $("#InvoiceNumber").focus();
                        rlt = false;
                    } else {
                        $('#invoNumberErr').hide();
                        $("#tdInvoNumber").removeClass("error");
                    }
                }

                if ($.trim($("#InvoiceAddress").val()) == "" || $.trim($('#ddlInvoiceCity').val())=="-1" || $.trim($('#ddlInvoiceArea').val())=="-1") {
                    $('#invoAddrErr').show();
                    $("#tdInvoAddr").addClass("error");
                    $("#InvoiceAddress").focus();
                    rlt = false;
                } else {
                    $('#invoAddrErr').hide();
                    $("#tdInvoAddr").removeClass("error");
                }

                return rlt;
            }

            return true;
        }

        function block_postback() {
            $.blockUI({ message: "<h1><img src='../Themes/PCweb/images/spinner.gif' />  交易進行中...</h1>", css: { width: '150px' } });
        }

        function checkForBuy() {
            return checkBankActivity() &&
                checkShoppingCart() &&
                checkopt() &&
                checkInstallmentOptions() &&
                checkGuestMemberAgreePolicy() &&
                checkTaishinPay();
        }

        function ClickBuy() {
            if (checkForBuy()) {
                setItemList();

                scupioCheckoutPage();

                showConfirmPage();
            }
        }
        
        function openTaishinPage(){
            window.open("<%=config.TaishinThirdPartyPayIndexUrl%>");
        }

        function checkTaishinPay(){
            if($('#rbPayWayTaishinPay').is(':checked')) {
                if($("#hidIsTaishinPayLinked").val() == 'True'){
                    //已串接檢查餘額
                    if(buyData.pendingAmount <= parseInt($('.TaishinCashPoint').text())) {
                        return true;
                    } else {
                        $.blockUI({ message: $('.taishin-pay-check-warning'), css: { border: '0px' } });
                        return false;
                    }
                } else {
                    //未串接顯示會員條款
                    $('.tk_jump_alert').bPopup({
                        fadeSpeed: 'slow', //can be a string ('slow'/'fast') or int
                        followSpeed: 500, //can be a string ('slow'/'fast') or int
                        modalColor: 'black',
                    });
                    $(function(){
                        var bPopup = $('.tk_jump_alert').bPopup();
                        $('.tk_jump_close').click(function(){
                            bPopup.close();
                        });
                        $('.tk_jump_close_and_go').click(function(){
                            window.location = "../mvc/ThirdPartyPay/TaishinBindBegin?returnUrl=<%=config.SSLSiteUrl%>/ppon/buy.aspx?bid=" + buyData.dealId;
                            bPopup.close();
                        });
                    });
                    return false;
                }
            }
            return true;
        }

        function checkInstallmentOptions() {
            if ($('#rbInstallmentDefault').is(':checked') == false) {
                return true;
            }
            if ($('#rbInstallment3Months').is(':checked') ||
                $('#rbInstallment6Months').is(':checked') || $('#rbInstallment12Months').is(':checked')) {
                return true;
            }
            alert('您尚未勾選刷卡分期數');
            return false;
        }

        function checkGuestMemberAgreePolicy() {
            if ($('#chkGuestMemberAgreePolicy').size() == 0 || $('#chkGuestMemberAgreePolicy').is(':checked') == true) {
                return true;
            }
            alert('您尚未勾選同意服務條款，請勾選後再點選"下一步"');
            return false;
        }

        function checkBankActivity() {
            //銀行檔次利用金額判斷是否正確
            if ($('#HfIsBankDeal').val() == 'True') {
                var dAmount = buyData.discountAmount;
                var total = buyData.totalAmount;
                if (total - dAmount != 0) {
                    alert('請輸入正確的活動序號。');
                    return false;
                }
            }
            return true;
        }

        function checkShoppingCart() {
            var totalCheck = true;

            if ("0" != $("#setError").html()) {
                $("#deliveryChargeError").show();
                $("#itemQty").focus();
                totalCheck = false;
            } else {
                $("#deliveryChargeError").hide();
            }

            if (!checkBranch()) {
                totalCheck = false;
            }

            if (!checkBuyerInfo(false)) {
                totalCheck = false;
            }

            if (!checkReceiverDelivery(false)) {
                totalCheck = false;
            }

            if (!checkReceiptAddress()) {
                totalCheck = false;
            }

            if (!CheckInvoiceInfo()) {
                totalCheck = false;
            }

            if (!checkVISACoupon()) {
                totalCheck = false;
            }

            if ($("#hidValidDiscountCode").val() == 'false') {
                $('#tbDiscount').focus();
                totalCheck = false;
            }

            return totalCheck;
        }

        function checkVISACoupon() {
            if ($("#hidIsVisaDiscountCode").val() == 'true') {
                if ($('#rbPayWayByMasterPass').is(':checked') || $('#rbPayWayByAtm').is(':checked')) {
                    $("#couponMessage").html("您使用的折價券不適用此付款方式，請選擇「信用卡付款」(限刷VISA 金融卡)，或取消使用折價券。");
                    $('#tbDiscount').focus();
                    return false;
                } else if ($('#rbPayWayByCreditCard').is(':checked') && buyData.pendingAmount === 0) {
                    $("#couponMessage").html("您使用的折價券不適用此付款方式，請選擇「信用卡付款」(限刷VISA 金融卡)，或取消使用折價券。");
                    $('#tbDiscount').focus();
                    return false;
                }

            }
            return true;
        }

       

        function BackToEdit() {
            $('#divFillPurchaseInfo').show();
            $('#divPurchaseInfoConfirm').hide();
            scroll(0, 180);
        }

        function showConfirmPage() {
            // 切換Panel
            $('#divFillPurchaseInfo').hide();
            $('#divPurchaseInfoConfirm').show();
            if ($('#lblSingleStore').text() != '') {
                buyData.storeInfo = $('#lblSingleStore').text();
            }

            if ("1" != $("#hidComboPack").val()) {
                $("#confirmComboSum").show();
            }

            fillBuyerInfo();
            fillReceiverInfo();

            var isEntrustSell = <%=EntrustSell != EntrustSellType.No ? "true" : "false"%>;
            if (isEntrustSell) {
                fillReceiptInfo();
            } else {
                fillInvoiceInfo();
            }
            fillPayWayInfo();

            calculateTotal();

            scroll(0, 180);
        }

        //[obsolete]
        function fillBuyerInfo() {
            $("#buyerNameInfo").html($("#BuyerName").val());
            $("#buyerTel").html($("#BuyerMobile").val());
            if ($("#ddlBuyerCity").val() != -1 && $("#ddlBuyerArea").val() != -1) {
                var buyerAddr = $("#ddlBuyerCity").children(":selected").html() +
                    $("#ddlBuyerArea").children(":selected").html() +
                    $("#BuyerAddress").val();
                $("#BuyerAddressConfirm").html(buyerAddr);
            } else {
                $("#ConfirmBuyerAddress").hide();
            }
        }

        function fillReceiverInfo() {
            $("#ConfirmReceiverName").html($("#ReceiverName").val());
            $("#ConfirmReceiverTel").html($("#ReceiverMobile").val());
            var receiverAddr = $("#ddlReceiverCity").children(":selected").html() +
                $("#ddlReceiverArea").children(":selected").html() +
                $("#ReceiverAddress").val();
            if ($('#rdoProductDeliveryNormal').is(':checked')) {
                $("#ConfirmReceiverAddress").html(receiverAddr);
                $('#productDeliveryUnitTitle').text('地址');
                $('#cProductDeliveryTypeRow').show();
                $('#cProductDeliveryType').text('宅配到府');
            }
        }

        function fillInvoiceInfo() {
            $('#divInvoiceConfirm')
                .find('.Duplicate2_PhoneCarrier, .Duplicate2_PersonalCertificate, .Duplicate2_Donation, .Duplicate2_Paper, .Duplicate3_Paper')
                .css('display', 'none');

            var addr = $("#<%= ddlInvoiceCity.ClientID %>").children(":selected").html() +
                       $("#<%= ddlInvoiceArea.ClientID %>").children(":selected").html() +
                       $("#<%= InvoiceAddress.ClientID %>").val();

            if ($("#rbInvoiceDuplicate").is(":checked")) {
                if ($('#ddlCarrierType').val() == '<%= (int)CarrierType.None %>') {
                    $('#atmInvoice').html('個人紙本發票(二聯式)');
                    $('.Duplicate2_Paper').show();
                    $('#confirmInvoiceName').html($('#InvoiceBuyerName').val());
                    //$('#confirmInvoiceAddress').html($('#InvoiceAddress').val());
                    $('#confirmInvoiceAddress').text(addr);
                    $('#hidConfirmInvoiceAddress').val(addr);
                } else {
                    $('#atmInvoice').html('個人電子發票(二聯式)');
                    if ($('#ddlCarrierType').val() == '<%= (int)CarrierType.Phone %>') {
                        $('.Duplicate2_PhoneCarrier').show();
                        $('#confirmDuplicate2PhoneCarrier').html($('#txtPhoneCarrier').val());
                    } else if ($('#ddlCarrierType').val() == '<%= (int)CarrierType.PersonalCertificate %>') {
                        $('.Duplicate2_PersonalCertificate').show();
                        $('#confirmDuplicate2PersonalCertificate').html($('#txtPersonalCertificateCarrier').val());
                    }
                }
                return;
            } else if ($("#rbInvoiceDonation").is(":checked")) {
                $('#atmInvoice').html('捐贈發票');
                $('.Duplicate2_Donation').show();
                $('#confirmDuplicate2DonationCode').html($('#txtLoveCode').val());
                $('#confirmDuplicate2DonationDesc').html($('#LoveCodeDesc').html());
                return;
            } else if ($("#rbInvoiceTriplicate").is(":checked")) {
                $('#atmInvoice').html('公司紙本發票(三聯式)');
                $('.Duplicate3_Paper').show();
                $('.Duplicate2_Paper').show();
                $("#confirmInvoiceTitle").html($("#InvoiceTitle").val());
                $("#confirmInvoiceNumber").html($("#InvoiceNumber").val());
                $("#confirmInvoiceName").html($("#InvoiceBuyerName").val());
                //$("#confirmInvoiceAddress").html($("#InvoiceAddress").val());
                $('#confirmInvoiceAddress').text(addr);
                $('#hidConfirmInvoiceAddress').val(addr);
            }
        }

        function fillReceiptInfo() {
            if ($("#rbReceiptType1").is(":checked")) {
                $("#receiptType").html("收據託管");

            } else {
                $("#receiptType").html("寄送代收轉付收據");
                var addr =
                    $("#ddlReceiptCity").children(":selected").html() +
                        $("#ddlReceiptArea").children(":selected").html() +
                        $("#txtReceiptAddress").val();

                $("#receiptTitle").text($("#txtReceiptTitle").val());
                $("#receiptNumber").text($("#txtReceiptNumber").val());
                $("#receiptAddress").text(addr);
            }
            if ($.trim($("#receiptType").text()) == '') {
                $('.form-unit').has('#receiptType').hide();
            }
            if ($.trim($("#receiptAddress").text()) == '') {
                $('.form-unit').has('#receiptAddress').hide();
            }
            if ($.trim($("#receiptNumber").text()) == '') {
                $('.form-unit').has('#receiptNumber').hide();
            }
            if ($.trim($("#receiptTitle").text()) == '') {
                $('.form-unit').has('#receiptTitle').hide();
            }
        }

        function fillPayWayInfo() {
            $("img[id$=imgMasterPassLogo]").hide();
            $("img[id$=imgWalletLogo]").hide();
            $('#btnPayWayProcess').show();
            $('#btnForApplePay').hide();
            var payway = '';
            if ($('#rbPayWayByCreditCard').is(':checked')) {
                payway = $('#rbPayWayByCreditCard').next('label').html();
            } else if ($('#rbPayWayTaishinPay').is(':checked')) {
                payway = $('#rbPayWayTaishinPay').next('label').html();
            } else if ($('#rbInstallment3Months').is(':checked')) {
                payway = '信用卡付款 (分3期)';
            } else if ($('#rbInstallment6Months').is(':checked')) {
                payway = '信用卡付款 (分6期)';
            } else if ($('#rbInstallment12Months').is(':checked')) {
                payway = '信用卡付款 (分12期)';
            } else if ($('#rbPayWayByAtm').is(':checked')) {
                payway = $('#rbPayWayByAtm').next('label').html();
            } else if ($('#rbPayWayByMasterPass').is(':checked')) {
                var masterPassCard = $("#ddlMasterPassCards").val();
                if (masterPassCard) {
                    $("img[id$=imgMasterPassLogo]").show();
                    $("img[id$=imgWalletLogo]").show();
                    var cardValue = masterPassCard.split(',');
                    payway = "<p>" + $("#ddlMasterPassCards option:selected").text() + "</p>";
                    payway += "<p>有效期限: " + cardValue[1] + "/" + cardValue[2] + "</p>";
                } else {
                    payway = $('#rbPayWayByMasterPass').next('label').html();
                    payway = payway + "&nbsp;&nbsp;<img width='48' height='30' alt='' src='../Themes/PCweb/images/masterpass.png' style='vertical-align:bottom' />";
                }
            } else if ($('#rbPayWayByLinePay').is(':checked')) {
                payway = $('#rbPayWayByLinePay').next('label').html();
                payway = payway + "&nbsp;&nbsp;<img src='../Themes/PCweb/images/linepay_76x30.png' width='76' height='30' alt='' style='vertical-align:bottom' />";
            } else if ($('#rbPayWayApplePay').is(':checked')){
                payway = payway + "&nbsp;&nbsp;<img src='../Themes/PCweb/images/ApplePay-black.png' width='48' height='' alt='' style='vertical-align:bottom' />";
                payway = payway + "&nbsp;&nbsp;<p class='blank'>使用Apple Pay付款</p>";
                $('#btnPayWayProcess').hide();
                $('#btnForApplePay').show();
            } else if ($('#rbPayWayFamilyIsp').is(':checked')) {
                payway = '全家取貨付款';
            } else if ($('#rbPayWaySevenIsp').is(':checked')) {
                payway = '7-11取貨付款';
            } else {
                payway = "發生錯誤";
            }
            $('#ConfirmPayWay').html($.trim(payway));
        }

        function copyBuyerInfo() {
            $('#receiverSelect').val('0');
            if ($("#chkBuyerSync").is(":checked")) {
                $("#receiverNameErr").hide();
                $("#tdRecName").removeClass("error").addClass("InvoiceTitleName");
                $("#receiverMobileErr").hide();
                $("#tdRecMobile").removeClass("error").addClass("InvoiceTitleName");
                $("#receiverAddrErr").hide();
                $("#receiverLostAddrErr").hide();
                $("#notDeliveryIslandsAddrErr").hide();
                $("#tdRecAddr").removeClass("error").addClass("InvoiceTitleName");
                $("#ReceiverName").val($("#BuyerName").val());
                $("#ReceiverMobile").val($("#BuyerMobile").val());
                $.cascadeAddress.copy($("#ddlBuyerCity"), $('#ddlBuyerArea'), $('#BuyerAddress'),
                    $('#ddlReceiverCity'), $('#ddlReceiverArea'), $('#ReceiverAddress'), window.deliveryToIsland);
            } else {
                $("#ReceiverName").val("");
                $("#ReceiverMobile").val("");
                $.cascadeAddress.update(
                    $("#ddlReceiverCity"),
                    $('#ddlReceiverArea'),
                    $("#ReceiverAddress"),
                    -1, -1, '', false);
            }
        }

        function updateReceiver(recId) {
            if (0 == recId) {
                $("#ReceiverName").val("");
                $("#ReceiverMobile").val("");
                $.cascadeAddress.update(
                    $("#ddlReceiverCity"),
                    $('#ddlReceiverArea'),
                    $("#ReceiverAddress"),
                    -1, -1, '', false);
            } else {
                $("#receiverNameErr").hide();
                $("#tdRecName").removeClass("error").addClass("InvoiceTitleName");
                $("#receiverMobileErr").hide();
                $("#tdRecMobile").removeClass("error").addClass("InvoiceTitleName");
                $("#receiverAddrErr").hide();
                $("#receiverLostAddrErr").hide();
                $("#notDeliveryIslandsAddrErr").hide();
                $("#tdRecAddr").removeClass("error").addClass("InvoiceTitleName");
                $.dnajax(window.ServerLocation + "service/ppon/GetReceiver", "{id:'" + recId + "'}", function(res) {
                    if (res == null) return;
                    $("#ReceiverName").val(res.name);
                    $("#ReceiverMobile").val(res.mobile);
                    $.cascadeAddress.update(
                        $("#ddlReceiverCity"),
                        $('#ddlReceiverArea'),
                        $("#ReceiverAddress"),
                        parseInt(res.city, 10), parseInt(res.town, 10), res.addr, false);
                        ValidateBuyerAddress();
                });
            }
        }

        function checkOptionLimit(id, qty, old) {
            var result = "";
            var dirty = false;
            var json = $.parseJSON($("#hidCurrentLimits").val());
            $.each(json, function() {
                if (id.indexOf(this.AccessoryId) >= 0) {
                    if (this.Limit < qty) {
                        result = this.AccessoryName;
                        return false;
                    } else {
                        this.Limit = this.Limit - qty - old;
                        dirty = true;
                    }
                }
            });

            if ("" === result && dirty) {
                $("#hidCurrentLimits").val(JSON.stringify(json));
            }

            return result;
        }

        function addOptionLimitBack(id, qty) {
            var dirty = false;
            var json = $.parseJSON($("#hidCurrentLimits").val());
            $.each(json, function() {
                if (id.indexOf(this.AccessoryId) >= 0) {
                    this.Limit = this.Limit + qty;
                    dirty = true;
                }
            });

            if (dirty) {
                $("#hidCurrentLimits").val(JSON.stringify(json));
            }
        }

        function getLackOfQuantityOfComboPack() {
            var cset = getComboPack();
            var cselect = getQtyAtCart();
            var y = Math.ceil(cselect / cset);
            $("#totalComboSet").html(y);
            $("#totalConfirmComboSet").html(y);
            $("#totalConfirmComboTotal").html(y * buyData.price);

            if (0 == cselect || 0 != (cselect % cset)) {
                return (cset - (cselect % cset));
            }

            return 0;
        }

        function checkSameItem(ids, qty) {
            var result = "0";
            var bd = null;
            $.each($("#itemList li"), function(index, val) {
                if ($(val).find('.clearfix span:eq(0)').text().indexOf(ids) >= 0) {
                    result = $(val).find('.clearfix span:eq(1)').text();
                    bd = $(val).find('.item-amount input');
                }
            });

            var ky = checkOptionLimit(ids, qty, parseInt(result, 10));
            if ("" != ky) {
                alert("抱歉，您所選擇的(" + ky + ")數量不足，請重新選擇數量或其他規格。");
                return "-1";
            } else {
                if (null != bd) {
                    delItemFromCart($(bd));
                }
            }

            return result;
        }

        window.guestbuyAmountLimit = <%=_GUESTBUY_LIMIT_PRICE %>;
        window.isAuthenticated = '<%=this.IsAuthenticated%>'.toLocaleLowerCase() == 'true';
        window.isShoppingCart = '<%= IsShoppingCart %>'.toLowerCase() == 'true';
        
        window.loadCart = $('#hidItemOptionsJson').val() !== '';
        window.loadOtherInfo = $('#hidOtherInfoJson').val() !== '';
        window.getQtyAtCart = function () {
            return parseInt($("#hidqty").val(), 10);
        };
        window.setQtyAtCart = function (qty) {
            $("#hidqty").val(qty.toString());
        };

        window.getQtyLimit = function() {
            return parseInt($("#hidQtyLimit").val(), 10);
        };

        window.getIspQtyLimit = function() {
            return parseInt($("#hidIspQuantityLimit").val(), 10);
        }

        window.getComboPack = function() {
            return parseInt($("#hidComboPack").val(), 10);
        };

        window.getItemQty = function() {
            return parseInt($("#itemQty").val(), 10);
        };

        window.setLTotal = function(ltotal) {
            buyData.totalAmount = ltotal;
            //每次更新總金額，需再檢查"折價券是否可用"&"折價券選單的狀態"
            checkDiscountCode();
            UpdateDiscountCodeList(ltotal);

        }

        window.siteUrl = '<%=VirtualPathUtility.AppendTrailingSlash(WebUtility.GetSiteRoot())%>';            

        function checkItemOptions() {
            var result = {titles: [], opts: [], valid: false};
            $('option:selected', '#itemOptions').each(function () {
                if (this.text.indexOf('已售完') > -1) {
                    alert('請勿選擇已售完的選項，謝謝');
                    return result;
                }
                if ($(this).val() == "Default") {
                    return result;
                } else {
                    result.titles.push(this.text);
                    result.opts.push($(this).val());
                }
            });
            if (result.opts.length > 0 && result.opts.length == $('option:selected', '#itemOptions').length) {
                result.valid = true;
            }
            return result;
        }
        function resetItemAtCart() {
            if (window.isShoppingCart) {
                return;
            }
            $("#itemError").hide();
            var qty = getItemQty();
            var qtyLimit = getQtyLimit();                
            var comboSet = getComboPack();

            if (qtyLimit < Math.ceil(qty / comboSet)) {
                alert("最多只能購買" + qtyLimit + "份。");
                return;
            }

            $('.cart-item').attr('qty', qty);

            setQtyAtCart(qty);
            subTotal(buyData.price, getQtyAtCart());
            window.calculateTotal();
            window.BuyEvents.OnIspQuantityChange.fire();
            window.BuyEvents.OnIspOptionsShowOrHide.fire();
        }
        function addItemToCart() {
            var tt = checkItemOptions();
            console.log('tt.valid: ' + tt.valid);
            if (tt.valid || window.isShoppingCart == false) {
                $("#itemError").hide();

                var qty = getItemQty();
                var qtyLimit = getQtyLimit();                
                var comboSet = getComboPack();

                if (qtyLimit < Math.ceil((getQtyAtCart() + qty) / comboSet)) {
                    alert("最多只能購買" + qtyLimit + "份。");
                    return;
                }

                var optTitles = '(' + tt.titles.join(') (') + ')';
                var optIds = tt.opts.join(',');
                var qk = parseInt(checkSameItem(optIds, qty), 10);

                if (-1 != qk) {
                    qty += qk;
                    var unitAdd = " x " + qty;

                    var hidCell = "<span style='display:none;'>" +  optIds + "</span><span style='display:none;'>" + qty + "</span>";

                    if (1 == comboSet) {
                        unitAdd = "$" + buyData.price + unitAdd;
                        var liItem = $("<li class='cart-item'><div class='item-name'>" + buyData.dealTitle + optTitles +
                            "</div><div class='item-amount'>" + unitAdd +
                            "<input type='button' class='btn btn-mini rd-margin cartDelete' onclick='delItemFromCart(this);window.BuyEvents.OnIspQuantityChange.fire();new MemoryCart().save();' " +
                            "value='刪除' /></div><div class='clearfix'>" + hidCell + "</div></li>").appendTo("#itemList");
                        liItem.attr('qty', qty);
                        liItem.attr('opts', optIds);

                    } else {            	
                        var liItem = $("<li class='cart-item'><div class='item-name'>" + optTitles + "</div><div class='item-amount'>" + unitAdd +
                                "<input type='button' class='btn btn-mini rd-margin cartDelete' onclick='delItemFromCart(this);window.BuyEvents.OnIspQuantityChange.fire();new MemoryCart().save();' " +
                                " value='刪除' /></div><div class='clearfix'>" + hidCell + "</div></li>")
                            .appendTo("#itemList");
                        liItem.attr('qty', qty);
                        liItem.attr('opts', optIds);

                        $("#comboSum").show();

                    }
                    setQtyAtCart(getQtyAtCart() + qty);
                    subTotal(buyData.price, getQtyAtCart());
                    window.calculateTotal();

                    var lackQty = getLackOfQuantityOfComboPack();
                    $("#setError").html(lackQty);
                    if (0 != lackQty) {
                        $("#deliveryChargeError").show().focus();
                    } else {
                        $("#deliveryChargeError").hide();
                    }
                }
                window.BuyEvents.OnIspQuantityChange.fire();
            } else if (window.isShoppingCart) {
                $("#itemError").show();
            }
        }

        function delItemFromCart(obj) {
            var q = parseInt($(obj).parents('.cart-item').attr('qty'), 10);
            setQtyAtCart(getQtyAtCart() - q);

            addOptionLimitBack($(obj).parent().siblings().eq(1).text(), q);
            $(obj).parent().parent().remove();
            subTotal(buyData.price, getQtyAtCart());

            var lackQtycl = getLackOfQuantityOfComboPack();
            $("#setError").html(lackQtycl);
            if (0 != lackQtycl) {
                $("#deliveryChargeError").show().focus();
            } else {
                $("#deliveryChargeError").hide();
            }

            if (0 == getQtyAtCart()) {
                $("#comboSum").hide();
            }

            window.calculateTotal();
        }

        function subTotal(price, cselect) {
            var cset = getComboPack();
            var total = price * Math.ceil(cselect / cset);
            buyData.subtotal = total;
            changeFreight(total, cset, price);

            if (buyData.totalAmount <= 0) {
                $("#tbInvoiceEdit").hide();
                $('#divInvoice3').hide();
                $('#tdInvoiceSave').hide();
            } else {
                $("#tbInvoiceEdit").show();
                if ($('#rbInvoiceTriplicate').is(':checked')) {
                    $('#divInvoice3').show();
                    $('#tdInvoiceSave').show();
                }
            }
        }

        function MemoryInfo() {
            this._CART_NAME = "e7lifeInfo";
            this.assert = function() {
                //return $('#BuyerName').is(':visible') && $('#BuyerEmail').is('[readonly]') === false;
                return $('#BuyerName').is(':visible');
            };
            this.save = function() {
                if (this.assert() == false) {
                    return;
                }
                //if ($('#chkRememberUserInfo').is(':checked') === false) {
                //    this.empty();
                //    return;
                //}

                var userInfo = this.getData();               
                var str = JSON.stringify(userInfo);

                console.log("save info:" + str);
                $('#hidBuyerInfoJson').val(str);
            };
            this.getData = function() {

                var userInfo = {};
                //userInfo.BuyerEmail = $('#BuyerEmail').val();
                userInfo.ProductDelivery = $(':radio[name="ctl00$ctl00$ML$MC$rdoProductDelivery"]:checked').val();
                userInfo.BuyerEmail = $('#BuyerEmail').val();
                userInfo.BuyerName = $('#BuyerName').val();
                userInfo.BuyerMobile = $('#BuyerMobile').val();
                if ($('#ReceiverName').is(":visible")) {
                    userInfo.ReceiverName = $('#ReceiverName').val();
                    userInfo.ReceiverMobile = $('#ReceiverMobile').val();

                    userInfo.ReceiverCity = $('#ddlReceiverCity').val();
                    userInfo.ReceiverArea = $('#ddlReceiverArea').val();
                    userInfo.ReceiverAddress = $('#ReceiverAddress').val();
                }
                console.log("userInfo:" + userInfo);
                return userInfo;
            }
            this.load = function() {
                if (this.assert() == false) {
                    return null;
                }
                console.log('load info');

                var str = $('#hidBuyerInfoJson').val();
                if (str != null && str.length > 0) {
                    try {
                        var userInfo = JSON.parse(str);
                        return userInfo;
                    } catch (e) {
                    }
                }
                return null;
            };
            this.empty = function() {
                $('#hidBuyerInfo').val('');
            }
            this.replay = function(info) {
                console.log('replay');
                if (this.assert() == false) {
                    return;
                }
                console.log('replay assert');
                if (info != null) {
                    $('#' + info.ProductDelivery).attr('checked', true);
                    window.setTimeout(function() {
                        $('#' + info.ProductDelivery).click();
                    }, 100);
                    if (info.BuyerEmail != null && info.BuyerEmail.length > 0) {
                        $('#BuyerEmail').val(info.BuyerEmail);
                    }
                    if (info.BuyerName != null && info.BuyerName.length > 0) {
                        $('#BuyerName').val(info.BuyerName);
                    }
                    if (info.BuyerMobile != null && info.BuyerMobile.length > 0) {
                        $('#BuyerMobile').val(info.BuyerMobile);
                    }
                    if ($('#ReceiverName').is(":visible")) {
                        if (info.ReceiverName != null && info.ReceiverName.length > 0) {
                            $('#ReceiverName').val(info.ReceiverName);
                        }
                        if (info.ReceiverMobile != null && info.ReceiverMobile.length > 0) {
                            $('#ReceiverMobile').val(info.ReceiverMobile);
                        }
                        if (info.ReceiverAddress != null && info.ReceiverAddress.length > 0) {
                            window.timerId = window.setInterval(function() {
                                console.log('memory address: wait for cascadeAddress ready.');
                                if ($.cascadeAddress.ready()) {
                                    console.log('memory address: try to set ' +
                                        info.ReceiverCity + '/' + info.ReceiverArea + '/' + info.ReceiverAddress);
                                    $.cascadeAddress.update($('#ddlReceiverCity'), $('#ddlReceiverArea'), $('#ReceiverAddress'),
                                        info.ReceiverCity, info.ReceiverArea, info.ReceiverAddress, false);
                                    window.clearInterval(window.timerId);
                                }
                            }, 100);
                        }
                    }
                }
            };
        }

        function MemoryOtherInfo() {
            this.assert = function() {
                return true;
            };
            this.save = function() {
                if (this.assert() === false) {
                    return;
                }

                var otherInfo = this.getData();               
                var str = JSON.stringify(otherInfo);

                console.log("save other:" + str);
                $('#hidOtherInfoJson').val(str);
            };
            this.getData = function() {

                var otherInfo = {};

                otherInfo.SCash = buyData.scash;
                otherInfo.BCash = buyData.bcash;
                otherInfo.PayMethod = $(':radio[name="ctl00$ctl00$ML$MC$PayWay"]:checked').val();
                otherInfo.EnivoiceMethod = $(':radio[name="ctl00$ctl00$ML$MC$InvoiceMode"]:checked').val();
                if ($('#tbDiscount').is(':visible')) {
                    otherInfo.Discount = $('#tbDiscount').val();
                }

                console.log("otherInfo:" + otherInfo);
                return otherInfo;
            }
            this.load = function() {
                if (this.assert() == false) {
                    return null;
                }
                var str = $('#hidOtherInfoJson').val();

                console.log('load other: ' + str);

                if (str != null && str.length > 0) {
                    try {
                        var userInfo = JSON.parse(str);
                        return userInfo;
                    } catch (e) {
                    }
                }
                return null;
            };
            this.empty = function() {
                $('#hidOtherInfoJson').val('');
            }
            this.replay = function(info) {
                console.log('replay');
                if (this.assert() == false) {
                    return;
                }
                console.log('replay assert');
                if (info != null) {
                    buyData.scash = info.SCash;
                    buyData.bcash = info.BCash;
                    if ($('#tbDiscount').is(':visible') && info.Discount !=='') {
                        $('#discountCodeList').find('li[id=' + info.Discount + ']').click();
                    }
                    $('#' + info.PayMethod).attr('checked', true);
                    window.setTimeout(function() {
                        $('#' + info.PayMethod).click();
                    }, 100);
                    window.setTimeout(function() {
                        $('#' + info.EnivoiceMethod).click();
                        ChangeInvoiceMode();
                    }, 100);
                    calculateTotal();
                }
            };
        }

        function MemoryCart() {
            this._CART_NAME = "e7lifeCart";
            this._EMPTY = { Version: 1, Items: [], StoreId: null };
            this.save = function() {
                var cartInfo = this.getData();
                $('#itemList li').each(function() {
                    var cartItem = {};
                    cartItem.qty = parseInt($(this).attr('qty'), 10);
                    cartItem.opts = $(this).attr('opts').split(',');
                    cartItem.bid = buyData.dealId;
                    cartInfo.Items.push(cartItem);
                });
                cartInfo.StoreId = $('#ddlStore').val();
        
                var str = JSON.stringify(cartInfo);
                console.log('save str:' + str);
                $('#hidItemOptionsJson').val(str);
            };

            this.getData = function() {
                var cartInfo = this._EMPTY;
                $('#itemList li').each(function() {
                    var cartItem = {};
                    cartItem.qty = parseInt($(this).attr('qty'), 10);
                    cartItem.opts = $(this).attr('opts').split(',');
                    cartItem.bid = buyData.dealId;
                    cartInfo.Items.push(cartItem);
                });
                cartInfo.StoreId = $('#ddlStore').val();
        
                return cartInfo;
            };

            this.load = function() {
                var str = $('#hidItemOptionsJson').val();
                if (str != null && str.length > 0) {
                    try {
                        var cartInfo = JSON.parse(str);
                        console.log('load cartInfo: ' + str);
                        if (cartInfo.Version != 1) {
                            return this._EMPTY;
                        }
                        return cartInfo;
                    } catch(e) {}
                }
                return this._EMPTY;
            };

            this.reset = function() {
                $('.cartDelete').each(function() {
                    delItemFromCart(this);
                });
            };

            this.empty = function() {
                $('#hidItemOptionsJson').val('');
            };

            this.replay = function(cartInfo) {
                if (cartInfo.Items == null || cartInfo.Items.length == 0 || 
                    cartInfo.Items[0].bid !== buyData.dealId) {
                    console.log('deal not match, skip replay');
                    return;
                }
                console.log('replay cart.');
                this.reset();
                for(var i in cartInfo.Items) {
                    var cartItem = cartInfo.Items[i];
                    for(var j in cartItem.opts) {
                        var opt = cartItem.opts[j];
                        $('.ddlAddOptgroup').eq(j).val(opt);
                    }
                    $('#itemQty').val(cartItem.qty.toString());
					
                    var isValid = true;
                    for (var j in cartItem.opts) {
                        if ($('.ddlAddOptgroup').eq(j).val() == 'Default') {
                            isValid = false;
                        }
                    }
                    if (isValid) {
                        addItemToCart();
                    }
                }
                if (cartInfo.StoreId != null && cartInfo.StoreId.length > 0 && cartInfo.StoreId != "請選擇") {
                    <%--有值就試著帶入，帶不到資料也沒損失--%>
                    $('#ddlStore').val(cartInfo.StoreId);
                }
            };
        }
     
        function setItemList() {
            $("#confirmList").empty();

            var comboSet = getComboPack();
            var isBankDeal = $('#HfIsBankDeal').val() == 'True';

            var cartInfo = new MemoryCart().getData();
            for (var i in cartInfo.Items) {
                var item = cartInfo.Items[i];
                var optsTitle = getOptsTitleByOpts(item.opts);
                if (1 == comboSet) {
                    if(isBankDeal) {
                        $("#confirmList").append("<li><div class='item-name'>" + buyData.dealTitle + optsTitle + 
                            "</div><div class='total-amount' style='visibility: hidden'>$" + buyData.price + " x" + item.qty + 
                            "&nbsp;=</div><div class='total-price' style='visibility: hidden'>$" + buyData.price * item.qty + 
                            "</div><div class='clearfix'></div></li>");
                    }else{
                        $("#confirmList").append("<li><div class='item-name'>" + buyData.dealTitle + optsTitle + 
                            "</div><div class='total-amount'>$" + buyData.price + " x" + item.qty + 
                            "&nbsp;=</div><div class='total-price'>$" + buyData.price * item.qty + 
                            "</div><div class='clearfix'></div></li>");
                    }
                } else {   
                    $("#confirmList").append("<li><div class='item-name'>" + optsTitle + 
                        "</div><div class='item-amount'>&nbsp;x" + item.qty + 
                        "&nbsp;<div class='empty-btn'></div></div><div class='clearfix'></div></li>");
                }
            }

            var caData = [];
            for (var i in cartInfo.Items) {
                var caItem = '';
                for (var j in cartInfo.Items[i].opts) {
                    if (caItem.length > 0) {
                        caItem += ',';
                    }
                    var optTitle = getOptTitle(cartInfo.Items[i].opts[j]);
                    var optSep = optTitle === '' ? "" : '|^|';
                    caItem += optTitle + optSep + cartInfo.Items[i].opts[j];
                }
                caItem += '|#|' + cartInfo.Items[i].qty;
                caData.push(caItem);
            }
            console.log("hidAllItems: " + caData.join('||'));
            console.log("hidAllItems(json): " + JSON.stringify(cartInfo));
            $('#hidAllItems').val(caData.join('||'));
            $('#hidItemOptionsJson').val(JSON.stringify(cartInfo));
        }

        function getOptsTitleByOpts(opts) {
            var result = [];
            for (var i in opts) {
                var title = $('.ddlAddOptgroup').find('option[value="' + opts[i] + '"]').text();
                result.push(title);
            }
            if (result.length == 0) {
                return '';
            }
            console.log('title: ' + result[0]);
            return '(' + result.join(') (') + ')';
        }

        function getOptTitle(opt) {
            return $('.ddlAddOptgroup').find('option[value="' + opt + '"]').text();
        }

        function resetShoppingCart() {
            // 清除所有購物清單內容
            $('.cartDelete').each(function () {
                delItemFromCart(this);
            });
            addItemToCart();
            new MemoryCart().save();
            window.calculateTotal();
        }

        // for InvoiceMode
        function ChangeInvoiceMode() {
            $('#divInvoice2').css('display','none');
            $('#divInvoiceDonation').css('display','none');
            $('#divInvoice3').css('display','none');
            $('#tdInvoiceSave').hide();
            $('#PaperInvoice').css('display','none');

            if ($("#rbInvoiceDuplicate").is(":checked")) {
                $('#divInvoice2').css('display','');
                $('#chkPersonalCarrier').attr('checked',false);
                CheckPersonalCarrier($('#chkPersonalCarrier'));
            } else if ($("#rbInvoiceTriplicate").is(":checked")) {
        
                $('#invoTitleErr').hide();
                $("#tdInvoTitle").removeClass("error");

                $('#invoNameErr').hide();
                $("#tdInvoName").removeClass("error");

                $('#invoAddrErr').hide();
                $("#tdInvoAddr").removeClass("error");

                $('#invoNumberErr').hide();
                $("#tdInvoNumber").removeClass("error");
        
                $('#divInvoice3').css('display','');
                $('#tdInvoiceSave').show();
                $('#PaperInvoice').css('display','');
        
            } else if ($("#rbInvoiceDonation").is(":checked")) {
                $('#divInvoiceDonation').css('display','');
                $('#ddlInvoiceDonation').prop('selectedIndex', 0);
                ChangeInvoiceDonation($('#ddlInvoiceDonation'));
            }
        }

        // for Carrier
        function CheckPersonalCarrier(obj) {
            if ($(obj).is(":checked")) {
                if ($('#ddlCarrierType').val()== '<%= (int)CarrierType.Member %>'){
                    $('#ddlCarrierType').val('<%= (int)CarrierType.Phone %>');                 
                }
                $('#ddlCarrierType').find('[value=<%= (int)CarrierType.Member %>]').css('display','none');
                $('#PersonalCarrier').css("display","");
                ChangeCarrierType($('#ddlCarrierType'));
            }else {
                $('#ddlCarrierType').val('<%= (int)CarrierType.Member %>');
                $('#PersonalCarrier').css("display","none");
                $('#PaperInvoice').css("display","none");
            }
        }

        function ChangeCarrierType(obj) {
            $('#tdPhoneCarrier').hide();
            $('#tdPersonalCertificateCarrier').hide();
            $('#PaperInvoice').css('display', 'none');
            //$('#txtPhoneCarrier').val('');
            //$('#txtPersonalCertificateCarrier').val('');
            //$('#InvoiceBuyerName').val('');
            //$('#InvoiceAddress').val('');
            $('#invoNameErr').hide();
            $("#tdInvoName").removeClass("error");
            $('#invoAddrErr').hide();
            $("#tdInvoAddr").removeClass("error");
            $('#tdPhoneCarrier').removeClass("error");
            $('#phoneCarrierErr').hide();
            $('#phoneCarrierInvalid').hide();
            $('#tdPersonalCertificateCarrier').removeClass("error");
            $('#personalCertificateCarrierErr').hide();
            $('#personalCertificateCarrierInvalid').hide();
            
            var opt = $(obj).find(":selected").val();
            if (opt == '<%= (int)CarrierType.Phone %>') {
                $('#tdPhoneCarrier').css('display', '');
                $('#carrierNote').text('使用手機條碼載具或自然人憑證載具，將由財政部通知中獎。');
            } else if (opt == '<%= (int)CarrierType.PersonalCertificate %>') {
                $('#tdPersonalCertificateCarrier').css('display', '');
                $('#carrierNote').text('使用手機條碼載具或自然人憑證載具，將由財政部通知中獎。');
            } else if (opt == '<%= (int)CarrierType.None %>') {
                $('#PaperInvoice').css('display', '');
                $('#carrierNote').text('響應發票無紙化，建議使用自動兌獎無負擔的電子發票。');
            }
        }

        // for Donation
        function ChangeInvoiceDonation(obj) {
            $('#txtLoveCode').val('');
            $('#divInvoiceDonationOther').css("display", "none");
            $('#divInvoicePezDonation').css("display", "none");
            $('#divInvoiceGenesisDonation').css("display", "none");
            $('#divInvoiceSunDonation').css("display", "none");
            $('#tdLoveCode').removeClass('error');
            $('#loveCodeErr').hide();
            $("#LoveCodeDesc").html('請於上方輸入正確的愛心碼。');

            if ($(obj).val() == '') {
                $('#divInvoiceDonationOther').css("display", "");
            } else {
                if ($(obj).val() == '<%= pezDonationCode %>') {
                    $('#divInvoicePezDonation').css("display", "");
                } else if (($(obj).val() == '<%= genesisDonationCode %>')) {
                    $('#divInvoiceGenesisDonation').css("display", "");
                } else if (($(obj).val() == '<%= sunDonationCode %>')) {
                    $('#divInvoiceSunDonation').css("display", "");
                }
                $("#txtLoveCode").val($(obj).val());
                $('#LoveCodeDesc').html($('#ddlInvoiceDonation').find(':selected').text().replace('捐贈', ''));
            }
        }

        function SlideContent()
        {
            if ($(window).width() >= 768) {
                $("#rbPayWayByLinePay").click(function(){//Line
                    $('.LinePayContent').slideDown();
                    $('.CreditCardContent').slideUp();
                    $('.phInstallmentContent').slideUp();
                    $('.MasterPassContent').slideUp();
                    $('.TaishinContent').slideUp();
                    $('.AtmPayContent').slideUp();
                });

                $("#rbPayWayByCreditCard").click(function(){//信用卡
                    $('.LinePayContent').slideUp();
                    $('.CreditCardContent').slideDown();
                    $('.phInstallmentContent').slideUp();
                    $('.MasterPassContent').slideUp();
                    $('.TaishinContent').slideUp();
                    $('.AtmPayContent').slideUp();
                });

                $("#rbInstallmentDefault").click(function(){//台新卡
                    $('.LinePayContent').slideUp();
                    $('.CreditCardContent').slideUp();
                    $('.phInstallmentContent').slideDown();
                    $('.MasterPassContent').slideUp();
                    $('.TaishinContent').slideUp();
                    $('.AtmPayContent').slideUp();
                });

                $("#rbPayWayByMasterPass").click(function(){//MasterPass付款
                    $('.LinePayContent').slideUp();
                    $('.CreditCardContent').slideUp();
                    $('.phInstallmentContent').slideUp();
                    $('.MasterPassContent').slideDown();
                    $('.TaishinContent').slideUp();
                    $('.AtmPayContent').slideUp();
                });

                $("#rbPayWayTaishinPay").click(function(){//台新儲值支付
                    $('.LinePayContent').slideUp();
                    $('.CreditCardContent').slideUp();
                    $('.phInstallmentContent').slideUp();
                    $('.MasterPassContent').slideUp();
                    $('.TaishinContent').slideDown();
                    $('.AtmPayContent').slideUp();
                });

                $("#rbPayWayByAtm").click(function(){//ATM
                    $('.LinePayContent').slideUp();
                    $('.CreditCardContent').slideUp();
                    $('.phInstallmentContent').slideUp();
                    $('.MasterPassContent').slideUp();
                    $('.TaishinContent').slideUp();
                    $('.AtmPayContent').slideDown();
                });
            }
        }
        function setListHeight(){        
            $('#discountMList').show();
            $('#discountCodeMList li .cover_m').each(function(){
                $(this).css('line-height', $(this).parent().height()+'px');
            });
            $('#discountMList').hide();
            $('#discountList').show();
            $('#discountCodeList li .cover').each(function(){
                $(this).height($(this).parent().height());
                $(this).css('line-height', $(this).parent().height()+'px');
            });
            $('#discountList').hide();
        }


        $(document).ready(function() {

            window.buyData = new Vue({
                el: '#PanelBuy',
                data: {
                    pendingAmount: 0,
                    totalAmount: 0,
                    subtotal: 0,
                    bcashMaxinum: <%=(Math.Floor(bcash/10)).ToString("F0")%>,
                    scashMaxinum: <%=(int)scash%>,
                    price: <%=TheDeal == null ? 0 : (int)TheDeal.ItemPrice%>,
                    bcash: '',
                    scash: '',
                    discountAmount: 0,
                    freightCharge: <%= deliveryCharge %>,
                    storeInfo: '',
                    dealId: '<%=TheDeal == null ? string.Empty : TheDeal.BusinessHourGuid.ToString()%>',
                    deliveryType: parseInt('<%=TheDeal == null ? 0 : TheDeal.DeliveryType.GetValueOrDefault()%>', 10),
                    dealTitle: '<%=TheDeal == null ? string.Empty : System.Net.WebUtility.HtmlEncode(TheDeal.ItemName.Trim())%>'
                },
                methods: {
                    recalculateTotal: function(event) {
                        window.calculateTotal();
                    }
                }
            });

            PezExchange();

            
            window.BuyEvents.OnBtnApplePayClick.fire();


            setListHeight();

            var taishinBindStatus = getUrlQueryVariable('taishinBindStatus');
            if (taishinBindStatus != '')
            {
                alert(decodeURI(taishinBindStatus));
            }

            $('#txtPhoneCarrier').focusout(function (e) {
                if($('#txtPhoneCarrier').val().length == 8)
                {
                    $('#loadingIMG').show();
                    $('#phoneCarrierInvalid').hide();                    
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url:"../WebService/EinvoiceService.asmx/CheckCarrid",
                        data: '{carrid: "' + $('#txtPhoneCarrier').val() +'" }',
                        dataType: "json",
                        success: function (data) {
                            if(data.d == "Y" || data.d == "P"){
                                $('#phoneCarrierInvalid').hide();
                                $('#hidCarrIdVerify').val('true');
                            }
                            else{
                                $('#phoneCarrierInvalid').show();
                                $('#hidCarrIdVerify').val('false');
                                $('#tdPhoneCarrier').addClass("error");
                            }
                        },
                        complete: function(){
                            $('#loadingIMG').hide();
                        },
                        error: function(data){
                            $('#phoneCarrierInvalid').hide();
                            $('#hidCarrIdVerify').val('true');
                        }
                    });
                }
                else
                {
                    $('#phoneCarrierInvalid').hide();
                }
            });

            if ($("#chkPersonalCarrier").is(":checked")) {
                CheckPersonalCarrier($('#chkPersonalCarrier'));
            }

            $(".ddlAddOptgroup").each(function() {
                $(this).append('<optgroup label=""></optgroup>');
            });
            $("#ExpYear, #ExpMonth").change(function() {
                if (getMMCardEditMode()==false) {
                    //swith to edit mode
                    setMMCardEditMode(true);
                }

                if($("#ExpMonth").val() != '' || $("#ExpYear").val() != '')
                {
                    $('#ExpDateRow').removeClass('error');
                    $('#ExpDate').closest('.data-input').find('.if-error').hide();
                }
            });
            $(":text.cc").bind("keypress", function(evt) {            
                if (getMMCardEditMode()==false) {
                    //swith to edit mode
                    setMMCardEditMode(true);
                }
            }).bind('keyup',function() {
                checkCardNo(this); 

           
                GetCreditCardName();
            }).css("text-align", "center").attr("autocomplete", "off");

            $("#<%= SecurityCode.ClientID %>").attr("autocomplete", "off").keyup(function() {            
                checkSecurityCode(this);      
            });

            $("#txtPhoneCarrier").click(function() {
                $("#tdPhoneCarrier").removeClass("error");
                $("#phoneCarrierErr").hide();
            });
            $("#txtPersonalCertificateCarrier").click(function() {
                $("#tdPersonalCertificateCarrier").removeClass("error");
                $('#personalCertificateCarrierErr').hide();
                $('#personalCertificateCarrierInvalid').hide();
            });
            $("#InvoiceTitle").click(function() {
                $('#invoTitleErr').hide();
                $("#tdInvoTitle").removeClass("error");
            });
            $("#InvoiceBuyerName").click(function() {
                $('#invoNameErr').hide();
                $("#tdInvoName").removeClass("error");
            });
            $("#InvoiceNumber").click(function() {
                $('#invoNumberErr').hide();
                $("#tdInvoNumber").removeClass("error");
            });
            $("#InvoiceAddress").click(function() {
                $('#invoAddrErr').hide();
                $("#tdInvoAddr").removeClass("error");
            });

            // click text will remove error hint
            $('#tbBuyerInfoEdit, #tbReceiverEdit').find(':text,select').click(function() {
                $(this).closest('.form-unit').find('.enter-error').hide();
                $(this).closest('.form-unit').removeClass("error");
            });

            $("#rbReceiptType1").click(function() {
                $('#receiptEditor').hide();
                $("#tdReceiptAddress").removeClass("error");
            });

            $("#txtPhoneCarrier, #txtPersonalCertificateCarrier").bind("keyup", function() {
                if (window.currKeyisLowerCase) {
                    $(this).val($(this).val().toUpperCase());
                }
            });
            $("#txtPhoneCarrier, #txtPersonalCertificateCarrier").keydown(function(e) {
                if (e.which >= 65 && e.which <= 90) {
                    window.currKeyisLowerCase = true;
                } else {
                    window.currKeyisLowerCase = false;
                }
            });
            $('#tdReceiptAddress').click(function() {
                $("#tdReceiptAddress").removeClass("error").find('.message').hide();
            });
            $.cascadeAddress.setup('<%=WebUtility.GetSiteRoot()%>', function() {
                $.cascadeAddress.bind($('#ddlBuyerCity'), $('#ddlBuyerArea'), $('#BuyerAddress'), <%= BuyerCity %>, <%= BuyerTownshipId %>, '<%= BuyerCityAddress %>', $("#hiNotDeliveryIslands").val() != "True");
                $.cascadeAddress.bind($('#ddlReceiptCity'), $('#ddlReceiptArea'), $('#txtReceiptAddress'), -1, -1, '', $("#hiNotDeliveryIslands").val() != "True");
                $.cascadeAddress.bind($('#ddlReceiverCity'), $('#ddlReceiverArea'), $('#ReceiverAddress'), -1, -1, '', $("#hiNotDeliveryIslands").val() != "True");
            });
            $('#txtReceiptTitle').click(function() {
                $('#txtReceiptTitle').parent().parent().removeClass("error");
                $('#txtReceiptTitle').next('.message').hide();
            });
            $('#txtReceiptNumber').click(function() {
                $('#txtReceiptNumber').parent().parent().removeClass("error");
                $('#txtReceiptNumber').next('.message').hide();
            });

            $("#receiverSelect").change(function (evt, init) {
                updateReceiver($(this).val());
                if (init !== true) {
                    $('#chkBuyerSync').removeAttr('checked');
                }
            });

            $("#ddlReceiverArea,#ReceiverAddress").change(function () {
                ValidateBuyerAddress();
            });

            //init form
            $('#receiverSelect').val('0').trigger('change', true);
            window.setQtyAtCart(0);
            $('#tbDiscount').val('');
            buyData.bcash = '';
            buyData.scash = '';
            calculateTotal();

            $("#ddlStore").change(function() { checkBranch(); });
            $("#setError").html($("#hidComboPack").val());

            // for receipt
            $.fn.inputableDepandOnCheckbox = function(chbox, defaultinputnable) {
                var inputboxs = this;
                chbox.click(function() {
                    if ($(this).is(':checked')) {
                        inputboxs.removeAttr('readonly').css('background-color', 'white');
                    } else {
                        inputboxs.val('').attr('readonly', true).css('background-color', '#ddd');
                    }
                });
                if (defaultinputnable) {
                    chbox.attr('checked', true);
                    inputboxs.removeAttr('readonly').css('background-color', 'white');
                } else {
                    chbox.removeAttr('checked');
                    inputboxs.val('').attr('readonly', true).css('background-color', '#ddd');
                }
            };
            $('#txtReceiptNumber,#txtReceiptTitle').inputableDepandOnCheckbox($('#chkReceiptForBusiness'), false);
            //calc total
            setLTotal($("#dCharge").html());


            //copy from piinlife
            $.fn.queueInterruptibleAction = function(delayMilseconds, actionCallback) {
                //cancel if other action exist
                this.cancelInterruptibleAction();
                // delay execute delayCallback
                var timerId = window.setTimeout(function() {
                    $.removeData(this, 'timerId');
                    actionCallback.call(this);
                }, delayMilseconds);
                $.data(this[0], 'timerId', timerId);
            };

            $.fn.cancelInterruptibleAction = function() {
                var timerId = $.data(this[0], 'timerId');
                if (timerId != null) {
                    $.removeData(this[0], 'timerId');
                    window.clearTimeout(timerId);
                }
            };

            $('.safelycode').mouseenter(function() {
                var target = $(this);
                target.queueInterruptibleAction(200, function() {
                    target.addClass('safelycode-hover');
                });
            });
            $('.safelycode').mouseleave(function() {
                var target = $(this);
                target.queueInterruptibleAction(50, function() {
                    target.removeClass('safelycode-hover');
                });
            });

            // for loveCode autoComplete
            if ($("#txtLoveCode").length != 0) {

                window.availableLoveCodes = <%= LoveCodes %>;
 
                $("#txtLoveCode").autocomplete({
                    source: function(request, response) {
                        var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), "i");
                        response($.grep(availableLoveCodes, function(value) {
                            return matcher.test(value.value) || matcher.test(value.desc);
                        }));
                    },
                    minLength: 1,
                    messages: {
                        noResults: '',
                        results: function() {}
                    },
                    select: function(event, ui) {
                        $("#txtLoveCode").val(ui.item.label);
                        $("#LoveId").val(ui.item.value);
                        $("#LoveCodeDesc").html(ui.item.desc);
                        $('#tdLoveCode').removeClass('error');
                        $('#loveCodeErr').hide();
                        return false;
                    },
                    response: function(event, ui) {
                        window.test = ui;
                        if (ui.content.length === 0) {
                            $("#loveCodeErr").show();
                            $('#tdLoveCode').addClass('error');
                        }
                    }
                }).data("autocomplete")._renderMenu = function(ul, items) {
                    var self = this;
                    $.each(items, function(index, item) {
                        if (index < 10) // here we define how many results to show
                        {
                            self._renderItem = function(ul, item) {
                                var listItem = $("<li></li>").data("item.autocomplete", item)
                                    .append("<a><span class='code'>" + item.label + "</span>" + item.desc + "</a>")
                                    .appendTo(ul);
                                return listItem;
                            };
                            self._renderItem(ul, item);
                        }
                    });
                };
            }

            //region mmcards
            <%=MemberCreditCardListSetupScript%>
            if (window.mmcards != null && window.mmcards.length > 0) {
                $('#ddlSelectCreditCard').find('option').remove();
                $('#ddlSelectCreditCard').append('<option value="">選擇信用卡</option>');
                for (var key in window.mmcards) {
                    var mmcard = window.mmcards[key];
                    if (mmcard.IsLastUsed) {
                        $('#ddlSelectCreditCard').append('<option selected=true value="' + mmcard.CardGuid + '">' + mmcard.CardName + '</option>');
                    } else {
                        $('#ddlSelectCreditCard').append('<option value="' + mmcard.CardGuid + '">' + mmcard.CardName + '</option>');
                    }
                }
            } else {
                $('#ddlSelectCreditCard').remove();
            }
            window.setMMCardEditMode = function(inEdit) {
                if (inEdit) {                    
                    $("#ddlSelectCreditCard").val($("#ddlSelectCreditCard option:first").val()).trigger('change');                                        
                    //$('#CardNo1').val(no);
                }
            };
            window.getMMCardEditMode = function() {
                if ($('#ddlSelectCreditCard').val() == null || $('#ddlSelectCreditCard').val() == '') {
                    return true;
                }
                return false;
            };
            $('#ddlSelectCreditCard').change(function() {
                var sel = this;
                var datas = $.grep(window.mmcards,function(t) {
                    return t.CardGuid == $(sel).val();
                });
                var data = datas.length > 0 ? datas[0] : null;
                $('.form-unit').removeClass('error');
                $('.form-unit').find('.if-error').hide();
                if (data == null) {
                    $('#CardNo1').val('');
                    $('#CardNo2').val('');
                    $('#CardNo3').val('');
                    $('#CardNo4').val('');
                    $("#ExpYear").val($("#ExpYear option:first").val());
                    $("#ExpMonth").val($("#ExpMonth option:first").val());
                    $('#txtCreditCardName').val('');
                    $('#chkSaveCreditCardInfo').attr('checked',true);
                    $('#divSaveCreditCardInfo').show();
                } else {
                    $('#CardNo1').val('XXXX');
                    $('#CardNo2').val('XXXX');
                    $('#CardNo3').val('XXXX');
                    $('#CardNo4').val(data.CardTail);
                    $('#ExpYear').val(data.ValidYear);
                    $('#ExpMonth').val(data.ValidMonth);
                    $('#txtCreditCardName').val(data.CardName);
                    $('#SecurityCode').focus();
                    $('#chkSaveCreditCardInfo').removeAttr('checked');
                    $('#divSaveCreditCardInfo').hide();
                }
            });
            $('#ddlSelectCreditCard').trigger('change');
            //endregion mmcards

            // 預帶載具資訊
            //var carrierType = '<%= (int)Carrier.Key %>';
            //var carrierId = '<%= Carrier.Value %>';
            //if (carrierType != '<%= (int)CarrierType.Member %>' && carrierType != '<%= (int)CarrierType.None %>') {
            //    $('#chkPersonalCarrier').attr('checked','checked');
            //    CheckPersonalCarrier($('#chkPersonalCarrier'));
            //    $('#ddlCarrierType').val(carrierType);
            //    ChangeCarrierType($('#ddlCarrierType'));
            //    if (carrierType == '<%= (int)CarrierType.Phone %>') {
            //        $('#txtPhoneCarrier').val(carrierId);
            //    } else if (carrierType == '<%= (int)CarrierType.PersonalCertificate %>') {
            //        $('#txtPersonalCertificateCarrier').val(carrierId);
            //    }
            //}

            try {
                console.log("window.isShoppingCart: " + window.isShoppingCart);
                // 非使用購物車檔次
                if (window.isShoppingCart == false) {
                    // 自動加入第一筆清單
                    var charge = parseInt($("#dCharge").html(), 10);
                    var total = buyData.totalAmount;
                    if (total - charge <= 0) {
                        setQtyAtCart(0); // 清空已選數量
                        addItemToCart();
                    }
                    // 每次異動 好康組合/數量 時，需重置購物清單內容
                    $('select[id*=ddlMultOption]').change(function() { 
                        resetShoppingCart();
                    });
                    $('#itemQty').change(function() {
                        resetItemAtCart();
                        new MemoryCart().save();
                        //resetShoppingCart();
                    });
                    $('#ddlStore').change(function() {
                        resetShoppingCart();
                    });
                } else {
                    // 顯示 [加入購物清單] 按鈕，及 [購物清單]
                    $('#mixitemcart').css('display', '');
                    $('#AddToCartButton').css('display', '');
                }
            } catch (ex) {
            }

            if (window.loadCart) {
                var cart = new MemoryCart();
                var cartItems = cart.load();
                cart.replay(cartItems);
            }
            if (window.loadOtherInfo) {
                var otherInfo = new MemoryOtherInfo();
                var otherInfoItems = otherInfo.load();
                otherInfo.replay(otherInfoItems);
            }

            //temp fix last name is email bug
            if (($('#BuyerName').val() || "").indexOf('@') > -1) {
                $('#BuyerName').val('');
            }

            $(document.body).on('click', '.memberLoginToo', function() {
                $('#btnMemberLogin').click();
            });
             
            $('#BuyerEmail').blur(function() {
                var userEmail = $.trim($(this).val());
                var userPhone = $.trim($('#BuyerMobile').val());
                if ($('#BuyerEmail').is('[readonly]')) {
                    $("#buyerEmailHint").hide();
                    return;
                }
                else if (userEmail == "" && userPhone == "") {
                    $("#buyerEmailHint").html("嗨! 建議<a class='memberLoginToo'>登入會員</a>以使用折價券等好康!!").show();
                    return;
                }
                else if (checkAccount(userEmail) == false) {
                    $("#buyerEmailErr").show();
                    $("#tdBuyerEmail").addClass("error");
                    return;
                }
                $.dnajax(window.ServerLocation + "service/ppon/GetMemberLevel", "{userEmail:'" + userEmail + "'}", function(res) {
                    if (res.Code != window.ApiSuccessCode) return;
                    if (res.Data.IsNew) {
                        $("#buyerEmailHint").html("哈囉新朋友，歡迎你!").show();
                    } else if (res.Data.IsGuest) {
                        $("#buyerEmailHint").html("嗨! 記得認證成為正式會員喔!").show();
                    } else {
                        $("#buyerEmailHint").html("嗨! 建議<a>登入會員</a>以使用折價券等好康!!").show();
                        $("#buyerEmailHint a").addClass('memberLoginToo');
                    }
                });
            }).keyup(function() {
                $("#buyerEmailHint").hide();
                $("#buyerEmailErr").hide();
                $("#tdBuyerEmail").removeClass("error");
            });
            $('#BuyerMobile').blur(function() {
                if ($("#BuyerMobile").val() != "" && checkMobile($("#BuyerMobile").val()) == false) {
                    $("#buyerMobileErr").show();
                    $("#tdBuyerMobile").addClass("error");
                }
            }).keyup(function() {
                $("#buyerMobileErr").hide();
                $("#tdBuyerMobile").removeClass("error");
            });

            $("input.numeric").numeric();

            var userInfo = new MemoryInfo().load();
            if (userInfo != null) {
                $('#chkRememberUserInfo').attr('checked', true);
                new MemoryInfo().replay(userInfo);
            }

            window.onunload = function() {
                if (window.isAuthenticated == false) {
                    new MemoryInfo().save();
                }
            };

            if ($('#BuyerEmail').is('[readonly]') == false) {
                $('#BuyerEmail').trigger('blur');
            }

            $("#BuyerEmail").completer({
                separator: "@",
                source: window.EmailDomainSource,
                itemSize: 7
            });

            $('.btnShare').click(function() {
                var bid = $('#hidBid').val();
                var userId = $('#hidUserId').val();
                Share(bid, userId);
            });

            $('.btnCouponList').click(function() {
                var oid = $('#hidOid').val();
                location.href = window.ServerLocation + 'User/CouponDetail.aspx?oid=' + oid;
            });

            $('.btnAuthMail').click(function() {
                var email = $('#hidAuthEmail').val();
                if (checkAccount(email)) {
                    reSend(email);
                }
            });
            window.calculateTotal();

            //installment 
            //installment 
            $('#rbPayWayByLinePay, #rbPayWayByCreditCard, #rbPayWayByMasterPass, #rbPayWayByAtm, #rbPayWayTaishinPay, #rbPayWayFamilyIsp').click(function() {
                $('#rbInstallment3Months, #rbInstallment6Months, #rbInstallment12Months').removeAttr('checked');
            });
            $('#rbInstallmentDefault').click(function() {
                $('#rbInstallment3Months').attr('checked', true);
            });
            $('#rbInstallment3Months, #rbInstallment6Months, #rbInstallment12Months').click(function() {
                $('#rbPayWayByLinePay, #rbPayWayByCreditCard, #rbPayWayByMasterPass, #rbPayWayByAtm, #rbPayWayTaishinPay, #rbPayWayFamilyIsp').removeAttr('checked');
                $('#rbInstallmentDefault').attr('checked', true);
            });


            //SlideDown&SlideUp
            SlideContent();

            $(window).resize(function(){
                SlideContent();
            });

            //台新儲值支付 說明視窗
            $(".surplus img").hover(
                function() {
                    $( ".lmMemoryLog-wrap" ).find(".lmMemoryLog-box").stop(false,true).fadeIn(500);
                }, function() {
                    $( ".lmMemoryLog-wrap" ).find(".lmMemoryLog-box").stop(false,true).fadeOut(500);
                }
            ); 

            //銀行檔次 隱藏付費欄位並用活動優惠序號付款
            if($('#HfIsBankDeal').val() =='True') {
                $('input[id*=btnSelectDiscount]').hide();
                $("#step3Title").text("活動優惠序號");
                $("#step3ActionNubmer").text("活動序號");
                $("#cDiscountPay>label").text("活動序號");
                $(".step3Div").hide();
                $(".step4Div").hide();
                $(".step5Div").hide();
                $(".SelectedPayWay").hide();
                $(".orderDiv").hide();   //購物清單-支付方法,發票資訊
            }     
            
            ChangeInvoiceMode();

            $('.memoryButton').click(function() {
                setItemList();
                new MemoryInfo().save();
                new MemoryOtherInfo().save();
            });

            $('#rdoProductDeliveryNormal,#rdoProductDeliveryFamily,#rdoProductDeliverySeven').click(function () {
                window.BuyEvents.OnProductDeliveryChange.fire();
            });

            window.MemberLastProductDeliveryType = '<%=MemberLastProductDeliveryType%>';
            window.UseFamilyProductDeliveryType = '<%=Request["cvsspot"]!=null%>';
            window.UseSevenProductDeliveryType = '<%=Request["csvemap"]!=null%>';
            if (window.UseFamilyProductDeliveryType === 'True' && $('#rdoProductDeliveryFamily').is(':visible')) {
                $('#rdoProductDeliveryFamily').click();
            } else if (window.UseSevenProductDeliveryType === 'True' && $('#rdoProductDeliverySeven').is(':visible')) {
                $('#rdoProductDeliverySeven').click();
            } else if (window.MemberLastProductDeliveryType === '0' && $('#rdoProductDeliveryNormal').is(':visible')) {
                $('#rdoProductDeliveryNormal').click();
            } else if (window.MemberLastProductDeliveryType === '1' && $('#rdoProductDeliveryFamily').is(':visible')) {
                $('#rdoProductDeliveryFamily').click();
            } else if (window.MemberLastProductDeliveryType === '2' && $('#rdoProductDeliverySeven').is(':visible')) {
                $('#rdoProductDeliverySeven').click();
            } else {
                $('.productDeliveryGroup').find(':radio').eq(0).click();
            }

            window.BuyEvents.OnProductDeliveryChange.fire();
            
        });

        function CopyInvoiceInfo() {
            if ($('#hidInvoiceCity').val() != $('#ddlInvoiceCity').val()) {
                $('#hidInvoiceArea').val(-1);
            } else {
                $('#hidInvoiceArea').val($('#ddlInvoiceArea').val());
            }

            $('#hidInvoiceCity').val($('#ddlInvoiceCity').val());
        }

        function ValidateBuyerAddress() {
            if ($('#ddlReceiverArea').val() != "-1" && $('#ReceiverAddress').val() != "") {
                    $('#loadingIMG2').show();
                    var address = $('#ddlReceiverCity option:selected').text() + $('#ddlReceiverArea option:selected').text() + $("#ReceiverAddress").val();
                    $.getJSON('<%=WebUtility.GetSiteRoot()%>' + '/api/location/validateaddress?address=' + address + '&townId=' + $('#ddlReceiverArea').val(), function (res) {
                        if (res.length > 0) {
                            $('#loadingIMG2').hide();
                            $('#lbReceiverAddressValidateMsg').html(res);
                            $('#lbReceiverAddressValidateMsg').show();
                        } else {
                            $('#loadingIMG2').hide();
                            $('#lbReceiverAddressValidateMsg').hide();
                        }
                    });
                }
        }

        function PezExchange()
        {

            //有綁定過直接帶,沒綁定過帶null去綁定
            var memNum='';
            var externalUserId='<%=ExternalUserId%>';
            if (externalUserId=='')
            {
                memNum = null;
            }
            else
            {
                memNum = externalUserId;
            }

            $('#btnPezExchange').renderUi({
                memNum: memNum,
                clientId: '<%=config.PcashXchClientId%>',
                clientSecret: '<%=config.PcashXchClientSecret%>',
                origin: '<%=config.SSLSiteUrl%>' + '/',
                merchantId: 'M0025',
                merchantContent:$('#hidMerchantContent').val(),
                bindingCallback: function(memNum, callback) {
                    //綁定帳號
                    var  result = BindingPezMember('<%=UserName%>', memNum);
                    callback({
                        success: result.isSuccess
                    })
                    if (!result.isSuccess)
                    {
                        //modal重產避免直接跳去兌換頁
                        alert('綁定失敗');
                        $('.gateway-modal').remove();
                        PezExchange();

                    }
                },
                successCallback: function(response, callback) {
                    //儲值Pcash至17life
                    isSuccess = false;
                    if (response.returnCode =='0000')
                    {
                        var result = DepositScash('<%=UserName%>', response.memNum, response.deductContent, response.merchantContent, response.returnCode);
                        buyData.scashMaxinum=result.sumScash;
                        isSuccess = result.isSuccess;

                        if (!result.isSuccess)
                        {
                            alert('兌換失敗');
                        }
                        else
                        {
                            alert('兌換點數已成功轉入您的購物金，請繼續完成您的購物流程。');
                        }
                        //merchant重新塞
                        $('#hidMerchantContent').val(result.newMerchantContent);
                        $('.gateway-modal').remove();
                        PezExchange();
                    }
                    else if (response.returnCode == 'E999') {
                        //關閉視窗
                    }
                    else {
                        DepositErrorLog('<%=UserName%>', response.returnCode);
                        alert(response.returnCode);
                    }
                    callback({
                        success: isSuccess
                    })
                }
            })
        }

    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MC" runat="server">    
    <asp:HiddenField ID="hidTicketId" runat="server" />
    <asp:HiddenField ID="hidDeliveryInfoV2" runat="server" />    
    <asp:HiddenField ID="hidqty" runat="server" />
    <asp:HiddenField ID="hidStoreGuid" runat="server" />
    <asp:HiddenField ID="hidFreight" runat="server" />
    <asp:HiddenField ID="hidAllItems" runat="server" />
    <asp:HiddenField ID="hidItemOptionsJson" runat="server" />
    <asp:HiddenField ID="hidBuyerInfoJson" runat="server" />
    <asp:HiddenField ID="hidOtherInfoJson" runat="server" />
    <asp:HiddenField ID="hidIspQuantityLimit" runat="server" />
    <asp:HiddenField ID="hidComboPack" runat="server" />
    <asp:HiddenField ID="hidCurrentLimits" runat="server" />
    <asp:HiddenField ID="hidQtyLimit" runat="server" />
    <asp:HiddenField ID="hidReceiverAddress" runat="server" />
    <asp:HiddenField ID="hiNotDeliveryIslands" runat="server" />
    <asp:HiddenField ID="hidMultiBranch" runat="server" />
    <asp:HiddenField ID="hidEinvoiceTripleId" runat="server" />
    <asp:HiddenField ID="hidIsVisaDiscountCode" runat="server" />
    <asp:HiddenField ID="hidValidDiscountCode" runat="server" />
    <asp:HiddenField ID="hidMPCallbackUrl" runat="server" />
    <asp:HiddenField ID="hidMPPairingRequestToken" runat="server" />
    <asp:HiddenField ID="hidMPRequestToken" runat="server" />
    <asp:HiddenField ID="hidMPRequestedDataTypes" runat="server" />
    <asp:HiddenField ID="hidMPMerchantCheckoutId" runat="server" />
    <asp:HiddenField ID="hidMPAcceptedCards" runat="server" />
    <asp:HiddenField ID="hidMPShippingSuppression" runat="server" />
    <asp:HiddenField ID="hidMPRewardsProgram" runat="server" />
    <asp:HiddenField ID="hidMPAuthLevelBasic" runat="server" />
    <asp:HiddenField ID="hidMPConsumerWalletId" runat="server" />
    <asp:HiddenField ID="hidMPPrecheckoutTransactionId" runat="server" />
    <asp:HiddenField ID="hidMPWalletName" runat="server" />
    <asp:HiddenField ID="hidIsTaishinPayLinked" runat="server" />
    <asp:HiddenField ID="hidIsShoppingCart" runat="server" />
    <asp:HiddenField ID="hidAllowGuestBuy" runat="server" />
    <asp:HiddenField ID="hidOid" runat="server" />
    <asp:HiddenField ID="hidInvoiceCity" runat="server" />
    <asp:HiddenField ID="hidInvoiceArea" runat="server" />
    <asp:HiddenField ID="hidConfirmInvoiceAddress" runat="server" />
    <input id="HfIsBankDeal" type="hidden" value="<%=IsBankDeal%>" />
    <input id="hidTagGuid" type="hidden" value="<%=MainGuid%>" />
        
    <img src='../Themes/PCweb/images/spinner.gif' style="display: none" />
    <div id="TOPBanner">
    </div>
    <asp:Panel ID="PanelBuy" runat="server">
        <input type="hidden" id="hidtotal" runat="server" v-model="pendingAmount" />
        <input type="hidden" id="hidbcash" runat="server" v-model="bcash" />
        <input type="hidden" id="hidscash" runat="server" v-model="scash" />
        <input type="hidden" id="hidsubtotal" runat="server" v-model="totalAmount" />
        <input type="hidden" id="hiddCharge" runat="server" v-model="freightCharge" />
        <div id="Left" class="rd-payment-Left-width">
            <div id="maincontent" class="clearfix">
                <asp:Panel ID="NomalPay" runat="server">
                    <%--好康交易結帳--%>
                    <div id="divFillPurchaseInfo" class="content-group payment">
                        <h1 class="rd-payment-h1">好康交易結帳</h1>
                        <hr class="header_hr rd-payment-width" />
                        <%--step 1--%>
                        <h2 class="rd-payment-h2 buystep1">
                            <div class="step-num">
                                1
                            </div>
                            選擇好康規格與數量</h2>
                        <div class="grui-form buystep1">
                            <asp:UpdatePanel ID="upxy" runat="server" ChildrenAsTriggers="true" RenderMode="Inline">
                                <ContentTemplate>
                                    <div id="tdStore" class="form-unit">
                                        <asp:Label ID="lblStoreChioce" runat="server" CssClass="unit-label" Text="分店選擇" Visible="false"></asp:Label>
                                        <div class="data-input">
                                            <asp:PlaceHolder ID="pStore" runat="server">
                                                <asp:Panel ID="multiStores" runat="server">
                                                    <asp:DropDownList ID="ddlStoreCity" CssClass="ddlAddOptgroup" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlStoreCity_SelectedIndexChanged">
                                                    </asp:DropDownList>
                                                    <asp:DropDownList ID="ddlStoreArea" CssClass="ddlAddOptgroup" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlStoreArea_SelectedIndexChanged">
                                                        <asp:ListItem Text="請選擇" Value="0" />
                                                    </asp:DropDownList>
                                                </asp:Panel>
                                                <asp:DropDownList ID="ddlStore" CssClass="ddlAddOptgroup" runat="server" />
                                                <span id="branchErr" class="error-text" style="display: none;">請選擇</span>
                                            </asp:PlaceHolder>
                                        </div>
                                        <div id="divSingleStore" runat="server" class="data-input">
                                            <p>
                                                <asp:Label ID="lblSingleStore" runat="server" />
                                            </p>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            <div class="form-unit">
                                <label for="" class="unit-label">
                                    好康內容</label>
                                <div class="data-input">
                                    <p><%=dealName%></p>
                                </div>
                            </div>
                            <div class="mixitem">
                                <div class="form-tittle">
                                    <h3 class="rd-payment-h3 small ">選擇好康組合</h3>
                                    <asp:PlaceHolder ID="ph_DealInfoUrl" runat="server" Visible="False"><a id="linkDealInfoUrl" class="exp-specification" target="_blank" href="<%= GetDealSpecificationPageUrl(BusinessHourGuid) %>">查看詳細規格</a></asp:PlaceHolder>
                                </div>
                                <div class="form-unit rd-pay-margin">
                                    <asp:Repeater ID="rpOption" runat="server" OnItemDataBound="rpOption_ItemDataBound">
                                        <HeaderTemplate>
                                            <label id="itemOptionsTitle" for="" class="unit-label">
                                                好康規格</label>
                                            <div id="itemOptions" class="data-input">
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:DropDownList ID="ddlMultOption" CssClass="opt ddlAddOptgroup" runat="server" size="1">
                                            </asp:DropDownList>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </div>
                                        </FooterTemplate>
                                    </asp:Repeater>
                                </div>
                                <div class="form-unit rd-pay-margin">
                                    <label for="" class="unit-label">
                                        數量</label>
                                    <div class="data-input">
                                        <asp:DropDownList ID="itemQty" runat="server">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div id="AddToCartButton" class="form-unit end-unit btn-center rd-pay-pos-fix" style="display: none;">
                                    <input type="button" onclick="javascript:addItemToCart();new MemoryCart().save();" class="btn btn-large btn-primary rd-payment-large-btn"
                                        value="加入購物清單" />
                                </div>
                                <div id="itemError" class="warning-box" style="display: none;">
                                    <p>
                                        請選擇您要購買的好康規格與數量後，加入購物清單
                                    </p>
                                </div>
                                <div id="deliveryChargeError" class="warning-box" style="display: none;">
                                    <p>
                                        還差 <span id="setError"></span>件商品才可以結帳！
                                    </p>
                                </div>
                            </div>
                            <div id="mixitemcart" class="mixitem" style="display: none;">
                                <h3>購物清單</h3>
                                <div class="item-list">
                                    <ul id="itemList">
                                    </ul>
                                </div>
                                <div id="comboSum" style="display: none;" class="item-list">
                                    <ul>
                                        <li>
                                            <div class="item-name">{{ dealTitle }}</div>
                                            <div class="item-amount">
                                                ${{ price }}&nbsp;x&nbsp;<span id="totalComboSet"></span>組
                                                <div class="empty-btn">
                                                </div>
                                            </div>
                                            <div class="clearfix">
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="item-list">
                                    <div class="form-unit end-unit rd-pay-pos-fix">
                                        <label for="" class="unit-label">
                                            總計</label>
                                        <div class="data-input">
                                            <p class="important shopping-list">
                                                $<span>{{ subtotal }}</span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-unit" style="<%= (IsBankDeal ? "visibility: hidden;" : "" )%>">
                                <label for="" class="unit-label">
                                    運費</label>
                                <div class="data-input">
                                    <p>
                                        $<span id="dCharge">{{ freightCharge }}</span><span id="lbldc" runat="server"></span></p>
                                </div>
                            </div>
                            <div class="form-unit rd-pay-pos-fix" style="<%= (IsBankDeal ? "visibility: hidden;" : "" )%>">
                                <label for="" class="unit-label">
                                    總計</label>
                                <div class="data-input">
                                    <p class="important">
                                        $<span>{{ totalAmount }}</span>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <%--1.1 超過config.CreditcardReferAmount 警示出現 --%>
                        <div class="grui-form" id="panGuestMemberLogin" style="display: none">
                            <div class="form-unit end-unit rd-pay-pos-fix warning-box-login">
                                <div class="text-center">
                                    <p class="hint">此筆訂單金額或數量較高，為維護您的權益，請登入會員再進行購買喔！</p>
                                </div>
                                <div class="data-input rd-pay-span-ctr">
                                    <p class="cancel-mr">
                                        <input type="button" class="btn btn-large btn-primary rd-payment-large-btn memberLogin" value="登入" runat="server" id="btnMemberLogin" onserverclick="btnMemberLogin_Click" />
                                        <input type="button" class="btn btn-large rd-payment-large-btn" value="註冊" onclick="window.open(window.registerUrl, '_blank');" />
                                    </p>
                                </div>
                            </div>
                        </div>
                        <%-- step 取貨方式 --%>
                        <asp:PlaceHolder id="phProductDeliveryType" runat="server">
                        <h2 class="rd-payment-h2"><div class="step-num"><i class="fa fa-hand-o-right" style="line-height: 25px;"></i></div>選擇取貨方式</h2>
                        <div class="grui-form message_box_wrapper">
                            <div id="rowProductDeliveryFamily" runat="server" class="form-unit productDeliveryGroup">
                                <label for="" class="unit-label title">取貨方式</label>
                                <div class="data-input">
                                    <label class="data-input-label" style="cursor:pointer">
                                        <input type="radio" id="rdoProductDeliveryFamily" runat="server" 
                                            name="rdoProductDelivery" />
                                        <p class="blank">全家取貨 <span class="hint">new!</span></p>
                                        <img src="../Themes/PCweb/images/icon_cvstore_logo_fami.png" width="116" />
                                        <p class="subnote rd-payment-display">
                                            <%=config.FamilyIspPromoTips %>
                                        </p>
                                    </label>
                                </div>
                            </div>
                            <div id="rowProductDeliverySeven" runat="server" class="productDeliveryGroup form-unit" visible="false">
                                <label for="" class="unit-label title">取貨方式</label>
                                <div class="data-input">
                                    <label class="data-input-label">
                                        <input type="radio" id="rdoProductDeliverySeven" runat="server" name="rdoProductDelivery"  />
                                        <p class="blank">7-11取貨 <span class="hint">new!</span></p>
                                        <img src="../Themes/PCweb/images/icon_cvstore_logo_seven.png" width="116" />
                                        <p class="subnote rd-payment-display">
                                            <%=config.SevenIspPromoTips %>
                                        </p>
                                    </label>
                                </div>
                            </div>
                            <div id="rowProductDeliveryNormal" class="form-unit productDeliveryGroup">
                                <label for="" class="unit-label title">取貨方式</label>
                                <div class="data-input">
                                    <label class="data-input-label" style="cursor:pointer">
                                        <input type="radio" id="rdoProductDeliveryNormal" runat="server" 
                                               name="rdoProductDelivery" />
                                        <p class="blank">宅配到府</p>
                                    </label>
                                </div>
                            </div>
                        </div>
                        </asp:PlaceHolder>
                        <%--step 2--%>
                        <h2 class="rd-payment-h2 buystep2">
                            <div class="step-num">
                                2
                            </div>
                            填寫購買資訊
                        </h2>
                        <div class="grui-form buystep2">
                            <%--購買人資訊--%>
                            <div id="tbBuyerInfoEdit">
                                <div id="tdBuyerEmail" class="form-unit">
                                    <label for="BuyerEmail" class="unit-label">
                                        E-mail</label>
                                    <div class="data-input">
                                        <asp:HiddenField ID="buyerUserName" runat="server" />
                                        <asp:TextBox ID="BuyerEmail" runat="server" CssClass="input-2over3" 
                                            placeholder="請填寫您常用的電子信箱，以便傳送購買資訊" />
                                        <p id="buyerEmailHint" class='note' style='display: none'>登入</p>
                                    </div>
                                    <div id="buyerEmailErr" class="data-input enter-error" style="display: none">
                                        <img src="../Themes/PCweb/images/enter-error.png" width="20" height="20">
                                        <p>請使用字母、數字及英文句點，如17life@example.com</p>
                                    </div>
                                </div>
                                <div id="tdBuyerName" class="form-unit">
                                    <label for="BuyerName" class="unit-label">
                                        姓名</label>
                                    <div class="data-input">
                                        <asp:TextBox ID="BuyerName" runat="server" CssClass="input-2over3" 
                                            placeholder="請填寫正確姓名，以維護您的權益" />
                                    </div>
                                    <div id="buyerNameErr" class="data-input enter-error" style="display: none">
                                        <img src="../Themes/PCweb/images/enter-error.png" width="20" height="20">
                                        <p>請填入姓名！</p>
                                    </div>
                                </div>
                                <div id="tdBuyerMobile" class="form-unit">
                                    <label for="BuyerMobile" class="unit-label">
                                        手機</label>
                                    <div class="data-input">
                                        <asp:TextBox ID="BuyerMobile" runat="server" CssClass="input-2over3 numeric" MaxLength="10" 
                                            placeholder="請填寫手機號碼，以便傳送簡訊兌換券" />
                                    </div>
                                    <div id="buyerMobileErr" class="data-input enter-error" style="display: none">
                                        <img src="../Themes/PCweb/images/enter-error.png" width="20" height="20">
                                        <p>請確認格式，僅可輸入純數字10碼，ex：0912345678</p>
                                    </div>
                                </div>
                                <asp:Panel ID="BuyerAddressPanel" runat="server">
                                    <div class="form-unit">
                                        <label for="" class="unit-label">
                                            地址</label>
                                        <div class="data-input">
                                            <asp:DropDownList ID="ddlBuyerCity" runat="server" CssClass="select-wauto" />
                                            <asp:DropDownList ID="ddlBuyerArea" runat="server" CssClass="select-wauto" />
                                            <asp:TextBox ID="BuyerAddress" runat="server" CssClass="input-full" />
                                        </div>
                                    </div>
                                </asp:Panel>
                            </div>
                            <%--收據資料--%>
                            <asp:PlaceHolder ID="phReceipt" runat="server">
                                <div id="divReceiptEdit">
                                    <div class="form-unit">
                                        <label for="" class="unit-label title">
                                            收據資料</label>
                                        <div class="data-input">
                                            <p class="note">
                                                本收據依財政部台財稅第821481937號函核准使用，僅提供代收轉付收據供您記帳之用，不另開立統一發票。
                                            </p>
                                        </div>
                                    </div>
                                    <div class="form-unit">
                                        <div class="data-input">
                                            <asp:RadioButton ID="rbReceiptType1" runat="server" GroupName="rbReceiptType" Checked="true"
                                                onclick="$('#receiptEditor').hide()" Text="代收轉付收據託管" />
                                            <p class="subnote">
                                                本公司將依法開立並保留代收轉付收據為購買證明，效用等同於統一發票，唯獨不可抵扣稅額，也不可以對獎。 相關問題請詳見「<a href="/ppon/newbieguide.aspx?s=88&q=89"
                                                    style="color: #00F;" target="_blank">常見問題</a>」
                                            </p>
                                        </div>
                                    </div>
                                    <div id="tdReceiptAddress" class="form-unit">
                                        <div class="data-input">
                                            <asp:RadioButton ID="rbReceiptType2" runat="server" GroupName="rbReceiptType" onclick="$('#receiptEditor').show()"
                                                Text="請寄送代收轉付收據" />
                                            <div id="receiptEditor" style="display: none">
                                                <div class="form-unit sublevel">
                                                    <label for="" class="unit-label">
                                                        地址</label>
                                                    <div class="data-input">
                                                        <asp:DropDownList ID="ddlReceiptCity" runat="server" CssClass="select-wauto" />
                                                        <asp:DropDownList ID="ddlReceiptArea" runat="server" CssClass="select-wauto" />
                                                        <asp:TextBox ID="txtReceiptAddress" runat="server" size="20" CssClass="input-full align-fix" />
                                                        <p class="message error-text" style="display: none">
                                                            請填寫正確的地址
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="form-unit sublevel">
                                                    <label for="" class="unit-label">
                                                        統一編號</label>
                                                    <div class="data-input">
                                                        <input type="checkbox" id="chkReceiptForBusiness" />
                                                        <asp:TextBox ID="txtReceiptNumber" runat="server" MaxLength="8" CssClass="cc input-half" />
                                                        <p class="message error-text" style="display: none">
                                                            請填寫統一編號八碼
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="form-unit sublevel">
                                                    <label for="" class="unit-label">
                                                        買受人</label>
                                                    <div class="data-input">
                                                        <asp:TextBox ID="txtReceiptTitle" runat="server" CssClass="input-half" />
                                                        <p class="message error-text" style="display: none">
                                                            請填寫買受人
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </asp:PlaceHolder>
                            <%--收件人資訊--%>
                            <asp:Panel ID="ReceiverInfoPanel" runat="server">
                                <div id="tbReceiverEdit">
                                    <div class="form-unit">
                                        <label for="" class="unit-label title">
                                            收件人資訊</label>
                                        <div class="data-input">
                                            <asp:CheckBox ID="chkBuyerSync" runat="server" Text="同購買人資訊" onclick="copyBuyerInfo();" />
                                            <asp:DropDownList ID="receiverSelect" runat="server" CssClass="select-wauto"
                                                EnableViewState="false" />
                                            <br />
                                            <p class="note">
                                                <span class="rd-payment-words-hide">請確實填寫收件人資訊，以確保商品順利送達。</span><span class="rd-payment-words-hide" />
                                            </p>
                                        </div>
                                    </div>
                                    <div id="tdRecName" class="form-unit">
                                        <label for="" class="unit-label">
                                            收件人姓名</label>
                                        <div class="data-input">
                                            <asp:TextBox ID="ReceiverName" runat="server" CssClass="input-half" />
                                        </div>
                                        <div class="data-input ispHint">
                                            <p class="note">請提供與證件相符姓名，以利門市人員查驗流程</p>
                                        </div>
                                        <div id="receiverNameErr" class="data-input enter-error" style="display: none">
                                            <img src="../Themes/PCweb/images/enter-error.png" width="20" height="20">
                                            <p>請填寫收件人姓名！</p>
                                        </div>
                                    </div>
                                    <div id="tdRecMobile" class="form-unit">
                                        <label for="" class="unit-label" id="receiverMobileLabel">聯絡電話</label>
                                        <div class="data-input">
                                            <asp:TextBox ID="ReceiverMobile" runat="server" CssClass="input-half" />
                                        </div>
                                        <div class="data-input ispHint">
                                            <p class="note">請提供真實手機號碼，以利商品送達門市通知收件人</p>
                                        </div>
                                        <div id="receiverMobileErr" class="data-input enter-error" style="display: none">
                                            <img src="../Themes/PCweb/images/enter-error.png" width="20" height="20">
                                            <p class="errorMessage">請填寫收件人的聯絡電話！</p>
                                        </div>
                                    </div>
                                    <!--超商店取-->
<%--                                    <asp:HiddenField ID="hidProductDeliveryType" runat="server" />--%>
                                    <asp:HiddenField ID="hidFamilyPickupStoreId" runat="server" />
                                    <asp:HiddenField ID="hidFamilyPickupStoreName" runat="server" />
                                    <asp:HiddenField ID="hidFamilyPickupStoreTel" runat="server" />
                                    <asp:HiddenField ID="hidFamilyPickupStoreAddr" runat="server" />
                                    <asp:HiddenField ID="hidSevenPickupStoreId" runat="server" />
                                    <asp:HiddenField ID="hidSevenPickupStoreName" runat="server" />
                                    <asp:HiddenField ID="hidSevenPickupStoreTel" runat="server" />
                                    <asp:HiddenField ID="hidSevenPickupStoreAddr" runat="server" />
                                    <div id="tdStorePickup" runat="server" class="form-unit rd-payment-form-unit-width">
                                        <label for="" class="unit-label">取貨門市</label>
                                        <div class="data-input" style="padding:0px 5px 5px 5px">
                                            <p id="pickupStoreSelect" class="cancel-mr">
                                                <asp:Button ID="btnSelectCVS" runat="server" CssClass="btn btn-small rd-payment-large-btn memoryButton"
                                                    Text="請選擇門市" OnClick="btnSelectCVS_Click" />
                                            </p>
                                            <img id="iconCvsFamily" style="display:none" src="../Themes/PCweb/images/icon_cvstore_logo_fami.png" width="116" />
                                            <img id="iconCvsSeven" style="display:none" src="../Themes/PCweb/images/icon_cvstore_logo_seven.png" width="116" />
                                        </div>
                                        <div id="pickupStoreShow" class="data-input grey">
                                            <p id="pPickupTitle" runat="server">全家芎林富林店 新竹縣芎林鄉富林路三段94號</p>                                            
                                            <p class="note" style="display: inline-block;">
                                                <a id="aSelectFamilyCVS" class="memoryButton" runat="server" onserverclick="btnSelectCVS_Click"><span style="text-decoration: underline;">更換取貨門市</span></a>
                                            </p>
                                        </div>
                                        <div id="pickupStoreErr" class="data-input enter-error" style="display: none">
                                            <img src="../Themes/PCweb/images/enter-error.png" width="20" height="20">
                                            <p>請選擇取貨門市！</p>
                                        </div>
                                    </div>                                    
                                    <div id="tdRecAddr" class="form-unit rd-payment-form-unit-width">
                                        <label for="" class="unit-label">
                                            地址</label>
                                        <div class="data-input">
                                            <asp:DropDownList ID="ddlReceiverCity" runat="server" CssClass="select-wauto" ClientIDMode="Static"/>
                                            <asp:DropDownList ID="ddlReceiverArea" runat="server" CssClass="select-wauto" ClientIDMode="Static"/>
                                            <asp:TextBox ID="ReceiverAddress" runat="server" CssClass="input-full" ClientIDMode="Static"/>
                                            <div id="loadingIMG2" style="display:none;"><img src="../Themes/PCweb/images/spinner.gif"/></div>
                                            <div style="color:red;display:none;" id="lbReceiverAddressValidateMsg"></div>
                                        </div>
                                        <div id="receiverAddrErr" class="data-input enter-error" style="display: none">
                                            <img src="../Themes/PCweb/images/enter-error.png" width="20" height="20">
                                            <p>請選擇收件的縣市與鄉鎮區域，並填寫完整的收件地址。</p>
                                        </div>
                                        <div id="receiverLostAddrErr" class="data-input enter-error" style="display: none">
                                            <img src="../Themes/PCweb/images/enter-error.png" width="20" height="20">
                                            <p>您的收件地址可能少了路名或街道名稱，請填寫完整的收件地址。</p>
                                        </div>
                                        <div id="notDeliveryIslandsAddrErr" class="data-input enter-error" style="display: none">
                                            <img src="../Themes/PCweb/images/enter-error.png" width="20" height="20">
                                            <p>註：此商品恕不配送離島、外島地區。請選擇並填寫本島的收件地址。</p>
                                        </div>
                                        <% if (this.IsAuthenticated)
                                           { %>
                                        <div class="data-input">
                                            <asp:CheckBox ID="AddReceiverInfo" runat="server" Text="新增本次填寫的收件人資料" />
                                        </div>
                                        <%} %>
                                    </div>
                                </div>
                            </asp:Panel>
                            <% if (this.IsAuthenticated == false)
                               { %>
                            <div class="form-unit">
                                <div class="data-input">
                                    <label class="data-input-label">
                                        <input id="chkRememberUserInfo" type="checkbox" checked="true" />
                                        <p class="cancel-mr rd-letter-spacing note">記住以上資訊（若您使用共用裝置，請勿勾選）</p>
                                    </label>
                                </div>
                            </div>
                            <% } %>
                        </div>
                        <%--step 3--%>
                        <h2 class="rd-payment-h2 buystep3">
                            <div class="step-num">
                                3
                            </div>
                            <span id="step3Title">紅利/購物金折抵</span></h2>
                        <div class="grui-form message_box_wrapper buystep3">
                            <asp:PlaceHolder runat="server" ID="pDiscount">
                                <div class="form-unit">
                                    <label id="step3ActionNubmer" for="" class="unit-label">
                                        17Life折價券</label>
                                    <asp:PlaceHolder runat="server" ID="pDiscountCodeInput">
                                        <div class="data-input">
                                            <p class="step3Div">
                                                $<span>{{ discountAmount }}</span>
                                            </p>
                                            <asp:TextBox ID="tbDiscount" runat="server" onblur="checkDiscountCode();" Style="text-transform: uppercase"
                                                CssClass="input-half" placeholder="請輸入折價碼" />
                                            <p class="cancel-mr">
                                                <asp:Button ID="btnSelectDiscount" Visible="false" runat="server" CssClass="btn btn-small rd-payment-large-btn btn_message" Text="選擇折價券" OnClientClick="ShowDiscount(); return false;" />
                                            </p>

                                        </div>

                                        <div class="data-input">
                                            <p id="couponMessage" class="note" style="display: inline-block;"></p>
                                        </div>

                                        <div class="data-input step3Div">
                                            <p class="note">
                                                每一筆訂單限抵用一個序號；一經使用，無論是否退貨皆不得返還。恕不找零。
                                            </p>
                                        </div>
                                    </asp:PlaceHolder>
                                    <asp:PlaceHolder runat="server" ID="pNotDiscount" Visible="false">
                                        <div class="data-input">
                                            <p class="error-text">
                                                優惠商品不適用折價券
                                            </p>
                                        </div>
                                    </asp:PlaceHolder>
                                </div>
                                <%--折價券區塊--%>
                                <div id="discountList" class="message_box discount_box step3Div" style="display: none;">
                                    <div class="message_box_arrow"></div>
                                    <div class="message_box_main">
                                        <p>請選擇折價券(共<span class="hint"><asp:Literal ID="liDiscountCount" runat="server"></asp:Literal></span>張)</p>
                                        <asp:Repeater ID="rptDiscountList" runat="server">
                                            <HeaderTemplate>
                                                <ul id="discountCodeList" style="display: block; position: relative;">
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <li id="<%# ((LunchKingSite.DataOrm.ViewDiscountDetail)Container.DataItem).Code %>" onclick="SelectDiscount(this);" style="cursor: pointer;" data-amount="<%# Convert.ToString(((LunchKingSite.DataOrm.ViewDiscountDetail)Container.DataItem).MinimumAmount)%>">
                                                    <div class="cover" style="">未達消費門檻</div>
                                                    <a>
                                                        <i class="fa fa-check fa-lg fa-fw"></i>折價券<span class="hint"><%# ((LunchKingSite.DataOrm.ViewDiscountDetail)Container.DataItem).Amount.Value.ToString("F0") %></span>元，
                                                    <%# ((LunchKingSite.DataOrm.ViewDiscountDetail)Container.DataItem).MinimumAmount.HasValue ? "消費滿<span class='hint'>" + ((LunchKingSite.DataOrm.ViewDiscountDetail)Container.DataItem).MinimumAmount.Value.ToString("F0") + "</span>可使用。" : "無使用門檻。" %>
                                                    <span class="date"><%# ((LunchKingSite.DataOrm.ViewDiscountDetail)Container.DataItem).EndTime.Value.ToString("yyyy/MM/dd HH:mm") %>前有效</span>
                                                    <br />
                                                    <p class="conform"><%# Helper.IsFlagSet(((LunchKingSite.DataOrm.ViewDiscountDetail)Container.DataItem).Flag, DiscountCampaignUsedFlags.CategoryLimit) ? "適用：" + PromotionFacade.GetDiscountCategoryString(((LunchKingSite.DataOrm.ViewDiscountDetail)Container.DataItem).Id) : string.Empty %></p>
                                                   </a>
                                                </li>
                                            </ItemTemplate>
                                            <FooterTemplate></ul></FooterTemplate>
                                        </asp:Repeater>
                                        <p class="note">僅顯示可使用之折價券，其餘請至 會員中心 &gt; 折價券 查詢</p>
                                        <p class="cancel-mr">
                                            <input type="button" class="btn btn-small rd-payment-large-btn btn_message_close" autocomplete="off" value="取消" onclick="CloseDiscount();">
                                        </p>
                                    </div>
                                </div>
                            </asp:PlaceHolder>
                            <asp:PlaceHolder ID="pBonus" runat="server">
                                <div v-if="bcashMaxinum>0" class="form-unit step3Div">
                                    <label for="" class="unit-label">
                                        使用紅利金</label>
                                    <div class="data-input">                              
                                        <input type="text" id="txtbcash" v-model="bcash" v-bind:disabled="price==0" 
                                            v-on:keyup="recalculateTotal()" v-on:blur="recalculateTotal"
                                            size="3" class="input-half" pattern="[0-9]*" value="0"/>
                                        <p>
                                            餘額：${{ bcashMaxinum }}</p>
                                        <p>
                                            <span id="bcashErr" class="error-text" style="display: none;">餘額不足</span> <span id="bcashNaN"
                                                class="error-text" style="display: none;">請輸入數字</span>
                                        </p>
                                    </div>
                                </div>
                            </asp:PlaceHolder>
                            <asp:PlaceHolder ID="pScash" runat="server">
                                <div class="form-unit step3Div">
                                    <label for="" class="unit-label">
                                        使用購物金</label>
                                    <div class="data-input">
                                        <input type="text" id="txtscash" v-model="scash" v-bind:disabled="price==0" 
                                            v-on:keyup="recalculateTotal()" v-on:blur="recalculateTotal"
                                               size="3" class="input-half" pattern="[0-9]*" value="0"/>                                        
                                        <p>
                                            餘額：${{ scashMaxinum }}</p>
                                        <p>
                                            <span id="scashErr" class="error-text" style="display: none;">餘額不足</span> <span id="scashNaN"
                                                class="error-text" style="display: none;">請輸入數字</span>
                                        </p>
                                        <asp:PlaceHolder ID="phPezExchange" runat="server" Visible="false">
                                            <p class="note">
                                            <span style="color:blue">歡迎使用PayEasy福利點數兌換17Life購物金</span>
                                            <input id="btnPezExchange" type="button" class="btn btn-small rd-payment-large-btn btn_message" value="點我前往" />
                                            <asp:HiddenField ID="hidMerchantContent" runat="server" />
                                            <br />
                                            綁定PayEasy帳號後即可進行PayEasy福利點數兌換17Life購物金。當您決定使用17Life購物金並填入金額後，支付時17Life將優先使用由兌換而來的購物金。其他說明與注意事項可至客服中心-常見問題中詳閱
                                        </p>
                                        </asp:PlaceHolder>
                                        
                                    </div>
                                </div>
                            </asp:PlaceHolder>
                            <div class="form-unit step3Div" style="<%= (IsBankDeal ? "visibility: hidden;" : "" )%>">
                                <label for="" class="unit-label">
                                    支付總額</label>
                                <div class="data-input">
                                    <p class="important">
                                        $<span>{{ pendingAmount }}</span>
                                    </p>
                                    <p>
                                        <span id="totalErr" class="error-text" style="display: none;">金額不可為負數</span>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <%--step 4--%>
                        <div class="step4Div">
                        <h2 id="hInvoiceEditTitle" class="rd-payment-h2 buystep4">
                            <div class="step-num rd-payment-step-num">
                                4
                            </div>
                            選擇發票開立方式
                            <p class="note">選擇發票開立方式後，不得要求原發票變更開立方式。</p>
                        </h2>
                        <div id="divInvoiceEditContent" class="grui-form buystep4">
                            <asp:PlaceHolder ID="phInvoice" runat="server">
                                <div id="divInvoiceCreate" runat="server">
                                    <div id="tbInvoiceEdit">
                                        <div class="form-unit">
                                            <label for="" class="unit-label title">
                                                發票開立方式</label>
                                            <div class="data-input">
                                                <ul class="permutation">
                                                    <li>
                                                        <asp:RadioButton ID="rbInvoiceDonation" Text="捐贈發票" runat="server" GroupName="InvoiceMode" onclick="ChangeInvoiceMode();" Checked="true" />
                                                    </li>
                                                    <li>
                                                        <asp:RadioButton ID="rbInvoiceDuplicate" Text="個人電子發票(二聯式)" runat="server" GroupName="InvoiceMode" onclick="ChangeInvoiceMode();" />
                                                    </li> 
                                                    <li>
                                                        <asp:RadioButton ID="rbInvoiceTriplicate" Text="公司紙本發票(三聯式)" runat="server" GroupName="InvoiceMode" onclick="ChangeInvoiceMode();" />
                                                    </li>
                                                </ul>
                                                <%--個人電子發票(二聯式)--%>
                                                <div id="divInvoice2" style="display: none;">
                                                    <p class="note">
                                                        電子發票依據財政部令台財稅字第0990216895號<br>
                                                        「 <a href="<%=config.InvoiceKeyPointUrl%>" target="_blank">電子發票實施作業要點</a> 」財北國稅中南營業一字第0990017821號函核准使用。
                                                    </p>
                                                    <p class="note">
                                                        ※ 17Life將依財政部提供之名單，主動將中獎發票以掛號至您的中獎發票地址。若需要更新，請至「帳號設定>中獎發票管理」下，進行調整。<br>
                                                        ※ 依統一發票使用辦法規定：二聯式發票一經開立，不得任意更改為三聯式發票。<br>
                                                        <label class="data-input-label">
                                                            <input id="chkPersonalCarrier" type="checkbox" onclick="CheckPersonalCarrier(this);">
                                                            <span class="blank">使用<a href="https://www.einvoice.nat.gov.tw/APMEMBERVAN/GeneralCarrier/generalCarrier?mp=1"
                                                                target="_blank">手機條碼載具</a>、<a href="https://www.einvoice.nat.gov.tw/APMEMBERVAN/NPCBarcode/NPCBarcode!pwdApply?CSRT=8396858996934606537"
                                                                    target="_blank">自然人憑證載具</a>。</span>
                                                        </label>
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
                                                    <div id="PersonalCarrier" style="display: none;">
                                                        <asp:DropDownList ID="ddlCarrierType" runat="server" CssClass="select-wauto" onchange="ChangeCarrierType(this);" />
                                                        <p id="carrierNote" class="note">使用手機條碼載具或自然人憑證載具，將由財政部通知中獎。</p>
                                                        <%--手機條碼載具--%>
                                                        <div id="tdPhoneCarrier" class="form-unit sublevel">
                                                            <label for="" class="unit-label">手機條碼</label>
                                                            <div class="data-input">
                                                                <asp:TextBox ID="txtPhoneCarrier" runat="server" CssClass="input-half" MaxLength="8" />
                                                                <asp:HiddenField ID="hidCarrIdVerify" runat="server" Value="true" EnableViewState="false" />
                                                                <p id="loadingIMG" style="display:none;"><img src="../Themes/PCweb/images/spinner.gif"/></p>
                                                                <p id="PhoneCarrierError" class="note">
                                                                    共8碼。限數字與大寫英文，並以 / 開頭。
                                                                </p>
                                                            </div>
                                                            <div id="phoneCarrierErr" class="data-input enter-error" style="display: none">
                                                                <img src="../Themes/PCweb/images/enter-error.png" width="20" height="20">
                                                                <p>請輸入手機條碼。</p>
                                                            </div>
                                                            <div id="phoneCarrierInvalid" class="data-input enter-error" style="display: none">
                                                                <img src="../Themes/PCweb/images/enter-error.png" width="20" height="20">
                                                                <p>此手機條碼無效，請您重新輸入。</p>
                                                            </div>
                                                        </div>
                                                        <%--自然人憑證載具--%>
                                                        <div id="tdPersonalCertificateCarrier" class="form-unit sublevel" style="display: none;">
                                                            <label for="" class="unit-label">自然人憑證</label>
                                                            <div class="data-input">
                                                                <asp:TextBox ID="txtPersonalCertificateCarrier" runat="server" CssClass="input-half" 
                                                                    MaxLength="16"></asp:TextBox>
                                                                <p class="note">
                                                                    共16碼：2碼大寫英文+14碼數字。
                                                                </p>
                                                            </div>
                                                            <div id="personalCertificateCarrierErr" class="data-input enter-error" style="display: none">
                                                                <img src="../Themes/PCweb/images/enter-error.png" width="20" height="20">
                                                                <p>請輸入自然人憑證條碼。</p>
                                                            </div>
                                                            <div id="personalCertificateCarrierInvalid" class="data-input enter-error" style="display: none">
                                                                <img src="../Themes/PCweb/images/enter-error.png" width="20" height="20">
                                                                <p>此自然人憑證條碼無效，請您重新輸入。</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <%--捐贈發票--%>
                                                <div id="divInvoiceDonation" >
                                                    <asp:DropDownList ID="ddlInvoiceDonation" runat="server" CssClass="select-wauto" 
                                                        onchange="ChangeInvoiceDonation(this);">
                                                    </asp:DropDownList>
                                                    <%--創世社會福利基金會--%>
                                                    <p id="divInvoiceGenesisDonation" class="headnote">
                                                        認識<a href="http://www.genesis.org.tw/benefaction.php" target="_blank">創世社會福利基金會</a>
                                                    </p>
                                                    <%--財團法人陽光社會福利基金會--%>
                                                    <p id="divInvoiceSunDonation" class="headnote">
                                                        認識<a href="http://www.sunshine.org.tw/" target="_blank">財團法人陽光社會福利基金會</a>
                                                    </p>

                                                    <%--其他捐贈單位--%>
                                                    <div id="divInvoiceDonationOther" style="display: none;">
                                                        <p class="headnote">
                                                            <a href="https://www.einvoice.nat.gov.tw/APMEMBERVAN/XcaOrgPreserveCodeQuery/XcaOrgPreserveCodeQuery?CSRT=8396858996934606537"
                                                                target="_blank">查詢社福團體愛心碼</a>
                                                        </p>
                                                        <div id="tdLoveCode" class="form-unit sublevel">
                                                            <label for="" class="unit-label">請輸入愛心碼</label>
                                                            <div class="data-input">
                                                                <input type="hidden" id="LoveId">
                                                                <asp:TextBox ID="txtLoveCode" runat="server" CssClass="input-half" />
                                                                <p class="note">請輸入3~7碼數字。</p>
                                                            </div>
                                                            <div id="loveCodeErr" class="data-input enter-error" style="display: none">
                                                                <img src="../Themes/PCweb/images/enter-error.png" width="20" height="20">
                                                                <p>請輸入3~7碼數字愛心碼。</p>
                                                            </div>
                                                        </div>
                                                        <div class="form-unit sublevel">
                                                            <label for="" class="unit-label">捐贈單位名稱</label>
                                                            <div class="data-input">
                                                                <p id="LoveCodeDesc" class="note">請於上方輸入正確的愛心碼。</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <%--公司紙本發票(三聯式)--%>
                                                <div id="divInvoice3" style="display: none;">
                                                    <p style="color:red;font-size:smaller">三聯式發票開立後，若有退貨需求，需將<span style="font-weight:800">紙本折讓單寄回。</span></p>
                                                    <p class="note">發票開立後，不得變更發票抬頭或統一編號。</p>
                                                    <div id="tdInvoTitle" class="form-unit sublevel">
                                                        <label for="" class="unit-label">發票抬頭</label>
                                                        <div class="data-input">
                                                            <asp:TextBox ID="InvoiceTitle" runat="server" CssClass="input-half" />
                                                        </div>
                                                        <div id="invoTitleErr" class="data-input enter-error" style="display: none">
                                                            <img src="../Themes/PCweb/images/enter-error.png" width="20" height="20">
                                                            <p>請填入發票抬頭。</p>
                                                        </div>
                                                    </div>
                                                    <div id="tdInvoNumber" class="form-unit sublevel">
                                                        <label for="" class="unit-label">統一編號</label>
                                                        <div class="data-input">
                                                            <asp:TextBox runat="server" ID="InvoiceNumber" CssClass="input-half" MaxLength="8" />
                                                        </div>
                                                        <div id="invoNumberErr" class="data-input enter-error" style="display: none">
                                                            <img src="../Themes/PCweb/images/enter-error.png" width="20" height="20">
                                                            <p>請填寫統一編號八碼。</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <%--紙本發票--%>
                                                <div id="PaperInvoice" style="display: none;">
                                                    <div id="tdInvoName" class="form-unit sublevel">
                                                        <label for="" class="unit-label">收件人</label>
                                                        <div class="data-input">
                                                            <asp:TextBox runat="server" ID="InvoiceBuyerName" CssClass="input-half" />
                                                        </div>
                                                        <div id="invoNameErr" class="data-input enter-error" style="display: none">
                                                            <img src="../Themes/PCweb/images/enter-error.png" width="20" height="20">
                                                            <p>請填寫發票收件人姓名。</p>
                                                        </div>
                                                    </div>
                                                    <div id="tdInvoAddr" class="form-unit sublevel">
                                                        <label for="" class="unit-label">發票地址</label>
                                                        <div class="data-input">
                                                           <asp:UpdatePanel ID="UpdatePanel1" runat="server" ChildrenAsTriggers="true" RenderMode="Inline">
                                                             <ContentTemplate> 
                                                                  <asp:DropDownList ID="ddlInvoiceCity" runat="server" CssClass="select-wauto"  onchange="CopyInvoiceInfo();" ClientIDMode="Static">
                                                                  </asp:DropDownList>
                                                                  <asp:DropDownList ID="ddlInvoiceArea" runat="server" CssClass="select-wauto"  onchange="CopyInvoiceInfo();" ClientIDMode="Static">
                                                                  </asp:DropDownList>
                                                                 <br />

                                                             </ContentTemplate>
                                                           </asp:UpdatePanel>
                                                        </div>

                                                        <div class="data-input">
                                                            <asp:TextBox runat="server" ID="InvoiceAddress" CssClass="input-full"  ClientIDMode="Static"/>
                                                        </div>
                                                        <div id="invoAddrErr" class="data-input enter-error" style="display: block">
                                                            <img src="../Themes/PCweb/images/enter-error.png" width="20" height="20">
                                                            <p>請填寫發票收件地址。</p>
                                                        </div>
                                                    </div>
                                                    <div id="tdInvoiceSave">
                                                        <asp:CheckBox ID="InvoiceSave" runat="server" Text="儲存本次填寫內容" Checked="True" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </asp:PlaceHolder>
                        </div>
                        </div>
                        <%--step 5--%>
                        <h2 class="rd-payment-h2 SelectedPayWay buystep5">
                            <div class="step-num rd-payment-step-num">
                                5
                            </div>
                            選擇購買方式
                        </h2>
                        <asp:Repeater ID="repBuyPromotionText" runat="server">
                            <HeaderTemplate>
                                <div class="cc_marketing step5Div">
                            </HeaderTemplate>
                            <ItemTemplate>
                                <div class="step5Div">
                                    <span>
                                        <img src="../Themes/PCweb/images/icon_cc_save.png" /></span>
                                    <strong><%#Eval("Text") %></strong>
                                </div>
                            </ItemTemplate>
                            <FooterTemplate>
                                </div><div class="clear"></div>
                            </FooterTemplate>
                        </asp:Repeater>
                        <div class="grui-form buystep5">
                            <%--付款方式-Apple Pay--%>
                            <asp:Panel ID="ApplePayWay" runat="server" Visible="false">
                                <div class="form-unit SelectedPayWay">
                                    <div class="data-input">
                                        <label class="data-input-label">
                                            <asp:RadioButton ID="rbPayWayApplePay" runat="server" GroupName="PayWay" Text="" />
                                            <img src="../Themes/PCweb/images/ApplePay-black.png" width="48" height="" alt="">
                                            <p class="blank">使用Apple Pay付款</p>
                                        </label>
                                    </div>
                                </div>
                                <asp:HiddenField ID="hdSessionVersion" runat="server" Value="" />
                                <asp:HiddenField ID="hdPrebuiltOrderGuid" runat="server" Value="" />
                            </asp:Panel>
                            <%--付款方式-信用卡--%>
                            <asp:Panel ID="pan_CreditCardWay" runat="server">                               
                                <div class="form-unit SelectedPayWay">
                                    <div class="data-input">
                                        <label class="data-input-label">
                                            <asp:RadioButton ID="rbPayWayByCreditCard" runat="server" GroupName="PayWay"
                                                Checked="true" Text="信用卡付款" />
                                            <div class="CreditCardContent">
                                                <p class="subnote rd-payment-display">
                                                    目前暫不接受美國運通(American Express)卡<br />
                                                    交易未達門檻前，不會進行扣款動作，請放心
                                                </p>
                                            </div>
                                            <!--
                                            <div class="visa-promo">
                                                    <img src="https://www.17life.com/images//17P/active/20150831-Mid-Autumn-Festival/310x110.jpg">
                                            </div>
                                            -->
                                        </label>
                                    </div>
                                </div>
                            </asp:Panel>
                            <%--付款方式-全家超商取貨付款--%>
                            <asp:PlaceHolder ID="phFamilyIspPayWay" runat="server">
                                <div id="familyIspPayWay" class="form-unit SelectedPayWay" style="display:none">
                                    <div class="data-input">
                                        <label class="data-input-label">
                                            <asp:RadioButton ID="rbPayWayFamilyIsp" runat="server" GroupName="PayWay" Text="全家取貨付款" />
                                            <img src="/Themes/PCweb/images/icon_cvstore_logo_fami.png" width="116">
                                            <p class="subnote rd-payment-display">
                                                商品送達至門市後於櫃檯付款並取貨，本付費方式不加收手續費。
                                            </p>
                                        </label>
                                    </div>
                                </div>
                            </asp:PlaceHolder>
                            <%--付款方式-7-11超商取貨付款--%>
                            <asp:PlaceHolder ID="phSevenIspPayWay" runat="server">
                                <div id="sevenIspPayWay" class="form-unit SelectedPayWay" style="display:none">
                                    <div class="data-input">
                                        <label class="data-input-label">
                                            <asp:RadioButton ID="rbPayWaySevenIsp" runat="server" GroupName="PayWay" Text="7-11取貨付款" />
                                            <%--<p class="blank">7-11取貨付款</p>--%>
                                            <img src="/Themes/PCweb/images/icon_cvstore_logo_seven.png" width="92">
                                            <p class="subnote rd-payment-display">
                                                商品送達至門市後於櫃檯付款並取貨，本付費方式不加收手續費。
                                            </p>
                                        </label>
                                    </div>
                                </div>
                            </asp:PlaceHolder>
                            <%--付款方式-台新信用卡分期--%>
                            <asp:PlaceHolder ID="phInstallmentContainer" runat="server">
                              <div class="form-unit SelectedPayWay">
                                <div class="data-input">
                                  <label class="data-input-label">
                                    <input type="radio" id="rbInstallmentDefault" 
                                        name="PayWay" runat="server" />
                                    <p class="blank">台新、國泰卡友享分期0利率</p></label>
                                    <div class="phInstallmentContent">
                                    <p class="subnote rd-payment-display">
                                      歡迎使用台新、國泰銀行信用卡分期服務<br />
                                        目前僅接受台新、國泰銀行信用卡使用分期0利率優<br />
                                      惠，暫不接受其他銀行信用卡分期服務。<br />
                                    注意！使用分期付款之訂單，如需退貨則需整筆<br />
                                    進行。若有相關問題，請洽客服中心。<br />
                                      <a target="_blank" href="https://www.17life.com/Ppon/NewbieGuide.aspx?id=li20531">信用卡分期說明</a>
                                    </p>
                                    <p class="subnote rd-payment-display">
                                      請選擇分期(*除不盡餘數於第一期收款)
                                    </p>
                                    </div>
                                    <asp:PlaceHolder ID="phInstallment3Months" runat="server">
                                    <div>
                                        <label class="second-label">
                                        <input type="radio" class="credit-instalment" id="rbInstallment3Months" 
                                            name="PayWayInstalment" runat="server" />
                                        <p class="instalment rd-payment-display">NT$ x 3期0利率</p>
                                        </label>
                                        <div class="clearfix"></div>
                                    </div>
                                    </asp:PlaceHolder>
                                    <asp:PlaceHolder ID="phInstallment6Months" runat="server">
                                    <div>
                                        <label class="second-label">
                                        <input type="radio" class="credit-instalment" id="rbInstallment6Months" 
                                            name="PayWayInstalment" runat="server" />
                                        <p class="instalment rd-payment-display">NT$ x 6期0利率</p>
                                        </label>
                                        <div class="clearfix"></div>
                                    </div>
                                    </asp:PlaceHolder>
                                    <asp:PlaceHolder ID="phInstallment12Months" runat="server">
                                    <div>
                                        <label class="second-label">
                                        <input type="radio" class="credit-instalment" id="rbInstallment12Months" 
                                            name="PayWayInstalment" runat="server" />
                                        <p class="instalment rd-payment-display">NT$35 x 12期0利率</p>
                                        </label>
                                    </div>
                                    </asp:PlaceHolder>
                                </div>
                              </div>
                            </asp:PlaceHolder>

                            <%--付款方式-MasterPass--%>
                            <asp:Panel ID="pan_MasterPass" runat="server" Visible="false">
                                <div class="form-unit SelectedPayWay">
                                    <div class="data-input">
                                        <asp:RadioButton ID="rbPayWayByMasterPass" runat="server" GroupName="PayWay"
                                            Text="MasterPass付款" />
                                        <img width="48" height="30" alt="" src="../Themes/PCweb/images/masterpass.png" />
                                        <p><a target="_blank" href="http://www.mastercard.com.tw/masterpass">了解更多</a></p>
                                        <p>
                                            <asp:DropDownList id="ddlMasterPassCards" Width="300" DataValueField="CardId" DataTextField="CardTitle" runat="server" Visible="false">
                                            </asp:DropDownList>
                                        </p>
                                        <div class="MasterPassContent">
                                        <p class="subnote rd-payment-display">
                                            歡迎使用MasterPass<br>
                                            目前暫不接受美國運通(American Express)卡<br>
                                            交易未達門檻前，不會進行扣款動作，請放心<br>
                                        </p>
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>

                            <%--付款方式-台新儲值支付--%>
                            <asp:Panel ID="pan_TaishinPay" runat="server" Visible="false">
                                <div class="form-unit SelectedPayWay">
                                    <div class="data-input lmMemoryLog-wrap">
                                        <asp:RadioButton ID="rbPayWayTaishinPay" runat="server" GroupName="PayWay" Text="台新儲值支付" />
                                        <asp:Panel ID="pan_nonTaishinCashPoint" runat="server" style="display:inline-block">
                                               <img src="../Themes/PCweb/images/taishinpay_90x30.jpg" width="76" height="30" alt="TaishinPay">
                                               <div class="surplus" style="display:inline-block"><a href="https://www.17life.com/images/17P/active/201603taishin/taishin.html" target="_blank">了解更多</a></div>
                                        </asp:Panel>
                                        <asp:Panel ID="pan_TaishinCashPoint" runat="server" Visible="false" style="display:inline-block">
                                        <div class="surplus" style="display:inline-block">
                                            (餘額：$<asp:Label ID="lbl_TaishinCashPoint" runat="server" CssClass="TaishinCashPoint"></asp:Label>，
                                            <a href="<%=config.TaishinThirdPartyPayIndexUrl%>" target="_blank">點此儲值</a> 
                                            <span class="rd-C-Hide">
                                            <img  src="../Themes/PCweb/images/lmQuestion.png" width="21" height="21">
                                            </span>)
                                        </div>     
                                        <div class="lmMemoryLog-box">
                                          <p >若您的餘額低於訂單金額，請先至台新儲值後，重新整理本頁面以更新餘額資訊。</p>
                                        </div>
                                        </asp:Panel>
                                        
                                        <div class="TaishinContent">
                                        <p class="subnote rd-payment-display">
                                          已有儲值支付帳戶，請點選"下一步"，<br />
                                          若尚未有帳戶，可<a href="<%=config.TaishinThirdPartyPayIndexUrl%>" target="_blank">點此註冊</a><br />
                                        </p>
                                        </div>
                                        
                                    </div>
                                </div>
                                <div class="taishin-pay-check-warning pop-window step5Div" style="display:none" >
                                  <div class="pop-content">
                                    <p>您目前的餘額不足，請至台新加值，或使用其它付款方式，謝謝!<br/>
                                    (加值後，請重新整理本頁面以更新餘額資訊。)
                                    </p>
                                    <div class="text-center">
                                      <input type="button" class="btn btn-large btn-primary" value="前往儲值" onclick="$.unblockUI();openTaishinPage();" > 
                                      <input type="button" class="btn btn-large" value="取消" onclick="$.unblockUI()"> 
                                    </div>
                                  </div>
                                  <div class="MGS-XX" onclick="$.unblockUI()">
                                      <input type="button" class="btn btn-large" value="關閉" onclick="$.unblockUI()">
                                  </div> 
                                </div>
                                
                            </asp:Panel>

                            <%--付款方式-ATM付款--%>
                            <div id="AtmPayWay" runat="server" class="form-unit SelectedPayWay">
                                <div class="data-input">
                                    <asp:RadioButton ID="rbPayWayByAtm" runat="server" GroupName="PayWay" Text="ATM付款" />
                                    <div class="AtmPayContent">
                                    <p class="subnote rd-payment-display">
                                        請於訂單成立當日的23:59:59前完成付款，<br />
                                        如因時間差在完成付款後，發生庫存已售完的情形，<br />
                                        我們會以客服信件通知，並會將費用全額退回，請您放心。<br />
                                    </p>
                                    </div>
                                </div>
                            </div>

                            <%--付款方式-LinePay--%>
                            <asp:Panel ID="LinePayWay" runat="server" Visible="false">
                                <div class="form-unit SelectedPayWay">                             
                                    <div class="data-input">
                                        <asp:RadioButton ID="rbPayWayByLinePay" runat="server" GroupName="PayWay"
                                            Text="LINE Pay" />
                                        <img src="../Themes/PCweb/images/linepay_120x40.png" alt="LinePay">
                                        <p><a href="http://line.me/zh-hant/pay" target="_blank">了解更多</a></p>
                                        <div class="LinePayContent">
                                        <p class="subnote rd-payment-display">
                                        
                                          歡迎使用LINE Pay<br />
                                          請準備您的手機以完成交易
                                        
                                        </p>
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>

                            <div class="form-unit end-unit rd-pay-pos-fix">
                                <% if ((config.GuestBuyEnabled || AllowGuestBuy) && this.IsAuthenticated == false)
                                   {%>
                                <div class="data-input">
                                    <label class="data-input-label">
                                        <input type="checkbox" id="chkGuestMemberAgreePolicy">
                                        <p class="cancel-mr rd-letter-spacing cancel-mr-width90">
                                            我願遵守本好康使用規範，以及
                                        <a href="https://www.17life.com/newmember/privacypolicy.aspx" target="_blank">服務條款</a> 和
                                        <a href="https://www.17life.com/newmember/privacy.aspx" target="_blank">隱私權條款</a>
                                        </p>
                                    </label>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
                                <% } %>
                                <div class="data-input rd-pay-span-ctr">
                                    <p class="subnote">
                                        <input type="button" onclick="new MemoryInfo().save();ClickBuy();" class="btn btn-large btn-primary rd-payment-xlarge-btn"
                                            value="下一步" />
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <%--訂單資料確認--%>
                    <div id="divPurchaseInfoConfirm" class="content-group payment" style="display: none">

                        <h1 id="TopTitle" class="rd-payment-h1">訂單資料確認</h1>
                        <hr class="header_hr rd-payment-width" />
                        <%--<h2 class="important rd-payment-h2">為了維護您的權益，請確認交易資料無誤後送出！</h2>--%>
                        <div class="grui-form">
                            <%--好康資訊--%>
                            <div id="confirmItemList">
                                <div class="form-unit" v-if="storeInfo != ''">
                                    <asp:Label ID="lblStoreChioceConfirm" runat="server" CssClass="unit-label title"
                                        Text="分店選擇"
                                        Visible="false"></asp:Label>
                                    <div class="data-input">
                                        <p>
                                            <span>{{ storeInfo }}</span>
                                        </p>
                                    </div>
                                </div>
                                <div class="form-unit">
                                    <label for="" class="unit-label title">
                                        好康內容</label>
                                    <div class="data-input">
                                        <p>{{ dealTitle }}</p>
                                    </div>
                                </div>
                                <div class="mixitem">
                                    <h3>購物清單</h3>
                                    <div class="item-list">
                                        <ul id="confirmList">
                                        </ul>
                                    </div>
                                    <div id="confirmComboSum" style="display: none;" class="item-list">
                                        <ul>
                                            <li>
                                                <div class="item-name">{{ dealTitle }}</div>
                                                <div class="item-amount">
                                                    ${{ price }}&nbsp;x&nbsp;<span id="totalConfirmComboSet"></span>組
                                                    <div class="empty-btn">
                                                    </div>
                                                </div>
                                                <div class="clearfix">
                                                    <span id="totalConfirmComboTotal" style="display: none"></span>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="item-list">
                                        <div class="form-unit end-unit rd-pay-pos-fix" style="<%= (IsBankDeal ? "visibility: hidden;" : "" )%>">
                                            <label for="" class="unit-label title">
                                                總計</label>
                                            <div class="data-input">
                                                <p class="important shopping-list">
                                                    $<span>{{ subtotal }}</span>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-unit" style="<%= (IsBankDeal ? "visibility: hidden;" : "" )%>">
                                    <label for="" class="unit-label title">
                                        運費</label>
                                    <div class="data-input">
                                        <p>
                                            $<span id="cdCharge">{{ freightCharge }}</span></p>
                                    </div>
                                </div>
                                <%--紅利/購物金折抵--%>
                                <div id="confirmBonusDiscount">
                                    <div id="cDiscountPay" class="form-unit" v-if="discountAmount > 0" style="<%= (IsBankDeal ? "visibility: hidden;" : "" )%>">
                                        <label for="" class="unit-label">
                                            17Life折價券</label>
                                        <div class="data-input">
                                            <p>
                                                -$<span>{{ discountAmount }}</span>
                                            </p>
                                        </div>
                                    </div>
                                    <div id="cBcashPay" class="form-unit" v-if="bcash > 0">
                                        <label for="" class="unit-label">
                                            紅利金</label>
                                        <div class="data-input">
                                            <p>
                                                -$<span>{{ bcash }}</span>
                                            </p>
                                        </div>
                                    </div>
                                    <div id="cScashPay" class="form-unit" v-if="scash > 0">
                                        <label for="" class="unit-label">
                                            購物金</label>
                                        <div class="data-input">
                                            <p>
                                                -$<span>{{ scash }}</span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-unit" style="<%= (IsBankDeal ? "visibility: hidden;" : "" )%>">
                                    <label for="" class="unit-label title">
                                        支付總額</label>
                                    <div class="data-input">
                                        <p class="important">
                                            $<span>{{ pendingAmount }}</span>
                                        </p>
                                    </div>
                                </div>
                                <div id="cProductDeliveryTypeRow" class="form-unit" style="display:none">
                                    <label for="" class="unit-label title">
                                        取貨方式</label>
                                    <div class="data-input">
                                        <p>
                                            <span id="cProductDeliveryType"></span>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <%--購買人資訊--%>
                            <div id="tbBuyerInfoConfirm">
                                <div class="form-unit">
                                    <label for="" class="unit-label">
                                        購買人姓名</label>
                                    <div class="data-input">
                                        <p>
                                            <span id="buyerNameInfo"></span>
                                        </p>
                                    </div>
                                </div>
                                <div class="form-unit">
                                    <label for="" class="unit-label">
                                        手機</label>
                                    <div class="data-input">
                                        <p>
                                            <span id="buyerTel"></span>
                                        </p>
                                    </div>
                                </div>
                                <div id="ConfirmBuyerAddress" class="form-unit" style="display: none">
                                    <label for="" class="unit-label">
                                        地址</label>
                                    <div class="data-input">
                                        <p>
                                            <span id="BuyerAddressConfirm"></span>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <%--收件人資訊--%>
                            <asp:Panel ID="ReceiverInfoConfirmPanel" runat="server">
                                <div id="tbReceiverConfirm">
                                    <div class="form-unit">
                                        <label for="" class="unit-label">
                                            收件人姓名</label>
                                        <div class="data-input">
                                            <p>
                                                <span id="ConfirmReceiverName"></span>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="form-unit">
                                        <label for="" class="unit-label">
                                            聯絡電話</label>
                                        <div class="data-input">
                                            <p>
                                                <span id="ConfirmReceiverTel"></span>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="form-unit">
                                        <label for="" class="unit-label" id="productDeliveryUnitTitle">
                                            地址</label>
                                        <div class="data-input">
                                            <p>
                                                <span id="ConfirmReceiverAddress"></span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>
                            <%--購買方式--%>
                            <div id="divConfirmPayWay" class="form-unit orderDiv">
                                <label for="" class="unit-label title">
                                    購買方式</label>
                                <div class="data-input">
                                    <p>
                                        <span id="ConfirmPayWay"></span>                                        
                                        <br />
                                        <asp:Image id="imgMasterPassLogo" Width="150px" ImageAlign="AbsBottom" runat="server" Visible="false"/>
                                        <asp:Image id="imgWalletLogo" Width="150px" ImageAlign="AbsBottom" runat="server" Visible="false"/>
                                    </p>
                                </div>
                            </div>
                            <%--發票資訊--%>
                            <asp:PlaceHolder ID="phInvoiceConfirm" runat="server">
                                <div id="divInvoiceConfirm" runat="server" class="orderDiv">
                                    <div class="form-unit">
                                        <label for="" class="unit-label title">
                                            發票開立方式</label>
                                        <div class="data-input">
                                            <p>
                                                <span id="atmInvoice"></span>
                                            </p>
                                        </div>
                                        <%--個人電子發票(載具)--%>
                                        <div class="data-input Duplicate2_PhoneCarrier" style="display: none;">
                                            <p>
                                                手機條碼載具：<span id="confirmDuplicate2PhoneCarrier"></span>
                                            </p>
                                        </div>
                                        <div class="data-input Duplicate2_PersonalCertificate" style="display: none;">
                                            <p>
                                                自然人憑證載具：<span id="confirmDuplicate2PersonalCertificate"></span>
                                            </p>
                                        </div>
                                        <%--捐贈發票--%>
                                        <div class="data-input Duplicate2_Donation" style="display: none;">
                                            <p>
                                                愛心碼：<span id="confirmDuplicate2DonationCode"></span>
                                            </p>
                                        </div>
                                        <div class="data-input Duplicate2_Donation" style="display: none;">
                                            <p>
                                                捐贈單位：<span id="confirmDuplicate2DonationDesc"></span>
                                            </p>
                                        </div>
                                        <%--個人紙本發票(二聯式)/公司紙本發票(三聯式)--%>
                                        <div class="data-input Duplicate3_Paper" style="display: none;">
                                            <p>
                                                發票抬頭：<span id="confirmInvoiceTitle"></span>
                                            </p>
                                        </div>
                                        <div class="data-input Duplicate3_Paper" style="display: none;">
                                            <p>
                                                統一編號：<span id="confirmInvoiceNumber"></span>
                                            </p>
                                        </div>
                                        <div class="data-input Duplicate2_Paper" style="display: none;">
                                            <p>
                                                收 件 人：<span id="confirmInvoiceName"></span>
                                            </p>
                                        </div>
                                        <div class="data-input Duplicate2_Paper" style="display: none;">
                                            <p>
                                                發票地址：<span id="confirmInvoiceAddress"></span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </asp:PlaceHolder>
                            <%--收據資料--%>
                            <asp:PlaceHolder ID="phReceiptConfirm" runat="server">
                                <div id="divReceiptConfirm">
                                    <div class="form-unit">
                                        <label for="" class="unit-label title">
                                            收據資料</label>
                                        <div class="data-input">
                                            <p>
                                                <span id="receiptType"></span>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="form-unit">
                                        <label for="" class="unit-label">
                                            地址</label>
                                        <div class="data-input">
                                            <p>
                                                <span id="receiptAddress"></span>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="form-unit">
                                        <label for="" class="unit-label">
                                            統一編號</label>
                                        <div class="data-input">
                                            <p>
                                                <span id="receiptNumber"></span>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="form-unit">
                                        <label for="" class="unit-label">
                                            買受人</label>
                                        <div class="data-input">
                                            <p>
                                                <span id="receiptTitle"></span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </asp:PlaceHolder>
                        </div>
                        <%--注意事項--%>
                        <hr class="header_hr rd-payment-width" />
                        <div class="form-unit">
                            <p class="qa_note">
                                注意事項：<br />
                                ◆為了維護您的權益，請確認交易資料正確無誤後再送出。<br />
                                ◆<span style="font-weight:700">若有退貨需求，請確認且同意17Life的退貨原則，並接受我們代為處理發票作廢及電子折讓事宜。</span><br />
                                ◆若您開立三聯式發票，退貨時，請務必將紙本折讓單寄回。<br />
                            </p>
                        </div>
                        <%--送出訂單or修改資料--%>
                        <div class="grui-form">
                            <div class="form-unit end-unit rd-pay-pos-fix">
                                <div class="data-input rd-pay-span-ctr">
                                    <p class="cancel-mr">
                                        <asp:Panel ID="panApplePay" runat="server" Visible="false" style="display:inline;">
                                            <div id="btnForApplePay"></div>
                                        </asp:Panel>
                                        <asp:Button ID="btnPayWayProcess" runat="server" CssClass="btn btn-large btn-primary rd-payment-large-btn"
                                            Text="送出訂單" OnClick="btnPayWay_Click" 
                                            OnClientClick="if(checkopt()){block_postback(); new MemoryCart().empty();return true;}else{$.unblockUI();return false;}" />
                                        <input type="button" class="btn btn-large rd-payment-large-btn" value="修改資料" onclick="BackToEdit();" />
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <div id="discountMList" class="content-group payment" style="display: none;">
                    <%--M版折價券區塊--%>
                    <div class="message_box_MoneyTicket discount_box">
                        <div class="message_box_main">
                            <p class="message_box_main_title">請選擇折價券(共<span class="hint"><asp:Literal ID="liDiscountMCount" runat="server"></asp:Literal></span>張)</p>
                            <p class="cancel-mr">
                                <input type="button" class="btn btn-large rd-payment-large-btn btn_message_close_rwd" value="取消" onclick="CloseDiscount();">
                            </p>
                            <div class="clearfix"></div>
                            <hr class="header_hr  rd-payment-width">
                            <asp:Repeater ID="rptDiscountMList" runat="server">
                                <HeaderTemplate>
                                    <ul id="discountCodeMList" style="display: block;">
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <li id="<%# ((LunchKingSite.DataOrm.ViewDiscountDetail)Container.DataItem).Code %>" onclick="SelectDiscount(this);" style="cursor: pointer; position:relative;" data-amount="<%# Convert.ToString(((LunchKingSite.DataOrm.ViewDiscountDetail)Container.DataItem).MinimumAmount)%>">
                                        <div class="cover_m" style="">未達消費門檻</div>
                                        <a>
                                            <i class="fa fa-check fa-lg fa-fw"></i>折價券<span class="hint"><%# ((LunchKingSite.DataOrm.ViewDiscountDetail)Container.DataItem).Amount.Value.ToString("F0") %></span>元，
                                        <%# ((LunchKingSite.DataOrm.ViewDiscountDetail)Container.DataItem).MinimumAmount.HasValue ? "消費滿<span class='hint'>" + ((LunchKingSite.DataOrm.ViewDiscountDetail)Container.DataItem).MinimumAmount.Value.ToString("F0") + "</span>可使用。" : "無使用門檻。" %>
                                        <br />
                                        <span class="date"><%# ((LunchKingSite.DataOrm.ViewDiscountDetail)Container.DataItem).EndTime.Value.ToString("yyyy/MM/dd HH:mm") %>前有效</span>
                                        <p class="conform-m"><%# Helper.IsFlagSet(((LunchKingSite.DataOrm.ViewDiscountDetail)Container.DataItem).Flag, DiscountCampaignUsedFlags.CategoryLimit) ? "適用：" + PromotionFacade.GetDiscountCategoryString(((LunchKingSite.DataOrm.ViewDiscountDetail)Container.DataItem).Id) : string.Empty %></p>
                                                                                                                                                                                 </a></li>
                                </ItemTemplate>
                                <FooterTemplate></ul></FooterTemplate>
                            </asp:Repeater>
                            <p class="note">僅顯示可使用之折價券，其餘請至 會員中心 &gt; 折價券 查詢</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="Rightarea" class="rd-payment-qma">
            <div class="side_block">
                <div class="side_block_content">
                    <div class="side_block_title for-payment">
                        訂購Q&amp;A
                    </div>
                    <p class="qa_title">
                        <a href="/ppon/newbieguide.aspx?s=2&q=20621" target="_blank">Q：我可以使用哪些方式付款？
                        </a>
                    </p>
                    <p class="qa_content">
                        A：您可透過以下方式付費：<br />
                        1. 購物金<br />
                        2. 紅利金<br />
                        3. 折價券<br />
                        4. 信用卡/Visa金融卡<br />
                        5. ATM付款
                    </p>
                    <p class="qa_title">
                        <a href="javascript:void(0)">Q：我買的好康能給別人用嗎?
                        </a>
                    </p>
                    <p class="qa_content">
                        A：可以。因為我們的配合商家僅認憑證不認人，但一張憑證只能使用一次。
                    </p>
                    <p class="qa_title">
                        <a href="/ppon/newbieguide.aspx?s=3&q=116" target="_blank">Q：已購買的好康如何使用？
                        </a>
                    </p>
                    <p class="qa_content">
                        A：可至會員專區 → <a href="https://www.17life.com/User/coupon_List.aspx" target="_blank">訂單/憑證</a>
                        中，下載或列印憑證後，持憑證至所提供之商家使用。<br />
                        您也可以<a href="https://www.17life.com/ppon/promo.aspx?cn=app" target="_blank">下載APP</a>，使用電子憑證。
                    </p>
                    <p class="qa_content line">
                        -------------------------- <a href="https://www.17life.com/Ppon/NewbieGuide.aspx"
                            target="_blank">更多常見Q&A >>></a>
                    </p>
                    <p class="qa_note">
                        特別說明：<br />
                        目前本站僅會先要求授權，不會直接進行請款， 當此優惠跨越集購門檻時，本站才會實際進行請款。若集購失敗，將不會有任何款項的請款，請放心!
                    </p>
                </div>
            </div>
            <uc3:Paragraph ID="p165" runat="server" />
        </div>
    </asp:Panel>
    <%--信用卡付款--%>
    <asp:Panel ID="PanelCreditCradAPI" runat="server" Visible="false">
        <asp:HiddenField ID="hidInstallment" runat="server" Value="0" />
        <asp:HiddenField ID="hidCreditcardsAllowInstallment" runat="server" Value="" />
        <asp:HiddenField ID="hidIsTaishinCreditCardOnly" runat="server" Value="" />
        <asp:HiddenField ID="hidTaishinCreditcards" runat="server" Value="" />
        <div id="Left" class="rd-payment-Left-width">
            <div id="maincontent" class="clearfix">
                <div class="content-group payment">
                    <h1 class="rd-payment-h1 creditcard">信用卡付款</h1>
                    <hr class="header_hr rd-payment-width" />
                    <div class="grui-form">
                        <div class="form-unit" style="display: none;">
                            <label for="" class="unit-label title">
                                訂單編號</label>
                            <div class="data-input">
                                <p>
                                    <asp:Label ID="OrderNumber" runat="server" Text="Label"></asp:Label>
                                    <asp:TextBox ID="HiddenOrderNumber" runat="server" Visible="false"></asp:TextBox>
                                </p>
                            </div>
                        </div>
                        <div class="form-unit">
                            <label for="" class="unit-label title">
                                交易日期</label>
                            <div class="data-input">
                                <p>
                                    <asp:Label ID="TransDate" runat="server" Text="Label"></asp:Label>
                                </p>
                            </div>
                        </div>
                        <div class="form-unit">
                            <label for="" class="unit-label title">
                                交易金額</label>
                            <div class="data-input">
                                <p class="important">
                                    $<asp:Label ID="TransAmount" runat="server" Text="Label"></asp:Label>
                                </p>
                                <asp:TextBox ID="HiddenTransAmount" runat="server" Visible="false"></asp:TextBox>
                            </div>
                        </div>
                        <div id="CardNo" class="form-unit">
                            <label for="" class="unit-label title">
                                信用卡號</label>
                            <div class="data-input">
                                <%--<asp:TextBox ID="TextBox1" runat="server" class="input-credit cc" MaxLength="4" pattern="[0-9]*" OnClick="this.value = ''"></asp:TextBox>--%>
                                <asp:TextBox ID="CardNo1" runat="server" class="input-credit cc" MaxLength="4" ></asp:TextBox>
                                -
                                <asp:TextBox ID="CardNo2" runat="server" class="input-credit cc" MaxLength="4" ></asp:TextBox>
                                -
                                <asp:TextBox ID="CardNo3" runat="server" class="input-credit cc" MaxLength="4" ></asp:TextBox>
                                -
                                <asp:TextBox ID="CardNo4" runat="server" class="input-credit cc" MaxLength="4" ></asp:TextBox>
                                <%-- // 記憶信用卡號專案 START --%>
                                <asp:DropDownList ID="ddlSelectCreditCard" runat="server" CssClass="select-wauto cc">
                                </asp:DropDownList>
                                <p id="errMsg" class="if-error" style="display: none;">輸入錯誤</p>
                                <%-- // 記憶信用卡號專案 END --%>
                            </div>
                        </div>
                        <div class="form-unit" id="ExpDateRow">
                            <label for="" class="unit-label title">
                                有效期限</label>
                            <div class="data-input" id="ExpDate">
                                <asp:DropDownList ID="ExpMonth" runat="server" CssClass="select-wauto cc">
                                    <asp:ListItem Value="">--</asp:ListItem>
                                    <asp:ListItem Value="01">01</asp:ListItem>
                                    <asp:ListItem Value="02">02</asp:ListItem>
                                    <asp:ListItem Value="03">03</asp:ListItem>
                                    <asp:ListItem Value="04">04</asp:ListItem>
                                    <asp:ListItem Value="05">05</asp:ListItem>
                                    <asp:ListItem Value="06">06</asp:ListItem>
                                    <asp:ListItem Value="07">07</asp:ListItem>
                                    <asp:ListItem Value="08">08</asp:ListItem>
                                    <asp:ListItem Value="09">09</asp:ListItem>
                                    <asp:ListItem Value="10">10</asp:ListItem>
                                    <asp:ListItem Value="11">11</asp:ListItem>
                                    <asp:ListItem Value="12">12</asp:ListItem>
                                </asp:DropDownList>
                                <p class="credit">
                                    月/20
                                </p>
                                <asp:DropDownList ID="ExpYear" runat="server" CssClass="select-wauto">
                                </asp:DropDownList>
                                <p>
                                    年
                                </p><p class="if-error" style="margin-left:12px; display:none">信用卡有效期限錯誤</p>
                            </div>
                        </div>
                        <div class="form-unit" id="securityCodeRow">
                            <asp:Panel ID="pan_SecurityCode" runat="server">
                                <label for="" class="unit-label title">
                                    信用卡背面末三碼</label>
                                <div class="data-input">
                                    <asp:TextBox ID="SecurityCode" runat="server" 
                                        class="input-credit" MaxLength="3" pattern="[0-9]*" />
                                    <div class="safelycode">
                                    </div><p class="if-error" style="margin-left:12px; display:none">請輸入末三碼</p>
                                </div>
                            </asp:Panel>
                        </div>
                        <%-- // 記憶信用卡號專案 START --%>
                        <div class="form-unit" id="divCreditCardName">
                            <label for="" class="unit-label title">信用卡名稱</label>
                            <div class="data-input">
                                <asp:TextBox ID="txtCreditCardName" runat="server" class="input-credit" 
                                    placeholder="請填寫信用卡名稱(非必填)" style="width:160px" />
                            </div>
                        </div>
                        <div class="form-unit" id="divSaveCreditCardInfo">
                            <label class="unit-label title">&nbsp;</label>
                            <div class="data-input">
                                <asp:CheckBox ID="chkSaveCreditCardInfo" runat="server" Text=" 本人同意將信用卡資訊加密儲存於個人信用卡管理，<br>並僅供17Life購物結帳使用。" checked="true"/>
                            </div>
                        </div>
                        <%-- // 記憶信用卡號專案 END --%>
                        <div class="member">
                            <p class="info import_info member_buy_atm_first_import" style="display: block;">
                                ※為保護您的權益，持卡人需與會員帳戶相同，您同意17Life進行消費確認及保留出貨權益。
                            </p>
                        </div>
                    </div>
                    <div class="grui-form">
                        <div class="form-unit end-unit rd-pay-pos-fix">
                            <div class="data-input rd-pay-span-ctr">
                                <p class="cancel-mr">

                                    <asp:Button runat="server" ID="AuthSSL" Text="確認付款" CssClass="btn btn-large btn-primary rd-payment-xlarge-btn"
                                        OnClick="AuthSSL_Click" OnClientClick='if(checkAPIopt()){block_postback();return true;}else{return false;}' />
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-check-warning">
                    <p class="check-point">很抱歉!您的信用卡沒有提供分期服務喔!</p>
                    <p>
                        請確認發卡銀行及信用卡號，或另外選擇其他<br>
                        付款方式，謝謝!
                    </p>
                    <p class="text-center">
                        <input type="button" class="btn btn-primary rd-payment-xlarge-btn nomargin" id="pop-in-check-yes" value="確定" onclick="$.unblockUI()">
                    </p>
                </div>
            </div>
        </div>
        <div id="Rightarea" class="rd-payment-qma">
            <div class="side_block">
                <div class="side_block_content">
                    <div class="side_block_title for-payment">
                        17Life安心交易
                    </div>
                    <p>
                        您好，本網站(17Life)之刷卡機制係採用台新銀行之刷卡金流系統；台新銀行已通過<span class="safelytext">ISO 27001:2005 / BS
                            7799:2005國際資訊安全管理標準</span>驗證，可鑑別並進行有效管理各項資訊運用所潛在的威脅與風險。本網站將不會留存您填寫的各項資料，您的刷卡資料會經由<span
                                class="safelytext">SSL安全協定加密</span>傳輸至台新銀行，絕對安全無虞，您可以放心刷卡。
                    </p>
                    <br />
                    <p>
                        <img name="lpkilemp" height="55" width="115" border="0" src="https://smarticon.geotrust.com/smarticon?ref=www.17life.com"
                            alt="Click for company profile" oncontextmenu="return false;">
                    </p>
                    <br />
                    <p class="important">
                        ※目前暫不接受美國運通(American Express)卡，<br />
                        造成不便敬請見諒。
                    </p>
                </div>
            </div>
        </div>
    </asp:Panel>
    <asp:Panel ID="PanelPaySucessForApplePay" runat="server" Visible="false">
        
    </asp:Panel>
    <asp:Panel ID="PanelResult" runat="server" Visible="false">

        <asp:PlaceHolder ID="ph_RetargetingPayDone" Visible="false" runat="server">
            <div style="display: none">
                <%=OrderArgs%>
            </div>
        </asp:PlaceHolder>

        <div id="FULL" class="rd-payment-Left-width">
            <div id="maincontent" class="clearfix">
                <asp:Panel ID="pan_Success" runat="server" Visible="false">
                    <div class="content-group payment">
                        <asp:Panel ID="pan_Success_CreditCard" runat="server">
                            <div class="pay_success">
                            </div>
                            <h1 class="success">恭喜您，交易成功！</h1>
                            <hr class="header_hr rd-payment-width" />
                            <asp:MultiView ID="mvSuccessResult0" runat="server" EnableViewState="false">
                                <asp:View runat="server">
                                    <div class="member">
                                        <p class="info">
                                            <asp:Label ID="ItemName" runat="server"></asp:Label>
                                            您隨時可至 會員中心 &gt; <a href="<%= ResolveUrl("~/User/coupon_List.aspx") %>">訂單/憑證</a> 查詢訂單明細、付款金額、發票，以及<%= BuySuccessText%>。
                                        </p>
                                    </div>
                                </asp:View>
                                <asp:View runat="server">
                                    <div class="member">
                                        <p class="info">
                                            17Life將自動發送兌換券簡訊至您的手機及 email 中，宅配商品將發送訂單明細至您的email。
                                        <br>
                                            欲查詢憑證狀態，可隨時至 客服中心 &gt; 其他服務 &gt;
                                        <a href="/Ppon/NewbieGuide.aspx?target=record" target="_blank">購買紀錄查詢</a>
                                        </p>
                                    </div>
                                </asp:View>
                            </asp:MultiView>
                        </asp:Panel>
                        <asp:Panel ID="pan_Success_ATM" runat="server">
                            <div class="pay_success">
                            </div>
                            <h1 class="success">訂購資訊</h1>
                            <hr class="header_hr rd-payment-width" />
                            <p class="info">
                                <span id="ItemNameATM">感謝您的訂購，訂單將於付款完成後成立<br />
                                </span>
                            </p>
                            <div class="grui-form">
                                <div class="form-unit">
                                    <div style="min-width:80px">
                                        訂單編號</div>
                                    <div>
                                        <asp:Literal id="litOrderNo" runat="server" />
                                    </div>
                                </div>
								<div class="form-unit">
									<div style="min-width: 80px">商品</div>
									<div>
										<asp:Literal id="litItemNameATM" runat="server" />
									</div>
								</div>
								<div class="form-unit">
                                    <div style="min-width:80px">
                                        銀行代碼</div>
                                    <div>822 (中國信託商業銀行)</div>
                                </div>
                                <div class="form-unit">
                                    <div style="min-width:80px">
                                        銀行帳號</div>
                                    <div>
                                        <asp:Label ID="ATMOrderAccount" runat="server"></asp:Label>
                                    </div>
                                </div>
                                <div class="form-unit">
                                    <div style="min-width:80px">
                                        繳費金額</div>
                                    <div>
										<asp:Label ID="ATMOrderTotal" runat="server" />
                                    </div>
                                </div>
                                <div class="form-unit">
                                    <div style="min-width:80px">
                                        付款期限</div>
                                    <div>
										<asp:Label ID="ATMOrderDate" runat="server" />23:59:59
                                    </div>
                                </div>
                                <div class="form-unit">
                                    <div style="min-width:80px">&nbsp;</div>
                                    <div>
                                        <p class="list-warning-b">
                                            若您超過這個時間付款，ATM會顯示交易失敗，您的訂單將被取消。
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </asp:Panel>
                        <%--行銷滿額贈活動--%>
                        <asp:Panel ID="divDiscountPromotion" runat="server" CssClass="fd_success_sn pay-evt-bg"
                            Visible="false">
                            <p class="pay-evt">
                                已獲得<span class="s-imp"><asp:Literal ID="liDiscountPromotionCount" runat="server"></asp:Literal></span>次<br>
                                抽刮刮樂機會，總獎金超過億元<br>
                                還差<span class="s-imp"><asp:Literal ID="liDiscountPromotion" runat="server"></asp:Literal></span>元即可再多一次
                                <a href="<%= ResolveUrl("~" + config.SpecialConsumingDiscountUrl) %>"
                                    target="_blank">活動詳情</a>
                            </p>
                        </asp:Panel>
                        <%--折價券活動--%>
                        <asp:Panel ID="divDiscountCode" runat="server" CssClass="fd_success_sn"
                            Visible="false">
                            <p class="pay-evt">
                                恭喜您，該筆訂單可獲得"買多少送多少折價券回饋"，<br />
                                將匯入您的帳戶。折價券回饋明細，請按<a href="<%= ResolveUrl("~/User/DiscountList.aspx") %>"
                                    target="_blank">折價券</a>查詢
                            </p>
                        </asp:Panel>
                        <asp:MultiView ID="mvSuccessMailHint" runat="server" EnableViewState="false">
                            <asp:View runat="server">
                                <%--ATM+會員+首購--%>
                                <div class="member">
                                    <p class="info import_info">
                                        感謝您首次購買17Life 好康！已贈送您500元折價券，<br>
                                        請至會員中心 &gt; 折價券 查詢，歡迎多加使用喔！
                                    </p>
                                </div>
                            </asp:View>
                            <asp:View runat="server">
                                <%--ATM+會員+非首購--%>
                            </asp:View>
                            <asp:View runat="server">
                                <%--ATM+訪客+首購--%>
                                <div class="member">
                                    <!--ATM付款_首購 請顯示-->
                                    <p class="info import_info member_buy_atm_first_import" style="display: block;">
                                        已發送認證信至您的E-mail：<asp:Literal ID="litSuccessEmail2" runat="server" />，
                                        請前往收信認證並設定密碼，即可登入會員中心，查看訂單/憑證等資訊，此筆訂單匯款完成後，
                                        我們將送您500元首購折價券！需登入會員才可使用，請盡早完成認證喔！
                                    </p>
                                </div>
                            </asp:View>
                            <asp:View runat="server">
                                <%--ATM+訪客+非首購--%>
                                <div class="member">
                                    <!--ATM付款_非首購 請顯示-->
                                    <p class="info import_info member_buy_atm_nonfirst_import" style="display: block;">
                                        已發送認證信至您的E-mail：<asp:Literal ID="litSuccessEmail3" runat="server" />，請前往收信認證並設定密碼，即可登入會員中心，查看訂單/憑證等資訊，並可使用折價券！
                                    </p>
                                </div>
                            </asp:View>
                            <asp:View runat="server">
                                <%--CreditCard+會員+首購--%>
                                <div class="member">
                                    <p class="info import_info">
                                        感謝您首次購買17Life好康！已贈送您500元折價券，請至 會員中心 &gt; 折價券 查詢，歡迎多加使用喔！
                                    </p>
                                </div>
                            </asp:View>
                            <asp:View runat="server">
                                <%--CreditCard+會員+非首購--%>
                            </asp:View>
                            <asp:View runat="server">
                                <%--CreditCard+訪客+首購--%>
                                <div class="member">
                                    <p class="info import_info member_buy_atm_nonfirst_import" style="display: block;">
                                        已發送認證信至您的E-mail：<asp:Literal ID="litSuccessEmail6" runat="server" />，請前往收信認證並設定密碼，即可登入會員中心，查看訂單/憑證等資訊，並可使用折價券！
                                    </p>
                                </div>
                            </asp:View>
                            <asp:View runat="server">
                                <%--CreditCard+訪客+非首購--%>
                                <div class="member">
                                    <p class="info import_info member_buy_atm_nonfirst_import" style="display: block;">
                                        已發送認證信至您的E-mail：<asp:Literal ID="litSuccessEmail7" runat="server" />，請前往收信認證並設定密碼，即可登入會員中心，查看訂單/憑證等資訊，並可使用折價券！
                                    </p>
                                </div>
                            </asp:View>
                        </asp:MultiView>
                        <asp:PlaceHolder ID="phGuestShowFirstBuyDiscountImage" runat="server">
                            <div class="member">
                                <p class="info">
                                    <span class="hint">首購折價券有使用效期，請盡早完成認證才可使用喔！</span>
                                </p>
                                <div class="fd_success_sn fistbuy-bg">
                                    <a href="<%=ResolveUrl("~/Ppon/NewbieGuide.aspx?s=5&q=20510#anc20510") %>" target="_blank">
                                        <img src="../Themes/PCweb/images/firstbuy_610x340.jpg"></a>
                                </div>
                            </div>
                        </asp:PlaceHolder>
                        <asp:MultiView ID="mvSuccessButtonGroup" runat="server">
                            <asp:View runat="server">
                                <p class="info btn-center">
                                    <asp:HiddenField ID="hidBid" runat="server" EnableViewState="false" />
                                    <asp:HiddenField ID="hidUserId" runat="server" EnableViewState="false" />
                                    <input type="button" class="btn btn-large rd-payment-xlarge-btn rd-dis-s btnShare" value="分享送500元">
                                    <input type="button" class="btn btn-large btn-primary rd-payment-xlarge-btn btnCouponList" value="查詢訂單">
                                </p>
                            </asp:View>
                            <asp:View runat="server">
                                <p class="info btn-center">
                                    <asp:HiddenField ID="hidAuthEmail" runat="server" EnableViewState="false" />
                                    <input type="button" class="btn btn-large btn-primary rd-payment-xlarge-btn btnAuthMail" value="重發認證信" />
                                </p>
                            </asp:View>
                        </asp:MultiView>

                        <%-- BuyAdBanner --%>
                        <asp:Panel ID="plBuyAdBanner" runat="server" CssClass="info-acPic" Visible="false">
                            <ucR:RandomParagraph ID="buyadbanner" runat="server" OnInit="RandomBuyAdInit" />
                        </asp:Panel>


                    </div>

                    <div id="Referral" style="display: none">
                        <div id="ReturnClose" style="cursor: pointer; margin-top: 3px; margin-right: 3px"
                            onclick="$.unblockUI();return false;">
                        </div>
                        <div id="ReferralContent">
                            <div class="Referralinvite">
                                <h1>邀請親友好康有獎</h1>
                            </div>
                            <div id="ReferralTitle">
                                <span class="icon-smile-o-2"></span>
                                <p>以下是您的專屬邀請連結</p>
                            </div>
                            <!--ReferralTitle-->
                            <div id="ReferralLinkarea">
                                <asp:TextBox ID="tbx_ReferralShare" runat="server" ReadOnly="true"></asp:TextBox>
                            </div>
                            <div id="ReferralStep" style="text-align: left;">
                                <li>
                                    <span class="list-step">步驟1</span>
                                    複製上面的邀請連結
                                </li>
                                <li>
                                    <span class="list-step">步驟2</span>
                                    把連結傳給好友
                                </li>
                                <li>
                                    <span class="list-step">步驟3</span>
                                    親友點連結後完成購買並核銷，宅配商品過鑑賞期且無退貨
                                </li>
                                <li>
                                    <span class="list-step">步驟4</span>
                                    您拿到推薦獎勵，折價券500元
                                    <span class="list-warning">(請注意使用期限)</span>
                                </li>
                            </div>

                            <div id="Referralexplain">
                                <li>您可使用下圖fb分享，或是複製分享連結並轉貼，</li>
                                <li>
                                    <span class="list-point">邀請次數無上限～</span>
                                    分享越多賺越多喔！
                                </li>
                                <li>
                                    <span class="list-point">訂單金額50元(含)以下恕不適用此折價券贈送活動，詳見<a target="_blank" href="<%=ResolveUrl("~/Ppon/NewbieGuide.aspx?id=share")%>">常見問題</a>
                                    </span>
                                </li>
                            </div>

                            <div id="referral_FB_area">
                                <a onclick="fbShareWindow = window.open(this.href, 'fbShareWindow', 'width=630,height=360','left=' + fbJumpWindowLeft + ',top=' + fbJumpWindowTop);BuyShareFB();" style="cursor: pointer;">
                                    <img src="<%= ResolveUrl("~/Themes/PCweb/images/FB_Btn.png")%>" />
                                </a>
                            </div>
                        </div>
                    </div>




                </asp:Panel>
                <asp:Panel ID="pan_Fail" runat="server" Visible="false">
                    <div class="content-group payment">
                        <div class="pay_failed">
                        </div>
                        <h1 class="faild">很抱歉，交易失敗！</h1>
                        <hr class="header_hr rd-payment-width" />
                        <p id="CreditcardFailInfo" class="info" runat="server" visible="false">
                            本次交易失敗可能是因為 <span class="important">刷卡授權失敗</span> 所造成。<br />
                            請確實使用本人信用卡，或洽客服人員處理。<br />
                            若您重新確認刷卡資料後仍無法成功授權交易，請聯絡您的發卡銀行！
                        </p>
                        <p id="PcashFailInfo" class="info" runat="server" visible="false">
                            本次交易失敗可能是因為 <span class="important">金額扣抵失敗（購物金或紅利金）</span>
                            <br />
                            所造成。 您可嘗試重新購買扣抵，或改以其他付款方式購買。
                        </p>
                        <p id="FailInfo" class="info" runat="server" visible="false">
                            本次交易失敗可能是因為 <span class="important">金額扣抵失敗</span> 或 <span class="important">刷卡授權失敗</span> 或 <span class="important">商品庫存不足</span>
                            所造成。<br />
                        </p>
                        <p id="ThirdPartyPayFailInfo" class="info" runat="server" visible="false">
                            本次交易失敗可能是因為 <span class="important" id="thirdPartyPayFailReason" runat="server"></span><br />
                            <asp:Literal ID="litThirdPartyPayFailMessage" runat="server" />
                        </p>
                        <p id="FamiPincodeFailInfo" class="info" runat="server" visible="false">
                            本次交易失敗可能是因為購買過程中系統異常所造成，請稍後再試或洽客服人員處理。謝謝。
                        </p>
                        <p class="info">
                            若有其他問題，也歡迎來信<a href="<%=config.SiteUrl + config.SiteServiceUrl %>" target="_blank">17Life客服中心</a>。
                        </p>
                        <p class="info btn-center">
                            <asp:HyperLink ID="hyp_Fail" runat="server">
                                <input id="Button3" type="button" class="btn btn-large btn-primary rd-payment-xlarge-btn" value="重新購買" />
                            </asp:HyperLink>
                        </p>
                    </div>
                </asp:Panel>
            </div>
        </div>


        <!--分享送50 -->
        <div class="share m-share">
            <div class="clearfix mar-center">
                <div class="share_text">
                    分享送<span class="bigtext">500</span>元
                </div>
                <div class="share_btn_box"></div>
                <a onclick='return Ppon.module.OpenSharePopWindow("<%=ResolveUrl("~/ppon/fbshare_proxy.aspx?type=facebook&bid=") + BusinessHourGuid%>", "fb");' class="m-fbBtn"></a>
                <a onclick='return Ppon.module.OpenSharePopWindow("<%=ResolveUrl("~/ppon/fbshare_proxy.aspx?type=line&bid=") + BusinessHourGuid + "&itemname=" + HttpUtility.UrlEncode(AppTitle)%>", "line");' class="m-lineBtn"></a>
            </div>
            <div class="share-tip">
                親友用你分享的連結購買成功，就送你500元！
                <a target="_blank" href="<%=ResolveUrl("~/Ppon/NewbieGuide.aspx?id=share")%>">詳見常見問題</a>
            </div>
        </div>

        <div class="clear"></div>
        <!--分享送50 End-->

        <%--相關好康推薦--%>
        <div id="section-1" class="clearfix">            
            <h3 class="local-title">相關好康推薦<img id="loadingRelateDeals" src="../Themes/PCweb/images/spinner.gif" style="display: none" /></h3>
            <div id="relateDeals"></div>
            <input id="relateDealsIndex" type="hidden" value="0" />
            <div id="relateDealsTemplate" class="evn-cop-buy-sf-box e_shadow" style="cursor: pointer;display: none;">
                <div class="evn-cop-buy-pic">
                    <a class="dealLink">
                        <img class="relateDealImg" style="border-width: 0px;" /></a>
                    <div class="evn-cop-buy-sold-out-bar-290"></div>
                </div>
                <div class="evn-cop-all-right">
                    <div class="evn-cop-buytitle e_textFamily">
                        <a class="dealLinkDealTitle"></a>
                    </div>
                    <div class="evn-cop-minorange-title">
                        <a class="dealLinkEventTitle"></a>
                    </div>
                    <div class="evn-cop-buyinformation">
                        <div class="evn-cop-discount"></div>
                        <div class="evn-cop-buytotle"></div>
                        <a class="dealLink">
                            <div class="st-e-cop-buybtn e_rounded">馬上買</div>
                        </a>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>

        <%--sidedeal--%>
    </asp:Panel>

    <asp:PlaceHolder ID="ph_PaySuccess" Visible="false" runat="server">
        <script src="<%=ResolveUrl("~/Tools/js/ppon.pay_success.js")%>" type="text/javascript"></script>
    </asp:PlaceHolder>
    
    <asp:PlaceHolder ID="ph_MasterPassLightbox" Visible="false" runat="server">
        <script type="text/javascript" language="Javascript">
            if($("#ddlMasterPassCards").val()){
                MasterPass.client.checkout({
                    "requestToken": $("input[id$=hidMPRequestToken]").val(),
                    "callbackUrl": $("input[id$=hidMPCallbackUrl]").val(),
                    "merchantCheckoutId": $("input[id$=hidMPMerchantCheckoutId]").val(),
                    "allowedCardTypes": $("input[id$=hidMPAcceptedCards]").val(),
                    "cardId":$("#ddlMasterPassCards option:selected").val().split(',')[0],
                    "shippingId":0,
                    "precheckoutTransactionId":$("input[id$=hidMPPrecheckoutTransactionId]").val(),
                    "suppressShippingAddressEnable": $("input[id$=hidMPShippingSuppression]").val().toLowerCase(),
                    "requestBasicCheckout" : $("input[id$=hidMPAuthLevelBasic]").val().toLowerCase(),
                    "walletName" : $("input[id$=hidMPWalletName]").val(),
                    "consumerWalletId" : $("input[id$=hidMPConsumerWalletId]").val(),
                    "loyaltyEnabled" : $("input[id$=hidMPRewardsProgram]").val().toLowerCase(),
                    "version": "v6"
                });
            } else {
                MasterPass.client.checkout({
                    "callbackUrl":$("input[id$=hidMPCallbackUrl]").val(),
                    "pairingRequestToken":$("input[id$=hidMPPairingRequestToken]").val(),
                    "requestToken": $("input[id$=hidMPRequestToken]").val(),
                    "requestedDataTypes": $("input[id$=hidMPRequestedDataTypes]").val(),
                    "merchantCheckoutId": $("input[id$=hidMPMerchantCheckoutId]").val(),
                    "allowedCardTypes":  $("input[id$=hidMPAcceptedCards]").val(),
                    "suppressShippingAddressEnable": $("input[id$=hidMPShippingSuppression]").val().toLowerCase() === "true",
                    "loyaltyEnabled" : $("input[id$=hidMPRewardsProgram]").val().toLowerCase(),
                    "requestPairing":false,
                    "requestBasicCheckout" : $("input[id$=hidMPAuthLevelBasic]").val().toLowerCase(),
                    "version" : "v6"
                });
            }
        </script>
    </asp:PlaceHolder>

    <asp:PlaceHolder ID="ph_relatedDeals" Visible="false" runat="server">
        <script type="text/javascript" language="Javascript">
            $(document).ready(function () {


                var relatedDealsLoaded = false;
                if ($("#section-1").length > 0) {
                    if (!relatedDealsLoaded) {
                        relatedDealsLoaded = true;
                        var bid = buyData.dealId;
                        $("#loadingRelateDeals").show();

                        //$.dnajax("/service/ppon/GetRelatedDeals", "{bid:'" + bid + "', categoryId:0, workCityId:0, travelCategoryId: 0, femaleCategoryId:0, filterCategoryIdList:[]}", function (res) {
                        //    if (res.Code != window.ApiSuccessCode) {
                        //        relatedDealsLoaded = false;
                        //        return;
                        //    }
                        
                        //    bindRelatedDeals($.parseJSON(res.Data));
                        //    $("#loadingRelateDeals").hide();                        
                        //});
                        $.dnajax("/service/ppon/GetRelatedDeals", "{bid:'" + bid + "'}", function (res) {
                            if (res.Code != window.ApiSuccessCode) {
                                relatedDealsLoaded = false;
                                return;
                            }

                            bindRelatedDeals($.parseJSON(res.Data));
                            $("#loadingRelateDeals").hide();                        
                        });

                    }
                }

                //將相關檔次填入列表
                function bindRelatedDeals(jsonData) {
                    var deals = $.parseJSON(jsonData);
                    $("#relateDeals").html("");
                    var maxRecord = 6;
                    if (deals.RelatedDeals.length < maxRecord) {
                        maxRecord = deals.RelatedDeals.length;
                    }
                    $("#relateDealsIndex").val(maxRecord);
                    for (var i = 0; i < maxRecord; i++) {
                        $('#relateDeals').append(getRelatedDealHtml(i, maxRecord, deals.RelatedDeals[i]));
                    }
                    
                    $("img.relateDealLazy" + maxRecord).lazyload({ threshold: 500, effect: "fadeIn" });
                }

                //Render相關檔次列表Html
                function getRelatedDealHtml(index, maxRecord, deal) {

                    var dealTemp = $("#relateDealsTemplate").clone();

                    dealTemp.attr("id", "relateDeals" + index);
                    dealTemp.css('display', '');
                    dealTemp.attr("onclick", "location.href='" + deal.RecommendLink + "'");
                    dealTemp.find('.dealLink').attr('href', deal.RecommendLink);
                    dealTemp.find('.dealLinkDealTitle').attr('href', deal.RecommendLink);
                    dealTemp.find('.dealLinkDealTitle').html(deal.DealTitle);
                    dealTemp.find('.dealLinkEventTitle').attr('href', deal.RecommendLink);
                    dealTemp.find('.dealLinkEventTitle').html(deal.EventTitle);
                    dealTemp.find('.relateDealImg').attr('data-original', deal.DealImageUrl);
                    dealTemp.find('.relateDealImg').addClass("relateDealLazy" + maxRecord);
                    dealTemp.find('.evn-cop-discount').html(deal.SaleMessage);
                    dealTemp.find('.evn-cop-buytotle').html(deal.ItemPrice);

                    return dealTemp;
                }
            });
        </script>
    </asp:PlaceHolder>

    <div class="tk_jump_alert">
        <i class="fa fa-times-circle fa-2x tk_jump_close"></i>
        <h1>台新儲值支付串接會員條款</h1>
        <hr class="header_hr  rd-payment-width">
        <ul>
            <li>​
                <ol style="list-style-type: decimal;">
                    <li>本服務由台新銀行提供，各項法規及注意事項請詳見台新銀行網站。<br /><br /></li>
                    <li>綁定台新銀行儲值支付帳戶之流程，17Life會將您的基本資料提供台新銀行，作為綁定儲值支付帳戶約定付款支付工具的身分驗證使用，申請本服務之成功與否，概依台新銀行驗證結果為定，如有任何問題請洽台新銀行24小時客服專線：(02)2655-3355。<br /><br /></li>
                    <li>您使用台新銀行儲值支付帳戶付款時，如發生交易失敗，但已扣除款項等情形，17Life將於2~5個工作天內全額退款至您的儲值支付帳戶，請您稍候，此外，您可登入台新銀行儲值支付平台查詢交易明細。<br /><br /></li>
                </ol>
            </li>
            
            <li class="btn-center">
                <br />
                <br />
                <input type="submit" class="btn btn-large btn-primary rd-payment-xlarge-btn tk_jump_close_and_go" value="同意並前往綁定" >
            </li>
        </ul>
    </div>
</asp:Content>


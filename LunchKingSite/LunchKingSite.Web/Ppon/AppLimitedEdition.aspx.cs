﻿using System;
using System.Linq;
using System.Web;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Views;
using LunchKingSite.WebLib.Presenters;

namespace LunchKingSite.Web.Ppon
{
    public partial class AppLimitedEdition : BasePage, IAppLimitedEdition
    {
        #region Props

        private AppLimitedEditionPresenter _presenter;

        public AppLimitedEditionPresenter Presenter
        {
            set
            {
                _presenter = value;
                if (value != null)
                {
                    _presenter.View = this;
                }
            }
            get { return _presenter; }
        }

        public Guid BusinessHourGuid
        {
            get 
            { 
                Guid bid;
                if (Page.RouteData.Values["bid"] != null)
                {
                    Guid.TryParse(Page.RouteData.Values["bid"].ToString(), out bid);
                }
                else if (!string.IsNullOrWhiteSpace(Request.QueryString["bid"]))
                {
                    Guid.TryParse(Request.QueryString["bid"], out bid);
                }
                else
                {
                    bid = Guid.Empty;
                }
                return bid;
            }
        }

        public string AppTitle { set; get; }
        public string PicAlt { get; set; }
        public bool IsAppLimitedEditionDeal { set; get; }

        /// <summary>
        /// meta og:title content
        /// </summary>
        public string OgTitle { set; get; }
        /// <summary>
        /// meta og:url content
        /// </summary>
        public string OgUrl { set; get; }
        /// <summary>
        /// meta og:description content
        /// </summary>
        public string OgDescription { set; get; }
        /// <summary>
        /// meta og:image content
        /// </summary>
        public string OgImage { set; get; }
        /// <summary>
        /// link rel=canonical href
        /// </summary>
        public string LinkCanonicalUrl { set; get; }
        /// <summary>
        /// link rel=image_src href
        /// </summary>
        public string LinkImageSrc { set; get; }

        protected static ISysConfProvider SystemConfig = ProviderFactory.Instance().GetConfig();

        #endregion Props

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Presenter.OnViewInitialized();
            }
            Presenter.OnViewLoaded();
        }

        public void RenderViewContent(IViewPponDeal vpd)
        {
            
            #region SEO
            
            var dealTinyurl = new Uri(HttpContext.Current.Request.Url.ToString());
            OgTitle = vpd.CouponUsage;
            OgUrl = dealTinyurl.AbsoluteUri;
            OgImage = ImageFacade.GetMediaPathsFromRawData(vpd.EventImagePath, MediaType.PponDealPhoto).DefaultIfEmpty(@"https://www.17life.com/Themes/default/images/17Life/G2/ppon-M1_pic.jpg").First();
            OgDescription = vpd.EventName;
            LinkImageSrc = (String.IsNullOrWhiteSpace(vpd.EventImagePath) ? string.Empty : ImageFacade.GetMediaPathsFromRawData(vpd.EventImagePath, MediaType.PponDealPhoto)[0]);
            LinkCanonicalUrl = dealTinyurl.AbsoluteUri;
            Page.MetaDescription = string.IsNullOrWhiteSpace(vpd.PageDesc) ? string.Format("{0}-{1}", vpd.CouponUsage, vpd.EventTitle) : vpd.PageDesc;
            Page.Title = string.IsNullOrWhiteSpace(vpd.PageTitle) ? string.Format("{0}-{1}，{2}", vpd.CouponUsage, vpd.EventTitle, SystemConfig.Title) : vpd.PageTitle;
            AppTitle = string.IsNullOrEmpty(vpd.AppTitle) ? vpd.CouponUsage : vpd.AppTitle;
            PicAlt = string.IsNullOrWhiteSpace(vpd.PicAlt) ? AppTitle : vpd.PicAlt;
            Page.MetaKeywords = string.Format("{0},{1}", string.IsNullOrEmpty(PicAlt) ? string.Empty : PicAlt.Replace("/", ","), Page.MetaKeywords);

            #endregion SEO

            //大圖輪播
            rpt_tp.DataSource = decimal.Equals(0, vpd.ItemPrice) ? ImageFacade.GetMediaPathsFromRawData(vpd.EventImagePath, MediaType.PponDealPhoto).Where((x, i) => i == 0) : ImageFacade.GetMediaPathsFromRawData(vpd.EventImagePath, MediaType.PponDealPhoto).Where((x, i) => i != 1 && i != 2);
            rpt_tp.DataBind();
            //商品主題活動行銷訊息活動 Logo
            lit_DealPromoImageMainContent.Text = (string.IsNullOrEmpty(vpd.DealPromoImage)) ? string.Empty : string.Format(@"<img src='{0}' alt='' />"
                , ImageFacade.GetMediaPath(vpd.DealPromoImage, MediaType.DealPromoImage));
        }

        public void RedirectToDefault(string siteUrl, Guid bid = new Guid())
        {
            Response.Redirect(bid == Guid.Empty ? string.Format("{0}", siteUrl) : string.Format("{0}/{1}", siteUrl, bid));
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Net.Mail;
using System.Text;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.WebLib;
using LunchKingSite.WebLib.Component;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.UI;
using LunchKingSite.WebLib.Views;
using log4net;

namespace LunchKingSite.Web.Ppon
{
    public partial class ContactUs : BasePage, IContactUsView
    {
        #region Props
        private static readonly ILog log = LogManager.GetLogger("MailLog");
        private ContactUsPresenter _presenter;
        public ContactUsPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        public DateTime Now
        {
            get
            {
                return DateTime.Now;
            }
        }

        /// <summary>
        /// 由master page取得的cityid
        /// </summary>
        public int CityId
        {
            get
            {
                return Master.SelectedCityId;
            }
            set
            {
                Master.SelectedPponCity = PponCityGroup.DefaultPponCityGroup.GetPponCityByCityId(value);
            }
        }

        public int CategoryID
        {
            get
            {
                string cat_querystring = string.Empty;
                int cat;
                if (Page.RouteData.Values["cat"] != null)
                {
                    cat_querystring = Page.RouteData.Values["cat"].ToString();
                }
                else if (!string.IsNullOrWhiteSpace(Request.QueryString["cat"]))
                {
                    cat_querystring = Request.QueryString["cat"];
                }

                if (!string.IsNullOrWhiteSpace(cat_querystring) && int.TryParse(cat_querystring, out cat))
                {
                    return cat;
                }
                else
                {
                    return -1;
                }
            }
        }

        public int TravelCategoryId
        {
            get
            {
                HttpCookie cookie = Request.Cookies.Get(LkSiteCookie.TravelCategoryId.ToString("g"));

                if (cookie == null)
                {
                    cookie = CookieManager.NewCookie(
                        LkSiteCookie.TravelCategoryId.ToString("g"),
                        ViewPponDealManager.DefaultManager.TravelDefaultCategoryId.ToString(),
                        DateTime.Now.AddDays(1)
                    );
                    Response.Cookies.Add(cookie);
                }

                int cid = int.TryParse(cookie.Value, out cid) ? cid : ViewPponDealManager.DefaultManager.TravelDefaultCategoryId;
                return cid == 0 ? ViewPponDealManager.DefaultManager.TravelDefaultCategoryId : cid;
            }
        }

        private bool _isKindDeal;

        public bool IsKindDeal
        {
            get
            {
                return _isKindDeal;
            }
            set
            {
                _isKindDeal = value;
            }
        }

        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();

        public static ISysConfProvider SystemConfig
        {
            get
            {
                return config;
            }
        }

        #endregion Props

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack) 
            {
                this.Presenter.OnViewInitialized();
            }
            
            this.Presenter.OnViewLoaded();
        }

        private static bool SendMail(string subject, string companyName, string contentHtml, out string resultMessage, int contactType) 
        {
            ISysConfProvider conf = ProviderFactory.Instance().GetConfig();
            try 
            {
                using (MailMessage msg = new MailMessage())
                {
                    StringBuilder sb = new StringBuilder();

                    sb.AppendLine("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">");
                    sb.AppendLine("<html xmlns=\"http://www.w3.org/1999/xhtml\"><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" /><title>");
                    sb.AppendLine("招商信件通知</title></head><body>");

                    msg.From = new MailAddress(conf.PponServiceEmail, companyName);

                    if (contactType == (int)ContactType.BusinessMail)
                    {
                        msg.To.Add(new MailAddress(conf.SolicitBusinessEmail, conf.ServiceName));
                    }
                    else if (contactType == (int)ContactType.MarketMail)
                    {
                        msg.To.Add(new MailAddress(conf.SolicitMarketEmail, conf.ServiceName));
                    }
                    else if (contactType == (int)ContactType.KindMail)
                    {
                        msg.To.Add(new MailAddress(conf.SolicitKindEmail, conf.ServiceName));
                    }

                    sb.AppendLine("<h2>" + subject + "</h2>");
                    sb.Append(contentHtml);
                    sb.AppendLine("</body></html>");

                    msg.Subject = subject;
                    msg.Body = sb.ToString();
                    sb.Clear();
                    msg.IsBodyHtml = true;
                    PostMan.Instance().Send(msg, SendPriorityType.Immediate);
                    resultMessage = "Send Success";
                }
            }
            catch (Exception ex) 
            {
                log.InfoFormat("招商專區({0})->{1}:{2}, {3}", subject, companyName, ex.Message, ex.StackTrace);
                resultMessage = ex.Message;
                return false;
            }
            return true;
        }

        #region Method

        public void SetTopSalesDeal(List<IViewPponDeal> vpd)
        {
            rpt_Deals.DataSource = vpd;
            rpt_Deals.DataBind();
        }

        protected void rpt_Deals_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is ViewPponDeal)
            {
                int index = e.Item.ItemIndex;
                ViewPponDeal deal = (ViewPponDeal)e.Item.DataItem;
                Literal clT = (Literal)e.Item.FindControl("lT");
                Literal clT2 = (Literal)e.Item.FindControl("lT2");
                Literal discount_2 = (Literal)e.Item.FindControl("lit_Discount_2");
                Image imgMainPic = (Image)e.Item.FindControl("imgMainPic");
                Literal dealPromoImage = (Literal)e.Item.FindControl("lit_DealPromoImage");
                Literal clit_scriptcountdown = (Literal)e.Item.FindControl("lit_scriptcountdown");
                
                _isKindDeal = Helper.IsFlagSet(deal.GroupOrderStatus ?? 0, GroupOrderStatus.KindDeal);
                clit_scriptcountdown.Text = "<div data-year='" + deal.BusinessHourOrderTimeE.Year + "'></div>";
                //貼上檔次 Icon 標籤

                if (e.Item.ItemIndex < 3)
                {
                    imgMainPic.ImageUrl = ImageFacade.GetMediaPathsFromRawData(deal.EventImagePath, MediaType.PponDealPhoto).DefaultIfEmpty(SystemConfig.SiteUrl + "/Themes/PCweb/images/ppon-M1_pic.jpg").First();
                }

                clT2.Text = (int)Math.Floor((deal.BusinessHourOrderTimeE - Now).TotalDays) + I18N.Phrase.Day;
                clT.Text = SetCountdown(deal.BusinessHourOrderTimeE, deal.BusinessHourOrderTimeS);
                string countDownId = string.Empty;
                if (_isKindDeal)
                {
                    discount_2.Text = "公益";
                }
                else if (Helper.IsFlagSet(deal.BusinessHourStatus, BusinessHourStatus.NoDiscountShown) 
                      || Helper.IsFlagSet(deal.BusinessHourStatus, BusinessHourStatus.PriceZeorShowOriginalPrice))
                {
                    discount_2.Text = "特選";
                }
                else if (deal.ItemPrice == 0)
                {
                    if (CityId.Equals(PponCityGroup.DefaultPponCityGroup.Family.CityId) && deal.ExchangePrice.HasValue && deal.ExchangePrice.Value > default(decimal))
                    {
                        if (Helper.IsFlagSet(deal.BusinessHourStatus, BusinessHourStatus.NoDiscountShown))
                        {
                            discount_2.Text = "特選";
                        }
                        else
                        {
                            discount_2.Text = ViewPponDealManager.GetDealDiscountString(Helper.IsFlagSet(deal.BusinessHourStatus, BusinessHourStatus.NoDiscountShown), CheckZeroPriceToShowPrice(deal), deal.ItemOrigPrice);
                            discount_2.Text += "<span class=\"smalltext\">折</span>";
                        }
                    }
                    else
                    {
                        discount_2.Text = "優惠";
                    }

                    Panel pnsale = (Panel)e.Item.FindControl("pn_sale");
                    if (pnsale != null)
                    {
                        pnsale.Visible = false;
                    }
                }
                else if (deal.BusinessHourGuid == new Guid("c204a448-3fbd-4ef3-a24b-dfa01f4334c1"))
                {
                    discount_2.Text = "<script>$('#" + index + "discount_min').attr(\"style\",\"display:none;\");</script>";
                }
                else
                {
                    string discount = ViewPponDealManager.GetDealDiscountString(Helper.IsFlagSet(deal.BusinessHourStatus, BusinessHourStatus.NoDiscountShown), CheckZeroPriceToShowPrice(deal), deal.ItemOrigPrice);
                    int length = discount.Length;
                    if (length > 0)
                    {
                        discount_2.Text = discount.Substring(0, 1);
                        if (discount.IndexOf('.') != -1 && length > 2)
                        {
                            discount_2.Text += discount.Substring(1, length - 1);
                        }

                        discount_2.Text += "<span class=\"smalltext\">折</span>";
                    }
                }

                //商品主題活動行銷活動LOGO圖壓圖
                dealPromoImage.Text = GetDealPromoImageHtmlTag(deal.DealPromoImage);
            }
        }

        public decimal CheckZeroPriceToShowPrice(ViewPponDeal deal)
        {
            if (decimal.Equals(0, deal.ItemPrice))
            {
                if ((deal.BusinessHourStatus & (int)BusinessHourStatus.PriceZeorShowOriginalPrice) > 0)
                {
                    return deal.ItemOrigPrice;
                }
                else if (CityId.Equals(PponCityGroup.DefaultPponCityGroup.Family.CityId) && deal.ExchangePrice.HasValue && deal.ExchangePrice.Value > default(decimal))
                {
                    return deal.ExchangePrice.Value;
                }
                else
                {
                    return deal.ItemPrice;
                }
            }
            else
            {
                return deal.ItemPrice;
            }
        }

        private string SetCountdown(DateTime timeE, DateTime timeS)
        {
            int year = timeE.Year;
            int month = timeE.Month;
            int day = timeE.Day;
            int hour = timeE.Hour;
            int min = timeE.Minute;
            int sec = timeE.Second;
            if (DateTime.Compare(new DateTime(Now.Year, Now.Month, Now.Day, hour, min, sec), Now) > 0)
            {
                if ((Now - timeS).Days < 1 && (timeE - Now).Days >= 1)
                {
                    year = Now.AddDays(1).Year;
                    month = Now.AddDays(1).Month;
                    day = Now.AddDays(1).Day;
                }
                else
                {
                    year = Now.Year;
                    month = Now.Month;
                    day = Now.Day;
                }
            }
            else
            {
                year = Now.AddDays(1).Year;
                month = Now.AddDays(1).Month;
                day = Now.AddDays(1).Day;
            }

            TimeSpan ts = timeE - Now;
            int dayUntil = (int)Math.Floor(ts.TotalDays);
            int hourUntil = (int)Math.Floor(ts.TotalHours - dayUntil * 24);
            int minutesUntil = (int)Math.Floor(ts.TotalMinutes - hourUntil * 60);
            int secondsUntil = (int)Math.Floor(ts.TotalSeconds - hourUntil * 3600 - minutesUntil * 60);
            return hourUntil + I18N.Phrase.Hour + minutesUntil + I18N.Phrase.Minute + secondsUntil + I18N.Phrase.Second;
        }

        private string GetDealPromoImageHtmlTag(string dealPromoImage)
        {
            return (string.IsNullOrEmpty(dealPromoImage)) ? string.Empty : string.Format(@"<img src='{0}' alt='' />"
                , ImageFacade.GetMediaPath(dealPromoImage, MediaType.DealPromoImage));
        }

        public string GetDealShortTitle(ViewPponDeal deal)
        {
            string shortTitle = (string.IsNullOrEmpty(deal.AppTitle)) ? deal.ItemName : deal.AppTitle;
            shortTitle = (deal.DeliveryType == (int)DeliveryType.ToHouse) ? string.Format("宅配：{0}", shortTitle) : shortTitle;
            return shortTitle.Replace("[72H到貨]", "").Replace("[24H出貨]", "").Replace("[24H到貨]", "");
        }

        public string GetImageString(string eventimagepath)
        {
            return ImageFacade.GetMediaPathsFromRawData(eventimagepath, MediaType.PponDealPhoto).DefaultIfEmpty(SystemConfig.SiteUrl + "/Themes/PCweb/images/ppon-M1_pic.jpg").First();
        }

        #endregion

        [WebMethod]
        public static string SendKindMail(string companyName, string address, string contactName, string contactPhoneNo,
            string callBackTime, string email, string hope)
        {
            ILog log = LogManager.GetLogger("MailLog");
            log.InfoFormat("招商專區(行銷與公益合作)->{0}({1}) Send Mail Start.", companyName, email);
            if (!checkStringNull(new string[] { companyName, address, contactName, contactPhoneNo, email, hope })) 
            {
                log.InfoFormat("招商專區(行銷與公益合作)->{0}({1}) validation error.", companyName, email);
                return new JsonSerializer().Serialize(new { SendResult = false, Message = "input validation error." });
            }

            SolicitBusinessEmailBody sbe;
            string sendResultMessage = string.Empty;
            bool sendSuccess = false;

            try 
            {
                sbe = new SolicitBusinessEmailBody(companyName, address, contactName, contactPhoneNo, callBackTime, email);
                sendSuccess = SendMail(string.Format("招商專區(行銷與公益合作)-{0}", companyName), companyName, sbe.GetKindMailBody(hope), out sendResultMessage, (int)ContactType.KindMail);   
            }
            catch (Exception ex) 
            {
                log.InfoFormat("招商專區(行銷與公益合作)->{0}({1}):{2}, {3}", companyName, email, ex.Message, ex.StackTrace);
            }
            return new JsonSerializer().Serialize(new { SendResult = sendSuccess, Message = sendResultMessage });
        }

        [WebMethod]
        public static string SendMarketMail(string companyName, string address, string contactName, string contactPhoneNo,
            string callBackTime, string email, string hope)
        {
            ILog log = LogManager.GetLogger("MailLog");
            log.InfoFormat("招商專區(非實體商品)->{0}({1}) Send Mail Start.", companyName, email);
            if (!checkStringNull(new string[] { companyName, address, contactName, contactPhoneNo, email, hope })) 
            {
                log.InfoFormat("招商專區(非實體商品)->{0}({1}) validation error.", companyName, email);
                return new JsonSerializer().Serialize(new { SendResult = false, Message = "input validation error." });
            }

            SolicitBusinessEmailBody sbe;
            string sendResultMessage = string.Empty;
            bool sendSuccess = false;

            try 
            {
                sbe = new SolicitBusinessEmailBody(companyName, address, contactName, contactPhoneNo, callBackTime, email);
                sendSuccess = SendMail(string.Format("招商專區(非實體商品)-{0}", companyName), companyName, sbe.GetMarketMailBody(hope), out sendResultMessage, (int)ContactType.MarketMail);
            }
            catch (Exception ex) 
            {
                log.InfoFormat("招商專區(非實體商品)->{0}({1}):{2}, {3}", companyName, email, ex.Message, ex.StackTrace);
            }
            return new JsonSerializer().Serialize(new { SendResult = sendSuccess, Message = sendResultMessage });
        }

        [WebMethod]
        public static string SendBusinessMail(string companyName, string address, string contactName, string contactPhoneNo,
            string callBackTime, string email, string hope) 
        {
            ILog log = LogManager.GetLogger("MailLog");
            log.InfoFormat("招商專區(實體商品)->{0}({1}) Send Mail Start.", companyName, email);
            if (!checkStringNull(new string[] { companyName, address, contactName, contactPhoneNo, email, hope })) 
            {
                log.InfoFormat("招商專區(實體商品)->{0}({1}) validation error.", companyName, email);
                return new JsonSerializer().Serialize(new { SendResult = false, Message = "input validation error." });
            }

            SolicitBusinessEmailBody sbe;
            string sendResultMessage = string.Empty;
            bool sendSuccess = false;

            try 
            {
                sbe = new SolicitBusinessEmailBody(companyName, address, contactName, contactPhoneNo, callBackTime, email);
                sendSuccess = SendMail(string.Format("招商專區(實體商品)-{0}", companyName), companyName, sbe.GetBusinessMailBody(hope), out sendResultMessage, (int)ContactType.BusinessMail);
            }
            catch (Exception ex) 
            {
                log.InfoFormat("招商專區(實體商品)->{0}({1}):{2}, {3}", companyName, email, ex.Message, ex.StackTrace);
            }
            return new JsonSerializer().Serialize(new { SendResult = sendSuccess, Message = sendResultMessage });
        }

        private static bool checkStringNull(string[] checkStringList) 
        {
            for (int i = 0; i < checkStringList.Length; i++) 
            {
                if (string.IsNullOrEmpty(checkStringList[i])) 
                {
                    return false;
                }
            }
            return true;
        }

    }
    public class SolicitBusinessEmailBody
    {
        //通用
        public string Address { set; get; }
        public string ContactName { set; get; }
        public string ContactPhoneNo { set; get; }
        public string CallBackTime { set; get; }
        public string Email { set; get; }
        public string SiteUrl { set; get; }

        //公司名稱/團體名稱
        public string CompanyName { set; get; }
        /// <summary>
        /// 統一編號/勸募號
        /// </summary>
        public string CompanyId { set; get; }
        /// <summary>
        /// 品牌
        /// </summary>
        public string BrandName { set; get; }
        /// <summary>
        /// 合作過的網站
        /// </summary>
        public string WorkedSite { set; get; }
        /// <summary>
        /// 合作經驗/網路平台合作經驗
        /// </summary>
        public bool WorkedExperience { set; get; }
        /// <summary>
        /// 曾做過的相關活動
        /// </summary>
        public string WorkedEvent { set; get; }
        /// <summary>
        /// 希望17Life做的事/想要推薦的商品
        /// </summary>
        public string Hope { set; get; }
        /// <summary>
        /// 是否有實體店面
        /// </summary>
        public bool Store { set; get; }
        /// <summary>
        /// 是否連鎖
        /// </summary>
        public bool IsStores { set; get; }
        /// <summary>
        /// 組織型態
        /// </summary>
        public string GroupType { set; get; }
        /// <summary>
        /// 配合方式
        /// </summary>
        public string Solution { set; get; }
        /// <summary>
        /// 電視購物
        /// </summary>
        public string TVBuy { set; get; }

        public SolicitBusinessEmailBody(string companyName, string address, string contactName, string contactPhoneNo, string callBackTime, string email) 
        {
            this.CompanyName = companyName;
            this.Address = address;
            this.ContactName = contactName;
            this.ContactPhoneNo = contactPhoneNo;
            this.CallBackTime = callBackTime;
            this.Email = email;
        }

        public string GetKindMailBody(string hope) 
        {
            //this.WorkedExperience = workedExp;
            //this.WorkedSite = workedSite;
            //this.WorkedEvent = workedEvent;
            this.Hope = hope;

            return GenerateArea(GetKindGroupNameHtml() + GetContactHtml() + TRTDTagString("希望17life為您做些什麼：", this.Hope));

            //return GenerateArea(1, "廠商資料", GetKindGroupNameHtml() + GetContactHtml()) +
            //    GenerateArea(2, "合作經驗", GetKindWorkContentHtml()) +
            //    GenerateArea(3, "希望17Life做些什麼", TRTDTagString("*希望17Life做些什麼：", this.Hope));
        }
        public string GetMarketMailBody(string hope) 
        {
            //this.WorkedExperience = workedExp;
            //this.WorkedSite = workedSite;
            //this.WorkedEvent = workedEvent;
            this.Hope = hope;

            return GenerateArea(GetMarketNameHtml() + GetContactHtml() + TRTDTagString("希望17life為您做些什麼：", this.Hope));

            //return GenerateArea(1, "廠商資料", GetBusinessNameHtml() + GetContactHtml()) +
            //    GenerateArea(2, "合作經驗", GetMarketWorkContentHtml()) +
            //    GenerateArea(3, "想希望與17Life一起做些什麼", TRTDTagString("*想希望與17Life一起做些什麼：", this.Hope));
        }
        public string GetBusinessMailBody(string hope) 
        {
            //this.Store = store;
            //this.IsStores = isStores;
            //this.GroupType = groupType;
            //this.Solution = solution;
            //this.WorkedExperience = workedExp;
            //this.WorkedSite = workedSite;
            //this.TVBuy = tvBuy;
            this.Hope = hope;

            return GenerateArea(GetBusinessNameHtml() + GetContactHtml() + TRTDTagString("希望17life為您做些什麼：", this.Hope));

            //return GenerateArea(1, "廠商資料", GetBusinessNameHtml() + GetContactHtml()) +
            //    GenerateArea(2, "店家類型/合作經驗", GetBusinessWorkContentHtml()) +
            //    GenerateArea(3, "想要推薦的商品", TRTDTagString("*想要推薦的商品：", this.Hope));
        }

        private string GetKindGroupNameHtml() 
        {
            StringBuilder contentSB = new StringBuilder();
            TRTDTagString("提案類型：", "行銷與公益合作", contentSB);
            //TRTDTagString("團體名稱：", this.CompanyName, contentSB);
            //TRTDTagString("勸募字號：", this.CompanyId, contentSB);
            return contentSB.ToString();
        }
        private string GetMarketNameHtml()
        {
            StringBuilder contentSB = new StringBuilder();
            TRTDTagString("提案類型：", "非實體商品", contentSB);
            return contentSB.ToString();
        }

        private string GetBusinessNameHtml() 
        {
            StringBuilder contentSB = new StringBuilder();
            TRTDTagString("提案類型：", "實體商品", contentSB);
            //TRTDTagString("店家/公司/團體名稱：", this.CompanyName, contentSB);
            //TRTDTagString("統一編號：", this.CompanyId, contentSB);
            //TRTDTagString("品牌名稱：", this.BrandName, contentSB);
            return contentSB.ToString();
        }

        private string GetContactHtml() 
        {
            StringBuilder contentSB = new StringBuilder();
            TRTDTagString("店家/公司/團體名稱：", this.CompanyName, contentSB);
            TRTDTagString("地址：", this.Address, contentSB);
            TRTDTagString("聯絡人", this.ContactName, contentSB);
            TRTDTagString("連絡電話：", this.ContactPhoneNo, contentSB);
            TRTDTagString("合適回覆時間：", this.CallBackTime, contentSB);
            TRTDTagString("E-mail：", this.Email, contentSB);
            //TRTDTagString("官網/部落格", this.SiteUrl, contentSB);
            return contentSB.ToString();
        }

        /// <summary>
        /// 取得店家類合作內容
        /// </summary>
        /// <returns></returns>
        //private string GetBusinessWorkContentHtml() 
        //{
        //    StringBuilder contentSB = new StringBuilder();
        //    TRTDTagString("實體店面：", (this.Store) ? "有" : "無", contentSB);
        //    TRTDTagString("公司類型：", (this.IsStores) ? "連鎖" : "單店", contentSB);
        //    TRTDTagString("組織型態：", this.GroupType, contentSB);
        //    TRTDTagString("選擇配合方式(複選)：", this.Solution, contentSB);
        //    TRTDTagString("網路平台合作經驗：", (this.WorkedExperience) ? "有" : "無", contentSB);
        //    TRTDTagString("曾合作過的網站：", this.WorkedSite, contentSB);
        //    TRTDTagString("電視購物：", this.TVBuy, contentSB);
        //    return contentSB.ToString();
        //}

        /// <summary>
        /// 取得廣告行銷類合作內容
        /// </summary>
        /// <returns></returns>
        //private string GetMarketWorkContentHtml() 
        //{
        //    StringBuilder contentSB = new StringBuilder();
        //    TRTDTagString("網路行銷合作經驗：", (this.WorkedExperience) ? "有" : "無", contentSB);
        //    TRTDTagString("曾合作過的網站：", this.WorkedSite, contentSB);
        //    TRTDTagString("曾做過廣告行銷活動：", this.WorkedEvent, contentSB);
        //    return contentSB.ToString();
        //}

        /// <summary>
        /// 取得公益類合作內容
        /// </summary>
        /// <returns></returns>
        //private string GetKindWorkContentHtml() 
        //{
        //    StringBuilder contentSB = new StringBuilder();
        //    TRTDTagString("網路公益合作經驗", (this.WorkedExperience) ? "有" : "無", contentSB);
        //    TRTDTagString("曾合作過的網站：", this.WorkedSite, contentSB);
        //    TRTDTagString("曾做過的相關活動：", this.WorkedEvent, contentSB);
        //    return contentSB.ToString();
        //}

        private void TRTDTagString(string title, string content, StringBuilder sb)
        {
            sb.AppendLine(string.Format("<tr><td style=\"padding:15px 8px;\">{0}</td><td>{1}</td></tr>", title, (string.IsNullOrEmpty(content)) ? "N/A" : content));
        }

        private string TRTDTagString(string title, string content)
        {
            return string.Format("<tr><td style=\"padding:15px 8px;\">{0}</td><td>{1}</td></tr>", title, (string.IsNullOrEmpty(content)) ? "N/A" : content);
        }

        private string GenerateArea(string areaBody)
        {
            //StringBuilder sb = new StringBuilder();
            //sb.AppendLine("<fieldset style=\"border: 3px solid gray; width:700px\">");
            //sb.AppendLine(string.Format("<legend>{0}、{1}</legend>", titleNo, titleName));
            //sb.AppendLine("<table>");
            //sb.Append(areaBody);
            //sb.AppendLine("</table>");
            //sb.AppendLine("</fieldset><br/><br/>");
            //return sb.ToString();
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("<fieldset style=\"border: 1px solid gray; width:600px\">");
            sb.AppendLine("<table style=\"font-family:Microsoft JhengHei;\">");
            sb.Append(areaBody);
            sb.AppendLine("</table>");
            sb.AppendLine("</fieldset>");
            return sb.ToString();
        }
    }
}
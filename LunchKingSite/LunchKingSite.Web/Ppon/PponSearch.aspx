﻿<%@ Page Language="C#" MasterPageFile="~/Ppon/Ppon.Master" AutoEventWireup="true" CodeBehind="PponSearch.aspx.cs" 
    Inherits="LunchKingSite.Web.Ppon.PponSearch" EnableViewState="false" %>

<%@ Import Namespace="LunchKingSite.Core" %>
<%@ MasterType VirtualPath="~/PPon/PPon.Master" %>

<asp:Content ID="Content2" ContentPlaceHolderID="SSC" runat="server">
    <%=LunchKingSite.Core.Helper.RenderCss("/Themes/PCweb/css/ppon_item.min.css") %>
    <link href='<%= ResolveUrl("../themes/PCweb/css/RDL-L.css") %>' rel="stylesheet" type="text/css" />
    <link href='<%= ResolveUrl("../themes/PCweb/css/RDL-M.css") %>' rel="stylesheet" type="text/css" />
    <link href='<%= ResolveUrl("../themes/PCweb/css/RDL-S.css") %>' rel="stylesheet" type="text/css" />
    <style type="text/css">
        .fb_iframe_widget_lift {
            z-index: 10 !important;
        }

        .ui-corner-all {
            -moz-border-radius: 0px;
            -webkit-border-radius: 0px;
            border-radius: 0px;
        }

        .ui-widget-content, .ui-state-default, .ui-state-focus, .ui-state-hover, .ui-state-active, .ui-widget-content .ui-state-active {
            border: none;
            font-weight: normal;
        }
        /*m版寬度1001以下隱藏background image*/
        @media screen and (max-width: 1001px) {
            #wrap {
                background: #f2f2f0 !important;
            }
        }
        @media screen and (max-width: 480px) {
            .tc_small_content_area {
                width: 100%;
            }
        }

        .item_wrap_grid {
            margin-right: 10px;
        }

        #divCenter {
            width:1060px;
        }
        .tc_small_content_area {
            width:1060px;
        }
        #searchinfo {
            display: none;
            width: 1030px;
            float: left
        }
        .HKL_Button_back_to_top_command {
            margin-left:1060px;
        }
    </style>
    <script type="text/javascript" src="<%=ResolveUrl("../Tools/js/ppon/pponMemberCollect.js")%>"></script>
    <script type="text/javascript">
		window.searchEngine = <%=Request["searchEngine"] == null ? "null" : Request["searchEngine"]%>;
		window.takeSize = <%=Request["size"] == null ? "200" : Request["size"]%>;

		$(document).ready(function () {
			if ('<%= Querystring %>' != '') {
				$('#querystring').val('<%= Querystring %>');
			}
			searchdeal();
			FilterSearchResult();
			var memberCollectDealGuids = $.parseJSON($('#hdMemberCollectDealGuidJson').val());
			$('#maincontent').pponMemberCollectList({ 'memberCollectionDealGuids': memberCollectDealGuids });

			window.countdown = new DealCountdown();
			window.countdown.trigger('.TimerTitleField', true);
		});

		function PponSearchSort(obj, type) {
			$('#ulSort a').each(function () { $(this).removeClass(); });
			if ($(obj).parent().attr('id') == 'priceDesc') {
				$('#priceDesc').hide();
				$('#priceAsc').show();
				$('#priceAsc a').addClass('on');
			} else if ($(obj).parent().attr('id') == 'priceAsc') {
				$('#priceAsc').hide();
				$('#priceDesc').show();
				$('#priceDesc a').addClass('on');
			} else {
				$('#priceAsc').hide();
				$('#priceDesc').show();
				$(obj).addClass('on');
			}

			$('#sortType').val(type);
			searchdeal();
		}

		function searchdeal() {
			$('#searchinfo').hide();
			$('#divSortType').hide();
			$('#nokeyword').hide();
			$('#filtererror').hide();
			$('#nodeals').hide();
			$('#promoDeals').hide();
			$('#promoTitle').hide();
			$('#loading').show();
			$('#PponSearchMain').html('');

			var querystring = $.trim($('#querystring').val());

			if (querystring != '' && querystring.length < 100) {
				$.ajax({
					type: "POST",
					url: "<%=SystemConfig.SiteUrl%>/ppon/partial/searchdeals",
					data: "{querystring:'" + escape(querystring) + "', sortType:'"
						+ $('#sortType').val() + "', searchEngine: " + searchEngine
						+ ", takeSize: " + takeSize + "}",
					contentType: "application/json; charset=utf-8",
					dataType: "html",
					success: function (msg) {
						$('#loading').hide();
						var res = msg.split("</noscript>");
						if ($.trim(res[1]).split("item_wrap item_wrap_grid").length === 1) {
							$('#nodeals').show();
							promotodayDeals();
							$('#search_query_no').html('"' + querystring + '"');
						}
						else {
							$('#PponSearchMain').append(msg);
							$('#divSortType').show();
							$('#searchinfo').show();
							$('#search_query').html('"' + querystring + '"');
							$('#search_count').html($('#dealcount').text());
							$("img.multipleDealLazy").lazyload({ placeholder: "https://www.17life.com/Themes/default/images/17Life/G2/ppon-M1_pic.jpg", effect: "fadeIn" }).removeClass("multipleDealLazy");
							window.countdown.refresh();
							$('.hover_place_3col').css('display', 'none');
							$('.item_3col').unbind();
							if ($(window).width() > 1025) {
								$('.hover_place_3col').css('display', 'none');
								$('.item_3col').hover(function () {
									$(this).find('.hover_place_3col').css('display', 'block');
								}, function () {
									$(this).find('.hover_place_3col').css('display', 'none');
								})
							}
						}

						var memberCollectDealGuids = $.parseJSON($('#hdMemberCollectDealGuidJson').val());
						$('#maincontent').pponMemberCollectList({ 'memberCollectionDealGuids': memberCollectDealGuids }).render();

					},
					error: function (xhr, status, error) {
						$('#loading').hide();
						$('#filtererror').show();
					}
				});
			}
			else {
				$('#loading').hide();
				$('#nokeyword').show();
				$('#querystring').focus();
			}
		}

		function promotodayDeals() {
			$('#promoDeals').html('');
			$.ajax({
				type: "POST",
				url: "<%=SystemConfig.SiteUrl%>/ppon/partial/searchtodaydeals",
				data: "{cityid:'<%= CityId %>', areaid:'0'}",
				contentType: "application/json; charset=utf-8",
				dataType: "html",
				success: function (msg) {
					$('#loading').hide();
					if ($.trim(msg) != '') {
						$('#promoTitle').show();
						$('#promoDeals').show();
						$('#promoDeals').append(msg);
						$("img.multipleDealLazy").lazyload({ placeholder: "https://www.17life.com/Themes/default/images/17Life/G2/ppon-M1_pic.jpg", effect: "fadeIn" }).removeClass("multipleDealLazy");
						window.countdown.refresh();
						$('.hover_place_3col').css('display', 'none');
						$('.item_3col').unbind();
						if ($(window).width() > 1025) {
							$('.hover_place_3col').css('display', 'none');
							$('.item_3col').hover(function () {
								$(this).find('.hover_place_3col').css('display', 'block');
							}, function () {
								$(this).find('.hover_place_3col').css('display', 'none');
							});
						}
					}

					var memberCollectDealGuids = $.parseJSON($('#hdMemberCollectDealGuidJson').val());
					$('#maincontent').pponMemberCollectList({ 'memberCollectionDealGuids': memberCollectDealGuids }).render();
				},
				error: function (xhr, status, error) {
					$('#loading').hide();
				}
			});
		}

		function FilterSearchResult() {
			$('#categoryFilter').on('change', function () {
				var count = 0;
				var filterId = String($(this).val()).split(',');

				var divPpon = $("#divPponFilter");
				divPpon.hide();
				var divCategoryItem = $("#divCategoryItemFilter");
				divCategoryItem.hide();


				var hidSearchCategory = $(".hidSearchCategory").html().split(',');

				if ($(this).val() == 87 || $(this).val() == 89 || $(this).val() == 90) {
					//美食
					var objPpon = $("#pponFilter");
					objPpon.empty();
					objPpon.append($("<option></option>").attr("value", "0").text("全部分類"));
					var pponDDLs = LoadPponDDL($(this).val());
					$.each(pponDDLs, function (idx, vp) {
						if ($.inArray(vp.v.toString(), hidSearchCategory) >= 0) {
							objPpon.append($("<option></option>").attr("value", vp.v).text(vp.n));
							divPpon.show();
						}
					});
				}

				if (filterId[0] == 0) {
					$('.item_wrap').show();
					count = $('.item_wrap').length;
				}
				else {
					$.each($('#PponSearchMain').children('.item_wrap'), function () {
						var category = $(this).data("category");
						var hasId = String(category).split(',');
						var intersect = filterId.filter(function (el) {
							return hasId.indexOf(el) != -1;
						});
						if (intersect.length > 0) {
							$(this).show();
							count++;
						}
						else {
							$(this).hide();
						}
					});
				}
				$('#search_count').text(count);
				$(window).scroll();//trigger lazyload
			});

			$('#pponFilter').on('change', function () {
				var count = 0;
				var filterId = [];
				if ($(this).val() == '0') {
					filterId.push(parseInt(String($("#categoryFilter").val()).split(',')));
				} else {
					filterId.push(parseInt($(this).val()));
				}
				filterId.push(parseInt($("#categoryFilter").val()));
				filterId.remove('0');

				var filterId = filterId.filter(DistinctArrays);

				var hidSearchCategory = $(".hidSearchCategory").html().split(',');

				var divCategoryItem = $("#divCategoryItemFilter");
				divCategoryItem.hide();

				if (filterId[0] == 0) {
					$('.item_wrap').show();
					count = $('.item_wrap').length;
				}
				else {
					$.each($('#PponSearchMain').children('.item_wrap'), function () {
						var category = $(this).data("category");
						var hasId = String(category).split(',');
						//var intersect = filterId.filter(function (el) {
						//    return hasId.indexOf(el) != -1;
						//});
						var b = CompareArray(category, filterId);
						//if (intersect.length > 0) {
						if (b) {
							$(this).show();
							count++;
						}
						else {
							$(this).hide();
						}
					});
					LoadCategoryItem();
				}
				$('#search_count').text(count);
				$(window).scroll();//trigger lazyload
			});

			$('#categoryItemFilter').on('change', function () {
				var count = 0;

				var filterId = [];
				if ($(this).val() == '0') {
					filterId.push(parseInt(String($("#pponFilter").val()).split(',')));
				} else {
					filterId.push(parseInt($(this).val()));
				}

				filterId.push(parseInt($("#categoryFilter").val()));
				filterId.push(parseInt($("#pponFilter").val()));
				filterId.remove('0');

				var filterId = filterId.filter(DistinctArrays);

				if (filterId[0] == 0) {
					$('.item_wrap').show();
					count = $('.item_wrap').length;
				}
				else {
					$.each($('#PponSearchMain').children('.item_wrap'), function () {
						var category = $(this).data("category");
						var hasId = String(category).split(',');
						//var intersect = filterId.filter(function (el) {
						//    return hasId.indexOf(el) != -1;
						//});
						var b = CompareArray(category, filterId);
						if (b) {
							$(this).show();
							count++;
						}
						else {
							$(this).hide();
						}
					});
				}
				$('#search_count').text(count);
				$(window).scroll();//trigger lazyload
			});
		}

		function DistinctArrays(value, index, self) {
			return self.indexOf(value) === index;
		}

		Array.prototype.remove = function () {
			var what, a = arguments, L = a.length, ax;
			while (L && this.length) {
				what = a[--L];
				while ((ax = this.indexOf(what)) !== -1) {
					this.splice(ax, 1);
				}
			}
			return this;
		};

		var CompareArray = function (haystack, arr) {
			return arr.every(function (v) {
				return haystack.indexOf(v) >= 0;
			});
		};

		function LoadPponDDL(id) {
			if (id == 87) {
				var layerPpon = [];
				layerPpon.push({
					v: 94,
					n: "台北"
				});
				layerPpon.push({
					v: 95,
					n: "桃園"
				});
				layerPpon.push({
					v: 96,
					n: "竹苗"
				});
				layerPpon.push({
					v: 97,
					n: "中彰"
				});
				layerPpon.push({
					v: 98,
					n: "嘉南"
				});
				layerPpon.push({
					v: 99,
					n: "高屏"
				});
				return layerPpon;
			} else if (id == 90) {
				var layerPpon = [];
				layerPpon.push({
					v: 101,
					n: "北部"
				});
				layerPpon.push({
					v: 102,
					n: "中部"
				});
				layerPpon.push({
					v: 103,
					n: "南部"
				});
				layerPpon.push({
					v: 104,
					n: "宜花東"
				});
				layerPpon.push({
					v: 105,
					n: "離島"
				});
				return layerPpon;
			} else if (id == 89) {
				var layerPpon = [];
				layerPpon.push({
					v: 142,
					n: "台北"
				});
				layerPpon.push({
					v: 143,
					n: "桃園"
				});
				layerPpon.push({
					v: 144,
					n: "竹苗"
				});
				layerPpon.push({
					v: 145,
					n: "中彰"
				});
				layerPpon.push({
					v: 146,
					n: "嘉南"
				});
				layerPpon.push({
					v: 147,
					n: "高屏"
				});
				return layerPpon;
			}

		}
		function LoadCategoryItem() {
			if ($("#pponFilter").val() != '0' && $("#pponFilter").val() != '') {
				var layerItem = [];
				$.ajax({
					type: "POST",
					url: "PponSearch.aspx/GetCategoryItem",
					data: "{channelId:'" + $("#categoryFilter").val() + "', areaId:'" + $("#pponFilter").val() + "'}",
					contentType: "application/json; charset=utf-8",
					dataType: "html",
					async: false,
					success: function (resp) {
						var js = JSON.parse(resp);
						if (js != null && js != undefined) {
							var data = JSON.parse(js.d);
							$.each(data, function (i, o) {
								layerItem.push({
									v: o.CategoryId,
									n: o.CategoryName
								});
							});
						}
					},
					error: function (xhr, status, error) {
						console.log(xhr.responseText);
					}
				});

				var hidSearchCategory = $(".hidSearchCategory").html().split(',');

				if (layerItem.length > 0) {
					//美食
					var objItem = $("#categoryItemFilter");
					var divCategoryItem = $("#divCategoryItemFilter");

					var objCateVal = $("#categoryFilter").val();
					var objPponVal = $("#pponFilter").val();

					objItem.empty();
					objItem.append($("<option></option>").attr("value", "0").text("全部分類"));
					$.each(layerItem, function (idx, vp) {
						var filterId = [];
						filterId.push(parseInt(objCateVal));
						filterId.push(parseInt(objPponVal));
						filterId.push(parseInt(vp.v));

						var itemList = [];

						$.each($('#PponSearchMain').children('.item_wrap'), function () {
							var category = $(this).data("category");
							var b = CompareArray(category, filterId);
							if (b) {
								itemList.push({
									v: vp.v,
									n: vp.n
								});
								//objItem.append($("<option></option>").attr("value", vp.v).text(vp.n));
								//divCategoryItem.show();
							}
						});

						if (itemList.length > 0) {
							var ls = [];
							$.each(itemList, function (i, o) {
								if ($.inArray(o.v, ls) == -1) {
									objItem.append($("<option></option>").attr("value", o.v).text(o.n));
									divCategoryItem.show();
									ls.push(o.v);
								}
							})
						}


						//if ($.inArray(vp.v.toString(), hidSearchCategory) >= 0) {
						//    objItem.append($("<option></option>").attr("value", vp.v).text(vp.n));
						//    divCategoryItem.show();
						//}                    
					});
				}
			}

			//return layerItem;
		}
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MC" runat="server">
    <input id="sortType" type="text" style="display: none" />
    <input id="hdMemberCollectDealGuidJson" type="hidden" runat="server" clientidmode="Static" />
    <div class="tc_small_content_area block-background">
        <img id="loading" src="<%=SystemConfig.SiteUrl%>/Themes/OWriteOff/loading.gif" style="display: none; margin: 0px auto;" alt="" />
        <div class="item_search_result_wrap">
            <div id="searchinfo" class="search-result-get">
                <i class="fa fa-check-circle fa-lg fa-fw"></i>
                為您找到 <span id="search_query" class="coupon-number"></span>相關好康 <span id="search_count" class="coupon-number"></span>檔
                <div class="result_filter">
                    <div class="select_1">
                        <select id="categoryFilter">
                            <option value="0">全部分類</option>
                            <option value="94,142">台北</option>
                            <option value="95,143">桃園</option>
                            <option value="96,144">竹苗</option>
                            <option value="97,145">中彰</option>
                            <option value="98,146">嘉南</option>
                            <option value="99,147">高屏</option>
                            <option value="88">宅配</option>
                            <option value="90">旅遊</option>
                            <option value="91">全家專區</option>
                            <option value="148">品生活</option>
                        </select>
                    </div>
                    <div class="select_1" id="divPponFilter" style="margin-left:5px;display:none">
                        <select id="pponFilter">
                            <option value="0">全部分類</option>
                        </select>
                    </div>
                    <div class="select_1" id="divCategoryItemFilter" style="margin-left:5px;display:none">
                        <select id="categoryItemFilter">
                            <option value="0">全部分類</option>
                        </select>
                    </div>
                </div>
            </div>
            <div id="divSortType" style="display: none;" class="NaviCitySequence">
                <ul id="ulSort" class="clearfix">
                    <li>排序依據：
                    </li>
                    <li style="float: left;" class="on">
                        <a title="最新上架" onclick="PponSearchSort(this, '<%= (int)CategorySortType.TopNews %>');" style="cursor: pointer;">最新</a>
                    </li>
                    <li style="float: left;">
                        <a title="銷量從高到低" onclick="PponSearchSort(this, '<%= (int)CategorySortType.TopOrderTotal %>');" style="cursor: pointer;">銷量</a>
                    </li>
                    <li style="float: left;">
                        <a title="折數從低到高" onclick="PponSearchSort(this, '<%= (int)CategorySortType.DiscountAsc %>');" style="cursor: pointer;">折數</a>
                    </li>
                    <li id="priceDesc" style="float: left; display: none;">
                        <a title="價格從高到低" onclick="PponSearchSort(this, '<%= (int)CategorySortType.PriceDesc %>');" style="cursor: pointer;">價格<span class="icon-arrow-up"></span></a>
                    </li>
                    <li id="priceAsc" style="float: left;">
                        <a title="價格從低到高" onclick="PponSearchSort(this, '<%= (int)CategorySortType.PriceAsc %>');" style="cursor: pointer;">價格<span class="icon-arrow-down"></span></a>
                    </li>
                </ul>
            </div>
            <div class="item_3col_wrap" id="PponSearchMain">
            </div>
            <!--搜尋條件錯誤-->
            <div id="filtererror" style="display: none;" class="search-result-none">
                <i class="fa fa-exclamation-circle fa-lg fa-fw"></i>
                搜尋條件錯誤。
            </div>
            <!--請輸入查詢條件-->
            <div id="nokeyword" style="display: none;" class="search-result-none">
                <i class="fa fa-exclamation-circle fa-lg fa-fw"></i>
                請輸入查詢條件。
            </div>
            <!--查無相關檔次-->
            <div id="nodeals" style="display: none;" class="search-result-none">
                <i class="fa fa-exclamation-circle fa-lg fa-fw"></i>
                抱歉，目前沒有 <span id="search_query_no" class="coupon-number"></span>相關好康，請換一個關鍵詞試試看。為您推薦其他好康如下
            </div>
            <div class="result_title">
                <h3 id="promoTitle" style="display: none;">熱門推薦</h3>
            </div>
            <div class="item_3col_wrap" id="promoDeals" style="display: none;"></div>
            <div class="hidSearchCategory" style="display:none"></div>
        </div>
    </div>
     <script type="text/javascript">
		 _bw('track', 'PageView', {
			 page_type: 'SearchResults',
			 search_string: '<%= Querystring %>'
		});
    </script>
</asp:Content>

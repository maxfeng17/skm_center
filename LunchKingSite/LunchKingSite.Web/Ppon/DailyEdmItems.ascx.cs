﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace LunchKingSite.Web.Ppon
{
    public partial class DailyEdmItems : System.Web.UI.UserControl
    {
        public List<EdmDetail> EdmDetailList { get; set; }

        public EdmDetail MainDetail { get; set; }

        public EdmDetailType MainType { get; set; }

        public EdmDetailType ItemType { get; set; }

        public bool IsFirst { get; set; }

        public string TitleColor { get; set; }

        public string SiteUrl { get; set; }

        public string Cpa { get; set; }

        public int ColumnNumber { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        public override void DataBind()
        {
            if (MainDetail.Id != 0)
            {
                lab_AreaName.Text = MainDetail.CityName;
                pan_Title.Visible = MainDetail.DisplayCityName;
                if (MainType == EdmDetailType.MainDeal_1 && MainDetail.DisplayCityName)
                {
                    lit_Separate.Text = "<br /><hr style='border: none; border-top: 1px solid #DDD;' /><br />";
                }

                if (MainDetail.DisplayCityName && IsFirst)
                {
                    pan_Title.Visible = true;
                }
                else
                {
                    pan_Title.Visible = false;
                }
            }

            List<IViewPponDeal> vpdc = ViewPponDealManager.DefaultManager.ViewPponDealGetListByGuidList(EdmDetailList.Where(x => x.Bid.HasValue).Select(x => x.Bid.Value).ToList());
            foreach (var item in EdmDetailList.Where(x => x.Bid.HasValue && !string.IsNullOrEmpty(x.Title)))
            {
                IViewPponDeal deal;
                if ((deal = vpdc.First(x => x.BusinessHourGuid == item.Bid.Value)) != null)
                {
                    deal.EventName = item.Title;
                }
            }
            int currenttotal = vpdc.Count;
            if (currenttotal > 0)
            {
                for (int i = 0; i < ColumnNumber - currenttotal; i++)
                {
                    vpdc.Add(new ViewPponDeal() { EventName = string.Empty, EventImagePath = string.Empty, ItemOrigPrice = 0, ItemPrice = 0 });
                }

                if (ColumnNumber == 1)
                {
                    pan_Single.Visible = true;
                    rpt_Single.DataSource = vpdc;
                    rpt_Single.DataBind();
                }

                if (ColumnNumber == 2)
                {
                    pan_Double.Visible = true;
                    rpt_Double.DataSource = vpdc;
                    rpt_Double.DataBind();
                }

                if (ColumnNumber == 3)
                {
                    pan_Triple.Visible = true;
                    rpt_Triple.DataSource = vpdc;
                    rpt_Triple.DataBind();
                }
            }
        }

        protected void rptDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is ViewPponDeal)
            {
                Image cimg_Pic = (Image)e.Item.FindControl("img_Pic");
                Label clab_Discount = (Label)e.Item.FindControl("lab_Discount");
                ViewPponDeal deal = (ViewPponDeal)e.Item.DataItem;
                string[] images_array = ImageFacade.GetMediaPathsFromRawData(deal.EventImagePath, MediaType.PponDealPhoto);
                //後台上檔預設 第一張輪播大圖檔名為 bid 置換"-"成"EDM"
                string default_edm_image_name = deal.BusinessHourGuid.ToString().Replace("-", "EDM");
                if (images_array.Any(x => x.Contains(default_edm_image_name)))
                {
                    cimg_Pic.ImageUrl = images_array.DefaultIfEmpty("../Themes/PCweb/images/ppon-M1_pic.jpg").First(x => x.Contains(default_edm_image_name));
                }
                else
                {
                    cimg_Pic.ImageUrl = ImageFacade.GetMediaPathsFromRawData(deal.EventImagePath, MediaType.PponDealPhoto).DefaultIfEmpty("../Themes/PCweb/images/ppon-M1_pic.jpg").First();
                }

                if (deal.ItemOrigPrice != 0)
                {
                    if (Helper.IsFlagSet(deal.BusinessHourStatus, BusinessHourStatus.NoDiscountShown))
                    {
                        clab_Discount.Text = "特選";
                    }
                    else if (deal.ItemPrice > 0)
                    {
                        string rateStr = (deal.ItemPrice * 10 / deal.ItemOrigPrice).FractionFloor(1).ToString("N1");
                        clab_Discount.Text = rateStr + " 折" + ((deal.BusinessHourStatus & (int)BusinessHourStatus.ComboDealMain) > 0 ? "起" : string.Empty);
                    }
                    else
                    {
                        clab_Discount.Text = "優惠";
                    }
                }
                else
                {
                    clab_Discount.Text = "優惠";
                }
            }
        }

        protected void rptPiinLifeDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is HiDealDeal)
            {
                Image cimg_Pic = (Image)e.Item.FindControl("img_Pic");
                HiDealDeal deal = (HiDealDeal)e.Item.DataItem;
                cimg_Pic.ImageUrl = ImageFacade.GetHiDealPhoto(deal, MediaType.HiDealSecondaryBigPhoto, true);
            }
        }

        protected string GetTitleTdColor()
        {
            return string.Format("color: #{0}; font-size: 24px; font-weight: bold; border-bottom: 1px solid #{0};width: 740px; height: 40px", TitleColor);
        }

        protected string GetDiscountBorderColor()
        {
            if (ColumnNumber == 1)
            {
                return string.Format("border: 1px solid #{0}; color: #{0}; font-size: 14px;font-weight: bold; width: 70px; height: 38px;", TitleColor);
            }
            else if (ColumnNumber == 2)
            {
                return string.Format("border: 1px solid #{0}; color: #{0}; font-size: 14px;font-weight: bold; width: 70px; height: 30px;line-height:0px", TitleColor);
            }
            else
            {
                return string.Format("border: 1px solid #{0}; color: #{0}; font-size: 14px;font-weight: bold; width: 65px; height: 22px;", TitleColor);
            }
        }

        protected string GetBuyTdColor()
        {
            if (ColumnNumber == 1)
            {
                return string.Format("border-radius: 5px; background: #{0}; padding-top:8px; padding-bottom:8px;", TitleColor);
            }
            else
            {
                return string.Format("border-radius: 5px; background: #{0}; width: 70px;", TitleColor);
            }
        }
    }
}
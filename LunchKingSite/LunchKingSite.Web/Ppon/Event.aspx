﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Ppon/Ppon.Master" AutoEventWireup="true" CodeBehind="Event.aspx.cs" Inherits="LunchKingSite.Web.Ppon.Event" %>
<%@ Register TagPrefix="uc1" Src="~/UserControls/LoginPanel.ascx" TagName="LoginPanel" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
<asp:Literal ID="liF" runat="server"></asp:Literal>
<asp:Literal ID="liT" runat="server"></asp:Literal>
<asp:Literal ID="liOI" runat="server"></asp:Literal>
<asp:Literal ID="liD" runat="server"></asp:Literal>
<asp:Literal ID="liI" runat="server"></asp:Literal>
<link href="/Themes/default/images/17Life/Gactivities/GoodiesActivity.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
    $(function () {
        $(".sliderWrapMenber").hide();
    });
    function FB_Share() {
        FB.ui(
       {
           method: 'feed',
           name: '<%=FbShareName%>',
           link: '<%=FbShareUrl%>',
           picture: '<%=FbShareImg%>',
           description: '<%=FbShareDesc%>',
       },
       function (response) {
           if (response && response.post_id) {
               alert('感謝你的分享');
           } 
       }
     );
    }

    function showEmail() {
    $.blockUI({ message: $(".GoodiesFillEmailFrame"),
        css: { top: ($(window).height() - 255) / 2 + 'px',
            left: ($(window).width() - 550) / 2 + 'px',
            width: '0px',
            height: '0px',
            border: 'none',
            cursor: 'default'
        },
        overlayCSS: { cursor: 'default' }
    });
    $('.blockOverlay').click($.unblockUI);
    $('.GoodiesFillEmailClose').click($.unblockUI);
    }

    function showRef() {
    $.blockUI({ message: $("#Referral"),
        css: { top: ($(window).height() - 440) / 2 + 'px',
            left: ($(window).width() - 700) / 2 + 'px',
            width: '0px',
            height: '0px',
            border: 'none',
            cursor: 'default'
        },
        overlayCSS: { cursor: 'default' }
    });
    <%  if (!String.IsNullOrEmpty(ButtonLink)&&EventType==3) { %>
    $('.blockOverlay').click(function() {redir();});
    $('#ReturnClose').click(function() {redir();});
    <% } else { %>
    $('.blockOverlay').click($.unblockUI);
    $('#ReturnClose').click($.unblockUI);
    <% } %>
    }

    function showEmailSucc() {
    $.blockUI({ message: $(".GoodiesFillEmailOk"),
        css: { top: ($(window).height() - 255) / 2 + 'px',
            left: ($(window).width() - 550) / 2 + 'px',
            width: '0px',
            height: '0px',
            border: 'none',
            cursor: 'default'
        },
        overlayCSS: { cursor: 'default' }
    });
    $('.blockOverlay').click($.unblockUI);
    }

    $(document).ready(function () {
    $('.GoodiesFillEmailFrame').hide();
    $('#Referral').hide();
    $('#GoodiesAcFBText:empty').each(function () {    $(this).hide() ;   }); 
    $('GoodiesAcFB_Fans:empty').hide();
    $('GoodiesAcFBShare_Friends:empty').hide();
    $('GoodiesAcFbSend:empty').hide();
    <% if(EventType==2 && InTime) { %>
        $('.GoodiesAcBtn').bind('click', function () { showEmail(); });
        $('.GoodiesFillEmailFrame').css("background-image", "url('<%=EmailImg %>')"); 
    <% }else if(EventType==3 && ShowRef && InTime) { %>
        //$('.GoodiesAcBtn').bind('click', function () { showRef(); });
        showRef();
    <% } 
    if (ShowEmail) { %>
    <% if (EmailSuccess) { %>
    showEmailSucc()
    <% if (!String.IsNullOrEmpty(ButtonLink)&&EventType==2) { %>
    setTimeout(function() {
      window.location.href = "<%=ButtonLink%>";
    }, 3000);
    <% }}else{ %>
    showEmail();
    <% }} %>

    });

    function fill() {
        var mailReg = /^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i;
        if (mailReg.test($$('TBEmail').val())) {
        var leng = $("#<%=TBEmail.ClientID %>").val();
        if (leng.length > 0) {
            $("#<%=HFEmail.ClientID %>").val(leng);
            }
        var leng = $("#<%=TBCapt.ClientID %>").val();
        if (leng.length > 0) {  $("#<%=HFCapt.ClientID %>").val(leng);  }
       // $$('HFEmail').text($$('TBEmail').val());
        $.unblockUI();
        return true;
        }
        else
        {
            $$('TBEmail').val('請輸入正確的Email');
            $$('TBEmail').css('color','red');
            return false;
        }
    }

    function chklogin() {
        return true;
    }

    function disapper() {
        if(($$('TBEmail').val()=='請填寫email') || ($$('TBEmail').val()=='請輸入正確的Email'))
        {
            $$('TBEmail').css('color','black');
            $$('TBEmail').val('');
        }
    }

    function disapper2() {
        if(($$('TBCapt').val()=='請輸入左方的驗證碼') || ($$('TBCapt').val()=='請輸入正確的認證碼'))
        {
            $$('TBCapt').css('color','black');
            $$('TBCapt').val('');
        }
    }

    function redir() {
        $.unblockUI();
        setTimeout(function() {
          window.location.href = "<%=ButtonLink%>";
        }, 1000);
    }

    function $$(id, context) {
        var el = $("#" + id, context);
        if (el.length < 1)
            el = $("*[id$=" + id + "]", context);
        return el;
    }
</script>
<style type="text/css">
.CityTop { display:none; }
#Cityarea { display:none; }
.center { overflow:visible;}
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MC" runat="server">
    <asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
        <CompositeScript ScriptMode="Release">
            <Scripts>
                <asp:ScriptReference Path="~/Tools/js/jquery.blockUI.js" />
            </Scripts>
        </CompositeScript>
    </asp:ScriptManagerProxy>
        <asp:PlaceHolder ID="pl" runat="server" Visible="false">
        <script type="text/javascript">
            $(document).ready(function () {
                $.blockUI({ message: $('#plp'), css: { border: '0px', width: '0'} });
            });
        </script>
        <div id="plp" style="display: none;">
            <uc1:LoginPanel ID="lp" runat="server" />
        </div>
    </asp:PlaceHolder>
<asp:HiddenField ID="HFEmail" runat="server"/>
<asp:HiddenField ID="HFCapt" runat="server"/>
<div class="GoodiesActivitiy">
  <div class="GoodiesAcTittle"><asp:HyperLink ID="HLtxt" runat="server" ></asp:HyperLink></div>
  <div class="GoodiesAcFaceBook">
    <div class="GoAcFbRight">    
    <div id="GoodiesAcFBText"><asp:Label runat="server" ID="LBlikeTxt"></asp:Label></div>
    <div id="GoodiesAcFB_Fans"> <asp:Label runat="server" ID="LBlike"></asp:Label></div>
    <div id="GoodiesAcFBText"><asp:Label runat="server" ID="LBshareTxt"></asp:Label></div>
    <div id="GoodiesAcFBShare_Friends" style="cursor:pointer"><asp:Image ID="ImgFBShare" runat="server" ImageUrl="/Themes/PCweb/images/FB_Btn.png" width="147" height="22" border="0" onclick="FB_Share();"/></div>
    <div id="GoodiesAcFBText"><asp:Label runat="server" ID="LBsendTxt"></asp:Label></div>
    <div class="GoodiesAcFbSend"><asp:Label runat="server" ID="LBsend"></asp:Label></div>
  </div></div>
  <div class="GoodiesAcMainVisual"><asp:HyperLink ID="HLview" runat="server" ></asp:HyperLink></div>
  <div class="GoodiesAcBtn" style="cursor:pointer"><asp:linkbutton runat="server" ID="LBbtn" OnClientClick="return chklogin()" OnCommand="btnLB_Click" Enabled="false" >
      <asp:Image ID="btnImg" runat="server" /></asp:linkbutton></div>
    <asp:Label runat="server" ID="LBreward" Visible="false">
  <div class="GoodiesAcWinne">
    <div class="GoodiesAcWinneTittle"></div>
    <div class="GoodiesAcWinneTeFrame" style="padding-left: 5px;">
      <div class="GoodiesAcWinneText" id="GoodiesAcWinne1" ><asp:Label runat="server" ID="LBEmailList3" ></asp:Label></div>
      <div class="GoodiesAcWinneText" id="GoodiesAcWinne2" ><asp:Label runat="server" ID="LBEmailList1" ></asp:Label></div>
      <div class="GoodiesAcWinneText" id="GoodiesAcWinne3" ><asp:Label runat="server" ID="LBEmailList2" ></asp:Label></div>
    </div>
      <div class="GoodiesAcWinneBottom"></div>
  </div>
  </asp:Label>
  <div class="GoodiesAcMeasures">
    <div class="GoodiesAcMeasuresTittle"></div>    
    <div class="GoodiesAcMeasuresText">
    <asp:Label runat="server" ID="LBrule"></asp:Label>
    </div>
    <div class="GoodiesAcMeasuresBottom"></div>
  </div>
  
</div>

<div class="GoodiesFillEmailFrame" style="display:none">
  <div class="GoodiesFillEmailClose"><img src="/Themes/default/images/17Life/Gactivities/MarketingEmailClose.png" width="35" height="35" alt="EmailClose"/></div>
  <div class="GoodiesMailText" ><asp:TextBox ID="TBEmail" runat="server" Width="300px" Text="請填寫email" onClick="disapper()"></asp:TextBox></div>
  <div class="GoodiesMailVerificationCode">
    <table width="255" border="0" cellspacing="0" cellpadding="0" >
      <tbody><tr>
         <td width="100" height="25" > <div align="center"><asp:Image ID="imCaptcha" ImageUrl="~/Service/Captcha.ashx" runat="server" /></div></td>
         <td width="155" height="25" class="GoodiesMailVCText"><div align="center"><asp:TextBox ID="TBCapt" runat="server" MaxLength="4" Width="150" value="請輸入左方的驗證碼" onClick="disapper2()"></asp:TextBox></div></td>
      </tr>
    </tbody></table>
  </div>
  <div class="GoodiesMailBtn"><asp:LinkButton ID="btnEmail" runat="server" OnClientClick="return fill();" OnCommand="btnEmail_Click">
  <img src="/Themes/default/images/17Life/Gactivities/MarketingEmailBtn.png" style="height:40px;width:150px;border-width:0px;"></asp:LinkButton></div>
</div>

<div class="GoodiesFillEmailOk" style="display:none">
  <div class="GoodiesFillEmailClose"><img src="/Themes/default/images/17Life/Gactivities/MarketingEmailClose.png" width="35" height="35"></div> 
</div>

<div id="Referral" style="display:none" >
    <div id="ReturnClose" style="cursor: pointer; margin-top: 3px; margin-right: 3px">
    </div>
    <div id="ReferralContent">
        <div id="ReferralTitle">
            <img src="/Themes/default/images/17Life/G2/star.png" width="30" height="31" />
            以下是您的專屬邀請連結
        </div>
        <!--ReferralTitle-->
        <div id="ReferralLinkarea" style="cursor: text; margin-top: -5px">
            <asp:TextBox ID="tbx_ReferralShare" Width="500" runat="server" ReadOnly="true"></asp:TextBox>
        </div>
        <div id="ReferralStep" style="margin-top: 9px">
            <p>複製上面的邀請連結</p>
            <p>把連結傳給好友</p>
            <p>親友點選連結後回到17Life完成首次購買</p>
            <p>您拿到推薦獎勵，折價券500元</p>
        </div>
        <div id="Referralexplain" style="margin-top: -9px; height: 70px">
            您可以用下面二種方式分享您的邀請連結<br />
            <span style="color: #f00; font-weight: bold;">邀請次數無上限～</span>分享越多賺越多喔！<br />
            <span style="color: #f00; font-weight: bold;">訂單金額50元(含)以下恕不適用此折價券贈送活動</span>
        </div>
        <div id="referral_FB_area">
            <asp:HyperLink ID="hhf" runat="server" Target="_blank" onclick="FB_Share();"> <img src="/Themes/PCweb/images/FB_Btn.png" width="113" height="30" /></asp:HyperLink>
        </div>
        <div id="referral_PL_area">
            <asp:HyperLink ID="hhp" runat="server" Target="_blank"> <img src="/Themes/default/images/17Life/G2/PLK_Btn.png" width="82" height="30" /></asp:HyperLink>
        </div>
    </div>
</div>

<div style="clear:both"></div>
</asp:Content>

<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OnlinePromoEDM.aspx.cs"
    Inherits="LunchKingSite.Web.Ppon.OnlinePromoEDM" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=Big5" />
    <title>PayEasy大團購 x 17Life，每天最殺好康即時報</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table width="750" border="0" align="center" cellpadding="1" cellspacing="0">
            <tr>
                <td>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-size: 12px;
                        color: #000; font-family: '新細明體'; line-height: 2.2em">
                        <tr>
                            <td>
                                PayEasy大團購 x 17Life，每天最殺好康即時報
                            </td>
                            <td align="right">
                                <div style="display: none">
                                    若您無法閱讀此EDM&rarr;<a href="<%= SiteUrl+"/ppon/onlinepromoedm.aspx?eid="+EdmID+"&amp;mode="+Mode %>"
                                        title="請點此" target="_blank" style="color: #09C">請點此</a>&nbsp;&nbsp;&nbsp; <a href="http://cs.payeasy.com.tw/cancel/payeasy"
                                            title="取消訂閱" target="_blank" style="color: #000000"><u>取消訂閱</u></a>
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="background-color: #f98bb5; font-size: 13px;">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0" style="background-color: #fff">
                        <tr>
                            <td>
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td>
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0" style="background-color: #ee4d8a;
                                                color: #fff">
                                                <tr>
                                                    <td valign="top" height="80">
                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td>
                                                                    <img src="<%=SiteUrl %>/Themes/default/images/17Life/EDM/header_logo.gif"
                                                                        width="443" height="54" alt="PayEasy大團購x17P" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td height="20" style="font-size: 13px">
                                                                    &nbsp;&nbsp;天天有新檔，每天最殺折扣好康&nbsp;&nbsp;&nbsp;&nbsp;所在城市：<span style="color: #fff"><asp:Label
                                                                        ID="lab_CityName" runat="server"></asp:Label></span>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td>
                                                        <a href="http://www.payeasy.com.tw" target="_blank">
                                                            <img src="https://www.payeasy.com.tw/cosmetics/edm/pay17p/0203/images/logo_pez.gif"
                                                                alt="PayEasy logo" width="214" height="80" border="0" /></a><a href="<%=SiteUrl %>/ppon/default.aspx?rsrc=PEZ_Bedm"
                                                                    target="_blank"><img src="<%=SiteUrl %>/Themes/default/images/17Life/EDM/logo_17p.gif"
                                                                        alt="17P logo" width="91" height="80" border="0" /></a>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table width="748" style="background-color: #f9f5ee" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td align="center">
                                                        <asp:Panel ID="pan_AD" runat="server">
                                                            <asp:Literal ID="lit_AD" runat="server"></asp:Literal>
                                                        </asp:Panel>
                                                        <table border="0" cellpadding="0" cellspacing="0" style="background-color: #f9f5ee"
                                                            width="748">
                                                            <tr>
                                                                <td>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0" style="background-color: #f9f5ee">
                                                <tr>
                                                    <td>
                                                        <table width="748" style="background-color: #f9f5ee">
                                                            <tr>
                                                                <td height="10">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="center">
                                                                    <asp:Panel ID="pan_MainDeal1" runat="server">
                                                                    </asp:Panel>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <table width="748" style="background-color: #f9f5ee" border="0" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td align="center">
                                                                    <table width="726" border="0" cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td>
                                                                            </td>
                                                                            <td>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                    <table width="726" border="0" cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td>
                                                                            </td>
                                                                            <td>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                    <table width="726" border="0" cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td>
                                                                            </td>
                                                                            <td>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                    <table width="726" border="0" cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td>
                                                                            </td>
                                                                            <td>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td height="15">
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Panel ID="pan_PEZ" runat="server">
                                                <table width="748" style="background-color: #f9f5ee" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td align="center">
                                                            <img src="https://www.payeasy.com.tw/cosmetics/edm/pay17p/0203/images/pez_bar.jpg"
                                                                width="748" height="51" alt="PayEasy每日一物" />
                                                            <asp:Literal ID="lit_PEZ" runat="server"></asp:Literal>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Panel ID="pan_Area" runat="server">
                                                <table width="748" style="background-color: #f9f5ee" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td align="center">
                                                            <asp:Panel ID="pan_Area1" runat="server">
                                                            </asp:Panel>
                                                            <table width="726" border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td>
                                                                    </td>
                                                                    <td>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <table width="726" border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td>
                                                                    </td>
                                                                    <td>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td height="15">
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table width="748" border="0" cellpadding="0" cellspacing="0" align="center" style="background-color: #F6EADF">
                                                <tr>
                                                    <td>
                                                        <table width="726" border="0" cellpadding="0" cellspacing="0" align="center">
                                                            <tr>
                                                                <td height="32" style="padding-left: 10px; color: #FFF; background-color: #3E3226;
                                                                    font-weight: bold">
                                                                    品生活
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <asp:Panel ID="pan_PiinLife1" runat="server">
                                                        </asp:Panel>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <table align="center" style="background-color: #ffcde0; font-size: 12px; color: #000;
            line-height: 1.8em; padding: 5px 0; width: 750px">
            <tr>
                <td align="center">
                    網頁內容如與結帳頁面不符，皆以商品結帳頁面為準，恕不另行通知，敬請見諒!<br />
                    如有任何問題或欲取消Payeasy所有行銷方式，請洽<a href="http://www.payeasy.com.tw/cs/" title="客服中心" target="_blank"
                        style="font-size: 12px; color: #09C">客服中心</a>0800-023-008<br />
                    17Life-康太數位整合股份有限公司，版權所有，轉載必究 Copyright &copy; <script language="javascript"> var Today = new Date(); document.write(Today.getFullYear()); </script>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>

﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.WebLib.Component;

namespace LunchKingSite.Web.Ppon
{
    public partial class PponSearch : BasePage
    {
        public int CityId
        {
            get
            {
                return Master.SelectedCityId;
            }
            set
            {
                Master.SelectedPponCity = PponCityGroup.DefaultPponCityGroup.GetPponCityByCityId(value);
            }
        }

        public string Querystring
        {
            get
            {
                string q = Request.QueryString["search"] != null ? Request.QueryString["search"].ToString() : string.Empty;
                q = q.Replace("'", "\\'");
                return q;
            }
        }

        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();

        public static ISysConfProvider SystemConfig
        {
            get
            {
                return config;
            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            if (CommonFacade.IsMobileVersion())
            {
                string mdealUrl = string.Format("/m/SearchResult");
                QueryStringBuilder qsb = new QueryStringBuilder(mdealUrl);
                qsb.Append("keywords", Querystring);
                string redirectUrl = qsb.ToString();
                Response.Redirect(redirectUrl);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            IMemberProvider mp = ProviderFactory.Instance().GetDefaultProvider<IMemberProvider>();
            DateTime baseDate = PponDealHelper.GetTodayBaseDate();
            int UserId = User.Identity is PponIdentity ? (User.Identity as PponIdentity).Id : 0;
            SetMemberCollection(mp.MemberCollectDealGetOnlineDealGuids(
                UserId, baseDate.AddDays(-1), baseDate.AddDays(1)));

            string queryString = HttpUtility.HtmlEncode(Querystring);

            Page.Title = string.Format("{0}{1}熱銷搜尋結果", queryString, string.IsNullOrEmpty(queryString) ? "" : "-");
            Page.MetaDescription =
                string.Format("你想找的{0}就在17life！這裡的{0}全台線上最優惠，提供多家品牌商品任你挑選，買好物來17Life就對了！", queryString);
        }

        private void SetMemberCollection(List<Guid> collectBids)
        {
            string jsonStr = ProviderFactory.Instance().GetDefaultSerializer()
                .Serialize(collectBids);
            hdMemberCollectDealGuidJson.Value = jsonStr;
        }

        #region webservice
        [System.Web.Services.WebMethod, System.Web.Script.Services.ScriptMethod(ResponseFormat = System.Web.Script.Services.ResponseFormat.Json, UseHttpGet = false)]
        public static string GetCategoryItem(int channelId, int areaId)
        {
            CategoryDealCountNode node = PponDealPreviewManager.GetCategoryDealCountNodeByChannel(channelId, areaId, areaId);
            var channels = node.DealCountList.Select(x => new { x.CategoryId, x.CategoryName }).ToList();
            return Newtonsoft.Json.JsonConvert.SerializeObject(channels);
        }

        #endregion webservice
    }
}
﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.I18N;
using LunchKingSite.WebLib;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using PaymentType = LunchKingSite.Core.PaymentType;

namespace LunchKingSite.Web.Ppon
{
    public partial class KindPay : PponPayBase, IPponKindPayView
    {
        #region Event

        public event EventHandler<CheckDataEventArgs> CheckOut = null;

        public event EventHandler ResetContent = null;

        public event EventHandler<ShowResultEventArgs> ShowPageResult = null;
        #endregion Event

        #region Field

        public decimal subtotal = 0;
        public decimal total = 0;
        public decimal deliveryCharge = 0;
        public string dealName = string.Empty;

        #endregion Field

        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(KindPay));
        private static ILocationProvider lp = ProviderFactory.Instance().GetProvider<ILocationProvider>();
        private static IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        public static ISysConfProvider conf = ProviderFactory.Instance().GetConfig();

        #region properties

        private PponKindPayPresenter _presenter;

        public PponKindPayPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        public ISysConfProvider SystemConfig
        {
            get
            {
                return conf;
            }
        }

        public Guid BusinessHourGuid
        {
            get
            {
                Guid ret = Guid.Empty;
                if (Request["bid"] != null)
                {
                    try
                    {
                        ret = new Guid(Request["bid"]);
                    }
                    catch
                    {
                        ret = Guid.Empty;
                    }
                }
                return ret;
            }
        }

        public string SessionId
        {
            get
            {
                return Session.SessionID;
            }
        }

        public string UserName
        {
            get
            {
                return User.Identity.Name;
            }
        }

        private string randomKey
        {
            get
            {
                return string.IsNullOrWhiteSpace(TicketId) ? string.Empty : TicketId.Split('_')[2];
            }
        }

        public string TransactionId
        {
            get
            {
                if (Session[LkSiteSession.TransId.ToString() + "_" + randomKey] == null)
                {
                    return string.Empty;
                }

                return Session[LkSiteSession.TransId.ToString() + "_" + randomKey].ToString();
            }
            set
            {
                Session[LkSiteSession.TransId.ToString() + "_" + randomKey] = value;
            }
        }

        public int DeliveryCharge
        {
            get
            {
                if (Session["DeliveryCharge" + "_" + randomKey] == null)
                {
                    return 0;
                }

                return int.Parse(Session["DeliveryCharge" + "_" + randomKey].ToString());
            }
            set
            {
                Session["DeliveryCharge" + "_" + randomKey] = value;
            }
        }

        public CouponFreightCollection TheCouponFreight
        {
            get;
            set;
        }

        public EntrustSellType EntrustSell { get; set; }

        public Guid OrderGuid
        {
            get
            {
                if (ViewState["OrderGuid"] != null)
                {
                    return (Guid)ViewState["OrderGuid"];
                }

                return Guid.Empty;
            }
            set
            {
                ViewState["OrderGuid"] = value;
            }
        }

        public Guid ParentOrderId
        {
            get
            {
                if (ViewState["ParentID"] != null)
                {
                    return (Guid)ViewState["ParentID"];
                }

                return Guid.Empty;
            }
            set
            {
                ViewState["ParentID"] = value;
            }
        }

        public string DeliveryDate
        {
            get
            {
                if (ViewState["DeliveryDate"] != null)
                {
                    return ViewState["DeliveryDate"].ToString();
                }

                return string.Empty;
            }
            set
            {
                ViewState["DeliveryDate"] = value;
            }
        }

        public string DeliveryTime
        {
            get
            {
                if (ViewState["DeliveryTime"] != null)
                {
                    return ViewState["DeliveryTime"].ToString();
                }

                return string.Empty;
            }
            set
            {
                ViewState["DeliveryTime"] = value;
            }
        }

        public bool IsItem
        {
            get
            {
                if (ViewState["IsItem"] != null)
                {
                    return (bool)ViewState["IsItem"];
                }

                return false;
            }
            set
            {
                ViewState["IsItem"] = value;
            }
        }

        public bool IsATMAvailable
        {
            get
            {
                if (Session["ATMokay_" + randomKey] == null)
                {
                    return false;
                }

                return bool.Parse(Session["ATMokay_" + randomKey].ToString());
            }
            set
            {
                Session["ATMokay_" + randomKey] = value;
            }
        }

        public bool IsATMLimitZero
        {
            get
            {
                if (Session["ATMZero_" + randomKey] == null)
                {
                    return true;
                }

                return bool.Parse(Session["ATMZero_" + randomKey].ToString());
            }
            set
            {
                Session["ATMZero_" + randomKey] = value;
            }
        }

        public bool IsPaidByATM
        {
            get
            {
                if (Session["IsPaidByATM_" + randomKey] == null)
                {
                    return false;
                }

                return bool.Parse(Session["IsPaidByATM_" + randomKey].ToString());
            }
            set
            {
                Session["IsPaidByATM_" + randomKey] = value;
            }
        }

        public bool NotDeliveryIslands
        {
            get
            {
                return bool.Parse(hiNotDeliveryIslands.Value);
            }
            set
            {
                hiNotDeliveryIslands.Value = value.ToString();
            }
        }

        public string AddressInfo
        {
            get
            {
                if (Session["AddressInfo_" + randomKey] == null)
                {
                    return string.Empty;
                }

                return Session["AddressInfo_" + randomKey].ToString();
            }
            set
            {
                Session["AddressInfo_" + randomKey] = value;
            }
        }

        public string UserInfo
        {
            get;
            set;
        }

        public bool MultipleBranch
        {
            get
            {
                if (Session["MultipleBranch" + randomKey] == null)
                {
                    return false;
                }

                bool rlt;
                bool.TryParse(Session["MultipleBranch" + randomKey].ToString(), out rlt);
                return rlt;
            }
            set
            {
                Session["MultipleBranch" + randomKey] = value;
            }
        }

        public string ATMAccount
        {
            get
            {
                if (Session["atmAccount_" + randomKey] == null)
                {
                    return string.Empty;
                }

                return Session["atmAccount_" + randomKey].ToString();
            }
            set
            {
                Session["atmAccount_" + randomKey] = value;
            }
        }

        public bool NoSippingFeeMessage
        {
            get;
            set;
        }

        public CpaClientMain<CpaClientEvent<CpaClientDeail>> NewCpa
        {
            get
            {
                if (Session[LkSiteSession.NewCpa.ToString()] != null)
                {
                    return (CpaClientMain<CpaClientEvent<CpaClientDeail>>)Session[LkSiteSession.NewCpa.ToString()];
                }
                else
                {
                    return null;
                }
            }
            set
            {
                Session[LkSiteSession.NewCpa.ToString()] = value;
            }
        }

        public PponDeliveryInfo DeliveryInfo
        {
            get
            {
                PponDeliveryInfo info = new PponDeliveryInfo();
                info.StoreGuid = SelectedStoreGuid;
                //P好康 讀取
                if (!IsItem)
                {
                    info.SellerType = DepartmentTypes.Ppon;
                    info.DeliveryType = DeliveryType.ToShop; //好康到店到府消費方式判斷
                    info.IsForFriend = false;
                }
                else
                {
                    info.SellerType = DepartmentTypes.PponItem;
                    info.DeliveryType = DeliveryType.ToHouse; //好康到店到府消費方式判斷
                    info.WriteName = ReceiverName.Text.Trim();
                    info.Phone = ReceiverMobile.Text.Trim();
                    info.Freight = string.IsNullOrWhiteSpace(hiddCharge.Value.Trim()) ? 0 : decimal.Parse(hiddCharge.Value);
                }

                info.ReceiptsType = (DonationReceiptsType)ViewState["rb"];
                info.DeliveryDateString = DeliveryDate;
                info.DeliveryTimeString = DeliveryTime;
                info.Notes = string.Empty;
                info.Quantity = hidIsZeroEvent.Value == "False" ? int.Parse(hidqty.Value) : 1;

                //開立收據取代發票
                info.IsEinvoice = false;
                info.InvoiceBuyerName = Request[txtReceiptTitle.UniqueID];

                string addr = string.Format("{0}{1}",
                            CityManager.CityTownShopStringGet(int.Parse(Request[ddlReceiptArea.UniqueID])),
                            txtReceiptAddress.Text);

                City town = CityManager.TownShipGetById(int.Parse(Request[ddlReceiptArea.UniqueID]));
                if (town != null && !string.IsNullOrWhiteSpace(town.ZipCode))
                {
                    info.InvoiceBuyerAddress = info.WriteAddress = town.ZipCode + " " + addr;
                }
                else
                {
                    info.InvoiceBuyerAddress = info.WriteAddress = addr;
                }

                info.UnifiedSerialNumber = Request[txtReceiptNumber.UniqueID];
                info.EntrustSell = this.EntrustSell;

                return info;
            }
        }

        public bool IsTest
        {
            get
            {
                if (ViewState["IsTest"] != null)
                {
                    return (bool)ViewState["IsTest"];
                }

                return false;
            }
            set
            {
                ViewState["IsTest"] = value;
            }
        }

        public string UserMemo
        {
            get;
            set;
        }

        public Guid SelectedStoreGuid
        {
            get
            {
                var selectedStr = ddlStore.SelectedValue;
                if (!IsItem && !string.IsNullOrWhiteSpace(selectedStr))
                {
                    return new Guid(selectedStr);
                }
                else if (!string.IsNullOrWhiteSpace(HiddenSingleStoreGuid.Text))
                {
                    return new Guid(HiddenSingleStoreGuid.Text);
                }
                return Guid.Empty;
            }
        }

        public ExternalMemberInfo SsoMemberInfo
        {
            get
            {
                return (ExternalMemberInfo)Session[LkSiteSession.ExternalMemberId.ToString()];
            }
        }

        public string RsrcSession
        {
            get
            {
                if (Session["rsrc"] != null)
                {
                    return Session["rsrc"].ToString();
                }
                else
                {
                    return null;
                }
            }
        }

        // add for credit card api
        public string TicketId
        {
            get
            {
                if (string.IsNullOrEmpty(Request.QueryString["TicketId"]))
                {
                    return string.Empty;
                }

                return Request.QueryString["TicketId"];
            }
        }

        public int Amount
        {
            get
            {
                if (ViewState[LkSiteSession.Amount.ToString()] == null)
                {
                    return 0;
                }

                return int.Parse(ViewState[LkSiteSession.Amount.ToString()].ToString());
            }
            set
            {
                ViewState[LkSiteSession.Amount.ToString()] = value;
            }
        }

        public IViewPponDeal TheDeal { get; set; }

        public string CookieReferenceId
        {
            get
            {
                HttpCookie cookie = Request.Cookies.Get(LkSiteCookie.ReferenceId.ToString("g"));
                if (cookie != null)
                {
                    return cookie.Value;
                }

                return string.Empty;
            }
        }

        public int CookieCityId
        {
            get
            {
                HttpCookie cookie = Request.Cookies.Get(LkSiteCookie.CityId.ToString("g"));

                if (cookie != null)
                {
                    try
                    {
                        return int.Parse(cookie.Value);
                    }
                    catch (Exception)
                    {
                        return -1;
                    }
                }

                return -1;
            }
        }

        public PaymentResultPageType PaymentResult
        {
            get
            {
                return ViewState["pr"] != null ? (PaymentResultPageType)ViewState["pr"] : PaymentResultPageType.CreditcardSuccess;
            }
            set
            {
                ViewState["pr"] = value;
            }
        }

        private IOrderProvider op = ProviderFactory.Instance().GetProvider<IOrderProvider>();

        public string ReferrerSourceId
        {
            get
            {
                return Request.Cookies[LkSiteCookie.ReferrerSourceId.ToString()] != null ? Request.Cookies[LkSiteCookie.ReferrerSourceId.ToString()].Value : null;
            }
        }

        // add for credit card api
        public bool IsPreview
        {
            get
            {
                if (Page.User.IsInRole("production"))
                {
                    return (string.IsNullOrWhiteSpace(Request["p"]) || !string.Equals("y", Request["p"])) ? false : true;
                }
                else
                {
                    return false;
                }
            }
        }

        public string IsHami
        {
            get
            {
                if (Session["HamiTicketId"] == null)
                {
                    return null;
                }

                return Session["HamiTicketId"].ToString();
            }
        }

        public OrderFromType FromType
        {
            get
            {
                string userAgent = Request.UserAgent;
                if (Request.Browser.IsMobileDevice || userAgent.Contains(MobileOsType.Android.ToString(), StringComparison.OrdinalIgnoreCase))
                {
                    if (userAgent.Contains(MobileOsType.Android.ToString(), StringComparison.OrdinalIgnoreCase))
                    {
                        return OrderFromType.ByMwebAndroid;
                    }
                    else if (userAgent.Contains("ipad", StringComparison.OrdinalIgnoreCase))
                    {
                        return OrderFromType.ByMwebiPad;
                    }
                    else if (userAgent.Contains("iphone", StringComparison.OrdinalIgnoreCase))
                    {
                        return OrderFromType.ByMwebiPhone;
                    }
                    else if (userAgent.Contains("ipod", StringComparison.OrdinalIgnoreCase))
                    {
                        return OrderFromType.ByMwebiPod;
                    }
                    else
                    {
                        return OrderFromType.ByMweb;
                    }
                }
                else
                {
                    return OrderFromType.ByWeb;
                }
            }
        }

        #endregion properties

        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            if (!Page.IsPostBack)
            {
                if (!_presenter.OnViewInitialized())
                {
                    Response.Redirect("~/error-sgo.aspx");
                    return;
                }
                else
                {
                    switch (PaymentResult) //付款狀況
                    {
                        case PaymentResultPageType.CreditcardSuccess:
                        case PaymentResultPageType.PromotionSuccess:
                        case PaymentResultPageType.PcashSuccess:
                            setSuccessContent();
                            break;

                        case PaymentResultPageType.CreditcardFailed:
                        case PaymentResultPageType.PcashFailed:
                            pan_CreditcardFail.Visible = true;
                            hyp_credit.NavigateUrl = ResolveUrl("~/ppon/KindPay.aspx?bid=" + TheDeal.BusinessHourGuid);
                            break;

                        case PaymentResultPageType.ZeroPriceSuccess:
                            pan_ZeroPrice.Visible = true;
                            ShareLinks();
                            if (TheDeal.GroupOrderStatus != null && !Helper.IsFlagSet(TheDeal.GroupOrderStatus.Value, GroupOrderStatus.PEZeventCouponDownload))
                            {
                                liPEZeventCouponDownload.Text = I18N.Message.PEZeventCouponDownloadMessage;
                            }
                            break;

                        default:
                            Response.Redirect("~/error-sgo.aspx");
                            break;
                    }
                }
            }
            _presenter.OnViewLoaded();
        }

        #region InterfaceMember

        public void GoToPponDefaultPage(Guid bid, string message)
        {
            if (string.IsNullOrEmpty(message))
            {
                Response.Redirect("../ppon/default.aspx?bid=" + bid.ToString());
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('" + message + "');window.location.href='" + ResolveClientUrl("~/ppon/default.aspx?bid=" + bid.ToString()) + "';", true);
            }
        }

        public void GoToDefaulPage()
        {
            Response.Redirect("~/ppon/default.aspx");
        }

        public void GoToZeroActivity()
        {
            Response.Redirect("~/ppon/ZeroActivity.aspx?bid=" + BusinessHourGuid.ToString() + (IsPreview ? "&p=y" : string.Empty));
        }

        public void RedirectToLogin()
        {
            FormsAuthentication.RedirectToLoginPage();
        }

        public void RedirectToLoginOnlyPEZ(string message, Guid bid)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('" + message + "');window.location.href='" + ResolveClientUrl("~/ppon/default.aspx?bid=" + bid) + "';", true);
        }

        public ListItemCollection GetItemsByInt(int total)
        {
            ListItemCollection lc = new ListItemCollection();
            for (int i = 1; i <= total; i++)
            {
                lc.Add(new ListItem(i.ToString(), i.ToString()));
            }

            if (total < 1)
            {
                lc.Add(new ListItem("0", "0"));
            }

            return lc;
        }

        public void SetContent(IViewPponDeal deal, 
            AccessoryGroupCollection multOption, int alreadyboughtcount, ViewPponStoreCollection stores,
            EntrustSellType entrustSell, bool isDailyRestriction)
        {
            hidPayDepartment.Value = deal.Department.ToString();

            #region User Info

            string[] badd = string.IsNullOrWhiteSpace(UserInfo) ? new string[] { } : UserInfo.Split('|');

            if (badd.Length > 0)
            {
                BuyerMobile.Text = badd[2];

                BuyerName.Text = badd[7];
                BuyerEmail.Text = badd[8];
            }

            #endregion User Info

            #region delivery type

            if (deal.DeliveryType.HasValue && int.Equals((int)DeliveryType.ToHouse, deal.DeliveryType.Value)) //好康到店到府消費方式判斷；PponItem=ToHouse
            {
                kindFreight.Visible = false;
                IsItem = true;

                List<City> cities = GetCities(null);
                List<City> receiverCities = GetReceiverCities(null, NotDeliveryIslands);
                CityCollection emptyAreas = new CityCollection();
                BuildingCollection emptyBuilding = new BuildingCollection();
                cities.Insert(0, new City { Id = 0, CityName = "請選擇" });
                receiverCities.Insert(0, new City { Id = 0, CityName = "請選擇" });
                emptyAreas.Insert(0, new City { Id = 0, CityName = "請選擇" });
                emptyBuilding.Insert(0, new Building { Guid = Guid.Empty, BuildingName = "請選擇" });

                ddlBuyerCity.DataSource = cities;
                ddlBuyerCity.DataBind();
                if (null != Request[ReceiverAddress.ClientID.Replace('_', '$')] || (badd.Length > 0 && !string.Equals("0", badd[3])))
                {
                    ddlBuyerCity.SelectedValue = null != Request[ddlReceiverCity.ClientID.Replace('_', '$')] ? Request[ddlReceiverCity.ClientID.Replace('_', '$')] : badd[3];
                    ddlBuyerArea.DataSource = GetCities(int.Parse(ddlBuyerCity.SelectedValue));
                    ddlBuyerArea.DataBind();

                    if (null == Request[ddlReceiverArea.ClientID.Replace('_', '$')] && string.Equals("0", badd[4]))
                    {
                        ddlBuyerBuilding.DataSource = emptyBuilding;
                        ddlBuyerBuilding.DataBind();
                    }
                    else
                    {
                        ddlBuyerArea.SelectedValue = null != Request[ddlReceiverArea.ClientID.Replace('_', '$')] ? Request[ddlReceiverArea.ClientID.Replace('_', '$')] : badd[4];
                        ddlBuyerBuilding.DataSource = GetBuildings(int.Parse(ddlBuyerArea.SelectedValue));
                        ddlBuyerBuilding.DataBind();

                        if (badd.Length > 0)
                        {
                            BuyerAddress.Text = badd[6];
                        }
                    }
                }
                else
                {
                    ddlBuyerCity.SelectedValue = "0";
                    ddlBuyerArea.DataSource = emptyAreas;
                    ddlBuyerArea.DataBind();
                    ddlBuyerBuilding.DataSource = emptyBuilding;
                    ddlBuyerBuilding.DataBind();
                }

                MemberDeliveryCollection Receivers = mp.MemberDeliveryGetCollection(UserId);
                Receivers.Insert(0, new MemberDelivery { AddressAlias = "選擇其他收件人", Id = 0 });
                ReceiverList.DataSource = Receivers;
                ReceiverList.DataBind();
            }
            else
            {
                IsItem = BuyerAddressPanel.Visible = ReceiverInfoPanel.Visible = false;
            }

            #endregion delivery type

            dealName = deal.EventName;
            hidtotal.Value = lprice.Text = hidprice.Value = hidsubtotal.Value = deal.ItemPrice.ToString("F0");
            subtotal = deal.ItemPrice;
            hidqty.Value = "1";
            total = deal.ItemPrice + deliveryCharge;

            #region 如果為零元檔次切換購買頁

            if (decimal.Equals(0, deal.ItemPrice))
            {
                GoToZeroActivity();
            }
            else
            {
                #region 可購買數量

                int qty = deal.MaxItemCount ?? 10;

                // 檢查進入頁面時的庫存上限
                int ordered = deal.OrderedQuantity ?? 0;
                int totalLimit = deal.OrderTotalLimit.HasValue ? (int)deal.OrderTotalLimit.Value : int.MaxValue;
                if (ordered + qty > totalLimit)
                {
                    qty = totalLimit - ordered;
                }

                if (deal.ItemDefaultDailyAmount != null)
                {
                    if (alreadyboughtcount < deal.ItemDefaultDailyAmount.Value)
                    {
                        int stillcanbuycount = deal.ItemDefaultDailyAmount.Value - alreadyboughtcount;
                        qty = qty >= stillcanbuycount ? stillcanbuycount : qty;
                    }
                    else
                    {
                        qty = 0;
                    }
                }

                if (int.Equals(0, qty))
                {
                    string message = string.Format(isDailyRestriction ? I18N.Message.DailyRestrictionLimit : I18N.Message.Alreadyboughtcount, alreadyboughtcount);
                    AlertMessageAndGoToDefault(message, deal.BusinessHourGuid);
                }
                else
                {
                    ddlQty.DataSource = GetItemsByInt(qty);
                    ddlQty.DataBind();
                }

                #endregion 可購買數量

                #region 付款方式設定

                #endregion 付款方式設定

                #region 運費

                if ((deal.BusinessHourStatus & (int)BusinessHourStatus.NoShippingFeeMessage) > 0)
                {
                    NoSippingFeeMessage = true;
                }

                // 階梯式
                if (null != TheCouponFreight && TheCouponFreight.Count > 0)
                {
                    // 規則：
                    // 1.運費=0 → 還差□份就免運費囉！
                    // 2.下一運費<目前運費 → 還差□份，運費將降為□元囉！
                    // 3.其他 → 無顯示
                    CouponFreight lowestCouponFreight = TheCouponFreight.OrderBy(x => x.FreightAmount).First();
                    bool hasZero = false;
                    decimal zeroPriceAmount = 0;
                    if (lowestCouponFreight.FreightAmount.Equals(0))
                    {
                        hasZero = true;
                        zeroPriceAmount = lowestCouponFreight.StartAmount;
                    }
                    bool find = false;
                    bool setDeliveryCharge = false;
                    decimal currentFreight = 0;
                    foreach (CouponFreight cf in TheCouponFreight.OrderBy(x => x.StartAmount))
                    {
                        if (!setDeliveryCharge)
                        {
                            deliveryCharge = cf.FreightAmount;
                        }

                        if (find)
                        {
                            if (hasZero)
                            {
                                lbldc.InnerHtml = string.Format("<span class='hint'>-還差{0}份就免運費囉！</span>", Math.Ceiling((zeroPriceAmount / deal.ItemPrice - 1)).ToString("F0"));
                            }
                            else
                            {
                                if (cf.FreightAmount < currentFreight)
                                {
                                    lbldc.InnerHtml = string.Format("<span class='hint'>-還差{0}份，運費將降為{1}元囉！</span>", Math.Ceiling((cf.StartAmount / deal.ItemPrice - 1)).ToString("F0"), cf.FreightAmount.ToString("F0"));
                                }
                                else
                                {
                                    lbldc.InnerHtml = string.Empty;
                                }
                            }

                            //hpromo.Value = lowbound;
                            lbldc.Visible = true;
                            break;
                        }

                        if (!find && deal.ItemPrice >= cf.StartAmount && cf.EndAmount > deal.ItemPrice)
                        {
                            currentFreight = cf.FreightAmount;
                            deliveryCharge = cf.FreightAmount;
                            find = true;
                        }
                    }

                    JsonSerializer serializer = new JsonSerializer();
                    hidFreight.Value = serializer.Serialize(from x in TheCouponFreight select new { startAmt = x.StartAmount, endAmt = x.EndAmount, freightAmt = x.FreightAmount });
                }
                else
                {
                    hidFreight.Value = string.Empty;
                    deliveryCharge = deal.BusinessHourDeliveryCharge;
                }
                hiddCharge.Value = hidcstatic.Value = deliveryCharge.ToString("F0");

                #endregion 運費

                #region 選項

                if (multOption.Count > 0)
                {
                    rpOption.DataSource = multOption;
                    rpOption.DataBind();
                }

                #endregion 選項

                #region 分店

                if (stores.Count > 0)
                {
                    if (MultipleBranch)
                    {
                        processBranches(stores);
                    }
                    else
                    {
                        ddlStoreCity.Visible = false;
                        ddlStoreArea.Visible = false;
                        setStore(stores);
                    }
                }
                else
                {
                    pStore.Visible = false;
                }

                #endregion 分店

                #region ATM

                if (IsATMLimitZero)
                {
                    AtmPayWay.Visible = false;
                }
                else
                {
                    DateTime AtmAllowTime = PaymentFacade.AtmAllowDateGet(Convert.ToDateTime(deal.BusinessHourOrderTimeE));

                    //如果今天日期小於販售截止日前一日的12:00, 則開啟ATM 功能
                    if (DateTime.Now < AtmAllowTime)
                    {
                        AtmPayWay.Visible = true;
                        if (IsATMAvailable)
                        {
                            rbPayWayByAtm.Enabled = ATMPayNote.Visible = true;
                            atmFull.Visible = false;
                        }
                        else
                        {
                            rbPayWayByAtm.Checked = false;
                            rbPayWayByAtm.Enabled = ATMPayNote.Visible = false;
                            atmFull.Visible = true;
                        }
                    }
                    else
                    {
                        AtmPayWay.Visible = false;
                        rbPayWayByAtm.Enabled = false;
                    }
                }

                #endregion ATM

                #region EntrustSell

                {
                    List<City> cities = GetCities(null);
                    cities.Insert(0, new City { Id = 0, CityName = "請選擇" });
                }
                //代收轉付檔次，將發票頁隱藏，開啟收據頁                
                this.EntrustSell = entrustSell;
                receiptPanel.Visible = this.EntrustSell == EntrustSellType.KindReceipt;                

                #endregion EntrustSell

                NomalPay.Visible = true;
                ZeroPay.Visible = false;
                hidIsZeroEvent.Value = "False";
            }

            #endregion 如果為零元檔次切換購買頁
        }

        private void setStore(ViewPponStoreCollection stores)
        {
            var count = 0;
            pStore.Visible = true;

            //若有一家以上分店；以下拉式選單方式顯示供使用者點選
            //但分店若只有一家；畫面上直接顯示該店家資料，不須以下拉式選單方式呈現
            if (stores.Count > 1)
            {
                ddlStore.Items.Clear();
                ddlStore.Items.Add("請選擇");
                divStore.Visible = true;
                divSingleStore.Visible = false;

                foreach (var store in stores.OrderBy(x => x.SortOrder))
                {
                    ListItem li = new ListItem(GetStoreOptionTitle(store), store.StoreGuid.ToString());
                    if (store.TotalQuantity <= 0 || store.OrderedQuantity >= store.TotalQuantity)
                    {
                        li.Attributes["disabled"] = "disabled";
                        li.Text += "(已售完)";
                    }
                    else
                    {
                        li.Attributes.Remove("disabled");
                    }

                    ddlStore.Items.Add(li);
                    count++;
                }
            }
            else if (stores.Count == 1)
            {
                divStore.Visible = false;
                divSingleStore.Visible = true;
                lblSingleStore.Text = GetStoreOptionTitle(stores[0]);
                HiddenSingleStoreGuid.Text = stores[0].StoreGuid.ToString();
                count++;
            }

            if (count == 0)
            {
                divStore.Visible = false;
                divSingleStore.Visible = false;
            }
        }

        public void GotoCreditCardPay(string ticketId)
        {
            string key = string.IsNullOrWhiteSpace(ticketId) ? string.Empty : ticketId.Split('_')[2];
            string givenName;
            string familyName = Helper.GetFamilyName(BuyerName.Text, out givenName);
            Session["AddressInfo_" + key] = string.Format("{0}|{1}|{2}|{3}^{4}|{5}|{6}|{7}",
                familyName,
                givenName,
                BuyerMobile.Text,
                (String.IsNullOrWhiteSpace(BuyerAddress.Text)
                    ? "0|0|" + Guid.Empty + "|"
                    : ddlBuyerCity.SelectedValue + "|" + Request[ddlBuyerArea.ClientID.Replace('_', '$')] + "|" +
                      Request[ddlBuyerBuilding.ClientID.Replace('_', '$')] + "|" + BuyerAddress.Text),
                ReceiverName.Text,
                ReceiverMobile.Text,
                (String.IsNullOrWhiteSpace(txtReceiptAddress.Text)
                    ? "0|0|" + Guid.Empty + "|"
                    : Request[ddlReceiptCity.UniqueID] + "|" +
                      Request[ddlReceiptArea.UniqueID] + "|" + Guid.Empty + "|" +
                      txtReceiptAddress.Text), AddReceiverInfo.Checked);
            Session["IsPaidByATM_" + key] = false;
            Response.Redirect(WebUtility.GetSiteRoot() + "/ppon/KindPay.aspx?TicketId=" + ticketId);
        }

        public void GotoATMPay(string ticketId)
        {
            string key = string.IsNullOrWhiteSpace(ticketId) ? string.Empty : ticketId.Split('_')[2];
            string givenName;
            string familyName = Helper.GetFamilyName(BuyerName.Text, out givenName);
            Session["AddressInfo_" + key] = string.Format("{0}|{1}|{2}|{3}^{4}|{5}|{6}|{7}",
                familyName,
                givenName, 
                BuyerMobile.Text, 
                (String.IsNullOrWhiteSpace(BuyerAddress.Text) ? "0|0|" + Guid.Empty.ToString() + "|" : ddlBuyerCity.SelectedValue + "|" + Request[ddlBuyerArea.ClientID.Replace('_', '$')] + "|" + Request[ddlBuyerBuilding.ClientID.Replace('_', '$')] + "|" + BuyerAddress.Text), 
                ReceiverName.Text, 
                ReceiverMobile.Text, 
                (String.IsNullOrWhiteSpace(ReceiverAddress.Text) ? "0|0|" + Guid.Empty.ToString() + "|" : ddlReceiverCity.SelectedValue + "|" + Request[ddlReceiverArea.ClientID.Replace('_', '$')] + "|" + Guid.Empty.ToString() + "|" + ReceiverAddress.Text), 
                AddReceiverInfo.Checked.ToString());
            Session["IsPaidByATM_" + key] = true;
            Response.Redirect(WebUtility.GetSiteRoot() + "/ppon/KindPay.aspx?TicketId=" + ticketId);
        }

        public void ShowTransactionPanel()
        {
            hidprice.Value = TheDeal.ItemPrice.ToString("F0");
            OrderNumber.Text = TransactionId;
            TransAmount.Text = Amount.ToString();
            TransDate.Text = DateTime.Today.ToShortDateString();

            PanelBuy.Visible = false;
            PanelCreditCradAPI.Visible = true;
            PanelResult.Visible = false;
        }

        public void ShowATMTransactionPanel()
        {
            if (int.Equals(14, ATMAccount.Length))
            {
                if (!string.IsNullOrWhiteSpace(TransactionId))
                {
                    PaymentTransaction pt = op.PaymentTransactionGet(TransactionId, PaymentType.ATM, PayTransType.Authorization);
                    int status = Helper.SetPaymentTransactionPhase(pt.Status, PayTransPhase.Requested);

                    PaymentFacade.UpdateTransaction(TransactionId, pt.OrderGuid, PaymentType.ATM, Convert.ToDecimal(Amount), string.Empty,
                        PayTransType.Authorization, DateTime.Now, ATMAccount, status, (int)PayTransResponseType.OK);

                    Response.Redirect(WebUtility.GetSiteRoot().Replace("https://", "http://") + "/ppon/KindPay.aspx?TransId=" + TransactionId + "&TicketId=" + TicketId);
                }
                else
                {
                    Response.Redirect(WebUtility.GetSiteRoot().Replace("https://", "http://") + "/ppon");
                }
            }
            else
            {
                AlertMessage("此檔好康ATM付款名額已滿，請您以刷卡方式購買本次好康");
                Response.Redirect(WebUtility.GetSiteRoot() + "ppon/KindPay.aspx?bid=" + TheDeal.BusinessHourGuid.ToString());
            }
        }

        public void SetZeroDealResult(CouponCollection coupons, GroupOrderStatus groupOrderStatus)
        {
            if (coupons.Count > 0)
            {
                foreach (Coupon c in coupons)
                {
                    if (!string.IsNullOrEmpty(c.Code) & Helper.IsFlagSet(groupOrderStatus, GroupOrderStatus.PEZevent))
                        lblCoupon.Text += string.Format(I18N.Message.PEZeventCouponFormat, c.Code) + "<br/>";
                    else
                        pnlShowCoupon.Visible = false;
                }
            }
            else
                pnlShowCoupon.Visible = false;
        }

        public void ShowResult(PaymentResultPageType result, IViewPponDeal pponDeal, int atmAmount)
        {
            PanelBuy.Visible = false;
            PanelCreditCradAPI.Visible = false;
            PanelResult.Visible = true;
            ATMOrderTotal.Text = atmAmount.ToString();

            PaymentResult = result;
            hfZ.NavigateUrl = ResolveUrl("~/ppon/fbshare_proxy.aspx?bid=" + pponDeal.BusinessHourGuid);

            SetRecommendDeals();
        }

        public void AlertMessage(string msg)
        {
            ClientScript.RegisterStartupScript(GetType(), "alerter", "alert('" + msg + "');", true);
        }

        private void AlertMessageAndGoToDefault(string msg, Guid bid)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('" + msg + "');window.location.href='" + ResolveClientUrl("~/ppon/default.aspx?bid=" + bid.ToString()) + "';", true);
        }

        public void CheckRefUrl()
        {
            if (Request.UrlReferrer != null)
            {
                if (Request.UrlReferrer.AbsolutePath.ToLower().IndexOf("bbhtodaydeal") >= 0)
                {
                    Session["rsrc"] = "BBH_ch";
                }
                else
                {
                    Session["rsrc"] = string.Empty;
                }
            }
            else
            {
                Session["rsrc"] = string.Empty;
            }
        }

        private bool IsSuccess()
        {
            return PaymentResult != PaymentResultPageType.CreditcardFailed;
        }

        public void ShowCreditcardChargeInfo()
        {
            if (PaymentResult == PaymentResultPageType.CreditcardSuccess)
            {
                ClientScript.RegisterStartupScript(GetType(), "alerterR", "alert('謝謝您！您的刷卡程序已完成並授權成功，系統會將您跳轉至17Life首頁繼續瀏覽。');", true);
            }
            else if (PaymentResult == PaymentResultPageType.CreditcardFailed)
            {
                ClientScript.RegisterStartupScript(GetType(), "alerterR", "alert('很抱歉！您的刷卡授權未成功，系統會將您跳轉至購買頁面，請您再試一次。');", true);
            }
            Response.Redirect("default.aspx");
        }

        public void ReEnterPaymentGate(PaymentType paymentType, String transId)
        {
            Response.Redirect("KindPay.aspx?TransId=" + transId + "&TicketId=" + TicketId);
        }

        public void RestartAuth()
        {
            ClientScript.RegisterStartupScript(GetType(), "alerterR", "alert('很抱歉！您的信用卡資訊有錯誤，請您再試一次。');", true);
            PanelBuy.Visible = false;
            PanelCreditCradAPI.Visible = true;
            PanelResult.Visible = false;
        }

        #endregion InterfaceMember

        #region PageEvent

        protected void rpOption_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            int count = 0;
            if (e.Item.DataItem is AccessoryGroup)
            {
                List<ViewItemAccessoryGroup> viagc = ((AccessoryGroup)e.Item.DataItem).members;
                DropDownList ddl = (DropDownList)e.Item.FindControl("ddlMultOption");
                ddl.Items.Add("請選擇");
                foreach (ViewItemAccessoryGroup v in viagc)
                {
                    ListItem li = new ListItem(v.AccessoryName, v.AccessoryGroupMemberGuid.ToString());
                    if (v.Quantity == null || v.Quantity > 0)
                    {
                        li.Attributes.Remove("disabled");
                    }
                    else
                    {
                        li.Attributes["disabled"] = "disabled";
                        li.Text += "(已售完)";
                    }

                    ddl.Items.Add(li);
                    count++;
                }
                if (count == 0)
                {
                    ddl.Visible = false;
                    Label lbl = (Label)e.Item.FindControl("lblMultOption");
                    lbl.Visible = false;
                }
            }
        }

        protected void btnPayWay_Click(object sender, EventArgs e)
        {
            if (rbPayWayByCreditCard.Checked)
            {
                IsPaidByATM = false;
            }
            else if (rbPayWayByAtm.Checked)
            {
                IsPaidByATM = true;
            }
            else
            {
                IsPaidByATM = false;
            }
            ibCheckOut_Click(sender, e);
        }

        public void ibCheckOut_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(TicketId))
            {
                int qty = hidIsZeroEvent.Value == "False" ? int.Parse(hidqty.Value) : 1;//如果是零元好康則數量自動帶1
                decimal price = decimal.TryParse(hidtotal.Value, out price) ? price : 0;

                if (!CheckBranch())
                {
                    AlertMessage("請選擇還有數量的分店");
                    ResetContent(this, null);
                    return;
                }

                // check option
                if (!checkOptions())
                {
                    AlertMessage("請選擇還有數量的選項");
                    ResetContent(this, null);
                    return;
                }

                //add for empty address
                if (IsItem && hidIsZeroEvent.Value == "False")
                {
                    if (string.IsNullOrWhiteSpace(hidReceiverAddress.Value) || hidReceiverAddress.Value.IndexOf("請選擇") >= 0)
                    {
                        AlertMessage("請選擇收件人地址");
                        ResetContent(this, null);
                        return;
                    }
                }

                //使用17Life購物金付全額仍需紀錄invoice_mode (invoice_mode不為0)
                int sc_price = Convert.ToInt32(price);

                ViewState["rb"] = DonationReceiptsType.None;//0

                foreach (RepeaterItem ri in rpOption.Items)
                {
                    DropDownList ddl = (DropDownList)ri.FindControl("ddlMultOption");
                    UserMemo += "," + ddl.SelectedItem.Text + "|" + ddl.SelectedItem.Value;
                }

                if (CheckOut != null)
                {
                    CheckOut(this, new CheckDataEventArgs
                    {
                        Quantity = qty,
                        PaidByAtm = (IsATMAvailable && rbPayWayByAtm.Checked)
                    });
                }
            }
            else
            {
                ShowTransactionPanel();
            }
        }

        private bool CheckBranch()
        {
            if (!IsItem && ddlStore.SelectedItem != null && ddlStore.SelectedItem.Text == "請選擇")
            {
                return false;
            }

            return true;
        }

        private bool checkOptions()
        {
            string Selected = string.Empty;
            foreach (RepeaterItem ri in rpOption.Items)
            {
                Selected = ((DropDownList)ri.FindControl("ddlMultOption")).SelectedItem.Text;

                if (Selected.IndexOf("請選擇") > -1 || Selected.IndexOf("已售完") > -1)
                {
                    return false;
                }
            }

            return true;
        }

        #endregion PageEvent

        #region Credit Card API

        protected void AuthSSL_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(TransactionId))
            {
                if (string.IsNullOrWhiteSpace(checkCreditCardInfo()))
                {
                    CreditCardAuthResult result =
                       CreditCardUtility.Authenticate(
                                    new CreditCardAuthObject()
                                    {
                                        TransactionId = TransactionId,
                                        Amount = Amount,
                                        CardNumber = CardNo1.Text + CardNo2.Text + CardNo3.Text + CardNo4.Text,
                                        ExpireYear = ExpYear.SelectedValue,
                                        ExpireMonth = ExpMonth.SelectedValue,
                                        SecurityCode = SecurityCode.Text,
                                        Description = TicketId.Split('_')[1]
                                    }, OrderGuid, UserName, OrderClassification.LkSite, CreditCardOrderSource.Ppon);
                    CheckResult(result);
                }
                else
                {
                    RestartAuth();
                }
            }
            else
            {
                Response.Redirect(WebUtility.GetSiteRoot().Replace("https://", "http://") + "/ppon");
            }
        }

        private void CheckResult(CreditCardAuthResult result)
        {
            PaymentTransaction pt = op.PaymentTransactionGet(TransactionId, PaymentType.Creditcard, PayTransType.Authorization);
            int status = Helper.SetPaymentTransactionPhase(pt.Status, result.TransPhase);
            int rlt = 0;
            try
            {
                rlt = int.Parse(result.ReturnCode);
            }
            catch
            {
                rlt = -9527;
            }

            if (Helper.GetPaymentTransactionPhase(pt.Status) != PayTransPhase.Created)
            {
                log.Info("trans_id: " + pt.TransId + " 被拒絕覆蓋已回覆的授權紀錄\n result: " + rlt + "\n message: " + result.TransMessage
                    + "\n api_provider: " + (int)result.APIProvider + "\n original_apiprovider: " + pt.ApiProvider + "\n status:" + pt.Status);
                if (Helper.GetPaymentTransactionPhase(pt.Status) == PayTransPhase.Successful)
                {
                    ShowPageResult(null, new ShowResultEventArgs
                    {
                        PageType = PaymentResultPageType.CreditcardSuccess
                    });
                }
                else
                {
                    Response.Redirect(WebUtility.GetSiteRoot().Replace("https://", "http://") + "/ppon/KindPay.aspx?TransId=" + TransactionId + "&TicketId=" + TicketId);
                }
            }
            else
            {
                PaymentFacade.UpdateTransaction(TransactionId, pt.OrderGuid, PaymentType.Creditcard, Convert.ToDecimal(Amount), result.AuthenticationCode,
                    result.TransType, result.OrderDate, result.TransMessage + " " + result.CardNumber, status, rlt, result.APIProvider);
                Response.Redirect(WebUtility.GetSiteRoot().Replace("https://", "http://") + "/ppon/KindPay.aspx?TransId=" + TransactionId + "&TicketId=" + TicketId);
            }
        }

        private string checkCreditCardInfo()
        {
            Regex rex = new Regex(@"\D");
            if (!int.Equals(CardNo1.MaxLength, CardNo1.Text.Length) || rex.IsMatch(CardNo1.Text))
            {
                return "card number error!!";
            }

            if (!int.Equals(CardNo2.MaxLength, CardNo2.Text.Length) || rex.IsMatch(CardNo2.Text))
            {
                return "card number error!!";
            }

            if (!int.Equals(CardNo3.MaxLength, CardNo3.Text.Length) || rex.IsMatch(CardNo3.Text))
            {
                return "card number error!!";
            }

            if (!int.Equals(CardNo4.MaxLength, CardNo4.Text.Length) || rex.IsMatch(CardNo4.Text))
            {
                return "card number error!!";
            }

            return string.Empty;
        }

        #endregion Credit Card API

        private void ShareLinks()
        {
            MemberLinkCollection mlc = mp.MemberLinkGetList(this.UserId);
            var goodWords = new List<string> { Phrase.LooksGood, Phrase.IHadHeardThat, Phrase.LetUsGoTogether, Phrase.PeopleSayThatStoreClerksAreHot, Phrase.RealBargain };
            String goodWord = goodWords.ElementAt(new Random().Next(goodWords.Count));
            hfZ.NavigateUrl = ResolveUrl("~/ppon/fbshare_proxy.aspx?bid=") + TheDeal.BusinessHourGuid;
            // TODO: to rewrite how plurk/mail referral address is generated, we only want passive short url generation, not aggressive generation that trigger with every page load for new deal
            // TODO: the preferred idea is make ajax request for short URL once user click on it
            string referralUrl = WebUtility.GetSiteRoot() + "/ppon/PponReferralRedirection.aspx?bid=" + TheDeal.BusinessHourGuid + "," + mlc[0].ExternalOrg + "|" + mlc[0].ExternalUserId;
            hpZ.NavigateUrl = "javascript: void(window.location.replace('http://www.plurk.com/?qualifier=shares&status=' .concat(encodeURIComponent('" + referralUrl + "')) .concat(' (') .concat(encodeURIComponent('"
                           + goodWord + " - " + TheDeal.EventName + "')) .concat(')')));";

           
        }

        private void setSuccessContent()
        {
            if (null == TheDeal && !string.IsNullOrWhiteSpace(TransactionId))
            {
                TheDeal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(new Guid(TicketId.Split('_')[1]));
            }

            if (TheDeal != null && !string.IsNullOrWhiteSpace(TheDeal.ItemName))
            {
                if (IsPaidByATM)
                {
                    ItemNameATM.Text = string.Format("感謝您以ATM付款方式訂購【{0}】優質好康。<br />", TheDeal.ItemName);
                }
                else
                {
                    ItemName.Text = string.Format("恭喜您成功購得【{0}】優質好康。<br />", TheDeal.ItemName);
                }
            }

            if (Page.User != null && Page.User.Identity.IsAuthenticated)
            {
                #region ATM

                ATMOrderDate.Text = DateTime.Today.ToString("yyyy-MM-dd");
                ATMOrderAccount.Text = ATMAccount;

                #endregion ATM

                #region Share

                ShareLinks();

                #endregion Share
            }

            pan_Success.Visible = true;
            pan_Success_CreditCard.Visible = !IsPaidByATM;
            pan_Success_ATM.Visible = IsPaidByATM;

            #region 行銷滿額贈活動
            if (conf.IsEnabledSpecialConsumingDiscount)
            {
                DateTime dateStart = DateTime.Parse(conf.SpecialConsumingDiscountPeriod.Split('|')[0]);
                DateTime dateEnd = DateTime.Parse(conf.SpecialConsumingDiscountPeriod.Split('|')[1]);
                if (DateTime.Now > dateStart && DateTime.Now < dateEnd)
                {
                    int amount = PromotionFacade.GetSpecialConsumingAmount(UserId);
                    Session[LkSiteSession.SpecialConsumingDiscount.ToString()] = amount;
                    KeyValuePair<int, int> consumingDiscount = PromotionFacade.GetSpecialConsumingDiscount(amount);
                    divDiscountPromotion.Visible = true;
                    liDiscountPromotion.Text = consumingDiscount.Value.ToString();
                    liDiscountPromotionCount.Text = consumingDiscount.Key.ToString();
                }
                else
                {
                    divDiscountPromotion.Visible = false;
                }
            }
            else
            {
                divDiscountPromotion.Visible = false;
            }
            #endregion
        }

        private void SetRecommendDeals()
        {
            var workCityId = int.Equals(-1, CookieCityId) ? PponCityGroup.DefaultPponCityGroup.TaipeiCity.CityId : CookieCityId;

            List<IViewPponDeal> Deals = ViewPponDealManager.DefaultManager.ViewPponDealGetListForPponByCityWithDataSlot(workCityId);
            if (!(Deals.Count > 3))
            {
                Deals.AddRange(ViewPponDealManager.DefaultManager.ViewPponDealGetListForPponByCityWithDataSlot(PponCityGroup.DefaultPponCityGroup.AllCountry.CityId));
            }
            var sideDeals = Deals.Where(x => !Guid.Equals(TheDeal.BusinessHourGuid, x.BusinessHourGuid));
            int k = 3;
            foreach (ViewPponDeal vpd in sideDeals)
            {
                if (int.Equals(0, k))
                {
                    break;
                }

                if (int.Equals(3, k))
                {
                    rt1.Text = vpd.ItemName;
                    rt1.NavigateUrl = rl1.NavigateUrl = rb1.NavigateUrl = ResolveUrl("~/ppon/default.aspx?bid=" + vpd.BusinessHourGuid.ToString());
                    op1.Text = Convert.ToInt32(vpd.ItemOrigPrice).ToString("D");
                    dp1.Text = Convert.ToInt32(vpd.ItemPrice).ToString("D");
                    discount1.Text = vpd.ItemOrigPrice > 0 ? ViewPponDealManager.GetDealDiscountString(Helper.IsFlagSet(vpd.BusinessHourStatus, BusinessHourStatus.NoDiscountShown), vpd.ItemPrice, vpd.ItemOrigPrice) : "--";
                    rp1.ImageUrl = ImageFacade.GetMediaPathsFromRawData(vpd.EventImagePath, MediaType.PponDealPhoto).DefaultIfEmpty(string.Empty).First();
                }

                if (int.Equals(2, k))
                {
                    rt2.Text = vpd.ItemName;
                    rt2.NavigateUrl = rl2.NavigateUrl = rb2.NavigateUrl = ResolveUrl("~/ppon/default.aspx?bid=" + vpd.BusinessHourGuid.ToString());
                    op2.Text = Convert.ToInt32(vpd.ItemOrigPrice).ToString("D");
                    dp2.Text = Convert.ToInt32(vpd.ItemPrice).ToString("D");
                    discount2.Text = vpd.ItemOrigPrice > 0 ? ViewPponDealManager.GetDealDiscountString(Helper.IsFlagSet(vpd.BusinessHourStatus, BusinessHourStatus.NoDiscountShown), vpd.ItemPrice, vpd.ItemOrigPrice) : "--";
                    rp2.ImageUrl = ImageFacade.GetMediaPathsFromRawData(vpd.EventImagePath, MediaType.PponDealPhoto).DefaultIfEmpty(string.Empty).First();
                }

                if (int.Equals(1, k))
                {
                    rt3.Text = vpd.ItemName;
                    rt3.NavigateUrl = rl3.NavigateUrl = rb3.NavigateUrl = ResolveUrl("~/ppon/default.aspx?bid=" + vpd.BusinessHourGuid.ToString());
                    op3.Text = Convert.ToInt32(vpd.ItemOrigPrice).ToString("D");
                    dp3.Text = Convert.ToInt32(vpd.ItemPrice).ToString("D");
                    discount3.Text = vpd.ItemOrigPrice > 0 ? ViewPponDealManager.GetDealDiscountString(Helper.IsFlagSet(vpd.BusinessHourStatus, BusinessHourStatus.NoDiscountShown), vpd.ItemPrice, vpd.ItemOrigPrice) : "--";
                    rp3.ImageUrl = ImageFacade.GetMediaPathsFromRawData(vpd.EventImagePath, MediaType.PponDealPhoto).DefaultIfEmpty(string.Empty).First();
                }

                k--;
            }
        }

        protected void GetShareLink(object sender, EventArgs e)
        {
            if (Page.User == null || !Page.User.Identity.IsAuthenticated)
            {
                Session[LkSiteSession.ShareLinkText.ToString()] = "y";
                FormsAuthentication.RedirectToLoginPage();
            }
            else
            {
                if (Session[LkSiteSession.ShareLinkText.ToString()] != null)
                {
                    Session.Remove(LkSiteSession.ShareLinkText.ToString());
                }

                ScriptManager.RegisterStartupScript(this, typeof(Button), "showshare", "$(document).ready(function(){$.blockUI({ message: $('#Referral'), css: { width: '700px', height: '440px', top: ($(window).height() - 420) / 2 + 'px', left: ($(window).width() - 700) / 2 + 'px'} });});", true);
            }
        }

        private static List<City> GetCities(int? parentId)
        {
            List<City> cityList;
            if (parentId == null)
            {
                cityList = CityManager.Citys.Where(x => x.Code != "SYS").ToList();
            }
            else
            {
                cityList = CityManager.TownShipGetListByCityId(parentId.Value);
            }

            return cityList;
        }

        /// <summary>
        /// 無離島的收件人地址
        /// </summary>
        private static List<City> GetReceiverCities(int? parentId, bool notDeliveryIslands)
        {
            List<City> cityList;
            if (notDeliveryIslands)
            {
                if (parentId == null)
                {
                    cityList = CityManager.Citys.WithoutIsland().Where(x => x.Code != "SYS").ToList();
                }

                else
                {
                    cityList = CityManager.TownShipGetListByParentCityIdWithoutIsland(parentId.Value);
                }
            }
            else
            {
                cityList = GetCities(parentId);
            }
            return cityList;
        }

        private static BuildingCollection GetBuildings(int cityId)
        {
            return lp.BuildingGetListByArea(cityId);
        }

        protected void ddlBuyerCity_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlBuyerArea.DataSource = GetCities(int.Parse(ddlBuyerCity.SelectedValue));
            ddlBuyerArea.DataBind();
            ddlBuyerArea.Items.Insert(0, new ListItem("請選擇", "0"));
            ddlBuyerArea.SelectedIndex = 0;
            emptyBuilding(ddlBuyerBuilding);
        }

        protected void ddlBuyerArea_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlBuyerBuilding.DataSource = GetBuildings(int.Parse(ddlBuyerArea.SelectedValue));
            ddlBuyerBuilding.DataBind();
        }

        private void emptyBuilding(DropDownList ddl)
        {
            ddl.DataSource = new ViewPponStoreCollection();
            ddl.DataBind();
            ddl.Items.Insert(0, new ListItem("請選擇", "0"));
        }

        protected void ddlStoreCity_SelectedIndexChanged(object sender, EventArgs e)
        {
            int cityId = int.Parse(ddlStoreCity.SelectedValue);
            Hashtable hts = new Hashtable();
            IList<string> stores = hidMultiBranch.Value.Split('|');
            IList<string> sTemp;
            foreach (string s in stores)
            {
                if (s.IndexOf(cityId + ",") >= 0)
                {
                    sTemp = s.Split(",");
                    if (!hts.Contains(sTemp[2]))
                    {
                        hts.Add(sTemp[2], sTemp[3]);
                    }
                }
            }

            ddlStoreArea.DataSource = hts;
            ddlStoreArea.DataTextField = "Value";
            ddlStoreArea.DataValueField = "Key";
            ddlStoreArea.DataBind();
            ddlStoreArea.Items.Insert(0, new ListItem("請選擇", "0"));
            ddlStoreArea.SelectedIndex = 0;
            emptyBuilding(ddlStore);
        }

        protected void ddlStoreArea_SelectedIndexChanged(object sender, EventArgs e)
        {
            int areaId = int.Parse(ddlStoreArea.SelectedValue);

            if (int.Equals(0, areaId))
            {
                emptyBuilding(ddlStore);
            }
            else
            {
                Hashtable hts = new Hashtable();
                IList<string> stores = hidMultiBranch.Value.Split('|');
                IList<string> sTemp;
                foreach (string s in stores)
                {
                    if (s.IndexOf(areaId + ",") > 0)
                    {
                        sTemp = s.Split(",");
                        if (!hts.Contains(sTemp[4]))
                        {
                            hts.Add(sTemp[4], sTemp[5]);
                        }
                    }
                }

                ddlStore.DataSource = hts;
                ddlStore.DataTextField = "Value";
                ddlStore.DataValueField = "Key";
                ddlStore.DataBind();
                ddlStore.Items.Insert(0, new ListItem("請選擇", "0"));
                ddlStore.SelectedIndex = 0;
            }
        }

        private void processBranches(ViewPponStoreCollection stores)
        {
            IList<string> branches = new List<string>();
            Hashtable branchCity = new Hashtable();
            foreach (ViewPponStore store in stores.OrderBy(x => x.CityId))
            {
                string phone = store.CityId + "," + store.CityName + "," + store.TownshipId + "," + store.TownshipName + "," + store.StoreGuid.ToString() + "," + store.StoreName + (string.IsNullOrEmpty(store.Phone) ? string.Empty : (" 預約專線" + store.Phone));
                if (store.TotalQuantity <= 0 || store.OrderedQuantity >= store.TotalQuantity)
                {
                    phone += "(已售完)";
                }

                branches.Add(phone);

                if (!branchCity.Contains(store.CityId))
                {
                    branchCity.Add(store.CityId, store.CityName);
                }
            }

            ddlStoreCity.DataSource = branchCity;
            ddlStoreCity.DataTextField = "Value";
            ddlStoreCity.DataValueField = "Key";
            ddlStoreCity.DataBind();
            ddlStoreCity.Items.Insert(0, new ListItem("請選擇", "0"));

            if (int.Equals(1, branchCity.Count))
            {
                ddlStoreCity_SelectedIndexChanged(this, EventArgs.Empty);
            }
            else
            {
                emptyBuilding(ddlStore);
            }

            if (branches.Count > 0)
            {
                hidMultiBranch.Value = string.Join("|", branches);
            }
        }

        [WebMethod]
        public static string GetZones(string pid, bool notDeliveryIslands)
        {
            try
            {
                List<City> cities = (string.IsNullOrEmpty(pid) || string.Equals("0", pid)) ? new List<City>() : GetReceiverCities(int.Parse(pid), notDeliveryIslands);
                cities.Insert(0, new City { Id = 0, CityName = "請選擇" });

                var jsonCol = from x in cities select new { id = x.Id, name = x.CityName };

                return new JsonSerializer().Serialize(jsonCol);
            }
            catch (Exception e)
            {
                WebUtility.LogExceptionAnyway(e);
            }

            return null;
        }

        [WebMethod]
        public static string GetLocations(int cid)
        {
            BuildingCollection buildings = int.Equals(0, cid) ? new BuildingCollection() : GetBuildings(cid);
            buildings.Insert(0, new Building { Guid = Guid.Empty, BuildingName = "請選擇" });
            var jsonCol = from x in buildings select new { id = x.Guid, name = x.BuildingName };

            return new JsonSerializer().Serialize(jsonCol);
        }
    }
}
﻿using System;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.UI;

namespace LunchKingSite.Web.Ppon
{
    public class PponPayBase : MemberPage
    {
        /// <summary>
        /// 分店名稱+地址
        /// </summary>
        /// <param name="store"></param>
        /// <returns></returns>
        protected string GetStoreOptionTitle(ViewPponStore store)
        {
            return string.Format("{0} {1}{2}{3}",
                store.StoreName, store.CityName, store.TownshipName, store.AddressString);
        }
    }
}
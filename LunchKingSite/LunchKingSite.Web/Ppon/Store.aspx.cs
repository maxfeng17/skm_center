﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Models;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.I18N;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Seller = LunchKingSite.DataOrm.Seller;

namespace LunchKingSite.Web.Ppon
{
    public partial class Store : BasePage, IStoreView
    {

        #region Props

        private StorePresenter _presenter;

        public StorePresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        /// <summary>
        /// rsrc參數
        /// </summary>
        public string Rsrc
        {
            get
            {
                if (Page.RouteData.Values["rsrc"] != null)
                {
                    return Page.RouteData.Values["rsrc"].ToString();
                }
                else if (!string.IsNullOrWhiteSpace(Request.QueryString["rsrc"]))
                {
                    return Request.QueryString["rsrc"];
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        public string GoogleApiKey
        {
            get
            {
                return config.GoogleMapsAPIKey;
            }
        }

        public string FB_AppId
        {
            get
            {
                return config.FacebookApplicationId;
            }
        }
        /// <summary>
        /// App Title or CouponUsage
        /// </summary>
        public string AppTitle { set; get; }
        /// <summary>
        /// meta og:title content
        /// </summary>
        public string OgTitle { set; get; }
        /// <summary>
        /// meta og:url content
        /// </summary>
        public string OgUrl { set; get; }
        /// <summary>
        /// meta og:description content
        /// </summary>
        public string OgDescription { set; get; }
        /// <summary>
        /// meta og:image content
        /// </summary>
        public string OgImage { set; get; }
        /// <summary>
        /// link rel=canonical href
        /// </summary>
        public string LinkCanonicalUrl { set; get; }
        /// <summary>
        /// link rel=image_src href
        /// </summary>
        public string LinkImageSrc { set; get; }

        public bool ShowEventEmail { get; set; }

        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();

        public static ISysConfProvider SystemConfig
        {
            get
            {
                return config;
            }
        }

        public string iOSAppId
        {
            get
            {
                return config.iOSAppId;
            }
        }

        public string AndroidAppId
        {
            get
            {
                return config.AndroidAppId;
            }
        }

        public void NoRobots()
        {
            phNorobots.Visible = true;
        }

        public bool IsNewMobileSetting
        {
            get { return config.IsNewMobileSetting; }
        }

        /// <summary>
        /// 由master page取得的cityid
        /// </summary>
        private int _cityId;
        public int CityId
        {
            get
            {
                return Master.SelectedCityId;
            }
            set
            {
                Master.SelectedPponCity = PponCityGroup.DefaultPponCityGroup.GetPponCityByCityId(value);
            }
        }

        public int CategoryId
        {
            get { return PponCityGroup.DefaultPponCityGroup.GetPponCityByCityId(CityId).CategoryId; }
        }
        public int CategoryID
        {
            get
            {
                string cat_querystring = string.Empty;
                int cat;
                if (Page.RouteData.Values["cat"] != null)
                {
                    cat_querystring = Page.RouteData.Values["cat"].ToString();
                }
                else if (!string.IsNullOrWhiteSpace(Request.QueryString["cat"]))
                {
                    cat_querystring = Request.QueryString["cat"];
                }

                if (!string.IsNullOrWhiteSpace(cat_querystring) && int.TryParse(cat_querystring, out cat))
                {
                    return cat;
                }
                else
                {
                    return CategoryManager.GetDefaultCategoryNode().CategoryId;
                }
            }
        }

        private int _dealCityId;
        public int DealCityId
        {
            set
            {
                _dealCityId = value;
            }
            get
            {
                if (_dealCityId == 0)
                {
                    return Master.SelectedCityId;
                }
                else
                {
                    return _dealCityId;
                }
            }
        }

        public int TravelCategoryId
        {
            get
            {
                HttpCookie cookie = Request.Cookies.Get(LkSiteCookie.TravelCategoryId.ToString("g"));

                if (cookie == null)
                {
                    cookie = CookieManager.NewCookie(
                        LkSiteCookie.TravelCategoryId.ToString("g"),
                        CategoryManager.GetDefaultCategoryNode().CategoryId.ToString(),
                        DateTime.Now.AddDays(1)
                    );
                    Response.Cookies.Add(cookie);
                }

                int cid = int.TryParse(cookie.Value, out cid) ? cid : CategoryManager.GetDefaultCategoryNode().CategoryId;
                return cid == 0 ? CategoryManager.GetDefaultCategoryNode().CategoryId : cid;
            }
        }

        public int FemaleCategoryId
        {
            get
            {
                HttpCookie cookie = Request.Cookies.Get(LkSiteCookie.FemaleCategoryId.ToString("g"));

                if (cookie == null)
                {
                    cookie = CookieManager.NewCookie(
                        LkSiteCookie.FemaleCategoryId.ToString("g"),
                        CategoryManager.Default.FemaleTaipei.CategoryId.ToString(),
                        DateTime.Now.AddDays(1)
                    );
                    Response.Cookies.Add(cookie);
                }

                int cid = int.TryParse(cookie.Value, out cid) ? cid : CategoryManager.Default.FemaleTaipei.CategoryId;
                return cid == 0 ? CategoryManager.Default.FemaleTaipei.CategoryId : cid;
            }
        }

        public int SubRegionCategoryId
        {
            get
            {


                string subcategoryString = string.Empty;
                if (Request.QueryString["subc"] != null)
                {
                    subcategoryString = Request.QueryString["subc"];
                }
                HttpCookie cookie = Request.Cookies.Get(LkSiteCookie.SubRegionCategroyId.ToString("g"));
                if (cookie == null)
                {
                    cookie = CookieManager.NewCookie(
                        LkSiteCookie.SubRegionCategroyId.ToString("g"),
                        subcategoryString,
                        DateTime.Now.AddDays(1)
                    );
                    Response.Cookies.Add(cookie);
                }
                else
                {
                    cookie.Value = subcategoryString;
                }
                int cid = int.TryParse(cookie.Value, out cid) ? cid : CategoryManager.GetDefaultCategoryNode().CategoryId;
                return cid == 0 ? CategoryManager.GetDefaultCategoryNode().CategoryId : cid;
            }
        }


        public bool IsMobileBroswer
        {
            get
            {
                return CommonFacade.ToMobileVersion();
            }
        }

        private bool _isKindDeal;

        public bool IsKindDeal
        {
            get
            {
                return _isKindDeal;
            }
            set
            {
                _isKindDeal = value;
            }
        }

        public DateTime Now
        {
            get
            {
                return DateTime.Now;
            }
        }

        public string EdmPopUpCacheName { get; set; }
        
        public Guid SellerGuid
        {
            get
            {
                if (!string.IsNullOrEmpty(Request["sid"]))
                {
                    return new Guid(Request.QueryString["sid"]);
                }
                else
                {
                    return Guid.Empty;
                }
            }
        }
        public string SellerName { get; set; }
        public string SellerIntroduct
        {
            get
            {
                return liSellerIntroduct.Text;
            }
            set
            {
                liSellerIntroduct.Text = value;
            }
        }
        #endregion Props

        #region AppLinks Meta
        /// <summary>
        /// Add Meta Tag Property
        /// </summary>
        /// <param name="property"></param>
        /// <param name="content"></param>
        protected void AddMetaProperty(string property, string content)
        {
            HtmlMeta metaObject = new HtmlMeta();
            metaObject.Attributes.Add("property", property);
            metaObject.Attributes.Add("content", content);

            this.phAppLinks.Controls.Add(metaObject);
            this.phAppLinks.Controls.Add(new LiteralControl(Environment.NewLine));
        }
        #endregion


        #region page
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (!Presenter.OnViewInitialized())
                {
                    Response.Redirect("default.aspx");
                }
            }
            this.Presenter.OnViewLoaded();

            if (_cityId == 0)
            {
                _cityId = CityId;
            }
        }

        protected void RandomPponNewsInit(object sender, EventArgs e)
        {
            int tempCity = _cityId == 0 ? CityId : _cityId;
            saleB.CityId = news.CityId = Master.PponNews.CityId = outsideAD.CityId = insideAD.CityId = active.CityId = tempCity;
            saleB.Type = news.Type = Master.PponNews.Type = outsideAD.Type = insideAD.Type = active.Type = RandomCmsType.PponRandomCms;
        }

        protected void rpt_CategoryDealsBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is MultipleMainDealPreview || e.Item.DataItem is ViewPponDeal)
            {
                IViewPponDeal deal = null;
                PponCity pponcity = null;

                if (e.Item.DataItem is MultipleMainDealPreview)
                {
                    deal = ((MultipleMainDealPreview)e.Item.DataItem).PponDeal;
                    pponcity = PponCityGroup.DefaultPponCityGroup.GetPponCityByCityId(((MultipleMainDealPreview)e.Item.DataItem).CityID);
                }

                if (e.Item.DataItem is ViewPponDeal)
                {
                    deal = (ViewPponDeal)e.Item.DataItem;
                }

                if (deal.CityList.Contains(CategoryManager.Default.Piinlife.CityId.ToString()))
                {
                    HtmlGenericControl divppon1 = e.Item.FindControl("divppon1") as HtmlGenericControl;
                    HtmlGenericControl divppon2 = e.Item.FindControl("divppon2") as HtmlGenericControl;
                    HtmlGenericControl divppon3 = e.Item.FindControl("divppon3") as HtmlGenericControl;

                    if (divppon1 != null)
                        divppon1.Attributes.Add("class", "forsdeal store-info pinn-info");
                    if (divppon2 !=null)
                        divppon2.Visible = false;
                    if (divppon3 !=null)
                        divppon3.Visible = false;
                }

                int index = e.Item.ItemIndex;
                Literal clit_CityName = (Literal)e.Item.FindControl("lit_CityName");
                Literal clit_CityName2 = (Literal)e.Item.FindControl("lit_CityName2");
                DealTimer counTimer = (DealTimer)e.Item.FindControl("timer");
                Literal discount_2 = (Literal)e.Item.FindControl("lit_Discount_2");
                Literal discount_1 = (Literal)e.Item.FindControl("lit_Discount_1");
                Literal deal_Label_1 = (Literal)e.Item.FindControl("lit_DealLabel_1");
                Literal deal_Label_2 = (Literal)e.Item.FindControl("lit_DealLabel_2");

                if (discount_1 == null)
                {
                    discount_1 = new Literal();
                }
                if (discount_2 == null)
                {
                    discount_2 = new Literal();
                }

                _isKindDeal = Helper.IsFlagSet(deal.GroupOrderStatus ?? 0, GroupOrderStatus.KindDeal);

                Repeater rpt = (Repeater)sender;
                //貼上檔次 Icon 標籤
                string icon_string = (rpt.ID == "rpt_MHotSale_Deals") ? PponFacade.GetDealIconHtmlContent(deal, 2) : PponFacade.GetDealIconHtmlContentList(deal, 2);
                if (deal_Label_1 != null)
                {
                    deal_Label_1.Text = icon_string;
                }
                if (deal_Label_2 != null)
                {
                    deal_Label_2.Text = icon_string;
                }

                if (clit_CityName != null)
                {
                    clit_CityName.Text = deal.IsMultipleStores ?
                       "<div class='hover_place'><span class='hover_place_text'><i class='fa fa-map-marker'></i>" + deal.HoverMessage + "</span></div>" : string.Empty;
                }
                if (clit_CityName2 != null)
                {
                    clit_CityName2.Text = ViewPponDealManager.DefaultManager.GetCityNameOrTravelPlace(
                    pponcity, deal.BusinessHourGuid);
                }

                if (counTimer != null)
                {
                    counTimer.EnableBuyCounter = false;
                    counTimer.TwentyFourHours = SystemConfig.TwentyFourHours;
                    counTimer.PponDeal = deal;
                    counTimer.DataBind();
                }

                if (_isKindDeal)
                {
                    discount_1.Text = discount_2.Text = "公益";
                }
                else if (Helper.IsFlagSet(deal.BusinessHourStatus, BusinessHourStatus.NoDiscountShown)
                      || Helper.IsFlagSet(deal.BusinessHourStatus, BusinessHourStatus.PriceZeorShowOriginalPrice))
                {
                    discount_1.Text = discount_2.Text = "特選";
                }
                else if (deal.ItemPrice == 0)
                {
                    if (CityId.Equals(PponCityGroup.DefaultPponCityGroup.Family.CityId) && deal.ExchangePrice.HasValue && deal.ExchangePrice.Value > default(decimal))
                    {
                        if (Helper.IsFlagSet(deal.BusinessHourStatus, BusinessHourStatus.NoDiscountShown))
                        {
                            discount_1.Text = discount_2.Text = "特選";
                        }
                        else
                        {
                            discount_2.Text = ViewPponDealManager.GetDealDiscountString(Helper.IsFlagSet(deal.BusinessHourStatus, BusinessHourStatus.NoDiscountShown), CheckZeroPriceToShowPrice(deal), deal.ItemOrigPrice);
                            discount_1.Text = discount_2.Text + "折";
                            discount_2.Text += "<span class=\"smalltext\">折</span>";
                        }
                    }
                    else
                    {
                        discount_1.Text = discount_2.Text = "優惠";
                    }

                    Panel pnsale = (Panel)e.Item.FindControl("pn_sale");
                    if (pnsale != null)
                    {
                        pnsale.Visible = false;
                    }
                }
                else
                {
                    string discount = ViewPponDealManager.GetDealDiscountString(Helper.IsFlagSet(deal.BusinessHourStatus, BusinessHourStatus.NoDiscountShown), CheckZeroPriceToShowPrice(deal), deal.ItemOrigPrice);
                    int length = discount.Length;
                    if (length > 0)
                    {
                        discount_2.Text = discount.Substring(0, 1);
                        if (discount.IndexOf('.') != -1 && length > 2)
                        {
                            discount_2.Text += discount.Substring(1, length - 1);
                        }
                        discount_1.Text = discount_2.Text + "折";
                        discount_2.Text += "<span class=\"smalltext\">折</span>";
                    }
                }

                Literal litInstallmentPriceLabel = (Literal)e.Item.FindControl("litInstallmentPriceLabel");
                if (litInstallmentPriceLabel != null)
                {
                    if (config.InstallmentPayEnabled == false)
                    {
                        litInstallmentPriceLabel.Text = "<p>好康價</p>";
                    }
                    else if (deal.Installment3months.GetValueOrDefault() == false)
                    {
                        litInstallmentPriceLabel.Text = "<p>好康價</p>";
                    }
                    else
                    {
                        litInstallmentPriceLabel.Text = "<p class='price_installment'>分期0利率</p>";
                    }
                }
            }
        }

        private string SetCountdown(DateTime timeE)
        {
            TimeSpan ts = timeE - DateTime.Now;
            int dayUntil = ts.Days;
            int hourUntil = ts.Hours;
            int minutesUntil = ts.Minutes;
            int secondsUntil = ts.Seconds;

            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("<div class='TimeDayCounter'>{0}</div>", dayUntil.ToString() +"天");
            sb.AppendFormat("<div class='TimeConDigit'>{0}</div>", hourUntil);
            sb.AppendFormat("<div class='TimeConUnit'>{0}</div>", Phrase.Hour);
            sb.AppendFormat("<div class='TimeConDigit'>{0}</div>", minutesUntil);
            sb.AppendFormat("<div class='TimeConUnit'>{0}</div>", "分");
            sb.AppendFormat("<div class='TimeConDigit'>{0}</div>", secondsUntil);
            sb.AppendFormat("<div class='TimeConUnit'>{0}</div>", Phrase.Second);
            return sb.ToString();
        }
        #endregion page

        #region interface methods
        public void SetCity(int value)
        {
            CityId = _cityId = value;
        }

        public void SetMutilpStoreDeals(ViewPponDealCollection storedeals)
        {
            rpt_Store_Deals.DataSource = storedeals;
            rpt_Store_Deals.DataBind();

            this.Page.Title = SellerName + SystemConfig.DefaultTitle;
            this.Page.MetaDescription = SystemConfig.DefaultMetaDescription;

            #region SEO
            //The Open Graph protocol (facebook 分享參數)
            this.OgUrl = this.LinkCanonicalUrl = new Uri(HttpContext.Current.Request.Url.ToString()).AbsoluteUri;
            this.OgTitle = SellerName + " - " + SystemConfig.DefaultMetaOgTitle;
            this.OgImage = this.LinkImageSrc = SystemConfig.DefaultMetaOgImage;
            this.OgDescription = SystemConfig.DefaultMetaOgDescription;
            #endregion SEO
        }

        public void SetSellerStoreList(string Availability)
        {
            avc.Availablity = ProviderFactory.Instance().GetSerializer().Deserialize<AvailableInformation[]>(Availability);
            if (avc.Availablity.Length > 0)
            {
                avc.DataBind();
            }
            else
            {
                Response.Redirect("default.aspx");
            }
        }

        /// <summary>
        /// 熱銷檔次
        /// </summary>
        /// <param name="hotdeals"></param>
        public void SetHotSaleMutilpMainDeals(List<MultipleMainDealPreview> hotdeals)
        {
            rpt_SideDeals.DataSource = hotdeals;
            rpt_SideDeals.DataBind();
        }
        /// <summary>
        /// 今日開賣檔次
        /// </summary>
        /// <param name="toDaydeals"></param>
        public void SetToDayMutilpMainDeals(List<MultipleMainDealPreview> todaymultiplemaindeals)
        {
            rpt_SideTodayDeals.DataSource = todaymultiplemaindeals;
            rpt_SideTodayDeals.DataBind();
        }
        /// <summary>
        /// 最後倒數檔次
        /// </summary>
        /// <param name="LastDaydeals"></param>
        public void SetLastDayMutilpMainDeals(List<MultipleMainDealPreview> lastdaymultiplemaindeals)
        {
            rpt_SideLastDayDeals.DataSource = lastdaymultiplemaindeals;
            rpt_SideLastDayDeals.DataBind();
        }

        public void SetEventMainActivities(KeyValuePair<EventActivity, CmsRandomCityCollection> pair)
        {
            Cache.Remove(EdmPopUpCacheName);
            //修正跳窗EDM暫存誤差
            double CacheMin = Math.Floor((pair.Key.EndDate - DateTime.Now).TotalMinutes);
            CacheMin = (CacheMin > 60 ? 60 : CacheMin);
            Cache.Insert(EdmPopUpCacheName, pair, null, DateTime.Now.AddMinutes(CacheMin), TimeSpan.Zero, System.Web.Caching.CacheItemPriority.NotRemovable, null);
            if (!string.IsNullOrWhiteSpace(Rsrc) && pair.Key.ExcludeCpaList.Contains(Rsrc))
            {
                ShowEventEmail = false;
            }
            else if (!pair.Value.Any(x => x.CityId == CityId))
            {
                ShowEventEmail = false;
            }
            else
            {
                ShowEventEmail = true;
            }
        }

        #endregion  interface methods

        #region other methods
        public decimal CheckZeroPriceToShowPrice(IViewPponDeal deal)
        {
            return PponFacade.CheckZeroPriceToShowPrice(deal, CityId);
        }
        #endregion other methods

    }
}
﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Models;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using PaymentType = LunchKingSite.Core.PaymentType;

namespace LunchKingSite.Web.Ppon
{
    public partial class ZeroActivity : PponPayBase, IPponZeroActivityView
    {
        #region Event

        public event EventHandler<EventArgs> CheckOut = null;

        #endregion Event

        public static ISysConfProvider conf = ProviderFactory.Instance().GetConfig();
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(ZeroActivity));

        #region properties

        #region private

        private PponZeroActivityPresenter _presenter;

        #endregion private

        #region public

        public bool IsShowSKMFilter
        {
            get { return conf.IsShowSKMFilter; }
        }

        public bool IsMobileAuthed
        {
            get { return MemberFacade.IsMemberMobileAuthed(User.Identity.Name); }
        }

        public Guid BusinessHourGuid
        {
            get
            {
                Guid ret = Guid.Empty;
                if (Request["bid"] != null)
                {
                    try { ret = new Guid(Request["bid"]); }
                    catch { ret = Guid.Empty; }
                }
                return ret;
            }
        }

        public Guid StoreGuid 
        {
            get 
            {
                Guid storeGuid = Guid.Empty;
                if (!string.IsNullOrEmpty(hidSingleStoreGuid.Value))
                {
                    Guid.TryParse(hidSingleStoreGuid.Value, out storeGuid);
                }
                else if (!string.IsNullOrEmpty(ddlStore.SelectedValue)) 
                {
                    Guid.TryParse(ddlStore.SelectedValue, out storeGuid);
                }
                return storeGuid;
            }
        }

        public int CookieCityId
        {
            get
            {
                HttpCookie cookie = Request.Cookies.Get(LkSiteCookie.CityId.ToString("g"));

                if (cookie != null)
                {
                    try
                    {
                        return int.Parse(cookie.Value);
                    }
                    catch (Exception)
                    {
                        return -1;
                    }
                }

                return -1;
            }
        }

        public List<CategoryNode> DealChannelList { get; set; }

        public OrderFromType FromType
        {
            get
            {
                string userAgent = Request.UserAgent;
                if (Request.Browser.IsMobileDevice || userAgent.Contains(MobileOsType.Android.ToString(), StringComparison.OrdinalIgnoreCase))
                {
                    if (userAgent.Contains(MobileOsType.Android.ToString(), StringComparison.OrdinalIgnoreCase))
                    {
                        return OrderFromType.ByMwebAndroid;
                    }
                    else if (userAgent.Contains("ipad", StringComparison.OrdinalIgnoreCase))
                    {
                        return OrderFromType.ByMwebiPad;
                    }
                    else if (userAgent.Contains("iphone", StringComparison.OrdinalIgnoreCase))
                    {
                        return OrderFromType.ByMwebiPhone;
                    }
                    else if (userAgent.Contains("ipod", StringComparison.OrdinalIgnoreCase))
                    {
                        return OrderFromType.ByMwebiPod;
                    }
                }
                return OrderFromType.ByWeb;
            }
        }

        public string IsHami
        {
            get
            {
                if (Session["HamiTicketId"] == null)
                {
                    return null;
                }

                return Session["HamiTicketId"].ToString();
            }
        }

        public string ReferrerSourceId
        {
            get
            {
                return Request.Cookies[LkSiteCookie.ReferrerSourceId.ToString()] != null ? Request.Cookies[LkSiteCookie.ReferrerSourceId.ToString()].Value : null;
            }
        }

        public bool IsPreview
        {
            get
            {
                if (Page.User.IsInRole("production"))
                {
                    return (string.IsNullOrWhiteSpace(Request["p"]) || !string.Equals("y", Request["p"])) ? false : true;
                }
                else
                {
                    return false;
                }
            }
        }

        public CpaClientMain<CpaClientEvent<CpaClientDeail>> NewCpa
        {
            get
            {
                if (Session[LkSiteSession.NewCpa.ToString()] != null)
                {
                    return (CpaClientMain<CpaClientEvent<CpaClientDeail>>)Session[LkSiteSession.NewCpa.ToString()];
                }
                else
                {
                    return null;
                }
            }
            set
            {
                Session[LkSiteSession.NewCpa.ToString()] = value;
            }
        }

        public PaymentResultPageType PaymentResult
        {
            get
            {
                return ViewState["pr"] != null ? (PaymentResultPageType)ViewState["pr"] : PaymentResultPageType.Initial;
            }
            set
            {
                ViewState["pr"] = value;
            }
        }

        public PponZeroActivityPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        public string SessionId
        {
            get
            {
                return Session.SessionID;
            }
        }

        public IViewPponDeal TheDeal { get; set; }

        public string RsrcSession
        {
            get
            {
                if (Session["rsrc"] != null)
                {
                    return Session["rsrc"].ToString();
                }
                else
                {
                    return null;
                }
            }
        }

        public string TicketId
        {
            get
            {
                if (string.IsNullOrWhiteSpace(Request.QueryString["TicketId"]))
                {
                    return string.Empty;
                }

                return Request.QueryString["TicketId"];
            }
        }

        public string TransactionId
        {
            get
            {
                if (string.IsNullOrWhiteSpace(Request.QueryString[LkSiteSession.TransId.ToString()]))
                {
                    return string.Empty;
                }

                return Request.QueryString[LkSiteSession.TransId.ToString()].ToString();
            }
        }

        public string UserName
        {
            get { return User.Identity.Name; }
        }
        
        #endregion public

        #endregion properties

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (!_presenter.OnViewInitialized())
                {
                    Response.Redirect("~/error-sgo.aspx");
                    return;
                }
                else
                {
                    #region 付款狀況

                    switch (PaymentResult) 
                    {
                        case PaymentResultPageType.Initial:
                            break;

                        case PaymentResultPageType.ZeroPriceFailed:
                            pan_Fail.Visible = true;
                            hyp_credit.NavigateUrl = ResolveUrl("~/ppon/ZeroActivity.aspx?bid=" + TheDeal.BusinessHourGuid);
                            break;

                        case PaymentResultPageType.ZeroPriceSuccess:
                            pan_Success.Visible = true;
                            if (TheDeal.GroupOrderStatus != null &&
                                !Helper.IsFlagSet(TheDeal.GroupOrderStatus.Value,
                                    GroupOrderStatus.PEZeventCouponDownload))
                            {
                                liPEZeventCouponDownload.Text = I18N.Message.PEZeventCouponDownloadMessage;
                            }
                            else
                            {
                                
                            }

                            #region 行銷滿額贈活動
                            if (conf.IsEnabledSpecialConsumingDiscount)
                            {
                                DateTime dateStart = DateTime.Parse(conf.SpecialConsumingDiscountPeriod.Split('|')[0]);
                                DateTime dateEnd = DateTime.Parse(conf.SpecialConsumingDiscountPeriod.Split('|')[1]);
                                if (DateTime.Now > dateStart && DateTime.Now < dateEnd)
                                {
                                    int amount = (int)Session[LkSiteSession.SpecialConsumingDiscount.ToString()];
                                    KeyValuePair<int, int> consumingDiscount = PromotionFacade.GetSpecialConsumingDiscount(amount);
                                    divDiscountPromotion.Visible = true;
                                    liDiscountPromotion.Text = consumingDiscount.Value.ToString();
                                    liDiscountPromotionCount.Text = consumingDiscount.Key.ToString();
                                }
                                else
                                {
                                    divDiscountPromotion.Visible = false;
                                }
                            }
                            else
                            {
                                divDiscountPromotion.Visible = false;
                            }
                            #endregion

                            break;
                        case PaymentResultPageType.FamiBarcodeSuccess:
                            pan_Fami_Success.Visible = true;

                            #region 行銷滿額贈活動
                            if (conf.IsEnabledSpecialConsumingDiscount)
                            {
                                DateTime dateStart = DateTime.Parse(conf.SpecialConsumingDiscountPeriod.Split('|')[0]);
                                DateTime dateEnd = DateTime.Parse(conf.SpecialConsumingDiscountPeriod.Split('|')[1]);
                                if (DateTime.Now > dateStart && DateTime.Now < dateEnd)
                                {
                                    int amount = (int)Session[LkSiteSession.SpecialConsumingDiscount.ToString()];
                                    KeyValuePair<int, int> consumingDiscount = PromotionFacade.GetSpecialConsumingDiscount(amount);
                                    divDiscountPromotion_F.Visible = true;
                                    liDiscountPromotion_F.Text = consumingDiscount.Value.ToString();
                                    liDiscountPromotionCount_F.Text = consumingDiscount.Key.ToString();
                                }
                                else
                                {
                                    divDiscountPromotion.Visible = false;
                                }
                            }
                            else
                            {
                                divDiscountPromotion.Visible = false;
                            }
                            #endregion

                            break;
                        case PaymentResultPageType.SkmZeroPriceSuccess:
                            SkmPanelResult.Visible = true;

                            break;
                        default:
                            Response.Redirect("~/error-sgo.aspx");
                            break;
                    }

                    #endregion
                }
            }
            _presenter.OnViewLoaded();
        }

        #region InterfaceMember

        public void AlertMessage(string msg)
        {
            ClientScript.RegisterStartupScript(GetType(), "alerter", "alert('" + msg + "');", true);
        }

        public void GoToClaim(string ticketId)
        {
            Response.Redirect(WebUtility.GetSiteRoot() + "/ppon/ZeroActivity.aspx?TicketId=" + ticketId);
        }

        public void GoToGet(string ticketId, string transactionId)
        {
            Response.Redirect(WebUtility.GetSiteRoot() + "/ppon/ZeroActivity.aspx?TicketId=" + ticketId + "&TransId=" + transactionId);
        }

        public void GoToLoginPage()
        {
            FormsAuthentication.RedirectToLoginPage();
        }

        public void GoToPay(Guid bid)
        {
            Response.Redirect("buy.aspx?bid=" + bid.ToString());
        }

        public void RedirectToAppLimitedEdtionPage()
        {
            Response.Redirect("~/ppon/AppLimitedEdition.aspx?bid=" + BusinessHourGuid.ToString());
        }

        public void GoToPponDefaultPage(Guid bid, string message)
        {
            if (string.IsNullOrWhiteSpace(message))
            {
                Response.Redirect("../ppon/default.aspx?bid=" + bid.ToString());
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('" + message +
                    "');window.location.href='" + ResolveClientUrl("~/ppon/default.aspx?bid=" + bid.ToString()) + "';", true);
            }
        }

        public void ReEnterPaymentGate(PaymentType paymentType, String transId)
        {
            Response.Redirect("ZeroActivity.aspx?TransId=" + transId + "&TicketId=" + TicketId);
        }

        public void SetContent(int alreadyBoughtCount, bool isDailyRestriction, ViewPponStoreCollection stores, bool isMultipleBranch)
        {
            if (TheDeal.ItemDefaultDailyAmount != null)
            {
                if (alreadyBoughtCount >= TheDeal.ItemDefaultDailyAmount.Value)
                {
                    string message = string.Format(isDailyRestriction ? I18N.Message.DailyRestrictionLimit : I18N.Message.Alreadyboughtcount, alreadyBoughtCount);
                    GoToPponDefaultPage(TheDeal.BusinessHourGuid, message);
                }
            }
            btnZeroCheckOut.Visible = true;
            var contentLite = ViewPponDealManager.DefaultManager.GetPponContentLite(TheDeal);
            lblDesc.Text = contentLite.Introduction;
            lblZeroSeller.Text = string.Format("【{0}】", TheDeal.SellerName);
            lblZeroTitle.Text = TheDeal.ItemName;
            SetStores(stores, isMultipleBranch);

            if (DealChannelList.Any(x => x.CategoryId == CategoryManager.Default.Skm.CategoryId))
            {
                lblZeroSeller.Visible = false;
            }
        }

        private void SetStores(ViewPponStoreCollection stores, bool isMultipleBranch) 
        {
            if (stores.Count <= 0) return;
            pStore.Visible = true;
            if (stores.Count == 1)
            {
                multiStores.Visible = false;
                lblStoreChioce.Visible = false;
                hidSingleStoreGuid.Value = stores[0].StoreGuid.ToString();
                var store = stores.OrderBy(x => x.SortOrder).FirstOrDefault();
                if (store != null)
                {
                    hidSingleStoreGuid.Value = store.StoreGuid.ToString();
                }
            }
            else
            {
                ddlStore.Visible = true;
                if (isMultipleBranch)
                {
                    multiStores.Visible = true;
                    ddlStoreCity.Items.Clear();
                    ddlStoreCity.Items.Add(new ListItem("請選擇", string.Empty));

                    //stores 與 CityManager.Citys 做 join , 依 CityManager.Citys Rank 做排序依據
                    var result = (from t in stores
                                  group t by new { t.CityId, t.CityName }
                                      into grp
                                      join cts in CityManager.Citys.Select(x => new { x.Id, x.Rank }) on grp.Key.CityId equals
                                          cts.Id
                                      select new { grp.Key.CityId, grp.Key.CityName, cts.Rank }
                                 ).OrderBy(x => x.Rank);

                    foreach (var city in result)
                    {
                        ddlStoreCity.Items.Add(new ListItem(city.CityName, city.CityId.ToString()));
                    }
                    ddlStoreArea.Items.Add(new ListItem("請選擇", string.Empty));
                    ddlStore.Items.Add(new ListItem("請選擇", string.Empty));
                    ddlStoreArea.Items[0].Attributes["disabled"] = "disabled";
                    ddlStore.Items[0].Attributes["disabled"] = "disabled";
                }
                else
                {
                    multiStores.Visible = false;
                    ddlStore.Items.Clear();
                    ddlStore.Items.Add(new ListItem("請選擇", string.Empty));
                    foreach (var store in stores.OrderBy(x => x.SortOrder))
                    {
                        var li = new ListItem(GetStoreOptionTitle(store), store.StoreGuid.ToString());
                        if (store.TotalQuantity <= 0 || store.OrderedQuantity >= store.TotalQuantity)
                        {
                            li.Attributes["disabled"] = "disabled";
                            li.Text += "(已售完)";
                        }
                        else
                        {
                            li.Attributes.Remove("disabled");
                        }

                        ddlStore.Items.Add(li);
                    }
                }
            }
        }

        public void SetDealCoupon(Coupon coupon, ViewPponOrder vpo)
        {
            var availablitys = ProviderFactory.Instance().GetSerializer().Deserialize<AvailableInformation[]>(vpo.Availability).ToList();
            lblCoupon.Visible = false;
            litSuccessMsg.Text = string.Format(@"感謝您參與【{0}】活動！<br/><br/>
                活動憑證：憑證編號 <strong>{1}</strong> ，確認碼 <strong>{2}</strong><br/>
                使用期限：{3}~{4}<br/>
                適用店家：【{5}】{6} {7}<br/>　　　　　{8}<br/><br/>", TheDeal.ItemName, coupon.SequenceNumber, coupon.Code
                ,
                (TheDeal.BusinessHourDeliverTimeS != null)
                    ? ((DateTime)TheDeal.BusinessHourDeliverTimeS).ToString("yyyy/MM/dd")
                    : string.Empty
                ,
                (TheDeal.BusinessHourDeliverTimeE != null)
                    ? ((DateTime)TheDeal.BusinessHourDeliverTimeE).ToString("yyyy/MM/dd")
                    : string.Empty
                , TheDeal.SellerName
                , (availablitys.Count > 0) ? availablitys[0].N : string.Empty
                , (availablitys.Count > 0) ? availablitys[0].P : string.Empty
                , (availablitys.Count > 0) ? availablitys[0].A : string.Empty);

            if (!string.IsNullOrWhiteSpace(litSuccessMsg.Text))
            {
                pnlShowCoupon.Visible = true;
            }
        }

        /// <summary>
        /// 新光
        /// </summary>
        public void SetSkmDealCoupon()
        {
            litSkmSuccessMsg.Text = TheDeal.ItemName;
        }

        /// <summary>
        /// 全家
        /// </summary>
        /// <param name="coupon"></param>
        /// <param name="groupOrderStatus"></param>
        /// <param name="vpd"></param>
        /// <param name="isEnableFami3Barcode"></param>
        public void SetDealCoupon(Coupon coupon, GroupOrderStatus groupOrderStatus, bool isEnableFami3Barcode)
        {
            if (string.IsNullOrWhiteSpace(coupon.Code)) return;
            if (!Helper.IsFlagSet(groupOrderStatus, GroupOrderStatus.FamiDeal)) return;

            hidCouponId.Value = coupon.Id.ToString();
            if (TheDeal.CouponCodeType == (int)CouponCodeType.BarcodeEAN13)
            {
                imgBarcode.ImageUrl = conf.SiteUrl + "/service/barcodeean13.ashx?code=" + coupon.Code + "&size=3&width=410&hight=300";
            }
            else
            {
                imgBarcode.Visible = false;

                if (isEnableFami3Barcode)
                {
                    ph_fami3barcode.Visible = true;
                    ph_fami3barcodeInfo.Visible = true;
                    ph_famibarcodeInfo.Visible = false;
                    var famipos = OrderFacade.FamiposBarcodeGetCreate(coupon.OrderDetailId, TheDeal.BusinessHourGuid,
                        TheDeal);
                    spanPincode.InnerText = coupon.Code;
                    litBarcodeImg.Text = string.Format(@"<br />
                            <img src='../service/barcode39.ashx?id={0}&mini=true' style='vertical-align:bottom' /><br />
                            <font size='1' style='color:#000;font-size:75%;vertical-align:top;line-height:20px;'>*{0}*</font><br />
                            <img src='../service/barcode39.ashx?id={1}&mini=true' style='vertical-align:bottom;margin-top:10px' /><br />
                            <font size='1' style='color:#000;font-size:75%;vertical-align:top;line-height:20px;'>*{1}*</font><br />
                            <img src='../service/barcode39.ashx?id={2}&mini=true' style='vertical-align:bottom;margin-top:10px' /><br />
                            <font size='1' style='color:#000;font-size:75%;vertical-align:top;line-height:20px;'>*{2}*</font>
                        ", famipos.Barcode1, famipos.Barcode2, famipos.Barcode3);
                }
                else
                {
                    ph_ButtonArea.Visible = false;
                    btnDownloadFamiBarcode.Visible = false;
                    btnPrintFamiBarcode.Visible = false;
                    ph_instruction.Visible = false;
                    ph_PincodePromo.Visible = true;
                    litPinCode.Visible = true;
                    litPinCode.Text = string.Format(I18N.Message.FamiPortPINCouponFormat, coupon.Code);

                    img_FamiPincodePromo.ImageUrl = ImageFacade.GetMediaPathsFromRawData(TheDeal.EventImagePath, MediaType.PponDealPhoto).Where((x, i) => i == 3).DefaultIfEmpty("~/Themes/default/images/17Life/G3/0act.jpg").First();

                    if (!string.IsNullOrEmpty(TheDeal.ActivityUrl))
                    {
                        hl_FamiPincodePromo.NavigateUrl = Server.UrlDecode(TheDeal.ActivityUrl.Trim());
                    }
                }
            }
        }

        public void SetFamiDealBarcodeContent(IViewPponDeal vpd)
        {
            lblCouponUsage.Text = vpd.CouponUsage;
        }

        public void ShowResult(PaymentResultPageType result)
        {
            PanelBuy.Visible = false;
            PanelResult.Visible = true;

            PaymentResult = result;
            img_ZeroPromo.ImageUrl = ImageFacade.GetMediaPathsFromRawData(TheDeal.EventImagePath, MediaType.PponDealPhoto).Where((x, i) => i == 3).DefaultIfEmpty("~/Themes/default/images/17Life/G3/0act.jpg").First();

            if (!string.IsNullOrEmpty(TheDeal.ActivityUrl))
            {
                hyp_ZeroPromo.NavigateUrl = Server.UrlDecode(TheDeal.ActivityUrl.Trim());
            }

            SetRecommendDeals();
        }

        public void ShowSkmResult(PaymentResultPageType result)
        {
            PanelBuy.Visible = false;
            PanelResult.Visible = false;
            SkmPanelResult.Visible = true;
            SetRecommendDeals(PponCityGroup.DefaultPponCityGroup.AllCountry.CityId);
        }

        private void SetRecommendDeals()
        {
            var workCityId = int.Equals(-1, CookieCityId) ? PponCityGroup.DefaultPponCityGroup.TaipeiCity.CityId : CookieCityId;

            var Deals = ViewPponDealManager.DefaultManager.ViewPponDealGetListForPponByCityWithDataSlot(workCityId);
            if (!(Deals.Count > 3))
            {
                Deals.AddRange(ViewPponDealManager.DefaultManager.ViewPponDealGetListForPponByCityWithDataSlot(PponCityGroup.DefaultPponCityGroup.AllCountry.CityId));
            }
            var sideDeals = Deals.Where(x => !Guid.Equals(TheDeal.BusinessHourGuid, x.BusinessHourGuid));
            int k = 3;
            foreach (ViewPponDeal vpd in sideDeals)
            {
                if (int.Equals(0, k))
                {
                    break;
                }

                if (int.Equals(3, k))
                {
                    rt1.Text = vpd.ItemName;
                    rt1.NavigateUrl = rl1.NavigateUrl = rb1.NavigateUrl = ResolveUrl("~/ppon/default.aspx?bid=" + vpd.BusinessHourGuid.ToString());
                    op1.Text = Convert.ToInt32(vpd.ItemOrigPrice).ToString("D");
                    dp1.Text = Convert.ToInt32(vpd.ItemPrice).ToString("D");
                    discount1.Text = vpd.ItemOrigPrice > 0 ? ViewPponDealManager.GetDealDiscountString(Helper.IsFlagSet(vpd.BusinessHourStatus, BusinessHourStatus.NoDiscountShown), vpd.ItemPrice, vpd.ItemOrigPrice) : "--";
                    rp1.ImageUrl = ImageFacade.GetMediaPathsFromRawData(vpd.EventImagePath, MediaType.PponDealPhoto).DefaultIfEmpty(string.Empty).First();
                }

                if (int.Equals(2, k))
                {
                    rt2.Text = vpd.ItemName;
                    rt2.NavigateUrl = rl2.NavigateUrl = rb2.NavigateUrl = ResolveUrl("~/ppon/default.aspx?bid=" + vpd.BusinessHourGuid.ToString());
                    op2.Text = Convert.ToInt32(vpd.ItemOrigPrice).ToString("D");
                    dp2.Text = Convert.ToInt32(vpd.ItemPrice).ToString("D");
                    discount2.Text = vpd.ItemOrigPrice > 0 ? ViewPponDealManager.GetDealDiscountString(Helper.IsFlagSet(vpd.BusinessHourStatus, BusinessHourStatus.NoDiscountShown), vpd.ItemPrice, vpd.ItemOrigPrice) : "--";
                    rp2.ImageUrl = ImageFacade.GetMediaPathsFromRawData(vpd.EventImagePath, MediaType.PponDealPhoto).DefaultIfEmpty(string.Empty).First();
                }

                if (int.Equals(1, k))
                {
                    rt3.Text = vpd.ItemName;
                    rt3.NavigateUrl = rl3.NavigateUrl = rb3.NavigateUrl = ResolveUrl("~/ppon/default.aspx?bid=" + vpd.BusinessHourGuid.ToString());
                    op3.Text = Convert.ToInt32(vpd.ItemOrigPrice).ToString("D");
                    dp3.Text = Convert.ToInt32(vpd.ItemPrice).ToString("D");
                    discount3.Text = vpd.ItemOrigPrice > 0 ? ViewPponDealManager.GetDealDiscountString(Helper.IsFlagSet(vpd.BusinessHourStatus, BusinessHourStatus.NoDiscountShown), vpd.ItemPrice, vpd.ItemOrigPrice) : "--";
                    rp3.ImageUrl = ImageFacade.GetMediaPathsFromRawData(vpd.EventImagePath, MediaType.PponDealPhoto).DefaultIfEmpty(string.Empty).First();
                }

                k--;
            }
        }
        
        //推薦檔次
        private void SetRecommendDeals(int cityId)
        {
            var recommendDeals = OrderFacade.GetRandomDeal(cityId, 6, 6);

            List<BuyRelatedDeal> deals = new List<BuyRelatedDeal>();
            foreach (ViewPponDeal vpd in recommendDeals)
            {
                var deal = new BuyRelatedDeal();

                deal.BusinessHourGuid = vpd.BusinessHourGuid;
                deal.CouponUsage = vpd.CouponUsage;
                deal.EventTitle = vpd.EventTitle;
                deal.ItemPrice = vpd.ItemPrice;
                deal.ItemOrigPrice = vpd.ItemOrigPrice;
                deal.EventImagePath = vpd.EventImagePath;
                deal.BusinessHourStatus = vpd.BusinessHourStatus;
                
                deal.NavigateUrl = ResolveUrl(string.Format("~/{0}", vpd.BusinessHourGuid.ToString()));
                deal.PriceText = Convert.ToInt32(vpd.ItemPrice).ToString("D");
                deal.discountText = ViewPponDealManager.GetDealDiscountString(Helper.IsFlagSet(vpd.BusinessHourStatus, BusinessHourStatus.NoDiscountShown), vpd.ItemPrice, vpd.ItemOrigPrice);
                deal.ImageUrl = ImageFacade.GetMediaPathsFromRawData(vpd.EventImagePath, MediaType.PponDealPhoto).DefaultIfEmpty(string.Empty).First();
                
                deals.Add(deal);
            }

            rptRecommendDeals.DataSource = deals;
            rptRecommendDeals.DataBind();
        }

        #endregion InterfaceMember

        #region PageEvent

        public void ibCheckOut_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(TicketId))
            {
                TheDeal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(BusinessHourGuid);
                if (StoreGuid != Guid.Empty || Helper.IsFlagSet(TheDeal.BusinessHourStatus, BusinessHourStatus.NoRestrictedStore)) 
                {
                    if (CheckOut != null)
                    {
                        CheckOut(this, EventArgs.Empty);
                    }
                }
            }
            else
            {
                GoToPponDefaultPage(BusinessHourGuid, string.Empty);
            }
        }

        #endregion PageEvent

        protected void ddlStoreCity_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            var item = ((DropDownList)sender).SelectedItem;
            ddlStoreArea.Items.Clear();
            ddlStore.Items.Clear();
            ddlStoreArea.Items.Add(new ListItem("請選擇", string.Empty));
            ddlStore.Items.Add(new ListItem("請選擇", string.Empty));
            lblStoreAddr.Text = lblStoreName.Text = string.Empty;
            if (string.IsNullOrEmpty(item.Value))
            {
                ddlStoreArea.Items[0].Attributes["disabled"] = "disabled";
                ddlStore.Items[0].Attributes["disabled"] = "disabled";
            }
            else
            {
                ddlStoreArea.Items[0].Enabled = true;
                foreach (var township in ViewPponStoreManager.DefaultManager.ViewPponStoreGetList(BusinessHourGuid)
                                                             .Where(x => x.CityId == int.Parse(item.Value))
                                                             .GroupBy(x => new {x.TownshipName, x.TownshipId})
                                                             .Select(
                                                                 x => new { x.Key.TownshipName, x.Key.TownshipId }
                                                                 ).OrderBy(x => x.TownshipId))
                {
                    ddlStoreArea.Items.Add(new ListItem(township.TownshipName, township.TownshipId.ToString()));
                }
            }
        }

        protected void ddlStoreArea_SelectedIndexChanged(object sender, EventArgs e)
        {
            var item = ((DropDownList)sender).SelectedItem;
            ddlStore.Items.Clear();
            ddlStore.Items.Add(new ListItem("請選擇", string.Empty));
            lblStoreAddr.Text = lblStoreName.Text = string.Empty;
            if (string.IsNullOrEmpty(item.Value))
            {
                ddlStore.Items[0].Attributes["disabled"] = "disabled";
            }
            else
            {
                ddlStore.Items[0].Enabled = true;
                foreach (var store in ViewPponStoreManager.DefaultManager.ViewPponStoreGetList(BusinessHourGuid)
                                             .Where(x => x.CityId == int.Parse(ddlStoreCity.SelectedValue) 
                                                 && x.TownshipId == int.Parse(item.Value)).OrderBy(x=>x.SortOrder))
                {
                    var li = new ListItem(GetStoreOptionTitle(store), store.StoreGuid.ToString());
                    if (store.TotalQuantity <= 0 || store.OrderedQuantity >= store.TotalQuantity)
                    {
                        li.Attributes["disabled"] = "disabled";
                        li.Text += "(已售完)";
                    }
                    else
                    {
                        li.Attributes.Remove("disabled");
                    }

                    ddlStore.Items.Add(li);
                }
            }
        }

        protected void ddlStore_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            var item = ((DropDownList)sender).SelectedItem;
            if (string.IsNullOrEmpty(item.Value))
            {
                lblStoreName.Text = string.Empty;
                lblStoreAddr.Text = string.Empty;
            }
            else
            {
                ViewPponStore store = ViewPponStoreManager.DefaultManager.ViewPponStoreGet(BusinessHourGuid, Guid.Parse(item.Value));
                lblStoreName.Text = string.Format("{0} {1}", store.StoreName, store.Phone);
                lblStoreAddr.Text = string.Format("{0}{1}{2}", store.CityName, store.TownshipName, store.AddressString);
            }
        }
    }
}
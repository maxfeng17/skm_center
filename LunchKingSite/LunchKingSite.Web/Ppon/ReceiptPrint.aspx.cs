﻿using System;
using System.Linq;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.UI;

namespace LunchKingSite.Web.Ppon
{
    public partial class ReceiptPrint : LocalizedBasePage
    {
        private IOrderProvider op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
        private IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        private IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();

        protected void Page_Load(object sender, EventArgs e)
        {
            Guid oid;

            if (Guid.TryParse(Request.QueryString["oid"], out oid))
            {
                ConstructPageElements(oid);
            }
            else
            {
                receiptContainer.Visible = false;
            }
        }

        private void ConstructPageElements(Guid oid)
        {
            #region pre-execution check
            CashTrustLogCollection ctCol = mp.CashTrustLogGetListByOrderGuid(oid, OrderClassification.LkSite);
            
            string userName = User.Identity.Name;
            string quser = Request.QueryString["user"] != null ? Request.QueryString["user"].ToString() : "";
            if (string.IsNullOrEmpty(userName))
            {
                userName = quser;
            }
            int userId = MemberFacade.GetUniqueId(userName);
            if (ctCol == null || ctCol.Count == 0 || ctCol.Any(x => x.UserId != userId))   //沒有cash_trust_log就沒有金額資料
            {
                receiptContainer.Visible = false;
                return;
            }

            ViewPponOrderDetailCollection vpods = pp.ViewPponOrderDetailGetListByOrderGuid(oid);
            ViewPponOrderDetail oneVpod = vpods.FirstOrDefault(x => true);
            if(oneVpod != null && !Helper.IsFlagSet(oneVpod.OrderStatus, OrderStatus.Complete)) //成功單才有意義
            {
                receiptContainer.Visible = false;
                return;
            }
            
            #endregion

            CashTrustLog oneLog = ctCol[0];
            Guid bid = oneLog.BusinessHourGuid.GetValueOrDefault();


            orderId.Text = op.OrderGet(oneLog.OrderGuid).OrderId;

            IViewPponDeal vpd = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(bid, true);

            eventName.Text = vpd.EventName;
            
            if (vpd != null && vpd.IsLoaded)
            {
                if (vpd.IsHideContent != null && vpd.IsHideContent == false)
                {
                    string newEventName = vpd.EventName;
                    if (!string.IsNullOrEmpty(vpd.ContentName))
                    {
                        eventName.Text = vpd.ContentName;
                    }
                }
            }
            
            paymentDate.Text = oneLog.CreateTime.ToString("yyyy/MM/dd");
            quantity.Text = vpods.Where(x => int.Equals((int)OrderDetailStatus.None, x.OrderDetailStatus)).Sum(x => x.ItemQuantity).ToString();
            if (oneVpod != null)
            {
                itemUnitPrice.Text = oneVpod.ItemUnitPrice.ToString("########");
            }

            #region 各種付款方式的金額
            var paymentTypesAmount = ctCol.GroupBy(x => x.OrderGuid).Select(x =>
            new
            {
                CreditCard = x.Sum(y => y.CreditCard),
                Atm = x.Sum(y => y.Atm),
                PCash = x.Sum(y => y.Pcash),
                SCash = x.Sum(y => y.Scash),
                Pscash = x.Sum(y=>y.Pscash),                
                BCash = x.Sum(y => y.Bcash),
                DCash = x.Sum(y => y.DiscountAmount)
            });
            var amount = paymentTypesAmount.FirstOrDefault();
            if (amount != null)
            {
                creditCardPaymentContainer.Visible = !int.Equals(0, amount.CreditCard);
                atmPaymentContainer.Visible = !int.Equals(0, amount.Atm);
                pcashPaymentContainer.Visible = !int.Equals(0, amount.PCash);
                scashPaymentContainer.Visible = !int.Equals(0, amount.SCash + amount.Pscash);
                //pscashPaymentContainer.Visible = !int.Equals(0, amount.Pscash);
                bcashPaymentContainer.Visible = !int.Equals(0, amount.BCash);
                dcashPaymentContainer.Visible = !int.Equals(0, amount.DCash);                
                creditCardAmount.Text = amount.CreditCard.ToString();
                atmAmount.Text = amount.Atm.ToString();
                pcashAmount.Text = amount.PCash.ToString();
                scashAmount.Text = (amount.SCash + amount.Pscash).ToString();
                //pscashAmount.Text = amount.Pscash.ToString();
                bcashAmount.Text = amount.BCash.ToString();
                dcashAmount.Text = amount.DCash.ToString();
                totalAmount.Text = (amount.CreditCard + amount.Atm + amount.PCash + amount.Pscash + amount.SCash + amount.BCash + amount.DCash).ToString();
            }
            #endregion

            #region 運費
            CashTrustLog freight = ctCol.Where(x => x.SpecialStatus == (int)TrustSpecialStatus.Freight).FirstOrDefault();
            if (freight != null)
            {
                freightContainer.Visible = true;
                freightAmount.Text = (freight.CreditCard + freight.Atm + freight.Pcash + freight.Scash + freight.Bcash + freight.DiscountAmount).ToString();
            }
            #endregion
        }
    }
}
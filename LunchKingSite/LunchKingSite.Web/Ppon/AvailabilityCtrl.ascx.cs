﻿using LunchKingSite.Core.Models;
using LunchKingSite.Core.UI;
using LunchKingSite.I18N;
using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.BizLogic.Facade;
using Microsoft.SqlServer.Types;

namespace LunchKingSite.Web.Ppon
{
    public partial class AvailabilityCtrl : BaseUserControl
    {
        public AvailabilityCtrl()
        {
            WrapLi = true;
        }

        private const string googleMapUrl = "https://maps.google.com.tw/maps?f=q&hl=zh-TW&z=16&t=m&iwloc=near&output=&q=";

        private static ISysConfProvider conf = ProviderFactory.Instance().GetConfig();

        public AvailableInformation[] Availablity { get; set; }

        public bool WrapLi { get; set; }

        [TemplateContainer(typeof(RepeaterItem))]
        public ITemplate ItemTemplate { get; set; }

        protected override void OnDataBinding(EventArgs e)
        {
            if (this.ItemTemplate != null)
            {
                rep_Multiple.ItemTemplate = this.ItemTemplate;
            }

            rep_Multiple.DataSource = Availablity;
            rep_Multiple.DataBind();
        }

        protected void rm_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            //HERE IS NEW
            if (e.Item.DataItem is AvailableInformation)
            {
                AvailableInformation ai = e.Item.DataItem as AvailableInformation;
                if (!string.IsNullOrEmpty(ai.N))
                {
                    Literal n = e.Item.FindControl("n") as Literal;
                    if (n != null)
                    {
                        n.Text = !string.IsNullOrEmpty(ai.U) ? string.Format(@"<a href=""{0}"" target=""_blank"" style=""color:#BF0000;"">{1}</a>", ai.U, ai.N) : ai.N;
                    }
                }

                if (!string.IsNullOrEmpty(ai.P))
                {
                    Literal p = e.Item.FindControl("p") as Literal;
                    if (p != null)
                    {
                        p.Text = TryWrapLi(Phrase.Phone + ": " + ai.P);
                    }
                }

                if (!string.IsNullOrEmpty(ai.A))
                {
                    Literal ap = e.Item.FindControl("ap") as Literal;
                    Literal a = e.Item.FindControl("a") as Literal;
                    if (ap != null)
                    {
                        if (Availablity.Length == 1)
                        {
                            ap.Text = "<div id='map' address='" + ai.A + "' longitude='" + ai.Longitude + "' latitude='" + ai.Latitude + "' style='width:209px;height:209px;margin-left: -12px;'></div>";
                        }
                    }

                    if (a != null)
                    {
                        string latitude = string.Empty;
                        string longitude = string.Empty;
                        ISysConfProvider config = ProviderFactory.Instance().GetConfig();

                        if (conf.GoogleMapInDefaultPage)
                        {
                            string mapHtml = "";
                            if (ai.HasLongLat)
                            {
                                mapHtml = string.Format("{0}:{1}<a target='_blank' href='{2}{3},{4}' class='map_btn'>&nbsp;</a>",
                                    Phrase.Address, ai.A, googleMapUrl, ai.Latitude, ai.Longitude);
                            }
                            else if (String.IsNullOrEmpty(ai.A) == false)
                            {
                                mapHtml = string.Format("{0}:{1}<a target='_blank' href='{2}{1}' class='map_btn'>&nbsp;</a>",
                                    Phrase.Address, ai.A, googleMapUrl);
                                a.Text = TryWrapLi(mapHtml);
                            }
                            a.Text = TryWrapLi(mapHtml);
                        }
                        else
                        {
                            if (config.EnableOpenStreetMap)
                            {
                                if (ai.HasLongLat)
                                {
                                    SqlGeography geo = LocationFacade.GetGeography(ai.A);
                                    if (geo != null && !geo.IsNull)
                                    {
                                        if (!geo.Lat.IsNull && !geo.Long.IsNull)
                                        {
                                            latitude = geo.Lat.ToString();
                                            longitude = geo.Long.ToString();
                                        }
                                    }
                                }
                                else
                                {
                                    latitude = ai.Latitude;
                                    longitude = ai.Longitude;
                                }
                            }
                            else
                            {
                                latitude = ai.Latitude;
                                longitude = ai.Longitude;
                            }
                            string showMapJs = string.Empty;
                            if (!string.IsNullOrWhiteSpace(latitude) && !string.IsNullOrWhiteSpace(longitude))
                            {
                                showMapJs = string.Format(" onclick='showmap(this, \"{0}\");' class=\"map_btn\" ",
                                    config.EnableOpenStreetMap);
                            }
                            string mapHtml = string.Format(
                                "{0}:{1}<a address='{1}' longitude='{2}' latitude='{3}' {4} style='cursor:pointer;'>&nbsp;</a>",
                                Phrase.Address, ai.A, longitude, latitude, showMapJs);
                            a.Text = TryWrapLi(mapHtml);
                        }

                    }
                }

                if (!string.IsNullOrEmpty(ai.R))
                {
                    Literal r = e.Item.FindControl("r") as Literal;
                    if (r != null)
                    {
                        r.Text = TryWrapLi(ai.R);
                    }
                }

                Literal op = e.Item.FindControl("op") as Literal;
                if (op != null)
                {
                    if (!string.IsNullOrWhiteSpace(ai.UT))
                    {
                        op.Text = TryWrapLi(Phrase.UseTime + Phrase.Colon + ai.UT);
                    }
                    else if (!string.IsNullOrEmpty(ai.OT))
                    {
                        op.Text = TryWrapLi(Phrase.OpeningTime + Phrase.Colon + ai.OT);
                    }
                }

                if (!string.IsNullOrEmpty(ai.CD))
                {
                    Literal cd = e.Item.FindControl("cd") as Literal;
                    if (cd != null)
                    {
                        cd.Text = TryWrapLi(Phrase.CloseDate + Phrase.Colon + ai.CD);
                    }
                }

                //if (!string.IsNullOrEmpty(ai.MR) || !string.IsNullOrEmpty(ai.CA) || !string.IsNullOrEmpty(ai.BU) || !string.IsNullOrEmpty(ai.OV))
                //{
                //    Literal liVehicle = e.Item.FindControl("liVehicle") as Literal;
                //    liVehicle.Text = TryWrapLi(Phrase.Vehicles + "<br/>");
                //}

                if (!string.IsNullOrEmpty(ai.MR))
                {
                    Literal mr = e.Item.FindControl("mr") as Literal;
                    if (mr != null)
                    {
                        mr.Text = TryWrapLi(Phrase.Mrt + Phrase.Colon + ai.MR);
                    }
                }

                if (!string.IsNullOrEmpty(ai.CA))
                {
                    Literal ca = e.Item.FindControl("ca") as Literal;
                    if (ca != null)
                    {
                        ca.Text = TryWrapLi(Phrase.Car + Phrase.Colon + ai.CA);
                    }
                }

                if (!string.IsNullOrEmpty(ai.BU))
                {
                    Literal bu = e.Item.FindControl("bu") as Literal;
                    if (bu != null)
                    {
                        bu.Text = TryWrapLi(Phrase.Bus + Phrase.Colon + ai.BU);
                    }
                }

                if (!string.IsNullOrEmpty(ai.OV))
                {
                    Literal ov = e.Item.FindControl("ov") as Literal;
                    if (ov != null)
                    {
                        ov.Text = TryWrapLi(Phrase.OtherVehicle + Phrase.Colon + ai.OV);
                    }
                }

                int limitLength = 55;
                if (!string.IsNullOrEmpty(ai.U))
                {
                    Literal u = e.Item.FindControl("u") as Literal;
                    if (u != null)
                    {
                        u.Text = TryWrapLi(Phrase.Link + Phrase.Colon + (!string.IsNullOrEmpty(ai.U) ? string.Format(@"<a href=""{0}"" target=""_blank"" style=""color:Blue;"">{1}</a>", ai.U, (ai.U.Length > limitLength ? ai.U.Substring(0, limitLength) + "..." : ai.U)) : ai.U));
                    }
                }

                if (!string.IsNullOrEmpty(ai.FB))
                {
                    Literal fb = e.Item.FindControl("fb") as Literal;
                    if (fb != null)
                    {
                        fb.Text = TryWrapLi(Phrase.FbFans + Phrase.Colon + (!string.IsNullOrEmpty(ai.FB) ? string.Format(@"<a href=""{0}"" target=""_blank"" style=""color:Blue;"">{1}</a>", ai.FB, (ai.FB.Length > limitLength ? ai.FB.Substring(0, limitLength) + "..." : ai.FB)) : ai.FB));
                    }
                }

                if (!string.IsNullOrEmpty(ai.BL))
                {
                    Literal bl = e.Item.FindControl("bl") as Literal;
                    if (bl != null)
                    {
                        bl.Text = TryWrapLi(Phrase.BlogFans + Phrase.Colon + (!string.IsNullOrEmpty(ai.BL) ? string.Format(@"<a href=""{0}"" target=""_blank"" style=""color:Blue;"">{1}</a>", ai.BL, (ai.BL.Length > limitLength ? ai.BL.Substring(0, limitLength) + "..." : ai.BL)) : ai.BL));
                    }
                }

                if (!string.IsNullOrEmpty(ai.OL))
                {
                    Literal ol = e.Item.FindControl("ol") as Literal;
                    if (ol != null)
                    {
                        ol.Text = TryWrapLi(Phrase.OtherFans + Phrase.Colon + (!string.IsNullOrEmpty(ai.OL) ? string.Format(@"<a href=""{0}"" target=""_blank"" style=""color:Blue;"">{1}</a>", ai.OL, (ai.OL.Length > limitLength ? ai.OL.Substring(0, limitLength) + "..." : ai.OL)) : ai.OL));
                    }
                }
            }
        }

        private string TryWrapLi(string origText)
        {
            if (WrapLi)
            {
                return "<li>" + origText + "</li>";
            }

            return origText;
        }
    }
}
﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OnlineEDM.aspx.cs" Inherits="LunchKingSite.Web.Ppon.OnlineEDM" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div style="width: 100%; margin: 0 auto; font-family: Arial, Microsoft JhengHei, Helvetica, sans-serif">
        <table width="100%" cellpadding="0" cellspacing="0" align="center" style="display: none">
            <tr>
                <td>
                    <div id="dv_Hint" runat="server" style="color:#fff; font-size:8px; height:1px; overflow:hidden;"></div>
                </td>
            </tr>
            <tr>
                <td height="50" align="center" style="font-size: 13px; background: #FFF;">
                    若信件內容無法正常顯示，您可以點選此處<a href="<%= SiteUrl+"/ppon/onlineedm.aspx?eid="+EdmID %>" target="_blank">查看線上版本</a><br />
                    若此信件被寄送至您的垃圾郵件中，請將 hi@edm.17life.com.tw 加入您的聯絡人清單中。
                </td>
            </tr>
        </table>
        <table cellpadding="0" cellspacing="0" style="width: 100%;">
            <tr>
                <td style="font-size: 13px; background: #E7E7E7;">
                    <table cellpadding="0" cellspacing="0" style="width: 770px;" align="center">
                        <tr>
                            <td>
                                <!--EDM Header-Start-->
                                <table cellpadding="0" cellspacing="0" align="center" style="background: #F3F3EE;
                                    color: #333; width: 770px;">
                                    <tr>
                                        <td style="width: 20px; height: 80px">
                                        </td>
                                        <td style="width: 140px; height: 80px">
                                            <a href="<%=SiteUrl %>/ppon/" target="_blank" title='<%= DeliveryDate.ToString("yyyy/MM/dd")+" 17Life EDM" %>'>
                                                <img src='<%=SiteUrl+"/Themes/PCweb/images/EDMLOGO.png" %>' alt='<%= DeliveryDate.ToString("yyyy/MM/dd")+" 17Life EDM" %>'
                                                    border="0" style="width: 132px; height: 80px;" /></a>
                                        </td>
                                        <td style="font-size: 26px; font-weight: bold; width: 480px; height: 80px">
                                            │<asp:Label ID="lab_CityName" runat="server"></asp:Label>
                                        </td>
                                        <td style="width: 110px; height: 80px">
                                            <asp:HyperLink ID="hyp_All" runat="server" Target="_blank" Style="color: #333; padding: 5px;
                                                font-size: 14px;">查看全部優惠</asp:HyperLink>
                                        </td>
                                        <td style="width: 20px; height: 80px">
                                        </td>
                                    </tr>
                                </table>
                                <table cellpadding="0" cellspacing="0" style="background: #F3F3EE; width: 770px;"
                                    align="center">
                                    <tr>
                                        <td style="width: 25px;">
                                        </td>
                                        <td style="width: 700px;">
                                            <table cellpadding="0" cellspacing="0" align="center" style="width: 740px">
                                                <tr>
                                                    <td colspan="3" style="border-top: 1px solid #DDD; height: 10px;">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 193px; height: 40px; text-align: right;">
                                                        <a href="https://www.facebook.com/17life.com.tw" target="_blank">
                                                            <img src='<%=SiteUrl+"/Themes/default/images/17Life/EDM/EDM_FB_btn.png" %>' border="0"
                                                                alt="" /></a>
                                                    </td>
                                                    <td style="color: #333; font-size: 14px; width: 280px; height: 40px; text-align: right;">
                                                        快下載17Life App，隨時隨地抓住好康→&nbsp;&nbsp;
                                                    </td>
                                                    <td style="width: 267px; height: 40px;">
                                                        <a href="https://play.google.com/store/apps/details?id=com.uranus.e7plife&amp;feature=search_result#?t=W251bGwsMSwxLDEsImNvbS51cmFudXMuZTdwbGlmZSJd" style="text-decoration: none;"
                                                            target="_blank">
                                                            <img src='<%=SiteUrl+"/Themes/default/images/17Life/EDM/App_android.png" %>' border="0"
                                                                alt="" />
                                                        </a>&nbsp; <a href="https://itunes.apple.com/tw/app/id543439591?mt=8" style="text-decoration: none;"
                                                            target="_blank">
                                                            <img src='<%=SiteUrl+"/Themes/default/images/17Life/EDM/App_iphone.png" %>' border="0"
                                                                alt="" />
                                                        </a>
                                                    </td>
                                                </tr>
                                            </table>
                                            <asp:Panel ID="pan_AD" runat="server">
                                                <table cellpadding="0" cellspacing="0" style="width: 740px;" align="center">
                                                    <tr>
                                                        <td style="height: 5px">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="height: 80px">
                                                            <asp:Literal ID="lit_AD" runat="server"></asp:Literal>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="border-top: 1px solid #DDD; height: 15px">
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                            <asp:Panel ID="pan_MainDeal1" runat="server">
                                            </asp:Panel>
                                            <asp:Panel ID="pan_MainDeal2" runat="server">
                                            </asp:Panel>
                                            <asp:Panel ID="pan_Area1" runat="server">
                                            </asp:Panel>
                                            <asp:Panel ID="pan_Area2" runat="server">
                                            </asp:Panel>
                                            <asp:Panel ID="pan_Area3" runat="server">
                                            </asp:Panel>
                                            <asp:Panel ID="pan_PiinLife1" runat="server">
                                            </asp:Panel>
                                            <asp:Panel ID="pan_PiinLife2" runat="server">
                                            </asp:Panel>
                                            <asp:Panel ID="pan_PEZ" runat="server">
                                                <table>
                                                    <tr>
                                                        <td width="740" height="40" style="color: #C82851; font-size: 24px; font-weight: bold;
                                                            border-bottom: 1px solid #C82851;">
                                                            ▌PayEasy每日一物
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td height="5">
                                                        </td>
                                                    </tr>
                                                </table>
                                                <asp:Literal ID="lit_PEZ" runat="server"></asp:Literal>
                                            </asp:Panel>
                                        </td>
                                        <td style="width: 25px;">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <table width="770" cellpadding="0" cellspacing="0" align="center" style="background: #333;
                        color: #FFF;">
                        <tr>
                            <td width="25">
                            </td>
                            <td height="80">
                                <div style="display: none">
                                    若您不希望再收到此訊息，請按此 <a style="color: #FFF;" target="_blank" href="%%UnSubscription%%">取消訂閱</a>
                                    。
                                </div>
                                <br />
                                <a href="<%=SiteUrl + SystemConfig.SiteServiceUrl %>" target="_blank" style="color: #FFF">客服中心</a>
                                | <a href="<%=SiteUrl %>/Ppon/WhiteListGuide.aspx" target="_blank" style="color: #FFF">
                                    加入白名單</a> | 服務時間：平日 9:00~18:00<br />
                                17Life 康太數位整合股份有限公司 版權所有 轉載必究 © <script language="javascript"> var Today = new Date(); document.write(Today.getFullYear()); </script>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="background: #FFF; width: 25px">
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>

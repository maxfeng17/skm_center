﻿using System.Web.Caching;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.WebLib.Component;
using LunchKingSite.WebLib.Models.Ppon;
using LunchKingSite.BizLogic.Models;

namespace LunchKingSite.Web.PPon
{
    public partial class _default : BasePage, IDefaultPponView
    {
        #region event

        public event EventHandler CheckEventMail;

        #endregion event

        #region Props

        static System.Collections.Concurrent.ConcurrentDictionary<string, int> routeNameUsedStat = new System.Collections.Concurrent.ConcurrentDictionary<string, int>();

        #region Provider
        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        private static ICacheProvider cache = ProviderFactory.Instance().GetProvider<ICacheProvider>();
        public static ISysConfProvider SystemConfig
        {
            get
            {
                return config;
            }
        }
        #endregion

        public const string CPA_HOT_DEALS = "cpa-17_hotdeals";

        private DefaultPponPresenter _presenter;

        public DefaultPponPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        #region 檔次列表
        /// <summary>
        /// 主要檔次
        /// </summary>
        private List<ViewMultipleMainDeal> _viewMultipleMainDeals;

        public List<ViewMultipleMainDeal> ViewMultipleMainDeals
        {
            get { return _viewMultipleMainDeals; }
        }

        /// <summary>
        /// 今日熱銷
        /// </summary>
        private List<ViewMultipleSideDeal> _viewMultipleHotDeals;

        public List<ViewMultipleSideDeal> ViewMultipleHotDeals
        {
            get { return _viewMultipleHotDeals; }
        }

        /// <summary>
        /// 最新上檔
        /// </summary>
        private List<ViewMultipleSideDeal> _viewMultipleTodayDeals;

        public List<ViewMultipleSideDeal> ViewMultipleTodayDeals
        {
            get { return _viewMultipleTodayDeals; }
        }

        /// <summary>
        /// 最後倒數
        /// </summary>
        private List<ViewMultipleSideDeal> _viewMultipleLastDayDeals;

        public List<ViewMultipleSideDeal> ViewMultipleLastDayDeals
        {
            get { return _viewMultipleLastDayDeals; }
        }
        #endregion

        #region city, category相關
        /// <summary>
        /// 由master page取得的cityid
        /// </summary>
        private int _cityId;
        
        /// <summary>
        /// 當前cityId
        /// </summary>
        public int CityId
        {
            get
            {
                return Master.SelectedCityId;
            }
            set
            {
                Master.SelectedPponCity = PponCityGroup.DefaultPponCityGroup.GetPponCityByCityId(value);
            }
        }

        private int? _pponCityId;
        /// <summary>
        /// 當前cityId
        /// </summary>

        public int? PponCityId
        {
            get
            {
                if (_pponCityId == null)
                {
                    _pponCityId = CookieManager.GetPponCityId();
                }
                return _pponCityId;
            }
            set
            {
                _pponCityId = value;
                if (value != null)
                {
                    CookieManager.SetPponCityId(value.Value);
                }
            }
        }

        /// <summary>
        /// 品生活分類Id
        /// </summary>
        public int CategoryIdInPiinLife
        {
            get
            {
                string cat_querystring = string.Empty;
                int cat;
                if (Page.RouteData.Values["cat"] != null)
                {
                    cat_querystring = Page.RouteData.Values["cat"].ToString();
                }
                else if (!string.IsNullOrWhiteSpace(Request.QueryString["cat"]))
                {
                    cat_querystring = Request.QueryString["cat"];
                }

                if (!string.IsNullOrWhiteSpace(cat_querystring) && int.TryParse(cat_querystring, out cat))
                {
                    return cat;
                }
                else
                {
                    return CategoryManager.GetDefaultCategoryNode().CategoryId;
                }
            }
        }
        
        public int TravelCategoryId
        {
            get
            {
                HttpCookie cookie = Request.Cookies.Get(LkSiteCookie.TravelCategoryId.ToString("g"));

                if (cookie == null)
                {
                    cookie = CookieManager.NewCookie(
                        LkSiteCookie.TravelCategoryId.ToString("g"), 
                        CategoryManager.GetDefaultCategoryNode().CategoryId.ToString(),
                        DateTime.Now.AddDays(1)
                    );
                    Response.Cookies.Add(cookie);
                }

                int cid = int.TryParse(cookie.Value, out cid) ? cid : CategoryManager.GetDefaultCategoryNode().CategoryId;
                return cid == 0 ? CategoryManager.GetDefaultCategoryNode().CategoryId : cid;
            }
        }

        public int FemaleCategoryId
        {
            get
            {
                HttpCookie cookie = Request.Cookies.Get(LkSiteCookie.FemaleCategoryId.ToString("g"));

                if (cookie == null)
                {
                    cookie = CookieManager.NewCookie(
                        LkSiteCookie.FemaleCategoryId.ToString("g"),
                        CategoryManager.Default.FemaleTaipei.CategoryId.ToString(),
                        DateTime.Now.AddDays(1)
                    );
                    Response.Cookies.Add(cookie);
                }

                int cid = int.TryParse(cookie.Value, out cid) ? cid : CategoryManager.Default.FemaleTaipei.CategoryId;
                return cid == 0 ? CategoryManager.Default.FemaleTaipei.CategoryId : cid;
            }
        }

        public int SubRegionCategoryId
        {
            get
            {
                string subcategoryString = string.Empty;
                if (Request.QueryString["subc"] != null)
                {
                    subcategoryString = Request.QueryString["subc"];
                }
                HttpCookie cookie = Request.Cookies.Get(LkSiteCookie.SubRegionCategroyId.ToString("g"));
                if (cookie == null)
                {
                    cookie = CookieManager.NewCookie(LkSiteCookie.SubRegionCategroyId.ToString("g"),
                        subcategoryString,
                        DateTime.Now.AddDays(1)
                    );
                    Response.Cookies.Add(cookie);
                }
                else
                {
                    cookie.Value = subcategoryString;
                }
                int cid = int.TryParse(cookie.Value, out cid) ? cid : CategoryManager.GetDefaultCategoryNode().CategoryId;
                return cid == 0 ? CategoryManager.GetDefaultCategoryNode().CategoryId : cid;
            }
        }

        public string CategoryName
        {
            get
            {
                Category cate = CategoryManager.CategoryGetById(CategoryIdInPiinLife);
                return cate != null ? cate.Name : string.Empty;
            }
        }

        /// <summary>
        /// 過濾「即買急用」或「最後一天」等 (checkbox的分類)
        /// </summary>
        public List<int> FilterCategoryIdList
        {
            get
            {
                if (Request.QueryString["filter"] != null)
                {
                    PponDealListCondition.SetFilterCategoryIdListFromSession(this.SelectedChannelId, Request.QueryString["filter"]);
                }
                return PponDealListCondition.GetFilterCategoryIdListFromSession(this.SelectedChannelId);
            }
        }

        public List<CategoryNode> DealChannelList { get; set; }

        /// <summary>
        /// 城市CityId
        /// </summary>
        public int SelectedCityId {
            get { return Master.SelectedCityId; }
        }

        /// <summary>
        /// 頻道CategoryId
        /// </summary>
        public int SelectedChannelId
        {
            get { return Master.SelectedChannelId; }
        }

        /// <summary>
        /// 區域categoryId
        /// </summary>
        public int SelectedRegionId
        {
            get { return Master.SelectedRegionId; }
        }

        /// <summary>
        /// 區域子分類categoryId
        /// </summary>
        public int SelectedSubcategoryId
        {
            get { return Master.SelectedSubcategoryId; }
        }

        /// <summary>
        /// 新版分類Id
        /// </summary>
        public int SelectedDealCategoryId
        {
            get { return Master.SelectedDealCategoryId; }
        }

        #endregion  city, category相關

        public Guid BusinessHourId
        {
            get
            {
                string bid_querystring = string.Empty;
                Guid bGuid = Guid.Empty;
                if (Page.RouteData.Values["bid"] != null)
                {
                    bid_querystring = Page.RouteData.Values["bid"].ToString();
                }
                else if (!string.IsNullOrWhiteSpace(Request.QueryString["bid"]))
                {
                    bid_querystring = Request.QueryString["bid"];
                }

                if (!string.IsNullOrWhiteSpace(bid_querystring))
                {
                    if (bid_querystring.Split(',').Length > 1)
                    {
                        bid_querystring = bid_querystring.Split(',')[0];
                    }

                    if (!Guid.TryParse(bid_querystring, out bGuid))
                    {
                        bGuid = Guid.Empty;
                    }
                }
                else if (ViewState["bid"] != null)
                {
                    bGuid = new Guid(ViewState["bid"].ToString());
                }

                return bGuid;
            }

            set
            {
                ViewState["bid"] = value;
            }
        }

        /// <summary>
        /// 檔號，目前先隱藏檔號判斷檔次，直接回傳-1
        /// </summary>
        public int BusinessHourUniqueId
        {
            get
            {
                return -1;
            }
            set
            {
                ViewState["uid"] = value;
            }
        }

        /// <summary>
        /// rsrc參數
        /// </summary>
        public string Rsrc
        {
            get
            {
                if (Page.RouteData.Values["rsrc"] != null)
                {
                    return Page.RouteData.Values["rsrc"].ToString();
                }
                else if (!string.IsNullOrWhiteSpace(Request.QueryString["rsrc"]))
                {
                    return Request.QueryString["rsrc"];
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        /// <summary>
        /// Hami
        /// </summary>
        public string Hami
        {
            get
            {
                if (Page.RouteData.Values["rsrc"] != null && Page.RouteData.Values["rsrc"].ToString().ToLower() == "hinet_hammi")
                {
                    Session[LkSiteSession.Hami.ToString()] = Page.RouteData.Values["rsrc"].ToString().ToLower();
                }
                else if (!string.IsNullOrWhiteSpace(Request.QueryString["rsrc"]) && Request.QueryString["rsrc"].ToLower() == "hinet_hammi")
                {
                    Session[LkSiteSession.Hami.ToString()] = Request.QueryString["rsrc"].ToLower();
                }
                return Session[LkSiteSession.Hami.ToString()] != null ? Session[LkSiteSession.Hami.ToString()].ToString() : string.Empty;
            }
        }

        public CategorySortType SortType
        {
            get
            {
                if (Session["SortType"] == null)
                {
                    Session["SortType"] = CategorySortType.Default;
                }

                return (CategorySortType)Session["SortType"];
            }
            set
            {
                Session["SortType"] = value;
            }
        }

        public string GoogleApiKey
        {
            get
            {
                return ProviderFactory.Instance().GetConfig().GoogleMapsAPIKey;
            }
        }

        public string FB_AppId
        {
            get
            {
                return ProviderFactory.Instance().GetConfig().FacebookApplicationId;
            }
        }
        /// <summary>
        /// App Title or CouponUsage
        /// </summary>
        public string AppTitle { set; get; }
        /// <summary>
        /// meta og:title content
        /// </summary>
        public string OgTitle { set; get; }
        /// <summary>
        /// meta og:url content
        /// </summary>
        public string OgUrl { set; get; }
        /// <summary>
        /// meta og:description content
        /// </summary>
        public string OgDescription { set; get; }
        /// <summary>
        /// meta og:image content
        /// </summary>
        public string OgImage { set; get; }
        /// <summary>
        /// link rel=canonical href
        /// </summary>
        public string LinkCanonicalUrl { set; get; }
        /// <summary>
        /// link rel=image_src href
        /// </summary>
        public string LinkImageSrc { set; get; }

        public int Encores { get; set; }

        public string UserName
        {
            get
            {
                return Page.User.Identity.Name;
            }
        }

        public int UserId
        {
            get
            {
                var pponUser = User.Identity as PponIdentity;
                if (pponUser != null)
                {
                    return pponUser.Id;
                }
                return MemberFacade.GetUniqueId(Page.User.Identity.Name);
            }
        }

        //前端資料以商品的憑證宅配撈取20120314
        public DeliveryType[] RepresentedDeliverytype
        {
            get
            {
                return new DeliveryType[2] { DeliveryType.ToHouse, DeliveryType.ToShop };
            }
        }

        private bool isDeliverDeal = false;
        public bool IsDeliveryDeal
        {
            set
            {
                this.isDeliverDeal = value;
            }
            get
            {
                return this.isDeliverDeal;
            }
        }

        public bool TwentyFourHours { get; set; }

        public bool ShowEventEmail { get; set; }

        public string HamiTicketId
        {
            get
            {
                if (!string.IsNullOrWhiteSpace(Request.QueryString["TicketNo"]))
                {
                    return Request.QueryString["TicketNo"];
                }
                return null;
            }
        }

        public int? DealType { get; set; }

        //public bool EntrustSell
        //{
        //    set
        //    {
        //        liEntrustSell.Visible = value;
        //    }
        //}

        public string PicAlt { get; set; }

        public bool isUseNewMember { get; set; }

        public DateTime Now
        {
            get
            {
                return DateTime.Now;
            }
        }

        public bool isMultipleMainDeals { get; set; }

        public bool BypassSsoVerify { get; set; }

        //public IExpireNotice ExpireNotice
        //{
        //    get
        //    {
        //        return PponExpireNotice;
        //    }
        //}
        public bool IsNewMobileSetting
        {
            get { return config.IsNewMobileSetting; }
        }

        public string EdmPopUpCacheName { get; set; }

        public bool IsOpenNewWindow { get; set; }

        public bool IsMobileBroswer
        {
            get
            {
                var userAgent = HttpContext.Current.Request.UserAgent;
                if ((!string.IsNullOrEmpty(userAgent)) &&
                    ((userAgent.ToLower().Contains("iphone")
                        || (userAgent.ToLower().Contains("ipod")
                        || (userAgent.ToLower().Contains("android")
                        || (userAgent.ToLower().Contains("Blackberry")))))))
                {
                    return true;
                }
                return false;
            }
        }

        private bool _isKindDeal;

        public bool IsKindDeal
        {
            get
            {
                return _isKindDeal;
            }
            set
            {
                _isKindDeal = value;
            }
        }

        public string iOSAppId
        {
            get
            {
                return config.iOSAppId;
            }
        }

        public string AndroidAppId
        {
            get
            {
                return config.AndroidAppId;
            }
        }

        public string AndroidUserAgent
        {
            get
            {
                return config.AndroidUserAgent;
            }
        }

        public string iOSUserAgent
        {
            get
            {
                return config.iOSUserAgent;
            }
        }

        public int CloseAppBlockHiddenDays
        {
            get
            {
                return config.CloseAppBlockHiddenDays;
            }
        }

        public int ViewAppBlockHiddenDays
        {
            get
            {
                return config.ViewAppBlockHiddenDays;
            }
        }

        public decimal MinGross
        {
            get
            {
                return config.GrossMargin;
            }
        }

        public bool EnableGrossMarginRestrictions
        {
            get
            {
                return config.EnableGrossMarginRestrictions;
            }
        }

        public string DealArgs { set; get; }

        #endregion Props

        #region interface methods

        public void SetMailInfo(int cityId)
        {
            p1.Visible = false;

            PponCity defaultCity = PponCityGroup.DefaultPponCityGroup.TaipeiCity;
            PponCity tmpCity = PponCityGroup.DefaultPponCityGroup.GetPponCityByCityId(cityId);
            if ((cityId != -1) && tmpCity != null)
            {
                defaultCity = tmpCity;
            }
        }

        public void RedirectToPiinlife()
        {
            Response.Redirect(string.Format("{0}/piinlife/{1}", SystemConfig.SiteUrl, BusinessHourId.ToString())
                + (!string.IsNullOrWhiteSpace(Rsrc) ? string.Format("/{0}", Rsrc) : string.Empty)
                + (Request.QueryString["token"] != null && !string.IsNullOrWhiteSpace(Request.QueryString["token"].ToString()) ? string.Format("?token={0}", Request.QueryString["token"].ToString()) : string.Empty));
        }

        public void RedirectToDefault()
        {
            Response.Redirect(string.Format("{0}/{1}", SystemConfig.SiteUrl, "ppon/default.aspx"));
        }

        /// <summary>
        /// 設定一組Cookie到client端
        /// </summary>
        /// <param name="aCookie"></param>
        public bool SetCookie(String value, DateTime expireTime, LkSiteCookie cookieField)
        {
            HttpCookie referenceCookie = CookieManager.NewCookie(cookieField.ToString("g"));

            referenceCookie.Value = value;
            referenceCookie.Expires = expireTime;
            //設定cookie值於使用者電腦
            try
            {
                Response.Cookies.Add(referenceCookie);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public void ShowMessage(string message)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('" + message + "');", true);
        }

        /// <summary>
        /// 加入meta避免被google爬蟲爬到
        /// </summary>
        public void NoRobots()
        {
            phNorobots.Visible = true;
        }

        public void SeoSetting(ViewPponDeal d, string appendTitle = "")
        {
            if (String.IsNullOrWhiteSpace(d.PageDesc))
            {
                this.Page.MetaDescription = d.CouponUsage + "-" + d.EventTitle; //好康券+橘標
            }
            else
            {
                this.Page.MetaDescription = d.PageDesc;
            }

            if (String.IsNullOrWhiteSpace(d.PageTitle))
            {
                if (!string.IsNullOrEmpty(appendTitle))
                {
                    this.Page.Title = string.Format("{0}，{1}", d.CouponUsage, appendTitle);
                }
                else
                {
                    this.Page.Title = string.Format("{0}-{1}，{2}", d.CouponUsage, d.ItemName, SystemConfig.Title);
                }
            }
            else
            {
                this.Page.Title = d.PageTitle;
            }

            PicAlt = string.IsNullOrWhiteSpace(d.PicAlt) ? (string.IsNullOrEmpty(d.AppTitle) ? d.CouponUsage : d.AppTitle) : d.PicAlt;
            this.Page.MetaKeywords = string.Format("{0},{1}", string.IsNullOrEmpty(PicAlt) ? string.Empty : PicAlt.Replace("/", ","), this.Page.MetaKeywords);
        }

        public void SetMultipleCategories(List<CategoryViewItem> dealCategoryCountList,
            List<CategoryDealCount> filterItemList, SystemCodeCollection piinlifeLinks)
        {
            // 分類
            rpt_Category.DataSource = dealCategoryCountList;
            rpt_Category.DataBind();

            repCategorysContainer.DataSource = dealCategoryCountList;
            repCategorysContainer.DataBind();

            // 篩選
            rpt_Filter.DataSource = filterItemList;
            rpt_Filter.DataBind();
        }

        protected void repCategorysContainerItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.Item && e.Item.ItemType != ListItemType.AlternatingItem)
            {
                return;
            }
            Repeater repSubCategory = (Repeater)e.Item.FindControl("repSubCategory");
            CategoryViewItem data = (CategoryViewItem)e.Item.DataItem;

            repSubCategory.DataSource = data.Children;
            repSubCategory.DataBind();


            PlaceHolder phContainerOn = (PlaceHolder)e.Item.FindControl("phContainerOn");
            PlaceHolder phContainerOff = (PlaceHolder)e.Item.FindControl("phContainerOff");
            phContainerOn.Visible = data.Selected  && data.Children.Count > 0;
            phContainerOff.Visible = data.Selected == false || data.Children.Count == 0;
        }

        /// <summary>
        /// 排序
        /// </summary>
        /// <returns></returns>
        public List<ViewCategorySortTypes> GetCategorySortTypes()
        {
            List<CategorySortType> categorySortTypes = Enum.GetValues(typeof(CategorySortType)).Cast<CategorySortType>().Where(x => x != CategorySortType.PriceDesc && x != CategorySortType.MemberCollectDesc && x != CategorySortType.MemberCollectAsc).ToList();
            List<ViewCategorySortTypes> sortTypes = new List<ViewCategorySortTypes>();
            foreach (var type in categorySortTypes)
            {
                string name = string.Empty;
                string cssClass = string.Empty;
                string title = string.Empty;

                CategorySortType data = (CategorySortType)type;
                title = Helper.GetLocalizedEnum(data);
                switch (data)
                {
                    case CategorySortType.TopNews:
                        name = "最新";
                        break;

                    case CategorySortType.TopOrderTotal:
                        name = "銷量";
                        break;

                    case CategorySortType.DiscountAsc:
                        name = "折數";
                        break;

                    case CategorySortType.PriceAsc:
                        string spanClass = "icon-arrow-up";
                        // 如果目前搜尋為價格(低到高)，則設定再次點選為價格(高到低)
                        if (SortType == CategorySortType.PriceAsc)
                        {
                            cssClass = "up";
                        }
                        else if (SortType == CategorySortType.PriceDesc)
                        {
                            title = Helper.GetLocalizedEnum(CategorySortType.PriceDesc);
                            cssClass = "down on";
                            spanClass = "icon-arrow-down";
                        }
                        else
                        {
                            cssClass = "up";
                        }
                        name = "價格<span class=\"" + spanClass + "\"></span>";
                        break;

                    case CategorySortType.Default:
                    default:
                        name = "推薦";
                        break;
                }
                cssClass += (data == SortType ? " on" : string.Empty);

                ViewCategorySortTypes sortType = new ViewCategorySortTypes();
                sortType.CssClass = cssClass;
                sortType.Name = name;
                sortType.Title = title;
                sortType.CategorySortType = data;
                sortTypes.Add(sortType);
            }
            return sortTypes;
        }

        /// <summary>
        /// 熱銷檔次
        /// </summary>
        /// <param name="hotdeals"></param>
        public void SetHotSaleMutilpMainDeals(List<MultipleMainDealPreview> hotdeals)
        {
            this._viewMultipleHotDeals = SetMultipleSideDeal(hotdeals);
        }
        /// <summary>
        /// 今日開賣檔次
        /// </summary>
        /// <param name="toDaydeals"></param>
        public void SetToDayMutilpMainDeals(List<MultipleMainDealPreview> todaymultiplemaindeals)
        {
            this._viewMultipleTodayDeals = SetMultipleSideDeal(todaymultiplemaindeals);
        }
        /// <summary>
        /// 最後倒數檔次
        /// </summary>
        /// <param name="LastDaydeals"></param>
        public void SetLastDayMutilpMainDeals(List<MultipleMainDealPreview> lastdaymultiplemaindeals)
        {
            this._viewMultipleLastDayDeals = SetMultipleSideDeal(lastdaymultiplemaindeals);
        }

        public void SetMutilpMainDeals(List<MultipleMainDealPreview> multiplemaindeals)
        {
            CheckMultipleMainDealPanel();
            pan_EmptyZone.Visible = !multiplemaindeals.Any();


            //The Open Graph protocol (facebook 分享參數)
            this.OgUrl = this.LinkCanonicalUrl = new Uri(HttpContext.Current.Request.Url.ToString()).AbsoluteUri;
            this.OgTitle = SystemConfig.DefaultMetaOgTitle;
            this.OgImage = SystemConfig.DefaultMetaOgImage;
            this.OgDescription = SystemConfig.DefaultMetaOgDescription;

            string title, metaDescription;
            GetChannelTitleAndMetaDescription(OgUrl, out title, out metaDescription);
            this.Page.Title = title;
            this.Page.MetaDescription = metaDescription;
        }

        private void GetChannelTitleAndMetaDescription(string url, out string title, out string metaDescription)
        {
            title = SystemConfig.DefaultTitle;
            metaDescription = SystemConfig.DefaultMetaDescription;

            if (Request.Path.Equals("/default.aspx", StringComparison.OrdinalIgnoreCase))
            {
                title = "17Life生活電商，團購、宅配24小時出貨、優惠券、即買即用";
                metaDescription = "17Life生活電商，團購商品、電子票券、餐券、咖啡寄杯超低折扣優惠，天天都有好驚喜！全台吃喝玩樂電子票券，讓您吃美食、做SPA、住飯店，精彩生活不打折！";
            }
            else if (this.SelectedChannelId != 0)
            {
                ChannelCategory channelCategory = (ChannelCategory)SelectedChannelId;
                switch (channelCategory)
                {
                    case ChannelCategory.Food:
                        title = String.Format("{0} - {1}", IsImmediately(url) ? "即買即用美食餐券" : "美食頻道",
                            SystemConfig.Title);
                        metaDescription = IsImmediately(url)
                            ? "想吃美食不用苦等餐券寄到家，17life電子餐券讓你即刻享受美味生活，記得先跟店家預約訂位唷！"
                            : "王品集團與數百家知名美食餐廳的【破盤價餐券】就在17life！吃到飽、海鮮、火鍋等人氣美食，即買即用超方便！";
                        break;
                    case ChannelCategory.Delivery:
                        title = string.Format("宅配頻道 - {0}", SystemConfig.Title);
                        metaDescription = "你想找的人氣團購美食，生活用品通通都在17life！宅配24H快速出貨，免運送到家。天天享有超低驚喜價，讓你輕鬆買到高CP好物！";
                        break;
                    case ChannelCategory.Travel:
                        title = string.Format("旅遊頻道 - {0}", SystemConfig.Title);
                        metaDescription = "想找高CP值旅遊票券就上17life！飯店、民宿、溫泉、樂園門票應有盡有，還有全網最殺獨家優惠，完美旅程一手搞定！";
                        break;
                    case ChannelCategory.Beauty:
                        title = string.Format("玩美休閒 - {0}", SystemConfig.Title);
                        metaDescription = "寵愛自己，體驗質感玩美生活就上17life！美髮沙龍、美甲美睫、精油SPA、中泰式按摩，可享全網最低驚喜價！";
                        break;
                    default:
                        title = SystemConfig.DefaultTitle;
                        metaDescription = SystemConfig.DefaultMetaDescription;
                        break;
                }
            }
        }

        private bool IsImmediately(string url)
        {
            return url.Split('/').Last() == ((int) ChannelCategory.Immediately).ToString();
        }

        private List<ViewMultipleSideDeal> SetMultipleSideDeal(List<MultipleMainDealPreview> deals)
        {
            List<ViewMultipleSideDeal> viewMultipleSideDeals = new List<ViewMultipleSideDeal>();
            foreach (var deal in deals)
            {
                ViewMultipleSideDeal viewSidePreview = new ViewMultipleSideDeal();
                PponCity pponcity = PponCityGroup.DefaultPponCityGroup.GetPponCityByCityId(deal.CityID);

                viewSidePreview.deal = deal;

                viewSidePreview.clit_CityName2 = ViewPponDealManager.DefaultManager.GetCityNameOrTravelPlace(
                pponcity, deal.PponDeal.BusinessHourGuid);

                if (_isKindDeal)
                {
                    viewSidePreview.discount_1 = "公益";
                }
                else if (Helper.IsFlagSet(deal.PponDeal.BusinessHourStatus, BusinessHourStatus.NoDiscountShown)
                      || Helper.IsFlagSet(deal.PponDeal.BusinessHourStatus, BusinessHourStatus.PriceZeorShowOriginalPrice))
                {
                    viewSidePreview.discount_1 = "特選";
                }
                else if (deal.PponDeal.ItemPrice == 0)
                {
                    if (CityId.Equals(PponCityGroup.DefaultPponCityGroup.Family.CityId) && deal.PponDeal.ExchangePrice.HasValue && deal.PponDeal.ExchangePrice.Value > default(decimal))
                    {
                        if (Helper.IsFlagSet(deal.PponDeal.BusinessHourStatus, BusinessHourStatus.NoDiscountShown))
                        {
                            viewSidePreview.discount_1 = "特選";
                        }
                        else
                        {
                            string discount_2 = ViewPponDealManager.GetDealDiscountString(Helper.IsFlagSet(deal.PponDeal.BusinessHourStatus, BusinessHourStatus.NoDiscountShown), CheckZeroPriceToShowPrice(deal.PponDeal), deal.PponDeal.ItemOrigPrice);
                            viewSidePreview.discount_1 = discount_2 + "折";
                        }
                    }
                    else
                    {
                        viewSidePreview.discount_1 = "優惠";
                    }
                }
                else
                {
                    string discount = ViewPponDealManager.GetDealDiscountString(Helper.IsFlagSet(deal.PponDeal.BusinessHourStatus, BusinessHourStatus.NoDiscountShown), CheckZeroPriceToShowPrice(deal.PponDeal), deal.PponDeal.ItemOrigPrice);
                    int length = discount.Length;
                    if (length > 0)
                    {
                        string discount_2 = discount.Substring(0, 1);
                        if (discount.IndexOf('.') != -1 && length > 2)
                        {
                            discount_2 += discount.Substring(1, length - 1);
                        }
                        viewSidePreview.discount_1 = discount_2 + "折";
                    }
                }
                viewSidePreview.EveryDayNewDeal = PponFacade.GetEveryDayNewDealFlag(deal.PponDeal.BusinessHourGuid);
                viewMultipleSideDeals.Add(viewSidePreview);
            }

            return viewMultipleSideDeals;
        }

        public void SetEventMainActivities(KeyValuePair<EventActivity, CmsRandomCityCollection> pair)
        {
            Cache.Remove(EdmPopUpCacheName);
            //修正跳窗EDM暫存誤差
            double CacheMin = Math.Floor((pair.Key.EndDate - DateTime.Now).TotalMinutes);
            CacheMin = (CacheMin > 60 ? 60 : CacheMin);
            Cache.Insert(EdmPopUpCacheName, pair, null, DateTime.Now.AddMinutes(CacheMin), TimeSpan.Zero, CacheItemPriority.NotRemovable, null);
            if (!string.IsNullOrWhiteSpace(Rsrc) && pair.Key.ExcludeCpaList.Contains(Rsrc))
            {
                ShowEventEmail = false;
            }
            else if (!pair.Value.Any(x => x.CityId == CityId))
            {
                ShowEventEmail = false;
            }
            else
            {
                ShowEventEmail = true;
            }
        }

        public void SetMemberCollection(List<Guid> collectBids)
        {
            string jsonStr = ProviderFactory.Instance().GetSerializer()
                .Serialize(collectBids);
            hdMemberCollectDealGuidJson.Value = jsonStr;
        }

        #endregion interface methods

        #region page

        protected void Page_Init(object sender, EventArgs e)
        {
            //緊急修補簡訊發錯，導向M
            if (CommonFacade.IsMobileVersion() && this.CityId == 477 && this.Rsrc == "17_text")
            {
                /*
                桌機copy的連結
                https://www.17life.com/channel/88
                手機copy的連結
                https://www.17life.com/m/477
                 */
                Response.Redirect("https://www.17life.com/m/477?rsrc=17_text");
            }

            string redirectUrl = SystemConfig.SiteUrl.TrimEnd('/');

            if (Request.QueryString["bid"] != null)
            {
                Uri tempUrl = new Uri(Helper.RemoveQueryStringByKey(HttpContext.Current.Request.Url.ToString().ToLower(), "bid"));
                redirectUrl += string.Format(@"/deal/{0}{1}", Request.QueryString["bid"], tempUrl.Query);
                Response.Redirect(redirectUrl);
            }
            if (Request["cid"] != null)
            {
                int cid;
                if (int.TryParse(Request["cid"], out cid))
                {
                    if (this.PponCityId != cid)
                    {
                        this.PponCityId = cid;
                    }
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            #region 收集商品頁使用了哪些 route ，以做為後續調整的依據

            string routeName = this.RouteData.GetRouteName();
            routeNameUsedStat.AddOrUpdate(routeName, 1, (s, i) => i + 1);
            if (Request.QueryString["stat"] == "1")
            {
                Response.ContentType = "text/plain";

                Response.Write("頁面瀏覽數:" + cache.Get<int>("DefaultPageCount") + "\r\n");
                Response.Write("瀏覽下頁數:" + cache.Get<int>("DefaultPponDealList") + "\r\n");
                Response.Write("頁面排序1:" + cache.Get<int>("DefaultPageSort-0") + "\r\n");
                Response.Write("頁面排序2:" + cache.Get<int>("DefaultPageSort-1") + "\r\n");
                Response.Write("頁面排序3:" + cache.Get<int>("DefaultPageSort-2") + "\r\n");
                Response.Write("頁面排序4:" + cache.Get<int>("DefaultPageSort-3") + "\r\n");
                Response.Write("頁面排序5:" + cache.Get<int>("DefaultPageSort-4") + "\r\n");
                Response.Write("頁面排序6:" + cache.Get<int>("DefaultPageSort-5") + "\r\n");
                Response.Write("頁面排序7:" + cache.Get<int>("DefaultPageSort-6") + "\r\n");
                Response.Write("頁面排序8:" + cache.Get<int>("DefaultPageSort-7") + "\r\n");

                Response.End();
            }
            else if (Request.QueryString["stat"] == "0")
            {
                Response.ContentType = "text/plain";
                ResetCache();
                Response.Write("清除快取\r\n");
                Response.End();
            }
            if (Request.QueryString["debug"] == "1")
            {
                Response.ContentType = "text/plain";           

                foreach (var pair in routeNameUsedStat)
                {
                    Response.Write(pair.Key + ": " + pair.Value + "\r\n");
                }
                Response.End();
            }

            IncrementDefaultPageCount();

            #endregion
            if (!Page.IsPostBack)
            {
                this.Presenter.OnViewInitialized();
                if (Session[LkSiteSession.ShareLinkText.ToString()] != null && User.Identity.IsAuthenticated)
                {
                    Session.Remove(LkSiteSession.ShareLinkText.ToString());
                    if (string.IsNullOrWhiteSpace(tbx_ReferralShare.Text))
                    {
                        tbx_ReferralShare.Text = GetShareText(BusinessHourId);
                    }
                    ScriptManager.RegisterStartupScript(this, typeof(Button), "showshare", "showshare('" + tbx_ReferralShare.Text + "');", true);
                }
            }
            this.Presenter.OnViewLoaded();
            this.Master.IsShowBanner = true;
            this.Master.IsShowCuration = true;

            if (_cityId == 0)
            {
                _cityId = CityId;
            }

            //App Links
            string bid = BusinessHourId == Guid.Empty ? string.Empty : BusinessHourId.ToString();
            HttpCookie cookie = Request.Cookies.Get(LkSiteCookie.ReferenceId.ToString("g"));
            string extId = cookie != null ? cookie.Value : string.Empty;

            if (string.IsNullOrWhiteSpace(bid))
            { 
                lit_PadCss.Text = "<link href='" + ResolveUrl("../themes/PCweb/css/RDL-Pad.css") + "' rel='stylesheet' type='text/css' />";
            }

            #region 導向推薦商品
            if (Request.QueryString["nsurl"] != null)
            {
                string expireDesc = Helper.GetEnumDescription(ExpireRedirectDisplay.Display1);
                string deUrl = HttpUtility.UrlDecode(Request.QueryString["nsurl"].ToString());
                if(deUrl.IndexOf(config.SSLSiteUrl) >= 0)
                {
                    List<string> correctUrl = new List<string>()
                    {
                        config.SSLSiteUrl + "/event/brandevent",
                        config.SSLSiteUrl + "/deal/",
                        config.SSLSiteUrl + "/ppon/pponsearch.aspx?search",
                    };

                    if( correctUrl.Any(x => deUrl.StartsWith(x)))
                    {
                        string js = "if (confirm('" + expireDesc + "')) {location.href='" + deUrl + "'}";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "ExpiredRedirectUrl", js, true);
                    }                    
                }
            }
            #endregion 導向推薦商品
        }

        protected void RandomPponNewsInit(object sender, EventArgs e)
        {
            int tempCity = _cityId == 0 ? CityId : _cityId;
            saleB.CityId = news.CityId = Master.PponNews.CityId = outsideAD.CityId = insideAD.CityId = active.CityId = tempCity;
            saleB.Type = news.Type = Master.PponNews.Type = outsideAD.Type = insideAD.Type = active.Type = RandomCmsType.PponRandomCms;
        }

        #endregion page

        #region other methods

        protected static string GetShareText(Guid bid)
        {
            if (HttpContext.Current.Session[LkSiteSession.ExternalMemberId.ToString()] == null)
            {
                MemberUtility.SetUserSsoInfoReady(MemberFacade.GetUniqueId(HttpContext.Current.User.Identity.Name));
            }

            ExternalMemberInfo emi = HttpContext.Current.Session[LkSiteSession.ExternalMemberId.ToString()] as ExternalMemberInfo;

            if (emi != null)
            {
                string origUrl = WebUtility.GetSiteRoot() + "/ppon/PponReferralRedirection.aspx?bid=" + bid + "," + (int)emi[0].Source + "|" + emi[0].ExternalId;
                string shortUrl = WebUtility.RequestShortUrl(origUrl);
                PromotionFacade.SetDiscountReferrerUrl(origUrl, shortUrl, MemberFacade.GetUniqueId(HttpContext.Current.User.Identity.Name), HttpContext.Current.User.Identity.Name, bid, 0);
                return shortUrl;
            }
            else
            {
                return string.Empty;
            }
        }

        protected void CheckMultipleMainDealPanel()
        {
            //pan_Multiple.Visible = isMultipleMainDeals;
            divCategory.Visible = (isMultipleMainDeals && CityId != PponCityGroup.DefaultPponCityGroup.Family.CityId) ? true : false;
            if (CityId == PponCityGroup.DefaultPponCityGroup.Family.CityId)
            {
                pan_Sorting.Visible = false;
                lit_Sorting.Text = " <ul id='ulSort' class='clearfix'><li>&nbsp;</li></ul>";
            }
        }

        public decimal CheckZeroPriceToShowPrice(IViewPponDeal deal)
        {
            return PponFacade.CheckZeroPriceToShowPrice(deal, CityId);
        }

        public bool IsShowAveragePriceInfo(ViewComboDeal deal)
        {
            return (deal.IsAveragePrice && deal.QuantityMultiplier != null && deal.QuantityMultiplier >= 1);
        }

        public decimal CheckZeroPriceToShowAveragePrice(ViewComboDeal deal)
        {
            if (decimal.Equals(0, deal.ItemPrice))
            {
                return deal.ItemPrice;
            }
            else
            {
                if (IsShowAveragePriceInfo(deal))
                {
                    return Math.Ceiling(deal.ItemPrice / deal.QuantityMultiplier.Value);
                }
                else
                {
                    return deal.ItemPrice;
                }
            }
        }

        /// <summary>
        /// 判斷當前頻道是否需要顯示區域選單 (給前端呼叫)
        /// </summary>
        /// <returns></returns>
        public bool IfShowCategoryTree()
        {
            bool show = false;
            List<int> channelNeedShow = new List<int>()
            {
                CategoryManager.pponChannelId.Food,
                CategoryManager.pponChannelId.Travel,
                CategoryManager.pponChannelId.Beauty,
            };

            if (channelNeedShow.Contains(this.SelectedChannelId))
            {
                show = true;
            }
            return show;
        }

        #region WebMethod

        [WebMethod]
        public static string GetShareLink(string businessHourId)
        {
            if (HttpContext.Current.User == null || !HttpContext.Current.User.Identity.IsAuthenticated)
            {
                return string.Empty;
            }
            else
            {
                Guid bid = Guid.TryParse(businessHourId, out bid) ? bid : Guid.Empty;
                string url = GetShareText(bid);
                return url ?? Helper.CombineUrl(config.SiteUrl, "deal/" + businessHourId);
            }
        }

        [WebMethod]
        public static string PostToFaceBook(string businessHourId)
        {
            if ((HttpContext.Current.User == null || !HttpContext.Current.User.Identity.IsAuthenticated) ||
                !(new DefaultPponPresenter()).AddUserTrack("", "", "/ppon/default.aspx", "", "FaceBook"))
            {
                return string.Empty;
            }
            else
            {
                Guid bid = Guid.TryParse(businessHourId, out bid) ? bid : Guid.Empty;
                GetShareText(bid);
                return "Y";
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="city"></param>
        /// <returns></returns>
        [WebMethod]
        public static string GetUrl(string city)
        {
            string url = config.SiteUrl;
            int cityId = int.TryParse(city, out cityId) ? cityId : PponCityGroup.DefaultPponCityGroup.TaipeiCity.CityId;
            PponCity pponCity = PponCityGroup.DefaultPponCityGroup.GetPponCityByCityId(cityId);
            switch (pponCity.CityCode)
            {
                case "ALL":
                    url += "/Event/Exhibition.aspx?u=PDelivery&rsrc=17_eventDL";
                    break;

                case "TRA":
                    url += "/Event/Exhibition.aspx?u=PTravel&rsrc=17_eventtravel";
                    break;

                case "PBU":
                    url += "/Event/Exhibition.aspx?u=PBeauty&rsrc=17_eventpeauty";
                    break;

                default:
                    url += "/" + pponCity.CityId.ToString();
                    break;
            }
            var referenceCookie = CookieManager.NewCookie(
                "CityId",
                pponCity.CityId.ToString(), 
                DateTime.Now.AddMonths(3)
            );
            HttpContext.Current.Response.Cookies.Add(referenceCookie);
            return url;
        }

        [WebMethod]
        public static void SetSortCategory(int type)
        {
            string cacheKey = string.Format("DefaultPageSort-{0}", type);
            cache.Increment(cacheKey);

            CategorySortType nowSortType = CategorySortType.Default;
            if (HttpContext.Current.Session["SortType"] != null)
            {
                nowSortType = (CategorySortType)HttpContext.Current.Session["SortType"];
            }
            CategorySortType ctype = (CategorySortType)type;
            CategorySortType sortType = ctype;
            if (ctype == CategorySortType.PriceAsc)
            {
                if (nowSortType == CategorySortType.PriceAsc)
                {
                    sortType = CategorySortType.PriceDesc;
                }
                else if (nowSortType == CategorySortType.PriceDesc)
                {
                    sortType = CategorySortType.PriceAsc;
                }
            }
            HttpContext.Current.Session["SortType"] = sortType;
        }

        [WebMethod]
        public static void SetSelectedFilter(int channel, string id, bool isChecked)
        {
            string key = string.Format("DefaultPageFilter-{0}", channel);
            cache.Increment(string.Format("{0}-{1}", key, id));
            PponDealListCondition.SetFilterCategoryIdListFromSession(channel, id, isChecked);
        }

        [WebMethod]
        public static bool CheckMemberCollectDealStatus(Guid businessHourGuid)
        {
            //check login, if not login return false.
            if (!HttpContext.Current.User.Identity.IsAuthenticated)
            {
                return false;
            }
            var pponUser = HttpContext.Current.User.Identity as PponIdentity;
            int userId;
            if (pponUser == null)
            {
                userId = MemberFacade.GetUniqueId(HttpContext.Current.User.Identity.Name);
            }
            else
            {
                userId = pponUser.Id;
            }
            return MemberFacade.MemberCollectDealIsCollected(userId, businessHourGuid);
        }

        [WebMethod]
        public static string SetMemberCollectDeal(Guid businessHourGuid, int cityId, bool isCollecting)
        {
            //check login.
            if (HttpContext.Current.User.Identity.IsAuthenticated == false)
            {
                HttpContext.Current.Response.StatusCode = 500;
                HttpContext.Current.Response.StatusDescription = "userNotLogin";
                return new LunchKingSite.Core.JsonSerializer().Serialize(new { Success = false, IsCollecting = false });
            }

            int memberUnique = MemberFacade.GetUniqueId(HttpContext.Current.User.Identity.Name);
            bool result = false;

            if (isCollecting && businessHourGuid != Guid.Empty)
            {
                MemberCollectDeal mcd = new MemberCollectDeal();
                mcd.MemberUniqueId = memberUnique;
                mcd.BusinessHourGuid = businessHourGuid;
                mcd.CollectStatus = (Byte)MemberCollectDealStatus.Collected;
                mcd.CollectTime = System.DateTime.Now;
                mcd.LastAccessTime = System.DateTime.Now;
                mcd.CityId = cityId;
                result = MemberFacade.MemberCollectDealSave(mcd);
            }
            else
            {
                result = MemberFacade.MemberCollectDealRemove(memberUnique, businessHourGuid);
            }

            return new LunchKingSite.Core.JsonSerializer().Serialize(new { Success = result, IsCollecting = isCollecting });
        }

        [WebMethod]
        public static string GetSideDeals(List<Guid> bids, int workCityId, int takeCount)
        {
            try
            {
                workCityId = workCityId == PponCityGroup.DefaultPponCityGroup.NewTaipeiCity.CityId
                               ? PponCityGroup.DefaultPponCityGroup.TaipeiCity.CityId
                               : workCityId;
                return new LunchKingSite.Core.JsonSerializer().Serialize(new { Success = true, Data = PponFacade.GetSideDealJson(bids, workCityId, takeCount) });
            }
            catch (Exception ex)
            {
                WebUtility.LogExceptionAnyway(ex);
                return new LunchKingSite.Core.JsonSerializer().Serialize(new { Success = false, Data = string.Empty });
            }
        }

        [WebMethod]
        public static string GetCityNameByGEOLocation(string latitude, string longitude)
        {
            double _latitude = (double.TryParse(latitude, out _latitude)) ? _latitude : default(double);
            double _longitude = (double.TryParse(longitude, out _longitude)) ? _longitude : default(double);
            City city;
            PponCity pponcity = PponCityGroup.DefaultPponCityGroup.TaipeiCity;

            if (_latitude <= 0 || _longitude <= 0)
            {
                return pponcity.CityName;
            }
            try
            {
                pponcity = LocationFacade.GetAreaCityWithGoogleMap(_latitude, _longitude, out city);
            }
            catch
            {
                return PponCityGroup.DefaultPponCityGroup.TaipeiCity.CityName;
            }
            //return "";
            return pponcity.CityName;
        }

        /// <summary>
        /// 使用者點擊或關閉蓋版活動，回傳導向的網址
        /// </summary>
        /// <param name="eventActivityId"></param>
        /// <returns>回傳要導向的網址</returns>
        [WebMethod]
        public static string OpenActivityEvent(int eventActivityId, bool openEvent, decimal staySeconds, bool mobileView)
        {
            var ea = EventFacade.GetEventActivity(eventActivityId);
            if (ea == null)
            {
                return string.Empty;
            }
            HttpCookie cookie = CookieManager.NewCookie("EventEmailCookie");
            cookie.Expires = DateTime.Now.AddDays(ea.Cookieexpire1.GetValueOrDefault(1));
            cookie.Value = ea.Id.ToString();
            HttpContext.Current.Response.SetCookie(cookie);
            
            int userId = 0;
            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                userId = MemberFacade.GetUniqueId(HttpContext.Current.User.Identity.Name);
            }
            int triggerType = mobileView ? 1 : 0;
            OrderFromType deviceType = Helper.GetOrderFromType();
            EventFacade.AddEventActivityHitLog(userId, ea.Id, DateTime.Now, openEvent, staySeconds, triggerType, deviceType);
            if (openEvent == false)
            {
                return string.Empty;
            }
            if (mobileView)
            {
                return ea.MobileLinkUrl ?? string.Empty;
            }
            else
            {
                return ea.WebLinkUrl ?? string.Empty;
            }
        }

        #endregion WebMethod

        #endregion other methods


        public static void IncrementDefaultPageCount()
        {
            string cacheKey = "DefaultPageCount";
            cache.Increment(cacheKey);
        }

        public static void ResetCache()
        {
            cache.Remove("DefaultPageCount");
            cache.Remove("DefaultPponDealList");
            cache.Remove("DefaultPageSort-0");
            cache.Remove("DefaultPageSort-1");
            cache.Remove("DefaultPageSort-2");
            cache.Remove("DefaultPageSort-3");
            cache.Remove("DefaultPageSort-4");
            cache.Remove("DefaultPageSort-5");
            cache.Remove("DefaultPageSort-6");
            cache.Remove("DefaultPageSort-7");
        }
    }

    public class GeoLoc
    {
        public string Latitude { get; set; }
        public string Lognitude { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string Host { get; set; }
        public string Ip { get; set; }
        public string Code { get; set; }
    }
}
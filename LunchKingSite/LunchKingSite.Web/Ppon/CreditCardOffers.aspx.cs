﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Models;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.I18N;
using LunchKingSite.Mongo.Facade;
using LunchKingSite.Web.Ppon;
using LunchKingSite.WebLib;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.UI;
using LunchKingSite.WebLib.Views;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using LunchKingSite.WebLib.Component;

namespace LunchKingSite.Web.Ppon
{
    public partial class CreditCardOffers : System.Web.UI.Page
    {
        protected ICmsProvider cp = ProviderFactory.Instance().GetProvider<ICmsProvider>();
        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        public bool IsNewMobileSetting
        {
            get { return config.IsNewMobileSetting; }
        }
        private string _dealContent;
        public string DealContent
        {
            set
            {
                _dealContent = value;
            }
            get
            {
               
                return _dealContent;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            string checkContentName = "/ppon/promo.aspx_pMain_creditcardoffers";
            CmsContent article = cp.CmsContentGet(checkContentName);
            DealContent = article.Body;
            lt_DealContent.Text = article.Body;
        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.Core.Component;
using LunchKingSite.Core;

namespace LunchKingSite.Web.Ppon
{
    public partial class WhiteListGuide : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request["h"] != null)
                pan_HelpMail.Visible = true;
            else
                pan_WhiteList.Visible = true;
        }
    }
}
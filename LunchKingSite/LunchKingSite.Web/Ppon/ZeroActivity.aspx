﻿<%@ Page Language="C#" MasterPageFile="~/Ppon/Ppon.Master" AutoEventWireup="true"
    CodeBehind="ZeroActivity.aspx.cs" Inherits="LunchKingSite.Web.Ppon.ZeroActivity"
    Title="17Life交易" %>

<%@ Import Namespace="LunchKingSite.Core" %>
<%@ Import Namespace="LunchKingSite.I18N" %>
<%@ Import Namespace="LunchKingSite.DataOrm" %>
<%@ Import Namespace="LunchKingSite.BizLogic.Component" %>
<%@ Import Namespace="LunchKingSite.BizLogic.Facade" %>

<%@ Register Src="~/UserControls/Paragraph.ascx" TagName="Paragraph"
    TagPrefix="uc1" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MC" runat="server">
    <script type="text/javascript" src="../Tools/js/jquery.blockUI.js"></script>
    <meta name="viewport" content="width=device-width initial-scale=1.0" />
    <link href="../Themes/default/images/17Life/G3/PPD.css" rel="stylesheet" type="text/css" />
    <link href="../Themes/default/images/17Life/G3/D2-5-Checkout.css" rel="stylesheet" type="text/css" />
    <link href="../themes/PCweb/css/RDL-L.css" rel="stylesheet" type="text/css" />
    <link href="../themes/PCweb/css/RDL-M.css" rel="stylesheet" type="text/css" />
    <link href="../themes/PCweb/css/RDL-S.css" rel="stylesheet" type="text/css" />
    <link href="../Themes/PCweb/css/popup_skm.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../Themes/default/images/17Life/edm_2015/javascript/jquery.bpopup.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#ddlStore").live('change', function () {
                $("#branchErr").hide();
            });
        });
        function block_postback() {
            if ($("#hidSingleStoreGuid").val() == "" && $("#ddlStore").val() == "") {
                $("#branchErr").show();
                return false;
            }
            $.blockUI({ message: "<h1><img src='../Themes/PCweb/images/spinner.gif' />  交易進行中...</h1>", css: { width: '150px' } });
        }
        function PrintFamiBarcode() {
            var cid = $('#hidCouponId').val();
            window.open('../User/FamiCouponDetail.aspx?print=true&cid=' + cid, '_blank');
        }
        function DownloadFamiBarcode() {
            var cid = $('#hidCouponId').val();
            window.open('../User/CouponGet.aspx?cid=' + cid, '_blank');
        }
        function slidingMenuToggle() {
            $("#wrap").toggleClass("mbe-menu-show");
        }
        $(function () {
            $(".mbe-menu-btn").click(slidingMenuToggle);
            $(".mbe-channel").click(slidingMenuToggle);
        });


        $(function () {
            $('.popup_select_list').bind('click', function (e) {
                e.preventDefault();
                $('.select-list-block').bPopup();
            });
            $('.popup_select_list2').bind('click', function (e) {
                e.preventDefault();

                    $('.select-list-block2').bPopup();
            });
        });


    </script>
    <div id="TOPBanner">
    </div>
    <asp:Panel ID="PanelBuy" runat="server">
        <%--索取好康--%>
        <div id="Left" class="rd-payment-Left-width">
            <div id="maincontent" class="clearfix">
                <div class="content-group payment">
                    <img src="../Themes/PCweb/images/spinner.gif" style="display:none" />
                    <h1 class="rd-payment-h1">
                        索取好康</h1>
                    <hr class="header_hr rd-payment-width" />
                    <h2 class="rd-payment-h2">
                        <asp:Label ID="lblZeroSeller" runat="server" Text=""></asp:Label>
                        <asp:Label ID="lblZeroTitle" runat="server" Text=""></asp:Label></h2>
                    <div class="grui-form">
                        <asp:PlaceHolder ID="pStore" Visible="false" runat="server">
                            <div class="form-unit">
                                <asp:Label ID="lblStoreChioce" runat="server" CssClass="unit-label" Text="分店選擇"></asp:Label>
                                <div class="data-input">
                                    <asp:UpdatePanel ID="upxy" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="true" RenderMode="Inline">
                                        <ContentTemplate>
                                            <asp:HiddenField ID="hidSingleStoreGuid" ClientIDMode="Static" runat="server" />
                                            <asp:Panel ID="multiStores" runat="server">
                                                <asp:DropDownList ID="ddlStoreCity" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlStoreCity_OnSelectedIndexChanged">
                                                </asp:DropDownList>
                                                <asp:DropDownList ID="ddlStoreArea" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlStoreArea_SelectedIndexChanged">
                                                </asp:DropDownList>
                                            </asp:Panel>
                                            <asp:DropDownList ID="ddlStore" Visible="false" ClientIDMode="Static" AutoPostBack="true" OnSelectedIndexChanged="ddlStore_OnSelectedIndexChanged" CssClass="ddlAddOptgroup" runat="server">
                                            </asp:DropDownList>
                                            <div>
                                                <asp:Label ID="lblStoreName" runat="server"></asp:Label>
                                                <br/>
                                                <asp:Label ID="lblStoreAddr" runat="server"></asp:Label>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                    <span id="branchErr" class="error-text" style="display: none;">請選擇分店</span>
                                </div>
                            </div>
                        </asp:PlaceHolder>
                        <div class="form-unit">
                            <div class="form-unit end-unit rd-pay-pos-fix">
                                <div class="btn-center">
                                    <asp:Button ID="btnZeroCheckOut" OnClientClick="return block_postback();" CssClass="btn btn-large btn-primary rd-payment-large-btn"
                                        runat="server" Text="我要索取" OnClick="ibCheckOut_Click" Visible="false" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <h2 class="rd-fd-title">
                        好康權益說明</h2>
                    <div class="grui-form">
                        <p>
                            <asp:Label ID="lblDesc" runat="server" Text=""></asp:Label>
                        </p>
                    </div>
                    <asp:HiddenField runat="server" ID="hidBuyerMobile" />
                </div>
            </div>
        </div>
        <%--訂購Q&A--%>
        <div id="Rightarea" class="rd-payment-qma">
            <div class="side_block">
                <div class="side_block_content">
                    <div class="side_block_title for-payment">
                        訂購Q&amp;A</div>
                    <p class="qa_title">
                        <a href="/ppon/newbieguide.aspx?s=2&q=20621" target="_blank">Q：我可以使用哪些方式付款？
                        </a>
                    </p>
                    <p class="qa_content">
                        A：您可透過以下方式付費：<br />
                        1. 17Life購物金<br />
                        2. 17Life紅利金<br />
                        3. 17Life折價券<br />
                        4. 信用卡/Visa金融卡<br />
                        5. ATM付款
                    </p>
                    <p class="qa_title">
                        <a href="javascript:void(0)">Q：我買的好康能給別人用嗎?
                        </a>
                    </p>
                    <p class="qa_content">
                        A：可以。因為我們的配合商家僅認憑證不認人，但一張憑證只能使用一次。
                    </p>
                    <p class="qa_title">
                        <a href="/ppon/newbieguide.aspx?s=3&q=116" target="_blank">Q：已購買的好康如何使用？
                        </a>
                    </p>
                    <p class="qa_content">
                        A：可至會員專區 → <a href="https://www.17life.com/User/coupon_List.aspx" target="_blank">訂單/憑證</a>
                        中，下載或列印憑證後，持憑證至所提供之商家使用。<br />
                        您也可以<a href="https://www.17life.com/ppon/promo.aspx?cn=app" target="_blank">下載APP</a>，使用電子憑證。
                    </p>
                    <p class="qa_content line">
                        -------------------------- <a href="https://www.17life.com/Ppon/NewbieGuide.aspx"
                            target="_blank">更多常見Q&A >>></a>
                    </p>
                    <p class="qa_note">
                        特別說明：<br />
                        目前本站僅會先要求授權，不會直接進行請款， 當此優惠跨越集購門檻時，本站才會實際進行請款。若集購失敗，將不會有任何款項的請款，請放心!
                    </p>
                </div>
            </div>
            <uc1:Paragraph ID="p165" runat="server" />
        </div>
    </asp:Panel>

    <%--索取結果--%>
    <asp:Panel ID="PanelResult" runat="server" Visible="false">
        <div id="Left" class="rd-payment-Left-width">
            <div id="maincontent" class="clearfix">
                <%--一般索取成功--%>
                <asp:Panel ID="pan_Success" runat="server" Visible="false">
                    <div class="content-group payment">
                        <div class="pay_success">
                        </div>
                        <h1 class="success">
                            恭喜您，索取成功！</h1>
                        <hr class="header_hr rd-payment-width" />
                        <asp:Panel runat="server" ID="pnlShowCoupon" Visible="false">
                            <p class="info">
                                <asp:Literal ID="litSuccessMsg" runat="server"></asp:Literal>
                            </p>
                            <asp:Label ID="lblCoupon" runat="server" Text=""></asp:Label>
                            <p class="info">
                                <asp:Literal ID="liPEZeventCouponDownload" runat="server"></asp:Literal>
                                您隨時可至「<a href="../User/coupon_List.aspx">會員專區</a>」內的「<a href="../User/coupon_List.aspx">訂單／憑證</a>」查詢訂單明細、付款金額、發票等詳細資訊，<br />
                                或者您也可以下載 <a href="https://www.17life.com/ppon/promo.aspx?cn=app">17Life app</a> ，方便您檢視活動兌換序號。
                            </p>
                        </asp:Panel>
                        <%--行銷滿額贈活動--%>
                        <asp:Panel ID="divDiscountPromotion" runat="server" CssClass="fd_success_sn pay-evt-bg" Visible="false">
                            <p class="pay-evt">
                                已獲得<span class="s-imp"><asp:Literal ID="liDiscountPromotionCount" runat="server"></asp:Literal></span>次
                                抽刮刮樂機會，總獎金超過億元<br>
                                還差<span class="s-imp"><asp:Literal ID="liDiscountPromotion" runat="server"></asp:Literal></span>元即可再多一次
                                <a href="<%= ResolveUrl("~" + conf.SpecialConsumingDiscountUrl) %>" target="_blank">
                                    活動詳情</a>
                            </p>
                        </asp:Panel>
                        <p class="btn-center">
                            <asp:HyperLink ID="hyp_ZeroPromo" runat="server" Target="_blank">
                                <asp:Image ID="img_ZeroPromo" runat="server" /></asp:HyperLink>
                        </p>
                    </div>
                </asp:Panel>
                <%--全家索取成功--%>
                <asp:Panel ID="pan_Fami_Success" runat="server" Visible="false">
                    <div class="content-group payment">
                        <div class="pay_success"></div>
                        <h1 class="success">恭喜您，索取成功！</h1>
                        <hr class="header_hr" />
                        <p class="info">
                            感謝您成功購得【
                               <asp:Label ID="lblCouponUsage" runat="server" Text=""></asp:Label> 
                            】優質好康。
                        </p>
                        <div class="fd_success_sn">
                            <asp:Literal ID="litPinCode" runat="server" Visible="false"></asp:Literal>
                            <asp:Image ID="imgBarcode" runat="server" ImageAlign="AbsBottom" style="padding-top:10px" />
                            <asp:PlaceHolder ID="ph_fami3barcode" runat="server" Visible="false">
                                <asp:Literal ID="litBarcodeImg" runat="server"></asp:Literal>
                            </asp:PlaceHolder>
                            <asp:PlaceHolder ID="ph_ButtonArea" runat="server">
                                <p class="info btn-center">
                                    <asp:Button ID="btnDownloadFamiBarcode" OnClientClick="DownloadFamiBarcode();return false;" CssClass="btn btn-large btn-primary rd-payment-xlarge-btn" runat="server" Text="下載優惠條碼" />
                                    <asp:Button ID="btnPrintFamiBarcode" OnClientClick="PrintFamiBarcode();return false;" CssClass="btn btn-large btn-primary rd-payment-xlarge-btn" runat="server" Text="列印優惠條碼" />
                                </p>
                            </asp:PlaceHolder>
                            <asp:HiddenField ID="hidCouponId" ClientIDMode="Static" runat="server" />
                        </div>
                        <asp:PlaceHolder ID="ph_fami3barcodeInfo" runat="server" Visible="false">
                            <p class="info">
                                ※ 若條碼無法正常讀取，請使用FamiPort輸入紅利PIN碼「<span id="spanPincode" runat="server" class="hint"></span>」列印後，即可持單據至櫃檯完成結帳動作。<a href="https://www.17life.com/ppon/promo.aspx?cn=family_purchaseflow" target="_blank">如何在FamiPort上輸入紅利PIN碼列印單據</a>
                                <br><br>
                                ※ 關閉此頁面後，您隨時可至「會員專區」內的「<a href="../User/coupon_List.aspx">訂單／憑證</a>」查詢活動兌換序號等訂單詳細資訊，或者下載 <a href="https://www.17life.com/ppon/promo.aspx?cn=app" target="_blank">17Life app</a>，方便您隨時檢視與兌換。
                                <br><br>
                                ※ 列印小白單步驟說明：使用 Famifport → 點選「紅利」 → 點選「17Life」 → 同意條款 → 輸入「紅利pin碼」 → 確認商品資訊 → 列印小白單 → 至櫃檯結帳。
                                <br><br>
                                ※ 刷取手機條碼結帳，店舖可不回收相關單據。
                            </p>                            
                        </asp:PlaceHolder>
                        <asp:PlaceHolder ID="ph_famibarcodeInfo" runat="server">
                            <p class="info">
                                您隨時可至「會員專區」內的「<a href="../User/coupon_List.aspx">訂單／憑證</a>」查詢訂單明細，<br />
                                或者您也可以下載<a href="https://www.17life.com/ppon/promo.aspx?cn=app">17Life app</a>
                                ，<%=(TheDeal.CouponCodeType == (int)CouponCodeType.Pincode) ? "方便您檢視活動兌換序號" : "方便您隨時使用條碼" %>。
                            </p>
                        </asp:PlaceHolder>
                        <%--行銷滿額贈活動--%>
                        <asp:Panel ID="divDiscountPromotion_F" runat="server" CssClass="fd_success_sn pay-evt-bg" Visible="false">
                            <p class="pay-evt">
                                已獲得<span class="s-imp"><asp:Literal ID="liDiscountPromotionCount_F" runat="server"></asp:Literal></span>次<br>
                                抽刮刮樂機會，總獎金超過億元<br>
                                還差<span class="s-imp"><asp:Literal ID="liDiscountPromotion_F" runat="server"></asp:Literal></span>元即可再多一次
                                <a href="<%= ResolveUrl("~" + conf.SpecialConsumingDiscountUrl) %>" target="_blank">
                                    活動詳情</a>
                            </p>
                        </asp:Panel>
                        <asp:PlaceHolder ID="ph_instruction" runat="server">
                            <p class="btn-center">
                                <img src="../Themes/PCweb/images/fami-barcode-tut-600.jpg" width="600" height="1320" alt="" />
                            </p>
                        </asp:PlaceHolder>
                        <asp:PlaceHolder ID="ph_PincodePromo" Visible="false" runat="server">
                            <asp:HyperLink ID="hl_FamiPincodePromo" runat="server" Target="_blank">
                                <asp:Image ID="img_FamiPincodePromo" runat="server" />
                            </asp:HyperLink>
                        </asp:PlaceHolder>
                    </div>
                </asp:Panel>
                <%--索取失敗--%>
                <asp:Panel ID="pan_Fail" runat="server" Visible="false">
                    <div class="content-group payment">
                        <div class="pay_failed">
                        </div>
                        <h1 class="faild">
                            很抱歉，這個序號無法被取得！</h1>
                        <hr class="header_hr rd-payment-width" />
                        <p class="info">
                            由於活動索取人數眾多，造成系統忙碌，暫時無法提供服務。<br />還請您回原活動頁再索取一次。
                        </p>
                        <p class="info">
                            若有其他問題，也歡迎來信<a href="<%=conf.SiteUrl + conf.SiteServiceUrl%>" target="_blank">17Life客服中心</a>。
                        </p>
                        <p class="info btn-center">
                            <asp:HyperLink ID="hyp_credit" runat="server">
                                <input id="Button1" type="button" class="btn btn-large btn-primary rd-payment-xlarge-btn" value="重新索取" />
                            </asp:HyperLink>
                        </p>
                    </div>
                </asp:Panel>
            </div>
        </div>
        <%--今日好康推薦--%>
        <div id="Rightarea" class="rd-payment-qma">
            <div class="side_block">
                <div class="side_block_content">
                    <div class="side_block_title for-payment">
                        今日好康推薦<a href="<%= ResolveUrl("~/ppon/default.aspx") %>" target="_blank">看更多</a></div>
                    <div class="sidedealbox">
                        <asp:HyperLink ID="rt1" runat="server"></asp:HyperLink>
                    </div>
                    <div class="siededeal_priceIMG">
                        <div id="sidedealimg">
                            <div class="Over1K_SS_Badge">
                            </div>
                            <asp:HyperLink ID="rl1" runat="server">
                                <asp:Image ID="rp1" runat="server" Width="220" Height="128" border="0" /></asp:HyperLink></div>
                        <div class="sidedealpricearea">
                            <div class="sidedealDiscount">
                                <asp:Label ID="discount1" runat="server"></asp:Label>折</div>
                            <div class="sidedealprice">
                                $<asp:Label ID="dp1" runat="server"></asp:Label></div>
                            <asp:HyperLink ID="rb1" runat="server"><div class="sidedealbuybtn">
                                </div></asp:HyperLink>
                            <div class="clearfix">
                                <asp:Label ID="op1" runat="server" Visible="false"></asp:Label>
                            </div>
                        </div>
                    </div>
                    <div id="sidedeall_hr">
                    </div>
                    <div class="sidedealbox">
                        <asp:HyperLink ID="rt2" runat="server"></asp:HyperLink>
                    </div>
                    <div class="siededeal_priceIMG">
                        <div id="sidedealimg">
                            <div class="HKL_SS_SoldOut_Badge">
                            </div>
                            <asp:HyperLink ID="rl2" runat="server">
                                <asp:Image ID="rp2" runat="server" Width="220" Height="128" border="0" /></asp:HyperLink></div>
                        <div class="sidedealpricearea">
                            <div class="sidedealDiscount">
                                <asp:Label ID="discount2" runat="server"></asp:Label>折</div>
                            <div class="sidedealprice">
                                $<asp:Label ID="dp2" runat="server"></asp:Label></div>
                            <asp:HyperLink ID="rb2" runat="server"><div class="sidedealbuybtn">
                                </div></asp:HyperLink>
                            <div class="clearfix">
                                <asp:Label ID="op2" runat="server" Visible="false"></asp:Label>
                            </div>
                        </div>
                    </div>
                    <div style="display: none;">
                        <div id="sidedeall_hr">
                        </div>
                        <div class="sidedealbox">
                            <asp:HyperLink ID="rt3" runat="server"></asp:HyperLink>
                        </div>
                        <div class="siededeal_priceIMG">
                            <div id="sidedealimg">
                                <div class="HKL_SS_SoldOut_Badge">
                                </div>
                                <asp:HyperLink ID="rl3" runat="server">
                                    <asp:Image ID="rp3" runat="server" Width="220" Height="128" border="0" /></asp:HyperLink></div>
                            <div class="sidedealpricearea">
                                <div class="sidedealDiscount">
                                    <asp:Label ID="discount3" runat="server"></asp:Label>折</div>
                                <div class="sidedealprice">
                                    $<asp:Label ID="dp3" runat="server"></asp:Label></div>
                                <asp:HyperLink ID="rb3" runat="server"><div class="sidedealbuybtn">
                                </div></asp:HyperLink>
                                <div class="clearfix">
                                    <asp:Label ID="op3" runat="server" Visible="false"></asp:Label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <%--sidedeal--%>
        </div>
    </asp:Panel>
    
    <%--新光索取結果--%>
    <asp:Panel ID="SkmPanelResult" runat="server" Visible="false">
        
        <div id="FULL" class="rd-payment-Left-width">
          <div id="maincontent" class="clearfix">
            <div class="content-group payment">
              <div class="pay_success"></div>
              <h1 class="success">恭喜您，索取成功！</h1>
              <hr class="header_hr" />
              <!--新光三越專區索取成功-->
              <div class="member member_buy_card" style="display:block;">
                <p class="info">
                  您已成功索取【<asp:Literal ID="litSkmSuccessMsg" runat="server"></asp:Literal>】好康。
                </p>
                 <p class="info">已寄送憑證號碼至您的會員信箱，您隨時可至 會員中心＞<a href="../User/coupon_List.aspx">訂單/憑證</a>[活動與抽獎]查詢兌換憑證號碼，</p>       
                <p class="info">歡迎點此下載 <a href="https://www.17life.com/ppon/promo.aspx?cn=app" target="_blank">17Life app</a>，方便您隨時檢視兌換。</p>
              </div>
              <!--新光三越專區索取成功 end--> </div>
          </div>
        </div>
        
        <%--好康推薦--%>
        <div id="section-1" class="clearfix">
          <h3 class="local-title">好康推薦</h3>
            <asp:Repeater ID="rptRecommendDeals" runat="server" >
                <ItemTemplate>
                  <div class="evn-cop-buy-sf-box e_shadow">
                        <div class="evn-cop-buy-pic">
                            <asp:HyperLink ID="rl1" runat="server" NavigateUrl="<%# ((BuyRelatedDeal)Container.DataItem).NavigateUrl %>">
                                <asp:Image ID="rp1" runat="server" ImageUrl="<%# ((BuyRelatedDeal)Container.DataItem).ImageUrl %>" />
                            </asp:HyperLink>
                            <div class="evn-cop-buy-sold-out-bar-290"></div>
                        </div>
                        <div class="evn-cop-all-right">
                            <div class="evn-cop-buytitle e_textFamily">
                                <asp:HyperLink ID="rtb1" runat="server" Text="<%#((BuyRelatedDeal)Container.DataItem).CouponUsage%>" NavigateUrl="<%# ((BuyRelatedDeal)Container.DataItem).NavigateUrl %>"></asp:HyperLink>
                            </div>
                            <div class="evn-cop-minorange-title">
                                <asp:HyperLink ID="rt1" runat="server" Text="<%#((BuyRelatedDeal)Container.DataItem).EventTitle%>" NavigateUrl="<%# ((BuyRelatedDeal)Container.DataItem).NavigateUrl %>"></asp:HyperLink>
                            </div>
                            <div class="evn-cop-buyinformation">
                                <div class="evn-cop-discount">
                                    <asp:Label ID="discount1" runat="server" Text="<%#((BuyRelatedDeal)Container.DataItem).discountText%>"></asp:Label><span class="smalltext">折</span>
                                </div>
                                <div class="evn-cop-buytotle">$<asp:Label ID="dp1" runat="server" Text="<%# ((BuyRelatedDeal)Container.DataItem).PriceText %>"></asp:Label></div>
                                <asp:HyperLink ID="rb1" runat="server" NavigateUrl="<%# ((BuyRelatedDeal)Container.DataItem).NavigateUrl %>"><div class="st-e-cop-buybtn e_rounded">馬上買</div></asp:HyperLink>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div> 
                </ItemTemplate>
            </asp:Repeater>
        </div>
    </asp:Panel>

    <uc1:Paragraph ID="sp" runat="server" Visible="false" />
    <%-- Google Code for &#36092;&#36023; Conversion Page --%>
<%--    <script type="text/javascript">
        /* <![CDATA[ */
        var google_conversion_id = 1015907304;
        var google_conversion_language = "en";
        var google_conversion_format = "2";
        var google_conversion_color = "ffffff";
        var google_conversion_label = "nm4CCID9pgIQ6Ie25AM";
        var google_conversion_value = 0;
    /* ]]> */
    </script>
    <script type="text/javascript" src="https://www.googleadservices.com/pagead/conversion.js">
    </script>
    <noscript>
        <div style="display: inline;">
            <img height="1" width="1" style="border-style: none;" alt="" src="https://www.googleadservices.com/pagead/conversion/1015907304/?value=0&label=nm4CCID9pgIQ6Ie25AM&guid=ON&script=0" />
        </div>
    </noscript>--%>
</asp:Content>

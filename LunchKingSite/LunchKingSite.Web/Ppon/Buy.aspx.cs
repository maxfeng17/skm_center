﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Helpers;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using log4net;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Facade.Models;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.BizLogic.Model.EInvoice;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.I18N;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;
using PaymentType = LunchKingSite.Core.PaymentType;
using LunchKingSite.BizLogic.Models;
using LunchKingSite.BizLogic.Models.Ppon;
using LunchKingSite.BizLogic.Models.QueueModels;
using JsonSerializer = LunchKingSite.Core.JsonSerializer;
using System.Web.Services;
using LunchKingSite.BizLogic.Model.API;

namespace LunchKingSite.Web.Ppon
{
    public partial class Buy : PponPayBase, IPponBuyView
    {

        #region Event

        public event EventHandler<DataEventArgs<PponPaymentDTO>> CheckOut;

        public event EventHandler<GuestMemberEventArgs> GetOrAddGuestMemberThenSetInfo;

        public event EventHandler ResetContent;

        #endregion Event

        #region Field

        public decimal deliveryCharge;
        public string dealName = string.Empty;

        #endregion Field

        #region const
        /// <summary>
        /// 社團法人台灣一起夢想公益協會
        /// </summary>
        public const string gomajiDonationCode = "510";
        /// <summary>
        /// 南投縣青少年空手道推展協會 愛心碼
        /// </summary>
        public const string pezDonationCode = "23981";
        /// <summary>
        /// 創世社會福利基金會 愛心碼
        /// </summary>
        public const string genesisDonationCode = "919";
        /// <summary>
        /// 財團法人陽光社會福利基金會 愛心碼
        /// </summary>
        public const string sunDonationCode = "13579";

        /// <summary>
        /// 結帳完成推薦檔次CPA
        /// </summary>
        public const string CPA_PROMO_DEALS = "cpa-17_payment";

        protected int _GUESTBUY_LIMIT_PRICE;
        #endregion

        private static readonly ILog logger = LogManager.GetLogger(typeof(PponBuyPresenter));
        private static IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        private static IItemProvider ip = ProviderFactory.Instance().GetProvider<IItemProvider>();
        protected static ISysConfProvider config = ProviderFactory.Instance().GetConfig();

        #region properties

        private PponBuyPresenter _presenter;

        public PponBuyPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        private Guid _businessHourGuid;
        public Guid BusinessHourGuid
        {
            get
            {
                if (_businessHourGuid == Guid.Empty)
                {
                    if (Request["bid"] != null)
                    {
                        try
                        {
                            _businessHourGuid = new Guid(Request["bid"]);
                            Session[LkSiteSession.Carrier + "_Guid"] = _businessHourGuid;
                        }
                        catch
                        {
                            _businessHourGuid = Guid.Empty;
                        }
                    }
                    else
                    {
                        if (Session[LkSiteSession.Carrier + "_Guid"] != null)
                        {
                            _businessHourGuid = (Guid)Session[LkSiteSession.Carrier + "_Guid"];
                        }
                    }
                }
                return _businessHourGuid;
            }
            set { _businessHourGuid = value; }
        }

        public string SessionId
        {
            get
            {
                return Session.SessionID;
            }
        }

        public string UserName
        {
            get
            {
                return User.Identity.Name;
            }
        }

        public bool IsAuthenticated
        {
            get
            {
                return User.Identity.IsAuthenticated;
            }
        }

        public string GtagDataJson { get; set; }

        public string GtagConversionJson { get; set; }

        public string UrADGtagConversionJson { get; set; }

        public string YahooDataJson { get; set; }

        public string ScupioJson { get; set; }

        public string ScupioCheckoutPageJson { get; set; }

        public string BackupTicketIdForHistoryBack
        {
            get
            {
                string sessionTicketKey = string.Format("{0}_backup_{1}_{2}",
                    this.ClientID, this.BusinessHourGuid, this.UserId);
                if (Session[sessionTicketKey] == null)
                {
                    return string.Empty;
                }
                return Session[sessionTicketKey].ToString();
            }

            set
            {
                string sessionTicketKey = string.Format("{0}_backup_{1}_{2}",
                    this.ClientID, this.BusinessHourGuid, this.UserId);
                Session[sessionTicketKey] = value;
            }
        }

        public KeyValuePair<CarrierType, string> Carrier
        {
            get
            {
                if (Session[LkSiteSession.Carrier.ToString()] == null)
                {
                    return new KeyValuePair<CarrierType, string>(CarrierType.Member, UserId.ToString());
                }
                return (KeyValuePair<CarrierType, string>)Session[LkSiteSession.Carrier.ToString()];
            }
            set
            {
                Session[LkSiteSession.Carrier.ToString()] = value;
            }
        }

        public bool IsSaveCreditCardInfo
        {
            get { return chkSaveCreditCardInfo.Checked; }
        }

        public string CreditCardName
        {
            get { return txtCreditCardName.Text; }
        }

        public PponPickupStore GetPponPickupStoreFromRequest()
        {
            if (Request["cvsspot"] != null)
            {
                return new PponPickupStore
                {
                    ProductDeliveryType = ProductDeliveryType.FamilyPickup,
                    StoreId = Request["cvsspot"],
                    StoreAddr = Request["addr"],
                    StoreName = Request["name"],
                    StoreTel = Request["tel"]
                };
            }

            if (Request["tempvar"] != null)
            {
                return new PponPickupStore
                {
                    ProductDeliveryType = ProductDeliveryType.SevenPickup,
                    StoreId = Request["storeid"],
                    StoreAddr = Request["address"],
                    StoreName = Request["storename"],
                    StoreTel = string.Empty
                };
            }
            return null;
        }

        public CouponFreightCollection TheCouponFreight
        {
            get;
            set;
        }

        public bool IsShoppingCart
        {
            get
            {
                bool tempResult;
                return bool.TryParse(hidIsShoppingCart.Value, out tempResult) && tempResult;
            }
            set { hidIsShoppingCart.Value = value.ToString(); }
        }

        public EntrustSellType EntrustSell { get; set; }


        #region 刷卡頁用

        public OrderInstallment Installment
        {
            get
            {
                if (hidInstallment.Value != string.Empty)
                {
                    return (OrderInstallment)(int.Parse(hidInstallment.Value));
                }

                return OrderInstallment.No;
            }
            set
            {
                hidInstallment.Value = ((int)value).ToString();
            }
        }

        public string CreditcardsAllowInstallment
        {
            get
            {
                return hidCreditcardsAllowInstallment.Value;
            }
            set
            {
                hidCreditcardsAllowInstallment.Value = value;
            }
        }

        public bool TaishinCreditCardOnly
        {
            get
            {
                if (ChannelFacade.GetOrderClassificationByHeaderToken() == AgentChannel.TaiShinMall)
                {
                    return true;
                }
                if (config.EnableTaishinCreditCardOnly)
                {
                    var rsrcCookie = Request.Cookies[LkSiteCookie.ReferrerSourceId.ToString()];
                    return (rsrcCookie != null) && (rsrcCookie.Value == config.TSBankAppRsrc);
                }
                return false;
            }
        }

        public string TaishinCreditcards
        {
            get
            {
                return hidTaishinCreditcards.Value;
            }
            set
            {
                hidTaishinCreditcards.Value = value;
            }
        }

        #endregion

        public string DeliveryDate
        {
            get
            {
                if (ViewState["DeliveryDate"] != null)
                {
                    return ViewState["DeliveryDate"].ToString();
                }

                return string.Empty;
            }
            set
            {
                ViewState["DeliveryDate"] = value;
            }
        }

        public string DeliveryTime
        {
            get
            {
                if (ViewState["DeliveryTime"] != null)
                {
                    return ViewState["DeliveryTime"].ToString();
                }

                return string.Empty;
            }
            set
            {
                ViewState["DeliveryTime"] = value;
            }
        }

        public bool IsItem
        {
            get
            {
                if (ViewState["IsItem"] != null)
                {
                    return (bool)ViewState["IsItem"];
                }

                return false;
            }
            set
            {
                ViewState["IsItem"] = value;
            }
        }

        public ThirdPartyPayment ThirdPartyPaymentSystem { get; set; }

        public bool IsApplePay { get; set; }

        public bool NotDeliveryIslands
        {
            get
            {
                return bool.Parse(hiNotDeliveryIslands.Value);
            }
            set
            {
                hiNotDeliveryIslands.Value = value.ToString();
            }
        }

        public BuyAddressInfo AddressInfo
        {
            get
            {
                string givenName;
                string familyName = Helper.GetFamilyName(ProposalFacade.RemoveSpecialCharacter(BuyerName.Text.Trim()), out givenName);

                BuyAddressInfo addressInfo = new BuyAddressInfo
                {
                    FamilyName = familyName,
                    GivenName = givenName,
                    BuyerMobile = BuyerMobile.Text.Trim(),
                    UserEmail = BuyerEmail.Text,
                    BuyerCityId = 0,
                    BuyerAreaId = 0,
                    BuyerBuildingGuid = Guid.Empty,
                    BuyerAddress = string.Empty,
                    ReceiverName = ProposalFacade.RemoveSpecialCharacter(ReceiverName.Text.Trim()),
                    ReceiverMobile = ReceiverMobile.Text.Trim(),
                    ReceiverCityId = 0,
                    ReceiverAreaId = 0,
                    ReceiverBuildingGuid = Guid.Empty,
                    ReceiverAddress = string.Empty,
                    AddReceiverInfo = AddReceiverInfo.Checked
                };
                string buyerAddress = BuyerAddress.Text.Trim();
                if (buyerAddress != string.Empty)
                {
                    int cityId;
                    int areaId;
                    if (int.TryParse(Request[ddlBuyerCity.UniqueID], out cityId))
                    {
                        addressInfo.BuyerCityId = cityId;
                    }
                    if (int.TryParse(Request[ddlBuyerArea.UniqueID], out areaId))
                    {
                        addressInfo.BuyerAreaId = areaId;
                    }
                    addressInfo.BuyerAddress = buyerAddress;
                }
                string receiverAddress = ProposalFacade.RemoveSpecialCharacter(ReceiverAddress.Text.Trim());
                if (receiverAddress != string.Empty)
                {
                    int cityId;
                    int areaId;
                    if (int.TryParse(Request[ddlReceiverCity.UniqueID], out cityId))
                    {
                        addressInfo.ReceiverCityId = cityId;
                    }
                    if (int.TryParse(Request[ddlReceiverArea.UniqueID], out areaId))
                    {
                        addressInfo.ReceiverAreaId = areaId;
                    }
                    addressInfo.ReceiverAddress = receiverAddress;
                }
                return addressInfo;
            }
        }

        public string UserInfo
        {
            get;
            set;
        }

        public int BuyerCity
        {
            get;
            set;
        }

        public int BuyerTownshipId
        {
            get;
            set;
        }

        private string buyerCityAddress;
        public string BuyerCityAddress
        {
            get { return buyerCityAddress; }
            set { buyerCityAddress = Helper.RemoveNewLineChar(value); }
        }

        public string BuySuccessText
        {
            get;
            set;
        }

        public CpaClientMain<CpaClientEvent<CpaClientDeail>> NewCpa
        {
            get
            {
                if (Session[LkSiteSession.NewCpa.ToString()] != null)
                {
                    return (CpaClientMain<CpaClientEvent<CpaClientDeail>>)Session[LkSiteSession.NewCpa.ToString()];
                }
                else
                {
                    return null;
                }
            }
            set
            {
                Session[LkSiteSession.NewCpa.ToString()] = value;
            }
        }

        public ProductDeliveryType ProductDelivery
        {
            get
            {
                if (rdoProductDeliveryFamily.Checked)
                {
                    return ProductDeliveryType.FamilyPickup;
                }
                if (rdoProductDeliverySeven.Checked)
                {
                    return ProductDeliveryType.SevenPickup;
                }
                return ProductDeliveryType.Normal;
            }
        }

        private string _memberLastProductDeliveryType = "";
        public string MemberLastProductDeliveryType
        {
            get
            {
                return _memberLastProductDeliveryType;
            }
            set
            {
                _memberLastProductDeliveryType = value;
            }
        }

        public PponDeliveryInfo DeliveryInfo
        {
            get
            {
                PponDeliveryInfo info = new PponDeliveryInfo();
                if (config.GuestBuyEnabled || AllowGuestBuy)
                {
                    info.BuyerUserId = this.GuestMemberId;
                }
                else
                {
                    info.BuyerUserId = this.UserId;
                }
                int forTrypasre;
                info.StoreGuid = SelectedStoreGuid;
                info.DiscountCode = DiscountCode;
                info.BonusPoints = int.TryParse(hidbcash.Value, out forTrypasre) == false ? 0 : forTrypasre;
                info.SCash = int.TryParse(hidscash.Value, out forTrypasre) == false ? 0 : forTrypasre;
                //後端會對 PendingAmount, TotalAmount 再做驗證
                info.PendingAmount = int.TryParse(hidtotal.Value, out forTrypasre) == false ? 0 : forTrypasre;
                info.TotalAmount = int.TryParse(hidsubtotal.Value, out forTrypasre) == false ? 0 : forTrypasre;
                info.DeliveryDateString = DeliveryDate;
                info.DeliveryTimeString = DeliveryTime;
                info.Notes = string.Empty;
                info.Quantity = int.Parse(hidqty.Value);
                if (TheDeal == null)
                {
                    info.DealAccBusinessGroupId = 0;
                    info.RootDealType = 0;
                }
                else
                {
                    info.DealAccBusinessGroupId = TheDeal.DealAccBusinessGroupId ?? 0;
                    info.RootDealType = TheDeal.DealType == null ? 0 : DealTypeFacade.GetRootNode((int)TheDeal.DealType);
                }

                

                //P好康
                if (!IsItem)
                {
                    info.SellerType = DepartmentTypes.Ppon;
                    info.DeliveryType = DeliveryType.ToShop; //好康到店到府消費方式判斷
                    info.IsForFriend = false;
                }
                else
                {
                    info.SellerType = DepartmentTypes.PponItem;
                    info.DeliveryType = DeliveryType.ToHouse; //好康到店到府消費方式判斷
                    info.WriteName = ProposalFacade.RemoveSpecialCharacter(ReceiverName.Text.Trim());

                    if (Request[receiverSelect.UniqueID] != null)
                    {
                        info.MemberDeliveryID = int.Parse(Request[receiverSelect.UniqueID]);
                    }
                    info.ProductDeliveryType = ProductDelivery;
                    if (info.ProductDeliveryType == ProductDeliveryType.Normal)
                    {
                        City city = CityManager.TownShipGetById(int.Parse(Request[ddlReceiverArea.UniqueID]));
                        if (string.IsNullOrWhiteSpace(hidReceiverAddress.Value) == false && hidReceiverAddress.Value.IndexOf("請選擇") == -1)
                        {
                            if (city != null && !string.IsNullOrWhiteSpace(city.ZipCode))
                            {
                                info.WriteAddress = city.ZipCode + " " + hidReceiverAddress.Value;
                            }
                            else
                            {
                                info.WriteAddress = hidReceiverAddress.Value;
                            }
                        }
                    }
                    else if (info.ProductDeliveryType == ProductDeliveryType.FamilyPickup)
                    {
                        info.ProductDeliveryType = ProductDeliveryType.FamilyPickup;
                        info.PickupStore = new PickupStore
                        {
                            StoreId = hidFamilyPickupStoreId.Value,
                            StoreName = hidFamilyPickupStoreName.Value,
                            StoreTel = hidFamilyPickupStoreTel.Value,
                            StoreAddr = hidFamilyPickupStoreAddr.Value
                        };
                    }
                    else if (info.ProductDeliveryType == ProductDeliveryType.SevenPickup)
                    {
                        info.ProductDeliveryType = ProductDeliveryType.SevenPickup;
                        info.PickupStore = new PickupStore
                        {
                            StoreId = hidSevenPickupStoreId.Value,
                            StoreName = hidSevenPickupStoreName.Value,
                            StoreTel = hidSevenPickupStoreTel.Value,
                            StoreAddr = hidSevenPickupStoreAddr.Value
                        };
                    }

                    info.Phone = ReceiverMobile.Text.Trim();
                    info.Freight = hiddCharge.Value.Trim() == "" ? 0 : decimal.Parse(hiddCharge.Value);
                }

                if (this.EntrustSell == EntrustSellType.No)
                {
                    #region 開立發票

                    info.Version = InvoiceVersion.CarrierEra;
                    if (rbInvoiceDuplicate.Checked)
                    {
                        #region 二聯式 電子(會員載具/手機/自然人憑證) or 紙本 發票

                        info.InvoiceType = "2";
                        CarrierType carrier = CarrierType.None;
                        CarrierType.TryParse(ddlCarrierType.SelectedValue, out carrier);
                        info.CarrierType = carrier;
                        switch (carrier)
                        {
                            case CarrierType.Member:
                                info.CarrierId = UserId.ToString(); // 會員載具預設為UserId
                                info.IsEinvoice = true;
                                break;
                            case CarrierType.Phone:
                                info.CarrierId = txtPhoneCarrier.Text.Trim();
                                info.IsEinvoice = true;
                                break;
                            case CarrierType.PersonalCertificate:
                                info.CarrierId = txtPersonalCertificateCarrier.Text.Trim();
                                info.IsEinvoice = true;
                                break;
                            case CarrierType.None:
                                info.InvoiceBuyerName = InvoiceBuyerName.Text;
                                info.InvoiceBuyerAddress = InvoiceAddress.Text;
                                break;
                            default:
                                break;
                        }

                        #endregion
                    }
                    else if (rbInvoiceDonation.Checked)
                    {
                        #region 捐贈

                        info.InvoiceType = "2";
                        info.LoveCode = txtLoveCode.Text.Replace(Phrase.Space, string.Empty);

                        #endregion
                    }
                    else if (rbInvoiceTriplicate.Checked)
                    {
                        #region 三聯式
                        info.IsInvoiceSave = InvoiceSave.Checked;
                        info.InvoiceSaveId = hidEinvoiceTripleId.Value;
                        info.InvoiceType = "3";
                        info.CompanyTitle = InvoiceTitle.Text;
                        info.UnifiedSerialNumber = InvoiceNumber.Text;
                        info.InvoiceBuyerName = InvoiceBuyerName.Text;
                        info.InvoiceBuyerAddress = (string.IsNullOrEmpty(hidInvoiceCity.Value) == true ? "" : hidInvoiceCity.Value + "|") + (string.IsNullOrEmpty(hidInvoiceArea.Value) == true ? "" : hidInvoiceArea.Value + "|") + InvoiceAddress.Text + "@" + hidConfirmInvoiceAddress.Value;

                        #endregion
                    }

                    #endregion
                }
                else
                {
                    #region 代收轉付

                    info.IsEinvoice = rbReceiptType1.Checked;
                    if (info.IsEinvoice == false)
                    {
                        info.InvoiceBuyerName = Request[txtReceiptTitle.UniqueID];

                        string addr = string.Format("{0}{1}",
                            CityManager.CityTownShopStringGet(int.Parse(Request[ddlReceiptArea.UniqueID])),
                            txtReceiptAddress.Text);
                        info.InvoiceBuyerAddress = addr;
                        info.UnifiedSerialNumber = Request[txtReceiptNumber.UniqueID];
                    }

                    #endregion
                }

                info.EntrustSell = this.EntrustSell;
                info.CreditCardInstallment = OrderInstallment.No;
                if (rbInstallment3Months.Checked)
                {
                    info.CreditCardInstallment = OrderInstallment.In3Months;
                }
                else if (rbInstallment6Months.Checked)
                {
                    info.CreditCardInstallment = OrderInstallment.In6Months;
                }
                else if (rbInstallment12Months.Checked)
                {
                    info.CreditCardInstallment = OrderInstallment.In12Months;
                }

                switch (BuyTransPaymentMethod)
                {
                    case BuyTransPaymentMethod.CreaditCardInstallment:
                    case BuyTransPaymentMethod.CreaditCardPayOff:
                    case BuyTransPaymentMethod.ApplePay:
                    case BuyTransPaymentMethod.MasterPass:
                        info.PayType = PaymentType.Creditcard;
                        break;
                    case BuyTransPaymentMethod.Atm:
                        info.PayType = PaymentType.ATM;
                        break;
                    case BuyTransPaymentMethod.TaishinPay:
                        info.PayType = PaymentType.TaishinPay;
                        break;
                    case BuyTransPaymentMethod.LinePay:
                        info.PayType = PaymentType.LinePay;
                        break;
                }
                if (BuyTransPaymentMethod == BuyTransPaymentMethod.ApplePay)
                {
                    info.IsApplePay = true;
                }
                info.PaymentMethod = BuyTransPaymentMethod;

                return info;
            }
        }

        public string EInvoiceIsNotMember { get; set; }

        public string UserMemo
        {
            get
            {
                return hidAllItems.Value;
            }
        }

        public string ItemOptionsJson
        {
            get { return hidItemOptionsJson.Value; }
            set { hidItemOptionsJson.Value = value; }
        }

        public string BuyerInfoJson
        {
            get { return hidBuyerInfoJson.Value; }
            set { hidBuyerInfoJson.Value = value; }
        }

        public string OtherInfoJson
        {
            get { return hidOtherInfoJson.Value; }
            set { hidOtherInfoJson.Value = value; }
        }

        public Guid SelectedStoreGuid
        {
            get
            {
                var selectedStr = ddlStore.SelectedValue;
                if (!IsItem && !string.IsNullOrWhiteSpace(selectedStr))
                {
                    return new Guid(selectedStr);
                }
                else if (!string.IsNullOrWhiteSpace(hidStoreGuid.Value))
                {
                    return new Guid(hidStoreGuid.Value);
                }
                return Guid.Empty;
            }
        }

        public double bcash { get; set; }

        public decimal scash { get; set; }

        public string RsrcSession
        {
            get
            {
                if (Session["rsrc"] != null)
                {
                    return Session["rsrc"].ToString();
                }
                else
                {
                    return null;
                }
            }
        }

        // add for credit card api
        public string _ticketId = string.Empty;
        public string TicketId
        {
            get
            {
                if (string.IsNullOrEmpty(_ticketId))
                {
                    if (string.IsNullOrEmpty(Request.QueryString["TicketId"]) == false)
                    {
                        _ticketId = Request.QueryString["TicketId"];
                        if (string.IsNullOrEmpty(Request["order_no"]) == false && _ticketId.Length >= 36)
                        {
                            //OTP回傳的網址需要修正
                            //會回傳 ticketId=ebdc33fb-a3ce-4d11-96dc-1f4ec53fe9bd?s_mid
                            //但我們只需要前36個字
                            _ticketId = _ticketId.Substring(0, 36);
                        }
                    }
                    //cvstemp 是超取選全家分店帶回的ticketid
                    if (string.IsNullOrEmpty(Request.QueryString["cvsid"]) == false)
                    {
                        _ticketId = Request.QueryString["cvsid"];
                    }
                    if (string.IsNullOrEmpty(hidTicketId.Value) == false)
                    {
                        _ticketId = hidTicketId.Value;
                    }
                }
                return _ticketId;
            }
            set { hidTicketId.Value = value; }
        }

        public IViewPponDeal TheDeal { get; set; }

        public bool AllowGuestBuy
        {
            get
            {
                bool result;
                return bool.TryParse(hidAllowGuestBuy.Value, out result) && result;
            }
            set
            {
                hidAllowGuestBuy.Value = value.ToString();
            }
        }
        public bool IsBankDeal { get; set; }

        public string CookieReferenceId
        {
            get
            {
                HttpCookie cookie = Request.Cookies.Get(LkSiteCookie.ReferenceId.ToString("g"));
                if (cookie != null)
                {
                    return cookie.Value;
                }

                return string.Empty;
            }
        }

        public int CookieCityId
        {
            get
            {
                HttpCookie cookie = Request.Cookies.Get(LkSiteCookie.CityId.ToString("g"));

                if (cookie != null)
                {
                    try
                    {
                        return int.Parse(cookie.Value);
                    }
                    catch (Exception)
                    {
                        return -1;
                    }
                }

                return -1;
            }
        }

        public PaymentResultPageType PaymentResult
        {
            get
            {
                return ViewState["pr"] != null ? (PaymentResultPageType)ViewState["pr"] : PaymentResultPageType.CreditcardSuccess;
            }
            set
            {
                ViewState["pr"] = value;
            }
        }

        private IOrderProvider op = ProviderFactory.Instance().GetProvider<IOrderProvider>();

        public string ReferrerSourceId
        {
            get
            {
                return Request.Cookies[LkSiteCookie.ReferrerSourceId.ToString()] != null ? Request.Cookies[LkSiteCookie.ReferrerSourceId.ToString()].Value : null;
            }
        }

        public string Rsrc
        {
            get
            {
                var queryString = HttpContext.Current.Request.QueryString["rsrc"] ?? string.Empty;
                return Page.RouteData.Values["rsrc"] != null ? Page.RouteData.Values["rsrc"].ToString() : queryString;
            }
        }

        public string CookieIChannelGid
        {
            get
            {
                return Request.Cookies[LkSiteCookie.iChannelGid.ToString()] != null ? Request.Cookies[LkSiteCookie.iChannelGid.ToString()].Value : null;
            }
        }
        public string CookieShopBackGid
        {
            get
            {
                return Request.Cookies[LkSiteCookie.shopBackGid.ToString()] != null ? Request.Cookies[LkSiteCookie.shopBackGid.ToString()].Value : null;
            }
        }

        public string CookiePezChannelGid
        {
            get
            {
                return Request.Cookies[LkSiteCookie.PezChannelGid.ToString()] != null ? Request.Cookies[LkSiteCookie.PezChannelGid.ToString()].Value : null;
            }
        }
        public string CookieLineShopGid
        {
            get
            {
                return Request.Cookies[LkSiteCookie.LineShopGid.ToString()] != null ? Request.Cookies[LkSiteCookie.LineShopGid.ToString()].Value : null;
            }
        }

        public string CookieAffiliatesGid
        {
            get
            {
                return Request.Cookies[LkSiteCookie.AffiliatesGid.ToString()] != null ? Request.Cookies[LkSiteCookie.AffiliatesGid.ToString()].Value : null;
            }
        }

        // add for credit card api
        public bool IsPreview
        {
            get
            {
                if (Page.User.IsInRole("production"))
                {
                    return (string.IsNullOrWhiteSpace(Request["p"]) || !string.Equals("y", Request["p"])) ? false : true;
                }
                else
                {
                    return false;
                }
            }
        }

        public string DiscountCode
        {
            get
            {
                return tbDiscount.Text.Trim().ToUpper();
            }
        }

        public string LoveCodes
        {
            get
            {
                dynamic codes = LoveCodeManager.Instance.LoveCodes.Select(x => new { value = x.Code, label = x.Code, desc = x.Name });
                return new JsonSerializer().Serialize(codes);
            }
        }

        public string OrderArgs { set; get; }


        public string MasterPass_OAuthToken
        {
            get
            {
                if (!string.IsNullOrEmpty(Request.QueryString["oauth_token"]))
                {
                    return Request.QueryString["oauth_token"];
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        public string MasterPass_OAuthVerifier
        {
            get
            {
                if (!string.IsNullOrEmpty(Request.QueryString["oauth_verifier"]))
                {
                    return Request.QueryString["oauth_verifier"];
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        public string MasterPass_CheckoutResourceUrl
        {
            get
            {
                if (!string.IsNullOrEmpty(Request.QueryString["checkout_resource_url"]))
                {
                    return Request.QueryString["checkout_resource_url"];
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        public string MasterPassPairingVerifier
        {
            get
            {
                if (!string.IsNullOrEmpty(Request.QueryString["pairing_verifier"]))
                {
                    return Request.QueryString["pairing_verifier"];
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        public string MasterPassPairingToken
        {
            get
            {
                if (!string.IsNullOrEmpty(Request.QueryString["pairing_token"]))
                {
                    return Request.QueryString["pairing_token"];
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        public bool IsMasterPassCancel
        {
            get
            {
                return string.IsNullOrEmpty(Request.QueryString["oauth_verifier"]);
            }
        }

        public bool IsLineCancel
        {
            get
            {
                bool temp;
                if (string.IsNullOrEmpty(Request.QueryString["LineCancel"]))
                {
                    return false;
                }
                return bool.TryParse(Request.QueryString["LineCancel"], out temp) && temp;
            }
        }

        public string LineTransactionId
        {
            get
            {
                return !string.IsNullOrEmpty(Request.QueryString["transactionId"]) ? Request.QueryString["transactionId"] : string.Empty;
            }
        }

        public bool IsTaishinPayFailure
        {
            get
            {
                bool temp;
                if (string.IsNullOrEmpty(Request.QueryString["TaishinPayFailure"]))
                {
                    return false;
                }
                return bool.TryParse(Request.QueryString["TaishinPayFailure"], out temp) && temp;
            }
        }

        /// <summary>
        /// 基礎毛利率
        /// </summary>
        public decimal BaseGrossMargin { set; get; }

        public Guid MainGuid { set; get; }

        public EinvoiceTriple TripleData { set; get; }

        public string AppTitle
        {
            get { return TheDeal.ItemName; }
        }

        public bool LinePayOnly
        {
            get
            {
                var payWayCookie = Request.Cookies[LkSiteCookie.PayWay.ToString()];
                if (payWayCookie == null) return false;
                var pwList = new JsonSerializer().Deserialize<List<PayWayLimit>>(payWayCookie.Value);
                return
                    pwList.Any(x => x.PaymentType == (int)PaymentType.LinePay && x.Bid == BusinessHourGuid && x.Expires > DateTime.Now);
            }
        }

        public Guid BuyTransactionGuid
        {
            get
            {
                if (Session[LkSiteSession.BuyTransactionGuid + "_Guid"] == null)
                {
                    Guid ret = Guid.NewGuid();
                    Session[LkSiteSession.BuyTransactionGuid + "_Guid"] = ret;
                    return ret;
                }
                Guid parseBuyTransGuid;
                Guid.TryParse(Session[LkSiteSession.BuyTransactionGuid + "_Guid"].ToString(), out parseBuyTransGuid);
                return parseBuyTransGuid;
            }
        }

        public BuyTransPaymentMethod BuyTransPaymentMethod { get; set; }

        public int BankId
        {
            get
            {
                DealPayment dealpayment = PponFacade.DealPaymentGetByBid(BusinessHourGuid);
                int BankId = 0;
                if (dealpayment != null)
                {
                    if (dealpayment.BankId.HasValue)
                    {
                        BankId = dealpayment.BankId.Value;
                    }
                    else
                    {
                        BankId = 0;
                    }
                }
                return BankId;
            }
        }

        public string MemberCreditCardListSetupScript
        {
            get
            {
                List<MemberCreditCardInfo> cards = MemberFacade.GetMemberCreditCardList(this.UserId, BankId);
                //台新商城只出現台新信用卡
                if (ChannelFacade.GetOrderClassificationByHeaderToken() == AgentChannel.TaiShinMall)
                {
                    cards = cards.Where(x => x.BankId == config.TaishinCreditCardBankId).ToList();
                }
                return string.Format("window.mmcards=JSON.parse('{0}')", ProviderFactory.Instance().GetSerializer().Serialize(cards));
            }
            //private set;
        }

        public int TaishinCreditCardBankId
        {
            get { return config.TaishinCreditCardBankId; }
        }

        public string ExternalUserId
        {
            get
            {
                string externalUserId = string.Empty;
                PcashMemberLink oldLink = mp.PcashMemberLinkGet(this.UserId);
                if (oldLink.IsLoaded)
                {
                    externalUserId = oldLink.ExternalUserId;
                }

                return externalUserId;
            }
        }

        #endregion properties

        private List<string> SessionNameList = new List<string>()
        {
            LkSiteSession.TransId.ToString(),
            LkSiteSession.Carrier.ToString()
        };
        protected void Page_Load(object sender, EventArgs e)
        {
            //將ticketId備份在session，是為了超商取貨跳轉選取貨店面，不選擇確定而是按退回頁，需帶回原本的選項
            if (Helper.IsInSiteUrl(HttpContext.Current.Request.UrlReferrer))
            {
                BackupTicketIdForHistoryBack = string.Empty;
            }

            string backTicketId = BackupTicketIdForHistoryBack;
            if (string.IsNullOrEmpty(backTicketId) == false)
            {
                this.TicketId = backTicketId;
            }

            if (Request.QueryString["TransId"] != null && Request.QueryString["ret_code"] != null && Request.QueryString["ret_msg"] != null)
            {
                //OTPLOG
                PaymentFacade.OTPApiLog(Request.QueryString["TransId"].ToString(), Newtonsoft.Json.JsonConvert.SerializeObject(new { ret_code = Request.QueryString["ret_code"], ret_msg = Request.QueryString["ret_msg"] }));
                logger.Info("BuyTest " + "transid:" + Request.QueryString["TransId"].ToString() + " " + Page.IsPostBack.ToString());
            }

            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            _GUESTBUY_LIMIT_PRICE = config.CreditcardReferAmount;
            
            if (!Page.IsPostBack)
            {
                if (string.IsNullOrEmpty(TicketId))
                {
                    //初進付款頁 刪除結帳未完成的session
                    var session_list = Session.GetEnumerator();
                    List<string> redundant_session_list = new List<string>();
                    while (session_list.MoveNext())
                    {
                        string sessionName = (string)session_list.Current;
                        if (SessionNameList.Any(x => x.StartsWith(sessionName, StringComparison.InvariantCultureIgnoreCase)))
                        {
                            redundant_session_list.Add(sessionName);
                        }
                    }
                    redundant_session_list.ForEach(x =>
                    {
                        Session.Remove(x);
                    });
                    Session[LkSiteSession.BuyTransactionGuid + "_Guid"] = null;
                    BuyTransPaymentMethod = BuyTransPaymentMethod.Init;

                    Context.Items[LkSiteContextItem.BuyTransaction] =
                        new BuyTransactionModel(BuyTransactionGuid, BuyTransPaymentMethod, BuyTransPaymentSection.BuyInit);
                }
                logger.Info("BuyTest " + "ticket：" + TicketId + "OnViewInitialized=outer");
                if (!_presenter.OnViewInitialized())
                {
                    logger.Info("BuyTest " + "ticket：" + TicketId + "OnViewInitialized=false");
                    if (Response.IsRequestBeingRedirected == false)
                    {
                        Response.Redirect("~/error-sgo.aspx");
                    }
                    return;
                }
                else
                {
                    if (PanelPaySucessForApplePay.Visible)
                    {
                        Response.Redirect(Request.RawUrl);
                    }
                }
            }

            pan_MasterPass.Visible = config.EnableMasterPass && !LinePayOnly;
            LinePayWay.Visible = config.EnableLinePay || LinePayOnly;
            if (config.EnableTaishinThirdPartyPay && User.Identity.IsAuthenticated)
            {
                pan_TaishinPay.Visible = true;
            }
            else
            {
                pan_TaishinPay.Visible = false;
            }

            if (LinePayOnly)
            {
                rbPayWayByCreditCard.Checked = false;
                rbPayWayByLinePay.Checked = true;
                pan_CreditCardWay.Visible = false;
                AtmPayWay.Visible = false;
            }

            //分期限台新、國泰卡
            if (rbInstallment3Months.Checked || rbInstallment6Months.Checked || rbInstallment12Months.Checked)
            {
                hidIsTaishinCreditCardOnly.Value = true.ToString();
                TaishinCreditcards = config.EnabledCathayPaymnetGateway ? CreditCardPremiumManager.GetCreditcardBankBinsByInstallment() : CreditCardPremiumManager.GetCreditcardBankBinsByBankId(1);
            }
            //由台新app進入僅能使用台新卡
            if (TaishinCreditCardOnly)
            {
                LinePayWay.Visible = false;
                pan_MasterPass.Visible = false;
                AtmPayWay.Visible = false;
                phSevenIspPayWay.Visible = false;
                hidIsTaishinCreditCardOnly.Value = TaishinCreditCardOnly.ToString();
            }

            #region 信用卡Only(目前只有台新)
            //先判斷是不是只能使用信用卡檔期

            if (BankId != 0)
            {
                //隱藏17life購物金的使用
                pScash.Visible = false;
                //隱藏17life紅利金的使用
                pBonus.Visible = false;
                //隱藏其他付款Panel
                pan_MasterPass.Visible = false;
                pan_TaishinPay.Visible = false;
                LinePayWay.Visible = false;
                AtmPayWay.Visible = false;
                phInstallmentContainer.Visible = false;
                repBuyPromotionText.Visible = false;
            }


            #endregion

            #region ApplePay

            bool isSupportApplePay = false;
            if (config.EnabledApplePay)
            {
                //identify user os
                var userAgent = Request.UserAgent;
                if ((userAgent.Contains("iPad") || userAgent.Contains("iPhone") || userAgent.Contains("Mac OS")) && userAgent.Contains("Safari") && userAgent.Contains("Version") && Request.IsSecureConnection)
                {
                    isSupportApplePay = true;
                }
            }
            hdSessionVersion.Value = config.EnabledApplePay ? config.ApplePaySessionVersion.ToString() : string.Empty;
            ApplePayWay.Visible = panApplePay.Visible = config.EnabledApplePay && isSupportApplePay;

            #endregion

            #region 兌換pez購物金
            hidMerchantContent.Value = PayeasyFacade.GetMerchantContent();

            if (config.EnablePcashXchUser == string.Empty)
                phPezExchange.Visible = true;
            else
                phPezExchange.Visible = config.EnablePcashXchUser.Contains(this.UserId.ToString());
            #endregion

            StyleSheetAdd();
            _presenter.OnViewLoaded();
        }

        #region InterfaceMember

        public void GoToPponDefaultPage(Guid bid, string message, bool isdefault)
        {
            if (string.IsNullOrEmpty(message))
            {
                if (isdefault)
                {
                    Response.Redirect(ResolveUrl(string.Format("~/deal/{0}{1}", bid, string.IsNullOrEmpty(Rsrc) ? string.Empty : "?rsrc=" + Rsrc)));
                }
                else
                {
                    Response.Redirect(string.Format("buy.aspx?bid={0}{1}", bid, string.IsNullOrEmpty(Rsrc) ? string.Empty : "&rsrc=" + Rsrc));
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(),
                    "alert", "alert('" + message + "');window.location.href='" + ResolveClientUrl(string.Format("~/deal/{0}{1}", bid.ToString(), string.IsNullOrEmpty(Rsrc) ? string.Empty : "?rsrc=" + Rsrc)) + "';", true);
            }
        }

        public void RedirectToLogin()
        {
            FormsAuthentication.RedirectToLoginPage();
        }

        public ListItemCollection GetItemsByInt(int total)
        {
            ListItemCollection lc = new ListItemCollection();
            if (total > 1000)
                total = 1000;
            for (int i = 1; i <= total; i++)
            {
                lc.Add(new ListItem(i.ToString(), i.ToString()));
            }

            if (total < 1)
            {
                lc.Add(new ListItem("0", "0"));
            }

            return lc;
        }

        public void SetContent(IViewPponDeal theDeal, ViewMemberBuildingCity deliveryInfo, AccessoryGroupCollection multOption,
            int alreadyboughtcount, ViewPponStoreCollection stores, bool isUseDiscountCode,
            EntrustSellType entrustSell, bool isDailyRestriction, EinvoiceTriple einvoiceTriple, IEnumerable<ViewDiscountDetail> discountList,
            bool installment3Months, bool installment6Months, bool installment12Months, List<BuyPromotionText> buyPromotionTexts,
            bool isATMAvailable, PponPickupStores pickupStores, ProductDeliveryType productDeliveryInfo, int ispQuantityLimit,
            ProductDeliveryType? memberLastProductDeliveryType)
        {
            int qty = theDeal.MaxItemCount ?? 10;

            InvoiceTitle.Text = einvoiceTriple.Title;
            InvoiceNumber.Text = einvoiceTriple.VatNumber == "0" ? "" : einvoiceTriple.VatNumber;
            InvoiceBuyerName.Text = einvoiceTriple.UserName;

            //顯示收據地址
            if (!string.IsNullOrEmpty(einvoiceTriple.Adress))
            {
                var addressArray = einvoiceTriple.Adress.Split('|');

                if (addressArray.Count() > 1 && addressArray.Count() == 3)
                {
                    hidInvoiceCity.Value = addressArray[0];
                    hidInvoiceArea.Value = addressArray[1];

                    var invoiceAddressArray = addressArray[2].Split('@');
                    if (invoiceAddressArray.Count() > 1 && invoiceAddressArray.Count() == 2)
                    {
                        InvoiceAddress.Text = invoiceAddressArray[0];
                    }
                    else
                    {
                        InvoiceAddress.Text = addressArray[2];
                    }
                }
                else
                {
                    InvoiceAddress.Text = einvoiceTriple.Adress;
                }
            }
            else
            {
                InvoiceAddress.Text = einvoiceTriple.Adress;
            }
            hidEinvoiceTripleId.Value = einvoiceTriple.Id.ToString();

            //是否顯示DiscountCode的顯示畫面

            pDiscount.Visible = isUseDiscountCode;

            if (config.EnableGrossMarginRestrictions)
            {
                if (isUseDiscountCode)
                {
                    Guid mainBid = ViewPponDealManager.DefaultManager.MainBusinessHourGuidGetByBid(theDeal.BusinessHourGuid); //母檔BID
                    if (PromotionFacade.GetDiscountUseType(mainBid))
                    {
                        pDiscountCodeInput.Visible = true;
                        pNotDiscount.Visible = false;
                    }
                    else
                    {
                        pDiscountCodeInput.Visible = false;
                        pNotDiscount.Visible = true;
                    }
                }
            }

            // 載入可用折價券資料
            if (config.EnableBuyMemberDiscountList)
            {
                if (isUseDiscountCode)
                {
                    btnSelectDiscount.Visible = true;
                    liDiscountCount.Text = liDiscountMCount.Text = "0";
                    if (discountList.Any())
                    {
                        var tmpData = discountList.Where(x => BaseGrossMargin >= x.MinGrossMargin || x.MinGrossMargin == null);
                        if (tmpData.Any())
                        {
                            rptDiscountList.DataSource = tmpData;
                            rptDiscountList.DataBind();
                            liDiscountCount.Text = tmpData.Count().ToString();
                            rptDiscountMList.DataSource = tmpData;
                            rptDiscountMList.DataBind();
                            liDiscountMCount.Text = tmpData.Count().ToString();
                        }

                    }
                }
            }

            if (theDeal.ItemDefaultDailyAmount != null)
            {
                if (alreadyboughtcount < theDeal.ItemDefaultDailyAmount.Value)
                {
                    int stillcanbuycount = theDeal.ItemDefaultDailyAmount.Value - alreadyboughtcount;
                    qty = qty >= stillcanbuycount ? stillcanbuycount : qty;
                }
                else
                {
                    string message = string.Format(isDailyRestriction ? I18N.Message.DailyRestrictionLimit : I18N.Message.Alreadyboughtcount, alreadyboughtcount);
                    AlertMessageAndGoToDefault(message, theDeal.BusinessHourGuid);
                }
            }

            bool isGroupCoupon = Helper.IsFlagSet(theDeal.BusinessHourStatus, BusinessHourStatus.GroupCoupon);

            if (isGroupCoupon)
            {
                qty = theDeal.SaleMultipleBase - theDeal.PresentQuantity ?? 0;
                hidQtyLimit.Value = qty.ToString();
                itemQty.DataSource =
                    new ListItemCollection {
                        new ListItem(qty.ToString() + ((theDeal.PresentQuantity ?? 0) > 0 ? "+" + theDeal.PresentQuantity : string.Empty), "1")
                    };
            }
            else
            {
                hidQtyLimit.Value = qty.ToString();
                itemQty.DataSource = GetItemsByInt(qty);
            }

            itemQty.DataTextField = "Text";
            itemQty.DataValueField = "Value";
            itemQty.DataBind();
            string[] badd = string.IsNullOrWhiteSpace(UserInfo) ? new string[] { } : UserInfo.Split('|');
            if (badd.Length > 0)
            {
                // 購買人資訊                            
                BuyerMobile.Text = badd[2];

                BuyerName.Text = badd[7];
                BuyerEmail.Text = badd[8];
                buyerUserName.Value = badd[9];

                if (User.Identity.IsAuthenticated && BuyerEmail.Text != string.Empty)
                {
                    BuyerEmail.ReadOnly = true;
                    BuyerEmail.Style["border"] = "0px";
                }


                int city = int.TryParse(badd[3], out city) ? city : -1;
                BuyerCity = city;
                int townshipId = int.TryParse(badd[4], out townshipId) ? townshipId : -1;
                BuyerTownshipId = townshipId;
                BuyerCityAddress = badd[6];
            }

            // 電子發票 載具類型
            ddlCarrierType.Items.Clear();
            ddlCarrierType.Items.Add(new ListItem(Helper.GetLocalizedEnum(CarrierType.Member), ((int)CarrierType.Member).ToString()));
            ddlCarrierType.Items.Add(new ListItem(Helper.GetLocalizedEnum(CarrierType.Phone), ((int)CarrierType.Phone).ToString()));
            ddlCarrierType.Items.Add(new ListItem(Helper.GetLocalizedEnum(CarrierType.PersonalCertificate), ((int)CarrierType.PersonalCertificate).ToString()));
            if (config.EnabledInvoicePaperRequest)
            {
                ddlCarrierType.Items.Add(new ListItem("個人紙本發票(二聯式)", ((int)CarrierType.None).ToString()));

                //帶入曾使用電子發票載具資料 & InvoiceAddress欄位要是空值時
                if (!string.IsNullOrEmpty(deliveryInfo.InvoiceBuyerAddress) && string.IsNullOrEmpty(InvoiceAddress.Text))
                {
                    InvoiceBuyerName.Text = deliveryInfo.InvoiceBuyerName;

                    var addressArray = deliveryInfo.InvoiceBuyerAddress.Split('|');

                    if (addressArray.Count() > 1 && addressArray.Count() == 3)
                    {
                        hidInvoiceCity.Value = addressArray[0];
                        hidInvoiceArea.Value = addressArray[1];

                        var invoiceAddressArray = addressArray[2].Split('@');
                        if (invoiceAddressArray.Count() > 1 && invoiceAddressArray.Count() == 2)
                        {
                            InvoiceAddress.Text = invoiceAddressArray[0];
                        }
                        else
                        {
                            InvoiceAddress.Text = addressArray[2];
                        }
                    }
                    else
                    {
                        InvoiceAddress.Text = deliveryInfo.InvoiceBuyerAddress;
                    }
                }
                else
                {
                    InvoiceAddress.Text = deliveryInfo.InvoiceBuyerAddress;
                }
            }



            switch (deliveryInfo.CarrierType)
            {
                case 0:
                    //member.carrier_type 預設值為0(個人電子發票(二聯式)), 若無發票收件人姓名 or 收件人地址, 則 carrier_type需設成 1 (會員載具)
                    if (!string.IsNullOrWhiteSpace(deliveryInfo.InvoiceBuyerName) && !string.IsNullOrWhiteSpace(deliveryInfo.InvoiceBuyerAddress))
                    {
                        ddlCarrierType.SelectedValue = ((int)CarrierType.None).ToString();
                        EInvoiceIsNotMember = "checked";
                    }
                    else
                    {
                        ddlCarrierType.SelectedValue = ((int)CarrierType.Member).ToString();
                        EInvoiceIsNotMember = string.Empty;
                    }
                    break;
                case 1:
                    ddlCarrierType.SelectedValue = ((int)CarrierType.Member).ToString();
                    EInvoiceIsNotMember = string.Empty;
                    break;
                case 2:
                    ddlCarrierType.SelectedValue = ((int)CarrierType.Phone).ToString();
                    txtPhoneCarrier.Text = deliveryInfo.CarrierId;
                    EInvoiceIsNotMember = "checked";
                    break;
                case 3:
                    ddlCarrierType.SelectedValue = ((int)CarrierType.PersonalCertificate).ToString();
                    txtPersonalCertificateCarrier.Text = deliveryInfo.CarrierId;
                    EInvoiceIsNotMember = "checked";
                    break;
            }

            // 捐贈單位
            ddlInvoiceDonation.Items.Clear();
            CodeName[] loveCodes = LoveCodeManager.Instance.LoveCodes;
            if (new Random().NextDouble() > 0.5)
            {
                //社團法人台灣一起夢想公益協會
                CodeName gomajiDonation = loveCodes.Where(x => string.Equals(gomajiDonationCode, x.Code)).FirstOrDefault();
                if (gomajiDonation != null)
                {
                    ddlInvoiceDonation.Items.Add(new ListItem("社團法人台灣一起夢想公益協會", gomajiDonation.Code));
                }

                // 南投縣青少年空手道推展協會
                CodeName pezDonation = loveCodes.Where(x => string.Equals(pezDonationCode, x.Code)).FirstOrDefault();
                if (pezDonation != null)
                {
                    ddlInvoiceDonation.Items.Add(new ListItem("南投縣青少年空手道推展協會", pezDonation.Code));
                }
            }
            else
            {
                // 南投縣青少年空手道推展協會
                CodeName pezDonation = loveCodes.Where(x => string.Equals(pezDonationCode, x.Code)).FirstOrDefault();
                if (pezDonation != null)
                {
                    ddlInvoiceDonation.Items.Add(new ListItem("南投縣青少年空手道推展協會", pezDonation.Code));
                };
                //社團法人台灣一起夢想公益協會
                CodeName gomajiDonation = loveCodes.Where(x => string.Equals(gomajiDonationCode, x.Code)).FirstOrDefault();
                if (gomajiDonation != null)
                {
                    ddlInvoiceDonation.Items.Add(new ListItem("社團法人台灣一起夢想公益協會", gomajiDonation.Code));
                }
            }
            // 創世社會福利基金會
            CodeName genesisDonation = loveCodes.Where(x => string.Equals(genesisDonationCode, x.Code)).FirstOrDefault();
            if (genesisDonation != null)
            {
                ddlInvoiceDonation.Items.Add(new ListItem("創世社會福利基金會", genesisDonation.Code));
            }
            // 財團法人陽光社會福利基金會
            CodeName sunDonation = loveCodes.Where(x => string.Equals(sunDonationCode, x.Code)).FirstOrDefault();
            if (sunDonation != null)
            {
                ddlInvoiceDonation.Items.Add(new ListItem("財團法人陽光社會福利基金會", sunDonation.Code));
            }


            ddlInvoiceDonation.Items.Add(new ListItem("其它捐贈單位", string.Empty));

            BuyerAddressPanel.Style.Add(HtmlTextWriterStyle.Display, "none"); // 購買人異動姓名Method MemberPurchaserSet需要townshipId
            // 好康到店到府消費方式判斷；PponItem=ToHouse
            if (theDeal.DeliveryType.HasValue && Equals((int)DeliveryType.ToHouse, theDeal.DeliveryType.Value))
            {
                IsItem = true;
                pStore.Visible = false;
                ph_DealInfoUrl.Visible = true;//商品(宅配)檔詳細說明連結

                // 收件人
                MemberDeliveryCollection receivers = mp.MemberDeliveryGetCollection(this.UserId);
                if (this.IsAuthenticated)
                {
                    receiverSelect.Items.Add(new ListItem
                    {
                        Text = "--選擇收件人資料--",
                        Value = "0"
                    });
                    foreach (MemberDelivery receiver in receivers)
                    {
                        receiverSelect.Items.Add(new ListItem
                        {
                            Text = receiver.AddressAlias,
                            Value = receiver.Id.ToString()
                        });
                    }
                }
                else
                {
                    receiverSelect.Visible = false;
                }

                // 隱藏分店選單
                ddlStore.Visible = false;
                lblSingleStore.Visible = false;

                if (TheDeal.EnableIsp)
                {
                    var dealServiceChannels = PponFacade.GetIspServiceChannelByBid(BusinessHourGuid);

                    rowProductDeliveryFamily.Visible = config.EnableFamiMap &&
                                                       dealServiceChannels.Contains(ServiceChannel.FamilyMart);
                    rowProductDeliverySeven.Visible = config.EnableSevenMap &&
                                                      dealServiceChannels.Contains(ServiceChannel.SevenEleven) &&
                                                      config.EnableSevenIsp;
                }
                else
                {
                    rowProductDeliveryFamily.Visible = rowProductDeliverySeven.Visible = false;
                }

            }
            else
            {
                IsItem = false;
                ReceiverInfoConfirmPanel.Visible = false;
                ReceiverInfoPanel.Visible = false;
                AddReceiverInfo.Checked = false;
                ph_DealInfoUrl.Visible = false;//商品(宅配)檔詳細說明連結

                //通用券檔次購買時無須選擇分店 or 全家檔次不秀相關store資訊
                pStore.Visible = !(Helper.IsFlagSet(theDeal.BusinessHourStatus, BusinessHourStatus.NoRestrictedStore) || Helper.IsFlagSet(theDeal.GroupOrderStatus ?? 0, (int)GroupOrderStatus.FamiDeal));

                // 分店選單
                if (pStore.Visible && stores.Count > 0)
                {
                    if (theDeal.MultipleBranch.GetValueOrDefault())
                    {
                        ProcessBranches(stores);
                    }
                    else
                    {
                        multiStores.Visible = false;
                        SetStore(stores);
                    }
                }
                else
                {
                    ddlStore.Visible = false;
                    lblSingleStore.Visible = false;
                }
                phProductDeliveryType.Visible = false;
            }

            dealName = string.Format("【{0}】", theDeal.ItemName);

            hidComboPack.Value = theDeal.ComboPackCount.HasValue ? theDeal.ComboPackCount.Value.ToString() : "1";
            hidqty.Value = "0";
            hidbcash.Value = "0";

            #region 運費

            // 階梯式
            if (null != TheCouponFreight && TheCouponFreight.Count > 0)
            {
                // 規則：
                // 1.運費=0 → 還差□份就免運費囉！
                // 2.下一運費<目前運費 → 還差□份，運費將降為□元囉！
                // 3.其他 → 無顯示
                CouponFreight lowestCouponFreight = TheCouponFreight.OrderBy(x => x.FreightAmount).First();
                CouponFreight highestCouponFreight = TheCouponFreight.OrderBy(x => x.FreightAmount).Last();
                deliveryCharge = highestCouponFreight.FreightAmount;

                bool hasZero = false;
                decimal zeroPriceAmount = 0;
                if (lowestCouponFreight.FreightAmount.Equals(0))
                {
                    hasZero = true;
                    zeroPriceAmount = lowestCouponFreight.StartAmount;
                }

                if (hasZero)
                {
                    lbldc.InnerHtml = string.Format("<span class='hint'>-還差{0}份就免運費囉！</span>", Math.Ceiling((zeroPriceAmount / theDeal.ItemPrice)).ToString("F0"));
                }
                else
                {
                    if (TheCouponFreight.Count == 1)
                    {
                        //如果只有設定一筆運費，且免運的購買金額設為0 ，即表示總是要收運費，那提示還差多少免運的訊息就隱藏
                        lbldc.Visible = false;
                    }
                    else
                    {
                        lbldc.InnerHtml = string.Format("<span class='hint'>-還差{0}份，運費將降為{1}元囉！</span>", Math.Ceiling((lowestCouponFreight.StartAmount / theDeal.ItemPrice)).ToString("F0"), lowestCouponFreight.FreightAmount.ToString("F0"));
                    }
                }

                JsonSerializer serializer = new JsonSerializer();
                hidFreight.Value = serializer.Serialize(from x in TheCouponFreight select new { startAmt = x.StartAmount, endAmt = x.EndAmount, freightAmt = x.FreightAmount });
            }
            else
            {
                hidFreight.Value = string.Empty;
                deliveryCharge = theDeal.BusinessHourDeliveryCharge;
            }

            #endregion 運費



            itemLimits = new List<OptionItemLimit>();
            if (multOption.Count == 0)
            {
                rpOption.HeaderTemplate = rpOption.FooterTemplate = null;
            }

            rpOption.DataSource = multOption;
            rpOption.DataBind();

            // 選項數量檢查
            hidCurrentLimits.Value = new JsonSerializer().Serialize(itemLimits);

            // ATM
            AtmPayWay.Visible = false;
            if (isATMAvailable && LinePayOnly == false)
            {
                AtmPayWay.Visible = true;
            }

            //如果是代收轉付檔次，將發票頁隱藏，開啟收據頁
            this.EntrustSell = entrustSell;
            phInvoice.Visible = phInvoiceConfirm.Visible = this.EntrustSell == EntrustSellType.No;
            phReceipt.Visible = phReceiptConfirm.Visible = this.EntrustSell != EntrustSellType.No;

            if (IsAuthenticated == false)
            {
                InvoiceSave.Checked = false;
                InvoiceSave.Visible = false;
            }

            //if ((deal.GroupOrderStatus & (int)GroupOrderStatus.KindDeal) == 0 && deal.ItemPrice > 0)
            //{
            //    ph_RetargetingCart.Visible = true;
            //}

            //分期付款
            //installment12Months = false;  //2015/10/20 [取消12期分期]
            installment12Months = installment3Months && BaseGrossMargin > config.EnableInstallment12GrossMargin && DateTime.Now >= config.EnableInstallment12TimeS && DateTime.Now <= config.EnableInstallment12TimeE;
            if (config.InstallmentPayEnabled == false)
            {
                phInstallmentContainer.Visible = false;
            }
            else if (installment3Months == false && installment6Months == false && installment12Months == false)
            {
                phInstallmentContainer.Visible = false;
            }
            else if (LinePayOnly)
            {
                phInstallmentContainer.Visible = false;
            }
            else
            {
                phInstallment3Months.Visible = installment3Months;
                phInstallment6Months.Visible = installment6Months;
                phInstallment12Months.Visible = installment12Months;
            }

            //台新儲值支付
            bool isTaishinPayLinked;
            decimal userTaishinPayCashPoint = MemberFacade.GetTaisihinPayCashPoint(UserId, out isTaishinPayLinked);
            hidIsTaishinPayLinked.Value = isTaishinPayLinked.ToString();
            if (isTaishinPayLinked)
            {
                lbl_TaishinCashPoint.Text = userTaishinPayCashPoint.ToString("F0");
                pan_TaishinCashPoint.Visible = true;
                pan_nonTaishinCashPoint.Visible = false;
            }
            if (buyPromotionTexts != null && buyPromotionTexts.Count > 0)
            {
                repBuyPromotionText.DataSource = buyPromotionTexts;
                repBuyPromotionText.DataBind();
            }
            if (pickupStores != null)
            {
                //hidProductDeliveryType.Value = ((int)productDeliveryInfo).ToString();

                hidFamilyPickupStoreId.Value = pickupStores.FamilyStore.StoreId;
                hidFamilyPickupStoreName.Value = pickupStores.FamilyStore.StoreName;
                hidFamilyPickupStoreTel.Value = pickupStores.FamilyStore.StoreTel;
                hidFamilyPickupStoreAddr.Value = pickupStores.FamilyStore.StoreAddr;

                hidSevenPickupStoreName.Value = pickupStores.SevenStore.StoreName;
                hidSevenPickupStoreTel.Value = pickupStores.SevenStore.StoreTel;
                hidSevenPickupStoreAddr.Value = pickupStores.SevenStore.StoreAddr;
                hidSevenPickupStoreId.Value = pickupStores.SevenStore.StoreId;

                if (productDeliveryInfo == ProductDeliveryType.FamilyPickup)
                {
                    pPickupTitle.InnerText = string.Format("{0} {1}",
                        pickupStores.FamilyStore.StoreName, pickupStores.FamilyStore.StoreAddr);
                }
                else
                {
                    pPickupTitle.InnerText = string.Format("{0} {1}",
                        pickupStores.SevenStore.StoreName, pickupStores.SevenStore.StoreAddr);
                }
                hidIspQuantityLimit.Value = ispQuantityLimit.ToString();
            }
            MemberLastProductDeliveryType = memberLastProductDeliveryType == null ? string.Empty : ((int)memberLastProductDeliveryType).ToString();
        }

        private void SetStore(ViewPponStoreCollection stores)
        {
            var count = 0;
            pStore.Visible = true;

            //若有一家以上分店；以下拉式選單方式顯示供使用者點選
            //但分店若只有一家；畫面上直接顯示該店家資料，不須以下拉式選單方式呈現
            if (stores.Count > 1)
            {
                #region 多家分店

                ddlStore.Visible = true;
                ddlStore.Items.Clear();
                ddlStore.Items.Add("請選擇");
                divSingleStore.Visible = false;
                lblSingleStore.Visible = false;
                lblStoreChioce.Visible = true;
                lblStoreChioceConfirm.Visible = true;
                foreach (var store in stores.OrderBy(x => x.SortOrder))
                {
                    ListItem li = new ListItem(GetStoreOptionTitle(store), store.StoreGuid.ToString());
                    if (store.TotalQuantity <= 0 || store.OrderedQuantity >= store.TotalQuantity)
                    {
                        li.Attributes["disabled"] = "disabled";
                        li.Text += "(已售完)";
                    }
                    else
                    {
                        li.Attributes.Remove("disabled");
                    }

                    ddlStore.Items.Add(li);
                    count++;
                }

                #endregion 多家分店
            }
            else if (stores.Count == 1)
            {
                #region 一家分店

                ddlStore.Visible = false;
                divSingleStore.Visible = true;
                lblSingleStore.Visible = true;
                lblStoreChioce.Visible = false;
                lblSingleStore.Text = GetStoreOptionTitle(stores[0]);
                hidStoreGuid.Value = stores[0].StoreGuid.ToString();
                count++;

                #endregion 一家分店
            }

            if (count == 0)
            {
                #region 無分店，宅配檔

                ddlStore.Visible = false;
                lblSingleStore.Visible = false;
                lblStoreChioce.Visible = false;

                #endregion 無分店，宅配檔
            }
        }

        public void GotoPay(string ticketId)
        {
            Response.Redirect("buy.aspx?TicketId=" + ticketId);
        }

        public void GotoMasterPass(MasterPassData data, string ticketId)
        {
            hidMPCallbackUrl.Value = data.CallbackUrl;
            hidMPPairingRequestToken.Value = data.PairingToken;
            hidMPRequestToken.Value = data.RequestToken;
            hidMPRequestedDataTypes.Value = string.Join(",", data.PairingDataTypes);
            hidMPMerchantCheckoutId.Value = data.CheckoutIdentifier;
            hidMPAcceptedCards.Value = data.AcceptedCards;
            hidMPShippingSuppression.Value = data.ShippingSuppression.ToString();
            hidMPRewardsProgram.Value = data.RewardsProgram.ToString();
            hidMPAuthLevelBasic.Value = data.AuthLevelBasic.ToString();
            ph_MasterPassLightbox.Visible = true;

            if (config.IsMasterPassLocalTest)
            {
                HttpContext.Current.Response.Redirect(
                    data.CallbackUrl + "&oauth_verifier=test&oauth_token=test&checkout_resource_url=test", false);
            }
        }

        public void SetMasterPassPreCheckoutData(MasterPassPreCheckoutData data)
        {
            if (data.Cards.Count <= 0) return;

            hidMPConsumerWalletId.Value = data.ConsumerWalletId;
            hidMPPrecheckoutTransactionId.Value = data.PrecheckoutTransactionId;
            hidMPWalletName.Value = data.WalletName;

            ddlMasterPassCards.Visible = true;
            ddlMasterPassCards.DataSource = data.Cards.Select(x => new MasterPassCardInfo(x)).ToList();
            ddlMasterPassCards.DataBind();
            var defaultCard = data.Cards.First(x => x.SelectedAsDefault);
            ddlMasterPassCards.SelectedValue = string.Format("{0},{1},{2}", defaultCard.CardId, defaultCard.ExpiryMonth.ToString().PadLeft(2, '0'),
                defaultCard.ExpiryYear.ToString());

            if (!string.IsNullOrEmpty(data.MasterpassLogoUrl))
            {
                imgMasterPassLogo.ImageUrl = data.MasterpassLogoUrl;
                imgMasterPassLogo.Visible = true;
            }
            if (!string.IsNullOrEmpty(data.WalletPartnerLogoUrl) && data.MasterpassLogoUrl != data.WalletPartnerLogoUrl)
            {
                imgWalletLogo.ImageUrl = data.WalletPartnerLogoUrl;
                imgWalletLogo.Visible = true;
            }
            rbPayWayByMasterPass.Checked = true;
        }

        public void ShowTransactionPanel(PponPaymentDTO paymentDTO)
        {
            OrderNumber.Text = paymentDTO.TransactionId;
            TransAmount.Text = paymentDTO.DeliveryInfo.PendingAmount.ToString();
            TransDate.Text = DateTime.Today.ToShortDateString();

            ExpYear = GenerateDropDownListYearItems(ExpYear);

            PanelBuy.Visible = false;
            if (paymentDTO.DeliveryInfo.PaymentMethod == BuyTransPaymentMethod.ApplePay)
            {
                PanelPaySucessForApplePay.Visible = true;
                PanelCreditCradAPI.Visible = false;
            }
            else
            {
                PanelPaySucessForApplePay.Visible = false;
                PanelCreditCradAPI.Visible = true;
            }
            PanelResult.Visible = false;
        }

        public void ShowATMTransactionPanel(PponPaymentDTO paymentDTO)
        {
            var theDeal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(paymentDTO.BusinessHourGuid);
            if (paymentDTO.ATMAccount != null && paymentDTO.ATMAccount.Length == 14)
            {
                if (!string.IsNullOrWhiteSpace(paymentDTO.TransactionId))
                {
                    PaymentTransaction pt = op.PaymentTransactionGet(paymentDTO.TransactionId, PaymentType.ATM, PayTransType.Authorization);
                    int status = Helper.SetPaymentTransactionPhase(pt.Status, PayTransPhase.Requested);

                    PaymentFacade.UpdateTransaction(paymentDTO.TransactionId, pt.OrderGuid, PaymentType.ATM,
                        Convert.ToDecimal(paymentDTO.DeliveryInfo.PendingAmount), string.Empty,
                        PayTransType.Authorization, DateTime.Now, paymentDTO.ATMAccount, status, (int)PayTransResponseType.OK);

                    Response.Redirect("buy.aspx?TransId=" + paymentDTO.TransactionId + "&TicketId=" + TicketId);
                }
                else
                {
                    Response.Redirect(config.SiteUrl);
                }
            }
            else
            {
                AlertMessage("此檔好康ATM付款名額已滿，請您以刷卡方式購買本次好康");
                Response.Redirect("buy.aspx?bid=" + theDeal.BusinessHourGuid);
            }
        }

        public void ShowResult(PaymentResultPageType result, MemberLinkCollection mlc,
            PponPaymentDTO paymentDTO, IViewPponDeal pponDeal, int atmAmount,
            string reason = null, Order o = null)
        {
            logger.Info("ShowResult Result=" + result.ToString() + " OrderGuid = " + paymentDTO.OrderGuid.ToString() + " TransactionId=" + paymentDTO.TransactionId);
            PanelBuy.Visible = false;
            PanelCreditCradAPI.Visible = false;
            PanelResult.Visible = true;
            ATMOrderTotal.Text = atmAmount.ToString();

            PaymentResult = result;

            if (o != null)
            {
                if ((pponDeal.GroupOrderStatus & (int)GroupOrderStatus.KindDeal) == 0 && pponDeal.ItemPrice > 0)
                {
                    ph_RetargetingPayDone.Visible = true;
                }
                ph_PaySuccess.Visible = true;
            }

            SetPaymentResult(paymentDTO, reason);
            ph_relatedDeals.Visible = true;

            Context.Items[LkSiteContextItem.BuyTransaction] =
                new BuyTransactionModel(BuyTransactionGuid, BuyTransPaymentMethod, BuyTransPaymentSection.BuyEnd, paymentDTO.OrderGuid, (int)result);
        }

        public void FillContentData(PponPaymentDTO dto)
        {
            ItemOptionsJson = dto.ItemOptionsJson;
            BuyerInfoJson = dto.BuyerInfoJson;
            OtherInfoJson = dto.OtherInfoJson;
        }

        public void AlertMessage(string msg)
        {
            ClientScript.RegisterStartupScript(GetType(), "alerter", "alert('" + msg + "');", true);
        }

        public void AlertMessageAndGoToDefault(string msg, Guid bid)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert",
                "alert('" + msg + "');window.location.href='" + ResolveClientUrl("~/deal/" + bid) + "';", true);
        }

        public void CheckRefUrl()
        {
            if (Request.UrlReferrer != null)
            {
                if (Request.UrlReferrer.AbsolutePath.ToLower().IndexOf("bbhtodaydeal") >= 0)
                {
                    Session["rsrc"] = "BBH_ch";
                }
                else
                {
                    Session["rsrc"] = "";
                }
            }
            else
            {
                Session["rsrc"] = "";
            }
        }

        public void ShowCreditcardChargeInfo()
        {
            if (PaymentResult == PaymentResultPageType.CreditcardSuccess)
            {
                ClientScript.RegisterStartupScript(GetType(), "alerterR", "alert('謝謝您！您的刷卡程序已完成並授權成功，系統會將您跳轉至17Life首頁繼續瀏覽。');", true);
            }
            else if (PaymentResult == PaymentResultPageType.CreditcardFailed)
            {
                ClientScript.RegisterStartupScript(GetType(), "alerterR", "alert('很抱歉！您的刷卡授權未成功，系統會將您跳轉至購買頁面，請您再試一次。');", true);
            }
            Response.Redirect("default.aspx");
        }

        public void ReEnterPaymentGate(PaymentType paymentType, String transId)
        {
            Response.Redirect("buy.aspx?TransId=" + transId + "&TicketId=" + TicketId + (paymentType == PaymentType.PCash ? "&pType=2" : string.Empty));
        }

        public void RestartBuy(bool isUseDiscountCode, Guid bid, string alertMessage)
        {
            PanelBuy.Visible = true;
            pDiscount.Visible = isUseDiscountCode;
            PanelCreditCradAPI.Visible = false;
            PanelResult.Visible = false;
            ClientScript.RegisterStartupScript(GetType(), "restart", "alert('" + alertMessage + "');window.location.href='"
                + config.SSLSiteUrl + "/ppon/buy.aspx?bid=" + bid + "';", true);
        }
        public void RestartAtmToCreditcard(string message = "")
        {
            ClientScript.RegisterStartupScript(GetType(), "alerterR", string.Format("alert('{0}');",
                string.IsNullOrEmpty(message) ?
                "很抱歉！目前ATM轉帳名額已額滿，將帶您前往信用卡付款頁。" : message), true);
        }
        public void RestartAuth(string message = "")
        {
            ClientScript.RegisterStartupScript(GetType(), "alerterR", string.Format("alert('{0}');",
                string.IsNullOrEmpty(message) ?
                "很抱歉！您的信用卡資訊有錯誤，請您再試一次。" : message), true);
            PanelBuy.Visible = false;
            PanelCreditCradAPI.Visible = true;
            PanelResult.Visible = false;
        }

        public void GoToZeroActivity()
        {
            Response.Redirect("~/ppon/ZeroActivity.aspx?bid=" + BusinessHourGuid.ToString() + (IsPreview ? "&p=y" : string.Empty));
        }

        public void GoToKindPay()
        {
            Response.Redirect("~/ppon/KindPay.aspx?bid=" + BusinessHourGuid.ToString() + (IsPreview ? "&p=y" : string.Empty));
        }

        public void RedirectToLoginOnlyPEZ(string message, Guid bid)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('" + message + "');window.location.href='" + ResolveClientUrl("~/deal/" + bid) + "';", true);
        }

        public void RedirectToAppLimitedEdtionPage()
        {
            Response.Redirect("~/ppon/AppLimitedEdition/" + BusinessHourGuid);
        }

        private IList<OptionItemLimit> itemLimits
        {
            get;
            set;
        }

        #region ThirdPartyPay

        public void SetThirdPartyPayFailContent(string reason, PponPaymentDTO paymentDTO)
        {
            SetFailContent(false, paymentDTO, true, false, reason);
        }

        public void GoToLinePay(string url)
        {
            Response.Redirect(url);
        }

        public void GoToTaishinPay(string payCode, string orderId)
        {
            Response.Redirect(string.Format("/mvc/ThirdPartyPay/TaishinPaySubmit?code={0}&orderId={1}", payCode, orderId));
        }

        #endregion ThirdPartyPay

        #endregion InterfaceMember

        #region PageEvent

        protected void rpOption_ItemDataBound(Object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is AccessoryGroup)
            {
                List<ViewItemAccessoryGroup> viagc = ((AccessoryGroup)e.Item.DataItem).members;
                DropDownList ddl = (DropDownList)e.Item.FindControl("ddlMultOption");

                if (viagc.Count > 0)
                {
                    ddl.Items.Add(new ListItem(viagc[0].AccessoryGroupName, "Default"));
                    for (int i = 0; i < viagc.Count; i++)
                    {
                        ViewItemAccessoryGroup v = viagc[i];
                        ListItem li = new ListItem(v.AccessoryName, v.AccessoryGroupMemberGuid.ToString());
                        OptionItemLimit oil = new OptionItemLimit() { AccessoryName = v.AccessoryName, AccessoryId = v.AccessoryGroupMemberGuid.ToString() };
                        if (v.Quantity == null || v.Quantity > 0)
                        {
                            li.Attributes.Remove("disabled");

                            if (v.Quantity > 0)
                            {
                                oil.Limit = v.Quantity.Value;
                            }
                        }
                        else
                        {
                            oil.Limit = 0;
                            li.Attributes["disabled"] = "disabled";
                            li.Text += "(已售完)";
                        }

                        ddl.Items.Add(li);

                        if (v.Quantity != null)
                        {
                            itemLimits.Add(oil);
                        }
                    }
                }
                else
                {
                    ddl.Visible = false;
                }
            }
        }

        public void ibCheckOut_Click(object sender, EventArgs e)
        {
            IViewPponDeal theDeal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(this.BusinessHourGuid);
            this.TheDeal = theDeal;
            logger.InfoFormat("\r\n{0}\r\n{1}\r\n{2}", UserMemo, theDeal.BusinessHourGuid, theDeal.ComboPackCount);
            PponDeliveryInfo deliveryInfo = DeliveryInfo;


            if (deliveryInfo.InvoiceType == "3" && !string.IsNullOrEmpty(RegExRules.CompanyNoCheck(deliveryInfo.UnifiedSerialNumber)))
            {
                AlertMessage("請填入正確的統一編號，謝謝！");
                if (ResetContent != null)
                {
                    ResetContent(this, null);
                }
                return;
            }

            if (AllowGuestBuy == false)
            {
                Member mem = mp.MemberGet(this.UserId);
                if (mem.IsApproved == false)
                {
                    AlertMessage("帳號已被封鎖，詳情請洽客服");
                    if (ResetContent != null)
                    {
                        ResetContent(this, null);
                    }
                    return;
                }
            }

            // check option
            if (CheckOptions(UserMemo) == false)
            {
                if (ResetContent != null)
                {
                    ResetContent(this, null);
                }
                return;
            }

            //add for empty address
            if (TheDeal.DeliveryType.GetValueOrDefault() == (int)DeliveryType.ToHouse)
            {
                if (ProductDelivery == ProductDeliveryType.Normal)
                {
                    if (string.IsNullOrEmpty(deliveryInfo.WriteAddress))
                    {
                        AlertMessage("請選擇收件人地址");
                        if (ResetContent != null)
                        {
                            ResetContent(this, null);
                        }
                        return;
                    }
                }
                else if (ProductDelivery == ProductDeliveryType.FamilyPickup || ProductDelivery == ProductDeliveryType.SevenPickup)
                {
                    if (DeliveryInfo.PickupStore == null || string.IsNullOrEmpty(DeliveryInfo.PickupStore.StoreId) ||
                        string.IsNullOrEmpty(DeliveryInfo.PickupStore.StoreName) || string.IsNullOrEmpty(DeliveryInfo.PickupStore.StoreAddr)
                        || (ProductDelivery == ProductDeliveryType.FamilyPickup && string.IsNullOrEmpty(DeliveryInfo.PickupStore.StoreTel)))
                    {
                        AlertMessage("超商取貨選擇分店不完整");
                        if (ResetContent != null)
                        {
                            ResetContent(this, null);
                        }
                        return;
                    }
                }
            }

            if ((config.GuestBuyEnabled || AllowGuestBuy) && IsAuthenticated == false &&
                deliveryInfo.PendingAmount > config.CreditcardReferAmount)
            {
                AlertMessage(string.Format("由於此筆交易總金額高於{0}，為維護您的權益，請登入會員再進行購買喔！", config.CreditcardReferAmount));
                if (ResetContent != null)
                {
                    ResetContent(this, null);
                }
                return;
            }

            if ((config.GuestBuyEnabled || AllowGuestBuy) && User.Identity.IsAuthenticated == false
                && GetOrAddGuestMemberThenSetInfo != null)
            {
                string userName = BuyerEmail.Text.Trim();
                string mobile = BuyerMobile.Text.Trim();
                string displayName = ProposalFacade.RemoveSpecialCharacter(BuyerName.Text.Trim());
                try
                {
                    GetOrAddGuestMemberThenSetInfo(this, new GuestMemberEventArgs
                    {
                        UserName = userName,
                        Mobile = mobile,
                        DisplayName = displayName
                    });
                    deliveryInfo.BuyerUserId = MemberFacade.GetUniqueId(userName);
                }
                catch (Exception ex)
                {
                    AlertMessage(ex.Message);
                    if (ResetContent != null)
                    {
                        ResetContent(this, null);
                    }
                    return;
                }
            }
            PponPaymentDTO paymentDTO;
            if (CheckOut != null)
            {
                paymentDTO = PponBuyFacade.GetPponPaymentDTOFromSession(this.TicketId);
                if (paymentDTO == null)
                {
                    string errMsg = string.Format("ticket: {0} can't get data.", TicketId);
                    logger.Warn(errMsg);
                    //throw new Exception(errMsg);
                    paymentDTO = new PponPaymentDTO
                    {
                        State = PponPaymentDTOState.Init,
                        TicketId = OrderFacade.MakeRegularTicketId()
                    };

                }
                paymentDTO.DeliveryInfo = deliveryInfo;
                paymentDTO.IsSaveCreditCardInfo = this.IsSaveCreditCardInfo;
                paymentDTO.BuyerAddressInfo = this.AddressInfo;
                paymentDTO.IsThirdPartyPay = this.ThirdPartyPaymentSystem != ThirdPartyPayment.None;
                paymentDTO.ThirdPartyPaymentSystem = this.ThirdPartyPaymentSystem;
                paymentDTO.ItemOptions = this.UserMemo;
                paymentDTO.ItemOptionsJson = this.ItemOptionsJson;
                paymentDTO.BuyerInfoJson = this.BuyerInfoJson;
                paymentDTO.OtherInfoJson = this.OtherInfoJson;
                Guid tempGuid;
                if (Guid.TryParse(this.hdPrebuiltOrderGuid.Value, out tempGuid))
                {
                    paymentDTO.PrebuiltOrderGuid = tempGuid;
                }

                var eventArg = new DataEventArgs<PponPaymentDTO>(paymentDTO);
                CheckOut(this, eventArg);
            }
        }

        private bool CheckOptions(string itemOptions)
        {
            IViewPponDeal vpd = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(BusinessHourGuid);
            if (ProposalFacade.IsVbsProposalNewVersion() && (vpd.IsHouseNewVer ?? false))
            {
                if (string.IsNullOrWhiteSpace(itemOptions))
                {
                    return false;
                }
                //int optionCount = ip.AccessoryGroupGetListByItem(ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(BusinessHourGuid).ItemGuid).Count;
                string[] kk = itemOptions.Split(new string[] { "||" }, StringSplitOptions.RemoveEmptyEntries);
                if (!IsShoppingCart && kk.Length > 1) // 非購物車檔，應無法選擇超過一種以上的組合
                {
                    AlertMessage("抱歉，您所選擇選項不完整，請重新選擇數量或其他規格。");
                }

                //重新取得多檔次數量
                int optionCount = 0;
                List<OptionItemLimit> itemLimits = PponBuyFacade.GetProductItemList(
                    ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(BusinessHourGuid), out optionCount);

                foreach (string kz in kk)
                {
                    List<AccessoryEntry> acc = new List<AccessoryEntry>();
                    string[] yy = kz.Split(new string[] { "|#|" }, StringSplitOptions.None);
                    string[] accs = yy[0].Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                    if (!Equals(optionCount, accs.Length))
                    {
                        AlertMessage("抱歉，您所選擇選項不完整，請重新選擇數量或其他規格。");
                        return false;
                    }

                    foreach (string a in accs)
                    {
                        string[] names = a.Split(new string[] { "|^|" }, StringSplitOptions.RemoveEmptyEntries);
                        int q = int.Parse(yy[1]);
                        foreach (OptionItemLimit oil in itemLimits)
                        {
                            if (string.Equals(oil.AccessoryId, names[1]))
                            {
                                if (oil.Limit < q)
                                {
                                    AlertMessage("抱歉，您所選擇的 ( " + oil.AccessoryName + " ) 數量不足，請重新選擇數量或其他規格。");
                                    return false;
                                }
                                else
                                {
                                    oil.Limit -= q;
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                //重新取得多檔次數量
                List<OptionItemLimit> itemLimits = PponBuyFacade.GetGetAccessoryGroupList(
                    ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(BusinessHourGuid).ItemGuid);

                // "L|^|1c20f3ca-2374-497f-ac8e-a1cea98cf615,白色|^|7a4320b8-bbc7-4bd5-b820-415daa45775d|#|2|L|^|1c20f3ca-2374-497f-ac8e-a1cea98cf615,白色|^|7a4320b8-bbc7-4bd5-b820-415daa45775d|#|3|L|^|1c20f3ca-2374-497f-ac8e-a1cea98cf615,白色|^|7a4320b8-bbc7-4bd5-b820-415daa45775d|#|1"

                if (string.IsNullOrWhiteSpace(itemOptions))
                {
                    return false;
                }

                int optionCount = ip.AccessoryGroupGetListByItem(ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(BusinessHourGuid).ItemGuid).Count;
                string[] kk = itemOptions.Split(new string[] { "||" }, StringSplitOptions.RemoveEmptyEntries);
                if (!IsShoppingCart && kk.Length > 1) // 非購物車檔，應無法選擇超過一種以上的組合
                {
                    AlertMessage("抱歉，您所選擇選項不完整，請重新選擇數量或其他規格。");
                }
                foreach (string kz in kk)
                {
                    List<AccessoryEntry> acc = new List<AccessoryEntry>();
                    string[] yy = kz.Split(new string[] { "|#|" }, StringSplitOptions.None);
                    string[] accs = yy[0].Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                    if (!Equals(optionCount, accs.Length))
                    {
                        AlertMessage("抱歉，您所選擇選項不完整，請重新選擇數量或其他規格。");
                        return false;
                    }

                    foreach (string a in accs)
                    {
                        string[] names = a.Split(new string[] { "|^|" }, StringSplitOptions.RemoveEmptyEntries);
                        int q = int.Parse(yy[1]);
                        foreach (OptionItemLimit oil in itemLimits)
                        {
                            if (string.Equals(oil.AccessoryId, names[1]))
                            {
                                if (oil.Limit < q)
                                {
                                    AlertMessage("抱歉，您所選擇的 ( " + oil.AccessoryId + " ) 數量不足，請重新選擇數量或其他規格。");
                                    return false;
                                }
                                else
                                {
                                    oil.Limit -= q;
                                }
                            }
                        }
                    }
                }
            }

            return true;
        }

        #endregion PageEvent

        #region Credit Card API

        protected void AuthSSL_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(this.TicketId))
            {
                Response.Redirect(config.SiteUrl);
                return;
            }
            PponPaymentDTO paymentDTO = PponBuyFacade.GetPponPaymentDTOFromSession(this.TicketId);
            if (paymentDTO == null)
            {
                Response.Redirect(config.SiteUrl);
                return;
            }
            IViewPponDeal deal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(paymentDTO.BusinessHourGuid);
            int orderTotalAmount = paymentDTO.DeliveryInfo.TotalAmount;

            Context.Items[LkSiteContextItem.BuyTransaction] =
                new BuyTransactionModel(BuyTransactionGuid, BuyTransPaymentMethod, BuyTransPaymentSection.BuyTypeCreaditCardNumEnd,
                    paymentDTO.OrderGuid);

            Guid cardGuid;
            string creditCardKey = Request[ddlSelectCreditCard.UniqueID];
            if ((Guid.TryParse(creditCardKey, out cardGuid) || string.IsNullOrEmpty(CheckCreditCardInfo()))
                && PponFacade.CheckPayInstallmentValid(deal, paymentDTO.DeliveryInfo.CreditCardInstallment, orderTotalAmount))
            {
                // 如果消費者曾經使用信用卡結帳，帶出上次卡號
                bool check = true;
                string cardNum = string.Empty;
                if (cardGuid != Guid.Empty)
                {
                    try
                    {
                        MemberCreditCard mcc = mp.MemberCreditCardGetByGuidUserId(cardGuid, UserId);
                        cardNum = MemberFacade.DecryptCreditCardInfo(mcc.CreditCardInfo, mcc.EncryptSalt);

                        if (BankId != 0)
                        {
                            if (!mcc.IsLoaded || mcc.BankId != BankId)
                            {
                                check = false;
                                RestartAuth(string.Format("此好康限定，{0}付款。", Helper.GetEnumDescription((LimitBankIdModel)BankId)));
                            }
                        }

                    }
                    catch (Exception)
                    {
                        check = false;
                        RestartAuth("取得信用卡資料錯誤，請再次確認。");
                    }
                }
                else
                {
                    cardNum = CardNo1.Text + CardNo2.Text + CardNo3.Text + CardNo4.Text;

                    if (BankId != 0)
                    {
                        //回傳BankId
                        var bankInfo = CreditCardPremiumManager.GetCreditCardBankInfo(cardNum);

                        if (!bankInfo.IsLoaded || bankInfo.BankId != BankId)
                        {
                            check = false;
                            RestartAuth(string.Format("此好康限定，{0}付款。", Helper.GetEnumDescription((LimitBankIdModel)BankId)));
                        }
                    }
                }

                if (string.IsNullOrWhiteSpace(ExpYear.SelectedValue) || string.IsNullOrWhiteSpace(ExpMonth.SelectedValue))
                {
                    check = false;
                    RestartAuth("信用卡有效期限未輸入，請再次確認。");
                }

                if (string.IsNullOrWhiteSpace(SecurityCode.Text))
                {
                    check = false;
                    RestartAuth("信用卡背面末三碼未輸入，請再次確認。");
                }
                //如使用VISA獨享折價券 檢查VISA卡號
                if (paymentDTO.IsVisaDiscountCode)
                {
                    if (!CreditCardPremiumManager.IsVisaCardNumber(cardNum))
                    {
                        check = false;
                        SecurityCode.Text = CardNo1.Text = CardNo2.Text = CardNo3.Text = CardNo4.Text = string.Empty;
                        RestartAuth("您使用的折價券限刷VISA 金融卡，請再次確認您的卡號。");
                    }
                }
                if (TaishinCreditCardOnly)
                {
                    var testCard = cardNum.Substring(0, 6);
                    var allowCardNumList = TaishinCreditcards.Split(',');
                    var isTaishinCard = allowCardNumList.Any(card => testCard == card);
                    if (!isTaishinCard)
                    {
                        check = false;
                        RestartAuth("僅限使用台新信用卡，請再次確認您的信用卡號。");
                    }
                }
                if (check)
                {
                    paymentDTO.CreditCardInfo = new PponCreditCardInfo
                    {
                        CreditCardGuid = cardGuid,
                        TransactionId = paymentDTO.TransactionId,
                        Amount = paymentDTO.DeliveryInfo.PendingAmount,
                        CardNumber = cardNum,
                        ExpireYear = ExpYear.SelectedValue,
                        ExpireMonth = ExpMonth.SelectedValue,
                        SecurityCode = SecurityCode.Text,
                        Description = string.Empty,
                        InstallmentPeriod = (int)paymentDTO.DeliveryInfo.CreditCardInstallment,
                        CreditCardName = this.CreditCardName
                    };
                    paymentDTO.IsSaveCreditCardInfo = this.IsSaveCreditCardInfo;
                    paymentDTO.State = PponPaymentDTOState.Paying;
                    PponBuyFacade.SetPponPaymentDTOToSession(this.TicketId, paymentDTO);

                    Response.Redirect("buy.aspx?TransId=" + paymentDTO.TransactionId + "&TicketId=" + TicketId);
                }
            }
            else
            {
                RestartAuth();
            }
        }

        public void ShowMessageAndLogout(string msg)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('" + msg + "');window.location.href='../NewMember/Logout.aspx';", true);
            string script = string.Format("<script type='text/javascript'>alert('{0}');location.href='../NewMember/Logout.aspx';</script>", msg);
            Response.Clear();
            Response.Write(script);
            Response.End();
        }

        private string CheckCreditCardInfo()
        {
            Regex rex = new Regex(@"\D");
            // 記憶信用卡號專案 START
            if (ddlSelectCreditCard.Text != "")
            {
                return string.Empty;
            }
            // 記憶信用卡號專案 END
            if (!Equals(CardNo1.MaxLength, CardNo1.Text.Length) || rex.IsMatch(CardNo1.Text))
            {
                return "card number error!!";
            }

            if (!Equals(CardNo2.MaxLength, CardNo2.Text.Length) || rex.IsMatch(CardNo2.Text))
            {
                return "card number error!!";
            }

            if (!Equals(CardNo3.MaxLength, CardNo3.Text.Length) || rex.IsMatch(CardNo3.Text))
            {
                return "card number error!!";
            }

            if (!Equals(CardNo4.MaxLength, CardNo4.Text.Length) || rex.IsMatch(CardNo4.Text))
            {
                return "card number error!!";
            }
            return string.Empty;
        }

        #endregion Credit Card API

        private bool IsPiinlife
        {
            get { return Request.Url.AbsolutePath.StartsWith("/piinlife", StringComparison.OrdinalIgnoreCase); }
        }

        private void StyleSheetAdd()
        {
            if (IsPiinlife)
            {
                liPiinlifeMasterPageCss.Text = "<link rel=\"stylesheet\" type=\"text/css\" href=\"" + ResolveUrl("~/Themes/PCweb/HighDeal/MasterPage_HD.css") + "\" />";
                liPiinlifePponCss.Text = "<link rel=\"stylesheet\" type=\"text/css\" href=\"" + ResolveUrl("~/Themes/PCweb/HighDeal/ppon_HD.css") + "\" />";
                liPiinlifeRDLCss.Text = "<link rel=\"stylesheet\" type=\"text/css\" href=\"" + ResolveUrl("~/Themes/PCweb/HighDeal/RDL_HD.css") + "\" />";
            }
        }

        private void SetPaymentResult(PponPaymentDTO paymentDTO, string reason)
        {
            Member mem = MemberFacade.GetMember(this.User.Identity.Name);
            if (mem.IsFraudSuspect)
            {
                SetFailContent(true, paymentDTO);
                return;
            }
            switch (PaymentResult)
            {
                case PaymentResultPageType.CreditcardSuccess:
                case PaymentResultPageType.PromotionSuccess:
                case PaymentResultPageType.PcashSuccess:
                case PaymentResultPageType.ThirdPartyPaySuccess:
                    SetSuccessContent(paymentDTO);
                    break;
                case PaymentResultPageType.CreditcardFailed:
                    SetFailContent(true, paymentDTO);
                    break;
                case PaymentResultPageType.PcashFailed:
                    SetFailContent(false, paymentDTO);
                    break;
                case PaymentResultPageType.ThirdPartyPayCancel:
                    SetFailContent(false, paymentDTO, true, false, "使用者取消");
                    break;
                case PaymentResultPageType.ThirdPartyPayFail:
                    string faildReason = string.IsNullOrEmpty(reason) ? string.Format("{0}授權失敗。",
                        Helper.GetEnumDescription(paymentDTO.ThirdPartyPaymentSystem)) : reason;
                    SetFailContent(false, paymentDTO, true, false, faildReason);
                    break;
                case PaymentResultPageType.FamilyNetGetPincodeFail:
                case PaymentResultPageType.HiLifeGetPincodeFail:
                    SetFailContent(false, paymentDTO, false, true);
                    break;
                default:
                    Response.Redirect("~/error-sgo.aspx");
                    break;
            }
        }

        private void SetFailContent(bool isCreditcard, PponPaymentDTO paymentDTO,
            bool isThirdPartyPay = false, bool isFamiPincode = false, string reason = "")
        {
            var theDeal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(paymentDTO.BusinessHourGuid);
            pan_Fail.Visible = true;
            CreditcardFailInfo.Visible = false;
            PcashFailInfo.Visible = false;
            FailInfo.Visible = false;
            ThirdPartyPayFailInfo.Visible = false;
            FamiPincodeFailInfo.Visible = false;

            if (isThirdPartyPay) thirdPartyPayFailReason.InnerText = reason;

            if (string.IsNullOrWhiteSpace(paymentDTO.TransactionId))
            {
                if (isCreditcard)
                {
                    CreditcardFailInfo.Visible = true;
                }
                else if (isThirdPartyPay)
                {
                    ThirdPartyPayFailInfo.Visible = true;
                }
                else if (isFamiPincode)
                {
                    FamiPincodeFailInfo.Visible = true;
                }
                else
                {
                    PcashFailInfo.Visible = true;
                }
            }
            else
            {
                if (isThirdPartyPay)
                {
                    ThirdPartyPayFailInfo.Visible = true;
                    litThirdPartyPayFailMessage.Text = string.Empty;
                    if (paymentDTO.ThirdPartyPaymentSystem == ThirdPartyPayment.TaishinPay)
                    {
                        litThirdPartyPayFailMessage.Text = "若您的儲值支付帳戶已被扣款，我們將於2~5個工作天內全額退款至您的儲值支付帳戶，請您稍候，造成您的不便請見諒，謝謝。";
                        /*
                            <%if((ThirdPartyPaymentSystem == ThirdPartyPayment.TaishinPay)) { 
                                if(string.IsNullOrWhiteSpace(TransactionId)){ //未扣款 %>
                                    若您重新確認資料後仍無法成功授權交易，請洽台新銀行處理。
                                <%}else { //已扣款 %>
                                    若您的儲值支付帳戶已被扣款，我們將於2~5個工作天內全額退款至您的儲值支付帳戶，請您稍候，造成您的不便請見諒，謝謝。
                                <%}
                            } else {%>
                                若您重新確認資料後仍無法成功授權交易，請洽<%=Helper.GetEnumDescription(ThirdPartyPaymentSystem)%>處理。
                            <%} %>
                         */
                    }
                    else if (paymentDTO.ThirdPartyPaymentSystem == ThirdPartyPayment.LinePay)
                    {
                        litThirdPartyPayFailMessage.Text =
                            string.Format("若您重新確認資料後仍無法成功授權交易，請洽{0}處理。", Helper.GetEnumDescription(ThirdPartyPaymentSystem));
                    }
                }
                else if (isFamiPincode)
                {
                    FamiPincodeFailInfo.Visible = true;
                }
                else
                {
                    FailInfo.Visible = true;
                }
            }

            hyp_Fail.NavigateUrl = ResolveUrl(Request.Url.AbsolutePath + "?bid=" + theDeal.BusinessHourGuid);
        }

        private void SetSuccessContent(PponPaymentDTO paymentDTO)
        {
            var theDeal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(paymentDTO.BusinessHourGuid);
            Order order = ProviderFactory.Instance().GetProvider<IOrderProvider>().OrderGet(paymentDTO.OrderGuid);
            if (theDeal == null)
            {
                throw new Exception("SetSuccessContent fail, deal not found. bid=" + paymentDTO.BusinessHourGuid);
            }

            if (paymentDTO.OrderGuid == Guid.Empty)
            {
                return;
            }

            int deliveryId = PponCityGroup.DefaultPponCityGroup.AllCountry.CityId;

            bool isPaidByATM = paymentDTO.DeliveryInfo.PaymentMethod == BuyTransPaymentMethod.Atm;
            if (theDeal != null && !string.IsNullOrWhiteSpace(theDeal.ItemName))
            {
                if (theDeal.CityList.IndexOf(Convert.ToString(deliveryId)) > 0)
                {
                    BuySuccessText = "出貨狀態";
                }
                else
                {
                    BuySuccessText = "憑證號碼";
                }
                if (isPaidByATM)
                {
                    //ItemNameATM.Text = string.Format("感謝您訂購【{0}】好康。<br />請於下單購買當日 23:59:59 前使用ATM或網路ATM完成付款，您的訂單才算交易成功喔！<br />", theDeal.ItemName);
                    litItemNameATM.Text = theDeal.ItemName;
                    litOrderNo.Text = order.OrderId;
                }
                else
                {
                    ItemName.Text = string.Format("您已成功購得【{0}】好康。<br />", theDeal.ItemName);

                }
            }

            ATMOrderDate.Text = DateTime.Today.ToString("yyyy-MM-dd");
            ATMOrderAccount.Text = paymentDTO.ATMAccount;

            pan_Success.Visible = true;
            pan_Success_CreditCard.Visible = !isPaidByATM;
            pan_Success_ATM.Visible = isPaidByATM;
            plBuyAdBanner.Visible = true;

            #region 行銷滿額贈活動

            if (config.IsEnabledSpecialConsumingDiscount)
            {
                // 不分區or活動設定為品生活且當前為品生活館or活動設定為p好康且當前非品生活
                if (config.SpecialConsumingDiscountCid == 0 ||
                    (IsPiinlife && config.SpecialConsumingDiscountCid == PponCityGroup.DefaultPponCityGroup.Piinlife.CategoryId) ||
                    (IsPiinlife == false && config.SpecialConsumingDiscountCid != PponCityGroup.DefaultPponCityGroup.Piinlife.CategoryId))
                {
                    DateTime dateStart = DateTime.Parse(config.SpecialConsumingDiscountPeriod.Split('|')[0]);
                    DateTime dateEnd = DateTime.Parse(config.SpecialConsumingDiscountPeriod.Split('|')[1]);
                    if (DateTime.Now > dateStart && DateTime.Now < dateEnd)
                    {
                        int amount = PromotionFacade.GetSpecialConsumingAmount(UserId);
                        Session[LkSiteSession.SpecialConsumingDiscount.ToString()] = amount;
                        KeyValuePair<int, int> consumingDiscount = PromotionFacade.GetSpecialConsumingDiscount(amount);
                        divDiscountPromotion.Visible = true;
                        liDiscountPromotion.Text = consumingDiscount.Value.ToString();
                        liDiscountPromotionCount.Text = consumingDiscount.Key.ToString();
                    }
                    else
                    {
                        divDiscountPromotion.Visible = false;
                    }
                }
                else
                {
                    divDiscountPromotion.Visible = false;
                }
            }
            else
            {
                divDiscountPromotion.Visible = false;
            }

            #endregion 行銷滿額贈活動

            //SetSuccessContent 應該是被 presenter 呼叫, order & member 的值應該在那邊被賦予，但現實卻是 SetSuccessContent 被 view 呼叫
            //所以暫時 provider撈資料寫在這，也是不得己的                        
            hidOid.Value = order.Guid.ToString();
            Member mem = mp.MemberGet(order.UserId);
            bool isFirstBuyDiscountCode = IsFirstBuyDiscountCode(order, theDeal);

            if (isPaidByATM)
            {

            }
            else // credit card
            {
                if (mem.IsGuest == false)
                {
                    mvSuccessResult0.ActiveViewIndex = 0;
                }
                else
                {
                    mvSuccessResult0.ActiveViewIndex = 1;
                }
            }

            //哈 有點蠢蠢的味道飄散在空氣中
            if (isPaidByATM && mem.IsGuest == false && isFirstBuyDiscountCode)
            {
                //ATM+會員+首購
                mvSuccessMailHint.ActiveViewIndex = 0;
            }
            else if (isPaidByATM && mem.IsGuest == false && isFirstBuyDiscountCode == false)
            {
                //ATM+會員+非首購
                mvSuccessMailHint.ActiveViewIndex = 1;
            }
            else if (isPaidByATM && mem.IsGuest && isFirstBuyDiscountCode)
            {
                //ATM+訪客+首購
                litSuccessEmail2.Text = mem.UserEmail;
                mvSuccessMailHint.ActiveViewIndex = 2;
            }
            else if (isPaidByATM && mem.IsGuest && isFirstBuyDiscountCode == false)
            {
                //ATM+訪客+非首購
                litSuccessEmail3.Text = mem.UserEmail;
                mvSuccessMailHint.ActiveViewIndex = 3;
            }
            else if (isPaidByATM == false && mem.IsGuest == false && isFirstBuyDiscountCode)
            {
                //CreditCard+會員+首購
                mvSuccessMailHint.ActiveViewIndex = 4;
            }
            else if (isPaidByATM == false && mem.IsGuest == false && isFirstBuyDiscountCode == false)
            {
                //CreditCard+會員+非首購
                mvSuccessMailHint.ActiveViewIndex = 5;
            }
            else if (isPaidByATM == false && mem.IsGuest && isFirstBuyDiscountCode)
            {
                //CreditCard+訪客+首購
                litSuccessEmail6.Text = mem.UserEmail;
                mvSuccessMailHint.ActiveViewIndex = 6;
            }
            else if (isPaidByATM == false && mem.IsGuest && isFirstBuyDiscountCode == false)
            {
                //CreditCard+訪客+非首購
                litSuccessEmail7.Text = mem.UserEmail;
                mvSuccessMailHint.ActiveViewIndex = 7;
            }

            phGuestShowFirstBuyDiscountImage.Visible = !isPaidByATM && isFirstBuyDiscountCode && mem.IsGuest;

            if (mem.IsGuest == false)
            {
                mvSuccessButtonGroup.ActiveViewIndex = 0;
                hidBid.Value = ViewPponDealManager.DefaultManager.MainBusinessHourGuidGetByBid(theDeal.BusinessHourGuid).ToString();
                hidUserId.Value = mem.UniqueId.ToString();
            }
            else
            {
                mvSuccessButtonGroup.ActiveViewIndex = 1;
                hidAuthEmail.Value = mem.UserEmail;
            }

            #region 消費滿一定金額，送折價券活動

            List<DiscountCampaign> campaignList = PromotionFacade.GetPromotionDiscountCampaignList(DiscountCampaignUsedFlags.FreeGiftWithPurchase);
            if (campaignList.Count > 0)
            {
                foreach (var campaign in campaignList)
                {
                    if (order.Total > campaign.FreeGiftDiscount.GetValueOrDefault() &&
                        DateTime.Now >= campaign.EventDateS.GetValueOrDefault() &&
                        DateTime.Now <= campaign.EventDateE.GetValueOrDefault())
                    {
                        string codeCode;
                        int codeAmount;
                        DateTime? codeEndTime;
                        int discountCodeId;
                        int codeMinimumAmount;
                        var res = PromotionFacade.InstantGenerateDiscountCode(campaign.Id, order.UserId, true,
                            out codeCode, out codeAmount, out codeEndTime, out discountCodeId, out codeMinimumAmount);
                        if (res == InstantGenerateDiscountCampaignResult.Success)
                        {
                            divDiscountCode.Visible = true;
                        }
                    }
                }
            }

            #endregion


            #region 1111消費贈等值折價券

            try
            {
                PromotionFacade.AutoSendDiscountCode(order.UserId, order.Guid, mem.UserName);
            }
            catch (Exception ex)
            {
                logger.Error("折價券自動補發ERROR", ex);
            }


            #endregion

        }


        /// <summary>
        /// 是否符合首購送500元折價券的資格
        /// </summary>
        /// <param name="o"></param>
        /// <param name="deal"></param>
        private bool IsFirstBuyDiscountCode(Order o, IViewPponDeal deal)
        {
            if (config.FirstBuyDiscountCodeEnabled)
            {
                int minAmount = 50;
                if (((o.OrderStatus & (int)OrderStatus.FirstOrder) == (int)OrderStatus.FirstOrder)
                    && o.Subtotal > minAmount && (deal.GroupOrderStatus & (int)GroupOrderStatus.KindDeal) == 0)
                {
                    return true;
                }
            }
            return false;
        }

        private void EmptyBuilding(DropDownList ddl)
        {
            ddl.DataSource = new BuildingCollection();
            ddl.DataBind();
            ddl.Items.Insert(0, new ListItem("請選擇", "0"));
        }

        protected void ddlStoreCity_SelectedIndexChanged(object sender, EventArgs e)
        {
            int cityId = int.Parse(ddlStoreCity.SelectedValue);
            Hashtable hts = new Hashtable();
            IList<string> stores = hidMultiBranch.Value.Split('|');
            IList<string> sTemp;
            foreach (string s in stores)
            {
                if (s.IndexOf(cityId + ",") >= 0)
                {
                    sTemp = s.Split(",");
                    if (!hts.Contains(sTemp[2]))
                    {
                        hts.Add(sTemp[2], sTemp[3]);
                    }
                }
            }

            ddlStoreArea.DataSource = hts;
            ddlStoreArea.DataTextField = "Value";
            ddlStoreArea.DataValueField = "Key";
            ddlStoreArea.DataBind();
            ddlStoreArea.Items.Insert(0, new ListItem("請選擇", "0"));
            ddlStoreArea.SelectedIndex = 0;
            EmptyBuilding(ddlStore);
        }

        protected void ddlStoreArea_SelectedIndexChanged(object sender, EventArgs e)
        {
            int areaId = int.Parse(ddlStoreArea.SelectedValue);

            if (Equals(0, areaId))
            {
                EmptyBuilding(ddlStore);
            }
            else
            {
                Hashtable hts = new Hashtable();
                IList<string> stores = hidMultiBranch.Value.Split('|');
                IList<string> sTemp;
                foreach (string s in stores)
                {
                    if (s.IndexOf(areaId + ",") > 0)
                    {
                        sTemp = s.Split(",");
                        if (!hts.Contains(sTemp[4]))
                        {
                            hts.Add(sTemp[4], sTemp[5]);
                        }
                    }
                }

                ddlStore.DataSource = hts;
                ddlStore.DataTextField = "Value";
                ddlStore.DataValueField = "Key";
                ddlStore.DataBind();
                ddlStore.Items.Insert(0, new ListItem("請選擇", "0"));
                ddlStore.SelectedIndex = 0;
            }
        }

        private void ProcessBranches(ViewPponStoreCollection stores)
        {
            IList<string> branches = new List<string>();
            Hashtable branchCity = new Hashtable();
            lblStoreChioceConfirm.Visible = true;
            lblStoreChioce.Visible = true;
            foreach (ViewPponStore store in stores.OrderBy(x => x.CityId))
            {
                string phone = store.CityId + "," + store.CityName + "," + store.TownshipId + "," + store.TownshipName + "," + store.StoreGuid.ToString() + "," + store.StoreName + (string.IsNullOrEmpty(store.Phone) ? string.Empty : (" 預約專線" + store.Phone));
                if (store.TotalQuantity <= 0 || store.OrderedQuantity >= store.TotalQuantity)
                {
                    phone += "(已售完)";
                }

                branches.Add(phone);

                if (!branchCity.Contains(store.CityId))
                {
                    branchCity.Add(store.CityId, store.CityName);
                }
            }

            ddlStoreCity.DataSource = branchCity;
            ddlStoreCity.DataTextField = "Value";
            ddlStoreCity.DataValueField = "Key";
            ddlStoreCity.DataBind();
            ddlStoreCity.Items.Insert(0, new ListItem("請選擇", "0"));

            if (Equals(1, branchCity.Count))
            {
                ddlStoreCity_SelectedIndexChanged(this, EventArgs.Empty);
            }
            else
            {
                EmptyBuilding(ddlStore);
            }

            if (branches.Count > 0)
            {
                hidMultiBranch.Value = string.Join("|", branches);
            }
        }
        /// <summary>
        /// 訪客購買的登入
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnMemberLogin_Click(object sender, EventArgs e)
        {
            SetTemporaryData();
            string returnUrl = string.Format("/ppon/buy.aspx?ticketId=" + this.TicketId);
            Response.Redirect(string.Format("{0}?returnUrl={1}", FormsAuthentication.LoginUrl,
                HttpUtility.UrlEncode(returnUrl)));
        }
        /// <summary>
        /// 選擇超商的導頁
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSelectCVS_Click(object sender, EventArgs e)
        {
            var redirectUrl = ISPFacade.GetIspEmapUrl(ProductDelivery, TicketId,
                hidSevenPickupStoreId.Value);
            if (string.IsNullOrEmpty(redirectUrl)) return;

            SetTemporaryData();
            this.BackupTicketIdForHistoryBack = this.TicketId;
            Response.Redirect(redirectUrl);
        }

        private void SetTemporaryData()
        {
            ThirdPartyPaymentSystem = ThirdPartyPayment.None;
            BuyTransPaymentMethod = BuyTransPaymentMethod.Init;
            int pendingAmount = int.Parse(hidtotal.Value);

            if (pendingAmount == 0)
            {
                BuyTransPaymentMethod = BuyTransPaymentMethod.BonusOrScashPayOff;
            }
            else if (rbPayWayByCreditCard.Checked || rbInstallment3Months.Checked || rbInstallment6Months.Checked || rbInstallment12Months.Checked)
            {
                if (rbInstallment3Months.Checked || rbInstallment6Months.Checked || rbInstallment12Months.Checked)
                {
                    BuyTransPaymentMethod = BuyTransPaymentMethod.CreaditCardInstallment;
                }
                else
                {
                    BuyTransPaymentMethod = BuyTransPaymentMethod.CreaditCardPayOff;
                }
            }
            else if (rbPayWayByAtm.Checked)
            {
                BuyTransPaymentMethod = BuyTransPaymentMethod.Atm;
            }
            else if (rbPayWayByMasterPass.Checked)
            {
                BuyTransPaymentMethod = BuyTransPaymentMethod.MasterPass;
            }
            else if (rbPayWayByLinePay.Checked)
            {
                ThirdPartyPaymentSystem = ThirdPartyPayment.LinePay;
                BuyTransPaymentMethod = BuyTransPaymentMethod.LinePay;
            }
            else if (rbPayWayTaishinPay.Checked)
            {
                ThirdPartyPaymentSystem = ThirdPartyPayment.TaishinPay;
                BuyTransPaymentMethod = BuyTransPaymentMethod.TaishinPay;
            }
            else if (rbPayWayApplePay.Checked)
            {
                BuyTransPaymentMethod = BuyTransPaymentMethod.ApplePay;
            }
            else if (rbPayWayFamilyIsp.Checked)
            {
                BuyTransPaymentMethod = BuyTransPaymentMethod.FamilyIsp;
            }
            else if (rbPayWaySevenIsp.Checked)
            {
                BuyTransPaymentMethod = BuyTransPaymentMethod.SevenIsp;
            }

            PponDeliveryInfo deliveryInfo = DeliveryInfo;

            PponPaymentDTO paymentDTO;

            paymentDTO = PponBuyFacade.GetPponPaymentDTOFromSession(this.TicketId);
            if (paymentDTO == null)
            {
                string errMsg = string.Format("ticket: {0} can't get data.", TicketId);
                logger.Warn(errMsg);
                throw new Exception(errMsg);
            }

            paymentDTO.State = PponPaymentDTOState.Loaded;

            paymentDTO.DeliveryInfo = deliveryInfo;
            paymentDTO.IsSaveCreditCardInfo = this.IsSaveCreditCardInfo;
            paymentDTO.BuyerAddressInfo = this.AddressInfo;
            paymentDTO.IsThirdPartyPay = this.ThirdPartyPaymentSystem != ThirdPartyPayment.None;
            paymentDTO.ThirdPartyPaymentSystem = this.ThirdPartyPaymentSystem;
            paymentDTO.ItemOptions = this.UserMemo;
            paymentDTO.ItemOptionsJson = this.ItemOptionsJson;
            paymentDTO.BuyerInfoJson = this.BuyerInfoJson;
            paymentDTO.OtherInfoJson = this.OtherInfoJson;
            Guid tempGuid;
            if (Guid.TryParse(this.hdPrebuiltOrderGuid.Value, out tempGuid))
            {
                paymentDTO.PrebuiltOrderGuid = tempGuid;
            }

            PponBuyFacade.SetPponPaymentDTOToSession(paymentDTO.TicketId, paymentDTO);
        }

        protected void btnPayWay_Click(object sender, EventArgs e)
        {
            string ticketId = OrderFacade.MakeRegularTicketId();

            ThirdPartyPaymentSystem = ThirdPartyPayment.None;
            BuyTransPaymentMethod = BuyTransPaymentMethod.Init;
            int pendingAmount = int.Parse(hidtotal.Value);

            if (pendingAmount == 0)
            {
                BuyTransPaymentMethod = BuyTransPaymentMethod.BonusOrScashPayOff;
            }
            else if (rbPayWayByCreditCard.Checked || rbInstallment3Months.Checked || rbInstallment6Months.Checked || rbInstallment12Months.Checked)
            {
                if (rbInstallment3Months.Checked || rbInstallment6Months.Checked || rbInstallment12Months.Checked)
                {
                    BuyTransPaymentMethod = BuyTransPaymentMethod.CreaditCardInstallment;
                }
                else
                {
                    BuyTransPaymentMethod = BuyTransPaymentMethod.CreaditCardPayOff;
                }
            }
            else if (rbPayWayByAtm.Checked)
            {
                BuyTransPaymentMethod = BuyTransPaymentMethod.Atm;
            }
            else if (rbPayWayByMasterPass.Checked)
            {
                BuyTransPaymentMethod = BuyTransPaymentMethod.MasterPass;
            }
            else if (rbPayWayByLinePay.Checked)
            {
                ThirdPartyPaymentSystem = ThirdPartyPayment.LinePay;
                BuyTransPaymentMethod = BuyTransPaymentMethod.LinePay;
            }
            else if (rbPayWayTaishinPay.Checked)
            {
                ThirdPartyPaymentSystem = ThirdPartyPayment.TaishinPay;
                BuyTransPaymentMethod = BuyTransPaymentMethod.TaishinPay;
            }
            else if (rbPayWayApplePay.Checked)
            {
                BuyTransPaymentMethod = BuyTransPaymentMethod.ApplePay;
            }
            else if (rbPayWayFamilyIsp.Checked)
            {
                BuyTransPaymentMethod = BuyTransPaymentMethod.FamilyIsp;
            }
            else if (rbPayWaySevenIsp.Checked)
            {
                BuyTransPaymentMethod = BuyTransPaymentMethod.SevenIsp;
            }

            Context.Items[LkSiteContextItem.BuyTransaction] =
                new BuyTransactionModel(BuyTransactionGuid, BuyTransPaymentMethod, BuyTransPaymentSection.BuySubmitOrder);
            ibCheckOut_Click(sender, new DataEventArgs<string>(ticketId));
        }

        /// <summary>
        /// 宅配商品檔URL
        /// </summary>
        /// <param name="bid"></param>
        /// <returns></returns>
        protected string GetDealSpecificationPageUrl(Guid bid)
        {
            if (bid != Guid.Empty)
            {
                return ResolveUrl(string.Format("~/{0}/{1}#DealDetail", PponCityGroup.DefaultPponCityGroup.AllCountry.CityId, ViewPponDealManager.DefaultManager.MainBusinessHourGuidGetByBid(bid)));
            }
            //無效Bid，取消連結顯示
            ph_DealInfoUrl.Visible = false;
            return string.Empty;
        }

        protected void RandomBuyAdInit(object sender, EventArgs e)
        {
            buyadbanner.CityId = -1;//不分區
            buyadbanner.Type = RandomCmsType.PponBuyAd;
        }

        /// <summary>
        /// 訪客會員的UserId，啟用訪客購買才會有值
        /// 
        /// 進入 MakeOrder/MakePayment 的狀態時，就不能從這個參數取值，而要改由 PponDeliveryInfo
        /// </summary>
        public int GuestMemberId { get; set; }

        /// <summary>
        /// 取得年份下拉選單
        /// 從當年度開始算起15年
        /// </summary>
        /// <param name="ddlYears"></param>
        /// <returns></returns>
        private DropDownList GenerateDropDownListYearItems(DropDownList ddlYears)
        {
            ddlYears.Items.Clear();
            ddlYears.Items.Add(new ListItem { Text = "--", Value = "" });
            for (int i = 0; i < 20; i++)
            {
                string shortYear = (DateTime.Now.AddYears(i).Year % 100).ToString();
                ddlYears.Items.Add(new ListItem { Text = shortYear, Value = shortYear });
            }
            return ddlYears;
        }

        public void RedirectToOTPPage(string url)
        {
            Response.Redirect(url);
        }

        private static List<City> GetCities(int? parentId)
        {
            List<City> cityList;
            if (parentId == null)
            {
                cityList = CityManager.Citys.Where(x => x.Code != "SYS").ToList();
            }
            else
            {
                cityList = CityManager.TownShipGetListByCityId(parentId.Value);
            }

            return cityList;
        }



        /// <summary>
        /// 儲值/更新購物金
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        [WebMethod]
        public static string DepositScash(string userName, string memNum, string deductContent, string merchantContent, string returnCode)
        {
            try
            {
                string logMsg = Newtonsoft.Json.JsonConvert.SerializeObject(
                new { userName, memNum, deductContent, merchantContent, returnCode }, Newtonsoft.Json.Formatting.Indented);
                logger.Info(logMsg);


                int userId = MemberFacade.GetUniqueId(userName);
                DeductContent deduct = Newtonsoft.Json.JsonConvert.DeserializeObject<DeductContent>(PayeasyFacade.DecryptASE2(deductContent));
                MerchantContent merchant = Newtonsoft.Json.JsonConvert.DeserializeObject<MerchantContent>(PayeasyFacade.DecryptASE2(merchantContent));

                #region 儲值至本站
                var pxOrder = OrderFacade.MakeNewPscashOrder(merchant.MerchantDealCode, userId);
                OrderFacade.ExchangePcashToPscash(pxOrder.Guid, "pez:" + deduct.PezAuthCode, deduct.DeductPoint, userId);
                #endregion


                //更新購物金
                decimal sumSacsh = OrderFacade.GetSCashSum(userId) + OrderFacade.GetPscashBalanceSum(userId);

                return new JsonSerializer().Serialize(new { IsSuccess = true, SumSacsh = sumSacsh, NewMerchantContent = PayeasyFacade.GetMerchantContent() });
            }
            catch (Exception ex)
            {
                logger.Error("儲值pez購物金錯誤", ex);
                return new JsonSerializer().Serialize(new { IsSuccess = false, Message = ex.Message });
            }


        }

        /// <summary>
        /// 綁定轉換Pcash帳戶
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="memNum">pez加密的顧客流水號</param>
        [WebMethod]
        public static string BindingPezMember(string userName, string memNum)
        {
            try
            {
                int userId = MemberFacade.GetUniqueId(userName);
                PcashMemberLink oldLink = mp.PcashMemberLinkGet(userId);
                if (oldLink.IsLoaded)
                {
                    //已經綁了
                    logger.Error("重複綁定pez購物金帳戶");
                }
                else
                {
                    PcashMemberLink link = new PcashMemberLink();
                    link.UserId = userId;
                    link.ExternalUserId = memNum;
                    link.CreateTime = DateTime.Now;
                    mp.PCashMemberLinkSet(link);
                }
                return new JsonSerializer().Serialize(new { IsSuccess = true });
            }
            catch (Exception ex)
            {
                logger.Error("綁定pez購物金帳戶錯誤", ex);
                return new JsonSerializer().Serialize(new { IsSuccess = false, Message = ex.Message });
            }


        }

        /// <summary>
        /// 儲值/更新購物金錯誤紀錄
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        [WebMethod]
        public static string DepositErrorLog(string userName, string returnCode)
        {
            try
            {
                string logMsg = Newtonsoft.Json.JsonConvert.SerializeObject(
                new { userName, returnCode}, Newtonsoft.Json.Formatting.Indented);
                logger.Error("DepositErrorLog:" + logMsg);

                return new JsonSerializer().Serialize(new { IsSuccess = true });
            }
            catch (Exception ex)
            {
                logger.Error("DepositErrorLog", ex);
                return new JsonSerializer().Serialize(new { IsSuccess = false, Message = ex.Message });
            }


        }

        [WebMethod]
        public static bool IsValidPhoneCarrier(string carrid)
        {
            try
            {
                return MemberFacade.IsValidPhoneCarrierId(carrid);
            }
            catch
            {
                return false;
            }
        }

    }

}


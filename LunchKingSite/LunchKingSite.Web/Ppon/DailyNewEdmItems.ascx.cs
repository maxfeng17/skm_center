﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Models;
using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace LunchKingSite.Web.Ppon
{
    public partial class DailyNewEdmItems : System.Web.UI.UserControl
    {
        public List<EdmDetail> EdmDetailList { get; set; }

        public EdmDetail MainDetail { get; set; }

        public EdmDetailType MainType { get; set; }

        public EdmDetailType ItemType { get; set; }

        public bool IsFirst { get; set; }

        public bool IsTravelEdmSpecialType { get; set; }

        public bool IsPponPiinLife { get; set; }

        public string TitleColor { get; set; }

        public string SiteUrl { get; set; }

        public string Cpa { get; set; }

        public int ColumnNumber { get; set; }

        public string CityName { get; set; }

        public int CityId { get; set; }

        private static IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        public override void DataBind()
        {
            if (MainDetail.Id != 0 && !IsPponPiinLife)
            {
                lab_AreaName.Text = CityName = MainDetail.CityName;
                pan_Title.Visible = MainDetail.DisplayCityName;
                if (MainType == EdmDetailType.MainDeal_1 && MainDetail.DisplayCityName)
                {
                    //lit_Separate.Text = "<br /><hr style='border: none; border-top: 1px solid #DDD;' /><br />";
                    lit_Separate.Text = "<br />";
                }

                if (MainDetail.DisplayCityName && IsFirst)
                {
                    pan_Title.Visible = true;
                }
                else
                {
                    pan_Title.Visible = false;
                }
            }

            List<IViewPponDeal> vpdc = ViewPponDealManager.DefaultManager.ViewPponDealGetListByGuidList(EdmDetailList.Where(x => x.Bid.HasValue).Select(x => x.Bid.Value).ToList());
            foreach (var item in EdmDetailList.Where(x => x.Bid.HasValue && !string.IsNullOrEmpty(x.Title)))
            {
                IViewPponDeal deal;
                if ((deal = vpdc.First(x => x.BusinessHourGuid == item.Bid.Value)) != null)
                {
                    deal.EventName = item.Title;
                }
            }
            int currenttotal = vpdc.Count;
            if (currenttotal > 0)
            {
                for (int i = 0; i < ColumnNumber - currenttotal; i++)
                {
                    vpdc.Add(new ViewPponDeal() { EventName = string.Empty, EventImagePath = string.Empty, ItemOrigPrice = 0, ItemPrice = 0 });
                }

                if (IsPponPiinLife)
                {
                    pan_PponPiinLife_1.Visible = true;
                    rpt_PponPiinLife_1.DataSource = vpdc;
                    rpt_PponPiinLife_1.DataBind();
                }
                else if (MainDetail.Type == (int)EdmDetailType.PponPiinLife_1)
                {
                    pan_PiinLife_1.Visible = true;
                    rpt_PiinLife_1.DataSource = vpdc;
                    rpt_PiinLife_1.DataBind();
                }
                else
                {
                    if (ColumnNumber == 1 && !IsTravelEdmSpecialType)
                    {
                        pan_Single.Visible = true;
                        rpt_Single.DataSource = vpdc;
                        rpt_Single.DataBind();
                    }

                    if (ColumnNumber == 2)
                    {
                        pan_Double.Visible = true;
                        rpt_Double.DataSource = vpdc;
                        rpt_Double.DataBind();
                    }

                    if (ColumnNumber == 3)
                    {
                        pan_Triple.Visible = true;
                        rpt_Triple.DataSource = vpdc;
                        rpt_Triple.DataBind();
                    }

                    if (IsTravelEdmSpecialType)
                    {
                        pan_SingleTravel.Visible = true;
                        rpt_SingleTravel.DataSource = vpdc;
                        rpt_SingleTravel.DataBind();
                    }
                }
            }
        }

        protected void rptDataBound_PponPiinlife(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is ViewPponDeal)
            {
                Image cimg_Pic = (Image)e.Item.FindControl("img_Pic");

                ViewPponDeal deal = (ViewPponDeal)e.Item.DataItem;
                string[] images_array = ImageFacade.GetMediaPathsFromRawData(deal.EventImagePath, MediaType.PponDealPhoto);
                //後台上檔預設 第一張輪播大圖檔名為 bid 置換"-"成"EDM"
                string default_edm_image_name = deal.BusinessHourGuid.ToString().Replace("-", "EDM");
                if (images_array.Any(x => x.Contains(default_edm_image_name)))
                {
                    cimg_Pic.ImageUrl = images_array.DefaultIfEmpty(SiteUrl + "/Themes/PCweb/images/ppon-M1_pic.jpg").First(x => x.Contains(default_edm_image_name));
                }
                else
                {
                    cimg_Pic.ImageUrl = ImageFacade.GetMediaPathsFromRawData(deal.EventImagePath, MediaType.PponDealPhoto).DefaultIfEmpty(SiteUrl + "/Themes/PCweb/images/ppon-M1_pic.jpg").First();
                }
            }
        }

        protected void rptDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is ViewPponDeal)
            {
                Image cimg_Pic = (Image)e.Item.FindControl("img_Pic");
                Label clab_Discount = (Label)e.Item.FindControl("lab_Discount");
                Literal clab_IconTage = (Literal)e.Item.FindControl("lit_IconTags");
                ViewPponDeal deal = (ViewPponDeal)e.Item.DataItem;
                string[] images_array = ImageFacade.GetMediaPathsFromRawData(deal.EventImagePath, MediaType.PponDealPhoto);
                //後台上檔預設 第一張輪播大圖檔名為 bid 置換"-"成"EDM"
                string default_edm_image_name = deal.BusinessHourGuid.ToString().Replace("-", "EDM");
                if (images_array.Any(x => x.Contains(default_edm_image_name)))
                {
                    cimg_Pic.ImageUrl = images_array.DefaultIfEmpty(SiteUrl + "/Themes/PCweb/images/ppon-M1_pic.jpg").First(x => x.Contains(default_edm_image_name));
                }
                else
                {
                    cimg_Pic.ImageUrl = ImageFacade.GetMediaPathsFromRawData(deal.EventImagePath, MediaType.PponDealPhoto).DefaultIfEmpty(SiteUrl + "/Themes/PCweb/images/ppon-M1_pic.jpg").First();
                }

                //TraveleDMSpecialBigPic
                if (IsTravelEdmSpecialType)
                {
                    //PponDeal ImageFacade.GetMediaPathsFromRawData(deal.DealContent.TravelEdmSpecialImagePath, MediaType.PponDealPhoto)
                    //cimg_Pic.ImageUrl = ImageFacade.GetMediaPathsFromRawData(deal.EventImagePath, MediaType.PponDealPhoto).DefaultIfEmpty("../Themes/PCweb/images/ppon-M1_pic.jpg").First();
                    PponDeal pponDeal = pp.PponDealGet(deal.BusinessHourGuid);
                    cimg_Pic.ImageUrl = ImageFacade.GetMediaPathsFromRawData(pponDeal.DealContent.TravelEdmSpecialImagePath, MediaType.PponDealPhoto).DefaultIfEmpty(SiteUrl + "/Themes/PCweb/images/ppon-M1_pic.jpg").First();
                }


                if (deal.ItemOrigPrice != 0)
                {
                    if (Helper.IsFlagSet(deal.BusinessHourStatus, BusinessHourStatus.NoDiscountShown) || deal.ItemPrice == deal.ItemOrigPrice)
                    {
                        clab_Discount.Text = "特選";
                    }
                    else if (deal.ItemPrice > 0)
                    {
                        string rateStr = (deal.ItemPrice * 10 / deal.ItemOrigPrice).FractionFloor(1).ToString("N1");
                        clab_Discount.Text = rateStr + " 折" + ((deal.BusinessHourStatus & (int)BusinessHourStatus.ComboDealMain) > 0 ? "起" : string.Empty);
                    }
                    else
                    {
                        clab_Discount.Text = "優惠";
                    }
                }
                else
                {
                    clab_Discount.Text = "優惠";
                }

                //iconHtmlTag
                if (clab_IconTage != null)
                {
                    clab_IconTage.Text = GetDealIconHtmlContent(deal);
                }

            }
        }

        protected void rptPiinLifeDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is HiDealDeal)
            {
                Image cimg_Pic = (Image)e.Item.FindControl("img_Pic");
                HiDealDeal deal = (HiDealDeal)e.Item.DataItem;
                cimg_Pic.ImageUrl = ImageFacade.GetHiDealPhoto(deal, MediaType.HiDealSecondaryBigPhoto, true);
            }
        }

        protected string GetTitleTdColor()
        {
            return string.Format("color: #{0}; font-size: 24px; font-weight: bold; border-bottom: 1px solid #{0};width: 740px; height: 40px", TitleColor);
        }

        protected string GetDiscountBorderColor()
        {
            if (ColumnNumber == 1)
            {
                return string.Format("border: 1px solid #{0}; color: #{0}; font-size: 14px;font-weight: bold; width: 70px; height: 38px;", TitleColor);
            }
            else if (ColumnNumber == 2)
            {
                return string.Format("border: 1px solid #{0}; color: #{0}; font-size: 14px;font-weight: bold; width: 70px; height: 30px;line-height:0px", TitleColor);
            }
            else
            {
                return string.Format("border: 1px solid #{0}; color: #{0}; font-size: 14px;font-weight: bold; width: 65px; height: 22px;", TitleColor);
            }
        }

        protected string GetBuyTdColor()
        {
            if (ColumnNumber == 1)
            {
                return string.Format("border-radius: 5px; background: #{0}; padding-top:8px; padding-bottom:8px;", TitleColor);
            }
            else
            {
                return string.Format("border-radius: 5px; background: #{0}; width: 70px;", TitleColor);
            }
        }

        private string GetDealIconHtmlContent(ViewPponDeal vpd)
        {
            string dealIconHtmlContent = string.Empty;
            //  顯示Icon上限        目前Icon計數
            int dealIconsLimit = 3, dealIconCount = 0;

            //排除公益檔次的破千破萬 Icon
            if (!Helper.IsFlagSet(vpd.GroupOrderStatus ?? 0, GroupOrderStatus.KindDeal))
            {
                QuantityAdjustment adj = vpd.GetAdjustedOrderedQuantity();
                //破千熱銷 Icon
                if (adj.Quantity >= 1000)
                {
                    if (ColumnNumber == 3)
                    {
                        dealIconHtmlContent = "<td><img src=\"" + SiteUrl + "/Themes/default/images/17Life/EDM/Tag_EDM_01Square-sml.png\" width=\"58\" height=\"24\" alt=\"破千熱銷\" /></td>";
                    }
                    else
                    {
                        dealIconHtmlContent = "<td width=\"69\"><img src=\"" + SiteUrl + "/Themes/default/images/17Life/EDM/Tag_EDM_01Square.png\" width=\"69\" height=\"29\" alt=\"破千熱銷\" /></td>";
                    }

                    dealIconCount++;
                }
            }

            if (!string.IsNullOrEmpty(vpd.LabelIconList))
            {
                List<int> iconCodeIdList = vpd.LabelIconList.Split(',').Select(int.Parse).ToList();
                foreach (int IconCodeId in iconCodeIdList)
                {
                    if (ColumnNumber == 3)
                    {
                        #region sml.png

                        switch ((DealLabelSystemCode)Enum.ToObject(typeof(DealLabelSystemCode), IconCodeId))
                        {
                            case DealLabelSystemCode.ArriveIn24Hrs:
                                dealIconHtmlContent += "<td><img src=\"" + SiteUrl +
                                                       "/Themes/default/images/17Life/EDM/Tag_EDM_03Square-sml.png\" width=\"58\" height=\"24\" alt=\"24h出貨\" /></td>";
                                break;
                            case DealLabelSystemCode.ArriveIn72Hrs:
                                //dealIconHtmlContent += "<td><img src=\"" + SiteUrl +
                                //                       "/Themes/default/images/17Life/EDM/Tag_EDM_03-2Square-sml.png\" width=\"58\" height=\"24\" alt=\"24h出貨\" /></td>";
                                break;
                            case DealLabelSystemCode.CanBeUsedImmediately:
                                dealIconHtmlContent += "<td><img src=\"" + SiteUrl +
                                                       "/Themes/default/images/17Life/EDM/Tag_EDM_02Square-sml.png\" width=\"58\" height=\"24\" alt=\"即買即用\" /></td>";
                                break;
                            case DealLabelSystemCode.TwentyFourHoursArrival:
                                dealIconHtmlContent += "<td width=\"\"><ul style=\"padding:0;margin:0;width:60px\"><li style=\"width: auto;height: 16px;line-height: 16px;text-shadow: none;margin: 0px 0 0 0px;padding: 2px 5px 2px 5px;border-radius: 0;color: #8B81C3;font-size: 11px;font-family: Din Alternate,Droid sans-serif,Ping FangSong TC,微軟正黑體;background-color: #ED3F62;color: #ffffff;\">24H到貨</li></ul></td>";
                                break;
                            case DealLabelSystemCode.FiveStarHotel:
                            case DealLabelSystemCode.FourStarHotel:
                            default:
                                break;
                        }

                        #endregion sml.jpg
                    }
                    else
                    {
                        #region big.png

                        switch ((DealLabelSystemCode)Enum.ToObject(typeof(DealLabelSystemCode), IconCodeId))
                        {
                            case DealLabelSystemCode.ArriveIn24Hrs:
                                dealIconHtmlContent += "<td width=\"69\"><img src=\"" + SiteUrl +
                                                       "/Themes/default/images/17Life/EDM/Tag_EDM_03Square.png\" width=\"69\" height=\"29\" alt=\"24h到貨\" /></td>";
                                break;
                            case DealLabelSystemCode.ArriveIn72Hrs:
                                //dealIconHtmlContent += "<td width=\"69\"><img src=\"" + SiteUrl +
                                //                       "/Themes/default/images/17Life/EDM/Tag_EDM_03-2Square.png\" width=\"69\" height=\"29\" alt=\"72h到貨\" /></td>";
                                break;
                            case DealLabelSystemCode.CanBeUsedImmediately:
                                dealIconHtmlContent += "<td width=\"69\"><img src=\"" + SiteUrl +
                                                       "/Themes/default/images/17Life/EDM/Tag_EDM_02Square.png\" width=\"69\" height=\"29\" alt=\"即買即用\" /></td>";
                                break;

                            case DealLabelSystemCode.TwentyFourHoursArrival:
                                dealIconHtmlContent += "<td width=\"\"><ul style=\"padding:0;margin:0;width:60px\"><li style=\"width: auto;height: 16px;line-height: 16px;text-shadow: none;margin: 0px 0 0 0px;padding: 2px 5px 2px 5px;border-radius: 0;color: #8B81C3;font-size: 11px;font-family: Din Alternate,Droid sans-serif,Ping FangSong TC,微軟正黑體;background-color: #ED3F62;color: #ffffff;\">24H到貨</li></ul></td>";
                                break;
                            case DealLabelSystemCode.FiveStarHotel:
                            case DealLabelSystemCode.FourStarHotel:
                            default:
                                break;

                        }

                        #endregion big.png
                    }

                    dealIconCount++;
                    if (dealIconCount >= dealIconsLimit)
                    {
                        break;
                    }
                }
            }

            return dealIconHtmlContent;
        }
    }
}
﻿<%@ Page Language="C#" MasterPageFile="~/Ppon/Ppon.Master" AutoEventWireup="true" CodeBehind="Unsubscription.aspx.cs" Inherits="LunchKingSite.Web.Ppon.Unsubscription" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="MC" runat="server">
    <div class="center">
        <div class="EDMun-full-block">
            <div class="EDMun-content">
                <%--EDMUnsubscribeArea Top--%>
                <asp:Panel ID="unsubscribeArea" class="EDMunsubscribeArea" runat="server">
                    <div class="Emailtitle">
                        <p>您確定要取消訂閱 <span class="EmailCity">
                            <asp:Literal ID="ltlCityArea" runat="server"></asp:Literal></span> 的好康嗎?</p>
                        <p class="Emailtitle2">您能夠忍受三折超殺優惠被別人搶光的遺憾嗎?</p>
                    </div>
                    <div class="unsubscribeBTN">
                        <div class="data-input">
                            <asp:Literal ID="ltlUrl" runat="server"></asp:Literal>
                            <asp:Button ID="unsubscribe" runat="server" Text="取消訂閱" CssClass="btn btn-large" OnClick="unsubscribe_Click" />
                        </div>
                    </div>
                </asp:Panel>
                <%--EDMUnsubscribeArea Bottom--%>
                <%--EDMCancelArea Top--%>
                <asp:Panel ID="unsubscribeOK" class="unsubscribeOK" runat="server">
                    <div class="Emailtitle">
                        <p>您已成功取消訂閱 <span class="EmailCity">
                            <asp:Literal ID="ltlCity" runat="server"></asp:Literal></span> 的好康!</p>
                    </div>
                    <div class="unsubscribeBTNOK">
                        <div class="data-input">
                            <asp:Literal ID="ltlUnSUrl" runat="server"></asp:Literal>
                        </div>
                    </div>
                </asp:Panel>
                <%--EDMCancelArea Bottom--%>
            </div>
            <!--content-->
        </div>
        <!--full-block-->

    </div>
</asp:Content>

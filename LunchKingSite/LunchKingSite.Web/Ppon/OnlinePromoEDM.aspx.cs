﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LunchKingSite.Web.Ppon
{
    public partial class OnlinePromoEDM : System.Web.UI.Page
    {
        #region property
        private ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        public int EdmID
        {
            get
            {
                int eid;
                if (!string.IsNullOrEmpty(Request.QueryString["eid"]) && int.TryParse(Request.QueryString["eid"], out eid))
                {
                    return eid;
                }
                else
                {
                    return 0;
                }
            }
        }

        public string Pass
        {
            get
            {
                if (!string.IsNullOrEmpty(Request.QueryString["pa"]))
                {
                    return Request.QueryString["pa"];
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        private string cpa;

        public string Cpa
        {
            get
            {
                if (!string.IsNullOrEmpty(Request.QueryString["cpa"]))
                {
                    return Request.QueryString["cpa"];
                }
                else
                {
                    return cpa;
                }
            }
            set
            {
                cpa = value;
            }
        }

        public string Mode
        {
            get
            {
                if (!string.IsNullOrEmpty(Request.QueryString["mode"]))
                {
                    if (Request.QueryString["mode"] == "17life")
                    {
                        return "17life";
                    }
                    else
                    {
                        return "pez";
                    }
                }
                else
                {
                    return "pez";
                }
            }
        }

        public string SiteUrl
        {
            get
            {
                return config.SiteUrl;
            }
        }

        public DateTime DeliveryDate { get; set; }

        private EasyDigitCipher cipher = new EasyDigitCipher();

        #endregion property

        #region page

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string pass = string.Empty;
                EdmMain main = EmailFacade.GetNewEdmMainById(EdmID);
                List<EdmDetail> items = EmailFacade.GetNewEdmDetailsByPid(main.Id);
                if (main.Id != 0 && items.Count > 0)
                {
                    bool check = true;

                    #region check date

                    if ((main.Type == (int)EdmMainType.Daily && main.DeliveryDate.AddHours(-1) > DateTime.Now) || (main.Type == (int)EdmMainType.Manual && main.DeliveryDate > DateTime.Now) || !main.Status)
                    {
                        //防止外人透過id抓取時間未到的EDM
                        if (!string.IsNullOrEmpty(Pass) && cipher.Decrypt(Pass, out pass) && pass == main.DeliveryDate.ToString("yyyyddMM"))
                        {
                            check = true;
                        }
                        else
                        {
                            //內部人員可依權限進入
                            if (!User.Identity.IsAuthenticated)
                            {
                                FormsAuthentication.RedirectToLoginPage();
                                return;
                            }
                            else if (!User.IsInRole("Administrator") && !User.IsInRole("Planning") && !User.IsInRole("Marketing"))
                            {
                                check = false;
                            }
                        }
                    }

                    #endregion check date

                    if (check)
                    {
                        InitPageWithContent(main, items);
                    }
                    else
                    {
                        Response.Redirect("default.aspx");
                    }
                }
                else
                {
                    Response.Redirect("default.aspx");
                }
            }
            Response.ContentEncoding = Encoding.GetEncoding("big5");
        }

        #endregion page

        #region method

        private void InitPageWithContent(EdmMain main, List<EdmDetail> items)
        {
            this.Page.Title = Helper.GetMaxString(main.Subject, 40, "...");
            lab_CityName.Text = main.CityName;
            DeliveryDate = main.DeliveryDate;

            EdmDetail detail;

            #region AD

            if (items.Any(x => x.Type == (int)EdmDetailType.AD && x.AdId.HasValue))
            {
                detail = items.First(x => x.Type == (int)EdmDetailType.AD);
                string html_content = HttpUtility.HtmlDecode(EmailFacade.GetNewEdmAdById(detail.AdId.Value).Body);
                if (string.IsNullOrEmpty(html_content))
                {
                    pan_AD.Visible = false;
                }
                else
                {
                    lit_AD.Text = html_content;
                }
            }
            else
            {
                pan_AD.Visible = false;
            }

            #endregion AD

            #region PEZ

            if (items.Any(x => x.Type == (int)EdmDetailType.PEZ) && Mode == "pez")
            {
                detail = items.First(x => x.Type == (int)EdmDetailType.PEZ);
                CmsContent article = EmailFacade.GetPEZCmsContent("/edm/promote_pez");
                lit_PEZ.Text = article.Body;
            }
            else
            {
                pan_PEZ.Visible = false;
            }

            #endregion PEZ

            if (string.IsNullOrEmpty(Cpa))
            {
                Cpa = main.Cpa;
            }
            //依區塊種類設定相關資料
            #region

            if (items.Any(x => x.Type == (int)EdmDetailType.MainDeal_1) && items.Any(x => x.Type == (int)EdmDetailType.MainDeal_1_Items))
            {
                detail = items.First(x => x.Type == (int)EdmDetailType.MainDeal_1);
                AddEdmDetailTable(items.Where(x => x.Type == (int)EdmDetailType.MainDeal_1_Items).ToList(), detail, EdmDetailType.MainDeal_1, EdmDetailType.MainDeal_1_Items, pan_MainDeal1, 1, 1);
            }

            if (items.Any(x => x.Type == (int)EdmDetailType.PiinLife_1) && items.Any(x => x.Type == (int)EdmDetailType.PiinLife_Items_1))
            {
                detail = items.First(x => x.Type == (int)EdmDetailType.PiinLife_1);
                AddEdmDetailTable(items.Where(x => x.Type == (int)EdmDetailType.PiinLife_Items_1).ToList(), detail, EdmDetailType.PiinLife_1, EdmDetailType.PiinLife_Items_1, pan_PiinLife1, 1, 1);
            }

            List<EdmDetail> area_items = new List<EdmDetail>();
            if (items.Any(x => x.Type == (int)EdmDetailType.MainDeal_2) && Mode == "17life")
            {
                detail = items.First(x => x.Type == (int)EdmDetailType.MainDeal_2);
                if (main.CityId == PponCityGroup.DefaultPponCityGroup.PBeautyLocation.CityId || main.CityId == PponCityGroup.DefaultPponCityGroup.Travel.CityId || main.CityId == PponCityGroup.DefaultPponCityGroup.AllCountry.CityId)
                {
                    area_items.AddRange(items.Where(x => x.Type == (int)EdmDetailType.MainDeal_2_Items).Take(2));
                    area_items.AddRange(items.Where(x => x.Type == (int)EdmDetailType.Area_1_Items).Take(1));
                    area_items.AddRange(items.Where(x => x.Type == (int)EdmDetailType.Area_2_Items).Take(1));
                }
                else
                {
                    area_items.AddRange(items.Where(x => x.Type == (int)EdmDetailType.MainDeal_2_Items).Take(1));
                    area_items.AddRange(items.Where(x => x.Type == (int)EdmDetailType.Area_1_Items).Take(1));
                    area_items.AddRange(items.Where(x => x.Type == (int)EdmDetailType.Area_2_Items).Take(1));
                    area_items.AddRange(items.Where(x => x.Type == (int)EdmDetailType.Area_3_Items).Take(1));
                }
                AddEdmDetailTable(area_items, detail, EdmDetailType.MainDeal_2, EdmDetailType.MainDeal_2_Items, pan_Area1, 2, 2);
            }
            else
            {
                pan_Area.Visible = false;
            }

            #endregion method
        }

        private void AddEdmDetailTable(List<EdmDetail> items, EdmDetail detail, EdmDetailType maintype, EdmDetailType itemtype, Panel pan, int column, int row)
        {
            string titlecolor = string.Empty;
            if (items.Count() > 0)
            {
                detail.DisplayCityName = false;
                titlecolor = "BF0000";
                for (int i = 0; i < row; i++)
                {
                    DailyEdmItems table = (DailyEdmItems)Page.LoadControl("DailyEdmItems.ascx");
                    table.EdmDetailList = items.Skip(i * column).Take(column).ToList();
                    table.MainType = maintype;
                    table.ItemType = itemtype;
                    table.MainDetail = detail;
                    table.TitleColor = titlecolor;
                    table.SiteUrl = SiteUrl;
                    table.IsFirst = (i == 0);
                    table.Cpa = string.IsNullOrEmpty(Cpa) ? string.Empty : ("/" + Cpa);
                    table.ColumnNumber = column;
                    pan.Controls.Add(table);
                    table.DataBind();
                }
            }
        }

        #endregion
    }
}
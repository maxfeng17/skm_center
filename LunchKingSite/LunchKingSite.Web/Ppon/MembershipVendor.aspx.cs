﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Enumeration;
using LunchKingSite.Core.Interface;

namespace LunchKingSite.Web.NewMember
{
    public partial class MembershipVendor : System.Web.UI.Page
    {
        public List<string> VendorNames;
        private static readonly IPCPProvider _pcp = ProviderFactory.Instance().GetProvider<IPCPProvider>();

        protected void Page_Load(object sender, EventArgs e)
        {
            //撈取上線熟客卡廠商名稱

            VendorNames=_pcp.ViewMembershipCarOnlineVendorGet();


        }
    }
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Ppon/Ppon.Master" AutoEventWireup="true" CodeBehind="ContactUs.aspx.cs" Inherits="LunchKingSite.Web.Ppon.ContactUs" %>

<%@ Import Namespace="LunchKingSite.WebLib" %>
<%@ Import Namespace="LunchKingSite.BizLogic.Component" %>
<%@ Import Namespace="LunchKingSite.BizLogic.Facade" %>
<%@ Import Namespace="LunchKingSite.DataOrm" %>
<%@ Import Namespace="LunchKingSite.Core" %>
<%@ MasterType VirtualPath="~/PPon/PPon.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <link href="../Themes/default/images/17Life/StoreRecruitme/PStoreRecruitment.css" rel="stylesheet" type="text/css" />
    <link href="../themes/PCweb/css/RDL-L.css" rel="stylesheet" type="text/css" />
    <link href="../themes/PCweb/css/RDL-M.css" rel="stylesheet" type="text/css" />
    <link href="../themes/PCweb/css/RDL-S.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../Tools/js/jquery.cascadeAddress.js"></script>
    <script src="../Tools/js/json2.js" type="text/javascript"></script>
    <script src="../Tools/js/jquery.blockUI.js" type="text/javascript"></script>
    <style type="text/css">
        .logo-inline{
            z-index: 1;
           position: relative;
        }
        .center-margin-top{
            margin-top: -32px;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            if (window.location.href.toLowerCase().indexOf("thanks") > -1) {
                $("#cooperation").hide();
                $("#thanks").show();
                $('#divtopbn').hide();
                $("#divCenter").removeClass("center-margin-top");
            }
            else {
                divTop();
                $("#cooperation").show();
                $("#thanks").hide();
                if ($('input:checkbox[name=proposalType]:checked').val() == undefined) {
                    $('input:checkbox[name=proposalType]').filter('[value=product]').prop('checked', true);
                    $(".hope").show();
                    $("#productText").show();
                    $("#kindText").hide();
                }

                //radioBind();
                requiredBind();
                checkboxBind();
                
                $.cascadeAddress.setup('<%=WebUtility.GetSiteRoot()%>', function () {
                    $.cascadeAddress.bind($('#<%= ddlCity.ClientID %>'), $('#<%= ddlArea.ClientID %>'), $('#<%= txtAddress.ClientID %>'), "", "", "", false);
                });
            }

            var mycountdown = new DealCountdown();
            mycountdown.render = function (dd, dh, dm, ds, ed, st, cur) {
                if (st > cur) {
                    $(this).html('即將開賣');
                    return;
                }
                if (ed < cur) {
                    $(this).html('已結束販售');
                    return;
                }
                if (dd >= 3) {
                    $(this).html('限時優惠中!');
                    return;
                }
                var s = '搶購倒數：<span class="day">{dn}天</span>{hn}時{mn}分{sn}秒';
                s = s.replace('{dn}', dd);
                s = s.replace('{hn}', dh);
                s = s.replace('{mn}', dm);
                s = s.replace('{sn}', ds);
                $(this).html(s);
            };
            mycountdown.trigger('.time_counter', true);

            /*
            var s_content_box = $('.s_content_box');
            s_content_box.mouseenter(function () {
                $(this).find('.time_counter').slideDown(300);
            }).mouseleave(function () {
                $(this).find('.time_counter').stop(true, true).slideUp(300);
            });
            s_content_box.mouseenter(function () {
                $(this).find('.time_counter').slideDown(300);
            }).mouseleave(function () {
                $(this).find('.time_counter').stop(true, true).slideUp(300);
            });
            */
        });

        function divTop() {
            $("#divtopbn").removeClass("topbn_bank_wrap");
            $("#divtopbn").addClass("header-block");
            $("#divCenter").addClass("center-margin-top");
            var srt = '<div class="header-center-message">'
            + '<p>17life擁有超過250萬的會員人次，於APP及17Life網站上活耀參與</p>'
            + '<p>2014年17Life推出LINE官方頻道，目前已有380萬粉絲數</p>'
            + '<p>技術超群的攝影團隊，搭配經驗豐富的編輯群</p>'
            + '<p>團購網唯一獲獎【2015臺灣服務業大評鑑】大型購物網站銀牌</p></div>';
            $('#divtopbn').html(srt);
            $('#divtopbn').show();
        }

        //function controlDivPanel(showDiv) {
        //    var divArray = ["product", "virtual", "kind"];
        //    for (var i = 0; i < divArray.length; i++) {
        //        if (divArray[i] == showDiv) {
        //            $("." + divArray[i]).show();
        //        }
        //        else {
        //            $("." + divArray[i]).hide();
        //        }
        //    }
        //    if (showDiv == "kind") {
        //        $(".copartnership").hide();
        //    }
        //    else {
        //        $(".copartnership").show();
        //    }
        //    resetError();
        //}

        function moveToControl(controlId) {
            var $body = (window.opera) ? (document.compatMode == "CSS1Compat" ? $('html') : $('body')) : $('html,body');
            var ctl = $('input[id*=' + controlId + ']');
            if ($(ctl).val() == undefined) {
                ctl = $('input[name*=' + controlId + ']');
            }
            if ($(ctl).val() == undefined) {
                ctl = $('select[name*=' + controlId + ']');
            }

            $body.animate({
                scrollTop: $(ctl).offset().top
            }, 800);
            return true;
        }

        function checkData() {
            var checkResult = true;
            var isMoveToErrorControl = false;

            //var proposalType = $('input:radio[name=proposalType]:checked').val();
            //if (proposalType == "kind") {
            //    if (!checkRequired("groupName") | !checkRequired("groupId")) {
            //        checkResult = false;
            //        if (!isMoveToErrorControl) {
            //            isMoveToErrorControl = moveToControl("groupName");
            //        }
            //    }
            //}
            //else {
            //    if (!checkRequired("brandName") | !checkRequired("companyName") | !checkCompanyId()) {
            //        checkResult = false;
            //        if (!isMoveToErrorControl) {
            //            isMoveToErrorControl = moveToControl("companyName");
            //        }
            //    }
            //}

            if (!checkCheckBoxRequired("proposalType")) {
                checkResult = false;
                if (!isMoveToErrorControl) {
                    isMoveToErrorControl = moveToControl("proposalType");
                }
            }

            if (!checkRequired("companyName")) {
                checkResult = false;
                if (!isMoveToErrorControl) {
                    isMoveToErrorControl = moveToControl("companyName");
                }
            }

            if (!checkRequired("txtAddress") | !checkDropDownList("ddlCity") | !checkDropDownList("ddlArea")
                | !checkRequired("contactName") | !checkRequired("contactPhoneNo")) {
                checkResult = false;
                if (!isMoveToErrorControl) {
                    isMoveToErrorControl = moveToControl("ddlCity");
                }
            }

            if (!checkRequired("contactEmail")) {
                checkResult = false;
                if (!isMoveToErrorControl) {
                    isMoveToErrorControl = moveToControl("contactEmail");
                }
            }
            else if (!isEmail($("input[id*=contactEmail]").val())) {
                $("input[id*=contactEmail]").parent().parent().addClass("error");
                $("input[id*=contactEmail]").parent().find("p").show();
                checkResult = false;
                if (!isMoveToErrorControl) {
                    isMoveToErrorControl = moveToControl("contactEmail");
                }
            }

            if ($("input:checkbox[value=product]").is(":checked") || $("input:checkbox[value=virtual]").is(":checked")) {
                if (!checkTextareaRequired("recommendProduct")) {
                    checkResult = false;
                    if (!isMoveToErrorControl) {
                        isMoveToErrorControl = moveToControl("recommendProduct");
                    }
                }
            }
            if ($("input:checkbox[value=kind]").is(":checked")) {
                if (!checkTextareaRequired("hopeKind")) {
                    checkResult = false;
                    if (!isMoveToErrorControl) {
                        isMoveToErrorControl = moveToControl("hopeKind");
                    }
                }
            }

            //if (proposalType == "kind") {
            //    if (!checkRadioRequired("netKindWorked") |
            //        !checkNetworkedSiteRequired("netKindWorked", "kindWrokedSite") |
            //        !checkRequired("workedKindEvent") | !checkTextareaRequired("hopeKind")) {
            //        checkResult = false;
            //        if (!isMoveToErrorControl) {
            //            isMoveToErrorControl = moveToControl("netKindWorked");
            //        }
            //    }
            //}
            //else {
            //    if (proposalType == "business") {
            //        if (!checkRadioRequired("store") | !checkRadioRequired("isStores") | !checkRadioRequired("groupType") |
            //            !checkCheckBoxRequired("solution") | !checkRadioRequired("platformWorked") |
            //            !checkNetworkedSiteRequired("platformWorked", "businessWrokedSite") | !checkTextareaRequired("recommendProduct")) {
            //            checkResult = false;
            //            if (!isMoveToErrorControl) {
            //                isMoveToErrorControl = moveToControl("store");
            //            }
            //        }
            //    }
            //    else if (proposalType == "market") {
            //        if (!checkRadioRequired("netMarketWorked") | !checkNetworkedSiteRequired("netMarketWorked", "marketWrokedSite") |
            //            !checkRequired("workedMarketEvent") | !checkTextareaRequired("hopeMarket")) {
            //            checkResult = false;
            //            if (!isMoveToErrorControl) {
            //                isMoveToErrorControl = moveToControl("netMarketWorked");
            //            }
            //        }
            //    }
            //}

            if (!checkCheckBoxRequired('privacyPolicy')) {
                checkResult = false;
                if (!isMoveToErrorControl) {
                    isMoveToErrorControl = moveToControl("privacyPolicy");
                }
            }
            return checkResult;
        }

        function sendMail() {
            if (checkData()) {
                var kind = $("input:checkbox[value=kind]").is(":checked");
                var product = $("input:checkbox[value=product]").is(":checked");
                var virtual = $("input:checkbox[value=virtual]").is(":checked");
                
                var companyName = $("input[id*=companyName]").val();
                var address = $("select[id*=ddlCity] option:selected").text() + $("select[id*=ddlArea] option:selected").text() + $("input[id*=txtAddress]").val();
                var contactName = $("input[id*=contactName]").val();
                var contactPhoneNo = $("input[id*=contactPhoneNo]").val();
                var callBackTime = $("select[id*=callbackTime]").val();
                var email = $("input[id*=contactEmail]").val();
                var hope = "";

                var sendSuccess = false;

                //var proposalType = $('input:radio[name=proposalType]:checked').val();
                $('#sendDiv').block({ message: null, overlayCSS: { backgroundColor: '#FFF', opacity: '0.0' } });

                if (product) {
                    hope = $("#recommendProduct").val();
                    $.ajax({
                        type: "POST",
                        url: "ContactUs.aspx/SendBusinessMail",
                        data: JSON.stringify({
                            companyName: companyName,
                            address: address,
                            contactName: contactName,
                            contactPhoneNo: contactPhoneNo,
                            callBackTime: callBackTime,
                            email: email,
                            hope: hope
                        }),
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            var result = $.parseJSON(data.d);
                            if (result.SendResult) {
                                sendSuccess = true;
                            } else {
                                alert("寄送失敗(" + result.Message + ")，請重新寄送。");
                            }
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            alert("寄送失敗(" + xhr.status + ", " + thrownError + " " + ajaxOptions + ")，請重新寄送。");
                        }
                    });
                }
                if (virtual) {
                    hope = $("#recommendProduct").val();
                    $.ajax({
                        type: "POST",
                        url: "ContactUs.aspx/SendMarketMail",
                        data: JSON.stringify({
                            companyName: companyName,
                            address: address,
                            contactName: contactName,
                            contactPhoneNo: contactPhoneNo,
                            callBackTime: callBackTime,
                            email: email,
                            hope: hope
                        }),
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            var result = $.parseJSON(data.d);
                            if (result.SendResult) {
                                sendSuccess = true;
                            } else {
                                alert("寄送失敗(" + result.Message + ")，請重新寄送。");
                            }
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            alert("寄送失敗(" + xhr.status + ", " + thrownError + " " + ajaxOptions + ")，請重新寄送。");
                        }
                    });
                }
                if (kind) {
                    hope = $("#hopeKind").val();
                    $.ajax({
                        type: "POST",
                        url: "ContactUs.aspx/SendKindMail",
                        data: JSON.stringify({
                            companyName: companyName,
                            address: address,
                            contactName: contactName,
                            contactPhoneNo: contactPhoneNo,
                            callBackTime: callBackTime,
                            email: email,
                            hope: hope
                        }),
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            var result = $.parseJSON(data.d);
                            if (result.SendResult) {
                                sendSuccess = true;
                            } else {
                                alert("寄送失敗(" + result.Message + ")，請重新寄送。");
                            }
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            alert("寄送失敗(" + xhr.status + ", " + thrownError + " " + ajaxOptions + ")，請重新寄送。");
                        }
                    });
                }
                $(document).ajaxStop(function () {
                    if (sendSuccess) {
                        location.href = "ContactUs.aspx?thanks";
                    }
                });
                return false;
            }
            else { return false; }
        }

        //function radioBind() {
        //    /**/
        //    //$('input:radio[name=proposalType]').live('click', function () {
        //    //    controlDivPanel($('input:radio[name=proposalType]:checked').val());
        //    //});
        //    $('input:radio[name=store]').live('click', function () {
        //        if ($("input:radio[name=store]:checked").val() != undefined) {
        //            removeErrorByName("store");
        //        }
        //    });
        //    $('input:radio[name=isStores]').live('click', function () {
        //        if ($("input:radio[name=isStores]:checked").val() != undefined) {
        //            removeErrorByName("isStores");
        //        }
        //    });
        //    $('input:radio[name=groupType]').live('click', function () {
        //        if ($("input:radio[name=groupType]:checked").val() != undefined) {
        //            removeErrorByName("groupType");
        //        }
        //    });
        //    $('input:radio[name=platformWorked]').live('click', function () {
        //        if ($("input:radio[name=platformWorked]:checked").val() != undefined) {
        //            removeErrorByName("platformWorked");
        //            if ($("input:radio[name=platformWorked]:checked").val() == "1") {
        //                $(".hadPlatformWorked").show();
        //            }
        //            else {
        //                $(".hadPlatformWorked").hide();
        //            }
        //        }
        //        else {
        //            $(".hadPlatformWorked").hide();
        //        }
        //    });
        //    $('input:radio[name=netMarketWorked]').live('click', function () {
        //        if ($("input:radio[name=netMarketWorked]:checked").val() != undefined) {
        //            removeErrorByName("netMarketWorked");
        //            if ($("input:radio[name=netMarketWorked]:checked").val() == "1") {
        //                $(".hadNetMarketWorked").show();
        //            }
        //            else {
        //                $(".hadNetMarketWorked").hide();
        //            }
        //        }
        //        else {
        //            $(".hadNetMarketWorked").hide();
        //        }
        //    });
        //    $('input:radio[name=netKindWorked]').live('click', function () {
        //        if ($("input:radio[name=netKindWorked]:checked").val() != undefined) {
        //            removeErrorByName("netKindWorked");
        //            if ($("input:radio[name=netKindWorked]:checked").val() == "1") {
        //                $(".hadNetKindWorked").show();
        //            }
        //            else {
        //                $(".hadNetKindWorked").hide();
        //            }
        //        }
        //        else {
        //            $(".hadNetKindWorked").hide();
        //        }
        //    });
        //}

        function checkboxBind() {
            //$("input:checkbox[name=solution]").live('change', function () {
            //    if ($(this).is(":checked")) {
            //        removeErrorByName("solution");
            //    }
            //});
            $("input:checkbox[name=privacyPolicy]").live('change', function () {
                if ($(this).is(":checked")) {
                    removeErrorByName("privacyPolicy");
                }
            });

            $("input:checkbox[name=proposalType]").live('change', function () {
                if ($("input:checkbox[value=kind]").is(":checked")) {
                    $("#kindText").show();
                    removeErrorByName("proposalType");
                }
                else {
                    $("#kindText").hide();
                }
                if ($("input:checkbox[value=product]").is(":checked") || $("input:checkbox[value=virtual]").is(":checked")) {
                    $("#productText").show();
                    removeErrorByName("proposalType");
                }
                else {
                    $("#productText").hide();
                }
                if (!$("input:checkbox[value=product]").is(":checked")
                    && !$("input:checkbox[value=virtual]").is(":checked")
                    && !$("input:checkbox[value=kind]").is(":checked")) {
                    $(".hope").hide();
                }
                else {
                    $(".hope").show();
                }
            });
        }

        function requiredBind() {
            inputKeyupRemoveError("companyName");
            inputKeyupRemoveError("txtAddress");
            inputKeyupRemoveError("contactName");
            inputKeyupRemoveError("contactPhoneNo");
            //inputKeyupRemoveError("groupName");
            //inputKeyupRemoveError("groupId");
            //inputKeyupRemoveError("companyId");
            //inputKeyupRemoveError("brandName");
            //inputKeyupRemoveError("workedMarketEvent");
            //inputKeyupRemoveError("workedKindEvent");
            //inputKeyupRemoveError("businessWrokedSite");
            //inputKeyupRemoveError("marketWrokedSite");
            //inputKeyupRemoveError("kindWrokedSite");
            inputEmailRemoveError();
            inputAddressDropDownListRemoveError();
            textareaKeyupRemoveError("recommendProduct");
            textareaKeyupRemoveError("hopeKind");
            //textareaKeyupRemoveError("hopeMarket");
        }
        function resetError() {
            removeErrorByName("proposalType");
            //removeError("groupName");
            //removeError("groupId");
            //removeError("companyId");
            //removeError("brandName");
            removeError("companyName");
            removeError("contactName");
            removeError("contactPhoneNo");
            removeError("contactEmail");
            //removeError("workedMarketEvent");
            //removeError("workedKindEvent");
            //removeError("businessWrokedSite");
            //removeError("marketWrokedSite");
            //removeError("kindWrokedSite");
            removeDropDownError("ddlCity");
            removeDropDownError("ddlArea");
            removeTextareaError("recommendProduct");
            removeTextareaError("hopeKind");
            //removeTextareaError("hopeMarket");
            //removeErrorByName("solution");
            //removeErrorByName("store");
            //removeErrorByName("isStores");
            //removeErrorByName("groupType");
            //removeErrorByName("netMarketWorked");
            //removeErrorByName("platformWorked");
            //removeErrorByName("netKindWorked");
            removeErrorByName("privacyPolicy");
            $("select[id*=ddlCity]").val("-1");
            $("select[id*=ddlArea]").val("-1");
            $("input[id*=txtAddress]").parent().parent().removeClass("error");
            $("input[id*=txtAddress]").parent().find("p").hide();
            $('html, body').animate({ scrollTop: 0 }, 'normal');
        }
        function textareaKeyupRemoveError(controlId) {
            $("#" + controlId).live("keyup", function () {
                removeTextareaError(controlId);
            });
        }

        function inputKeyupRemoveError(controlId) {
            $("input[id*=" + controlId + "]").live("keyup", function () {
                removeError(controlId);
            });
        }

        function inputAddressDropDownListRemoveError() {
            $("select[id*=ddlCity]").live("change", function () {
                if ($("select[id*=ddlCity]").val() != "-1" && $("select[id*=ddlArea]").val() != "-1") {
                    removeDropDownError("ddlCity");
                }
            });
            $("select[id*=ddlArea]").live("change", function () {
                if ($("select[id*=ddlCity]").val() != "-1" && $("select[id*=ddlArea]").val() != "-1") {
                    removeDropDownError("ddlArea");
                }
            });
        }

        function inputEmailRemoveError() {
            $("input[id*=contactEmail]").live("keyup", function () {
                if ($("input[id*=contactEmail]").val() && isEmail($("input[id*=contactEmail]").val())) {
                    removeError("contactEmail");
                }
            });
        }

        function checkDropDownList(controlId) {
            if ($('select[id*=' + controlId + ']').val() == "-1") {
                $("select[id*=" + controlId + "]").parent().parent().addClass("error");
                $("select[id*=" + controlId + "]").parent().find("p").show();
                return false;
            }
            else {
                return true;
            }
        }

        function checkRequired(controlId) {
            if (!$("input[id*=" + controlId + "]").val()) {
                $("input[id*=" + controlId + "]").parent().parent().addClass("error");
                $("input[id*=" + controlId + "]").parent().find("p").show();
                return false;
            }
            else {
                removeError(controlId);
                return true;
            }
        }

        //function checkCompanyId() {
        //    if (!$("input[id*=companyId]").val() || $("input[id*=companyId]").val().length != 8 | !isNumber($("input[id*=companyId]").val())) {
        //        $("input[id*=companyId]").parent().parent().addClass("error");
        //        $("input[id*=companyId]").parent().find("p").show();
        //        return false;
        //    }
        //    else {
        //        return true;
        //    }
        //}
        function isNumber(val) {
            var reg = /^[0-9]*$/;
            return reg.test(val);
        }

        //function checkRadioRequired(controlId) {
        //    if ($("input:radio[name*=" + controlId + "]:checked").val() == undefined) {
        //        $("input:radio[name*=" + controlId + "]").parent().parent().parent().addClass("error");
        //        $("input:radio[name*=" + controlId + "]").parent().parent().children().filter("p:first").show();
        //        return false;
        //    }
        //    else {
        //        return true;
        //    }
        //}

        function checkCheckBoxRequired(controlName) {
            var checkvalues = $('input[name*=' + controlName + ']:checkbox:checked').map(function () {
                return $(this).val();
            }).get().join(',');

            if (checkvalues == "") {
                $("input:checkbox[name*=" + controlName + "]").parent().parent().parent().addClass("error");
                $("input:checkbox[name*=" + controlName + "]").parent().parent().children().filter("p:first").show();
                return false;
            }
            else {
                return true;
            }
        }

        //function checkNetworkedSiteRequired(controlId, checkId) {
        //    if ($("input:radio[name*=" + controlId + "]:checked").val() == "1") {
        //        return checkRequired(checkId);
        //    }
        //    return true;
        //}

        function checkTextareaRequired(controlId) {
            if (!$("#" + controlId).val()) {
                $("#" + controlId).parent().parent().addClass("error");
                $("#" + controlId).parent().parent().find("p").show();
                return false;
            }
            else {
                return true;
            }
        }

        function removeError(controlId) {
            if (controlId != "txtAddress") {
                $("input[id*=" + controlId + "]").parent().parent().removeClass("error");
            } else {
                if ($("select[id*=ddlCity]").val() != "-1" && $("select[id*=ddlArea]").val() != "-1") {
                    $("input[id*=" + controlId + "]").parent().parent().removeClass("error");
                }
            }
            $("input[id*=" + controlId + "]").parent().find("p").hide();
        }

        function removeTextareaError(controlId) {
            $("#" + controlId).parent().parent().removeClass("error");
            $("#" + controlId).parent().parent().children().filter("p:first").hide();
        }

        function removeErrorByName(controlName) {
            $("input[name*=" + controlName + "]").parent().parent().parent().removeClass("error");
            $("input[name*=" + controlName + "]").parent().parent().children().filter("p:first").hide();
        }

        function removeDropDownError(controlId) {
            if ($("input[id*=txtAddress]").val()) {
                $("select[id*=" + controlId + "]").parent().parent().removeClass("error");
            }
            $("select[id*=" + controlId + "]").parent().find("p").hide();
        }

        function isEmail(email) {
            var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            return regex.test(email);
        }

        function resetForm() {
            if (confirm('確定要清除所有資料？')) {

                //var proposalType = $('input:checked[name=proposalType]:checked').val();
                //if (proposalType == undefined) {
                //    proposalType = "business";
                //}

                document.forms[0].reset();

                $('input:checkbox[name=proposalType]').filter('[value=product]').prop('checked', true);
                //controlDivPanel(proposalType);
                $(".hope").show();
                $("#productText").show();
                $("#kindText").hide();
                resetError();
                $('html, body').animate({ scrollTop: 0 }, 'normal');
                return false;
            } else {
                return false;
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MC" runat="server">
    <div id="cooperation" class="center">
        <div class="ly-rt-full-block">
            <div class="rec-content">
                <h1 class="st-h1name">跟我們合作</h1>
                <hr class="header_hr">
                <div class="grui-form">
                    <div class="form-unit st-rec-height">
                        <label for="" class="unit-label">
                            <h2 class="st-h2name"><span class="st-rec-warningred">*</span>提案類型(可複選)</h2>
                        </label>
                        <div class="data-input">
                            <label class="data-input-label">
                                <input name="proposalType" type="checkbox" value="product">
                                <span class="blank">實體商品</span>
                            </label>
                            <label class="data-input-label">
                                <input name="proposalType" type="checkbox" value="virtual">
                                <span class="blank">非實體商品</span>
                            </label>
                            <label class="data-input-label">
                                <input name="proposalType" type="checkbox" value="kind">
                                <span class="blank">行銷與公益合作</span>
                            </label>
                            <p style="display: none">
                                <span class="st-rec-warningred">請選擇提案類型</span>
                            </p>
                            <p class="note">商品若為實體商品，請勾選「實體商品」；若為餐飲、票券、旅遊、美髮美甲美容相關，請勾選「非實體商品」。
請注意，若您勾選多種類型，將會有多位專員與您聯繫。</p>
                        </div>
                    </div>
                </div>
                <!--grui-->
<%--                <h2 class="st-h2name"  style="margin-left:35px;">請填寫以下表單，17Life將有專人與您聯繫
                    <span class="st-rec-warningred">*</span>
                    <span class="st-rec-swords">為必填欄位</span>
                </h2>--%>
                <div class="grui-form">
                    <div class="form-unit">
                      <label for="" class="unit-label"><span class="st-rec-warningred">*</span>店家/公司/團體名稱</label>
                            <div class="data-input">
                                <input id="companyName" type="text" class="input-full">
                                <p style="display: none">
                                    <span class="st-rec-warningred">請輸入您的店家/公司/團體名稱</span>
                                </p>
                            </div>
                    </div>
<%--                    <div class="copartnership">
                        <div class="form-unit">
                            <label for="" class="unit-label"><span class="st-rec-warningred">*</span>店家/公司/團體名稱</label>
                            <div class="data-input">
                                <input id="companyName" type="text" class="input-full">
                                <p style="display: none">
                                    <span class="st-rec-warningred">請輸入您的店家/公司名稱</span>
                                </p>
                            </div>
                        </div>
                        <div class="form-unit">
                            <label for class="unit-label"><span class="st-rec-warningred">*</span>統一編號</label>
                            <div class="data-input">
                                <input id="companyId" type="text" class="input-full">
                                <p style="display: none">
                                    <span class="st-rec-warningred">請輸入或檢查您的統一編號</span>
                                </p>
                            </div>
                        </div>
                        <div class="form-unit">
                            <label for class="unit-label"><span class="st-rec-warningred">*</span>品牌名稱</label>
                            <div class="data-input">
                                <input id="brandName" type="text" class="input-full">
                                <p style="display: none">
                                    <span class="st-rec-warningred">請輸入您的品牌名稱</span>
                                </p>
                            </div>
                        </div>
                    </div>--%>
<%--                    <div class="kind">
                        <div class="form-unit">
                            <label for class="unit-label"><span class="st-rec-warningred">*</span>團體名稱</label>
                            <div class="data-input">
                                <input id="groupName" type="text" class="input-full">
                                <p style="display: none">
                                    <span class="st-rec-warningred">請輸入您的團體名稱</span>
                                </p>
                            </div>
                        </div>
                        <div class="form-unit">
                            <label for class="unit-label"><span class="st-rec-warningred">*</span>勸募字號</label>
                            <div class="data-input">
                                <input id="groupId" type="text" class="input-full">
                                <p style="display: none">
                                    <span class="st-rec-warningred">請輸入您的勸募字號</span>
                                </p>
                            </div>
                        </div>
                    </div>--%>
                    <div class="form-unit">
                        <label for="" class="unit-label"><span class="st-rec-warningred">*</span>地址</label>
                        <div class="data-input">
                            <asp:DropDownList ID="ddlCity" class="select-wauto" runat="server">
                                <asp:ListItem Text="請選擇" Value="0"></asp:ListItem>
                            </asp:DropDownList>
                            <asp:DropDownList ID="ddlArea" class="select-wauto" runat="server">
                                <asp:ListItem Text="請選擇" Value="0"></asp:ListItem>
                            </asp:DropDownList>
                            <p style="display: none">
                                <span class="st-rec-warningred">請選擇您的縣市與鄉鎮市區</span>
                            </p>
                        </div>
                        <div class="data-input">
                            <asp:TextBox ID="txtAddress" ClientIDMode="Static" class="input-full" runat="server"></asp:TextBox>
                            <p style="display: none">
                                <span class="st-rec-warningred">請輸入您的地址</span>
                            </p>
                        </div>
                    </div>
                    <div class="form-unit">
                        <label for="" class="unit-label"><span class="st-rec-warningred">*</span>聯絡人</label>
                        <div class="data-input">
                            <input id="contactName" type="text" class="input-full">
                            <p style="display: none">
                                <span class="st-rec-warningred">請輸入您的聯絡人</span>
                            </p>
                        </div>
                    </div>
                    <div class="form-unit">
                        <label for="" class="unit-label"><span class="st-rec-warningred">*</span>聯絡電話</label>
                        <div class="data-input">
                            <input id="contactPhoneNo" type="text" class="input-full">
                            <p style="display: none">
                                <span class="st-rec-warningred">請輸入您的聯絡電話</span>
                            </p>
                        </div>
                    </div>
                    <div class="form-unit">
                        <label for="" class="unit-label">合適回覆時間</label>
                        <div class="data-input">
                            <select id="callbackTime" name="select" class="select-wauto">
                                <option value="未選擇" selected="selected">請選擇適合聯繫您的時間</option>
                                <option value="上午9:00-12:00">上午9:00-12:00</option>
                                <option value="下午14:00-18:00">下午14:00-18:00</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-unit">
                        <label for="" class="unit-label"><span class="st-rec-warningred">*</span>E-mail</label>
                        <div class="data-input">
                            <input id="contactEmail" type="text" class="input-full">
                            <p style="display: none">
                                <span class="st-rec-warningred">您輸入的信箱格式有誤，請重新輸入
                                </span>
                            </p>
                        </div>
                    </div>
<%--                    <div class="form-unit">
                        <label for class="unit-label">官網/部落格</label>
                        <div class="data-input">
                            <input id="myWebSite" type="text" class="input-full">
                            <p style="display: none">
                                <span class="st-rec-warningred">請輸入您的官網/部落格</span>
                            </p>
                        </div>
                    </div>--%>
                    <div class="hope">
<%--                        <div class="form-unit">
                            <label for class="unit-label">
                                <p><span class="st-rec-warningred">*</span>實體店面</p>
                            </label>
                            <div class="data-input">
                                <label>
                                    <input name="store" type="radio" value="1"><p class="cancel-mr">有</p>
                                </label>
                                <label>
                                    <input name="store" type="radio" value="0"><p class="cancel-mr">無</p>
                                </label>
                                <p style="display: none">
                                    <span class="st-rec-warningred">請選擇您是否有實體店面</span>
                                </p>
                            </div>
                        </div>
                        <div class="form-unit">
                            <label for class="unit-label">
                                <p><span class="st-rec-warningred">*</span>公司類型</p>
                            </label>
                            <div class="data-input">
                                <label>
                                    <input name="isStores" type="radio" value="0"><p class="cancel-mr">單店</p>
                                </label>
                                <label>
                                    <input name="isStores" type="radio" value="1"><p class="cancel-mr">連鎖</p>
                                </label>
                                <p style="display: none">
                                    <span class="st-rec-warningred">請選擇您的公司類型</span>
                                </p>
                            </div>
                        </div>
                        <div class="form-unit">
                            <label for class="unit-label">
                                <p><span class="st-rec-warningred">*</span>組織型態</p>
                            </label>
                            <div class="data-input">
                                <label>
                                    <input name="groupType" value="原廠" type="radio"><p class="cancel-mr">原廠</p>
                                </label>
                                <label>
                                    <input name="groupType" value="總代理經銷商" type="radio"><p class="cancel-mr">總代理經銷商</p>
                                </label>
                                <label>
                                    <input name="groupType" value="進出口商" type="radio"><p class="cancel-mr">進出口商</p>
                                </label>
                                <label>
                                    <input name="groupType" value="其它" type="radio"><p class="cancel-mr">其它</p>
                                </label>
                                <p style="display: none">
                                    <span class="st-rec-warningred">請選擇您的組織型態</span>
                                </p>
                            </div>
                        </div>
                        <div class="form-unit form-unit-valign">
                            <label for class="unit-label">
                                <p><span class="st-rec-warningred">*</span>選擇配合方式（可複選，將依合作方式不同由不同單位承辦人員與你們聯繫！）</p>
                            </label>
                            <div class="data-input data-input-valign">
                                <label>
                                    <input name="solution" value="在地團購" type="checkbox"><p class="cancel-mr">在地團購</p>
                                </label>
                                <label>
                                    <input name="solution" value="宅配商品" type="checkbox"><p class="cancel-mr">宅配商品</p>
                                </label>
                                <label>
                                    <input name="solution" value="店取/宅配食品" type="checkbox"><p class="cancel-mr">店取/宅配食品</p>
                                </label>
                                <label>
                                    <input name="solution" value="旅遊渡假" type="checkbox"><p class="cancel-mr">旅遊渡假</p>
                                </label>
                                <label>
                                    <input name="solution" value="優惠券" type="checkbox"><p class="cancel-mr">優惠券</p>
                                </label>
                                <p style="display: none">
                                    <span class="st-rec-warningred">請勾選您的配合方式</span>
                                </p>
                            </div>
                        </div>
                        <div class="form-unit">
                            <label for class="unit-label">
                                <p><span class="st-rec-warningred">*</span>網路平台合作經驗</p>
                            </label>
                            <div class="data-input">
                                <label>
                                    <input name="platformWorked" value="1" type="radio"><p class="cancel-mr">有</p>
                                </label>
                                <label>
                                    <input name="platformWorked" value="0" type="radio"><p class="cancel-mr">無</p>
                                </label>
                                <p style="display: none">
                                    <span class="st-rec-warningred">請選擇您的組織型態</span>
                                </p>
                            </div>
                        </div>
                        <!--如無平台合作經驗則將form-unit改成form-unit rec-display-none-->
                        <div class="hadPlatformWorked form-unit" style="display: none">
                            <label for="" class="unit-label">
                                <p><span class="st-rec-warningred">*</span>曾合作過的網站</p>
                            </label>
                            <div class="data-input">
                                <input id="businessWrokedSite" type="text" class="input-full" placeholder="請填入網址。(ex.https://www.17life.com/ppon/default.aspx)" autocomplete="off">
                                <p style="display: none">
                                    <span class="st-rec-warningred">請輸入您曾經合作過的網站</span>
                                </p>
                            </div>
                        </div>
                        <div class="form-unit">
                            <label for="" class="unit-label">
                                <p>電視購物</p>
                            </label>
                            <div class="data-input">
                                <input id="tvBuy" type="text" class="input-full">
                            </div>
                        </div>--%>
                        <div class="form-unit st-rec-height">
                            <label for="" class="unit-label">
                                <p><span class="st-rec-warningred">*</span>希望17Life為您做些什麼</p>
                            </label>
                            <div class="data-input" id="productText">
                                <textarea id="recommendProduct" class="rec" type="text" placeholder="請簡單描述你的商品，如商品/餐廳的特色、價格、優惠方案等。有任何網站連結，也請一併附上，讓我們更了解您的產品喔。" autocomplete="off"></textarea>
                            </div>
                            <div class="data-input" id="kindText">
                                <textarea id="hopeKind" class="rec" rows="3" type="text" placeholder="有關行銷與公益合作的提案，請描述期待的行銷模式(廣告贊助、廣告交換)、公益合作模式(募款、徵求物資)，也可敘述過去有做過的活動案例喔。" autocomplete="off"></textarea>
                            </div>
                            <p style="text-align:center; display: none">
                                <span class="st-rec-warningred">請輸入您希望與17Life一起做些什麼</span>
                            </p>
                        </div>
                    </div>
<%--                    <div class="market">
                        <div class="form-unit">
                            <label for class="unit-label">
                                <p><span class="st-rec-warningred">*</span>網路行銷合作經驗</p>
                            </label>
                            <div class="data-input">
                                <label>
                                    <input name="netMarketWorked" value="1" type="radio"><p class="cancel-mr">有</p>
                                </label>
                                <label>
                                    <input name="netMarketWorked" value="0" type="radio"><p class="cancel-mr">無</p>
                                </label>
                                <p style="display: none">
                                    <span class="st-rec-warningred">請選擇您是否有網路行銷合作經驗</span>
                                </p>
                            </div>
                        </div>
                        <!--如無平台合作經驗則將form-unit改成form-unit rec-display-none-->
                        <div class="hadNetMarketWorked form-unit" style="display: none">
                            <label for class="unit-label">
                                <p><span class="st-rec-warningred">*</span>曾合作過的網站</p>
                            </label>
                            <div class="data-input">
                                <input id="marketWrokedSite" type="text" class="input-full" placeholder="請填入網址。(ex.https://www.17life.com/ppon/default.aspx)" autocomplete="off">
                                <p style="display: none">
                                    <span class="st-rec-warningred">請輸入您曾經合作過的網站</span>
                                </p>
                            </div>
                        </div>
                        <div class="form-unit">
                            <label for class="unit-label">
                                <p><span class="st-rec-warningred">*</span>曾做過廣告行銷活動</p>
                            </label>
                            <div class="data-input">
                                <input id="workedMarketEvent" type="text" class="input-full" placeholder="(實體活動、報章雜誌等等)" autocomplete="off">
                                <p style="display: none">
                                    <span class="st-rec-warningred">請輸入您曾做過廣告行銷活動</span>
                                </p>
                            </div>
                        </div>
                        <div class="form-unit st-rec-height">
                            <label for class="unit-label">
                                <p><span class="st-rec-warningred">*</span>希望17Life為您做些什麼</p>
                            </label>
                            <div class="data-input">
                                <textarea id="hopeMarket" class="rec" type="text" placeholder="請簡單描述你的商品，如商品/餐廳的特色、價格、優惠方案等。有任何網站連結，也請一併附上，讓我們更了解您的產品喔。" autocomplete="off"></textarea>
                            </div>
                                <p style="display: none">
                                <span class="st-rec-warningred">請輸入您想希望與17Life一起做些什麼</span>
                            </p>
                        </div>
                    </div>--%>
<%--                    <div class="kind">
                        <div class="form-unit">
                            <label for class="unit-label">
                                <p><span class="st-rec-warningred">*</span>網路公益合作經驗</p>
                            </label>
                            <div class="data-input">
                                <label>
                                    <input name="netKindWorked" value="1" type="radio"><p class="cancel-mr">有</p>
                                </label>
                                <label>
                                    <input name="netKindWorked" value="0" type="radio"><p class="cancel-mr">無</p>
                                </label>
                                <p style="display: none">
                                    <span class="st-rec-warningred">請選擇您是否有網路公益合作經驗</span>
                                </p>
                            </div>
                        </div>
                        <!--如無平台合作經驗則將form-unit改成form-unit rec-display-none-->
                        <div class="hadNetKindWorked form-unit" style="display: none">
                            <label for class="unit-label">
                                <p><span class="st-rec-warningred">*</span>曾合作過的網站</p>
                            </label>
                            <div class="data-input">
                                <input id="kindWrokedSite" type="text" class="input-full" placeholder="請填入網址。(ex.https://www.17life.com/ppon/default.aspx)" autocomplete="off">
                                <p style="display: none">
                                    <span class="st-rec-warningred">請輸入您曾經合作過的網站</span>
                                </p>
                            </div>
                        </div>
                        <div class="form-unit">
                            <label for class="unit-label">
                                <p><span class="st-rec-warningred">*</span>曾做過的相關活動</p>
                            </label>
                            <div class="data-input">
                                <input id="workedKindEvent" type="text" class="input-full" placeholder="(實體活動、報章雜誌等等)" autocomplete="off">
                                <p style="display: none">
                                    <span class="st-rec-warningred">請輸入您曾做過的相關活動</span>
                                </p>
                            </div>
                        </div>
                        <div class="form-unit st-rec-height">
                            <label for="" class="unit-label">
                                <p><span class="st-rec-warningred">*</span>希望17Life為您做些什麼</p>
                            </label>
                            <div class="data-input">
                                <textarea id="hopeKind" class="rec" rows="3" type="text" placeholder="有關行銷與公益合作的提案，請描述期待的行銷模式(廣告贊助、廣告交換)、公益合作模式(募款、徵求物資)，也可敘述過去有做過的活動案例喔。" autocomplete="off"></textarea>
                            </div>
                                <p style="display: none">
                                <span class="st-rec-warningred">請輸入您希望與17Life一起做些什麼</span>
                            </p>
                        </div>
                    </div>--%>
                    <div class="form-unit signle_form_unit">
                        <div class="data-input" style="margin-left: 0;">
                            <label>
                                <input id="privacyPolicy" name="privacyPolicy" type="checkbox">
                                <p class="cancel-mr">我已閱讀並同意「<a target="_blank" href="../newmember/privacy.aspx">17Life網站個人資料蒐集告知及隱私權保護政策</a>」，同意提供以上資料供17Life使用。</p>
                            </label>
                            <p style="display: none">
                                <span class="st-rec-warningred">您還未同意隱私權政策</span>
                            </p>
                        </div>
                    </div>
                    <div class="btn-group btn-group-full-page-center clearfix">
                        <div class="btn-half">
                            <a class="btn btn-default btn-default-flat"  onclick="return sendMail();">送出</a>
                        </div>
                        <div class="btn-half">
                            <a class="btn btn-primary btn-primary-flat" onclick="return resetForm();">清除重填</a>
                        </div>
                    </div>
<%--                    <div class="form-unit st-rec-border-none">
                        <div class="data-input sendDiv">
                            <input class="btn btn-large btn-primary" value="送出" />
                            <input class="btn btn-large" value="清除重填" />
                        </div>
                    </div>--%>
                </div>
                <!--grui-->
            </div>
            <!--content-->
        </div>
        <!--full-block-->
    </div>
    <div id="thanks" class="center ly-rt-full-block">
        <div class="rec-content">
            <h1 class="st-h1name">跟我們合作</h1>
            <hr class="header_hr st-rec-marginb">
            <%--<h2 class="st-h2nameb">感謝您的提案!</h2>--%>
            <div class="grui-form">
                <div class="st-rec-line">
                    <ul class="st-h3name">
                        <li>感謝您的提案，我們將速速派出精銳的專員與您聯絡。</li>
                        <li>讓 17Life 為你帶來滿滿的大！平！台！</li>
                        <li>並協助您的產品賣好、賣滿。</li>
                    </ul>
                </div>
            </div>
            <!--grui-->
<%--            <h2 class="st-h2name st-rec-marginl">對合作提案有其他疑問？</h2>
            <div class="rec-phone-space">
                <div class="grui-form">
                    <div class="st-rec-line st-rec-line-none">
                        <ul>
                            <li>17Life業務專線：0919-176580 (0919-17Life幫您)</li>
                            <li>服務時間：平日 10:00-12:00 & 14:00-18:00</li>
                        </ul>
                    </div>
                </div>
                <!--grui-->
            </div>--%>
            <div class="rec-eg">
                <img style="float:right;" src="../Themes/default/images/17Life/StoreRecruitme/eg.png" width="81" height="106">
            </div>
            <h1 class="st-h1name">參考其他合作活動內容</h1>
            <hr class="header_hr">
            <div class="tc_small_content_area">
                <asp:Repeater ID="rpt_Deals" runat="server" OnItemDataBound="rpt_Deals_ItemDataBound">
                    <ItemTemplate>
                        <div class="s_content_box fl rec-width" style="height: 306px;">
                            <div class="title">
                                <a target="_blank" href='<%#ResolveUrl(string.Format("~/{0}", ((ViewPponDeal)(Container.DataItem)).BusinessHourGuid))%>'>
                                    <%# CommonFacade.GetStringByByteLength(GetDealShortTitle(((ViewPponDeal)(Container.DataItem))), 49, "...")%>
                                </a>
                            </div>
                            <div class="deal_pic">
                                <a target="_blank" href='<%#ResolveUrl(string.Format("~/{0}", ((ViewPponDeal)(Container.DataItem)).BusinessHourGuid))%>'>
                                    <span id='<%# Container.ItemIndex + "mainPic"%>' value='<%# GetImageString(((ViewPponDeal)(Container.DataItem)).EventImagePath)%>'
                                        title='<%# ((ViewPponDeal)(Container.DataItem)).EventName%>'>
                                        <asp:Image ID="imgMainPic" runat="server" Width="290" Height="162" />
                                    </span>
                                </a>
                                <div class="time_counter" id='<%# Container.ItemIndex + "outlTAll"%>'
                                    data-start="<%#((ViewPponDeal)(Container.DataItem)).BusinessHourOrderTimeS.ToJavaScriptMilliseconds()%>"
                                    data-end="<%#((ViewPponDeal)(Container.DataItem)).BusinessHourOrderTimeE.ToJavaScriptMilliseconds()%>">
                                    搶購倒數：<span class="day"><asp:Literal ID="lT2" runat="server" /></span>
                                    <asp:Literal ID="lT" runat="server" />
                                </div>
                                <asp:HiddenField ID="hidTimeCounterId" runat="server" Value="outlTAll" />
                                <div class='<%# ((((ViewPponDeal)(Container.DataItem)).OrderedQuantity >= ((ViewPponDeal)(Container.DataItem)).OrderTotalLimit) ? "SoldOut_Bar_290" :
                                    string.Empty)%>'>
                                </div>
                                <div class="activeLogo alo-m">
                                    <asp:Literal ID="lit_DealPromoImage" runat="server"></asp:Literal>
                                </div>
                            </div>
                            <div class="smallprice">
                                <div class="discount fl">
                                    <asp:Literal ID="lit_Discount_2" runat="server"></asp:Literal>
                                </div>
                                <div class="price fl">
                                    <span class="fami_price_text" style="<%#(((ViewPponDeal)(Container.DataItem)).ExchangePrice.HasValue && ((ViewPponDeal)(Container.DataItem)).ExchangePrice > default(decimal))?"": "display:none;" %>">全家兌換價</span>
                                    $<%# CheckZeroPriceToShowPrice(((ViewPponDeal)(Container.DataItem))).ToString("F0")%><span
                                        class="smalltext"><%# (((ViewPponDeal)(Container.DataItem)).BusinessHourStatus & (int)(BusinessHourStatus.ComboDealMain)) > 0 ? "起" : string.Empty%></span>
                                </div>
                                <div class="ori fl">
                                    <%# "$" + ((ViewPponDeal)(Container.DataItem)).ItemOrigPrice.ToString("F0")%>
                                </div>
                                <div class="clear">
                                </div>
                            </div>
                            <div class="bottom bottom-width-small">
                                <div class="buyer fl">
                                    <%# ((ViewPponDeal)(Container.DataItem)).GetAdjustedOrderedQuantity().IsAdjusted ? string.Format("<span class=\"text\">已售出</span>{0}<span class=\"smalltext\">份</span>", ((ViewPponDeal)(Container.DataItem)).GetAdjustedOrderedQuantity().Quantity) : ((ViewPponDeal)(Container.DataItem)).GetAdjustedOrderedQuantity().Quantity + "<span class=\"text\">人已購買</span>"%>
                                </div>
                                <a target="_blank" href='<%#ResolveUrl(string.Format("~/{0}", ((ViewPponDeal)(Container.DataItem)).BusinessHourGuid))%>' class="go_button fr">馬上看</a>
                            </div>
                            <div style="display: none">
                                <asp:Literal ID="lit_scriptcountdown" runat="server"></asp:Literal>
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
            <div class="clear"></div>
        </div>
        <!--content-->
    </div>
    <!--full-block-->
    <!--center-->
</asp:Content>

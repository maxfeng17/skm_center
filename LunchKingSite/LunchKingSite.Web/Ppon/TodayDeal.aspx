﻿<%@ Page Language="C#" MasterPageFile="~/Ppon/Ppon.Master" AutoEventWireup="true" CodeBehind="TodayDeal.aspx.cs" Inherits="LunchKingSite.Web.Ppon.TodayDeal" %>
<%@ Import Namespace="LunchKingSite.BizLogic.Component" %>
<%@ Import Namespace="LunchKingSite.DataOrm" %>
<%@ Import Namespace="LunchKingSite.Core" %>
<%@ Import Namespace="LunchKingSite.BizLogic.Facade" %>
<%@ MasterType VirtualPath="~/PPon/PPon.Master" %>
<%@ Register TagPrefix="uc1" Src="~/UserControls/LoginPanel.ascx" TagName="LoginPanel" %>
<%@ Register Src="~/UserControls/RandomParagraph.ascx" TagName="RandomParagraph" TagPrefix="ucR" %>
<%@ Register Src="~/UserControls/Paragraph.ascx" TagName="Paragraph" TagPrefix="ucP" %>

<asp:Content ID="cDefaultMeta" ContentPlaceHolderID="cphPponMeta" runat="server">
    <meta property="fb:app_id" content="<%=FB_AppId%>">
    <meta property="og:type" content="website" />
    <meta property="og:title" content="<%=OgTitle%>" />
    <meta property="og:url" content="<%=OgUrl%>" />
    <meta property="og:site_name" content="17Life" />
    <meta property="og:description" content="<%=OgDescription%>" />
    <meta property="og:image" content="<%=OgImage%>" />
    <meta name="apple-itunes-app" content="app-id=<%= iOSAppId%>">
    <meta name="google-play-app" content="app-id=<%= AndroidAppId%>">
    <asp:PlaceHolder ID="phNorobots" runat="server" Visible="false">
        <meta name="robots" content="noindex,nofollow,noarchive" />
    </asp:PlaceHolder>
    <link rel="image_src" href="<%=LinkImageSrc%>" />
    <link rel="canonical" href="<%=LinkCanonicalUrl%>" />
    <asp:PlaceHolder ID="phAppLinks" runat="server"></asp:PlaceHolder>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <link href='<%= ResolveUrl("../themes/PCweb/css/Rightside.css")%>' rel="stylesheet" type="text/css" />
    <link href='<%= ResolveUrl("../themes/PCweb/css/RDL-L.css") %>' rel="stylesheet" type="text/css" />
    <link href='<%= ResolveUrl("../themes/PCweb/css/RDL-M.css") %>' rel="stylesheet" type="text/css" />
    <link href='<%= ResolveUrl("../themes/PCweb/css/RDL-S.css") %>' rel="stylesheet" type="text/css" />
    <asp:Literal ID="lit_PadCss" runat="server"></asp:Literal>
    <%=LunchKingSite.Core.Helper.RenderCss("/Themes/PCweb/css/ppon_item.min.css") %>
    <script type="text/javascript" src="<%= ResolveUrl("../Tools/js/jquery.smartbanner.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("../Tools/js/homecook.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("../Tools/js/osm/OpenLayers.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("../Tools/js/jquery-sliding-menu.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("../Tools/js/TouchSlide.1.1.source.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("../Tools/js/jquery.SuperSlide.2.1.1.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("../Tools/js/ppon/pponMemberCollect.js")%>"></script>
    <script type="text/javascript">
        $(function () {
            /*
            * 隱藏選單
            */
            $("#WebNaviCity").hide();

            /*
            *預設棋盤式瀏覽
            */
            $("#Left .item_wrap").removeClass("item_wrap_list").addClass("item_wrap_grid");
            $('.item_price_more').css('display', 'none');
            $('.item_price_less').css('display', 'block');/**/

            //每次resize、切換棋盤/條列瀏覽 皆重新抓一次寬高
            $('.item_pic').css('height', $('.item_soldout_480').height());
            $('.item_pic').css('width', $('.item_soldout_480').width());
            $('#divhotsaleDeals').show();

            /*
            *
            */
            $('#naviIndexLink').removeClass('navbtn_inpage').addClass("navbtn");

            var memberCollectDealGuids = $.parseJSON($('#hdMemberCollectDealGuidJson').val());
            $('#maincontent').pponMemberCollectList({ 'memberCollectionDealGuids': memberCollectDealGuids }).render();       
            $('#maincontent').pponMemberCollectList({ 'memberCollectionDealGuids': memberCollectDealGuids });  
        });

      
    </script>
    <style type="text/css">
        .fb_iframe_widget_lift {
            z-index: 10 !important;
        }

        .ui-corner-all {
            -moz-border-radius: 0px;
            -webkit-border-radius: 0px;
            border-radius: 0px;
        }

        .NaviCityArea a {
            cursor: pointer;
        }
        #Shopareatitle li + li {
            margin-top: 10px;
        }

        @media screen and (max-width: 480px) {
            .ui-widget-content, .ui-state-default, .ui-state-focus, .ui-state-hover, .ui-state-active, .ui-widget-content .ui-state-active {
                border: none;
                font-weight: normal;
            }
        }
        /*m版寬度1001以下隱藏background image*/
        @media screen and (max-width: 1001px) {
            #wrap {
                background: #f2f2f0 !important;
            }
        }
    	#divCenter {
			margin-top: 30px !important;
    	}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MC" runat="server">
    <div id="Default">
        <asp:PlaceHolder ID="p1" runat="server">
            <input id="HfHami" type="hidden" value="<%= Hami%>" />
            <input id="HfShowEventEmail" type="hidden" value="<%= ShowEventEmail%>" />
            <input id="HfCloseAppBlockHiddenDays" type="hidden" value="<%= CloseAppBlockHiddenDays%>" />
            <input id="HfViewAppBlockHiddenDays" type="hidden" value="<%= ViewAppBlockHiddenDays%>" />
            <input id="HfIsOpenNewWindow" type="hidden" value="<%= IsOpenNewWindow%>" />
            <input id="HfIsMobileBroswer" type="hidden" value="<%= IsMobileBroswer%>" />
            <input id="HfCityId" type="hidden" value="<%= CityId%>" />
            <input id="HfSubRegionId" type="hidden" value="<%= SubRegionCategoryId%>" />
            <input id="HfSubCategoryId" type="hidden" value="<%= CategoryID%>" />
            <input id="HfDealCategoryId" type="hidden" value="<%=PponCategory%>" />
            <input id="HfBusinessHourId" type="hidden" value="<%=BusinessHourId%>" />
            <input id="HfIsDeliveryDeal" type="hidden" value="<%=IsDeliveryDeal%>" />
            <input id="HfLoginUrl" type="hidden" value="<%=string.Format("{0}?ReturnUrl={1}", FormsAuthentication.LoginUrl, Server.UrlEncode((string)Session[LkSiteSession.NowUrl.ToString()]))%>" />
            <input id="HfFilterCategoryIdList" type="hidden" value="<%=string.Join(",", FilterCategoryIdList)%>" />
            <input id="HfTravelCategoryId" type="hidden" value="<%=TravelCategoryId%>" />
            <input id="HfFemaleCategoryId" type="hidden" value="<%=FemaleCategoryId%>" />
            <input id="HfIsLogin" type="hidden" value="<%=(Page.User.Identity.IsAuthenticated) ? "1" : "0"%>" />
            <input id="HfShortType" type="hidden" value="<%=(int)SortType%>" />
            <input id="HfPicAlt" type="hidden" value="<%=PicAlt%>" />
            <input id="HfMShareType" type="hidden" value="fb" />
            <input id="HfBindDivClick" type="hidden" value="0" />
            <input id="HfFilterColumn" type="hidden" value="" />
            <input id="hdMemberCollectDealGuidJson" type="hidden" runat="server" clientidmode="Static" />

            <div id="ousideAd" class="outside_AD" style="display: none;">
                <ucR:RandomParagraph ID="outsideAD" runat="server" OnInit="RandomPponNewsInit" />
            </div>
            <asp:Panel ID="pan_EmptyZone" runat="server" CssClass="empty-zone" Visible="False">
                <span class="icon-smile-o"></span>
                <p>暫無提供任何好康，請重新選取</p>
            </asp:Panel>
            <div class="LeftRight_wrap">
                <div id="Left">
                    <div id="maincontent" class="clearfix">
                        <%
                            int picIndex = 0;
                            foreach (var item in ViewMultipleMainDeals)
                            {
                                picIndex += 1;
                        %><div class="item_wrap item_wrap_grid">
                            <a class="open_new_window" href="<%=ResolveUrl(string.Format("~/{0}/{1}", item.deal.CityID, item.deal.PponDeal.BusinessHourGuid))%>" target="_blank">
                                <div class="item_soldout_480" style="<%= (item.deal.PponDeal.OrderedQuantity >= item.deal.PponDeal.OrderTotalLimit) ? "": "display: none"%>">
                                    <img src="/Themes/PCweb/images/soldout_bar_480.png">
                                </div>
                                <%if (item.EveryDayNewDeal != PponFacade._BLANK_IMAGE_SRC) {%>
                                <div class="item_sitcker">
                                    <img src="<%=item.EveryDayNewDeal %>" />
                                </div>
                                <%} %>
                                <div class="item_activeLogo" style="display: block">
                                    <%= item.deal.PponDeal.PromoImageHtml %>
                                </div>
                                <div class="item_pic" style="height: 189px; width: 340px;">
                                    <%=item.clit_CityName %>
                                    <% if (picIndex <= 6)
                                        {
                                            %>
                                            <img class="multipleDealLazy" src="<%= item.deal.PponDeal.DefaultDealImage %>" alt="<%=(picIndex>5)?(string.IsNullOrEmpty(item.deal.PponDeal.PicAlt))?(string.IsNullOrEmpty(item.deal.PponDeal.AppTitle))? item.deal.PponDeal.CouponUsage:item.deal.PponDeal.AppTitle:item.deal.PponDeal.PicAlt:string.Empty %>" />
                                            <%
                                        }
                                        else
                                        {
                                            %>
                                            <img class="multipleDealLazy" data-original="<%= item.deal.PponDeal.DefaultDealImage %>" alt="<%=(picIndex>5)?(string.IsNullOrEmpty(item.deal.PponDeal.PicAlt))?(string.IsNullOrEmpty(item.deal.PponDeal.AppTitle))? item.deal.PponDeal.CouponUsage:item.deal.PponDeal.AppTitle:item.deal.PponDeal.PicAlt:string.Empty %>" />
                                            <%
                                        }
                                    %>
									<%if (item.deal.PponDeal.CategoryIds.Contains(ViewPponDealManager._24H_ARRIVAL_CATEGORY_ID)) { %>
										<img class="flag-24h" src="/themes/PCweb/images/24h_cover_flag.svg" alt="" />
									<%} %>
                                </div>
                                <div class="item_price_more" style="display: none;">
                                    <div class="discount">
                                        <%=item.discount_2 %>
                                    </div>
                                    <ul>
                                        <%=item.deal_Label_1 %>
                                    </ul>
                                    <div class="price_detail">
                                        <p>好康價</p>
                                        <p class="price">$<%= CheckZeroPriceToShowPrice(item.deal.PponDeal).ToString("F0")%>元</p>

                                        <p>原價<span class="oriprice">$<%=item.deal.PponDeal.ItemOrigPrice.ToString("F0")%></span>| 節省$<%= (item.deal.PponDeal.ItemOrigPrice - CheckZeroPriceToShowPrice(item.deal.PponDeal)).ToString("F0")%></p>

                                        <div class="TimerTitleField forlist" data-start="<%=item.timerTitleField_dataStart%>" data-end="<%=item.timerTitleField_dataEnd%>">
                                            <span class="icon-clock-o"></span>
                                            <div class="TimerField">
                                                <div class="TimeDayCounter">
                                                    <%=item.litDays %>
                                                </div>
                                                <div class="TimeCounter">
                                                    <%=item.litCountdownTime %>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--//price_detail//end-->
                                    <div class="btn_buy_wrap">
                                        <div class="btn_buy">馬上看</div>
                                    </div>
                                </div>

                                <div class="item_title">
                                    <%= OrderedQuantityHelper.Show(item.deal.PponDeal, OrderedQuantityHelper.ShowType.BetaInPortal)%>
                                    <ul>
                                        <%=item.deal_Label_2 %>
                                    </ul>
                                    <% if (!string.IsNullOrEmpty(item.clit_CityName2)) { %>
                                    <span class="tag_place">
                                        <%=item.clit_CityName2 %>
                                    </span>
                                    <%}%>
                                    <div class="tag_title"><%=PponFacade.GetDisplayPponName(item.deal.PponDeal)%></div>
                                    <p class="tag_subtitle"><%= item.deal.PponDeal.EventTitle%></p>
                                    <div class="item_info clearfix">
                                        <div class="item_info_detail">
                                            <span>原價</span><span class="text_line"><%=item.deal.PponDeal.ItemOrigPrice.ToString("F0")%></span><span>| </span>
                                        <span><%= new HtmlString(item.discount_1) %></span> 
                                            <% if(item.deal.PponDeal.DiscountPrice.HasValue) { %>
                                                <span>好康價</span>
                                            <span class="text_big">$ <%= PponFacade.CheckZeroPriceToShowPrice(item.deal.PponDeal, item.deal.CityID).ToString("F0") %></span>
                                            <span><%= (item.deal.PponDeal.ComboDelas != null && item.deal.PponDeal.ComboDelas.Count > 1) ? "起" : "元" %></span>
                                            <% } %>
                                        </div>
                                        <!-- 在地檔才有評價 -->
                                        <% if (item.RatingShow)
                                        {  %>
                                        <div class="item_info_rating">
                                            <% if (item.RatingNumber == 0)
                                            { %>
                                            <span style="display: block">(評價累積中)</span>
                                            <% }
                                            else
                                            { %>
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                            <span class="count"><%= item.RatingScoreStr %></span>
                                            <sapn>(<%= item.RatingNumber %>)</sapn>
                                            <% } %>
                                        </div>
                                        <% } %>
                                    </div>
                                </div>

                               <%-- <div class="item_price_less" style="display: block;">
                            <span class="discount">
                            <%=item.discount_1 %>
                                    </span>
                                    <span class="price">$<%= CheckZeroPriceToShowPrice(item.deal.PponDeal).ToString("F0")%>元<%= (item.deal.PponDeal.BusinessHourStatus & (int)(BusinessHourStatus.ComboDealMain)) > 0 ? "起" : string.Empty%></span><span class="oriprice">$<%= item.deal.PponDeal.ItemOrigPrice.ToString("F0")%></span><span class="btn_buy btn-primary"><%=PponFacade.GetDealItemBtnText(item.deal.PponDeal) %></span></div>--%>

                                <div class="item_price_less">
                                    <% if (!item.deal.PponDeal.DiscountPrice.HasValue)
                                        { //憑證或無符合折扣券價 %>

                                    <div class="item_discount clearfix">
                                        <span class="disc_black">超值好康價</span>
                                        <span class="disc_price color_red">$ <%= PponFacade.CheckZeroPriceToShowPrice(item.deal.PponDeal, item.deal.CityID).ToString("F0") %></span>
                                        <span class="disc_black" style="display: inline-block"><%= (item.deal.PponDeal.ComboDelas != null && item.deal.PponDeal.ComboDelas.Count > 1) ? "起" : "元" %></span>
                                    </div>
                                    <% }
                                        else
                                        { %>
                                    <div class="item_discount clearfix">
                                        <i class="fa fa-bullhorn" aria-hidden="true"></i>
                                        <div class="disc_text">結帳使用折價</div>
                                        <span class="disc_price color_red">$<%= item.deal.PponDeal.DiscountPrice.ToString("0") %></span><span class="disc_black" style="display: inline-block"><%= (item.deal.PponDeal.ComboDelas != null && item.deal.PponDeal.ComboDelas.Count > 1) ? "起" : "元" %></span></div>
                                    <% } %>
                                    <div class="item_collect" data-bid="<%= item.deal.PponDeal.BusinessHourGuid %>">
                                        <a href="javascript:void(0)" class="">
                                            <i class="fa fa-heart-o" aria-hidden="true"></i>收藏
                                        </a>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <% } %>
                        <div id="NoneDeal" class="text-center" style="padding-top: 30px; display: none;"><span class="bigtext">沒有您篩選的好康喔</span></div>
                    </div>
                </div>
                <!--Left end-->
                <div id="Rightarea">
                    <div id="Stickers">
                        <ucP:Paragraph ID="paragraph1" SetContentName="/ppon/default.aspx_paragraph1" runat="server" />
                    </div>
                    <div class="inside_AD" style="display: none;">
                        <ucR:RandomParagraph ID="insideAD" runat="server" OnInit="RandomPponNewsInit" />
                    </div>
                    <div id="active_block" style="margin-bottom: 10px">
                        <ucR:RandomParagraph ID="active" runat="server" OnInit="RandomPponNewsInit" />
                    </div>
                    <div id="news_block">
                        <ucR:RandomParagraph ID="news" runat="server" OnInit="RandomPponNewsInit" />
                    </div>

                    <%=LunchKingSite.WebLib.Component.WebHelper.PageBlock("新好友限定好康") %>

                    <div id="saleB_block">
                        <ucR:RandomParagraph ID="saleB" runat="server" OnInit="RandomPponNewsInit" />
                    </div>

                    <div id="Sidedeal" class="side_block">
                        <div class="side_block_title">
                            <i class="fa fa-star fa-fw"></i>今日熱銷
                        </div>
                        <div class="side_item_wrap">
                            <%
                                int hotIndex = 0;
                                foreach (var item in ViewMultipleHotDeals)
                                {
                                    hotIndex += 1;
                            %><div class="side_item">
                                <a class="open_new_window" href="<%=ResolveUrl(string.Format("~/{0}/{1}", item.deal.CityID, item.deal.PponDeal.BusinessHourGuid))%>" target="_blank">
                                    <%if (item.EveryDayNewDeal != PponFacade._BLANK_IMAGE_SRC) {%>
                                    <div class="item_sitcker">
                                        <img src="<%=item.EveryDayNewDeal %>" />
                                    </div>
                                    <%} %>
                                    <div class="item_activeLogo" style="display: block">
                                        <%= item.deal.PponDeal.PromoImageHtml %>
                                    </div>
                                    <div class="side_item_pic">
                                        <span class="num_hot"><%=hotIndex%></span>
                                        <img src="<%=item.deal.PponDeal.DefaultDealImage %>" alt="<%=(hotIndex>5)?(string.IsNullOrEmpty(item.deal.PponDeal.PicAlt))?(string.IsNullOrEmpty(item.deal.PponDeal.AppTitle))? item.deal.PponDeal.CouponUsage:item.deal.PponDeal.AppTitle:item.deal.PponDeal.PicAlt:string.Empty %>" />
                                        <span class="side_item_soldout_wrap" style='<%= ((item.deal.PponDeal.OrderedQuantity >= item.deal.PponDeal.OrderTotalLimit) ? "": "display: none")%>'>
                                            <div class="item_soldout_220">
                                            </div>
                                        </span>
                                    </div>

                                    <div class="side_item_deal">
                                        <span class="tag_place">
                                            <%=item.clit_CityName2 %>
                                        </span>
                                        <div class="tag_subtitle"><%=item.deal.PponDeal.ItemName%></div>
                                    </div>

                                    <div class="side_item_price">
                                        <span class="discount" style="color: rgb(153, 153, 153);">
                                            <%=item.discount_1 %>
                                        </span>
                                        <span class="price" style="color: rgb(191, 0, 0);">$<%= CheckZeroPriceToShowPrice(item.deal.PponDeal).ToString("F0")%>元<%= (item.deal.PponDeal.BusinessHourStatus & (int)(BusinessHourStatus.ComboDealMain)) > 0 ? "起" : string.Empty%></span></div>
                                </a>
                            </div>
                            <%
                                }
                            %>
                        </div>
                    </div>

                    <div class="side_block">
                        <div class="side_block_title">
                            <i class="fa fa-facebook-square fa-fw"></i>17Life粉絲團
                        </div>
                        <div class="side_block_ad">
                            <div id="FacebookareaContent" style="padding-bottom: 0px">
                                <iframe id="fbfans" data-src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2F17life.com.tw&amp;send=false&amp;layout=standard&amp;width=230&amp;show_faces=true&amp;action=like&amp;colorscheme=light&amp;font=arial&amp;height=80"
                                    scrolling="no" frameborder="0" style="border: none; overflow: hidden; width: 230px; padding-bottom: 0px; padding-left: 2px; height: 70px;"
                                    allowtransparency="true"></iframe>
                                <br />
                                <a href="https://www.facebook.com/17life.com.tw" target="_blank">
                                    <img src="<%= ResolveUrl("~/Themes/PCweb/images/goto_fbpage.png")%>" width="202" height="28"
                                        border="0" alt="" />
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="side_block">
                        <div class="side_block_title">
                            <i class="fa fa-users fa-fw"></i>合作提案
                        </div>
                        <div class="side_block_ad">
                            <div id="Shopareatitle">
                                <ul>
                                    <li style="font-size:18px;font-weight:bold;">手牽手，力量比較大</li>
                                    <li>17life 竭力在尋找各種優質、有趣的店家及產品，為我們的平台帶來更多生命力。</li>
                                    <li>若您有任何有趣的合作方案，我們都非常歡迎並誠摯的期待來自你的提案。</li> 
                                    <li>如對廣告行銷、公益合作有興趣者，也歡迎跟我們提案噢。</li> 
                                </ul>
                            </div>
                            <div style="width:100%;text-align:center;margin-top:15px;margin-bottom:5px;">
                                <a class="btn btn-primary btn-primary-flat " style="font-size:18px;" href="<%=SystemConfig.SiteUrl+"/Ppon/ContactUs"%>">填寫提案表</a>
                            </div>
                        </div>
<%--                        <div class="side_block_ad">
                            <div id="Div4">
                                歡迎各類企業及店家與我們合作，您只要準備最優質的產品/服務，以及消費者至上的誠意，就可以在17Life的平台上，精準行銷，一起賣到翻~
                        <br />
                                <br />
                                如對廣告行銷、公益合作有興趣者，也歡迎跟我們提案。
                            </div>
                            <a class="ShopBtn" href="<%=SystemConfig.SiteUrl%>/Ppon/ContactUs.aspx"></a>
                            <p id="ShopinnerIMG">
                                將一卡車的客戶送到您面前
                            </p>
                        </div>--%>
                    </div>
                </div>
            </div>
            <div id="Referral" style="display: none">
                <div id="ReturnClose" style="cursor: pointer; margin-top: 3px; margin-right: 3px"
                    onclick="$.unblockUI();return false;">
                </div>
                <div id="ReferralContent">
                    <div class="Referralinvite">
                        <h1>邀請親友好康有獎</h1>
                    </div>
                    <div id="ReferralTitle">
                        <span class="icon-smile-o-2"></span>
                        <p>以下是您的專屬邀請連結</p>
                    </div>
                    <!--ReferralTitle-->
                    <div id="ReferralLinkarea">
                        <asp:TextBox ID="tbx_ReferralShare" ClientIDMode="Static" runat="server" ReadOnly="true"></asp:TextBox>
                    </div>
                    <div id="ReferralStep" style="text-align: left;">
                        <li>
                            <span class="list-step">步驟1</span>
                            複製上面的邀請連結
                        </li>
                        <li>
                            <span class="list-step">步驟2</span>
                            把連結傳給好友
                        </li>
                        <li>
                            <span class="list-step">步驟3</span>
                            親友點連結後完成首次購買並核銷，宅配商品過鑑賞期且無退貨
                        </li>
                        <li>
                            <span class="list-step">步驟4</span>
                            您拿到推薦獎勵，折價券500元
                            <span class="list-warning">(請注意使用期限)</span>
                        </li>
                    </div>

                    <div id="Referralexplain">
                        <li>您可使用下圖fb分享，或是複製分享連結並轉貼，</li>
                        <li>
                            <span class="list-point">邀請次數無上限～</span>
                            分享越多賺越多喔！
                        </li>
                        <li>
                            <span class="list-point">訂單金額50元(含)以下恕不適用此折價券贈送活動，詳見<a target="_blank" href="<%=ResolveUrl("~/Ppon/NewbieGuide.aspx?id=share")%>">常見問題</a>
                            </span>
                        </li>
                    </div>

                    <div id="referral_FB_area">
                        <% if (!SystemConfig.NewShareLinkEnabled)
                            { %>
                        <asp:HyperLink ID="hhf" runat="server" Target="_blank">
                            <img src="<%= ResolveUrl("~/Themes/PCweb/images/FB_Btn.png")%>" />
                        </asp:HyperLink>
                        <% }
                            else
                            { %>
                        <a onclick="fbShareWindow = window.open(this.href, 'fbShareWindow', 'width=630,height=360','left=' + fbJumpWindowLeft + ',top=' + fbJumpWindowTop);ShareFB();" style="cursor: pointer;">
                            <img src="<%= ResolveUrl("~/Themes/PCweb/images/FB_Btn.png")%>" />
                        </a>
                        <% } %>
                    </div>
                </div>
            </div>
            <div id="blockMap" class="Multi-grade-Setting" style="height: 100%; border: 0px; display: none;">
                <div id="map" style="height: 100%; background: transparent;">
                    <iframe id="iMap" style="width: 100%; height: 100%;"></iframe>
                </div>
                <div class="MGS-XX" onclick="blockUIClose(this);" style="cursor: pointer;">
                    <input type="button" class="btn btn-large btn-primary" value="關閉">
                </div>
            </div>
            <div style="clear: both">
            </div>
        </asp:PlaceHolder>
        <div id="newpoplogin" style="display: none">
            <uc1:LoginPanel ID="LoginPanel1" runat="server" />
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="LastLoadJs" runat="server">

</asp:Content>

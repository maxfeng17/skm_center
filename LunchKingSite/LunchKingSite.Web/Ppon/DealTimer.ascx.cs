﻿using System.Text;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core;
using LunchKingSite.Core.Models;
using LunchKingSite.DataOrm;
using System;
using LunchKingSite.I18N;
using LunchKingSite.BizLogic.Facade;

namespace LunchKingSite.Web.Ppon
{
    public partial class DealTimer : System.Web.UI.UserControl
    {
        public IViewPponDeal PponDeal { get; set; }

        public bool TwentyFourHours { get; set; }

        private bool isMBrowerStyle;
        public bool IsMBrowerStyle
        {
            get { return isMBrowerStyle; }
            set { isMBrowerStyle = value; }
        }

        private bool enableBuyCounter = true;
        public bool EnableBuyCounter
        {
            get { return enableBuyCounter; }
            set { enableBuyCounter = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        public override void DataBind()
        {
            if (PponDeal != null)
            {
                SetStatus();
                pan_BuyCounter.Visible = EnableBuyCounter;
                if (isMBrowerStyle)
                {
                    timerTitleField.Attributes["class"] = "forlist";
                }
                else
                {
                    timerTitleField.Attributes["class"] = "forlist TimerTitleField";
                }
            }
        }

        private void SetStatus()
        {
            PponDealStage dealStage = PponDeal.GetDealStage();
            timerTitleField.Attributes["data-start"] = "0";
            timerTitleField.Attributes["data-end"] = "0";

            switch (dealStage)
            {
                case PponDealStage.Created:
                case PponDealStage.Ready:
                    lblStatus.Text = "好康尚未開始<br/>";
                    litTime.Text = "即將開賣";
                    break;

                case PponDealStage.ClosedAndFail:
                    lblStatus.Text = "未達門檻，團購不成立!";
                    litTime.Text = "已結束販售";
                    break;

                case PponDealStage.ClosedAndOn:
                case PponDealStage.CouponGenerated:
                    lblStatus.Text = OrderedQuantityHelper.Show(PponDeal, OrderedQuantityHelper.ShowType.MainDealClosedAndOn);
                    litTime.Text = "已結束販售";
                    break;

                default:
                    //if (TwentyFourHours)
                    //{
                    //    SetCountdown(PponDeal.BusinessHourOrderTimeE, PponDeal.BusinessHourOrderTimeS);
                    //}
                    //else
                    //{
                    //    SetCountdown(PponDeal.BusinessHourOrderTimeE);
                    //}

                    if (PponFacade.IsEveryDayNewDeal(PponDeal.BusinessHourGuid))
                    {
                        EveryDayNewDeal flag = ViewPponDealManager.DefaultManager.GetEveryDayNewDealByBid(PponDeal.BusinessHourGuid);
                        DateTime end = DateTime.Now;
                        if (flag == EveryDayNewDeal.Limited24HR)
                            end = PponDeal.BusinessHourOrderTimeS.AddDays(1);
                        else if (flag == EveryDayNewDeal.Limited48HR)
                            end = PponDeal.BusinessHourOrderTimeS.AddDays(2);
                        else if (flag == EveryDayNewDeal.Limited72HR)
                            end = PponDeal.BusinessHourOrderTimeS.AddDays(3);
                        else
                            end = PponDeal.BusinessHourOrderTimeE;

                        int daysToClose = (int)(end - DateTime.Now).TotalDays;


                        if (daysToClose > 3)
                        {
                            litTime.Text = "限時優惠中!";
                        }
                        else
                        {
                            litDays.Text = daysToClose + "天";
                            litTime.Text = SetCountdown(end);
                            timerTitleField.Attributes["data-end"] =
                                PponDeal.BusinessHourOrderTimeE.ToJavaScriptMilliseconds().ToString();
                            timerTitleField.Attributes["data-start"] =
                                PponDeal.BusinessHourOrderTimeS.ToJavaScriptMilliseconds().ToString();
                            timerTitleField.Attributes["data-everydaynewdeal-end"] =
                                end.ToJavaScriptMilliseconds().ToString();
                            timerTitleField.Attributes["data-everydaynewdeal"] = "True";
                        }
                    }
                    else
                    {
                        int daysToClose = (int)(PponDeal.BusinessHourOrderTimeE - DateTime.Now).TotalDays;

                        if (daysToClose > 3)
                        {
                            litTime.Text = "限時優惠中!";
                        }
                        else
                        {
                            litDays.Text = daysToClose + "天";
                            litTime.Text = SetCountdown(PponDeal.BusinessHourOrderTimeE);
                            timerTitleField.Attributes["data-end"] =
                                PponDeal.BusinessHourOrderTimeE.ToJavaScriptMilliseconds().ToString();
                            timerTitleField.Attributes["data-start"] =
                                PponDeal.BusinessHourOrderTimeS.ToJavaScriptMilliseconds().ToString();
                            timerTitleField.Attributes["data-everydaynewdeal"] = "False";
                        }
                    }
                    
                    if (dealStage == PponDealStage.Running)
                    {
                        lblStatus.Text = OrderedQuantityHelper.Show(
                            PponDeal, OrderedQuantityHelper.ShowType.MainDealRunning);
                    }
                    else if (dealStage == PponDealStage.RunningAndOn)
                    {
                        lblStatus.Text = OrderedQuantityHelper.Show(PponDeal, OrderedQuantityHelper.ShowType.MainDealRunningAndOn);
                    }
                    else
                    {
                        lblStatus.Text = OrderedQuantityHelper.Show(PponDeal, OrderedQuantityHelper.ShowType.MainDealRunningAndFull);
                    }
                    break;
            }
        }

        public static string SetCountdown(DateTime timeE)
        {
            TimeSpan ts = timeE - DateTime.Now;
            int hourUntil = ts.Hours;
            int minutesUntil = ts.Minutes;
            int secondsUntil = ts.Seconds;

            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("<div class='TimeConDigit hn'>{0}</div>", hourUntil);
            sb.AppendFormat("<div class='TimeConUnit hl'>{0}</div>", Phrase.Hour);
            sb.AppendFormat("<div class='TimeConDigit mn'>{0}</div>", minutesUntil);
            sb.AppendFormat("<div class='TimeConUnit ml'>{0}</div>", "分");
            sb.AppendFormat("<div class='TimeConDigit sn'>{0}</div>", secondsUntil);
            sb.AppendFormat("<div class='TimeConUnit sl'>{0}</div>", Phrase.Second);
            return sb.ToString();
        }
    }
}
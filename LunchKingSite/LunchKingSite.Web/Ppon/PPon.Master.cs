﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.BizLogic.Models;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.Web.Themes.controls;
using LunchKingSite.WebLib.Component;
using LunchKingSite.WebLib.UI;
using LunchKingSite.WebLib.Views;

namespace LunchKingSite.Web.Ppon
{
    public partial class Ppon : MemberMasterPage
    {
        public bool Subscription { get; set; }
        public static ISysConfProvider Config = ProviderFactory.Instance().GetConfig();
        public int PopUpEdm = (int)EventActivityType.PopUpEdm;
        public int PopUp = (int)EventActivityType.PopUp;

        private int? selectedCityId;

        public bool IsShowBanner { get; set; }

        public bool IsShowCuration { get; set; }

        protected string EventActivityHtmlBody { get; set; }
        protected int EventActivityId { get; set; }
        protected bool ShowEventActivity { get; set; }
        protected int EventActivityOpenTarget { get; set; }

        /// <summary>
        /// 城市CityId
        /// </summary>
        public int SelectedCityId
        {
            get
            {
                if (selectedCityId == null)
                {
                    string absoluteUri = Request.Url.AbsoluteUri.ToLower();

                    int cityId = new PponDealListCondition(absoluteUri).CityId;

                    if (Page.RouteData.Values["ch"] != null & cityId == CategoryManager.pponCityId.Taipei)
                    {
                        int channel = 0;
                        int.TryParse(Page.RouteData.Values["ch"].ToString(), out channel);
                        selectedCityId = CategoryManager.GetCityIdByChannelId(channel);
                    }
                    else
                    {
                        selectedCityId = (cityId == 0) ? CategoryManager.pponCityId.Taipei : cityId;
                    }
                }
                return selectedCityId.Value;
            }
        }

        /// <summary>
        /// 頻道CategoryId
        /// </summary>
        public int SelectedChannelId
        {
            get
            {
                int channelId = 0;
                if (Page.RouteData.Values["ch"] != null && Page.RouteData.Values["ch"].ToString()!="100001")
                {
                    int.TryParse(Page.RouteData.Values["ch"].ToString(),out channelId);
                }
                else
                {
                    channelId = CategoryManager.GetChannelIdByCityId(this.SelectedCityId);
                }

                return channelId;
            }
        }

        /// <summary>
        /// 地區categoryId
        /// </summary>
        public int SelectedRegionId
        {
            get
            {
                int regionId = 0;
                string path = Request.Path;
                path = (path != null) ? path.ToLower().Replace("delivery/", "") : path;
                if (!string.IsNullOrWhiteSpace(path) && !path.Contains("ppon/default.aspx"))
                {
                    Match matchChar = Regex.Match(path, "[a-z]+", RegexOptions.IgnoreCase);//不含英文字元

                    if (!matchChar.Success)
                    {
                        var splitArray = path.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
                        if (splitArray.Length > 1)
                        {
                            if (!splitArray[1].Contains(",") && splitArray[0] != "channel")
                            {
                                //ex. /199/94 、 /356/95 、 ...
                                Int32.TryParse(splitArray[1], out regionId);
                            }
                        }
                    }
                    else
                    {
                        var splitArray = path.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
                        if (splitArray.Length > 1)
                        {
                            if (splitArray[1].Contains(",") && splitArray[0] == "channel")
                            {
                                var splitComma = splitArray[1].Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                                if (splitComma.Length > 1)
                                {
                                    Int32.TryParse(splitComma[1], out regionId);
                                }
                            }
                            else
                            {
                                if (Request.QueryString["aid"] != null)
                                {
                                    Int32.TryParse(Request.QueryString["aid"].ToString(), out regionId);
                                }
                            }
                        }
                    }
                }

                if (regionId == 0)
                {
                    regionId = CategoryManager.GetDefaultRegionIdByChannelId(this.SelectedChannelId);
                }
                return regionId;
            }
        }

        /// <summary>
        /// 區域子分類categoryId
        /// </summary>
        public int SelectedSubcategoryId
        {
            get
            {
                //categoryId = regionId，代表沒有選子分類，只選到區域
                int categoryId = this.SelectedRegionId;

                if (!String.IsNullOrEmpty(Request.QueryString["subc"]))
                {
                    string subc = Request.QueryString["subc"];
                    Int32.TryParse(subc, out categoryId);
                }

                return categoryId;
            }
        }

        /// <summary>
        /// 新版分類Id
        /// </summary>
        public int SelectedDealCategoryId
        {
            get
            {
                int dealCategoryId = 0;
                string path = Request.Path;
                path = (path != null) ? path.ToLower().Replace("delivery/", "") : path;
                if (!string.IsNullOrWhiteSpace(path) && !path.Contains("ppon/default.aspx"))
                {
                    //ex. /Ppon/Default.aspx 或 199/{Guid} 或 199/94 
                    Match matchChar = Regex.Match(path, "[a-z]+", RegexOptions.IgnoreCase);

                    if (!matchChar.Success)
                    {
                        //ex.  /199/94/801 、 /356/95/801 、 ...(舊版)
                        var splitArray = path.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
                        if (splitArray.Length > 2)
                        {
                            Int32.TryParse(splitArray[2], out dealCategoryId);
                        }
                    }
                    else
                    {
                        var splitArray = path.Split(new char[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
                        //ex.  /channel/199,94,801 、 /channel/356,95,801 、 ...(DeepLink)
                        if (splitArray.Length > 1)
                        {
                            if (splitArray[1].Contains(","))
                            {
                                var splitComma = splitArray[1].Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                                if (splitComma.Length > 2)
                                {
                                    Int32.TryParse(splitComma[2], out dealCategoryId);
                                }
                            }
                        }
                    }
                }

                if (dealCategoryId == 0)
                {
                    if (!string.IsNullOrEmpty(Request.QueryString["cat"]))
                    {
                        int.TryParse(Request.QueryString["cat"], out dealCategoryId);
                    }
                }

                return dealCategoryId;
            }
        }

        /// <summary>
        /// 當前City
        /// </summary>
        /// <remarks>
        /// 用來替換掉原本控制項LocationSelectHorizon的SelectedCity
        /// 待確認是否有此property必要
        /// </remarks>
        public PponCity SelectedPponCity
        {
            get
            {
                return PponCityGroup.DefaultPponCityGroup.GetPponCityByCityId(this.SelectedCityId);
            }
            set
            {
                //目前設定改成透過Url去做，此setter只是讓compile可以過
                this.SelectedPponCity = value;
            }
        }

        public AgentChannel ChannelAgent
        {
            get { return ChannelFacade.GetOrderClassificationByHeaderToken(); }
        }

        //public MasterPageTopBanner PponNews = new MasterPageTopBanner("pponnews");
        public MasterPageTopBanner PponNews = new MasterPageTopBanner("banner-swiper-top");

        public bool IsNewMobileSetting
        {
            get { return Config.IsNewMobileSetting; }
        }

        public string UserName
        {
            get
            {
                string name = "";
                if (Page.User != null && Page.User.Identity.IsAuthenticated)
                {
                    name= Page.User.Identity.Name;
                    name = name.ToLower();
                }
                return name;
            }
        }

        public bool IsMobileBroswer
        {
            get
            {
                return CommonFacade.ToMobileVersion();
            }
        }

        public Guid QueryBussinessHour
        {
            get
            {
                string bid_querystring = string.Empty;
                Guid bGuid = Guid.Empty;
                if (Page.RouteData.Values["bid"] != null)
                {
                    bid_querystring = Page.RouteData.Values["bid"].ToString();
                }
                else if (!string.IsNullOrEmpty(Request.QueryString["bid"]))
                {
                    bid_querystring = Request.QueryString["bid"];
                }

                if (!string.IsNullOrEmpty(bid_querystring))
                {
                    if (bid_querystring.Split(',').Length > 1)
                    {
                        bid_querystring = bid_querystring.Split(',')[0];
                    }
                    if (!Guid.TryParse(bid_querystring, out bGuid))
                    {
                        bGuid = Guid.Empty;
                    }
                }
                return bGuid;
            }
        }

        public int QueryBusinessUniqueId
        {
            get
            {
                string uid_querystring = string.Empty;
                int uid;
                if (Page.RouteData.Values["uid"] != null)
                {
                    uid_querystring = Page.RouteData.Values["uid"].ToString();
                }
                else if (!string.IsNullOrEmpty(Request.QueryString["uid"]))
                {
                    uid_querystring = Request.QueryString["uid"];
                }

                if (!string.IsNullOrEmpty(uid_querystring) && int.TryParse(uid_querystring, out uid))
                {
                    return uid;
                }
                else
                {
                    return -1;
                }
            }
        }

        public Dictionary<decimal, int> DiscountList
        {
            get
            {
                Dictionary<decimal, int> discount = new Dictionary<decimal, int>();
                if (Page.User.Identity.IsAuthenticated)
                {
                    HttpCookie cookie;
                    if (Response.Cookies.AllKeys.Contains(LkSiteCookie.MemberDiscount.ToString()))
                    {
                        cookie = Response.Cookies[LkSiteCookie.MemberDiscount.ToString()];
                    }
                    else
                    {
                        cookie = Request.Cookies[LkSiteCookie.MemberDiscount.ToString()];
                    }
                    if (cookie != null)
                    {
                        var cookieval = new JsonSerializer().Deserialize<CookieModel>(Helper.Decrypt(cookie.Value));
                        if (cookieval.Value.ToString().IndexOf('#') > 0)
                        {
                            string[] discountinfo = cookieval.Value.ToString().Split('|');
                            foreach (string item in discountinfo)
                            {
                                string[] tmp = item.Split('#');
                                if (tmp.Count() == 2)
                                {
                                    decimal amount = decimal.TryParse(tmp[0], out amount) ? amount : 0;
                                    int count = int.TryParse(tmp[1], out count) ? count : 0;
                                    discount.Add(amount, count);
                                }
                            }
                        }
                    }
                    else
                    {
                        discount = PromotionFacade.GetMemberDiscountCode(Page.User.Identity.Name);
                        CookieManager.SetMemberDiscount(discount);
                    }
                }
                return discount;
            }
        }

        public int QueryCityId
        {
            get
            {
                string cid_querystring = string.Empty;
                int result;
                if (Page.RouteData.Values["cid"] != null)
                {
                    cid_querystring = Page.RouteData.Values["cid"].ToString();
                }
                else if (!string.IsNullOrEmpty(Request.QueryString["cid"]))
                {
                    cid_querystring = Request.QueryString["cid"];
                }

                if (!string.IsNullOrEmpty(cid_querystring) && int.TryParse(cid_querystring, out result))
                {
                    return result;
                }
                else
                {
                    return -1;
                }
            }
        }

        public AgentChannel FixChannelAgent
        {
            get
            {
                int agent;
                if (int.TryParse(hidChannelAgent.Value, out agent))
                {
                    return (AgentChannel) agent;
                }

                return AgentChannel.NONE;
            }
        }

        public int PayWayLimit
        {
            get
            {
                string temp;
                if (Page.RouteData.Values["pwl"] != null)
                {
                    temp = Page.RouteData.Values["pwl"].ToString();
                }
                else if (!string.IsNullOrEmpty(HttpContext.Current.Request.QueryString["pwl"]))
                {
                    temp = HttpContext.Current.Request.QueryString["pwl"];
                }
                else
                {
                    return 0;
                }

                if (!SystemConfig.PayWayLimitPaymentType.Split(',').Any()) return 0;

                int payWayLimit;
                if (!int.TryParse(temp, out payWayLimit))
                {
                    return 0;
                }

                return !SystemConfig.PayWayLimitPaymentType.Split(',').Select(int.Parse).Contains(payWayLimit) ? 0 : payWayLimit;
            }
        }

        public string MasterPageType
        {
            get
            {
                if (!string.IsNullOrEmpty(Request.QueryString["pType"]))
                {
                    Session[LkSiteSession.MasterPageType.ToString()] = Request.QueryString["pType"];
                }
                return Session[LkSiteSession.MasterPageType.ToString()] != null ? Session[LkSiteSession.MasterPageType.ToString()].ToString() : string.Empty;
            }
        }

        public bool ViewPortEnabled
        {
            set
            {
                ((site)base.Master).ViewPortEnabled = value;
            }
        }
        public decimal DiscountAmount
        {
            set;
            get;
        }
        public int GiftCount
        {
            set;
            get;
        }
        public int CardCount
        {
            set;
            get;
        }
        
        protected void Page_Init(object sender, EventArgs e)
        {
            if (Request.UserAgent != null && Request.UserAgent.Contains("iPad") && Request.UserAgent.Contains("CriOS"))
            {
                Page.ClientTarget = "uplevel";
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Default_SetUp();
                if (ChannelAgent == AgentChannel.TaiShinMall && hidChannelAgent.Value == "0")
                {
                    hidChannelAgent.Value = Convert.ToString((int)AgentChannel.TaiShinMall);
                }
            }

            if (Page.User != null && Page.User.Identity.IsAuthenticated)
            {
                SetupUserInfoPanel();
            }

            if (Request.Cookies[LkSiteCookie.ReferrerSourceId.ToString()] != null)
            {
                ReferrerSourceId.Value = Request.Cookies[LkSiteCookie.ReferrerSourceId.ToString()].Value;
            }

            if (SystemConfig.PayWayLimitEnable && !string.IsNullOrEmpty(SystemConfig.PayWayLimitPaymentType))
            {
                RegisterPayWayLimit(HttpContext.Current);
            }

            #region for blockUI subscription

            Setup();
            //if (Request.Cookies["EventEmailCookie"] == null)
            //    SetCookie(GetEventMailCookieDaysType(EventMailCookieDaysType.Default));

            #endregion

            if (FixChannelAgent == AgentChannel.TaiShinMall && Request.Headers["Authorization"] == null)
            {
                Request.Headers.Add("Authorization", string.Format("Bearer {0}", Config.TaiShinMallToken));
            }
        }

        protected void Default_SetUp()
        {
            SetUserDiscountPanel();

            if (Page.AppRelativeVirtualPath.ToLower().IndexOf("piinlife", StringComparison.OrdinalIgnoreCase) >= 0)
            {
                BindPiinlifeCategory();
            }

            #region 行銷滿額贈活動
            if (Config.IsEnabledSpecialConsumingDiscount)
            {
                bool isPiinlife = Page.AppRelativeVirtualPath.ToLower().IndexOf("/piinlife", StringComparison.OrdinalIgnoreCase) >= 0 || Request.QueryString["style"] == "pl";
                // 不分區or活動設定為品生活且當前為品生活館or活動設定為p好康且當前非品生活
                if (Config.SpecialConsumingDiscountCid == 0 ||
                    (isPiinlife && Config.SpecialConsumingDiscountCid == PponCityGroup.DefaultPponCityGroup.Piinlife.CategoryId) ||
                    (!isPiinlife && Config.SpecialConsumingDiscountCid != PponCityGroup.DefaultPponCityGroup.Piinlife.CategoryId))
                {
                    DateTime dateStart = DateTime.Parse(Config.SpecialConsumingDiscountPeriod.Split('|')[0]);
                    DateTime dateEnd = DateTime.Parse(Config.SpecialConsumingDiscountPeriod.Split('|')[1]);
                    if (DateTime.Now > dateStart && DateTime.Now < dateEnd)
                    {

                        ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowRWDPromoBanner", "$('.center').prepend($('.evtbar'));", true);
                    }
                }

            }
            #endregion
        }

        private void SetUserDiscountPanel()
        {
            liDiscountCount.Text = liMDiscountCount.Text = DiscountList.Sum(x => x.Value).ToString();
            rptDiscount.DataSource = DiscountList.OrderByDescending(x => x.Key);
            rptDiscount.DataBind();
        }

        private void SetupUserInfoPanel()
        {
            liMemberName.Text = this.MemberName.TrimToMaxLength(12,"...");
            var userName = Page.User.Identity.Name;
            var userId = MemberFacade.GetUniqueId(userName);

            switch (Config.DiscountUserCollectType)
            {
                case DiscountUserCollectType.Manually:
                    DiscountAmount = OrderFacade.GetUnsentDiscountUserList(userId).Sum(x => x.Amount.Value * x.Qty);
                    break;
                case DiscountUserCollectType.Auto:
                    //檢查是否有未領折價券，並領取
                    List<string> newCodes = OrderFacade.SendUncollectedDiscountList(userId, userName);
                    if (newCodes.Count > 0)
                    {
                        //清掉折價券cookie
                        Request.Cookies.Remove(LkSiteCookie.MemberDiscount.ToString());
                        //更新顯示折價券數量
                        SetUserDiscountPanel();
                    }
                    break;
                default:
                    break;
            }

            if (Config.MGMEnabled)
            {
                divMGMInfo.Visible = true;
                dropdownListMGM.Visible = true;
                var giftCount = MGMFacade.MemberFastInfoGetUserId(userId);
                GiftCount = giftCount.GiftCardCount ?? 0;
                CardCount = giftCount.ReplyCardCount ?? 0;         
                if (CardCount > 0)
                {
                    DivGiftCard.Visible = true;
                }
                if (giftCount.MessageCount > 0)
                {
                    DivGiftCard.Visible = false;
                    DivGiftinfo.Visible = true;
                }
            }

        }

        private void BindPiinlifeCategory()
        {
            CategoryNode channel = CategoryManager.Default.Piinlife;
            CategoryDealCountNode node = PponDealPreviewManager.GetCategoryDealCountNodeByChannel(channel.CategoryId, null);

            // 新增一項預設分類
            CategoryDealCount allDealCount = new CategoryDealCount(0, "全部");
            allDealCount.DealCount = node.TotalCount;
            node.DealCountList.Insert(0, allDealCount);

            List<CategoryDealCount> filterNodes =
                PponDealPreviewManager.GetSpecialDealCountListByChannel(channel.CategoryId, null).Where(x => x.DealCount > 0).ToList();

            // 過濾沒有檔次的分類
            node.DealCountList = node.DealCountList.Where(x => x.DealCount > 0).ToList();
            filterNodes = filterNodes.Where(x => x.DealCount > 0).ToList();

            // 第一個分類為全部，從第二個位置insert即買即用等特殊分類
            if (node.DealCountList.Count > 0)
            {
                node.DealCountList.InsertRange(1, filterNodes);

                Func<CategoryDealCount, bool> checkVisa =
                    x => CategoryManager.GetDealLabelSystemCodeByCategoryId(x.CategoryId) == DealLabelSystemCode.Visa;

                rptHD_Category.DataSource = node.DealCountList.OrderBy(x => checkVisa(x)).ToList();
                rptHD_Category.DataBind();
            }
        }

        private void BindTravelLocationSelected()
        {
            List<int> dealCategories = PponFacade.GetDealAllCategories(QueryBussinessHour).Select(x => x.Cid).ToList();
            int dealFirstTravelCategory = dealCategories.Intersect(CategoryManager.CategoryGetListByType(CategoryType.PponChannelArea).Select(x => x.Id)).DefaultIfEmpty(0).Last();
            //LocationSelected.SetTravelCategoryCookie(dealFirstTravelCategory, 1);
            //LocationSelected.RepDataBind();
        }

        protected void RandomPponMasterInit(object sender, EventArgs e)
        {
            masterbanner.CityId = curation.CityId = SelectedCityId;
            masterbanner.Type = curation.Type = RandomCmsType.PponMasterPage;
        }

        #region for blockUI subscription

        IEventProvider ep = ProviderFactory.Instance().GetProvider<IEventProvider>();
        ICmsProvider cmp = ProviderFactory.Instance().GetProvider<ICmsProvider>();
        public EventActivity activity;
        public string FacebookAppId { get { return Config.FacebookApplicationId; } }
        public string downloadAppUrl
        {
            get
            {
                if (Request.UserAgent != null && Request.UserAgent.ToLower().Contains("android"))
                {
                    return Config.SiteUrl + "/Ppon/OpenApp.aspx";
                }
                else if (Request.UserAgent != null && (Request.UserAgent.ToLower().Contains("iphone") || Request.UserAgent.ToLower().Contains("ipad") || Request.UserAgent.ToLower().Contains("ipod")))
                {
                    return "open17life://www.17life.com";
                }
                return "#";
            }
        }
        public static ISysConfProvider SystemConfig
        {
            get { return Config; }
        }

        public enum EventMailCookieDaysType
        {
            Default,
            Subscribe,
            Skip
        }

        public int CityId
        {
            get
            {
                HttpCookie cookie = Request.Cookies.Get(LkSiteCookie.CityId.ToString("g"));
                int cityid;
                if (cookie != null && int.TryParse(cookie.Value, out cityid))
                    return cityid;
                else
                    return -1;
            }
        }

        public bool Close { get; set; }

        protected void Setup()
        {
            int eventActivityId;
            string htmlBody;
            int openTarget;
            ShowEventActivity = EventFacade.TryGetEventActivityToShow(true, Request.Cookies["EventEmailCookie"], 
                out eventActivityId, out htmlBody, out openTarget);
            if (ShowEventActivity)
            {
                EventActivityHtmlBody = htmlBody;
                HfEventId.Value = eventActivityId.ToString();
                EventActivityId = eventActivityId;
                EventActivityOpenTarget = openTarget;
            }
            else
            {
                HfEventId.Value = "0";
                EventActivityId = 0;
            }
        }

        #endregion

        public void DiscountListRefresh(Dictionary<decimal, int> DiscountList)
        {
            liDiscountCount.Text = liMDiscountCount.Text = DiscountList.Sum(x => x.Value).ToString();
            rptDiscount.DataSource = DiscountList.OrderByDescending(x => x.Key);
            rptDiscount.DataBind();
        }

        private void RegisterPayWayLimit(HttpContext context)
        {
            var payWayLimit = PayWayLimit;
            var bid = QueryBussinessHour;
            if (payWayLimit == 0 || bid == Guid.Empty) return;

            HttpCookie cookie = Request.Cookies[LkSiteCookie.PayWay.ToString()];
            List<PayWayLimit> pwList = new List<PayWayLimit>();
            if (cookie != null && !String.IsNullOrEmpty(cookie.Value))
            {                   
                pwList = new JsonSerializer().Deserialize<List<PayWayLimit>>(cookie.Value);
                pwList.RemoveAll(x => x.Expires < DateTime.Now);
            }

            var pwItem = pwList.FirstOrDefault(x => x.Bid == bid && x.PaymentType == payWayLimit);
            if (pwItem != null)
            {
                pwItem.Expires = DateTime.Now.AddMinutes(SystemConfig.PayWayLimitExpiresMinute);
            }
            else
            {
                pwList.Add(new PayWayLimit
                {
                    Bid = QueryBussinessHour,
                    PaymentType = payWayLimit,
                    Expires = DateTime.Now.AddMinutes(SystemConfig.PayWayLimitExpiresMinute)
                });
            }

            string cookieValue = new JsonSerializer().Serialize(pwList);
            //限制長度不得大於 2048, 瀏覽器cookie長度chrome 4076 IE 4095 firefox 4096
            while (cookieValue.Length > 2048)
            {
                pwList.Remove(pwList.OrderBy(x => x.Expires).First());
                cookieValue = new JsonSerializer().Serialize(pwList);
            }

            cookie = CookieManager.NewCookie(
                LkSiteCookie.PayWay.ToString(),
                new JsonSerializer().Serialize(pwList),
                DateTime.Now.AddMinutes(SystemConfig.PayWayLimitExpiresMinute)
            );
            context.Response.SetCookie(cookie);
        }

    }
}

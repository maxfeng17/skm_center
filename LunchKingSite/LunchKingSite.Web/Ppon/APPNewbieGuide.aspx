﻿<%@ Page Title="" Language="C#" AutoEventWireup="true"
    CodeBehind="APPNewbieGuide.aspx.cs" Inherits="LunchKingSite.Web.Ppon.APPNewbieGuide" %>


<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>17Life</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

    <link href="<%=ResolveUrl("~/Themes/PCweb/css/ie6.css")%>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/Themes/PCweb/css/MasterPage.css")%>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/Themes/PCweb/css/ppon.css")%>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/Themes/PCweb/css/ppf-tw.css")%>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/themes/PCweb/css/RDL-L.css")%>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/themes/PCweb/css/RDL-M.css")%>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/themes/PCweb/css/RDL-S.css")%>" rel="stylesheet" type="text/css" />

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8/jquery.min.js" type="text/javascript"></script>
    <script src="//code.jquery.com/ui/1.10.4/jquery-ui.js" type="text/javascript"></script>

    <script type="text/javascript">
        $(function () {
            $("#accordion").accordion({ header: '.fq-qtitle', active: false, collapsible: true, heightStyle: "content" });
            $("#accordion").show();
            $('.faqArea ul li').css({
                'line-height': '48px',
                'border-bottom': '1px solid #ccc',
                'padding-left': '20px'
            });
        });

        //FAQ-M版分類展開收合 選單可以一展開另一自動收合
        $(document).ready(function () {
            $(".faq").click(function () {
                if (!$('.faq').is('.rdl-faqarrow-on')) {
                    $(".faqArea").slideUp(500);//全部先收起來
                    $(".faqMail").slideUp(500);//全部先收起來
                    $(".faq").addClass("rdl-faqarrow-on"); //faq上箭頭
                }
                else {
                    $(".faqArea").slideUp(500);//全部先收起來
                    $(".faqMail").slideUp(500);//全部先收起來
                    $(".faqArea").slideDown(500);//點下去的這個打開
                    $(".faq").removeClass("rdl-faqarrow-on"); //faq下箭頭
                }
            });
            
            checkQueryString();
        });

        function checkQueryString() {
            var sec = '<%=Request.QueryString["s"]%>'
            var qid = '<%=Request.QueryString["q"]%>'
            if (sec != '' && qid != '') {
                $('.fq-inlbox ul li[id=li' + sec + ']').addClass('on');
                $('.ubx' + qid +' > .fq-info').show();
                location.hash = '#anc' + qid;
            }
            else {
                $('.fq-inlbox ul li:first').addClass('on');
            }
        }

    </script>
    <style type="text/css">
        .fq-questionBOX img, .fq-phonebox {
            display: inline-block !important;
        }
    </style>
</head>

<body>
    <div class="fq_box">
        <!--faq-M 版分類 Start-->
        <div class="rdl-fqtitle-box rdl-faqcol-1-2">
            <ul>
                <li style="width: 100%;" class="rdl-faqarrow-down rdl-faqarrow-on faq ">常見問題<span class="icon-chevron-faq"></span></li>
            </ul>
        </div>
        <div class="mbe-switch mbe-switch2 rdl-faqdis">
            <!--常見問題-->
            <div class="faqArea">
                <asp:Literal ID="LinkSectionM" runat="server"></asp:Literal>
                <div class="clearfix"></div>
            </div>

           
            <!--faqArea -->


        </div>
        <!--faq-M 版分類 end-->

        <div class="fq-Rbox">
            <div class="fq-inlbox">
                <div class="qtitle">
                    <img src="../Themes/PCweb/images/FAQtitlepic_11.png" alt="" />
                </div>
                <asp:Literal ID="LinkSection" runat="server"></asp:Literal>

                <div class="etitle linetop" style="display: none">
                    <img src="../Themes/PCweb/images/FAQtitlepic_13.png" alt="" />
                </div>
                <div class="etitle linetop" style="display: none">
                    <img src="../Themes/PCweb/images/FAQtitlepic_15.png" alt="" />
                </div>
                <div class="etitle linetop" style="display: none">
                    <img src="../Themes/PCweb/images/FAQtitlepic_14.png" alt="" />
                </div>
                <div class="etitle" style="display: none">
                    <img src="../Themes/PCweb/images/FAQtitlepic_16.png" alt="" />
                </div>
                <ul style="display: none">
                    <li>查詢購買記錄</li>
                    <li>會員認證</li>
                </ul>
            </div>
        </div>

        <div class="fq-Lbox" id="accordion">
            <asp:Literal ID="ContentSection" runat="server"></asp:Literal>
        </div>
        <!--fq-Lbox-->
    </div>
    <!--fq_box-->


</body>
</html>

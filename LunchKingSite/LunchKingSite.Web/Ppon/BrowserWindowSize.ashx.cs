﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;

namespace LunchKingSite.Web.Ppon
{
    /// <summary>
    /// Summary description for BrowserWindowSize1
    /// </summary>
    public partial class BrowserWindowSize : IHttpHandler,System.Web.SessionState.IRequiresSessionState
    {
        private ISysConfProvider conf;
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "application/json";
            conf = ProviderFactory.Instance().GetConfig();
            var json = new System.Web.Script.Serialization.JavaScriptSerializer();
            string output;

            if (!conf.IsMobileWindowSizeSession)
            {
                output = json.Serialize(new { isFirst = false });
                context.Response.Write(output);
                return;
            }

            try
            {
                output = json.Serialize(new { isFirst = context.Session["BrowserWidth"] == null });
                context.Response.Write(output);

                context.Session["BrowserWidth"] = context.Request.QueryString["Width"];
                //context.Session["BrowserHeight"] = context.Request.QueryString["Height"];
            }
            catch (Exception)
            {
                output = json.Serialize(new { isFirst = false });
                context.Response.Write(output);
            }
        }

        public bool IsReusable
        {
            get { throw new NotImplementedException(); }
        }
    }
}
﻿using System;
using System.Text;
using System.Web;
using System.Web.Services;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.I18N;
using LunchKingSite.WebLib;
using LunchKingSite.WebLib.Presenters;

namespace LunchKingSite.Web.Ppon
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        public int Bonus
        {
            get
            {
                return 50;
            }
        }

        private IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        private IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        private ICmsProvider cp = ProviderFactory.Instance().GetProvider<ICmsProvider>();
        private ViewPponDeal pponDeal;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["bid"]))
            {
                pponDeal = pp.ViewPponDealGetByBusinessHourGuid(new Guid(Request.QueryString["bid"]));
            }
            else
            {
                var deals = pp.ViewPponDealGetListByCityIdOrderBySlotSeq(Master.SelectedCityId, new DeliveryType[] { DeliveryType.ToHouse, DeliveryType.ToShop });
                if (deals == null || deals.Count == 0)
                {
                    Response.Redirect("~/ppon");
                    return;
                }
                pponDeal = deals[0];
            }

            if (Page.User != null && Page.User.Identity.IsAuthenticated)
            {
                p1.Visible = true;
                p2.Visible = false;

                MemberLinkCollection mlc = mp.MemberLinkGetList(User.Identity.Name);

                string origUrl = WebUtility.GetSiteRoot() + "/ppon/PponReferralRedirection.aspx?bid=" + pponDeal.BusinessHourGuid + "," + mlc[0].ExternalOrg + "|" + mlc[0].ExternalUserId;
                string shortenedUrl = WebUtility.RequestShortUrl(origUrl);
                PromotionFacade.SetDiscountReferrerUrl(origUrl, shortenedUrl, MemberFacade.GetUniqueId(HttpContext.Current.User.Identity.Name), HttpContext.Current.User.Identity.Name, pponDeal.BusinessHourGuid, 0);
                tbReferral.Text = shortenedUrl;
                tbReferral.Attributes.Add("onfocus", "this.select()");

                hf.NavigateUrl = "http://www.facebook.com/share.php?u=" + tbReferral.Text;
                hp.NavigateUrl =
                    "javascript: void(window.location.replace('http://www.plurk.com/?qualifier=shares&status=' .concat(encodeURIComponent('" +
                    tbReferral.Text + "')) .concat(' (') .concat(encodeURIComponent('" +
                    I18N.Phrase.ReferralShareShowForPushLink + "')) .concat(')')));";
                hm.NavigateUrl = string.Format("mailto:?subject={0}&body={1}",
                                                HttpUtility.UrlEncode(Encoding.Convert(Encoding.Unicode, Encoding.GetEncoding(950),
                                                                                        Encoding.Unicode.GetBytes(Phrase.LetUsP))),
                                                HttpUtility.UrlEncode(Encoding.Convert(Encoding.Unicode, Encoding.GetEncoding(950),
                                                                                        Encoding.Unicode.GetBytes(Phrase.PponInfoIsHere +
                                                                                            " " + tbReferral.Text + "\r\n " +
                                                                                            Phrase.PponIsBetterWithYou))));
            }
            else
            {
                p1.Visible = false;
                p2.Visible = true;
                hl.NavigateUrl = WebUtility.GetPayEasySsoPath().Replace(".aspx", "%2Dp.aspx");
                ISysConfProvider config = ProviderFactory.Instance().GetConfig();
                hj.NavigateUrl = WebUtility.Get17LifeRegisterPath(!config.BypassSsoVerify);
            }
        }

        [WebMethod]
        public static string PostToFaceBook()
        {
            bool success = (new DefaultPponPresenter()).AddUserTrack("", "", "/ppon/referral.aspx", "", "FaceBook");
            if (success)
            {
                return "Y";
            }
            else
            {
                return string.Empty;
            }
        }
        [WebMethod]
        public static string PostToPlurk()
        {
            bool success = (new DefaultPponPresenter()).AddUserTrack("", "", "/ppon/referral.aspx", "", "Plurk");
            if (success)
            {
                return "Y";
            }
            else
            {
                return string.Empty;
            }
        }
    }
}
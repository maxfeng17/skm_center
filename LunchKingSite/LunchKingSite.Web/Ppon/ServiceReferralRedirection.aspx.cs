﻿using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.I18N;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;
using System;
using System.Web;
using System.Web.UI;

namespace LunchKingSite.Web.Ppon
{
    public partial class ServiceReferralRedirection : System.Web.UI.Page
    {
        private static ISysConfProvider cp = ProviderFactory.Instance().GetConfig();

        public string ServicePara
        {
            get
            {
                string ServicePara = string.Empty;
                if (!string.IsNullOrEmpty(Request.QueryString["ServicePara"]))
                {
                    ServicePara = Request.QueryString["ServicePara"];
                }
                return ServicePara;
            }
        }

        public string WebRoot
        {
            get
            {
                return cp.SiteUrl;
            }
        }

        public int ServiceType
        {
            get
            {
                int serviceType = 0;

                if (!string.IsNullOrEmpty(Request.QueryString["ServiceType"]))
                {
                    int.TryParse(Request.QueryString["ServiceType"], out serviceType);
                }

                return serviceType;
            }
        }

        public string AndroidUserAgent
        {
            get
            {
                return "/" + cp.AndroidUserAgent + "/i";
            }
        }

        public string iOSUserAgent
        {
            get
            {
                return "/" + cp.iOSUserAgent + "/i";
            }
        }



        protected void Page_Load(object sender, EventArgs e)
        {
            if (ServiceType != 0 && !string.IsNullOrEmpty(ServicePara))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "goto",
                                                    "appPromo('" + WebRoot + "/User/ServiceConversation?IssueType=" + ServiceType + "&no=" + ServicePara + "');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "window.location.href='" + WebRoot + "/User/ServiceList" + "';", true);
            }

        }
    }
}
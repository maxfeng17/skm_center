﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<%@ page language="C#" autoeventwireup="true" enableviewstate="false" inherits="LunchKingSite.Web.User.coupon_detail" %>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>商家紙本憑證</title>
    <%@ register src="AvailabilityCtrl.ascx" tagname="AvailabilityCtrl" tagprefix="uc1" %>
    <link id="coupon_print" rel="stylesheet" href="../Themes/default/images/17Life/G2/coupon_print_V2.css" type="text/css" />
</head>
<body>
    <% if (!ForceStaticMap)
       { %>
    <style type="text/css" media="screen">
        #Manual_Map_Print
        {
            visibility: hidden;
            display: none;
        }
    </style>
    <style type="text/css" media="print">
        #Manual_Map
        {
            visibility: hidden;
            display: none;
        }
    </style>
    <% } %>
    <div class="Coupon">
        <div class="Coupon_number">
            <asp:Literal ID="StoreSequenceCode" runat="server" Visible="false">序號(僅排序用)：</asp:Literal>
            <asp:Literal ID="CSSC" runat="server" />
        </div>
        <div class="Coupon_Header">
            <div class="Coupon_Logo">
                <img src="../Themes/default/images/17Life/G2/17PLOGO_BW_HD.png" width="260" height="80"
                    alt="" /></div>
            <div class="Coupon_Barcode">
                條碼 & QR Code:<br />
                <asp:Image ID="iVC" runat="server" Width="250" Height="36" CssClass="Coupon_Barcode" />
            </div>
            <div class="Coupon_QRcode">
                <asp:Image ID="iQC" runat="server" Width="80px" Height="80px" />
            </div>
            <div class="Clear">
            </div>
        </div>
        <div class="Coupon_Seriel">
            <div class="SN_serial">
                <asp:Literal ID="lCS" runat="server" /></div>
            <div class="SN_confirmcode">
                <asp:Literal ID="liCouponCode" runat="server" /></div>
            <div class="Clear">
            </div>
            <div class="Coupon_SN_Note FontBold" style="font-size: 14px; padding-top: 3px; margin-left: 0px">
                <asp:Label ID="lab_Notice2" runat="server"></asp:Label>
            </div>
            <div class="Coupon_SN_Note" style="font-size: 14px; padding-top: 3px; margin-left: 0px">
                <asp:Label ID="lab_Notice" runat="server"></asp:Label>
            </div>
        </div>
        <div class="Coupon_Info">
            <% if (IsZeroActivityShowCoupon)
               { %>
                親愛的【
            <% } else { %>
                訂購人：
            <% } %>
            <asp:Literal ID="lU" runat="server" />
            <% if (IsZeroActivityShowCoupon) { %>
                    】您好：
            <% } %>
            <br />
            <% if (!IsZeroActivityShowCoupon)
               { %>
<%--            優惠有效期間：
            <asp:Literal ID="lUS" runat="server" />
            ~ <span id="StrikeThroughDecoration" runat="server"><span style="color: Black;">
                <asp:Literal ID="lUE" runat="server" />
            </span></span>
            <br />--%>
            <span id="ChangeExpireDateContent" runat="server" style="color: Red;">有效期間更改至：<asp:Literal
                ID="ChangedExpireDate" runat="server" />
            </span>
            <% } %>
        </div>
        <div class="Coupon_Shop">
            <% if (IsZeroActivityShowCoupon)
               { %>
            感謝您參與【
            <% } %>
            <asp:HyperLink ID="hN" runat="server" />
            <% if (IsZeroActivityShowCoupon)
               { %>
            】活動！
            <% } %>
        </div>
        <div class="Coupon_Title">
            <asp:HyperLink ID="hNB" runat="server" /></div>
        <% if (IsZeroActivityShowCoupon)
               { %>
         <div class="Coupon_Info">
            使用期限：
            <asp:Literal ID="lus2" runat="server" />
            ~ <span id="StrikeThroughDecoration2" runat="server"><span style="color: Black;">
                <asp:Literal ID="lue2" runat="server" />
            </span></span>
            <br />
            <span id="ChangeExpireDateContent2" runat="server" style="color: Red;">使用期限更改至：<asp:Literal
                ID="ChangedExpireDate2" runat="server" />
            </span></div>
            <% } %>
        <div class="Coupon_Title" style="line-height:35px">
            <asp:Literal ID="litDealTags" runat="server"></asp:Literal>
        </div>
        <div class="Coupon_Note">
            好康注意事項：
            <div style="padding-left: 14px">
            <div>
                <asp:Literal ID="lV" runat="server" /></div>
            <asp:Literal ID="lR" runat="server" />
            <asp:Panel ID="pan_Trust" runat="server">
                ●此憑證由等值購物金兌換之<br />
                <asp:Literal ID="liTaishin" runat="server" Visible="false">
                ●信託期間：發售日起一年內<br />
                ●本商品(服務)禮券所收取之金額，已存入發行人於台新銀行開立之信託專戶，專款專用。所稱專用，係指供發行人履行交付商品或提供服務義務使用<br />
                </asp:Literal>
                <asp:Literal ID="liHwatai" runat="server" Visible="false">
                ●信託期間：發售日起一年內<br />
                ●本商品(服務)禮券所收取之金額，已存入發行人於華泰銀行開立之信託專戶，專款專用。所稱專用，係指供發行人履行交付商品或提供服務義務使用<br />
                </asp:Literal>
                <asp:Literal ID="liMohist" runat="server" Visible="false">
                ●信託期間：發售日起一年內<br />
                ●本商品(服務)禮券所收取之金額，已存入發行人於陽信銀行開立之信託專戶，專款專用。所稱專用，係指供發行人履行交付商品或提供服務義務使用<br />
                ●信託履約禮券查詢網址 http://trust.sunnybank.com.tw/ (於收到兌換券後次日10點起即可查詢)<br />
                </asp:Literal>
                <asp:Literal ID="liEntrustSell" runat="server" Visible="false">
                ●代收轉付收據將於核銷使用後開立<br />
                </asp:Literal>
            </asp:Panel>
            <asp:Label runat="server" Text="●本憑證不得與其他優惠合併使用，且恕無法兌換現金及找零<br />●好康憑證不限本人使用，使用時店家將會核對憑證編號，一組編號限用一次<br />"
                Font-Size="12px"></asp:Label>
                 <asp:Literal runat="server" ID ="liGroupCoupon" Visible="false">
                    ●成套票券商品為整套/組購入，故僅限於所有套/組憑證均兌換完畢才能享有完整優惠<br />
                    ●當有退貨情事發生時，將依其整套/組進貨規則進行剩餘面額價值進行退款<br />
                </asp:Literal>
            </div>
        </div>
        <div class="Shop_Note">
            <asp:Panel ID="pan_ShopInfo" runat="server">
                商店資訊：
                <span class="Shop_Name">
                    <asp:Literal ID="lSl" runat="server" Visible="false" /></span>
                <ul style="padding-left: 30px; margin-top: 5px;">
                    <uc1:AvailabilityCtrl ID="avc" runat="server">
                        <itemtemplate>                 
                                    <asp:Literal ID="n" runat="server" />
                                    <asp:Literal ID="p" runat="server" />
                                    <asp:Literal ID="a" runat="server" />
                                    <asp:Literal ID="r" runat="server" />
                                    <asp:Literal ID="op" runat="server" />
                                    <asp:Literal ID="liVehicle" runat="server" />
                                    <asp:Literal ID="mr" runat="server" />
                                    <asp:Literal ID="ca" runat="server" />
                                    <asp:Literal ID="bu" runat="server" />
                                    <asp:Literal ID="ov" runat="server" />
                        
                            </itemtemplate>
                    </uc1:AvailabilityCtrl>
                </ul>
            </asp:Panel>
            <br />
            17Life客服專線：<%=config.ServiceTel %> (平日 09:00 ~ 18:00)
        </div>
    </div>
    <div id="Manual" style="display: none">
        <div id="Manual_Content">
            <div id="Manual_Note">
                <div id="Manual_Note_Title">
                    如何使用好康憑證?</div>
                <asp:Literal ID="lUse" runat="server" />
            </div>
            <div id="Manual_Location">
                <div id="Manual_Shop">
                    <span id="Manual_Shop_Title">商店資訊：</span>
                    <%--  <uc1:AvailabilityCtrl ID="avc" runat="server">
                        <itemtemplate>
                <ul>
                    <asp:Literal ID="n" runat="server" />
                    <asp:Literal ID="p" runat="server" />
                    <asp:Literal ID="a" runat="server" />
                    <asp:Literal ID="r" runat="server" />
                    <asp:Literal ID="op" runat="server" />
                    <asp:Literal ID="liVehicle" runat="server" />
                    <asp:Literal ID="mr" runat="server" />
                    <asp:Literal ID="ca" runat="server" />
                    <asp:Literal ID="bu" runat="server" />
                    <asp:Literal ID="ov" runat="server" />
                </ul>
            </itemtemplate>
                    </uc1:AvailabilityCtrl>--%>
                </div>
                <% if (!string.IsNullOrEmpty(MapAddress))
                   {
                       if (!ForceStaticMap)
                       { %>
                <div id="Manual_Map" style="width: 300px; height: 250px;">
                </div>
                <% } %>
                <div id="Manual_Map_Print">
                    <img src="https://maps.google.com/maps/api/staticmap?sensor=false&size=300x250&center=<%=MapAddress%>&zoom=16&language=zh-TW&markers=<%=MapAddress%>" />
                </div>
                <% } %>
                <span></span>
                <span>17Life客服專線：<%=config.ServiceTel %> (平日 09:00 ~ 18:00)</span>
            </div>
        </div>
        <div id="Manual_Clear">
        </div>
    </div>
    <% if (!ForceStaticMap)
       { %>
    <script type="text/javascript" src="//www.google.com/jsapi?key=<%=GoogleApiKey%>"></script>
    <script type="text/javascript">
    //<![CDATA[
    var timeout = 2000;
    google.load("maps", "3", { 'other_params': 'sensor=false&language=zh-TW' });

    function drawMap(addr, title, mapId) {
        if (addr == '')
            return;

        var geocoder = new google.maps.Geocoder();
        if (geocoder) {
            geocoder.geocode({ 'address': addr, 'region': 'tw' }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    var options = {
                        zoom: 16,
                        center: results[0].geometry.location,
                        mapTypeId: google.maps.MapTypeId.ROADMAP
                    };
                    var map = new google.maps.Map(document.getElementById(mapId), options);
                    var marker = new google.maps.Marker({
                        position: results[0].geometry.location,
                        map: map,
                        title: title
                    });
                }

                google.maps.event.addListener(map, 'tilesloaded', function () {
                    if (geocoder != null)
                        setTimeout("window.print()", timeout)
                    geocoder = null;
                });
            });
        }
    }
    google.setOnLoadCallback(function () {
    <% if (!string.IsNullOrEmpty(MapAddress)) { %>
        drawMap('<%=MapAddress%>','<%=lSl.Text%>','Manual_Map');
    <% } else {%>
        setTimeout("window.print()", timeout);
    <% } %>
    });
    //]]>
    </script>
    <% } %>
</body>
</html>

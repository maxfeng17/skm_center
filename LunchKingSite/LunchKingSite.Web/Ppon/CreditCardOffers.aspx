﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Ppon/Ppon.Master" AutoEventWireup="true" 
    CodeBehind="CreditCardOffers.aspx.cs" Inherits="LunchKingSite.Web.Ppon.CreditCardOffers" %>

<%@ MasterType VirtualPath="~/PPon/PPon.Master" %>
<%@ Import Namespace="LunchKingSite.BizLogic.Component" %>
<%@ Import Namespace="LunchKingSite.DataOrm" %>
<%@ Import Namespace="LunchKingSite.Core" %>
<%@ Import Namespace="LunchKingSite.BizLogic.Facade" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <link href='<%= ResolveUrl("../themes/PCweb/css/Rightside.css")%>' rel="stylesheet" type="text/css" />
    <link href='<%= ResolveUrl("../themes/PCweb/css/RDL-L.css") %>' rel="stylesheet" type="text/css" />
    <link href='<%= ResolveUrl("../themes/PCweb/css/RDL-M.css") %>' rel="stylesheet" type="text/css" />
    <link href='<%= ResolveUrl("../themes/PCweb/css/RDL-S.css") %>' rel="stylesheet" type="text/css" />
    <link href='<%= ResolveUrl("../themes/PCweb/css/RDL-Pad.css") %>' rel="stylesheet" type="text/css" />
    <%=LunchKingSite.Core.Helper.RenderCss("/Themes/PCweb/css/ppon_item.min.css") %>
    <style type="text/css">        

    	.bank_item img {
    		width:unset !important;
			height:unset !important;
    	}
    	.bank_content {
			word-break: break-all;
    	}
	</style>
    <script type="text/javascript" src="<%= ResolveUrl("../Tools/js/jquery.smartbanner.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("../Tools/js/homecook.js")%>"></script>
    <script type="text/javascript" src="<%= ResolveUrl("../Tools/js/osm/OpenLayers.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("../Tools/js/jquery-sliding-menu.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("../Tools/js/TouchSlide.1.1.source.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("../Tools/js/jquery.SuperSlide.2.1.1.js")%>"></script>
    <script type="text/javascript">
		window.imageVer = "<%=Helper.GetImageVer()%>";
		$(function () {
			$("#navi").hide();
			$('.topbn_bank_wrap').html(
				`<div class="topbn_bank">
					<div class="link_17life">
						<a href="/ppon/default.aspx"><img src="../Themes/PCweb/images/ppon-M2_LOGO.png" alt="17Life"></a>
					</div>
					<img src="https://www.17life.com/images/17P/17topbn/bank/bank_960.jpg?` + window.imageVer + `">
				</div>`
			);
			$('.topbn_bank_wrap').css('background',
				'url(https://www.17life.com/images/17P/17topbn/bank/bank_1920.jpg?' +
				window.imageVer + ') center no-repeat');
			$('.topbn_bank_wrap').show();
		});
		$(document).ready(function () {
			$('.grid_bank .bank_item').find('a').click(function () {
				// 取得要前往的 id
				var _id = $(this).attr('href');
				//console.log("this_id:"+_id);      
				$('html, body').animate({
					scrollTop: $(_id).position().top
				}, 'slow');
				return false;
			});
		});
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MC" runat="server">
    <asp:Literal ID="lt_DealContent"  runat="server" />   
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="LastLoadJs" runat="server">
    
</asp:Content>


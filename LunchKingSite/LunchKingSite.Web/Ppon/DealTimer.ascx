﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DealTimer.ascx.cs" Inherits="LunchKingSite.Web.Ppon.DealTimer" %>
<div class="TimerTitleField" id="timerTitleField" runat="server">
    <span class="icon-clock-o"></span>
    <div class="TimerField">
        <div class="TimeDayCounter" >
        <asp:Literal ID="litDays" runat="server" />
    </div>
        <div class="TimeCounter"><asp:Literal ID="litTime" runat="server" Mode="PassThrough" /></div>
    </div>
</div>
<!--Buycounter-->
<asp:Panel ID="pan_BuyCounter" CssClass="buycounter" runat="server">
    <span class="icon-user"></span>
    <asp:Literal ID="lblStatus" runat="server"></asp:Literal>
</asp:Panel>

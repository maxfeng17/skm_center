﻿<%@ Page Language="C#" MasterPageFile="~/Ppon/Ppon.Master" AutoEventWireup="true"
    CodeBehind="Promo.aspx.cs" Inherits="LunchKingSite.Web.Ppon.Promo" %>

<%@ MasterType VirtualPath="~/Ppon/Ppon.Master" %>
<%@ Register Src="~/UserControls/Paragraph.ascx" TagName="Paragraph"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            $(".mc-menu-btn").hide();
            $(".mbe-menu2").hide();
            <% if (ShowEventEmail)
               { %>
                showSubscription();
            <% } %>
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MC" runat="server">
    <uc1:Paragraph ID="pMain" ExternalCache="true" runat="server" />
</asp:Content>

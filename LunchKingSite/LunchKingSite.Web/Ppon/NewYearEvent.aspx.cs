﻿using System;
using System.Web;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core.Component;
using LunchKingSite.Core;

namespace LunchKingSite.Web.Ppon
{
    public partial class NewYearEvent : System.Web.UI.Page
    {
        public ISysConfProvider SystemConfig
        {
            get { return ProviderFactory.Instance().GetConfig(); }
        }
    }
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Ppon/Ppon.Master" AutoEventWireup="true"
    CodeBehind="NewbieGuide.aspx.cs" Inherits="LunchKingSite.Web.Ppon.NewbieGuide" EnableViewState="false" %>

<%@ Import Namespace="LunchKingSite.BizLogic.Component" %>


<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <link href="../Themes/default/images/17Life/G1-8/17UG.css" rel="stylesheet" type="text/css" />
    <link href="../Themes/default/images/17Life/G3/PPF.css" rel="stylesheet" type="text/css" />
    <link href="../Themes/PCweb/css/service.css" rel="stylesheet" type="text/css" />
    <link href="../Themes/PCweb/css/ppf-tw.css" rel="stylesheet" type="text/css" />
    <link href="../themes/PCweb/css/RDL-L.css" rel="stylesheet" type="text/css" />
    <link href="../themes/PCweb/css/RDL-M.css" rel="stylesheet" type="text/css" />
    <link href="../themes/PCweb/css/RDL-S.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .question_content {
            display:none;
        }

        .ui-accordion-header-active {
            background:url(/Themes/PCweb/images/arrow_black_down.png) right center no-repeat !important;}
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#accordion").accordion({
                header: '.fq-qtitle',
                active: 0,
                collapsible: true,
                heightStyle: "content",
                activate: function(event, ui) {
                    var q = ui.newHeader.parent().data('q');
                    var s = ui.newHeader.parent().data('s');
                    if (q != null && s != null && $.urlParam('q') != q) { //判斷若是回上頁或下頁就不記
                        window.history.pushState("", "", "/ppon/newbieguide.aspx?s=" + s + "&q=" + q);
                    }
                }
            });

 

            $(".fq-inlbox li").click(function () {
                //var li_service_num = $(this).attr('class').substr(-2); //選項被選到的編號 
                var li_service = $(this).attr('class');              
                if (li_service) {
                    var li_service_num = $(this).attr('class').replace("li_service_", "").replace(" on", "");
                    //console.log("li_service="+li_service_num); 
                    $(".fq-inlbox li").removeClass('on');
                    $(this).addClass('on');
                    $(".question_content").css('display', 'none'); //隱藏左側內容區塊
                    $(".question_" + li_service_num).css('display', 'block'); //顯示左側內容區塊被選取的區塊   
                }
                var s = $(this).data('s');
                if (s!=null && li_service.indexOf('on') == -1) {
                    window.history.pushState('', '', '/ppon/newbieguide.aspx?s=' + s);
                }
            });



            /*RWD 常見問題與其他服務 頁籤切換*/
            $("#tabsA").tabs();

            /*RWD列表點擊 隱藏列表 顯示下方內容*/
            $(".rwd_service_wrap .service_content_list li").click(function () {
                var li_service = $(this).attr('class');
                if (li_service != "undefined") {
                    var li_service_num = $(this).attr('class').replace("li_service_", "").replace(" on", "");
                    $(".fq-inlbox li").removeClass('on');
                    $(".fq-inlbox li.li_service_" + li_service_num).addClass('on');
                    $(".question_content").css('display', 'none'); //隱藏左側內容區塊
                    $(".question_" + li_service_num).css('display', 'block'); //顯示左側內容區塊被選取的區塊 
                    $(".rwd_service_wrap").css('display', 'none'); //隱藏列表
                    $(".fq-Lbox").css('display', 'block'); //顯示下方內容
                }

            });

            /*RWD的"回客服首頁按鈕"點擊後 顯示列表 隱藏下方內容*/
            $(".rwd_btn_service_list").click(function () {
                $(".rwd_service_wrap").css('display', 'block'); //顯示 RWD列表
                $(".fq-Lbox").css('display', 'none'); //隱藏 content
                $(".fq-Rbox").css('display', 'none'); //隱藏 web選單
            });


            //resize時判斷 顯示RWD或是web
            $(window).resize(function () {
                now_width = $(window).width();
                if ($("#width").val() != now_width) {
                    $("#width").val(now_width);
                    if (now_width > 1000) {
                        //console.log(">=1000~~~")
                        $('.rwd_service_wrap').css('display', 'none'); //RWD選單隱藏
                        $('.fq-Rbox').css('display', 'block'); //web選單 顯示
                        $('.fq-Lbox').css('display', 'block'); //content 顯示
                    }
                    if (now_width <= 1000) {
                        //console.log("<=1000~~~")
                        $('.rwd_service_wrap').css('display', 'block'); //RWD選單隱藏
                        $('.fq-Rbox').css('display', 'none'); //web選單 顯示
                        $('.fq-Lbox').css('display', 'none'); //content 顯示
                    }
                }
            });
            now_width = $(window).width();
            $("#width").val(now_width);
            if (now_width > 1000) {
                //console.log(">=1000~")
                $('.rwd_service_wrap').css('display', 'none'); //RWD選單隱藏
                $('.fq-Rbox').css('display', 'block'); //web選單 顯示
                $('.fq-Lbox').css('display', 'block'); //content 顯示
            }
            if (now_width <= 1000) {
                //console.log("<=1000~")
                $('.rwd_service_wrap').css('display', 'block'); //RWD選單隱藏
                $('.fq-Rbox').css('display', 'none'); //web選單 顯示
                $('.fq-Lbox').css('display', 'none'); //content 顯示
            }


            if (window.history && window.history.pushState) {
                $(window).on('popstate', function () {
                    var dataS= $.urlParam('s');
                    var $target = $(".fq-inlbox ul").find(`[data-s='${dataS}']`);
                    if ($target[0]) {
                        var li_service = $target.attr('class');
                        if (li_service != "undefined") {
                            $(".fq-inlbox li").removeClass('on');
                            $target.addClass('on');
                            $(".question_content").css('display', 'none'); //隱藏左側內容區塊
                            $(".question_" + dataS).css('display', 'block'); //顯示左側內容區塊被選取的區塊 
                            var dataQ = $.urlParam('q');
                            //mustPushState = false;
                            var $questionTarget = $(".question_" + dataS).find(`[data-q='${dataQ}']`); //question box的值
                            if ($questionTarget[0]) {
                                $("#accordion").accordion("option",
                                    "active",
                                    parseInt($questionTarget.attr("data-accordionindex"), 10));
                            } else {
                                $("#accordion").accordion("option", "active", -1); //收合
                            }
                        }
                    }
                });
            }

            $.urlParam = function (name) {
                var results = new RegExp('[\?&]' + name + '=([^&#]*)')
                    .exec(window.location.search);

                return (results !== null) ? results[1] || 0 : false;
            }

        });




        //查詢購買紀錄
        function sendCouponListMail() {
            var userEmail = $('#userEmail').val();
            if (userEmail == '') {
                alert('請輸入E-mail!');
                return false;
            }

            if (checkAccount(userEmail)) {
                $('.errorEmailMsg').hide();
                var duringSelected = $('.rbDuring:checked').val();
                var duringString = $('.rbDuring:checked').attr('text');
                $.ajax({
                    type: "POST",
                    url: "NewbieGuide.aspx/SendCouponListMail",
                    data: '{"userEmail":"' + userEmail + '","duringSelected":"' + duringSelected + '","duringString":"' + duringString + '"}',
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        alert(data.d.Msg);
                    }
                });
                return true;
            } else {
                $('.errorEmailMsg').show();
                return false;
            }
        }

        //發送認證信件
        function sendAuthMail() {
            var userEmail = $('#AuthuserEmail').val();
            if (userEmail == '') {
                alert('請輸入E-mail!');
                return false;
            }

            if (checkAccount(userEmail)) {
                $('.errorEmailMsg').hide();
                reSendAuthMail();
                return true;
            } else {
                $('.errorEmailMsg').show();
            }
            return false;
        }

        function reSendAuthMail() {
            var userEmail = $('#AuthuserEmail').val();
            $.ajax({
                type: "GET",
                url: "../api/Ppon/ReSendAuthMail?email=" + userEmail,
                //data: '{"userEmail":"' + userEmail + '"}',
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    alert(data.Message);
                }
            });
        }
    </script>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MC" runat="server">
    <div id="PC">
        <div class="fq_box">
            <input type="hidden" id="width">
            <div class="rwd_service_wrap">
                <!--// tabs //-->
                <div id="tabsA">
                    <ul class="service_menu">
                        <li><a href="#tabs-1" class="">常見問題</a></li>
                        <li><a href="#tabs-2">其他服務</a></li>
                    </ul>

                    <!--// RWD列表 //-->
                    <div class="service_content_list">
                        <div class="tabs" id="tabs-1">
                            <asp:Literal ID="TopLinkSection" runat="server" Mode="PassThrough"></asp:Literal>
                            <div class="ad_link clearfix">
                                <div class="service_link">
                                    <a href="/User/ServiceList">
                                        <p class="service_title">聯絡客服</p>
                                        <img src="https://www.17life.com/images/17P/active/service/bn_service.jpg">
                                        <p>立即聯絡客服人員，讓我們幫助你！</p>
                                    </a>
                                &nbsp;</div>
                                <div class="contactus_link">
                                    <a href="/Ppon/ContactUs">
                                        <p class="service_title">合作提案</p>
                                        <img src="https://www.17life.com/images/17P/active/service/bn_contactus.jpg">
                                        <p>想打造品牌知名度，還是快速提升銷售率？立即加入17Life為您打開網路購物大門。</p>
                                    </a>
                                &nbsp;</div>
                            </div>
                        </div>
                        <div class="tabs" id="tabs-2" style="">
                            <ul>
                                <li class="li_service_record">
                                    <a href="javascript:void(0)">查詢購買紀錄
                                <i class="fa fa-angle-right fa-lg fa-fw"></i>
                                    </a>
                                </li>
                                <li class="li_service_certification">
                                    <a href="javascript:void(0)">會員認證
                                <i class="fa fa-angle-right fa-lg fa-fw"></i>
                                    </a>
                                </li>
                            </ul>
                            <div class="ad_link clearfix">
                                <div>
                                    <a href="/User/ServiceList">
                                        <p class="service_title">聯絡客服</p>
                                        <img src="https://www.17life.com/images/17P/active/service/bn_service.jpg">
                                        <p>立即聯絡客服人員，讓我們幫助你！</p>
                                    </a>
                                &nbsp;</div>
                                <div>
                                    <a href="/Ppon/ContactUs">
                                        <p class="service_title">合作提案</p>
                                        <img src="https://www.17life.com/images/17P/active/service/bn_contactus.jpg">
                                        <p>想打造品牌知名度，還是快速提升銷售率？立即加入17Life為您打開網路購物大門。</p>
                                    </a>
                                &nbsp;</div>
                            </div>
                        </div>
                    </div>
                    <!--// RWD列表//end-->
                </div>
                <!--// tabs //end-->
                <div class="clearfix"></div>
            </div>

            <div class="fq-Rbox">
                <div class="fq-inlbox">
                    <ul>
                        <div class="li_service_title">常見問題</div>
                        <asp:Literal ID="LinkSection" runat="server" Mode="PassThrough"></asp:Literal>
                    </ul>
                    <ul>
                        <div class="li_service_title">其他服務</div>
                        <li class="li_service_record" data-s="record">查詢購買紀錄</li>
                        <li class="li_service_certification" data-s="certification">會員認證</li>
                    </ul>

                    <div class="li_service_title">聯絡客服</div>
                    <a class="link_img" href="/User/ServiceList">
                        <img src="https://www.17life.com/images/17P/active/service/bn_service.jpg">
                        <p>立即聯絡客服人員，讓我們幫助你！</p>
                    </a>

                    &nbsp;<div class="li_service_title">合作提案</div>
                    <a class="link_img" href="/Ppon/ContactUs">
                        <img src="https://www.17life.com/images/17P/active/service/bn_contactus.jpg" alt="">
                        <p>想打造品牌知名度，還是快速提升銷售率？立即加入17Life為您打開網路購物大門。</p>
                    </a>
                &nbsp;</div>
            </div>
            
           
            <div class="fq-Lbox" id="accordion">

                <asp:Literal ID="ContentSection" runat="server"></asp:Literal>
                <div class="question_content question_record">

                <div class="service service_detail">

                    <div class="service">

                        <div class="form">

                            <h1 class="title">查詢購買記錄<a href="javascript: void(0)" class="rwd_btn_service_list">回客服首頁</a></h1>
                            <div class="col100" id="divLoginPanel" runat="server">
                                <p>若您已是會員，建議您直接<a href='<%=ResolveUrl(FormsAuthentication.LoginUrl) %>'>登入17Life</a> ，有更詳細的資訊哦！</p>
                            </div>
                            <div>
                                <br />
                                <h2 class="rd-payment-h2">
                                    <div class="step-num-dot">1</div>
                                    選擇查詢期間</h2>
                                <div class="grui-form">
                                    <div class="form-unit rd-pay-margin">
                                        <div class="data-input">
                                            <ul class="permutation">
                                                <li>
                                                    <input class="rbDuring" type="radio" name="rbGroup" value="3" text="最近三個月的紀錄" checked="checked">
                                                    <p>最近三個月的紀錄</p>
                                                </li>
                                                <li>
                                                    <input class="rbDuring" type="radio" name="rbGroup" value="6" text="最近六個月的紀錄">
                                                    <p>最近六個月的紀錄</p>
                                                </li>
                                                <li>
                                                    <input class="rbDuring" type="radio" name="rbGroup" value="-1" text="全部的記錄">
                                                    <p>全部的紀錄</p>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <br />
                                <br />
                                <h2 class="rd-payment-h2">
                                    <div class="step-num-dot">2</div>
                                    輸入欲查詢的E-mail帳號</h2>
                                <div class="grui-form">
                                    <div class="form-unit">
                                        <div class="data-input">
                                            <input type="text" id="userEmail" class="userEmail input-full" placeholder="輸入您的E-mail" clientidmode="Static" />
                                            <input type="button" id="btnSend" onclick="return sendCouponListMail();" value="查詢" class="btn btn-large btn-primary rd-payment-xlarge-btn btn-fordetail" />
                                        </div>
                                        <div class="errorEmailMsg data-input enter-error" style="display: none">
                                            <img src="../Themes/PCweb/images/enter-error.png" width="20" height="20">
                                            <p>請使用字母、數字及英文句點，如17life@example.com</p>
                                        </div>
                                    </div>
                                </div>
                                <br />
                            </div>
                        </div>


                        <div class="clear"></div>
                    </div>
                </div>
                </div>
                <div class="question_content question_certification">
                    <div class="service service_detail">
                        <div class="service">
                            <div class="form">
                                <h1 class="title">會員認證<a href="javascript: void(0)" class="rwd_btn_service_list">回客服首頁</a></h1>
                                <div class="col100">請輸入E-mail帳號，我們將發送認證信件給您</div>
                                <div class="grui-form">
                                    <div class="form-unit">
                                        <div class="data-input">
                                            <input type="text" id="AuthuserEmail" class="userEmail input-full" placeholder="輸入您的E-mail" clientidmode="Static" />
                                            <input type="button" onclick="return sendAuthMail();" value="送出" class="btn btn-large btn-primary rd-payment-xlarge-btn btn-fordetail" />
                                        </div>
                                        <div class="errorEmailMsg data-input enter-error" style="display: none">
                                            <img src="../Themes/PCweb/images/enter-error.png" width="20" height="20">
                                            <p>請使用字母、數字及英文句點，如17life@example.com</p>
                                        </div>
                                    </div>
                                </div>
                                <br />
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>
                </div>
            </div>
            <!--fq-Lbox-->



        </div>
        <!--fq_box-->
    </div>


</asp:Content>

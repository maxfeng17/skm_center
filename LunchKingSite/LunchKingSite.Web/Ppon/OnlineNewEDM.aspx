﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OnlineNewEDM.aspx.cs" Inherits="LunchKingSite.Web.Ppon.OnlineNewEDM"  EnableViewState="false"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:Panel ID="pan_Ppon" runat="server">
            <div style="width: 100%; margin: 0 auto; font-family: Arial, Microsoft JhengHei, Helvetica, sans-serif, '微軟正黑體';">
                <!--EDM Notification-Start-->
                <table width="100%" cellpadding="0" cellspacing="0" align="center" style="display: none">
                    <tr>
                        <td>
                            <div id="dv_Hint" runat="server" style="color: #fff; font-size: 8px; height: 1px; overflow: hidden;"></div>
                        </td>
                    </tr>
                    <tr>
                        <td height="50" align="center" style="font-size: 13px; background: #FFF;">若信件內容無法正常顯示，您可以點選此處<a href="<%= SiteUrl+"/ppon/onlinenewedm.aspx?eid=" + EdmID + "&rsrc=" + Cpa %>" target="_blank">查看線上版本</a><br />
                            若此信件被寄送至您的垃圾郵件中，請將 hi@edm.17life.com.tw 加入您的聯絡人清單中。
                        </td>
                    </tr>
                </table>
                <!--EDM Notification-End-->
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="font-size: 13px; background: #fff;">
                            <table width="795" cellpadding="0" cellspacing="0" align="center" style="background: #<%=(IsTravelCity)?"2D81E8":"A72828"%>;">
                                <tr>
                                    <td height="5"></td>
                                </tr>
                                <tr>
                                    <td>
                                        <!--EDM Header-Start-->
                                        <table width="785" cellpadding="0" cellspacing="0" align="center" style="background: #F3F3EE; color: #333;">
                                            <tr>
                                                <td width="20" height="80"></td>
                                                <td width="140" height="80">
                                                    <a href="<%=SiteUrl %>/ppon/default.aspx<%=Cpa%>" target="_blank" title='<%= DeliveryDate.ToString("yyyy/MM/dd")+" 17Life EDM" %>'>
                                                        <img src="<%=SiteUrl+"/Themes/PCweb/images/EDMLOGO.png" %>" width="110" height="80" alt="<%= DeliveryDate.ToString("yyyy/MM/dd")+" 17Life EDM" %>" border="0" />
                                                    </a>
                                                </td>
                                                <td width="480" height="80" style="font-size:26px; font-weight:bold;"></td>
                                                <td width="110" height="80" style="text-align: right;">                        
                                                    <ul style="list-style: none;">
                                                        <li>
                                                            <a href="https://line.me/R/ti/p/%4017life" target="_blank">
                                                                <img src="https://www.17life.com/Themes/default/images/17Life/EDM/Line_beFriends.png" style="height: 2.4rem" alt="Add To Line APP">
                                                            </a>                
                                                        </li>
                                                        <li>
                                                            <p style="line-height:1rem;margin-top: .2rem;text-align: center;font-weight: 400;color:#888;">超值優惠天天有</p>
                                                        </li>
                                                    </ul>
                                                </td>
                                                <td width="20" height="80"></td>
                                            </tr>
                                        </table>
                                        <!--EDM Header-End-->
                                        <!--EDM Main-Start-->
                                        <table width="785" cellpadding="0" cellspacing="0" align="center" style="background: #F3F3EE;">
                                            <tr>
                                                <td width="25"></td>
                                                <td width="740">
                                                    <!---->
                                                    <!--FB & App Promo-Start-->
                                                    <!--FB & App Promo-End-->
                                                    <!--Ad Area-Start-->
                                                    <asp:Panel ID="pan_AD" runat="server">
                                                        <table width="740" cellpadding="0" cellspacing="0" align="center">
                                                            <tr>
                                                                <td height="10"></td>
                                                            </tr>
                                                            <tr>
                                                                <td height="80">
                                                                    <asp:Literal ID="lit_AD" runat="server"></asp:Literal>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td height="15"></td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
                                                    <!--Ad Area-End-->
                                                    <asp:Panel ID="pan_MainDeal1" runat="server">
                                                    </asp:Panel>
                                                    <asp:Panel ID="pan_MainDeal2" runat="server">
                                                    </asp:Panel>
                                                    <asp:Panel ID="pan_Area1" runat="server">
                                                    </asp:Panel>
                                                    <asp:Panel ID="pan_Area2" runat="server">
                                                    </asp:Panel>
                                                    <asp:Panel ID="pan_Area3" runat="server">
                                                    </asp:Panel>
                                                    <asp:Panel ID="pan_PiinLife1" runat="server">
                                                    </asp:Panel>
                                                    <asp:Panel ID="pan_PiinLife2" runat="server">
                                                    </asp:Panel>
                                                    <asp:Panel ID="pan_PponPiinLife_1" runat="server">
                                                    </asp:Panel>
                                                    <!--PezDailyItem-Start-->
                                                    <asp:Panel ID="pan_PEZ" runat="server">
                                                        <table>
                                                            <tr>
                                                                <td width="740" height="40" style="color: #C82851; font-size: 24px; font-weight: bold; border-bottom: 1px solid #C82851;">▌PayEasy每日一物
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td height="5"></td>
                                                            </tr>
                                                        </table>
                                                        <asp:Literal ID="lit_PEZ" runat="server"></asp:Literal>
                                                    </asp:Panel>
                                                    <!--PezDailyItem-End-->
                                                    <!--floow_us-Start-->
                                                    <table width="740" border="0" cellpadding="0" cellspacing="0" align="center" style="">
                                                        <tr height="30"><td></td></tr>
                                                        <tr height="40">
                                                            <td width="280"></td>
                                                            <td width="60">跟隨我們:</td>
                                                            <td width="42"><a href="https://www.facebook.com/17life.com.tw" target="_blank"><img src="https://www.17life.com/Themes/default/images/17Life/EDM/EDM-btn-facebook.png" width="40" height="40" /></a></td>
                                                            <td width="42"><a href="https://line.me/R/ti/p/%4017life<%=Cpa %>" target="_blank"><img src="https://www.17life.com/Themes/default/images/17Life/EDM/EDM-btn-line.png" width="40" height="40" /></a></td>
                                                            <td width="300"></td>
                                                        </tr>
                                                        <tr height="30"><td></td></tr>
                                                    </table>
                                                    <!--floow_us-End-->
                                                    <br />
                                                </td>
                                                <td width="25"></td>
                                            </tr>
                                        </table>
                                        <!--EDM Main-End-->
                                    </td>
                                </tr>
                            </table>
                            <!--EDM Footer-Start-->
                            <table width="800" cellpadding="0" cellspacing="0" align="center" style="background: #333; color: #FFF;">
                                <tr>
                                    <td width="25"></td>
                                    <td height="80">若您不希望再收到此訊息，請按此<a href="%%UnSubscription%%" target="_blank" style="color: #FFF;">取消訂閱</a>。<br />
                                        <br />
                                        <a style="color: #eee" target="_blank" href="https://www.17life.com/user/service.aspx<%=Cpa %>">客服中心</a>
                                        <span> | </span>
                                        <a style="color: #eee" target="_blank" href="https://www.17life.com/Ppon/WhiteListGuide.aspx<%=Cpa %>">加入白名單</a>
                                        <span> | </span>
                                        <a style="color: #eee" target="_blank" href="https://www.17life.com/User/UserAccount.aspx#edmTpis">取消訂閱</a>
                                        <span> | </span>
                                        服務時間：平日 9:00~18:00<br />
                                        17Life 康太數位整合股份有限公司 版權所有 轉載必究 © 
                                        <script language="javascript">
                                            var Today = new Date();
                                            document.write(Today.getFullYear());
                                        </script>
                                    </td>
                                    <!-- App Store Links -->
                                    <td>
                                        <a href="https://itunes.apple.com/tw/app/id543439591?mt=8">
                                            <img src="https://www.17life.com/images/17P/20150402-pcp-promo/images/ios-icon.png"
                                                alt="到蘋果 App Store" style="height:40px">
                                        </a>
                                    </td>
                                    <td>
                                        <a href="https://play.google.com/store/apps/details?id=com.uranus.e7plife&feature=search_result#?t=W251bGwsMSwxLDEsImNvbS51cmFudXMuZTdwbGlmZSJd">
                                            <img src="https://www.17life.com/images/17P/20150402-pcp-promo/images/android-icon.png"
                                                alt="到安卓 Google Play" style="height:40px">
                                        </a>
                                    </td>
                                    <!-- App Store Links End -->
                                </tr>
                            </table>
                            <!--EDM Footer-End-->
                        </td>
                    </tr>
                    <tr>
                        <td height="25" style="background: #FFF;"></td>
                    </tr>
                </table>
            </div>
        </asp:Panel>
        <asp:Panel ID="pan_Piinlife" runat="server">
            <table width="100%" align="center">
                <tr>
                    <td>
                        <div style="background-color: #F6EADF; font-size: 13px;">
                            <table width="700" border="0" cellspacing="0" cellpadding="0" align="center">
                                <tr>
                                    <td height="5"></td>
                                </tr>
                                <tr>
                                    <td align="center">若信件內容無法正常顯示，您可以點選 <a href="<%= SiteUrl+"/ppon/onlinenewedm.aspx?eid=" + EdmID + "&rsrc=" + Cpa %>" target="_blank" style="color: #FC7D49;">查看線上版本</a>。<br />
                                        若此信件被寄送至您的垃圾郵件中，請將 <a href="#" target="_blank" style="color: #FC7D49;">hi@edm.17life.com.tw</a>加入您的聯絡人清單中。</td>
                                </tr>
                                <tr>
                                    <td height="10"></td>
                                </tr>
                            </table>
                            <table width="700" align="center" border="0" cellspacing="0" cellpadding="0" style="background-color: white; border: 1px solid #E1D6CC;">
                                <tr>
                                    <td>
                                        <!--MailHeader_Start-->
                                        <div id="Div1">
                                            <table width="700" border="0" cellspacing="0" cellpadding="0" style="background-color: #3E3226;">
                                                <tr>
                                                    <td width="200"><a href="<%=SiteUrl%>/piinlife/default.aspx<%=Cpa%>" target="_blank">
                                                        <img src="https://www.17life.com/Themes/HighDeal/images/EDM/HDEDM_LOGO.png" width="250" height="60" border="0" alt="PiinLife 品生活" /></a></td>
                                                    <td width="350" style="color: white; font-size: 22px;"></td>
                                                    <td width="85" align="right"><a href="<%=SiteUrl%>/piinlife/default.aspx<%=Cpa%>" target="_blank" style="color: white;">查看全部優惠</a></td>
                                                    <td width="15"></td>
                                                </tr>
                                            </table>
                                        </div>
                                        <!--MailHeader_End-->
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div id="MailContent">
                                            <table width="700" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td height="20" colspan="3"></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="3">
                                                        <asp:Panel ID="pan_PponPiinLife" runat="server">
                                                        </asp:Panel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td height="10" colspan="3"></td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <!--MailFooter_Start-->
                                        <div id="MailFooter">
                                            <table width="650" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td></td>
                                                </tr>
                                            </table>
                                        </div>
                                        <!--MailFooter_End-->
                                    </td>
                                </tr>
                            </table>
                            <table width="700" border="0" cellspacing="0" cellpadding="0" align="center">
                                <tr>
                                    <td height="10" colspan="3"></td>
                                </tr>
                                <tr>
                                    <td width="600" align="left">若您不希望再收到此訊息，請按此<a href="%%UnSubscription%%" target="_blank"
                                            style="color: #FC7D49;">取消訂閱</a>。</td>
                                    <td width="100" style="text-align: center;vertical-align: center;">
                                        官方Line@                                        
                                    </td>
                                    <td width="130">
                                        <a href="https://line.me/R/ti/p/%4017life<%=Cpa %>">
                                            <img src="https://www.17life.com/Themes/default/images/17Life/EDM/Line_beFriends.png" style="height: 2.4rem" alt="Add To Line APP">
                                        </a>
                                    </td>
                                    <td width="124" align="right">加入粉絲團&nbsp;&nbsp;</td>
                                    <td width="26" align="center"><a href="https://www.facebook.com/PiinLife" target="_blank">
                                            <img src="https://www.17life.com/Themes/HighDeal/images/EDM/HD_FBIcon.png"
                                                width="26" height="26" alt="加入粉絲團" /></a></td>
                                </tr>
                                <tr>
                                    <td height="30" colspan="3"></td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </form>
</body>
</html>

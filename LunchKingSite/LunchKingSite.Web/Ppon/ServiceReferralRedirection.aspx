﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ServiceReferralRedirection.aspx.cs" Inherits="LunchKingSite.Web.Ppon.ServiceReferralRedirection" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <script type="text/javascript" src="../Tools/js/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="../Tools/js/jquery.blockUI.js"></script>
    <script type="text/javascript">

        function openAtAPP(d, url) {
            if (d == "ios") {
                var start = new Date();
                window.setTimeout(function () {
                    if (new Date() - start > 2000) {
                        return;
                    }
                    window.location = "https://itunes.apple.com/tw/app/17life/id543439591";
                }, 1000);
                window.location = "open17life://www.17life.com/service/servicetype=<%=ServiceType%>&servicepara=<%=ServicePara%>";
            }
            else if (d == "android") {
                try {
                    var isOpera = !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;
                    // Opera 8.0+ (UA detection to detect Blink/v8-powered Opera)
                    var isFirefox = typeof InstallTrigger !== 'undefined';   // Firefox 1.0+
                    var isSafari = Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0;
                    // At least Safari 3+: "[object HTMLElementConstructor]"
                    var isChrome = !!window.chrome && !isOpera;              // Chrome 1+
                    var isIE = /*@cc_on!@*/false || !!document.documentMode; // At least IE6
                    window.location = "open17life://www.17life.com/service/servicetype=<%=ServiceType%>&servicepara=<%=ServicePara%>";

                } catch (e) {
                    alert("發生錯誤");
                    window.location = url;
                }
            }


            //window.setTimeout(function () {
            //    window.location = url;
            //}, 1500);
        }

        function appPromo(url) {
            // 行動裝置(不含iPad)出現建議下載app
            var device = '';
            if (navigator.userAgent.match(<%= AndroidUserAgent%>)) {
                device = 'android';
            }
            else if (navigator.userAgent.match(<%= iOSUserAgent%>)) {
                if (navigator.userAgent.indexOf('iPad') < 0) {
                    device = 'ios';
                }
            }

            if (device != '') {
                //-------------------Mobile---------------------------
                <%--這裡不能用confirm()，會造成手機瀏覽器詢問選單跳不出來--%>

                askIfOpenOnApp();//開啟blockUI詢問視窗
                $('.answer').on('click', function () {
                    var answer = $(this).attr('name');
                    if (answer == "yes") {
                        $.unblockUI();
                        //嘗試開始APP
                        openAtAPP(device, url);
                    }
                    else {
                        $.unblockUI();
                        window.location = url;
                        return false;
                    }
                });
                //----------------------------------------------------
            } else {
                //---------------------PC-----------------------------
                window.location = url;
                //----------------------------------------------------
            }
        };

        function askIfOpenOnApp() {
            try {
                document.body.style.zoom = 1.0
            } catch (e) {
                var scale = 'scale(1)';
                document.body.style.transform = scale;
            }
            
            //手機尺寸
            $('#question').css('font-size', '30pt');
            $('.answer').css('font-size', '30pt');
            $('.answer[name=yes]').css('margin-right', '100px');

            var mobileCSS = {
                padding: 30,
                margin: 0,
                top: '20%',
                left: '15%',
                textAlign: 'center',
                color: '#000',
                border: '3px solid #aaa',
                backgroundColor: '#fff',
                cursor: 'default',
                width: '60%'
            };

            $.blockUI({
                message: $('#question'),
                css: mobileCSS
            });
        }
        function isMobileWidth() {
            return $('#mobile-indicator').is(':visible');
        }


        function hintThenRedirect(url) {
            alert("你的瀏覽器不支援以App開啟功能");
            window.location = url;
        }

    </script>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
        </div>
        
        <div id="question" style="font-size:14px;display:none;"> 
            <div id="desc">是否於17Life APP開啟連結？</div>
            <input class="answer" type="button" name="yes" value="是" /> 
            <input class="answer" type="button" name="no" value="否" /> 
        </div>
    </form>
</body>
</html>

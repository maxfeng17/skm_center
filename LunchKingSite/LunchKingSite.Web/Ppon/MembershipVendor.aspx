﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MembershipVendor.aspx.cs" Inherits="LunchKingSite.Web.NewMember.MembershipVendor" %>

<%@ Import Namespace="System.Activities.Debugger" %>
<%@ Import Namespace="System.Activities.Statements" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, user-scalable=no" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <title>熟客系統合作商家</title>
    <style>
        /*--------- reset ---------*/
        html, body, div, dl, dt, dd, ul, ol, li, h1, h2, h3, h4, h5, h6, pre, code, form, fieldset, legend, input, button, textarea, p, blockquote, th, td, figure {
            margin: 0;
            padding: 0;
        }

        table {
            border-collapse: collapse;
            border-spacing: 0;
            font-size: inherit;
            font: 100%;
        }

        fieldset, img {
            border: none;
        }

        address, caption, cite, code, dfn, em, th, var {
            font-style: normal;
            font-weight: normal;
        }

        img {
            vertical-align: middle;
        }

        abbr, acronym {
            border: none;
            font-variant: normal;
        }

        sup {
            vertical-align: text-top;
        }

        sub {
            vertical-align: text-bottom;
        }

        input, button, textarea, select {
            *font-size: 100%;
        }

        input {
            vertical-align: middle;
        }

        select, input, button, textarea {
            font: 100% Verdana, Arial, Helvetica, sans-serif;
        }

        pre, code, kbd, samp, tt {
            font-family: Verdana, Arial, Helvetica, sans-serif;
        }

        small {
            font-size: 100%;
        }

        ins {
            text-decoration: none;
        }

        li {
            list-style: none;
        }
        /*--------- style ---------*/
        html {
            text-align: left;
        }

        html, legend {
            color: #333;
        }

        body {
            font-size: 16px;
            line-height: 1.7;
            font-family: \5FAE\8EDF\6B63\9ED1\9AD4;
            background-color: #eee;
        }

        input, button, textarea, select {
            font-family: inherit;
            font-size: inherit;
            font-weight: inherit;
            color: #333;
            padding: 1px;
        }

        p {
            margin: 0 0 10px 0;
        }

        a {
            text-decoration: none;
        }

            a:hover {
                text-decoration: underline;
            }

        .clear {
            clear: both;
        }

        .center {
            text-align: center;
        }
        /*--------- layout start ---------*/
        .section {
            float: left;
            width: 100%;
            margin-bottom: 12px;
            background-color: #FFF;
            border-bottom: 1px solid #ddd;
            position: relative;
        }

            .section .result_img {
                position: absolute;
                right: 0;
                top: 0;
            }

            .section .topic {
                float: left;
                width: 92%;
                margin-bottom: 12px;
                padding: 20px 4%;
                border-bottom: 1px solid #ddd;
                text-align: center;
            }

                .section .topic span {
                    float: left;
                    margin-top: 2px;
                    margin-right: 4px;
                }

                .section .topic strong {
                    font-size: 20px;
                    line-height: 24px;
                    color: #090;
                }

            .section .dec {
                float: left;
                width: 92%;
                line-height: 16px;
                margin-bottom: 16px;
                padding: 0 4%;
            }

            .section ul.dec li {
                line-height: 1.6;
                float: left;
                margin: 3px 0px;
                color: #999;
                /*background: pink;*/
            }

            .section .link_17life {
                clear: both;
                margin: 0 auto;
                display: block;
                text-align: center;
                margin-bottom: 20px;
            }

                .section .link_17life > a {
                    display: inline-block;
                    padding: 5px 10px;
                    background: #E74C3C;
                    color: #fff;
                    border-radius: 5px;
                    text-decoration: none;
                }

                    .section .link_17life > a:hover {
                        background: #f05949;
                    }



        /*L*/
        @media only screen and (min-width:961px) {
            .link_wrap_web {
                width: 960px;
                margin: 0 auto;
            }

            .link_wrap_web {
                margin-top: 50px;
            }

            .section {
                border-radius: 10px;
                box-shadow: 0px 0px 5px rgba(0,0,0,0.4);
            }

                .section .topic strong {
                    float: left;
                }

            .newyear .container {
                width: 960px;
            }

            .link_wrap {
                width: 480px;
                margin: 0 auto;
            }

            .header {
                display: none;
            }

            .link_wrap_web {
                display: block;
            }

            .link_wrap {
                display: none;
            }
        }

        /*M*/
        @media screen and (min-width: 640px) and (max-width: 960px) {
            .link_wrap {
                width: 480px;
                margin: 0 auto;
            }

            .link_wrap_web {
                display: none;
            }

            .link_wrap {
                display: block;
            }
        }

        /*S*/
        @media only screen and (max-width:640px) {
            .link_wrap_web {
                display: none;
            }

            .link_wrap {
                display: block;
            }
        }
    </style>


</head>
<body>
    <div>
        <div class="container">
            <div class="section">
                <div class="topic"><strong>熟客系統合作店家</strong></div>
                <!-- -->
                <div class="section_normal">
                    <ul class="dec">
                        <% 

                        %>
                        <li><%= string.Join("、",VendorNames) %> </li>
                        <%
                            
                        %>
                    </ul>
                </div>
                <!-- -->
            </div>
        </div>

    </div>

</body>
</html>

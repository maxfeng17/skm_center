﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using LunchKingSite.BizLogic.Component;
using System.Collections.Generic;

namespace LunchKingSite.Web.Ppon
{
    public partial class subscription : System.Web.UI.Page
    {

        #region property
        public string CategoryName { get; set; }

        public string GetPponDealCSSP(int CategoryId)
        {
            switch (CategoryId)
            {
                case 94:
                    return "background: rgba(237,28,58,0.6);";
                case 95:
                    return " background: rgba(246,105,165,0.6);";
                case 96:
                    return "background: rgba(230,31,141,0.6);";
                case 97:
                    return "background: rgba(255,102,0,0.6);";
                case 98:
                    return "background: rgba(159,212,6,0.6);";
                case 99:
                    return "background: rgba(36,198,253,0.6);";
                default:
                    return "";
            }
        }
        public string GetChannelCSS(int CategoryId)
        {
            if (CategoryId == 89)
            {
                return "large";
            }
            else
            {
                return "medium";
            }
        }
        public string GetChannelSubTitle(int CategoryId)
        {
            switch (CategoryId)
            {
                case 88:
                    return "輕鬆購";
                case 89:
                    return "迷人";
                case 90:
                    return "身心靈";
                case 184:
                    return "小編推薦";
                case 148:
                    return "體驗美好";
                default:
                    return "";
            }
        }
        public string GetTitleBanner()
        {
            //string x = @"<div class=""swiper-container""><div class=""top-title""><h3>超過3000個 全台熱門團購</h3><h1>免費訂閱</h1>" 
            //x += @"</div>" 
            //x += @" <div class=""swiper-wrapper""><div class=""swiper-slide"">" 
            //x += @" <img src=""https://www.17life.com/images/17P/17topbn/edm_2015/edm_bn_02.jpg"">  " 
            //x += @"</div> " 
            //x += @"<div class=""swiper-slide"">" 
            //x += @" <img src=""https://www.17life.com/images/17P/17topbn/edm_2015/edm_bn_03.jpg"">  " 
            //x += @"</div> " 
            //x += @"<div class=""swiper-slide"">" 
            //x += @" <img src=""https://www.17life.com/images/17P/17topbn/edm_2015/edm_bn_04.jpg"">  " 
            //x += @"</div> " 
            //x += @"<div class=""swiper-slide"">" 
            //x += @" <img src=""https://www.17life.com/images/17P/17topbn/edm_2015/edm_bn_05.jpg"">  " 
            //x += @"</div> " 
            //x += @"<div class=""swiper-slide"">" 
            //x += @" <img src=""https://www.17life.com/images/17P/17topbn/edm_2015/edm_bn_01.jpg"">  " 
            //x += @"</div> " 

            //x += @"</div> " 
            //x += @"</div> " 
            //x += @"<div class="swiper-container mobile-pic"> " 
            //x += @"<div class="top-title"> " 
            //x += @"<h3>超過3000個 全台熱門團購</h3> " 
            //x += @"<h1>免費訂閱</h1> " 
            //x += @"</div> " 
            //x += @"<div class="swiper-wrapper"> " 
            //x += @"<div class=""swiper-slide"">  " 
            //x += @" <img src=""https://www.17life.com/images/17P/17topbn/edm_2015/M-2.jpg"">  " 
            //x += @"</div> " 
            //x += @"<div class=""swiper-slide"">  " 
            //x += @" <img src=""https://www.17life.com/images/17P/17topbn/edm_2015/M-1.jpg"">  " 
            //x += @"</div> " 
            //x += @"<div class=""swiper-slide"">  " 
            //x += @" <img src=""https://www.17life.com/images/17P/17topbn/edm_2015/M-3.jpg"">  " 
            //x += @"</div> " 
            //x += @"<div class=""swiper-slide"">  " 
            //x += @" <img src=""https://www.17life.com/images/17P/17topbn/edm_2015/M-4.jpg"">  " 
            //x += @"</div> " 
            //x += @"<div class=""swiper-slide"">  " 
            //x += @" <img src=""https://www.17life.com/images/17P/17topbn/edm_2015/M-5.jpg"">  " 
            //x += @"</div> " 
            //x += @"  </div>  " 
            //x += @"  </div>  " 
            //x += @"" 
            //x += @"  <div class=""swiper-bottom swiper-bottom-header""> " 
            //x += @"<div class=""mail-click"">  " 
            //x += @" <input id=""subscribeEmail1"" type=""text"" class=""edm-keyin"" placeholder=""輸入您的信箱如：service@17life.com"" autocomplete=""off""> " 
            //x += @" <div class=""mail-select""> " 
            //x += @"  <div class=""select-list active popup_select_list"">" 
            //x += @"選擇品項 " 
            //x += @"<sapn class=""choose_num""></sapn> " 
            //x += @"  </div>  " 
            //x += @" </div>" 
            //x += @"" 
            ////x += @" <input type=""button"" class=""mail-btn"" style=""cursor: pointer"" value=""訂閱"" onclick=""onSubscribe(\'1\');"">" 
            //x += @"</div> " 
            //x += @" </div>" 
            //return x;
            string x = @"<div class=""swiper-container web-pic"" id=""topbn""><div class=""top-title""><h3>超過3000個 全台熱門團購</h3><h1>免費訂閱</h1></div> <div class=""swiper-wrapper""><div class=""swiper-slide""><img src=""https://www.17life.com/images/17P/17topbn/edm_2015/edm_bn_02.jpg""></div><div class=""swiper-slide""><img src=""https://www.17life.com/images/17P/17topbn/edm_2015/edm_bn_03.jpg""></div><div class=""swiper-slide""><img src=""https://www.17life.com/images/17P/17topbn/edm_2015/edm_bn_04.jpg""></div><div class=""swiper-slide""><img src=""https://www.17life.com/images/17P/17topbn/edm_2015/edm_bn_05.jpg""></div><div class=""swiper-slide""><img src=""https://www.17life.com/images/17P/17topbn/edm_2015/edm_bn_01.jpg""></div></div></div><div class=""swiper-container mobile-pic""><div class=""top-title""><h3>超過3000個 全台熱門團購</h3><h1>免費訂閱</h1></div><div class=""swiper-wrapper""><div class=""swiper-slide""><img src=""https://www.17life.com/images/17P/17topbn/edm_2015/M-2.jpg""></div><div class=""swiper-slide""> <img src=""https://www.17life.com/images/17P/17topbn/edm_2015/M-1.jpg""></div><div class=""swiper-slide""><img src=""https://www.17life.com/images/17P/17topbn/edm_2015/M-3.jpg""></div><div class=""swiper-slide""> <img src=""https://www.17life.com/images/17P/17topbn/edm_2015/M-4.jpg""></div><div class=""swiper-slide""> <img src=""https://www.17life.com/images/17P/17topbn/edm_2015/M-5.jpg""></div></div></div><div class=""swiper-bottom swiper-bottom-header""><div class=""mail-click""><input id=""subscribeEmail1"" type=""text"" class=""edm-keyin"" placeholder=""輸入您的信箱如：service@17life.com"" autocomplete=""off""><div class=""mail-select""><div class=""select-list active popup_select_list"">選擇品項<sapn class=""choose_num""></sapn></div></div><input type=""button"" class=""mail-btn"" style=""cursor: pointer"" value=""訂閱"" onclick=""onSubscribe(\'1\');""></div></div>";
            return x;
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            List<CategoryNode> nodes = CategoryManager.CategoryNodesForSubscription;

            CategoryNode pponDeal = CategoryManager.Default.PponDeal;

            // 設定美食的訂閱頻道
            List<CategoryNode> pponDealArea = pponDeal.NodeDatas
                .Where(x => x.NodeType == Core.CategoryType.PponChannelArea)
                .FirstOrDefault().CategoryNodes;

            CategoryName = pponDeal.CategoryName;

            rptPponDealAreaPop.DataSource = pponDealArea;
            rptPponDealAreaPop.DataBind();
            rptPponDealArea.DataSource = pponDealArea;
            rptPponDealArea.DataBind();

            rptChannelPop.DataSource = nodes.Where(x => !pponDealArea.Select(y => y.CategoryId).Contains(x.CategoryId));
            rptChannelPop.DataBind();
            rptChannel.DataSource = rptChannelPop.DataSource;
            rptChannel.DataBind();
        }

        protected void rptPponDealArea_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is CategoryNode)
            {
                CategoryNode tmpnode = (CategoryNode)e.Item.DataItem;
                Image img = (Image)e.Item.FindControl("imgPponDeal");

                switch (tmpnode.CategoryId)
                {
                    case 94:
                        img.ImageUrl = "https://www.17life.com/images/17P/17topbn/edm_2015/edm_01.jpg";
                        break;
                    case 95:
                        img.ImageUrl = "https://www.17life.com/images/17P/17topbn/edm_2015/edm_02.jpg";
                        break;
                    case 96:
                        img.ImageUrl = "https://www.17life.com/images/17P/17topbn/edm_2015/edm_03.jpg";
                        break;
                    case 97:
                        img.ImageUrl = "https://www.17life.com/images/17P/17topbn/edm_2015/edm_04.jpg";
                        break;
                    case 98:
                        img.ImageUrl = "https://www.17life.com/images/17P/17topbn/edm_2015/edm_05.jpg";
                        break;
                    case 99:
                        img.ImageUrl = "https://www.17life.com/images/17P/17topbn/edm_2015/edm_06.jpg";
                        break;
                    default:
                        img.ImageUrl = "https://www.17life.com/images/17P/17topbn/edm_2015/edm_01.jpg";
                        break;
                }
            }
        }

        protected void rptChannel_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is CategoryNode)
            {
                CategoryNode tmpnode = (CategoryNode)e.Item.DataItem;
                Image img = (Image)e.Item.FindControl("imgChannel");
                switch (tmpnode.CategoryId)
                {
                    case 88://宅配
                        img.ImageUrl = "https://www.17life.com/images/17P/17topbn/edm_2015/edm_09.jpg";
                        break;
                    case 89: //玩美‧休閒
                        img.ImageUrl = "https://www.17life.com/images/17P/17topbn/edm_2015/edm_08.jpg";
                        break;
                    case 90://旅遊
                        img.ImageUrl = "https://www.17life.com/images/17P/17topbn/edm_2015/edm_07.jpg";
                        break;
                    case 184://精選好康
                        img.ImageUrl = "https://www.17life.com/images/17P/17topbn/edm_2015/edm_10.jpg";
                        break;
                    case 148://品生活
                        img.ImageUrl = "https://www.17life.com/images/17P/17topbn/edm_2015/edm_11.jpg";
                        break;
                    default:
                        img.ImageUrl = "https://www.17life.com/images/17P/17topbn/edm_2015/edm_08.jpg";
                        break;
                }
            }
        }
    }
}

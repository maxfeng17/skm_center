﻿<%@ Page Language="C#" MasterPageFile="~/Ppon/Ppon.Master" AutoEventWireup="true"
    CodeBehind="KindPay.aspx.cs" Inherits="LunchKingSite.Web.Ppon.KindPay" Title="17Life交易" %>

<%@ Import Namespace="LunchKingSite.Core" %>
<%@ Import Namespace="LunchKingSite.I18N" %>
<%@ Import Namespace="LunchKingSite.WebLib" %>
<%@ Register Src="~/UserControls/Paragraph.ascx" TagName="Paragraph"
    TagPrefix="uc1" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MC" runat="server">
    <script type="text/javascript" src="../Tools/js/jquery.blockUI.js"></script>
    <script type="text/javascript" src="../Tools/js/jquery.cascadeAddress.js"></script>
    <meta name="viewport" content="width=device-width initial-scale=1.0" />
    <link href="../Themes/default/images/17Life/G3/PPD.css" rel="stylesheet" type="text/css" />
    <link href="../Themes/default/images/17Life/G3/D2-5-Checkout.css" rel="stylesheet" type="text/css" />
    <link href="../themes/PCweb/css/RDL-L.css" rel="stylesheet" type="text/css" />
    <link href="../themes/PCweb/css/RDL-M.css" rel="stylesheet" type="text/css" />
    <link href="../themes/PCweb/css/RDL-S.css" rel="stylesheet" type="text/css" />
    <style>
        .receipt_table td
        {
            padding: 3px 0px 3px 2px;
        }
        .receipt_table input[type="radio"]
        {
            position: inherit;
        }
        .credit_item label
        {
            font-weight: normal;
        }
    </style>
    <script type="text/javascript" language="javascript">
        function ChangeQty(check) {
            var oprice = $("#<%=hidprice.ClientID %>").val();
            var qty = $("#<%=ddlQty.ClientID%>").val();
            var Number = /^\d*$/;

            changeFreight(qty, oprice);

            $("#lsubtotal").html((qty * oprice).toString());

            $("#<%= hidqty.ClientID %>").val(qty);

            $("#<%= hidsubtotal.ClientID %>").val((qty * oprice).toString());
            var total = qty * oprice + parseInt($("#<%= hiddCharge.ClientID %>").val());
            $("#total").html(total.toString());
            $("#cltotal").html(total.toString());
            $("#<%= hidtotal.ClientID %>").val(total.toString());
            $("#totalErr").hide();

            if (parseInt(total) <= 0) {
                $('.SelectedPayWay').hide();
                $('#divConfirmPayWay').hide();
                $('#<%= rbPayWayByCreditCard.ClientID %>').attr('checked', 'checked');
                if (total < 0) {
                    $('.SelectedPayWay').show();
                    $('#divConfirmPayWay').show();
                    $("#totalErr").show();
                }
            } else {
                $('.SelectedPayWay').show();
                $('#divConfirmPayWay').show();
            }
        }

        function changeFreight(qty, oprice){
            var freight = $("#<%= hidFreight.ClientID %>").val();
            var zeroFreightAmt = "0"; 
            var subTotal = qty * oprice;

            if ("" != freight) {
                var freights = jQuery.parseJSON(freight);
                var currenFreight = undefined;
                var currentEndAmt = 0;
                $.each(freights, function(key, val) {
                    if (subTotal >= val.startAmt && subTotal < val.endAmt ) {
                        currenFreight = val.freightAmt;
                        $("#dCharge").html(val.freightAmt);
                        currentEndAmt = val.endAmt;
                    }

                    if (subTotal < val.startAmt && val.freightAmt == 0 && ("0" == zeroFreightAmt || parseInt(zeroFreightAmt) > parseInt(val.startAmt))) {
                        zeroFreightAmt = val.startAmt;
                    }
                });
                
                if ("0" == zeroFreightAmt) {
                    var nextFreight = 0;
                    $.each(freights, function(key, val) {
                        if (currentEndAmt == val.startAmt) {
                            nextFreight = val.freightAmt;
                        }
                    });

                    if (0 != nextFreight && nextFreight < parseInt(currenFreight)) {
                        $("#<%= lbldc.ClientID %>").html("<span class='hint'>-還差" + Math.ceil((currentEndAmt - subTotal) / oprice) + "份，運費將降為" + nextFreight + "元囉！</span>");
                    } else {
                        $("#<%= lbldc.ClientID %>").html("");
                    }
                } else {
                    if (currenFreight == "0") {
                        $("#<%= lbldc.ClientID %>").html("");
                    } else {
                        $("#<%= lbldc.ClientID %>").html("<span class='hint'>-還差" + Math.ceil((parseInt(zeroFreightAmt) - subTotal) / oprice) + "份就免運費囉！</span>");
                    }
                }
                
                $("#<%= hiddCharge.ClientID %>").val($("#dCharge").html());
                $("#ltotal").html((subTotal + (isNaN(parseInt($("#dCharge").html().replace('$',''))) ? 0 : parseInt($("#dCharge").html().replace('$','')))).toString());   
            } else {
                var po = $('#<%= hpromo.ClientID %>').val();
                var hdc = $("#<%= hiddCharge.ClientID %>").val();
                var ldc = $("#dCharge").html();
                var hsdc = document.getElementById('<%= hidcstatic.ClientID %>');

                if (po != '') {
                    if (Math.ceil(parseInt(po) - (subTotal)) <= 0) {
                        $("#dCharge").html("0");
                        $("#<%= hiddCharge.ClientID %>").val("0");
                        document.getElementById('<%= lbldc.ClientID %>').innerHTML = "";
                        $("#ltotal").html((subTotal).toString());
                    } else {
                        $("#dCharge").html(hsdc.value);
                        $("#<%= hiddCharge.ClientID %>").val(hsdc.value);
                        $("#ltotal").html((subTotal + parseInt(hdc)).toString());
                        document.getElementById('<%= lbldc.ClientID %>').innerHTML = "<span class='hint'>-還差" + Math.ceil((parseInt(po) - subTotal) / oprice) + "份就免運費囉！</span>";
                    }
                } else {
                    $("#ltotal").html((subTotal + parseInt(hdc)).toString());
                }
            }
        }

        function checkopt() {
            if ($("#<%=ddlQty.ClientID %>").val() <= 0) {
                alert('購買數量不能為零，請重新選擇，謝謝');
                return false;
            }

            var totalCheck = true;
            if (parseInt($("#total").html()) < 0) {
                $("#totalErr").show();
                totalCheck = false;
            }

            if ($("#<%= BuyerName.ClientID %>").val() == "") {
                $("#buyerNameErr").show();
                $("#tdBuyerName").addClass("error");
                $("#<%= BuyerName.ClientID %>").focus();
                totalCheck = false;
            } else {
                $("#buyerNameErr").hide();
                $("#tdBuyerName").removeClass("error");
            }

            if ($("#<%= hidIsZeroEvent.ClientID %>").val() == "False") {
                if (!checkMobile($("#<%= BuyerMobile.ClientID %>").val())) {
                    $("#buyerMobileErr").show();
                    $("#tdBuyerMobile").addClass("error");
                    $("#<%= BuyerMobile.ClientID %>").focus();
                    totalCheck = false;
                } else {
                    $("#buyerMobileErr").hide();
                    $("#tdBuyerMobile").removeClass("error");
                }
            }

            if (!checkReceiverDelivery()) {
                totalCheck = false;
            }

            if (!checkBranch()) {
                totalCheck = false;
            }

            if (!checkReceiptAddress()) {
                totalCheck = false;
            }

            return totalCheck;
        }

        function checkBranch(){
            var rlt = true;
            $('option:selected', 'select[id$=ddlStore], select[id$=ddlMultOption]').each(function () {
                if (this.text == '請選擇') {
                    if ($(this).parent().attr("id").indexOf("ddlStore") < 0){
                        $(this).parent().siblings("span").show();
                        $('.form-unit').has($(this)).addClass("error");
                    } else {
                        $("#errBranch").show();
                        $('.form-unit').has($(this)).addClass("error");
                    }
                    $(this).parent().focus();
                    scroll(0,250);
                    rlt = false;
                } else {
                    if ($(this).parent().attr("id").indexOf("ddlStore") < 0){
                        $(this).parent().siblings("span").hide();
                    } else {
                        $("#errBranch").hide();
                    }
                    $('.form-unit').has($(this)).removeClass("error");
                }
                    
                if (this.text.indexOf('已售完') > -1) {
                    alert('請勿選擇已售完的選項，謝謝');
                    rlt = false;
                }
            });
            return rlt;
        }

        function checkCardNo(tb) {
            $("#CardNo").css("background-color", "");
            $("#errMsg").hide();
            var maxLength = $(tb).attr('maxlength');
            var reg = new RegExp("\\D");
            if (reg.test($(tb).val())) {
                $(tb).val($(tb).val().replace(reg, ''));
            }

            if ($(tb).val().length == maxLength) {
                var cc = $(".cc");
                cc.eq((cc.index($(tb))) + 1).focus();
            }
        }

        function checkReceiverDelivery() {
            updateArea($("#<%= ddlReceiverCity.ClientID %>").val(), "<%= ddlReceiverArea.ClientID %>", parseInt($("#<%= ddlReceiverArea.ClientID %>").val(), 10));
            $("#<%=hidReceiverAddress.ClientID %>").val($("#<%= txtReceiptAddress.ClientID %>").val());            
            if ($("#<%= ReceiverInfoPanel.ClientID %>:visible").length > 0) {
                var recCheck = true;
                if ($("#<%= ReceiverName.ClientID %>").val() == "") {
                    $("#receiverNameErr").show();
                    $("#tdRecName").addClass("error").removeClass("InvoiceTitleName");
                    $("#<%= ReceiverName.ClientID %>").focus();
                    recCheck = false;
                } else {
                    $("#receiverNameErr").hide();
                    $("#tdRecName").removeClass("error").addClass("InvoiceTitleName");
                }

                if ($("#<%= ReceiverMobile.ClientID %>").val() == "") {
                    $("#receiverMobileErr").show();
                    $("#tdRecMobile").addClass("error").removeClass("InvoiceTitleName");
                    $("#<%= ReceiverMobile.ClientID %>").focus();
                    recCheck = false;
                }

                if ($("#<%= ReceiverAddress.ClientID %>").val() == "") {                         
                    $("#receiverAddrErr").show();
                    if ($("#<%= hiNotDeliveryIslands.ClientID %>").val() == "True")
                    {
                        $("#notDeliveryIslandsAddrErr").show();
                    }
                    $("#tdRecAddr").addClass("error").removeClass("InvoiceTitleName");
                    $("#<%= ReceiverAddress.ClientID %>").focus();
                    recCheck = false;
                } else {
                    var drb = $("#<%= ddlReceiverArea.ClientID %> option:selected").text();
                    if(drb == "請選擇" || drb == ""){
                        $("#receiverAddrErr").show();
                        if ($("#<%= hiNotDeliveryIslands.ClientID %>").val() == "True")
                        {
                            $("#notDeliveryIslandsAddrErr").show();
                        }
                        $("#tdRecAddr").addClass("error").removeClass("InvoiceTitleName");
                        $("#<%= ddlReceiverArea.ClientID %>").focus();
                        recCheck = false;
                    } else {
                        if($("#<%= ddlReceiverCity.ClientID %> option:selected").text() == "請選擇"){
                            $("#receiverAddrErr").show();
                            if ($("#<%= hiNotDeliveryIslands.ClientID %>").val() == "True")
                            {
                                $("#notDeliveryIslandsAddrErr").show();
                            }
                            $("#tdRecAddr").addClass("error").removeClass("InvoiceTitleName");
                            $("#<%= ddlReceiverCity.ClientID %>").focus();
                            recCheck = false;
                        }
                    }
                }

                if (recCheck) {
                    var receiverAddr = $("#<%= ddlReceiverCity.ClientID %>").children(":selected").html() + 
                        $("#<%= ddlReceiverArea.ClientID %>").children(":selected").html() + 
                        $("#<%= ReceiverAddress.ClientID %>").val();
                    $("#<%= hidReceiverAddress.ClientID %>").val(receiverAddr);
                }

                return recCheck;
            } else {
                return true;
            }
        }

        function checkReceiptAddress() {
            var result = true;
            
            if ($("select[id$=ddlReceiptCity] option:selected").val() == "-1") {
                $("select[id$=ddlReceiptCity]").focus();
                result = false;
            }
            if ($("select[id$=ddlReceiptArea] option:selected").val() == "-1") {
                $("select[id$=ddlReceiptArea]").focus();
                result = false;
            }
            if ($.trim($("input[id$=txtReceiptAddress]").val()) == "") {
                $("input[id$=txtReceiptAddress]").focus();
                result = false;
            }
            if (result == false) {
                $("#tdReceiptAddress").addClass("error").find('.message').show();
            } else {
                $("#tdReceiptAddress").removeClass("error").find('.message').hide();
            }

            if ($.trim($('#txtReceiptTitle').val()) == '') {
                $('#txtReceiptTitle').parent().parent().addClass("error");
                $('#txtReceiptTitle').siblings('.message').show();
                result = false;
            } else {
                $('#txtReceiptTitle').parent().parent().removeClass("error");
                $('#txtReceiptTitle').siblings('.message').hide();
            }
            if ($('input[id$=txtReceiptNumber]').val().length > 0 && $('input[id$=txtReceiptNumber]').val().length  < 8) {
                $('input[id$=txtReceiptNumber]').parent().addClass("error");
                $('input[id$=txtReceiptNumber]').next('.message').show();
                result = false;
            } else {
                $('input[id$=txtReceiptNumber]').parent().removeClass("error");
                $('input[id$=txtReceiptNumber]').next('.message').hide();
            }

            return result;
        }

        function checkAPIopt() {
            var reg = new RegExp("\\D");
            var cards = $(":text.cc");
            var wrong = false;
            $.each(cards, function () {
                if (reg.test($(this).val()) || $(this).val().length != $(this).attr('maxlength')) {
                    wrong = true;
                }
            });

            if (wrong) {
                $("#CardNo").css("background-color", "#feb3b3").css("width", "95%");
                $("#errMsg").show();
                return false;
            } else {
                return true;
            }
        }

        $(document).ready(function () {
            $(":text.cc").bind("keyup", function () { checkCardNo(this); }).css("text-align", "center").attr("autocomplete", "off");
            $("#<%= SecurityCode.ClientID %>").attr("autocomplete", "off"); 
            $("#<%= ReceiverMobile.ClientID %>").click(function () {$("#receiverMobileErr").hide(); $("#tdRecMobile").removeClass("error").addClass("InvoiceTitleName"); });
            $("#<%= ReceiverAddress.ClientID %>").click(function () {$("#receiverAddrErr").hide(); $("#tdRecAddr").removeClass("error").addClass("InvoiceTitleName"); });
            $("#rdl").change(function () { updateReceiver($("#rdl").val()); });
            $("#txtReceiptTitle").bind('click keydown', function() {
                $('#txtReceiptTitle').parent().parent().removeClass("error");
                $('#txtReceiptTitle').siblings('.message').hide();
            });
            $("#ReceiverName").bind('click keydown', function() {
                $("#receiverNameErr").hide();
                $("#tdRecName").removeClass("error").addClass("InvoiceTitleName");
            });
            $('#ddlReceiptCity, #ddlReceiptArea, #txtReceiptAddress').bind('click keydown', function() {
                $("#tdReceiptAddress").removeClass("error").find('.message').hide();
            });
            

            $(document).find("select").each(function () {
                if (this.id.indexOf("ddlMultOption") != -1 || this.id == "<%= ddlStore.ClientID %>") {
                    $(this).change(function () { checkBranch(); });
                }
            });

            if ($("#<%= hiNotDeliveryIslands.ClientID %>").val() == "True") {
                window.deliveryToIsland = false;
            } else {
                window.deliveryToIsland = true;
            }
            $.cascadeAddress.setup('<%=WebUtility.GetSiteRoot()%>', function() {
                $.cascadeAddress.bind($('#ddlReceiptCity'), $('#ddlReceiptArea'), $('#txtReceiptAddress'), -1, -1, '', window.deliveryToIsland);
                $.cascadeAddress.bind($('#ddlReceiverCity'), $('#ddlReceiverArea'), $('#ReceiverAddress'), -1, -1, '', window.deliveryToIsland);
            });

            ChangeQty();
            if ("<%= hidprice.Value %>" == "0") {
                $("#<%=ddlQty.ClientID%>").attr("disabled", true);
            }

            if ($('#<%=rbPayWayByAtm.ClientID %>').is(':checked')) {
                $("#tdATMNotice").show();
                $("#tdCreditCardNotice").hide();
            } else {
                $("#tdATMNotice").hide();
                $("#tdCreditCardNotice").show();
            }
            
            // for receipt
            $.fn.inputableDepandOnCheckbox = function(chbox, defaultinputnable) {
                var inputboxs = this;
                chbox.click(function() {
                    if ($(this).is(':checked')) {
                        inputboxs.removeAttr('readonly').css('background-color', 'white');
                    } else {
                        inputboxs.val('').attr('readonly', true).css('background-color', '#ddd');
                    }
                });
                if (defaultinputnable) {
                    chbox.attr('checked', true);
                    inputboxs.removeAttr('readonly').css('background-color', 'white');
                } else {
                    chbox.removeAttr('checked');
                    inputboxs.val('').attr('readonly', true).css('background-color', '#ddd');
                }
            };
            $('input[id$=txtReceiptNumber],#txtReceiptTitle').inputableDepandOnCheckbox($('#chkReceiptForBusiness'), true);

            $('input[id$=rbPayWayByCreditCard]').click(function() {
                $('#tdATMNotice').hide();
                $('#tdCreditCardNotice').css('visibility', 'visible');
            });

            // 收件人資料同捐款人
            copyBuyerInfo();
        });

        function block_postback() {
            $.blockUI({ message: "<h1><img src='../Themes/PCweb/images/spinner.gif' />  交易進行中...</h1>", css: { width: '150px'} });
        }

        function BackToEdit() {
            $('#divFillPurchaseInfo').show();
            $('#divPurchaseInfoConfirm').hide();
            scroll(0,180);
        }

        function goToPay(){
            
            // 切換Panel
            $('#divFillPurchaseInfo').hide();
            $('#divPurchaseInfoConfirm').show();

            fillPponInfo();
            filBuyerInfo();

            var isEntrustSell = <%=EntrustSell != EntrustSellType.No ? "true" : "false"%>;

            if (isEntrustSell) {
                fillReceiptInfo();
            }

            if (0 == $("#<%= ReceiverInfoPanel.ClientID %>").length) {
                $("#ConfirmReceiverInfo").hide();
            } else {
                fillReceiverInfo();
            }

            if (0 == $("#<%= BuyerAddressPanel.ClientID %>").length) {
                $("#ConfirmBuyerAddress").hide();
            }

            if (0 == parseInt($("#total").html())) {
                $("#divConfirmPayWay").hide();
            } else {
                $("#divConfirmPayWay").show();
            }
            scroll(0,180);
        }

        function fillPponInfo(){
            var info = "[atmOption]<div class='form-unit'><label for='' class='unit-label title'>專案名稱</label><div class='data-input'><p>" + 
                $('#dealName').text() + "</p></div></div><div class='form-unit'><label for='' class='unit-label title'>數量</label><div class='data-input'><p>" + $("#<%= ddlQty.ClientID %>").val() + 
                "</p></div></div><div class='form-unit'><label class='unit-label title'>總計</label><div class='data-input'><p class='important'>$" + $('#lsubtotal').text() + "</p></div></div>";

            if ($('#dCharge').length > 0) {
                info += "<div class='form-unit'><label class='unit-label title'>運費</label><div class='data-input'><p>" + $('#dCharge').text() + "</p></div></div>";
            }

            var t = "";
            $(document).find('input:checked,option:selected').each(function () {
                if (this.type != 'radio' && this.id != "<%= AddReceiverInfo.ClientID %>" && this.id != "<%= chkBuyerSync.ClientID %>" 
                    && this.id != 'chkReceiptForBusiness') {
                    var parentId = $(this).parent().attr("id");
                    if (parentId != undefined && parentId.indexOf("ddlMultOption") != -1) {
                        if (this.text != '請選擇') {
                            t += "<div class='form-unit'><label for='' class='unit-label title'>" + 
                                $(this).parent().parent().siblings("#MultOptions").children("span").html() +
                                    "</label><div class='data-input'><p>" + this.text + "</p></div></div>";
                        }
                    } else if (parentId == "<%= ddlStore.ClientID %>") {
                        if (this.text != '請選擇') {
                            t += "<div class='form-unit'><label for='' class='unit-label title'>公益團體</label><div class='data-input'><p>" + this.text + "</p></div></div>";
                        }
                    }
                }
            });
            info = info.replace("[atmOption]", t);
            $("#atmOption").html(info);
            var payway = '';
            if ($('#<%= rbPayWayByCreditCard.ClientID %>').is(':checked')) {
                payway = $('#<%= rbPayWayByCreditCard.ClientID %>').next('label').html();
            } else if ($('#<%= rbPayWayByAtm.ClientID %>').is(':checked')) {
                payway = $('#<%= rbPayWayByAtm.ClientID %>').next('label').html();
            } else {
                payway = "發生錯誤";
            }
            $('#ConfirmPayWay').html($.trim(payway));
        }
        
        function filBuyerInfo(){
            $("#buyerNameInfo").html($("#<%= BuyerName.ClientID %>").val());
            $("#buyerTel").html($("#<%= BuyerMobile.ClientID %>").val());
            if (0 != $("#<%= BuyerAddressPanel.ClientID %>").length) {
                var buyerAddr = $("#<%= ddlBuyerCity.ClientID %>").children(":selected").html() + 
                    $("#<%= ddlBuyerArea.ClientID %>").children(":selected").html() + 
                    $("#<%= BuyerAddress.ClientID %>").val();
                $("#BuyerAddressConfirm").html(buyerAddr);
            }
        }
        
        function fillReceiverInfo(){
            $("#ConfirmReceiverName").html($("#<%= ReceiverName.ClientID %>").val());
            $("#ConfirmReceiverTel").html($("#<%= ReceiverMobile.ClientID %>").val());
            $("#ConfirmReceiverAddress").html($("#<%= hidReceiverAddress.ClientID %>").val());
        }

        function fillReceiptInfo() {
            
            if ($('#chkReceiptForBusiness').is(':checked') == true) {
                $('.ai').show();
            }else {
                $('.ai').hide();
            }

            var addr = $("#<%= ddlReceiptCity.ClientID %>").children(":selected").html() +
                $("#<%= ddlReceiptArea.ClientID %>").children(":selected").html() +
                $("#<%= txtReceiptAddress.ClientID %>").val();
            
            $("#receiptTitle").text($("#txtReceiptTitle").val());            
            $("#ConfirmReceiverName").html($("#<%= ReceiverName.ClientID %>").val());
            $("#receiptNumber").text($("#<%= txtReceiptNumber.ClientID %>").val());            
            if ($.trim($("#receiptNumber").text())=='') {
                $("#receiptNumber").closest('.form-unit').hide();
            }
            $("#receiptAddress").text(addr);
        }

        function copyBuyerInfo() {
            if ($("#<%= chkBuyerSync.ClientID %>").is(":checked")) {                
                $("#receiverNameErr").hide(); 
                $("#tdRecName").removeClass("error").addClass("InvoiceTitleName");
                $("#receiverMobileErr").hide(); 
                $("#tdRecMobile").removeClass("error").addClass("InvoiceTitleName");
                $("#receiverAddrErr").hide(); 
                $("#notDeliveryIslandsAddrErr").hide();
                $("#tdRecAddr").removeClass("error").addClass("InvoiceTitleName");
                $("#<%=ReceiverMobile.ClientID %>").val($("#<%=BuyerMobile.ClientID %>").val());
                $("#<%= ddlReceiverCity.ClientID %>").val($("#<%= ddlBuyerCity.ClientID %>").val());
                //updateArea($("#<%= ddlBuyerCity.ClientID %>").val(), "<%= ddlReceiverArea.ClientID %>", parseInt($("#<%= ddlBuyerArea.ClientID %>").val(), 10));
                $("#<%=ReceiverAddress.ClientID %>").val($("#<%=BuyerAddress.ClientID %>").val());
            } else {
                $("#<%=ReceiverName.ClientID %>").val("");
                $("#<%=ReceiverMobile.ClientID %>").val("");
                $("#<%= ddlReceiverCity.ClientID %>").val("0");
                //updateArea(0, "<%= ddlReceiverArea.ClientID %>", 0);
                $("#<%=ReceiverAddress.ClientID %>").val("");
            }
        }

        function updateArea(cid, tagId, selectedValue) {
            $.dnajax("KindPay.aspx/GetZones", "{pid:'" + cid +"',notDeliveryIslands:'"+$("#<%= hiNotDeliveryIslands.ClientID %>").val()+ "'}", function (msg) {
                var el = $('#' + tagId);
                el.children().remove();
                el.addItems(eval(msg.d), function (t) { return t.name; }, function (t) { return t.id; });
                el.val(selectedValue);
            });
        }

        function updateBuilding(cid, tagId, selectedValue) {
            $.dnajax("KindPay.aspx/GetLocations", "{cid:'" + cid + "'}", function (msg) {
                var el = $('#' + tagId);
                el.children().remove();
                el.addItems(eval(msg.d), function (t) { return t.name; }, function (t) { return t.id; });
                el.val(selectedValue);
            });
        }

        function updateReceiver(recId) {
            if (0 == recId) {
                $("#<%=ReceiverName.ClientID %>").val("");
                $("#<%=ReceiverMobile.ClientID %>").val("");
                $("#<%=ddlReceiverCity.ClientID %>").val("0");
                updateArea(0, "<%= ddlReceiverArea.ClientID %>", 0);                

                $("#<%=ReceiverAddress.ClientID %>").val("");
            } else {
                $("#receiverNameErr").hide(); 
                $("#tdRecName").removeClass("error").addClass("InvoiceTitleName");
                $("#receiverMobileErr").hide(); 
                $("#tdRecMobile").removeClass("error").addClass("InvoiceTitleName");
                $("#receiverAddrErr").hide(); 
                $("#tdRecAddr").removeClass("error").addClass("InvoiceTitleName");
                $.dnajax(window.ServerLocation + "service/ppon/GetReceiver", "{id:'" + recId + "'}", function (res) {
                    if (res == null) return;
                    $("#<%=ReceiverName.ClientID %>").val(res.name);
                    $("#<%=ReceiverMobile.ClientID %>").val(res.mobile);
                    $("#<%=ddlReceiverCity.ClientID %>").val(res.city);
                    updateArea(res.city, "<%= ddlReceiverArea.ClientID %>", res.town);
                    $("#<%=ReceiverAddress.ClientID %>").val(res.addr);
                });
            }
        }

        function ClickBuy() {
            if (checkopt()) {
                goToPay();
            } else {
                $.unblockUI(); 
                return false;
            }
        }

        function SyncDeliveryPerson() {
            $('[id*=ReceiverName]').add('#txtReceiptTitle').val($('#BuyerName').val());
            $("#<%= ddlReceiptCity.ClientID %>").val($("#<%= ddlBuyerCity.ClientID %>").val());
            updateArea($("#<%= ddlBuyerCity.ClientID %>").val(), "<%= ddlReceiptArea.ClientID %>", parseInt($("#<%= ddlBuyerArea.ClientID %>").val(), 10));
            $("#<%=txtReceiptAddress.ClientID %>").val($("#<%=BuyerAddress.ClientID %>").val());
            updateArea($("#<%= ddlReceiverCity.ClientID %>").val(), "<%= ddlReceiverArea.ClientID %>", parseInt($("#<%= ddlReceiverArea.ClientID %>").val(), 10));
            $("#<%=hidReceiverAddress.ClientID %>").val($("#<%=BuyerAddress.ClientID %>").val());
            $("#receiverNameErr").hide();
            $("#tdRecName").removeClass("error").addClass("InvoiceTitleName").find('.message').hide();
            if ($("select[id$=ddlReceiptCity] option:selected").val() == "-1") {
                $("select[id$=ddlReceiptCity]").focus();
            }
            if ($("select[id$=ddlReceiptArea] option:selected").val() == "-1") {
                $("select[id$=ddlReceiptArea]").focus();
            }
            if ($.trim($("input[id$=txtReceiptAddress]").val()) == "") {
                $("input[id$=txtReceiptAddress]").focus();
            }
            $("#tdReceiptAddress").removeClass("error").find('.message').hide();
            $('#txtReceiptTitle').parent().parent().removeClass("error");
            $('#txtReceiptTitle').siblings('.message').hide();

        }

        function CopyReceiverInfo() {
            $('[id*=ReceiverMobile]').val($('input[id*=BuyerMobile]').val());
            $.cascadeAddress.copy($('#ddlReceiptCity'), $('#ddlReceiptArea'), $('#txtReceiptAddress'), 
                $('#ddlReceiverCity'), $('#ddlReceiverArea'), $('#ReceiverAddress'), window.deliveryToIsland);
        }
    </script>
    <asp:HiddenField ID="hidprice" runat="server" />
    <asp:HiddenField ID="hidsubtotal" runat="server" />
    <asp:HiddenField ID="hidtotal" runat="server" />
    <asp:HiddenField ID="hidqty" runat="server" />
    <asp:HiddenField ID="hidIPromoValue" runat="server" Value='' />
    <asp:HiddenField ID="hpromo" runat="server" />
    <asp:HiddenField ID="hidcstatic" runat="server" />
    <asp:HiddenField ID="hiddCharge" runat="server" />
    <asp:HiddenField ID="hidPayDepartment" runat="server" />
    <asp:HiddenField ID="hidFreight" runat="server" />
    <asp:HiddenField ID="hidReceiverAddress" runat="server" />
    <asp:HiddenField ID="hiNotDeliveryIslands" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hidMultiBranch" runat="server" />
    <asp:HiddenField ID="hidIsZeroEvent" runat="server" />
    <div id="TOPBanner">
    </div>
    <asp:Panel ID="PanelBuy" runat="server">
        <div id="Left" class="rd-payment-Left-width">
            <div id="maincontent" class="clearfix">
                <asp:Panel ID="NomalPay" runat="server">
                    <%--好康交易結帳--%>
                    <div id="divFillPurchaseInfo" class="content-group payment">
                        <h1 class="rd-payment-h1">
                            愛心公益捐款</h1>
                        <hr class="header_hr rd-payment-width" />
                        <h2 class="rd-payment-h2">
                            <div class="step-num">
                                1</div>
                            選擇受贈單位 / 活動</h2>
                        <div class="grui-form">
                            <asp:Panel ID="pnlOption" runat="server">
                                <asp:UpdatePanel ID="upxy" runat="server" ChildrenAsTriggers="true" RenderMode="Inline">
                                    <ContentTemplate>
                                        <div class="form-unit">
                                            <label class="unit-label" for="">
                                                公益團體</label>
                                            <div class="data-input">
                                                <asp:PlaceHolder ID="pStore" runat="server">
                                                    <asp:PlaceHolder ID="divStore" runat="server">
                                                        <asp:DropDownList ID="ddlStoreCity" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlStoreCity_SelectedIndexChanged">
                                                        </asp:DropDownList>
                                                        <asp:DropDownList ID="ddlStoreArea" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlStoreArea_SelectedIndexChanged">
                                                            <asp:ListItem Text="請選擇" Value="0" />
                                                        </asp:DropDownList>
                                                        <asp:DropDownList ID="ddlStore" runat="server">
                                                        </asp:DropDownList>
                                                        <span id="errBranch" class="error-text" style="display: none;">請選擇</span> </asp:PlaceHolder>
                                                    <asp:TextBox ID="HiddenSingleStoreGuid" runat="server" Visible="false"></asp:TextBox>
                                                </asp:PlaceHolder>
                                            </div>
                                            <div id="divSingleStore" runat="server" class="data-input">
                                                <p>
                                                    <asp:Label ID="lblSingleStore" runat="server"></asp:Label>
                                                </p>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                                <asp:Repeater ID="rpOption" runat="server" OnItemDataBound="rpOption_ItemDataBound">
                                    <ItemTemplate>
                                        <div class="form-unit">
                                            <label id="MultOptions" class="unit-label" for="">
                                                <asp:Label ID="lblMultOption" runat="server" Text='<%# Eval("accessorygroupname") %>'></asp:Label>
                                            </label>
                                            <div class="data-input">
                                                <asp:DropDownList ID="ddlMultOption" runat="server">
                                                </asp:DropDownList>
                                                <span class="error-text" style="display: none;">請選擇</span>
                                            </div>
                                        </div>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </asp:Panel>
                            <div class="form-unit rd-payment-form-unit-disappear">
                                <label class="unit-label" for="">
                                    專案名稱</label>
                                <div class="data-input">
                                    <p id="dealName">
                                        <%=dealName %><span id="dsName" style="display: none;"></span>
                                    </p>
                                </div>
                            </div>
                            <div class="form-unit">
                                <label class="unit-label" for="">
                                    捐款金額</label>
                                <div class="data-input">
                                    <p class="important">
                                        $<asp:Literal ID="lprice" runat="server"></asp:Literal>
                                    </p>
                                </div>
                            </div>
                            <div class="form-unit">
                                <label class="unit-label" for="">
                                    數量</label>
                                <div class="data-input">
                                    <asp:DropDownList ID="ddlQty" runat="server" onblur="ChangeQty()" onchange="ChangeQty('1')">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="form-unit">
                                <label class="unit-label" for="">
                                    小計</label>
                                <div class="data-input">
                                    <p class="important">
                                        $<span id="lsubtotal"><%=subtotal.ToString("F0") %></span></p>
                                </div>
                            </div>
                            <div id="kindFreight" runat="server" class="form-unit">
                                <label class="unit-label" for="">
                                    運費
                                </label>
                                <div class="data-input">
                                    <p>
                                        <span id="dCharge">
                                            <%= NoSippingFeeMessage ? "<span class='hint'>-貨到付運費</span>" : (DeliveryCharge.ToString("F0"))%>
                                        </span><span id="lbldc" runat="server"></span>
                                    </p>
                                </div>
                            </div>
                            <div class="form-unit">
                                <label class="unit-label" for="">
                                    總計</label>
                                <div class="data-input">
                                    <p class="important">
                                        $<span id="ltotal"><%=total.ToString("F0") %></span></p>
                                </div>
                            </div>
                        </div>
                        <h2 class="rd-payment-h2">
                            <div class="step-num">
                                2</div>
                            填寫捐款人資料
                        </h2>
                        <div class="grui-form">
                            <%--捐款人資訊--%>
                            <div id="tbBuyerInfoEdit">
                                <div id="tdEmail" class="form-unit">
                                    <label for="" class="unit-label">
                                        E-mail</label>
                                    <div class="data-input">
                                      <asp:TextBox id="BuyerEmail" runat="server" CssClass="input-full" style="border:0px" ReadOnly="true" ClientIDMode="Static"/>
                                    </div>
                                </div>
                                <div id="tdBuyerName" class="form-unit">
                                    <label for="" class="unit-label">
                                        姓名</label>
                                    <div class="data-input">
                                        <asp:TextBox ID="BuyerName" runat="server" CssClass="input-2over3" ClientIDMode="Static" 
                                             placeholder="請填寫正確姓名，以維護您的權益" />
                                        <div id="buyerNameErr" style="display:none">
                                            <span class="error-text">請填入購買人姓名！ </span>
                                        </div>
                                    </div>
                                </div>
                                <div id="tdBuyerMobile" class="form-unit">
                                    <label class="unit-label" for="">
                                        手機</label>
                                    <div class="data-input">
                                        <asp:TextBox ID="BuyerMobile" runat="server" CssClass="input-2over3" onblur="CopyReceiverInfo();"
                                            placeholder="請填寫手機號碼，以便傳送簡訊兌換券"/>
                                        <p class="note">
                                            <span id="buyerMobileErr" class="error-text" style="display: none;">請填入正確的手機號碼，共十碼！</span></p>
                                    </div>
                                </div>
                                <asp:Panel ID="BuyerAddressPanel" runat="server">
                                    <div class="form-unit" style="display: none;">
                                        <label class="unit-label" for="">
                                            地址</label>
                                        <div class="data-input">
                                            <asp:UpdatePanel ID="upz" runat="server" ChildrenAsTriggers="true" RenderMode="Inline">
                                                <ContentTemplate>
                                                    <asp:DropDownList ID="ddlBuyerCity" runat="server" AutoPostBack="true" CssClass="select-wauto"
                                                        DataTextField="CityName" DataValueField="Id" OnSelectedIndexChanged="ddlBuyerCity_SelectedIndexChanged">
                                                    </asp:DropDownList>
                                                    <asp:DropDownList ID="ddlBuyerArea" runat="server" AutoPostBack="true" CssClass="select-wauto"
                                                        DataTextField="CityName" DataValueField="Id" OnSelectedIndexChanged="ddlBuyerArea_SelectedIndexChanged">
                                                    </asp:DropDownList>
                                                    <asp:DropDownList ID="ddlBuyerBuilding" runat="server" CssClass="select-wauto" DataTextField="BuildingName"
                                                        DataValueField="Guid" />
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                            <asp:TextBox ID="BuyerAddress" runat="server" CssClass="input-full"></asp:TextBox>
                                        </div>
                                    </div>
                                </asp:Panel>
                            </div>
                            <%--收據資料--%>
                            <asp:PlaceHolder ID="receiptPanel" runat="server">
                                <div id="divReceiptEdit">
                                    <div class="form-unit">
                                        <label class="unit-label title" for="">
                                            收據資料</label>
                                        <div class="data-input">
                                            <p class="note">
                                                (將由公益團體開立捐贈收據寄送至收據地址)&nbsp;&nbsp;&nbsp;&nbsp;<a href="#" onclick="SyncDeliveryPerson();return false;">同會員個人資料</a>
                                            </p>
                                        </div>
                                    </div>
                                    <div>
                                        <div class="form-unit">
                                            <label class="unit-label" for="">
                                                收據抬頭</label>
                                            <div class="data-input">
                                                <asp:TextBox ID="txtReceiptTitle" runat="server" CssClass="input-half" ClientIDMode="Static" />
                                                <p class="note">姓名或公司行號</p>
                                                <span class="message error-text" style="display: none;">請填寫收據抬頭！</span>
                                            </div>
                                        </div>
                                        <div id="tdRecName" class="form-unit">
                                            <label class="unit-label" for="">
                                                收據收件人</label>
                                            <div class="data-input">
                                                <asp:TextBox ID="ReceiverName" runat="server" CssClass="input-half" ClientIDMode="Static"></asp:TextBox>
                                                <span id="receiverNameErr" class="error-text" style="display: none;">請填寫收件人姓名！</span>
                                            </div>
                                        </div>
                                        <div id="tdReceiptAddress" class="form-unit">
                                            <label class="unit-label" for="">
                                                地址</label>
                                            <div class="data-input">
                                                <asp:UpdatePanel ID="UpdatePanel1" runat="server" ChildrenAsTriggers="true" RenderMode="Inline">
                                                    <ContentTemplate>
                                                        <asp:DropDownList ID="ddlReceiptCity" runat="server" CssClass="select-wauto" onchange="CopyReceiverInfo();" ClientIDMode="Static">
                                                        </asp:DropDownList>
                                                        <asp:DropDownList ID="ddlReceiptArea" runat="server" CssClass="select-wauto" onchange="CopyReceiverInfo();" ClientIDMode="Static">
                                                        </asp:DropDownList>
                                                        <br />
                                                        <asp:TextBox ID="txtReceiptAddress" runat="server" CssClass="input-full align-fix"
                                                            size="20" onchange="CopyReceiverInfo();" ClientIDMode="Static"/>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                                <p class="message error-text" style="display: none">
                                                    請填寫正確的地址
                                                </p>
                                            </div>
                                        </div>
                                        <div class="form-unit">
                                            <label class="unit-label" for="">
                                                統一編號</label>
                                            <div class="data-input">
                                                <input type="checkbox" id="chkReceiptForBusiness" checked="checked" style="display: none;" />
                                                <asp:TextBox ID="txtReceiptNumber" runat="server" CssClass="cc input-half" MaxLength="8" />
                                                <p class="message error-text" style="display: none">
                                                    請填寫統一編號共八碼
                                                </p>
                                                <p class="note">(將由公益團體開立捐贈收據寄送至收據地址)</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </asp:PlaceHolder>
                            <%--收件人資訊--%>
                            <asp:Panel ID="ReceiverInfoPanel" runat="server" Style="display: none;">
                                <div id="tbReceiverEdit">
                                    <div class="form-unit">
                                        <label class="unit-label title" for="">
                                            收件人資訊</label>
                                        <div class="data-input">
                                            <asp:CheckBox ID="chkBuyerSync" runat="server" onclick="copyBuyerInfo();" Text="同捐款人資訊"
                                                Checked="true" />
                                            <asp:Repeater ID="ReceiverList" runat="server">
                                                <HeaderTemplate>
                                                    <select id="rdl" class="select-wauto" name="receiverSelect">
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <option <%#Container.ItemIndex == 0 ? "selected='selected'" : "" %> value='<%#Eval("Id")%>'>
                                                        <%#Eval("AddressAlias")%></option>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    </select>
                                                </FooterTemplate>
                                            </asp:Repeater>
                                            <p class="note">
                                                請確實填寫收件人資訊，以確保商品順利送達。</p>
                                        </div>
                                    </div>
                                    <div id="tdRecMobile" class="form-unit">
                                        <label class="unit-label">
                                            聯絡電話</label>
                                        <div class="data-input">
                                            <asp:TextBox ID="ReceiverMobile" runat="server" CssClass="input-half"></asp:TextBox>
                                            <span id="receiverMobileErr" class="error-text" style="display: none;">請填寫收件人的聯絡電話！</span>
                                        </div>
                                    </div>
                                    <div id="tdRecAddr" class="form-unit rd-payment-form-unit-width">
                                        <label class="unit-label" for="">
                                            地址</label>
                                        <div class="data-input">
                                            <asp:UpdatePanel ID="upy" runat="server" ChildrenAsTriggers="true" RenderMode="Inline">
                                                <ContentTemplate>
                                                    <asp:DropDownList ID="ddlReceiverCity" runat="server" CssClass="select-wauto" ClientIDMode="Static">
                                                    </asp:DropDownList>
                                                    <asp:DropDownList ID="ddlReceiverArea" runat="server" CssClass="select-wauto" ClientIDMode="Static">
                                                    </asp:DropDownList>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                            <asp:TextBox ID="ReceiverAddress" runat="server" CssClass="input-full" ClientIDMode="Static"></asp:TextBox>
                                            <span id="receiverAddrErr" class="error-text" style="display: none;">請填寫收件地址！</span>
                                            <span id="notDeliveryIslandsAddrErr" class="error-text" style="display: none;">註:此商品恕不適用離島、外島地區，若您選擇本島以外收件人，請重新選擇收件人或更改收件地址！</span>
                                            <br />
                                            <asp:CheckBox ID="AddReceiverInfo" runat="server" Text="新增本次填寫的收件人資料" />
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>
                        </div>
                        <h2 class="rd-payment-h2 SelectedPayWay">
                            <div class="step-num rd-payment-step-num">
                                3</div>
                            選擇捐款方式
                        </h2>
                        <div class="grui-form">
                            <div class="form-unit SelectedPayWay">
                                <label class="unit-label title" for="">
                                    付款方式</label>
                                <div class="data-input">
                                    <asp:RadioButton ID="rbPayWayByCreditCard" runat="server" Checked="true" GroupName="PayWay"
                                        Text="信用卡付款" />
                                    <p class="subnote rd-payment-display">
                                        歡迎使用Visa卡<br />
                                        目前暫不接受美國運通(American Express)卡
                                    </p>
                                </div>
                            </div>
                            <div id="AtmPayWay" runat="server" class="form-unit SelectedPayWay">
                                <div class="data-input">
                                    <asp:RadioButton ID="rbPayWayByAtm" runat="server" GroupName="PayWay" Text="ATM付款" />
                                    <p id="ATMPayNote" runat="server" class="subnote rd-payment-display">
                                        請於今晚24時前完成付款
                                    </p>
                                    <p id="atmFull" runat="server">
                                        此檔好康ATM付款名額已滿，請您以刷卡方式購買本次好康</p>
                                </div>
                            </div>                   
                            <div class="form-unit end-unit rd-pay-pos-fix">
                                <div class="data-input rd-pay-span-ctr">
                                    <p id="ChoosePurchase" class="subnote">
                                        <input type="button" onclick="javascript:ClickBuy();" class="btn btn-large btn-primary rd-payment-xlarge-btn"
                                            value="下一步" />
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <%--訂單資料確認--%>
                    <div id="divPurchaseInfoConfirm" class="content-group payment" style="display: none">
                        <h1 id="TopTitle" class="rd-payment-h1">
                            受贈單位 / 活動確認</h1>
                        <hr class="header_hr rd-payment-width" />
                        <h2 class="important rd-payment-h2">
                            為了維護您的權益，請確認捐款資料無誤後送出！</h2>
                        <div class="grui-form">
                            <%--好康資訊--%>
                            <div id="confirmItemList">
                                <div id="atmOption">
                                </div>
                                <div class='form-unit'>
                                    <label for='' class='unit-label title'>
                                        支付總額</label><div class='data-input'>
                                            <p class='important'>
                                                $<span id="cltotal">0</span>
                                            </p>
                                        </div>
                                </div>
                            </div>
                            <%--捐款人資訊--%>
                            <div id="tbBuyerInfoConfirm">
                                <div class="form-unit">
                                    <label for="" class="unit-label">
                                        捐款人姓名</label>
                                    <div class="data-input">
                                        <p>
                                            <span id="buyerNameInfo"></span>
                                        </p>
                                    </div>
                                </div>
                                <div class="form-unit">
                                    <label for="" class="unit-label">
                                        手機</label>
                                    <div class="data-input">
                                        <p>
                                            <span id="buyerTel"></span>
                                        </p>
                                    </div>
                                </div>
                                <div id="ConfirmBuyerAddress" class="form-unit" style="display: none;">
                                    <label for="" class="unit-label">
                                        地址</label>
                                    <div class="data-input">
                                        <p>
                                            <span id="BuyerAddressConfirm"></span>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <%--收據資料--%>
                            <asp:PlaceHolder ID="phReceiptConfirm" runat="server">
                                <div id="divReceiptConfirm">
                                    <div class="form-unit ai">
                                        <label for="" class="unit-label">
                                            收據抬頭</label>
                                        <div class="data-input">
                                            <p>
                                                <span id="receiptTitle"></span>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="form-unit">
                                        <label for="" class="unit-label">
                                            收據收件人</label>
                                        <div class="data-input">
                                            <p>
                                                <span id="ConfirmReceiverName"></span>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="form-unit ai">
                                        <label for="" class="unit-label">
                                            統一編號</label>
                                        <div class="data-input">
                                            <p>
                                                <span id="receiptNumber"></span>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="form-unit">
                                        <label for="" class="unit-label">
                                            地址</label>
                                        <div class="data-input">
                                            <p>
                                                <span id="receiptAddress"></span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </asp:PlaceHolder>
                            <%--收件人資訊--%>
                            <asp:Panel ID="ReceiverInfoConfirmPanel" runat="server" Style="display: none;">
                                <div id="ConfirmReceiverInfo">
                                    <div class="form-unit">
                                        <label for="" class="unit-label title">
                                            收件人資訊</label>
                                        <div class="data-input" style="color: #090">
                                            <p>
                                                請確認收件人資料無誤，以確保商品順利送達。</p>
                                        </div>
                                    </div>
                                    <div class="form-unit">
                                        <label for="" class="unit-label">
                                            聯絡電話</label>
                                        <div class="data-input">
                                            <p>
                                                <span id="ConfirmReceiverTel"></span>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="form-unit">
                                        <label for="" class="unit-label">
                                            地址</label>
                                        <div class="data-input">
                                            <p>
                                                <span id="ConfirmReceiverAddress"></span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>
                            <%--捐款方式--%>
                            <div id="divConfirmPayWay" class="form-unit">
                                <label for="" class="unit-label title">
                                    捐款方式</label>
                                <div class="data-input">
                                    <p>
                                        <span id="ConfirmPayWay"></span>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <%--送出訂單or修改資料--%>
                        <div class="grui-form">
                            <div class="form-unit end-unit rd-pay-pos-fix">
                                <div class="data-input rd-pay-span-ctr">
                                    <p class="cancel-mr">
                                        <asp:Button ID="btnPayWayProcess" runat="server" CssClass="btn btn-large btn-primary rd-payment-large-btn"
                                            Text="送出愛心" OnClick="btnPayWay_Click" OnClientClick="block_postback();" />
                                        <input type="button" class="btn btn-large rd-payment-large-btn" value="修改資料" onclick="BackToEdit();" />
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <asp:Panel ID="ZeroPay" runat="server" Visible="false">
                    <div class="content-group payment">
                        <h1 class="rd-payment-h1">
                            索取好康</h1>
                        <hr class="header_hr rd-payment-width" />
                        <h2 class="rd-payment-h2">
                            【<asp:Label ID="lblZeroSeller" runat="server" Text=""></asp:Label>】
                            <asp:Label ID="lblZeroTitle" runat="server" Text=""></asp:Label></h2>
                        <div class="grui-form">
                            <div class="form-unit">
                                <div class="form-unit end-unit rd-pay-pos-fix">
                                    <div class="btn-center">
                                        <asp:Button ID="btnZeroCheckOut" OnClientClick="block_postback();" CssClass="btn btn-large btn-primary rd-payment-large-btn"
                                            runat="server" Text="我要索取" OnClick="ibCheckOut_Click" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <h2 class="rd-fd-title">
                            好康權益說明</h2>
                        <div class="grui-form">
                            <p>
                                <asp:Label ID="lblDesc" runat="server" Text=""></asp:Label>
                            </p>
                        </div>
                        <asp:HiddenField runat="server" ID="hidBuyerMobile" />
                    </div>
                </asp:Panel>
            </div>
        </div>
        <div id="Rightarea" class="rd-payment-qma">
            <div class="side_block">
                <div class="side_block_content">
                    <div class="side_block_title for-payment">
                        訂購Q&amp;A</div>
                    <p class="qa_title">
                        <a href="/ppon/newbieguide.aspx?s=2&q=20621" target="_blank">Q：我可以使用哪些方式付款？
                        </a>
                    </p>
                    <p class="qa_content">
                        A：您可透過以下方式付費：<br />
                        1. 17Life購物金<br />
                        2. 17Life紅利金<br />
                        3. 17Life折價券
                        <br />
                        4. PayEasy購物金<br />
                        5. 信用卡/Visa金融卡<br />
                        6. ATM付款
                    </p>
                    <p class="qa_title">
                        <a href="javascript:void(0)">Q：我買的好康能給別人用嗎?
                        </a>
                    </p>
                    <p class="qa_content">
                        A：可以。因為我們的配合商家僅認憑證不認人，但一張憑證只能使用一次。
                    </p>
                    <p class="qa_title">
                        <a href="/ppon/newbieguide.aspx?s=3&q=116" target="_blank">Q：已購買的好康如何使用？
                        </a>
                    </p>
                    <p class="qa_content">
                        A：可至會員專區 → <a href="https://www.17life.com/User/coupon_List.aspx" target="_blank">訂單/憑證</a>
                        中，下載或列印憑證後，持憑證至所提供之商家使用。<br />
                        您也可以<a href="https://www.17life.com/ppon/promo.aspx?cn=app" target="_blank">下載APP</a>，使用電子憑證。
                    </p>
                    <p class="qa_content line">
                        -------------------------- <a href="https://www.17life.com/Ppon/NewbieGuide.aspx"
                            target="_blank">更多常見Q&A >>></a>
                    </p>
                    <p class="qa_note">
                        特別說明：<br />
                        目前本站僅會先要求授權，不會直接進行請款， 當此優惠跨越集購門檻時，本站才會實際進行請款。若集購失敗，將不會有任何款項的請款，請放心!
                    </p>
                </div>
            </div>
            <uc1:Paragraph ID="p165" runat="server" />
        </div>
    </asp:Panel>
    <%--信用卡付款--%>
    <asp:Panel ID="PanelCreditCradAPI" runat="server" Visible="false">
        <div id="Left" class="rd-payment-Left-width">
            <div id="maincontent" class="clearfix">
                <div class="content-group payment">
                    <h1 class="rd-payment-h1 creditcard">
                        信用卡付款</h1>
                    <hr class="header_hr rd-payment-width" />
                    <div class="grui-form">
                        <div class="form-unit" style="display: none;">
                            <label for="" class="unit-label title">
                                訂單編號</label>
                            <div class="data-input">
                                <p>
                                    <asp:Label ID="OrderNumber" runat="server" Text="Label"></asp:Label>
                                    <asp:TextBox ID="HiddenOrderNumber" runat="server" Visible="false"></asp:TextBox></p>
                            </div>
                        </div>
                        <div class="form-unit">
                            <label for="" class="unit-label title">
                                交易日期</label>
                            <div class="data-input">
                                <p>
                                    <asp:Label ID="TransDate" runat="server" Text="Label"></asp:Label></p>
                            </div>
                        </div>
                        <div class="form-unit">
                            <label for="" class="unit-label title">
                                交易金額</label>
                            <div class="data-input">
                                <p class="important">
                                    $<asp:Label ID="TransAmount" runat="server" Text="Label"></asp:Label></p>
                                <asp:TextBox ID="HiddenTransAmount" runat="server" Visible="false"></asp:TextBox>
                            </div>
                        </div>
                        <div id="CardNo" class="form-unit">
                            <label for="" class="unit-label title">
                                信用卡號</label>
                            <div class="data-input">
                                <asp:TextBox ID="CardNo1" runat="server" class="input-credit cc" MaxLength="4" pattern="[0-9]*"></asp:TextBox>
                                -
                                <asp:TextBox ID="CardNo2" runat="server" class="input-credit cc" MaxLength="4" pattern="[0-9]*"></asp:TextBox>
                                -
                                <asp:TextBox ID="CardNo3" runat="server" class="input-credit cc" MaxLength="4" pattern="[0-9]*"></asp:TextBox>
                                -
                                <asp:TextBox ID="CardNo4" runat="server" class="input-credit cc" MaxLength="4" pattern="[0-9]*"></asp:TextBox>
                                <span id="errMsg" class="error-text" style="display: none;">輸入錯誤</span>
                            </div>
                        </div>
                        <div class="form-unit">
                            <label for="" class="unit-label title">
                                有效期限</label>
                            <div class="data-input">
                                <asp:DropDownList ID="ExpMonth" runat="server" CssClass="select-wauto cc">
                                    <asp:ListItem Value="01">01</asp:ListItem>
                                    <asp:ListItem Value="02">02</asp:ListItem>
                                    <asp:ListItem Value="03">03</asp:ListItem>
                                    <asp:ListItem Value="04">04</asp:ListItem>
                                    <asp:ListItem Value="05">05</asp:ListItem>
                                    <asp:ListItem Value="06">06</asp:ListItem>
                                    <asp:ListItem Value="07">07</asp:ListItem>
                                    <asp:ListItem Value="08">08</asp:ListItem>
                                    <asp:ListItem Value="09">09</asp:ListItem>
                                    <asp:ListItem Value="10">10</asp:ListItem>
                                    <asp:ListItem Value="11">11</asp:ListItem>
                                    <asp:ListItem Value="12">12</asp:ListItem>
                                </asp:DropDownList>
                                <p class="credit">
                                    月/20</p>
                                <asp:DropDownList ID="ExpYear" runat="server" CssClass="select-wauto">
                                    <asp:ListItem Value="16">16</asp:ListItem>
                                    <asp:ListItem Value="17">17</asp:ListItem>
                                    <asp:ListItem Value="18">18</asp:ListItem>
                                    <asp:ListItem Value="19">19</asp:ListItem>
                                    <asp:ListItem Value="20">20</asp:ListItem>
                                    <asp:ListItem Value="21">21</asp:ListItem>
                                    <asp:ListItem Value="22">22</asp:ListItem>
                                    <asp:ListItem Value="23">23</asp:ListItem>
                                    <asp:ListItem Value="24">24</asp:ListItem>
                                    <asp:ListItem Value="25">25</asp:ListItem>
                                    <asp:ListItem Value="26">26</asp:ListItem>
                                    <asp:ListItem Value="27">27</asp:ListItem>
                                    <asp:ListItem Value="28">28</asp:ListItem>
                                    <asp:ListItem Value="29">29</asp:ListItem>
                                    <asp:ListItem Value="30">30</asp:ListItem>
                                </asp:DropDownList>
                                <p>
                                    年</p>
                            </div>
                        </div>
                        <div class="form-unit">
                            <label for="" class="unit-label title">
                                信用卡背面末三碼</label>
                            <div class="data-input">
                                <asp:TextBox ID="SecurityCode" runat="server" class="input-credit" MaxLength="3"
                                    pattern="[0-9]*"></asp:TextBox>
                                <div class="safelycode">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="grui-form">
                        <div class="form-unit end-unit rd-pay-pos-fix">
                            <div class="data-input rd-pay-span-ctr">
                                <p class="cancel-mr">
                                    <asp:Button runat="server" ID="AuthSSL" Text="確認付款" CssClass="btn btn-large btn-primary rd-payment-xlarge-btn"
                                        OnClick="AuthSSL_Click" OnClientClick='if(checkAPIopt()){block_postback();return true;}else{$.unblockUI(); return false;}' />
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="Rightarea" class="rd-payment-qma">
            <div class="side_block">
                <div class="side_block_content">
                    <div class="side_block_title for-payment">
                        17Life安心交易</div>
                    <p>
                        您好，本網站(17Life)之刷卡機制係採用台新銀行之刷卡金流系統；台新銀行已通過<span class="safelytext">ISO 27001:2005 / BS
                            7799:2005國際資訊安全管理標準</span>驗證，可鑑別並進行有效管理各項資訊運用所潛在的威脅與風險。本網站將不會留存您填寫的各項資料，您的刷卡資料會經由<span
                                class="safelytext">SSL安全協定加密</span>傳輸至台新銀行，絕對安全無虞，您可以放心刷卡。
                    </p>
                    <br />
                    <p>
                        <img name="lpkilemp" height="55" width="115" border="0" src="https://smarticon.geotrust.com/smarticon?ref=www.17life.com"
                            alt="Click for company profile" oncontextmenu="return false;"></p>
                    <br />
                    <p class="important">
                        ※目前暫不接受美國運通(American Express)卡，<br />
                        造成不便敬請見諒。</p>
                </div>
            </div>
        </div>
    </asp:Panel>
    <asp:Panel ID="PanelResult" runat="server" Visible="false">
        <div id="Left" class="rd-payment-Left-width">
            <div id="maincontent" class="clearfix">
                <asp:Panel ID="pan_CreditcardSuccess" runat="server" Visible="false">
                    <div class="TaishinPayNote">
                        <div class="GeoTru">
                            <%-- webbot bot="HTMLMarkup" startspan --%>
                            <script language="JavaScript" type="text/javascript" src="https://smarticon.geotrust.com/si.js">
                            </script>
                            <%-- webbot bot="HTMLMarkup" endspan --%>
                        </div>
                    </div>

                </asp:Panel>
                <asp:Panel ID="pan_ZeroPrice" runat="server" Visible="false">
                    <div class="content-group payment">
                        <div class="pay_success">
                        </div>
                        <h1 class="success">
                            恭喜您，索取成功！</h1>
                        <hr class="header_hr rd-payment-width" />
                        <asp:Panel runat="server" ID="pnlShowCoupon" Visible="false">
                            <div class="fd_success_sn">
                                <asp:Label ID="lblCoupon" runat="server" Text=""></asp:Label></div>
                            <p class="info">
                                <asp:Literal ID="liPEZeventCouponDownload" runat="server"></asp:Literal>
                                您隨時可至「<a href="../User/coupon_List.aspx">會員專區</a>」內的「<a href="../User/coupon_List.aspx">訂單／憑證</a>」查詢訂單明細、付款金額、收據等詳細資訊，<br />
                                或者您也可以下載 <a href="https://www.17life.com/ppon/promo.aspx?cn=app">17Life app</a> ，方便您檢視活動兌換序號。
                            </p>
                        </asp:Panel>
                        <p class="btn-center">
                            <asp:HyperLink ID="hyp_ZeroPromo" runat="server" Target="_blank">
                                <asp:Image ID="img_ZeroPromo" runat="server" /></asp:HyperLink>
                        </p>
                        <div class="info_bar rd-payment-hide">
                            <div class="share">
                                <div class="share_text rd-share-text">
                                    與朋友分享購買好康的喜悅，還有機會獲得 <span class="bigtext">50元</span> 紅利金！
                                </div>
                                <div class="share_btn_box">
                                    <asp:HyperLink ID="hfZ" runat="server" onclick="bi();" class="fb_btn"></asp:HyperLink>
                                    <asp:HyperLink ID="hpZ" runat="server" onclick="bi();" class="plurk_btn"></asp:HyperLink>
                                    <asp:LinkButton ID="hhZ" runat="server" OnClick="GetShareLink" class="link_btn"></asp:LinkButton>
                                </div>
                            </div>
                            <div class="clear">
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <asp:Panel ID="pan_CreditcardFail" runat="server" Visible="false">
                    <div class="content-group payment">
                        <div class="pay_failed">
                        </div>
                        <h1 class="faild">
                            很抱歉，交易失敗！</h1>
                        <hr class="header_hr rd-payment-width" />
                        <p class="info">
                            本次交易失敗可能是因為 <span class="important">刷卡授權失敗</span> 所造成。<br />
                            若您重新確認刷卡資料後仍無法成功授權交易，請聯絡您的發卡銀行！
                        </p>
                        <p class="info">
                            若有其他問題，也歡迎來信<a href="<%=SystemConfig.SiteUrl + SystemConfig.SiteServiceUrl %>" target="_blank">17Life客服中心</a>。
                        </p>
                        <p class="info btn-center">
                            <asp:HyperLink ID="hyp_credit" runat="server">
                                <input id="Button1" type="button" class="btn btn-large btn-primary rd-payment-xlarge-btn" value="重新購買" />
                            </asp:HyperLink>
                        </p>
                    </div>
                </asp:Panel>
                <asp:Panel ID="pan_Success" runat="server" Visible="false">
                    <div class="content-group payment">
                        <asp:Panel ID="pan_Success_CreditCard" runat="server">
                            <div class="pay_success">
                            </div>
                            <h1 class="success">
                                恭喜您，捐款成功！</h1>
                            <hr class="header_hr rd-payment-width" />
                            <p class="info">
                                <asp:Label ID="ItemName" runat="server" Visible="false"></asp:Label>感謝您的愛心！<br />
                                <br />
                                您隨時可至「<a href="<%= ResolveUrl("~/User/coupon_List.aspx") %>">會員專區</a>」內的「<a href="<%= ResolveUrl("~/User/coupon_List.aspx") %>">訂單／憑證</a>」查詢捐款明細、付款金額、收據等詳細資訊，<br />
                                系統將寄發確認信到您的信箱
                                <%= this.UserName %>。
                            </p>
                        </asp:Panel>
                        <asp:Panel ID="pan_Success_ATM" runat="server">
                            <div class="pay_success">
                            </div>
                            <h1 class="success">
                                訂單產生成功！</h1>
                            <hr class="header_hr rd-payment-width" />
                            <p class="info">
                                <asp:Label ID="ItemNameATM" runat="server"></asp:Label>
                                您的帳單已成功產生，請於<span class="important">下單購買當日的 23:59:59 前</span>完成付款，即可加入本次好康喔！<br />
                                您隨時可至「<a href="<%= ResolveUrl("~/User/coupon_List.aspx") %>">會員專區</a>」內的「<a href="<%= ResolveUrl("~/User/coupon_List.aspx") %>">訂單／憑證</a>」查詢訂單明細、付款金額、收據等詳細資訊，<br />
                                或於好康成立後索取好康憑證。
                            </p>
                            <p class="info important">
                                提醒您，請您於繳費期限內完成付款，以避免造成您的繳費期限已過，<br />
                                而優惠已結束無法重新訂購的情形發生。為了確保您的訂單流程順利，<br />
                                請於匯款時確認付款金額與訂單總金額是否相符，及輸入的帳號是否正確。
                            </p>
                            <h2 class="important rd-payment-h2">
                                訂單匯款資訊</h2>
                            <div class="grui-form">
                                <div class="form-unit">
                                    <label for="" class="unit-label title">
                                        銀行代碼</label>
                                    <div class="data-input">
                                        <p>
                                            中國信託商業銀行(822)</p>
                                    </div>
                                </div>
                                <div class="form-unit">
                                    <label for="" class="unit-label title">
                                        銀行帳號</label>
                                    <div class="data-input">
                                        <p>
                                            <asp:Label ID="ATMOrderAccount" runat="server"></asp:Label>
                                            (共14碼)</p>
                                    </div>
                                </div>
                                <div class="form-unit">
                                    <label for="" class="unit-label title">
                                        繳費金額</label>
                                    <div class="data-input">
                                        <p>
                                            <asp:Label ID="ATMOrderTotal" runat="server"></asp:Label>
                                            元</p>
                                    </div>
                                </div>
                                <div class="form-unit">
                                    <label for="" class="unit-label title">
                                        付款期限</label>
                                    <div class="data-input">
                                        <p>
                                            <asp:Label ID="ATMOrderDate" runat="server"></asp:Label>
                                            23:59:59 (請於下單購買當日的 23:59:59 前完成付款 )</p>
                                    </div>
                                </div>
                                <div class="form-unit">
                                    <label for="" class="unit-label title">
                                        &nbsp;</label>
                                    <div class="data-input">
                                        <p>
                                            若您超過這個時間匯款，ATM會顯示交易失敗，您的訂單將被取消。</p>
                                    </div>
                                </div>
                            </div>
                        </asp:Panel>
                        <%--行銷滿額贈活動--%>
                        <asp:Panel ID="divDiscountPromotion" runat="server" CssClass="fd_success_sn pay-evt-bg" Visible="false">
                            <p class="pay-evt">
                                已獲得<span class="s-imp"><asp:Literal ID="liDiscountPromotionCount" runat="server"></asp:Literal></span>次<br>
                                抽刮刮樂機會，總獎金超過億元<br>
                                還差<span class="s-imp"><asp:Literal ID="liDiscountPromotion" runat="server"></asp:Literal></span>元即可再多一次
                                <a href="<%= ResolveUrl("~" + conf.SpecialConsumingDiscountUrl) %>" target="_blank">
                                    活動詳情</a>
                            </p>
                        </asp:Panel>
                        <%--查詢訂單--%>
                        <p class="info btn-center">
                            <a href="<%= ResolveUrl("~/User/coupon_list.aspx") %>">
                                <input type="button" class="btn btn-large btn-primary rd-payment-xlarge-btn" value="立即查詢訂單" />
                            </a>
                        </p>
                    </div>

                </asp:Panel>
            </div>
        </div>
        <%--今日好康推薦--%>
        <div id="Rightarea" class="rd-payment-qma">
            <div class="side_block">
                <div class="side_block_content">
                    <div class="side_block_title for-payment">
                        今日好康推薦<a href="<%= ResolveUrl("~/ppon/default.aspx") %>" target="_blank">看更多</a></div>
                    <div class="sidedealbox">
                        <asp:HyperLink ID="rt1" runat="server"></asp:HyperLink>
                    </div>
                    <div class="siededeal_priceIMG">
                        <div id="sidedealimg">
                            <div class="Over1K_SS_Badge">
                            </div>
                            <asp:HyperLink ID="rl1" runat="server">
                                <asp:Image ID="rp1" runat="server" Width="220" Height="128" border="0" /></asp:HyperLink></div>
                        <div class="sidedealpricearea">
                            <div class="sidedealDiscount">
                                <asp:Label ID="discount1" runat="server"></asp:Label>折</div>
                            <div class="sidedealprice">
                                $<asp:Label ID="dp1" runat="server"></asp:Label></div>
                            <asp:HyperLink ID="rb1" runat="server"><div class="sidedealbuybtn">
                                </div></asp:HyperLink>
                            <div class="clearfix">
                                <asp:Label ID="op1" runat="server" Visible="false"></asp:Label>
                            </div>
                        </div>
                    </div>
                    <div id="sidedeall_hr">
                    </div>
                    <div class="sidedealbox">
                        <asp:HyperLink ID="rt2" runat="server"></asp:HyperLink>
                    </div>
                    <div class="siededeal_priceIMG">
                        <div id="sidedealimg">
                            <div class="HKL_SS_SoldOut_Badge">
                            </div>
                            <asp:HyperLink ID="rl2" runat="server">
                                <asp:Image ID="rp2" runat="server" Width="220" Height="128" border="0" /></asp:HyperLink></div>
                        <div class="sidedealpricearea">
                            <div class="sidedealDiscount">
                                <asp:Label ID="discount2" runat="server"></asp:Label>折</div>
                            <div class="sidedealprice">
                                $<asp:Label ID="dp2" runat="server"></asp:Label></div>
                            <asp:HyperLink ID="rb2" runat="server"><div class="sidedealbuybtn">
                                </div></asp:HyperLink>
                            <div class="clearfix">
                                <asp:Label ID="op2" runat="server" Visible="false"></asp:Label>
                            </div>
                        </div>
                    </div>
                    <div style="display: none;">
                        <div id="sidedeall_hr">
                        </div>
                        <div class="sidedealbox">
                            <asp:HyperLink ID="rt3" runat="server"></asp:HyperLink>
                        </div>
                        <div class="siededeal_priceIMG">
                            <div id="sidedealimg">
                                <div class="HKL_SS_SoldOut_Badge">
                                </div>
                                <asp:HyperLink ID="rl3" runat="server">
                                    <asp:Image ID="rp3" runat="server" Width="220" Height="128" border="0" /></asp:HyperLink></div>
                            <div class="sidedealpricearea">
                                <div class="sidedealDiscount">
                                    <asp:Label ID="discount3" runat="server"></asp:Label>折</div>
                                <div class="sidedealprice">
                                    $<asp:Label ID="dp3" runat="server"></asp:Label></div>
                                <asp:HyperLink ID="rb3" runat="server"><div class="sidedealbuybtn">
                                </div></asp:HyperLink>
                                <div class="clearfix">
                                    <asp:Label ID="op3" runat="server" Visible="false"></asp:Label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <%--sidedeal--%>
        </div>
    </asp:Panel>
    <uc1:Paragraph ID="sp" runat="server" Visible="false" />
    <%-- Google Code for &#36092;&#36023; Conversion Page --%>
<%--    <script type="text/javascript">
//<![CDATA[
        var google_conversion_id = 1015907304;
        var google_conversion_language = "en";
        var google_conversion_format = "2";
        var google_conversion_color = "ffffff";
        var google_conversion_label = "nm4CCID9pgIQ6Ie25AM";
        var google_conversion_value = 0;
//]]>
    </script>
    <script type="text/javascript" src="https://www.googleadservices.com/pagead/conversion.js">
    </script>
    <noscript>
        <div style="display: inline;">
            <img height="1" width="1" style="border-style: none;" alt="" src="https://www.googleadservices.com/pagead/conversion/1015907304/?label=nm4CCID9pgIQ6Ie25AM&amp;guid=ON&amp;script=0" />
        </div>
    </noscript>--%>
  
</asp:Content>

﻿using LunchKingSite.BizLogic.Facade.Models;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LunchKingSite.Web.Ppon
{
    public partial class DealPreview : PponPayBase, IPponDealPreviewView
    {
        #region Field

        public decimal subtotal = 0;
        public decimal total = 0;
        public decimal deliveryCharge = 0;
        public string dealName = string.Empty;
        public string shortDealName = string.Empty;

        #endregion Field

        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(DealPreview));
        protected static ISysConfProvider conf = ProviderFactory.Instance().GetConfig();

        #region properties

        private PponDealPreviewPresenter _presenter;

        public PponDealPreviewPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        public Guid BusinessHourGuid
        {
            get
            {
                Guid ret = Guid.Empty;
                if (Request["bid"] != null)
                {
                    try
                    {
                        ret = new Guid(Request["bid"]);
                    }
                    catch
                    {
                        ret = Guid.Empty;
                    }
                }
                return ret;
            }
        }

        public bool IsShoppingCart { get; set; }

        public string SessionId
        {
            get
            {
                return Session.SessionID;
            }
        }

        public int DeliveryCharge
        {
            get;
            set;
        }

        public CouponFreightCollection TheCouponFreight
        {
            get;
            set;
        }

        public bool IsItem
        {
            get
            {
                if (ViewState["IsItem"] != null)
                {
                    return (bool)ViewState["IsItem"];
                }

                return false;
            }
            set
            {
                ViewState["IsItem"] = value;
            }
        }

        public bool MultipleBranch
        {
            get;
            set;
        }

        #endregion properties

        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            if (!Page.IsPostBack)
            {
                if (!_presenter.OnViewInitialized())
                {
                    Response.Redirect("~/error-sgo.aspx");
                    return;
                }
            }
            _presenter.OnViewLoaded();
        }

        #region InterfaceMember

        public void RedirectToAppLimitedEdtionPage()
        {
            Response.Redirect("~/ppon/AppLimitedEdition/" + BusinessHourGuid);
        }

        public void GoToPponDefaultPage(Guid bid, string message)
        {
            if (string.IsNullOrEmpty(message))
            {
                Response.Redirect(ResolveUrl("~/ppon/default.aspx?bid=" + bid.ToString()));
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('" + message + "');window.location.href='" + ResolveClientUrl("~/ppon/default.aspx?bid=" + bid.ToString()) + "';", true);
            }
        }

        public ListItemCollection GetItemsByInt(int total)
        {
            ListItemCollection lc = new ListItemCollection();
            for (int i = 1; i <= total; i++)
            {
                lc.Add(new ListItem(i.ToString(), i.ToString()));
            }

            if (total < 1)
            {
                lc.Add(new ListItem("0", "0"));
            }

            return lc;
        }

        public void SetContent(IViewPponDeal deal, ViewMemberBuildingCity deliveryInfo, AccessoryGroupCollection multOption,
            int alreadyboughtcount, ViewPponStoreCollection stores, bool isUseDiscountCode, EntrustSellType entrustSell, bool isDailyRestriction)
        {
            int qty = deal.MaxItemCount ?? 10;

            if (deal.ItemDefaultDailyAmount != null)
            {
                if (alreadyboughtcount < deal.ItemDefaultDailyAmount.Value)
                {
                    int stillcanbuycount = deal.ItemDefaultDailyAmount.Value - alreadyboughtcount;
                    qty = qty >= stillcanbuycount ? stillcanbuycount : qty;
                }
                else
                {
                    string message = string.Format(isDailyRestriction ? I18N.Message.DailyRestrictionLimit : I18N.Message.Alreadyboughtcount, alreadyboughtcount);
                    AlertMessageAndGoToDefault(message,deal.BusinessHourGuid);
                }
            }

            bool isGroupCoupon = Helper.IsFlagSet(deal.BusinessHourStatus, BusinessHourStatus.GroupCoupon);
            qty = isGroupCoupon ? deal.SaleMultipleBase - deal.PresentQuantity ?? 0 : qty;

            hidQtyLimit.Value = qty.ToString();
            itemQty.DataSource = isGroupCoupon ? new ListItemCollection { new ListItem(qty.ToString() + ((deal.PresentQuantity ?? 0) > 0 ? "+" + deal.PresentQuantity : string.Empty), qty.ToString()) } : GetItemsByInt(qty);
            itemQty.DataBind();

            // 好康到店到府消費方式判斷；PponItem=ToHouse
            if (deal.DeliveryType.HasValue && int.Equals((int)DeliveryType.ToHouse, deal.DeliveryType.Value))
            {
                IsItem = true;
                pStore.Visible = false;

                // 隱藏分店選單
                ddlStore.Visible = false;
                lblSingleStore.Visible = false;
            }
            else
            {
                //通用券檔次購買時無須選擇分店
                pStore.Visible = !Helper.IsFlagSet(deal.BusinessHourStatus, BusinessHourStatus.NoRestrictedStore);

                // 分店選單
                if (pStore.Visible && stores.Count > 0)
                {
                    if (MultipleBranch)
                    {
                        processBranches(stores);
                    }
                    else
                    {
                        multiStores.Visible = false;
                        setStore(stores);
                    }
                }
                else
                {
                    ddlStore.Visible = false;
                    lblSingleStore.Visible = false;
                }
            }

            dealName = deal.EventName;
            if (Request.QueryString["preview"] != null)
            {
                if (Request.QueryString["preview"] == "setup")
                {
                    string newEventName = LunchKingSite.BizLogic.Facade.PponFacade.PponNewContentGet(deal.BusinessHourGuid);
                    dealName = newEventName;
                }
            }
            shortDealName = Server.HtmlEncode(deal.ItemName);
            hidtotal.Value = lprice.Text = hidprice.Value = hidsubtotal.Value = deal.ItemPrice.ToString("F0");
            subtotal = deal.ItemPrice;

            hidComboPack.Value = deal.ComboPackCount.HasValue ? deal.ComboPackCount.Value.ToString() : "1";
            hidqty.Value = "0";

            #region 運費

            // 階梯式
            if (null != TheCouponFreight && TheCouponFreight.Count > 0)
            {
                // 規則：
                // 1.運費=0 → 還差□份就免運費囉！
                // 2.下一運費<目前運費 → 還差□份，運費將降為□元囉！
                // 3.其他 → 無顯示
                CouponFreight lowestCouponFreight = TheCouponFreight.OrderBy(x => x.FreightAmount).First();
                CouponFreight highestCouponFreight = TheCouponFreight.OrderBy(x => x.FreightAmount).Last();
                deliveryCharge = highestCouponFreight.FreightAmount;

                bool hasZero = false;
                decimal zeroPriceAmount = 0;
                if (lowestCouponFreight.FreightAmount.Equals(0))
                {
                    hasZero = true;
                    zeroPriceAmount = lowestCouponFreight.StartAmount;
                }

                if (hasZero)
                {
                    lbldc.InnerHtml = string.Format("<span class='hint'>-還差{0}份就免運費囉！</span>", Math.Ceiling((zeroPriceAmount / deal.ItemPrice)).ToString("F0"));
                }
                else
                {
                    if (TheCouponFreight.Count == 1)
                    {
                        //如果只有設定一筆運費，且免運的購買金額設為0 ，即表示總是要收運費，那提示還差多少免運的訊息就隱藏
                        lbldc.Visible = false;
                    }
                    else
                    {
                        lbldc.InnerHtml = string.Format("<span class='hint'>-還差{0}份，運費將降為{1}元囉！</span>", Math.Ceiling((lowestCouponFreight.StartAmount / deal.ItemPrice)).ToString("F0"), lowestCouponFreight.FreightAmount.ToString("F0"));
                    }
                }

                JsonSerializer serializer = new JsonSerializer();
                hidFreight.Value = serializer.Serialize(from x in TheCouponFreight select new { startAmt = x.StartAmount, endAmt = x.EndAmount, freightAmt = x.FreightAmount });
            }
            else
            {
                hidFreight.Value = string.Empty;
                deliveryCharge = deal.BusinessHourDeliveryCharge;
            }
            hiddCharge.Value = hidcstatic.Value = deliveryCharge.ToString("F0");

            #endregion 運費

            total = deliveryCharge;

            itemLimits = new List<OptionItemLimit>();
            if (multOption.Count == 0)
            {
                rpOption.HeaderTemplate = rpOption.FooterTemplate = null;
            }
            // 選項數量檢查
            hidCurrentLimits.Value = new JsonSerializer().Serialize(itemLimits);

            rpOption.DataSource = multOption;
            rpOption.DataBind();
        }

        private void setStore(ViewPponStoreCollection stores)
        {
            var count = 0;
            pStore.Visible = true;

            //若有一家以上分店；以下拉式選單方式顯示供使用者點選
            //但分店若只有一家；畫面上直接顯示該店家資料，不須以下拉式選單方式呈現
            if (stores.Count > 1)
            {
                #region 多家分店

                ddlStore.Visible = true;
                ddlStore.Items.Clear();
                ddlStore.Items.Add("請選擇");
                divSingleStore.Visible = false;
                lblSingleStore.Visible = false;
                lblStoreChioce.Visible = true;
                foreach (var store in stores.OrderBy(x => x.SortOrder))
                {
                    ListItem li = new ListItem(GetStoreOptionTitle(store), store.StoreGuid.ToString());
                    if (store.TotalQuantity <= 0 || store.OrderedQuantity >= store.TotalQuantity)
                    {
                        li.Attributes["disabled"] = "disabled";
                        li.Text += "(已售完)";
                    }
                    else
                    {
                        li.Attributes.Remove("disabled");
                    }

                    ddlStore.Items.Add(li);
                    count++;
                }

                #endregion 多家分店
            }
            else if (stores.Count == 1)
            {
                #region 一家分店

                ddlStore.Visible = false;
                divSingleStore.Visible = true;
                lblSingleStore.Visible = true;
                lblStoreChioce.Visible = false;
                lblSingleStore.Text = GetStoreOptionTitle(stores[0]);
                HiddenSingleStoreGuid.Text = stores[0].StoreGuid.ToString();
                count++;

                #endregion 一家分店
            }

            if (count == 0)
            {
                #region 無分店，宅配檔

                ddlStore.Visible = false;
                lblSingleStore.Visible = false;
                lblStoreChioce.Visible = false;

                #endregion 無分店，宅配檔
            }
        }

        private void AlertMessageAndGoToDefault(string msg, Guid bid)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('" + msg + "');window.location.href='" + ResolveClientUrl("~/ppon/default.aspx?bid=" + bid.ToString()) + "';", true);
        }

        private IList<OptionItemLimit> itemLimits
        {
            get;
            set;
        }

        #endregion InterfaceMember

        #region PageEvent

        protected void rpOption_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is AccessoryGroup)
            {
                List<ViewItemAccessoryGroup> viagc = ((AccessoryGroup)e.Item.DataItem).members;
                DropDownList ddl = (DropDownList)e.Item.FindControl("ddlMultOption");

                if (viagc.Count > 0)
                {
                    ddl.Items.Add(new ListItem(viagc[0].AccessoryGroupName, "Default"));
                    for (int i = 0; i < viagc.Count; i++)
                    {
                        ViewItemAccessoryGroup v = viagc[i];
                        ListItem li = new ListItem(v.AccessoryName, v.AccessoryGroupMemberGuid.ToString());
                        OptionItemLimit oil = new OptionItemLimit() { AccessoryName = v.AccessoryName, AccessoryId = v.AccessoryGroupMemberGuid.ToString() };
                        if (v.Quantity == null || v.Quantity > 0)
                        {
                            li.Attributes.Remove("disabled");

                            if (v.Quantity > 0)
                            {
                                oil.Limit = v.Quantity.Value;
                            }
                        }
                        else
                        {
                            oil.Limit = 0;
                            li.Attributes["disabled"] = "disabled";
                            li.Text += "(已售完)";
                        }

                        ddl.Items.Add(li);

                        if (v.Quantity != null)
                        {
                            itemLimits.Add(oil);
                        }
                    }
                }
                else
                {
                    ddl.Visible = false;
                }
            }
        }

        #endregion PageEvent

        private void emptyBuilding(DropDownList ddl)
        {
            ddl.DataSource = new BuildingCollection();
            ddl.DataBind();
            ddl.Items.Insert(0, new ListItem("請選擇", "0"));
        }

        protected void ddlStoreCity_SelectedIndexChanged(object sender, EventArgs e)
        {
            int cityId = int.Parse(ddlStoreCity.SelectedValue);
            Hashtable hts = new Hashtable();
            IList<string> stores = hidMultiBranch.Value.Split('|');
            IList<string> sTemp;
            foreach (string s in stores)
            {
                if (s.IndexOf(cityId + ",") >= 0)
                {
                    sTemp = s.Split(",");
                    if (!hts.Contains(sTemp[2]))
                    {
                        hts.Add(sTemp[2], sTemp[3]);
                    }
                }
            }

            ddlStoreArea.DataSource = hts;
            ddlStoreArea.DataTextField = "Value";
            ddlStoreArea.DataValueField = "Key";
            ddlStoreArea.DataBind();
            ddlStoreArea.Items.Insert(0, new ListItem("請選擇", "0"));
            ddlStoreArea.SelectedIndex = 0;
            emptyBuilding(ddlStore);
        }

        protected void ddlStoreArea_SelectedIndexChanged(object sender, EventArgs e)
        {
            int areaId = int.Parse(ddlStoreArea.SelectedValue);

            if (int.Equals(0, areaId))
            {
                emptyBuilding(ddlStore);
            }
            else
            {
                Hashtable hts = new Hashtable();
                IList<string> stores = hidMultiBranch.Value.Split('|');
                IList<string> sTemp;
                foreach (string s in stores)
                {
                    if (s.IndexOf(areaId + ",") > 0)
                    {
                        sTemp = s.Split(",");
                        if (!hts.Contains(sTemp[4]))
                        {
                            hts.Add(sTemp[4], sTemp[5]);
                        }
                    }
                }

                ddlStore.DataSource = hts;
                ddlStore.DataTextField = "Value";
                ddlStore.DataValueField = "Key";
                ddlStore.DataBind();
                ddlStore.Items.Insert(0, new ListItem("請選擇", "0"));
                ddlStore.SelectedIndex = 0;
            }
        }

        private void processBranches(ViewPponStoreCollection stores)
        {
            IList<string> branches = new List<string>();
            Hashtable branchCity = new Hashtable();
            lblStoreChioce.Visible = true;
            foreach (ViewPponStore store in stores.OrderBy(x => x.CityId))
            {
                string phone = store.CityId + "," + store.CityName + "," + store.TownshipId + "," + store.TownshipName + "," + store.StoreGuid.ToString() + "," + store.StoreName + (string.IsNullOrEmpty(store.Phone) ? string.Empty : (" 預約專線" + store.Phone));
                if (store.TotalQuantity <= 0 || store.OrderedQuantity >= store.TotalQuantity)
                {
                    phone += "(已售完)";
                }

                branches.Add(phone);

                if (!branchCity.Contains(store.CityId))
                {
                    branchCity.Add(store.CityId, store.CityName);
                }
            }

            ddlStoreCity.DataSource = branchCity;
            ddlStoreCity.DataTextField = "Value";
            ddlStoreCity.DataValueField = "Key";
            ddlStoreCity.DataBind();
            ddlStoreCity.Items.Insert(0, new ListItem("請選擇", "0"));

            if (int.Equals(1, branchCity.Count))
            {
                ddlStoreCity_SelectedIndexChanged(this, EventArgs.Empty);
            }
            else
            {
                emptyBuilding(ddlStore);
            }

            if (branches.Count > 0)
            {
                hidMultiBranch.Value = string.Join("|", branches);
            }
        }
    }
}
﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DailyNewEdmItems.ascx.cs"
    Inherits="LunchKingSite.Web.Ppon.DailyNewEdmItems" %>
<%@ Import Namespace="LunchKingSite.DataOrm" %>
<%@ Import Namespace="LunchKingSite.Core" %>
<%@ Import Namespace="LunchKingSite.BizLogic.Component" %>

<asp:Panel ID="pan_Title" runat="server" Visible="false">
    <table>
        <tr>
            <td width="740" height="40" style="color: #<%=TitleColor%>; font-size: 22px;">
                <table width="740" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="<%=(!string.IsNullOrEmpty(CityName)&&CityName.Length<=2)?"53":((CityName.Length==3)?"75":"130") %>">
                            <asp:Label ID="lab_AreaName" runat="server"></asp:Label></td>
                        <td valign="middle">
                            <hr style="border: none; border-top: 1px solid #<%=TitleColor%>;" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td height="5"></td>
        </tr>
    </table>
</asp:Panel>
<asp:Panel ID="pan_Single" runat="server" Visible="false">
    <asp:Repeater ID="rpt_Single" runat="server" OnItemDataBound="rptDataBound">
        <ItemTemplate>
            <table width="740" border="0" cellspacing="0" cellpadding="0" style="background: #FFF;
                border: 1px solid #999999;">
                <tr>
                    <td rowspan="8" width="440" height="245">
                        <a href="<%# SiteUrl+"/deal/"+((ViewPponDeal)Container.DataItem).BusinessHourGuid+Cpa %>"
                            target="_blank">
                            <asp:Image ID="img_Pic" runat="server" Width="440" Height="245" BorderWidth="0" AlternateText='<%# Helper.GetMaxString( ((ViewPponDeal)Container.DataItem).ItemName,28,"...")%>' />
                        </a>
                    </td>
                    <td width="20" rowspan="8"></td>
                    <td width="260">
                        <table width="260" height="29" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td height="5"></td>
                            </tr>
                            <tr>
                                <asp:Literal ID="lit_IconTags" runat="server"></asp:Literal>
                            </tr>
                        </table>
                    </td>
                    <td width="20" rowspan="8"></td>
                </tr>
                <tr>
                    <td height="10"></td>
                </tr>
                <tr>
                    <td valign="top" height="60">
                        <div style="font-size: 15px; height: 55px; word-break: break-all; overflow: hidden;
                            margin: 0px; line-height: 18px;">
                            <span style=" float: left;border-radius: 20px;background: #999;color: white;margin: 0 5px 0 0;padding: 0px 7px;">
                                <%# ViewPponDealManager.DefaultManager.GetCityNameOrTravelPlace(PponCityGroup.DefaultPponCityGroup.GetPponCityByCityId(CityId), ((ViewPponDeal)Container.DataItem).BusinessHourGuid) %>
                            </span>
                            <a href="<%# SiteUrl+"/deal/"+((ViewPponDeal)Container.DataItem).BusinessHourGuid+Cpa %>"
                                target="_blank" style="color: #5b5b5b; text-decoration: none;">
                                <%# Helper.GetMaxString(((ViewPponDeal)Container.DataItem).EventName,53,"..")%>
                            </a>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td></td>
                </tr>
                <tr>
                    <td height="10"></td>
                </tr>
                <tr>
                    <td height="15" style="color: #999; text-align: left; line-height: 15px;">原價：<span
                        style="text-decoration: line-through;">$<%# ((ViewPponDeal)Container.DataItem).ItemOrigPrice.ToString("F0") %></span>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width="260" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td height="30" style="font-size: 32px; line-height: 30px; color: #<%=TitleColor%>;">
                                    $<%# ((ViewPponDeal)Container.DataItem).ItemPrice.ToString("F0") %></td>
                                <td align="center" style="border: 1px solid #<%=TitleColor%>; color: #<%=TitleColor%>;
                                    font-size: 14px; font-weight: bold;">
                                    <asp:Label ID="lab_Discount" runat="server"></asp:Label></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td height="65" align="center">
                        <table width="90" cellpadding="0" cellspacing="0">
                            <tr>
                                <td height="37" align="center" style="border-radius: 5px; background: #<%=TitleColor%>;">
                                    <a href="<%# SiteUrl+"/deal/"+((ViewPponDeal)Container.DataItem).BusinessHourGuid+Cpa %>"
                                        target="_blank" style="color: #FFF; font-size: 19px; text-decoration: none;">馬上看</a>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <br>
        </ItemTemplate>
    </asp:Repeater>
</asp:Panel>
<asp:Panel ID="pan_Double" runat="server" Visible="false">
    <asp:Repeater ID="rpt_Double" runat="server" OnItemDataBound="rptDataBound">
        <HeaderTemplate>
            <table border="0" cellspacing="0" cellpadding="0" align="center" style="width: 740px">
                <tr>
        </HeaderTemplate>
        <ItemTemplate>
            <td width="363" height="340" align="center" valign="top" style="background: #FFF;
                border: 1px solid #999999;">
                <table width="363" border="0" cellspacing="0" cellpadding="0" align="center">
                    <tr>
                        <td width="363" height="202" colspan="5" align="center">
                            <a href="<%# SiteUrl+"/deal/"+((ViewPponDeal)Container.DataItem).BusinessHourGuid+Cpa %>"
                                target="_blank">
                                <asp:Image ID="img_Pic" runat="server" Width="345" Height="192" BorderWidth="0" AlternateText='<%# Helper.GetMaxString( ((ViewPponDeal)Container.DataItem).ItemName,24,"...")%>' />
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td width="10" height="36"></td>
                        <td height="36" colspan="3">
                            <table width="343" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td height="3" colspan="5"></td>
                                </tr>
                                <tr>
                                    <asp:Literal ID="lit_IconTags" runat="server"></asp:Literal>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td height="4" colspan="5"></td>
                                </tr>
                            </table>
                        </td>
                        <td width="10" height="36"></td>
                    </tr>
                    <tr></tr>
                    <tr>
                        <td width="10"></td>
                        <td width="345" colspan="3" style="border-bottom: 1px dashed #CCC;">
                            <div style="width: 345px; height: 38px; line-height: 17px; word-break: break-all;
                                overflow: hidden; margin-bottom: 5px;">
                                <span style=" float: left;border-radius: 20px;background: #999;color: white;margin: 0 5px 0 0;padding: 0px 7px;">
                                    <%# ViewPponDealManager.DefaultManager.GetCityNameOrTravelPlace(PponCityGroup.DefaultPponCityGroup.GetPponCityByCityId(CityId), ((ViewPponDeal)Container.DataItem).BusinessHourGuid) %>
                                </span>
                                <a href="<%# SiteUrl+"/deal/"+((ViewPponDeal)Container.DataItem).BusinessHourGuid+Cpa %>"
                                    target="_blank" style="color: #000; line-height: 17px; text-decoration: none;
                                    color: #5b5b5b;">
                                    <%# Helper.GetMaxString(((ViewPponDeal)Container.DataItem).EventName,50,"..")%>
                                </a>
                            </div>
                        </td>
                        <td width="10"></td>
                    </tr>
                    <tr>
                        <td height="8" colspan="5"></td>
                    </tr>
                    <tr>
                        <td width="10"></td>
                        <td width="70" height="30" align="center" style="border: 1px solid #<%=TitleColor%>;
                            color: #<%=TitleColor%>; font-size: 14px; font-weight: bold;">
                            <asp:Label ID="lab_Discount" runat="server"></asp:Label></td>
                        <td width="200" style="font-size: 24px; color: #<%=TitleColor%>;">&nbsp;$<%# ((ViewPponDeal)Container.DataItem).ItemPrice.ToString("F0") %>&nbsp;<span
                            style="text-decoration: line-through; color: #999; font-size: 13px;">$<%# ((ViewPponDeal)Container.DataItem).ItemOrigPrice.ToString("F0") %></span>
                        </td>
                        <td width="70" align="center" style="border-radius: 5px; background: #<%=TitleColor%>;">
                            <a href="<%# SiteUrl+"/"+((ViewPponDeal)Container.DataItem).BusinessHourGuid+Cpa %>"
                                target="_blank" style="color: #FFF; font-size: 15px; text-decoration: none;">馬上看</a>
                        </td>
                        <td width="10"></td>
                    </tr>
                </table>
            </td>
            <%# (Container.ItemIndex+1)%2==0?string.Empty:"<td width='10'></td>" %>
        </ItemTemplate>
        <FooterTemplate>
            </tr>
        <tr>
            <td colspan="5" height="20"></td>
        </tr>
            </table>
        </FooterTemplate>
    </asp:Repeater>
</asp:Panel>
<asp:Panel ID="pan_Triple" runat="server" Visible="false">
    <asp:Repeater ID="rpt_Triple" runat="server" OnItemDataBound="rptDataBound">
        <HeaderTemplate>
            <table border="0" cellspacing="0" cellpadding="0" align="center" style="width: 740px">
                <tr>
        </HeaderTemplate>
        <ItemTemplate>
            <td width="240" height="250" valign="top" style="background: #FFF; border: 1px solid #999999;">
                <table width="240" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="240" height="132" colspan="4">
                            <a href="<%# SiteUrl+"/deal/"+((ViewPponDeal)Container.DataItem).BusinessHourGuid+Cpa %>"
                                target="_blank">
                                <asp:Image ID="img_Pic" runat="server" Width="240" Height="132" BorderWidth="0" AlternateText='<%# Helper.GetMaxString( ((ViewPponDeal)Container.DataItem).ItemName,14,"...")%>' />
                            </a></td>
                    </tr>
                    <tr>
                        <td width="240" height="5" colspan="4"></td>
                    </tr>
                    <tr>
                        <td width="10">&nbsp;</td>
                        <td width="220" colspan="2" height="29">
                            <table border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <asp:Literal ID="lit_IconTags" runat="server"></asp:Literal>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td height="5" colspan="4"></td>
                                </tr>
                            </table>
                        </td>
                        <td width="10">&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td width="220" colspan="2" style="border-bottom: 1px dashed #CCC;">
                            <div style="width: 220px; height: 38px; line-height: 17px; word-break: break-all;
                                overflow: hidden;">    
                                <span style=" float: left;border-radius: 20px;background: #999;color: white;margin: 0 5px 0 0;padding: 0px 7px;">
                                    <%# ViewPponDealManager.DefaultManager.GetCityNameOrTravelPlace(PponCityGroup.DefaultPponCityGroup.GetPponCityByCityId(CityId), ((ViewPponDeal)Container.DataItem).BusinessHourGuid) %>
                                </span>
                                <a href="<%# SiteUrl+"/deal/"+((ViewPponDeal)Container.DataItem).BusinessHourGuid+Cpa %>"
                                    target="_blank" style="color: #000; text-decoration: none; color: #5b5b5b;">
                                    <%# Helper.GetMaxString(((ViewPponDeal)Container.DataItem).EventName,31,"..")%>
                                </a>
                            </div>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td height="5" colspan="4"></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td width="65" height="22" align="center" style="border: 1px solid #<%=TitleColor%>;
                            color: #<%=TitleColor%>; font-size: 14px; font-weight: bold;">
                            <asp:Label ID="lab_Discount" runat="server"></asp:Label></td>
                        <td width="155" style="font-size: 24px; text-align: right; color: #<%=TitleColor%>;">
                            &nbsp;$<%# ((ViewPponDeal)Container.DataItem).ItemPrice.ToString("F0") %>&nbsp;<span
                                style="text-decoration: line-through; color: #999; font-size: 13px;">$<%# ((ViewPponDeal)Container.DataItem).ItemOrigPrice.ToString("F0") %></span>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                </table>
            </td>
            <%# (Container.ItemIndex+1)%3==0?string.Empty:"<td width='10'></td>" %>
        </ItemTemplate>
        <FooterTemplate>
            </tr>
        <tr>
            <td colspan="5" height="20"></td>
        </tr>
            </table>
        </FooterTemplate>
    </asp:Repeater>
</asp:Panel>

<asp:Panel ID="pan_SingleTravel" runat="server" Visible="false">
    <asp:Repeater ID="rpt_SingleTravel" runat="server" OnItemDataBound="rptDataBound">
        <ItemTemplate>
            <table width="740" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="740" height="230" colspan="3" style="background: #FFF; border: 1px solid #999999;">
                        <a href="<%# SiteUrl+"/deal/"+((ViewPponDeal)Container.DataItem).BusinessHourGuid+Cpa %>"
                            target="_blank">
                            <asp:Image ID="img_Pic" runat="server" BorderWidth="0" AlternateText='<%# Helper.GetMaxString( ((ViewPponDeal)Container.DataItem).ItemName,28,"...")%>' />
                        </a>
                    </td>
                </tr>
                <tr>
                    <td width="540" height="3"></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td width="540">
                        <table width="540" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td colspan="5"></td>
                            </tr>
                            <tr>
                                <asp:Literal ID="lit_IconTags" runat="server"></asp:Literal>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td height="3" colspan="5"></td>
                            </tr>
                        </table>
                    </td>
                    <td width="10">&nbsp;</td>
                    <td width="180">&nbsp;</td>
                </tr>
                <tr>
                    <td valign="top">
                        <table width="540" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td height="10"></td>
                            </tr>
                            <tr>
                                <td width="540">
                                    <div style="font-size: 19px; height: 57px; word-break: break-all; overflow: hidden;">
                                        <a href="<%# SiteUrl+"/deal/"+((ViewPponDeal)Container.DataItem).BusinessHourGuid+Cpa %>"
                                            target="_blank" style="color: #5b5b5b; text-decoration: none;">
                                            <%# Helper.GetMaxString(((ViewPponDeal)Container.DataItem).EventName,53,"..")%>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        </table>

                    </td>
                    <td>&nbsp;</td>
                    <td>
                        <table width="180" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="70" rowspan="3" style="border: 1px solid #<%=TitleColor%>; color: #<%=TitleColor%>;
                                    font-size: 14px; font-weight: bold; text-align: center;">
                                    <asp:Label ID="lab_Discount" runat="server"></asp:Label></td>
                                <td width="20"></td>
                                <td width="90" height="30" align="center" style="border-radius: 5px; background: #<%=TitleColor%>;">
                                    <a href="<%# SiteUrl+"/deal/"+((ViewPponDeal)Container.DataItem).BusinessHourGuid+Cpa %>"
                                        target="_blank" style="color: #FFF; font-size: 17px; text-decoration: none;">馬上看</a>
                            </tr>
                            <tr>
                                <td height="5"></td>
                                <td></td>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td style="font-size: 24px; text-align: center; color: #<%=TitleColor%>;">$<%# ((ViewPponDeal)Container.DataItem).ItemPrice.ToString("F0") %>
                                    </td>
                                </tr>

                                <td></td>
                                <td></td>
                                <td style="text-decoration: line-through; color: #999; font-size: 13px; text-align: right;">
                                    $<%# ((ViewPponDeal)Container.DataItem).ItemOrigPrice.ToString("F0") %>&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <br />
            <hr style="border: none; border-top: 1px solid #DDD;" />
        </ItemTemplate>
    </asp:Repeater>
</asp:Panel>
<asp:Panel ID="pan_PponPiinLife_1" runat="server" Visible="false">
    <asp:Repeater ID="rpt_PponPiinLife_1" runat="server" OnItemDataBound="rptDataBound_PponPiinlife">
        <ItemTemplate>
            <table width="700" border="0" cellpadding="0" cellspacing="0" align="center" style="background-color: #FFF;">
                <tr>
                    <td width="25"></td>
                    <td width="650" height="240" colspan="2"><a href="<%# SiteUrl+"/piinlife/"+((ViewPponDeal)Container.DataItem).BusinessHourGuid+Cpa %>"
                        target="_blank">
                        <asp:Image ID="img_Pic" runat="server" BorderWidth="0" Width="650" Height="260" AlternateText='<%# Helper.GetMaxString( ((ViewPponDeal)Container.DataItem).ItemName,28,"...")%>' />
                    </a></td>
                    <td width="25"></td>
                </tr>
                <tr>
                    <td width="25"></td>
                    <td width="520" height="80">
                        <div style="width: 500px; height: 30px; word-break: break-all; overflow: hidden;
                            font-size: 16px;"><a href="<%# SiteUrl+"/piinlife/"+((ViewPponDeal)Container.DataItem).BusinessHourGuid+Cpa %>"
                                target="_blank" style="color: #EC7d49; text-decoration: none;"><%# Helper.GetMaxString(((ViewPponDeal)Container.DataItem).EventName,30,"..")%></a>
                        </div>
                        <div style="width: 520px; height: 35px; word-break: break-all; overflow: hidden;"><a
                            href="<%# SiteUrl+"/piinlife/"+((ViewPponDeal)Container.DataItem).BusinessHourGuid+Cpa %>"
                            target="_blank" style="color: #333333; font-size: 13px; text-decoration: none;">
                            <%# Helper.GetMaxString(((ViewPponDeal)Container.DataItem).EventTitle,75,"..")%></a>
                        </div>
                    </td>
                    <td width="80" height="80" align="center" style="background-color: #2B1E13; font-size: 24px;">
                        <a href="<%# SiteUrl+"/piinlife/"+((ViewPponDeal)Container.DataItem).BusinessHourGuid+Cpa %>"
                            target="_blank" style="color: white; text-decoration: none;">VIEW</a></td>
                    <td width="25"></td>
                </tr>
                <tr>
                    <td colspan="4" height="30"></td>
                </tr>
            </table>
        </ItemTemplate>
    </asp:Repeater>
</asp:Panel>
<asp:Panel ID="pan_PiinLife_1" runat="server" Visible="false">
    <asp:Repeater ID="rpt_PiinLife_1" runat="server" OnItemDataBound="rptDataBound_PponPiinlife">
        <HeaderTemplate>
            <table width="740" border="0" cellpadding="0" cellspacing="0" align="center" style="background: #FFF;
                border: 1px solid #DDD;">
                <tr>
                    <td height="30"></td>
                </tr>
                <tr>
                    <td>
        </HeaderTemplate>
        <ItemTemplate>
            <table border="0" cellpadding="0" cellspacing="0" align="center" style="width: 650px">
                <tr>
                    <td colspan="2" style="width: 650px; height: 260px">
                        <a href="<%# SiteUrl+"/piinlife/"+((ViewPponDeal)Container.DataItem).BusinessHourGuid+Cpa %>"
                            target="_blank">
                            <asp:Image ID="img_Pic" runat="server" Width="650" Height="260" BorderWidth="0" AlternateText='<%# Helper.GetMaxString(((ViewPponDeal)Container.DataItem).ItemName,47,"..")%>' />
                        </a>
                    </td>
                </tr>
                <tr>
                    <td style="width: 570px; height: 80px">
                        <div style="width: 550px; height: 30px; word-break: break-all; overflow: hidden;
                            font-size: 16px;">
                            <a href="<%# SiteUrl+"/piinlife/"+((ViewPponDeal)Container.DataItem).BusinessHourGuid+Cpa %>"
                                target="_blank" style="color: #EC7d49; text-decoration: none;">
                                <%# Helper.GetMaxString(((ViewPponDeal)Container.DataItem).EventName,30,"..")%></a>
                        </div>
                        <div style="width: 550px; height: 35px; word-break: break-all; overflow: hidden;">
                            <a href="<%# SiteUrl+"/piinlife/"+((ViewPponDeal)Container.DataItem).BusinessHourGuid+Cpa %>"
                                target="_blank" style="color: black; text-decoration: none;">
                                <%# Helper.GetMaxString(((ViewPponDeal)Container.DataItem).EventTitle,75,"..")%></a>
                        </div>
                    </td>
                    <td align="center" style="background-color: #2B1E13; font-size: 24px; width: 80px;
                        height: 80px">
                        <a href="<%# SiteUrl+"/piinlife/"+((ViewPponDeal)Container.DataItem).BusinessHourGuid+Cpa %>"
                            target="_blank" style="color: white; text-decoration: none;">VIEW</a>
                    </td>
                </tr>
            </table>
        </ItemTemplate>
        <FooterTemplate>
            </td> </tr>
            <tr>
                <td style="padding-top: 15px; padding-bottom: 15px;"></td>
            </tr>
            </table>
            <br />
        </FooterTemplate>
    </asp:Repeater>
</asp:Panel>
<asp:Literal ID="lit_Separate" runat="server"></asp:Literal>

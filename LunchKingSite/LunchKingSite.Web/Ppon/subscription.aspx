﻿<%@ Page Language="C#" MasterPageFile="~/Ppon/Ppon.Master" AutoEventWireup="true" CodeBehind="subscription.aspx.cs" Inherits="LunchKingSite.Web.Ppon.subscription" Title="Untitled Page" %>

<%@ Import Namespace="LunchKingSite.BizLogic.Component" %>
<asp:Content ID="Content2" ContentPlaceHolderID="cphPponMeta" runat="server">
    <title>訂閱17Life</title>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <link href='<%= ResolveUrl("../themes/PCweb/css/RDL-L.css") %>' rel="stylesheet" type="text/css" />
    <link href='<%= ResolveUrl("../themes/PCweb/css/RDL-M.css") %>' rel="stylesheet" type="text/css" />
    <link href='<%= ResolveUrl("../themes/PCweb/css/RDL-S.css")%>' rel="stylesheet" type="text/css" />
    <!--for 訂閱EDM頁-->
    <link href="<%= ResolveUrl("../Themes/PCweb/css/A1-EDM.css")%>" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="<%= ResolveUrl("../Themes/default/images/17Life/edm_2015/javascript/jquery.bpopup.js")%>"></script>
    <script type="text/javascript" src="../Tools/js/completer.js"></script>
    <link href="../Tools/js/css/completer.css" rel="stylesheet" type="text/css" />
    <!--for 訂閱EDM頁-->
        
    <!--訂閱EDM頁 // 同步選擇打勾 //-->
    <script type="text/javascript">
        $(document).ready(function () {

            var edm_num = ["94", "95", "96", "97", "98", "99", "89", "90", "88", "184", "148"]; //edm分類編號
            var flag_click_boolean = ["true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true"]; //true為沒點選
            var click_boolean_num = 0;  // 計算flag_click_boolean的false個數
            /*console.log('n0='+click_boolean_num); */

            /*banner區的下拉式選單*/
            $(".select-list-block ul li").click(function () {
                for (i = 0; i < flag_click_boolean.length; i++) {

                    var lastDash = $(this).attr('class').lastIndexOf("_") + 1;
                    var flag_num = $(this).attr('class').substr(lastDash); //flag_num:選項被選到的編號
                    var edm_click = edm_num[i];

                    /*console.log('選項被選到的編號(文字):flag_num=' + flag_num);*/
                    /*console.log('edm編號(文字):edm_click=' + edm_click);*/

                    if (flag_num == edm_click && flag_click_boolean[i] == "true") {
                        $('.food-pic span.food_pic_' + flag_num).find('i').css('color', '#0ea146');
                        $('.select-list-block ul li.edm_memu_' + flag_num).find('span').addClass('active');
                        flag_click_boolean[i] = "false";
                        /*console.log('flag_click_boolean=' + flag_click_boolean);*/

                        /*顯示點選數量:大於0才顯示;等於0則移除*/
                        click_boolean_num = $.grep(flag_click_boolean, function (str) { return str == 'false' }).length;
                        if (click_boolean_num != 0) {
                            $('.choose_num').text('(' + click_boolean_num + ')');
                        } else if (click_boolean_num == 0) {
                            $('.choose_num').text('');
                        }
                        setSubscriptionList(flag_num, true);
                        return false;
                    }
                    else if (flag_num == edm_click && flag_click_boolean[i] == "false") {
                        $('.food-pic span.food_pic_' + flag_num).find('i').css('color', '#333');
                        $('.select-list-block ul li.edm_memu_' + flag_num).find('span').removeClass('active');
                        flag_click_boolean[i] = "true";
                        /*console.log('flag_chflag_click_booleaneck_boolean=' + flag_click_boolean);*/

                        /*顯示點選數量:大於0才顯示;等於0則移除*/
                        click_boolean_num = $.grep(flag_click_boolean, function (str) { return str == 'false' }).length;
                        if (click_boolean_num != 0) {
                            $('.choose_num').text('(' + click_boolean_num + ')');
                        } else if (click_boolean_num == 0) {
                            $('.choose_num').text('');
                        }
                        setSubscriptionList(flag_num, false);
                        return false;
                    }

                }
            });

            /*美食區＋各式好康*/
            $(".food-select .food-pic span").click(function () {
                for (i = 0; i < flag_click_boolean.length; i++) {
                    var lastDash = $(this).attr('class').lastIndexOf("_") + 1;
                    var flag_num = $(this).attr('class').substr(lastDash); //flag_num:選項被選到的編號
                    var edm_click = edm_num[i];

                    /*console.log('選項被選到的編號(圖片):flag_num=' + flag_num);*/
                    /*console.log('edm編號(圖片):edm_click=' + edm_click);*/

                    if (flag_num == edm_click && flag_click_boolean[i] == "true") {
                        $('.food-pic span.food_pic_' + flag_num).find('i').css('color', '#0ea146');
                        $('.select-list-block ul li.edm_memu_' + flag_num).find('span').addClass('active');
                        flag_click_boolean[i] = "false";
                        /*console.log('flag_click_boolean=' + flag_click_boolean);*/

                        /*顯示點選數量:大於0才顯示;等於0則移除*/
                        click_boolean_num = $.grep(flag_click_boolean, function (str) { return str == 'false' }).length;
                        if (click_boolean_num != 0) {
                            $('.choose_num').text('(' + click_boolean_num + ')');
                        } else if (click_boolean_num == 0) {
                            $('.choose_num').text('');
                        }
                        setSubscriptionList(flag_num, true);
                        return false;
                    }
                    else if (flag_num == edm_click && flag_click_boolean[i] == "false") {
                        $('.food-pic span.food_pic_' + flag_num).find('i').css('color', '#333');
                        $('.select-list-block ul li.edm_memu_' + flag_num).find('span').removeClass('active');
                        flag_click_boolean[i] = "true";
                        /*console.log('flag_click_boolean=' + flag_click_boolean);*/

                        /*顯示點選數量:大於0才顯示;等於0則移除*/
                        click_boolean_num = $.grep(flag_click_boolean, function (str) { return str == 'false' }).length;
                        if (click_boolean_num != 0) {
                            $('.choose_num').text('(' + click_boolean_num + ')');
                        } else if (click_boolean_num == 0) {
                            $('.choose_num').text('');
                        }
                        setSubscriptionList(flag_num, false);
                        return false;
                    }

                }
            });

        });

        function setSubscriptionList(id, IsCheck) {
            var tempList = $("#selectlist").val().split(",");
            if (IsCheck == true) {
                tempList.push(id);
            } else {
                tempList = deleteFromList(tempList, id);
            }

            tempList = popEmpty(tempList);
            $("#selectlist").val(tempList);
        }

        function deleteFromList(List, delData) {
            var remain = new Array();
            for (var i in List) {
                if (List[i] == delData) {
                    continue;
                }
                remain.push(List[i]);
            }
            return remain;
        }

        function popEmpty(List) {
            if (List[0] == "") {
                List.shift();
            }
            return List;
        }
    </script>

    <!--訂閱EDM頁 // 滑入圖片效果:頁面寬度>768才有hover效果 //-->
    <script type="text/javascript">
        $(document).ready(function () {
            if ($(window).width() > 768) {
                $(".food-select .food-pic span").hover(
                  function () {
                      $(this).find('i').css({
                          'background-color': 'rgba(255,255,255,0.8)',
                          'transition-duration': '0.3s;',
                          '-moz-transition-duration': '0.3s',  /*Firefox 4*/
                          '-webkit-transition-duration': '0.3s', /* Safari 和 Chrome */
                          '-o-transition-duration': '0.3s'
                      });
                  }, function () {
                      $(this).find('i').css({
                          'background-color': 'rgba(255,255,255,0.3)'
                      });
                  }
                )
            } else {
                $(".food-select .food-pic span").find('i').css({
                    'background-color': '#fff'
                });
            }
            $(window).resize(function () {
                if ($(window).width() > 768) {
                    $(".food-select .food-pic span").hover(
                      function () {
                          $(this).find('i').css({
                              'background-color': 'rgba(255,255,255,0.8)',
                              'transition-duration': '0.3s;',
                              '-moz-transition-duration': '0.3s',  /*Firefox 4*/
                              '-webkit-transition-duration': '0.3s', /* Safari 和 Chrome */
                              '-o-transition-duration': '0.3s'
                          });
                      }, function () {
                          $(this).find('i').css({
                              'background-color': 'rgba(255,255,255,0.3)'
                          });
                      }
                    )
                } else {
                    $(".food-select .food-pic span").find('i').css({
                        'background-color': '#fff'
                    });
                }
            });
        });
    </script>
    
    <!--載入大B // Slide效果 //-->
     <!-- //訂閱EDM頁 // 選擇項目-彈跳視窗 (需先讀入#divtopbn  彈出視窗才有效)-->
<script>
    $(function () {
        $("#divtopbn").removeClass();
        $('#divtopbn').html('<%=GetTitleBanner() %>');
        $('#divtopbn').show();
        var swiper = new Swiper('#topbn', {
            autoplay: 3500,
            autoplayDisableOnInteraction: false,
            speed: 2000,
            loop: true,
            effect: 'fade',
            fade: {
                crossFade: true,
            }
        })

        //自動完成email
        $("#subscribeEmail1,#subscribeEmail2").completer({
            separator: "@",
            source: window.EmailDomainSource,
            itemSize: 7
        });

        $('.popup_select_list').bind('click', function (e) {
            e.preventDefault();
            $('.select-list-block').bPopup();
        });

        $(".food_pic_95").find(".food-text").find("p").text("美食‧桃竹苗");
        $(".food_pic_96").parent().hide();

        $(".food_pic_97").find(".food-text").find("p").text("美食‧中彰投");

        $(".food_pic_99").find(".food-text").find("p").text("美食‧雲嘉南");
        $(".food_pic_98").parent().hide();

        $(".food_pic_90").find(".food-text").find("h4").text("旅遊‧玩美‧休閒");
        $(".food_pic_89").parent().hide();
    });
</script>

    <script type="text/javascript">
        function Subscribe(email, categoryId) {
            var postData = { email: email, categoryId: categoryId };
            $.ajax({
                type: "POST",
                url: '<%= ResolveUrl("~/service/ppon/AddEdm")%>',
                data: JSON.stringify(postData),
                contentType: "application/json",
                dataType: "json",
                success: function (res) {
                    success = true;
                }
            });
        }

        function onSubscribe(id) {
            var mailReg = /^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i;
            var mail = $('#subscribeEmail' + id).val();
            var sublist = $("#selectlist").val();
            if (mailReg.test(mail)) {
                if (sublist.trim() == '') {
                    alert('請至少勾選一個頻道喔!');
                } else {
                    var cid = sublist.split(',');
                    for (var i = 0; i < cid.length; i++) {
                        Subscribe(mail, cid[i]);
                    }
                    alert('謝謝您！訂閱成功！我們將持續發送好康訊息給您');
                }
            } else {
                alert('您的電子信箱格式不符');
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MC" runat="server">
        <div class="center" style="display: block">

        <!--// popup //start-->
            <div class="select-list-block">
                <div class="b-close b-cancel"><i class="fa fa-times fa-fw"></i></div>
                <ul>
                    <asp:Repeater ID="rptPponDealAreaPop" runat="server">
                        <ItemTemplate>
                            <li class="edm_memu_<%# ((CategoryNode)Container.DataItem).CategoryId %>">
                                <span>
                                    <i class="fa fa-check-circle fa-fw"></i><%= CategoryName %>‧<%# ((CategoryNode)Container.DataItem).CategoryName %></span></li>
                        </ItemTemplate>
                    </asp:Repeater>
                    <asp:Repeater ID="rptChannelPop" runat="server">
                        <ItemTemplate>
                            <li class="edm_memu_<%# ((CategoryNode)Container.DataItem).CategoryId %>">
                                <span>
                                    <i class="fa fa-check-circle fa-fw"></i><%# ((CategoryNode)Container.DataItem).CategoryName %>
                                </span>
                            </li>
                        </ItemTemplate>
                    </asp:Repeater>
                    <li class="float-none"></li>
                </ul>
                <p class="b-close b-done">
                    確定
                    <sapn class="choose_num"></sapn>
                </p>
            </div>

        <h2>全台美食</h2>
        <h4>熱門團購</h4>
        <div class="food-select">
            <asp:Repeater ID="rptPponDealArea" runat="server" OnItemDataBound="rptPponDealArea_ItemDataBound">
                <ItemTemplate>
                    <div class="food-pic">
                        <span class="food_pic_<%# ((CategoryNode)Container.DataItem).CategoryId %>">
                            <div class="food-text">
                                <p style="<%# GetPponDealCSSP(((CategoryNode)Container.DataItem).CategoryId) %>"><%= CategoryName %>‧<%# ((CategoryNode)Container.DataItem).CategoryName %></p>
                            </div>
                            <asp:Image ID="imgPponDeal" runat="server" />
                            <i class="fa fa-check fa-2x"></i>
                        </span>
                    </div>
                </ItemTemplate>
            </asp:Repeater>
        </div>

        <h2>專屬好康</h2>
        <h4>優惠不漏接</h4>
        <div class="food-select deal-bottom">
            <asp:Repeater ID="rptChannel" runat="server" OnItemDataBound="rptChannel_ItemDataBound">
                <ItemTemplate>
                    <div class="food-pic <%# GetChannelCSS(((CategoryNode)Container.DataItem).CategoryId) %>">
                        <span class="food_pic_<%# ((CategoryNode)Container.DataItem).CategoryId %>">
                            <div class="food-text food-text-center">
                                <h4><%# ((CategoryNode)Container.DataItem).CategoryName %></h4>
                                <p><%# GetChannelSubTitle(((CategoryNode)Container.DataItem).CategoryId) %></p>
                            </div>
                            <asp:Image ID="imgChannel" runat="server" />
                            <i class="fa fa-check fa-2x"></i>
                        </span>
                    </div>
                </ItemTemplate>
            </asp:Repeater>
        </div>

        <div class="swiper-bottom-footer">
            <div class="swiper-bottom">
                <div class="mail-click">
                    <input id="subscribeEmail2" type="text" class="edm-keyin" placeholder="輸入您的信箱如：service@17life.com" autocomplete="off">
                    <div class="mail-select">
                        <div class="select-list active popup_select_list">
                            選擇品項
                            <sapn class="choose_num"></sapn>
                        </div>
                    </div>
                    <input type="button" class="mail-btn" style="cursor: pointer" value="訂閱" onclick="onSubscribe('2');">
                </div>
            </div>
        </div>

    </div>
    <input type="hidden" id="selectlist" name="selectlist">
    
</asp:Content>

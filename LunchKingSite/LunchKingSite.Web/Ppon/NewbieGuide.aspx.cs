﻿using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using System;
using System.Linq;
using LunchKingSite.WebLib.Component;
using LunchKingSite.BizLogic.Facade;
using System.Collections.Generic;
using System.Web.Services;
using LunchKingSite.BizLogic.Model.CouponList;
using LunchKingSite.Core.Component.Template;
using LunchKingSite.Core.UI;
using System.Text;

namespace LunchKingSite.Web.Ppon
{
    /// <summary>
    /// 客服頁面
    /// 使用範例1
    /// https://www.17life.com/Ppon/NewbieGuide.aspx?s=20603&q=20640
    /// 使用範例2
    /// https://www.17life.com/Ppon/NewbieGuide.aspx?target=record
    /// </summary>
    public partial class NewbieGuide : BasePage
    {
        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        public static ISysConfProvider SystemConfig
        {
            get
            {
                return config;
            }
        }

        public int UserId
        {
            get
            {
                PponPrincipal user = this.User as PponPrincipal;

                if (user != null)
                {
                    return user.Identity.Id;
                }
                else
                {
                    return MemberFacade.GetUniqueId(this.User.Identity.Name);
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            SetFaq();
            InitShowPanel();
        }

        private void InitShowPanel()
        {
            string target = Server.UrlEncode(Request["s"] ?? Request["target"] ?? string.Empty);
            string ques = Server.UrlEncode(Request["q"] ?? string.Empty);
            //特別修正
            //為[新手上路] [七日猶豫期 與 通訊交易解除權合理例外情事適用準則]
            if (target == "3" && ques == "20469")
            {
                target = "20432";
                ques = "20644";
            }
            if (!string.IsNullOrWhiteSpace(target))
            {
                string initPanelJs = string.Format(@"
$(function () {{
    $('.fq-info').hide();
    $('.fq-inlbox li').removeClass('on');
    $('.li_service_{0}').click();
    $('.li_service_{0}').addClass('on');
    var accordionindex = $('.answer_content_{1}').parent().data('accordionindex');
    $('#accordion').accordion('activate', accordionindex );
}});", target, ques);
                ClientScript.RegisterClientScriptBlock(typeof(string), "qls", initPanelJs, true);
            }
            else
            {
                string initPanelJs = string.Format(@"
$(function () {{
    $('.question_content').hide().eq(0).show();
}});", target, ques);
                ClientScript.RegisterClientScriptBlock(typeof(string), "qls", initPanelJs, true);
            }
        }

        private void SetFaq()
        {
            FaqCollection faqs = ProviderFactory.Instance().GetProvider<IPponProvider>().FaqCollectionGet();
            string links = "<ul>";
            string toplinks = "<ul>";
            int index = 0;
            int faqIndex = 0;
            foreach (Faq sectionItem in faqs.Where(x => x.Pid == 0 && x.Mark != "H").OrderBy(x => x.Mark).ThenBy(x => x.Sequence))
            {
                ContentSection.Text += string.Format("<div class='question_content question_{0}'><h1 class='title'>{1}<a href='javascript: void(0)' class='rwd_btn_service_list'>回客服首頁</a></h1>",
                    sectionItem.Id, sectionItem.Contents);
                if ((Request["id"] == null && index == 0) || (Request["id"] != null && Request["id"] == "li" + sectionItem.Id))
                {
                    links += string.Format("<li class='li_service_{0} on' data-s='{0}'>{1}</li>",
                        sectionItem.Id, sectionItem.Contents);
                    toplinks += string.Format("<li class='li_service_{0}' data-s='{0}'><a href='javascript:void(0)'>{1}<i class='fa fa-angle-right fa-lg fa-fw'></i></a></li>",
                                            sectionItem.Id, sectionItem.Contents);
                }
                else
                {
                    links += string.Format("<li class='li_service_{0}' data-s='{0}'>{1}</li>",
                        sectionItem.Id, sectionItem.Contents);
                    toplinks += string.Format("<li class='li_service_{0}' data-s='{0}'><a href='javascript:void(0)'>{1}<i class='fa fa-angle-right fa-lg fa-fw'></i></a></li>",
                                            sectionItem.Id, sectionItem.Contents);
                }
                int listCount = 1;
                foreach (Faq questionItem in faqs.Where(x => x.Pid == sectionItem.Id).OrderBy(x => x.Sequence))
                {
                    ContentSection.Text += "<div class='fq-questionBOX' data-s='" + sectionItem.Id + "' data-q='" + questionItem.Id + 
                        "' data-accordionindex='" + faqIndex + 
                        "'><div class='fq-qtitle'><div class='qt-box'>" + listCount + ". " + questionItem.Contents + "</div>";
                    ContentSection.Text += "<span class='icon-chevron-faq rdl-iconshow'></span></div><div class='fq-info answer_content_"+questionItem.Id+"'>";

                    Faq answerItem = faqs.Where(x => x.Pid == questionItem.Id).DefaultIfEmpty().First();

                    ContentSection.Text += (answerItem == null ? string.Empty : answerItem.Contents) + "</div></div>";
                    listCount++;
                    faqIndex++;
                }
                ContentSection.Text += "</div>";
                index++;
            }
            LinkSection.Text = LinkSection.Text = links + "</ul>";
            TopLinkSection.Text = TopLinkSection.Text = toplinks + "</ul>";
        }




        [WebMethod]
        public static dynamic SendCouponListMail(string userEmail, int duringSelected, string duringString)
        {
            var startDate = DateTime.Today;
            var endDate = DateTime.Today;

            List<ViewCouponListMain> couponListMain;

            var userId = MemberFacade.GetUniqueId(userEmail);
            if (duringSelected == (int)MailOrderDuringFilter.All)
            {
                couponListMain = MemberFacade.ViewCouponListMainGetListByUser(0, 0, userId, NewCouponListFilterType.None, Guid.Empty, false, true).ToList();
                if (couponListMain.Count != 0)
                {
                    startDate = couponListMain.Min(x => x.CreateTime);
                }
            }
            else
            {
                startDate = (duringSelected == (int)MailOrderDuringFilter.ThreeMonths) ? startDate.AddMonths(-3) : startDate.AddMonths(-6);
                couponListMain = MemberFacade.ViewCouponListMainGetListByUser(0, 0, userId, NewCouponListFilterType.None, Guid.Empty, false, true)
                                    .Where(x => x.CreateTime >= startDate).ToList();
            }

            if (couponListMain.Count == 0)
            {
                return new { IsSuccess = true, Msg = "您輸入的信箱並無購買紀錄，請您填寫曾訂購過的信箱哦！" };
            }

            var member = MemberFacade.GetMember(userEmail);
            var mailCouponListMainList = SetCouponListMain(couponListMain);
            var userTotalBonusPoint = MemberFacade.GetMemberPromotionValue(userEmail);

            var template = TemplateFactory.Instance().GetTemplate<MemberCouponListMail>();
            template.UserEmail = userEmail;
            template.UserName = member.DisplayName;
            template.IsGuest = member.IsGuest;
            template.AuthLink = member.IsGuest ? GetAuthLink(member) : string.Empty;
            template.CashPoint = OrderFacade.GetSCashSum(userEmail).ToString("F0");
            template.BonusPoint = Math.Floor(userTotalBonusPoint / 10).ToString("F0");
            template.PayeasyCash = MemberFacade.GetPayeasyCashPoint(member.UniqueId).ToString("F0");
            template.DuringDate = string.Format("({0:yyyy/MM/dd} ~ {1:yyyy/MM/dd})", startDate, endDate);
            template.DuringString = duringString;
            template.MainList = mailCouponListMainList;

            bool isSuccess = EmailFacade.SendCouponListToMail(userEmail, member.DisplayName, template);
            return isSuccess ? new { IsSuccess = true, Msg = "17Life已將購買紀錄寄到您的信箱中，請您前往查看，謝謝！" }
                             : new { IsSuccess = false, Msg = "寄送失敗！" };
        }

        /// <summary>
        /// 取得訂單
        /// </summary>
        /// <param name="couponListMain"></param>
        /// <returns></returns>
        private static List<MailCouponListMain> SetCouponListMain(List<ViewCouponListMain> couponListMain)
        {
            var mailCouponListMainList = new List<MailCouponListMain>();
            var couponSeqList = MemberFacade.GetCouponListSequenceByOidList(couponListMain.Select(x => x.Guid).ToList()).ToList(); //全部憑證

            foreach (var main in couponListMain.OrderByDescending(x => x.CreateTime))
            {
                var isFami = (main.Status & (int)GroupOrderStatus.FamiDeal) > 0;
                var mainInfo = OrderFacade.GetCouponListMainStatus(main);

                if (isFami || mainInfo.IsZeroPponDeal) continue;

                var orderGuid = main.Guid;
                var cuponListDetail = SetCouponListDetail(main, mainInfo, couponSeqList.Where(x => x.Guid == orderGuid).ToList());
                var orderPrice = (((main.BusinessHourStatus & (int)LunchKingSite.Core.BusinessHourStatus.PriceZeorShowOriginalPrice) > 0)
                    ? main.ItemOrigPrice
                    : main.ItemPrice);
                List<int> statusType;
                if (mainInfo.IsPpon)
                {
                    if (mainInfo.IsPayAtm)
                    {
                        statusType = mainInfo.IsShowPayAtm
                            ? new List<int> { (int)MailCouponListStatusType.NotUsed } //ATM 尚未付款
                            : new List<int> { (int)MailCouponListStatusType.None };//ATM 逾期未付款
                    }
                    else
                    {
                        statusType = cuponListDetail.Select(x => x.StatusType).ToList();
                    }
                }
                else
                {
                    statusType = new List<int> { mainInfo.MainOrderStatusType };
                }

                mailCouponListMainList.Add(
                    new MailCouponListMain
                    {
                        OrderGuid = main.Guid,
                        OrderDate = main.CreateTime,
                        OrderId = main.OrderId,
                        OrderName = main.ItemName.TrimToMaxLength(17, "..."),
                        OrderNameLink = string.Format("{0}/{1}", config.SiteUrl, main.BusinessHourGuid),
                        OrderNameTag = "好康價: " + orderPrice.ToString("F0"),
                        OrderExp = (main.DeliveryType.HasValue && int.Equals(2, main.DeliveryType.Value))
                            ? "-"
                            : ((main.BusinessHourDeliverTimeE == null)
                                ? string.Empty
                                : main.BusinessHourDeliverTimeE.Value.ToString("yyyy/MM/dd")),
                        OrderStatus = (mainInfo.IsPayAtm && mainInfo.IsShowPayAtm) ? "ATM 尚未付款" : mainInfo.MainOrderStatus,
                        IsDownloadCouponPdf = mainInfo.IsDownloadCouponPdf,
                        CouponListDetail = cuponListDetail,
                        StatusType = statusType
                    });
            }
            return mailCouponListMainList;
        }

        /// <summary>
        /// 取得憑證明細
        /// </summary>
        /// <param name="main"></param>
        /// <param name="mainInfo"></param>
        /// <param name="couponDetailList"></param>
        /// <returns></returns>
        private static List<MailCouponListDetail> SetCouponListDetail(ViewCouponListMain main, CouponListMainInfo mainInfo, List<ViewCouponListSequence> couponDetailList)
        {
            var mailCouponListDetailList = new List<MailCouponListDetail>();
            var cancelStatus = main.ReturnStatus;
            var isFami = (main.Status & (int)GroupOrderStatus.FamiDeal) > 0;
            var isPEZevent = ((main.Status & (int)GroupOrderStatus.PEZevent) > 0) &&
                         (cancelStatus == (int)ProgressStatus.Completed ||
                          cancelStatus == (int)ProgressStatus.CompletedWithCreditCardQueued);

            foreach (var detail in couponDetailList)
            {
                var detailOrderSn = (isFami || isPEZevent || mainInfo.IsPayAtm) ? string.Empty : detail.SequenceNumber;
                var detailOrderCode = (isFami || isPEZevent || mainInfo.IsPayAtm) ? string.Empty : detail.Code;
                var downloadLink = (mainInfo.IsDownloadCouponPdf)
                    ? string.Format("{0}/User/CouponGet.aspx?cid={1}&sequencenumber={2}&couponcode={3}&ct={4}",
                                    config.SiteUrl, detail.Id, detail.SequenceNumber, detail.Code, detail.OrderDetailCreateTime.Ticks)
                    : string.Empty;

                var detailStatus = OrderFacade.GetCouponListDetailStatus(main, detail);

                mailCouponListDetailList.Add(new MailCouponListDetail
                {
                    OrderDetailCreatime = detail.OrderDetailCreateTime,
                    OrderSn = mainInfo.IsPayAtm ? "-" : detailOrderSn,
                    OrderCode = mainInfo.IsPayAtm ? "-" : detailOrderCode,
                    CouponStatus = mainInfo.IsPayAtm ? "-" : detailStatus.Status,
                    StatusType = detailStatus.StatusType,
                    IsEnabledDownLoad = detailStatus.IsEnabledDownLoad,
                    DownloadLink = downloadLink
                });
            }

            //!isPpon 並沒有憑證明細，需new一個detail於前端顯示「憑證狀態」、以及做為「憑證分類」依據
            if (couponDetailList.Count == 0 && !mainInfo.IsPpon)
            {
                mailCouponListDetailList.Add(new MailCouponListDetail
                {
                    OrderDetailCreatime = main.CreateTime,
                    OrderSn = "-",
                    OrderCode = "-",
                    CouponStatus = mainInfo.MainCouponStatus,
                    StatusType = (int)MailCouponListStatusType.None,
                    DownloadLink = ""
                });
            }

            return mailCouponListDetailList;
        }

        /// <summary>
        /// 取得認證連結
        /// </summary>
        /// <param name="member"></param>
        /// <returns></returns>
        private static string GetAuthLink(Member member)
        {
            var result = string.Empty;

            var authInfo = MemberFacade.MemberAuthInfoGet(member.UniqueId);
            result = string.Format("{0}/NewMember/memberauth.aspx?uid={1}&key={2}&code={3}", config.SiteUrl, member.UniqueId, authInfo.AuthKey, authInfo.AuthCode);

            return result;
        }
    }
}
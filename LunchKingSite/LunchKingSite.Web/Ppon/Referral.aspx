﻿<%@ Page Language="C#" MasterPageFile="~/Ppon/Ppon.Master" AutoEventWireup="true" CodeBehind="Referral.aspx.cs" Inherits="LunchKingSite.Web.Ppon.WebForm1" %>
<%@ Register Src="~/UserControls/Paragraph.ascx" TagName="Paragraph" TagPrefix="uc1" %>
<%@ Register Src="~/UserControls/FacebookLoginButton.ascx" TagName="FacebookLoginButton" TagPrefix="uc1" %>
<%@ MasterType VirtualPath="~/Ppon/Ppon.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MC" runat="server">
    <script type="text/javascript" language="javascript">
        function UserTrackSet(t, u) {
            switch (t) {
                case 1:
                    $.ajax({
                        type: "POST",
                        url: "Referral.aspx/PostToFaceBook",
                        data: '{}',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (msg) {
                            if (msg.d != '') {
                                location.href = u.href;
                            }
                        }
                    });
                    break;
                case 2:
                    $.ajax({
                        type: "POST",
                        url: "Referral.aspx/PostToPlurk",
                        data: '{}',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (msg) {
                            if (msg.d != '') {
                                location.href = u.href;
                            }
                        }
                    });
                    break;
            }
            return false;
        };
    </script>

    <div id="referral_Nlogin">
        <div id="referral_NTitle"></div>

        <asp:PlaceHolder ID="p1" runat="server">
        <div id="referral_infoBackground">
            <div id="referral_MsnTitle"><img src="../Themes/default/images/17life/G1-8/star.png" width="30" height="31" />以下是您的專屬邀請連結</div>
            <div id="referral_Msninfo">
                <asp:TextBox ID="tbReferral" runat="server" Columns="80"></asp:TextBox>
            </div>
              <div id="Referral_EGi"><img src="../Themes/default/images/17Life/G1-8/referral_EGi.png" width="117" height="143" /></div>
              <div id="referral_info">
 步驟1.複製上面的邀請連結<br />
 步驟2.把連結傳給親友<br />
 步驟3.親友點選連結後回到17Life完成首次購買<br />
 步驟4.您拿到推薦紅利<%=Bonus.ToString()%>元<br />
            </div>
            <div id="referral_Title2">您還可以用下面三種方式分享您的邀請連結<br />
                <span class="red_buy">邀請次數無上限～</span> 分享越多，紅利越多喔！<br />
                <span class="red_buy"><%=Bonus.ToString()%>元(含)以下的好康恕不適用此紅利贈送活動 </span></div>
                </div>
            <div id="referral_FB_PL_MA_area">
                <div id="referral_FB_area"><td width="100"><asp:HyperLink ID="hf" runat="server" ImageUrl="../Themes/default/images/17Life/G1-8/FB_Btn.png" Width="113" Height="30" BorderWidth="0"  onclick = "return UserTrackSet(1,this);"></asp:HyperLink></td></div>
                <div id="referral_PL_area"><asp:HyperLink ID="hp" runat="server" ImageUrl="../Themes/default/images/17Life/G1-8/PLK_Btn.png" Width="82" Height="30" BorderWidth="0"  onclick = "return UserTrackSet(2,this);"></asp:HyperLink></div>
                <div id="referral_MA_area"><asp:HyperLink ID="hm" runat="server" ImageUrl="../Themes/default/images/17Life/G1-8/MAIL_Btn.png" Width="81" Height="30" BorderWidth="0"></asp:HyperLink></div>
            </div>
        </div>
        </asp:PlaceHolder>

        <asp:PlaceHolder ID="p2" runat="server">
        <div id="referral_NinfoBackground">
            <div id="referral_Ninfo">
                每邀請一位親友完成首次好康購買，就送您<%=Bonus.ToString()%>元喔～<br />
            系統將會於親友購買的好康成立後將紅利存入您的17Life帳戶中<br />
            </div>
            <div id="referral_N_Logintitle">請先註冊或登入</div>
            <div id="referral_Facebook_Login"><uc1:FacebookLoginButton ID="fbl" runat="server" ReturnUrl="~/user/sso.aspx?v=p" /></div>
            <div id="referral_NPez_17login"><asp:HyperLink ID="hl" runat="server" ImageUrl="../Themes/default/images/17Life/G1-8/17p_Header_pezlogin.png" /></div>
            <div id="referral_Njoin"><asp:HyperLink ID="hj" runat="server" Text="加入會員"/></div>
            <div class="red_buy" id="referral_Tip"><%=Bonus.ToString()%>元(含)以下的好康恕不適用此紅利贈送活動</div>
        </div>
        </asp:PlaceHolder>
</div>

<div id="referral_Faq">
  <div id="referral_FaqTitle"><img src="../Themes/default/images/17Life/G1-8/17p_45f1.png" width="239" height="42" /></div>
  <div id="referral_Faqcontent">
      <uc1:Paragraph ID="sp" runat="server" />
  </div>
  <div id="referral_Faqend"></div>
</div>
</asp:Content>

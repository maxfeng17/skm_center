﻿using System;
using System.Web;
using System.Web.Services;
using System.Text.RegularExpressions;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.WebLib;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.UI;

namespace LunchKingSite.Web.Ppon
{
    public partial class Promo : BasePage
    {
        public bool ShowEventEmail { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["cn"]))
                pMain.ContentName = Request.QueryString["cn"];

        }

        [WebMethod]
        public static string SendAppDownloadSms(string phone)
        {
            bool errorFormat = false;
            if (phone.Trim().Length != 10)
            {
                errorFormat = true;
            }
            else 
            {
                string pattern = "09[0-9]{2}[0-9]{6}";
                Regex Regex1 = new Regex(pattern, RegexOptions.IgnoreCase);
                if (!Regex1.IsMatch(phone)) 
                {
                    errorFormat = true;
                }
            }

            if (errorFormat) 
            {
                return new LunchKingSite.Core.JsonSerializer().Serialize(new { Success = false, Message = "您輸入的手機格式錯誤。" });
            }

            return EventFacade.AppDownloadSendSMS(phone, Helper.GetClientIP());
        }

    }
}
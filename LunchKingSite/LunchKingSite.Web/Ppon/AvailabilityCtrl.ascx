﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AvailabilityCtrl.ascx.cs"
    Inherits="LunchKingSite.Web.Ppon.AvailabilityCtrl" %>
<asp:Repeater ID="rep_Multiple" runat="server" OnItemDataBound="rm_ItemDataBound">
    <ItemTemplate>
        <div id="AddressTitle">
            <asp:Literal ID="n" runat="server" />
        </div>
        <div id="Addressinner">
            <table>
                <tr>
                    <td colspan="2">
                        <ul>
                            <asp:Literal ID="p" runat="server" />
                            <asp:Literal ID="a" runat="server" />
                            <asp:Literal ID="op" runat="server" />
                            <asp:Literal ID="cd" runat="server" /></ul>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <ul>
                            <asp:Literal ID="liVehicle" runat="server" /></ul>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                    </td>
                </tr>
                <tr>
                    <!--<td style="width: 15px">
                    </td>-->
                    <td colspan="2">
                        <ul>
                            <asp:Literal ID="mr" runat="server" />
                            <asp:Literal ID="ca" runat="server" />
                            <asp:Literal ID="bu" runat="server" />
                            <asp:Literal ID="ov" runat="server" />
                        </ul>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <ul>
                            <asp:Literal ID="u" runat="server" /></ul>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <ul>
                            <asp:Literal ID="fb" runat="server" />
                            <asp:Literal ID="bl" runat="server" />
                            <asp:Literal ID="ol" runat="server" /></ul>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <ul>
                            <asp:Literal ID="r" runat="server" /></ul>
                    </td>
                </tr>
            </table>
            </ul>
            <asp:Literal ID="ap" runat="server" Visible="false" />
        </div>
    </ItemTemplate>
</asp:Repeater>


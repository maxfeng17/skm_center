﻿<%@ Page Language="C#" MasterPageFile="~/Ppon/Ppon.Master" AutoEventWireup="true" CodeBehind="Unsubscription2.aspx.cs" Inherits="LunchKingSite.Web.Ppon.Unsubscription2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <link href="../Themes/default/images/17Life/G1-8/17UG.css" rel="stylesheet" type="text/css" />
    <link href="../Themes/default/images/17Life/G3/PPF.css" rel="stylesheet" type="text/css" />
    <link href="../Themes/PCweb/css/service.css" rel="stylesheet" type="text/css" />
    <link href="../Themes/PCweb/css/ppf-tw.css" rel="stylesheet" type="text/css" />
    <link href="../themes/PCweb/css/RDL-L.css" rel="stylesheet" type="text/css" />
    <link href="../themes/PCweb/css/RDL-M.css" rel="stylesheet" type="text/css" />
    <link href="../themes/PCweb/css/RDL-S.css" rel="stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="MC" runat="server">
    <div class="center">
        <div class="EDMun-full-block">
            <div class="EDMun-content" style="font-size: 20px; text-align: center; padding: 20px; font-weight:bold">

                    您的訂閱已取消，謝謝您的惠顧
  
            </div>
        </div>
    </div>
</asp:Content>

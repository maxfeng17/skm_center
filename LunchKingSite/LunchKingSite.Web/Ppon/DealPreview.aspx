﻿<%@ Page Language="C#" MasterPageFile="~/Ppon/Ppon.Master" AutoEventWireup="true"
    CodeBehind="DealPreview.aspx.cs" Inherits="LunchKingSite.Web.Ppon.DealPreview"
    Title="17Life交易" %>

<%@ Register Src="~/UserControls/Paragraph.ascx" TagName="Paragraph"
    TagPrefix="uc3" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MC" runat="server">
    <script type="text/javascript" src="../Tools/js/jquery.blockUI.js"></script>
    <script type="text/javascript" src="../Tools/js/jquery.cascadeAddress.js"></script>
    <meta name="viewport" content="width=device-width initial-scale=1.0" />
    <link href="../Themes/default/images/17Life/G3/PPD.css" rel="stylesheet" type="text/css" />
    <link href="../Themes/default/images/17Life/G3/D2-5-Checkout.css" rel="stylesheet" type="text/css" />
    <link href="../themes/PCweb/css/RDL-L.css" rel="stylesheet" type="text/css" />
    <link href="../themes/PCweb/css/RDL-M.css" rel="stylesheet" type="text/css" />
    <link href="../themes/PCweb/css/RDL-S.css" rel="stylesheet" type="text/css" />
    <script src="<%=ResolveUrl("~/Tools/js/json2.js") %>" type="text/javascript"></script>
    <script type="text/javascript">
        function calculateTotal() {
            var otherAmount = bcash + pcash + scash;
            var total = parseInt($("#<%= hidsubtotal.ClientID %>").val(), 10) - otherAmount;

            $("#total").html(total.toString());
            $("#cltotal").html(total.toString());
            $("#<%= hidtotal.ClientID %>").val(total.toString());
            $("#totalErr").hide();

            if (parseInt(total) <= 0) {
                $('.SelectedPayWay').hide();
                $('#divConfirmPayWay').hide();
                if (total < 0) {
                    $('.SelectedPayWay').show();
                    $('#divConfirmPayWay').show();
                    $("#totalErr").show();
                }
            } else {
                $('.SelectedPayWay').show();
                $('#divConfirmPayWay').show();
            }
        }

        function changeFreight(subTotal, combopack, oprice){
            var freight = $("#<%= hidFreight.ClientID %>").val();
            var zeroFreightAmt = "0";
            var unit = "份";
            if (combopack > 1) {
                unit = "組";
            }

            if ("" != freight) {
                var freights = jQuery.parseJSON(freight);
                var currenFreight = undefined;
                var currentEndAmt = 0;
                $.each(freights, function(key, val) {
                    if (subTotal >= val.startAmt && subTotal < val.endAmt ) {
                        currenFreight = val.freightAmt;
                        $("#dCharge").html(val.freightAmt);
                        $("#cdCharge").html(val.freightAmt);
                        currentEndAmt = val.endAmt;
                    }

                    if (subTotal < val.startAmt && val.freightAmt == 0 && ("0" == zeroFreightAmt || parseInt(zeroFreightAmt, 10) > parseInt(val.startAmt, 10))) {
                        zeroFreightAmt = val.startAmt;
                    }
                });

                if ("0" == zeroFreightAmt) {
                    var nextFreight = 0;
                    $.each(freights, function(key, val) {
                        if (currentEndAmt == val.startAmt) {
                            nextFreight = val.freightAmt;
                        }
                    });
                    if (0 != nextFreight && nextFreight < parseInt(currenFreight, 10)) {
                        $("#<%= lbldc.ClientID %>").html("<span class='hint'>-還差" + Math.ceil((currentEndAmt - subTotal) / oprice) + unit + "，運費將降為" + nextFreight + "元喔!</span>");
                    } else {
                        $("#<%= lbldc.ClientID %>").html("");
                    }
                } else {
                    if (currenFreight == "0") {
                        $("#<%= lbldc.ClientID %>").html("");
                    } else {
                        $("#<%= lbldc.ClientID %>").html("<span class='hint'>-還差" + Math.ceil((parseInt(zeroFreightAmt, 10) - subTotal) / oprice) + unit + "就可以免運費喔!</span>");
                    }
                }
                $("#<%= hiddCharge.ClientID %>").val($("#dCharge").html());
                $("#ltotal").html((subTotal + (isNaN(parseInt($("#dCharge").html().replace('$',''))) ? 0 : parseInt($("#dCharge").html().replace('$','')))).toString());
            } else {
                var po = $('#<%= hpromo.ClientID %>').val();
                var hdc = $("#<%= hiddCharge.ClientID %>").val();
                var ldc = $("#dCharge").html();
                var hsdc = document.getElementById('<%= hidcstatic.ClientID %>');

                if (po != '') {
                    if (Math.ceil(parseInt(po, 10) - (subTotal)) <= 0) {
                        $("#dCharge").html("0");
                        $("#cdCharge").html("0");
                        $("#<%= hiddCharge.ClientID %>").val("0");
                        $('#<%= lbldc.ClientID %>').html("");
                        $("#ltotal").html((subTotal).toString());
                    } else {
                        $("#dCharge").html(hsdc.value);
                        $("#cdCharge").html(hsdc.value);
                        $("#<%= hiddCharge.ClientID %>").val(hsdc.value);
                        $("#ltotal").html((subTotal + parseInt(hdc, 10)).toString());
                        $("#<%= lbldc.ClientID %>").html('<span class="hint">-還差' + Math.ceil((parseInt(po) - subTotal) / oprice) + unit + '就可以免運費喔!</span>');
                    }
                } else {
                    $("#ltotal").html((subTotal + parseInt(hdc, 10)).toString());
                }
            }
            $("#total").html($("#ltotal").html());
        }

function checkItemOptions(){
    var rlt = "";
    var opt = "";
    var oid = "";
    $('option:selected', '#itemOptions').each(function () {
        if (this.text.indexOf('已售完') > -1) {
            alert('請勿選擇已售完的選項，謝謝');
            return "||||";
        }
        if ($(this).val() == "Default") {
            return "";
        } else {
            rlt += " (" + this.text + ")";
            opt += "," + this.text + "|^|"  + $(this).val();
            oid += "," + $(this).val();
        }
    });
    return rlt + "||" + opt.substr(1, opt.length - 1) + "|#|ggyy8" + "||" + oid.substr(1, oid.length - 1);
}

function checkOptionLimit(id, qty, old) {
    var result = "";
    var dirty = false;
    var json = $.parseJSON($("#<%= hidCurrentLimits.ClientID %>").val());
    $.each(json, function() {
        if (id.indexOf(this.AccessoryId) >= 0) {
            if (this.Limit < qty) {
                result = this.AccessoryName;
                return false;
            } else {
                this.Limit = this.Limit - qty - old;
                dirty = true;
            }
        }
    });

    if ("" === result && dirty) {
        $("#<%= hidCurrentLimits.ClientID %>").val(JSON.stringify(json));
}

    return result;
}

function addOptionLimitBack(id, qty) {
    var dirty = false;
    var json = $.parseJSON($("#<%= hidCurrentLimits.ClientID %>").val());
    $.each(json, function() {
        if (id.indexOf(this.AccessoryId) >= 0) {
            this.Limit = this.Limit + qty;
            dirty = true;
        }
    });

    if (dirty) {
        $("#<%= hidCurrentLimits.ClientID %>").val(JSON.stringify(json));
}
}

function checkSameItem(ids, qty) {
    var result = "0";
    var bd = null;
    $.each($("#itemList li"), function (index, val) {
        if ($(val).find('.clearfix span:eq(0)').text().indexOf(ids) >= 0) {
            result = $(val).find('.clearfix span:eq(1)').text();
            bd = $(val).find('.item-amount input');
        }
    });

    var ky = checkOptionLimit(ids, qty, parseInt(result, 10));
    if ("" != ky) {
        alert("抱歉，您所選擇的(" + ky + ")數量不足，請重新選擇數量或其他規格。");
        return "-1";
    } else {
        if (null != bd) {
            delItemFromCart($(bd));
        }
    }

    return result;
}

function addItemToCart() {
    var opt = checkItemOptions();
    var tt = opt.split("||");
    if ($('select.opt').length === 0 || ("" !== tt[2] && ($('select.opt').length == tt[2].split(',').length))) {
        $("#itemError").hide();

        var p = parseInt($("#<%= hidprice.ClientID %>").val(), 10);
        var q = parseInt($("#<%= itemQty.ClientID %>").val(), 10);
        var aq = parseInt($("#<%= hidqty.ClientID %>").val(), 10);
        var lq = parseInt($("#<%= hidQtyLimit.ClientID %>").val(), 10);
        var cset = parseInt($("#<%= hidComboPack.ClientID %>").val(), 10);

        if (lq < Math.ceil((aq + q) / cset)) {
            alert("最多只能購買" + $("#<%= hidQtyLimit.ClientID %>").val() + "份。");
            return;
        }

        var gg = Math.floor(Math.random() * 100);
        while ($("#cltr" + gg).length > 0) {
            gg = Math.floor(Math.random() * 100);
        }
        var qk = parseInt(checkSameItem(tt[2], q), 10);

        if (-1 != qk) {
            q += qk;
            var unitAdd = " x " + q;
            var hidCell = "<span style='display:none;'>" +  tt[2] + "</span><span style='display:none;'>" + q + "</span>";
            if (1 == cset) {
                unitAdd = "$" + p + unitAdd;
                $("#itemList").append("<li><div class='item-name'><%= shortDealName %>" + tt[0] + "</div><div class='item-amount'>" + unitAdd + "<input type='button' class='btn btn-mini rd-margin cartDelete' onclick='delItemFromCart(this);' xgg='" + gg + "' value='刪除' xqty='" + q + "' /></div><div class='clearfix'>" +  hidCell + "</div></li>");
                $("#confirmList").append("<li id='cltr" + gg + "'><div class='item-name'><%= shortDealName %>" + tt[0] + "</div><div class='total-amount'>$" + p + " x" + q + "&nbsp;=</div><div class='total-price'>$" + p * q + "</div><div class='clearfix'><span class='hid' style='display:none;'>" + tt[1].replace(/ggyy8/ig, q) + "</span></div></li>");
            } else {
                $("#itemList").append("<li><div class='item-name'>" + tt[0] + "</div><div class='item-amount'>" + unitAdd + "<input type='button' class='btn btn-mini rd-margin cartDelete' onclick='delItemFromCart(this);' xgg='" + gg + "' value='刪除' xqty='" + q + "' /></div><div class='clearfix'>" +  hidCell + "</div></li>");
                $("#comboSum").show();
                $("#confirmList").append("<li id='cltr" + gg + "'><div class='item-name'>" + tt[0] + "</div><div class='item-amount'>&nbsp;x" + q + "&nbsp;<div class='empty-btn'></div></div><div class='clearfix'><span class='hid' style='display:none;'>" + tt[1].replace(/ggyy8/ig, q) + "</span></div></li>");
            }

            var cselect = parseInt($("#<%= hidqty.ClientID %>").val(), 10);
            $("#<%= hidqty.ClientID %>").val(cselect + q);
            subTotal(p, cselect + q);

            var cset = checkComboSet();
            $("#setError").html(cset);
            if (0 != cset) {
                $("#deliveryChargeError").show().focus();
            } else {
                $("#deliveryChargeError").hide();
            }
        }
    } else if ('<%= IsShoppingCart %>'.toLowerCase() == 'true') {
        $("#itemError").show();
    }
}

function delItemFromCart(obj) {
    var cltrId = "cltr" + $(obj).attr("xgg");
    var q = parseInt($(obj).attr("xqty"), 10);
    var cselect = parseInt($("#<%= hidqty.ClientID %>").val(), 10);
    $("#<%= hidqty.ClientID %>").val(cselect - q);

    addOptionLimitBack($(obj).parent().siblings().eq(1).text(), q);
    $(obj).parent().parent().remove();
    $("#" + cltrId).remove();
    subTotal(parseInt($("#<%= hidprice.ClientID %>").val(), 10), cselect - q);

    var cset = checkComboSet();
    $("#setError").html(cset);
    if (0 != cset) {
        $("#deliveryChargeError").show().focus();
    } else {
        $("#deliveryChargeError").hide();
    }

    if (0 == (cselect - q)) {
        $("#comboSum").hide();
    }
}

function subTotal(price, cselect) {
    var cset = parseInt($("#<%= hidComboPack.ClientID %>").val(), 10);
    var total = price * Math.ceil(cselect / cset);
    $("#lsubtotal").html(total);
    $("#clsubtotal").html(total);
    changeFreight(total, cset, price);

    $("#<%= hidsubtotal.ClientID %>").val($("#ltotal").html());
}

function resetShoppingCart() {
    // 清除所有購物清單內容
    $('.cartDelete').each(function () {
        delItemFromCart(this);
    });
    addItemToCart();
}

function checkComboSet() {
    var cset = parseInt($("#<%= hidComboPack.ClientID %>").val(), 10);
            var cselect = parseInt($("#<%= hidqty.ClientID %>").val(), 10);
            var y = Math.ceil(cselect / cset);
            $("#totalComboSet").html(y);
            $("#totalConfirmComboSet").html(y);
            $("#totalConfirmComboTotal").html(y * parseInt($("#<%= hidprice.ClientID %>").val(), 10));

        if (0 == cselect || 0 != (cselect % cset)) {
            return (cset - (cselect % cset));
        }

        return 0;
    }

$(document).ready(function () {
    $("#<%= ddlStore.ClientID %>").change(function () { checkBranch(); });
    $("#setError").html($("#<%= hidComboPack.ClientID %>").val());

    //calc total
    $("#ltotal").html($("#dCharge").html());

    // 非使用購物車檔次
    if ('<%= IsShoppingCart %>'.toLowerCase() == 'false') {
        // 自動加入第一筆清單
        addItemToCart();
        // 每次異動 好康組合/數量 時，需重置購物清單內容
        $('select[id*=ddlMultOption]').change(function () { resetShoppingCart(); });
        $('select[id*=itemQty]').change(function () { resetShoppingCart(); });
    }else {
        // 顯示 [加入購物清單] 按鈕，及 [購物清單]
        $('#mixitemcart').css('display','');
        $('#AddToCartButton').css('display','');
    }

});
    </script>
    <asp:HiddenField ID="hidprice" runat="server" />
    <asp:HiddenField ID="hidsubtotal" runat="server" />
    <asp:HiddenField ID="hidtotal" runat="server" />
    <asp:HiddenField ID="hidqty" runat="server" />
    <asp:HiddenField ID="hpromo" runat="server" />
    <asp:HiddenField ID="hidcstatic" runat="server" />
    <asp:HiddenField ID="hiddCharge" runat="server" />
    <asp:HiddenField ID="hidFreight" runat="server" />
    <asp:HiddenField ID="hidComboPack" runat="server" />
    <asp:HiddenField ID="hidCurrentLimits" runat="server" />
    <asp:HiddenField ID="hidQtyLimit" runat="server" />
    <asp:HiddenField ID="hidMultiBranch" runat="server" />
    <img src='../Themes/PCweb/images/spinner.gif' style="display: none" />
    <div id="TOPBanner">
    </div>
    <asp:Panel ID="PanelBuy" runat="server">
        <div id="Left" class="rd-payment-Left-width">
            <div id="maincontent" class="clearfix">
                <asp:Panel ID="NomalPay" runat="server">
                    <%--好康交易結帳--%>
                    <div id="divFillPurchaseInfo" class="content-group payment">
                        <h1 class="rd-payment-h1">好康交易結帳</h1>
                        <hr class="header_hr rd-payment-width" />
                        <h2 class="rd-payment-h2">
                            <div class="step-num">
                                1
                            </div>
                            選擇好康規格與數量</h2>
                        <div class="grui-form">
                            <asp:UpdatePanel ID="upxy" runat="server" ChildrenAsTriggers="true" RenderMode="Inline">
                                <ContentTemplate>
                                    <div id="tdStore" class="form-unit">
                                        <asp:Label ID="lblStoreChioce" runat="server" CssClass="unit-label" Text="分店選擇" Visible="false"></asp:Label>
                                        <div class="data-input">
                                            <asp:PlaceHolder ID="pStore" runat="server">
                                                <asp:Panel ID="multiStores" runat="server">
                                                    <asp:DropDownList ID="ddlStoreCity" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlStoreCity_SelectedIndexChanged">
                                                    </asp:DropDownList>
                                                    <asp:DropDownList ID="ddlStoreArea" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlStoreArea_SelectedIndexChanged">
                                                        <asp:ListItem Text="請選擇" Value="0" />
                                                    </asp:DropDownList>
                                                </asp:Panel>
                                                <asp:DropDownList ID="ddlStore" runat="server">
                                                </asp:DropDownList>
                                                <span id="branchErr" class="error-text" style="display: none;">請選擇</span>
                                                <asp:TextBox ID="HiddenSingleStoreGuid" runat="server" Visible="false"></asp:TextBox>
                                            </asp:PlaceHolder>
                                        </div>
                                        <div id="divSingleStore" runat="server" class="data-input">
                                            <p>
                                                <asp:Label ID="lblSingleStore" runat="server"></asp:Label>
                                            </p>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            <div class="form-unit rd-payment-form-unit-disappear">
                                <label for="" class="unit-label">
                                    好康內容</label>
                                <div class="data-input">
                                    <p>
                                        <%=dealName %><span id="dsName" style="display: none;"><%= shortDealName %></span>
                                    </p>
                                </div>
                            </div>
                            <div class="mixitem">
                                <h3 class="rd-payment-h3">選擇好康組合</h3>
                                <div class="form-unit rd-pay-margin">
                                    <asp:Repeater ID="rpOption" runat="server" OnItemDataBound="rpOption_ItemDataBound">
                                        <HeaderTemplate>
                                            <label id="itemOptionsTitle" for="" class="unit-label">
                                                好康規格</label>
                                            <div id="itemOptions" class="data-input">
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:DropDownList ID="ddlMultOption" CssClass="opt" runat="server" size="1">
                                            </asp:DropDownList>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </div>
                                        </FooterTemplate>
                                    </asp:Repeater>
                                </div>
                                <div class="form-unit rd-pay-margin">
                                    <label for="" class="unit-label">
                                        數量</label>
                                    <div class="data-input">
                                        <asp:DropDownList ID="itemQty" runat="server">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div id="AddToCartButton" class="form-unit end-unit btn-center rd-pay-pos-fix" style="display: none;">
                                    <input type="button" onclick="javascript:addItemToCart();" class="btn btn-large btn-primary rd-payment-large-btn"
                                        value="加入購物清單" />
                                </div>
                                <div id="itemError" class="warning-box" style="display: none;">
                                    <p>
                                        請選擇您要購買的好康規格與數量後，加入購物清單
                                    </p>
                                </div>
                                <div id="deliveryChargeError" class="warning-box" style="display: none;">
                                    <p>
                                        還差 <span id="setError"></span>件商品才可以結帳！
                                    </p>
                                </div>
                            </div>
                            <div id="mixitemcart" class="mixitem" style="display: none;">
                                <h3>購物清單</h3>
                                <div class="item-list">
                                    <ul id="itemList">
                                    </ul>
                                </div>
                                <div id="comboSum" style="display: none;" class="item-list">
                                    <ul>
                                        <li>
                                            <div class="item-name">
                                                <%= shortDealName %>
                                            </div>
                                            <div class="item-amount">
                                                $<asp:Literal ID="lprice" runat="server"></asp:Literal>&nbsp;x&nbsp;<span id="totalComboSet"></span>組
                                                <div class="empty-btn">
                                                </div>
                                            </div>
                                            <div class="clearfix">
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="item-list">
                                    <div class="form-unit end-unit rd-pay-pos-fix">
                                        <label for="" class="unit-label">
                                            總計</label>
                                        <div class="data-input">
                                            <p class="important shopping-list">
                                                $<span id="lsubtotal">0</span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-unit">
                                <label for="" class="unit-label">
                                    運費</label>
                                <div class="data-input">
                                    <p>
                                        $<span id="dCharge"><%=deliveryCharge.ToString("F0") %></span><span id="lbldc" runat="server"></span></p>
                                </div>
                            </div>
                            <div class="form-unit rd-pay-pos-fix">
                                <label for="" class="unit-label">
                                    總計</label>
                                <div class="data-input">
                                    <p class="important">
                                        $<span id="ltotal">0</span>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
            </div>
        </div>
        <div id="Rightarea" class="rd-payment-qma">
            <div class="side_block">
                <div class="side_block_content">
                    <div class="side_block_title for-payment">
                        訂購Q&amp;A
                    </div>
                    <p class="qa_title">
                        <a href="/ppon/newbieguide.aspx?s=2&q=20621" target="_blank">Q：我可以使用哪些方式付款？
                        </a>
                    </p>
                    <p class="qa_content">
                        A：您可透過以下方式付費：<br />
                        1. 17Life購物金<br />
                        2. 17Life紅利金<br />
                        3. 17Life折價券
                        <br />
                        4. PayEasy購物金<br />
                        5. 信用卡/Visa金融卡<br />
                        6. ATM付款
                    </p>
                    <p class="qa_title">
                        <a href="javascript:void(0)" target="_blank">Q：我買的好康能給別人用嗎?
                        </a>
                    </p>
                    <p class="qa_content">
                        A：可以。因為我們的配合商家僅認憑證不認人，但一張憑證只能使用一次。
                    </p>
                    <p class="qa_title">
                        <a href="/ppon/newbieguide.aspx?s=3&q=116" target="_blank">Q：已購買的好康如何使用？
                        </a>
                    </p>
                    <p class="qa_content">
                        A：可至會員專區 → <a href="https://www.17life.com/User/coupon_List.aspx" target="_blank">訂單/憑證</a>
                        中，下載或列印憑證後，持憑證至所提供之商家使用。<br />
                        您也可以<a href="https://www.17life.com/ppon/promo.aspx?cn=app" target="_blank">下載APP</a>，使用電子憑證。
                    </p>
                    <p class="qa_content line">
                        -------------------------- <a href="https://www.17life.com/Ppon/NewbieGuide.aspx"
                            target="_blank">更多常見Q&A >>></a>
                    </p>
                    <p class="qa_note">
                        特別說明：<br />
                        目前本站僅會先要求授權，不會直接進行請款， 當此優惠跨越集購門檻時，本站才會實際進行請款。若集購失敗，將不會有任何款項的請款，請放心!
                    </p>
                </div>
            </div>
            <uc3:Paragraph ID="p165" runat="server" />
        </div>
    </asp:Panel>
</asp:Content>
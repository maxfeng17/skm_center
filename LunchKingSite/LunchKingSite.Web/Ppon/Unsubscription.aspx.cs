﻿using System;
using System.Security.Cryptography;
using System.Text;
using System.Web.UI;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;

namespace LunchKingSite.Web.Ppon
{
    public partial class Unsubscription : System.Web.UI.Page
    {   
        private IPponProvider pp;
        protected static ISysConfProvider config;
        private Subscription ss;
        private string Email = string.Empty;
        private int MemberCity = -1;
        private string City = "城市";
        private string Code = string.Empty;
        protected void PreUnsubscription()
        {
            pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            config = ProviderFactory.Instance().GetConfig();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            unsubscribeArea.Visible = true;
            unsubscribeOK.Visible = false;
            if(!IsPostBack)
            {   
                if(ChkQueryString(0))
                {
                    GetQuerystringParams();
                    ss = pp.SubscriptionGet(Email, PponCityGroup.DefaultPponCityGroup.GetPponCityByCityId(MemberCity).CategoryId);
                    ltlCityArea.Text = City;
                    ltlUrl.Text = @"<input type=""button"" class=""btn btn-large btn-primary"" value=""我後悔了~帶我去看今日好康吧!"" onclick=""location.href='{url}'"" />".Replace("{url}", config.SiteUrl + "/ppon");
                    
                    if(ss.Status == 4)
                    {
                        ActionJS(GetJavaScriptMessage(4));
                    }
                }
            }
        }
        #region Event

        protected void unsubscribe_Click(object sender, EventArgs e)
        {   
            if(!ChkQueryString(1))
            {
                ActionJS(GetJavaScriptMessage(1));
            }
            else
            {
                if(!ChkCode())
                {
                    ActionJS(GetJavaScriptMessage(2));
                } 
                else
                {   
                    ltlCity.Text = City;
                    ltlUnSUrl.Text = @"<a class=""BTNunsubscribe03"" href=""{url}""></a>".Replace("{url}", config.SiteUrl + "/ppon");
                    ltlUnSUrl.Text = @"<input type=""button"" class=""btn btn-large btn-primary"" value=""瞧瞧今日好康"" onclick=""location.href='{url}'"" />".Replace("{url}", config.SiteUrl + "/ppon");
                    
                    ss = pp.SubscriptionGet(Email, PponCityGroup.DefaultPponCityGroup.GetPponCityByCityId(MemberCity).CategoryId);
                    pp.SubscriptionCancel(Convert.ToInt32(ss.Id));
                    unsubscribeArea.Visible = false;
                    unsubscribeOK.Visible = true;
                }
            }
        }
        #endregion

        #region Function
        private bool ChkQueryString(int iType)
        {
            string sJavascript = string.Empty;
            if (Request.QueryString["email"] == null ||
                Request.QueryString["mc"] == null ||
                Request.QueryString["code"] == null)
            {   
                ActionJS(GetJavaScriptMessage(iType));
                return false;
            }
            return true;
        }

        private bool ChkCode()
        {   
            GetQuerystringParams();
            return GetUnsubscriptionUrl(Email, MemberCity, Code);
        }

        private void GetQuerystringParams()
        {
            PreUnsubscription();
            Email = Request.QueryString["email"]?? string.Empty;
            MemberCity = (Request.QueryString["mc"] != null)
                ? Convert.ToInt32(Request.QueryString["mc"])
                : -1;
            Code = Request.QueryString["code"] ?? string.Empty;
            if (PponCityGroup.DefaultPponCityGroup.GetPponCityByCityId(MemberCity) != null)
            {
                City = PponCityGroup.DefaultPponCityGroup.GetPponCityByCityId(MemberCity).CityName;
                City = (string.IsNullOrEmpty(City)) ? "城市" : ((City == I18N.Phrase.PponCityAllCountry) ? "宅配" : City);
            }

        }

        /// <summary>
        /// 0:第一次進來的判斷
        /// 1:按下確定取消，QueryString不完整的判斷訊息
        /// 2:按下確定取消，code不符合之訊息
        /// </summary>
        /// <param name="iType"></param>
        /// <returns></returns>
        private string GetJavaScriptMessage(int iType)
        {
            string sUrlPpon = "http://" + Request.Url.Authority + "/ppon/";
            string sMsg = string.Empty;
            switch (iType)
            {
                case 0:
                    sMsg = @"
                        window.onload = function(){
                            alert(""這不是您的電子郵件哦"");
                            document.location.href="""+ sUrlPpon + @""";
                            return false;
                        }";
                    break;
                case 1:
                    sMsg = @"
                        var beforeAction1 = window.onload || function () {};
                        window.onload = function(){
                            alert(""這不是您的電子郵件哦"");
                            beforeAction1();
                            return false;
                        }";
                    break;
                case 2:
                    sMsg = @"
                        var beforeAction2 = window.onload || function () {};
                        window.onload = function(){
                            alert(""請經由您的電子郵件取消訂閱"");
                            beforeAction2();
                            return false;
                        }";
                    break;
                case 4:
                    sMsg = @"
                        window.onload = function(){
                            alert(""本帳號已經取消訂閱嘍!!"");
                            document.location.href="""+ sUrlPpon + @""";
                            return false;
                        }";
                    break;
            }
            return sMsg;
        }

        public void ActionJS(string JavascriptString)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "SubcriptionJS", JavascriptString, true);
        }

        public bool GetUnsubscriptionUrl(string Mail, int City, string Code)
        {
            string theCode = City.ToString() + Mail + "17Life_Ppon";
            MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
            byte[] b = md5.ComputeHash(Encoding.UTF8.GetBytes(theCode));

            return (Code == BitConverter.ToString(b).Replace("-", string.Empty)) ? true : false;
        }
        #endregion

        protected void btnRedirectDefault_Click(object sender, EventArgs e)
        {

        }
    }
}
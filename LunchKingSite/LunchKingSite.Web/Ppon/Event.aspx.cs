﻿using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;
using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LunchKingSite.Web.Ppon
{
    public partial class Event : BasePage, IEventView
    {
        public event EventHandler<DataEventArgs<EventEmailList>> EmailEnter;

        public event EventHandler Reload;

        #region props

        private EventPresenter _presenter;

        public EventPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        public string Url
        {
            get
            {
                if (Page.RouteData.Values["url"] != null)
                {
                    return Page.RouteData.Values["url"].ToString();
                }
                else if (!string.IsNullOrEmpty(Request.QueryString["url"]))
                {
                    return Request.QueryString["url"];
                }
                else
                {
                    return null;
                }
            }
        }

        public string eid
        {
            get
            {
                if (Session["EventGUID"] == null)
                {
                    return null;
                }
                else
                {
                    return Session["EventGUID"].ToString();
                }
            }
            set
            {
                Session["EventGUID"] = value;
            }
        }

        public ISysConfProvider ServerConfig { get; set; }

        public int EventType { get; set; }

        public string UserName
        {
            get
            {
                return User.Identity.Name;
            }
        }

        public string FbShareName { get; set; }

        public string FbShareUrl { get; set; }

        public string FbShareDesc { get; set; }

        public string FbShareImg { get; set; }

        public string ButtonLink { get; set; }

        public string ShortUrl { get; set; }

        public string EmailImg { get; set; }

        public string CPA { get; set; }

        public bool EmailSuccess { get; set; }

        public bool ShowEmail { get; set; }

        public bool ShowRef { get; set; }

        public bool ShowLogin { get; set; }

        public bool InTime { get; set; }

        public string SeoKeyword
        {
            get;
            set;
        }
        public string SeoDescription
        {
            get;
            set;
        }

        #endregion props

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                this.Presenter.OnViewInitialized();
            }
            this.Presenter.OnViewLoaded();
            Page.MetaDescription = string.IsNullOrEmpty(SeoDescription) ? Page.MetaDescription : SeoDescription;
            Page.MetaKeywords = string.IsNullOrEmpty(SeoKeyword) ? Page.MetaKeywords : SeoKeyword;
        }

        public void FillForm(EventContent ec)
        {
            if (ec.Url == null || ec.Enable != 1)
            {
                ReturnDefault();
                return;
            }

            if (ec.StartTime < DateTime.Now && ec.EndTime > DateTime.Now)
            {
                InTime = true;
            }
            else
            {
                InTime = false;
                TimeOut();
            }
            Session["EventGUID"] = ec.Guid;
            eid = ec.Guid.ToString();
            HFEmail.Value = null;

            #region 主題文字

            if (!string.IsNullOrEmpty(ec.TextImg))
            {
                HLtxt.ImageUrl = ec.TextImg;
                if (!string.IsNullOrEmpty(ec.TextLink))
                {
                    HLtxt.NavigateUrl = ec.TextLink;
                }
            }
            else
            {
                HLtxt.Visible = false;
            }

            #endregion 主題文字

            #region 主視覺

            if (!string.IsNullOrEmpty(ec.ViewImg))
            {
                HLview.ImageUrl = ec.ViewImg;
                if (!string.IsNullOrEmpty(ec.ViewLink))
                {
                    HLview.NavigateUrl = ec.ViewLink;
                }
            }
            else
            {
                HLtxt.Visible = false;
            }

            #endregion 主視覺

            #region 活動button

            if (!string.IsNullOrEmpty(ec.ButtonImg))
            {
                btnImg.ImageUrl = ec.ButtonImg;
                if (!string.IsNullOrEmpty(ec.ButtomLink))
                {
                    ButtonLink = ec.ButtomLink;
                    if (ec.Type == 1 && InTime)
                    {
                        LBbtn.Enabled = true;
                    }
                }
            }
            else
            {
                btnImg.Visible = false;
            }

            #endregion 活動button

            #region Facebook

            string FBUrl = ServerConfig.SiteUrl + "/event/" + ec.Url;
            if (ec.FbLikeEnable == 1)
            {
                LBlikeTxt.Text = ec.FbLikeText;
                LBlike.Text = @"<fb:like href='http://www.facebook.com/17life.com.tw' send='false' layout='button_count' width='90' show_faces='false'></fb:like>";
            }
            else
            {
                LBlikeTxt.Visible = false;
                LBlike.Visible = false;
            }

            if (ec.FbShareEnable == 1)
            {
                LBshareTxt.Text = ec.FbShareText;
            }
            else
            {
                LBshareTxt.Visible = false;
                ImgFBShare.Visible = false;
            }

            FbShareName = ec.FbShareCaption;
            FbShareUrl = (string.IsNullOrEmpty(ec.FbEventUrl)) ? FBUrl : ec.FbEventUrl;
            FbShareImg = ec.FbShareImg;
            FbShareDesc = ec.FbShareDesc;

            if (ec.FbSendEnable == 1)
            {
                LBsendTxt.Text = ec.FbSendText;
                if (string.IsNullOrEmpty(ec.FbEventUrl))
                {
                    LBsend.Text = @"<fb:send href='" + FBUrl + @"'></fb:send>";
                }
                else
                {
                    LBsend.Text = @"<fb:send href='" + ec.FbEventUrl + @"'></fb:send>";
                }
            }
            else
            {
                LBsendTxt.Visible = false;
                LBsend.Visible = false;
            }

            liF.Text = "<meta property='fb:app_id' content='" + ProviderFactory.Instance().GetConfig().FacebookApplicationId + "'/>";
            string ogTitle = ec.FbShareCaption.TrimToMaxLength(60, "...");
            liT.Text = "<meta property='og:title' content='" + HttpUtility.HtmlEncode(ogTitle) + "'/>";
            liOI.Text = "<meta property='og:image' content='" + (String.IsNullOrEmpty(ec.FbShareImg) ? string.Empty : ec.FbShareImg) + "' />";
            liD.Text = "<meta property='og:description' content='" + ec.FbShareDesc + "' />";
            liI.Text = "<link rel='image_src' href='" + (String.IsNullOrEmpty(ec.FbShareImg) ? string.Empty : ec.FbShareImg) + "' />";

            #endregion Facebook

            if (ec.RewardEnable == 1)
            {
                LBreward.Visible = true;
            }

            if (ec.Type == 2)
            {
                imCaptcha.Visible = true;
            }

            if (ec.Type == 3)
            {
                CPA = ec.CPAurl;
                if (!ShowRef)
                {
                    LBbtn.Enabled = true;
                }
            }

            EventType = Convert.ToInt16(ec.Type);
            EmailImg = ec.EmailImg;
            if (!String.IsNullOrEmpty(ec.RewardList))
            {
                RewardDiv(ec.RewardList);   //email list
            }

            eid = ec.Guid.ToString();

            LBrule.Text = HttpUtility.HtmlDecode(ec.EventRule);
        }

        public void ReturnDefault()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('無此活動');window.location.href='/ppon/default.aspx';", true);
        }

        public void TimeOut()
        {
            LBbtn.Enabled = false;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('此活動尚未開始或已經結束');", true);
        }

        private void RewardDiv(string list)
        {
            string[] ar = list.Split(',');
            for (int i = 0; i < ar.Length; i++)
            {
                switch (i % 3)
                {
                    case 1:
                        LBEmailList1.Text += @"<div class='reward'>" + MakeStar(ar[i]) + @"</div>";
                        break;

                    case 2:
                        LBEmailList2.Text += @"<div class='reward'>" + MakeStar(ar[i]) + @"</div>";
                        break;

                    case 0:
                        LBEmailList3.Text += @"<div class='reward'>" + MakeStar(ar[i]) + @"</div>";
                        break;
                }
            }
        }

        protected void btnEmail_Click(object sender, CommandEventArgs e)
        {
            if (HFCapt.Value == Session["Captcha"].ToString())
            {
                if (!string.IsNullOrEmpty(HFEmail.Value.Trim()) && Regex.IsMatch(HFEmail.Value.Trim(), @"^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$"))
                {
                    EventEmailList el = new EventEmailList();
                    el.Email = HFEmail.Value.Trim();
                    el.Eid = eid;
                    el.Creattime = DateTime.Now;
                    if (EmailEnter != null)
                    {
                        EmailEnter(this, new DataEventArgs<EventEmailList>(el));
                    }

                    TBEmail.Text = (EmailSuccess) ? "輸入成功" : "您已參加過本活動";
                }
                else
                {
                    TBEmail.Text = @"請輸入正確的Email";
                    TBEmail.ForeColor = System.Drawing.Color.Red;
                }
            }
            else
            {
                TBCapt.Text = @"請輸入正確的認證碼";
                TBCapt.ForeColor = System.Drawing.Color.Red;
            }

            ReloadDeal();
            btnEmail.Enabled = (EmailSuccess) ? false : true;
            ShowEmail = true;
        }

        #region 邀請活動

        protected void btnLB_Click(object sender, CommandEventArgs e)
        {
            ReloadDeal();
            if (EventType == 1)
            {
                Response.Redirect(ButtonLink);
            }
            else if (EventType == 3)
            {
                CheckLogin();
            }
        }

        public void CheckLogin()
        {
            if (Session[LkSiteSession.ExternalMemberId.ToString()] == null)
            {
                pl.Visible = true;
            }
            else
            {
                if (Page.User != null && Page.User.Identity.IsAuthenticated)
                {
                    string RefUrl = GetShareText();
                    if ((!string.IsNullOrEmpty(CPA) && RefUrl != "連結產生失敗，請重登後再試"))
                    {
                        RefUrl += "?rsrc=" + CPA;
                    }

                    FbShareUrl = RefUrl;
                    hhp.NavigateUrl = "javascript: void(window.location.replace('http://www.plurk.com/?qualifier=shares&status=' .concat(encodeURIComponent('" + RefUrl + "')) .concat(' (') .concat(encodeURIComponent('" + FbShareName + "')) .concat(')')));";

                    tbx_ReferralShare.Text = RefUrl;
                }
                ShowRef = true;
            }
        }

        protected string GetShareText()
        {
            ExternalMemberInfo emi = Session[LkSiteSession.ExternalMemberId.ToString()] as ExternalMemberInfo;
            EventShorturl es = new EventShorturl();
            if (emi != null)
            {
                es.Eid = new Guid(eid);
                es.MemberSource = emi[0].Source.ToString();
                es.MemberKey = emi[0].ExternalId.ToString();
                string origUrl = WebUtility.GetSiteRoot() + "/Event/EventRedirection.aspx?eid=" + eid + "," +
                                    (int)emi[0].Source + "|" + emi[0].ExternalId;
                return WebUtility.RequestShortUrl(origUrl);
            }
            else
            {
                return "連結產生失敗，請稍後再試";
            }
        }

        #endregion 邀請活動

        private void ReloadDeal()
        {
            if (Reload != null)
            {
                Reload(this, null);
            }
        }

        #region Tools

        private string MakeStar(string email)
        {
            string result = string.Empty;
            string[] ar = email.Split('@');
            int len = ar[0].Length;
            if (ar.Length > 1)
            {
                if (len > 1)
                {
                    result = ar[0].Substring(0, len - len / 2) + StrDup(len / 2, "*");
                }
                else
                {
                    result = "*";
                }
                return result + "@" + ar[1];
            }
            else
            {
                return string.Empty;
            }
        }

        private static string StrDup(int _number, string _text)
        {
            StringBuilder sb_value = new StringBuilder();
            for (int i = 0; i < _number; i++)
            {
                sb_value.Append(_text);
            }

            return sb_value.ToString();
        }

        public void DoClear()
        {
            Session.Contents.Remove("EventGUID");
        }

        #endregion Tools
    }
}
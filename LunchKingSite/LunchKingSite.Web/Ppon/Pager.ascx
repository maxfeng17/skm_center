<%@ Control Language="C#" AutoEventWireup="true" Codebehind="Pager.ascx.cs" Inherits="LunchKingSite.Web.Ppon.Pager" %>
<table class="Pagination" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td width="200" height="20">
            <a href="#" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('<%=btnFirst.ClientID %>','','../Themes/default/images/17Life/G2/page2_01.png',1)">
                <asp:ImageButton ID="btnFirst" OnClick="pager_Click" runat="server" ImageUrl="~/Themes/default/images/17Life/G2/page_01.png"
                    Text="<< 第一頁" CommandArgument="First"></asp:ImageButton></a>
            <asp:LinkButton ID="lFirst" CssClass="ablack4" Font-Bold="true" runat="server" CommandArgument="First"
                OnClick="link_Click" Visible="false">第一頁</asp:LinkButton>
        </td>
        <td width="160" height="20">
            <a href="#" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('<%=btnPrev.ClientID %>','','../Themes/default/images/17Life/G2/page2_02.png',1)">
                <asp:ImageButton ID="btnPrev" OnClick="pager_Click" runat="server" ImageUrl="~/Themes/default/images/17Life/G2/page_02.png"
                    Text="< 上一頁" CommandArgument="Prev"></asp:ImageButton></a>
            <asp:LinkButton ID="lPrev" CssClass="ablack4" Font-Bold="true" runat="server" CommandArgument="Prev"
                OnClick="link_Click" Visible="false">上一頁</asp:LinkButton>
        </td>
        <td width="160" height="20">
            <a href="#" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('<%=btnNext.ClientID %>','','../Themes/default/images/17Life/G2/page2_03.png',1)">
                <asp:ImageButton ID="btnNext" OnClick="pager_Click" runat="server" Text="下一頁 >" ImageUrl="~/Themes/default/images/17Life/G2/page_03.png"
                    CommandArgument="Next"></asp:ImageButton></a>
            <asp:LinkButton ID="lNext" CssClass="ablack4" Font-Bold="true" runat="server" CommandArgument="Next"
                OnClick="link_Click" Visible="false">下一頁</asp:LinkButton>
        </td>
        <td width="200" height="20">
            <a href="#" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('<%=btnLast.ClientID %>','','../Themes/default/images/17Life/G2/page2_04.png',1)">
                <asp:ImageButton ID="btnLast" OnClick="pager_Click" runat="server" Text="最後頁 >>"
                    ImageUrl="~/Themes/default/images/17Life/G2/page_04.png" CommandArgument="Last">
                </asp:ImageButton></a>
            <asp:LinkButton ID="lLast" CssClass="ablack4" Font-Bold="true" runat="server" CommandArgument="Last"
                OnClick="link_Click" Visible="false">最後頁</asp:LinkButton>
        </td>
        <td width="120" height="20" align="center">
            頁數
        </td>
        <td width="110" height="23">
            <asp:DropDownList ID="ddlPages" runat="server" OnSelectedIndexChanged="ddlPages_SelectedIndexChanged"
                AutoPostBack="True">
            </asp:DropDownList>
        </td>
        <td width="430" height="23">
            <asp:Label ID="lblPageCount" Font-Bold="true" runat="server"></asp:Label>
            &nbsp;&nbsp;(<asp:Label ID="lrc" runat="server" />
            筆資料)
            <asp:Label ID="lblRecentCount" runat="server" Visible="false" />
        </td>
    </tr>
</table>






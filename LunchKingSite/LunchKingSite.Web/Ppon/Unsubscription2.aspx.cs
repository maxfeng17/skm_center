﻿using LunchKingSite.Core;
using LunchKingSite.Core.Component;

namespace LunchKingSite.Web.Ppon
{
    public partial class Unsubscription2 : System.Web.UI.Page
    {
        IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();

        public void Page_Load()
        {
            string m = Helper.DecryptEmail(Request.QueryString["m"]);
            if (string.IsNullOrEmpty(m) == false)
            {
                //既然使用者要求，就全刪了吧
                foreach (var subscription in pp.SubscriptionGetList(m))
                {
                    pp.SubscriptionDelete(subscription.Id);
                }
            }
        }
    }
}
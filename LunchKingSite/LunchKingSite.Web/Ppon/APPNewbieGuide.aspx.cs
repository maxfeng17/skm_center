﻿using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using System;
using System.Linq;
using System.Web;
using LunchKingSite.WebLib.Component;
using LunchKingSite.BizLogic.Facade;
using System.Text;

namespace LunchKingSite.Web.Ppon
{
    public partial class APPNewbieGuide : System.Web.UI.Page
    {
        public ISysConfProvider SystemConfig
        {
            get
            {
                return ProviderFactory.Instance().GetConfig();
            }
        }

        private string _returnUrl;

        public string ReturnUrl
        {
            get
            {
                return ResolveUrl(string.IsNullOrEmpty(_returnUrl) ? "~/user/sso.aspx?v=p" : _returnUrl);
            }
            set
            {
                _returnUrl = value;
            }
        }

        public int UserId
        {
            get
            {
                PponPrincipal user = this.User as PponPrincipal;

                if (user != null)
                {
                    return user.Identity.Id;
                }
                else
                {
                    return MemberFacade.GetUniqueId(this.User.Identity.Name);
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            setFAQ();
        }
        
        private void setFAQ()
        {
            FaqCollection faqs = ProviderFactory.Instance().GetProvider<IPponProvider>().FaqCollectionGet();
            string links = "<ul>";
            int index = 0;
            foreach (Faq sectionItem in faqs.Where(x => x.Pid == 0 && x.Mark != "H").OrderBy(x => x.Mark).ThenBy(x => x.Sequence))
            {
                ContentSection.Text += "<div id='UCT" + sectionItem.Id + "' class='sh'><h1 class='title'>" + sectionItem.Contents + "</h1>";
                links += "<li id='li" + sectionItem.Id + "'" + " onclick='$(\".sh\").hide();$(\"#UCT" + sectionItem.Id +
                    "\").show();$(\"li.on\").removeClass(\"on\");$(this).addClass(\"on\");$(\".faqArea\").slideToggle();$(\".faq\").addClass(\"rdl-faqarrow-on\");'>" + sectionItem.Contents + "</li>";
                int listCount = 1;
                foreach (Faq questionItem in faqs.Where(x => x.Pid == sectionItem.Id).OrderBy(x => x.Sequence))
                {
                    ContentSection.Text += "<a name='anc" + questionItem.Id + "'></a><div class='fq-questionBOX ubx" +
                        questionItem.Id + "'><div class='fq-qtitle'><div class='qt-box'><span title='?s=" + sectionItem.Id +
                        "&q=" + questionItem.Id + "'>" + listCount + "</span>. " + questionItem.Contents +
                        "</div><span class='icon-chevron-faq rdl-iconshow'></span></div><div class='fq-info'>";

                    Faq answerItem = faqs.Where(x => x.Pid == questionItem.Id).DefaultIfEmpty().First();

                    if (answerItem.Contents.Contains("</a>"))
                    {
                        ContentSection.Text += (answerItem == null ? string.Empty : NoHTMLButKeepImage(answerItem.Contents)) + "</div></div>";
                    }
                    else
                    {
                        ContentSection.Text += (answerItem == null ? string.Empty : answerItem.Contents) + "</div></div>";
                    }
                    
                    listCount++;
                }
                ContentSection.Text += "</div>";
                index++;
            }
            LinkSection.Text = LinkSectionM.Text = links + "</ul>";
        }

        public static string NoHTML(string htmlstring)
        {
            //檢查輸入的資料，若為空白或null，直接回傳空字串
            if (string.IsNullOrWhiteSpace(htmlstring))
            {
                return string.Empty;
            }
            int startIndex = htmlstring.IndexOf("<");
            int endIndex = htmlstring.IndexOf(">");
            while (startIndex >= 0 && endIndex > startIndex)
            {
                int length = endIndex - startIndex + 1;
                htmlstring = htmlstring.Remove(startIndex, length);
                startIndex = htmlstring.IndexOf("<");
                endIndex = htmlstring.IndexOf(">");
            }
            htmlstring = htmlstring.Replace("\t", "");
            htmlstring = htmlstring.Replace("\r\n\r\n", "\r\n");
            if (htmlstring.IndexOf("\r\n") == 0)
            {
                htmlstring = htmlstring.Remove(0, 2);
            }
            //轉換特殊字元
            htmlstring = HttpUtility.HtmlDecode(htmlstring);

            return htmlstring;
        }

        public static string NoHTMLButKeepImage(string htmlstring)
        {
            //檢查輸入的資料，若為空白或null，直接回傳空字串
            if (string.IsNullOrWhiteSpace(htmlstring))
            {
                return string.Empty;
            }

            StringBuilder sb = new StringBuilder();
            StringBuilder sbTag = new StringBuilder();
            bool skip = false;
            
            for (int i = 0; i < htmlstring.Length; i++)
            {
                char ch = htmlstring[i];
                if (ch == '<' && skip == false)
                {
                    skip = true;
                    sbTag.Clear();
                }
                else if (ch == '>' && skip)
                {
                    skip = false;
                    string tagHtml = sbTag.ToString();
                    if (string.IsNullOrEmpty(tagHtml) == false && tagHtml.Length >= 3 && tagHtml.StartsWith("img", StringComparison.OrdinalIgnoreCase))
                    {
                        sb.Append("<");
                        sb.Append(tagHtml);
                        sb.Append(">");
                    }
                    if (string.IsNullOrEmpty(tagHtml) == false && tagHtml.Length >= 4 && tagHtml.StartsWith("/div", StringComparison.OrdinalIgnoreCase))
                    {
                        sb.Append("<br />");
                    }
                }
                else if (skip == false)
                {
                    sb.Append(ch);
                }
                else
                {
                    sbTag.Append(ch);                    
                }
            }

            htmlstring = sb.ToString();

            htmlstring = htmlstring.Replace("\t", "");
            htmlstring = htmlstring.Replace("\r\n\r\n", "\r\n");
            if (htmlstring.IndexOf("\r\n") == 0)
            {
                htmlstring = htmlstring.Remove(0, 2);
            }
            //轉換特殊字元
            htmlstring = HttpUtility.HtmlDecode(htmlstring);

            return htmlstring;
        }

    }
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Ppon/Ppon.Master" AutoEventWireup="true" CodeBehind="AppLimitedEdition.aspx.cs" Inherits="LunchKingSite.Web.Ppon.AppLimitedEdition" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphPponMeta" runat="server">
    <meta property="fb:app_id" content="<%=SystemConfig.FacebookApplicationId%>">
    <meta property="og:type" content="website" />
    <meta property="og:title" content="<%=OgTitle%>" />
    <meta property="og:url" content="<%=OgUrl%>" />
    <meta property="og:site_name" content="17Life" />
    <meta property="og:description" content="<%=OgDescription%>" />
    <meta property="og:image" content="<%=OgImage%>" />
    <meta name="apple-itunes-app" content="app-id=<%=SystemConfig.iOSAppId%>">
    <meta name="google-play-app" content="app-id=<%=SystemConfig.AndroidAppId%>">
    <link rel="image_src" href="<%=LinkImageSrc%>" />
    <link rel="canonical" href="<%=LinkCanonicalUrl%>" />      
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="SSC" runat="server">
    <link href='<%= ResolveUrl("../themes/PCweb/css/RDL-L.css") %>' rel="stylesheet" type="text/css" />
    <link href='<%= ResolveUrl("../themes/PCweb/css/RDL-M.css") %>' rel="stylesheet" type="text/css" />
    <link href='<%= ResolveUrl("../themes/PCweb/css/RDL-S.css") %>' rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MC" runat="server">
    <div class="cnt-box">
        <div class="cnt-title">
            <h1>此優惠只限在 APP 購買唷</h1>
            <hr class="header_hr rd-payment-width">
        </div>
        <div class="cnt-zone clearfix">
            <div id="MainPic" class="picforsdeal">
                <div class="slider nivoSlider default-pic">
                    <asp:Repeater ID="rpt_tp" runat="server" Visible="true">
                        <ItemTemplate>
                            <img src="<%# (string)Container.DataItem%>" alt="<%# PicAlt%>" />
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
                <div class="activeLogo alo-l">
                    <asp:Literal ID="lit_DealPromoImageMainContent" runat="server"></asp:Literal>
                </div>
            </div>
            <div class="app-content">
                <div class="app-pdname">
                    <%=AppTitle%>
                </div>
                <div class="app-download">
                    <div class="download-qrcode">
                    </div>
                    <div class="download-appstone">
                    </div>
                    <div class="download-googleplay">
                    </div>
                    <p>快速掃描專屬優惠 QRcode 進行購買。</p>
                </div>
            </div>
        </div>
        <div class="app-download-box">
            <h3>快來下載免費APP，搶購專屬優惠吧！</h3>
            <div class="mms-box HeaderPhone">
                <div class="mms-title">免費傳送下載簡訊</div>
                <input id="textfield" name="textfield" placeholder="請輸入接收簡訊的手機號碼" size="25" type="text" maxlength="10">
                <input class="phoneBTN" type="submit" value="">
            </div>
            <a href="https://itunes.apple.com/tw/app/id543439591?mt=8" target="_blank" class="btn-ios"></a>
            <a href="https://play.google.com/store/apps/details?id=com.uranus.e7plife&amp;feature=search_result#?t=W251bGwsMSwxLDEsImNvbS51cmFudXMuZTdwbGlmZSJd" target="_blank" class="btn-android"></a>
        </div>
    </div>
    <script type="text/javascript">
        function openAtAPP(d) {
            if (d == "ios") {
                var start = new Date();
                window.setTimeout(function () {
                    if (new Date() - start > 2000) {
                        return;
                    }
                    document.location = "https://itunes.apple.com/tw/app/17life/id543439591";
                }, 1000);
                document.location = "open17life://www.17life.com/deal/<%=BusinessHourGuid%>";
            }
            else if (d == "android") {
                document.location = "<%=string.Format("{0}/Ppon/OpenApp.aspx?bid={1}", SystemConfig.SiteUrl, BusinessHourGuid)%>";
            }
        }

        $(document).ready(function() {
            // 行動裝置(不含iPad)出現建議下載app
            var device = '';
            if (navigator.userAgent.match(<%=string.Format("/{0}/i", SystemConfig.AndroidUserAgent)%>)) {
                device = 'android';
            } else if (navigator.userAgent.match(<%=string.Format("/{0}/i", SystemConfig.iOSUserAgent)%>)) {
                if (navigator.userAgent.indexOf('iPad') < 0) {
                    device = 'ios';
                }
            }

            if (device != '') {
                if (confirm("是否於17Life APP開啟連結。")) {
                    //嘗試開始APP
                    openAtAPP(device);
                }
            }
        });
    </script>
</asp:Content>

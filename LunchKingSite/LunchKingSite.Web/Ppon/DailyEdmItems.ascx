﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DailyEdmItems.ascx.cs"
    Inherits="LunchKingSite.Web.Ppon.DailyEdmItems" %>
<%@ Import Namespace="LunchKingSite.DataOrm" %>
<%@ Import Namespace="LunchKingSite.Core" %>
<asp:Panel ID="pan_Title" runat="server" Visible="false">
    <table>
        <tr>
            <td style="<%=GetTitleTdColor()%>">
                ▌<asp:Label ID="lab_AreaName" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="height: 5px">
            </td>
        </tr>
    </table>
</asp:Panel>
<asp:Panel ID="pan_Single" runat="server" Visible="false">
    <asp:Repeater ID="rpt_Single" runat="server" OnItemDataBound="rptDataBound">
        <ItemTemplate>
            <table cellpadding="0" cellspacing="0" align="center" style="background: #FFF; padding: 10px;
                border: 1px solid #DDD; width: 740px;">
                <tr>
                    <td rowspan="5" style="width: 400px; height: 223px">
                        <a href="<%# SiteUrl+"/"+((ViewPponDeal)Container.DataItem).BusinessHourGuid+Cpa %>"
                            target="_blank">
                            <asp:Image ID="img_Pic" runat="server" Width="400" Height="223" BorderWidth="0" AlternateText='<%# Helper.GetMaxString( ((ViewPponDeal)Container.DataItem).ItemName,28,"...")%>' />
                        </a>
                    </td>
                    <td rowspan="5" style="border-right: 1px solid #DDD; width: 10px">
                    </td>
                    <td rowspan="5" style="width: 10px">
                    </td>
                    <td colspan="3">
                    </td>
                </tr>
                <tr>
                    <td colspan="3" style="width: 320px; height: 25px">
                    </td>
                </tr>
                <tr>
                    <td colspan="3" style="width: 320px; height: 50px">
                        <div style="font-size: 16px; height: 71px; word-break: break-all; overflow: hidden;
                            text-align: left;">
                            <a href="<%# SiteUrl+"/"+((ViewPponDeal)Container.DataItem).BusinessHourGuid+Cpa %>"
                                target="_blank" style="color: #000; text-decoration: none;">
                                <%# Helper.GetMaxString(((ViewPponDeal)Container.DataItem).EventName,53,"..")%></a>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td align="center" style="<%=GetDiscountBorderColor()%>">
                        <asp:Label ID="lab_Discount" runat="server"></asp:Label>
                    </td>
                    <td style="font-size: 32px;">
                        &nbsp;$
                        <%# ((ViewPponDeal)Container.DataItem).ItemPrice.ToString("F0") %>
                    </td>
                    <td style="color: #999">
                        &nbsp;&nbsp;&nbsp;原價：<span style="text-decoration: line-through;">$
                            <%# ((ViewPponDeal)Container.DataItem).ItemOrigPrice.ToString("F0") %></span>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" align="center" style="width: 320px; height: 90px">
                        <table cellpadding="0" cellspacing="0" style="width: 100px">
                            <tr>
                                <td align="center" style="<%=GetBuyTdColor()%>">
                                    <a href="<%# SiteUrl+"/"+((ViewPponDeal)Container.DataItem).BusinessHourGuid+Cpa %>"
                                        target="_blank" style="color: #FFF; font-size: 16px;">瞧瞧去</a>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </ItemTemplate>
    </asp:Repeater>
</asp:Panel>
<asp:Panel ID="pan_Double" runat="server" Visible="false">
    <asp:Repeater ID="rpt_Double" runat="server" OnItemDataBound="rptDataBound">
        <HeaderTemplate>
            <table border="0" cellspacing="0" cellpadding="0" align="center" style="width: 740px">
                <tr>
        </HeaderTemplate>
        <ItemTemplate>
            <td align="center" style="background: #FFF; border: 1px solid #DDD; width: 365px;">
                <table border="0" cellspacing="0" cellpadding="0" align="center" style='<%# (((ViewPponDeal)Container.DataItem).BusinessHourGuid==Guid.Empty?"visibility:hidden;": "width: 345px;")+"height: 305px;padding-top:10px;padding-bottom:10px;"%>'>
                    <tr>
                        <td colspan="4" align="center" style="width: 345px; height: 192px">
                            <a href="<%# SiteUrl+"/"+((ViewPponDeal)Container.DataItem).BusinessHourGuid+Cpa %>"
                                target="_blank">
                                <asp:Image ID="img_Pic" runat="server" Width="345" Height="192" BorderWidth="0" AlternateText='<%# Helper.GetMaxString( ((ViewPponDeal)Container.DataItem).ItemName,24,"...")%>' />
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" style="border-bottom: 1px dashed #CCC; width: 345px; text-align: left">
                            <div style="width: 345px; height: 50px; line-height: 25px; word-break: break-all;
                                overflow: hidden;">
                                <a href="<%# SiteUrl+"/"+((ViewPponDeal)Container.DataItem).BusinessHourGuid+Cpa %>"
                                    target="_blank" style="color: #000; text-decoration: none;">
                                    <%# Helper.GetMaxString(((ViewPponDeal)Container.DataItem).EventName,50,"..")%></a></div>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-top: 4px; padding-bottom: 4px;">
                        </td>
                    </tr>
                    <tr>
                        <td align="center" style="<%=GetDiscountBorderColor()%>">
                            <asp:Label ID="lab_Discount" runat="server"></asp:Label>
                        </td>
                        <td style="font-size: 24px; width: 215px">
                            &nbsp;$<%# ((ViewPponDeal)Container.DataItem).ItemPrice.ToString("F0") %>&nbsp;&nbsp;<span
                                style="text-decoration: line-through; color: #999; font-size: 13px;">$<%# ((ViewPponDeal)Container.DataItem).ItemOrigPrice.ToString("F0") %></span>
                        </td>
                        <td align="center" style="<%=GetBuyTdColor()%>">
                            <a href="<%# SiteUrl+"/"+((ViewPponDeal)Container.DataItem).BusinessHourGuid+Cpa %>"
                                target="_blank" style="color: #FFF; font-size: 12px;">瞧瞧去</a>
                        </td>
                    </tr>
                </table>
            </td>
            <%# (Container.ItemIndex+1)%2==0?string.Empty:"<td width='10'></td>" %>
        </ItemTemplate>
        <FooterTemplate>
            </tr>
            <tr>
                <td colspan="5" style="padding-top: 10px; padding-bottom: 10px;">
                </td>
            </tr>
            </table>
        </FooterTemplate>
    </asp:Repeater>
</asp:Panel>
<asp:Panel ID="pan_Triple" runat="server" Visible="false">
    <asp:Repeater ID="rpt_Triple" runat="server" OnItemDataBound="rptDataBound">
        <HeaderTemplate>
            <table border="0" cellspacing="0" cellpadding="0" align="center" style="width: 740px">
                <tr>
        </HeaderTemplate>
        <ItemTemplate>
            <td style="background: #FFF; border: 1px solid #DDD; width: 240px;">
                <table border="0" cellspacing="0" cellpadding="0" align="center" style='<%# (((ViewPponDeal)Container.DataItem).BusinessHourGuid==Guid.Empty?"visibility:hidden;": "width: 220px;")+"height:230px;padding-top:10px;padding-bottom:10px;"%>'>
                    <tr>
                        <td colspan="4" style="width: 220px; height: 122px">
                            <a href="<%# SiteUrl+"/"+((ViewPponDeal)Container.DataItem).BusinessHourGuid+Cpa %>"
                                target="_blank">
                                <asp:Image ID="img_Pic" runat="server" Width="220" Height="122" BorderWidth="0" AlternateText='<%# Helper.GetMaxString( ((ViewPponDeal)Container.DataItem).ItemName,14,"...")%>' />
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" align="left" style="border-bottom: 1px dashed #CCC; width: 220px;
                            height: 50px">
                            <div style="width: 220px; height: 40px; line-height: 20px; word-break: break-all;
                                overflow: hidden;">
                                <a href="<%# SiteUrl+"/"+((ViewPponDeal)Container.DataItem).BusinessHourGuid+Cpa %>"
                                    target="_blank" style="color: #000; text-decoration: none;">
                                    <%# Helper.GetMaxString(((ViewPponDeal)Container.DataItem).EventName,31,"..")%></a></div>
                        </td>
                    </tr>
                    <tr>
                        <td height="8" style="height: 8px">
                        </td>
                    </tr>
                    <tr>
                        <td align="center" style="<%=GetDiscountBorderColor()%>">
                            <asp:Label ID="lab_Discount" runat="server"></asp:Label>
                        </td>
                        <td style="font-size: 24px; width: 175px">
                            &nbsp;$<%# ((ViewPponDeal)Container.DataItem).ItemPrice.ToString("F0") %>&nbsp;<span
                                style="text-decoration: line-through; color: #999; font-size: 13px;">$<%# ((ViewPponDeal)Container.DataItem).ItemOrigPrice.ToString("F0") %></span>
                        </td>
                    </tr>
                </table>
            </td>
            <%# (Container.ItemIndex+1)%3==0?string.Empty:"<td width='10'></td>" %>
        </ItemTemplate>
        <FooterTemplate>
            </tr>
            <tr>
                <td colspan="5" style="padding-top: 10px; padding-bottom: 10px">
                </td>
            </tr>
            </table></FooterTemplate>
    </asp:Repeater>
</asp:Panel>
<asp:Panel ID="pan_PiinLife_1" runat="server" Visible="false">
    <asp:Repeater ID="rpt_PiinLife_1" runat="server" OnItemDataBound="rptPiinLifeDataBound">
        <HeaderTemplate>
            <table width="740" border="0" cellpadding="0" cellspacing="0" align="center" style="background: #FFF;
                border: 1px solid #DDD;">
                <tr>
                    <td height="30">
                    </td>
                </tr>
                <tr>
                    <td>
        </HeaderTemplate>
        <ItemTemplate>
            <table border="0" cellpadding="0" cellspacing="0" align="center" style="width: 650px">
                <tr>
                    <td colspan="2" style="width: 650px; height: 360px">
                        <a href="<%# SiteUrl + "/piinlife/HiDeal.aspx?did=" + ((HiDealDeal)Container.DataItem).Id+Cpa%>"
                            target="_blank">
                            <asp:Image ID="img_Pic" runat="server" Width="650" Height="360" BorderWidth="0" AlternateText='<%#  Helper.GetMaxString(((HiDealDeal)Container.DataItem).Name,47,"...")%>' />
                        </a>
                    </td>
                </tr>
                <tr>
                    <td style="width: 570px; height: 80px">
                        <div style="width: 550px; height: 30px; word-break: break-all; overflow: hidden;
                            font-size: 16px;">
                            <a href="<%# SiteUrl + "/piinlife/HiDeal.aspx?did=" + ((HiDealDeal)Container.DataItem).Id+Cpa%>"
                                target="_blank" style="color: #EC7d49; text-decoration: none;">
                                <%#  ((HiDealDeal)Container.DataItem).PromoShortDesc%></a></div>
                        <div style="width: 550px; height: 35px; word-break: break-all; overflow: hidden;">
                            <a href="<%# SiteUrl + "/piinlife/HiDeal.aspx?did=" + ((HiDealDeal)Container.DataItem).Id+Cpa%>"
                                target="_blank" style="color: black; text-decoration: none;">
                                <%#  ((HiDealDeal)Container.DataItem).PromoLongDesc%></a>
                        </div>
                    </td>
                    <td align="center" style="background-color: #2B1E13; font-size: 24px; width: 80px;
                        height: 80px">
                        <a href="<%# SiteUrl + "/piinlife/HiDeal.aspx?did=" + ((HiDealDeal)Container.DataItem).Id+Cpa%>"
                            target="_blank" style="color: white; text-decoration: none;">VIEW</a>
                    </td>
                </tr>
            </table>
        </ItemTemplate>
        <FooterTemplate>
            </td> </tr>
            <tr>
                <td style="padding-top: 15px; padding-bottom: 15px;">
                </td>
            </tr>
            </table>
            <br />
        </FooterTemplate>
    </asp:Repeater>
</asp:Panel>
<asp:Panel ID="pan_PiinLife_2" runat="server" Visible="false">
    <asp:Repeater ID="rpt_PiinLife_2" runat="server" OnItemDataBound="rptPiinLifeDataBound">
        <HeaderTemplate>
            <table width="740" border="0" cellspacing="0" cellpadding="0" align="center">
                <tr>
        </HeaderTemplate>
        <ItemTemplate>
            <td width="365" height="285" align="center" style="background: #FFF; border: 1px solid #DDD;">
                <table border="0" cellspacing="0" cellpadding="0" style="width: 315px">
                    <tr>
                        <td colspan="2" style="width: 315px; height: 174px">
                            <a href="<%# ResolveUrl("~/piinlife/HiDeal.aspx?did=") + ((HiDealDeal)Container.DataItem).Id%>"
                                target="_blank">
                                <asp:Image ID="img_Pic" runat="server" Width="315" Height="174" BorderWidth="0" AlternateText='<%#  Helper.GetMaxString(((HiDealDeal)Container.DataItem).Name,22,"...")%>' />
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 315px; height: 60px">
                            <div style="width: 315px; height: 25px; line-height: 25px; word-break: break-all;
                                overflow: hidden;">
                                <a href="<%# ResolveUrl("~/piinlife/HiDeal.aspx?did=") + ((HiDealDeal)Container.DataItem).Id%>"
                                    target="_blank" style="color: #EC7d49; text-decoration: none;">
                                    <%#  ((HiDealDeal)Container.DataItem).PromoShortDesc%></a></div>
                            <div style="width: 315px; height: 35px; word-break: break-all; overflow: hidden;">
                                <a href="<%# ResolveUrl("~/piinlife/HiDeal.aspx?did=") + ((HiDealDeal)Container.DataItem).Id%>"
                                    target="_blank" style="color: black; text-decoration: none;">
                                    <%#  ((HiDealDeal)Container.DataItem).PromoLongDesc%></a></div>
                        </td>
                    </tr>
                </table>
            </td>
            <%# (Container.ItemIndex+1)%2==0?string.Empty:"<td width='10'></td>" %>
        </ItemTemplate>
        <FooterTemplate>
            </tr>
            <tr>
                <td colspan="3" style="padding-top: 10px; padding-bottom: 10px">
                </td>
            </tr>
            </table>
        </FooterTemplate>
    </asp:Repeater>
</asp:Panel>
<asp:Literal ID="lit_Separate" runat="server"></asp:Literal>

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GetClientInfo.aspx.cs" Inherits="LunchKingSite.Web.Ppon.GetClientInfo" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <script>
        var browserwidth = window.screen.width;
        var browserheight = window.screen.height;
        //var browserwidth = window.innerWidth;
        //var browserheight = window.innerHeight;
        //console.log("browserwidth:"+browserwidth+";browserheight:"+browserheight);
        //console.log("browserwidth:"+window.innerWidth+";browserheight:"+window.innerHeight);
        var cookieName = 'ToMobile';
        var minWidth = browserwidth < browserheight ? browserwidth : browserheight;
        //alert(minWidth);
        var cookieValue = minWidth<<%=MobileMaxWidth%>?1:0;
        //alert(cookieValue);
        var myDate = new Date();
        var time = myDate.getTime();
        var expireTime = time + 864000;//十天
        myDate.setTime(expireTime);
        
        document.cookie = cookieName + "=" + cookieValue + ";expires=" + myDate + ";path=/";
        
        var path = "";
        if(location.search) {
            path = location.search;
        }

        if (cookieValue == 1)
        {
            window.location.href = "/m" + path;
        }
        else
        {         
            window.location.href = "/" + path;
        }

    </script>
</head>
<body>
    
</body>
</html>

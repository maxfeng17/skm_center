﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Ppon/Ppon.Master" AutoEventWireup="true"
    CodeBehind="FbActivities.aspx.cs" Inherits="LunchKingSite.Web.Ppon.FbActivities" %>

<%@ Register Src="~/UserControls/Paragraph.ascx" TagName="Paragraph" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MC" runat="server">
    <link href="../Themes/default/images/17Life/G1-8/17UG.css" rel="stylesheet" type="text/css" />
    <link href="../Themes/default/images/17Life/G1-8/FbActivities.css" rel="stylesheet" type="text/css" />
    <script type='text/javascript'>
        function shareToFB() {
            var appId = "<%=FacebookAppId%>";
            var name = document.location.href;
            var caption = "加入粉絲團 天天賺現金";
            var picture = "<%:WebUrl%>" + "/Themes/default/images/17Life/G1-8/FB_activities_logo.jpg"
            var desc = "活動期間即日起至2011/3/31，17Life粉絲團天天抽出17幸運粉絲贈送17Life好康購物金$100●17Life 吃喝玩樂應有盡有 天天三折起●";
            var url = "http://www.facebook.com/dialog/feed?app_id=" + appId + "&link=" + document.location.href +
            "&caption=" + caption + "&picture=" + picture +
            "&name=" + name + "&description=" + desc + "&redirect_uri=" + document.location.href;
            document.location.href = encodeURI(url);
            return false;
        }
    </script>
    <div id="yellow_area_Top">
        <img src="../Themes/default/images/17Life/G1-8/17p_Header_Bak_yellow_top.png" width="1014"
            height="12" />
    </div>
    <div id="yellow_content">
        <div id="yellow_content_in">
            <div id="FbConten">
                <div id="fbTop">
                </div>
                <div id="fbCenBg">
                    <div id="bottom">
                        <div id="FbArrow1">
                            <img src="../Themes/default/images/17Life/G1-8/fbArrow.gif" width="20" height="14" /></div>
                        <div id="Life_Fans">
                            <script type="text/javascript" src="//connect.facebook.net/zh_TW/sdk.js#xfbml=1"></script>
                            <fb:like href="http://www.facebook.com/17life.com.tw" layout="button_count" show_faces="false">
                            </fb:like>
                        </div>
                        <div id="FbArrow2">
                            <img src="../Themes/default/images/17Life/G1-8/fbArrow.gif" width="20" height="14" /></div>
                        <div id="Share_Friends">
                            <img src="../Themes/default/images/17Life/G1-8/fb_friend.png" width="97" height="22"
                                onclick="shareToFB();" style="cursor: pointer;" alt="Share to Facebook" />
                        </div>
                    </div>
                </div>
                <div id="fbText">
                    <div id="text">
                        <uc1:Paragraph ID="fbactwinner" runat="server" />
                    </div>
                </div>
                <div id="fbEnd">
                    <div id="fbText2">
                        <uc1:Paragraph ID="fbact" runat="server" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="yellow_area_down">
    </div>
</asp:Content>

﻿using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.I18N;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;
using System;
using System.Web;
using System.Web.UI;
using LunchKingSite.BizLogic.Component;

namespace LunchKingSite.Web.Ppon
{
    public partial class PponReferralRedirection : System.Web.UI.Page, IPponReferralRedirectionView
    {
        private static String CPA_CODE_FOR_NEW_MEMBER = "rsrc=17_test50"; //This is for marketing analysis
        private static ISysConfProvider cp = ProviderFactory.Instance().GetConfig();

        private PponReferralRedirectionPresenter _presenter;

        public PponReferralRedirectionPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        public string ReferenceId
        {
            get
            {
                string referenceId = string.Empty;
                if (!string.IsNullOrEmpty(Request.QueryString["bid"]) && Request.QueryString["bid"].Split(',').Length > 1)
                {
                    referenceId = Request.QueryString["bid"].Split(',')[1];
                }
                return referenceId;
            }
        }

        public string WebRoot
        {
            get
            {
                return cp.SiteUrl;
            }
        }

        public Guid BusinessHourId
        {
            get
            {
                Guid bGuid = Guid.Empty;

                if (!string.IsNullOrEmpty(Request.QueryString["bid"]) && Request.QueryString["bid"].Split(',').Length > 1 &&
                    !String.IsNullOrEmpty(Request.QueryString["bid"].Split(',')[0]))
                {
                    try
                    {
                        bGuid = new Guid(Request.QueryString["bid"].Split(',')[0]);
                    }
                    catch
                    {
                    }
                }

                return bGuid;
            }
        }

        public string UserName
        {
            get
            {
                return Page.User.Identity.Name;
            }
        }

        public string AndroidUserAgent
        {
            get
            {
                return "/" + cp.AndroidUserAgent + "/i";
            }
        }

        public string iOSUserAgent
        {
            get
            {
                return "/" + cp.iOSUserAgent + "/i";
            }
        }

        /// <summary>
        /// 設定一組Cookie到client端
        /// </summary>
        /// <param name="aCookie"></param>
        public bool SetCookie(String value, DateTime expireTime, LkSiteCookie cookieField)
        {
            HttpCookie referenceCookie = CookieManager.NewCookie(cookieField.ToString("g"));

            referenceCookie.Value = value;
            referenceCookie.Expires = expireTime;
            //設定cookie值於使用者電腦
            try
            {
                Response.Cookies.Add(referenceCookie);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public void SetPageMeta(IViewPponDeal mainDeal)
        {
            liF.Text = "<meta property='fb:app_id' content='" + ProviderFactory.Instance().GetConfig().FacebookApplicationId + "'/>";
            litLineHeader.Text = PponFacade.RenderMetasForDeal(mainDeal);
            litCanonicalRel.Text = PponFacade.RenderCanonicalRel(mainDeal);
        }

        public void ShowMessage(string message, string goToUrl)
        {
            if (string.IsNullOrEmpty(goToUrl))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('" + message + "');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('" + message + "');window.location.href='" + goToUrl + "';", true);
            }
        }

        public void RedirectPage(string goToUrl)
        {
            if (!string.IsNullOrEmpty(goToUrl))
            {
                if (goToUrl.Contains("?"))
                {
                    goToUrl = goToUrl + "&";
                }
                else
                {
                    goToUrl = goToUrl + "?";
                }
                if (cp.ReferralToApp)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "goto",
                                                        "appPromo('" + goToUrl + CPA_CODE_FOR_NEW_MEMBER + "');", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "window.location.href='" + goToUrl + CPA_CODE_FOR_NEW_MEMBER + "';", true);
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                this.Presenter.OnViewInitialized();
            }

            this.Presenter.OnViewLoaded();
        }
    }
}
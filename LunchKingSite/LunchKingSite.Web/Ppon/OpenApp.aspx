﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OpenApp.aspx.cs" Inherits="LunchKingSite.Web.Ppon.OpenApp" %>
<!DOCTYPE html>
<html>
<head runat="server">
    <meta charset="UTF-8">
    <title></title>
    <meta http-equiv="refresh" content="0; url=open17life://www.17life.com/ppon/OpenApp.aspx?bid=<%=Request.QueryString["bid"]%>&groupId=<%=Request.QueryString["groupId"]%>&refereeId=<%=Request.QueryString["refereeId"]%>&orderguid=<%=Request.QueryString["orderguid"]%>" />
    <meta name="viewport" content="width=device-width initial-scale=1.0 maximum-scale=1" />

    <link href="<%=ResolveUrl("~/Themes/PCweb/css/Rightside.css")%>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/Themes/PCweb/css/RDL-L.css")%>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/Themes/PCweb/css/RDL-M.css")%>" rel="stylesheet" type="text/css" />
    <link href="<%=ResolveUrl("~/Themes/PCweb/css/RDL-S.css")%>" rel="stylesheet" type="text/css" />
    <style>
        body{
            padding-top: 40%
        }
        .m-buyBtn a{
            text-decoration:none;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        
    </form>
<script type="text/javascript">
    var flag = 0;

    window.onload = function (e) {
        window.setTimeout(function () {
            if (flag == 0) {
                window.location = "market://details?id=com.uranus.e7plife";
            }
        }, 500);
    };
    document.addEventListener("visibilitychange", function(){
        if (document.visibilityState == "hidden") {
            flag++;
            if (flag >= 1) {
                history.back();
            }
        }
    });
</script>
</body>
</html>

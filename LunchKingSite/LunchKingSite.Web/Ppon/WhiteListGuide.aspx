﻿<%@ Page Title="收不到好康郵件？請將good_deal@edm.17life.com.tw加入白名單喔~~" Language="C#" MasterPageFile="~/Ppon/Ppon.Master"
    AutoEventWireup="true" CodeBehind="WhiteListGuide.aspx.cs" Inherits="LunchKingSite.Web.Ppon.WhiteListGuide" %>
<%@ Register Src="~/UserControls/Paragraph.ascx" TagName="Paragraph"
    TagPrefix="ucr" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MC" runat="server">
    <link href="../Themes/default/images/17Life/WhiteList/HelpMail.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
<!--
.HelpMailArea {
	width:710px;
	height:auto;
	z-index:4;
	background-repeat: no-repeat;
}
-->
</style>
    <script type="text/javascript">
        $(function(){
            $('.sliderWrapMenber').eq(0).hide();
            $('div[id*="pan_WhiteList"]').prev().remove();
        });
        function chooselinkcss(target, panel) {
            $('.nav >li').removeClass().addClass('conbgn linav');
            $(target).removeClass('conbgn linav').addClass('ChooseMailGreen');
            $('#div_Yahoo').hide();
            $('#div_Pchome').hide();
            $('#div_Outlook').hide();
            $('#div_Hotmail').hide();
            $('#div_Gmail').hide();
            $(panel).show();
        } 
    </script>
    <ucR:Paragraph ID="paragraph1" runat="server" />
    <asp:Panel ID="pan_WhiteList" runat="server" Visible="false">
        <div id="Left">
            <div id="div_Yahoo">
                <div class="HelpMailArea">
                    <div class="HelpMailTop">
                    </div>
                    <div class="HelpMailDetail">
                        <div class="HelpMailDetai1Content">
                            收不到好康郵件？請將<strong>hi@edm.17life.com.tw</strong> 加入白名單 &gt;&gt;<a href="https://login.yahoo.com/config/mail?.intl=tw">立即開始設定</a></div>
                    </div>
                    <!--HelpMailDetail-->
                    <div class="HelpMailContent">
                        <div class="HelpMailStep">
                            第一步：打開電子信箱，勾選欲進行設定的信件。</div>
                        <div class="HelpMailStepContent">
                            <img src="../Themes/default/images/17Life/WhiteList/YahooHelpEmailContent01.jpg" /></div>
                        <div class="HelpMailStep">
                            第二步：點選<strong>「了解更多」</strong>後出現下拉式選單，再選擇<strong>「信件自動分類」</strong>。</div>
                        <div class="HelpMailStepContent">
                            <img src="../Themes/default/images/17Life/WhiteList/YahooHelpEmailContent02.jpg" /></div>
                        <div class="HelpMailStep">
                            第三步：在<strong>「新增自動分類」</strong>內，<strong>「名稱」</strong>會自動幫您填好，<strong>「寄件者」</strong>mail處請填寫<strong>「hi@edm.17life.com.tw」</strong>，然後點擊<strong>「儲存」</strong>按鈕。
                        </div>
                        <div class="HelpMailStepContent">
                            <img src="../Themes/default/images/17Life/WhiteList/YahooHelpEmailContent03.jpg" /></div>
                    </div>
                    <!--HelpMailYahooContent-->
                    <div class="HelpMailDetail">
                        <div class="HelpMailDetai1Content">
                            收不到好康郵件？請將<strong> hi@edm.17life.com.tw</strong> 加入白名單 &gt;&gt;<a href="https://login.yahoo.com/config/mail?.intl=tw">立即開始設定</a>
                        </div>
                    </div>
                    <!--HelpMailDetail-->
                    <div class="HelpMailBottom">
                    </div>
                </div>
            </div>
            <div id="div_Pchome" style="display: none">
                <div class="HelpMailArea">
                    <div class="HelpMailTop">
                    </div>
                    <div class="HelpMailDetail">
                        <div class="HelpMailDetai1Content">
                            收不到好康郵件？請將<strong>hi@edm.17life.com.tw</strong> 加入白名單 &gt;&gt;<a href="https://mail.google.com/">立即開始設定</a>
                        </div>
                    </div>
                    <!--HelpMailDetail-->
                    <div class="HelpMailContent">
                        <div class="HelpMailStep">
                            第一步：打開電子信箱，點擊左列選單中的<strong>「通訊錄」</strong>，並在展開的選單中點擊「<strong>白名單管理」</strong>。</div>
                        <div class="HelpMailStepContent">
                            <img src="../Themes/default/images/17Life/WhiteList/PchomeHelpEmailContent01.jpg" /></div>
                        <div class="HelpMailStep">
                            第二步：在<strong>「新增白名單的郵件信箱」</strong>中填寫<strong>「hi@edm.17life.com.tw」</strong>，並點擊<strong>「新增白名單」</strong>按鈕。</div>
                        <div class="HelpMailStepContent">
                            <img src="../Themes/default/images/17Life/WhiteList/PchomeHelpEmailContent02.jpg" /></div>
                    </div>
                    <!--HelpMailYahooContent-->
                    <div class="HelpMailDetail">
                        <div class="HelpMailDetai1Content">
                            收不到好康郵件？請將<strong> hi@edm.17life.com.tw</strong> 加入白名單 &gt;&gt;<a href="https://login.yahoo.com/config/mail?.intl=tw">立即開始設定</a>
                        </div>
                    </div>
                    <!--HelpMailDetail-->
                    <div class="HelpMailBottom">
                    </div>
                </div>
            </div>
            <div id="div_Outlook" style="display: none">
                <div class="HelpMailArea">
                    <div class="HelpMailTop">
                    </div>
                    <div class="HelpMailDetail">
                        <div class="HelpMailDetai1Content">
                            收不到好康郵件？請將<strong>hi@edm.17life.com.tw</strong> 加入白名單 &gt;&gt;<a href="#">立即開始設定</a>
                        </div>
                    </div>
                    <!--HelpMailDetail-->
                    <div class="HelpMailContent">
                        <div class="HelpMailStep">
                            <p>
                                第一步：打開Outlook，點擊上方工具列中的<strong>「動作」</strong>(舊版「執行」)，並在展開的選單中點擊<strong>「垃圾郵件」</strong>&gt;<strong>「垃圾郵件選項」</strong>
                            。
                        </div>
                        <div class="HelpMailStepContent">
                            <img src="../Themes/default/images/17Life/WhiteList/OutlookHelpEmailContent01.jpg" /></div>
                        <div class="HelpMailStep">
                            第二步：選擇視窗中<strong>「安全的寄件者」</strong>標籤。</div>
                        <div class="HelpMailStepContent">
                            <img src="../Themes/default/images/17Life/WhiteList/OutlookHelpEmailContent02.jpg" /></div>
                        <div class="HelpMailStep">
                            第三步：點擊<strong>「新增」</strong>按鈕，在<strong>「新增位置或網域」</strong>的輸入框內填寫<strong>「hi@edm.17life.com.tw」</strong>
                            ，並點擊<strong>「確定」</strong>按鈕。</div>
                        <div class="HelpMailStepContent">
                            <img src="../Themes/default/images/17Life/WhiteList/OutlookHelpEmailContent03.jpg" /></div>
                        <div class="HelpMailStep">
                            第四步：<strong>「 確定」</strong>設定完成。
                        </div>
                        <div class="HelpMailStepContent">
                            <img src="../Themes/default/images/17Life/WhiteList/OutlookHelpEmailContent04.jpg" /></div>
                    </div>
                    <!--HelpMailYahooContent-->
                    <div class="HelpMailDetail">
                        <div class="HelpMailDetai1Content">
                            收不到好康郵件？請將<strong> hi@edm.17life.com.tw</strong> 加入白名單 &gt;&gt;<a href="#">立即開始設定</a>
                        </div>
                    </div>
                    <!--HelpMailDetail-->
                    <div class="HelpMailBottom">
                    </div>
                </div>
            </div>
            <div id="div_Hotmail" style="display: none">
                <div class="HelpMailArea">
                    <div class="HelpMailTop">
                    </div>
                    <div class="HelpMailDetail">
                        <div class="HelpMailDetai1Content">
                            收不到好康郵件？請將<strong>hi@edm.17life.com.tw</strong> 加入白名單 &gt;&gt;<a href="http://sn124w.snt124.mail.live.com/default.aspx/">立即開始設定</a>
                        </div>
                    </div>
                    <!--HelpMailDetail-->
                    <div class="HelpMailContent">
                        <div class="HelpMailStep">
                            第一步：打開電子信箱，點擊網頁右上角的<strong>「齒輪」</strong>圖樣，並在展開的選單中點擊<strong>「更多郵件設定」</strong>。</div>
                        <div class="HelpMailStepContent">
                            <img src="../Themes/default/images/17Life/WhiteList/HotmailHelpEmail01.jpg" /></div>
                        <div class="HelpMailStep">
                            第二步：點擊<strong>「防止垃圾郵件」</strong>下的<strong>「安全及封鎖的寄件者」</strong>。</div>
                        <div class="HelpMailStepContent">
                            <img src="../Themes/default/images/17Life/WhiteList/HotmailHelpEmail02.jpg" /></div>
                        <div class="HelpMailStep">
                            第三步：點擊<strong>「安全的寄件者」</strong>。</div>
                        <div class="HelpMailStepContent">
                            <img src="../Themes/default/images/17Life/WhiteList/HotmailHelpEmail03.jpg" /></div>
                        <div class="HelpMailStep">
                            第四步：在<strong>「安全的寄件者或網域」</strong>中填寫<strong>「hi@edm.17life.com.tw」</strong>，並點擊<strong>「新增至清單」</strong>按鈕。</div>
                        <div class="HelpMailStepContent">
                            <img src="../Themes/default/images/17Life/WhiteList/HotmailHelpEmail04.jpg" /></div>
                    </div>
                    <!--HelpMailYahooContent-->
                    <div class="HelpMailDetail">
                        <div class="HelpMailDetai1Content">
                            收不到好康郵件？請將<strong> hi@edm.17life.com.tw</strong> 加入白名單 &gt;&gt;<a href="http://sn124w.snt124.mail.live.com/default.aspx/">立即開始設定</a>
                        </div>
                    </div>
                    <!--HelpMailDetail-->
                    <div class="HelpMailBottom">
                    </div>
                </div>
            </div>
            <div id="div_Gmail" style="display: none">
                <div class="HelpMailArea">
                    <div class="HelpMailTop">
                    </div>
                    <div class="HelpMailDetail">
                        <div class="HelpMailDetai1Content">
                            收不到好康郵件？請將<strong>hi@edm.17life.com.tw</strong> 加入白名單 &gt;&gt;<a href="https://mail.google.com/">立即開始設定</a>
                        </div>
                    </div>
                    <!--HelpMailDetail-->
                    <div class="HelpMailContent">
                        <div class="HelpMailStep">
                            第一步：打開電子信箱，按下搜尋列右側的三角形。</div>
                        <div class="HelpMailStepContent">
                            <img src="../Themes/default/images/17Life/WhiteList/GmailHelpEmailContetnt01.jpg" /></div>
                        <div class="HelpMailStep">
                            第二步：在<strong>「寄件者」</strong>中填寫上<strong>「hi@edm.17life.com.tw」</strong>，並點擊<strong>「根據這個搜尋條件建立篩選器」</strong>繼續。</div>
                        <div class="HelpMailStepContent">
                            <img src="../Themes/default/images/17Life/WhiteList/GmailHelpEmailContetnt02.jpg" /></div>
                        <div class="HelpMailStep">
                            第三步：勾選<strong>「不要將他傳送到垃圾郵件」</strong>，並勾選<strong>「同時將這個篩選器套用到所有相符項目」</strong>，最後按下<strong>「建立篩選器」</strong>按鈕。</div>
                        <div class="HelpMailStepContent">
                            <img src="../Themes/default/images/17Life/WhiteList/GmailHelpEmailContetnt03.jpg" /></div>
                    </div>
                    <!--HelpMailYahooContent-->
                    <div class="HelpMailDetail">
                        <div class="HelpMailDetai1Content">
                            收不到好康郵件？請將<strong> hi@edm.17life.com.tw</strong> 加入白名單 &gt;&gt;<a href="https://login.yahoo.com/config/mail?.intl=tw">立即開始設定</a>
                        </div>
                    </div>
                    <!--HelpMailDetail-->
                    <div class="HelpMailBottom">
                    </div>
                </div>
            </div>
            <!--HelpMailYahooArea-->
        </div>
        <div id="Rightarea">
            <div class="ChooseMailarea">
                <div class="ChooseMailTop">
                </div>
                <div id="ChooseMailContent">
                    <ul class="nav">
                        <li class="ChooseMailGreen" id="li_yahoo"><a style="cursor: pointer" onclick="chooselinkcss($('#li_yahoo'),$('#div_Yahoo'));">
                            Yahoo信箱</a></li>
                        <li class="conbgn linav" id="li_gmail"><a style="cursor: pointer" onclick="chooselinkcss($('#li_gmail'),$('#div_Gmail'));">
                            Gmail信箱</a></li>
                        <li class="conbgn linav" id="li_hotmail"><a style="cursor: pointer" onclick="chooselinkcss($('#li_hotmail'),$('#div_Hotmail'));">
                            MSN Hotmail信箱</a></li>
                        <li class="conbgn linav" id="li_pchome"><a style="cursor: pointer" onclick="chooselinkcss($('#li_pchome'),$('#div_Pchome'));">
                            PChome信箱</a></li>
                        <li class="conbgn linav" id="li_outlook"><a style="cursor: pointer" onclick="chooselinkcss($('#li_outlook'),$('#div_Outlook'));">
                            Outlook</a></li>
                    </ul>
                </div>
                <!--ChooseMailContent-->
                <div class="ChooseMailBottom">
                </div>
            </div>
        </div>
    </asp:Panel>
    <asp:Panel ID="pan_HelpMail" runat="server" Visible="false">
        <div class="HelpMailsimplifyArea">
            <div class="HelpMailsimplifyTop">
            </div>
            <div class="HelpMailContent">
                <img src="../Themes/default/images/17Life/WhiteList/help_email_simplify_12.jpg" width="710"
                    height="1743" /></div>
            <div class="HelpMailsimplifyBottom">
            </div>
        </div>
    </asp:Panel>
</asp:Content>

﻿<%@ Page Language="C#" MasterPageFile="~/PPon/PPon.Master"
    AutoEventWireup="true" CodeBehind="Detail.aspx.cs" Inherits="LunchKingSite.Web.Ppon.Detail" EnableViewState="false" %>
<%@ Import Namespace="LunchKingSite.BizLogic.Component" %>
<%@ Import Namespace="LunchKingSite.Core" %>
<%@ Import Namespace="LunchKingSite.WebLib.Views" %>
<%@ Import Namespace="LunchKingSite.BizLogic.Facade" %>
<%@ MasterType VirtualPath="~/PPon/PPon.Master" %>
<%@ Register TagPrefix="uc1" Src="~/UserControls/LoginPanel.ascx" TagName="LoginPanel" %>
<%@ Register Src="~/UserControls/RandomParagraph.ascx" TagName="RandomParagraph"
    TagPrefix="ucR" %>
<%@ Register Src="~/UserControls/Paragraph.ascx" TagName="Paragraph"
    TagPrefix="ucP" %>
<%@ Register Src="AvailabilityCtrl.ascx" TagName="AvailabilityCtrl" TagPrefix="uc1" %>
<%@ Register Src="~/UserControls/ExpireNotice.ascx" TagName="ExpireNotice"
    TagPrefix="uc2" %>

<asp:Content ID="cDefaultMeta" ContentPlaceHolderID="cphPponMeta" runat="server">
    <meta property="fb:app_id" content="<%=FB_AppId%>">
<asp:Literal ID="litDealMetas" runat="server" Mode="PassThrough" />
    <asp:Literal ID="litCanonicalRel" runat="server" Mode="PassThrough" />
    <asp:PlaceHolder ID="phNorobots" runat="server" Visible="false">
        <meta name="robots" content="noindex,nofollow,noarchive" />
    </asp:PlaceHolder>
    <asp:PlaceHolder ID="phAppLinks" runat="server"></asp:PlaceHolder>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <link href='/themes/PCweb/css/Rightside.css' rel="stylesheet" type="text/css" />
    <%=LunchKingSite.Core.Helper.RenderCss("/Themes/PCweb/css/ppon_item.min.css") %>
    <link href='/Themes/default/images/17Life/G2/A3-event_coupons.css' rel="stylesheet" type="text/css" />
    <script type="application/ld+json"><%=MicroDataJson%></script>
    <script type="text/javascript">
        <%=ScupioDataJson%>
    </script>
    <script type="text/javascript">
        <%=GtagDataJson%>
    </script>
    <script type="application/javascript">
        <%=YahooDataJson%>
    </script>
    <script type="text/javascript">
        function scupioCartPage() {
            <%=ScupioCartPageDataJson%>
        }
    </script>
    <script type="text/javascript" src="/Tools/js/jquery.smartbanner.js"></script>
    <script type="text/javascript" src="/Tools/js/homecook.js"></script>
    <script type="text/javascript" src="/Tools/js/osm/OpenLayers.js"></script>
    <script type="text/javascript" src="/Tools/js/jquery-sliding-menu.js"></script>
    <script type="text/javascript" src="/Tools/js/TouchSlide.1.1.source.js"></script>
    <script type="text/javascript" src="/Tools/js/jquery.SuperSlide.2.1.1.js"></script>
    <script type="text/javascript" src="/Tools/js/TweenMax.min.js"></script>
    <script type="text/javascript" src="/Themes/OWriteOff/plugin/bpopup/jquery.bpopup.js"></script>
    <style type="text/css">
        .fb_iframe_widget_lift {
            z-index: 10 !important;
        }

        .ui-corner-all {
            -moz-border-radius: 0px;
            -webkit-border-radius: 0px;
            border-radius: 0px;
        }

        .NaviCityArea a {
            cursor: pointer;
        }
        #Shopareatitle li + li {
            margin-top: 10px;
        }

        .Curatorial-Sort {
            display: inherit;
        }

        #mobile-indicator {
            display: none;
        }

        .fb_btn {
            display: none !important;
        }
        #referral_FB_area {
            display: none !important;
        }
        .bn_share {
            display: none !important;
        }
        .m-fbBtn {
            display: none !important;
        }
        .deal_item_wrap .deal_info {
            height: auto!important;
        }

        /*折後價的版面調整*/
        .deal_main {
            width:480px !important; 
        }
        .deal_item_wrap {
            width: 716px !important;
        }
        #maincontent .forsdeal, #maincontent .content-group, #Entry {
            width: 716px !important;
        }
        #event_wrapper{
            width: 716px !important;
        }
        #Rightarea {
            width:220px !important;
        }
        #Rightarea .side_block_title{
            font-size: 17px !important;
        }
        <%if (Model.ItemPrice.ToString("0").Length == 6) { %>
        .deal_item_wrap .deal_info .info_discount .info_num {
            font-size: 26px !important;
        }
        .deal_item_wrap .deal_info .info_price .info_num {
            font-size: 26px !important;
        }
        <%} else if (Model.ItemPrice.ToString("0").Length == 5) { %>
        .deal_item_wrap .deal_info .info_discount .info_num {
            font-size: 30px !important;
        }
        .deal_item_wrap .deal_info .info_price .info_num {
            font-size: 30px !important;
        }
        <%}%>
        <%if (this.IsHiddenBanner) {%>
           .slide-bn-block { display: none !important;}
        <%} %>

		#detailDesc {
			overflow-wrap: break-word;
		}

		#detailProductSpec {
			overflow-wrap: break-word;
		}

        #detailDesc img {
            max-width:650px;
            vertical-align: middle;
            border-style: none;
        }

        /* 用bpopup.js製作的彈窗 以及 其內UI Start */
        .btn-default {
            width: 100px;
            height: 20px;
            background: #ccc;
            padding: 10px;
            text-align: center;
            border-radius: 5px;
            color: white;
            font-size:medium;
        }
        .btn-warn {
            width: 100px;
            height: 20px;
            background: #bf0000;
            padding: 10px;
            text-align: center;
            border-radius: 5px;
            color: white;
            font-size:medium;
        }
        .bpopup-modal {
            /*彈窗預設為隱藏，直到jquery調用才顯示*/
            display: none;
            /*margin-left: 250px;*/
            width: calc(100vw - 575px);
            background: #fff;
        }

        .bpopup-modal .row {
            margin-bottom: 0;
        }

        .bpopup-modal.bpopup-modal-toast {
            width: 600px;
            color: #000;
            border-radius: 10px;
        }


        .bpopup-modal.bpopup-modal-toast .bpopup-modal-title {
            text-align: left;
            margin: 20px;
            margin-top:50px;
            margin-bottom:50px;
            font-size: 18px;
        }

        .bpopup-modal-footer {
            margin-top: 20px;
            margin-left:20px;
            margin-bottom:50px;
            text-align:center;
        }

        .bpopup-modal a {
            cursor: pointer;
        }

        /* 用bpopup.js製作的彈窗 以及 其內UI End */
    </style>
    <script type="text/javascript">
        //GA
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
        ga('create', 'UA-3157038-1', 'auto');

        //Facebook Pixel Code
        fbq('track', 'ViewContent', {
            content_ids: ['<%=BusinessHourId%>'],
            content_type: 'product',
            value: <%=FPCvalue%>,
            currency: 'TWD'
        });

        //global var
        var count = 0;
        var timers = [];

        //ready function
        $(function() {
            var obj = $('.DealPriceInfo .discount:visible');
            if(obj.text().trim().length <= 0){
                $('.discount').attr("style", "visibility: hidden");
                $('.dealpricetext').attr("style", "visibility: hidden");
            }

        });

        var window_height = $(window).height();
        var window_width = $(window).width();
        var block_layer = $('#renderpage');
        var title = $(document).find("title").text().split("，")[0];
        function block_coupon_pc(brandId){
            var promise = $.ajax({
                type: "POST",
                url: '<%=ResolveUrl("~/Event/GetDiscountCode")%>',
                data:{
                    brandId: brandId,
                    bid: '<%=BusinessHourId%>'
                },
                dataType: "html",
                success:function (data) {
                    $('#renderpage').empty().append(data);
                }
            });
            $.blockUI({ 
                message: $('#coupon_wrapper') ,
                css: { 
                    background: 'none', 
                    border: 'none',
                    position: 'absolute',
                    width: '100%',
                    top: '0',
                    left: '0',
                }
            });
            promise.done(function () {
                var pic_src = $('#MainPic .slider img').attr('src');
                var mainpic = $('#divMain');
                var mainpic_img = mainpic.find('img');
                mainpic_img.attr('src', pic_src);
                mainpic_img.css('margin-top','30px');
                mainpic.css('height','300px');
                mainpic.css('margin-bottom','30px');
                $('.coupon_name').html(title);
                var buy_btn = $('.btn_normal'); 
                var btn_attr_href = buy_btn.attr('href');
                var btn_attr_onclick = buy_btn.attr('onclick');            
                if (typeof btn_attr_href !== typeof undefined && btn_attr_href !== false) {
                    $('.coupon_rule_btn a').attr('href', btn_attr_href);
                }
                else if (typeof btn_attr_onclick !== typeof undefined && btn_attr_onclick !== false){
                    $('.coupon_rule_btn a').attr('onclick', btn_attr_onclick);
                }
            });

            //GA
            ga('send', 'event', '檔次頁_折價券', '折價券Banner', '點擊_折價券Banner');
        }
        function block_coupon_mb(brandId) {
            var promise = $.ajax({
                type: "POST",
                url: '<%=ResolveUrl("~/Event/GetDiscountCode")%>',
                data:{
                    brandId: brandId,
                    bid: '<%=BusinessHourId%>'
                },
                dataType: "html",
                success:function (data) {
                    $('#renderpage').empty().append(data);
                }
            });
            TweenMax.from($("#coupon_wrapper"), 0.5, { y: window_height });
            TweenMax.to($("#coupon_wrapper"), 0.5, { y: 0 });
            $(document.body).addClass('noscroll');
            $.blockUI({ 
                message: $('#coupon_wrapper') ,
                css: { 
                    background: 'none',
                    border: 'none',
                    position: 'absolute',
                    width: '100%',
                    top: '0',
                    left: '0',
                }
            }); 
            promise.done(function () {
                var pic_src = $('#MainPic .slider img').attr('src');
                var mainpic = $('#divMain');
                var mainpic_img = mainpic.find('img');
                mainpic_img.attr('src', pic_src);
                $('.coupon_name').html(title);
                var buy_btn = $('.btn_normal');
                var btn_attr_href = buy_btn.attr('href');
                var btn_attr_onclick = buy_btn.attr('onclick');            
                if (typeof btn_attr_href !== typeof undefined && btn_attr_href !== false) {
                    $('#buy_btn_moblie a').attr('href', btn_attr_href);
                }
                else if (typeof btn_attr_onclick !== typeof undefined && btn_attr_onclick !== false){
                    $('#buy_btn_moblie a').attr('onclick', 'unblock_buy_mb()');
                }
            });
        }
        function unblock_coupon_pc() {
            $.unblockUI();
        }
        function unblock_coupon_mb() {
            TweenMax.from($("#coupon_wrapper"), 0.5, { y: 0 });;
            TweenMax.to($("#coupon_wrapper"), 0.5, { y: window_height });
            $(document.body).removeClass('noscroll');
            $.unblockUI();
        }
        function unblock_buy_pc() {
            PopWindow($('#combodeals'));
        }
        function unblock_buy_mb() {
            TweenMax.from($("#coupon_wrapper"), 0.5, { y: 0 });;
            TweenMax.to($("#coupon_wrapper"), 0.5, { y: window_height });
            $(document.body).removeClass('noscroll');
            $.unblockUI();
            PopWindow($('#combodeals'));
        }
        function setBrand(data){
            if(data.length>0){
                var bx = $(".bx");
                var speed = 150;

                if ($(window).width()>767){ //PC
                    var block_wrapper = $('#coupon_wrapper');
                    block_wrapper.css('left', (window_width-700)/2); 
                    block_wrapper.css('top', ($('.function-menu-bar').height()+$('.logo-inline').height())*1.5); 
                    var event_ul = $('#event_wrapper .event_slider');   
                    for(i=0; i<data.length; i++){
                        if (data[i].BrandId != 0){
                            var src = null;
                            if(data[i].WebPic!=null){
                                var str = data[i].WebPic.split(',');
                                src = '<%=SystemConfig.SiteUrl%>/media/'+str[0]+'/'+str[1];
                            }
                            else{
                                src = '<%=SystemConfig.SiteUrl%>/Themes/default/images/17P/coupon_banner_pc.jpg';
                            }
                            event_ul.append('<li><a href="#" class="discount_banner_event" onclick="block_coupon_pc('+data[i].BrandId+');"><img src="'+src+'" alt="" /></a></li>');
                        }
                    }
                    var event_li = event_ul.find('li');
                    var event_num = event_li.length;
                    var nextbtnpc = $(".bx-next-pc");
                    var prevbtnpc = $(".bx-prev-pc");
                    if (event_num < 2) {
                        bx.hide();
                    }
                    nextbtnpc.click(function () {
                        var now = $(this).parent().next("ul.event_slider").children(":visible");
                        var last = $(this).parent().next("ul.event_slider").children(":last");
                        var prev = now.prev();
                        prev = prev.index() == -1 ? last : prev;
                        now.fadeOut(speed, function() {prev.fadeIn(speed);});
                    });
                    prevbtnpc.click(function () {
                        var now = $(this).parent().next("ul.event_slider").children(':visible');
                        var first = $(this).parent().next("ul.event_slider").children(':first');
                        var next = now.next();
                        next = next.index() == -1 ? first : next;
                        now.fadeOut(speed, function() {next.fadeIn(speed);});
                    });
                    $('.close_layer').attr('title','Click to unblock').click(unblock_coupon_pc); 
                    $('#event_wrapper').show();

                    //GA
                    ga('send', 'event', '檔次頁_折價券', '折價券Banner', '瀏覽_折價券Banner');
                }
                else{ //MB
                    $('#event_wrapper').hide();
                    $('#event_wrapper_mb').show();         
                    $('.close_layer').hide();       

                    var event_ul = $('#event_wrapper_mb .event_slider');
                    for(i=0; i<data.length; i++){
                        if (data[i].BrandId != 0){
                            var src = null;
                            if(data[i].MobilePic!=null){
                                var str = data[i].MobilePic.split(',');
                                src = '<%=SystemConfig.SiteUrl%>/media/'+str[0]+'/'+str[1];
                            }
                            else{
                                src = '<%=SystemConfig.SiteUrl%>/Themes/default/images/17P/coupon_banner_mb.jpg';
                            }
                            event_ul.append('<div class="swiper-slide"><a class="discount_banner_event" onclick="block_coupon_mb('+data[i].BrandId+');"><img src="'+src+'" alt="" /></a></div>');
                        }
                    }

                    var event = event_ul.find('div');
                    var event_num = event.length;

                    if (event_num > 1) {
                        var mySwiper = new Swiper ('.swiper-container', {
                            pagination: '.swiper-pagination',
                            nextButton: '.swiper-button-next',
                            prevButton: '.swiper-button-prev',
                            slidesPerView: 1, //1
                            paginationClickable: true,
                            spaceBetween: 10, //10
                            loop: true,
                            autoplayDisableOnInteraction: false
                        })         
                    }
                    else{
                        bx.hide();
                    }                                       
                }

                //M版折價券Banner
                $('#event_wrapper_mb .swiper-slide').find('img').css('width', window_width*0.875);
                $('#event_wrapper_mb .swiper-container').css('width', window_width*0.875);
                $('#event_wrapper_mb .swiper-slide').css('margin', '0 auto');
            }
            else{
                if($(window).width()>767){
                    $('#event_wrapper').hide();
                }
                else{                    
                    $('#event_wrapper_mb').hide();
                }
            }
        }

        //M版折價券Banner
        $(window).resize(function(){
            $('#event_wrapper_mb .swiper-slide').find('img').css('width', $(window).width()*0.875);   
            $('#event_wrapper_mb .swiper-container').css('width', $(window).width()*0.875);
            $('#event_wrapper_mb .swiper-slide').css('margin', '0 auto');
        })

        $(document).ready(function () {
            if($("input[id=HfChannelAgent]").val() == '<%=((int) AgentChannel.TaiShinMall)%>') {
                $(".info_bar").hide();
                $(".share.m-share").hide();
            }

            $.ajax(
                {
                    type: "POST",
                    url: '<%=ResolveUrl("~/Event/GetBrandId")%>',
                    data: { bid: '<%=BusinessHourId%>' },
                    dataType: "json",
                    success: setBrand,
                    error: function() {
                        alert("伺服器忙碌中，請重新操作一次。");
                    }
                }
            );
            $.fn.updateTimer = function() {
 
                this
                return this;
 
            };
            
            $.fn.updateTimer = function () {
                var nn = parseInt($(this).data('finalTime') - new Date().getTime(), 10);
                if (nn < 0) {
                    nn = 0;
                }
                nn = nn / 1000;
                var dd = parseInt(nn / (60 * 60 * 24), 10);
                nn = nn % (60 * 60 * 24);
                var hh = parseInt( nn / (60 * 60), 10);
                nn = nn % (60 * 60);
                var mm = parseInt( nn / 60, 10);
                nn = nn % 60;
                var ss = parseInt( nn % 60);
                var timerNums = $(this).find('.timer_num');
                timerNums.eq(0).text(dd < 10 ? '0' + dd : dd);
                timerNums.eq(1).text(hh < 10 ? '0' + hh : hh);
                timerNums.eq(2).text(mm < 10 ? '0' + mm : mm);
                timerNums.eq(3).text(ss < 10 ? '0' + ss : ss);
                return this;
            };

            $('.info_timer').each(function () {
                var val = $(this).data('countdown');
                if ($.isNumeric(val)) {
                    $('.countdown-timer').show();
                    $('.countdown-text').hide();

                    $(this).find('.timer_num').text('__');
                    $(this).data('finalTime', new Date(new Date().getTime() + val));
                    var self = this;
                    window.setInterval(function () {
                        $(self).updateTimer();
                    }, 995);
                } else {
                    $('.countdown-timer').hide();
                    $('.countdown-text').show();
                }                    
            });
            $('.info_timer').updateTimer();

            var redirectUrl = '<%=ExpireRedirectUrl%>';
             if (redirectUrl.length > 0 && show_message()) { 
                
            }
        });
        var show_message = function () {
            $('#bpopup-toast-message').bPopup({
                modalClose: false,
                onOpen: function () { $('.bpopup-step_alert-confirm').click(RedirectExpiredUrl); },
            });
        }

        function RedirectExpiredUrl() {
            location.href = '<%=ExpireRedirectUrl%>';
        }
    </script>
    <!--Facebook Pixel Code-->
    <noscript>
      <img height="0" width="0" style="display:none" src="https://www.facebook.com/tr?id=274288942924758&ev=ViewContent&cd[content_ids]=<%=BusinessHourId%>&cd[content_type]=product&cd[value]=<%=FPCvalue%>&currency=TWD&noscript=1" />
    </noscript>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MC" runat="server">
    <div id="bpopup-toast-message" class="card-panel bpopup-modal bpopup-modal-toast">
        <div class="row">
            <div class="col s12 bpopup-modal-title alert_message">
                <%=ExpireRedirectDisplay%>
            </div>
            <div class="col s12 center-align bpopup-modal-footer">
                <a class="waves-effect waves-light btn-warn b-close bpopup-step_alert-confirm">確定</a>
                <a class="waves-effect waves-light btn-default b-close">取消</a>
            </div>
        </div>
    </div>
    <div id="Default">
        <asp:PlaceHolder ID="p1" runat="server">
            <input id="HfHami" type="hidden" value="<%= Hami%>" />
            <input id="HfShowEventEmail" type="hidden" value="<%= ShowEventEmail%>" />
            <input id="HfIsOpenNewWindow" type="hidden" value="<%= IsOpenNewWindow%>" />
            <input id="HfIsMobileBroswer" type="hidden" value="<%= IsMobileBroswer%>" />
            <input id="HfCityId" type="hidden" value="<%= CityId%>" />
            <input id="HfSubCategoryId" type="hidden" value="<%= CategoryID%>" />
            <input id="HfDealCategoryId" type="hidden" value="<%=PponCategory%>" />
            <input id="HfBusinessHourId" type="hidden" value="<%=BusinessHourId%>" />
            <input id="HfIsDeliveryDeal" type="hidden" value="<%=IsDeliveryDeal%>" />
            <input id="HfLoginUrl" type="hidden" value="<%=string.Format("{0}?ReturnUrl={1}", FormsAuthentication.LoginUrl, Server.UrlEncode((string)Session[LkSiteSession.NowUrl.ToString()]))%>" />
            <input id="HfTravelCategoryId" type="hidden" value="<%=TravelCategoryId%>" />
            <input id="HfFemaleCategoryId" type="hidden" value="<%=FemaleCategoryId%>" />
            <input id="HfIsLogin" type="hidden" value="<%=(Page.User.Identity.IsAuthenticated) ? "1" : "0"%>" />
            <input id="HfShortType" type="hidden" value="<%=(int)SortType%>" />
            <input id="HfPicAlt" type="hidden" value="<%=PicAlt%>" />
            <input id="HfMShareType" type="hidden" value="fb" />
            <input id="HfBindDivClick" type="hidden" value="0" />
            <input id="HfFilterColumn" type="hidden" value="" />
            <div id="ousideAd" class="outside_AD" style="display: none;">
                <ucR:RandomParagraph ID="outsideAD" runat="server" />
            </div>

            <asp:HiddenField ID="FPType" runat="server" />

        <asp:Repeater ID="repRecommendDealsForCloseDeal" runat="server" OnItemDataBound="repRecommendDealsForCloseDeal_ItemDataBound">
            <HeaderTemplate>
            <div class="search-result-none">
                <div class="text_grey">此商品已售完。為您推薦相關商品如下</div>
                <div class="soldout_wrap clearfix">
                    <ul>
            </HeaderTemplate>
            <ItemTemplate>
                <li>
                    <a title="<%#Eval("EventTitle") %>" href="/deal/<%#Eval("BusinessHourGuid") %>">
                        <asp:Literal ID="litDealImage" runat="server" Mode="PassThrough"/>
                        <div class="soldout_title"><%#Eval("ItemName") %></div>
                        <div class="soldout_price">
                            <span class="price_oriprice">$<asp:Literal ID="litItemOrigPrice" runat="server" /></span>
                            <span class="price_discount"><asp:Literal ID="litDiscount" runat="server" Mode="PassThrough" /></span>
                            <span class="price_dealprice"><asp:Literal ID="litPrice" runat="server" Mode="PassThrough"/></span>
                        </div>
                        <asp:PlaceHolder ID="phDiscountPrice" runat="server">
                        <div class="soldout_discount clearfix">
                            <span class="text_discount">使用折價</span> 
                            <span class="text_big">
                                {discountPrice}<span class="text_normal">{priceTail}</span>
                            </span>
                        </div>
                        </asp:PlaceHolder>
                    </a>
                </li>
            </ItemTemplate>
            <FooterTemplate>
                    </ul>
                </div>
            </div>
            </FooterTemplate>
        </asp:Repeater>

            <asp:Panel ID="pan_EmptyZone" runat="server" CssClass="empty-zone" Visible="False">
                <span class="icon-smile-o"></span>
                <p>暫無提供任何好康，請重新選取</p>
            </asp:Panel>
            <div class="LeftRight_wrap">
                <div id="Left">
                    <div id="maincontent" class="clearfix" style="display:none">

                            <!--outside-top-Opt-->
                            <div id="dealAnchorPoint" class="outside-topOptBox">
                                <div id="leftCollectSuccessMsg" class="HKL_juwindow HKL_leftwind collectSuccessBox" style="display: none">
                                    <div class="HKL_ju-x">
                                        <img onclick="return Ppon.module.CloseCollectSuccessMsg();" src="<%=SystemConfig.SiteUrl%>/Themes/PCweb/images/x.png" width="20" height="20" />
                                    </div>
                                    <div class="HKL_ju-tbox">
                                        <p class="ju-title">
                                            <img src="/Themes/PCweb/images/Tick.png" width="20" height="20" />
                                            收藏成功!
                                        </p>
                                        <p class="ju-title">
                                            <a href="<%=SystemConfig.SiteUrl%>/User/Collect.aspx" class="ju-intext">去我的收藏看看</a>
                                        </p>
                                    </div>
                                </div>
                                <div id="divLeftCollectDeal"></div>
                                
                                <p class="outTOPT goTop4"><a href="#otTOP4">優惠方案</a></p>
                                <p class="outTOPT goTop2"><a href="#otTOP2">詳細介紹</a></p>
                                <p class="outTOPT goTop3"><a href="#otTOP3">店家資訊</a></p>
                                 <%if (SystemConfig.IsVbsProposalNewVersion)
                                     { %>
                                <asp:PlaceHolder ID="phdTop5Link" runat="server">
                                    <p class="outTOPT goTop5"><a href="#otTOP5">商品規格</a></p>
                                </asp:PlaceHolder>
                                <%} %>
                                <p class="outTOPT goTop1"><a href="#otTOP1">權益說明</a></p>
                            </div>
                            <!--outside-top-Opt-->
                    

                        <div class="deal_item_wrap clearfix <%= IsKindDeal ? "Publicwelfare" : string.Empty %>">
                            <asp:PlaceHolder ID="pan_share" runat="server">
                            <div class="deal_header">
                                <div class="share info_bar">
                                    <div class="share_text">分享賺取
                                        <span class="bigtext">500元!</span>
                                    </div>
                                    <div class="share_btn_box">
                                        <a href="javascript:void(0)" onclick="ShareLink();" class="copy_btn"><i class="fa fa-link" aria-hidden="true"></i>
                                            邀請連結
                                        </a>
                                    </div>
                                    <div class="payment_state">
                                        <div class="masterpass"></div>
                                        <div class="creditcard"></div>
                                        <div runat="server" id="ImageATM" class="atm" visible="false">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </asp:PlaceHolder>
                            <div runat="server" id="ExpireNotificationContent" visible="true">
                                <uc2:ExpireNotice ID="PponExpireNotice" runat="server" />
                            </div>
                            <div class="deal_main">
                                <div class="main_content">
                                    <div class="main_title">
                                        <%if (!string.IsNullOrEmpty(Model.CityName)) {%>
                                            <span class="tag_place"><%=Model.CityName %></span>
                                        <%} %>
                                        <h1><%= PponFacade.GetDisplayPponName(Model.MainDeal) %></h1>
                                    </div>
                                    <p class="main_subtitle">
                                        <%=Model.EventTitle %>
                                    </p>
                                    <ul class="tag_list">
                                        <%=Model.TagHtml %>
                                    </ul>
                                </div>
                                <div class="deal_img">
                                    <div id="slider" class="slider deal_main_img">
                                        <%foreach (string mainPic in Model.MainPics) {
                                              Response.Write(string.Format("<img src='{0}' alt='{1}' />", mainPic, Model.PicAlt));
                                          }%>
                                    </div>
                                    <asp:Literal ID="lit_badge_type" runat="server"></asp:Literal>
                                    <div class="item_sitcker">
                                        <asp:Literal ID="lit_everydaynewdeal" runat="server"></asp:Literal>
                                    </div>
                                    <div class="activeLogo alo-l">
                                        <asp:Literal ID="lit_DealPromoImageMainContent" runat="server"></asp:Literal>
                                    </div>
									<%if (Model.MainDeal.CategoryIds.Contains(ViewPponDealManager._24H_ARRIVAL_CATEGORY_ID)) { %>
										<img class="flag-24h" src="/themes/PCweb/images/24h_cover_flag.svg" alt="" />
									<%} %>
                                </div>
                            </div>
                            <div class="deal_info">
                              <div class="info_timer" data-countdown="<%=Model.Countdown %>">
                                <i class="fa fa-clock-o" aria-hidden="true"></i>
                                  <span style="display:none;" class="countdown-timer">
                                    <span class="timer_num">00</span>
                                    <span class="timer_text">天</span>
                                    <span class="timer_num">00</span>
                                    <span class="timer_text">時</span>
                                    <span class="timer_num">00</span>
                                    <span class="timer_text">分</span>
                                    <span class="timer_num">00</span>
                                    <span class="timer_text">秒</span>
                                  </span>
                                  <span style="display:none" class="countdown-text"><%=Model.CountdownText %></span>
                              </div>
                              <div class="info_buyer">
                                <i class="fa fa-users" aria-hidden="true"></i>
                                  <%= OrderedQuantityHelper.Show(Model.MainDeal , OrderedQuantityHelper.ShowType.InPortal201804)%>
                              </div>
                              <div class="info_star">
                                <%
                                    if (Model.RatingShow)
                                    {
                                        if (Model.RatingScore > 0)
                                        {
                                            int n = (int)Model.RatingScore;
                                            for (int i = 0; i < (int)Model.RatingScore; i++)
                                            {
                                                Response.Write("<i class='fa fa-star' aria-hidden='true'></i> ");
                                            }
                                            if (Model.RatingScore - (int)Model.RatingScore > 0)
                                            {
                                                n++;
                                                Response.Write("<i class='fa fa-star-half-o' aria-hidden='true'></i> ");
                                            }
                                            for (int i = 0; i < 5-n; i++)
                                            {
                                                Response.Write("<i class='fa fa-star-o' aria-hidden='true'></i> ");
                                            }
                                            Response.Write(string.Format("<span class='timer_text'>({0})</span>", Model.RatingNumber));
                                        }
                                    }
                                %>                                
                                
                              </div>                         
                              <span class="info_heart_dash" style="position:relative">
                                <div class="info_heart">
                                  <a href="javascript:void(0)" class="off">
                                    <i class="fa fa-heart-o" aria-hidden="true"></i>
                                    <span class="timer_text">收藏</span>
                                  </a>
                                </div>
                                  <div id="collectSuccessMsg" class="HKL_juwindow collectSuccessBox" style="display: none; position:absolute">
                                      <div class="HKL_ju-x">
                                          <img onclick="return Ppon.module.CloseCollectSuccessMsg();" src="/Themes/PCweb/images/x.png" width="20" height="20" />
                                      </div>
                                      <div class="HKL_ju-tbox">
                                          <p class="ju-title">
                                              <img src="/Themes/PCweb/images/Tick.png" width="20" height="20" />收藏成功!
                                          </p>
                                          <p class="ju-title"><a href="/User/Collect.aspx" class="ju-intext">去我的收藏看看</a></p>
                                      </div>
                                  </div>
                              </span>
                                <%if (Model.IsKindDeal) { //公益檔%>
                                    <div class="info_price">
                                        <span class="info_text"><%=Model.ItemPriceLabel %></span>
                                        <span class="info_num color_red">$<%=Model.ItemPrice.ToString("0") %></span>
                                        <span class="info_text"><%=Model.ItemPriceFrom %></span>
                                    </div>
                                    <div class="info_save">公益活動</div>
                                <%} else if (Model.DiscountPrice.GetValueOrDefault() > 0) { //有折後價%>
                                <div class="info_price">
                                    <span class="info_text"><%=Model.ItemPriceLabel %></span>
                                    <span class="info_num">$<%=Model.ItemPrice.ToString("0") %></span>
                                    <span class="info_text"><%=Model.ItemPriceFrom %></span>
                                </div>
                                <%{
                                        bool isRenderPrice = false;
                                        if (Model.MainDeal.DeliveryType == (int)DeliveryType.ToHouse && (SystemConfig.DealPagePriceMode == 1 || SystemConfig.DealPagePriceMode == 2 && Request["gclid"] != null))
                                        {
                                            var prodEntity = ProductEntries.Where(t=>t.IsSoldOut == false).FirstOrDefault();
                                            if (prodEntity != null)
                                            {
                                                string[] titleParts = prodEntity.Title.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                                                string prodEntityTitle;
                                                if (titleParts.Length > 1)
                                                {
                                                    prodEntityTitle = titleParts.Last();
                                                }
                                                else
                                                {
                                                    prodEntityTitle = prodEntity.Title;
                                                }
                                                Response.Write("<div class='info_save'>");
                                                Response.Write(string.Format("最低購買：{0} {1}", prodEntityTitle, prodEntity.Price));
                                                isRenderPrice = true;
                                                Response.Write("</div>");
                                            }
                                        }
                                        if (isRenderPrice == false)
                                        {
                                            Response.Write("<div class='info_save'>");
                                            Response.Write(string.Format("原價{0} | {1}現省${2}",
                                                Model.ItemOrigPrice.ToString("0"),
                                                Model.DiscountString,
                                                (Model.ItemOrigPrice - Model.ItemPrice).ToString("0")));
                                            Response.Write("</div>");
                                        }
                                    }%>
                                <div class="info_discount">
                                    <div class="discount_title">
                                    <i class="fa fa-bullhorn" aria-hidden="true"></i>
                                    <span class="discount_text">結帳使用折價券</span>
                                    </div>
                                    <span class="info_text">折後價</span>
                                    <span class="info_num color_red">$<%=Model.DiscountPrice.ToString("0") %></span>
                                    <span class="info_text">起</span>
                                </div>
                                <% } else  { // 沒折後價%>
                                    <div class="info_price">
                                        <span class="info_text"><%=Model.ItemPriceLabel %></span>
                                        <span class="info_num color_red">$<%=Model.ItemPrice.ToString("0") %></span>
                                        <span class="info_text"><%=Model.ItemPriceFrom %></span>
                                    </div>
                                <%{
                                        bool isRenderPrice = false;
                                        if (Model.MainDeal.DeliveryType == (int)DeliveryType.ToHouse && (SystemConfig.DealPagePriceMode == 1 || SystemConfig.DealPagePriceMode == 2 && Request["gclid"] != null))
                                        {
                                            var prodEntity = ProductEntries.Where(t=>t.IsSoldOut == false).FirstOrDefault();
                                            if (prodEntity != null)
                                            {
                                                string[] titleParts = prodEntity.Title.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                                                string prodEntityTitle;
                                                if (titleParts.Length > 1)
                                                {
                                                    prodEntityTitle = titleParts.Last();
                                                }
                                                else
                                                {
                                                    prodEntityTitle = prodEntity.Title;
                                                }
                                                Response.Write("<div class='info_save'>");
                                                Response.Write(string.Format("最低購買：{0} {1}", prodEntityTitle, prodEntity.Price));
                                                isRenderPrice = true;
                                                Response.Write("</div>");
                                            }
                                        }
                                        if (isRenderPrice == false)
                                        {
                                            Response.Write("<div class='info_save'>");
                                            Response.Write(string.Format("原價{0} | {1}現省${2}",
                                                Model.ItemOrigPrice.ToString("0"),
                                                Model.DiscountString,
                                                (Model.ItemOrigPrice - Model.ItemPrice).ToString("0")));
                                            Response.Write("</div>");
                                        }
                                    }%>
                                <%} %>
                              <div class="info_buy">
                                  <%=Model.BuyBtnHtml%>
                              </div>
                              <div class="" style="display:none;">
                              優惠商品不適用折價券
                              </div>
                            </div>

                        </div>
                        
                        <br />
                        <asp:PlaceHolder ID="ph_Retargeting" Visible="false" runat="server">
                            <div style="display: none">
                                <%=DealArgs%>
                            </div>
                            <div style="display: none">
                            <asp:Literal ID="lp" runat="server"></asp:Literal>
                            </div>
                        </asp:PlaceHolder>
                    </div>
                    <div id="event_wrapper" style="display: none;">
                        <div class="slider_btn">
                            <a href="javascript:void(0)" class="bx bx-prev-pc">
                              <i class="fa fa-angle-left"></i>
                            </a>
                            <a href="javascript:void(0)" class="bx bx-next-pc">
                              <i class="fa fa-angle-right"></i>
                            </a> 
                        </div>
                        <ul class="event_slider">
                        </ul>
                    </div>
                    <div id="event_wrapper_mb" class="swiper-container" style="display: none;">
                        <div class="slider_btn">
                            <div class="swiper-button-prev swiper-button-black bx bx-prev-mb">
                              <%--<i class="fa fa-angle-left"></i>--%>
                            </div>
                            <div class="swiper-button-next swiper-button-black bx bx-next-mb">
                              <%--<i class="fa fa-angle-right"></i>--%>
                            </div> 
                        </div>
                        <div class="swiper-wrapper event_slider">
                        </div>   
                    </div>

                    <asp:Panel ID="pan_Single_2" runat="server" Visible="True">
                        <div class="share m-share" style="<%= (IsBankDeal ? "display:none;" : "" )%>">
                            <asp:Panel ID="pan_m_share" runat="server">
                                <div class="clearfix mar-center">
                                    <div class="share_text">
                                        分享送<span class="bigtext">500</span>元
                                    </div>
                                    <div class="share_btn_box"></div>
                                    <a onclick='return Ppon.module.OpenSharePopWindow("<%=ResolveUrl("~/ppon/fbshare_proxy.aspx?type=facebook&bid=") + BusinessHourId%>", "fb");' class="m-fbBtn"></a>
                                    <a onclick='return Ppon.module.OpenSharePopWindow("<%=ResolveUrl("~/ppon/fbshare_proxy.aspx?type=line&bid=") + BusinessHourId + "&itemname=" + Server.UrlEncode(AppTitle)%>", "line");' class="m-lineBtn"></a>
                                </div>
                                <div class="share-tip">
                                    親友用你分享的連結購買並核銷成功，就送你500元！
                            <a target="_blank" href="<%=ResolveUrl("~/Ppon/NewbieGuide.aspx?id=share")%>">詳見常見問題</a>
                                </div>
                            </asp:Panel>
                        </div>
                        <div class="clear"></div>
                        <div class="share-popW" style="display: none">
                            <div class="share-popZone">
                                <h3>分享送500元！</h3>
                                <hr>
                                <p>
                                    登入後再分享，<br>
                                    就有機會賺500元唷！
                                </p>
                                <div class="text-center">
                                    <input onclick='Ppon.module.RedirectLoginPage("<%= SystemConfig.SiteUrl.Replace("http://", "https://").Replace("/lksite", "").Replace("/releasebranch", "") + FormsAuthentication.LoginUrl%>    ")' type="submit" class="btn btn-large btn-primary m-btn100" value="登入分享賺500">
                                    <input onclick='Ppon.module.ShareWithOutLogin("<%=AppTitle%>    ", "<%=string.Format(@"{0}/{1}/{2}", SystemConfig.SiteUrl, CityId, BusinessHourId)%>    ")' type="submit" class="btn btn-large m-btn100" value="純分享">
                                </div>
                            </div>
                            <div class="share-popX" onclick="return Ppon.module.CloseSharePopWindow();"></div>
                        </div>
                        <div id="Entry" class="clearfix">
                            <div id="EntryContent">
                                <div id="EntryContentLeft" class="EntryContentLeft">
                                    <div class="EntryTitle PublicwelfareTBk">
                                        <a name="otTOP4" id="otTOP4"></a><a name="DealDetail"></a>
                                        優惠方案
                                    </div>
                                    <div class="Multi-grade-Setting content-menu">
                                        <div id="div_combodeallist2">
                                            <%                                                
                                                foreach (PponDealProductEntry entry in ProductEntries)
                                                {
                                            %>
                                                <div class="mgs-item-box" style="cursor: default;display: <%=entry.IsExpiredDeal ? "none" : "block" %>">
                                                    <div class="<%=entry.IsSoldOut ? "mgs-SoldOut" : "" %>">
                                                        <div class="mgs-content">
                                                            <div class="mgs-item-title">
                                                                <%=entry.IsEnableIsp ? "<span class='title-tag'>可超商取貨</span>" : string.Empty%>
                                                                <%=entry.Title%>
                                                            </div>
                                                            <div class="mgs-item-state">
                                                                <div class="mgs-sale rdl-a2wdis">
                                                                    <%=entry.Discount%>
                                                                </div>
                                                                <div class="mgs-sale oriprice">
                                                                    原價 $<%= entry.OrigPrice%>
                                                                </div>
                                                                <div class="mgs-sale">
                                                                    <%=entry.Freight %>
                                                                </div>
                                                                <div class="mgs-buyer mgs-brcolor-org ">
                                                                    <%=entry.AveragePriceTag%>&nbsp;<%=entry.AveragePrice%>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="mgs-price-box">
                                                            <div class="<%=entry.IsSoldOut ? "mgs-price sot-Grey" : "mgs-price" %>">
                                                                <%= entry.Price%>
                                                            </div>
                                                        </div>
                                                        <div class="clearfix">
                                                        </div>
                                                    </div>
                                                </div>
                                            <%
                                                }
                                            %>
                                        </div>
                                    </div>

                                    <asp:PlaceHolder ID="phdRelatedDeal" runat="server" Visible="false">
                                        <div id="Related">
                                            <div id="RelatedDeal" class="EntryTitle">
                                                前往相關優惠
                                            </div>
                                            <div class="EntryZone">

                                                <%--<asp:Literal ID="litRelatedDeal" runat="server"></asp:Literal>--%>
                                                <asp:Repeater ID="rptRelatedDeal" runat="server" OnItemDataBound="rptRelatedDeal_ItemDataBound">
                                                    <ItemTemplate>
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                         <span>
                                                                            <i class="fa fa-chevron-circle-right"></i>&nbsp;<asp:HyperLink style="color:blue;" ID="hypRelatedLink" runat="server"></asp:HyperLink><br />
                                                                         </span>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </div>
                                        </div>
                                    </asp:PlaceHolder>
                                    
                                    
                                    <div id="Dialog">
                                        <div id="DialogTop" class="EntryTitle">
                                            一姬說好康
                                        </div>
                                        <div class="EntryZone">
                                            <div id="Dialoginner">
                                                <asp:Literal ID="lrt" runat="server"></asp:Literal>
                                            </div>
                                            <div id="DialogBottom">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="EntryTitle PublicwelfareTBk">
                                        <a name="otTOP2" id="otTOP2"></a><a name="DealDetail"></a>
                                        詳細介紹
                                    </div>
                                    <div class="EntryZone clearfix">
                                        <div id="DetailinnerV2">
                                            <div id="detailDesc">
                                                <asp:Literal ID="litDesc" runat="server"></asp:Literal>
                                            </div>
                                        </div>
                                    </div>


                                    <%if (SystemConfig.IsVbsProposalNewVersion)
                                      { %>
                                    <asp:PlaceHolder ID="phdDetailDesc" runat="server">
                                        <div class="EntryTitle PublicwelfareTBk">
                                            <a name="otTOP5" id="otTOP5"></a>
                                            商品規格
                                        </div>
                                        <div class="EntryZone">
                                            <div id="DetailinnerDesc">
                                                <div id="detailProductSpec">
                                                    <asp:Literal ID="lps" runat="server"></asp:Literal>
                                                </div>
                                            </div>
                                        </div>
                                    </asp:PlaceHolder>
                                    <%}%>

                                    <div class="Detail_buy_box" id="DetailBuyBox" runat="server">
                                        <div class="price" style="<%= (IsBankDeal ? "visibility: hidden;": "" )%>">
                                            <span class="tsText">好康價</span><span class="bigprice"><asp:Literal ID="ldr2" runat="server"></asp:Literal>
                                                │ $<asp:Literal ID="lp2" runat="server"></asp:Literal></span><span class="redtext"><asp:Label
                                                    ID="lmutiple2" runat="server"></asp:Label></span>
                                        </div>
                                        <div class="buy_btn_box">
                                            <asp:Literal ID="lb2" runat="server"></asp:Literal>
                                        </div>
                                    </div>
                                    <span id="SellerAvailabilityLabel"></span>
                                    <div class="EntryTitle PublicwelfareTBk">
                                        相關標籤
                                    </div>
                                    <asp:Repeater id="repTags" runat="server" OnItemDataBound="repTags_ItemDataBound">
                                        <HeaderTemplate>
                                            <div class="EntryZone">
                                            <ul class="rule clearfix">
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <li><asp:HyperLink ID="link" runat="server" /></li>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </ul>                                        
                                            </div>                                            
                                        </FooterTemplate>
                                    </asp:Repeater>
                                    <div class="EntryTitle PublicwelfareTBk">
                                        <a name="otTOP1" id="otTOP1"></a>
                                        <asp:Literal ID="liEntryTitle" runat="server" Text="權益說明"></asp:Literal>
                                    </div>   
                                    <div class="EntryZone">
                                        <div class="dds">
                                            <asp:Literal ID="lit_DealTag" runat="server"></asp:Literal>
                                        </div>
                                        <div id="Detailinner" class="Equity">
                                            <asp:Literal ID="li" runat="server"></asp:Literal>
                                            <br />
                                            <asp:Literal ID="liEquivalent" runat="server">
                                        <font style="color: Red">●此憑證由等值購物金兌換之</font><br />
                                            </asp:Literal>
                                            <asp:Label ID="lblnotDeliveryIslands" runat="server" Text="●此宅配商品恕不適用離島、外島地區<br />"
                                                Font-Size="14px" ForeColor="Red" Visible="False"></asp:Label>
                                            <asp:Label ID="lblRegularProvision" runat="server" Text="●本憑證不得與其他優惠合併使用，且恕無法兌換現金<br />●好康憑證不限本人使用，使用時店家將會核對憑證編號，一組編號限用一次<br />"
                                                Font-Size="14px" Visible="False"></asp:Label>
                                            <asp:Label ID="lblDeliveryPponProvision" runat="server" Text="●本實體票券不得與其他優惠合併使用，且恕無法兌換現金<br />"
                                                Font-Size="14px" Visible="False"></asp:Label>
                                            <asp:Literal ID="liTaishin" runat="server" Visible="false">
                                                ●信託期間：發售日起一年內<br />
                                                ●本商品(服務)禮券所收取之金額，已存入發行人於台新銀行開立之信託專戶，專款專用。所稱專用，係指供發行人履行交付商品或提供服務義務使用<br />
                                            </asp:Literal>
                                            <asp:Literal ID="liHwatai" runat="server" Visible="false">
                                                ●信託期間：發售日起一年內<br />
                                                ●本商品(服務)禮券所收取之金額，已存入發行人於華泰銀行開立之信託專戶，專款專用。所稱專 用，係指供發行人履行交付商品或提供服務義務使用<br />
                                            </asp:Literal>
                                            <asp:Literal ID="liMohist" runat="server" Visible="false">
                                                ●信託期間：發售日起一年內<br />
                                                ●本商品(服務)禮券所收取之金額，已存入發行人於陽信銀行開立之信託專戶，專款專用。所稱專用，係指供發行人履行交付商品或提供服務義務使用<br />
                                                ●信託履約禮券查詢網址 https://trust.sunnybank.com.tw/ (於收到兌換券後次日10點起即可查詢)<br />
                                            </asp:Literal>
                                            <asp:Literal ID="liEntrustSell" runat="server" Visible="false">
                                                ●代收轉付收據將於核銷使用後開立<br />
                                            </asp:Literal>
                                            <asp:Literal ID="liGroupCoupon" runat="server" Visible="false">
                                        <font style="color: Red">●成套票券商品為整套/組購入，故僅限於所有套/組憑證均兌換完畢才能享有完整優惠</font><br />
                                        <font style="color: Red">●當有退貨情事發生時，將依其整套/組進貨規則進行剩餘面額價值進行退款</font><br />
                                            </asp:Literal>
                                        </div>
                                        <div class="Equitylink">
                                            <ul>
                                                <li class="rule">
                                                    <asp:HyperLink ID="hd" runat="server" ToolTip="(點此看詳細使用說明)" Target="_blank">查看好康憑證使用條款</asp:HyperLink></li>
                                                <li class="shop">
                                                    <asp:HyperLink ID="hlkSellerInfo" runat="server" href='#SellerAvailabilityLabel' Text="查看店家資訊"></asp:HyperLink></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div id="SellerAvailability" runat="server" class="EntryTitle PublicwelfareTBk StoreInfo">
                                        <a name="otTOP3" id="otTOP3"></a>
                                        <asp:Literal ID="liSellerAvailabilityTitle" runat="server" Text="店家資訊"></asp:Literal>
                                    </div>
                                    <div id="storeInfoZone" class="EntryZone StoreInfo">
                                        <uc1:AvailabilityCtrl ID="avc" runat="server" />
                                    </div>

                                    

                                </div>
                                <asp:PlaceHolder ID="pDetailinner" runat="server">
                                    <div class="EntryTitle three-promise">
                                        安心三大保證
                                    </div>
                                    <div id="Div3" class="rd-guarantee EntryZone">
                                        <p class="h1_title">1.銀行信託保證</p>
                                        <p>
                                            17Life好康憑證由等值購物金兌換之。17Life購物金所收取之金額，<br />
                                            將分別存入發行人於台新銀行、陽信銀行開立之信託專戶，專款專用。<br />
                                            更詳細的17Life購物金使用規範內容，請查閱<a href="<%=ResolveUrl("~/newmember/privacypolicy.aspx?p=f")%>" style="font-family: 微軟正黑體; color: #00F;"
                                                target="_blank">17Life服務條款</a>。
                                        </p>
                                        <br />
                                        <p class="h1_title">2.全程退費保證</p>
                                        <p>
                                            在好康憑證優惠期間內：尚未到店兌換的好康憑證，可辦理全額退費，作業手續費$0。<br />
                                            超過好康憑證優惠期間：尚未到店兌換的好康憑證，可辦理全額退費，作業手續費$0。<br />
                                            ※ 部分好康憑證所指稱之內容為依據特定單一時間與特定場合所提供之服務 ( 包括但不限於演唱會、 音樂會或展覽 )，當好康憑證持有人因故未能於該特定時間與特定場合兌換該服務，恕無法要求退費
                                    或另行補足差額重現該服務。<br />
                                            一般非憑證類宅配商品：鑑賞期七天內可享有全額退費。
                                        </p>
                                        <br />
                                        <p class="h1_title">3.店家風險保證</p>
                                        <p>
                                            若店家於團購優惠兌換期內倒閉或因故無法提供商品或服務，則消費者原支付之金額<br />
                                            將由 17Life 全額退費。
                                        </p>
                                    </div>
                                </asp:PlaceHolder>
                            </div>
                        </div>
                    </asp:Panel>
                </div>
                <!--Left end-->
                <div id="Rightarea">
                    <div id="Stickers">
                        <ucP:Paragraph ID="paragraph1" SetContentName="/ppon/default.aspx_paragraph1" runat="server" />
                    </div>
                    <div class="inside_AD" style="display: none;">
                        <ucR:RandomParagraph ID="insideAD" runat="server" />
                    </div>
                    <div id="active_block" style="margin-bottom: 10px">
                        <ucR:RandomParagraph ID="active" runat="server" />
                    </div>
                    <div id="news_block">
                        <ucR:RandomParagraph ID="news" runat="server" />
                    </div>

					<%=LunchKingSite.WebLib.Component.WebHelper.PageBlock("新好友限定好康") %>

                    <div id="saleB_block">
                        <ucR:RandomParagraph ID="saleB" runat="server" />
                    </div>

                    <div id="Sidedeal" class="side_block">
                        <div class="side_block_title">
                            <i class="fa fa-star fa-fw"></i>今日熱銷
                        </div>
                        <div class="side_item_wrap">
                            <%
                                int hotIndex = 0;
                                foreach (var item in ViewMultipleHotDeals)
                                {
                                    hotIndex += 1;
                            %><div class="side_item">
                                <a class="open_new_window" href="<%=ResolveUrl(string.Format("~/deal/{1}?cid={0}", item.deal.CityID, item.deal.PponDeal.BusinessHourGuid))%>" target="_blank">
                                    <div class="item_sitcker">
                                        <img src="<%= item.EveryDayNewDeal %>" />
                                    </div>
                                    <div class="item_activeLogo" style="display: block">
                                        <%= item.deal.PponDeal.PromoImageHtml %>
                                    </div>
                                    <div class="side_item_pic">
                                        <span class="num_hot"><%=hotIndex%></span>
                                        <img src="<%=item.deal.PponDeal.DefaultDealImage %>" alt="<%=(hotIndex>5)?(string.IsNullOrEmpty(item.deal.PponDeal.PicAlt))?(string.IsNullOrEmpty(item.deal.PponDeal.AppTitle))? item.deal.PponDeal.CouponUsage:item.deal.PponDeal.AppTitle:item.deal.PponDeal.PicAlt:string.Empty %>" />
                                        <span class="side_item_soldout_wrap" style='<%= ((item.deal.PponDeal.OrderedQuantity >= item.deal.PponDeal.OrderTotalLimit) ? "": "display: none")%>'>
                                            <div class="item_soldout_220">
                                            </div>
                                        </span>
                                    </div>

                                    <div class="side_item_deal">
                                        <%if(!string.IsNullOrEmpty(item.clit_CityName2)) {%>
                                        <span class="tag_place">
                                            <%=item.clit_CityName2 %>
                                        </span>
                                        <%}%>
                                        <div class="tag_subtitle"><%=item.deal.PponDeal.ItemName%></div>
                                    </div>

                                    <div class="side_item_price">
                                        <span class="discount" style="color: rgb(153, 153, 153);">
                                            <%=item.discount_1 %>
                                        </span>
                                        <span class="price" style="color: rgb(191, 0, 0);">$<%= CheckZeroPriceToShowPrice(item.deal.PponDeal).ToString("F0")%>元<%= (item.deal.PponDeal.BusinessHourStatus & (int)(BusinessHourStatus.ComboDealMain)) > 0 ? "起" : string.Empty%></span>
                                        <span class="tag_buycounter" style="color: rgb(153, 153, 153);"><%= OrderedQuantityHelper.Show(item.deal.PponDeal, OrderedQuantityHelper.ShowType.MStyleDeal)%></span>
                                        <span class="btn_buy btn-primary">馬上看</span>
                                    </div>

                                </a>
                            </div>
                            <%
                                }
                            %>
                        </div>
                    </div>

                    <div id="SideTodaydeal" class="side_block">
                        <div class="side_block_title">
                            <i class="fa fa-star fa-fw"></i>最新上檔
                        </div>
                        <div class="side_item_wrap">
                            <%
                                foreach (var item in ViewMultipleTodayDeals)
                                {
                            %><div class="side_item">
                                <a class="open_new_window" href="<%=ResolveUrl(string.Format("~/deal/{1}?cid={0}", item.deal.CityID, item.deal.PponDeal.BusinessHourGuid))%>" target="_blank">
                                    <div class="item_sitcker">
                                        <img src="<%= item.EveryDayNewDeal %>" />
                                    </div>
                                    <div class="item_activeLogo" style="display: block">
                                        <%= item.deal.PponDeal.PromoImageHtml %>
                                    </div>
                                    <div class="side_item_pic">
                                        <img src="<%=item.deal.PponDeal.DefaultDealImage %>" alt="<%=(string.IsNullOrEmpty(item.deal.PponDeal.PicAlt))?(string.IsNullOrEmpty(item.deal.PponDeal.AppTitle))? item.deal.PponDeal.CouponUsage:item.deal.PponDeal.AppTitle:item.deal.PponDeal.PicAlt %>" />
                                        <span class="side_item_soldout_wrap" style='<%= ((item.deal.PponDeal.OrderedQuantity >= item.deal.PponDeal.OrderTotalLimit) ? "": "display: none")%>'>
                                            <div class="item_soldout_220">
                                            </div>
                                        </span>
                                    </div>

                                    <div class="side_item_deal">
                                        <%if(!string.IsNullOrEmpty(item.clit_CityName2)) {%>
                                        <span class="tag_place">
                                            <%=item.clit_CityName2 %>
                                        </span>
                                        <%}%>
                                        <div class="tag_subtitle"><%=item.deal.PponDeal.ItemName%></div>
                                    </div>

                                    <div class="side_item_price">
                                        <span class="discount" style="color: rgb(153, 153, 153);">
                                            <%=item.discount_1 %>
                                        </span>
                                        <span class="price" style="color: rgb(191, 0, 0);">$<%= CheckZeroPriceToShowPrice(item.deal.PponDeal).ToString("F0")%>元<%= (item.deal.PponDeal.BusinessHourStatus & (int)(BusinessHourStatus.ComboDealMain)) > 0 ? "起" : string.Empty%></span>
                                        <span class="tag_buycounter" style="color: rgb(153, 153, 153);"><%= OrderedQuantityHelper.Show(item.deal.PponDeal, OrderedQuantityHelper.ShowType.MStyleDeal)%></span>
                                        <span class="btn_buy btn-primary">馬上看</span>
                                    </div>

                                </a>
                            </div>
                            <%
                                }
                            %>
                        </div>
                    </div>

                    <div id="SideLastDaydeal" class="side_block">
                        <div class="side_block_title">
                            <i class="fa fa-star fa-fw"></i>最後倒數
                        </div>
                        <div class="side_item_wrap">
                            <%
                                foreach (var item in ViewMultipleLastDayDeals)
                                {
                            %><div class="side_item">
                                <a class="open_new_window" href="<%=ResolveUrl(string.Format("~/deal/{1}?cid={0}", item.deal.CityID, item.deal.PponDeal.BusinessHourGuid))%>" target="_blank">
                                    <div class="item_sitcker">
                                        <img src="<%= item.EveryDayNewDeal %>" />
                                    </div>
                                    <div class="item_activeLogo" style="display: block">
                                        <%= item.deal.PponDeal.PromoImageHtml %>
                                    </div>
                                    <div class="side_item_pic">
                                        <img src="<%=item.deal.PponDeal.DefaultDealImage %>" alt="<%=(string.IsNullOrEmpty(item.deal.PponDeal.PicAlt))?(string.IsNullOrEmpty(item.deal.PponDeal.AppTitle))? item.deal.PponDeal.CouponUsage:item.deal.PponDeal.AppTitle:item.deal.PponDeal.PicAlt %>" />
                                        <span class="side_item_soldout_wrap" style='<%= ((item.deal.PponDeal.OrderedQuantity >= item.deal.PponDeal.OrderTotalLimit) ? "": "display: none")%>'>
                                            <div class="item_soldout_220">
                                            </div>
                                        </span>
                                    </div>

                                    <div class="side_item_deal">
                                        <%if(!string.IsNullOrEmpty(item.clit_CityName2)) {%>
                                        <span class="tag_place">
                                            <%=item.clit_CityName2 %>
                                        </span>
                                        <%}%>
                                        <div class="tag_subtitle"><%=item.deal.PponDeal.ItemName%></div>
                                    </div>

                                    <div class="side_item_price">
                                        <span class="discount" style="color: rgb(153, 153, 153);">
                                            <%=item.discount_1 %>
                                        </span>
                                        <span class="price" style="color: rgb(191, 0, 0);">$<%= CheckZeroPriceToShowPrice(item.deal.PponDeal).ToString("F0")%>元<%= (item.deal.PponDeal.BusinessHourStatus & (int)(BusinessHourStatus.ComboDealMain)) > 0 ? "起" : string.Empty%></span>
                                        <span class="tag_buycounter" style="color: rgb(153, 153, 153);"><%= OrderedQuantityHelper.Show(item.deal.PponDeal, OrderedQuantityHelper.ShowType.MStyleDeal)%></span>
                                        <span class="btn_buy btn-primary">馬上看</span>
                                    </div>

                                </a>
                            </div>
                            <%
                                }
                            %>
                        </div>
                    </div>


                    <div class="side_block">
                        <div class="side_block_title">
                            <i class="fa fa-facebook-square fa-fw"></i>17Life粉絲團
                        </div>
                        <div class="side_block_ad">
                            <div id="FacebookareaContent" style="padding-bottom: 0px">
                                <iframe id="fbfans" data-src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2F17life.com.tw&amp;send=false&amp;layout=standard&amp;width=230&amp;show_faces=true&amp;action=like&amp;colorscheme=light&amp;font=arial&amp;height=80"
                                    scrolling="no" frameborder="0" style="border: none; overflow: hidden; width: 230px; padding-bottom: 0px; padding-left: 2px; height: 70px;"
                                    allowtransparency="true"></iframe>
                                <br />
                                <a href="https://www.facebook.com/17life.com.tw" target="_blank">
                                    <img src="<%= ResolveUrl("~/Themes/PCweb/images/goto_fbpage.png")%>" width="202" height="28"
                                        border="0" alt="" />
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="side_block">
                        <div class="side_block_title">
                            <i class="fa fa-users fa-fw"></i>合作提案
                        </div>
                        <div class="side_block_ad">
                            <div id="Shopareatitle">
                                <ul>
                                    <li style="font-size:18px;font-weight:bold;">手牽手，力量比較大</li>
                                    <li>17life 竭力在尋找各種優質、有趣的店家及產品，為我們的平台帶來更多生命力。</li>
                                    <li>若您有任何有趣的合作方案，我們都非常歡迎並誠摯的期待來自你的提案。</li> 
                                    <li>如對廣告行銷、公益合作有興趣者，也歡迎跟我們提案噢。</li> 
                                </ul>
                            </div>
                            <div style="width:100%;text-align:center;margin-top:15px;margin-bottom:5px;">
                                <a class="btn btn-primary btn-primary-flat " style="font-size:18px;" href="<%=SystemConfig.SiteUrl+"/Ppon/ContactUs"%>">填寫提案表</a>
                            </div>
                        </div>
<%--                        <div class="side_block_ad">
                            <div id="Div4">
                                歡迎各類企業及店家與我們合作，您只要準備最優質的產品/服務，以及消費者至上的誠意，就可以在17Life的平台上，精準行銷，一起賣到翻~
                        <br />
                                <br />
                                如對廣告行銷、公益合作有興趣者，也歡迎跟我們提案。
                            </div>
                            <a class="ShopBtn" href="<%=SystemConfig.SiteUrl%>/Ppon/ContactUs.aspx"></a>
                            <p id="ShopinnerIMG">
                                將一卡車的客戶送到您面前
                            </p>
                        </div>--%>
                    </div>
                </div>
            </div>
            <div id="Referral" style="display: none">
                <div id="ReturnClose" style="cursor: pointer; margin-top: 3px; margin-right: 3px"
                    onclick="$.unblockUI();return false;">
                </div>
                <div id="ReferralContent">
                    <div class="Referralinvite">
                        <h1>邀請親友好康有獎</h1>
                    </div>
                    <div id="ReferralTitle">
                        <span class="icon-smile-o-2"></span>
                        <p>以下是您的專屬邀請連結</p>
                    </div>
                    <!--ReferralTitle-->
                    <div id="ReferralLinkarea">
                        <asp:TextBox ID="tbx_ReferralShare" ClientIDMode="Static" runat="server" ReadOnly="true"></asp:TextBox>
                    </div>
                    <div id="ReferralStep" style="text-align: left;">
                        <li>
                            <span class="list-step">步驟1</span>
                            複製上面的邀請連結
                        </li>
                        <li>
                            <span class="list-step">步驟2</span>
                            把連結傳給好友
                        </li>
                        <li>
                            <span class="list-step">步驟3</span>
                            親友點連結後完成購買並核銷，宅配商品過鑑賞期且無退貨
                        </li>
                        <li>
                            <span class="list-step">步驟4</span>
                            您拿到推薦獎勵，折價券500元
                            <span class="list-warning">(請注意使用期限)</span>
                        </li>
                    </div>

                    <div id="Referralexplain">
                        <li>您可使用下圖fb分享，或是複製分享連結並轉貼，</li>
                        <li>
                            <span class="list-point">邀請次數無上限～</span>
                            分享越多賺越多喔！
                        </li>
                        <li>
                            <span class="list-point">訂單金額50元(含)以下恕不適用此折價券贈送活動，詳見<a target="_blank" href="<%=ResolveUrl("~/Ppon/NewbieGuide.aspx?id=share")%>">常見問題</a>
                            </span>
                        </li>
                    </div>

                    <div id="referral_FB_area">
                        <% if (!SystemConfig.NewShareLinkEnabled)
                            { %>
                        <asp:HyperLink ID="hhf" runat="server" Target="_blank">
                            <img src="<%= ResolveUrl("~/Themes/PCweb/images/FB_Btn.png")%>" />
                        </asp:HyperLink>
                        <% }
                            else
                            { %>
                        <a onclick="fbShareWindow = window.open(this.href, 'fbShareWindow', 'width=630,height=360','left=' + fbJumpWindowLeft + ',top=' + fbJumpWindowTop);ShareFB();" style="cursor: pointer;">
                            <img src="<%= ResolveUrl("~/Themes/PCweb/images/FB_Btn.png")%>" />
                        </a>
                        <% } %>
                    </div>
                </div>
            </div>
            <div id="blockMap" class="Multi-grade-Setting" style="height: 100%; border: 0px; display: none;">
                <div id="map" style="height: 100%; background: transparent;">
                    <iframe id="iMap" style="width: 100%; height: 100%;"></iframe>
                </div>
                <div class="MGS-XX" onclick="blockUIClose(this);" style="cursor: pointer;">
                    <input type="button" class="btn btn-large btn-primary" value="關閉">
                </div>
            </div>
            <div id="combodeals" class="Multi-grade-Setting" style="display: none">
                <div class="mgs-boxtitle">
                    請選擇好康
                </div>
                <div id="div_combodeallist">                    
                    <% foreach (PponDealProductEntry entry in ProductEntries)
                        { %>                    
                        <a href="<%= SystemConfig.SSLSiteUrl+"/ppon/buy.aspx?bid=" + entry.Bid%>" onclick="<%= entry.IsSoldOut ? "return false;" : string.Empty%>">
                            <div class="mgs-item-box" style="display: <%= entry.IsExpiredDeal ? "none" : "block"%>">
                                <div class="<%= entry.IsSoldOut ? "mgs-SoldOut" : string.Empty%>">
                                    <div class="mgs-content">
                                        <div class="mgs-item-title">
                                            <%= entry.IsEnableIsp ? "<span class='title-tag'>可超商取貨</span>" : string.Empty%><%= entry.Title%>
                                        </div>
                                        <div class="mgs-item-state">
                                            <div class="mgs-sale rdl-a2wdis">
                                                <%=entry.Discount%>
                                            </div>
                                            <div class="mgs-sale">
                                                原價 $<%= entry.OrigPrice%>
                                            </div>
                                            <div class="<%=entry.IsSoldOut ? "mgs-buyer mgs-brcolor-org sot-Grey" : "mgs-buyer mgs-brcolor-org"%>">
                                                <%=entry.AveragePriceTag%>&nbsp;<%=entry.AveragePrice%>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mgs-price-box">
                                        <div class="<%= entry.IsSoldOut ? "mgs-price sot-Grey" : "mgs-price"%>">
                                            <%= entry.Price%>
                                        </div>
                                    </div>
                                    <div class="clearfix">
                                    </div>
                                </div>
                            </div>
                        </a>
                    <% } %>
                </div>
                <div class="MGS-XX" onclick="blockUIClose(this);">
                    <input type="button" class="btn btn-large btn-primary" value="返回">
                </div>
                <asp:Panel ID="panelComboDealCount" runat="server" CssClass="Multi-grade-Setting-CEN-farm"
                    Visible="false">
                </asp:Panel>
            </div>
            <div style="clear: both">
            </div>
        </asp:PlaceHolder>
        <div id="newpoplogin" style="display: none">
            <uc1:LoginPanel ID="LoginPanel1" runat="server" />
        </div>
    </div>
    <div id="mobile-indicator" style="height: 1px; width: 1px;"></div>
    <div id="coupon_wrapper" style="display: none;">
        <div class="close_layer"><i class="fa fa-times"></i></div>
        <div id="renderpage">
            <%--<%WebFormMVCUtil.RenderAction("GetDiscountCode", "Event", new { brandId=13, bid = BusinessHourId }); %>--%>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="LastLoadJs" runat="server">


    <script type="text/javascript">
        var winWidth = $(window).width();
        if (winWidth < 560) {
            var _iframe = $("iframe");
            $.each(_iframe, function (idx, obj) {
                var _iw = $(obj).attr("width");
                var _ih = $(obj).attr("height");
                if (_iw > winWidth) {
                    var w = ((_iw - (winWidth - 50)) / _iw);
                    var h = parseInt(_ih) - parseInt(_ih * w);
                    $(obj).attr("width", winWidth - 50);
                    $(obj).attr("height", parseInt(h));
                }
            })
            //alert(_iframe.length);
        } else {
            var _iframe = $("iframe");
            $.each(_iframe, function (idx, obj) {
                var _iw = $(obj).attr("width");
                var _ih = $(obj).attr("height");
                if (_iw < 650) {
                    var w = ((650 - _iw) / _iw);
                    var h = parseInt(_ih) + parseInt(_ih * w);
                    $(obj).attr("width", 650);
                    $(obj).attr("height", parseInt(h));
                }
            })
        }
    </script>
    
    <script type="text/javascript">
        //e7data Code
        !function (w, d, e, v, q, t, s) {
            if (w.e7qq) return;
            q = w.e7qq = function () {
                q.queue.push(arguments[0]);
            }
            q.queue = [];
            t = d.createElement(e);
            t.async = !0;
            t.src = v;
            s = d.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t, s);
        }(window, document, 'script', '/tools/js/e7e7.js?0.2');
        e7qq(['view', "deal",  '<%=BusinessHourId%>', location.href, '<%=Request.UrlReferrer%>']);

        //if (getCookie('_li')) {
        //    e7qq('login', { uid: getCookie("_li") });
        //    setCookie('_li', '', -1);
        //}
    </script>
</asp:Content>


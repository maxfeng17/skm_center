﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LunchKingSite.Web.Ppon
{
    public partial class OnlineEDM : System.Web.UI.Page
    {
        #region property

        public ISysConfProvider SystemConfig
        {
            get { return ProviderFactory.Instance().GetConfig(); }
        }

        public int EdmID
        {
            get
            {
                int eid;
                if (!string.IsNullOrEmpty(Request.QueryString["eid"]) && int.TryParse(Request.QueryString["eid"], out eid))
                {
                    return eid;
                }
                else
                {
                    return 0;
                }
            }
        }

        public string Pass
        {
            get
            {
                if (!string.IsNullOrEmpty(Request.QueryString["pa"]))
                {
                    return Request.QueryString["pa"];
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        private string cpa;

        public string Cpa
        {
            get
            {
                if (!string.IsNullOrEmpty(Request.QueryString["cpa"]))
                {
                    return Request.QueryString["cpa"];
                }
                else
                {
                    return cpa;
                }
            }
            set
            {
                cpa = value;
            }
        }

        public string SiteUrl
        {
            get
            {
                return SystemConfig.SiteUrl;
            }
        }

        public int Cid
        {
            get
            {
                int cid;
                if (!string.IsNullOrEmpty(Request.QueryString["cid"]) && int.TryParse(Request.QueryString["cid"], out cid))
                {
                    return cid;
                }
                else
                {
                    return 0;
                }
            }
        }

        public DateTime DeliveryDate { get; set; }

        private EasyDigitCipher cipher = new EasyDigitCipher();

        #endregion property

        #region page

        protected void Page_Load(object sender, EventArgs e)
        {

            Response.Redirect("../Ppon/OnlineNewEDM.aspx?eid=" + EdmID.ToString());

            if (!IsPostBack)
            {
                string pass = string.Empty;
                EdmMain main = EdmID != 0 ? EmailFacade.GetNewEdmMainById(EdmID) : EmailFacade.GetNewEdmMainByCityId(Cid);
                List<EdmDetail> items = EmailFacade.GetNewEdmDetailsByPid(main.Id);
                if (main.Id != 0 && items.Count > 0)
                {
                    bool check = true;

                    #region check date

                    if ((main.Type == (int)EdmMainType.Daily && main.DeliveryDate.AddHours(-1) > DateTime.Now) || (main.Type == (int)EdmMainType.Manual && main.DeliveryDate > DateTime.Now) || !main.Status)
                    {
                        //防止外人透過id抓取時間未到的EDM
                        if (!string.IsNullOrEmpty(Pass) && cipher.Decrypt(Pass, out pass) && pass == main.DeliveryDate.ToString("yyyyddMM"))
                        {
                            check = true;
                        }
                        else
                        {
                            //內部人員可依權限進入
                            if (!User.Identity.IsAuthenticated)
                            {
                                FormsAuthentication.RedirectToLoginPage();
                                return;
                            }
                            else if (!User.IsInRole("Administrator") && !User.IsInRole("Planning") && !User.IsInRole("Marketing"))
                            {
                                check = false;
                            }
                        }
                    }

                    #endregion check date

                    if (check)
                    {
                        InitPageWithContent(main, items);
                    }
                    else
                    {
                        Response.Redirect("default.aspx");
                    }
                }
                else
                {
                    Response.Redirect("default.aspx");
                }
            }
        }

        #endregion page

        #region method

        private void InitPageWithContent(EdmMain main, List<EdmDetail> items)
        {
            this.Page.Title = Helper.GetMaxString(main.Subject, 45, "...");
            lab_CityName.Text = main.CityName;
            DeliveryDate = main.DeliveryDate;
            hyp_All.NavigateUrl = SiteUrl + "/" + main.CityId;
            EdmDetail detail;

            #region AD

            if (items.Any(x => x.Type == (int)EdmDetailType.AD && x.AdId.HasValue))
            {
                detail = items.First(x => x.Type == (int)EdmDetailType.AD);
                string html_content = HttpUtility.HtmlDecode(EmailFacade.GetNewEdmAdById(detail.AdId.Value).Body);
                if (string.IsNullOrEmpty(html_content))
                {
                    pan_AD.Visible = false;
                }
                else
                {
                    lit_AD.Text = html_content;
                }
            }
            else
            {
                pan_AD.Visible = false;
            }

            #endregion AD

            #region PEZ

            if (items.Any(x => x.Type == (int)EdmDetailType.PEZ))
            {
                detail = items.First(x => x.Type == (int)EdmDetailType.PEZ);
                CmsContent article = EmailFacade.GetPEZCmsContent("/edm/promote_pez");
                lit_PEZ.Text = article.Body;
            }
            else
            {
                pan_PEZ.Visible = false;
            }

            #endregion PEZ

            if (string.IsNullOrEmpty(Cpa))
            {
                Cpa = main.Cpa;
            }
            //依區塊種類設定相關資料
            AddEdmDetailTable(items, EdmDetailType.MainDeal_1, EdmDetailType.MainDeal_1_Items, pan_MainDeal1);
            AddEdmDetailTable(items, EdmDetailType.MainDeal_2, EdmDetailType.MainDeal_2_Items, pan_MainDeal2);
            AddEdmDetailTable(items, EdmDetailType.Area_1, EdmDetailType.Area_1_Items, pan_Area1);
            AddEdmDetailTable(items, EdmDetailType.Area_2, EdmDetailType.Area_2_Items, pan_Area2);
            AddEdmDetailTable(items, EdmDetailType.Area_3, EdmDetailType.Area_3_Items, pan_Area3);
            AddEdmDetailTable(items, EdmDetailType.PponPiinLife_1, EdmDetailType.PponPiinLife_Item_1, pan_PiinLife1);
            if (string.IsNullOrEmpty(dv_Hint.InnerText))
            {
                dv_Hint.InnerText = "吃喝玩樂三折起，優惠都在17Life！";
            }
            else
            {
                dv_Hint.InnerText = dv_Hint.InnerText.Remove(dv_Hint.InnerText.Length - 1, 1) + "，吃喝玩樂三折起，優惠都在17Life！";
            }
        }

        private void AddEdmDetailTable(List<EdmDetail> items, EdmDetailType maintype, EdmDetailType itemtype, Panel pan)
        {
            string titlecolor = string.Empty;

            if (items.Count(x => x.Type == (int)maintype) > 0)
            {
                EdmDetail detail;
                detail = items.First(x => x.Type == (int)maintype);
                if (detail.CityId == PponCityGroup.DefaultPponCityGroup.PBeautyLocation.CityId)
                {
                    titlecolor = "C82851";
                }
                else if (detail.CityId == PponCityGroup.DefaultPponCityGroup.Travel.CityId)
                {
                    titlecolor = "2D81E8";
                }
                else
                {
                    titlecolor = "BF0000";
                }

                int column = detail.ColumnNumber;
                for (int i = 0; i < detail.RowNumber; i++)
                {
                    DailyEdmItems table = (DailyEdmItems)Page.LoadControl("DailyEdmItems.ascx");
                    table.EdmDetailList = items.Where(x => x.Type == (int)itemtype).Skip(i * column).Take(column).ToList();
                    table.MainType = maintype;
                    table.ItemType = itemtype;
                    table.MainDetail = detail;
                    table.TitleColor = titlecolor;
                    table.SiteUrl = SiteUrl;
                    table.IsFirst = (i == 0);
                    table.Cpa = string.IsNullOrEmpty(Cpa) ? string.Empty : ("/" + Cpa);
                    table.ColumnNumber = column;
                    pan.Controls.Add(table);
                    table.DataBind();
                    if ((maintype == EdmDetailType.MainDeal_2 && table.EdmDetailList.Count > 0) || (maintype == EdmDetailType.MainDeal_1 && table.EdmDetailList.Count > 0))
                    {
                        int count = 1;
                        string maildealStr = string.Empty;
                        foreach (var edmdetail in table.EdmDetailList)
                        {
                            if (count > 3)
                            {
                                continue;
                            }

                            if (edmdetail.Title.Contains("【") && edmdetail.Title.Contains("】"))
                            {
                                int startWith = edmdetail.Title.IndexOf("【") + 1;
                                int endWith = edmdetail.Title.IndexOf("】");
                                maildealStr += edmdetail.Title.Substring(startWith, endWith - startWith) + "╳";
                                count++;
                            }
                        }

                        if (maildealStr.Length > 0)
                        {
                            dv_Hint.InnerText += maildealStr;
                        }
                    }
                }
            }
        }

        #endregion method
    }
}
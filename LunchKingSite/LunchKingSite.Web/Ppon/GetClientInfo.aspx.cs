﻿using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LunchKingSite.Web.Ppon
{
    public partial class GetClientInfo : System.Web.UI.Page
    {
        public int MobileMaxWidth
        {
            get { return sp.MobileMaxWidth; }
        }

        protected static ISysConfProvider sp;

        public GetClientInfo()
        {
            sp = ProviderFactory.Instance().GetConfig();
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }
    }
}
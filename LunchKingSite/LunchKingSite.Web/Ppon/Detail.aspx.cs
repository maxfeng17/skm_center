﻿using System.Web.Caching;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Models;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.UI;
using LunchKingSite.WebLib.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.WebLib.Component;
using LunchKingSite.WebLib.Models.Ppon;
using System.IO;
using LunchKingSite.Core.ModelCustom;
using LunchKingSite.BizLogic.Models;
using log4net;

namespace LunchKingSite.Web.Ppon
{
    public partial class Detail : BasePage, IDetailPponView
    {
        #region event

        public event EventHandler CheckEventMail;

        #endregion event

        #region Props

        static System.Collections.Concurrent.ConcurrentDictionary<string, int> routeNameUsedStat = new System.Collections.Concurrent.ConcurrentDictionary<string, int>();

        static ILog logger = LogManager.GetLogger(typeof(Detail));

        #region Provider
        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        public static ISysConfProvider SystemConfig
        {
            get
            {
                return config;
            }
        }
        #endregion

        public string MicroDataJson { get; set; }

        public string ScupioDataJson { get; set; }

        public string ScupioCartPageDataJson { get; set; }

        public string GtagDataJson { get; set; }

        public string YahooDataJson { get; set; }

        public const string CPA_HOT_DEALS = "cpa-17_hotdeals";

        public int Bonus = 50;

        private DetailPponPresenter _presenter;

        public DetailPponPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }


        #region 檔次列表
        /// <summary>
        /// 今日熱銷
        /// </summary>
        private List<ViewMultipleSideDeal> _viewMultipleHotDeals;

        public List<ViewMultipleSideDeal> ViewMultipleHotDeals
        {
            get { return _viewMultipleHotDeals; }
        }

        /// <summary>
        /// 最新上檔
        /// </summary>
        private List<ViewMultipleSideDeal> _viewMultipleTodayDeals;

        public List<ViewMultipleSideDeal> ViewMultipleTodayDeals
        {
            get { return _viewMultipleTodayDeals; }
        }

        /// <summary>
        /// 最後倒數
        /// </summary>
        private List<ViewMultipleSideDeal> _viewMultipleLastDayDeals;

        public List<ViewMultipleSideDeal> ViewMultipleLastDayDeals
        {
            get { return _viewMultipleLastDayDeals; }
        }

        /// <summary>
        /// 優惠內容
        /// </summary>
        public List<PponDealProductEntry> ProductEntries { get; set; }

        #endregion

        #region city, category相關

        private int _dealCityId;

        /// <summary>
        /// 當前cityId
        /// </summary>
        public int CityId
        {
            get
            {
                return Master.SelectedCityId;
            }
            set
            {
                Master.SelectedPponCity = PponCityGroup.DefaultPponCityGroup.GetPponCityByCityId(value);
            }
        }

        public int CategoryID
        {
            get
            {
                string cat_querystring = string.Empty;
                int cat;
                if (Page.RouteData.Values["cat"] != null)
                {
                    cat_querystring = Page.RouteData.Values["cat"].ToString();
                }
                else if (!string.IsNullOrWhiteSpace(Request.QueryString["cat"]))
                {
                    cat_querystring = Request.QueryString["cat"];
                }

                if (!string.IsNullOrWhiteSpace(cat_querystring) && int.TryParse(cat_querystring, out cat))
                {
                    return cat;
                }
                else
                {
                    return CategoryManager.GetDefaultCategoryNode().CategoryId;
                }
            }
        }

        public int DealCityId
        {
            set
            {
                _dealCityId = value;
            }
            get
            {
                if (_dealCityId == 0)
                {
                    return Master.SelectedCityId;
                }
                else
                {
                    return _dealCityId;
                }
            }
        }

        public int PponCategory
        {
            get { return PponCityGroup.DefaultPponCityGroup.GetPponCityByCityId(DealCityId).CategoryId; }
        }

        public int TravelCategoryId
        {
            get
            {
                HttpCookie cookie = Request.Cookies.Get(LkSiteCookie.TravelCategoryId.ToString("g"));

                if (cookie == null)
                {
                    cookie = CookieManager.NewCookie(
                        LkSiteCookie.TravelCategoryId.ToString("g"),
                        CategoryManager.GetDefaultCategoryNode().CategoryId.ToString(),
                        DateTime.Now.AddDays(1)
                    );
                    Response.Cookies.Add(cookie);
                }

                int cid = int.TryParse(cookie.Value, out cid) ? cid : CategoryManager.GetDefaultCategoryNode().CategoryId;
                return cid == 0 ? CategoryManager.GetDefaultCategoryNode().CategoryId : cid;
            }
        }

        public int FemaleCategoryId
        {
            get
            {
                HttpCookie cookie = Request.Cookies.Get(LkSiteCookie.FemaleCategoryId.ToString("g"));

                if (cookie == null)
                {
                    cookie = CookieManager.NewCookie(
                        LkSiteCookie.FemaleCategoryId.ToString("g"),
                        CategoryManager.Default.FemaleTaipei.CategoryId.ToString(),
                        DateTime.Now.AddDays(1)
                    );
                    Response.Cookies.Add(cookie);
                }

                int cid = int.TryParse(cookie.Value, out cid) ? cid : CategoryManager.Default.FemaleTaipei.CategoryId;
                return cid == 0 ? CategoryManager.Default.FemaleTaipei.CategoryId : cid;
            }
        }

        public string CategoryName
        {
            get
            {
                Category cate = CategoryManager.CategoryGetById(CategoryID);
                return cate != null ? cate.Name : string.Empty;
            }
        }

        public PponDealEvaluateStar EvaluateAverage { get; set; }

        public List<CategoryNode> DealChannelList { get; set; }

        /// <summary>
        /// 城市CityId
        /// </summary>
        public int SelectedCityId
        {
            get { return Master.SelectedCityId; }
        }

        /// <summary>
        /// 頻道CategoryId
        /// </summary>
        public int SelectedChannelId
        {
            get { return Master.SelectedChannelId; }
        }

        /// <summary>
        /// 區域categoryId
        /// </summary>
        public int SelectedRegionId
        {
            get { return Master.SelectedRegionId; }
        }
        #endregion  city, category相關

        public Guid BusinessHourId
        {
            get
            {
                string bid_querystring = string.Empty;
                Guid bGuid = Guid.Empty;
                if (Page.RouteData.Values["bid"] != null)
                {
                    bid_querystring = Page.RouteData.Values["bid"].ToString();
                }
                else if (!string.IsNullOrWhiteSpace(Request.QueryString["bid"]))
                {
                    bid_querystring = Request.QueryString["bid"];
                }

                if (!string.IsNullOrWhiteSpace(bid_querystring))
                {
                    if (bid_querystring.Split(',').Length > 1)
                    {
                        bid_querystring = bid_querystring.Split(',')[0];
                    }

                    if (!Guid.TryParse(bid_querystring, out bGuid))
                    {
                        bGuid = Guid.Empty;
                    }
                }
                else if (ViewState["bid"] != null)
                {
                    bGuid = new Guid(ViewState["bid"].ToString());
                }

                return bGuid;
            }

            set
            {
                ViewState["bid"] = value;
            }
        }

        /// <summary>
        /// 檔號，目前先隱藏檔號判斷檔次，直接回傳-1
        /// </summary>
        public int BusinessHourUniqueId
        {
            get
            {
                return -1;
            }
            set
            {
                ViewState["uid"] = value;
            }
        }

        /// <summary>
        /// rsrc參數
        /// </summary>
        public string Rsrc
        {
            get
            {
                if (Page.RouteData.Values["rsrc"] != null)
                {
                    return Page.RouteData.Values["rsrc"].ToString();
                }
                else if (!string.IsNullOrWhiteSpace(Request.QueryString["rsrc"]))
                {
                    return Request.QueryString["rsrc"];
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        /// <summary>
        /// Hami
        /// </summary>
        public string Hami
        {
            get
            {
                if (Page.RouteData.Values["rsrc"] != null && Page.RouteData.Values["rsrc"].ToString().ToLower() == "hinet_hammi")
                {
                    Session[LkSiteSession.Hami.ToString()] = Page.RouteData.Values["rsrc"].ToString().ToLower();
                }
                else if (!string.IsNullOrWhiteSpace(Request.QueryString["rsrc"]) && Request.QueryString["rsrc"].ToLower() == "hinet_hammi")
                {
                    Session[LkSiteSession.Hami.ToString()] = Request.QueryString["rsrc"].ToLower();
                }
                return Session[LkSiteSession.Hami.ToString()] != null ? Session[LkSiteSession.Hami.ToString()].ToString() : string.Empty;
            }
        }

        public CategorySortType SortType
        {
            get
            {
                if (Session["SortType"] == null)
                {
                    Session["SortType"] = CategorySortType.Default;
                }

                return (CategorySortType)Session["SortType"];
            }
            set
            {
                Session["SortType"] = value;
            }
        }

        public string GoogleApiKey
        {
            get
            {
                return ProviderFactory.Instance().GetConfig().GoogleMapsAPIKey;
            }
        }

        public string FB_AppId
        {
            get
            {
                return ProviderFactory.Instance().GetConfig().FacebookApplicationId;
            }
        }
        /// <summary>
        /// App Title or CouponUsage
        /// </summary>
        public string AppTitle { set; get; }
        /// <summary>
        /// meta og:title content
        /// </summary>
        public string OgTitle { set; get; }
        /// <summary>
        /// meta og:url content
        /// </summary>
        public string OgUrl { set; get; }
        /// <summary>
        /// meta og:description content
        /// </summary>
        public string OgDescription { set; get; }
        /// <summary>
        /// meta og:image content
        /// </summary>
        public string OgImage { set; get; }
        /// <summary>
        /// link rel=canonical href
        /// </summary>
        public string LinkCanonicalUrl { set; get; }
        /// <summary>
        /// link rel=image_src href
        /// </summary>
        public string LinkImageSrc { set; get; }

        public int Encores { get; set; }

        public string UserName
        {
            get
            {
                return Page.User.Identity.Name;
            }
        }

        public int UserId
        {
            get
            {
                var pponUser = User.Identity as PponIdentity;
                if (pponUser != null)
                {
                    return pponUser.Id;
                }
                return MemberFacade.GetUniqueId(Page.User.Identity.Name);
            }
        }

        //前端資料以商品的憑證宅配撈取20120314
        public DeliveryType[] RepresentedDeliverytype
        {
            get
            {
                return new DeliveryType[2] { DeliveryType.ToHouse, DeliveryType.ToShop };
            }
        }

        private bool isDeliverDeal = false;
        public bool IsDeliveryDeal
        {
            set
            {
                this.isDeliverDeal = value;
            }
            get
            {
                return this.isDeliverDeal;
            }
        }

        public bool TwentyFourHours { get; set; }

        public bool ShowEventEmail { get; set; }

        public string HamiTicketId
        {
            get
            {
                if (!string.IsNullOrWhiteSpace(Request.QueryString["TicketNo"]))
                {
                    return Request.QueryString["TicketNo"];
                }
                return null;
            }
        }

        public int? DealType { get; set; }

        public bool EntrustSell
        {
            set
            {
                liEntrustSell.Visible = value;
            }
        }

        public string PicAlt { get; set; }

        public bool isUseNewMember { get; set; }

        public DateTime Now
        {
            get
            {
                return DateTime.Now;
            }
        }

        public bool isMultipleMainDeals { get; set; }

        public bool BypassSsoVerify { get; set; }

        public IExpireNotice ExpireNotice
        {
            get
            {
                return PponExpireNotice;
            }
        }
        public bool IsNewMobileSetting
        {
            get { return config.IsNewMobileSetting; }
        }

        public string EdmPopUpCacheName { get; set; }

        public bool IsHiddenBanner { get; set; }

        public bool IsOpenNewWindow { get; set; }

        public bool IsMobileBroswer
        {
            get
            {
                return CommonFacade.ToMobileVersion();
            }
        }

        private bool _isKindDeal;

        public bool IsKindDeal
        {
            get
            {
                return _isKindDeal;
            }
            set
            {
                _isKindDeal = value;
            }
        }

        public decimal MinGross
        {
            get
            {
                return config.GrossMargin;
            }
        }

        public bool EnableGrossMarginRestrictions
        {
            get
            {
                return config.EnableGrossMarginRestrictions;
            }
        }

        public bool StarRatingEnabled
        {
            get { return config.StarRatingEnabled; }
        }
        public string DealArgs { set; get; }
        public string FPCvalue
        {
            get
            {
                return lp.Text.Substring(1);
            }
        }

        /// <summary>
        /// 熱銷檔次
        /// </summary>
        /// <param name="hotdeals"></param>
        public void SetHotSaleMutilpMainDeals(List<MultipleMainDealPreview> hotdeals)
        {
            this._viewMultipleHotDeals = SetMultipleSideDeal(hotdeals);
        }
        /// <summary>
        /// 今日開賣檔次
        /// </summary>
        /// <param name="toDaydeals"></param>
        public void SetToDayMutilpMainDeals(List<MultipleMainDealPreview> todaymultiplemaindeals)
        {
            this._viewMultipleTodayDeals = SetMultipleSideDeal(todaymultiplemaindeals);
        }
        /// <summary>
        /// 最後倒數檔次
        /// </summary>
        /// <param name="LastDaydeals"></param>
        public void SetLastDayMutilpMainDeals(List<MultipleMainDealPreview> lastdaymultiplemaindeals)
        {
            this._viewMultipleLastDayDeals = SetMultipleSideDeal(lastdaymultiplemaindeals);
        }


        private List<ViewMultipleSideDeal> SetMultipleSideDeal(List<MultipleMainDealPreview> deals)
        {
            List<ViewMultipleSideDeal> viewMultipleSideDeals = new List<ViewMultipleSideDeal>();
            foreach (var deal in deals)
            {
                ViewMultipleSideDeal viewSidePreview = new ViewMultipleSideDeal();
                PponCity pponcity = PponCityGroup.DefaultPponCityGroup.GetPponCityByCityId(deal.CityID);

                viewSidePreview.deal = deal;

                viewSidePreview.clit_CityName2 = ViewPponDealManager.DefaultManager.GetCityNameOrTravelPlace(
                pponcity, deal.PponDeal.BusinessHourGuid);

                if (_isKindDeal)
                {
                    viewSidePreview.discount_1 = "公益";
                }
                else if (Helper.IsFlagSet(deal.PponDeal.BusinessHourStatus, BusinessHourStatus.NoDiscountShown)
                      || Helper.IsFlagSet(deal.PponDeal.BusinessHourStatus, BusinessHourStatus.PriceZeorShowOriginalPrice))
                {
                    viewSidePreview.discount_1 = "特選";
                }
                else if (deal.PponDeal.ItemPrice == 0)
                {
                    if (CityId.Equals(PponCityGroup.DefaultPponCityGroup.Family.CityId) && deal.PponDeal.ExchangePrice.HasValue && deal.PponDeal.ExchangePrice.Value > default(decimal))
                    {
                        if (Helper.IsFlagSet(deal.PponDeal.BusinessHourStatus, BusinessHourStatus.NoDiscountShown))
                        {
                            viewSidePreview.discount_1 = "特選";
                        }
                        else
                        {
                            string discount_2 = ViewPponDealManager.GetDealDiscountString(Helper.IsFlagSet(deal.PponDeal.BusinessHourStatus, BusinessHourStatus.NoDiscountShown), CheckZeroPriceToShowPrice(deal.PponDeal), deal.PponDeal.ItemOrigPrice);
                            viewSidePreview.discount_1 = discount_2 + "折";
                        }
                    }
                    else
                    {
                        viewSidePreview.discount_1 = "優惠";
                    }
                }
                else
                {
                    string discount = ViewPponDealManager.GetDealDiscountString(Helper.IsFlagSet(deal.PponDeal.BusinessHourStatus, BusinessHourStatus.NoDiscountShown), CheckZeroPriceToShowPrice(deal.PponDeal), deal.PponDeal.ItemOrigPrice);
                    int length = discount.Length;
                    if (length > 0)
                    {
                        string discount_2 = discount.Substring(0, 1);
                        if (discount.IndexOf('.') != -1 && length > 2)
                        {
                            discount_2 += discount.Substring(1, length - 1);
                        }
                        viewSidePreview.discount_1 = discount_2 + "折";
                    }
                }                
                viewSidePreview.EveryDayNewDeal = PponFacade.GetEveryDayNewDealFlag(deal.PponDeal.BusinessHourGuid);
                viewMultipleSideDeals.Add(viewSidePreview);
            }

            return viewMultipleSideDeals;
        }

        private bool _isBankDeal;

        public bool IsBankDeal
        {
            get
            {
                return _isBankDeal;
            }
            set
            {
                _isBankDeal = value;
            }
        }
        PcwebMainDealModel _model = new PcwebMainDealModel();
        public PcwebMainDealModel Model
        {
            get
            {
                return _model;
            }
        }

        public string ExpireRedirectDisplay { get; set; }
        public string ExpireRedirectUrl { get; set; }
        public bool? UseExpireRedirectUrlAsCanonical { get; set; }

        public bool ForceReload
        {
            get
            {
                return Request["reload"] != null;
            }
        }

        #endregion Props

        #region interface methods

        public void PopulateDealInfo(IViewPponDeal mainDeal, MemberLinkCollection memberLinks, Dictionary<Guid, string> relatedDeals)
        {
            //CheckMultipleMainDealPanel();
            //var goodWords = new List<string> { Phrase.LooksGood, Phrase.IHadHeardThat, Phrase.LetUsGoTogether, Phrase.PeopleSayThatStoreClerksAreHot, Phrase.RealBargain };
            //String goodWord = goodWords.ElementAt(new Random().Next(goodWords.Count));
            p1.Visible = true;
            _isKindDeal = Helper.IsFlagSet(mainDeal.GroupOrderStatus ?? 0, GroupOrderStatus.KindDeal);


            #region 檔次 Icon&權益說明 標籤


            lit_DealTag.Text = mainDeal.IconTags;

            repTags.DataSource = mainDeal.Tags;
            repTags.DataBind();
            #endregion

            //銀行檔次不顯示欄位
            if (mainDeal.IsBankDeal != null && mainDeal.IsBankDeal.Value)
            {
                _isBankDeal = true;
                
            }

            decimal zero_price = CheckZeroPriceToShowPrice(mainDeal);            

            //6/9新增 (0元好康 僅顯示一張圖 不輪播)
            if (decimal.Equals(0, mainDeal.ItemPrice))
            {
                Model.MainPics.AddRange(
                    ImageFacade.GetMediaPathsFromRawData(mainDeal.EventImagePath, MediaType.PponDealPhoto).Where((x, i) => i == 0));
            }
            else
            {
                Model.MainPics.AddRange(
                    ImageFacade.GetMediaPathsFromRawData(mainDeal.EventImagePath, MediaType.PponDealPhoto).Where((x, i) => i != 1 && i != 2));
            }

            Model.MainDeal = mainDeal;
            Model.ItemName = mainDeal.ItemName;
            Model.EventTitle = mainDeal.EventTitle;
            Model.TagHtml = PponFacade.GetDealIconHtmlContentList(mainDeal, 2);
            
            lp.Text = lp2.Text = zero_price.ToString("f0");

            switch (mainDeal.DiscountType)
            {
                case (int)DiscountType.None:
                    lp.Text = "$" + lp.Text;
                    break;
                case (int)DiscountType.DiscountPrice:

                    lp.Text = "$" + (mainDeal.DiscountValue ?? 0).ToString("f0");
                    break;
                case (int)DiscountType.DiscountCash:
                    lp.Text = "折抵$" + (mainDeal.DiscountValue ?? 0).ToString("f0");
                    break;
                case (int)DiscountType.DiscountPercent:
                    lp.Text = ((double)(mainDeal.DiscountValue ?? 0) / 100 * 10).ToString("#.#") + "折券";
                    break;
            }

            SetupOrderableElements(mainDeal);
            //設定動態內容指定 City(指頻道)
            SetRandomParagraphCity(mainDeal.CityIds);
            string rateStr = string.Empty;
            if (!decimal.Equals(0, mainDeal.ItemOrigPrice))
            {
                if (_isKindDeal)
                {
                    ldr2.Text = "公益";
                }
                else if (Helper.IsFlagSet(mainDeal.BusinessHourStatus, BusinessHourStatus.NoDiscountShown)
                      || Helper.IsFlagSet(mainDeal.BusinessHourStatus, BusinessHourStatus.PriceZeorShowOriginalPrice)
                      || mainDeal.ItemPrice == mainDeal.ItemOrigPrice)
                {
                    ldr2.Text = "特選";
                }
                else if (mainDeal.ItemPrice > 0)
                {
                    rateStr = ViewPponDealManager.GetDealDiscountString(Helper.IsFlagSet(mainDeal.BusinessHourStatus, BusinessHourStatus.NoDiscountShown), mainDeal.ItemPrice, mainDeal.ItemOrigPrice);
                    ldr2.Text = rateStr + "<span class=\"smalltext\">折</span>";
                }
                else
                {
                    if (CityId.Equals(PponCityGroup.DefaultPponCityGroup.Family.CityId) && mainDeal.ExchangePrice.HasValue && mainDeal.ExchangePrice.Value > default(decimal))
                    {
                        if (Helper.IsFlagSet(mainDeal.BusinessHourStatus, BusinessHourStatus.NoDiscountShown))
                        {
                            ldr2.Text = "特選";
                        }
                        else
                        {
                            ldr2.Text = ViewPponDealManager.GetDealDiscountString(Helper.IsFlagSet(mainDeal.BusinessHourStatus, BusinessHourStatus.NoDiscountShown), zero_price, mainDeal.ItemOrigPrice);
                            ldr2.Text += "<span class=\"smalltext\">折</span>";
                        }
                    }
                    else
                    {
                        ldr2.Text = "優惠";
                    }
                }
            }
            else
            {
                ldr2.Text =
                            Helper.IsFlagSet(mainDeal.BusinessHourStatus, BusinessHourStatus.NoDiscountShown)
                                ? "特選"
                                : "優惠";
            }            

            // 銷售一空 Sold Out
            lit_badge_type.Text = (mainDeal.OrderedQuantity >= mainDeal.OrderTotalLimit) ? (IsBankDeal ? "<div class=\"SoldOut_Bar_BankDeal\"></div>" : "<div class=\"SoldOut_Bar_480\"></div>") : string.Empty;
            lit_badge_type.Text = mainDeal.IsExperience ?? false ? string.Empty : lit_badge_type.Text;
            // 每日下殺
            lit_everydaynewdeal.Text = "<img src = \"" + PponFacade.GetEveryDayNewDealFlag(mainDeal.BusinessHourGuid) + "\" />";

            // 商品主題活動行銷訊息活動Logo
            lit_DealPromoImageMainContent.Text = mainDeal.PromoImageHtml;

            var contentLite = ViewPponDealManager.DefaultManager.GetPponContentLite(mainDeal);
            li.Text = contentLite.Restrictions;

            // 憑證檔帶出文字由等值購物金兌換文字
            liEquivalent.Visible = mainDeal.DeliveryType.Equals((int)DeliveryType.ToShop);

            hd.NavigateUrl = ResolveUrl("~/newmember/privacypolicy.aspx#restrictions");

            // 公益檔不顯示無法配送離島訊息
            if (Helper.IsFlagSet(mainDeal.BusinessHourStatus, BusinessHourStatus.NotDeliveryIslands) && !_isKindDeal && !mainDeal.IsHouseDealNewVersion())
            {
                lblnotDeliveryIslands.Visible = true;
            }
            else
            {
                lblnotDeliveryIslands.Visible = false;
            }

            // 好康固定條款(只有憑證帶入)
            lblRegularProvision.Visible = (mainDeal.DeliveryType).Equals((int)DeliveryType.ToShop);
            // 在地宅配(宅配+舊版)固定條款
            lblDeliveryPponProvision.Visible = (mainDeal.DeliveryType).Equals((int)DeliveryType.ToHouse) && !mainDeal.IsHouseDealNewVersion();

            // 信託銀行文字說明
            TrustProvider trustProvider = Helper.GetBusinessHourTrustProvider(mainDeal.BusinessHourStatus);
            switch (trustProvider)
            {
                case TrustProvider.TaiShin:
                    liTaishin.Visible = true;
                    break;

                case TrustProvider.Sunny:
                    liMohist.Visible = true;
                    break;

                case TrustProvider.Hwatai:
                    liHwatai.Visible = true;
                    break;

                case TrustProvider.Initial:
                default:
                    break;
            }

            //零元檔次or公益檔次or宅配 不顯示信託提示
            if (decimal.Equals(0, mainDeal.ItemPrice) || _isKindDeal || (mainDeal.DeliveryType).Equals((int)DeliveryType.ToHouse))
            {
                liEquivalent.Visible = liTaishin.Visible = false;
            }

            //成套票券檔次條款說明
            if (Helper.IsFlagSet(mainDeal.BusinessHourStatus, BusinessHourStatus.GroupCoupon))
            {
                liGroupCoupon.Visible = true;
            }

            if (_isKindDeal)
            {
                liEntryTitle.Text = "捐款注意事項";
                liSellerAvailabilityTitle.Text = "公益單位資訊";
                hlkSellerInfo.Text = "查看公益單位資訊";
            }

            if (SellerFacade.GetStorePageCheck(mainDeal.SellerGuid))
            {
                if ((mainDeal.GroupOrderStatus & (int)GroupOrderStatus.SKMDeal) > 0)
                {
                    liSellerAvailabilityTitle.Text = "店家資訊";
                }
                else
                {
                    liSellerAvailabilityTitle.Text = string.Format("<a href='/ppon/store.aspx?sid={0}' class='shop-title'>店家資訊<i class='fa fa-plus-circle'>詳細資訊</i></a>", mainDeal.SellerGuid);
                }
            }

            //Here we don't want to let medical cosmetology deals or kind deals have hyperlinks to ppondetail.aspx
            if (decimal.Equals(0, mainDeal.ItemPrice) || _isKindDeal)
            {
                hd.Visible = false;
            }

            string dealDesc = contentLite.Description;
            if (!string.IsNullOrWhiteSpace(dealDesc))
            {
                if (Request.Browser.IsMobileDevice)
                {
                    string description = string.Empty;
                    if (mainDeal.Cchannel.GetValueOrDefault(false) && !string.IsNullOrEmpty(mainDeal.CchannelLink))
                    {
                        description += PponFacade.GetCchannelLink(mainDeal.CchannelLink) + "<br/>";
                        description += "<div style='height:20px'></div>";
                    }
                    description += dealDesc;

                    description = PponFacade.ReplaceMobileYoutubeTag(description);

                    litDesc.Text = string.Format("{0}<br />{1}", description.Replace("<img alt=\"\" src=", "<img alt=\"\" data-original="), mainDeal.DealPromoDescription);
                }
                else
                {
                    string description = string.Empty;
                    if (mainDeal.Cchannel.GetValueOrDefault(false) && !string.IsNullOrEmpty(mainDeal.CchannelLink))
                    {
                        description += PponFacade.GetCchannelLink(mainDeal.CchannelLink) + "<br />";
                        description += "<div style='height:20px'></div>";
                    }
                    description += dealDesc;
                    description = description.Replace("48999-LQ-041-10.jpg?1497518227", "48999-LQ-041-10-notfound.jpg?1497518227");
                    litDesc.Text = string.Format("{0}<br />{1}", 
                        description.Replace("<img alt=\"\" src=", "<img alt=\"\" data-original="), 
                        mainDeal.DealPromoDescription);
                }
            }
            if (!string.IsNullOrWhiteSpace(mainDeal.ProductSpec))
            {
                lps.Text = string.Format("{0}<br />", mainDeal.ProductSpec.Replace("<img alt=\"\" src=", "<img alt=\"\" data-original="));
            }
            else
            {
                phdDetailDesc.Visible = false;
                phdTop5Link.Visible = false;
            }
            if (relatedDeals.Count > 0)
            {
                phdRelatedDeal.Visible = true;
                rptRelatedDeal.DataSource = relatedDeals;
                rptRelatedDeal.DataBind();
            }
            if (!string.IsNullOrWhiteSpace(contentLite.Remark))
            {
                lrt.Text = contentLite.Remark.Split(new[] { "||" }, StringSplitOptions.None)[2];
            }
            avc.Availablity = ProviderFactory.Instance().GetSerializer()
                .Deserialize<AvailableInformation[]>(contentLite.Availability);
            if ((mainDeal.GroupOrderStatus & (int)GroupOrderStatus.SKMDeal) > 0)
            {
                avc.Availablity.ForEach(x =>
                {
                    x.MR = string.Empty;
                });
            }
            if (avc.Availablity.Length > 0)
            {
                avc.DataBind();
            }
            else
            {
                SellerAvailability.Visible = false;
                hlkSellerInfo.Visible = false;
            }

            // 全家檔or公益檔or新光檔次 不顯示安心三大保證
            if ((mainDeal.GroupOrderStatus & (int)GroupOrderStatus.FamiDeal) > 0 || _isKindDeal || (mainDeal.GroupOrderStatus & (int)GroupOrderStatus.SKMDeal) > 0)
            {
                pDetailinner.Visible = false;
            }
            

            #region SEO

            litDealMetas.Text = PponFacade.RenderMetasForDeal(mainDeal);
            litCanonicalRel.Text = PponFacade.RenderCanonicalRel(mainDeal);

            #endregion SEO

            this.AppTitle = string.IsNullOrEmpty(mainDeal.AppTitle) ? mainDeal.CouponUsage : mainDeal.AppTitle;

            if (mainDeal.ItemPrice > 0)
            {
                ph_Retargeting.Visible = true;
            }
        }

        public void RedirectToPiinlife()
        {
            Response.Redirect(string.Format("{0}/piinlife/{1}", SystemConfig.SiteUrl, BusinessHourId.ToString())
                + (!string.IsNullOrWhiteSpace(Rsrc) ? string.Format("/{0}", Rsrc) : string.Empty)
                + (Request.QueryString["token"] != null && !string.IsNullOrWhiteSpace(Request.QueryString["token"].ToString()) ? string.Format("?token={0}", Request.QueryString["token"].ToString()) : string.Empty));
        }

        public void RedirectToDefault()
        {
            NoRobots();

            Response.Redirect(string.Format("{0}/{1}", SystemConfig.SiteUrl, "ppon/default.aspx"));
        }

        public void RedirectNoShowToDefault(string nsUrl)
        {
            NoRobots();
            Response.Redirect(string.Format("{0}/{1}?nsurl={2}", SystemConfig.SiteUrl, "ppon/default.aspx", HttpUtility.UrlEncode(nsUrl)));
        }

        public void RedirectToGameDetail(Guid bid)
        {
            Response.Redirect(string.Format("/game/productDetail?bid={0}", bid));
        }

        /// <summary>
        /// 設定一組Cookie到client端
        /// </summary>
        /// <param name="aCookie"></param>
        public bool SetCookie(String value, DateTime expireTime, LkSiteCookie cookieField)
        {
            HttpCookie referenceCookie = CookieManager.NewCookie(cookieField.ToString("g"));

            referenceCookie.Value = value;
            referenceCookie.Expires = expireTime;
            //設定cookie值於使用者電腦
            try
            {
                Response.Cookies.Add(referenceCookie);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public void ShowMessage(string message)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('" + message + "');", true);
        }

        /// <summary>
        /// 加入meta避免被google爬蟲爬到
        /// </summary>
        public void NoRobots()
        {
            phNorobots.Visible = true;
        }

        public void SeoSetting(IViewPponDeal d)
        {
            if (String.IsNullOrWhiteSpace(d.PageDesc))
            {
                this.Page.MetaDescription = d.CouponUsage + "-" + d.EventTitle; //好康券+橘標
            }
            else
            {
                this.Page.MetaDescription = d.PageDesc;
            }

            if (String.IsNullOrWhiteSpace(d.PageTitle))
            {
                this.Page.Title = PponFacade.GetDisplayPponName(d);
            }
            else
            {
                this.Page.Title = d.PageTitle;
            }

            PicAlt = string.IsNullOrWhiteSpace(d.PicAlt) ? (string.IsNullOrEmpty(d.AppTitle) ? d.CouponUsage : d.AppTitle) : d.PicAlt;
            Model.PicAlt = string.IsNullOrWhiteSpace(d.PicAlt) ? (string.IsNullOrEmpty(d.AppTitle) ? d.CouponUsage : d.AppTitle) : d.PicAlt;
            this.Page.MetaKeywords = string.Format("{0},{1}", string.IsNullOrEmpty(PicAlt) ? string.Empty : PicAlt.Replace("/", ","), this.Page.MetaKeywords);
        }

        public void ShowATMIcon()
        {
            ImageATM.Visible = true;
        }

        public void SetHami()
        {
            Session["HamiTicketId"] = HamiTicketId;
        }

        //檔次判斷，沒有則導回首頁
        public void RedirectToComboDealMain(Guid? mainBid, Guid bid)
        {
            if (mainBid == null || mainBid == Guid.Empty)
            {
                Response.Redirect(string.Format("{0}/{1}", SystemConfig.SiteUrl, "ppon/default.aspx"));
            }
            else
            {
                var url = PathAndQueryBuilder.Parse(this.Request.Url.PathAndQuery);
                url.Path = "/deal/" + mainBid.Value.ToString();
                if (string.IsNullOrEmpty(Request.QueryString["rsrc"]) == false)
                {
                    url.Append("rsrc", Request.QueryString["rsrc"]);
                }
                string newUrl = url.ToString();
                Response.Redirect(newUrl);
            }
        }
        //請選擇好康
        public void SetProductEntries(IList<ViewComboDeal> combodeals, IViewPponDeal mainDeal)
        {
            if (combodeals == null || combodeals.Count == 0)
            {
                ProductEntries = PponDealProductEntry.ConvertFrom(mainDeal);
            }
            else
            {
                ProductEntries = PponDealProductEntry.ConvertFrom(combodeals);
            }
        }

        public void SetEventMainActivities(KeyValuePair<EventActivity, CmsRandomCityCollection> pair)
        {
            Cache.Remove(EdmPopUpCacheName);
            //修正跳窗EDM暫存誤差
            double CacheMin = Math.Floor((pair.Key.EndDate - DateTime.Now).TotalMinutes);
            CacheMin = (CacheMin > 60 ? 60 : CacheMin);
            Cache.Insert(EdmPopUpCacheName, pair, null, DateTime.Now.AddMinutes(CacheMin), TimeSpan.Zero, CacheItemPriority.NotRemovable, null);
            if (!string.IsNullOrWhiteSpace(Rsrc) && pair.Key.ExcludeCpaList.Contains(Rsrc))
            {
                ShowEventEmail = false;
            }
            else if (!pair.Value.Any(x => x.CityId == CityId))
            {
                ShowEventEmail = false;
            }
            else
            {
                ShowEventEmail = true;
            }
        }

        #endregion interface methods

        #region page

        protected void Page_Init(object sender, EventArgs e)
        {
            //強跳跳轉
            if (this.BusinessHourId != Guid.Empty)
            {
                Dictionary<Guid, Old2NewBid> old2NewBIds = PponFacade.GetOld2NewBids();
                Old2NewBid old2NewBid;
                if (old2NewBIds.TryGetValue(this.BusinessHourId, out old2NewBid) && IsRedirectFromGoogleSearch())
                {
                    var url = PathAndQueryBuilder.Parse(this.Request.Url.PathAndQuery);
                    url.Path = "/deal/" + old2NewBid.NewBid;
                    url.Append("rsrc", old2NewBid.TrackingCode);
                    string newUrl = url.ToString();
                    Response.Redirect(newUrl);
                    return;
                }
            }

            if (config.MDealEnabled && CommonFacade.IsMobileVersion())
            {
                string mdealUrl = string.Format("/m/deal/{0}", BusinessHourId);
                QueryStringBuilder qsb = new QueryStringBuilder(mdealUrl);
                qsb.AppendQuery(Request.Url.Query);
                qsb.AppendRoute(RouteData);
                qsb.Remove("bid");
                string redirectUrl = qsb.ToString();
                Response.Redirect(redirectUrl);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            #region 收集商品頁使用了哪些 route ，以做為後續調整的依據
            string routeName = this.RouteData.GetRouteName();
            routeNameUsedStat.AddOrUpdate(routeName, 1, (s, i) => i + 1);
            if (Request.QueryString["debug"] == "1")
            {
                Response.ContentType = "text/plain";
                foreach (var pair in routeNameUsedStat)
                {
                    Response.Write(pair.Key + ": " + pair.Value + "\r\n");
                }
                Response.End();
            }

            #endregion

            if (!Page.IsPostBack)
            {
                this.Presenter.OnViewInitialized();
                if (Session[LkSiteSession.ShareLinkText.ToString()] != null && User.Identity.IsAuthenticated)
                {
                    Session.Remove(LkSiteSession.ShareLinkText.ToString());
                    if (string.IsNullOrWhiteSpace(tbx_ReferralShare.Text))
                    {
                        tbx_ReferralShare.Text = GetShareText(BusinessHourId);
                    }
                    ScriptManager.RegisterStartupScript(this, typeof(Button), "showshare", "showshare('" + tbx_ReferralShare.Text + "');", true);
                }
            }
            this.Presenter.OnViewLoaded();
            this.Master.IsShowBanner = !this.IsHiddenBanner;

            //App Links
            string bid = BusinessHourId == Guid.Empty ? string.Empty : BusinessHourId.ToString();
            HttpCookie cookie = Request.Cookies.Get(LkSiteCookie.ReferenceId.ToString("g"));
            string extId = cookie != null ? cookie.Value : string.Empty;
        }

        private void SetRandomParagraphCity(List<int> cityIds)
        {
            int tempCity;
            if (Request["cid"] == null || int.TryParse(Request["cid"], out tempCity) == false)
            {
                if (cityIds.Count > 0)
                {
                    tempCity = cityIds[0];
                }
                else
                {
                    tempCity = CategoryManager.pponCityId.Taipei;
                }
            }
            saleB.CityId = news.CityId = Master.PponNews.CityId = outsideAD.CityId = insideAD.CityId = active.CityId = tempCity;
            saleB.Type = news.Type = Master.PponNews.Type = outsideAD.Type = insideAD.Type = active.Type = RandomCmsType.PponRandomCms;
        }

        #endregion page

        #region other methods
        private void SetupOrderableElements(IViewPponDeal deal)
        {

            ISysConfProvider conf = ProviderFactory.Instance().GetConfig();
            string buyBtnCss = "btn_normal";            
            string buyBtnOtherAttribute = string.Empty;
            string buyBtnDesc = "立即買";
            bool isSkmDeal = deal.GroupOrderStatus != null ? (deal.GroupOrderStatus.Value & (int)GroupOrderStatus.SKMDeal) > 0 : false;
            if (IsBankDeal)
            {
                buyBtnDesc = "兌換";
            }
            string cpaKeyParam = null;
            string cpaKeyValie = CookieManager.GetCpaKey();
            if (string.IsNullOrEmpty(cpaKeyValie) == false)
            {
                cpaKeyParam = "cpaKey=" + cpaKeyValie;
            }


            Dictionary<Guid, string> participateBtn = new Dictionary<Guid, string>();
            if (!string.IsNullOrWhiteSpace(conf.ParticipateBtnBid1) && conf.ParticipateBtnBid1.Split(',').Length > 1)
            {
                string btnDesc = conf.ParticipateBtnBid1.Split(',')[0];
                foreach (string participateBtnBid in conf.ParticipateBtnBid1.Split(',').Skip(1))
                {
                    Guid bid = Guid.TryParse(participateBtnBid, out bid) ? bid : Guid.Empty;
                    if (!bid.Equals(Guid.Empty))
                    {
                        participateBtn.Add(bid, btnDesc);
                    }
                }
            }

            if (!string.IsNullOrWhiteSpace(conf.ParticipateBtnBid2) && conf.ParticipateBtnBid2.Split(',').Length > 1)
            {
                string btnDesc = conf.ParticipateBtnBid2.Split(',')[0];
                foreach (string participateBtnBid in conf.ParticipateBtnBid2.Split(',').Skip(1))
                {
                    Guid bid = Guid.TryParse(participateBtnBid, out bid) ? bid : Guid.Empty;
                    if (!bid.Equals(Guid.Empty))
                    {
                        participateBtn.Add(bid, btnDesc);
                    }
                }
            }

            if (!string.IsNullOrWhiteSpace(conf.ParticipateBtnBid3) && conf.ParticipateBtnBid3.Split(',').Length > 1)
            {
                string btnDesc = conf.ParticipateBtnBid3.Split(',')[0];
                foreach (string participateBtnBid in conf.ParticipateBtnBid3.Split(',').Skip(1))
                {
                    Guid bid = Guid.TryParse(participateBtnBid, out bid) ? bid : Guid.Empty;
                    if (!bid.Equals(Guid.Empty))
                    {
                        participateBtn.Add(bid, btnDesc);
                    }
                }
            }
            bool isClosedDeal = false;
            bool isClosedAndFail = false;
            string url = string.Empty;
            switch (deal.GetDealStage())
            {
                case PponDealStage.ClosedAndFail:
                    #region 已結檔且未達門檻
                    buyBtnCss = "btn_dontclick";
                    buyBtnDesc = "未達門檻";
                    isClosedAndFail = true;
                    #endregion
                    break;
                case PponDealStage.Created:
                case PponDealStage.Ready:
                    #region 未上檔 || 已上檔
                    if (participateBtn.Count > 0 && participateBtn.Keys.Contains(deal.BusinessHourGuid))
                    {
                        buyBtnCss = "btn_normal";
                        buyBtnDesc = participateBtn.Where(x => x.Key.Equals(deal.BusinessHourGuid)).FirstOrDefault().Value;
                    }
                    else if (Helper.IsFlagSet(deal.GroupOrderStatus ?? 0, GroupOrderStatus.KindDeal))
                    {
                        buyBtnCss = "btn_normal";
                        buyBtnDesc = "我要捐款";
                    }
                    else if (decimal.Equals(0, deal.ItemPrice))
                    {
                        if ((deal.BusinessHourStatus & (int)BusinessHourStatus.PriceZeorShowOriginalPrice) == 0)
                        {
                            buyBtnCss = "btn_normal";
                            buyBtnDesc = "參 加";
                        }

                        if ((deal.GroupOrderStatus & (int)GroupOrderStatus.FamiDeal) > 0 || (deal.GroupOrderStatus & (int)GroupOrderStatus.SKMDeal) > 0)
                        {
                            buyBtnCss = "btn_normal";
                            buyBtnDesc = SystemConfig.FamilyCityButtonString;
                        }
                    }
                    #endregion
                    break;
                case PponDealStage.Running:
                case PponDealStage.RunningAndOn:
                    #region 已上檔且未達最低門檻 || 已上檔且達最低門檻 || 已上檔且未售出份數0,最低門檻1,最高購買數0


                    if ((deal.BusinessHourStatus & (int)BusinessHourStatus.ComboDealMain) > 0)
                    {
                        buyBtnOtherAttribute = @" OnClick=""scupioCartPage();PopWindow($('#combodeals'));"" style=""cursor:pointer;""";
                        if (participateBtn.Count > 0 && participateBtn.Keys.Contains(deal.BusinessHourGuid))
                        {
                            buyBtnCss = "btn_normal";
                            buyBtnDesc = participateBtn.Where(x => x.Key.Equals(deal.BusinessHourGuid)).FirstOrDefault().Value;
                        }
                    }
                    else if (deal.OrderedQuantity == 0 && deal.BusinessHourOrderMinimum == 1 && deal.OrderTotalLimit == 0)
                    {
                        //條件與售完一致
                        buyBtnCss = "btn_dontclick";
                        buyBtnDesc = (IsBankDeal) ? "兌換完畢" : "已售完";




                    }
                    else
                    {
                        if (participateBtn.Count > 0 && participateBtn.Keys.Contains(deal.BusinessHourGuid))
                        {
                            buyBtnCss = "btn_normal";
                            buyBtnDesc = participateBtn.Where(x => x.Key.Equals(deal.BusinessHourGuid)).FirstOrDefault().Value;
                        }
                        else if (Helper.IsFlagSet(deal.GroupOrderStatus ?? 0, GroupOrderStatus.KindDeal))
                        {
                            buyBtnCss = "btn_normal";
                            buyBtnDesc = "我要捐款";
                        }
                        else if (decimal.Equals(0, deal.ItemPrice))
                        {
                            if ((deal.BusinessHourStatus & (int)BusinessHourStatus.PriceZeorShowOriginalPrice) == 0)
                            {
                                buyBtnCss = "btn_normal";
                                buyBtnDesc = "參 加";
                            }

                            if ((deal.GroupOrderStatus & (int)GroupOrderStatus.FamiDeal) > 0 || (deal.GroupOrderStatus & (int)GroupOrderStatus.SKMDeal) > 0)
                            {
                                buyBtnCss = "btn_normal";
                                buyBtnDesc = SystemConfig.FamilyCityButtonString;
                            }

                            if (CityId == PponCityGroup.DefaultPponCityGroup.Skm.CityId)
                            {
                                buyBtnDesc = "立即索取";
                            }
                        }
                        buyBtnOtherAttribute = string.Format(@" OnClick=""scupioCartPage();window.location ='{0}'""", conf.SSLSiteUrl + "/ppon/buy.aspx?bid=" + deal.BusinessHourGuid);
                    }
                    #endregion
                    break;
                case PponDealStage.RunningAndFull:
                case PponDealStage.ClosedAndOn:
                case PponDealStage.CouponGenerated:
                    #region 已上檔且達購買上限 || 已結檔且達最低門檻 ||已結檔且優惠券已產生
                    buyBtnCss = "btn_dontclick";
                    if (Helper.IsFlagSet(deal.GroupOrderStatus ?? 0, GroupOrderStatus.KindDeal))
                    {
                        buyBtnDesc = "已截止";
                    }
                    else if (deal.IsExperience ?? false)
                    {
                        buyBtnDesc = "立即索取";
                        break;
                    }
                    else
                    {
                        buyBtnDesc = (IsBankDeal) ? "兌換完畢" : "已售完";
                        isClosedDeal = true;
                    }
                    #endregion
                    break;
                default:
                    break;
            }

            lb2.Text = string.Format(@"<a class=""buy_btn {0}"" {1}>{2}</a>", buyBtnCss, buyBtnOtherAttribute, buyBtnDesc);

            Model.BuyBtnHtml = string.Format(@"<a class=""buy_btn {0}"" {1}>{2}</a>", 
                buyBtnCss.Replace("btn_normal", "buy_btn").Replace("btn_dontclick", "sold_out_btn"), 
                buyBtnOtherAttribute, buyBtnDesc);           

            if (isClosedDeal || isClosedAndFail)
            {               
                repRecommendDealsForCloseDeal.DataSource = PponFacade.GetRelatedDeals(deal);
                repRecommendDealsForCloseDeal.DataBind();

                IsHiddenBanner = true;
            }

        }

        protected static string GetShareText(Guid bid)
        {
            if (HttpContext.Current.Session[LkSiteSession.ExternalMemberId.ToString()] == null)
            {
                MemberUtility.SetUserSsoInfoReady(MemberFacade.GetUniqueId(HttpContext.Current.User.Identity.Name));
            }

            ExternalMemberInfo emi = HttpContext.Current.Session[LkSiteSession.ExternalMemberId.ToString()] as ExternalMemberInfo;

            if (emi != null)
            {
                string origUrl = WebUtility.GetSiteRoot() + "/ppon/PponReferralRedirection.aspx?bid=" + bid + "," + (int)emi[0].Source + "|" + emi[0].ExternalId;
                string shortUrl = WebUtility.RequestShortUrl(origUrl);
                PromotionFacade.SetDiscountReferrerUrl(origUrl, shortUrl, MemberFacade.GetUniqueId(HttpContext.Current.User.Identity.Name), HttpContext.Current.User.Identity.Name, bid, 0);
                return shortUrl;
            }
            else
            {
                return string.Empty;
            }
        }

        public decimal CheckZeroPriceToShowPrice(IViewPponDeal deal)
        {
            //若是全家檔次須將CityId改成全家CityId
            if ((deal.GroupOrderStatus & (int)GroupOrderStatus.FamiDeal) > 0)
            {
                return PponFacade.CheckZeroPriceToShowPrice(deal, PponCityGroup.DefaultPponCityGroup.Family.CityId);
            }
            else
            {
                return PponFacade.CheckZeroPriceToShowPrice(deal, CityId);
            }
        }



        public string GetDealFreight(ViewComboDeal vcd)
        {
            IViewPponDeal deal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(vcd.BusinessHourGuid, true);
            if (deal == null || deal.FreightAmount == 0)
            {
                return string.Empty;
            }
            return string.Format("| 運費 ${0}", (int)deal.FreightAmount);
        }
  
        #endregion other methods

        protected void repRecommendDealsForCloseDeal_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.Item && e.Item.ItemType != ListItemType.AlternatingItem)
            {
                return;
            }
            IViewPponDeal deal = e.Item.DataItem as IViewPponDeal;
            if (deal == null)
            {
                return;
            }
            Literal litDealImage = e.Item.FindControl("litDealImage") as Literal;
            Literal litPrice = e.Item.FindControl("litPrice") as Literal;
            Literal litItemOrigPrice = e.Item.FindControl("litItemOrigPrice") as Literal;
            Literal litDiscount = e.Item.FindControl("litDiscount") as Literal;
            PlaceHolder phDiscountPrice = e.Item.FindControl("phDiscountPrice") as PlaceHolder;
            if (litDealImage == null || litPrice == null || litItemOrigPrice == null || litDiscount == null || phDiscountPrice == null)
            {
                throw new Exception("FindControl error");
            }
            litItemOrigPrice.Text = ((int)deal.ItemOrigPrice).ToString();

            litDealImage.Text = string.Format("<img src='{0}' />", ImageFacade.GetDealAppPic(deal, notFoundTryPlanB: true));

            litPrice.Text = string.Format("${0}{1}", PponFacade.CheckZeroPriceToShowPrice(deal, this.CityId).ToString("F0"),
                ((deal.BusinessHourStatus & (int)(BusinessHourStatus.ComboDealMain)) > 0 ? "<span class='text_s'>起</span>" : string.Empty));

            string tempSaleMessage = ViewPponDealManager.GetDealDiscountString(
                Helper.IsFlagSet(deal.BusinessHourStatus, BusinessHourStatus.NoDiscountShown), deal.ItemPrice, deal.ItemOrigPrice);
            litDiscount.Text = tempSaleMessage != string.Empty ? string.Format("{0}<span class='smalltext'>折</span>", tempSaleMessage) : "特選";

            var phDiscountPriceText = phDiscountPrice.Controls[0] as LiteralControl;
            if (phDiscountPriceText != null && deal.DiscountPrice != null)
            {
                string discountPrice = deal.DiscountPrice.Value.ToString("0");
                string priceTail = (deal.ComboDelas == null || deal.ComboDelas.Count == 0) ? "元" : "起";
                phDiscountPriceText.Text = phDiscountPriceText.Text
                    .Replace("{discountPrice}", discountPrice).Replace("{priceTail}", priceTail);
            }
            else
            {
                phDiscountPrice.Visible = false;
            }
        }

        //<a href="https://www.17life.com/ppon/pponsearch.aspx?search=早午餐"><%Container %></a>
        protected void repTags_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.Item && e.Item.ItemType != ListItemType.AlternatingItem)
            {
                return;
            }
            HyperLink link = e.Item.FindControl("link") as HyperLink;
            if (link == null)
            {
                throw new Exception("repTags error.");
            }
            string dataItem = (string)e.Item.DataItem;
            link.Text = "# " + dataItem;
            link.NavigateUrl = "/ppon/pponsearch.aspx?search=" + HttpUtility.UrlEncode(dataItem);
        }

        protected void rptRelatedDeal_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType != ListItemType.Item && e.Item.ItemType != ListItemType.AlternatingItem)
            {
                return;
            }
            HyperLink link = e.Item.FindControl("hypRelatedLink") as HyperLink;
            if (link == null)
            {
                throw new Exception("relatedLink error.");
            }
            KeyValuePair<Guid, string> dataItem = (KeyValuePair<Guid, string>)e.Item.DataItem;
            link.Target = "_blank";
            link.Text = dataItem.Value;
            link.NavigateUrl = string.Format("{0}/{1}", config.SiteUrl, dataItem.Key.ToString());
        }

        private bool IsRedirectFromGoogleSearch()
        {
            if (this.Request.UrlReferrer != null && 
                this.Request.UrlReferrer.ToString().Contains("google.com", StringComparison.OrdinalIgnoreCase))
            {
                return true;
            }
            return false;
        }
    }
}
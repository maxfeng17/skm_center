﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TestPage.aspx.cs" Inherits="LunchKingSite.Web.TestPage" %>
<%@ Import Namespace="System.Net" %>
<%@ Import Namespace="LunchKingSite.Core" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>17Life測試頁</title>
    <style>
        .yes
        {
            font-weight: bold;
            color: blue;
        }

        .no
        {
            font-weight: bold;
            color: red;
        }

        div
        {
            padding: 3px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:PlaceHolder ID="phOutline" runat="server" EnableViewState="false" Visible="false">
            <iframe id="outline" width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="TestPage.aspx?cookieCheck=false"></iframe>
        </asp:PlaceHolder>
        <asp:PlaceHolder ID="phContent" runat="server" EnableViewState="false" Visible="false">
        <div>
            <h2>17Life 測試頁 <span>(<%=Dns.GetHostName() %>)</span></h2>
            <div>您目前瀏覽器: <%=Page.Request.Browser.Browser %> <%=Page.Request.Browser.MajorVersion %>.<%=Page.Request.Browser.MinorVersion %></div>
            <div>您來自的IP: <%=GetClientIP() %></div>
            <div>你的時間: <span id="timeDiff"></span></div>
            <div>系統時間: <%=DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")%></div>
            <div>瀏覽器支援Cookie: <%=Page.Request.Browser.Cookies ? "<span class='yes'>是</span>" : "<span class='no'>否</span>" %></div>
            <div>實際測試Cookie: <%=TestCookie ? "<span class='yes'>是</span>" : "<span class='no'>否</span>" %></div>
            <div>目前登入者: <%=User.Identity.Name %></div>
            <div><a href="/User/bonuslistnew.aspx">會員餘額明細</a></div>
        </div>
        <script>
            window.onload = function () {
                var timeDiff = document.getElementById('timeDiff');
                var d = new Date();
                var yourTime = d.getFullYear() + '/' + (d.getMonth() + 1) + '/' + d.getDate() + ' '
                    + d.getHours() + ':' + d.getMinutes() + ':' + d.getSeconds();
                if (timeDiff.innerText) {
                    timeDiff.innerText = yourTime;
                } else {
                    timeDiff.textContent = yourTime;
                }
            }
        </script>
        </asp:PlaceHolder>
    </form>
</body>
</html>

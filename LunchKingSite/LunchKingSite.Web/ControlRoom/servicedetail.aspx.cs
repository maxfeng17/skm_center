﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.Core.Component;
using LunchKingSite.Core;
using LunchKingSite.Core.Enumeration;
using LunchKingSite.DataOrm;
using System.Net.Mail;
using LunchKingSite.Core.UI;

namespace LunchKingSite.Web.ControlRoom
{
    public partial class servicedetail : RolePage
    {
        IMemberProvider im = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        ISysConfProvider configs = ProviderFactory.Instance().GetConfig();

        #region property
        public ServiceMessageType ServiceType
        {
            get
            {
                int type;
                return int.TryParse(hidType.Value, out type) ? (ServiceMessageType)type : ServiceMessageType.Ppon;
            }
        }
        #endregion

        #region page
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                LoadData();
            }
        }

        protected void btnSend_Click(object sender, EventArgs e)
        {
            if (ddlstatus.SelectedIndex == 0)
            {
                Response.Write("<script>window.alert('請選擇處理狀態');</script>");
                ddlstatus.Focus();
            }
            else if (rdlWorkType.SelectedIndex < 0)
            {
                Response.Write("<script>window.alert('嘿！尚未點選轉件項目！');</script>");
                rdlWorkType.Focus();
            }
            else
            {
                SetServiceLog("已寄出");
                UpdateServiceStatus();
                SendEmail();
                Response.Write("<script>window.alert('新增完成');</script>");
            }
            LoadData();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {

            if (ddlstatus.SelectedIndex == 0)
            {
                Response.Write("<script>window.alert('請選擇處理狀態');</script>");
                ddlstatus.Focus();
            }
            else if (rdlWorkType.SelectedIndex < 0)
            {
                Response.Write("<script>window.alert('嘿！尚未點選轉件項目！');</script>");
                rdlWorkType.Focus();
            }
            else
            {
                SetServiceLog("已儲存");
                UpdateServiceStatus();
                Response.Write("<script>window.alert('儲存完成');</script>");
            }
            LoadData();
        }

        protected void Repeater1_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
            {
                string s = ((Label)e.Item.FindControl("lblm")).Text;
                ((Label)e.Item.FindControl("lblm")).Text = HttpUtility.HtmlDecode(s);

                ServiceLog obj = (ServiceLog)e.Item.DataItem;
                if (!string.IsNullOrEmpty(obj.CreateId))
                {
                    Label lblcreateId = (Label)e.Item.FindControl("lblCreatdId");
                    lblcreateId.Text = obj.CreateId.Substring(0, obj.CreateId.IndexOf('@'));
                }
            }
        }

        #endregion

        #region private method
        private void LoadData()
        {
            //建構UI
            this.buildPriorityRbl(ref rblPriority);

            // 處理狀態為完成，儲存按鈕鎖定
            bool enable = ddlstatus.SelectedValue.Equals("complete");
            this.btnSave.Enabled = !enable;
            this.btnSend.Enabled = !enable;

            #region Get ServiceMessage
            ServiceMessage sm = im.ServiceMessageGet(int.Parse(Request["id"]));
            if (sm.Priority != null) { rblPriority.SelectedIndex = Convert.ToInt32(sm.Priority); }
            lblMsg.Text = HttpUtility.HtmlDecode(sm.Message.Replace("\n", "<br>").Replace("\r\n", "<br>"));

            lblCateGory.Text = (im.ServiceMessageCategoryGetByCategoryId((int)sm.Category)).CategoryName + " - " + (im.ServiceMessageCategoryGetByCategoryId((int)sm.SubCategory)).CategoryName;
            hidType.Value = sm.Type.ToString();
            lblType.Text = ((ServiceMessageType)sm.Type).ToString();
            lblEmail.Text = sm.Email;
            lnhistory.Target = "blank";
            lnhistory.NavigateUrl = "serviceworker.aspx?name=" + sm.Email;
            if (sm.UserId.HasValue)
            {
                lblname.Text = "<a href='../controlroom/user/users_edit.aspx?username=" + im.MemberGetbyUniqueId((int)sm.UserId).UserName + "'>" + sm.Name + "</a>";
            }
            else
            {
                lblname.Text = sm.Name;
            }
            lblphone.Text = sm.Phone;
            lblTime.Text = sm.CreateTime.ToString();
            ddlstatus.SelectedValue = sm.Status;
            rdlWorkType.SelectedValue = sm.WorkType.ToString();
            btnSave.Enabled = !sm.Status.Equals("complete");
            btnSend.Enabled = !sm.Status.Equals("complete");
            //讓CKEDITOR 能正確顯示輸入內容(不包含html code) 
            txtMessage.Text = HttpUtility.HtmlDecode(txtMessage.Text);
            txtRemark_Seller.Text = HttpUtility.HtmlDecode(txtRemark_Seller.Text);
            #endregion

            #region GetServiceLogs
            ServiceLogCollection slc = im.ServiceLogGetList(Request["id"]);
            Repeater1.DataSource = slc;
            Repeater1.DataBind();
            #endregion

            #region Set Subject
            switch ((ServiceMessageType)ServiceType)
            {
                case ServiceMessageType.Ppon:
                    txttitle.Text = "17Life客服中心回覆通知函";
                    break;
                case ServiceMessageType.Hideal:
                    txttitle.Text = "品生活客服中心回覆通知函";
                    break;
                default:
                    break;
            }
            #endregion
        }

        private void SendEmail()
        {
            //檢核EMail
            if (RegExRules.CheckEmail(lblEmail.Text) && !string.IsNullOrEmpty(lblEmail.Text.Trim()))
            {
                MailMessage msg = new MailMessage();
                msg.From = GetFormEmail(ServiceType);
                msg.To.Add(lblEmail.Text);
                msg.Subject = txttitle.Text;
                msg.Body = HttpUtility.HtmlDecode(GetHeaderHtml(ServiceType));
                msg.IsBodyHtml = true;
                PostMan.Instance().Send(msg, SendPriorityType.Immediate);
            }
        }

        private string GetHeaderHtml(ServiceMessageType type)
        {
            string pponHeaderMsg = @"<body>
                                        <div style=""width: 100%; margin: 0 auto; font-family: Arial, Microsoft JhengHei, Helvetica, sans-serif"">
                                            <table width=""100%"" cellpadding=""0"" cellspacing=""0"">
                                                <tr>
                                                    <td style=""font-size: 13px; background: #fff;"">
                                                        <table width=""770"" cellpadding=""0"" cellspacing=""0"" align=""center"">
                                                            <tr>
                                                                <td>
                                                                    <!--Header-Start-->
                                                                    <table width=""770"" cellpadding=""0"" cellspacing=""0"" align=""center"" style=""background: #F3F3EE;
                                                                        color: #333;"">
                                                                        <tr>
                                                                            <td width=""20"" height=""80"">
                                                                            </td>
                                                                            <td width=""140"" height=""80"">
                                                                                <a href=""" + configs.SiteUrl + @"/ppon/"" target=""_blank"">
                                                                                    <img src=""" + configs.SiteUrl + @"Themes/PCweb/images/EDMLOGO.png"" width=""132"" height=""80""
                                                                                        alt="""" border=""0"" /></a>
                                                                            </td>
                                                                            <td width=""480"" height=""80"" style=""font-size: 26px; font-weight: bold;"">
                                                                                <a href=""" + configs.SiteUrl + @"/piinlife/"" target=""_blank"">
                                                                                    <img src=""" + configs.SiteUrl + @"Themes/default/images/17Life/EDM/EDMLOGO_HD.png"" width=""260"" height=""80""
                                                                                        alt="""" border=""0"" /></a>
                                                                            </td>
                                                                            <td width=""110"" height=""80"" style=""text-align: right;"">
                                                                                <a href=""" + configs.SiteUrl + @"/ppon/"" target=""_blank"" style=""color: #999;"">www.17life.com</a>
                                                                                <a href=""" + configs.SiteUrl + @"/piinlife/"" target=""_blank"" style=""color:#999;"">www.Piinlife.com</a>
                                                                            </td>
                                                                            <td width=""20"" height=""80"">
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                    <!--Header-End-->
                                                                    <!--Main-Start-->
                                                                    <table width=""770"" cellpadding=""0"" cellspacing=""0"" align=""center"" style=""background: #F3F3EE;"">
                                                                        <tr>
                                                                            <td width=""25"">
                                                                            </td>
                                                                            <tr>
                                                                                <td align=""center"">
                                                                                    <table width=""740"" cellpadding=""0"" cellspacing=""0"" align=""center"">
                                                                                        <tr>
                                                                                            <td height=""15"" style=""border-top: 1px solid #DDD;"">
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                    <table width=""740"" border=""0"" align=""center"" cellpadding=""0"" cellspacing=""0"" style=""font-family: '微軟正黑體';
                                                                                        color: #000; line-height: 30px; font-size: 14px; background-color: #FFF; border: 1px solid #DDD;"">
                                                                                        <tr>
                                                                                            <td height=""5"">
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td align=""center"">
                                                                                                <table width=""700"" border=""0"" cellspacing=""1"" cellpadding=""1"" style=""background-color: #FFF;
                                                                                                    font-weight: bold; text-align: left;"">
                                                                                                    <tr>
                                                                                                        <td height=""50"">
                                                                                                            親愛的會員您好：
                                                                                                    <tr>
                                                                                                        <td height=""40"">
                                                                                                            <p>
                                                                                                                " + txtMessage.Text.Trim() + @"</p>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td height=""10"">
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td style=""line-height: 24px;"">
                                                                                                            感謝您對17Life的支持，我們會持續努力提供更好的服務。<br />
                                                                                                            <br />
                                                                                                            此封信函為系統發出，所以請勿直接回覆，<br />
                                                                                                            若您仍有其他問題，歡迎您再次透過 <a href=""" + configs.SiteUrl + configs.SiteServiceUrl + @""">客服中心</a> 連結與我們聯繫。<br />
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td height=""25"">
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td height=""30"" style=""line-height: 24px;"">
                                                                                                            17Life客服中心 敬上
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td height=""20"">
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td style=""border-top: 1px dashed #6d9345; margin-top: 10px; margin-bottom: 10px;
                                                                                                            line-height: 23px;"">
                                                                                                            <table border=""0"" cellspacing=""0"" cellpadding=""0"">
                                                                                                                <tr>
                                                                                                                    <td height=""5"">
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td style=""font-size: 13px;"">
                                                                                                                        提醒您～17Life客服人員不會主動以email或電話要求您更改結帳方式或提供個人資料，<br />
                                                                                                                        若您接獲類似訊息，請拒絕回應，並立刻與17Life聯絡。
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                <tr>
                                                                                                                    <td height=""5"">
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                            </table>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td height=""30"" valign=""bottom"" style=""color: #bf0000;"">
                                                                                                            此信函為系統發出，請勿直接回覆喔。
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                </table>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td height=""5"">
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <td width=""25"">
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                    <!--EDM Main-End-->
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td height=""15"" bgcolor=""#F3F3EE"">
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <!--EDM Footer-Start-->
                                                        <table width=""770"" cellpadding=""0"" cellspacing=""0"" align=""center"" style=""background: #333;
                                                            color: #FFF;"">
                                                            <tr>
                                                                <td width=""25"">
                                                                </td>
                                                                <td height=""80"">
                                                                    <br />
                                                                    <a href=""" + configs.SiteUrl + configs.SiteServiceUrl + @""" target=""_blank"" style=""color: #FFF"">
                                                                        客服中心</a> | <a href=""" + configs.SiteUrl + @"/Ppon/WhiteListGuide.aspx"" target=""_blank""
                                                                            style=""color: #FFF"">加入白名單</a> | 服務時間：平日 9:00~18:00<br />
                                                                    17Life 康太數位整合股份有限公司 版權所有 轉載必究 c" + DateTime.Now.ToString("yyyy") + @"
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <!--EDM Footer-End-->
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td height=""25"" style=""background: #FFF;"">
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </body>";

            string hidealHeaderMsg = @" <body>
                                        <table width=""750"" align=""center"">
                                            <tr>
                                                <td>
                                                    <div style=""background-color: #F6EADF;"">
                                                        <table width=""700"" align=""center"" border=""0"" cellspacing=""0"" cellpadding=""0"">
                                                            <tr>
                                                                <td>
                                                                    <!--MailHeader_Start-->
                                                                    <div id=""MailHeader"">
                                                                        <table width=""700"" height=""80"" border=""0"" cellspacing=""0"" cellpadding=""0"">
                                                                            <tr>
                                                                                <td height=""20"">
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <img src=""" + configs.SiteUrl + @"/Themes/HighDeal/images/mail/HDmail_Header.png"" width=""700"" height=""80""
                                                                                        alt=""HighDeal"" style=""display: block;"" />
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </div>
                                                                    <!--MailHeader_End-->
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <!--MailContent_Start-->
                                                                    <div id=""MailContent"">
                                                                        <table width=""700"" border=""0"" cellspacing=""0"" cellpadding=""0"" style=""background-color: white;
                                                                            border-left: 1px solid #E1D6CC; border-right: 1px solid #E1D6CC;"">
                                                                            <tr>
                                                                                <td height=""10"" colspan=""3"">
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    <table width=""698"" border=""0"" cellspacing=""0"" cellpadding=""0"" align=""center"" style=""font-size: 13px;
                                                                                        color: #414141;"">
                                                                                        <tr>
                                                                                            <td width=""25"">
                                                                                            </td>
                                                                                            <td style=""word-break: break-all; word-wrap: break-word; text-align: left;"">
                                                                                                <p>
                                                                                                    親愛的會員，您好︰</p>
                                                                                                <br />
                                                                                                <div id=""Reply_Content"">
                                                                                                    " + txtMessage.Text.Trim() + @"
                                                                                                </div>
                                                                                                <br />
                                                                                                <p>
                                                                                                    感謝您對品生活的支持，我們會持續努力提供更好的服務。</p>
                                                                                                <p>
                                                                                                    品生活客服中心 敬上</p>
                                                                                                <br />
                                                                                                <hr style=""border-top: none; border-bottom: 1px solid #C8C8C8"" />
                                                                                                <p>
                                                                                                    ※ 客服人員不會主動以email或電話要求您更改結帳方式或提供個人資料，若您接獲類似訊息，請拒絕回應，<br />
                                                                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;並立刻與我們聯絡。<br />
                                                                                                    ※ 本信件由系統發送，請勿直接回覆。若有任何問題，請洽<a href=""" + configs.SiteUrl + configs.SiteServiceUrl + @""" style=""color: #FC7D49;"">客服中心</a>。</p>
                                                                                            </td>
                                                                                            <td width=""25"">
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td height=""10"" colspan=""3"">
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </div>
                                                                    <!--MailContent_End-->
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <!--MailFooter_Start-->
                                                                    <div id=""MailFooter"">
                                                                        <table width=""700"" height=""80"" border=""0"" cellspacing=""0"" cellpadding=""0"">
                                                                            <tr>
                                                                                <td>
                                                                                    <img src="""+configs.SiteUrl+@"/Themes/HighDeal/images/mail/HDmail_Footer.png"" width=""700"" height=""80""
                                                                                        alt=""HighDeal"" style=""display: block;"" />
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td height=""20"">
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </div>
                                                                    <!--MailFooter_End-->
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </body>";

            switch (type)
            {
                case ServiceMessageType.Ppon:
                    return pponHeaderMsg;
                case ServiceMessageType.Hideal:
                    return hidealHeaderMsg;
                default:
                    return string.Empty;
            }
        }

        private MailAddress GetFormEmail(ServiceMessageType type)
        {
            switch (type)
            {
                case ServiceMessageType.Ppon:
                default:
                    return new MailAddress(configs.PponServiceEmail, configs.ServiceName);
                case ServiceMessageType.Hideal:
                    return new MailAddress(configs.PiinlifeServiceEmail, configs.HidealCustomerServiceName);
            }
        }

        private void UpdateServiceStatus()
        {
            ServiceMessage sm = im.ServiceMessageGet(int.Parse(Request["id"]));
            sm.Status = ddlstatus.SelectedItem.Value;
            if (!string.IsNullOrEmpty(rdlWorkType.SelectedValue))
                sm.WorkType = Convert.ToInt32(rdlWorkType.SelectedValue);
            if (!string.IsNullOrEmpty(rblPriority.SelectedValue))
                sm.Priority = Convert.ToInt32(rblPriority.SelectedValue);
            if (sm.WorkUser == null)
                sm.WorkUser = Page.User.Identity.Name;
            sm.ModifyTime = DateTime.Now;
            sm.ModifyId = Page.User.Identity.Name;
            sm.RemarkSeller = txtRemark_Seller.Text;

            im.ServiceMessageSet(sm);
        }

        private void SetServiceLog(string sendType)
        {
            int refId;
            ServiceLog sl = new ServiceLog();
            sl.CreateTime = DateTime.Now;
            sl.CreateId = Page.User.Identity.Name;
            sl.RefX = int.TryParse(Request["id"], out refId) ? refId : 0;
            sl.Message = txtMessage.Text;
            sl.Remark = txtremark.Text;
            sl.RemarkSeller = txtRemark_Seller.Text;
            sl.SendTo = lblEmail.Text;
            sl.SendType = sendType;

            im.ServiceLogSet(sl);
        }

        private void buildPriorityRbl(ref RadioButtonList rbl)
        {
            rbl.Items.Clear();
            rbl.Items.Add(new ListItem(ServiceConfig.replyPriorityString.Normal, ((int)ServiceConfig.replyPriority.Normal).ToString()));
            rbl.Items.Add(new ListItem(ServiceConfig.replyPriorityString.High, ((int)ServiceConfig.replyPriority.High).ToString()));
            rbl.Items.Add(new ListItem("<font color='red'>" + ServiceConfig.replyPriorityString.Extreme + "</font>", ((int)ServiceConfig.replyPriority.Extreme).ToString()));
            rbl.SelectedValue = ((int)ServiceConfig.replyPriority.Normal).ToString();
        }

        #endregion
    }
}

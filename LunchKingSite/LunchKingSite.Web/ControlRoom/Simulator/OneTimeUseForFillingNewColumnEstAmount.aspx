﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OneTimeUseForFillingNewColumnEstAmount.aspx.cs" Inherits="LunchKingSite.Web.ControlRoom.Simulator.OneTimeUseForFillingNewColumnEstAmount" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Button runat="server" Text="執行" OnClick="Exec_Click" />
        </div>
        <asp:Literal runat="server" ID="litExecTime" />
    </form>
</body>
</html>

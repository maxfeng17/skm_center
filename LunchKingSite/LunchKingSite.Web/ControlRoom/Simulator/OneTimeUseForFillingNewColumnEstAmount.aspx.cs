﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.DataOrm;
using SubSonic;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LunchKingSite.Web.ControlRoom.Simulator
{
    public partial class OneTimeUseForFillingNewColumnEstAmount : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected void Exec_Click(object sender, EventArgs e)
        {            
            IEnumerable<int> bsIds = GetBalanceSheetIds();
            
            Stopwatch timer = new Stopwatch();
            timer.Start();
            Parallel.ForEach(bsIds, x =>
                {
                    BalanceSheetService.RecomputeBalanceSheetEstAmount(x);
                });
            timer.Stop();
            litExecTime.Text = timer.ElapsedMilliseconds.ToString();
        }

        private IEnumerable<int> GetBalanceSheetIds()
        {
            QueryCommand qc = new QueryCommand("select * from balance_sheet where pay_report_id is null", BalanceSheet.Schema.Provider.Name);
            BalanceSheetCollection bs = new BalanceSheetCollection();
            bs.LoadAndCloseReader(DataService.GetReader(qc));
            return bs.Select(x => x.Id).ToList();
        }
    }
}
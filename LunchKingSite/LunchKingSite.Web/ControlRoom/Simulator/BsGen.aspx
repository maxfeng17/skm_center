﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BsGen.aspx.cs" Inherits="LunchKingSite.Web.ControlRoom.Simulator.BsGen" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="../../Tools/js/jquery-1.4.2.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="../../Tools/js/jquery-ui.1.8.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#simTime').datepicker({ dateFormat: 'yy/mm/dd' });
            //add event to toggle manual triggered store guid input
            $('input:radio[name="balanceSheetType"]').click(
                function () {
                    if ($(this).val() === 'triggered') {
                        $('#storeGuidContainer').show();
                    } else {
                        $('#storeGuidContainer').hide();
                    }
                }
            );

            //show message if not empty string
            var message = $('#hidInfo').val();
            if (message !== '') {
                alert(message);
            }

        });
        
        function getStore() {
            
            $.ajax({
                type: "POST",
                url: "/virdir/controlroom/simulator/BsGen.aspx/GetStore",
                data: "{}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(msg) {
                // Do something interesting here.
                }        
            });
        }
    </script>
    <style>
        h3
        {
            font-weight: bold;
            font-size: larger;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>            
            <asp:HiddenField ID="hidInfo" runat="server" ClientIDMode="Static" />
            <h3>商業模式</h3>            
            <input type="radio" name="businessModel" value="ppon" />
            P好康檔號: <input type="text" id="txtUid" name="uid" />
            <br />
            <input type="radio" name="businessModel" value ="piin" />品生活 Pid:
            <input type="text" id="txtPid" name="pid" />
            <br />
            <h3>對帳單類型</h3>
            <input type="radio" name="balanceSheetType" value="weekly" />週對帳單
            <br />
            <input type="radio" name="balanceSheetType" value="monthly" />月對帳單
            <br />
            <input type="radio" name="balanceSheetType" value="triggered" />人工觸發對帳單            
            <span id="storeGuidContainer" style="display: none;">
                *觸發 store guid:
                <input type="text" name="storeGuid" />
            </span>

            <br />
        </div>
       <br />
        <fieldset>
            <legend>產對帳單</legend>
            <label for="simTime">模擬產對帳單的時間</label>            
            <input type="text" id="simTime" name="simTime" />            
            <div>
                <asp:Button ID="GenerateBS" runat="server" OnClick="GenerateBS_Click" Text="產對帳單" />                                           
            </div>
        </fieldset>
        
        <fieldset>
            <legend>亂七八糟</legend>            
        </fieldset>
        
        <asp:ListView runat="server" ID="lvFixMissingWPMBS">
            <LayoutTemplate>
                <table>
                    <tbody>
                        <tr runat="server" ID="itemPlaceholder" />
                    </tbody>
                </table>
            </LayoutTemplate>
            <ItemTemplate>
                <tr>
                    <td class="tdCenter"><%# Container.DataItem %></td>
                </tr>
            </ItemTemplate>
        </asp:ListView>

    </form>
</body>
</html>

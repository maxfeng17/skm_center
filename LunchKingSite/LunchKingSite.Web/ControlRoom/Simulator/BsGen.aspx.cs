﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Web.Services;

namespace LunchKingSite.Web.ControlRoom.Simulator
{
    public partial class BsGen : BasePage
    {
        protected int? Uid { get; set; }

        protected int? Pid { get; set; }

        protected BusinessModel BizModel { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            int uid;
            if (int.TryParse(Request.Form["uid"], out uid))
            {
                this.Uid = uid;
            }

            int productId;
            if (int.TryParse(Request.Form["pid"], out productId))
            {
                this.Pid = productId;
            }

            this.BizModel = Request.Form["businessModel"] == "ppon"
                                        ? BusinessModel.Ppon
                                        : BusinessModel.PiinLife;
        }

        protected void GenerateBS_Click(object sender, EventArgs e)
        {
            BalanceSheetGenerationFrequency bsCatg;
            switch (Request.Form["balanceSheetType"])
            {
                case "weekly":
                    bsCatg = BalanceSheetGenerationFrequency.WeekBalanceSheet;
                    break;

                case "monthly":
                    bsCatg = BalanceSheetGenerationFrequency.MonthBalanceSheet;
                    break;

                case "triggered":
                    bsCatg = BalanceSheetGenerationFrequency.ManualTriggeredBalanceSheet;
                    break;

                default:
                    bsCatg = BalanceSheetGenerationFrequency.Unknown;
                    break;
            }

            DateTime simTime;
            if (!DateTime.TryParse(Request.Form["simTime"], out simTime))
            {
                hidInfo.Value = "模擬時間解析錯誤!";
                return;
            }

            if (bsCatg == BalanceSheetGenerationFrequency.Unknown)
            {
                hidInfo.Value = "unknown generation frequency!";
                return;
            }

            Guid? storeGuid = null;
            Guid tempGuid;
            if (Guid.TryParse(Request.Form["storeGuid"], out tempGuid))
            {
                storeGuid = tempGuid;
            }

            ReadOnlyCollection<BalanceSheetGenerationResult> results = null;
            BalanceSheetSpec spec;
            if (this.BizModel == BusinessModel.Ppon)
            {
                IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
                DealProperty dp = pp.DealPropertyGet(this.Uid.GetValueOrDefault(0));
                spec = new BalanceSheetSpec()
                           {
                               SheetCategory = bsCatg,
                               BizModel = this.BizModel,
                               BusinessHourGuid = dp.BusinessHourGuid
                           };
            }
            else
            {
                spec = new BalanceSheetSpec()
                           {
                               SheetCategory = bsCatg,
                               BizModel = this.BizModel,
                               HiDealProductId = this.Pid,
                           };
            }

            try
            {
                results = BalanceSheetService.SimulateCreate(spec, simTime);
            }
            catch (Exception ex)
            {
                hidInfo.Value = ex.Message;
                return;
            }

            StringBuilder sb = new StringBuilder();
            foreach (BalanceSheetGenerationResult result in results)
            {
                if (result.StoreGuid != null)
                {
                    sb.Append(string.Format("分店-{0} : ", result.StoreGuid));
                }

                if (result.IsSuccess)
                {
                    sb.Append(string.Format("成功-對帳單 id = {0}", result.BalanceSheetId));
                }
                else
                {
                    if (result.Status == BalanceSheetGenerationResultStatus.Exception)
                    {
                        sb.Append("失敗-");
                        sb.Append(result.FailureMessage.TrimToMaxLength(800, "...blah blah blah..."));
                    }
                    else
                    {
                        sb.Append("不用產-");
                        sb.Append(result.Status.ToString() + ":");
                        sb.Append(result.FailureMessage);
                    }
                }

                sb.Append("\r\n");
            }

            hidInfo.Value = sb.ToString();
        }

        private string GenerationResultsToMessage(IEnumerable<BalanceSheetGenerationResult> results)
        {
            StringBuilder storeMessage = new StringBuilder();
            foreach (BalanceSheetGenerationResult result in results)
            {
                if (result.StoreGuid != null)
                {
                    storeMessage.Append(string.Format("分店-{0} : ", result.StoreGuid));
                }

                if (result.IsSuccess)
                {
                    storeMessage.Append(string.Format("成功-對帳單 id = {0}", result.BalanceSheetId));
                }
                else
                {
                    if (result.Status == BalanceSheetGenerationResultStatus.Exception)
                    {
                        storeMessage.Append("失敗-");
                        storeMessage.Append(result.FailureMessage.TrimToMaxLength(800, "...blah blah blah..."));
                    }
                    else
                    {
                        storeMessage.Append("不用產-");
                        storeMessage.Append(result.Status.ToString() + ":");
                        storeMessage.Append(result.FailureMessage);
                    }
                }

                storeMessage.Append("\r\n");
            }

            return storeMessage.ToString();
        }

        [WebMethod]
        public static string GetStores()
        {
            return string.Empty;
        }
    }
}
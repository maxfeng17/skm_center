﻿<%@ Page Language="C#" MasterPageFile="~/ControlRoom/backend.master" AutoEventWireup="true" ValidateRequest="false"
    CodeBehind="BlogContent.aspx.cs" Inherits="LunchKingSite.Web.ControlRoom.Blog.BlogContent" %>

<%@ Import Namespace="LunchKingSite.Core" %>
<%@ Import Namespace="LunchKingSite.DataOrm" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
    <link type="text/css" rel="stylesheet" href="/Themes/OWriteOff/plugin/materialize/css/materialize.css" media="screen,projection" />
    <link type="text/css" rel="stylesheet" href="/Themes/OWriteOff/plugin/font-awesome/font-awesome.css" />
    <link type="text/css" rel="stylesheet" href="/Themes/OWriteOff/OWriteOff.css" />
    <link type="text/css" rel="stylesheet" href="/Themes/OWriteOff/css/style.css" media="screen,projection" />
    <link type="text/css" href='/Tools/js/css/ui-lightness/jquery-ui-1.8rc3.custom.css' rel="stylesheet" />
    <link href="/Themes/OWriteOff/plugin/tag-it/css/jquery.tagit.css" rel="stylesheet" type="text/css" />


    <script src='/Tools/js/jquery-ui.js' type="text/javascript"></script>
    <script type="text/javascript" src="/Tools/js/jquery.ui.datepicker-zh-TW.js"></script>
    <script type="text/javascript" src="/Tools/ckeditor/ckeditor.js"></script>
    <script type="text/javascript" src="/Themes/OWriteOff/plugin/bpopup/jquery.bpopup.js"></script>
    <script src="/Themes/OWriteOff/plugin/tag-it/js/tag-it.js" type="text/javascript" charset="utf-8"></script>
    <link type="text/css" href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <style>
        body {
            background-color: #fff0d4;
            overflow: auto;
        }

        td, th {
            padding: 15px 5px;
            display: table-cell;
            text-align: left;
            vertical-align: top;
            border-radius: 2px;
        }
    </style>

    <form id="BlogForm" action="BlogContent.aspx" method="post">
        <div class="row">
            <div class="col s12">
                <div class="form-unit">
                    <label class="unit-label">
                        標題
                    </label>
                    <div class="data-input">

                        <p>
                            <asp:TextBox ID="txtGuid" runat="server" Style="width: 50%; display: none" ClientIDMode="Static"></asp:TextBox>
                            <asp:TextBox ID="txtTitle" runat="server" Style="width: 50%;" ClientIDMode="Static" MaxLength="255"></asp:TextBox>
                        </p>
                    </div>
                </div>
                <div class="form-unit">
                    <label class="unit-label">
                        關鍵字
                    </label>
                    <div class="data-input">
                        <ul id="ulKeyword" style="width: 50%">
                        </ul>
                        <p>
                            <asp:TextBox ID="txtKeyWord" runat="server" Style="width: 50%; display: none" ClientIDMode="Static" MaxLength="255"></asp:TextBox>
                        </p>
                    </div>
                </div>
                <div class="form-unit">
                    <label class="unit-label">
                        描述
                    </label>
                    <div class="data-input">

                        <p>
                            <asp:TextBox ID="txtDescription" runat="server" Style="width: 50%;" ClientIDMode="Static" MaxLength="255"></asp:TextBox>
                        </p>
                    </div>
                </div>
                <div class="form-unit">
                    <label class="unit-label">
                        發布日期
                    </label>
                    <div class="data-input">
                        <p>
                            <asp:TextBox ID="txtBeginDate" runat="server" Style="width: 20%" class="datepicker" ClientIDMode="Static" MaxLength="10"></asp:TextBox>

                        </p>
                    </div>
                </div>
                <%--<div class="form-unit">
                    <label class="unit-label">
                        排序
                    </label>
                    <div class="data-input">
                        <p>
                            <asp:TextBox ID="txtSort" runat="server" Style="width: 50px;" ClientIDMode="Static" Text="9999" MaxLength="4"></asp:TextBox>
                        </p>
                    </div>
                </div>--%>
                <%--<div class="form-unit">
                    <label class="unit-label">
                        狀態
                    </label>
                    <div class="data-input">
                        <p>
                            <asp:CheckBox ID="chkStatus" runat="server" ClientIDMode="Static" class="filled-in" Checked="true" />啟用
                        </p>
                    </div>
                </div>--%>
                <div class="form-unit">
                    <label class="unit-label">
                        文章內容
                    </label>
                    <div class="data-input">
                        <p>
                            <asp:TextBox ID="txtContent" runat="server" TextMode="MultiLine" ClientIDMode="Static"></asp:TextBox>
                            <%--                        <textarea id="BlogContent" style="display:none" rows="10" cols="10"></textarea>--%>
                        </p>
                    </div>
                </div>
            </div>
        </div>

        <div style="text-align: center">
            <%--<input type="button" id="btnSave" value="儲存" class="btn" />--%>
            <asp:Button ID="btnSave" runat="server" Text="儲存" OnClick="btnSave_Click" OnClientClick="return checkData();" CssClass="btn" />
            <a class="waves-effect waves-light btn secondary" onclick="deletePost();"><i class="material-icons left">delete</i>刪除</a>
            <input type="button" id="btnCancel" value="返回列表" class="btn secondary" />
        </div>
    </form>

    <div id="bpopup_loading_saving" class="card-panel bpopup-modal bpopup-modal-alert">
        <div class="row">
            <div class="col s12 bpopup-modal-title">
                <i class="fa fa-spinner fa-spin fa-3x fa-fw" aria-hidden="true"></i>資料處理中，請稍候...
            </div>
        </div>
    </div>
    <div id="bpopup-toast-save-success" class="card-panel bpopup-modal bpopup-modal-toast">
        <div class="row">
            <div class="col s12 bpopup-modal-title toast_message">
                資料已更新!
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function () {
            initControl();

            $("#btnCancel").click(function () {
                if (!confirm("確定要離開?")) {
                    return false;
                }
                location.href = "BlogList.aspx";
            });
            $("form").submit(function () {
                var loadSaving = $('#bpopup_loading_saving').bPopup({
                    //autoClose: 1500,
                });
                $("#btnSave").hide();
            });
        });

        function deletePost() {
            if (!confirm("確定要刪除此文章")) {
                return false;
            }
            var loadSaving = $('#bpopup_loading_saving').bPopup({
                //autoClose: 1500,
            });
            $.ajax({
                type: "POST",
                url: "BlogContent.aspx/DeletePost",
                data: " { 'guid': '" + $("#txtGuid").val() + "' }",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    location.href = "BlogList.aspx";
                },
                complete: function () {
                },
                failure: function (response) {
                    console.log(response.d);
                },
                error: function (response) {
                    console.log(response);
                }
            });
        }

        function completeSave(toast_message_text) {
            $("#bpopup-toast-save-success").find('.toast_message').text(toast_message_text);
            $('#bpopup-toast-save-success').bPopup({
                autoClose: 1000,//toast的維持時間
                speed: 750,//toast漸入漸出的動畫秒數
                opacity: 0,//toast的背景為全透明
                modalClose: true,//toast浮現期間，點擊背景可直接取消
            });
        }

        function checkData() {
            var Content = htmlEncode(CKEDITOR.instances.txtContent.getData());
            var keyword = $("#ulKeyword").tagit('assignedTags');
            var keyList = [];
            $.each(keyword, function (idx, obj) {
                keyList.push(obj);
            });

            $("#txtKeyWord").val(JSON.stringify(keyList));

            if ($.trim($("#txtTitle").val()) == "") {
                alert("請輸入標題");
                return false;
            }
            if ($.trim($("#txtBeginDate").val()) == "") {
                alert("請輸入起始日期");
                return false;
            }
            if ($.trim($("#txtKeyWord").val()) == "") {
                alert("請輸入關鍵字");
                return false;
            }
            if ($.trim($("#txtDescription").val()) == "") {
                alert("請輸入描述");
                return false;
            }
            if ($.trim(Content) == "") {
                alert("請輸入內容");
                return false;
            }

            CKEDITOR.instances.txtContent.setData(Content);
        }
        function checkDate(d) {
            if (d.length != 10) {
                return false;
            }
            if (d.indexOf("/") == -1) {
                return false;
            }
            return true;
        }
        function htmlEncode(value) {
            return $('<div/>').text(value).html();
        }
        function initControl() {
            $(".datepicker").datepicker({
                dateFormat: "yy/mm/dd"
            });

            var arrKeyword;
            if ($("#txtKeyWord").val() != "") {
                arrKeyword = eval($("#txtKeyWord").val());

                $('#ulKeyword').empty();
                $.each(arrKeyword, function (idx, obj) {
                    $('#ulKeyword').append("<li>" + obj + "</li>");
                });
            }



            $('#ulKeyword').tagit();

            var bidpath = "<%= BlogGid%>";
            var foldercat = bidpath.substring(0, 1);

            CKEDITOR.replace('txtContent', {
                customConfig: '/Tools/ckeditor/config.proposal.js',
                extraPlugins: 'youtube,uploadimage',
                imageMaxWidth: 960,
                enterMode : CKEDITOR.ENTER_BR,
                filebrowserBrowseUrl: '/Tools/ckeditor/ckfinder/ckfinder.html',
                filebrowserImageBrowseUrl: '/Tools/ckeditor/ckfinder/ckfinder.html?type=Images&startupPath=Images:/blog/' + bidpath + '/',
                filebrowserImageUploadUrl: '/Tools/ckeditor/ckfinder/core/connector/aspx/connector.aspx?command=QuickUpload&type=Images&bid=' + bidpath + "&maxwidth=960"
            });
        }
    </script>
</asp:Content>

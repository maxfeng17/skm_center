﻿using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LunchKingSite.Web.ControlRoom.Blog
{
    public partial class BlogList : RolePage, IBlogListView
    {
        protected IBlogProvider _blogProv = ProviderFactory.Instance().GetProvider<IBlogProvider>();
        private BlogListPresenter _presenter;
        public BlogListPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        public string UserName
        {
            get { return Page.User.Identity.Name; }
        }

        public int PageSize
        {
            get { return ucPager.PageSize; }
            set { ucPager.PageSize = value; }
        }

        public int CurrentPage
        {
            get
            {
                return ucPager.CurrentPage;
            }
        }

        public string BlogTitle
        {
            get
            {
                return txtTitle.Text.Trim();
            }
        }
        public DateTime BeginDate
        {
            get
            {
                DateTime beginDate = DateTime.MinValue.Date;
                DateTime.TryParse(txtBeginDate.Text.Trim(), out beginDate);
                return beginDate;
            }
        }

        public DateTime EndDate
        {
            get
            {
                DateTime endDate = DateTime.MinValue.Date;
                DateTime.TryParse(txtEndDate.Text.Trim(), out endDate);
                return endDate;
            }
        }

        #region Event
        public event EventHandler Search;
        public event EventHandler<DataEventArgs<int>> OnGetCount;
        public event EventHandler<DataEventArgs<int>> PageChanged;
        public void SetBlogListContent(List<BlogPost> blogList)
        {
            rptBlogList.DataSource = blogList;
            rptBlogList.DataBind();
        }
        #endregion Event

        #region page
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Presenter.OnViewInitialized();
                ucPager.ResolvePagerView(CurrentPage, true);
            }
            _presenter.OnViewLoaded();
        }
        protected void UpdateHandler(int pageNumber)
        {
            if (this.PageChanged != null)
            {
                this.PageChanged(this, new DataEventArgs<int>(pageNumber));
            }
        }

        protected int GetCount()
        {
            DataEventArgs<int> e = new DataEventArgs<int>(0);
            if (OnGetCount != null)
            {
                OnGetCount(this, e);
            }
            return e.Data;
        }

        protected void rptBlogList_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is BlogPost)
            {
                Label lblId = (Label)e.Item.FindControl("lblId");
                Label lblCount = (Label)e.Item.FindControl("lblCount");
                List<string> filter = new List<string>();
                filter.Add(BlogPageView.Columns.BlogId + "=" + lblId.Text);

                int c = _blogProv.BlogPageViewGetCount(filter.ToArray());
                lblCount.Text = c.ToString();
            }
        }
        protected void btnNew_Click(object sender, EventArgs e)
        {
            Response.Redirect("BlogContent.aspx");
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            if (this.Search != null)
            {
                this.Search(this, e);
            }
            ucPager.ResolvePagerView(CurrentPage, true);
        }
        #endregion page

        #region method
        public string GetKeyWord(string key)
        {
            if (string.IsNullOrEmpty(key))
            {
                return string.Empty;
            }
            string[] data = new JsonSerializer().Deserialize<string[]>(key);

            return string.Join(",", data);
        }
        #endregion method
    }
}
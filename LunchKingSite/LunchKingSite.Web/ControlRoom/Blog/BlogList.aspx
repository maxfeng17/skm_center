﻿<%@ Page Language="C#" MasterPageFile="~/ControlRoom/backend.master" AutoEventWireup="true"
    CodeBehind="BlogList.aspx.cs" Inherits="LunchKingSite.Web.ControlRoom.Blog.BlogList" %>

<%@ Import Namespace="LunchKingSite.Core" %>
<%@ Import Namespace="LunchKingSite.DataOrm" %>
<%@ Register Src="~/User/Pager.ascx" TagName="Pager" TagPrefix="uc1" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
    <link type="text/css" rel="stylesheet" href="/Themes/OWriteOff/plugin/materialize/css/materialize.css" media="screen,projection" />
    <link type="text/css" rel="stylesheet" href="/Themes/OWriteOff/plugin/font-awesome/font-awesome.css" />
    <link type="text/css" rel="stylesheet" href="/Themes/OWriteOff/OWriteOff.css" />
    <link type="text/css" rel="stylesheet" href="/Themes/OWriteOff/css/style.css" media="screen,projection" />
    <link type="text/css" href='/Tools/js/css/ui-lightness/jquery-ui-1.8rc3.custom.css' rel="stylesheet" />
    <link type="text/css" href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <script type="text/javascript" src="/Tools/js/jquery.ui.datepicker-zh-TW.js"></script>
    <script src='/Tools/js/jquery-ui.js' type="text/javascript"></script>

    <style>
        body {
            background-color: #fff0d4;
            overflow: auto;
        }

        td, th {
            padding: 15px 5px;
            display: table-cell;
            text-align: left;
            vertical-align: top;
            border-radius: 2px;
        }
    </style>

    <div class="row">
        <div class="col xl6 l12 m12 s12">
            <div class="form-unit">
                <label class="unit-label">
                    標題
                </label>
                <div class="data-input">
                    <p>
                        <asp:TextBox ID="txtTitle" runat="server"></asp:TextBox>
                    </p>
                </div>
            </div>


            <div class="divider hide-on-xlarge-only"></div>
        </div>

        <div class="col xl6 l12 m12 s12">
            <div class="form-unit">
                <label class="unit-label">
                    發布日期
                </label>
                <div class="data-input">
                    <p>
                        <asp:TextBox ID="txtBeginDate" runat="server" Style="width: 200px" class="datepicker"></asp:TextBox>
                        ~ 
                        <asp:TextBox ID="txtEndDate" runat="server" Style="width: 200px" class="datepicker"></asp:TextBox>
                    </p>
                </div>
            </div>

        </div>
    </div>
    <div class="row">
        <div class="col xl6 l12 s12 right-align">
            <asp:Button ID="btnSearch" runat="server" Text="搜尋" OnClick="btnSearch_Click" CssClass="btn" />
            <asp:Button ID="btnNew" runat="server" Text="新增文章" OnClick="btnNew_Click" CssClass="btn secondary" />
        </div>

    </div>


    <asp:Repeater ID="rptBlogList" runat="server" OnItemDataBound="rptBlogList_ItemDataBound">
        <HeaderTemplate>
            <table width="100%" class="bordered">
                <thead>
                    <tr>
                        <th style="width: 5%; text-align: center">編號
                        </th>
                        <th style="width: 25%; text-align: center">標題
                        </th>
                        <th style="width: 10%; text-align: center">發布日期
                        </th>
                        <th style="width: 20%; text-align: center">關鍵字
                        </th>
                        <th style="width: 5%; text-align: center">瀏覽人數
                        </th>
                        <th style="width: 15%; text-align: center">修改日期
                        </th>
                        <th style="width: 10%; text-align: center">修改人員
                        </th>
                        <th style="width: 15%; text-align: center">功能
                        </th>
                    </tr>
                </thead>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
                <td style="text-align: center">
                    <asp:Label ID="lblId" runat="server" Text="<%# ((BlogPost)Container.DataItem).Id %>"></asp:Label>
                </td>
                <td>
                    <a style="color: blue; text-decoration: underline;" href="BlogContent.aspx?id=<%# ((BlogPost)Container.DataItem).Id %>"><%# ((BlogPost)Container.DataItem).Title %></a>
                </td>
                <td style="text-align: center">
                    <%# ((BlogPost)Container.DataItem).BeginDate.ToString("yyyy/MM/dd") %>
                </td>
                <td style="text-align: center">
                    <%# GetKeyWord(((BlogPost)Container.DataItem).KeyWord) %>
                </td>
                <td style="text-align: center">
                    <asp:Label ID="lblCount" runat="server" Text="0"></asp:Label>
                </td>
                <td style="text-align: center">
                    <%# ((BlogPost)Container.DataItem).ModifyTime %>
                </td>
                <td style="text-align: center">
                    <%# ((BlogPost)Container.DataItem).ModifyId %>
                </td>
                <td style="text-align: center">
                    <a style="color: blue; text-decoration: underline;" href="BlogContent.aspx?id=<%# ((BlogPost)Container.DataItem).Id %>">編輯</a> | 
                    <a style="color: blue; text-decoration: underline;" target="_blank" href="/blog/<%# ((BlogPost)Container.DataItem).Guid %>">預覽</a>
                </td>
            </tr>
        </ItemTemplate>
        <FooterTemplate>
            </table>
        </FooterTemplate>
    </asp:Repeater>

    <div style="text-align: center">
        <uc1:Pager ID="ucPager" runat="server" PageSize="10" ongetcount="GetCount" onupdate="UpdateHandler"></uc1:Pager>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            initControl();
        });
        function initControl() {
            $(".datepicker").datepicker({
                dateFormat: "yy/mm/dd"
            });
        }
    </script>

</asp:Content>

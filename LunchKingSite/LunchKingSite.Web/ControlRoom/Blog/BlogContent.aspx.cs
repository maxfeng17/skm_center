﻿using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;

namespace LunchKingSite.Web.ControlRoom.Blog
{
    public partial class BlogContent : RolePage, IBlogContentView
    {
        private BlogContentPresenter _presenter;
        protected static IBlogProvider _blogProv = ProviderFactory.Instance().GetProvider<IBlogProvider>();
        public BlogContentPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }
        public string UserName
        {
            get { return Page.User.Identity.Name; }
        }
        public int Id
        {
            get
            {
                int id = 0;
                if(Request["id"] != null)
                {
                    int.TryParse(Request["id"], out id);
                }                
                return id;
            }
        }
        public Guid BlogGid
        {
            get
            {
                Guid bid = Guid.Empty;
                Guid.TryParse(txtGuid.Text, out bid);
                return bid;
            }
            set
            {
                txtGuid.Text = value.ToString();
            }
        }

        public string BlogTitle
        {
            get
            {
                return txtTitle.Text.Trim();
            }
        }
        public string BlogKeyWord
        {
            get
            {
                return txtKeyWord.Text;
            }
        }
        public string BlogDescription
        {
            get
            {
                return txtDescription.Text.Trim();
            }
        }
        public DateTime BeginDate
        {
            get
            {
                DateTime beginDate = DateTime.MinValue.Date;
                DateTime.TryParse(txtBeginDate.Text.Trim(), out beginDate);
                return beginDate;
            }
        }
        
        public string BlogContentText
        {
            get
            {
                return txtContent.Text;
            }
        }
        #region Event
        public event EventHandler Save;
        public void SetBlogContent(BlogPost blog)
        {
            if (blog.IsLoaded)
            {
                txtTitle.Text = blog.Title;
                if (blog.BeginDate.Year != DateTime.MinValue.Year)
                {
                    txtBeginDate.Text = blog.BeginDate.ToString("yyyy/MM/dd");
                }
                txtContent.Text = HttpUtility.HtmlDecode(blog.BlogContent);
                txtDescription.Text = blog.BlogDescription;
                txtKeyWord.Text = blog.KeyWord;
            }
            else
            {
                txtBeginDate.Text = DateTime.Today.ToString("yyyy/MM/dd");
            }
        }
        #endregion Event

        #region page
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Presenter.OnViewInitialized();
            }
            _presenter.OnViewLoaded();
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (this.Save != null)
            {
                this.Save(this, e);
            }
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("BlogList.aspx");
        }


        #endregion page

        #region method
        public void ShowMessage(string msg)
        {
            string script = string.Empty;
            script = string.Format("completeSave('{0}'); ", msg);

            if (!string.IsNullOrWhiteSpace(script))
            {
                Page.ClientScript.RegisterStartupScript(GetType(), "alert", script, true);
            }
        }
        #endregion methid

        #region webmethod
        [WebMethod]
        public static bool DeletePost(string guid)
        {
            Guid bid = Guid.Empty;
            Guid.TryParse(guid, out bid);
            if(bid != Guid.Empty)
            {
                BlogPost blog = _blogProv.BlogPostGetAllByGid(bid);
                if (blog.IsLoaded)
                {
                    blog.Status = -1;
                    _blogProv.BlogPostSet(blog);
                }
            }
            return true;
        }
        #endregion webmethod

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.Core;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.UI;

namespace LunchKingSite.Web.ControlRoom.Order
{
    public partial class refunPcash : RolePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnRun_Click(object sender, EventArgs e)
        {
            IOrderProvider op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
            string[] sR = txtData.Text.Split("\r\n");
            string feeback = "";
            foreach (string s in sR)
            {
                try
                {
                    string[] sC = s.Split(',');
                    string transId = sC[0];
                    string authCode = sC[1];
                    decimal amount = decimal.Parse(sC[2]);
                    string result = PCashWorker.DeCheckOut(transId, authCode, amount);

                    Guid orderGuid = Guid.Empty;
                    PaymentTransaction pt = op.PaymentTransactionGet(transId, LunchKingSite.Core.PaymentType.PCash, PayTransType.Authorization);
                    if (pt.OrderGuid != null) orderGuid = pt.OrderGuid.Value;

                    PayTransResponseType responseType = PayTransResponseType.OK;
                    PayTransPhase transPhase = PayTransPhase.Created;
                    if (result == "0000")
                    {
                        transPhase = PayTransPhase.Successful;
                    }
                    else
                    {
                        transPhase = PayTransPhase.Failed;
                        responseType = PayTransResponseType.GenericError;
                    }
                    int status = Helper.SetPaymentTransactionPhase(Helper.SetPaymentTransactionDepartmentTypes(0, DepartmentTypes.Ppon), transPhase);

                    PaymentFacade.NewTransaction(transId, orderGuid, LunchKingSite.Core.PaymentType.PCash, amount, authCode,
                        PayTransType.Refund, DateTime.Now, Page.User.Identity.Name, result, status, responseType);
                    if (result == "0000")
                        result = "成功";
                    else
                        feeback = feeback + "transId:" + transId + " fail result(" + result + ")<br/>";
                }
                catch (Exception ex)
                {
                    feeback = feeback + "錯誤:" +ex.Message+")<br/>";
                }

            }
            lblresult.Text = feeback;
        }
    }
}
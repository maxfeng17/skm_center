﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OrderShipBatchClear.aspx.cs" MasterPageFile="~/ControlRoom/backend.master" Inherits="LunchKingSite.Web.ControlRoom.Order.OrderShipBatchClear" %>
<%@ Import Namespace="LunchKingSite.Core" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <script type="text/javascript" src="../../Tools/js/jquery-1.4.2.min.js"></script>
    <script type="text/javascript" src="../../Tools/js/jquery-ui.1.8.min.js"></script>
    <script type="text/javascript" src="../../Tools/js/jquery.ui.datepicker-zh-TW.js"></script>
    <link rel="stylesheet" type="text/css" href="../../Tools/js/css/ui-lightness/jquery-ui-1.8rc3.custom.css"/> 
    <style type="text/css">
        .tblDealInfo
        {
            border:1px solid grey;
        }        
    
        .tblDealInfo td
        {
            border:1px solid grey;
        }
    
        .tblDealInfo tr 
        {
            border:1px solid grey;
        }  
        .ui-datepicker { width: 17em; padding: .2em .2em 0; display: none; font-size: 80.5%; }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.date').datepicker();
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table>
        <tr>
            <td><asp:DropDownList ID="ddlDealType" runat="server" DataTextField="Value"  AutoPostBack="true"  DataValueField="Key" Width="103px" OnSelectedIndexChanged="ddlDealType_SelectedIndexChanged" ClientIDMode="Static"/></td>
            <td><asp:DropDownList ID="ddlQueryOption" runat="server" DataTextField="Value" DataValueField="Key" Width="103px" ClientIDMode="Static" /></td>
            <td><asp:TextBox ID="txtKeyWord" runat="server" Width="265px" onkeypress="en(event)" ClientIDMode="Static"></asp:TextBox></td>
        </tr>
        <tr>
            <td></td>
            <td>
                <asp:DropDownList ID="ddlQueryTime" runat="server" DataTextField="Value" DataValueField="Key" Width="103px" ClientIDMode="Static">
                    <asp:ListItem Value="orderTime">銷售期間</asp:ListItem> 
                    <asp:ListItem Value="useTime">兌換期間</asp:ListItem> 
                </asp:DropDownList>
            </td>
            <td>
                <asp:TextBox ID="txtDS" runat="server" Columns="12" CssClass="date" ClientIDMode="Static"/>
                至
                <asp:TextBox ID="txtDE" runat="server" Columns="12" CssClass="date" ClientIDMode="Static"/>
                <asp:Button ID="btnSearch" runat="server" Text="查詢" OnClick="SearchClick" ClientIDMode="Static"/>
            </td>
        </tr>
    </table>
    <asp:Label ID="lbQueryResult" runat="server"></asp:Label>
    <asp:Panel ID="setDealInfo" runat="server" Visible="false">
    <table style="width:100%" class="tblDealInfo">
        <tr style="text-align:center">
            <td style="width: 120px">
                賣家名稱
            </td>
            <td style="width: 60px">
                檔號
            </td>
            <td style="width: 250px">
                檔次名稱
            </td>
            <td style="width: 200px">
                配送期間
            </td>
            <td style="width: 120px">
                清冊資訊
            </td>
            <td style="width: 120px">
                刪除出貨資料
            </td>
        </tr>        
        <asp:Repeater ID="repDealList" runat="server"  OnItemCommand="repDealList_ItemCommand" OnItemDataBound="repDealList_ItemDataBound">        
            <ItemTemplate>
                <tr>                    
                    <td style="width: 90px">
                        <asp:Literal ID="ltSellerName" runat="server" Text='<%# Eval("SellerName")%>' /> 
                    </td>
                    <td style="width: 60px">
                        <asp:Literal ID="ltUniqueId" runat="server" Text='<%# Eval("UniqueId")%>' />
                    </td>
                    <td style="width: 90px">
                        <asp:HyperLink ID="ltDealName" runat="server" NavigateUrl='<%# int.Parse(Eval("OrderClassification").ToString()) == (int)OrderClassification.LkSite ? Eval("ProductGuid", "../../Ppon/default.aspx?bid={0}") : Eval("UniqueId", "../../piinlife/HiDeal.aspx?pid=={0}") %>' Target="_blank"
                            Text='<%# Eval("ProductName")%>'></asp:HyperLink>                   
                    </td>
                    <td style="width: 80px">
                        <asp:Literal ID="ltDeliveyRange" runat="server" Text='<%# GetTheDate(DataBinder.Eval(Container, "DataItem.UseStartTime").ToString()) + " 至 " + GetTheDate(DataBinder.Eval(Container, "DataItem.UseEndTime").ToString())%>' /> 
                    </td>
                    <td style="width: 90px">
                        <asp:Literal ID="ltTotalCount" runat="server" Text='<%# "清冊訂單 " + Eval("TotalCount")%>' /><br/>
                        <asp:Literal ID="ltShipCount" runat="server" Text='<%# "已核銷訂單 " + Eval("ShipCount")%>' /> <br/>
                        <asp:Literal ID="ltUnShipCount" runat="server" Text='<%# "未核銷訂單 " + Eval("UnShipCount")%>' /><br/>
                        <asp:Literal ID="ltReturnCount" runat="server" Text='<%# "退貨訂單 " + Eval("ReturnCount")%>' /> 
                    </td>
                    <td style="width: 80px;text-align:center">
                        <asp:button ID="btnOrderShipClear" runat="server" Text="刪除" CommandName="ClearOrderShip" CommandArgument='<%# Eval("ProductGuid")%>' Visible='<%# Eval("ShippedDate") != null ? false : true %>' ClientIDMode ="Static"/>
                        <asp:Label ID="lbShipStatus" runat="server" Visible='<%# Eval("ShippedDate") != null ? true : false %>' Text='<%# Eval("ShippedDate") != null ? "已壓記出貨回覆日" : string.Empty %>'></asp:Label>
                    </td>
                </tr>            
            </ItemTemplate>
        </asp:Repeater>
    </table>
    </asp:Panel>
</asp:Content>

﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;
using LunchKingSite.BizLogic.Component;
using System.Linq;
using LunchKingSite.Core.Component;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;

public partial class order_return : RolePage, IOrderReturnView
{
    public event EventHandler<DataEventArgs<int>> GetOrderCount;
    public event EventHandler SortClicked;
    public event EventHandler<DataEventArgs<int>> PageChanged;
    public event EventHandler SearchClicked;
    public event EventHandler ExportToExcel;


    protected static IOrderProvider op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
    protected static IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
    protected static IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();

    #region props
    private OrderReturnPresenter _presenter;
    public OrderReturnPresenter Presenter
    {
        set
        {
            this._presenter = value;
            if (value != null)
            {
                this._presenter.View = this;
            }
        }
        get
        {
            return this._presenter;
        }
    }

    public string SortExpression
    {
        get { return (ViewState != null && ViewState["se"] != null) ? (string)ViewState["se"] : ViewOrderMemberBuildingSeller.Columns.OrderCreateTime + " desc"; }
        set { ViewState["se"] = value; }
    }

    public string FilterUser
    {
        get { return tbSearch.Text; }
        set { tbSearch.Text = value; }
    }

    /*
    public string[] FilterInternal
    {
        get
        {
            string[] filter = new string[3];

            if (!string.IsNullOrEmpty(tbDS.Text))
                filter[0] = ViewOrderMemberBuildingSeller.Columns.OrderCreateTime + " >= " + tbDS.Text;
            else
                filter[0] = null;

            if (!string.IsNullOrEmpty(tbDE.Text))
                filter[1] = ViewOrderMemberBuildingSeller.Columns.OrderCreateTime + " < " + tbDE.Text;
            else
                filter[1] = null;
            return filter;
        }
    }
    */

    public string FilterType
    {
        get { return ddlST.SelectedValue; }
        set { ddlST.SelectedValue = value; }
    }

    public DateTime? MinOrderCreateTime
    {
        get
        {
            DateTime result;
            if (DateTime.TryParse(tbDS.Text, out result))
            {
                return result;
            }

            return null;
        }
    }

    public DateTime? MaxOrderCreateTime
    {
        get
        {
            DateTime result;
            if (DateTime.TryParse(tbDE.Text, out result))
            {
                return result;
            }

            return null;
        }
    }

    public string SearchExpression
    {
        get { return tbSearch.Text; }
    }


    public int PageSize
    {
        get { return gridPager.PageSize; }
        set { gridPager.PageSize = value; }
    }

    public int CurrentPage
    {
        get { return gridPager.CurrentPage; }
    }

    /*
    public string OrderByInternal
    {
        get { return ViewOrderMemberBuildingSeller.Columns.OrderCreateTime + " desc"; }
    }
    */

    public string DateType
    {
        get { return ddlDate.SelectedValue; }
    }

    public string CsvType
    {
        get { return ddlType.SelectedValue; }
    }



    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        //gvOrder.Columns[0].SortExpression = ViewOrderMemberBuildingSeller.Columns.OrderId;
        //gvOrder.Columns[2].SortExpression = ViewOrderMemberBuildingSeller.Columns.ProductDeliveryType;
        //gvOrder.Columns[3].SortExpression = ViewOrderMemberBuildingSeller.Columns.OrderCreateTime;
        /*
        gvOrder.Columns[0].SortExpression = ViewOrderMemberBuildingSeller.Columns.MemberName;
        gvOrder.Columns[7].SortExpression = ViewOrderMemberBuildingSeller.Columns.OrderCreateTime;
        gvOrder.Columns[8].SortExpression = ViewOrderMemberBuildingSeller.Columns.DeliveryTime;
        gvOrder.Columns[9].SortExpression = ViewOrderMemberBuildingSeller.Columns.SellerName;
        gvOrder.Columns[10].SortExpression = ViewOrderMemberBuildingSeller.Columns.Total;
        */
        if (!Page.IsPostBack)
        {
            this.Presenter.OnViewInitialized();
            this.gridPager.Visible = false;
        }
        else
        {
            this.gridPager.Visible = true;
            this.Presenter.OnViewLoaded();
        }


    }

    public void SetOrderList(ViewOrderMemberBuildingSellerCollection orders)
    {
        GridPanel.Visible = true;
        gvOrder.DataSource = orders;
        gvOrder.DataBind();
    }


    public void SetFilterTypeDropDown(Dictionary<string, string> types)
    {
        ddlST.DataSource = types;
        ddlST.DataBind();
    }



    public string GetFilterTypeDropDown()
    {
        return ddlST.SelectedValue;
    }

    protected void bSearchBtn_Click(object sender, EventArgs e)
    {
        if (this.SearchClicked != null)
            this.SearchClicked(this, e);
        gridPager.ResolvePagerView(1, true);
    }

    protected void ImportExcel_Click(object sender, EventArgs e)
    {
        if (ExportToExcel != null)
        {
            ExportToExcel(this, e);
        }
    }

    protected void gvOrder_Sorting(object sender, GridViewSortEventArgs e)
    {
        ViewState["sd"] = ((string)ViewState["sd"] == " ASC") ? " DESC" : " ASC";
        this.SortExpression = e.SortExpression + (string)ViewState["sd"];
        if (this.SortClicked != null)
            SortClicked(this, new EventArgs());
        gridPager.ResolvePagerView(1);
    }

    protected void UpdateHandler(int pageNumber)
    {
        DataEventArgs<int> e = new DataEventArgs<int>(pageNumber);
        if (this.PageChanged != null)
            this.PageChanged(this, e);
    }

    protected int RetrieveOrderCount()
    {
        DataEventArgs<int> e = new DataEventArgs<int>(0);
        if (GetOrderCount != null)
            GetOrderCount(this, e);
        return e.Data;
    }



    protected void gvOrder_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.DataItem is ViewOrderMemberBuildingSeller)
        {
            string itemName = string.Empty;
            ViewOrderMemberBuildingSeller item = (ViewOrderMemberBuildingSeller)e.Row.DataItem;
            Label lblStoreConfirm = ((Label)e.Row.FindControl("lblStoreConfirm"));
            Label lblQuestionType = ((Label)e.Row.FindControl("lblQuestionType"));
            Label lblCsvReturnDate = ((Label)e.Row.FindControl("lblCsvReturnDate"));
            Label lblId = ((Label)e.Row.FindControl("lblId"));
            HyperLink lblOrderId = ((HyperLink)e.Row.FindControl("lblOrderId"));
            Label lblStoreName = ((Label)e.Row.FindControl("lblStoreName"));
            Label lblShipmentNo = ((Label)e.Row.FindControl("lblShipmentNo"));
            Label lblReceiver = ((Label)e.Row.FindControl("lblReceiver"));
            Label lblProduct = ((Label)e.Row.FindControl("lblProduct"));
            Label lblSpec = ((Label)e.Row.FindControl("lblSpec"));
            Label lblCount = ((Label)e.Row.FindControl("lblCount"));
            Label lblPrice = ((Label)e.Row.FindControl("lblPrice"));
            Label lblOrderDate = ((Label)e.Row.FindControl("lblOrderDate"));

            if (item.DcReturn != 0)
            {
                lblStoreConfirm.Text = item.DcReturn == 1 ? "未確認" : "確認";
                lblQuestionType.Text = "刷退<br />" + item.DcReturnReason;
                lblCsvReturnDate.Text = item.DcReturnTime == null ? "" : item.DcReturnTime.Value.ToString("yyyy/MM/dd");
            }
            else
            {
                if (item.DcAcceptStatus == (int)IspDcAcceptStatus.None)
                {
                    lblStoreConfirm.Text = "請儘速出貨";
                }
                else if (item.DcAcceptStatus == (int)IspDcAcceptStatus.S03 || item.DcAcceptStatus == (int)IspDcAcceptStatus.N05)
                {
                    lblStoreConfirm.Text = "";
                }
                else
                {
                    lblStoreConfirm.Text = item.DcReceiveFail == 1 ? "未確認" : "確認";
                }
                lblQuestionType.Text = "驗退<br />" + item.DcReceiveFailReason;
                lblCsvReturnDate.Text = item.DcReceiveFailTime == null ? "" : item.DcReceiveFailTime.Value.ToString("yyyy/MM/dd");
            }
            lblId.Text = item.UniqueId.ToString();
            lblOrderId.Text = item.OrderId;
            lblOrderId.NavigateUrl = "~/controlroom/Order/order_detail.aspx?oid=" + item.OrderGuid.ToString();
            lblOrderId.Target = "_blank";
            if (item.ProductDeliveryType == (int)ProductDeliveryType.FamilyPickup)
            {
                lblStoreName.Text = "全家<br />" + item.FamilyStoreName;
            }
            else if (item.ProductDeliveryType == (int)ProductDeliveryType.SevenPickup)
            {
                lblStoreName.Text = "7-11<br />" + item.SevenStoreName;
            }
            lblShipmentNo.Text = item.ShipNo;
            lblReceiver.Text = item.MemberName;

            CashTrustLogCollection ctls = mp.CashTrustLogListGetByOrderId(item.OrderGuid);
            IViewPponDeal theDeal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(item.BusinessHourGuid);
            List<int> uniqueIds = new List<int>();
            uniqueIds.Add(item.UniqueId);
            var orderProductOptionLists = op.ViewOrderProductOptionListGetIsCurrentList(uniqueIds);
            var orderItemLists = op.GetOrderProductOptionList(orderProductOptionLists)
                                        .GroupBy(x => x.OrderGuid)
                                        .Select(x => new
                                        {
                                            x.Key,
                                            OptionDescription = string.Join("<br />", x.Select(o => o.OptionDescription)),
                                            ItemNo = string.Join("\n", x.Select(o => o.ItemNo.Trim(','))),
                                            Quantity = string.Join("<br />", x.Select(o => o.Quantity)),
                                            ItemCount = x.Sum(o => o.Quantity)
                                        })
                                        .ToDictionary(x => x.Key, x => x);
            var dealCost = pp.DealCostGetList(item.BusinessHourGuid);

            lblCount.Text = (orderItemLists.ContainsKey(item.OrderGuid)) ? orderItemLists[item.OrderGuid].Quantity : string.Empty;
            lblProduct.Text = theDeal.ItemName;
            lblSpec.Text = (orderItemLists.ContainsKey(item.OrderGuid)) ? orderItemLists[item.OrderGuid].OptionDescription : string.Empty;
            lblPrice.Text = dealCost.Count > 0 ? (dealCost[dealCost.Count() - 1].Cost ?? 0m).ToString() + "<br />" + theDeal.ItemPrice.ToString() : 0m + "<br />" + theDeal.ItemPrice.ToString();
            if (item.ShipTime == null)
            {
                lblOrderDate.Text = item.OrderCreateTime.ToString("yyyy/MM/dd");
            }
            else
            {
                lblOrderDate.Text = item.OrderCreateTime.ToString("yyyy/MM/dd") + "<br />" + item.ShipTime.Value.ToString("yyyy/MM/dd");
            }
        }
    }

    public void Export(ViewOrderMemberBuildingSellerCollection orders)
    {
        Workbook workbook = new HSSFWorkbook();

        Sheet sheet = workbook.CreateSheet("超商刷驗退清冊");
        /*列表寬度設定*/
        sheet.SetColumnWidth(0, 20 * 256);
        sheet.SetColumnWidth(1, 10 * 256);
        sheet.SetColumnWidth(2, 34 * 256);
        sheet.SetColumnWidth(3, 14 * 256);
        sheet.SetColumnWidth(4, 14 * 256);
        sheet.SetColumnWidth(5, 20 * 256);
        sheet.SetColumnWidth(6, 10 * 256);
        sheet.SetColumnWidth(7, 18 * 256);
        sheet.SetColumnWidth(8, 20 * 256);
        sheet.SetColumnWidth(9, 20 * 256);
        sheet.SetColumnWidth(10, 60 * 256);
        sheet.SetColumnWidth(11, 40 * 256);
        sheet.SetColumnWidth(12, 6 * 256);
        sheet.SetColumnWidth(13, 10 * 256);
        sheet.SetColumnWidth(14, 10 * 256);
        sheet.SetColumnWidth(15, 14 * 256);
        sheet.SetColumnWidth(16, 14 * 256);

        Row cols = sheet.CreateRow(0);
        cols.CreateCell(0).SetCellValue("商家收到退貨包裹");
        cols.CreateCell(1).SetCellValue("問題類別");
        cols.CreateCell(2).SetCellValue("問題敘述");
        cols.CreateCell(3).SetCellValue("超商驗退日");
        cols.CreateCell(4).SetCellValue("檔號");
        cols.CreateCell(5).SetCellValue("訂單編號");
        cols.CreateCell(6).SetCellValue("超商類型");
        cols.CreateCell(7).SetCellValue("取貨門市");
        cols.CreateCell(8).SetCellValue("超商物流編號");
        cols.CreateCell(9).SetCellValue("收件人");
        cols.CreateCell(10).SetCellValue("檔次(商品)名稱");
        cols.CreateCell(11).SetCellValue("規格");
        cols.CreateCell(12).SetCellValue("數量");
        cols.CreateCell(13).SetCellValue("進貨價");
        cols.CreateCell(14).SetCellValue("售價");
        cols.CreateCell(15).SetCellValue("訂購日");
        cols.CreateCell(16).SetCellValue("出貨完成日");

        int rowIdx = 1;
        foreach (var item in orders)
        {
            Row row = sheet.CreateRow(rowIdx);
            if (item.DcReturn != 0)
            {
                row.CreateCell(0).SetCellValue(item.DcReturn == 1 ? "未確認" : "確認");
                row.CreateCell(1).SetCellValue("刷退");
                row.CreateCell(2).SetCellValue(item.DcReturnReason);
                row.CreateCell(3).SetCellValue(item.DcReturnTime == null ? "" : item.DcReturnTime.Value.ToString("yyyy/MM/dd"));
            }
            else
            {
                if (item.DcAcceptStatus == (int)IspDcAcceptStatus.None)
                {
                    row.CreateCell(0).SetCellValue("請儘速出貨");
                }
                else if (item.DcAcceptStatus == (int)IspDcAcceptStatus.S03 || item.DcAcceptStatus == (int)IspDcAcceptStatus.N05)
                {
                    row.CreateCell(0).SetCellValue("");
                }
                else
                {
                    row.CreateCell(0).SetCellValue(item.DcReceiveFail == 1 ? "未確認" : "確認");
                }
                row.CreateCell(1).SetCellValue("驗退");
                row.CreateCell(2).SetCellValue(item.DcReceiveFailReason);
                row.CreateCell(3).SetCellValue(item.DcReceiveFailTime == null ? "" : item.DcReceiveFailTime.Value.ToString("yyyy/MM/dd"));
            }
            row.CreateCell(4).SetCellValue(item.UniqueId.ToString());
            row.CreateCell(5).SetCellValue(item.OrderId);
            if (item.ProductDeliveryType == (int)ProductDeliveryType.FamilyPickup)
            {
                row.CreateCell(6).SetCellValue("全家");
                row.CreateCell(7).SetCellValue(item.FamilyStoreName);
            }
            else if (item.ProductDeliveryType == (int)ProductDeliveryType.SevenPickup)
            {
                row.CreateCell(6).SetCellValue("7-11");
                row.CreateCell(7).SetCellValue(item.SevenStoreName);
            }
            row.CreateCell(8).SetCellValue(item.ShipNo);
            row.CreateCell(9).SetCellValue(item.MemberName);


            CashTrustLogCollection ctls = mp.CashTrustLogListGetByOrderId(item.OrderGuid);
            IViewPponDeal theDeal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(item.BusinessHourGuid);
            List<int> uniqueIds = new List<int>();
            uniqueIds.Add(item.UniqueId);
            var orderProductOptionLists = op.ViewOrderProductOptionListGetIsCurrentList(uniqueIds);
            var orderItemLists = op.GetOrderProductOptionList(orderProductOptionLists)
                                        .GroupBy(x => x.OrderGuid)
                                        .Select(x => new
                                        {
                                            x.Key,
                                            OptionDescription = string.Join("\n", x.Select(o => o.OptionDescription)),
                                            ItemNo = string.Join("\n", x.Select(o => o.ItemNo.Trim(','))),
                                            Quantity = string.Join("\n", x.Select(o => o.Quantity)),
                                            ItemCount = x.Sum(o => o.Quantity)
                                        })
                                        .ToDictionary(x => x.Key, x => x);
            var dealCost = pp.DealCostGetList(item.BusinessHourGuid);

            row.CreateCell(10).SetCellValue(theDeal.ItemName);
            row.CreateCell(11).SetCellValue((orderItemLists.ContainsKey(item.OrderGuid)) ? orderItemLists[item.OrderGuid].OptionDescription : string.Empty);
            var rowHeight = row.GetCell(11).StringCellValue.Split("\n");
            int defaultHeight = 16;
            row.HeightInPoints = rowHeight.Count() * defaultHeight;
            row.GetCell(11).CellStyle.WrapText = true;
            row.CreateCell(12).SetCellValue((orderItemLists.ContainsKey(item.OrderGuid)) ? orderItemLists[item.OrderGuid].Quantity : string.Empty);
            row.CreateCell(13).SetCellValue(dealCost.Count > 0 ? (dealCost[dealCost.Count() - 1].Cost ?? 0m).ToString() : "");
            row.CreateCell(14).SetCellValue(theDeal.ItemPrice.ToString());
            if (item.ShipTime == null)
            {
                row.CreateCell(15).SetCellValue(item.OrderCreateTime.ToString("yyyy/MM/dd"));
                row.CreateCell(16).SetCellValue("");
            }
            else
            {
                row.CreateCell(15).SetCellValue(item.OrderCreateTime.ToString("yyyy/MM/dd"));
                row.CreateCell(16).SetCellValue(item.ShipTime.Value.ToString("yyyy/MM/dd"));
            }
            rowIdx++;
        }



        Response.AddHeader("Content-Disposition", string.Format("attachment; filename={0}_超商刷驗退清冊.xls", DateTime.Now.ToString("yyyy-MM-dd")));
        workbook.Write(Response.OutputStream);
    }
}

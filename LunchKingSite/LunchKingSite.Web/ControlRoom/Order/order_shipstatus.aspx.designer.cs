﻿//------------------------------------------------------------------------------
// <自動產生的>
//     這段程式碼是由工具產生的。
//
//     對這個檔案所做的變更可能會造成錯誤的行為，而且如果重新產生程式碼，
//     所做的變更將會遺失。 
// </自動產生的>
//------------------------------------------------------------------------------



public partial class order_shipstatus {
    
    /// <summary>
    /// tbx_Bid 控制項。
    /// </summary>
    /// <remarks>
    /// 自動產生的欄位。
    /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
    /// </remarks>
    protected global::System.Web.UI.WebControls.TextBox tbx_Bid;
    
    /// <summary>
    /// btn_Search 控制項。
    /// </summary>
    /// <remarks>
    /// 自動產生的欄位。
    /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
    /// </remarks>
    protected global::System.Web.UI.WebControls.Button btn_Search;
    
    /// <summary>
    /// lbl_Message 控制項。
    /// </summary>
    /// <remarks>
    /// 自動產生的欄位。
    /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
    /// </remarks>
    protected global::System.Web.UI.WebControls.Label lbl_Message;
    
    /// <summary>
    /// pan_DealInfo 控制項。
    /// </summary>
    /// <remarks>
    /// 自動產生的欄位。
    /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
    /// </remarks>
    protected global::System.Web.UI.WebControls.Panel pan_DealInfo;
    
    /// <summary>
    /// lbl_SellerName 控制項。
    /// </summary>
    /// <remarks>
    /// 自動產生的欄位。
    /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
    /// </remarks>
    protected global::System.Web.UI.WebControls.Label lbl_SellerName;
    
    /// <summary>
    /// lbl_ItemName 控制項。
    /// </summary>
    /// <remarks>
    /// 自動產生的欄位。
    /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
    /// </remarks>
    protected global::System.Web.UI.WebControls.Label lbl_ItemName;
    
    /// <summary>
    /// lbl_Status 控制項。
    /// </summary>
    /// <remarks>
    /// 自動產生的欄位。
    /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
    /// </remarks>
    protected global::System.Web.UI.WebControls.Label lbl_Status;
    
    /// <summary>
    /// lbl_OrderTimeS 控制項。
    /// </summary>
    /// <remarks>
    /// 自動產生的欄位。
    /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
    /// </remarks>
    protected global::System.Web.UI.WebControls.Label lbl_OrderTimeS;
    
    /// <summary>
    /// lbl_OrderTimeE 控制項。
    /// </summary>
    /// <remarks>
    /// 自動產生的欄位。
    /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
    /// </remarks>
    protected global::System.Web.UI.WebControls.Label lbl_OrderTimeE;
    
    /// <summary>
    /// lbl_DeliverTimeS 控制項。
    /// </summary>
    /// <remarks>
    /// 自動產生的欄位。
    /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
    /// </remarks>
    protected global::System.Web.UI.WebControls.Label lbl_DeliverTimeS;
    
    /// <summary>
    /// lbl_DeliverTimeE 控制項。
    /// </summary>
    /// <remarks>
    /// 自動產生的欄位。
    /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
    /// </remarks>
    protected global::System.Web.UI.WebControls.Label lbl_DeliverTimeE;
    
    /// <summary>
    /// lbl_OrderMin 控制項。
    /// </summary>
    /// <remarks>
    /// 自動產生的欄位。
    /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
    /// </remarks>
    protected global::System.Web.UI.WebControls.Label lbl_OrderMin;
    
    /// <summary>
    /// lbl_OrderQuantity 控制項。
    /// </summary>
    /// <remarks>
    /// 自動產生的欄位。
    /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
    /// </remarks>
    protected global::System.Web.UI.WebControls.Label lbl_OrderQuantity;
    
    /// <summary>
    /// tbx_DefaultShipAgent 控制項。
    /// </summary>
    /// <remarks>
    /// 自動產生的欄位。
    /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
    /// </remarks>
    protected global::System.Web.UI.WebControls.TextBox tbx_DefaultShipAgent;
    
    /// <summary>
    /// tbx_DefaultShipDate 控制項。
    /// </summary>
    /// <remarks>
    /// 自動產生的欄位。
    /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
    /// </remarks>
    protected global::System.Web.UI.WebControls.TextBox tbx_DefaultShipDate;
    
    /// <summary>
    /// ce_DefaultShipDate 控制項。
    /// </summary>
    /// <remarks>
    /// 自動產生的欄位。
    /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
    /// </remarks>
    protected global::AjaxControlToolkit.CalendarExtender ce_DefaultShipDate;
    
    /// <summary>
    /// UpdatePanel1 控制項。
    /// </summary>
    /// <remarks>
    /// 自動產生的欄位。
    /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
    /// </remarks>
    protected global::System.Web.UI.UpdatePanel UpdatePanel1;
    
    /// <summary>
    /// hif_CurrentPage 控制項。
    /// </summary>
    /// <remarks>
    /// 自動產生的欄位。
    /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
    /// </remarks>
    protected global::System.Web.UI.WebControls.HiddenField hif_CurrentPage;
    
    /// <summary>
    /// hif_Bid 控制項。
    /// </summary>
    /// <remarks>
    /// 自動產生的欄位。
    /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
    /// </remarks>
    protected global::System.Web.UI.WebControls.HiddenField hif_Bid;
    
    /// <summary>
    /// hif_PageCount 控制項。
    /// </summary>
    /// <remarks>
    /// 自動產生的欄位。
    /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
    /// </remarks>
    protected global::System.Web.UI.WebControls.HiddenField hif_PageCount;
    
    /// <summary>
    /// hif_edit 控制項。
    /// </summary>
    /// <remarks>
    /// 自動產生的欄位。
    /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
    /// </remarks>
    protected global::System.Web.UI.WebControls.HiddenField hif_edit;
    
    /// <summary>
    /// gv_Orders 控制項。
    /// </summary>
    /// <remarks>
    /// 自動產生的欄位。
    /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
    /// </remarks>
    protected global::System.Web.UI.WebControls.GridView gv_Orders;
    
    /// <summary>
    /// gridPager 控制項。
    /// </summary>
    /// <remarks>
    /// 自動產生的欄位。
    /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
    /// </remarks>
    protected global::LunchKingSite.Web.ControlRoom.Controls.Pager gridPager;
}

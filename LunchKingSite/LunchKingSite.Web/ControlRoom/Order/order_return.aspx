﻿<%@ Page Language="C#" MasterPageFile="~/ControlRoom/backend.master" AutoEventWireup="true"
    CodeBehind="order_return.aspx.cs" Inherits="order_return" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="../Controls/Pager.ascx" TagName="Pager" TagPrefix="uc1" %>
<%@ Register Assembly="System.Web.Extensions"
    Namespace="System.Web.UI" TagPrefix="aspajax" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
    <script type="text/javascript" src="../../Tools/js/jquery-1.7.2.min.js"></script>
    <script type="text/javascript" src="../../Tools/js/jquery-ui.1.8.min.js"></script>
    <script type="text/javascript" src="../../Tools/ckeditor/jquery.query-2.1.7.js"></script>
    <script type="text/javascript" src="../../Tools/js/jquery.maxlength-min.js"></script>
    <script type="text/javascript" src="../../Tools/js/jquery.ui.datepicker-zh-TW.js"></script>
    <script type="text/javascript" src="../../Tools/js/jquery.blockUI.js"></script>
    <link type="text/css" href="../../Tools/js/css/ui-lightness/jquery-ui-1.8rc3.custom.css" rel="stylesheet" />
    <link href="../../Themes/PCweb/css/ppon.css" type="text/css" rel="Stylesheet" />
    <link href="../../Themes/PCweb/plugin/font-awesome/font-awesome.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../Tools/ckeditor/ckeditor.js"></script>
    <script type="text/javascript" src="../../Tools/js/json2.js"></script>
    <script type="text/javascript" src="../../Tools/js/jquery.numeric.js"></script>
    <script type="text/javascript" src="../../Tools/js/jquery.checkboxtree.js"></script>
    <script type="text/javascript" src="//html2canvas.hertzen.com/build/html2canvas.js"></script>
    <link type="text/css" href="../../Tools/js/jquery.checkboxtree.css" rel="stylesheet" />
    <style type="text/css">
        .searchTable {
            padding: 10px 10px 10px 10px;
            letter-spacing: 3px;
            line-height: 24px;
            width: 1400px;
            background-color: #EFEBEB;
        }
    </style>
    <script type="text/javascript">
        var b_editor;
        function setupdatepicker(from, to) {
            var dates = $('#' + from + ', #' + to).datepicker({
                changeMonth: true,
                numberOfMonths: 3,
                onSelect: function (selectedDate) {
                    var option = this.id == from ? "minDate" : "maxDate";
                    var instance = $(this).data("datepicker");
                    var date = $.datepicker.parseDate(instance.settings.dateFormat || $.datepicker._defaults.dateFormat, selectedDate, instance.settings);
                    dates.not(this).datepicker("option", option, date);
                }
            });
        }



        $(document).ready(function () {
            $('#tbSearch').focus();
            setupdatepicker('tbDS', 'tbDE');

        });

        function check() {
            if ($("#tbSearch").val() == "" && $("#tbDS").val() == "" && $("#tbDE").val() == "") {
                alert("請輸入查詢條件");
                return false;
            }
            else if ($("#tbSearch").val() == "" && $("#tbDS").val() == "" && $("#tbDE").val() != "") {
                alert("請輸入起始日期");
                return false;
            }
            else if ($("#tbSearch").val() == "" && $("#tbDS").val() != "" && $("#tbDE").val() == "") {
                alert("請輸入結束日期");
                return false;
            }
            else if ($("#tbDS").val() != "" && $("#tbDE").val() != "") {
                var sDate = new Date($("#tbDS").val());
                var eDate = new Date($("#tbDE").val());
                var dateDiff = sDate.dateDiff("d", eDate);
                if (dateDiff > 7) {
                    alert("日期區間不超過7天");
                    return false;
                }
                else {
                    return true;
                }
            }
            else {
                return true;
            }
        }

        Date.prototype.dateDiff = function (interval, objDate) {
            var dtEnd = new Date(objDate);
            if (isNaN(dtEnd)) return undefined;
            switch (interval) {
                case "s": return parseInt((dtEnd - this) / 1000);
                case "n": return parseInt((dtEnd - this) / 60000);
                case "h": return parseInt((dtEnd - this) / 3600000);
                case "d": return parseInt((dtEnd - this) / 86400000);
                case "w": return parseInt((dtEnd - this) / (86400000 * 7));
                case "m": return (dtEnd.getMonth() + 1) + ((dtEnd.getFullYear() - this.getFullYear()) * 12) - (this.getMonth() + 1);
                case "y": return dtEnd.getFullYear() - this.getFullYear();
            }
        }
        //-->
    </script>

    <font size="5"><b>超商刷退驗退查詢與維護</b></font>
    <br>
    <br>

    <table class="searchTable">
        <tr>
            <td>
                <font color="red">＊</font><asp:DropDownList runat="server" ID="ddlST" DataTextField="Value" DataValueField="Key" CssClass="adminitem">
                </asp:DropDownList>
                <asp:TextBox ID="tbSearch" runat="server" ClientIDMode="Static" />
                <font color="red">＊</font><asp:DropDownList runat="server" ID="ddlDate" CssClass="adminitem">
                    <asp:ListItem Text="訂購日" Value="1"></asp:ListItem>
                    <asp:ListItem Text="出貨確認日" Value="2"></asp:ListItem>
                    <asp:ListItem Text="刷退/驗退日" Value="3"></asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="tbDS" CssClass="date" runat="server" ClientIDMode="Static" MaxLength="10" size="10" placeholder="" />
                ～
                <asp:TextBox ID="tbDE" CssClass="date" runat="server" ClientIDMode="Static" MaxLength="10" size="10" placeholder="" />
                (日期選擇範圍最大一周)
            </td>
        </tr>
        <tr>
            <td>超商類型：
                <asp:DropDownList runat="server" ID="ddlType" ClientIDMode="Static" CssClass="adminitem">
                    <asp:ListItem Text="全部" Value=""></asp:ListItem>
                    <asp:ListItem Text="全家" Value="1"></asp:ListItem>
                    <asp:ListItem Text="7-11" Value="2"></asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
                <asp:Button ID="bSearchBtn" OnClick="bSearchBtn_Click" OnClientClick="return check();" runat="server" Text="送出"></asp:Button>
            </td>
        </tr>
    </table>
    <table width="1400px" >
        <tr>
            <td align="right"><asp:Button ID="ImportExcel" OnClick="ImportExcel_Click" runat="server" Text="匯出Excel"></asp:Button></td>
        </tr>
    </table>
    <asp:Panel ID="GridPanel" runat="server" Visible="false">
        <asp:GridView ID="gvOrder" runat="server" OnRowDataBound="gvOrder_RowDataBound"
            GridLines="Horizontal" ForeColor="Black" CellPadding="4" BorderWidth="1px" BorderStyle="None" AllowSorting="True"
            BorderColor="#DEDFDE" BackColor="White" EmptyDataText="無符合的資料" AutoGenerateColumns="false"
            Font-Size="Small" Width="1400px">
            <FooterStyle BackColor="#CCCC99"></FooterStyle>
            <RowStyle BackColor="#F7F7DE" Height="20px" HorizontalAlign="Center"></RowStyle>
            <SelectedRowStyle BackColor="#CE5D5A" ForeColor="White" Font-Bold="True"></SelectedRowStyle>
            <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Center"></PagerStyle>
            <HeaderStyle BackColor="#6B696B" ForeColor="White" Font-Bold="True" Height="20px"
                HorizontalAlign="Center"></HeaderStyle>
            <Columns>
                <asp:TemplateField HeaderText="商家確認" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="6%">
                    <ItemTemplate>
                        <asp:Label ID="lblStoreConfirm" runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="問題類型"  ItemStyle-HorizontalAlign="Center" ItemStyle-Width="8%">
                    <ItemTemplate>
                        <asp:Label ID="lblQuestionType" runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="超商驗退日" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="7%">
                    <ItemTemplate>
                        <asp:Label ID="lblCsvReturnDate" runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="檔號" sortExpression="OrderCreateTime" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5%">
                    <ItemTemplate>
                        <asp:Label ID="lblId" runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="訂單編號" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="7%">
                    <ItemTemplate>
                        <asp:HyperLink ID="lblOrderId" runat="server"></asp:HyperLink>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="超商類型<br>取貨門市" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="8%">
                    <ItemTemplate>
                        <asp:Label ID="lblStoreName" runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="超商物流編號" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="7%">
                    <ItemTemplate>
                        <asp:Label ID="lblShipmentNo" runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="收件人" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="10%">
                    <ItemTemplate>
                        <asp:Label ID="lblReceiver" runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="檔次(商品)名稱" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="14%">
                    <ItemTemplate>
                        <asp:Label ID="lblProduct" runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="規格" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="26%">
                    <ItemTemplate>
                        <asp:Label ID="lblSpec" runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="數量" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
                    <ItemTemplate>
                        <asp:Label ID="lblCount" runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="進貨價<br />售價" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5%">
                    <ItemTemplate>
                        <asp:Label ID="lblPrice" runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="訂購日<br />出貨完成日" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5%">
                    <ItemTemplate>
                        <asp:Label ID="lblOrderDate" runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
        <uc1:Pager ID="gridPager" runat="server" PageSize="15" ongetcount="RetrieveOrderCount" onupdate="UpdateHandler"></uc1:Pager>
    </asp:Panel>


</asp:Content>

﻿<%@ Page Language="C#" MasterPageFile="~/ControlRoom/backend.master" AutoEventWireup="true"
    CodeBehind="order_detail.aspx.cs" Inherits="order_detail" ValidateRequest="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Import Namespace="LunchKingSite.Core" %>
<%@ Import Namespace="LunchKingSite.DataOrm" %>
<%@ Register Src="../Controls/AuditBoard.ascx" TagName="AuditBoard" TagPrefix="uc2" %>
<%@ Register Src="../Controls/EinvoiceControl.ascx" TagName="EinvoiceControl" TagPrefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <script src="<%=ResolveClientUrl("~/Tools/js/jquery-ui.1.8.min.js") %>" type="text/javascript"></script>
    <script type="text/javascript" src="../../Tools/js/jquery.ui.datepicker-zh-TW.js"></script>
    <script type="text/javascript" src="../../Tools/js/homecook.js"></script>
    <script type="text/javascript" src="../../Tools/js/jquery.blockUI.js"></script>
    <link type="text/css" href="../../Tools/js/css/ui-lightness/jquery-ui-1.8rc3.custom.css" rel="stylesheet" />

    <style type="text/css">
        .information{
            width:400px;
            height:auto;
            border-spacing:0;
            border-collapse:collapse;
            word-break: break-all;
        }
        .information tbody {
            height: 300px;
            width: 400px;
            background-color:#F5F5F5;
        }
        .inforTop {
            width: 30%;
            background-color: #AAAAAA;
            padding: 10px;
        }
        .inforleft {
            width: 30%;
            background-color: #F5F5F5;
            padding: 10px;
        }

        .inforright {
            width: 70%;
            background-color: #F5F5F5;
            padding: 10px;
        }
        .wmsInforTop {
            width: 10%;
            background-color: #AAAAAA;
            padding: 10px;
        }
        .wmsInforleft {
            width: 40%;
            background-color: #F5F5F5;
            padding: 10px;
        }

        .wmsInforright {
            width: 60%;
            background-color: #F5F5F5;
            padding-left: 60px;
        }

    	.fuli {
			margin-bottom: 20px; border: 1px solid silver; display: flex; flex-direction: column;
			display:none;
			position:relative;
    	}
    	.fuli .fuli-body .fuli-row {
			display:flex;
    	}
		.fuli .fuli-body .fuli-row .fuli-chk {
			width:auto; padding:3px;
    	}
		.fuli .fuli-body .fuli-row .fuli-chk input {
			cursor:pointer;
    	}
		.fuli .fuli-body .fuli-row .fuli-code {
			width:auto; padding:3px;
    	}
		.fuli .fuli-body .fuli-row .fuli-status {
			width:auto; padding:3px;
    	}
    	.fuli button {
			position:absolute;
			right:0px;
			bottom:0px;
			display:none;
			cursor:pointer;
    	}
    </style>

    <script language="javascript" type="text/javascript">
        $().ready(function () {

            $('.datepick').datepicker({ dateFormat: 'yy-mm-dd', minDate: new Date('<%=ShipStartDate.ToString("yyyy/MM/dd") %>') });
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(function () {
                $('.datepick').datepicker({ dateFormat: 'yy-mm-dd', minDate: new Date('<%=ShipStartDate.ToString("yyyy/MM/dd") %>') });
            });

            
            //若為維護客服紀錄 則按下runat server按鈕 都需跳回客服紀錄頁面 故鎖定scroll top位置
            window.scrollTo(0, $('input[id*=HiddenUseServiceFlag]').val());
            $('#<%=tbShipMemo.ClientID%>').attr('maxlength', '30');
            //判斷分期是否開啟ATM
            if (parseInt($("#<%= hdInstallment.ClientID %>").val()) > 0) {
                openATM();
            }

            vendorPaymentFine();
            wmsRefund();
        });

        $('#<%=ddlAddShipCompanyId.ClientID%>').change(function () { //事件發生
            $('#<%=tbAddShipNo.ClientID%>').val("111");
        });

        function openATM() {
            var installment = parseInt($("#<%= hdInstallment.ClientID %>").val());
            var pnATM = $("#<%= pnATM.ClientID %>");
            var isPartialCancel = pnATM.is(':visible');
            var isReturnScash = $('#<%=chkCouponReturnSCash.ClientID%>').is(':visible') ? $('#<%=chkCouponReturnSCash.ClientID%>').is(':checked') : $('#<%=chkSCash.ClientID%>').is(':checked');
            var coupons = $("#<%= gvInStore.ClientID %> input[id*='cbCancel']").length;

            if (isPartialCancel) {
                pnATM.show();
            } else {
                pnATM.hide();
            }
            //預設ATM不顯示時才會觸發事件
            if (!isPartialCancel) {
                //憑證檢查分期退貨是否開啟ATM退款
                $("#<%= gvInStore.ClientID %> input[id*='cbCancel'],#<%=chkCouponReturnSCash.ClientID%>").click(function () {
                    if (installment > 0 && !$('#<%=chkCouponReturnSCash.ClientID%>').is(':checked') && (coupons != $("#<%= gvInStore.ClientID %> input[id*='cbCancel']:checked").length || isPartialCancel)) {
                        pnATM.show();
                    } else {
                        pnATM.hide();
                    }
                })
                //宅配檢查分期退貨是否開啟ATM退款
                $("#<%= OrderProductContainer.ClientID %> select[id*='returningCountSelections']").change(function () {
                    var totalCount = 0;
                    $("#<%= OrderProductContainer.ClientID %> input[id*='hdTotalCount']").each(function () {
                        totalCount += parseInt($(this).val(), 10);
                    })
                    var returnCount = 0;
                    $("#<%= OrderProductContainer.ClientID %> select[id*='returningCountSelections']").each(function () {
                        returnCount += parseInt($(this).attr('value'), 10);
                    })
                    if (installment > 0 && totalCount != returnCount && !$('#<%=chkSCash.ClientID%>').is(':checked')) {
                        pnATM.show();
                    } else {
                        pnATM.hide();
                    }
                })
                $("#<%= OrderProductContainer.ClientID %> select[id*='returningCountSelections']").trigger('change');
                $('#<%=chkSCash.ClientID%>').click(function () {
                    var totalCount = 0;
                    $("#<%= OrderProductContainer.ClientID %> input[id*='hdTotalCount']").each(function () {
                        totalCount += parseInt($(this).val(), 10);
                    })
                    var returnCount = 0;
                    $("#<%= OrderProductContainer.ClientID %> select[id*='returningCountSelections']").each(function () {
                        returnCount += parseInt($(this).attr('value'), 10);
                    })
                    if (installment > 0 && totalCount != returnCount && !$('#<%=chkSCash.ClientID%>').is(':checked')) {
                        pnATM.show();
                    } else {
                        pnATM.hide();
                    }
                })
            }
        }

        var timer;
        function pad(s, l) {
            var data = String(s);
            var result = data;
            for (i = data.length; i < l; i++) { result = '0' + result; }
            return result;
        }

        function appResend() {
            var a = window.prompt("請輸入電子郵件", $('#<%=hif_SelectMemberEmail.ClientID%>').val());
            if (a != "" && a != null) {
                $('#<%=hif_SelectMemberEmail.ClientID%>').val(a);
            }
        }

        function countdown(sec, hrstr, mnstr, sstr) {
            second = sec - 1; hourstr = hrstr; minstr = mnstr; secstr = sstr;
            var el = $get('countdowntext');

            dhour = Math.floor(sec / 3600);
            dmin = Math.floor(sec / 60);
            dsec = Math.floor(sec % 60);

            el.innerHTML = '';
            if (dsec < 0)
                dsec = 0;
            el.innerHTML += pad(dsec, 2) + ' ' + sstr;
            if (dmin > 0 || dhour > 0)
                el.innerHTML = pad(dmin, 2) + ' ' + mnstr + ' ' + el.innerHTML;
            if (dhour > 0)
                el.innerHTML = pad(dhour, 2) + ' ' + hrstr + ' ' + el.innerHTML;
            if (dhour >= 0 || dmin >= 0 || dsec >= 0) {
                timer = setTimeout("countdown(second, hourstr, minstr, secstr)", 1000)
            }
            return;
        }

        function CheckData() {
            var Category_value = $('#<%=ddlCategory.ClientID%>').val();
            var SubCategory_value = $('#<%=ddlSubCategory.ClientID%>').val();
            var rdlWorkType_value = $("input:radio[name='<%=rdlWorkType.ClientID.Replace("_","$")%>']:checked").val();


            if (Category_value == 0) {
                alert('請選擇客服紀錄的分類!');
                return false;
            }
            if (SubCategory_value == 0) {
                alert('請選擇客服紀錄的子分類!');
                return false;
            }
            if (rdlWorkType_value == undefined) {
                alert('嘿！尚未點選轉件項目!');
                return false;
            }
            if ($('textarea[id*=txtIMessage]').val() == '') {
                alert('請輸入客服紀錄的問題說明!');
                return false;
            }



            btnSetServiceMsg_Click();
            return true;
        }

        function bAddServiceLog_Click() {
            //按下[新增客服紀錄]按鈕 須保持視窗停留於客服紀錄 紀錄目前scrolltop位置
            $('input[id*=HiddenUseServiceFlag]').val(document.body.scrollTop);
        }

        function btnSetServiceMsg_Click() {
            //按下客服紀錄維護畫面[確認]按鈕 須保持視窗停留於客服紀錄 紀錄目前scrolltop位置
            $('input[id*=HiddenUseServiceFlag]').val(document.body.scrollTop);
        }

        function btnAddServiceMsgClose_Click() {
            //按下客服紀錄維護畫面[退出]按鈕 毋須保持視窗停留於客服紀錄 0:scrolltop位置
            $('input[id*=HiddenUseServiceFlag]').val(0);
        }

        function History(orderGuid) {
            $.ajax({
                type: "POST",
                url: "order_detail.aspx/GetHistoryList",
                data: "{guidstring: '" + orderGuid + "'}",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    var cates = $.parseJSON(data.d);
                    html = "<table class='information'>";
                    html = html + "<tr><td class='infortop'>日期</td><td class='infortop'>狀態<a href='javascript:void(0)' onclick='ClosePop()'><img src='/images/close.gif' align='right'></a></td></tr>";
                    $.each(cates, function (i, item) {
                        html = html + "<tr><td class='inforleft'>" + GetFormattedDate(item.deliveryTime) + "</td><td class='inforright'>" + item.delvieryDesc + "</td></tr>";
                    })
                    html = html + "</table>";
                    $.blockUI({ message: html, css: { width: '400px', cursor: 'default' }, overlaycss: { cursor: 'default' }, onOverlayClick: $.unblockUI });

                },
                error: function () {
                }
            });
        }

        
        function WmsHistory(orderGuid,shipNo) {
            $.ajax({
                type: "POST",
                url: "order_detail.aspx/GetWmsHistoryList",
                data: "{guidstring: '" + orderGuid + "',shipNo:'"+shipNo+"'}",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                cache: true,
                success: function (data) {
                    var cates = $.parseJSON(data.d);
                    html = "<table class='information'>";
                    html = html + "<thead style='display: block;'><tr><td class='wmsInfortop'>日期</td><td class='wmsInfortop'>狀態<a href='javascript:void(0)' onclick='ClosePop()'><img src='/images/close.gif' align='right'></a></tr></thead><tbody style='overflow-y: scroll; overflow-x: hidden; display: block;'>";
                    $.each(cates, function (i, item) {
                        html = html + "<tr><td class='wmsInforleft'>" + GetFormattedDateTime(item.deliveryTime) + "</td><td class='wmsInforright'>" + item.delvieryDesc + "</td></tr>";
                    })
                    html = html + "</tbody></table>";
                    $.blockUI({ message: html, css: { width: '400px', cursor: 'default' }, overlaycss: { cursor: 'default' }, onOverlayClick: $.unblockUI });

                },
                error: function () {
                }
            });
        }


        function ClosePop() {
            $.unblockUI();
        }

        function GetFormattedDate(date) {
            var todayTime = new Date(date);
            return todayTime.getFullYear() + "-" + (todayTime.getMonth() + 1) + "-" + todayTime.getDate();
        }

        function GetFormattedDateTime(date) {
            var dayTime = new Date(date);
            return dayTime.getFullYear() + "-" + (dayTime.getMonth() + 1) + "-" + dayTime.getDate() + " " 
                + (dayTime.getHours()<10?'0':'') + dayTime.getHours() + "：" + (dayTime.getMinutes()<10?'0':'') + dayTime.getMinutes();
        }


        function buildSubCategory() {
            var ddlMain = $("#<%=ddlCategory.ClientID%>");
            var ddlMainSelected = $("#<%=ddlCategory.ClientID%> :selected");
            var ddlSub = $("#<%=ddlSubCategory.ClientID%>");

            $.ajax({
                type: "POST",
                url: "order_detail.aspx/GetServiceMessageCategory",
                data: "{parentId: " + ddlMain.val() + "}",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    var cates = $.parseJSON(data.d);
                    ddlSub.empty();
                    ddlSub.append($('<option></option>').val('0').html('請選擇'));
                    $.each(cates, function (i, item) {
                        ddlSub.append($('<option></option>').val(item.CategoryId).html(item.CategoryName));
                    })
                },
                error: function () {
                    ddlSub.empty();
                    ddlSub.append($('<option></option>').val('0').html('請選擇'));
                }
            });
            if (ddlMainSelected.text() == "會員(帳號)問題" || ddlMainSelected.text() == "其他問題" || ddlMainSelected.text() == "請選擇") {
                $('.OrderText').css('display', 'none');
            } else {
                $('.OrderText').css('display', '');
            }
        }
        //在Save客服紀錄的分類時，無法抓取到子分類下拉式選單的值，所以存在一個hidden去寫資料.
        function setSubCategory() {
            var ddlSub = $("#<%=ddlSubCategory.ClientID%> :selected");
            var hdSub = $("#<%=hdSubCategory.ClientID%>");
            hdSub.val(ddlSub.val());
        }




        function checkTriplicate() {
            if ($('#ddlEinvoiceMode').val() == '1' && $('#ddlEinvoiceMode').is(':visible')) {
                return confirm('一旦改為三聯即無法再恢復二聯,是否更新?');
            }
            //空值代表註銷重開
            if ($('#hfCarrierInfo').val() != '' && $('#hfCarrierInfo').val() != $('#ddlCarrierType').val() + '|' + $('#txtCarrierId').val()) {
                return confirm('變更載具系統會先註銷發票並在2天後重新上傳,是否註銷?');
            }
            if ($('#hfComInfo').val() != '' && $('#hfComInfo').val() != $('#txtInvoiceCompanyId').val() + '|' + $('#txtInvoiceTitle').val()) {
                return confirm('變更發票抬頭會先作廢原發票並重新開立,是否更新?');
            }
            return true;
        }

        function showInvLog() {
            block_ui('InvLog');
        }

        function showFreezeDialog() {
            $("#<%=txtFreezeType.ClientID%>").val("1");
            $('#PopFreezeReason').show();
            return false;
        }
        function showCloseFreezeDialog() {
            $("#<%=txtFreezeType.ClientID%>").val("2");
            $('#PopFreezeReason').show();
            return false;
        }
        function hideFreezeReason() {
            $('#PopFreezeReason').hide();
            return false;
        }
        function CheckFreezeReason() {
            var FreezeReason = $('#<%=txtFreezeReason.ClientID%>').val();

            if ($.trim(FreezeReason) == "") {
                alert('若無輸入原因則無法執行凍結請款功能');
                return false;
            }
        }

        function openExchangeMessage(id) {
            window.open("ExchangeMessage?id=" + id, "_blank", "toobar=no, location=no, directories=no, titlebar=no, toolbar=no, scrollbars=no, resizable=no, menubar=no, winth=100, height=300");
        }

        function openOrderShip(id) {
            window.open("OrderShipInfo?id=" + id, "_blank", "toobar=no, location=no, directories=no, titlebar=no,, status=no, toolbar=no, scrollbars=no, resizable=no, menubar=no, winth=100, height=400");
        }

        function block_ui(obj) {
            obj = '#' + obj;
            var target = $(obj);
            unblock_ui();
            $.blockUI({ message: $(target), css: { backgroundcolor: '#F7F7F7', border: '10px solid #fff', top: ($(window).height()) / 8 + 'px', left: ($(window).width() - 700) / 2 + 'px', width: '700px' },bindEvents : false })

            if (obj = 'divFine')
            {
                var dtStart = new Date();
                var dtExpire = new Date();
                dtExpire.setMonth(dtExpire.getMonth() + 3);
                $('.blockUI #divFine [name=txtBonusStartTime]').val(dtStart.getFullYear() + '-' + (dtStart.getMonth() + 1) + '-' + dtStart.getDate());
                $('.blockUI #divFine [name=txtBonusExpireTime]').val(dtExpire.getFullYear() + '-' + (dtExpire.getMonth() + 1) + '-' + dtExpire.getDate());
                $('.blockUI #divFine #<%=rdCompensateYes.ClientID%>').prop('checked', true);

                
            }
            
        }

        function unblock_ui() {
            $.unblockUI();
            return false;
        }

        function block_postback() {
            $.blockUI({ message: "<center><font color='blue'>處理中，請稍候...</font></center>", css: { width: '250px' } });
        }

        function vendorPaymentFine()
        {
            $(document).on('click', '#<%=btnFine.ClientID%>', function (event) {
                $.ajax({
                        type: "POST",
                        url: "order_detail.aspx/IsInBalanceTimeRange",
                        data: JSON.stringify({
                            bid: "<%=BusinessHourGuid%>"
                        }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        async: true,
                        success: function (msg) {
                            var result = $.parseJSON(msg.d);
                            if (result.Success) {
                                block_ui('divFine');
                            }
                            else {
                                alert(result.Message);
                            }
                        },
                        error: function (msg) {
                            var result = $.parseJSON(msg.d);
                            alert(result.Message);
                        }
                    });
            })

            $(document).on('click', '#<%=btnUpdateNextBalanceSheet.ClientID%>', function (event) {

                if (isNaN($('.blockUI #divFine #<%=txtAmount.ClientID%>').val()) || $('.blockUI #divFine #<%=txtAmount.ClientID%>').val()=='' || $('.blockUI #divFine #<%=txtAmount.ClientID%>').val()=='0')
                    alert('罰款金額格式錯誤');
                else if ($('.blockUI #divFine #<%=rdCompensateYes.ClientID%>').is(':checked') && (isNaN($('.blockUI #divFine #<%=txtPromotionValue.ClientID%>').val()) || $('.blockUI #divFine #<%=txtPromotionValue.ClientID%>').val()=='' || $('.blockUI #divFine #<%=txtPromotionValue.ClientID%>').val()=='0'))
                    alert('紅利金格式錯誤');
                else if ($('.blockUI #divFine #<%=rdCompensateYes.ClientID%>').is(':checked') && (!isValidDate($('.blockUI #divFine [name=txtBonusStartTime]').val()) || !isValidDate($('.blockUI #divFine [name=txtBonusExpireTime]').val())))
                    alert('紅利金生效期限格式錯誤');
                else
                {
                    
                    $.ajax({
                        type: "POST",
                        url: "order_detail.aspx/UpdateNextBalanceSheet",
                        data: JSON.stringify({
                            compensate: $('.blockUI #divFine #<%=rdCompensateYes.ClientID%>').is(':checked'),
                            strBid: "<%=BusinessHourGuid%>",
                            strOrderGuid: "<%=OrderId%>",
                            strAmount: $('.blockUI #divFine #<%=txtAmount.ClientID%>').val(),
                            reason: $('.blockUI #divFine #<%=ddlReason.ClientID%> option:selected').text() + '(' + $('.blockUI #divFine #<%=txtReason.ClientID%>').val() + ')',
                            fineType: $('.blockUI #divFine #<%=ddlReason.ClientID%> option:selected').val(),
                            strPromotionValue: $('.blockUI #divFine #<%=txtPromotionValue.ClientID%>').val(),
                            strBonusStartTime: $('.blockUI #divFine [name=txtBonusStartTime]').val(),
                            strBonusExpireTime: $('.blockUI #divFine [name=txtBonusExpireTime]').val(),
                            bonusAction: $('.blockUI #divFine #<%=txtBonusAction.ClientID%>').val(),
					        balanceSheetId: null,
					        userName: "<%=CurrentUser%>"
                        }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        async: true,
                        success: function (msg) {
                            var result = $.parseJSON(msg.d);
                            if (result.Success) {
                                alert(result.Message);
                                unblock_ui();
                            }
                            else {
                                alert(result.Message);
                            }
                        },
                        error: function (msg) {
                            var result = $.parseJSON(msg.d);
                            alert(result.Message);
                        }
                    });
                }

                
				
            });


            $(document).on('click', '#<%=btnUpdateNowBalanceSheet.ClientID%>', function (event) {

                if (isNaN($('.blockUI #divFine #<%=txtAmount.ClientID%>').val()) || $('.blockUI #divFine #<%=txtAmount.ClientID%>').val()=='')
                    alert('罰款金額格式錯誤');
                else if ($('.blockUI #divFine #<%=rdCompensateYes.ClientID%>').is(':checked') && (isNaN($('.blockUI #divFine #<%=txtPromotionValue.ClientID%>').val()) || $('.blockUI #divFine #<%=txtPromotionValue.ClientID%>').val()==''))
                    alert('紅利金格式錯誤');
                else if ($('.blockUI #divFine #<%=rdCompensateYes.ClientID%>').is(':checked') && (!isValidDate($('.blockUI #divFine [name=txtBonusStartTime]').val()) || !isValidDate($('.blockUI #divFine [name=txtBonusExpireTime]').val())))
                    alert('紅利金生效期限格式錯誤');
                else
                {
                    $('#spanAmount').text($('.blockUI #divFine #<%=txtAmount.ClientID%>').val());
                    $('#spanPromotionValue').text($('.blockUI #divFine #<%=txtPromotionValue.ClientID%>').val());
                    $('#spanReason').text($('.blockUI #divFine #<%=ddlReason.ClientID%> option:selected').text() + '(' + $('.blockUI #divFine #<%=txtReason.ClientID%>').val() + ')');
                    unblock_ui();
                    block_ui('divFineNow')
                }
                

            });

            $(document).on('click', '#<%=btnNowCancel.ClientID%>', function (event) {
                unblock_ui();
            });

            $(document).on('click', '#<%=btnNowYes.ClientID%>', function (event) {

                $.ajax({
					type: "POST",
					url: "order_detail.aspx/UpdateNextBalanceSheet",
					data: JSON.stringify({
					    compensate :$('.blockUI #divFine #<%=rdCompensateYes.ClientID%>').is(':checked'),
					    strBid: "<%=BusinessHourGuid%>",
					    strOrderGuid: "<%=OrderId%>",
					    strAmount: $('.blockUI #divFine #<%=txtAmount.ClientID%>').val(),
					    reason: $('.blockUI #divFine #<%=ddlReason.ClientID%> option:selected').text() + '(' + $('.blockUI #divFine #<%=txtReason.ClientID%>').val() + ')',
					    strPromotionValue: $('.blockUI #divFine #<%=txtPromotionValue.ClientID%>').val(),
                        fineType:$('.blockUI #divFine #<%=ddlReason.ClientID%> option:selected').val(),
					    strBonusStartTime: $('.blockUI #divFine [name=txtBonusStartTime]').val(),
					    strBonusExpireTime: $('.blockUI #divFine [name=txtBonusExpireTime]').val(),
					    bonusAction: $('.blockUI #divFine #<%=txtBonusAction.ClientID%>').val(),
					    balanceSheetId: $('.blockUI #divFineNow #<%=ddlChangeBalanceSheet.ClientID%> option:selected').val(),
                        userName : "<%=CurrentUser%>"
					}),
					contentType: "application/json; charset=utf-8",
					dataType: "json",
					async: true,
					success: function (msg) {
					    var result = $.parseJSON(msg.d);
					    if (result.Success)
					    {
					        alert(result.Message);
					        unblock_ui();
					    }
					    else
					    {
					        alert(result.Message);
					    }
					},
					error: function (msg) {
					    var result = $.parseJSON(msg.d);
					    alert(result.Message);
					}
                });
            });

         
            $(document).on('change', '[name*=IsCompensateConsumer]', function (event) {
                if ($('.blockUI #divFine #<%=rdCompensateYes.ClientID%>').is(':checked'))
                {
                    $('.blockUI #divFine #<%=txtPromotionValue.ClientID%>').prop('disabled', false);
                    $('.blockUI #divFine #tbBonus').show();
                }
                else if ($('.blockUI #divFine #<%=rdCompensateNo.ClientID%>').is(':checked')) {
                    $('.blockUI #divFine #<%=txtPromotionValue.ClientID%>').prop('disabled', true);
                    $('.blockUI #divFine #<%=txtPromotionValue.ClientID%>').val('');
                    $('.blockUI #divFine #tbBonus').hide();
                }
            })


            $(document).on('change', '#<%=txtAmount.ClientID%>', function (event) {
                if ($(this).val() > 0)
                {
                    $('.blockUI #divFine #tbBonus').hide();
                    $('.blockUI #divFine #<%=rdCompensateNo.ClientID%>').prop('checked', true);
                    $('.blockUI #divFine #<%=rdCompensateYes.ClientID%>').prop('disabled', true);
                }
                else
                {
                    $('.blockUI #divFine #tbBonus').show();
                    $('.blockUI #divFine #<%=rdCompensateYes.ClientID%>').prop('disabled', false);
                }
                $("[name*=IsCompensateConsumer]").trigger('change');
            })
        }

        function wmsRefund()
        {
            $(document).on('click', '#' + $('[id=btnWmsRefund]').attr('id'), function (event) {
                var returnFormId = $(this).data('id');
                $('#hidReturnFormId').val(returnFormId);
                block_ui('divWmsRefund');
            })

            $(document).on('click', '#<%=btnWmsRefundSubmit.ClientID%>', function (event) {

                
                var isCheck = true;
                if ($('input[name=wmsRefundFrom]:checked').val() == undefined)
                {
                    alert('請選擇項目');
                    isCheck = false;
                }
                else if ($('input[name=wmsRefundFrom]:checked').val() == '<%= (int)WmsRefundFrom.Pchome %>')
                {
                    if ($('#ContactName').val() == '' || $('#ContactTel').val() == '' || $('#ContactEmail').val() == '' || $('#ContactAddress').val() == '')
                    {
                        alert('請完整輸入聯絡資訊');
                        isCheck = false;
                    }
                }
                if (isCheck)
                {
                    $.ajax({
                        type: "POST",
                        url: "order_detail.aspx/WmsRefund",
                        data: JSON.stringify({
                            returnFormId: $('#hidReturnFormId').val(),
                            refundFrom: $('input[name=wmsRefundFrom]:checked').val(),
                            orderGuid: "<%=OrderId%>",
                            contactName: $('input[name=wmsRefundFrom]:checked').val() == '<%= (int)WmsRefundFrom.Pchome %>' ? $('#ContactName').val() : "",
                            contactTel: $('input[name=wmsRefundFrom]:checked').val() == '<%= (int)WmsRefundFrom.Pchome %>' ? $('#ContactTel').val() : "",
                            contactMobile: $('input[name=wmsRefundFrom]:checked').val() == '<%= (int)WmsRefundFrom.Pchome %>' ? $('#ContactMobile').val() : "",
                            contactEmail: $('input[name=wmsRefundFrom]:checked').val() == '<%= (int)WmsRefundFrom.Pchome %>' ? $('#ContactEmail').val() : "",
                            contactAddress: $('input[name=wmsRefundFrom]:checked').val() == '<%= (int)WmsRefundFrom.Pchome %>' ? $('#ContactAddress').val() : "",
                            userName : "<%=CurrentUser%>"
                        }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        async: true,
                        success: function (msg) {
                            var result = $.parseJSON(msg.d);
                            if (result.Success) {

                                //立即更新畫面資訊
                                var btnWmsRefund = $('[id=btnWmsRefund][data-id=' + $('#hidReturnFormId').val() + ']');
                               
                                $(btnWmsRefund).parent().prev().text('已前往收件');
                                if (result.IsReceive)
                                {
                                    $(btnWmsRefund).parent().text($(btnWmsRefund).parent().text().replace('客服待處理', '').replace('(已前往消費者收件)', '').replace('(已前往Pchome收件)', '') + '前往消費者處收件');
                                }
                                else
                                {
                                    $(btnWmsRefund).parent().text($(btnWmsRefund).parent().text().replace('客服待處理', '').replace('(已前往消費者收件)', '').replace('(已前往Pchome收件)', '') + '前往PC倉庫收件');
                                }
                                
                                alert(result.Message);
                                unblock_ui();
                            }
                            else {
                                alert(result.Message);
                            }
                        },
                        error: function (msg) {
                            var result = $.parseJSON(msg.d);
                            alert(result.Message);
                        }
                    });
                }
            })
        }

        function isValidDate(dateStr) {

            if (dateStr == null || dateStr == '') return false;
            var parts = dateStr.split('-');
            if (parts.length != 3) return false;
            if (parts[0] > 9999 || parts[0] < 1753)
                return false;
            var accDate = new Date(dateStr);
            if (parseFloat(parts[0]) == accDate.getFullYear()
                && parseFloat(parts[1]) == accDate.getMonth() + 1
                && parseFloat(parts[2]) == accDate.getDate()) {
                return true;
            }
            else {
                return false;
            }
        }

        function checkReceiver()
        {
            <%if (config.EnabledRefundOrExchangeEditReceiverInfo && !IsCoupon)
        {%>
            $('#ReceiverNameErr').hide();
            $('#ReceiverAddressErr').hide();
            if ($('.txtReceiverName').val() == "" || $('.txtReceiverAddress').val() == "") {
                if ($('.txtReceiverName').val() == "") {
                    $('#ReceiverNameErr').show();
                    $('.txtReceiverName').focus();
                }
                if ($('.txtReceiverAddress').val() == "") {
                    $('#ReceiverAddressErr').show();
                    $('.txtReceiverAddress').focus();
                }
                return false;
            }
            else
            {
                return true;
            }
            <%}
            else { %>
            return true;
            <% }%>
        }

		window.couponData = JSON.parse('<%=this.CouponDataJson%>');
		$(document).ready(function () {
			if (window.couponData.SellerName == "必勝客 Pizza Hut" || window.couponData.SellerName == "肯德基 KFC") {
				$('#fuli').show();
				$('#fuli .fuli-body').empty();
				for (let i in window.couponData.CouponCodes) {
					let couponCode = window.couponData.CouponCodes[i];
					let row = $("<div class='fuli-row'>").appendTo($('.fuli-body'));					
					row.append("<div class='fuli-chk'><input type='checkbox' disabled /></div>");
					row.append("<div class='fuli-code'>" + couponCode + "</div>");
					row.append("<div class='fuli-status'>" + "查詢中" + "</div>");
				}
				let loadFuliData = function (codes, cancel) {
					axios.post("order_detail.aspx/GetFuliCodeResult", { codes: codes, cancel: cancel }).then(function (response) {
						let items = JSON.parse(response.data.d).items;
						for (let i in items) {
							let item = items[i];
							$(".fuli-code:contains('" + item.code + "')").siblings('.fuli-status').text(item.status);
							var chk = $(".fuli-code:contains('" + item.code + "')").siblings('.fuli-chk')
								.find(':checkbox');
							chk.removeAttr('checked');
							if (item.available) {
								chk.removeAttr('disabled').val(item.code);
							} else {
								chk.attr('disabled');
							}
						}
					}).catch(function (error) {
						console.log('error: ' + error);
						$('.fuli-status').text('查詢錯誤');
					});
				}
				loadFuliData(window.couponData.CouponCodes, false);


				$('#fuli').on('change', ':checkbox', function () {
					if ($('#fuli').find(':checkbox:checked').length > 0) {
						$('#fuli').find('button').show();
					} else {
						$('#fuli').find('button').hide();
					}
				});
				$('#fuli').find('button').click(function () {
					if (window.confirm('確定要使用富利系統，註銷憑證嗎?') == false) {
						return;
					}
					let cancelCodes = [];
					$('#fuli').find(':checkbox:checked').each(function () {
						cancelCodes.push($(this).val());
					});
					console.log('fuli button clicked');
					window.cancelCodes = cancelCodes;
					loadFuliData(cancelCodes, true);
				});
			}
		});

    </script>
    <style type="text/css">
        input.date {
            width: 80px;
        }

        input.readonly {
            border: 0px;
            background-color: transparent;
        }

        .ui-datepicker {
            font-size: 10px;
        }

        .style1 {
            width: 283px;
        }

        table.tbl-block {
            border-style: solid;
            border-width: thin;
            border-collapse: collapse;
            border-color: black;
            min-width: 800px;
            margin: 4px;
        }

            table.tbl-block > thead th {
                border-style: solid;
                border-width: thin;
                border-collapse: collapse;
                background-color: #cccccc;
            }

            table.tbl-block > tbody table {
                border-collapse: collapse;
                margin: 2px;
                width: 98%;
            }

                table.tbl-block > tbody th, table.tbl-block > tbody table tbody td {
                    border-style: solid;
                    border-width: thin;
                    border-collapse: collapse;
                    border-color: #cccccc;
                }

        .tbOrderUserMemo th, .tbOrderUserMemo > tbody > tr > td {
            border: 1px solid grey;
        }

        .functionButton {
            clear: both;
            display: block;
            margin-left: auto;
            margin-right: auto;
        }

        .modalPopup {
            background-color: #ffffdd;
            border-width: 3px;
            border-style: solid;
            border-color: Gray;
            position: fixed;
            top: 50%;
            left: 50%;
            width: 30em;
            height: 18em;
            margin-top: -9em; /*set to a negative number 1/2 of your height*/
            margin-left: -15em; /*set to a negative number 1/2 of your width*/
        }

        #divBankInfo li.fr {
            border-bottom: none;
        }

        ol, ul {
            list-style: none;
            padding: 0px;
        }

        .suggestionBox {
            position: absolute;
            top: 23px;
            left: 80px;
            width: 173px;
            background-color: #fff;
            border: 1px #545454;
            color: #666;
            overflow: hidden;
            z-index: 99;
            text-align: left;
            -moz-box-shadow: 2px 2px 3px #545454;
            -webkit-box-shadow: 2px 2px 3px #545454;
            box-shadow: 2px 2px 3px #545454;
            /* For IE 8 */
            -ms-filter: "progid:DXImageTransform.Microsoft.Shadow(Strength=3, Direction=135, Color='#545454')";
            /* For IE 5.5 - 7 */
            filter: progid:DXImageTransform.Microsoft.Shadow(Strength=3, Direction=135, Color='#545454');
        }

        .suggestionBox2 {
            position: absolute;
            top: 43px;
            left: 64px;
            width: 173px;
            background-color: #fff;
            border: 1px #545454;
            color: #666;
            overflow: hidden;
            z-index: 99;
            text-align: left;
            -moz-box-shadow: 2px 2px 3px #545454;
            -webkit-box-shadow: 2px 2px 3px #545454;
            box-shadow: 2px 2px 3px #545454;
            /* For IE 8 */
            -ms-filter: "progid:DXImageTransform.Microsoft.Shadow(Strength=3, Direction=135, Color='#545454')";
            /* For IE 5.5 - 7 */
            filter: progid:DXImageTransform.Microsoft.Shadow(Strength=3, Direction=135, Color='#545454');
        }

        .suggestionList, .suggestionList2 {
            margin: 0px;
            padding: 0px 0px 0px 0px;
        }

            .suggestionList li, .suggestionList2 li {
                margin: 0px 0px 3px 0px;
                padding: 7px 0px 7px 7px;
                cursor: pointer;
                width: 100%;
                font-size: 10pt;
            }

                .suggestionList li:hover, .suggestionList .active {
                    background-color: #e0e0e0; /* #e0e0e0 #bf0000 */
                    /*color: #FFFFFF;*/
                }

                .suggestionList2 li:hover, .suggestionList2 .active {
                    background-color: #e0e0e0; /* #e0e0e0 #bf0000 */
                    /*color: #FFFFFF;*/
                }

        .SelectedItem {
            background-color: lightgreen;
        }
        .UnSelectedItem {
            background-color:lightgrey;
        }

        .ItemTitles {
            background-color: gray;
            color: white;
            font-weight:700;
        }

        .divScrollbar {
            scrollbar-DarkShadow-Color:#9FB7D7;   
            scrollbar-Track-Color:#F7F7F7;       
            scrollbar-Face-Color:#C7CFFF;        
            scrollbar-Shadow-Color:#FFFFFF;        
            scrollbar-Highlight-Color:#FFFFFF;   
            scrollbar-3dLight-Color:#C7CFF7;     
            scrollbar-Arrow-Color:#4F5F87;
            overflow-y:auto;   
            overflow-x:auto;  
            HEIGHT:580px; 
            margin-top:15px; 
            margin-left:0px;
            Color:#000000;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
    <asp:checkbox id="cbx_IsCoupon" runat="server" visible="false" />
    <asp:UpdatePanel ID="upM" runat="server">
        <Triggers>        
            <asp:PostBackTrigger ControlID="btnZipCode" />
        </Triggers>
        <ContentTemplate>
            <asp:HiddenField ID="hidChannelAgent" ClientIDMode="Static" Value="0" runat="server" />
            
            <%---------------------------------- 資策會雙北APP訂單----------------------------------%>
            <asp:PlaceHolder ID="pIDEAS" runat="server" Visible="false">
                <table width="800" style="border: double 3px black;">
                    <tr>
                        <th colspan="4" style="background-color: red">資策會雙北APP訂單
                        </th>
                    </tr>
                    <tr>
                        <td class="lbl" style="width: 100px">購買人姓名
                        </td>
                        <td style="width: 300px">
                            <asp:Label ID="IDEASPurchaserName" runat="server"></asp:Label>
                        </td>

                        <td class="lbl" style="width: 100px">購買人電話
                        </td>
                        <td style="width: 300px">
                            <asp:Label ID="IDEASPurchaserPhone" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="lbl">購買人EMail
                        </td>
                        <td>
                            <asp:Label ID="IDEASPurchaserEmail" runat="server"></asp:Label>
                        </td>
                        <td class="lbl">購買人地址
                        </td>
                        <td>
                            <asp:Label ID="IDEASPurchaserAddress" runat="server"></asp:Label>
                        </td>
                    </tr>
                </table>
            </asp:PlaceHolder>
            <%---------------------------------- end of 資策會雙北APP訂單----------------------------------%>

            <asp:Panel ID="p1" runat="server">
                <asp:Panel ID="pan_Tmall" runat="server">
                    <table width="800px">
                        <tr>
                            <td colspan="4" style="text-align: center; background-color: orange; font-weight: bolder;">
                                <asp:Label ID="lab_TmallType" runat="server"></asp:Label>訂單資料</td>
                        </tr>
                        <tr style="background-color: white;">
                            <td style="text-align: right;">對應訂單編號 : </td>
                            <td style="text-align: left;">
                                <asp:Label ID="lab_TnallOrder" runat="server"></asp:Label></td>
                            <td style="text-align: right;">手機號碼 : </td>
                            <td style="text-align: left;">
                                <asp:Label ID="lab_TmallMobile" runat="server"></asp:Label></td>
                        </tr>
                    </table>
                </asp:Panel>
                <%---------------------------------- 訂單基本資料----------------------------------%>
                <table width="800px" style="border: double 3px black;">
                    <tr>
                        <th colspan="2" style="background-color: Gray">訂單基本資料
                        </th>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:Label ID="lblPponCity" runat="server" ForeColor="Red" Font-Size="Larger"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 283px; height: 15px;" valign="top">
                            <table>
                                <tr>
                                    <td class="lbl">檔 號:
                                    </td>
                                    <td style="width: 183px">
                                        <asp:Label ID="lbUniqueId" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="lbl">訂單編號:
                                    </td>
                                    <td style="width: 183px">
                                        <asp:Label ID="lOI" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="lbl">賣家:
                                    </td>
                                    <td style="width: 183px">
                                        <asp:HyperLink ID="hlS" runat="server" Target="_blank" />
                                        <asp:Literal ID="sellerExpireDateInformation" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="lbl">分店:
                                    </td>
                                    <td style="width: 183px">
                                        <asp:HyperLink ID="storeLink" runat="server" Target="_blank" />
                                        <asp:Literal ID="storeExpireDateInformation" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="lbl">買家:
                                    </td>
                                    <td style="width: 183px">
                                        <asp:HyperLink ID="hlM" runat="server">[hlM]</asp:HyperLink>&nbsp;
                                        <asp:Label ID="lMG" runat="server" />
                                        <asp:TextBox ID="HiddenMemberEmail" runat="server" Visible="false"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="lbl">買家電話:
                                    </td>
                                    <td style="width: 183px">
                                        <asp:TextBox ID="tbP" runat="server" Width="183px" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="lbl">買家手機:
                                    </td>
                                    <td style="width: 183px">
                                        <asp:TextBox ID="tbM" runat="server" Width="183px" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="lbl">賣家電話:
                                    </td>
                                    <td style="width: 183px">
                                        <asp:Label ID="lSP" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="lbl" style="height: 23px">賣家傳真:
                                    </td>
                                    <td style="width: 183px; height: 23px;">
                                        <asp:Label ID="lSF" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td valign="top" style="height: 15px">
                            <table>
                                <tr>
                                    <td style="width: 103px; height: 23px;" class="lbl">買家公司:
                                    </td>
                                    <td style="width: 244px; height: 23px;">
                                        <asp:Label ID="lMC" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="lbl" style="width: 103px">外送地址:
                                    </td>
                                    <td style="width: 244px">
                                        <asp:TextBox ID="tbDA" Width="350px" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="lbl" style="width: 103px; height: 23px;">檔次:
                                    </td>
                                    <td style="width: 244px; height: 23px;">
                                        <asp:Label ID="lblDeal" runat="server" />
                                        <a href="../../<%=LinkImageBid%>">
                                            <img src="/Images/icons/icon_find.gif" border="0" alt=""></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="lbl" style="width: 103px; height: 23px;">訂購時間:
                                    </td>
                                    <td style="width: 244px; height: 23px;">
                                        <asp:Label ID="lOT" runat="server" />
                                    </td>
                                </tr>
                                <%--客服目前已無使用，先隱藏--%>
                                <tr style="display: none">
                                    <td class="lbl" style="width: 103px;">配送時間: 
                                    </td>
                                    <td style="color: #ff66ff; width: 244px;">
                                        <asp:Label ID="lDT" ForeColor="Fuchsia" runat="server" Font-Bold="True" Font-Size="Large" />&nbsp;
                                        〞
                                    </td>
                                </tr>
                                <tr>
                                    <td class="lbl" style="width: 103px; height: 23px;">最後修改人:
                                    </td>
                                    <td style="width: 244px; height: 23px;">
                                        <asp:Label ID="lMP" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="lbl" style="width: 103px">最後修改日期:
                                    </td>
                                    <td>
                                        <asp:Label ID="lMT" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="lbl" style="width: 103px">上檔期間
                                    </td>
                                    <td>
                                        <asp:Label ID="orderTime" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="lbl" style="width: 134px">優惠期限/配送日期出貨資訊
                                    </td>
                                    <td>
                                        <asp:Label ID="deliverTime" runat="server" />
                                        <br />
                                        <asp:Label ID="ChangedExpireDate" runat="server" ForeColor="Red" />
                                    </td>
                                </tr>
                                <asp:Panel ID="palLastShipDate" runat="server" Visible="false">
                                    <tr>
                                        <td class="lbl" style="width: 134px">最晚應出貨日
                                        </td>
                                        <td>
                                            <asp:Label ID="lastShipDate" runat="server" />
                                            <br />
                                            <asp:Label ID="lblOverdue" runat="server" ForeColor="Red" Text="※此訂單延遲上傳出貨資訊，系統逾期罰款" Visible="false"/>
                                        </td>
                                    </tr>
                                </asp:Panel>
                                
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <%--客服目前已無使用，先隱藏--%>
                            <table style="display: none">
                                <tr>
                                    <td style="width: 100px" class="lbl">評鑑:
                                    </td>
                                    <td style="width: 500px">
                                        <asp:Image ID="iRV" runat="server" ImageUrl="~/Themes/default/images/ui/space2.jpg" />
                                        <asp:Label ID="lRV" runat="server" />
                                    </td>
                                    <td>
                                        <asp:Label ID="lRD" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="lbl" style="width: 100px">評鑑回覆:
                                    </td>
                                    <td style="width: 500px">
                                        <asp:TextBox ID="tbRR" runat="server" Width="500px" Enabled="false" />
                                    </td>
                                    <td>
                                        <asp:Label ID="lRR" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 283px; height: 75px; vertical-align: top;">
                            <table>
                                <tr>
                                    <td style="width: 100px; height: 22px;" class="lbl">訂購價:
                                    </td>
                                    <td style="width: 183px; height: 22px;">
                                        <asp:Label ID="lST" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 100px; height: 22px" class="lbl">好康單價:
                                    </td>
                                    <td style="width: 183px; height: 22px">
                                        <asp:Label ID="lbUnitPrice" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 100px; height: 22px" class="lbl" valign="top">付款明細:
                                    </td>
                                    <td style="width: 183px; height: 22px">
                                        <asp:Label ID="lbPayments" runat="server" />
                                        <span style="color: #ff66ff;">
                                            <asp:Label ID="lbMasterPass" runat="server" />
                                        </span>
                                        <asp:HiddenField ID="hdInstallment" runat="server"></asp:HiddenField>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 100px; height: 22px" class="lbl" valign="top">付款狀態:
                                    </td>
                                    <td style="width: 183px; height: 22px">
                                        <asp:Label ID="lbPayStatus" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 100px; height: 22px" class="lbl" valign="top">付款日:
                                    </td>
                                    <td style="width: 183px; height: 22px">
                                        <asp:Label ID="lbPayDate" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 100px; height: 22px"></td>
                                    <td style="width: 183px; height: 22px">
                                        <asp:CheckBox ID="cbATM" Text="以ATM付款" Enabled="false" Visible="false" Checked="false"
                                            runat="server"></asp:CheckBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 100px; height: 22px;" class="lbl" valign="top">請款狀態:
                                    </td>
                                    <td style="width: 183px; height: 22px;">
                                        <asp:Label ID="lbChargingSituation" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 100px; height: 22px;" class="lbl" valign="top">核銷狀態:
                                    </td>
                                    <td>
                                        <asp:Label ID="lbVerityType" runat="server" ForeColor="Red"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 100px; height: 22px;" class="lbl" valign="top">檔次狀態:
                                    </td>
                                    <td>
                                        <asp:Label ID="lblBusinessHourSituation" runat="server" ForeColor="Red"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 100px; height: 22px;" class="lbl" valign="top">退貨狀態:
                                    </td>
                                    <td>
                                        <asp:Label ID="lblLatestReturnStatus" runat="server" ForeColor="Red"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 100px; height: 22px;" class="lbl" valign="top">換貨狀態:
                                    </td>
                                    <td>
                                        <asp:Label ID="orderStatusInfo" runat="server" ForeColor="Red"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 100px; height: 22px;" class="lbl" valign="top">檔次退換貨完成日:
                                    </td>
                                    <td>
                                        <asp:Literal ID="ltlFinalBalanceSheetDate" runat="server"></asp:Literal>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 100px; height: 22px;" class="lbl" valign="top">凍結狀態:
                                    </td>
                                    <td>
                                        <asp:Label ID="lblDealFreeze" runat="server" Text=""></asp:Label>
                                        <asp:Button ID="btnFreeze" runat="server" Text="凍結請款" OnClientClick="showFreezeDialog()" />
                                        <asp:Button ID="btnCancelFreeze" runat="server" Text="解除凍結請款" OnClientClick="showCloseFreezeDialog()" />
                                    </td>
                                </tr>
                                <tr style="display: none">
                                    <td style="width: 100px; height: 22px;" class="lbl" valign="top">請款單:
                                    </td>
                                    <td>
                                        <asp:Literal ID="lblBS" runat="server" Text=""></asp:Literal>
                                    </td>
                                </tr>
                                <tr style="display: none">
                                    <td style="width: 100px; height: 22px;" class="lbl" valign="top">罰款列表:
                                    </td>
                                    <td>
                                        <asp:Literal ID="ltlVpc" runat="server" Text=""></asp:Literal>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td style="height: 75px; vertical-align: top;">
                            <asp:MultiView ID="mvInvoiceOrReceipt" runat="server" ActiveViewIndex="0">
                                <asp:View ID="View1" runat="server">
                                    <table>
                                        <tr>
                                            <td style="width: 100px; vertical-align: top;" class="lbl">發票:
                                            </td>
                                            <td style="width: 350px">
                                                <table>
                                                    <tr id="InvoiceVersion" class="ItemTitles">
                                                        <td colspan="2">
                                                            <asp:Label ID="lblInvoiceVersion" runat="server" Font-Bold="true" color="white"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <% if (HaveInvoice)
                                                       { %>
                                                    <tr class="SelectedItem">
                                                        <td colspan="2">
                                                            <div style="width: 200px; float: left;">是否開立發票</div>
                                                            <div style="width: 140px; text-align: right; float: left;">
                                                                <img alt="" style="width: 20px;" src="../../Themes/default/images/17Life/G2/blueTick.png" />
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <% }
                                                       else
                                                       { %>
                                                    <tr class="UnSelectedItem">
                                                        <td colspan="2">是否開立發票
                                                        </td>
                                                    </tr>
                                                    <% } %>
                                                    <tr class="UnSelectedItem">
                                                        <td colspan="2">免稅檔次
                                                        </td>
                                                    </tr>
                                                    <tr class="UnSelectedItem">
                                                        <td colspan="2">不開立發票
                                                        </td>
                                                    </tr>
                                                    <% if (EinvoicePapered)
                                                       { %>
                                                    <tr class="SelectedItem">
                                                        <td colspan="2">
                                                            <div style="width: 200px; float: left;">已印製紙本</div>
                                                            <div style="width: 140px; text-align: right; float: left;">
                                                                <img alt="" style="width: 20px;" src="../../Themes/default/images/17Life/G2/blueTick.png" />
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <% }
                                                       else
                                                       { %>
                                                    <tr class="UnSelectedItem">
                                                        <td colspan="2">已印製紙本</td>
                                                    </tr>
                                                    <% } %>
                                                    <tr>
                                                        <td>&nbsp;</td>

                                                    </tr>
                                                    <tr class="ItemTitles">
                                                        <td colspan="2">【發票狀態】<div style="float: right;">
                                                            <input id="clickLog" type="button" value="發票歷程" onclick="showInvLog()" /></div>
                                                            <br />
                                                            <asp:Label ID="lab_EinvoiceHistory" runat="server" ForeColor="White"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <% if (EinvoiceWinnerVisible)
                                                       {
                                                           if (EinvoiceWinner)
                                                           { %>
                                                    <tr class="SelectedItem">
                                                        <td colspan="2">
                                                            <div style="width: 200px; float: left;">此發票中獎</div>
                                                            <div style="width: 140px; text-align: right; float: left;">
                                                                <img alt="" style="width: 20px;" src="../../Themes/default/images/17Life/G2/blueTick.png" />
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <% }
                                                           else
                                                           { %>
                                                    <tr class="UnSelectedItem">
                                                        <td colspan="2">此發票中獎
                                                        </td>
                                                    </tr>
                                                    <% }
                                                       } %>
                                                    <% if (AllowanceSentBackVisible)
                                                       {
                                                           if (AllowanceSentBack)
                                                           { %>
                                                    <tr class="SelectedItem">
                                                        <td colspan="2">
                                                            <div style="width: 200px; float: left;">紙本折讓單已寄回</div>
                                                            <div style="width: 140px; text-align: right; float: left;">
                                                                <img alt="" style="width: 20px;" src="../../Themes/default/images/17Life/G2/blueTick.png" />
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <% }
                                                    else
                                                    { %>
                                                    <tr class="UnSelectedItem">
                                                        <td colspan="2">紙本折讓單已寄回
                                                        </td>
                                                    </tr>
                                                    <% }
                                                       } %>
                                                    <% if (InvoiceSentBackVisible)
                                                       {
                                                           if (InvoiceSentBack)
                                                           { %>
                                                    <tr class="SelectedItem">
                                                        <td colspan="2">
                                                            <div style="width: 200px; float: left;">發票已寄回</div>
                                                            <div style="width: 140px; text-align: right; float: left;">
                                                                <img alt="" style="width: 20px;" src="../../Themes/default/images/17Life/G2/blueTick.png" />
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <% }
                                                           else
                                                           { %>
                                                    <tr class="UnSelectedItem">
                                                        <td colspan="2">發票已寄回
                                                        </td>
                                                    </tr>
                                                    <% }
                                                       } %>

                                                    <tr>
                                                        <td colspan="2">
                                                            <asp:Panel ID="pnRefundForm" runat="server">
                                                                <%--↓--客服目前已無使用，先隱藏--↓--%>
                                                                <asp:CheckBox ID="cbPaper" runat="server" Text="退貨申請書已寄回" Visible="false"></asp:CheckBox><br />
                                                                <a target="_blank" href='<%= string.Format("../../User/ReturnApplicationForm.aspx?u={0}&oid={1}",HttpUtility.UrlEncode(MemberEmail), OrderId)%>' style="display: none">檢視申請書</a>&nbsp;&nbsp; <a href='<%= string.Format("../../Service/returnapplicationform.ashx?u={0}&oid={1}", Server.UrlEncode(MemberEmail), OrderId)%>' style="display: none">匯出申請書</a>&nbsp;&nbsp;
                                                                <asp:LinkButton ID="lbRefundApplicationResend" OnClientClick="appResend()" runat="server"
                                                                    OnClick="lbReturnApplicationResend_Click" Style="display: none">寄出申請書</asp:LinkButton><br />
                                                                <%--↑--客服目前已無使用，先隱藏--↑--%>
                                                                <a target="_blank" href='<%= string.Format("../../User/ReturnDiscount.aspx?u={0}&oid={1}", HttpUtility.UrlEncode(MemberEmail), OrderId)%>'>檢視折讓單</a>&nbsp;&nbsp; <a href='<%= string.Format("../../Service/returndiscount.ashx?u={0}&oid={1}", Server.UrlEncode(MemberEmail), OrderId)%>'>匯出折讓單</a>&nbsp;&nbsp;
                                                                <asp:LinkButton ID="lbRefundDiscountResend" OnClientClick="appResend()" runat="server"
                                                                    OnClick="lbReturnDiscountResend_Click">寄出折讓單</asp:LinkButton>
                                                            </asp:Panel>
                                                            <asp:HiddenField ID="hif_SelectMemberEmail" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <asp:Panel ID="create_Invoice" runat="server">
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lbINItemName" runat="server" Text="發票號碼:"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lbl_EinvoiceNum" runat="server"></asp:Label>
                                                                <asp:HiddenField ID="hif_EinvoiceId" runat="server"></asp:HiddenField>

                                                            </td>

                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">
                                                                <asp:Panel ID="pnInvoice" runat="server" Visible="false" Width="350">
                                                                    <table>
                                                                        <uc2:EinvoiceControl ID="invCtrl" runat="server" />
                                                                    </table>
                                                                </asp:Panel>
                                                            </td>
                                                        </tr>
                                                    </asp:Panel>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:View>
                                <asp:View ID="View2" runat="server">
                                    <table>
                                        <tr>
                                            <td style="width: 100px; vertical-align: top;" class="lbl">代收轉付:
                                            </td>
                                            <td style="width: 343px">
                                                <div style="padding-top: 4px">
                                                    <asp:CheckBox ID="chkReceiptIsGenerated" runat="server" Text="己開立代收轉付收據" Enabled="false"
                                                        Checked="false"></asp:CheckBox>
                                                </div>
                                                <div style="padding-top: 4px">
                                                    <span style="display: inline-block; width: 140px">代收轉付收據註記: </span>
                                                    <asp:TextBox ID="txtReceiptRemark" runat="server" Enabled="false" MaxLength="8" Width="150" />
                                                </div>
                                                <div style="padding-top: 4px">
                                                    <span style="display: inline-block; width: 140px">代收轉付收據號碼: </span>
                                                    <asp:TextBox ID="txtReceiptCode" runat="server" Enabled="false" MaxLength="8" Width="150" />
                                                </div>
                                                <div style="padding-top: 4px">
                                                    <asp:CheckBox ID="chkReceiptIsPhysical" runat="server" Text="需寄送收據" Checked="false" />
                                                </div>
                                                <div style="padding-top: 4px">
                                                    <span style="vertical-align: top; display: inline-block; width: 80px; text-align: right; padding-right: 10px; margin-left: 20px">地 址: </span>
                                                    <asp:TextBox ID="txtReceiptAddress" runat="server" MaxLength="8" Width="200" />
                                                </div>
                                                <div style="padding-top: 4px">
                                                    <span style="vertical-align: top; display: inline-block; width: 80px; text-align: right; padding-right: 10px; margin-left: 20px">統一編號: </span>
                                                    <asp:TextBox ID="txtReceiptNumber" runat="server" MaxLength="8" Width="200" />
                                                </div>
                                                <div style="padding-top: 4px">
                                                    <span style="vertical-align: top; display: inline-block; width: 80px; text-align: right; padding-right: 10px; margin-left: 20px">買受人: </span>
                                                    <asp:TextBox ID="txtReceiptTitle" runat="server" MaxLength="8" Width="200" />
                                                </div>
                                                <asp:PlaceHolder ID="phDeliveryReceiptMailBack" runat="server">
                                                    <div style="padding-top: 4px">
                                                        取消訂單檢核項目
                                                    </div>
                                                    <div style="clear: right; padding-top: 5px">
                                                        <span style="display: block; float: left; width: 48%">
                                                            <asp:CheckBox ID="chkReceiptMailbackAllowance" runat="server" Text="折讓單已寄回" /></span>
                                                        <span style="display: block; float: left; width: 48%">
                                                            <asp:CheckBox ID="chkReceiptMailbackReceipt" runat="server" Text="代收轉付收據已寄回" /></span>
                                                        <span style="display: block; float: left; width: 48%">
                                                            <asp:CheckBox ID="chkReceiptMailbackPaper" runat="server" Text="退貨申請書已寄回" /></span>
                                                    </div>
                                                </asp:PlaceHolder>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:View>
                            </asp:MultiView>
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top;" class="style1">
                            <%--<table>
                                
                               <tr>
                                    <td style="width: 100px; height: 22px" class="lbl" valign="top">
                                        憑證編號:
                                    </td>
                                    <td style="width: 183px; height: 22px">
                                        <asp:Label ID="lbCouponNo" runat="server"/>
                                    </td>
                                </tr>
                            </table>--%>
                        </td>
                        <td style="vertical-align: top;">
                            <table>
                                <tr>
                                    <%--<td style="width: 100px; height: 22px; vertical-align: top;" class="lbl">
                                        取消訂單申請:
                                    </td>--%>
                                    <td style="width: 400px; height: 22px">
                                        <table>
                                            <tr>
                                                <%--<td>
                                                    訂單狀態：
                                                </td>--%>
                                                <td>
                                                    <asp:Label ID="lbReturnSituation" runat="server" ForeColor="Red" Visible="False"
                                                        Enabled="False" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:RadioButton ID="rbRsProcessing" runat="server" Text="人工處理中" GroupName="ReturnSteps"
                                                        Visible="False" Enabled="False"></asp:RadioButton>
                                                </td>
                                                <td>
                                                    <%--<a href='<%=string.Format("../Ppon/PponReturnApplication.aspx?oid={0}", OrderId)%>'
                                                        target="_blank">產生取消訂單申請書</a>--%>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:RadioButton ID="rbRsSuccess" runat="server" Text="完成" GroupName="ReturnSteps"
                                                        Visible="False" Enabled="False"></asp:RadioButton>
                                                </td>
                                                <td>
                                                    <asp:Button ID="btReturnMail" runat="server" Text="發送處理完畢通知" Width="200" Enabled="false"
                                                        Visible="False" OnClick="btReturnMail_Click"></asp:Button>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:RadioButton ID="rbRsFailure" runat="server" Text="無法取消" GroupName="ReturnSteps"
                                                        Visible="False" Enabled="False"></asp:RadioButton>
                                                </td>
                                                <td>
                                                    <%--<asp:TextBox ID="tbReturnReason" runat="server" Width="200"></asp:TextBox>--%>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:RadioButton ID="rbRsAuto" runat="server" Text="自動處理中" GroupName="ReturnSteps"
                                                        Visible="False" Enabled="False"></asp:RadioButton>
                                                </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <asp:RadioButton ID="rbRsAutoScash" runat="server" Text="自動退購物金處理中" GroupName="ReturnSteps"
                                                        Visible="False" Enabled="False"></asp:RadioButton>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <asp:RadioButton ID="rbRsProcessingScash" runat="server" Text="人工退購物金處理中" GroupName="ReturnSteps"
                                                        Visible="False" Enabled="False"></asp:RadioButton>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <table>
                                <asp:Panel ID="oldExchange" runat="server">
                                    <tr>
                                        <td style="width: 100px" class="lbl">調整狀態:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlST" runat="server" DataTextField="Value" DataValueField="Key"
                                                Width="103px" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 100px" class="lbl">處理備註:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tbReturnMessage" runat="server" Width="600"></asp:TextBox>
                                        </td>
                                    </tr>
                                </asp:Panel>
                                <tr>
                                    <td style="width: 100px" class="lbl">訂單狀態:
                                    </td>
                                    <td>
                                        <asp:CheckBoxList ID="cbS" Enabled="false" ForeColor="black" Width="700" DataValueField="Key"
                                            DataTextField="Value" RepeatDirection="Horizontal" RepeatColumns="5" runat="server"
                                            Height="13px" />
                                         &nbsp;<asp:CheckBox ID="chkIsCanceling" runat="server" Checked="true" Visible="false" Text="訂單取消中" Enabled="false"/>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <table width="100%">
                                <tr>
                                    <td align="center" style="height: 26px;" colspan="2">&nbsp;
                                        <asp:Button ID="btnUpdate" runat="server"
                                            OnClientClick="if (CheckInvoiceInfo()) {return checkTriplicate();} else{return false;} " OnCommand="UpdateClick" Text="更新基本資料" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <%---------------------------------- end of 訂單基本資料----------------------------------%>

                <%---------------------------------- 訂單備註 ----------------------------------%>
                <table width="800px" style="border: double 3px black; text-align: center;">
                    <tr>
                        <th style="background-color: Gray" colspan="2">訂單備註</th>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%" >
                                <tr>
                                    <td width="30%">內容（將更新於線上清冊中）：<br />
                                        <asp:Label ID="lbShipMemo" runat="server" ForeColor="Red" Text="已開始出貨，新增後請另外聯繫廠商。"
                                            Visible="false" Font-Size="Small"></asp:Label>
                                    </td>
                                    <td width="60%">
                                        <asp:TextBox ID="tbUserMemo" TextMode="MultiLine" runat="server" Width="100%" Height="70px"></asp:TextBox>
                                    </td>
                                    <td width="10%" align="center">
                                        <asp:Button runat="server" Text="新增" OnClick="AddOrderUserMemo_Click" />
                                        <asp:Button ID="btRefer" runat="server" OnClick="btRefer_Click" Text="照會成功" />
                                        <asp:Button ID="btReferFail" runat="server" Text="照會失敗" OnClick="btReferFail_Click" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%" class="tbOrderUserMemo">
                                <tr>
                                    <th width="15%">建立時間</th>
                                    <th width="55%">內容</th>
                                    <th width="20%">建立者</th>
                                    <th width="10%">編輯</th>
                                </tr>
                                <asp:Repeater ID="repOrderUserMemo" runat="server" OnItemCommand="repOrderUserMemo_ItemCommand">
                                    <ItemTemplate>
                                        <tr>
                                            <td align="center">
                                                <asp:Literal runat="server" Text='<%# Eval("CreateTime", "{0:yyyy-MM-dd HH:mm}")%>'></asp:Literal>
                                            </td>
                                            <td>
                                                <asp:Literal runat="server" Text='<%# Eval("UserMemo")%>'></asp:Literal>
                                            </td>
                                            <td align="center">
                                                <asp:Literal runat="server" Text='<%# Eval("CreateId")%>'></asp:Literal>
                                            </td>
                                            <td align="center">
                                                <asp:Button ID="btnDeleteUserMemo" runat="server" Text="刪除" CommandName="DeleteUserMemo"
                                                    CommandArgument='<%# Eval("Id")%>' />
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </table>
                        </td>
                    </tr>
                </table>
                <%---------------------------------- end of 訂單備註 ----------------------------------%>

                <br />
                <%---------------------------------- 商品資訊 ----------------------------------%>
                <div id="OrderProductContainer" runat="server">
                    <table width="800px" class="tbl-block">
                        <thead>
                            <tr>
                                <th>商品資訊</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <table>
                                        <thead>
                                            <tr>
                                                <th>規格</th>
                                                <th>總數</th>
                                                <th>本次退貨(可退數)</th>
                                                <th>折讓單</th>
                                                <th>發票號碼</th>
                                                <th>中獎發票</th>
                                            </tr>
                                        </thead>
                                        <asp:ListView ID="lvOrderProducts" runat="server" ItemPlaceholderID="Row"
                                            OnItemDataBound="lvOrderProducts_ItemDataBound">
                                            <LayoutTemplate>
                                                <tbody>
                                                    <tr id="Row" runat="server"></tr>
                                                </tbody>
                                            </LayoutTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <td>
                                                        <%--規格--%>
                                                        <asp:HiddenField runat="server" ID="csvProductIds" />
                                                        <asp:Literal runat="server" Text='<%# Eval("Spec") %>' />
                                                    </td>
                                                    <td style="text-align: center;">
                                                        <%--總數--%>
                                                        <asp:HiddenField runat="server" ID="hdTotalCount" Value='<%# Eval("TotalCount") %>' />
                                                        <asp:Literal runat="server" Text='<%# Eval("TotalCount") %>' />
                                                    </td>
                                                    <td style="text-align: center;">
                                                        <%--本次退貨(可退數)--%>
                                                        <asp:DropDownList ID="returningCountSelections" runat="server" />
                                                    </td>
                                                    <td id="AllowanceCell" runat="server" style="text-align: center;">
                                                        <%--折讓單--%>
                                                        <a href='<%= string.Format("../../Service/returndiscount.ashx?u={0}&oid={1}", MemberEmail, OrderId)%>'>匯出</a>
                                                        <asp:LinkButton ID="linkSendCreditNoteEmail" runat="server"
                                                            OnClientClick="appResend()" OnClick="lbReturnDiscountResend_Click">寄出</asp:LinkButton>
                                                    </td>
                                                    <td id="InvoiceNumberCell" runat="server">
                                                        <%--發票號碼--%>
                                                        <asp:Literal ID="litInvoiceNumber" runat="server"></asp:Literal>
                                                        <div style="float: right; margin-right: 5px;">
                                                            <asp:Button ID="btnRequestInvoice" runat="server" Text="申請紙本" Visible="false" OnClick="RequestInvoice_Click" />
                                                        </div>
                                                    </td>
                                                    <td id="WinningInvoiceCell" runat="server" style="text-align: center;">
                                                        <%--中獎發票--%>
                                                        <asp:CheckBox ID="chkWinningInvoice" runat="server" Enabled="False" />
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:ListView>
                                        <tfoot>
                                            <tr>
                                                <td colspan="6">
                                                    <asp:Label ID="lbRefundReason" runat="server">申請退貨原因：</asp:Label><br />
                                                    <asp:TextBox ID="txtRefundReason" runat="server" TextMode="MultiLine" Width="600px" Height="60px"></asp:TextBox><br />
                                                    <asp:Panel ID="panelReceiver" runat="server" Visible ="false">
                                                        <asp:Label id="lbReceiver" runat="server" class="unit-label">收件人資訊：</asp:Label>&nbsp;<br />
                                                        <asp:Label id="lbReceiverName" runat="server" class="unit-label">姓名</asp:Label>&nbsp;
                                                        <asp:TextBox ID="txtReceiverName" runat="server" CssClass="txtReceiverName input-2over3 input-full" width="150px" />
                                                        <span id="ReceiverNameErr" style="display: none;color:red;">請填入姓名！</span><br />
                                                        <asp:Label  id="lbReceiverAddress" runat="server" class="unit-label">地址</asp:Label>&nbsp;
                                                        <asp:TextBox ID="txtReceiverAddress" runat="server" CssClass="txtReceiverAddress input-full" width="300px"/>
                                                        <span id="ReceiverAddressErr" style="display: none; color: red;">請填入地址！</span>
                                                    </asp:Panel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="6">
                                                    <input type="hidden" id="hidNoReturnMsg" runat="server" class="noReturnMsg" />
                                                    <input type="hidden" id="hidNoExchangeMsg" runat="server" class="noExchangeMsg" />
                                                    <asp:CheckBox ID="chkSCash" runat="server" Checked="True" Text="只退購物金" />
                                                    <asp:Button ID="btnCreateRefundForm" runat="server" Text="建立退貨單"
                                                        OnClientClick="if(!checkReceiver()){return false;}else{if(!confirm('確定申請退貨，並建立退貨單嗎？')){return false;}; if($(this).parent().find('.noReturnMsg').val() != ''){return confirm( $(this).parent().find('.noReturnMsg').val()+'，確定建立退貨單嗎？' );}}"
                                                        OnClick="CreateRefundForm_Click" />
                                                    <asp:Button ID="btnCreateExchangeLog" runat="server" Text="建立換貨單" Visible="false"
                                                        OnClientClick="if(!checkReceiver()){return false;}else{if(!confirm('確定申請換貨，並建立換貨單嗎？')){return false;}; if($(this).parent().find('.noExchangeMsg').val() != ''){return confirm( $(this).parent().find('.noExchangeMsg').val()+'，確定建立換貨單嗎？' );}}"
                                                        OnClick="CreateExchangeLog_Click" />
                                                    <asp:Button ID="btnCancelOrder" runat="server" Text="取消訂單" Visible="false" OnClick="CancelOrder_Click" />
                                                    <asp:Button ID="btnWithdrawCancelOrder" runat="server" Text="抽回[取消訂單]申請" Visible="false" OnClick="WithdrawCancelOrder_Click" />
                                                </td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <%---------------------------------- end of 商品資訊 ----------------------------------%>


                <%---------------------------------- 憑證資訊 ----------------------------------%>
                <div id="CouponContainer" runat="server">
                    <table class="tbl-block"  width="100%" >
                        <thead>
                            <tr>
                                <th>憑證資訊</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <asp:GridView ID="gvInStore" runat="server" AutoGenerateColumns="False" Width="98%"
                                        OnRowCommand="gvInStore_RowCommand" OnRowDataBound="gvInStore_RowDataBound">
                                        <Columns>
                                            <asp:TemplateField HeaderText="核取">
                                                <ItemTemplate>
                                                    <asp:HiddenField ID="hfCouponId" Value='<%# Eval("CouponId") %>' runat="server" />
                                                    <asp:CheckBox Enabled="False" ID="cbCancel" runat="server"></asp:CheckBox>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="憑證編號" ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:HyperLink ID="hlSN" Text='<%# Eval("SequenceNumber") %>' runat="server" NavigateUrl='<%# "../Verification/CouponVerification.aspx?csn=" + Eval("SequenceNumber") %>'>
                                                    </asp:HyperLink>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="CouponCode" HeaderText="確認碼" />
                                            <asp:TemplateField HeaderText="狀態">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblStatus" runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="簡訊憑證">
                                                <ItemTemplate>
                                                    <asp:Literal ID="litSmsSentCount" runat="server"></asp:Literal>
                                                    <%--<asp:Button ID="btSmsSentCount" runat="server" CommandName="ResetSmsLogStatus" CommandArgument='<%# Eval("CouponId") %>' Text="Reset" />--%>
                                                    <asp:Button ID="btSmsSentCount" runat="server" CommandName="SmsLogStatus" CommandArgument='<%# Eval("CouponId") %>'
                                                        Text="發送" />
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="強制開放憑證">
                                                <ItemTemplate>
                                                    <asp:Literal ID="Literal1" runat="server" Text='<%# Eval("CouponAvailable") != null ? CouponAvailableToString( Eval("CouponAvailable").ToString()) : "NULL" %>'></asp:Literal>
                                                    <asp:Button ID="btCouponAvailableToggle" runat="server" CommandName="ToggleCouponAvailable"
                                                        CommandArgument='<%# Eval("CouponId") + "," + (Eval("CouponAvailable") ?? "NULL") %>'
                                                        Text='改變' />
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="強制核銷回收款項處理">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbUnrecovered" runat="server" Text='<%# Eval("CouponId") != null ? CouponAvailableToString( Eval("CouponId").ToString()) : string.Empty %>'></asp:Label>
                                                    <asp:Button ID="btRecoveredConfirm" runat="server" CommandName="RecoveredConfirm"
                                                        CommandArgument='<%# Eval("CouponId") + "," + (Eval("CouponAvailable") ?? "NULL") %>'
                                                        Text='改變' />
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="發票號碼" HeaderStyle-CssClass="invoice">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbInvoiceNumber" runat="server"></asp:Label>
                                                    <div>
                                                        <asp:Button ID="btnRequestInvoice" runat="server" CommandName="RequestInvoice" Text="申請紙本" Visible="false" />
                                                    </div>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="中獎發票" HeaderStyle-CssClass="invoice">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="cbInvoiceWinning" Enabled="false" runat="server"></asp:CheckBox>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="代收轉付收據已寄回" HeaderStyle-CssClass="receipt" Visible="true">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="cbReceipGot" Enabled="false" runat="server" />
                                                    <asp:Button ID="btReceiptGot" Visible="false" runat="server" CommandName="ReceiptMailBackSet"
                                                        CommandArgument='<%# Eval("CouponId") %>' Text="改變" />
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="預約申請時間">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblReservationCreateDate" runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="預約時間">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblReservationDateTime" runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="預約狀態">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblReservationStatus" runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>

                                    <asp:Panel ID="create_Return" runat="server">
                                        <span>申請退貨原因：</span><br/>
                                        <asp:TextBox ID="txtCouponRefundReason" runat="server" TextMode="MultiLine" Width="600px" Height="60px"></asp:TextBox>
                                        <asp:CheckBox ID="chkCouponReturnSCash" Checked="True" runat="server" Text="只退購物金" />
                                        <input type="hidden" id="hidCouponNoReturnMsg" runat="server" class="noReturnMsg" />
                                        <asp:Button ID="btnCreateCouponReturnForm" runat="server" Text="建立退貨單"
                                            OnClientClick="if(!confirm('確定申請退貨，並建立退貨單嗎？')){return false;}; if($(this).parent().find('.noReturnMsg').val() != ''){return confirm( $(this).parent().find('.noReturnMsg').val()+'，確定建立退貨單嗎？' );}"
                                            OnClick="CreateCouponReturnForm_Click" />
                                    </asp:Panel>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <%---------------------------------- end of 憑證資訊 ----------------------------------%>
                <br />
				

				<div id="fuli" class="fuli">
					<div style="background-color: #cccccc;border: 1px solid black;text-align: center;padding: 3px;font-weight: bold;">富利憑證即時狀態</div>
					<div class="fuli-body">
						<div class="fuli-row">
							<div class="fuli-code">
								###########
							</div>
							<div class="fuli-status">
								查詢中
							</div>
						</div>
					</div>
					<button type="button">註銷</button>
				</div>

                <%--<asp:Button ID="bAddServiceLog" runat="server" Text="新增客服記錄" CssClass="ab_b" OnClick="bAddServiceLog_Click"
                    OnClientClick="bAddServiceLog_Click();" />--%>
                <asp:Button ID="bServiceIntegrate" runat="server" Text="客服整合系統" CssClass="ab_b" OnClick="bServiceIntegrate_Click" />
                <asp:Button ID="btCtFilled" runat="server" Text="補憑證信託資料" CssClass="ab_b" OnClick="btCtFilled_Click" />
                <asp:Button ID="btnCouponDetail" runat="server" Text="成套票券退款細目" CssClass="ab_b" Visible="true" OnClientClick="block_ui('divTrustCouponDetail');" />
                <asp:Button ID="btnFine" runat="server" Text="廠商罰款" CssClass="ab_b" Visible="false" ClientIDMode="static"/>

                
                <asp:Button ID="btnZipCode" runat="server" Text="下載憑證PDF" CssClass="ab_b" Visible="true" ClientIDMode="static"  OnClick="GetCouponToZip" />
                <asp:HiddenField ID="HiddenUseServiceFlag" runat="server" />
                <asp:HiddenField ID="HiddenIsInvoiceExpired" Value="false" runat="server" />

                <%---------------------------------- 憑證信託資料 ----------------------------------%>
                <div id="divTrustCouponDetail" class="pop-window" style="display: none; text-align: left;">
                    <div>
                        <asp:Repeater ID="rp_TrustCouponList" runat="server" OnItemDataBound="rp_TrustSequenceItemBound">
                            <HeaderTemplate>
                                <table width="480px" border="0" cellspacing="0" cellpadding="0" class="rd-mcOrdetail" style="text-align: center">
                                    <thead>
                                        <tr class="rd-C-Hide">
                                            <th class="OrderSN">
                                                信託碼
                                            </th>
                                            <th class="">
                                                信託金額
                                            </th>
                                            <th class="OrderPonState">
                                                使用狀況
                                            </th>
                                            <th class="OrderPonState">核銷時間
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr class="">
                                    <td class="">
                                        <%# ((CashTrustLog)(Container.DataItem)).TrustSequenceNumber%>
                                    </td>
                                    <td class="">
                                        <%# ((CashTrustLog)(Container.DataItem)).Amount == 0 ? "--" : "$" + (((CashTrustLog)(Container.DataItem)).Amount - ((CashTrustLog)(Container.DataItem)).DiscountAmount).ToString("F0") %>
                                    </td>
                                    <td class="">
                                        <asp:Label ID="lab_rpCouponSequenceStatus" runat="server" ></asp:Label>
                                        <asp:HiddenField ID="hif_rpCouponId" runat="server" Value="<%# ((CashTrustLog)(Container.DataItem)).CouponId%>" />
                                    </td>
                                    <td class="">
                                        <%# ((CashTrustLog)(Container.DataItem)).UsageVerifiedTime%>
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </tbody></table>
                            </FooterTemplate>
                        </asp:Repeater>
                        <p>註1：信託碼不等於憑證，此處為本張訂單進入信託行的資料</p>
                    </div>
                    <div class="MGS-XX" onclick="unblock_ui();" style="cursor: pointer; background: url(../../Themes/PCweb/images/A2-Multi-grade-Setting_xx.png); height: 28px; width: 27px; position: absolute; top: -24px; right: -25px; z-index: 800;">
                    </div>
                </div>
                <%---------------------------------- end of 憑證信託資料 ----------------------------------%>
                
                <%---------------------------------- 廠商罰款、對帳單 ----------------------------------%>
                <div id="divFine" class="pop-window" style="display: none; text-align: left;">
                    <div>
                        <table width="800px">
                            <tr style="text-align:center">
                                <td  colspan="2"><h3>廠商罰款</h3></td>
                            </tr>
                            <tr>
                                <td style="width:30%">扣款/異動金額：</td>
                                <td style="width:70%"><asp:TextBox ID="txtAmount" runat="server"></asp:TextBox>&nbsp;/ 元 
                                    <span style="color:red">(扣款請寫：-200)</span></td>
                            </tr>
                            <tr>
                                <td colspan="2"><asp:RadioButton ID="rdCompensateYes" runat="server" Text="須給消費者" Checked="true" GroupName="IsCompensateConsumer" />
                                    ，金額：<asp:TextBox ID="txtPromotionValue" runat="server"></asp:TextBox>&nbsp;/ 元
                                    <asp:RadioButton ID="rdCompensateNo" runat="server" Text="不須給消費者" GroupName="IsCompensateConsumer"/>
                                </td>
                            </tr>
                            <tr>
                                <td style="width:30%">扣款/異動原因：</td>
                                <td style="width:70%"><asp:DropDownList ID="ddlReason" runat="server"></asp:DropDownList></td>
                            </tr>
                            <tr>
                                <td style="width:30%">給商家的資訊：</td>
                                <td style="width:70%"><asp:TextBox ID="txtReason" runat="server" TextMode="MultiLine"></asp:TextBox></td>
                            </tr>
                        </table>
                        <table width="800px" id="tbBonus">
                            <tr>
                                <td style="width:30%">紅利金生效期限：</td>
                                <td style="width:70%">
                                    <input name="txtBonusStartTime" type="text" class="datepick" />
                                    ~<input name="txtBonusExpireTime" type="text" class="datepick" />
                                </td>
                            </tr>
                            <tr>
                                <td style="width:30%">給客人的紅利金摘要：</td>
                                <td style="width:70%"><asp:TextBox ID="txtBonusAction" runat="server" TextMode="MultiLine"></asp:TextBox></td>
                            </tr>
                        </table>
                        <asp:Button ID="btnUpdateNowBalanceSheet" runat="server" Text="異動已產出對帳單"/>
                        <asp:Button ID="btnUpdateNextBalanceSheet" runat="server" Text="異動下期對帳單"/>
                    </div>
                    
                    <div class="MGS-XX" onclick="unblock_ui();" style="cursor: pointer; background: url(../../Themes/PCweb/images/A2-Multi-grade-Setting_xx.png); height: 28px; width: 27px; position: absolute; top: -24px; right: -25px; z-index: 800;">
                    </div>
                </div>

                <div id="divWmsRefund" class="pop-window" style="display: none; text-align: left;">
                    <div>
                        <table width="800px">
                            <tr style="text-align:center">
                                <td><h3>PCHOME退貨</h3></td>
                            </tr>
                            <tr>
                                <td>
                                    <input id="FromPC" name="wmsRefundFrom" type="radio" value="<%=(int)WmsRefundFrom.Pchome %>"/>PChome倉庫>廠商<br />
                                    ＊聯絡人<input id="ContactName" type="text" value="邱欣儀" /><br /><br />
                                    ＊電話<input id="ContactTel" type="text" value="(02)2700-0898 #3423"/><br /><br />
                                    手機<input id="ContactMobile" type="text" /><br /><br />
                                    ＊信箱<input id="ContactEmail" type="text" value="csdm@17life.com" /><br /><br />
                                    ＊地址<input id="ContactAddress" type="text" value="333 桃園市龜山區民生北路一段386號" style="width:300px"/><br /><br />
                                </td>
                                
                            </tr>
                            <tr>
                                <td>
                                    <input id="FromCustomer" name="wmsRefundFrom" type="radio" value="<%=(int)WmsRefundFrom.Customer %>"/>消費者>廠商
                                </td>
                                
                            </tr>
                        </table>
                        <asp:Button ID="btnWmsRefundSubmit" runat="server" Text="確定"/>
                        <input type="hidden"id="hidReturnFormId" />
                    </div>
                    
                    <div class="MGS-XX" onclick="unblock_ui();" style="cursor: pointer; background: url(../../Themes/PCweb/images/A2-Multi-grade-Setting_xx.png); height: 28px; width: 27px; position: absolute; top: -24px; right: -25px; z-index: 800;">
                    </div>
                    <br />
                </div>
                
                <div id="divFineNow" class="pop-window" style="display: none; text-align: left;">
                    <div>
                        <table width="800px">
                            <tr style="text-align:center">
                                <td colspan="2"><h3>廠商罰款</h3></td>
                            </tr>
                            <tr>
                                <td style="width:30%">扣款/異動金額：</td>
                                <td style="width:70%"><span id="spanAmount"></span>/元</td>
                            </tr>
                            <tr>
                                <td style="width:30%">須給消費者，金額：</td>
                                <td style="width:70%"><span id="spanPromotionValue"></span>/元</td>
                            </tr>
                            <tr>
                                <td style="width:30%">扣款原因：</td>
                                <td style="width:70%"><span id="spanReason"></span></td>
                            </tr>
                            <tr>
                                <td style="width:30%">請選擇欲調整之對帳單：</td>
                                <td style="width:70%"><asp:DropDownList ID="ddlChangeBalanceSheet" runat="server"></asp:DropDownList></td>
                            </tr>
                        </table>
                        <asp:Button ID="btnNowCancel" runat="server" Text="取消"/>
                        <asp:Button ID="btnNowYes" runat="server" Text="確定"/>
                    </div>
                    
                    <div class="MGS-XX" onclick="unblock_ui();" style="cursor: pointer; background: url(../../Themes/PCweb/images/A2-Multi-grade-Setting_xx.png); height: 28px; width: 27px; position: absolute; top: -24px; right: -25px; z-index: 800;">
                    </div>
                    <br />
                </div>
                <%---------------------------------- end of 廠商罰款、對帳單 ----------------------------------%>
                

                <%--維護客服紀錄時 須記錄目前scroll-top位置以保持視窗停留於客服紀錄位置--%>
                

                <%---------------------------------- 出貨資訊 ----------------------------------%>
                <asp:Panel ID="divDeliverManage" runat="server" >
                    <asp:Label ID="lbMsgDeliver" runat="server" ForeColor="Red" ViewStateMode="Disabled" />
                    <table style="border: double 3px black; text-align: center; width:100%;">
                        <tr>
                            <th style="background-color: Gray" colspan="11">出貨資訊</th>
                        </tr>
                        <tr>
                            <td>
                                <%-------- 新增出貨 --------%>
                                <table width="100%" id="addDeliver" runat="server">
                                    <tr>
                                        <th style="background-color: Gray; width:15%;">出貨日期</th>
                                        <th style="background-color: Gray; width:15%;">運單編號</th>
                                        <th style="background-color: Gray; width:15%;">物流公司</th>
                                        <th style="background-color: Gray; width:50%;">出貨備註</th>
                                        <th style="background-color: Gray; width:5%;">編輯</th>
                                    </tr>
                                    <tr align="center">
                                        <td>
                                            <asp:TextBox ID="tbAddShipTime" CssClass="datepick" runat="server" MaxLength="10" Width="95%"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tbAddShipNo" runat="server" MaxLength="30" Width="95%"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlAddShipCompanyId" runat="server" Width="95%"></asp:DropDownList>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="tbShipMemo" TextMode="MultiLine" runat="server" MaxLength="30" Rows="5" Width="98%"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:Button ID="addDeliverData" runat="server" Text="新增" OnClientClick="block_postback();"
                                                OnClick="addDeliverData_Click" Width="95%"/>
                                        </td>
                                    </tr>
                                </table>
                                <%-------- 配送資訊 --------%>
                                <asp:GridView ID="gvDeliver" runat="server" AutoGenerateColumns="False" Width="100%"
                                    OnRowEditing="gvDeliver_RowEditing"
                                    OnRowCancelingEdit="gvDeliver_RowCancelingEdit"  
                                    DataKeyNames="OrderShipId" OnRowDataBound="gvDeliver_RowDataBound" OnRowUpdating="gvDeliver_RowUpdating"
                                    OnRowDeleting="gvDeliver_RowDeleting">
                                    <Columns>
                                        <asp:TemplateField HeaderText="日期" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:Label ID="lbShipTime" runat="server"></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="tbShipTime" CssClass="datepick" Text='<%# Eval("ShipTime", "{0:yyyy-MM-dd}") %>'
                                                    runat="server"></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="出貨狀態" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:Label ID="lbStatus" runat="server"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="出貨方式" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:Label ID="lbShipType"  runat="server"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="物流公司" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:Label ID="lbShipCompanyId" Text='<%# Bind("ShipCompanyId") %>' runat="server"></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:DropDownList ID="ddlShipCompanyId" runat="server">
                                                </asp:DropDownList>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="超商門市" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:Label ID="lbStore" runat="server"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="物流編號" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:Label ID="lbShipNo" runat="server"></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="tbShipNo" runat="server"></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="出貨備註" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:Label ID="lbShipMemo" runat="server"></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="tbShipMemo" TextMode="MultiLine"
                                                    runat="server" MaxLength="30"></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="廠商上傳出貨日" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:Label ID="lbCreateTime" Text='<%# Eval("OrderShipCreateTime", "{0:yyyy-MM-dd HH:mm:ss}") %>' runat="server"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="最後修改日期" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:Label ID="lbModifyTime" Text='<%# Eval("OrderShipModifyTime", "{0:yyyy-MM-dd HH:mm:ss}") %>'
                                                    runat="server"></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:Label ID="lbModifyTime" Text='<%# Eval("OrderShipModifyTime", "{0:yyyy-MM-dd HH:mm:ss}") %>'
                                                    runat="server"></asp:Label>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="最後修改人" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:Label ID="lbModifyId" Text='<%# Bind("OrderShipModifyId") %>' runat="server"></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:Label ID="lbModifyId" Text='<%# Bind("OrderShipModifyId") %>' runat="server"></asp:Label>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="管理" ItemStyle-HorizontalAlign="Center">
                                            <EditItemTemplate>
                                                <asp:Button ID="btnUpdate" runat="server" CausesValidation="True" CommandName="Update"
                                                    Text="儲存"></asp:Button>
                                                &nbsp;<asp:Button ID="btnCancel" runat="server" CausesValidation="False" CommandName="Cancel"
                                                    Text="取消"></asp:Button>
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:Button ID="btnEdit" runat="server" CausesValidation="False" CommandName="Edit"
                                                    Text="編輯"></asp:Button>
                                                <asp:Button ID="btnDelete" runat="server" CausesValidation="False" CommandName="Delete"
                                                    OnClientClick="return confirm('確定刪除這筆出貨資訊?');"
                                                    Text="刪除"></asp:Button>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle BackColor="Gray" />
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                    <br />
                </asp:Panel>
                <%---------------------------------- end of 出貨資訊 ----------------------------------%>

                
                <%---------------------------------- 商品退貨紀錄 ----------------------------------%>
                <div id="HomeDeliveryReturnFormContainer" runat="server">
                    <table class="tbl-block" width="100%">
                        <thead>
                            <tr>
                                <th>商品退貨紀錄</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <table width="100%">
                                        <thead>
                                            <tr>
                                                <th>申請時間</th>
                                                <th>申請<br/>退貨</th>
                                                <th>退貨原因</th>
                                                <%if (config.EnabledRefundOrExchangeEditReceiverInfo && !IsCoupon) {%>
                                                <th>退貨<br/>收件人</th>
                                                <th>退貨地址</th>
                                                <%} %>
                                                <th>廠商<br/>處理時間</th>
                                                <th>廠商<br/>處理進度</th>
                                                <th>退貨進度</th>
                                                <th>完成<br/>退貨</th>
                                                <th>功能</th>
                                                <th>折讓單狀態</th>
                                                <th>結案狀態</th>
                                            </tr>
                                        </thead>
                                        <asp:ListView ID="lvHomeDeliveryReturnForms" runat="server" ItemPlaceholderID="Row"
                                            OnItemDataBound="lvHomeDeliveryReturnForms_ItemDataBound"
                                            OnItemCommand="lvHomeDeliveryReturnForms_ItemCommand" >
                                            <LayoutTemplate>
                                                <tbody>
                                                    <tr id="Row" runat="server"></tr>
                                                </tbody>
                                            </LayoutTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <td style="text-align: center;width:80px;">
                                                        <%--申請時間--%>
                                                        <asp:Literal runat="server" Text='<%# Eval("CreateTime", "{0:yyyy/MM/dd}") %>' />
                                                        <br />
                                                        <asp:Literal runat="server" Text='<%# Eval("CreateTime", "{0:HH:mm}") %>' />
                                                    </td>
                                                    <td style="text-align: center;width:40px;">
                                                        <%--申請退貨--%>
                                                        <asp:Literal ID="litItemSpec" runat="server" />
                                                    </td>
                                                    <td style="width:150px;">
                                                        <%--退貨原因--%>
                                                        <asp:Literal runat="server" Text='<%# Eval("ReturnReason") %>' />
                                                    </td>
                                                    <%if (config.EnabledRefundOrExchangeEditReceiverInfo && !IsCoupon) {%>
                                                    <td style="width:80px;">
                                                        <%--退貨收件人--%>
                                                        <asp:Literal runat="server" Text='<%# Eval("ReceiverName") %>' />
                                                    </td>
                                                    <td style="width:350px;">
                                                        <%--退貨地址--%>
                                                        <asp:Literal runat="server" Text='<%# Eval("ReceiverAddress") %>' />
                                                    </td>
                                                    <%} %>
                                                    <td style="text-align: center;width:80px;">
                                                        <%--廠商處理時間--%>
                                                        <asp:Literal runat="server" Text='<%# Eval("VendorProcessTime", "{0:yyyy/MM/dd}") %>' />
                                                        <br />
                                                        <asp:Literal runat="server" Text='<%# Eval("VendorProcessTime", "{0:HH:mm}") %>' />
                                                    </td>
                                                    <td style="text-align: center;width:70px;">
                                                        <%--廠商處理進度--%>
                                                        <asp:Literal ID="litVendorProgress" runat="server"></asp:Literal>
                                                        <asp:Button ID="btnUnreturnableProcessed" runat="server" Visible="False" CssClass="functionButton"
                                                            Text="已處理" CommandName="UnreturnableProcessed" CommandArgument='<%# Eval("Id") %>' />
                                                        <asp:Button ID="btnRecover" runat="server" Visible="False" CssClass="functionButton"
                                                            Text="復原" CommandName="Recover" CommandArgument='<%# Eval("Id") %>' />                                                    
                                                    </td>
                                                    <td style="text-align: center;width:80px">
                                                        <%--退貨進度--%>
                                                        <asp:Literal ID="litProgress" runat="server"></asp:Literal>
                                                        <input type="button" id="btnWmsRefund" value="發起逆物流" style="<%# ((int)Eval("ProgressStatus") == (int)ProgressStatus.ConfirmedForCS || (int)Eval("ProgressStatus") == (int)ProgressStatus.ConfirmedForUnArrival) ? "" : "display:none" %>" class="functionButton" data-id='<%# Eval("Id") %>' data-status='<%# (int)Eval("VendorProgressStatus") %>'   />

                                                    </td>
                                                    <td style="text-align: center;width:40px;">
                                                        <%--完成退貨--%>
                                                        <asp:Literal ID="litReturnedItemSpec" runat="server"></asp:Literal>
                                                    </td>
                                                    <td style="width:80px;">
                                                        <%--功能--%>
                                                        <input type="hidden" runat="server" id="hidRequireCreditNote" class="requireCreditNote" />
                                                        <asp:Button ID="btnCancel" runat="server" Visible="False" CssClass="functionButton"
                                                            OnClientClick="return confirm('確定取消退貨嗎? 若已完成退款，將一併回收款項。');"
                                                            Text="取消退貨" CommandName="CancelReturn" CommandArgument='<%# Eval("Id") %>' />
                                                        <asp:Button ID="btnUpdateAtmRefund" runat="server" Visible="False" CssClass="functionButton"
                                                            Text="已更新資料" CommandName="UpdateAtmRefund" CommandArgument='<%# Eval("Id") %>' />
                                                        <asp:Button ID="btnImmediateRefund" runat="server" Visible="False" CssClass="functionButton"
                                                            OnClientClick="if(!confirm('確定立即執行退貨嗎？')){return false;}; if($(this).parent().find('.requireCreditNote').val() == '1'){ alert('折讓單未回，請確認折讓單已收取，再進行退款作業'); return false;}"
                                                            Text="立即退貨" CommandName="ImmediateRefund" CommandArgument='<%# Eval("Id") %>' />
                                                        <asp:Button ID="btnScashToCash" runat="server" Visible="False" CssClass="functionButton"
                                                            OnClientClick="return confirm('退貨的購物金，將全數刷退或退ATM?');"
                                                            Text="購物金轉現金" CommandName="ScashToCash" CommandArgument='<%# Eval("Id") %>' />
                                                        <asp:Button ID="btnCashToSCash" runat="server" Visible="False" CssClass="functionButton"
                                                            OnClientClick="return confirm('確定轉退購物金?');"
                                                            Text="轉退購物金" CommandName="CashToSCash" CommandArgument='<%# Eval("Id") %>' />
                                                        <asp:Button ID="btnArtificialRefund" runat="server" Visible="False" CssClass="functionButton"
                                                            OnClientClick="return confirm('確定人工匯退?');"
                                                            Text="人工匯退" CommandName="ArtificialRefund" CommandArgument='<%# Eval("Id") %>' />
                                                        <asp:Button ID="btnRecoverToRefundScash" runat="server" Visible="False" CssClass="functionButton"
                                                            OnClientClick="return confirm('確定取消[退購物金轉刷退]狀態並回復為[退購物金]退貨已完成狀態?');"
                                                            Text="回復退購物金完成" CommandName="RecoverToRefundScash" CommandArgument='<%# Eval("Id") %>' />
                                                        <%if (config.EnabledRefundOrExchangeEditReceiverInfo && !IsCoupon) {%>
                                                        <asp:Button ID="btnEditReceiver" runat="server" Visible="False" CssClass="functionButton"
                                                            Text="編輯收件資訊" CommandName="EditReceiver" CommandArgument='<%# Eval("Id") %>' />
                                                        <%}%>
                                                    </td>
                                                    <td style="width:100px;">
                                                        <%--折讓單狀態--%>                                                        
                                                        <asp:Literal ID="litAllowanceDesc" runat="server"></asp:Literal><br />                                                        
                                                        <asp:Button ID="btnChangeToElcAllowance" runat="server" CssClass="functionButton"
                                                            OnClientClick="if (confirm('確認此訂單將使用電子折讓?')) { block_postback(); } else { return false; }"
                                                            Text="改用電子折讓" CommandName="ChangeToElcAllowance" CommandArgument='<%# Eval("Id") %>' />                                                     
                                                        <asp:Button ID="btnChangeToPaperAllowance" runat="server" CssClass="functionButton"
                                                            OnClientClick="if (confirm('確認此訂單將使用紙本折讓?')) { block_postback(); } else { return false; } "
                                                            Text="改用紙本折讓" CommandName="ChangeToPaperAllowance" CommandArgument='<%# Eval("Id") %>' /> 
                                                        <asp:Button ID="btnReceivedCreditNote" runat="server" Visible="False" CssClass="functionButton"
                                                            OnClientClick="if (confirm('確定本次申請的折讓單已回, 金額無誤?')) { block_postback(); } else { return false; }"
                                                            Text="折讓單已回" CommandName="ReceivedCreditNote" CommandArgument='<%# Eval("Id") %>' />   
                                                    </td>
                                                    <td style="text-align: center;width:80px">
                                                        <%--結案狀態--%>
                                                        <asp:Literal ID="litFinishDescription" runat="server"></asp:Literal>
                                                        <br />
                                                        <asp:Literal runat="server" Text='<%# Eval("FinishTime", "{0:yyyy/MM/dd HH:mm}") %>' />
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <tr>
                                                    <td style="text-align: center;width:80px;">
                                                        <%--申請時間--%>
                                                        <asp:Literal runat="server" Text='<%# Eval("CreateTime", "{0:yyyy/MM/dd}") %>' />
                                                        <br />
                                                        <asp:Literal runat="server" Text='<%# Eval("CreateTime", "{0:HH:mm}") %>' />
                                                    </td>
                                                    <td style="text-align: center;width:40px;">
                                                        <%--申請退貨--%>
                                                        <asp:Literal ID="litItemSpec" runat="server" />
                                                    </td>
                                                    <td style="width:auto;">
                                                        <%--退貨原因--%>
                                                        <asp:Literal runat="server" Text='<%# Eval("ReturnReason") %>' />
                                                    </td>
                                                    <%if (config.EnabledRefundOrExchangeEditReceiverInfo && !IsCoupon) {%>
                                                    <td style="width:80px;">
                                                        <%--退貨收件人--%>
                                                        <asp:TextBox ID="txtReceiverNameR" Text='<%# Eval("ReceiverName") %>' width="150px"
                                                            CssClass="txtReceiverNameR input-2over3 input-full" runat="server"  />
                                                    </td>
                                                    <td style="width:350px;">
                                                        <%--退貨地址--%>
                                                        <asp:TextBox ID="txtReceiverAddressR" Text='<%# Eval("ReceiverAddress") %>' width="350px"
                                                            CssClass="txtReceiverAddressR input-full" runat="server"  />
                                                    </td>
                                                    <%} %>
                                                    <td style="text-align: center;width:80px;">
                                                        <%--廠商處理時間--%>
                                                        <asp:Literal runat="server" Text='<%# Eval("VendorProcessTime", "{0:yyyy/MM/dd}") %>' />
                                                        <br />
                                                        <asp:Literal runat="server" Text='<%# Eval("VendorProcessTime", "{0:HH:mm}") %>' />
                                                    </td>
                                                    <td style="text-align: center;width:70px;">
                                                        <%--廠商處理進度--%>
                                                        <asp:Literal ID="litVendorProgress" runat="server"></asp:Literal>
                                                    </td>
                                                    <td style="text-align: center;width:80px">
                                                        <%--退貨進度--%>
                                                        <asp:Literal ID="litProgress" runat="server"></asp:Literal>
                                                    </td>
                                                    <td style="text-align: center;width:40px;">
                                                        <%--完成退貨--%>
                                                        <asp:Literal ID="litReturnedItemSpec" runat="server"></asp:Literal>
                                                    </td>
                                                    <td style="width:80px;">
                                                        <%--功能--%>
                                                        <input type="hidden" runat="server" id="hidRequireCreditNote" class="requireCreditNote" />
                                                        <asp:Button ID="btnUpdateReceiver" runat="server" Visible="True" CssClass="functionButton" Text="更新" 
                                                            CommandName="UpdateReceiver" CommandArgument='<%# Eval("Id") %>' />
                                                        <asp:Button ID="btnCancelEditReceiver" runat="server" Visible="True" CssClass="functionButton" Text="取消" 
                                                            CommandName="CancelReceiver" CommandArgument='<%# Eval("Id") %>' />
                                                    </td>
                                                    <td style="width:100px;">
                                                        <%--折讓單狀態--%>                                                        
                                                        <asp:Literal ID="litAllowanceDesc" runat="server"></asp:Literal><br />                                                        
                                                    </td>
                                                    <td style="text-align: center;width:80px">
                                                        <%--結案狀態--%>
                                                        <asp:Literal ID="litFinishDescription" runat="server"></asp:Literal>
                                                        <br />
                                                        <asp:Literal runat="server" Text='<%# Eval("FinishTime", "{0:yyyy/MM/dd HH:mm}") %>' />
                                                    </td>
                                                </tr>
                                            </EditItemTemplate>
                                        </asp:ListView>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <br />
                </div>
                
                <%---------------------------------- end of 商品退貨紀錄 ----------------------------------%>
                

                <%---------------------------------- 商品換貨紀錄 ----------------------------------%>
                <asp:Panel ID="divExchangeInfo" Width="100%" runat="server"  Visible="false">
                    <table class="tbl-block" width="100%">
                        <thead>
                            <tr>
                                <th>商品換貨紀錄</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <table width="100%">
                                        <thead>
                                            <tr>
                                                <th>申請時間</th>
                                                <th>換貨原因</th>
                                                <%if (config.EnabledRefundOrExchangeEditReceiverInfo && !IsCoupon) {%>
                                                <th>換貨<br/>收件人</th>
                                                <th>換貨地址</th>
                                                <%} %>
                                                <th>廠商<br/>處理時間</th>
                                                <th>廠商<br/>處理進度</th>
                                                <th>功能</th>
                                                <th>結案狀態</th>
                                            </tr>
                                        </thead>

                                        <asp:ListView ID="lvExchangeInfo" runat="server" ItemPlaceholderID="Row"
                                            OnItemDataBound="lvExchangeInfo_ItemDataBound"
                                            OnItemCommand="lvExchangeInfo_ItemCommand">
                                            <LayoutTemplate>
                                                <tbody>
                                                    <tr id="Row" runat="server"></tr>
                                                </tbody>
                                            </LayoutTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <td style="text-align: center;width:80px;">
                                                        <%--申請時間--%>
                                                        <asp:Literal runat="server" Text='<%# Eval("CreateTime", "{0:yyyy/MM/dd}") %>' />
                                                        <br />
                                                        <asp:Literal runat="server" Text='<%# Eval("CreateTime", "{0:HH:mm}") %>' />
                                                    </td>
                                                    <td style="width:300px;font-size:11px">
                                                        <%--換貨原因--%>
                                                        <asp:Literal ID="litReason" runat="server" />
                                                    </td>
                                                    <%if (config.EnabledRefundOrExchangeEditReceiverInfo && !IsCoupon) {%>
                                                    <td style="width:80px;">
                                                        <%--換貨收件人--%>
                                                        <asp:Literal runat="server" Text='<%# Eval("ReceiverName") %>' />
                                                    </td>
                                                    <td style="width:350px;">
                                                        <%--換貨地址--%>
                                                        <asp:Literal runat="server" Text='<%# Eval("ReceiverAddress") %>' />
                                                    </td>
                                                    <%} %>
                                                    <td style="text-align: center;width:80px;">
                                                        <%--廠商處理時間--%>
                                                        <asp:Literal runat="server" Text='<%# Eval("VendorProcessTime", "{0:yyyy/MM/dd}") %>' />
                                                        <br />
                                                        <asp:Literal runat="server" Text='<%# Eval("VendorProcessTime", "{0:HH:mm}") %>' />
                                                    </td>
                                                    <td style="text-align: center;width:70px;">
                                                        <%--廠商處理進度--%>
                                                        <asp:Literal ID="litVendorProgress" runat="server"></asp:Literal>
                                                    </td>
                                                    <td style="width:80px;">
                                                        <%--功能--%>
                                                        <asp:Button ID="btnExchangeMessage" runat="server" CssClass="functionButton"
                                                            OnClientClick='<%# "openExchangeMessage(" + Eval("Id") + ");" %>'
                                                            Text="訊息更新" />
                                                        <asp:Button ID="btnCompleteExchange" runat="server" Visible="False" CssClass="functionButton"
                                                            OnClientClick="return confirm('確定完成換貨嗎？');"
                                                            Text="確認完成" CommandName="CompleteExchange" CommandArgument='<%# Eval("Id") %>' />
                                                        <asp:Button ID="btnCancelExchange" runat="server" Visible="True" CssClass="functionButton"
                                                            OnClientClick="return confirm('確定取消換貨嗎?');"
                                                            Text="確認取消" CommandName="CancelExchange" CommandArgument='<%# Eval("Id") %>' />
                                                        <asp:Button ID="btnUpdateOrderShip" runat="server" Visible="False" CssClass="functionButton"
                                                            OnClientClick='<%# "openOrderShip(" +  Eval("Id") + ");" %>'
                                                            Text="出貨資訊" />
                                                        <%if (config.EnabledRefundOrExchangeEditReceiverInfo && !IsCoupon) {%>
                                                        <asp:Button ID="btnEditExchangeReceiver" runat="server" Visible="True" CssClass="functionButton"
                                                            Text="編輯收件資訊" CommandName="EditReceiver" CommandArgument='<%# Eval("Id") %>'/>
                                                        <%}%>
                                                    </td>
                                                    <td style="text-align: center;width:80px">
                                                        <%--結案狀態--%>
                                                        <asp:Literal ID="litStatus" runat="server"></asp:Literal>
                                                        <br />
                                                        <asp:Literal ID="litFinishTime" runat="server" />
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <tr>
                                                    <td style="text-align: center;width:80px;">
                                                        <%--申請時間--%>
                                                        <asp:Literal runat="server" Text='<%# Eval("CreateTime", "{0:yyyy/MM/dd}") %>' />
                                                        <br />
                                                        <asp:Literal runat="server" Text='<%# Eval("CreateTime", "{0:HH:mm}") %>' />
                                                    </td>
                                                    <td style="width:300px;font-size:11px">
                                                        <%--換貨原因--%>
                                                        <asp:Literal ID="litReason" runat="server" />
                                                    </td>
                                                    <%if (config.EnabledRefundOrExchangeEditReceiverInfo && !IsCoupon) {%>
                                                    <td style="width:80px;">
                                                        <%--換貨收件人--%>
                                                        <asp:TextBox ID="txtReceiverNameE" Text='<%# Eval("ReceiverName") %>' width="150px"
                                                            CssClass="txtReceiverNameE input-2over3 input-full" runat="server"  />
                                                    </td>
                                                    <td style="width:350px;">
                                                        <%--換貨地址--%>
                                                        <asp:TextBox ID="txtReceiverAddressE" Text='<%# Eval("ReceiverAddress") %>' width="350px"
                                                            CssClass="txtReceiverNameE input-full" runat="server"  />
                                                    </td>
                                                    <%} %>
                                                    <td style="text-align: center;width:80px;">
                                                        <%--廠商處理時間--%>
                                                        <asp:Literal runat="server" Text='<%# Eval("VendorProcessTime", "{0:yyyy/MM/dd}") %>' />
                                                        <br />
                                                        <asp:Literal runat="server" Text='<%# Eval("VendorProcessTime", "{0:HH:mm}") %>' />
                                                    </td>
                                                    <td style="text-align: center;width:70px;">
                                                        <%--廠商處理進度--%>
                                                        <asp:Literal ID="litVendorProgress" runat="server"></asp:Literal>
                                                    </td>
                                                    <td style="width:80px;">
                                                        <%--功能--%>
                                                        <asp:Button ID="btnUpdateExchangeReceiver" runat="server" Visible="True" CssClass="functionButton"
                                                            Text="更新" CommandName="UpdateReceiver" CommandArgument='<%# Eval("Id") %>'/>
                                                        <asp:Button ID="btnCancelExchangeReceiver" runat="server" Visible="True" CssClass="functionButton"
                                                            Text="取消" CommandName="CancelReceiver" CommandArgument='<%# Eval("Id") %>'/>
                                                    </td>
                                                    <td style="text-align: center;width:80px">
                                                        <%--結案狀態--%>
                                                        <asp:Literal ID="litStatus" runat="server"></asp:Literal>
                                                        <br />
                                                        <asp:Literal ID="litFinishTime" runat="server" />
                                                    </td>
                                                </tr>
                                            </EditItemTemplate>
                                        </asp:ListView>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <table width="100%" style="border: double 3px black">
                        <tr>
                            <th style="background-color: Gray">換貨紀錄</th>
                        </tr>
                        <tr>
                            <td>
                                <asp:Repeater ID="rpt_RefundLog" runat="server">
                                    <HeaderTemplate>
                                        <hr />
                                        <table style="width: 100%">
                                            <tr>
                                                <td style="background-color: #507CD1; color: White; width: 15%">建立時間</td>
                                                <td style="background-color: #507CD1; color: White; width: 15%">結案狀態</td>
                                                <td style="background-color: #507CD1; color: White; width: 15%">廠商處理進度</td>
                                                <td style="background-color: #507CD1; color: White; width: 40%">訊息</td>
                                                <td style="background-color: #507CD1; color: White; width: 15%">建立者</td>
                                            </tr>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td style="background-color: #EFF3FB;width: 15%;">
                                                <%# ((OrderStatusLog)(Container.DataItem)).CreateTime.ToString("yyyy/MM/dd HH:mm")  %>
                                            </td>
                                            <td style="background-color: #EFF3FB;width: 15%;">
                                                <%# GetOrderStatusLog(((OrderStatusLog)(Container.DataItem)).Status)%>
                                            </td>
                                            <td style="background-color: #EFF3FB;width: 15%;">
                                                <%# GetExchangeVendorProgressStatusDesc(((OrderStatusLog)(Container.DataItem)).VendorProgressStatus)%>
                                            </td>
                                            <td style="background-color: #EFF3FB;width: 40%;">
                                                <%# ((OrderStatusLog)(Container.DataItem)).Message %>
                                            </td>
                                            <td style="background-color: #EFF3FB;width: 15%;">
                                                <%# ((OrderStatusLog)(Container.DataItem)).CreateId %>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </td>
                        </tr>
                    </table>
                    <br />
                </asp:Panel>
                <%---------------------------------- end of 商品換貨紀錄 ----------------------------------%>
                

                <%----------------------- 憑證退貨紀錄 ----------------------------%>
                <div id="CouponReturnFormContainer" runat="server">
                    <table class="tbl-block" width="100%">
                        <thead>
                            <tr>
                                <th>憑證退貨紀錄</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <table>
                                        <thead>
                                            <tr>
                                                <th>申請時間</th>
                                                <th>申請退貨</th>
                                                <th>退貨原因</th>
                                                <th>處理時間</th>
                                                <th>退貨進度</th>
                                                <th>完成退貨</th>
                                                <th>功能</th>
                                                <th>折讓單狀態</th>
                                                <th>結案狀態</th>
                                            </tr>
                                        </thead>
                                        <asp:ListView ID="lvCouponReturnForms" runat="server" ItemPlaceholderID="Row"
                                            OnItemDataBound="lvCouponReturnForms_ItemDataBound"
                                            OnItemCommand="lvCouponReturnForms_ItemCommand">
                                            <LayoutTemplate>
                                                <tbody>
                                                    <tr id="Row" runat="server"></tr>
                                                </tbody>
                                            </LayoutTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <td style="text-align: center;">
                                                        <%--申請時間--%>
                                                        <asp:Literal runat="server" Text='<%# Eval("CreateTime", "{0:yyyy/MM/dd}") %>' />
                                                        <br />
                                                        <asp:Literal runat="server" Text='<%# Eval("CreateTime", "{0:HH:mm}") %>' />
                                                    </td>
                                                    <td>
                                                        <%--申請退貨--%>
                                                        <asp:Literal ID="litItemSpec" runat="server" />
                                                    </td>
                                                    <td>
                                                        <%--退貨原因--%>
                                                        <asp:Literal runat="server" Text='<%# Eval("ReturnReason") %>' />
                                                    </td>
                                                    <td style="text-align: center;">
                                                        <%--處理時間--%>
                                                        <asp:Literal runat="server" Text='<%# Eval("LastProcessingTime", "{0:yyyy/MM/dd}") %>' />
                                                        <br />
                                                        <asp:Literal runat="server" Text='<%# Eval("LastProcessingTime", "{0:HH:mm}") %>' />
                                                    </td>
                                                    <td style="text-align: center;">
                                                        <%--退貨進度--%>
                                                        <asp:Literal ID="litProgress" runat="server"></asp:Literal>
                                                    </td>
                                                    <td>
                                                        <%--完成退貨--%>
                                                        <asp:Literal ID="litReturnedItemSpec" runat="server"></asp:Literal>
                                                    </td>
                                                    <td>
                                                        <%--功能--%>
                                                        <input type="hidden" runat="server" id="hidRequireCreditNote" class="requireCreditNote" />
                                                        <asp:Button ID="btnCancel" runat="server" Visible="False" CssClass="functionButton"
                                                            OnClientClick="return confirm('確定取消退貨嗎? 若已完成退款，將一併回收款項。');"
                                                            Text="取消退貨" CommandName="CancelReturn" CommandArgument='<%# Eval("Id") %>' />
                                                        <asp:Button ID="btnUpdateAtmRefund" runat="server" Visible="False" CssClass="functionButton"
                                                            Text="已更新資料" CommandName="UpdateAtmRefund" CommandArgument='<%# Eval("Id") %>' />
                                                        <asp:Button ID="btnImmediateRefund" runat="server" Visible="False" CssClass="functionButton"
                                                            OnClientClick="if(!confirm('確定立即執行退貨嗎？')){return false;}; if($(this).parent().find('.requireCreditNote').val() == '1'){alert('折讓單未回，請確認折讓單已收取，再進行退款作業'); return false;}"
                                                            Text="立即退貨" CommandName="ImmediateRefund" CommandArgument='<%# Eval("Id") %>' />
                                                        <asp:Button ID="btnScashToCash" runat="server" Visible="False" CssClass="functionButton"
                                                            OnClientClick="return confirm('退貨的購物金，將全數刷退或退ATM?');"
                                                            Text="購物金轉現金" CommandName="ScashToCash" CommandArgument='<%# Eval("Id") %>' />
                                                        <asp:Button ID="btnCashToSCash" runat="server" Visible="False" CssClass="functionButton"
                                                            OnClientClick="return confirm('確定轉退購物金?');"
                                                            Text="轉退購物金" CommandName="CashToSCash" CommandArgument='<%# Eval("Id") %>' />
                                                        <asp:Button ID="btnArtificialRefund" runat="server" Visible="False" CssClass="functionButton"
                                                            OnClientClick="return confirm('確定人工匯退?');"
                                                            Text="人工匯退" CommandName="ArtificialRefund" CommandArgument='<%# Eval("Id") %>' />
                                                        <asp:Button ID="btnRecoverToRefundScash" runat="server" Visible="False" CssClass="functionButton"
                                                            OnClientClick="return confirm('確定取消[退購物金轉刷退]狀態並回復為[退購物金]退貨已完成狀態?');"
                                                            Text="回復退購物金完成" CommandName="RecoverToRefundScash" CommandArgument='<%# Eval("Id") %>' />
                                                    </td>
                                                    <td>
                                                        <%--折讓單狀態--%>
                                                        <asp:Literal ID="litAllowanceDesc" runat="server"></asp:Literal><br />
                                                        <asp:Button ID="btnChangeToElcAllowance" runat="server" CssClass="functionButton"
                                                            OnClientClick="if (confirm('確認此訂單將使用電子折讓?')) { block_postback(); } else { return false; }"
                                                            Text="改用電子折讓" CommandName="ChangeToElcAllowance" CommandArgument='<%# Eval("Id") %>' />
                                                        <asp:Button ID="btnChangeToPaperAllowance" runat="server" CssClass="functionButton"
                                                            OnClientClick="if (confirm('確認此訂單將使用紙本折讓?')) { block_postback(); } else { return false; }"
                                                            Text="改用紙本折讓" CommandName="ChangeToPaperAllowance" CommandArgument='<%# Eval("Id") %>' /> 
                                                        <asp:Button ID="btnReceivedCreditNote" runat="server" Visible="False" CssClass="functionButton"
                                                            OnClientClick="if (confirm('確定本次申請的折讓單已回, 金額無誤?')) { block_postback(); } else { return false; }"
                                                            Text="折讓單已回" CommandName="ReceivedCreditNote" CommandArgument='<%# Eval("Id") %>' />
                                                    </td>
                                                    <td style="text-align: center;">
                                                        <%--結案狀態--%>
                                                        <asp:Literal ID="litFinishDescription" runat="server"></asp:Literal>
                                                        <br />
                                                        <asp:Literal runat="server" Text='<%# Eval("FinishTime", "{0:yyyy/MM/dd HH:mm}") %>' />
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:ListView>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <br />
                </div>
                <%----------------------- end of 憑證退貨紀錄 ----------------------------%>
                

                <%----------------------- 新增客服紀錄 ----------------------------%>
                <asp:Panel ID="divAddServiceLog" runat="server" Width="658px">
                    <asp:Label ID="lblMessage" runat="server" ForeColor="Red" />
                    <table width="100%">
                        <tr>
                            <td align="center">
                                <table width="100%" style="border: double 3px black">
                                    <tr>
                                        <td style="background-color: Gray" colspan="4">新增客服紀錄
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right" class="style13">姓名
                                        </td>
                                        <td align="left">
                                            <asp:Label ID="lblIName" runat="server"></asp:Label>
                                        </td>
                                        <td align="right" class="style13">Email
                                        </td>
                                        <td align="left">
                                            <asp:Label ID="lblIEmail" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right" class="style13">電話
                                        </td>
                                        <td align="left">
                                            <asp:Label ID="lblIPhone" runat="server"></asp:Label>
                                        </td>
                                        <td align="right" class="style13">OrderId
                                        </td>
                                        <td align="left">
                                            <asp:Label ID="lblIOrder" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right" class="style13">形式
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlItype" runat="server">
                                                <asp:ListItem Selected="True">來電</asp:ListItem>
                                                <asp:ListItem>外撥</asp:ListItem>
                                                <asp:ListItem>去信</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td align="right" class="style13">分類
                                        </td>
                                        <td align="left">
                                            <asp:DropDownList ID="ddlCategory" runat="server" onchange="buildSubCategory()">
                                                <asp:ListItem>請選擇</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:DropDownList ID="ddlSubCategory" runat="server" onchange="setSubCategory()">
                                                <asp:ListItem>請選擇</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:TextBox ID="hdSubCategory" runat="server" htmlreadonly="true" Width="30px" Text="請選擇:"
                                                Style="display: none;" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right" class="style13">狀態
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlIStatus" runat="server">
                                                <asp:ListItem Value="wait">待處理</asp:ListItem>
                                                <asp:ListItem Value="process">處理中</asp:ListItem>
                                                <asp:ListItem Value="complete">完成</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td colspan="2">
                                            <asp:HiddenField ID="HiddenOrderGuid" runat="server" />
                                            <asp:HiddenField ID="HiddenAddType" Value="0" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">轉件項目
                                        </td>
                                        <td align="left" colspan="3">
                                            <asp:RadioButtonList ID="rdlWorkType" runat="server" RepeatDirection="Horizontal">
                                                <asp:ListItem Text="商品" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="好康" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="退貨" Value="2"></asp:ListItem>
                                            </asp:RadioButtonList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">緊急程度
                                        </td>
                                        <td align="left" colspan="3">
                                            <asp:RadioButtonList ID="rblPriority" runat="server" RepeatDirection="Horizontal">
                                            </asp:RadioButtonList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right" class="style13">問題說明
                                        </td>
                                        <td align="left" colspan="3">
                                            <asp:TextBox ID="txtIMessage" runat="server" TextMode="MultiLine" Height="130px"
                                                Width="520px"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right" class="style13">回報廠商備註
                                        </td>
                                        <td align="left" colspan="3">
                                            <asp:TextBox ID="txtRemarkSeller" runat="server" TextMode="MultiLine" Height="50px"
                                                Width="520px"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" align="center" style="text-align: center">
                                            <asp:Button ID="btnSetServiceMsg" runat="server" Text="確認" OnClick="btnSetServiceMsg_Click"
                                                OnClientClick="return CheckData();" />
                                            <asp:Button ID="btnAddServiceMsgClose" runat="server" Text="退出" OnClick="btnAddServiceMsgClose_Click"
                                                OnClientClick="btnAddServiceMsgClose_Click();" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <br />
                </asp:Panel>
                <%----------------------- end of 新增客服紀錄 ----------------------------%>
                
                
                <%----------------------- ATM申請退款受款帳戶資料 ----------------------------%>
                <asp:Panel ID="pnATM" runat="server" Visible="false">
                    <table width="100%" style="border: double 3px black">
                        <tr>
                            <th style="background-color: Gray">ATM申請退款受款帳戶資料
                            </th>
                        </tr>
                        <tr>
                            <td>序號：<asp:Label ID="lbSi" runat="server" Text=""></asp:Label>&nbsp;&nbsp; 戶名：<asp:TextBox
                                ID="tbAcountName" runat="server"></asp:TextBox>&nbsp;&nbsp; 身分證字號或統編：<asp:TextBox
                                    ID="tbIdcardNo" runat="server" MaxLength="10"></asp:TextBox>
                            </td>
                        </tr>
                        <tr id="divBankInfo">
                            <td style="position: relative;">銀行名稱：<asp:TextBox ID="txtBankName" runat="server" AutoCompleteType="None"></asp:TextBox><asp:Label ID="labBankNameErr" runat="server" ForeColor="red"></asp:Label>
                                <div class="suggestionBox" style="display: none">
                                    <div class="suggestionList">
                                        <ul></ul>
                                    </div>
                                </div>

                                <br />
                                分行別：<asp:TextBox ID="txtBankBranch" runat="server" AutoCompleteType="None"></asp:TextBox><asp:Label ID="labBankBranchErr" runat="server" ForeColor="red"></asp:Label>
                                <div class="suggestionBox2" style="display: none">
                                    <div class="suggestionList2">
                                        <ul></ul>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>帳號：<asp:TextBox ID="tbAccountNo" runat="server" MaxLength="20"></asp:TextBox>&nbsp;&nbsp;
                                手機：<asp:TextBox
                                    ID="tbMobile" runat="server"></asp:TextBox>&nbsp;&nbsp;
                                        <asp:Button ID="btSaveAtm" runat="server" Text="儲存" OnCommand="btSaveAtm_Click" />
                                <script type="text/javascript">
                                    Sys.Application.add_load(openATM);
                                </script>
                        </tr>
                        <tr>
                            <td>
                                <asp:GridView ID="gvAtmRefund" runat="server" AutoGenerateColumns="False" Width="100%">
                                    <Columns>
                                        <asp:BoundField DataField="Si" HeaderText="序號" />
                                        <asp:BoundField DataField="Amount" HeaderText="金額" />
                                        <asp:BoundField DataField="AccountName" HeaderText="戶名" />
                                        <asp:BoundField DataField="Id" HeaderText="證號" />
                                        <asp:BoundField DataField="BankName" HeaderText="銀行" />
                                        <asp:BoundField DataField="BranchName" HeaderText="分行" />
                                        <asp:BoundField DataField="AccountNumber" HeaderText="帳號" />
                                        <asp:BoundField DataField="Phone" HeaderText="電話" />
                                        <asp:BoundField DataField="CreateTime" HeaderText="建立時間" />
                                        <asp:TemplateField HeaderText="狀態">
                                            <ItemTemplate>
                                                <asp:Label ID="lbAtmRStatus" runat="server" Text='<%# GetAtmRStatus(Eval("Status").ToString(), Eval("FailReason") != null ? Eval("FailReason").ToString() : string.Empty) %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                    <br />
                </asp:Panel>
                <%----------------------- end of ATM申請退款受款帳戶資料 ----------------------------%>
                
                
                <%----------------------- 記錄 ----------------------------%>
                <table  width="100%"  style="border: double 3px black">
                    <tr>
                        <th style="background-color: Gray">記錄</th>
                    </tr>
                    <tr>
                        <td>
                            <uc2:AuditBoard ID="ab" runat="server" Width="100%" EnableAdd="true" ShowAllRecords="true"
                                ReferenceType="Order" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Repeater ID="rpt_RefundLog_old" runat="server">
                                <HeaderTemplate>
                                    <hr />
                                    <table style="width: 100%">
                                        <tr>
                                            <td style="background-color: #507CD1; color: White; width: 80px;">換貨建立時間
                                            </td>
                                            <td style="background-color: #507CD1; color: White; width: 30%">狀態
                                            </td>
                                            <td style="background-color: #507CD1; color: White;">訊息
                                            </td>
                                        </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td style="background-color: #EFF3FB;">
                                            <%# ((OrderStatusLog)(Container.DataItem)).CreateTime.ToString("yyyy/MM/dd HH:mm")  %>
                                        </td>
                                        <td style="background-color: #EFF3FB;">
                                            <%# GetOrderStatusLog(((OrderStatusLog)(Container.DataItem)).Status)%>
                                        </td>
                                        <td style="background-color: #EFF3FB;">
                                            <%# ((OrderStatusLog)(Container.DataItem)).Message %>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <%----------------------- 清冊匯出時間 ----------------------------%>
                            <asp:Repeater ID="rpt_ExportLog" runat="server">
                                <HeaderTemplate>
                                    <hr />
                                    <table style="width: 100%">
                                        <tr>
                                            <td style="background-color: #507CD1; color: White; width: 30%">清冊匯出時間
                                            </td>
                                            <td style="background-color: #507CD1; color: White;">操作人
                                            </td>
                                        </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td style="background-color: #EFF3FB;">
                                            <%# ((ExportLog)(Container.DataItem)).CreateTime%>
                                        </td>
                                        <td style="background-color: #EFF3FB;">
                                            <%# ((ExportLog)(Container.DataItem)).Name%>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                            <%----------------------- end of 清冊匯出時間 ----------------------------%>
                        </td>
                    </tr>
                </table>
                <%----------------------- end of 記錄 ----------------------------%>

                <br />
                
                <asp:UpdatePanel ID="upOD" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="False" width="100%">
                    <ContentTemplate>
                        <br />
                        <asp:GridView ID="gvOrderDetail" runat="server" AutoGenerateColumns="False" BackColor="White"
                            BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px" CellPadding="4" EmptyDataText="無符合的資料"
                            ForeColor="Black" GridLines="Vertical" DataKeyNames="OrderDetailGuid" Font-Size="Small"
                            Width="500px" OnRowCancelingEdit="gvOrderDetail_RowCancelingEdit" OnRowEditing="gvOrderDetail_RowEditing"
                            OnRowUpdating="gvOrderDetail_RowUpdating" OnRowDeleting="gvOrderDetail_RowDeleting"
                            ShowFooter="True" OnRowDataBound="gvOrderDetail_RowDataBound">
                            <FooterStyle BackColor="#CCCC99" />
                            <Columns>
                                <asp:TemplateField HeaderText="項次">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="Label1" runat="server"></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle Wrap="False" />
                                    <ItemStyle HorizontalAlign="Right" />
                                </asp:TemplateField>
                                <asp:HyperLinkField DataNavigateUrlFields="OrderGUID,OrderDetailGuid" DataNavigateUrlFormatString="./order_detail.aspx?oid={0}&amp;odid={1}"
                                    DataTextField="ItemName" HeaderText="檔次名稱">
                                    <ItemStyle Wrap="True" Width="500px" />
                                    <HeaderStyle Wrap="False" Width="500px" />
                                    <ControlStyle Width="300px" />
                                    <FooterStyle Width="300px" />
                                </asp:HyperLinkField>
                                <asp:BoundField DataField="ItemUnitPrice" HeaderText="單價" ReadOnly="True" DataFormatString="{0:N0}">
                                    <ItemStyle Wrap="False" HorizontalAlign="Right" />
                                    <HeaderStyle Wrap="False" />
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="數量">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextItemQuantity" runat="server" Text='<%# Bind("ItemQuantity") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="LabelItemQuantity" runat="server" Text='<%# Bind("ItemQuantity") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle Wrap="False" />
                                    <ItemStyle Wrap="False" HorizontalAlign="Right" />
                                    <ControlStyle Width="40px" />
                                    <FooterStyle HorizontalAlign="Right" />
                                </asp:TemplateField>
                                <asp:BoundField DataField="OrderDetailTotal" DataFormatString="{0:N0}" HeaderText="總額"
                                    HtmlEncode="False"
                                    ReadOnly="True">
                                    <ItemStyle HorizontalAlign="Right" Wrap="False" />
                                    <HeaderStyle Wrap="False" />
                                </asp:BoundField>
                                <asp:BoundField DataField="ConsumerName" HeaderText="買家" ReadOnly="True">
                                    <HeaderStyle Wrap="False" />
                                    <ItemStyle Wrap="False" />
                                </asp:BoundField>
                                <asp:BoundField DataField="ConsumerTeleExt" HeaderText="買家分機" ReadOnly="True">
                                    <HeaderStyle Wrap="False" />
                                    <ItemStyle Wrap="False" />
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="編輯" ShowHeader="False">
                                    <ItemTemplate>
                                        <asp:Button ID="Button1" runat="server" CausesValidation="False" CommandName="Edit"
                                            Text="編輯" />
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:Button ID="Button2" runat="server" CausesValidation="False" CommandName="Update"
                                            Text="更新" OnClientClick="return confirm('確定更新這筆記錄?');" />
                                        <asp:Button ID="Button3" runat="server" CausesValidation="False" CommandName="Cancel"
                                            Text="取消" />
                                    </EditItemTemplate>
                                    <ItemStyle Wrap="False" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="刪除" ShowHeader="False">
                                    <ItemTemplate>
                                        <asp:Button ID="Button4" runat="server" CausesValidation="False" CommandName="Delete"
                                            Text="刪除" OnClientClick="return confirm('確定刪除這筆記錄?');" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <RowStyle BackColor="#F7F7DE" />
                            <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                            <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                            <AlternatingRowStyle BackColor="White" />
                        </asp:GridView>
                        &nbsp;
                    </ContentTemplate>
                </asp:UpdatePanel>

            </asp:Panel>

        </ContentTemplate>
    </asp:UpdatePanel>

    <div id="PopFreezeReason" class="modalPopup" style="display: none; width: 658px;">
        <asp:Panel ID="Panel3" runat="server" Width="658px" Style="cursor: move; background-color: #DDDDDD; border: solid 1px Gray; color: Black">
            <fieldset>
                <legend>凍結請款原因</legend>
                <table width="100%">
                    <tr>
                        <td align="center">
                            <table>
                                <tr>
                                    <td align="left">
                                        <asp:TextBox ID="txtFreezeReason" runat="server" TextMode="MultiLine" Height="210px"
                                            Width="520px"></asp:TextBox>
                                        <asp:TextBox ID="txtFreezeType" runat="server" TextMode="SingleLine" Style="display: none"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" align="center" style="text-align: center">
                                        <asp:Button ID="btnFreezeReason" runat="server" Text="確認" OnClick="btnSetFreeze_Click"
                                            OnClientClick="return CheckFreezeReason();" />

                                        <asp:Button ID="btnCancelFreezeReason" runat="server" Text="退出" OnClientClick="return hideFreezeReason();" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </fieldset>
        </asp:Panel>
    </div>
    <div id="InvLog" class="divScrollbar" style="display: none; width: 700px;">
        <% LunchKingSite.WebLib.Component.WebFormMVCUtil.RenderAction("~/Views/Order/OrderDetailEinvoiceChangeLog.cshtml", GetModel(this.OrderId)); %>
        <div class="MGS-XX" onclick="unblock_ui();" style="cursor: pointer; background: url(../../Themes/PCweb/images/A2-Multi-grade-Setting_xx.png); height: 28px; width: 27px; position: absolute; top: -24px; right: -25px; z-index: 800;">
        </div>
    </div>
</asp:Content>



﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.Web.ControlRoom.Controls;
using LunchKingSite.I18N;
using LunchKingSite.WebLib.Component;


public partial class order_shipstatus : RolePage, IOrderShipStatusView
{
    #region event
    public event EventHandler<DataEventArgs<Guid>> SearchPponOrderList;
    public event EventHandler<DataEventArgs<KeyValuePair<OrderStatusLog, int>>> UpdateShipOrderStatus;
    public event EventHandler<DataEventArgs<int>> PageChange;
    #endregion
    #region property
    private OrderShipStatusPresenter _presenter;
    public OrderShipStatusPresenter Presenter
    {
        set
        {
            this._presenter = value;
            if (value != null)
            {
                this._presenter.View = this;
            }
        }
        get
        {
            return this._presenter;
        }
    }
    public string UserName
    {
        get { return this.Page.User.Identity.Name; }
    }
    public int PageCount
    {
        get
        {
            if (hif_PageCount.Value != string.Empty)
            {
                int pagecount;
                return int.TryParse(hif_PageCount.Value, out pagecount) ? pagecount : 1;
            }
            else
                return 1;
        }
        set
        {
            hif_PageCount.Value = value.ToString();
        }
    }
    public Guid? Bid
    {
        get
        {
            Guid bid;
            if (Guid.TryParse(hif_Bid.Value, out bid))
                return bid;
            else
                return null;
        }
        set
        {
            hif_Bid.Value = value.ToString();
        }
    }
    public int CurrentPage
    {
        get
        {
            if (hif_CurrentPage.Value != string.Empty)
            {
                int currentpage;
                return int.TryParse(hif_CurrentPage.Value, out currentpage) ? currentpage : 1;
            }
            else
                return 1;
        }
        set
        {
            hif_CurrentPage.Value = value.ToString();
        }
    }
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {

        if (!Page.IsPostBack)
            this.Presenter.OnViewInitialized();
        this.Presenter.OnViewLoaded();
    }

    public void gvDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.DataItem is ClassForViewPponOrder_OrderStatusLog)
        {
            ClassForViewPponOrder_OrderStatusLog data = (ClassForViewPponOrder_OrderStatusLog)e.Row.DataItem;
            OrderMemo memo = new OrderMemo(data.Viewpponorder.OrderMemo);
            GridViewRow row = e.Row;
            if (row.RowIndex != gv_Orders.EditIndex)
            {
                Label shipstatus = ((Label)row.FindControl("lbl_ShipStatus"));
                LinkButton edit = ((LinkButton)row.FindControl("lbt_Edit"));
                OrderLogStatus log;
                if (data.Orderstatuslog.Id != 0 && Enum.TryParse<OrderLogStatus>(data.Orderstatuslog.Status.ToString(), out log))
                {
                    switch (log)
                    {
                        case OrderLogStatus.Failure:
                            shipstatus.Text = "退貨失敗";
                            break;
                        case OrderLogStatus.Processing:
                            shipstatus.Text = "退貨處理中";
                            edit.Enabled = false;
                            break;
                        case OrderLogStatus.Success:
                            shipstatus.Text = "退貨完成";
                            edit.Enabled = false;
                            break;
                    }
                }
                else
                {
                    if (Helper.IsFlagSet(data.Viewpponorder.OrderStatus, OrderStatus.Cancel))
                    {
                        shipstatus.Text = "<div style='color:red'>" + Resources.Localization.OrderCancel + "</div>";
                        edit.Enabled = false;
                    }
                    else if (Helper.IsFlagSet(data.Viewpponorder.OrderStatus, OrderStatus.ProductShipped))
                        shipstatus.Text = Resources.Localization.ProductShipped;
                    else if (Helper.IsFlagSet(data.Viewpponorder.OrderStatus, OrderStatus.ProductPreparing))
                        shipstatus.Text = Resources.Localization.ProductPreparing;
                }
                ((Label)row.FindControl("lbl_ShipAgent")).Text = memo.ShipAgent;
                ((Label)row.FindControl("lbl_ShipNo")).Text = memo.ShipNo;
                ((Label)row.FindControl("lbl_ShipDate")).Text = (memo.ShipDate == null ? string.Empty : memo.ShipDate.Value.ToString("yyyy/MM/dd"));
            }
            else
            {
                ((TextBox)row.FindControl("tbx_ShipAgent")).Text = string.IsNullOrEmpty(memo.ShipAgent) ? tbx_DefaultShipAgent.Text : memo.ShipAgent;
                ((TextBox)row.FindControl("tbx_ShipNo")).Text = memo.ShipNo;
                ((TextBox)row.FindControl("tbx_ShipDate")).Text = (memo.ShipDate == null ? tbx_DefaultShipDate.Text : memo.ShipDate.Value.ToString("yyyy/MM/dd"));
                ((HiddenField)row.FindControl("hif_OrderMemo")).Value = data.Viewpponorder.OrderMemo;
                ((HiddenField)row.FindControl("hif_OrderReason")).Value = data.Orderstatuslog.Reason;
                ((HiddenField)row.FindControl("hif_Guid")).Value = data.Viewpponorder.Guid.ToString();
                ((HiddenField)row.FindControl("hif_OrderStatus")).Value = data.Viewpponorder.OrderStatus.ToString();
                if (Helper.IsFlagSet(data.Viewpponorder.OrderStatus, OrderStatus.ProductShipped))
                    ((RadioButton)row.FindControl("rbt_Shipped")).Checked = true;
                else if (Helper.IsFlagSet(data.Viewpponorder.OrderStatus, OrderStatus.ProductPreparing))
                    ((RadioButton)row.FindControl("rbt_Preparing")).Checked = true;
            }
        }
    }

    protected void gvRowEditing(object sender, GridViewEditEventArgs e)
    {
        gv_Orders.EditIndex = e.NewEditIndex;
        hif_edit.Value = "true";
        PageChange(sender, new DataEventArgs<int>(CurrentPage));
    }

    protected void gvRowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        hif_edit.Value = "false";
        PageChange(sender, new DataEventArgs<int>(CurrentPage));
    }

    protected void gvRowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        int orderstatus;
        int orderstatus_log = 0;
        GridViewRow row = gv_Orders.Rows[e.RowIndex];
        string ordermemostring = ((HiddenField)row.FindControl("hif_OrderMemo")).Value;
        orderstatus = int.TryParse(((HiddenField)row.FindControl("hif_OrderStatus")).Value, out orderstatus) ? orderstatus : 0;
        if (((RadioButton)row.FindControl("rbt_Preparing")).Checked)
        {
            orderstatus = (int)Helper.SetFlag(true, (int)Helper.SetFlag(false, orderstatus, ((int)OrderStatus.ProductShipped)), OrderStatus.ProductPreparing);
            orderstatus_log = (int)OrderLogStatus.ProductPreparing;
        }
        else if (((RadioButton)row.FindControl("rbt_Shipped")).Checked)
        {
            orderstatus = (int)Helper.SetFlag(true,(int)Helper.SetFlag(false, orderstatus, (int)OrderStatus.ProductPreparing),OrderStatus.ProductShipped);
            orderstatus_log = (int)OrderLogStatus.ProductShipped;
        }
        string strguid = ((HiddenField)row.FindControl("hif_Guid")).Value;

        OrderMemo ordermemo = new OrderMemo(ordermemostring);
        ordermemo.ShipAgent = ((TextBox)row.FindControl("tbx_ShipAgent")).Text;
        ordermemo.ShipNo = ((TextBox)row.FindControl("tbx_ShipNo")).Text;
        DateTime date;
        if (DateTime.TryParse(((TextBox)row.FindControl("tbx_ShipDate")).Text, out date))
            ordermemo.ShipDate = date;
        else
            ordermemo.ShipDate = null;

        if (this.UpdateShipOrderStatus != null)
        {
            hif_edit.Value = "false";
            Guid guid;
            OrderStatusLog ol = new OrderStatusLog();
            ol.CreateId = UserName;
            ol.Status = orderstatus_log;
            ol.OrderGuid = Guid.TryParse(strguid, out guid) ? guid : Guid.Empty;
            ol.CreateTime = DateTime.Now;
            ol.Message = ordermemo.ToString();
            ol.Reason = ((HiddenField)row.FindControl("hif_OrderReason")).Value;

            this.UpdateShipOrderStatus(sender, new DataEventArgs<KeyValuePair<OrderStatusLog, int>>(new KeyValuePair<OrderStatusLog, int>(ol, orderstatus)));
        }
    }

    public void SearchOrders(object sender, EventArgs e)
    {
        lbl_Message.Text = string.Empty;
        hif_edit.Value = "false";
        if (this.SearchPponOrderList != null)
        {
            Guid bid;
            if (Guid.TryParse(tbx_Bid.Text, out bid))
            {
                CurrentPage = 1;
                this.SearchPponOrderList(sender, new DataEventArgs<Guid>(bid));
                gridPager.SetupPaging();
                gridPager.ResolvePagerView(CurrentPage);
            }
            else
            {
                lbl_Message.Text = "Guid錯誤!!";
                ClearData();
            }
        }
    }

    protected void ClearData()
    {
        gv_Orders.Dispose();
        gv_Orders.DataBind();
        foreach (Control item in pan_DealInfo.Controls)
        {
            if (item is Label)
            {
                Label l = item as Label;
                l.Text = string.Empty;
            }
        }
    }

    protected int RetrieveOrderCount()
    {
        return PageCount;
    }

    protected void UpdateHandler(int pageNumber)
    {
        DataEventArgs<int> e = new DataEventArgs<int>(pageNumber);
        if (this.PageChange != null)
        {
            hif_edit.Value = "false";
            this.PageChange(this, e);
        }
    }

    public void SetOrderList(List<ClassForViewPponOrder_OrderStatusLog> lvpoc, ViewPponDeal vpd)
    {
        if (hif_edit.Value == "false")
            gv_Orders.EditIndex = -1;
        gv_Orders.DataSource = lvpoc;
        gv_Orders.DataBind();

        lbl_SellerName.Text = vpd.SellerName;
        lbl_ItemName.Text = vpd.EventName;
        lbl_OrderTimeS.Text = vpd.BusinessHourOrderTimeS.ToString("yyyy/MM/dd HH:mm");
        lbl_OrderTimeE.Text = vpd.BusinessHourOrderTimeE.ToString("yyyy/MM/dd HH:mm");
        lbl_DeliverTimeS.Text = vpd.BusinessHourDeliverTimeS == null ? string.Empty : vpd.BusinessHourDeliverTimeS.Value.ToString("yyyy/MM/dd HH:mm");
        lbl_DeliverTimeE.Text = vpd.BusinessHourDeliverTimeE == null ? string.Empty : vpd.BusinessHourDeliverTimeE.Value.ToString("yyyy/MM/dd HH:mm");
        lbl_OrderMin.Text = vpd.BusinessHourOrderMinimum.ToString("F0");
        lbl_OrderQuantity.Text = vpd.OrderedQuantity == null ? "0" : vpd.OrderedQuantity.Value.ToString();

        if (vpd.BusinessHourOrderTimeS > DateTime.Now)
            lbl_Status.Text = Phrase.NotToBegin;
        else if (vpd.BusinessHourOrderTimeS <= DateTime.Now && vpd.BusinessHourOrderTimeE > DateTime.Now)
        {
            lbl_Status.Text = Phrase.Proceeding;
            lbl_Status.Text += vpd.BusinessHourOrderMinimum <= vpd.OrderedQuantity ? "(" + Phrase.DealIsOn + ")" : string.Empty;
        }
        else if (vpd.BusinessHourOrderTimeE <= DateTime.Now)
        {
            lbl_Status.Text = Phrase.EventExpired;
            lbl_Status.Text += vpd.BusinessHourOrderMinimum <= vpd.OrderedQuantity ? "(" + Phrase.DealIsOn + ")" : "(" + Phrase.DealIsFailure + ")";
        }
    }
}

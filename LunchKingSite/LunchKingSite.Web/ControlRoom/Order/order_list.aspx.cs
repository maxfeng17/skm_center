﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;
using LunchKingSite.BizLogic.Component;

public partial class order_list : RolePage, IOrderListView
{
    public event EventHandler<DataEventArgs<int>> GetOrderCount;
    public event EventHandler SortClicked;
    public event EventHandler<DataEventArgs<int>> PageChanged;
    public event EventHandler SearchClicked;


    protected static IOrderProvider op;

    #region props
    private OrderListPresenter _presenter;
    public OrderListPresenter Presenter
    {
        set
        {
            this._presenter = value;
            if (value != null)
            {
                this._presenter.View = this;
            }
        }
        get
        {
            return this._presenter;
        }
    }

    public string SortExpression
    {
        get { return (ViewState != null && ViewState["se"] != null) ? (string)ViewState["se"] : ViewOrderMemberBuildingSeller.Columns.OrderCreateTime + " desc"; }
        set { ViewState["se"] = value; }
    }

    public string FilterUser
    {
        get { return tbSearch.Text; }
        set { tbSearch.Text = value; }
    }

    public string[] FilterInternal
    {
        get
        {
            string[] filter = new string[3];

            if (!string.IsNullOrEmpty(tbDS.Text))
                filter[0] = ViewOrderMemberBuildingSeller.Columns.OrderCreateTime + " >= " + tbDS.Text;
            else
                filter[0] = null;

            if (!string.IsNullOrEmpty(tbDE.Text))
                filter[1] = ViewOrderMemberBuildingSeller.Columns.OrderCreateTime + " < " + tbDE.Text;
            else
                filter[1] = null;
            return filter;
        }
    }

    public string FilterType
    {
        get { return ddlST.SelectedValue; }
        set { ddlST.SelectedValue = value; }
    }

    /*
    public bool ExcludeFamilyMart
    {
        get { return chkExcludeFamilyMart.Checked; }
    }
    public bool PaymentFreeze
    {
        get { return chkPaymentFreeze.Checked; }
    }
    */

    public DateTime? MinOrderCreateTime
    {
        get
        {
            DateTime result;
            if (DateTime.TryParse(tbDS.Text, out result))
            {
                return result;
            }

            return null;
        }
    }

    public DateTime? MaxOrderCreateTime
    {
        get
        {
            DateTime result;
            if (DateTime.TryParse(tbDE.Text, out result))
            {
                return result;
            }

            return null;
        }
    }

    public string SearchExpression
    {
        get { return tbSearch.Text; }
    }


    public int PageSize
    {
        get { return gridPager.PageSize; }
        set { gridPager.PageSize = value; }
    }

    public int CurrentPage
    {
        get { return gridPager.CurrentPage; }
    }

    public string OrderByInternal
    {
        get { return ViewOrderMemberBuildingSeller.Columns.OrderCreateTime + " desc"; }
    }

    public string DateType
    {
        get { return ddlDate.SelectedValue; }
    }

    public string ShipType
    {
        get { return ddlShipmentType.SelectedValue; }
    }

    public string CsvType
    {
        get { return ddlType.SelectedValue; }
    }




    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        gvOrder.Columns[0].SortExpression = ViewOrderMemberBuildingSeller.Columns.OrderId;
        gvOrder.Columns[2].SortExpression = ViewOrderMemberBuildingSeller.Columns.ProductDeliveryType;
        gvOrder.Columns[3].SortExpression = ViewOrderMemberBuildingSeller.Columns.OrderCreateTime;
        /*
        gvOrder.Columns[0].SortExpression = ViewOrderMemberBuildingSeller.Columns.MemberName;
        gvOrder.Columns[7].SortExpression = ViewOrderMemberBuildingSeller.Columns.OrderCreateTime;
        gvOrder.Columns[8].SortExpression = ViewOrderMemberBuildingSeller.Columns.DeliveryTime;
        gvOrder.Columns[9].SortExpression = ViewOrderMemberBuildingSeller.Columns.SellerName;
        gvOrder.Columns[10].SortExpression = ViewOrderMemberBuildingSeller.Columns.Total;
        */
        if (!Page.IsPostBack)
        {
            this.Presenter.OnViewInitialized();
            this.gridPager.Visible = false;
        }
        else
        {
            this.gridPager.Visible = true;
            this.Presenter.OnViewLoaded();
        }


    }

    public void SetOrderList(ViewOrderMemberBuildingSellerCollection orders)
    {
        UpdatePanel.Visible = true;
        gvOrder.DataSource = orders;
        gvOrder.DataBind();
    }

    public void SetFilterTypeDropDown(Dictionary<string, string> types)
    {
        ddlST.DataSource = types;
        ddlST.DataBind();
    }

    public string GetFilterTypeDropDown()
    {
        return ddlST.SelectedValue;
    }

    protected void bSearchBtn_Click(object sender, EventArgs e)
    {
        if (this.SearchClicked != null)
            this.SearchClicked(this, e);
        gridPager.ResolvePagerView(CurrentPage, true);
    }

    protected void gvOrder_Sorting(object sender, GridViewSortEventArgs e)
    {
        ViewState["sd"] = ((string)ViewState["sd"] == " ASC") ? " DESC" : " ASC";
        this.SortExpression = e.SortExpression + (string)ViewState["sd"];
        if (this.SortClicked != null)
            SortClicked(this, new EventArgs());
        gridPager.ResolvePagerView(CurrentPage, true);
    }

    protected void UpdateHandler(int pageNumber)
    {
        DataEventArgs<int> e = new DataEventArgs<int>(pageNumber);
        if (this.PageChanged != null)
            this.PageChanged(this, e);
    }

    protected int RetrieveOrderCount()
    {
        DataEventArgs<int> e = new DataEventArgs<int>(0);
        if (GetOrderCount != null)
            GetOrderCount(this, e);
        return e.Data;
    }



    protected void gvOrder_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.DataItem is ViewOrderMemberBuildingSeller)
        {
            string itemName = string.Empty;
            ViewOrderMemberBuildingSeller item = (ViewOrderMemberBuildingSeller)e.Row.DataItem;
            HyperLink hpOrderId = ((HyperLink)e.Row.FindControl("hpOrderId"));
            Label lblSendType = ((Label)e.Row.FindControl("lblSendType"));
            Label lblShipType = ((Label)e.Row.FindControl("lblShipType"));
            Label lblOrderDate = ((Label)e.Row.FindControl("lblOrderDate"));
            Label lblProductName = ((Label)e.Row.FindControl("lblProductName"));
            Label lblSellerName = ((Label)e.Row.FindControl("lblSellerName"));
            Label lblMember = ((Label)e.Row.FindControl("lblMember"));
            Label lblMobile = ((Label)e.Row.FindControl("lblMobile"));
            Label lblPrice = ((Label)e.Row.FindControl("lblPrice"));
            Label lblStatus = ((Label)e.Row.FindControl("lblStatus"));

            //先抓取母檔Bid
            Guid mainBid = ViewPponDealManager.DefaultManager.MainBusinessHourGuidGetByBid(item.BusinessHourGuid);
            var viewPponDeal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(mainBid);
            if (viewPponDeal.IsLoaded)
            {
                itemName = viewPponDeal.ItemName;
            }

            hpOrderId.Text = item.OrderId;
            hpOrderId.NavigateUrl = "~/controlroom/Order/order_detail.aspx?oid=" + item.OrderGuid.ToString();
            hpOrderId.Target = "_blank";
            lblSendType.Text = SellerFacade.GetTransportConfidence(item.SellerStatus.Value) == SellerTransportConfidence.None ? Resources.Localization.TransportConfidenceNone : SellerFacade.GetTransportConfidence(item.SellerStatus.Value) == SellerTransportConfidence.Low ? Resources.Localization.TransportConfidenceLow : Resources.Localization.TransportConfidenceHigh;
            string cnDeliveryType = string.Empty;
            if (item.DeliveryType == (int)DeliveryType.ToShop)
            {
                cnDeliveryType = "憑證";
            }
            else
            {
                if (item.ProductDeliveryType == (int)ProductDeliveryType.Normal)
                {
                    cnDeliveryType = "宅配";
                }
                else
                {
                    if (item.ProductDeliveryType == (int)ProductDeliveryType.FamilyPickup)
                    {
                        cnDeliveryType = "全家";
                    }
                    else if (item.ProductDeliveryType == (int)ProductDeliveryType.SevenPickup)
                    {
                        cnDeliveryType = "統一";
                    }
                    else if (item.ProductDeliveryType == (int)ProductDeliveryType.Wms)
                    {
                        cnDeliveryType = "PChome";
                    }
                }
            }

            lblShipType.Text = cnDeliveryType;
            var ispOrder = OrderFacade.GetIspPaymentOrder(item.OrderGuid);
            lblOrderDate.Text = (ispOrder.IsLoaded && ispOrder.DeliveryTime != null) ? ((DateTime)ispOrder.DeliveryTime).ToString("yyyy/MM/dd") : item.OrderCreateTime.ToString("yyyy/MM/dd");
            lblProductName.Text = item.UniqueId.ToString() + "<br />" + itemName;
            lblSellerName.Text = item.SellerId + "<br />" + item.SellerName;
            lblMember.Text = item.MemberName;
            lblMobile.Text = item.MemberMobile;
            lblPrice.Text = ((int)item.Total).ToString();
            lblStatus.Text = item.DeliveryType == (int)DeliveryType.ToShop ? "可使用" : item.ShipTime == null ? "待出貨" : "已出貨";

        }
    }
}

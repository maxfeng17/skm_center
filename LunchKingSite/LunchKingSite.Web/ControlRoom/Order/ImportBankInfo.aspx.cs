﻿using System;
using System.Collections;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using System.Web.Services;
using LunchKingSite.Core.UI;

namespace LunchKingSite.Web.ControlRoom.Order
{
    public partial class ImportBankInfo : RolePage
    {
        IOrderProvider op = ProviderFactory.Instance().GetProvider<IOrderProvider>();

        protected void Page_Load(object sender, EventArgs e)
        { }

        protected void Import_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(BankList.FileName))
            {
                ClientScript.RegisterStartupScript(GetType(), "lockedOut", "alert('請選擇檔案！');", true);
            }
            else
            {
                if (null != BankList.PostedFile && BankList.PostedFile.ContentLength > 0)
                {
                    HSSFWorkbook hssfworkbook = new HSSFWorkbook(BankList.PostedFile.InputStream);
                    Sheet currentSheet = hssfworkbook.GetSheetAt(0);
                    Row row;
                    Hashtable bank = new Hashtable();
                    BankInfoCollection bic = new BankInfoCollection();

                    for (int k = 1; k <= currentSheet.LastRowNum; k++)
                    {
                        row = currentSheet.GetRow(k);
                        int bankNo = 0;
                        if (null != row.GetCell(0) && int.TryParse(row.GetCell(0).StringCellValue, out bankNo))
                        {
                            string bankNoString = row.GetCell(0).StringCellValue;
                            string branchNo = null == row.GetCell(1) ? string.Empty : row.GetCell(1).StringCellValue.Replace(bankNoString, string.Empty);

                            if (string.IsNullOrWhiteSpace(branchNo) && !bank.Contains(bankNo))
                            {
                                bank.Add(bankNo, row.GetCell(2).StringCellValue);
                            }

                            string bankName = bank[bankNo].ToString();
                            string branchName = row.GetCell(2).StringCellValue.Replace(bankName, string.Empty);
                            string address = null == row.GetCell(3) ? string.Empty : row.GetCell(3).StringCellValue;
                            string telephone = null == row.GetCell(4) ? string.Empty : row.GetCell(4).StringCellValue.Replace("-", string.Empty);
                            BankInfo bi = op.BankInfoGet(bankNoString);
                            if (null == bi || !bi.IsLoaded)
                            {
                                bi = new BankInfo();
                            }

                            bi.BankNo = bankNoString;
                            bi.BankName = bankName;
                            if (!string.IsNullOrWhiteSpace(branchNo))
                            {
                                bi.BranchNo = branchNo;
                                bi.BranchName = branchName;
                            }
                            bi.Address = address;
                            bi.Telephone = telephone;

                            bic.Add(bi);
                        }
                    }

                    op.BankInfoCollectionSet(bic);

                    ClientScript.RegisterStartupScript(GetType(), "lockedOut", "alert('匯入成功！');", true);
                }
            }
        }
    }
}
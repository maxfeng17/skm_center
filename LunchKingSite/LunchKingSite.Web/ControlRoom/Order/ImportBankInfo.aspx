﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ImportBankInfo.aspx.cs"
    Inherits="LunchKingSite.Web.ControlRoom.Order.ImportBankInfo" MasterPageFile="~/ControlRoom/backend.master" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <asp:FileUpload ID="BankList" runat="server"></asp:FileUpload>
        <asp:Button ID="Import" runat="server" Text="Excel表匯入" OnClick="Import_Click" /><br />
        備註：Excel資料來源為<a href="http://www.banking.gov.tw/Layout/main_ch/FscSearch_BankMain.aspx?path=1614&Type=1">金管會</a>，下載後請先嘗試打開，若出現格式錯誤訊息請先另存為97~2000格式(.xls)再匯入。
    </div>

</asp:Content>

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.Security;
using System.Web.Services;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model.Refund;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Enumeration;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.I18N;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;
using PaymentType = LunchKingSite.Core.PaymentType;
using LunchKingSite.BizLogic.Vbs.BS;
using System.Data;

public partial class order_detail : RolePage, IOrderDetailView
{
    int itemCount = 0;

    #region Events
    public event EventHandler<DataEventArgs<UpdatableOrderInfo>> UpdateOrderInfo;
    public event EventHandler<DataEventArgs<OrderDetail>> UpdateOrderDetail;
    public event EventHandler<DataEventArgs<OrderShipArgs>> UpdateOrderShip;
    public event EventHandler<DataEventArgs<OrderShipArgs>> DeleteOrderShip;
    public event EventHandler<DataEventArgs<OrderShipArgs>> AddOrderShip;
    public event EventHandler<DataEventArgs<int>> GetOrderDetailCount;
    public event EventHandler<DataEventArgs<int>> OrderDetailPageChanged;
    public event EventHandler SendRefundResult;
    public event EventHandler<DataEventArgs<string>> ReturnApplicationResend;
    public event EventHandler<DataEventArgs<string>> ReturnDiscountResend;
    public event EventHandler<DataEventArgs<int>> GetCashTrustLog;
    public event EventHandler CouponAvailableToggle;
    public event EventHandler<DataEventArgs<Dictionary<int, string>>> CouponBound;
    public event EventHandler<DataEventArgs<int>> GetSMS;

    public event EventHandler RecoveredConfirm;
    public event EventHandler<DataEventArgs<int>> InvoiceMailBackSet;
    public event EventHandler<DataEventArgs<CtAtmRefund>> SetAtmRefund;
    public event EventHandler<DataEventArgs<ServiceMessage>> ServiceMsgSet;
    public event EventHandler<DataEventArgs<int>> ReceiptMailBackSet;
    public event EventHandler<DataEventArgs<int>> ReceiptMailBackAllowanceSet;
    public event EventHandler<DataEventArgs<int>> ReceiptMailBackPaperSet;
    public event EventHandler CashTrustlogFilled;
    public event EventHandler<ReturnFormArgs> CreateReturnForm;
    public event EventHandler<ReturnFormReceiverArgs> UpdateReturnFormReceiver;
    public event EventHandler<CouponReturnFormArgs> CreateCouponReturnForm;
    public event EventHandler<DataEventArgs<int>> ImmediateRefund;
    public event EventHandler<DataEventArgs<int>> ReceivedCreditNote;
    public event EventHandler<DataEventArgs<int>> ChangeToElcAllowance;
    public event EventHandler<DataEventArgs<int>> ChangeToPaperAllowance;
    public event EventHandler<DataEventArgs<int>> CancelReturnForm;
    public event EventHandler<DataEventArgs<int>> UpdateAtmRefund;
    public event EventHandler<DataEventArgs<int>> RecoverVendorProgress;
    public event EventHandler<DataEventArgs<int>> ProcessedVendorUnreturnable;
    public event EventHandler<DataEventArgs<int>> SCashToCash;
    public event EventHandler<DataEventArgs<int>> CashToSCash;
    public event EventHandler<DataEventArgs<int>> ArtificialRefund;
    public event EventHandler<DataEventArgs<int>> RecoverToRefundScash;
    public event EventHandler<DataEventArgs<string>> RequestInvoice;
    public event EventHandler<DataEventArgs<OrderUserMemoList>> OrderUserMemoSet;
    public event EventHandler<DataEventArgs<int>> OrderUserMemoDelete;
    public event EventHandler CreditcardReferSet;
    public event EventHandler<DataEventArgs<OrderFreezeInfo>> OrderFreezeSet;
    public event EventHandler<ExchangeLogArgs> CreateExchangeLog;
    public event EventHandler<UpdateExchangeLogArgs> UpdateExchangeLog;
    public event EventHandler<DataEventArgs<int>> CompleteExchangeLog;
    public event EventHandler<DataEventArgs<int>> CancelExchangeLog;
    public event EventHandler<DataEventArgs<Guid>> CancelOrder;
    public event EventHandler<DataEventArgs<Guid>> WithdrawCancelOrder;
    public event EventHandler<DataEventArgs<int>> DownloadCouponZip;
    #endregion

    #region props
    private IMemberProvider memProv;
    private IAccountingProvider _ap;
    private IOrderProvider _op;
    private IPponProvider _pp;
    private IWmsProvider _wp;
    private OrderDetailPresenter _presenter;
    public OrderDetailPresenter Presenter
    {
        set
        {
            this._presenter = value;
            if (value != null)
            {
                this._presenter.View = this;
            }
        }
        get
        {
            return this._presenter;
        }
    }

    public AgentChannel ChannelAgent
    {
        get
        {
            int val;
            int.TryParse(hidChannelAgent.Value, out val);
            return (AgentChannel)val;
        }

        set
        {
            hidChannelAgent.Value = Convert.ToString((int)value);
        }
    }

    public string CurrentUser
    {
        get { return User.Identity.Name; }
    }

    private Guid _oid;
    public Guid OrderId
    {
        get { return (ViewState != null && ViewState["id"] != null) ? (Guid)ViewState["id"] : Guid.Empty; }
        set { ViewState["id"] = value; }
    }
    public Guid BusinessHourGuid
    {
        get { return (ViewState != null && ViewState["bid"] != null) ? (Guid)ViewState["bid"] : Guid.Empty; }
        set { ViewState["bid"] = value; }
    }
    public int CashTrustLogStatus
    {
        get { return (ViewState != null && ViewState["CashTrustLogStatus"] != null) ? (int)ViewState["CashTrustLogStatus"] : 0; }
        set { ViewState["CashTrustLogStatus"] = value; }
    }

    public int ShipMemoLimit
    {
        get { return 30; }
    }

    public bool OldInvoiceAllowance
    {
        get;
        set;
    }

    public bool OldInvoicePaper
    {
        get;
        set;
    }

    public bool OldReturnPaper
    {
        get;
        set;
    }

    public IAuditBoardView AuditBoardView
    {
        get { return ab; }
    }

    private string _os;
    public string OrderSituation
    {
        get { return _os; }
        set { _os = value; }
    }

    //public string ReturnMessage
    //{
    //    get { return tbReturnMessage.Text; }
    //}

    public string alertMessage
    {
        set { AjaxControlToolkit.ToolkitScriptManager.RegisterStartupScript(this, this.GetType(), string.Empty, "alert('" + value + "')", true); }
    }

    public List<string> alertMultiMessage
    {
        set { AjaxControlToolkit.ToolkitScriptManager.RegisterStartupScript(this, this.GetType(), string.Empty, "alert('" + string.Join("');alert('", value.ToArray()) + "');", true); }
    }

    private bool _isExpired;
    public bool IsExpired
    {
        get { return _isExpired; }
        set { _isExpired = value; }
    }

    public bool IsCoupon
    {
        get
        {
            return cbx_IsCoupon.Checked;
        }
        set
        {
            cbx_IsCoupon.Checked = value;
        }
    }

    public string DealFreeze
    {
        set
        {
            lblDealFreeze.Text = value;
        }
    }
    public bool FreezeVisible
    {
        set
        {
            btnFreeze.Visible = value;
        }
    }
    public bool CancelFreezeVisible
    {
        set
        {
            btnCancelFreeze.Visible = value;
        }
    }


    private CashTrustLog _ct;
    public CashTrustLog CtLog
    {
        get { return _ct; }
        set { _ct = value; }
    }

    private CtAtmRefundCollection _atmRefundCol;
    public CtAtmRefundCollection AtmRefundCol
    {
        get { return _atmRefundCol; }
        set { _atmRefundCol = value; }
    }

    private ShipCompanyCollection _shipCompanyCol;
    public ShipCompanyCollection ShipCompanyCol
    {
        get { return _shipCompanyCol; }
        set { _shipCompanyCol = value; }
    }

    public int TotalQuantity
    {
        get { return (ViewState != null && ViewState["tq"] != null) ? (int)ViewState["tq"] : 0; }
        set { ViewState["tq"] = value; }
    }


    public bool IsToHouse
    {
        get { return (ViewState != null && ViewState["IsToHouse"] != null) ? (bool)ViewState["IsToHouse"] : false; }
        set { ViewState["IsToHouse"] = value; }
    }

    public int SmsCount { get; set; }

    public string RecoveredInfo { get; set; }

    public EinvoiceMain Invoice { get; set; }

    public EntrustSellReceipt Receipt { get; set; }

    public bool IsEntrustSell { get; set; }

    public string MemberEmail { get; set; }

    public bool AllowanceSentBack { get; set; }
    public bool AllowanceSentBackVisible { get; set; }
    public bool InvoiceSentBack { get; set; }
    public bool InvoiceSentBackVisible { get; set; }
    public bool EinvoiceWinner { get; set; }
    public bool EinvoiceWinnerVisible { get; set; }
    public bool EinvoicePapered { get; set; }
    public bool HaveInvoice { get; set; }

    public bool ReturnPaperSentBack
    {
        get { return cbPaper.Checked; }
        set { cbPaper.Checked = value; }
    }

    public bool NoTax { get; set; }

    public bool IsInvoiceExpired
    {
        get
        {
            return Convert.ToBoolean(this.HiddenIsInvoiceExpired.Value);
        }
        set
        {
            this.HiddenIsInvoiceExpired.Value = value.ToString();
        }
    }

    public int GoodsReturnStatus
    {
        get { return (int)ViewState["GoodsReturnStatus"]; }
        set { ViewState["GoodsReturnStatus"] = value; }
    }

    public DateTime ShipStartDate { get; set; }

    public bool CheckIsShipable
    {
        get { return (ViewState != null && ViewState["CheckIsShipable"] != null) ? (bool)ViewState["CheckIsShipable"] : true; }
        set { ViewState["CheckIsShipable"] = value; }
    }

    public bool CheckIsExchanging
    {
        get { return (ViewState != null && ViewState["CheckIsExchanging"] != null) ? (bool)ViewState["CheckIsExchanging"] : true; }
        set { ViewState["CheckIsExchanging"] = value; }
    }

    /// <summary>
    /// 登入者是否有退貨的權限
    /// </summary>
    public bool CanRefund
    {
        get
        {
            return CommonFacade.IsInSystemFunctionPrivilege(User.Identity.Name, SystemFunctionType.Refund);
        }
    }

    /// <summary>
    /// 登入者是否有取消退貨的權限
    /// </summary>
    public bool CanCancelRefund
    {
        get
        {
            //代銷訂單只限特定權限可使用取消退貨
            if (ChannelAgent != AgentChannel.NONE)
            {
                return CommonFacade.IsInSystemFunctionPrivilege(User.Identity.Name, SystemFunctionType.CancelAgentOrderRefund);
            }

            return CommonFacade.IsInSystemFunctionPrivilege(User.Identity.Name, SystemFunctionType.Refund);
        }
    }

    protected static ISysConfProvider config = ProviderFactory.Instance().GetConfig();

    public DropDownList ddlServiceCategory
    {
        get { return this.ddlCategory; }
    }

    public DropDownList ddlServiceSubCategory
    {
        get { return this.ddlSubCategory; }
    }

    private string _linkImageBid = "";

    public string LinkImageBid
    {
        get { return _linkImageBid; }
    }

    /// <summary>
    /// 是否紙本折讓單
    /// </summary>
    public bool IsPaperAllowance
    {
        get { return (ViewState != null && ViewState["IsPaperAllowance"] != null) ? (bool)ViewState["IsPaperAllowance"] : true; }
        set { ViewState["IsPaperAllowance"] = value; }
    }

    /// <summary>
    /// 是否啟用轉換電子折讓單
    /// </summary>
    public bool IsEnableElecAllowance
    {
        get { return (ViewState != null && ViewState["IsEnableElecAllowance"] != null) ? (bool)ViewState["IsEnableElecAllowance"] : false; }
        set { ViewState["IsEnableElecAllowance"] = value; }
    }

    public List<EinvoiceFacade.EinvoiceChangeLogMain> EinvoiceChangeLogMainData
    {
        get; set;
    }

    public bool IsWms { get; set; }

    public string CouponDataJson { get; set; }

    #endregion

    #region interface methods

    public void SetOrderStatusLog(OrderStatusLogCollection ol)
    {
        if (!config.EnabledExchangeProcess)
        {
            rpt_RefundLog_old.DataSource = ol.OrderByDescending(x => x.CreateTime);
            rpt_RefundLog_old.DataBind();
        }
        else
        {
            rpt_RefundLog.DataSource = ol.OrderByDescending(x => x.CreateTime);
            rpt_RefundLog.DataBind();
        }
    }

    public void SetOrderInfo(ViewOrderMemberBuildingSeller odr, ViewPponDeal deal, ViewPponOrderStatusLog firstOrderLog,
         ViewPponOrderStatusLog lastOrderLog, ViewPponCouponCollection coupons, PaymentTransactionCollection paymentsPT,
         ViewOrderMemberPaymentCollection paymentsCS, EinvoiceMain einvoice, EntrustSellType entrustSell,
         EntrustSellReceipt receipt, DealProperty property, GroupOrder go, BankInfoCollection bks,
         DateTime newInvoiceDate, DealAccounting accounting, EinvoiceMainCollection einvCol, CashTrustLogCollection ctls, OrderCorresponding oc, bool is_masterpass,
         VendorPaymentChangeCollection vpcs, Order o)
    {
        // check piinlife pponcity
        List<int> citys = (deal != null && deal.CityList != null) ? new JsonSerializer().Deserialize<List<int>>(deal.CityList) : null;
        if (citys != null)
        {
            if (citys.Contains(PponCityGroup.DefaultPponCityGroup.Piinlife.CityId))
            {
                lblPponCity.Text = PponCityGroup.DefaultPponCityGroup.Piinlife.CityName;
            }
        }

        pan_Tmall.Visible = false;
        p1.Visible = true;
        create_Return.Enabled = true;
        create_Invoice.Enabled = true;

        #region Tmall
        if (oc != null && oc.Id != 0)
        {
            ChannelAgent = (AgentChannel)oc.Type;
            pan_Tmall.Visible = true;
            
            lab_TmallMobile.Text = oc.Mobile;
            lab_TnallOrder.Text = oc.RelatedOrderId;
            if (oc.Type == (int)AgentChannel.LifeEntrepot)
            {
                create_Return.Enabled = false;
                create_Invoice.Enabled = false;
            }

            var targetCol = Enum.GetValues(typeof(AgentChannel)).Cast<AgentChannel>().Where(t => (int)t == oc.Type);
            lab_TmallType.Text = targetCol.Any() ? Helper.GetEnumDescription(targetCol.FirstOrDefault()) : string.Empty;
        }
        #endregion

        lbUniqueId.Text = odr.UniqueId.ToString();
        lOI.Text = odr.OrderId;
        hlS.Text = string.Format("{0} {1}", odr.SellerId, odr.SellerName);
        hlS.NavigateUrl = ResolveUrl("../seller/seller_add.aspx") + "?sid=" + odr.SellerGuid.ToString();

        ViewPponCoupon anyCoupon = coupons.FirstOrDefault();
        sellerExpireDateInformation.Text = GetSellerExpireDateContent(anyCoupon);

        if (anyCoupon != null)
        {
            storeLink.Text = string.Format("{0} {1}", anyCoupon.StoreSellerId, anyCoupon.StoreName);
            storeLink.NavigateUrl = ResolveUrl(string.Format("../seller/seller_add.aspx?sid={0}", anyCoupon.StoreGuid.ToString()));
            storeExpireDateInformation.Text = GetStoreExpireDateContent(anyCoupon);
        }
        hlM.Text = odr.MemberName;
        hlM.NavigateUrl = ResolveUrl("../user/users_edit.aspx?username=") + System.Web.HttpUtility.UrlEncode(odr.MemberEmail);
        HiddenMemberEmail.Text = odr.MemberEmail;
        if (odr.Gender == 1)
            lMG.Text = Resources.Localization.Mr;
        else
            lMG.Text = Resources.Localization.Ms;
        tbP.Text = odr.OrderPhone;
        tbM.Text = odr.OrderMobile;
        if (odr.PrimaryContactMethod == (int)ContactMethod.LandLine)
            tbP.Font.Bold = true;
        else
            tbM.Font.Bold = true;
        lMC.Text = odr.CompanyName;
        tbDA.Text = (odr.DeliveryAddress != null) ? odr.DeliveryAddress.Replace("<hr />", string.Empty) : string.Empty;
        lOT.Text = OrderFacade.GetIspPaymentOrder(odr.OrderGuid).IsLoaded && odr.DeliveryTime != null ? ((DateTime)odr.DeliveryTime).ToString("yyyy/MM/dd HH:mm:ss")
            : odr.OrderCreateTime.ToString("yyyy/MM/dd HH:mm:ss");
        lDT.Text = odr.DeliveryTime.HasValue ? odr.DeliveryTime.Value.ToString("yyyy/MM/dd HH:mm:ss") : string.Empty;
        lSP.Text = odr.SellerTel;
        lSF.Text = odr.SellerFax;
        lMP.Text = odr.OrderModifyId;
        lMT.Text = odr.OrderModifyTime.HasValue ? odr.OrderModifyTime.Value.ToString("yyyy/MM/dd HH:mm:ss") : string.Empty;
        orderTime.Text = (deal != null) ? deal.BusinessHourOrderTimeS.ToString("yyyy/MM/dd HH:mm:ss") + " ~ " + deal.BusinessHourOrderTimeE.ToString("yyyy/MM/dd HH:mm:ss") : string.Empty;
        deliverTime.Text = (deal != null) ? ((DateTime)deal.BusinessHourDeliverTimeS).ToString("yyyy/MM/dd") + " ~ " + ((DateTime)deal.BusinessHourDeliverTimeE).ToString("yyyy/MM/dd") : string.Empty;
        if (deal.DeliveryType == (int)DeliveryType.ToHouse)
        {
            palLastShipDate.Visible = true;
            OrderProductDelivery opd = _op.OrderProductDeliveryGetByOrderGuid(odr.OrderGuid);
            if (opd.IsLoaded)
            {
                if (opd.LastShipDate != null)
                {
                    lastShipDate.Text = opd.LastShipDate.Value.ToString("yyyy/MM/dd");
                    if (!string.IsNullOrWhiteSpace(deal.LabelIconList))
                    {
                        string[] ss = deal.LabelIconList.Split(',');
                        if (ss.Contains(((int)DealLabelSystemCode.TwentyFourHoursArrival).ToString()))
                        {
                            lastShipDate.Text = "24H到貨不適用";
                        }
                    }
                }
                else
                {
                    lastShipDate.Text = "照會中";
                }

                ViewSimpleOverdueOrder voo = _op.ViewSimpleOverdueOrderGet(odr.OrderGuid);
                VendorPaymentOverdue vpo = _ap.VendorPaymentOverdueGetByOrderGuid(odr.OrderGuid);//只看sys的
                if (voo.IsLoaded || vpo.IsLoaded)
                {
                    lblOverdue.Visible = true;
                }
            }
            btnFine.Visible = true;
        }
        IsWms = deal.IsWms;

        ChangedExpireDate.Text = anyCoupon != null ? !string.IsNullOrEmpty(CouponFacade.GetFinalExpireDateContent(anyCoupon.OrderGuid, Guid.Empty, Guid.Empty)) ? "(至" + CouponFacade.GetFinalExpireDateContent(anyCoupon.OrderGuid, Guid.Empty, Guid.Empty) + "憑證停止使用)" : "" : string.Empty;

        if (property != null && (property.SellerVerifyType == null || property.SellerVerifyType == (int)SellerVerifyType.VerificationOffline))
        {
            lbVerityType.Text = Resources.Localization.VerificationOffline;
        }
        else if (property != null && property.SellerVerifyType == (int)SellerVerifyType.VerificationOnline)
        {
            lbVerityType.Text = Resources.Localization.VerificationOnline;
        }
        else
            lbVerityType.Text = string.Empty;

        #region 檔次狀態

        string businessHourSituation = string.Empty;
        string NoRefundPhrase = "起不可退貨";

        if (go != null)
        {
            //不接受退貨 (購買起日起)
            if ((go.Status & (int)GroupOrderStatus.NoRefund) > 0)
            {
                businessHourSituation += string.Format("{0}({1} {2}){3}", Phrase.NoRefund, deal.BusinessHourOrderTimeS.Date.ToShortDateString(), NoRefundPhrase, Phrase.HtmlNewLine);
            }
            //結檔後七天不能退貨 (購買迄日+7+1日起)
            if ((go.Status & (int)GroupOrderStatus.DaysNoRefund) > 0)
            {
                businessHourSituation += string.Format("{0}({1} {2}){3}", Phrase.DaysNoRefund, deal.BusinessHourOrderTimeE.AddDays(config.GoodsAppreciationPeriod + 1).Date.ToShortDateString(), NoRefundPhrase, Phrase.HtmlNewLine);
            }
            //過期不能退貨 (配送迄日+1日起)
            if ((go.Status & (int)GroupOrderStatus.ExpireNoRefund) > 0)
            {
                businessHourSituation += string.Format("{0}({1} {2}){3}", Phrase.ExpireNoRefund, Convert.ToDateTime(deal.BusinessHourDeliverTimeE).AddDays(1).Date.ToShortDateString(), NoRefundPhrase, Phrase.HtmlNewLine);
            }
            //演出時間前十日內不能退貨
            if ((go.Status & (int)GroupOrderStatus.NoRefundBeforeDays) > 0)
            {
                businessHourSituation += string.Format("{0}({1} {2}){3}", Phrase.NoRefundBeforeDays,
                    Convert.ToDateTime(deal.BusinessHourDeliverTimeS).AddDays(-config.PponNoRefundBeforeDays).Date.ToShortDateString(), NoRefundPhrase, Phrase.HtmlNewLine);
            }
        }

        #endregion

        lblBusinessHourSituation.Text = businessHourSituation;
        //檔次連結
        if (deal != null)
        {
            //寄倉檔次標記
            string consignmentDesc = "";
            if (deal.Consignment.GetValueOrDefault(false))
            {
                consignmentDesc = "[商品寄倉]";
            }

            string fastget = "";
            if (!string.IsNullOrWhiteSpace(deal.LabelIconList))
            {
                foreach (var s in deal.LabelIconList.Split(','))
                {
                    if (s == ((int)DealLabelSystemCode.ArriveIn24Hrs).ToString() ||
                        s == ((int)DealLabelSystemCode.ArriveIn72Hrs).ToString())
                    {
                        fastget += string.Format("[{0}]", SystemCodeManager.GetDealLabelCodeName(Convert.ToInt32(s)));
                    }
                    else if (s == ((int)DealLabelSystemCode.TwentyFourHoursArrival).ToString())
                    {
                        fastget += string.Format("[{0}]", SystemCodeManager.GetDealLabelCodeName(Convert.ToInt32(s)));
                    }
                }
            }

            # region 追加檔次標題
            //檔次標題
            string deliveryType;

            switch (deal.DeliveryType)
            {
                case (int)DeliveryType.ToShop:
                    bool isGroupCoupon = Helper.IsFlagSet(deal.BusinessHourStatus, BusinessHourStatus.GroupCoupon);
                    deliveryType = "[憑證]";
                    if (isGroupCoupon)
                    {
                        if ((deal.GroupCouponDealType ?? 0) == (int)GroupCouponDealType.AvgAssign)
                        {
                            deliveryType = "[憑證x成套票券A]";
                        }
                        if ((deal.GroupCouponDealType ?? 0) == (int)GroupCouponDealType.CostAssign)
                        {
                            deliveryType = "[憑證x成套票券B]";
                        }
                    }
                    btnCouponDetail.Visible = isGroupCoupon;
                    break;
                case (int)DeliveryType.ToHouse:
                    deliveryType = "[宅配]";
                    break;
                default:
                    deliveryType = string.Empty;
                    break;
            }
            #endregion

            _linkImageBid = deal.BusinessHourGuid.ToString();
            lblDeal.Text = string.Format("<a href='{0}/controlroom/ppon/setup.aspx?bid={1}' target='_blank'>{2}{3}{4}{5}</a>",
                config.SiteUrl, deal.BusinessHourGuid, consignmentDesc, fastget, deliveryType, deal.ItemName);
        }
        //if (odr.ReviewScore.HasValue)
        //{
        //    iRV.ImageUrl = OrderFacade.BindFaces((OrderReviewType)odr.ReviewScore, "~/Themes/default/images/ui");
        //    lRV.Text = odr.Review;
        //    lRD.Text = odr.ReviewDate.Value.ToString();
        //    tbRR.Enabled = true;
        //}
        //else
        iRV.Visible = false;

        SetCouponList(coupons);
        SetTrustCouponDetail(ctls);

        //是否核對對帳單
        if (accounting.FinalBalanceSheetDate != null)
        {
            DateTime finalBalanceDate = (DateTime)accounting.FinalBalanceSheetDate;
            ltlFinalBalanceSheetDate.Text = finalBalanceDate.ToString("yyyy-MM-dd HH:mm:ss");
        }
        else
        {
            ltlFinalBalanceSheetDate.Text = "尚未核對對帳單";
        }

        //tbRR.Text = odr.ReviewReply;
        //lRR.Text = odr.ReviewReplyDate.HasValue ? odr.ReviewReplyDate.Value.ToString() : string.Empty;

        //給消費者紅利
        var memberPromotionInfo = memProv.MemberPromotionDepositGetListByOids(odr.OrderGuid);
        //不給消費者紅利
        var membernoneInfo = memProv.MemberNoneDepositGetListByOids(odr.OrderGuid);
        List<int> vpc_id = new List<int>();
        foreach (var m in memberPromotionInfo)
        {
            vpc_id.Add(m.VendorPaymentChangeId ?? 0);
        }
        foreach (var m in membernoneInfo)
        {
            vpc_id.Add(m.VendorPaymentChangeId ?? 0);
        }
        //罰款列表
        var paymentInfo = _ap.VendorPaymentChangeGetListByIds(vpc_id);
        ltlVpc.Text = "";
        foreach (var p in paymentInfo)
        {
            ltlVpc.Text += p.CreateTime + " " + p.Reason + "<br />";
        }

        //撈取對帳單資料
        lblBS.Text = "";
        ThirdPartyPayment tpp = ThirdPartyPayment.None;
        CashTrustLogCollection ctlogs = memProv.CashTrustLogGetListByOrderGuid(odr.OrderGuid, OrderClassification.LkSite);
        if (ctlogs.Count() > 0)
        {
            BalanceSheetDetailCollection bsdcs = _ap.BalanceSheetDetailGetListByTrustId(ctlogs.Select(x => x.TrustId).ToList());
            lblBS.Text = string.Join("<br />", bsdcs.Select(x => x.BalanceSheetId).Distinct());
            tpp = (ThirdPartyPayment)ctlogs.First().ThirdPartyPayment;
        }

        lST.Text = odr.Subtotal.ToString("C0");

        cbPaper.Checked = odr.ReturnPaper;

        #region show Invoice Info and entrust sell receipt
        if (entrustSell == EntrustSellType.No)
        {
            lab_EinvoiceHistory.Text = "";
            foreach (EinvoiceMain inv in einvCol.OrderBy(x => x.InvoiceNumber))
            {
                string invoiceType = EinvoiceFacade.GetEinvoiceStatusDesc(inv);
                if (!string.IsNullOrEmpty(inv.InvoiceNumber))
                {
                    lab_EinvoiceHistory.Text += inv.InvoiceNumber + invoiceType;
                    lab_EinvoiceHistory.Text += "<br/>";
                }
            }

            mvInvoiceOrReceipt.ActiveViewIndex = 0;
            if (einvoice.Id != 0)
            {
                pnInvoice.Visible = true;
                HaveInvoice = true;
                lbl_EinvoiceNum.Text = einvoice.InvoiceNumber;
                hif_EinvoiceId.Value = einvoice.Id.ToString();
                EinvoicePapered = einvoice.InvoicePapered;
                AllowanceSentBack = einvoice.InvoiceMailbackAllowance ?? false;
                InvoiceSentBack = einvoice.InvoiceMailbackPaper ?? false;

                invCtrl.SetData(einvoice, einvCol);

                EinvoiceWinner = einvoice.InvoiceWinning;
                InvoiceVersion version = (InvoiceVersion)einvoice.Version;
                if (version == InvoiceVersion.Old)
                {
                    lblInvoiceVersion.Text = "【舊電子發票】";
                }
                else if (version == InvoiceVersion.CarrierEra)
                {
                    lblInvoiceVersion.Text = "【發票載具】";
                }
            }
        }
        else
        {
            mvInvoiceOrReceipt.ActiveViewIndex = 1;

            if (deal != null && deal.DeliveryType != null && (DeliveryType)deal.DeliveryType.Value == DeliveryType.ToHouse)
            {
                phDeliveryReceiptMailBack.Visible = true;
            }
            else
            {
                phDeliveryReceiptMailBack.Visible = false;
            }

            txtReceiptTitle.Enabled = receipt.ForBusiness;
            txtReceiptNumber.Enabled = receipt.ForBusiness;

            chkReceiptIsGenerated.Checked = receipt.ReceiptCode != null;
            txtReceiptCode.Text = receipt.ReceiptCode;
            txtReceiptRemark.Text = receipt.ReceiptRemark;
            chkReceiptIsPhysical.Checked = receipt.IsPhysical;
            txtReceiptTitle.Text = receipt.BuyerName;
            txtReceiptNumber.Text = receipt.ComId;
            txtReceiptAddress.Text = receipt.BuyerAddress;
            chkReceiptMailbackPaper.Checked = receipt.MailbackPaper;
            chkReceiptMailbackAllowance.Checked = receipt.MailbackAllowance;
            chkReceiptMailbackReceipt.Checked = receipt.MailbackReceipt;
        }
        #endregion

        #region show return step and message
        if (lastOrderLog != null)
        {
            string exchangeStatusDesc = string.Empty;
            tbReturnMessage.Text = lastOrderLog.OrderLogMessage;

            switch ((OrderLogStatus)lastOrderLog.OrderLogStatus)
            {
                case OrderLogStatus.ExchangeCancel:
                case OrderLogStatus.ExchangeFailure:
                case OrderLogStatus.ExchangeProcessing:
                case OrderLogStatus.ExchangeSuccess:
                case OrderLogStatus.SendToSeller:
                    exchangeStatusDesc = Helper.GetLocalizedEnum(Resources.Localization.ResourceManager, (OrderLogStatus)lastOrderLog.OrderLogStatus);
                    break;
                default:
                    break;
            }

            orderStatusInfo.Text = exchangeStatusDesc;
        }

        #endregion

        #region 訂單狀態

        #endregion

        #region 出貨紀錄
        if (deal != null)
        {
            //憑證檔不可填出貨紀錄
            if (deal.DeliveryType != (int)DeliveryType.ToHouse)
            {
                divDeliverManage.Visible = false;
            }
            this.ShipStartDate = Convert.ToDateTime(deal.BusinessHourDeliverTimeS);
        }
        if (accounting != null)
        {
            //舊版核銷系統檔次不可填出貨紀錄
            if (accounting.VendorBillingModel == (int)VendorBillingModel.None)
            {
                divDeliverManage.Visible = false;
            }
        }
        #endregion

        lbUnitPrice.Text = (deal != null) ? deal.ItemPrice.ToString("C0") : string.Empty;
        lbReturnSituation.Text = (deal != null)
                                 ? GetReturnSituation((DepartmentTypes)deal.Department,
                                    (firstOrderLog == null) ? DateTime.Now : firstOrderLog.OrderLogCreateTime,
                                    deal.BusinessHourDeliverTimeE.Value.AddDays(1))
                                 : string.Empty;
        //lbCouponNo.Text = GetCouponList(coupons);

        // 判斷若cash_trust_log有值，以其credit_car、pcash...計算，否則以payment_transaction計算
        if (ctls.Count > 0)
        {
            lbPayments.Text = GetPaymentList(ctls, o);
        }
        else
        {
            lbPayments.Text = GetPaymentList(paymentsPT);
        }

        //取得付款資訊
        DateTime? payDate = OrderFacade.GetPaymentCompletedDate(odr.OrderGuid);
        if (payDate == null)
        {
            lbPayStatus.Text = "未付款";
            lbPayDate.Text = "";
        }
        else
        {
            lbPayStatus.Text = "已付款";
            lbPayDate.Text = payDate.Value.ToString("yyyy-MM-dd");
        }

        if (is_masterpass)
        {
            lbMasterPass.Text = "透過MasterPass電子錢包";
        }
        lbChargingSituation.Text = GetChargingSituation(paymentsCS);
        //cbRefundExpiredFee.Checked = (IsExpired) ? true : false;

        if (property != null && property.DeliveryType == (int)DeliveryType.ToShop)
        {
            AllowanceSentBackVisible = false;
            InvoiceSentBackVisible = false;
            EinvoiceWinnerVisible = false;
            lbINItemName.Visible = false;
            lbl_EinvoiceNum.Visible = false;
            panelReceiver.Visible = false;
            cbx_IsCoupon.Checked = true;
        }
        else
        {
            AllowanceSentBackVisible = true;
            InvoiceSentBackVisible = true;
            EinvoiceWinnerVisible = true;
            lbINItemName.Visible = true;
            lbl_EinvoiceNum.Visible = true;

            if (config.EnabledRefundOrExchangeEditReceiverInfo)
            { //啟用退換貨收件地址管理功能
                txtReceiverName.Text = o.MemberName;
                txtReceiverAddress.Text = o.DeliveryAddress;
                panelReceiver.Visible = true;
            }
        }

        #region Set ATM Refund
        //因為{ 超過一年的訂單退貨無法刷退, LinePay超過60天, TaishinPay退貨api未接上之前 } 也要用 ATM 匯退, 所以要顯示ATM 帳戶面板.
        if (Helper.IsFlagSet(odr.OrderStatus, (long)OrderStatus.ATMOrder) ||
            tpp == ThirdPartyPayment.TaishinPay ||
            (tpp == ThirdPartyPayment.LinePay && odr.OrderCreateTime < DateTime.Now.AddDays(-config.LinePayRefundPeriodLimit)) ||
            odr.OrderCreateTime < DateTime.Now.AddDays(-config.NoneCreditRefundPeriod) || o.Installment > 0 ||
             Helper.IsFlagSet(odr.OrderStatus, (long)OrderStatus.FamilyIspOrder) ||
             Helper.IsFlagSet(odr.OrderStatus, (long)OrderStatus.SevenIspOrder)
            )
        {
            cbATM.Visible = cbATM.Checked = Helper.IsFlagSet(odr.OrderStatus, (long)OrderStatus.ATMOrder);
            hdInstallment.Value = o.Installment.ToString();
            pnATM.Visible = true;

            PponOrder po = new PponOrder(new Guid(Request.QueryString["oid"]));
            IEnumerable<SummarizedProductSpec> source = po.GetToHouseProductSummary();

            if (o.Installment > 0 && (!o.IsPartialCancel && !source.Any(x => x.TotalCount != x.ReturnableCount)))
            {
                pnATM.Style.Add("display", "none");
            }
            else
            {
                pnATM.Style.Remove("display");
            }

            lbSi.Text = "new";
            if (AtmRefundCol.Count() > 0)
            {
                if (AtmRefundCol.Last() != null && AtmRefundCol.Last().Status < (int)AtmRefundStatus.AchProcess)
                {
                    lbSi.Text = AtmRefundCol.Last().Si.ToString();
                    tbAcountName.Text = AtmRefundCol.Last().AccountName;
                    tbIdcardNo.Text = AtmRefundCol.Last().Id;
                    tbAccountNo.Text = AtmRefundCol.Last().AccountNumber;
                    tbMobile.Text = AtmRefundCol.Last().Phone;
                    txtBankName.Text = AtmRefundCol.Last().BankCode.Substring(0, 3) + " " + AtmRefundCol.Last().BankName;
                    txtBankBranch.Text = AtmRefundCol.Last().BankCode.Substring(3, 4) + " " + AtmRefundCol.Last().BranchName;
                }

                gvAtmRefund.DataSource = AtmRefundCol.Reverse();
                gvAtmRefund.DataBind();
            }
        }
        #endregion

        #region Set Panel AddServiceLog
        lblIName.Text = odr.MemberName;
        lblIEmail.Text = odr.MemberEmail;
        if (RegExRules.CheckEmail(odr.MemberEmail) == false)
        {
            Member mem = MemberFacade.GetMember(odr.UserId.Value);
            if (mem.IsLoaded)
            {
                lblIEmail.Text = mem.UserEmail;
            }
        }

        if (!string.IsNullOrEmpty(odr.OrderMobile))
            lblIPhone.Text = odr.OrderMobile;
        else
            lblIPhone.Text = odr.OrderPhone;
        lblIOrder.Text = odr.OrderId;
        divAddServiceLog.Visible = false;
        HiddenOrderGuid.Value = odr.OrderGuid.ToString();
        #endregion

        if (odr.Department == null && odr.BuildingGuid == null)
        {
            lOI.ForeColor = System.Drawing.Color.Red;
            lOI.Text = "該會員地址不全，無法顯示訂單明細。請在後台補齊後再試，謝謝！";
        }
    }

    //符合三種退貨條件的其中之一, 則回傳True, 否則回傳False
    private bool IsNoReturn(GroupOrder go, ViewPponDeal deal)
    {
        return (((go.Status & (int)GroupOrderStatus.NoRefund) > 0 && DateTime.Now.Date > deal.BusinessHourOrderTimeS.Date) ||
            ((go.Status & (int)GroupOrderStatus.ExpireNoRefund) > 0 && DateTime.Now.Date > ((DateTime)deal.BusinessHourDeliverTimeE).Date) ||
            ((go.Status & (int)GroupOrderStatus.DaysNoRefund) > 0 && DateTime.Now.Date > deal.BusinessHourOrderTimeE.AddDays(config.GoodsAppreciationPeriod).Date) ||
            ((go.Status & (int)GroupOrderStatus.NoRefundBeforeDays) > 0 && DateTime.Now.Date >= ((DateTime)deal.BusinessHourDeliverTimeS).AddDays(-config.PponNoRefundBeforeDays).Date));
    }

    private string GetStoreExpireDateContent(ViewPponCoupon coupon)
    {
        if (coupon == null)
            return string.Empty;

        if (coupon.StoreCloseDownDate.HasValue && coupon.StoreCloseDownDate.Value < coupon.BusinessHourDeliverTimeE)
            return string.Format("(結束營業{0})", coupon.StoreCloseDownDate.Value.ToString("yyyy/MM/dd"));

        if (coupon.StoreChangedExpireDate.HasValue && coupon.StoreChangedExpireDate.Value < coupon.BusinessHourDeliverTimeE)
            return string.Format("(停止使用{0})", coupon.StoreChangedExpireDate.Value.ToString("yyyy/MM/dd"));

        return string.Empty;
    }

    private string GetSellerExpireDateContent(ViewPponCoupon coupon)
    {
        if (coupon == null)
            return string.Empty;

        if (coupon.SellerCloseDownDate.HasValue && coupon.SellerCloseDownDate.Value < coupon.BusinessHourDeliverTimeE)
            return string.Format("(結束營業{0})", coupon.SellerCloseDownDate.Value.ToString("yyyy/MM/dd"));

        if (coupon.BhChangedExpireDate.HasValue && coupon.BhChangedExpireDate.Value < coupon.BusinessHourDeliverTimeE)
            return string.Format("(停止使用{0})", coupon.BhChangedExpireDate.Value.ToString("yyyy/MM/dd"));

        return string.Empty;
    }

    private string GetReturnSituation(DepartmentTypes d, DateTime returnTime, DateTime validTime)
    {
        if (d == DepartmentTypes.Ppon)
        {
            if (returnTime > validTime)
            {
                IsExpired = true;
                return Phrase.CouponExpired;
            }
            else
            {
                IsExpired = false;
                return Phrase.CouponNotExpired;
            }
        }
        else if (d == DepartmentTypes.PponItem || d == DepartmentTypes.Peauty)
        {
            if (returnTime > validTime)
                return Phrase.GoodsExpired;
            else return Phrase.GoodsNotExpired;
        }
        else return string.Empty;
    }

    //private string GetCouponList(ViewPponCouponCollection coupons)
    //{
    //    string cList = string.Empty;
    //    foreach (var c in coupons)
    //        cList += c.SequenceNumber + "(" + c.CouponCode + ")" + "<br/>";
    //    return cList;
    //}

    private string GetPaymentList(CashTrustLogCollection ctls, Order o)
    {
        string pList = string.Empty;
        int ccash = ctls.Sum(x => x.CreditCard);
        int pcash = ctls.Sum(x => x.Pcash);
        int bcash = ctls.Sum(x => x.Bcash);
        int scash = ctls.Sum(x => x.Scash);
        int pscash = ctls.Sum(x => x.Pscash);
        int generalScash = scash + pscash;
        int acash = ctls.Sum(x => x.Atm);
        int dcash = ctls.Sum(x => x.DiscountAmount);
        int lcash = ctls.Sum(x => x.Lcash);
        int tcash = ctls.Sum(x => x.Tcash);
        int familycash = ctls.Sum(x => x.FamilyIsp);
        int sevencash = ctls.Sum(x => x.SevenIsp);

        if (ccash > 0)
        {
            //加入銀行分期顯示文字
            if (o.IsLoaded)
            {
                if (o.Installment == (int)OrderInstallment.In3Months || o.Installment == (int)OrderInstallment.In6Months || o.Installment == (int)OrderInstallment.In12Months)
                {
                    pList += Resources.Localization.CreditCard + "分期(" + o.Installment + "期) : " + ccash.ToString();
                }
                else
                {
                    pList += Resources.Localization.CreditCard + ": " + ccash.ToString();
                }
                if (o.MobilePayType != null)
                {
                    pList += "(" + (MobilePayType)o.MobilePayType.Value + ")";
                }
            }
            pList += "<br/>";
        }
        if (pcash > 0)
        {
            pList += Resources.Localization.PCash + ": " + pcash.ToString() + "<br/>";
        }
        if (bcash > 0)
        {
            pList += Resources.Localization.BonusPoint + ": " + bcash.ToString() + "<br/>";
        }
        if (generalScash > 0)
        {
            pList += Resources.Localization.SCash + ": " + generalScash.ToString() + "<br/>";
            pList += "　-17Life: " + scash.ToString("N0") + "<br/>";
            pList += "　-Payeasy兌換: " + pscash.ToString("N0") + "<br/>";
        }
        if (acash > 0)
        {
            pList += Resources.Localization.ATM + ": " + acash.ToString() + "<br/>";
        }
        if (dcash > 0)
        {
            pList += Resources.Localization.DiscountCode + ": " + dcash.ToString() + "<br/>";
            var discountData = OrderFacade.GetViewDiscountDetailByOrderGuid(o.Guid);
            foreach (var detail in discountData)
            {
                pList += "折價券名稱: " + detail.Name + "<br/>";
            }
        }
        if (lcash > 0)
        {
            pList += OrderFacade.GetOrderCorrespondingTypeName(o.Guid) + Resources.Localization.LCash + ": " + lcash.ToString() + "<br/>";
        }
        if (tcash > 0 && ctls.Count > 0)
        {
            pList +=
                    Helper.GetEnumDescription((ThirdPartyPayment)ctls.First().ThirdPartyPayment) + ": " + tcash.ToString() + "<br/>";
        }

        if (familycash > 0)
        {
            pList += Resources.Localization.FamilyIsp + ": " + familycash.ToString() + "<br/>";
        }

        if (sevencash > 0)
        {
            pList += Resources.Localization.SevenIsp + ": " + sevencash.ToString() + "<br/>";
        }

        return pList;
    }

    private string GetPaymentList(PaymentTransactionCollection payments)
    {
        string pList = string.Empty;
        int ccash = 0;
        int pcash = 0;
        int bcash = 0;
        int scash = 0;
        int pscash = 0;
        int acash = 0;
        int dcash = 0;
        int tcash = 0;
        ThirdPartyPayment tpp = ThirdPartyPayment.None;

        foreach (var p in payments)
        {
            switch ((PaymentType)p.PaymentType)
            {
                case PaymentType.Creditcard:
                    ccash = Convert.ToInt32(p.Amount);
                    break;
                case PaymentType.PCash:
                    pcash = Convert.ToInt32(p.Amount);
                    break;
                case PaymentType.BonusPoint:
                    bcash = Convert.ToInt32(p.Amount);
                    break;
                case PaymentType.SCash:
                    scash = Convert.ToInt32(p.Amount);
                    break;
                case PaymentType.Pscash:
                    pscash = Convert.ToInt32(p.Amount);
                    break;
                case PaymentType.ATM:
                    acash = Convert.ToInt32(p.Amount);
                    break;
                case PaymentType.DiscountCode:
                    dcash = Convert.ToInt32(p.Amount);
                    break;
                case PaymentType.LinePay:
                    tcash = Convert.ToInt32(p.Amount);
                    tpp = ThirdPartyPayment.LinePay;
                    break;
                case PaymentType.TaishinPay:
                    tcash = Convert.ToInt32(p.Amount);
                    tpp = ThirdPartyPayment.TaishinPay;
                    break;
                default:
                    break;
            }
        }
        if (ccash > 0) pList += Resources.Localization.CreditCard + ": " + ccash.ToString() + "<br/>";
        if (pcash > 0) pList += Resources.Localization.PCash + ": " + pcash.ToString() + "<br/>";
        if (bcash > 0) pList += Resources.Localization.BonusPoint + ": " + bcash.ToString() + "<br/>";

        var creditcard = payments.Where(x => x.PaymentType == (int)PaymentType.Creditcard && (x.Status & 15) == 3);
        if (creditcard.Any() && scash - ccash > 0)
        {
            //有用信用卡+成功+有購物金差額
            pList += Resources.Localization.SCash + ": " + (scash - ccash).ToString() + "<br/>";

        }
        else if (scash > 0)
        {
            //失敗or OTP中 +有購物金
            pList += Resources.Localization.SCash + ": " + scash.ToString() + "<br/>";
        }

        if (pscash > 0) pList += Resources.Localization.PScash + ": " + pscash.ToString() + "<br/>";
        if (acash > 0) pList += Resources.Localization.ATM + ": " + acash.ToString() + "<br/>";
        if (dcash > 0) pList += Resources.Localization.DiscountCode + ": " + dcash.ToString() + "<br/>";
        if (tcash > 0)
        {
            pList += Helper.GetEnumDescription(tpp) + ": " + tcash.ToString() + "<br/>";
        }

        return pList;
    }

    private string GetChargingSituation(ViewOrderMemberPaymentCollection payments)
    {
        string pList = string.Empty;
        string pType = string.Empty;
        string tType = string.Empty;
        string pStatus = string.Empty;

        foreach (var p in payments)
        {
            if (p != null)
            {
                switch ((PaymentType)p.PaymentType)
                {
                    case PaymentType.Creditcard:
                        pType = Resources.Localization.CreditCard;
                        break;
                    case PaymentType.PCash:
                        pType = Resources.Localization.PCash;
                        break;
                    case PaymentType.BonusPoint:
                        pType = Resources.Localization.BonusPoint;
                        break;
                    default:
                        break;
                }
                switch ((PayTransType)p.TransType)
                {
                    case PayTransType.Authorization:
                        if ((PaymentType)p.PaymentType == PaymentType.Creditcard)
                            tType = Resources.Localization.PayTransAuthorization;
                        else
                            tType = Resources.Localization.PayTransCharging;
                        break;
                    case PayTransType.Charging:
                        tType = Resources.Localization.PayTransCharging;
                        break;
                    case PayTransType.Refund:
                        tType = Resources.Localization.PayTransRefund;
                        break;
                    default:
                        break;
                }
                switch ((PayTransPhase)(p.PaymentStatus & (int)PayTransStatusFlag.PhaseMask))
                {
                    case PayTransPhase.Created:
                        pStatus = Resources.Localization.PayTransCreated;
                        break;
                    case PayTransPhase.Requested:
                        pStatus = Resources.Localization.PayTransRequested;
                        break;
                    case PayTransPhase.Canceled:
                        pStatus = Resources.Localization.PayTransCanceled;
                        break;
                    case PayTransPhase.Successful:
                        pStatus = Resources.Localization.PayTransSuccessful;
                        break;
                    case PayTransPhase.Failed:
                        pStatus = Resources.Localization.PayTransFailed;
                        break;
                    default:
                        break;
                }
                pList += pType + tType + pStatus + "<br/>";
            }
        }
        return pList;
    }

    protected string GetCouponStatus(string couponid)
    {
        if (this.GetCashTrustLog != null)
            this.GetCashTrustLog(this, new DataEventArgs<int>(int.Parse(couponid)));

        if (CtLog != null && CtLog.CouponId == int.Parse(couponid))
        {
            if (Helper.IsFlagSet(CtLog.SpecialStatus, TrustSpecialStatus.AtmRefunding))
            {
                return "ATM退款中，無法退貨";
            }

            switch ((TrustStatus)CtLog.Status)
            {
                case TrustStatus.Initial:
                    return "未核銷，可進行退貨";
                case TrustStatus.Trusted:
                    return "未核銷，可進行退貨";
                case TrustStatus.Verified:
                    if (Helper.IsFlagSet(CtLog.SpecialStatus, TrustSpecialStatus.VerificationForced))
                        return "已強制核銷";
                    else if (Helper.IsFlagSet(CtLog.SpecialStatus, TrustSpecialStatus.VerificationLost))
                        return "已核銷，清冊遺失";
                    else
                        return "已核銷，無法再退貨";
                case TrustStatus.Returned:
                    if (Helper.IsFlagSet(CtLog.SpecialStatus, TrustSpecialStatus.ReturnForced))
                        return "已強制退貨";
                    else
                        return "已退購物金，無法再退貨";
                case TrustStatus.Refunded:
                    if (Helper.IsFlagSet(CtLog.SpecialStatus, TrustSpecialStatus.ReturnForced))
                        return "已強制退貨";
                    else
                        return "已轉退現金，無法再退貨";
                case TrustStatus.ATM:
                    return "未付款，無法退貨";
                default:
                    return "查無即時核銷資訊";
            }
        }
        else
        {
            return "查無即時核銷資訊";
        }

    }

    public void ShowHomeDeliveryInfo()
    {
        OrderProductContainer.Visible = true;
        CouponContainer.Visible = false;
        CouponReturnFormContainer.Visible = false;
        btnZipCode.Visible = false;
    }

    public void ShowCouponInfo()
    {
        OrderProductContainer.Visible = false;
        HomeDeliveryReturnFormContainer.Visible = false;
        CouponContainer.Visible = true;
        CouponReturnFormContainer.Visible = true;
        btnZipCode.Visible = true;
    }

    public ListItemCollection GetItemsByInt(int total)
    {
        ListItemCollection lc = new ListItemCollection();
        for (int i = 1; i <= total; i++)
        {
            lc.Add(new ListItem(i.ToString(), i.ToString()));
        }

        if (total < 1)
        {
            lc.Add(new ListItem("0", "0"));
        }
        return lc;
    }

    public void SetOrderStatusList(Dictionary<int, string> data, int selected, bool isCanceling)
    {
        cbS.DataSource = data;
        cbS.DataBind();
        foreach (ListItem l in cbS.Items)
        {
            l.Selected = (selected & int.Parse(l.Value)) > 0;
        }

        if (isCanceling && config.EnableCancelPaidByIspOrder)
            chkIsCanceling.Visible = true;

    }

    public void SetDeliverData(ViewOrderMemberBuildingSeller odr, ViewOrderShipListCollection osCol)
    {
        //先判斷是一般宅配還是超商取貨
        if (odr != null && odr.DeliveryType == (int)DeliveryType.ToHouse && odr.ProductDeliveryType == (int)ProductDeliveryType.Normal)
        {
            //成立單
            if (Helper.IsFlagSet(odr.OrderStatus, OrderStatus.Complete))
            {

                if (osCol.Any() && osCol.FirstOrDefault().OrderShipModifyTime != null)
                {
                    addDeliver.Visible = false; //有出貨資訊, 故隱藏新增功能
                    gvDeliver.Visible = true;
                    gvDeliver.DataSource = osCol;
                    gvDeliver.DataBind();
                }
                else
                {
                    addDeliver.Visible = true;
                    gvDeliver.Visible = false;
                    BindDdlShipCompany(ref ddlAddShipCompanyId, _shipCompanyCol);
                }

            }
            else
            {
                divDeliverManage.Visible = false;
            }
        }
        else if (odr != null && odr.DeliveryType == (int)DeliveryType.ToHouse && odr.ProductDeliveryType == (int)ProductDeliveryType.Wms)
        {
            addDeliver.Visible = false; //隱藏新增功能
            if (Helper.IsFlagSet(odr.OrderStatus, OrderStatus.Complete))
            {

                if (osCol.Any() && osCol.FirstOrDefault().OrderShipModifyTime != null)
                {

                    gvDeliver.Visible = true;
                    gvDeliver.DataSource = osCol;
                    gvDeliver.DataBind();
                }
                else
                {
                    gvDeliver.Visible = false;
                    BindDdlShipCompany(ref ddlAddShipCompanyId, _shipCompanyCol);
                }

            }
            else
            {
                divDeliverManage.Visible = false;
            }
        }
        else
        {
            addDeliver.Visible = false; //超取狀況不需新增功能
            gvDeliver.Visible = true;
            gvDeliver.DataSource = osCol;
            gvDeliver.DataBind();
        }

    }

    public void SetOrderItemList(ViewPponOrderDetailCollection odrCol)
    {
        gvOrderDetail.DataSource = odrCol;
        gvOrderDetail.DataBind();
    }

    private void SetCouponList(ViewPponCouponCollection coupons)
    {
        ViewPponCouponCollection vpcc = new ViewPponCouponCollection();
        foreach (ViewPponCoupon vpc in coupons)
        {
            if (vpcc.Where(x => x.CouponId == vpc.CouponId).ToList().Count <= 0)
            {
                vpcc.Add(vpc);
            }
        }

        gvInStore.DataSource = vpcc;
        gvInStore.DataBind();

        if (vpcc.Count > 0)
        {
            this.CouponDataJson = Newtonsoft.Json.JsonConvert.SerializeObject(
                new
                {
                    SellerId = vpcc.First().SellerId,
                    SellerName = vpcc.First().SellerName,
                    CouponCodes = vpcc.Select(t => t.CouponCode).ToList()
                }
            );
        } 
        else
        {
            this.CouponDataJson = "[]";
        }
    }

    private void SetTrustCouponDetail(CashTrustLogCollection ctls)
    {
        rp_TrustCouponList.DataSource = ctls.OrderByDescending(x => x.Amount);
        rp_TrustCouponList.DataBind();
    }

    public void SetOrderExportLog(string exportlog)
    {
        List<ExportLog> list = new List<ExportLog>();
        if (!string.IsNullOrEmpty(exportlog))
        {
            string[] log_list = exportlog.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
            foreach (var item in log_list)
            {
                int index;
                if ((index = item.IndexOf("_")) != -1)
                    list.Add(new ExportLog(item.Substring(0, index), item.Substring(index + 1)));
            }
        }
        rpt_ExportLog.DataSource = list;
        rpt_ExportLog.DataBind();
    }
    public void SetIsCanceling(bool isCanceling)
    {
        chkIsCanceling.Visible = isCanceling;
    }


    public void SetIDEASOrderData(CompanyUserOrder order)
    {
        pIDEAS.Visible = true;
        IDEASPurchaserName.Text = order.PurchaserName;
        IDEASPurchaserPhone.Text = order.PurchaserPhone;
        IDEASPurchaserEmail.Text = order.PurchaserEmail;
        IDEASPurchaserAddress.Text = order.PurchaserAddress;
    }

    public void BindDdlShipCompany(ref DropDownList ddl, ShipCompanyCollection scCol)
    {
        foreach (ShipCompany sc in scCol.Where(x => x.Status == true).OrderBy(x => x.Sequence))
        {
            ddl.Items.Add(new ListItem(sc.ShipCompanyName, sc.Id.ToString()));
        }
    }

    public void Exit()
    {
        Response.Redirect("order_list.aspx");
    }

    public bool IsUserInRole(string role)
    {
        return Roles.IsUserInRole(role);
    }

    /// <summary>
    /// 設定宅配商品資訊
    /// </summary>
    public void SetHomeDeliveryProductInfo()
    {
        PponOrder odr = new PponOrder(new Guid(Request.QueryString["oid"]));
        IEnumerable<SummarizedProductSpec> source = odr.GetToHouseProductSummary();
        lvOrderProducts.DataSource = source;
        lvOrderProducts.DataBind();

        if (config.EnabledExchangeProcess)
        {
            lbRefundReason.Text = "申請退/換貨原因：";
            btnCreateExchangeLog.Visible = true;
            divExchangeInfo.Visible = true;
            //預設顯示，啟用退換貨功能後宅配隱藏，但憑證卻可以看得見換貨專用欄位?! 感覺是已經作廢的功能。
            oldExchange.Visible = false;
            rpt_RefundLog_old.Visible = false;
        }

        string noReturnMsg;
        if (!ReturnService.AllowReturn(odr.OrderGuid, out noReturnMsg))
        {
            hidNoReturnMsg.Value = noReturnMsg;
            if (!CanRefund)
            {
                btnCreateRefundForm.Enabled = false;
            }
        }

        string noExchangeMsg;
        if (!OrderExchangeUtility.AllowExchange(odr.OrderGuid, out noExchangeMsg))
        {
            hidNoExchangeMsg.Value = noExchangeMsg;
        }


        ViewCouponListMain main = memProv.GetCouponListMainByOid(odr.OrderGuid);
        if (config.EnableCancelPaidByIspOrder &&
            (Helper.IsFlagSet(main.OrderStatus, (int)OrderStatus.FamilyIspOrder) ||
            Helper.IsFlagSet(main.OrderStatus, (int)OrderStatus.SevenIspOrder)) &&
            !Helper.IsFlagSet(main.OrderStatus, (int)OrderStatus.Complete))
        {
            btnCancelOrder.Visible = true;
            btnWithdrawCancelOrder.Visible = true;
        }
    }

    /// <summary>
    /// 設定憑證商品資訊
    /// </summary>
    /// <param name="coupons"></param>
    public void SetCouponInfo(ViewPponCouponCollection coupons)
    {
        ViewPponCouponCollection vpcc = new ViewPponCouponCollection();
        foreach (ViewPponCoupon vpc in coupons)
        {
            if (vpcc.Where(x => x.CouponId == vpc.CouponId).ToList().Count <= 0)
            {
                vpcc.Add(vpc);
            }
        }

        gvInStore.DataSource = vpcc;
        gvInStore.DataBind();
        SetCouponReturnInfo();

        string noReturnMsg;
        if (!ReturnService.AllowReturn(new Guid(Request.QueryString["oid"]), out noReturnMsg))
        {
            hidCouponNoReturnMsg.Value = noReturnMsg;
            if (!CanRefund)
            {
                btnCreateCouponReturnForm.Enabled = false;
            }
        }
    }

    /// <summary>
    /// 設定宅配退貨資訊
    /// </summary>
    public void SetHomeDeliverReturnInfo()
    {
        IList<ReturnFormEntity> returnForms = ReturnFormRepository.FindAllByOrder(new Guid(Request.QueryString["oid"]));
        if (returnForms.Count > 0)
        {
            ReturnFormEntity form = returnForms.Where(x => x.ProgressStatus.EqualsAny(ProgressStatus.Processing, ProgressStatus.AtmQueueing, ProgressStatus.AtmQueueSucceeded))
                .OrderByDescending(x => x.VendorProcessTime).FirstOrDefault();
            if (form == null)
            {
                form = returnForms.OrderByDescending(x => x.VendorProcessTime).FirstOrDefault();
            }
            lblLatestReturnStatus.Text = OrderFacade.GetRefundType(form) + " " + OrderFacade.GetRefundStatus(form);
            lvHomeDeliveryReturnForms.DataSource = returnForms;
            lvHomeDeliveryReturnForms.DataBind();
        }
    }

    /// <summary>
    /// 設定憑證退貨資訊
    /// </summary>
    public void SetCouponReturnInfo()
    {
        IList<ReturnFormEntity> returnForms = ReturnFormRepository.FindAllByOrder(new Guid(Request.QueryString["oid"]));
        if (returnForms.Count > 0)
        {
            ReturnFormEntity form = returnForms.Where(x => x.ProgressStatus.EqualsAny(ProgressStatus.Processing, ProgressStatus.AtmQueueing, ProgressStatus.AtmQueueSucceeded))
                .OrderByDescending(x => x.VendorProcessTime).FirstOrDefault();
            if (form == null)
            {
                form = returnForms.OrderByDescending(x => x.VendorProcessTime).FirstOrDefault();
            }
            lblLatestReturnStatus.Text = OrderFacade.GetRefundType(form) + " " + OrderFacade.GetRefundStatus(form);
            lvCouponReturnForms.DataSource = returnForms;
            lvCouponReturnForms.DataBind();
        }
    }

    public void DownloadZip(byte[] zipFile)
    {
        try
        {
            if (zipFile != null && zipFile.Length > 0)
            {
                Response.Clear();
                Response.ContentType = "application/zip";
                Response.AddHeader("Content-Disposition", "attachment; filename=aaa.zip");
                Response.AddHeader("Content-Length", zipFile.Length.ToString());
                Response.OutputStream.Write(zipFile, 0, zipFile.Length);
                Response.Buffer = true;
                Response.Flush();
                Response.Close();
            }
        }
        catch (Exception ex)
        {

        }
    }

    public void SetOrderUserMemoInfo(OrderUserMemoListCollection orderUserMemos)
    {
        if (!string.IsNullOrEmpty(tbUserMemo.Text.Trim()))
        {
            tbUserMemo.Text = "";
        }
        if (DateTime.Compare(ShipStartDate, DateTime.Today) <= 0)
        {
            lbShipMemo.Visible = true;
        }
        repOrderUserMemo.DataSource = orderUserMemos.OrderByDescending(x => x.CreateTime);
        repOrderUserMemo.DataBind();
    }

    /// <summary>
    /// 設定宅配換貨資訊
    /// </summary>
    public void SetHomeDeliverExchangeInfo()
    {
        var exchangeInfos = OrderExchangeUtility.FindAllExchangeLog(new Guid(Request.QueryString["oid"]));
        if (exchangeInfos.Any())
        {
            lvExchangeInfo.DataSource = exchangeInfos;
            lvExchangeInfo.DataBind();
        }
    }

    public void SetVendorFine()
    {
        var vbslc = _ap.ViewBalanceSheetListGetList(new List<Guid> { BusinessHourGuid }).Where(x => !(x.IsConfirmedReadyToPay && x.EstAmount != 0));

        foreach (var vbsl in vbslc)
        {
            ddlChangeBalanceSheet.Items.Add(new ListItem(GetBalanceSheetFrequenyDesc(vbsl.GenerationFrequency, string.Format("{0}/{1}", vbsl.Year, vbsl.Month), vbsl.IntervalEnd), vbsl.Id.ToString()));
        }

        var vfcc = _ap.VendorFineCategoryGetList().ToList();
        if (!IsToHouse)
            vfcc = vfcc.Where(x => x.Type == (int)VendorFineType.Change).ToList();
        foreach (var vfc in vfcc)
        {
            ddlReason.Items.Add(new ListItem(vfc.Content, vfc.Type.ToString()));
        }
    }

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        memProv = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        _ap = ProviderFactory.Instance().GetProvider<IAccountingProvider>();
        _op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
        _pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        _wp = ProviderFactory.Instance().GetProvider<IWmsProvider>();

        if (!Page.IsPostBack)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["oid"]))
            {
                try { this._oid = new Guid(Request.QueryString["oid"]); }
                catch { this._oid = Guid.Empty; }
                this.OrderId = this._oid;
                ab.ReferenceId = this._oid.ToString();
            }
            this.buildPriorityRbl(ref rblPriority);
            this.Presenter.OnViewInitialized();

            txtBankName.Attributes.Add("autocomplete", "Off");
            txtBankBranch.Attributes.Add("autocomplete", "Off");
        }
        else
        {
            this.BuildSubCategory();
            //解決非同步更新後無法呼叫jquery導致EinvoiceControl產生bug的問題
            ScriptManager.RegisterStartupScript(this, this.GetType(), "call", "init()", true);
        }
        this.Presenter.OnViewLoaded();

        if (!Page.IsPostBack)
        {
            this.hif_SelectMemberEmail.Value = MemberFacade.GetUserEmail(MemberEmail);
        }
    }

    protected void lvOrderProducts_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        if (e.Item.ItemType == ListViewItemType.DataItem)
        {
            var dataSource = (IList<SummarizedProductSpec>)lvOrderProducts.DataSource;

            #region 商品只有一格發票

            if (e.Item.DataItemIndex == 0)
            {
                //第一列要設定 rowspan
                int count = dataSource.Count;
                HtmlTableCell invoiceCell = e.Item.FindControl("InvoiceNumberCell") as HtmlTableCell;
                invoiceCell.RowSpan = count;
                HtmlTableCell allowanceCell = e.Item.FindControl("AllowanceCell") as HtmlTableCell;
                allowanceCell.RowSpan = count;
                HtmlTableCell winningCell = e.Item.FindControl("WinningInvoiceCell") as HtmlTableCell;
                winningCell.RowSpan = count;
                EinvoiceMain einvoice = Presenter
                    .GetEinvoice(this.OrderId)
                    .LastOrDefault();  // 商品訂單只有一張發票
                if (einvoice != null)
                {
                    Literal invoiceNumber = e.Item.FindControl("litInvoiceNumber") as Literal;
                    if (invoiceNumber != null)
                    {
                        EinvoiceMainCollection emc = Presenter.GetEinvoice(this.OrderId);
                        foreach (EinvoiceMain em in emc)
                        {
                            invoiceNumber.Text += em.InvoiceNumber + (em.InvoiceStatus.Value == (int)EinvoiceType.C0501 ? "(作廢)" + "<br/>" : (em.InvoiceStatus.Value == (int)EinvoiceType.D0401) ? "(折讓)" + "<br/>" : string.Empty);
                        }
                    }
                    CheckBox invoiceWon = e.Item.FindControl("chkWinningInvoice") as CheckBox;
                    if (invoiceWon != null)
                    {
                        invoiceWon.Checked = einvoice.InvoiceWinning;
                    }

                    #region 申請紙本發票
                    //Literal invoice_Number = e.Item.FindControl("litInvoiceNumber") as Literal;
                    if (!string.IsNullOrEmpty(einvoice.InvoiceNumber) && einvoice.InvoicePaperedTime == null && einvoice.InvoiceSumAmount > 0 && !einvoice.IsDonateMark && (einvoice.InvoiceStatus ?? 0) == (int)EinvoiceType.C0401 && ((!einvoice.IsIntertemporal && einvoice.CarrierType == (int)CarrierType.None) || einvoice.IsIntertemporal))
                    {
                        var btnRequestInvoice = e.Item.FindControl("btnRequestInvoice") as Button;
                        btnRequestInvoice.Visible = true;
                        if (OrderFacade.IsRequestInvoice(einvoice.InvoiceNumber))
                        {
                            btnRequestInvoice.Text = "已申請紙本";
                            btnRequestInvoice.Enabled = false;
                        }
                    }
                    #endregion
                }
            }
            else
            {
                //非第一列要隱藏
                e.Item.FindControl("InvoiceNumberCell").Visible = false;
                e.Item.FindControl("AllowanceCell").Visible = false;
                e.Item.FindControl("WinningInvoiceCell").Visible = false;
            }

            #endregion


            ListViewDataItem dataItem = (ListViewDataItem)e.Item;
            var productSpec = (SummarizedProductSpec)dataItem.DataItem;

            #region 組 productIds

            HiddenField productIds = (HiddenField)e.Item.FindControl("csvProductIds");
            productIds.Value = string.Join(",", productSpec.ReturnableProductIds);

            #endregion

            #region 塞可退貨下拉選單的數字

            DropDownList returnableSelections = (DropDownList)e.Item.FindControl("returningCountSelections");

            var num = new List<int>();
            for (int i = 0; i <= productSpec.ReturnableCount; i++)
            {
                num.Add(i);
            }

            returnableSelections.DataSource = num;
            returnableSelections.DataBind();
            returnableSelections.SelectedValue = productSpec.ReturnableCount.ToString();

            #endregion

            #region 設定寄出折讓單的連結

            LinkButton link = (LinkButton)e.Item.FindControl("linkSendCreditNoteEmail");
            link.Click += lbReturnDiscountResend_Click;

            #endregion
        }
    }

    protected void lvHomeDeliveryReturnForms_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        if (e.Item.ItemType == ListViewItemType.DataItem)
        {
            ListViewDataItem dataItem = (ListViewDataItem)e.Item;
            var form = (ReturnFormEntity)dataItem.DataItem;

            #region 申請退貨規格

            var litItemSpec = e.Item.FindControl("litItemSpec") as Literal;
            if (litItemSpec != null)
            {
                litItemSpec.Text = form.GetReturnSpec().Replace(Environment.NewLine, "<br/>");
            }

            #endregion 申請退貨規格

            #region 廠商處理進度

            WmsRefundOrder order = _wp.WmsRefundOrderGet(form.Id);
            var litVendorProgress = e.Item.FindControl("litVendorProgress") as Literal;
            if (litVendorProgress != null)
            {
                string vendorStatusDesc;
                vendorStatusDesc = Helper.GetLocalizedEnum(Resources.Localization.ResourceManager, form.VendorProgressStatus) ?? "資料有問題, 請聯絡技術部";

                if (form.VendorProgressStatus == VendorProgressStatus.Unreturnable
                    || form.VendorProgressStatus == VendorProgressStatus.UnreturnableProcessed)
                {
                    vendorStatusDesc += ":<br/>" + form.VendorMemo;
                }
                else if (form.VendorProgressStatus == VendorProgressStatus.CompletedAndRetrievied)
                {
                    vendorStatusDesc += "<br/>" + form.GetCollectedSpec().Replace(Environment.NewLine, "<br/>");
                }
                else if (form.VendorProgressStatus == VendorProgressStatus.ConfirmedForUnArrival)
                {
                    vendorStatusDesc += "<br/>" + form.GetCollectedSpec().Replace(Environment.NewLine, "<br/>") + "(已前往" + Helper.GetEnumDescription((WmsRefundFrom)order.RefundFrom) + "收件)";
                }

                litVendorProgress.Text = vendorStatusDesc;
            }

            //已處理
            var btnUnreturnableProcessed = e.Item.FindControl("btnUnreturnableProcessed") as Button;
            if (btnUnreturnableProcessed != null)
            {
                if (form.ProgressStatus == ProgressStatus.Processing
                    && form.VendorProgressStatus == VendorProgressStatus.Unreturnable)
                {
                    btnUnreturnableProcessed.Visible = true;
                }
            }


            //復原:未完成退款作業前 且 廠商處理進度不為:系統自動退 可使用復原按鈕復原廠商處理進度狀態
            var btnRecover = e.Item.FindControl("btnRecover") as Button;
            if (btnRecover != null)
            {
                if ((form.ProgressStatus == ProgressStatus.Processing ||
                    form.ProgressStatus == ProgressStatus.Unreturnable) &&
                    (form.VendorProgressStatus == VendorProgressStatus.CompletedAndNoRetrieving ||
                    form.VendorProgressStatus == VendorProgressStatus.CompletedAndRetrievied ||
                    form.VendorProgressStatus == VendorProgressStatus.CompletedAndUnShip) &&
                    !IsWms)
                {
                    btnRecover.Visible = true;
                }
            }

            #endregion 廠商處理進度

            #region 退貨進度

            var litProgress = e.Item.FindControl("litProgress") as Literal;
            if (litProgress != null)
            {
                string litProgressDesc = OrderFacade.GetRefundStatus(form);
                string refundDesc;
                switch (form.RefundType)
                {
                    case RefundType.Scash:
                        refundDesc = "退購物金";
                        break;
                    case RefundType.Cash:
                    case RefundType.ScashToCash:
                        refundDesc = "刷退";
                        break;
                    case RefundType.Atm:
                    case RefundType.ScashToAtm:
                        refundDesc = "退ATM";
                        break;
                    case RefundType.Tcash:
                    case RefundType.ScashToTcash:
                        refundDesc = string.Format("退{0}", Helper.GetEnumDescription(form.ThirdPartyPaymentSystem));
                        break;
                    default:
                        refundDesc = "資料有問題, 請聯絡技術部";
                        break;
                }

                if (order.IsLoaded && !string.IsNullOrEmpty(order.ShipInfo))
                {
                    List<LunchKingSite.BizLogic.Models.Wms.WmsRefundTrace.LogisticTraceInfo> shipInfos = new JsonSerializer().Deserialize<LunchKingSite.BizLogic.Models.Wms.WmsRefundTrace>(order.ShipInfo).Info;
                    foreach (LunchKingSite.BizLogic.Models.Wms.WmsRefundTrace.LogisticTraceInfo shipInfo in shipInfos)
                    {
                        litProgressDesc += "<br/>" + shipInfo.ShipId.Replace(Environment.NewLine, "<br/>");
                    }
                }
                litProgress.Text = string.Format("[{0}]{1}{2}", refundDesc, "<br/>", litProgressDesc);
            }
            #endregion 退貨進度

            #region 完成退貨規格

            var litReturnedItemSpec = e.Item.FindControl("litReturnedItemSpec") as Literal;
            if (litReturnedItemSpec != null)
            {
                litReturnedItemSpec.Text = form.GetRefundedSpec().Replace(Environment.NewLine, "<br/>");
            }

            #endregion

            #region 功能按鈕

            //立即退貨
            var btnImmediateRefund = e.Item.FindControl("btnImmediateRefund") as Button;
            if (btnImmediateRefund != null)
            {
                if (((form.ProgressStatus == ProgressStatus.Processing
                    || form.ProgressStatus == ProgressStatus.Retrieving
                    || form.ProgressStatus == ProgressStatus.ConfirmedForCS
                    || form.ProgressStatus == ProgressStatus.RetrieveToPC
                    || form.ProgressStatus == ProgressStatus.RetrieveToCustomer
                    || form.ProgressStatus == ProgressStatus.ConfirmedForVendor
                    || form.ProgressStatus == ProgressStatus.ConfirmedForUnArrival)
                    && !form.ReturnFormRefunds.Any(x => x.IsInRetry))
                    && form.RefundType != RefundType.ScashToAtm
                    && form.RefundType != RefundType.ScashToCash
                    && form.RefundType != RefundType.ScashToTcash)
                {

                    btnImmediateRefund.Visible = true;
                    btnImmediateRefund.Enabled = CanRefund;
                }
            }

            //是否走作廢
            bool isCancel = form.CreditNoteType == (int)AllowanceStatus.None;

            //紙本折讓單已回
            var btnReceivedCreditNote = e.Item.FindControl("btnReceivedCreditNote") as Button;
            if (btnReceivedCreditNote != null)
            {
                btnReceivedCreditNote.Visible = (form.ProgressStatus == ProgressStatus.Processing ||
                                                 form.ProgressStatus == ProgressStatus.Retrieving ||
                                                 form.ProgressStatus == ProgressStatus.ConfirmedForCS ||
                                                 form.ProgressStatus == ProgressStatus.RetrieveToPC ||
                                                 form.ProgressStatus == ProgressStatus.RetrieveToCustomer ||
                                                 form.ProgressStatus == ProgressStatus.ConfirmedForVendor ||
                                                 form.ProgressStatus == ProgressStatus.ConfirmedForUnArrival ||
                                                 form.IsCreditNoteReceived) &&
                                                IsPaperAllowance && !isCancel;
                btnReceivedCreditNote.Enabled = !form.IsCreditNoteReceived &&
                                                CanRefund;
            }

            //顯示紙本/電子折讓
            var litAllowanceDesc = e.Item.FindControl("litAllowanceDesc") as Literal;
            if (litAllowanceDesc != null)
            {
                litAllowanceDesc.Text = form.CreditNoteType.Equals(0)
                    ? GetAllowanceDesc(
                        (form.ProgressStatus == ProgressStatus.Completed || form.ProgressStatus == ProgressStatus.Canceled) &&
                        !form.IsCreditNoteReceived && IsPaperAllowance
                        ? null
                        : (bool?)IsPaperAllowance)
                    : GetAllowanceDesc(form.CreditNoteType);
            }

            //紙本折讓改用電子折讓
            var btnChangeToElcAllowance = e.Item.FindControl("btnChangeToElcAllowance") as Button;
            if (btnChangeToElcAllowance != null)
            {
                btnChangeToElcAllowance.Visible = form.ProgressStatus == ProgressStatus.Processing &&
                                                  IsPaperAllowance &&
                                                  !form.IsCreditNoteReceived && !isCancel;
                btnChangeToElcAllowance.Enabled = IsPaperAllowance &&
                                                  IsEnableElecAllowance &&
                                                  CanRefund;
            }

            //電子折讓改用紙本折讓
            var btnChangeToPaperAllowance = e.Item.FindControl("btnChangeToPaperAllowance") as Button;
            if (btnChangeToPaperAllowance != null)
            {
                btnChangeToPaperAllowance.Visible = form.ProgressStatus == ProgressStatus.Processing &&
                                                    !IsPaperAllowance && !isCancel;
                btnChangeToPaperAllowance.Enabled = !IsPaperAllowance &&
                                                    CanRefund;
            }

            //取消退貨單
            var btnCancel = e.Item.FindControl("btnCancel") as Button;
            if (btnCancel != null)
            {
                btnCancel.Visible = form.IsCancelable();
                btnCancel.Enabled = CanCancelRefund;
            }

            //已更新資料
            var btnUpdateAtmRefund = e.Item.FindControl("btnUpdateAtmRefund") as Button;
            if (btnUpdateAtmRefund != null)
            {
                if (form.ProgressStatus == ProgressStatus.AtmFailed)
                {
                    btnUpdateAtmRefund.Visible = true;
                    btnUpdateAtmRefund.Enabled = CanRefund;
                }
            }

            //購物金轉現金
            var btnScashToCash = e.Item.FindControl("btnScashToCash") as Button;
            if (btnScashToCash != null)
            {
                string dummy;
                if (form.CanChangeToCashRefund(out dummy))
                {
                    btnScashToCash.Visible = true;
                    btnScashToCash.Enabled = CanRefund;
                }

            }
            //轉退購物金
            var btnCashToSCash = e.Item.FindControl("btnCashToSCash") as Button;
            if (btnCashToSCash != null)
            {
                string dummy2;
                if (form.CanChangeToSCashRefund(out dummy2))
                {
                    btnCashToSCash.Visible = true;
                    btnCashToSCash.Enabled = CanRefund;
                }
            }

            //人工匯退
            var btnArtificialRefund = e.Item.FindControl("btnArtificialRefund") as Button;
            if (btnArtificialRefund != null)
            {
                CtAtmRefund car = OrderFacade.GetCtAtmRefundByOid(new Guid(Request.QueryString["oid"]));
                if (car.Status == (int)AtmRefundStatus.AchSend || car.Status == (int)AtmRefundStatus.AchProcess
                 || car.Status == (int)AtmRefundStatus.RefundFail)
                {
                    btnArtificialRefund.Visible = true;
                    btnArtificialRefund.Enabled = CanRefund;
                }
            }

            //由[退購物金轉刷退]回復為[退購物金完成]狀態
            var btnRecoverToRefundScash = e.Item.FindControl("btnRecoverToRefundScash") as Button;
            if (btnRecoverToRefundScash != null)
            {
                if ((form.RefundType == RefundType.ScashToCash ||
                    form.RefundType == RefundType.ScashToTcash) &&
                    form.ProgressStatus == ProgressStatus.Processing)
                {
                    btnRecoverToRefundScash.Visible = true;
                    btnRecoverToRefundScash.Enabled = CanRefund;
                }
            }

            //編輯退貨收件人
            var btnEditReceiver = e.Item.FindControl("btnEditReceiver") as Button;
            if (btnEditReceiver != null)
            {
                if (form.ProgressStatus == ProgressStatus.Processing && !form.ReturnFormRefunds.Any(x => x.IsInRetry))
                {
                    btnEditReceiver.Visible = true;
                }
            }
            #endregion 功能按鈕

            #region 結案狀態

            var litFinishDescription = e.Item.FindControl("litFinishDescription") as Literal;
            litFinishDescription.Text = OrderFacade.GetFinishStatusDescription(form);

            #endregion 結案狀態

            #region 折讓單提醒

            var hidRequireCreditNote = e.Item.FindControl("hidRequireCreditNote") as HtmlInputHidden;
            hidRequireCreditNote.Value = ReturnService.RequireCreditNote(form)
                                             ? "1"
                                             : "0";

            #endregion 折讓單提醒
        }
    }

    protected void lvHomeDeliveryReturnForms_ItemCommand(object sender, ListViewCommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case "ImmediateRefund":
                if (ImmediateRefund != null)
                {
                    int returnFormId = int.Parse(e.CommandArgument.ToString());
                    ImmediateRefund(sender, new DataEventArgs<int>(returnFormId));
                }
                break;
            case "ReceivedCreditNote":
                if (ReceivedCreditNote != null)
                {
                    int returnFormId = int.Parse(e.CommandArgument.ToString());
                    ReceivedCreditNote(sender, new DataEventArgs<int>(returnFormId));
                }
                break;
            case "ChangeToElcAllowance":
                if (ChangeToElcAllowance != null)
                {
                    int returnFormId = int.Parse(e.CommandArgument.ToString());
                    ChangeToElcAllowance(sender, new DataEventArgs<int>(returnFormId));
                }
                break;
            case "ChangeToPaperAllowance":
                if (ChangeToPaperAllowance != null)
                {
                    int returnFormId = int.Parse(e.CommandArgument.ToString());
                    ChangeToPaperAllowance(sender, new DataEventArgs<int>(returnFormId));
                }
                break;
            case "CancelReturn":
                if (CancelReturnForm != null)
                {
                    int returnFormId = int.Parse(e.CommandArgument.ToString());
                    CancelReturnForm(sender, new DataEventArgs<int>(returnFormId));
                }
                break;
            case "UpdateAtmRefund":
                if (UpdateAtmRefund != null)
                {
                    int returnFormId = int.Parse(e.CommandArgument.ToString());
                    UpdateAtmRefund(sender, new DataEventArgs<int>(returnFormId));
                }
                break;
            case "UnreturnableProcessed":
                if (ProcessedVendorUnreturnable != null)
                {
                    int returnFormId = int.Parse(e.CommandArgument.ToString());
                    ProcessedVendorUnreturnable(sender, new DataEventArgs<int>(returnFormId));
                }
                break;
            case "Recover":
                if (RecoverVendorProgress != null)
                {
                    int returnFormId = int.Parse(e.CommandArgument.ToString());
                    RecoverVendorProgress(sender, new DataEventArgs<int>(returnFormId));
                }
                break;
            case "ScashToCash":
                {
                    int returnFormId = int.Parse(e.CommandArgument.ToString());
                    string cannotReason;
                    ReturnFormEntity form = ReturnFormRepository.FindById(returnFormId);
                    if (form.CanChangeToCashRefund(out cannotReason))
                    {
                        if (SCashToCash != null)
                        {
                            SCashToCash(sender, new DataEventArgs<int>(returnFormId));
                        }
                    }
                    else
                    {
                        alertMessage = cannotReason;
                    }
                }
                break;
            case "CashToSCash":
                {
                    int returnFormId = int.Parse(e.CommandArgument.ToString());
                    string cannotReason;
                    ReturnFormEntity form = ReturnFormRepository.FindById(returnFormId);
                    if (form.CanChangeToSCashRefund(out cannotReason))
                    {
                        if (CashToSCash != null)
                        {
                            CashToSCash(sender, new DataEventArgs<int>(returnFormId));
                        }
                    }
                    else
                    {
                        alertMessage = cannotReason;
                    }
                }
                break;
            case "ArtificialRefund":
                {
                    if (ArtificialRefund != null)
                    {
                        int returnFormId = int.Parse(e.CommandArgument.ToString());
                        ArtificialRefund(sender, new DataEventArgs<int>(returnFormId));
                    }
                }
                break;
            case "RecoverToRefundScash":
                {
                    if (RecoverToRefundScash != null)
                    {
                        int returnFormId = int.Parse(e.CommandArgument.ToString());
                        RecoverToRefundScash(sender, new DataEventArgs<int>(returnFormId));
                    }
                }
                break;
            case "EditReceiver":
                lvHomeDeliveryReturnForms.EditIndex = e.Item.DataItemIndex;
                SetHomeDeliverReturnInfo();
                break;
            case "CancelReceiver":
                lvHomeDeliveryReturnForms.EditIndex = -1;
                SetHomeDeliverReturnInfo();
                break;
            case "UpdateReceiver":
                {
                    TextBox txtReceiverNameR = (TextBox)e.Item.FindControl("txtReceiverNameR");
                    TextBox txtReceiverAddressR = (TextBox)e.Item.FindControl("txtReceiverAddressR");

                    if (txtReceiverNameR != null && txtReceiverAddressR != null)
                    {
                        var returnFormReceiverArgs = new ReturnFormReceiverArgs
                        {
                            ReturnFormId = int.Parse(e.CommandArgument.ToString()),
                            ReceiverName = txtReceiverNameR.Text,
                            ReceiverAddress = txtReceiverAddressR.Text
                        };

                        UpdateReturnFormReceiver(sender, returnFormReceiverArgs);
                    }
                    else
                    {
                        alertMessage = "系統錯誤!";
                    }
                }
                lvHomeDeliveryReturnForms.EditIndex = -1;
                SetHomeDeliverReturnInfo();
                break;
            default:
                break;
        }

        AjaxControlToolkit.ToolkitScriptManager.RegisterStartupScript(this, this.GetType(), "unblockUI", "$.unblockUI();", true);
    }

    protected void lvCouponReturnForms_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        if (e.Item.ItemType == ListViewItemType.DataItem)
        {
            ListViewDataItem dataItem = (ListViewDataItem)e.Item;
            var form = (ReturnFormEntity)dataItem.DataItem;

            #region 申請退貨規格

            var litItemSpec = e.Item.FindControl("litItemSpec") as Literal;
            if (litItemSpec != null)
            {
                litItemSpec.Text = form.GetReturnSpec().Replace(Environment.NewLine, "<br/>");
            }

            #endregion 申請退貨規格

            #region 退貨進度

            var litProgress = e.Item.FindControl("litProgress") as Literal;

            string litProgressDesc = OrderFacade.GetRefundStatus(form);

            string refundDesc;
            switch (form.RefundType)
            {
                case RefundType.Scash:
                    refundDesc = "退購物金";
                    break;
                case RefundType.Cash:
                    refundDesc = "刷退";
                    break;
                case RefundType.Atm:
                    refundDesc = "退ATM";
                    break;
                case RefundType.Tcash:
                    refundDesc = string.Format("退{0}", Helper.GetEnumDescription(form.ThirdPartyPaymentSystem));
                    break;
                case RefundType.ScashToAtm:
                    refundDesc = "購物金轉退ATM";
                    break;
                case RefundType.ScashToCash:
                    refundDesc = "購物金轉刷退";
                    break;
                case RefundType.ScashToTcash:
                    refundDesc = string.Format("購物金轉退{0}", Helper.GetEnumDescription(form.ThirdPartyPaymentSystem));
                    break;
                default:
                    refundDesc = "資料有問題, 請聯絡技術部";
                    break;
            }

            litProgress.Text = string.Format("[{0}]{1}{2}", refundDesc, "<br/>", litProgressDesc);

            #endregion 退貨進度

            #region 完成退貨規格

            if (form.ProgressStatus == ProgressStatus.Completed ||
                form.ProgressStatus == ProgressStatus.CompletedWithCreditCardQueued)
            {
                var litReturnedItemSpec = e.Item.FindControl("litReturnedItemSpec") as Literal;
                litReturnedItemSpec.Text = form.GetRefundedSpec().Replace(Environment.NewLine, "<br/>");
            }


            #endregion

            #region 功能按鈕

            //立即退貨
            var btnImmediateRefund = e.Item.FindControl("btnImmediateRefund") as Button;
            if ((form.ProgressStatus == ProgressStatus.Processing && !form.ReturnFormRefunds.Any(x => x.IsInRetry))
                && form.RefundType != RefundType.ScashToAtm
                && form.RefundType != RefundType.ScashToCash
                && form.RefundType != RefundType.ScashToTcash)
            {
                btnImmediateRefund.Visible = true;
                btnImmediateRefund.Enabled = CanRefund;
            }

            var isInvoiceCreate = ReturnService.IsInvoiceCreate(form);
            //是否走作廢
            bool isCancel = form.CreditNoteType == (int)AllowanceStatus.None;

            //折讓單已回
            var btnReceivedCreditNote = e.Item.FindControl("btnReceivedCreditNote") as Button;
            btnReceivedCreditNote.Visible = (form.ProgressStatus == ProgressStatus.Processing ||
                                             form.IsCreditNoteReceived) &&
                                            IsPaperAllowance &&
                                            isInvoiceCreate && !isCancel;
            btnReceivedCreditNote.Enabled = !form.IsCreditNoteReceived &&
                                            CanRefund;
            //顯示紙本/電子折讓
            var litAllowanceDesc = e.Item.FindControl("litAllowanceDesc") as Literal;
            litAllowanceDesc.Text = form.CreditNoteType.Equals(0) ? GetAllowanceDesc((form.ProgressStatus == ProgressStatus.Completed || form.ProgressStatus == ProgressStatus.Canceled) && !form.IsCreditNoteReceived && IsPaperAllowance ? null : (bool?)IsPaperAllowance) : GetAllowanceDesc(form.CreditNoteType);

            //紙本折讓改用電子折讓
            var btnChangeToElcAllowance = e.Item.FindControl("btnChangeToElcAllowance") as Button;
            btnChangeToElcAllowance.Visible = form.ProgressStatus == ProgressStatus.Processing &&
                                              IsPaperAllowance &&
                                              !form.IsCreditNoteReceived && !isCancel;
            btnChangeToElcAllowance.Enabled = IsPaperAllowance &&
                                              IsEnableElecAllowance &&
                                              CanRefund;

            //電子折讓改用紙本折讓
            var btnChangeToPaperAllowance = e.Item.FindControl("btnChangeToPaperAllowance") as Button;
            btnChangeToPaperAllowance.Visible = form.ProgressStatus == ProgressStatus.Processing &&
                                                !IsPaperAllowance &&
                                                isInvoiceCreate && !isCancel;
            btnChangeToPaperAllowance.Enabled = !IsPaperAllowance &&
                                                CanRefund;

            //取消退貨單
            var btnCancel = e.Item.FindControl("btnCancel") as Button;
            btnCancel.Visible = form.IsCancelable();
            btnCancel.Enabled = CanCancelRefund;

            //已更新資料
            var btnUpdateAtmRefund = e.Item.FindControl("btnUpdateAtmRefund") as Button;
            if (form.ProgressStatus == ProgressStatus.AtmFailed)
            {
                btnUpdateAtmRefund.Visible = true;
                btnUpdateAtmRefund.Enabled = CanRefund;
            }

            //購物金轉現金
            var btnScashToCash = e.Item.FindControl("btnScashToCash") as Button;
            string dummy;
            if (form.CanChangeToCashRefund(out dummy))
            {
                btnScashToCash.Visible = true;
                btnScashToCash.Enabled = CanRefund;
            }

            //購物金轉現金
            var btnCashToSCash = e.Item.FindControl("btnCashToSCash") as Button;
            string dummy2;
            if (form.CanChangeToSCashRefund(out dummy2))
            {
                btnCashToSCash.Visible = true;
                btnCashToSCash.Enabled = CanRefund;
            }

            //人工匯退
            var btnArtificialRefund = e.Item.FindControl("btnArtificialRefund") as Button;
            CtAtmRefund car = OrderFacade.GetCtAtmRefundByOid(new Guid(Request.QueryString["oid"]));
            if (car.Status == (int)AtmRefundStatus.AchSend || car.Status == (int)AtmRefundStatus.AchProcess
            || car.Status == (int)AtmRefundStatus.RefundFail)
            {
                btnArtificialRefund.Visible = true;
                btnArtificialRefund.Enabled = CanRefund;
            }

            //由[退購物金轉刷退]回復為[退購物金完成]狀態
            var btnRecoverToRefundScash = e.Item.FindControl("btnRecoverToRefundScash") as Button;
            if ((form.RefundType == RefundType.ScashToCash ||
                form.RefundType == RefundType.ScashToTcash) &&
                form.ProgressStatus == ProgressStatus.Processing)
            {
                btnRecoverToRefundScash.Visible = true;
                btnRecoverToRefundScash.Enabled = CanRefund;
            }

            #endregion 功能按鈕

            #region 結案狀態

            var litFinishDescription = e.Item.FindControl("litFinishDescription") as Literal;
            litFinishDescription.Text = OrderFacade.GetFinishStatusDescription(form);

            #endregion 結案狀態

            #region 折讓單提醒

            var hidRequireCreditNote = e.Item.FindControl("hidRequireCreditNote") as HtmlInputHidden;
            hidRequireCreditNote.Value = ReturnService.RequireCreditNote(form)
                                             ? "1"
                                             : "0";

            #endregion 折讓單提醒
        }
    }

    protected void lvCouponReturnForms_ItemCommand(object sender, ListViewCommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case "ImmediateRefund":
                if (ImmediateRefund != null)
                {
                    int returnFormId = int.Parse(e.CommandArgument.ToString());
                    ImmediateRefund(sender, new DataEventArgs<int>(returnFormId));
                }
                break;
            case "ReceivedCreditNote":
                if (ReceivedCreditNote != null)
                {
                    int returnFormId = int.Parse(e.CommandArgument.ToString());
                    ReceivedCreditNote(sender, new DataEventArgs<int>(returnFormId));
                }
                break;
            case "ChangeToElcAllowance":
                if (ChangeToElcAllowance != null)
                {
                    int returnFormId = int.Parse(e.CommandArgument.ToString());
                    ChangeToElcAllowance(sender, new DataEventArgs<int>(returnFormId));
                }
                break;
            case "ChangeToPaperAllowance":
                if (ChangeToPaperAllowance != null)
                {
                    int returnFormId = int.Parse(e.CommandArgument.ToString());
                    ChangeToPaperAllowance(sender, new DataEventArgs<int>(returnFormId));
                }
                break;
            case "CancelReturn":
                if (CancelReturnForm != null)
                {
                    int returnFormId = int.Parse(e.CommandArgument.ToString());
                    CancelReturnForm(sender, new DataEventArgs<int>(returnFormId));
                }
                break;
            case "UpdateAtmRefund":
                if (UpdateAtmRefund != null)
                {
                    int returnFormId = int.Parse(e.CommandArgument.ToString());
                    UpdateAtmRefund(sender, new DataEventArgs<int>(returnFormId));
                }
                break;
            case "ScashToCash":
                {
                    int returnFormId = int.Parse(e.CommandArgument.ToString());
                    string cannotReason;
                    ReturnFormEntity form = ReturnFormRepository.FindById(returnFormId);
                    if (form.CanChangeToCashRefund(out cannotReason))
                    {
                        if (SCashToCash != null)
                        {

                            SCashToCash(sender, new DataEventArgs<int>(returnFormId));
                        }
                    }
                    else
                    {
                        alertMessage = cannotReason;
                    }
                }
                break;
            case "CashToSCash":
                {
                    int returnFormId = int.Parse(e.CommandArgument.ToString());
                    string cannotReason;
                    ReturnFormEntity form = ReturnFormRepository.FindById(returnFormId);
                    if (form.CanChangeToSCashRefund(out cannotReason))
                    {
                        if (CashToSCash != null)
                        {
                            CashToSCash(sender, new DataEventArgs<int>(returnFormId));
                        }
                    }
                    else
                    {
                        alertMessage = cannotReason;
                    }
                }
                break;
            case "ArtificialRefund":
                {
                    if (ArtificialRefund != null)
                    {
                        int returnFormId = int.Parse(e.CommandArgument.ToString());
                        ArtificialRefund(sender, new DataEventArgs<int>(returnFormId));
                    }

                }
                break;

            case "RecoverToRefundScash":
                {
                    if (RecoverToRefundScash != null)
                    {
                        int returnFormId = int.Parse(e.CommandArgument.ToString());
                        RecoverToRefundScash(sender, new DataEventArgs<int>(returnFormId));
                    }
                }
                break;
            default:
                break;

        }

        AjaxControlToolkit.ToolkitScriptManager.RegisterStartupScript(this, this.GetType(), "unblockUI", "$.unblockUI();", true);
    }

    protected void repOrderUserMemo_ItemCommand(object sender, RepeaterCommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case "DeleteUserMemo":
                if (OrderUserMemoDelete != null)
                {
                    int orderUserMemoId;
                    if (int.TryParse(e.CommandArgument.ToString(), out orderUserMemoId))
                    {
                        OrderUserMemoDelete(this, new DataEventArgs<int>(orderUserMemoId));
                    }
                    else
                    {
                        alertMessage = "訂單備註Id有誤";
                        return;
                    }
                }
                break;

            default:
                break;
        }
    }

    protected void lvExchangeInfo_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        if (e.Item.ItemType == ListViewItemType.DataItem)
        {
            var dataItem = (ListViewDataItem)e.Item;
            var info = (OrderReturnList)dataItem.DataItem;

            var litReason = e.Item.FindControl("litReason") as Literal;
            if (litReason != null) litReason.Text = string.IsNullOrEmpty(info.Reason) ? "" : info.Reason.Replace("\n", "<br/>");

            var litVendorProgress = e.Item.FindControl("litVendorProgress") as Literal;
            if (litVendorProgress != null) litVendorProgress.Text = GetExchangeVendorProgressStatusDesc(info.VendorProgressStatus);

            if (info.VendorProgressStatus == (int)ExchangeVendorProgressStatus.ExchangeCompleted &&
                info.Status != (int)OrderReturnStatus.ExchangeSuccess &&
                info.Status != (int)OrderReturnStatus.ExchangeCancel &&
                info.Status != (int)OrderReturnStatus.ExchangeFailure)
            {
                var btnCompleteExchange = e.Item.FindControl("btnCompleteExchange") as Button;
                if (btnCompleteExchange != null) btnCompleteExchange.Visible = true;
            }

            var litStatus = e.Item.FindControl("litStatus") as Literal;
            if (litStatus != null) litStatus.Text = GetOrderReturnStatusDesc(info.Status);

            var litFinishTime = e.Item.FindControl("litFinishTime") as Literal;
            if (info.Status == (int)OrderReturnStatus.ExchangeSuccess ||
                info.Status == (int)OrderReturnStatus.ExchangeCancel ||
                info.Status == (int)OrderReturnStatus.ExchangeFailure)
            {
                var btnCancelExchange = e.Item.FindControl("btnCancelExchange") as Button;
                if (btnCancelExchange != null) btnCancelExchange.Visible = false;

                var btnExchangeMessage = e.Item.FindControl("btnExchangeMessage") as Button;
                if (btnExchangeMessage != null) btnExchangeMessage.Visible = false;

                var btnEditExchangeReceiver = e.Item.FindControl("btnEditExchangeReceiver") as Button;
                if (btnEditExchangeReceiver != null) btnEditExchangeReceiver.Visible = false;

                if (info.ModifyTime != null && litFinishTime != null)
                {
                    litFinishTime.Text = info.ModifyTime.Value.ToString("yyyy/MM/dd HH:mm");
                }
            }
            if (info.OrderShipId.HasValue)
            {
                var btnUpdateOrderShip = e.Item.FindControl("btnUpdateOrderShip") as Button;
                if (btnUpdateOrderShip != null) btnUpdateOrderShip.Visible = true;
            }
        }
    }

    protected void lvExchangeInfo_ItemCommand(object sender, ListViewCommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case "CompleteExchange":
                if (CompleteExchangeLog != null)
                {
                    int exchangeLogId = int.Parse(e.CommandArgument.ToString());
                    CompleteExchangeLog(sender, new DataEventArgs<int>(exchangeLogId));
                }
                break;
            case "CancelExchange":
                if (CancelExchangeLog != null)
                {
                    int exchangeLogId = int.Parse(e.CommandArgument.ToString());
                    CancelExchangeLog(sender, new DataEventArgs<int>(exchangeLogId));
                }
                break;
            case "EditReceiver":
                lvExchangeInfo.EditIndex = e.Item.DataItemIndex;
                SetHomeDeliverExchangeInfo();
                break;
            case "UpdateReceiver":
                {
                    TextBox txtReceiverNameE = (TextBox)e.Item.FindControl("txtReceiverNameE");
                    TextBox txtReceiverAddressE = (TextBox)e.Item.FindControl("txtReceiverAddressE");

                    if (txtReceiverNameE != null && txtReceiverAddressE != null)
                    {
                        var updateExchangeLogArgs = new UpdateExchangeLogArgs
                        {
                            ExchangeLogId = int.Parse(e.CommandArgument.ToString()),
                            ReceiverName = txtReceiverNameE.Text,
                            ReceiverAddress = txtReceiverAddressE.Text
                        };

                        UpdateExchangeLog(sender, updateExchangeLogArgs);
                    }
                    else
                    {
                        alertMessage = "系統錯誤!";
                    }
                }
                lvExchangeInfo.EditIndex = -1;
                SetHomeDeliverExchangeInfo();
                break;
            case "CancelReceiver":
                lvExchangeInfo.EditIndex = -1;
                SetHomeDeliverExchangeInfo();
                break;
        }
    }

    #region event handlers

    protected void CreateRefundForm_Click(object sender, EventArgs e)
    {
        string refundReason = txtRefundReason.Text;

        if (ReturnService.HasProcessingReturnForm(this.OrderId))
        {
            alertMessage = "仍有退款中之退貨單，無法再申請。";
            return;
        }

        if (string.IsNullOrWhiteSpace(refundReason))
        {
            alertMessage = "請填寫原因，以供廠商參考。";
            return;
        }

        if (ReturnService.IsInvoice2To3(this.OrderId))
        {
            alertMessage = "發票二聯改三聯尚未完成，無法申請退貨";
            return;
        }

        //1. find items in listview
        //2. create refund form check
        List<int> orderProductIds = new List<int>();
        foreach (ListViewDataItem item in lvOrderProducts.Items)
        {
            string rtnCount = ((DropDownList)item.FindControl("returningCountSelections")).SelectedValue;
            string prodIds = ((HiddenField)item.FindControl("csvProductIds")).Value;

            int count = int.Parse(rtnCount);
            IEnumerable<string> prods = prodIds.Split(',');

            prods
                .Take(count)
                .ForEach(id => orderProductIds.Add(int.Parse(id)));
        }

        if (ReturnService.HasExchangingLog(this.OrderId, orderProductIds.Count))
        {
            alertMessage = "仍有換貨處理中訂單，不允許全部退貨。";
            return;
        }

        var args = new ReturnFormArgs
        {
            OrderGuid = this.OrderId,
            OrderProductIds = orderProductIds,
            RefundReason = refundReason,
            RefundSCash = chkSCash.Checked,
            ReceiverName = txtReceiverName.Text,
            ReceiverAddress = txtReceiverAddress.Text,
            Bid = BusinessHourGuid,
            ChannelAgent = ChannelAgent
        };

        if (CreateReturnForm != null)
        {
            CreateReturnForm(this, args);
        }
    }

    protected void CreateCouponReturnForm_Click(object sender, EventArgs e)
    {
        List<int> couponIds = new List<int>();
        foreach (GridViewRow row in gvInStore.Rows)
        {
            HiddenField hfCouponId = (HiddenField)row.FindControl("hfCouponId");
            CheckBox chk = (CheckBox)row.FindControl("cbCancel");
            Label lblReservationStatus = (Label)row.FindControl("lblReservationStatus");
            if (chk.Checked)
            {
                couponIds.Add(int.Parse(hfCouponId.Value));
            }
        }

        var args = new CouponReturnFormArgs
        {
            OrderGuid = this.OrderId,
            CouponIds = couponIds,
            RefundReason = txtCouponRefundReason.Text,
            RefundSCash = chkCouponReturnSCash.Checked
        };

        if (CreateCouponReturnForm != null)
        {
            CreateCouponReturnForm(this, args);
        }
    }

    public void ReturnFormCreated(DeliveryType deliveryType)
    {
        switch (deliveryType)
        {
            case DeliveryType.ToHouse:
                txtRefundReason.Text = string.Empty;
                chkSCash.Checked = true;
                break;
            case DeliveryType.ToShop:
                chkCouponReturnSCash.Checked = true;
                txtCouponRefundReason.Text = string.Empty;
                break;
            default:
                break;
        }
    }

    protected void btRefer_Click(object sender, EventArgs e)
    {
        if (CreditcardReferSet != null)
        {
            CreditcardReferSet(sender, e);
        }
    }
    protected void btReferFail_Click(object sender, EventArgs e)
    {
        if (CreditcardReferSet != null)
        {
            CreditcardReferSet(sender, e);
        }
    }

    protected void AddOrderUserMemo_Click(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(tbUserMemo.Text.Trim()))
        {
            alertMessage = "請輸入訂單備註!";
            return;
        }

        var userMemo = new OrderUserMemoList
        {
            OrderGuid = this.OrderId,
            DealType = (int)VbsDealType.Ppon,
            UserMemo = tbUserMemo.Text.Trim(),
            CreateId = Page.User.Identity.Name,
            CreateTime = DateTime.Now
        };

        if (OrderUserMemoSet != null)
        {
            OrderUserMemoSet(this, new DataEventArgs<OrderUserMemoList>(userMemo));
        }
    }

    protected void UpdateClick(object sender, CommandEventArgs e)
    {
        UpdatableOrderInfo oi = new UpdatableOrderInfo();
        oi.CustomerMobile = tbM.Text;
        oi.CustomerPhone = tbP.Text;
        oi.DeliveryAddress = tbDA.Text;
        oi.ReviewReply = tbRR.Text; //評鑑回覆(目前隱藏於前台)

        #region Invoice Info 發票 二/三聯資訊、載具

        oi.EinvoiceAddress = invCtrl.BuyerAddress;
        oi.EinvoiceName = invCtrl.BuyerName;
        oi.EinvoiceId = hif_EinvoiceId.Value;
        //oi.InvoiceMailbackAllowance = AllowanceSentBack;
        //oi.InvoiceMailbackPaper = InvoiceSentBack;
        //oi.ReturnPaper = ReturnPaperSentBack;

        if (invCtrl.InvoiceMode == string.Empty)
        {
            //不更新發票, 不做事情
            oi.IsUpdateInvoice = false;
        }
        else
        {
            if (invCtrl.InvoiceMode == ((int)InvoiceMode2.Triplicate).ToString())
            {
                //三聯式發票必須填寫統編和抬頭
                if (!string.IsNullOrEmpty(RegExRules.CompanyNoCheck(invCtrl.CompanyId)) || string.IsNullOrEmpty(invCtrl.CompanyName))
                {
                    alertMessage = "若欲變更為三聯式發票，請填寫正確的統編與抬頭，謝謝！";
                    invCtrl.CompanyId = string.Empty;
                    invCtrl.CompanyName = string.Empty;
                    _presenter.OnViewInitialized();
                    return;
                }
                //公司紙本發票(三聯式), 要寫入統編和公司抬頭
                oi.IsUpdateInvoice = true;
                oi.UniCode = invCtrl.CompanyId;
                oi.Title = invCtrl.CompanyName;
                oi.DrType = (int)DonationReceiptsType.DoNotContribute;
                oi.InvoiceType = ((int)InvoiceMode2.Triplicate).ToString(); //
                oi.CarrierType = CarrierType.None; //
            }
            else if (invCtrl.InvoiceMode == ((int)InvoiceMode2.Duplicate).ToString())
            {
                //個人電子發票(二聯式), 不寫統編和抬頭
                oi.IsUpdateInvoice = true;
                oi.UniCode = string.Empty;
                oi.Title = string.Empty;
                oi.DrType = (int)DonationReceiptsType.DoNotContribute;
                oi.InvoiceType = ((int)InvoiceMode2.Duplicate).ToString(); //
                oi.CarrierType = CarrierType.Member; //
            }

            //發票載具
            if (invCtrl.Version == InvoiceVersion.CarrierEra)
            {
                oi.CarrierId = invCtrl.CarrierId;
                oi.CarrierType = invCtrl.CarrierType;
                if (invCtrl.CarrierType == CarrierType.Member)
                {
                    oi.CarrierId = invCtrl.UserId.ToString();
                }
                else if (invCtrl.CarrierType == CarrierType.None)
                {
                    oi.CarrierId = null;
                }
            }
        }

        #endregion

        #region Return Info 換貨註記

        //當未啟用換貨do why? -> 疑似是宅配換貨功能啟前使用，現在不用了。
        if (!config.EnabledExchangeProcess)
        {
            oi.ReturnSituation = int.Parse(ddlST.SelectedValue); //憑證 調整狀態
            oi.ReturnReason = tbReturnMessage.Text; //憑證 處理備註
        }
        #endregion

        #region entrust sell receipt info 發票相關退貨單據是否己寄回

        oi.ReceiptMailbackPaper = chkReceiptMailbackPaper.Checked; //checkbox 退貨申請書已寄回
        oi.ReceiptMailbackAllowance = chkReceiptMailbackAllowance.Checked; //checkbox 折讓單已寄回
        oi.ReceiptMailbackReceipt = chkReceiptMailbackReceipt.Checked; //checkbox 代收轉付收據已寄回

        #endregion

        oi.ReceiptAddress = txtReceiptAddress.Text;
        oi.ReceiptNumber = txtReceiptNumber.Text;
        oi.ReceiptTitle = txtReceiptTitle.Text;
        oi.ReceiptIsPhysical = chkReceiptIsPhysical.Checked; //需寄送收據

        if (UpdateOrderInfo != null)
        {
            UpdateOrderInfo(this, new DataEventArgs<UpdatableOrderInfo>(oi));
        }
    }

    protected int OdPg_GetCount()
    {
        DataEventArgs<int> e = new DataEventArgs<int>(0);
        if (this.GetOrderDetailCount != null)
        {
            this.GetOrderDetailCount(this, e);
        }
        return e.Data;
    }

    protected void OdPg_Update(int pageNumber)
    {
        DataEventArgs<int> e = new DataEventArgs<int>(pageNumber);
        if (this.OrderDetailPageChanged != null)
            this.OrderDetailPageChanged(this, e);
    }

    protected void gvOD_Delete(object sender, EventArgs e)
    {
        if (this.UpdateOrderDetail != null)
            this.UpdateOrderDetail(this, new DataEventArgs<OrderDetail>(new OrderDetail()));
    }

    protected void getMerchandise_Command(object sender, EventArgs e)
    {
    }

    protected void RequestInvoice_Click(object sender, EventArgs e)
    {
        foreach (ListViewDataItem a in lvOrderProducts.Items)
        {
            if (((Literal)(a.FindControl("litInvoiceNumber"))) != null)
            {
                var itemid = a.DataItemIndex;
            }
        }
        Literal invoiceNumber = lvOrderProducts.Items[0].FindControl("litInvoiceNumber") as Literal;
        if (this.RequestInvoice != null)
            this.RequestInvoice(this, new DataEventArgs<string>(invoiceNumber.Text));
    }

    protected void CreateExchangeLog_Click(object sender, EventArgs e)
    {
        string reason = txtRefundReason.Text;

        if (string.IsNullOrWhiteSpace(reason))
        {
            alertMessage = "請填寫原因，以供廠商參考。";
            return;
        }

        var args = new ExchangeLogArgs
        {
            OrderGuid = this.OrderId,
            Reason = reason,
            ReceiverName = txtReceiverName.Text,
            ReceiverAddress = txtReceiverAddress.Text,
            Bid = BusinessHourGuid
        };

        if (CreateExchangeLog != null)
        {
            CreateExchangeLog(this, args);
        }
    }

    protected void CancelOrder_Click(object sender, EventArgs e)
    {
        if (CancelOrder != null)
        {
            CancelOrder(sender, new DataEventArgs<Guid>(this.OrderId));
        }
    }

    protected void WithdrawCancelOrder_Click(object sender, EventArgs e)
    {
        if (WithdrawCancelOrder != null)
        {
            WithdrawCancelOrder(sender, new DataEventArgs<Guid>(this.OrderId));
        }
    }


    #endregion

    #region order detail
    protected void gvOrderDetail_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
            itemCount = 0;
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Cells[0].Text = (e.Row.DataItemIndex + 1).ToString();
            itemCount += ((ViewPponOrderDetail)e.Row.DataItem).ItemQuantity;
        }
        else if (e.Row.RowType == DataControlRowType.Footer)
        {
            e.Row.Cells[2].Text = Resources.Localization.Subtotal;
            e.Row.Cells[3].Text = itemCount.ToString();
            //qS.Text = itemCount.ToString();
        }
    }
    protected void gvOrderDetail_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        gvOrderDetail.EditIndex = -1;
        this._presenter.OnViewInitialized();
    }
    protected void gvOrderDetail_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        if (this.UpdateOrderDetail != null)
        {
            OrderDetail od = new OrderDetail();
            od.Guid = (Guid)gvOrderDetail.DataKeys[e.RowIndex].Value;
            od.ItemQuantity = 0;
            this.UpdateOrderDetail(this, new DataEventArgs<OrderDetail>(od));
        }
    }
    protected void gvOrderDetail_RowEditing(object sender, GridViewEditEventArgs e)
    {
        gvOrderDetail.EditIndex = e.NewEditIndex;
        this._presenter.OnViewInitialized();
    }
    protected void gvOrderDetail_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        int q = 0;
        if (!int.TryParse(((TextBox)gvOrderDetail.Rows[e.RowIndex].FindControl("textItemQuantity")).Text.Trim(), out q))
            q = -1;

        gvOrderDetail.EditIndex = -1;
        if (this.UpdateOrderDetail != null)
        {
            OrderDetail info = new OrderDetail();
            info.Guid = (Guid)gvOrderDetail.DataKeys[e.RowIndex].Value;
            info.ItemQuantity = q;
            this.UpdateOrderDetail(this, new DataEventArgs<OrderDetail>(info));
        }
    }
    #endregion

    #region Deliver GridView
    protected void gvDeliver_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DropDownList ddl = (DropDownList)e.Row.FindControl("ddlShipCompanyId");
            if (ddl != null)
            {
                BindDdlShipCompany(ref ddl, _shipCompanyCol);
                ddl.SelectedValue = ((ViewOrderShipList)e.Row.DataItem).ShipCompanyId != null ? ((ViewOrderShipList)e.Row.DataItem).ShipCompanyId.ToString() : string.Empty;
            }

            Label lb = (Label)e.Row.FindControl("lbShipCompanyId");
            Label lbShipType = (Label)e.Row.FindControl("lbShipType");
            Label lbStore = (Label)e.Row.FindControl("lbStore");
            TextBox tb = (TextBox)e.Row.FindControl("tbShipNo");
            Label lbShipNo = (Label)e.Row.FindControl("lbShipNo");
            Label lbStatus = (Label)e.Row.FindControl("lbStatus");
            Label lbShipMemo = (Label)e.Row.FindControl("lbShipMemo");
            TextBox tbShipMemo = (TextBox)e.Row.FindControl("tbShipMemo");
            Label lbShipTime = (Label)e.Row.FindControl("lbShipTime");
            Label lbModifyTime = (Label)e.Row.FindControl("lbModifyTime");
            Label lbModifyId = (Label)e.Row.FindControl("lbModifyId");
            Label lbCreateTime = (Label)e.Row.FindControl("lbCreateTime");

            //區分是一般宅配還是超取
            if (((ViewOrderShipList)e.Row.DataItem).ProductDeliveryType == (int)ProductDeliveryType.FamilyPickup)
            {

                //取得賣家歷程
                var sellerHistory = ISPFacade.GetIspOrderHistory(((ViewOrderShipList)e.Row.DataItem).OrderGuid);
                int? sellerGoods = ((ViewOrderShipList)e.Row.DataItem).SellerGoodsStatus;
                string shipTime = sellerHistory.Where(p => p.DelvieryStatus == sellerGoods).OrderByDescending(p => p.DeliveryTime).Select(p => p.DeliveryTime).FirstOrDefault().ToString("yyyy-MM-dd");

                lbShipTime.Text = shipTime;
                lbShipType.Text = "超商";
                lb.Text = "超商物流";
                lbStore.Text = ((ViewOrderShipList)e.Row.DataItem).FamilyStoreName;
                lbShipNo.Text = ((ViewOrderShipList)e.Row.DataItem).PreShipNo;
                lbStatus.Text = Helper.GetDescription((GoodsStatus)((ViewOrderShipList)e.Row.DataItem).SellerGoodsStatus);
                lbShipMemo.Text = "無使用";
                e.Row.Cells[10].Text = "<a href='javascript:void(0)' onclick=History('" + ((ViewOrderShipList)e.Row.DataItem).OrderGuid + "')>查詢歷程</a>";
                lbModifyTime.Text = "超取訂單無修改資料";
                lbModifyId.Text = "超取訂單無修改資料";
            }
            else if (((ViewOrderShipList)e.Row.DataItem).ProductDeliveryType == (int)ProductDeliveryType.SevenPickup)
            {
                //取得賣家歷程
                var sellerHistory = ISPFacade.GetIspOrderHistory(((ViewOrderShipList)e.Row.DataItem).OrderGuid);
                int? sellerGoods = ((ViewOrderShipList)e.Row.DataItem).SellerGoodsStatus;
                string shipTime = sellerHistory.Where(p => p.DelvieryStatus == sellerGoods).OrderByDescending(p => p.DeliveryTime).Select(p => p.DeliveryTime).FirstOrDefault().ToString("yyyy-MM-dd");

                lbShipTime.Text = shipTime;
                lbShipType.Text = "超商";
                lb.Text = "超商物流";
                lbStore.Text = ((ViewOrderShipList)e.Row.DataItem).SevenStoreName;
                lbShipNo.Text = ((ViewOrderShipList)e.Row.DataItem).PreShipNo;
                lbStatus.Text = Helper.GetDescription((GoodsStatus)((ViewOrderShipList)e.Row.DataItem).SellerGoodsStatus);
                lbShipMemo.Text = "無使用";
                e.Row.Cells[10].Text = "<a href='javascript:void(0)' onclick=History('" + ((ViewOrderShipList)e.Row.DataItem).OrderGuid + "')>查詢歷程</a>";
                lbModifyTime.Text = "超取訂單無修改資料";
                lbModifyId.Text = "超取訂單無修改資料";
            }
            else if (((ViewOrderShipList)e.Row.DataItem).ProductDeliveryType == (int)ProductDeliveryType.Wms)
            {
                if (lbShipTime != null)
                {
                    lbShipTime.Text = ((ViewOrderShipList)e.Row.DataItem).ShipTime == null ? "" : ((ViewOrderShipList)e.Row.DataItem).ShipTime.Value.ToString("yyyy-MM-dd");
                }
                if (lb != null)
                {
                    lb.Text = ((ViewOrderShipList)e.Row.DataItem).ShipCompanyName != null ? ((ViewOrderShipList)e.Row.DataItem).ShipCompanyName : string.Empty;
                }
                if (tb != null)
                {
                    tb.Text = ((ViewOrderShipList)e.Row.DataItem).ShipNo;

                }
                if (lbShipNo != null)
                {
                    lbShipNo.Text = ((ViewOrderShipList)e.Row.DataItem).ShipNo;
                }
                lbStatus.Text = Helper.GetDescription((WmsDeliveryStatus)(((ViewOrderShipList)e.Row.DataItem).ProgressStatus ?? 0));
                lbShipType.Text = "PCHOME倉儲";
                lbStore.Text = lbShipMemo.Text = lbCreateTime.Text = "無使用";
                lbModifyTime.Text = lbModifyId.Text = "倉儲訂單無修改資料";
                e.Row.Cells[10].Text = "<a href='javascript:void(0)' onclick=WmsHistory('" + ((ViewOrderShipList)e.Row.DataItem).OrderGuid + "','" + ((ViewOrderShipList)e.Row.DataItem).ShipNo + "')>查詢歷程</a>";
            }
            else
            {
                if (lbShipTime != null)
                {
                    lbShipTime.Text = ((ViewOrderShipList)e.Row.DataItem).ShipTime == null ? "" : ((ViewOrderShipList)e.Row.DataItem).ShipTime.Value.ToString("yyyy-MM-dd");
                }
                lbShipType.Text = "宅配";
                if (lb != null)
                {
                    lb.Text = ((ViewOrderShipList)e.Row.DataItem).ShipCompanyName != null ? ((ViewOrderShipList)e.Row.DataItem).ShipCompanyName : string.Empty;
                }
                lbStore.Text = "宅配直送";
                if (tb != null)
                {
                    tb.Text = ((ViewOrderShipList)e.Row.DataItem).ShipNo;

                }
                if (lbShipNo != null)
                {
                    lbShipNo.Text = ((ViewOrderShipList)e.Row.DataItem).ShipNo;
                }
                lbStatus.Text = "已出貨";
                if (lbShipMemo != null)
                {
                    lbShipMemo.Text = ((ViewOrderShipList)e.Row.DataItem).ShipMemo;
                }
                if (tbShipMemo != null)
                {
                    tbShipMemo.Text = ((ViewOrderShipList)e.Row.DataItem).ShipMemo;
                    tb.Attributes.Add("maxlength", ShipMemoLimit.ToString());
                }

            }


        }
    }
    protected void gvDeliver_RowEditing(object sender, GridViewEditEventArgs e)
    {
        //全部退貨的話, 不得異動出貨資訊
        if (!CheckIsShipable || CheckIsExchanging)
        {
            alertMessage = "已退貨或換貨處理中的訂單無法編輯出貨資訊!";
            return;
        }

        gvDeliver.EditIndex = e.NewEditIndex;
        _presenter.OnViewInitialized();
    }
    protected void gvDeliver_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        gvDeliver.EditIndex = -1;
        this._presenter.OnViewInitialized();
    }
    protected void gvDeliver_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        if (UpdateOrderShip != null)
        {
            OrderShipArgs os = new OrderShipArgs();
            os.Id = (int)gvDeliver.DataKeys[e.RowIndex].Value;
            DropDownList ddlShipCompanyId = (DropDownList)gvDeliver.Rows[e.RowIndex].FindControl("ddlShipCompanyId");
            os.ShipCompanyId = ddlShipCompanyId != null ? Convert.ToInt32(ddlShipCompanyId.SelectedValue) : 0;
            TextBox tbShipTime = (TextBox)gvDeliver.Rows[e.RowIndex].FindControl("tbShipTime"); DateTime shipTime;
            TextBox txtShipMemo = (TextBox)gvDeliver.Rows[e.RowIndex].FindControl("tbShipMemo");
            os.ShipMemo = txtShipMemo != null ? txtShipMemo.Text : string.Empty;
            if (tbShipTime != null && DateTime.TryParse(tbShipTime.Text, out shipTime))
            {
                os.ShipTime = shipTime;
            }
            else
            {
                this.alertMessage = "請輸入正確的出貨日期!";
                return;
            }
            TextBox tbShipNo = (TextBox)gvDeliver.Rows[e.RowIndex].FindControl("tbShipNo");
            if (tbShipNo != null && tbShipNo.Text.Trim() != string.Empty)
            {
                os.ShipNo = tbShipNo != null ? tbShipNo.Text.Trim() : string.Empty;
            }
            else
            {
                this.alertMessage = "請輸入運單編號!";
                return;
            }
            this.UpdateOrderShip(this, new DataEventArgs<OrderShipArgs>(os));
            gvDeliver.EditIndex = -1;
            this._presenter.OnViewInitialized();
        }
    }
    protected void gvDeliver_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        //部份退貨可編輯出貨資訊
        if (this.DeleteOrderShip != null)
        {
            OrderShipArgs os = new OrderShipArgs();
            os.Id = (int)gvDeliver.DataKeys[e.RowIndex].Value;
            DeleteOrderShip(this, new DataEventArgs<OrderShipArgs>(os));
            gvDeliver.EditIndex = -1;
            _presenter.OnViewInitialized();
        }
    }
    #endregion

    protected void addDeliverData_Click(object sender, EventArgs e)
    {
        if (this.AddOrderShip != null)
        {
            OrderShipArgs osa = new OrderShipArgs();
            DateTime ShipTime;
            if (DateTime.TryParse(tbAddShipTime.Text, out ShipTime))
            {
                osa.ShipTime = ShipTime;
            }
            else
            {
                alertMessage = "請輸入正確的出貨日期!";
                return;
            }
            if (tbAddShipNo.Text.Trim() == string.Empty)
            {
                alertMessage = "請輸入運單編號!";
                return;
            }
            osa.ShipNo = tbAddShipNo.Text;
            osa.ShipCompanyId = Convert.ToInt32(ddlAddShipCompanyId.SelectedValue);
            osa.ShipMemo = tbShipMemo.Text;
            AddOrderShip(this, new DataEventArgs<OrderShipArgs>(osa));
            AjaxControlToolkit.ToolkitScriptManager.RegisterStartupScript(this, this.GetType(), "unblockUI", "$.unblockUI();", true);
            _presenter.OnViewInitialized();
        }
    }

    protected void btReturnMail_Click(object sender, EventArgs e)
    {
        if (this.SendRefundResult != null)
            this.SendRefundResult(this, e);
    }

    //寄出申請書     前台隱藏，客服目前已無使用
    protected void lbReturnApplicationResend_Click(object sender, EventArgs e)
    {
        //使用者輸入的 Email Address
        string sme = hif_SelectMemberEmail.Value;

        if (this.ReturnApplicationResend != null && sme != string.Empty)
        {
            this.ReturnApplicationResend(this, new DataEventArgs<string>(sme));
        }
    }

    //寄出折讓單
    protected void lbReturnDiscountResend_Click(object sender, EventArgs e)
    {
        //使用者輸入的 Email Address
        string sme = hif_SelectMemberEmail.Value;

        if (this.ReturnDiscountResend != null && sme != string.Empty)
        {
            this.ReturnDiscountResend(this, new DataEventArgs<string>(sme));
        }
    }

    protected void btSaveAtm_Click(object sender, EventArgs e)
    {
        CtAtmRefund ct = new CtAtmRefund();

        if (string.IsNullOrEmpty(txtBankName.Text))
        {
            labBankNameErr.Text = "請選擇受款銀行";
            return;
        }

        string tempBank = txtBankName.Text.Trim();
        string bankCode = string.Empty;
        string bankName = string.Empty;
        if (tempBank.IndexOf(" ") > -1)
        {
            bankCode = tempBank.Substring(0, tempBank.IndexOf(" "));
            bankName = tempBank.Substring(tempBank.IndexOf(" ") + 1);

            if (bankCode == bankName)
            {
                labBankNameErr.Text = "受款銀行名稱有誤";
                labBankNameErr.Focus();
                return;
            }
            if (!VourcherFacade.IsValidBankInfo(bankCode, bankName))
            {
                labBankNameErr.Text = "受款銀行名稱有誤";
                labBankNameErr.Focus();
                return;
            }
        }
        else
        {
            labBankNameErr.Text = "受款銀行名稱有誤";
            labBankNameErr.Focus();
            return;
        }

        string tempBranch = txtBankBranch.Text.Trim();
        string branchCode = string.Empty;
        string branchName = string.Empty;
        if (tempBranch.IndexOf(" ") > -1)
        {
            branchCode = tempBranch.Substring(0, tempBranch.IndexOf(" "));
            branchName = tempBranch.Substring(tempBranch.IndexOf(" ") + 1);

            if (branchCode == branchName)
            {
                labBankBranchErr.Text = "受款銀行分行有誤";
                labBankBranchErr.Focus();
                return;
            }
            if (!VourcherFacade.IsValidBankBranchInfo(bankCode, branchCode, branchName))
            {
                labBankBranchErr.Text = "受款銀行分行有誤";
                labBankBranchErr.Focus();
                return;
            }
        }
        else
        {
            labBankBranchErr.Text = "受款銀行分行有誤";
            labBankBranchErr.Focus();
            return;
        }

        if (string.IsNullOrEmpty(RegExRules.PersonalIdCheck(tbIdcardNo.Text.Trim())) || string.IsNullOrEmpty(RegExRules.CompanyNoCheck(tbIdcardNo.Text.Trim())))
        {
            ct.Id = tbIdcardNo.Text;
        }
        else
        {
            this.alertMessage = "請輸入正確的身分證字號或統編";
            return;
        }
        long a;
        if (tbAccountNo.Text.Trim().Length <= 14 && long.TryParse(tbAccountNo.Text, out a))
        {
            ct.AccountNumber = tbAccountNo.Text.Trim().PadLeft(14, '0');
        }
        else
        {
            this.alertMessage = "帳號欄位有誤，請重新輸入並且請勿超過14字元";
            return;
        }

        if (tbMobile.Text.Trim().Length == 10)
            ct.Phone = tbMobile.Text;
        else
        {
            this.alertMessage = "手機欄位請輸入十碼數字";
            return;
        }

        ct.AccountName = tbAcountName.Text;
        ct.BankName = bankName;
        ct.BranchName = branchName;
        ct.BankCode = bankCode + branchCode;

        if (this.SetAtmRefund != null)
            this.SetAtmRefund(this, new DataEventArgs<CtAtmRefund>(ct));
    }

    public string CouponAvailableToString(string s)
    {
        return (s == bool.TrueString) ? "開放" : "關閉";
    }


    //config.EnabledExchangeProcess:false
    public Dictionary<string, string> FilterTypes
    {
        get
        {
            Dictionary<string, string> data = new Dictionary<string, string>();
            data.Add("-1", "狀態選擇");

            if (IsToHouse)
            {
                data.Add(((int)OrderLogStatus.SendToSeller).ToString(), Resources.Localization.SendToSeller);
                data.Add(((int)OrderLogStatus.ExchangeProcessing).ToString(), Resources.Localization.ExchangeProcessing + "(重覆換貨)");
                data.Add(((int)OrderLogStatus.ExchangeSuccess).ToString(), Resources.Localization.ExchangeSuccess);
                data.Add(((int)OrderLogStatus.ExchangeFailure).ToString(), Resources.Localization.ExchangeFailure);
                data.Add(((int)OrderLogStatus.ExchangeCancel).ToString(), Resources.Localization.ExchangeCancel);
            }
            return data;
        }
    }
    //config.EnabledExchangeProcess:false
    public void SetFilterTypeDropDown(Dictionary<string, string> types)
    {
        ddlST.DataSource = types;
        ddlST.DataBind();
    }

    protected string GetOrderStatusLog(int status)
    {
        string result = string.Empty;
        switch (status)
        {
            case (int)(OrderLogStatus.Processing):
                result = Resources.Localization.RefundProcessing + "-刷退";
                break;
            case (int)(OrderLogStatus.AutoProcessing):
                result = Resources.Localization.RefundAutoProcessing + "-刷退";
                break;
            case (int)(OrderLogStatus.AutoProcessingScashOnly):
                result = Resources.Localization.RefundAutoProcessingScashOnly + "-退購物金";
                break;
            case (int)(OrderLogStatus.ProcessingScashOnly):
                result = Resources.Localization.RefundProcessingScashOnly + "-退購物金";
                break;
            default:
                result = Helper.GetLocalizedEnum(Resources.Localization.ResourceManager, (OrderLogStatus)status) ?? "UnKnown";
                break;
        }
        return result;
    }

    private void buildPriorityRbl(ref RadioButtonList rbl)
    {
        rbl.Items.Clear();
        rbl.Items.Add(new ListItem(Resources.Localization.PriorityNormal, ((int)ServiceConfig.replyPriority.Normal).ToString()));
        rbl.Items.Add(new ListItem(Resources.Localization.PriorityHigh, ((int)ServiceConfig.replyPriority.High).ToString()));
        rbl.Items.Add(new ListItem("<font color='red'>" + Resources.Localization.PriorityExtreme + "</font>", ((int)ServiceConfig.replyPriority.Extreme).ToString()));
        rbl.SelectedValue = ((int)ServiceConfig.replyPriority.Normal).ToString();
    }

    private int NoAllowanceCount(IList<string> cids, IEnumerable<EinvoiceMain> invoices)
    {
        int noAllawanceCount = 0;   //折讓單未回數目

        if (cids.Count == 0 || string.IsNullOrEmpty(cids.First()))
        {
            noAllawanceCount = invoices.Where(x => !Convert.ToBoolean(x.InvoiceMailbackAllowance)
                                                && IsInvoiceExceedPeriod(x.InvoiceNumberTime.Value)).Count();
        }
        else
        {
            foreach (var cid in cids)
            {
                noAllawanceCount += invoices.Where(x => x.OrderClassification == (int)OrderClassification.LkSite
                                                     && x.CouponId == Convert.ToInt32(cid)
                                                     && !Convert.ToBoolean(x.InvoiceMailbackAllowance)
                                                     && IsInvoiceExceedPeriod(x.InvoiceNumberTime.Value)
                                                  ).Count();
            }
        }
        return noAllawanceCount;
    }

    /// <summary>
    /// 判斷發票是否跨期退貨
    /// </summary>
    /// <param name="returnDate">發票開立日期</param>
    /// <returns>發票是否跨期退貨</returns>
    private bool IsInvoiceExceedPeriod(DateTime invDate)
    {
        DateTime now = DateTime.Now;
        if (invDate.Year != now.Year)
        {
            //不同年份的話, 必定跨期了
            return true;
        }
        else
        {
            if (
                (invDate.Month.EqualsAny(1, 2) && now.Month.EqualsAny(1, 2)) ||
                (invDate.Month.EqualsAny(3, 4) && now.Month.EqualsAny(3, 4)) ||
                (invDate.Month.EqualsAny(5, 6) && now.Month.EqualsAny(5, 6)) ||
                (invDate.Month.EqualsAny(7, 8) && now.Month.EqualsAny(7, 8)) ||
                (invDate.Month.EqualsAny(9, 10) && now.Month.EqualsAny(9, 10)) ||
                (invDate.Month.EqualsAny(11, 12) && now.Month.EqualsAny(11, 12))
                )
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }

    #region ATM Refund
    protected string GetAtmRStatus(string s, string failReason)
    {
        int i = Convert.ToInt32(s);
        switch ((AtmRefundStatus)i)
        {
            case AtmRefundStatus.Initial:
                return "退貨申請";
            case AtmRefundStatus.Request:
                return "提出退貨需求";
            case AtmRefundStatus.AchProcess:
                return "產生ACH P01中";
            case AtmRefundStatus.AchSend:
                return "產生完成";
            case AtmRefundStatus.RefundSuccess:
                return "退貨完成";
            case AtmRefundStatus.RefundFail:
                return string.Format("退貨失敗-{0}", string.IsNullOrEmpty(failReason) ? "無訊息" : failReason);
            default:
                return string.Empty;
        }
    }
    #endregion

    #region 客服紀錄

    public void BuildServiceCategory(ServiceMessageCategoryCollection serviceCates, DropDownList ddl)
    {
        ddl.Items.Clear();
        ddl.Items.Add(new ListItem("請選擇", "0"));
        foreach (var cate in serviceCates)
        {
            ddl.Items.Add(new ListItem(cate.CategoryName, cate.CategoryId.ToString()));
        }
    }

    protected void BuildSubCategory()
    {
        int cate = 0, subCate = 0;
        //有選擇客服紀錄的母類別，才要建置子類別
        if (int.TryParse(ddlCategory.SelectedValue, out cate) && cate > 0)
        {
            this.BuildServiceCategory(MemberFacade.ServiceMessageCategoryGetListByParentId(cate), ddlSubCategory);
            //利用Hidden把子類別的值記下來，並塞回下拉式選單
            if (int.TryParse(hdSubCategory.Text, out subCate))
            {
                ddlSubCategory.SelectedValue = subCate.ToString();
            }
        }
    }

    protected void btnSetServiceMsg_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(lblIOrder.Text.Trim()))
        {
            if (ServiceMsgSet != null)
            {
                ServiceMessage ServiceMessageData = GetServiceMessageData();
                ServiceMsgSet(sender, new DataEventArgs<ServiceMessage>(ServiceMessageData));

                CommonFacade.AddAudit(HiddenOrderGuid.Value, AuditType.Member, ServiceMessageData.MessageType + "-"
                + MemberFacade.ServiceMessageCategoryGetByCategoryId((int)ServiceMessageData.Category).CategoryName + "-"
                + MemberFacade.ServiceMessageCategoryGetByCategoryId((int)ServiceMessageData.SubCategory).CategoryName, User.Identity.Name, true);

                lblMessage.Text = "客服資料新增成功";
                ddlCategory.SelectedIndex = 0;
                //記錄頁面reload 以顯示最新紀錄
                AuditBoardView.Presenter.OnViewInitialized();
            }
            else
            {
                lblMessage.Text = "客服資料新增失敗";
            }
        }
        else
        {
            lblMessage.Text = "無此訂單編號!!";
        }
    }


    protected void btnSetFreeze_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(txtFreezeReason.Text.Trim()))
        {
            var info = new OrderFreezeInfo
            {
                OrderGuid = this.OrderId,
                FreezeType = txtFreezeType.Text == "1" ? FreezeType.FreezeOrder : FreezeType.CancelFreezeOrder,
                Reason = txtFreezeReason.Text,
                UserId = User.Identity.Name
            };

            if (OrderFreezeSet != null)
            {
                OrderFreezeSet(this, new DataEventArgs<OrderFreezeInfo>(info));
            }
        }
        else
        {
            lblMessage.Text = "若無輸入原因則無法執行凍結請款功能!!";
        }
    }

    protected void btnAddServiceMsgClose_Click(object sender, EventArgs e)
    {
        divAddServiceLog.Visible = false;
    }

    protected void bAddServiceLog_Click(object sender, EventArgs e)
    {
        divAddServiceLog.Visible = true;
    }

    protected void bServiceIntegrate_Click(object sender, EventArgs e)
    {
        Guid orderGuid;
        if (HiddenOrderGuid.Value != "" && Guid.TryParse(HiddenOrderGuid.Value, out orderGuid))
        {
            Order o = ProviderFactory.Instance().GetProvider<IOrderProvider>().OrderGet(orderGuid);
            if (o.IsLoaded)
            {
                Response.Redirect("../User/ServiceIntegrate.aspx?userId=" + o.UserId);
            }
        }
        Response.Redirect("../User/ServiceIntegrate.aspx?name=" + lblIEmail.Text);
    }

    private ServiceMessage GetServiceMessageData()
    {
        ServiceMessage sm = new ServiceMessage();
        sm.Email = lblIEmail.Text;
        sm.Phone = lblIPhone.Text;
        sm.Name = lblIName.Text;
        sm.OrderId = lblIOrder.Text;

        Guid orderGuid;
        if (HiddenOrderGuid.Value != "" && Guid.TryParse(HiddenOrderGuid.Value, out orderGuid))
        {
            Order o = ProviderFactory.Instance().GetProvider<IOrderProvider>().OrderGet(orderGuid);
            if (o.IsLoaded)
            {
                sm.UserId = o.UserId;
            }
        }

        if (!string.IsNullOrEmpty(rdlWorkType.SelectedValue))
        {
            int workType;
            int.TryParse(rdlWorkType.SelectedValue, out workType);
            sm.WorkType = workType;
        }
        sm.Message = txtIMessage.Text;
        sm.MessageType = ddlItype.SelectedValue;
        sm.Type = Convert.ToInt32(HiddenAddType.Value);

        int temp;
        sm.Category = int.TryParse(ddlCategory.SelectedValue, out temp) ? temp : 0;
        sm.SubCategory = int.TryParse(hdSubCategory.Text, out temp) ? temp : 0;

        sm.Status = ddlIStatus.SelectedValue;
        sm.WorkUser = Page.User.Identity.Name;
        sm.CreateId = Page.User.Identity.Name;
        sm.CreateTime = DateTime.Now;
        sm.ModifyId = Page.User.Identity.Name;
        sm.ModifyTime = DateTime.Now;
        if (!string.IsNullOrEmpty(rblPriority.SelectedValue))
        {
            sm.Priority = Convert.ToInt32(rblPriority.SelectedValue);
        }
        sm.RemarkSeller = txtRemarkSeller.Text;

        return sm;
    }

    private string GetBalanceSheetFrequenyDesc(int generationFrequency, string yearMonth, DateTime intervalEnd)
    {
        switch ((BalanceSheetGenerationFrequency)generationFrequency)
        {
            case BalanceSheetGenerationFrequency.MonthBalanceSheet:
                return yearMonth;
            case BalanceSheetGenerationFrequency.WeekBalanceSheet:
            case BalanceSheetGenerationFrequency.FortnightlyBalanceSheet:
                return string.Format("{0:yyyy/MM/dd}", intervalEnd.AddDays(-1));
            default:
                return string.Format("{0:yyyy/MM/dd}", intervalEnd);
        }
    }

    [WebMethod]
    public static string GetServiceMessageCategory(int parentId)
    {
        var serviceCates = MemberFacade.ServiceMessageCategoryGetListByParentId(parentId).ToList();
        return new JsonSerializer().Serialize(serviceCates);
    }

    [WebMethod]
    public static string GetHistoryList(string guidstring)
    {
        Guid orderGuid;
        Guid.TryParse(guidstring, out orderGuid);
        var sellerHistory = ISPFacade.GetIspOrderHistory(orderGuid).Where(p => p.HistoryType == (int)IspHistoryType.Seller).OrderBy(p => p.SerialNo).ToList();
        return new JsonSerializer().Serialize(sellerHistory);
    }

    [WebMethod]
    public static string GetWmsHistoryList(string guidstring, string shipNo)
    {
        Guid orderGuid;
        Guid.TryParse(guidstring, out orderGuid);
        var wmsHistory = WmsFacade.GetWmsOrderHistory(orderGuid, shipNo);
        return new JsonSerializer().Serialize(wmsHistory);
    }

    /// <summary>
    /// 逾期罰款
    /// </summary>
    /// <param name="compensate"></param>
    /// <param name="strBid"></param>
    /// <param name="strOrderGuid"></param>
    /// <param name="strAmount"></param>
    /// <param name="reason"></param>
    /// <param name="fineType"></param>
    /// <param name="strPromotionValue"></param>
    /// <param name="strBonusStartTime"></param>
    /// <param name="strBonusExpireTime"></param>
    /// <param name="bonusAction"></param>
    /// <param name="balanceSheetId"></param>
    /// <param name="userName"></param>
    /// <returns></returns>
    [WebMethod]
    public static string UpdateNextBalanceSheet(bool compensate, string strBid, string strOrderGuid, string strAmount, string reason, int fineType, string strPromotionValue, string strBonusStartTime, string strBonusExpireTime, string bonusAction, int? balanceSheetId, string userName)
    {
        try
        {
            IAccountingProvider _ap = ProviderFactory.Instance().GetProvider<IAccountingProvider>();
            IOrderProvider _op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
            Guid bid = Guid.Empty;
            Guid orderGuid = Guid.Empty;
            int amount;
            int promotionValue;
            DateTime bonusStartTime;
            DateTime bonusExpireTime;

            int.TryParse(strAmount, out amount);

            if (compensate)
            {
                //要給消費者紅利金
                int.TryParse(strPromotionValue, out promotionValue);
            }
            else
            {
                promotionValue = 0;
            }


            int vendorPaymentId = 0;
            Guid.TryParse(strOrderGuid, out orderGuid);
            Guid.TryParse(strBid, out bid);

            if (fineType == (int)VendorFineType.Change)
            {
                //異動金額
                var vpc = new VendorPaymentChange
                {
                    BusinessHourGuid = bid,
                    Reason = reason,
                    Amount = amount,
                    PromotionValue = promotionValue,
                    CreateId = userName,
                    CreateTime = DateTime.Now,
                    ModifyId = userName,
                    ModifyTime = DateTime.Now,
                    AllowanceAmount = amount,
                    BalanceSheetId = balanceSheetId,
                };

                vendorPaymentId = _ap.VendorPaymentChangeSet(vpc);
            }
            else if (fineType == (int)VendorFineType.Overdue)
            {
                //逾期出貨
                var vpo = new VendorPaymentOverdue
                {
                    BusinessHourGuid = bid,
                    OrderGuid = orderGuid,
                    Reason = reason,
                    PromotionAmount = promotionValue,
                    Amount = amount,
                    BalanceSheetId = balanceSheetId,
                    CreateId = userName,
                    CreateTime = DateTime.Now
                };
                vendorPaymentId = _ap.VendorPaymentOverdueSet(vpo);
            }

            if (balanceSheetId != null)
            {
                var updateBsIds = new List<int>();
                var updateFail = false;
                #region 更新廠商補扣款資料

                if (vendorPaymentId != 0)
                {
                    if (fineType == (int)VendorFineType.Change)
                    {
                        var paymentChanges = _ap.VendorPaymentChangeGetList(new List<int>() { vendorPaymentId }.AsEnumerable());
                        foreach (var paymentChange in paymentChanges)
                        {
                            try
                            {
                                if (paymentChange.BalanceSheetId.HasValue)
                                {
                                    updateBsIds.Add(paymentChange.BalanceSheetId.Value);
                                }
                                var updateBsId = balanceSheetId;
                                var actionDesc = string.Format("Id:{0} 廠商補扣款關聯對帳單更新 : {1} -> {2}", paymentChange.Id, paymentChange.BalanceSheetId, updateBsId);
                                paymentChange.BalanceSheetId = updateBsId;
                                _ap.VendorPaymentChangeSet(paymentChange);
                                CommonFacade.AddAudit(paymentChange.BusinessHourGuid, AuditType.DealAccounting, actionDesc, userName, true);
                            }
                            catch (Exception)
                            {
                                updateFail = true;
                                return new JsonSerializer().Serialize(new { Success = false, Message = string.Format("Id:{0} 廠商補扣款關聯對帳單更新失敗，請洽技術部。\r\n", paymentChange.Id) });
                            }
                        }
                    }
                    else if (fineType == (int)VendorFineType.Overdue)
                    {
                        var paymentOverdues = _ap.VendorPaymentOverdueGetListByOverdueId(vendorPaymentId);
                        foreach (var paymentOverdue in paymentOverdues)
                        {
                            try
                            {
                                if (paymentOverdue.BalanceSheetId.HasValue)
                                {
                                    updateBsIds.Add(paymentOverdue.BalanceSheetId.Value);
                                }
                                var updateBsId = balanceSheetId;
                                var actionDesc = string.Format("Id:{0} 廠商補扣款關聯對帳單更新 : {1} -> {2}", paymentOverdue.Id, paymentOverdue.BalanceSheetId, updateBsId);
                                paymentOverdue.BalanceSheetId = updateBsId;
                                _ap.VendorPaymentOverdueSet(paymentOverdue);
                                CommonFacade.AddAudit(paymentOverdue.BusinessHourGuid, AuditType.DealAccounting, actionDesc, userName, true);
                            }
                            catch (Exception)
                            {
                                updateFail = true;
                                return new JsonSerializer().Serialize(new { Success = false, Message = string.Format("Id:{0} 廠商補扣款關聯對帳單更新失敗，請洽技術部。\r\n", paymentOverdue.Id) });
                            }
                        }
                    }


                }

                #endregion 更新廠商補扣款資料

                #region 更新對帳單金額

                if (!updateFail)
                {
                    foreach (var bsId in updateBsIds)
                    {
                        var bsModel = new BalanceSheetModel(Convert.ToInt32(balanceSheetId));
                        var bs = bsModel.GetDbBalanceSheet();
                        var newAmount = (int)bsModel.GetAccountsPayable().TotalAmount;
                        var newOverdueAmount = (int)bsModel.GetAccountsPayable().OverdueAmount;
                        var newVendorPositivePaymentOverdueAmount = (int)bsModel.GetAccountsPayable().VendorPositivePaymentOverdueAmount;
                        var newVendorNegativePaymentOverdueAmount = (int)bsModel.GetAccountsPayable().VendorNegativePaymentOverdueAmount;
                        //更新對帳單金額
                        //只有尚未確認或是金額為0才能異動
                        if (bs.EstAmount != newAmount && (!bs.IsConfirmedReadyToPay || bs.EstAmount == 0))
                        {
                            try
                            {
                                var actionDesc = string.Format("Id : {0} UpdateBalanceSheetEstAmount : {1} -> {2}", bs.Id, bs.EstAmount, newAmount);
                                bs.EstAmount = newAmount;
                                bs.OverdueAmount = newOverdueAmount;
                                _ap.BalanceSheetEstAmountSet(bs, newVendorPositivePaymentOverdueAmount, newVendorNegativePaymentOverdueAmount);
                                CommonFacade.AddAudit(bs.ProductGuid, AuditType.DealAccounting, actionDesc, userName, true);
                                var bsInfos = _ap.GetPayByBillBalanceSheetHasNegativeAmount(bid);
                                if (bsInfos.AsEnumerable().Any())
                                {
                                    BalanceSheetManager.ProductBalanceSheetWithNegativeAmountMailNotify(bsInfos);
                                }
                            }
                            catch (Exception)
                            {
                                updateFail = true;
                                return new JsonSerializer().Serialize(new { Success = false, Message = string.Format("Id:{0} 對帳單應付金額更新失敗，請洽技術部。\r\n", bsId) });
                            }
                        }
                    }
                }

                #endregion 更新對帳單金額
            }



            if (compensate)
            {
                DateTime.TryParse(strBonusStartTime, out bonusStartTime);
                DateTime.TryParse(strBonusExpireTime, out bonusExpireTime);



                //給消費者紅利金
                Order o = _op.OrderGet(orderGuid);
                var mpd = new MemberPromotionDeposit
                {
                    OrderGuid = orderGuid,
                    StartTime = bonusStartTime,
                    ExpireTime = bonusExpireTime,
                    PromotionValue = promotionValue * 10, // 紅利金換算規則為 : 台幣金額 * 10倍
                    Action = bonusAction,
                    CreateId = userName,
                    CreateTime = DateTime.Now,
                    UserId = o.UserId,
                    OrderClassification = (int)OrderClassification.LkSite,
                    VendorPaymentChangeId = vendorPaymentId
                };
                MemberFacade.MemberPromotionProcess(mpd, o.CreateId, false);
            }

            //紀錄log
            string msg = "扣款/異動金額：" + amount.ToString() + "元 <br /> 扣款 / 異動原因：" + reason + "<br />  紅利金摘要： " + bonusAction + " <br />  須給消費者紅利金：" + promotionValue + "元";
            CommonFacade.AddAudit(orderGuid, AuditType.Order, msg, userName, false);



            return new JsonSerializer().Serialize(new { Success = true, Message = "異動成功" });
        }
        catch (Exception ex)
        {
            return new JsonSerializer().Serialize(new { Success = false, Message = ex.Message });
        }
    }


    [WebMethod]
    public static string IsInBalanceTimeRange(Guid bid)
    {
        bool IsInBalanceTimeRange = BalanceSheetManager.IsInBalanceTimeRange(bid, DateTime.Now);
        if (IsInBalanceTimeRange)
            return new JsonSerializer().Serialize(new { Success = IsInBalanceTimeRange });
        else
            return new JsonSerializer().Serialize(new { Success = IsInBalanceTimeRange, Message = "此檔次已過對帳區間，需走財務人工流程" });

    }

    /// <summary>
    /// 客服壓逆物流
    /// </summary>
    /// <param name="returnFormId"></param>
    /// <param name="refundFrom"></param>
    /// <param name="orderGuid"></param>
    /// <param name="contactName"></param>
    /// <param name="contactTel"></param>
    /// <param name="contactMobile"></param>
    /// <param name="contactEmail"></param>
    /// <param name="contactAddress"></param>
    /// <param name="userName"></param>
    /// <returns></returns>
    [WebMethod]
    public static string WmsRefund(int returnFormId, int refundFrom, Guid orderGuid, string contactName, string contactTel, string contactMobile, string contactEmail, string contactAddress, string userName)
    {
        try
        {

            IOrderProvider _op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
            IWmsProvider _wp = ProviderFactory.Instance().GetProvider<IWmsProvider>();
            ReturnForm rf = _op.ReturnFormGet(returnFormId);
            contactMobile = string.IsNullOrEmpty(contactMobile) ? "x" : contactMobile;


            bool isReceive = false;
            if (refundFrom == (int)WmsRefundFrom.Customer)
            {
                isReceive = true;

                //消費者改抓個資
                ViewOrderMemberBuildingSeller vombs = _op.ViewOrderMemberBuildingSellerGet(orderGuid);
                contactName = rf.ReceiverName;
                contactTel = vombs.OrderMobile;
                contactMobile = vombs.OrderMobile;
                contactEmail = vombs.UserEmail;
                contactAddress = rf.ReceiverAddress;
            }


            if (rf.VendorProgressStatus == (int)VendorProgressStatus.ConfirmedForCS)
            {
                //要退貨的商品
                ReturnFormProductCollection products = _op.ReturnFormProductGetList(returnFormId);


                string errMessage = string.Empty;
                bool isSuccess = ReturnFormRepository.WmsRefund(isReceive, orderGuid, returnFormId, refundFrom, contactName, contactTel, contactMobile, contactEmail, contactAddress, userName, products, out errMessage);

                if (isSuccess)
                {
                    if (isReceive)
                        rf.ProgressStatus = (int)ProgressStatus.RetrieveToCustomer;
                    else
                        rf.ProgressStatus = (int)ProgressStatus.RetrieveToPC;

                    rf.VendorProgressStatus = (int)VendorProgressStatus.Retrieving;
                    rf.ModifyId = userName;
                    rf.ModifyTime = DateTime.Now;
                    _op.ReturnFormSet(rf);

                    ReturnFormStatusLog log = ReturnFormRepository.MakeStatusLog(rf, userName, DateTime.Now);
                    _op.ReturnFormStatusLogSet(log);

                    return new JsonSerializer().Serialize(new { Success = isSuccess, IsReceive = isReceive, Message = "建立逆物流成功" });
                }
                else
                {
                    return new JsonSerializer().Serialize(new { Success = isSuccess, Message = errMessage });
                }

            }
            else if (rf.VendorProgressStatus == (int)VendorProgressStatus.ConfirmedForUnArrival)
            {
                rf.ProgressStatus = refundFrom == (int)WmsRefundFrom.Customer ? (int)ProgressStatus.RetrieveToCustomer : (int)ProgressStatus.RetrieveToPC;
                rf.VendorProgressStatus = (int)VendorProgressStatus.Retrieving;
                rf.ModifyId = userName;
                rf.ModifyTime = DateTime.Now;
                _op.ReturnFormSet(rf);

                ReturnFormStatusLog log = ReturnFormRepository.MakeStatusLog(rf, userName, DateTime.Now);
                _op.ReturnFormStatusLogSet(log);

                WmsRefundOrder refund = _wp.WmsRefundOrderGet(rf.Id);
                refund.CsConfirmedTime = DateTime.Now;
                refund.ModifyId = userName;
                refund.ModifyTime = DateTime.Now;
                _wp.WmsRefundOrderSet(refund);


                WmsRefundOrderStatusLog refundLog = WmsFacade.MakeWmsRefundOrderStatusLog(refund, userName, DateTime.Now);
                _wp.WmsRefundOrderStatusLogSet(refundLog);

                return new JsonSerializer().Serialize(new { Success = true, IsReceive = isReceive, Message = "更新狀態成功" });
            }
            else
            {
                return new JsonSerializer().Serialize(new { Success = false, Message = "僅「客服待確認」狀態才可以發動逆物流" });
            }
        }
        catch (Exception ex)
        {
            return new JsonSerializer().Serialize(new { Success = false, Message = ex.Message });
        }

    }

    [WebMethod]
    public static string GetFuliCodeResult(string[] codes, bool cancel)
    {
        if (codes == null || codes.Length == 0)
        {
            throw new Exception("argument empty.");
        }
        string code = string.Join(",", codes);
        var result = FuliAdapter.QueryCodes(code, cancel);
        return result;
    }


    #endregion

    #region 舊訂單補cash_trust_log以應付退貨
    protected void btCtFilled_Click(object sender, EventArgs e)
    {
        if (this.CashTrustlogFilled != null)
        {
            this.CashTrustlogFilled(sender, e);
        }
    }
    #endregion

    protected void gvInStore_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case "ToggleCouponAvailable":
                if (CouponAvailableToggle != null)
                {
                    CouponAvailableToggle(this, e); // Command argument = "CouponId,CouponAvailable"  => "5134,False"
                }
                break;
            case "SmsLogStatus":
                if (GetSMS != null)
                {
                    GetSMS(this, new DataEventArgs<int>(int.Parse(e.CommandArgument.ToString())));
                }
                break;
            case "RecoveredConfirm":
                if (RecoveredConfirm != null)
                {
                    RecoveredConfirm(this, e);
                }
                break;
            case "InvoiceMailBackSet":
                if (InvoiceMailBackSet != null)
                {
                    InvoiceMailBackSet(this, new DataEventArgs<int>(int.Parse(e.CommandArgument.ToString())));
                }
                break;
            case "ReceiptMailBackSet":
                if (ReceiptMailBackSet != null)
                {
                    ReceiptMailBackSet(this, new DataEventArgs<int>(int.Parse(e.CommandArgument.ToString())));
                }
                break;
            case "ReceiptMailBackAllowanceSet":
                if (ReceiptMailBackAllowanceSet != null)
                {
                    ReceiptMailBackAllowanceSet(this, new DataEventArgs<int>(int.Parse(e.CommandArgument.ToString())));
                }
                break;
            case "ReceiptMailBackPaperSet":
                if (ReceiptMailBackPaperSet != null)
                {
                    ReceiptMailBackPaperSet(this, new DataEventArgs<int>(int.Parse(e.CommandArgument.ToString())));
                }
                break;
            case "RequestInvoice":
                if (RequestInvoice != null)
                {
                    RequestInvoice(this, new DataEventArgs<string>(e.CommandArgument.ToString()));
                }
                break;
        }
    }

    protected void rp_TrustSequenceItemBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.DataItem is CashTrustLog)
        {
            Label clab_rpUseCount = ((Label)e.Item.FindControl("lab_rpCouponSequenceStatus"));
            HiddenField chif_rpCouponId = ((HiddenField)e.Item.FindControl("hif_rpCouponId"));
            int couponId = 0;
            int.TryParse(chif_rpCouponId.Value, out couponId);
            //todo:超過一年以上的?
            string couponStatusText = Presenter.GetCouponStatus(couponId);
            clab_rpUseCount.Text = couponStatusText;
        }
    }

    protected void gvInStore_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            bool isReservationLock = ((ViewPponCoupon)e.Row.DataItem).IsReservationLock.GetValueOrDefault(false);

            #region reservation

            ViewBookingSystemCouponReservationCollection vbscrCol = BookingSystemFacade.GetViewBookingSystemCouponReservationBySequenceNumber(((ViewPponCoupon)e.Row.DataItem).OrderDetailGuid, ((ViewPponCoupon)e.Row.DataItem).SequenceNumber);
            if (vbscrCol.Any())
            {
                ((Label)e.Row.FindControl("lblReservationCreateDate")).Text = vbscrCol.OrderByDescending(x => x.CreateDatetime).FirstOrDefault().CreateDatetime.ToString("yyyy/MM/dd HH:mm:ss");
                ((Label)e.Row.FindControl("lblReservationDateTime")).Text = ((DateTime)vbscrCol.OrderByDescending(x => x.CreateDatetime).FirstOrDefault().ReservationDateTime).ToString("yyyy/MM/dd HH:mm");

                if (vbscrCol.OrderByDescending(x => x.CreateDatetime).FirstOrDefault().IsCancel)
                {
                    ((Label)e.Row.FindControl("lblReservationStatus")).Text = "已取消(" + ((DateTime)vbscrCol.OrderByDescending(x => x.CreateDatetime).FirstOrDefault().CancelDate).ToString("yyyy/MM/dd") + ")";
                }
                if (vbscrCol.OrderByDescending(x => x.CreateDatetime).FirstOrDefault().ReservationDate < System.DateTime.Today)
                {
                    ((Label)e.Row.FindControl("lblReservationStatus")).Text = "已過期";
                }
            }

            if (isReservationLock)
            {
                ((Label)e.Row.FindControl("lblReservationStatus")).Text = "已鎖定";
            }

            #endregion reservation

            int? couponId = ((ViewPponCoupon)e.Row.DataItem).CouponId;
            string invoiceNum = ((ViewPponCoupon)e.Row.DataItem).InvoiceNumber;

            if (couponId.HasValue)
            {
                var lblStatus = e.Row.FindControl("lblStatus") as Label;
                if (lblStatus != null)
                {
                    lblStatus.Text = Presenter.GetCouponStatus(couponId.Value);
                }
            }

            if (!isReservationLock
                && couponId.HasValue
                && Presenter.IsCouponReturnable(couponId.Value))
            {
                ((CheckBox)e.Row.FindControl("cbCancel")).Enabled = true;
                ((CheckBox)e.Row.FindControl("cbCancel")).Checked = true;
            }

            if (Helper.IsFlagSet(((ViewPponCoupon)e.Row.DataItem).BusinessHourStatus, BusinessHourStatus.GroupCoupon))
            {
                ((CheckBox)e.Row.FindControl("cbCancel")).Enabled = false;
            }

            if (couponId.HasValue & CouponBound != null)
            {
                Dictionary<int, string> info = new Dictionary<int, string>();
                info.Add(couponId.Value, invoiceNum);
                CouponBound(sender, new DataEventArgs<Dictionary<int, string>>(info));
                //CouponBound(sender, new DataEventArgs<int>(couponId.Value));
                ((Literal)e.Row.FindControl("litSmsSentCount")).Text = SmsCount.ToString();
                ((Button)e.Row.FindControl("btSmsSentCount")).Enabled = true;
                ((Label)e.Row.FindControl("lbUnrecovered")).Text = RecoveredInfo;
                if (RecoveredInfo == "非強制核銷")
                    ((Button)e.Row.FindControl("btRecoveredConfirm")).Visible = false;
            }
            else
            {
                (e.Row.FindControl("btSmsSentCount")).Visible = true;
            }



            if (this.IsEntrustSell == false)
            {
                //TODO rewrite later?
                var receiptColumns = (from t in gvInStore.Columns.OfType<DataControlField>()
                                      where t.HeaderStyle.CssClass == "receipt"
                                      select t).ToList();
                foreach (var column in receiptColumns)
                {
                    column.Visible = false;
                }

                if (Invoice != null && Invoice.Id > 0)
                {
                    ((Label)e.Row.FindControl("lbInvoiceNumber")).Text = (!string.IsNullOrEmpty(Invoice.InvoiceNumber))
                        //? ((Invoice.InvoiceStatus.Value == (int)EinvoiceType.C0501 || Invoice.InvoiceStatus.Value == (int)EinvoiceType.D0401) ? Invoice.InvoiceNumber + "(作廢)" : Invoice.InvoiceNumber)
                        ? Invoice.InvoiceNumber
                        : "未開立發票";
                    ((CheckBox)e.Row.FindControl("cbInvoiceWinning")).Checked = Invoice.InvoiceWinning;
                    //((Button)e.Row.FindControl("btAllowanceGot")).Visible = true;
                    //((CheckBox)e.Row.FindControl("cbAllowanceGot")).Checked = Invoice.InvoiceMailbackAllowance ?? false;
                    #region 申請紙本發票
                    if (!string.IsNullOrEmpty(Invoice.InvoiceNumber) && Invoice.IsAllowPaperd && Invoice.InvoiceSumAmount > 0 && !Invoice.IsDonateMark && (Invoice.InvoiceStatus ?? 0) == (int)EinvoiceType.C0401)
                    {
                        Button btnRequestInvoice = (Button)e.Row.FindControl("btnRequestInvoice");
                        btnRequestInvoice.Visible = true;
                        btnRequestInvoice.CommandArgument = Invoice.InvoiceNumber;
                        if (Invoice.InvoiceRequestTime != null)
                        {
                            btnRequestInvoice.Text = "已申請紙本";
                            btnRequestInvoice.Enabled = false;
                        }
                    }
                    #endregion
                }
                else
                {
                    ((Label)e.Row.FindControl("lbInvoiceNumber")).Text = "不開立發票";
                    ((CheckBox)e.Row.FindControl("cbInvoiceWinning")).Checked = false;
                    //((Button)e.Row.FindControl("btAllowanceGot")).Visible = false;
                    //((CheckBox)e.Row.FindControl("cbAllowanceGot")).Checked = false;
                }
            }
            else
            {
                //TODO rewrite later?
                var invoiceColumns = (from t in gvInStore.Columns.OfType<DataControlField>()
                                      where t.HeaderStyle.CssClass == "invoice"
                                      select t).ToList();
                foreach (var column in invoiceColumns)
                {
                    column.Visible = false;
                }

                if (this.Receipt.IsLoaded)
                {
                    //((Button)e.Row.FindControl("btReceiptAllowanceGot")).Visible = true;
                    //((CheckBox)e.Row.FindControl("cbReceiptAllowanceGot")).Checked = Receipt.MailbackAllowance;
                    ((Button)e.Row.FindControl("btReceiptGot")).Visible = true;
                    ((CheckBox)e.Row.FindControl("cbReceipGot")).Checked = Receipt.MailbackReceipt;
                    ((Button)e.Row.FindControl("btReceiptPaperGot")).Visible = true;
                    ((CheckBox)e.Row.FindControl("cbReceipPaperGot")).Checked = Receipt.MailbackPaper;
                }
                else
                {
                    //((Button)e.Row.FindControl("btReceiptAllowanceGot")).Visible = false;
                    //((CheckBox)e.Row.FindControl("cbReceiptAllowanceGot")).Checked = false;
                    ((Button)e.Row.FindControl("btReceiptGot")).Visible = false;
                    ((CheckBox)e.Row.FindControl("cbReceipGot")).Checked = false;
                    ((Button)e.Row.FindControl("btReceiptPaperGot")).Visible = false;
                    ((CheckBox)e.Row.FindControl("cbReceipPaperGot")).Checked = false;
                }
            }
        }
    }



    protected void GetCouponToZip(object sender, EventArgs e)
    {
        if (DownloadCouponZip != null)
        {
            DownloadCouponZip(sender, new DataEventArgs<int>(0));
        }
    }

    protected string GetExchangeVendorProgressStatusDesc(int? status)
    {
        return status.HasValue
                ? Helper.GetLocalizedEnum(Resources.Localization.ResourceManager, (ExchangeVendorProgressStatus)status.Value)
                : string.Empty;
    }

    protected string GetOrderReturnStatusDesc(int? status)
    {
        return status.HasValue
                ? Helper.GetLocalizedEnum(Resources.Localization.ResourceManager, (OrderReturnStatus)status.Value) ?? "UnKnown"
                : string.Empty;
    }

    protected string GetAllowanceDesc(bool? isPaperAllowance)
    {
        return isPaperAllowance == null ? "" : (isPaperAllowance.Value
                ? "使用紙本折讓"
                : "同意電子折讓");
    }

    protected string GetAllowanceDesc(int allowanceType)
    {
        switch (allowanceType)
        {
            case (int)AllowanceStatus.PaperAllowance:
                return "使用紙本折讓";
            case (int)AllowanceStatus.ElecAllowance:
                return "同意電子折讓";
            case (int)AllowanceStatus.None:
                return "不需要折讓";
            default:
                return "";
        }
    }

    protected List<EinvoiceFacade.EinvoiceChangeLogMain> GetModel(Guid orderGuid)
    {
        return EinvoiceChangeLogMainData;
    }


}

public class ExportLog
{
    public string CreateTime { get; set; }
    public string Name { get; set; }
    public ExportLog(string createtime, string name)
    {
        CreateTime = createtime;
        Name = name;
    }
}

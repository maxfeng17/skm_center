﻿<%@ Page Language="C#" MasterPageFile="~/ControlRoom/backend.master" AutoEventWireup="true"
    CodeBehind="order_list.aspx.cs" Inherits="order_list" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="../Controls/Pager.ascx" TagName="Pager" TagPrefix="uc1" %>
<%@ Register Assembly="System.Web.Extensions"
    Namespace="System.Web.UI" TagPrefix="aspajax" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
    <script type="text/javascript" src="../../Tools/js/jquery-1.7.2.min.js"></script>
    <script type="text/javascript" src="../../Tools/js/jquery-ui.1.8.min.js"></script>
    <script type="text/javascript" src="../../Tools/ckeditor/jquery.query-2.1.7.js"></script>
    <script type="text/javascript" src="../../Tools/js/jquery.maxlength-min.js"></script>
    <script type="text/javascript" src="../../Tools/js/jquery.ui.datepicker-zh-TW.js"></script>
    <script type="text/javascript" src="../../Tools/js/jquery.blockUI.js"></script>
    <link type="text/css" href="../../Tools/js/css/ui-lightness/jquery-ui-1.8rc3.custom.css" rel="stylesheet" />
    <link href="../../Themes/PCweb/css/ppon.css" type="text/css" rel="Stylesheet" />
    <link href="../../Themes/PCweb/plugin/font-awesome/font-awesome.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../Tools/ckeditor/ckeditor.js"></script>
    <script type="text/javascript" src="../../Tools/js/json2.js"></script>
    <script type="text/javascript" src="../../Tools/js/jquery.numeric.js"></script>
    <script type="text/javascript" src="../../Tools/js/jquery.checkboxtree.js"></script>
    <script type="text/javascript" src="//html2canvas.hertzen.com/build/html2canvas.js"></script>
    <link type="text/css" href="../../Tools/js/jquery.checkboxtree.css" rel="stylesheet" />
    <style type="text/css">
        .searchTable {
            padding: 10px 10px 10px 10px;
            letter-spacing: 3px;
            line-height: 24px;
            width: 1000px;
            background-color: #EFEBEB;
        }
    </style>
    <script type="text/javascript">
        var b_editor;
        function setupdatepicker(from, to) {
            var dates = $('#' + from + ', #' + to).datepicker({
                changeMonth: true,
                numberOfMonths: 3,
                onSelect: function (selectedDate) {
                    var option = this.id == from ? "minDate" : "maxDate";
                    var instance = $(this).data("datepicker");
                    var date = $.datepicker.parseDate(instance.settings.dateFormat || $.datepicker._defaults.dateFormat, selectedDate, instance.settings);
                    dates.not(this).datepicker("option", option, date);
                }
            });
        }

        /*
        function $$(id, context) {
            var el = $("#" + id, context);
            if (el.length < 1)
                el = $("*[id$=" + id + "]", context);
            return el;
        }
        */

        function en(e) {
            var key = window.event ? e.keyCode : e.which;
            if (key == 32 || key == 13)
                $('#bSearchBtn').click();
        }

        $(document).ready(function () {
            $('#tbSearch').focus();
            setupdatepicker('tbDS', 'tbDE');

            if ($("#ddlShipmentType").val() == "2") {
                $("#ddlType").attr("disabled", false);
            }
            else
            {
                $("#ddlType").attr("disabled", true);
            }


            $(".target").change(function () {
                alert("Handler for .change() called.");
            });

            $("#ddlShipmentType").change(function () {
                if ($("#ddlShipmentType").val() == "2") {
                    $("#ddlType").attr("disabled", false);
                }
                else
                {
                    $("#ddlType").attr("disabled", true);
                }
            });
        });

        function check() {
            if ($("#tbSearch").val() == "" && $("#tbDS").val()=="" && $("#tbDE").val()=="") {
                alert("請輸入查詢條件");
                return false;
            }
            else if ($("#tbSearch").val() == "" && $("#tbDS").val() == "" && $("#tbDE").val() != "") {
                alert("請輸入起始日期");
                return false;
            }
            else if ($("#tbSearch").val() == "" && $("#tbDS").val() != "" && $("#tbDE").val() == "")
            {
                alert("請輸入結束日期");
                return false;
            }
            else if ($("#tbDS").val() != "" && $("#tbDE").val() != "") {
                var sDate = new Date($("#tbDS").val());
                var eDate = new Date($("#tbDE").val());
                var dateDiff = sDate.dateDiff("d", eDate);
                if (dateDiff > 7) {
                    alert("日期區間不超過7天");
                    return false;
                }
                else {
                    return true;
                }                
            }
            else {
                return true;
            }
        }

        Date.prototype.dateDiff = function (interval, objDate) {
            var dtEnd = new Date(objDate);
            if (isNaN(dtEnd)) return undefined;
            switch (interval) {
                case "s": return parseInt((dtEnd - this) / 1000);
                case "n": return parseInt((dtEnd - this) / 60000);
                case "h": return parseInt((dtEnd - this) / 3600000);
                case "d": return parseInt((dtEnd - this) / 86400000);
                case "w": return parseInt((dtEnd - this) / (86400000 * 7));
                case "m": return (dtEnd.getMonth() + 1) + ((dtEnd.getFullYear() - this.getFullYear()) * 12) - (this.getMonth() + 1);
                case "y": return dtEnd.getFullYear() - this.getFullYear();
            }
        }
        //-->
    </script>

    <font size="5"><b>訂單查詢</b></font>
    <br>
    <br>

    <table class="searchTable">
        <tr>
            <td>
                <font color="red">＊</font><asp:DropDownList runat="server" ID="ddlST" DataTextField="Value" DataValueField="Key" CssClass="adminitem"></asp:DropDownList>
                <asp:TextBox ID="tbSearch" runat="server" ClientIDMode="Static" onkeypress="en(event)" />
                <font color="red">＊</font><asp:DropDownList runat="server" ID="ddlDate" CssClass="adminitem">
                    <asp:ListItem Text="訂購日" Value="1"></asp:ListItem>
                    <asp:ListItem Text="應出貨日" Value="2"></asp:ListItem>
                </asp:DropDownList>              
                <asp:TextBox ID="tbDS" CssClass="date" runat="server" ClientIDMode="Static" MaxLength="10" size="10" placeholder="" />
                ～
                <asp:TextBox ID="tbDE" CssClass="date" runat="server" ClientIDMode="Static" MaxLength="10" size="10" placeholder="" />
                (日期選擇範圍最大一周)
            </td>
        </tr>
        <tr>
            <td>出貨方式：
                <asp:DropDownList runat="server" ID="ddlShipmentType" ClientIDMode="Static" CssClass="adminitem">
                    <asp:ListItem Text="全部" Value=""></asp:ListItem>
                    <asp:ListItem Text="憑證" Value="0"></asp:ListItem>
                    <asp:ListItem Text="宅配" Value="1"></asp:ListItem>
                    <asp:ListItem Text="超商" Value="2"></asp:ListItem>
                </asp:DropDownList>
                <asp:DropDownList runat="server" ID="ddlType" ClientIDMode="Static" CssClass="adminitem">
                    <asp:ListItem Text="------" Value=""></asp:ListItem>
                    <asp:ListItem Text="全家" Value="1"></asp:ListItem>
                    <asp:ListItem Text="7-11" Value="2"></asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
                <asp:Button ID="bSearchBtn" OnClick="bSearchBtn_Click" OnClientClick="return check();" runat="server" Text="送出"></asp:Button>
            </td>
        </tr>
    </table>

    <asp:Panel ID="UpdatePanel" runat="server" Visible="false">
        <asp:GridView ID="gvOrder" runat="server" OnRowDataBound="gvOrder_RowDataBound" OnSorting="gvOrder_Sorting"
            GridLines="Horizontal" ForeColor="Black" CellPadding="4" BorderWidth="1px" BorderStyle="None" AllowSorting="True"
            BorderColor="#DEDFDE" BackColor="White" EmptyDataText="無符合的資料" AutoGenerateColumns="false"
            Font-Size="Small" Width="1000px">
            <FooterStyle BackColor="#CCCC99"></FooterStyle>
            <RowStyle BackColor="#F7F7DE" Height="20px" HorizontalAlign="Center"></RowStyle>
            <SelectedRowStyle BackColor="#CE5D5A" ForeColor="White" Font-Bold="True"></SelectedRowStyle>
            <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Center"></PagerStyle>
            <HeaderStyle BackColor="#6B696B" ForeColor="White" Font-Bold="True" Height="20px"
                HorizontalAlign="Center"></HeaderStyle>
            <Columns>
                <asp:TemplateField HeaderText="訂單編號" sortExpression="OrderId" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="20%">
                    <ItemTemplate>
                        <asp:HyperLink ID="hpOrderId" runat="server"></asp:HyperLink>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="送單類型"  ItemStyle-HorizontalAlign="Center" ItemStyle-Width="8%">
                    <ItemTemplate>
                        <asp:Label ID="lblSendType" runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="出貨方式" sortExpression="ProductDeliveryType" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="7%">
                    <ItemTemplate>
                        <asp:Label ID="lblShipType" runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="訂購日" sortExpression="OrderCreateTime" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="10%">
                    <ItemTemplate>
                        <asp:Label ID="lblOrderDate" runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="檔號<br>商品名稱" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="10%">
                    <ItemTemplate>
                        <asp:Label ID="lblProductName" runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="商家編號<br>商家名稱" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="15%">
                    <ItemTemplate>
                        <asp:Label ID="lblSellerName" runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="會員" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="10%">
                    <ItemTemplate>
                        <asp:Label ID="lblMember" runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="手機" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="10%">
                    <ItemTemplate>
                        <asp:Label ID="lblMobile" runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="售價" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5%">
                    <ItemTemplate>
                        <asp:Label ID="lblPrice" runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="狀態" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5%">
                    <ItemTemplate>
                        <asp:Label ID="lblStatus" runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
        <uc1:Pager ID="gridPager" runat="server" PageSize="15" ongetcount="RetrieveOrderCount" onupdate="UpdateHandler"></uc1:Pager>
    </asp:Panel>


</asp:Content>

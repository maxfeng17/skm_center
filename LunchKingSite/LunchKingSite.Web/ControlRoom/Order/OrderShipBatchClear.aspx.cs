﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.Core;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.I18N;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;

namespace LunchKingSite.Web.ControlRoom.Order
{
    public partial class OrderShipBatchClear : RolePage, IOrderShipBatchClearView
    {
        public event EventHandler SearchClicked;
        public event EventHandler OrderShipClearClicked;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                this.Presenter.OnViewInitialized();
            }
            this.Presenter.OnViewLoaded();
        }

        private OrderShipBatchClearPresenter _presenter;
        public OrderShipBatchClearPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        public string CurrentUser
        {
            get { return User.Identity.Name; }
        }

        /// <summary>
        /// P好康或品生活
        /// </summary>
        public string FilterDealType
        {
            get { return ddlDealType.SelectedValue; }
            set { ddlDealType.SelectedValue = value; }
        }

        public string FilterQueryOption
        {
            get { return ddlQueryOption.SelectedValue; }
            set { ddlQueryOption.SelectedValue = value; }
        }

        /// <summary>
        /// 上檔區間或兌換區間
        /// </summary>
        public string FilterQueryTime
        {
            get { return ddlQueryTime.SelectedValue; }
            set { ddlQueryTime.SelectedValue = value; }
        }

        public string FilterKeyWord
        {
            get { return txtKeyWord.Text.Trim(); }
            set { txtKeyWord.Text = value; }
        }

        public string FilterQueryStartTime
        {
            get { return txtDS.Text; }
        }

        public string FilterQueryEndTime
        {
            get { return txtDE.Text; }
        }

        public Dictionary<int, string> DealTypeInfos
        {
            get
            {
                Dictionary<int, string> data = new Dictionary<int, string>();
                data.Add((int)DepartmentTypes.Ppon, Phrase.SDT_Ppon);
                data.Add((int)DepartmentTypes.HiDeal, Phrase.SDT_HiDeal);
                return data;
            }
        }

        public Dictionary<string, string> QueryOptionInfo
        {
            get
            {
                Dictionary<string, string> data = new Dictionary<string, string>();
                if (int.Parse(FilterDealType) == (int)DepartmentTypes.Ppon)
                {
                    data.Add("productName", Resources.Localization.DealName);
                    data.Add("sellerName", Resources.Localization.SellerName);
                    data.Add("productGuid", "BID");
                    data.Add("uniqueId", Resources.Localization.UniqueId);
                }
                else
                {
                    data.Add("productName", Resources.Localization.DealName);
                    data.Add("sellerName", Resources.Localization.SellerName);
                    data.Add("dealId", "DID" + Resources.Localization.UniqueId);
                    data.Add("uniqueId", "PID" + Resources.Localization.PID);
                }
                return data;
            }
        }
        
        protected void ddlDealType_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetFilterTypeDropDown(QueryOptionInfo);
        }

        public void SetFilterTypeDropDown(Dictionary<string, string> types)
        {
            ddlQueryOption.DataSource = types;
            ddlQueryOption.DataBind();
        }

        public void SetDealType(Dictionary<int, string> types)
        {
            ddlDealType.DataSource = types;
            ddlDealType.DataBind();
        }

        protected void SearchClick(object sender, EventArgs e)
        {
            if (this.SearchClicked != null)
            {
                this.SearchClicked(this, e);
            }
        }

        public void SetDealList(IEnumerable<ShipDealInfo> dealInfo)
        {
            if (dealInfo.Count() == 0)
            {
                lbQueryResult.Text = "查無符合的資料";
                setDealInfo.Visible = false;
            }
            else
            {
                int resultCount = 0;
                setDealInfo.Visible = true;

                repDealList.DataSource = dealInfo;
                resultCount = dealInfo.Count();
                repDealList.DataBind();

                lbQueryResult.Text = "查詢結果：" + resultCount + "筆";
            }
        }

        protected string GetTheDate(string time)
        {
            DateTime date;
            return DateTime.TryParse(time, out date) ? date.ToString("yyyy/MM/dd") : "";
        }

        protected void repDealList_ItemCommand(object sender, RepeaterCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "ClearOrderShip":
                    if (this.OrderShipClearClicked != null)
                        this.OrderShipClearClicked(this, e);
                    break;
            }
        }

        public void ShowMessage(string msg)
        {
            System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('" + msg + "');", true);
        }

        protected void repDealList_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            string confirmMsg = "";
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (DataBinder.Eval(e.Item.DataItem, "ProductGuid") != null)
                {
                    Button orderShipClear = (Button)e.Item.FindControl("btnOrderShipClear");
                    confirmMsg = "執行刪除，將會清空此檔已填單的所有出貨資料內容(申請過換貨的訂單除外)。\\n您確定要繼續執行刪除嗎？";
                    orderShipClear.Attributes.Add("onclick", "javascript:return " + "confirm('" + confirmMsg + "')");
                }
            }
        }
    }
}
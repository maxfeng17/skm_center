﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/ControlRoom/backend.master"CodeBehind="order_shipstatus.aspx.cs"Inherits="order_shipstatus" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="../Controls/Pager.ascx" TagName="Pager" TagPrefix="uc1" %>
<%@ Import Namespace="LunchKingSite.DataOrm" %>
<%@ Import Namespace="LunchKingSite.WebLib.Views" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
    <table style="border: double 3px black; width: 100%">
        <tr>
            <td colspan="2" style="text-align: center; font-weight: bolder">
                商品出貨狀態調整
            </td>
        </tr>
        <tr>
            <td>
                Bid:
            </td>
            <td>
                <asp:TextBox ID="tbx_Bid" runat="server" Width="250"></asp:TextBox>
                <asp:Button ID="btn_Search" runat="server" Text="確認" OnClick="SearchOrders" />
                <asp:Label ID="lbl_Message" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Panel ID="pan_DealInfo" runat="server">
                    <table style="border: double 3px black; width: 100%">
                        <tr>
                            <td style="background-color: #FF98FB; width: 25%">
                                店家名稱
                            </td>
                            <td colspan="3">
                                <asp:Label ID="lbl_SellerName" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="background-color: #FFFFFE;">
                                活動名稱
                            </td>
                            <td style="width: 25%">
                                <asp:Label ID="lbl_ItemName" runat="server"></asp:Label>
                            </td>
                            <td style="background-color: #FFFFFE; width: 25%">
                                活動狀態
                            </td>
                            <td colspan="3">
                                <asp:Label ID="lbl_Status" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="background-color: #FF98FB">
                                訂購時間起訖
                            </td>
                            <td>
                                <asp:Label ID="lbl_OrderTimeS" runat="server"></asp:Label>
                                ~<br />
                                <asp:Label ID="lbl_OrderTimeE" runat="server"></asp:Label>
                            </td>
                            <td style="background-color: #FF98FB">
                                運送時間起訖
                            </td>
                            <td>
                                <asp:Label ID="lbl_DeliverTimeS" runat="server"></asp:Label>
                                ~<br />
                                <asp:Label ID="lbl_DeliverTimeE" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="background-color: #FFFFFE">
                                成單門檻
                            </td>
                            <td>
                                <asp:Label ID="lbl_OrderMin" runat="server"></asp:Label>
                            </td>
                            <td style="background-color: #FFFFFE">
                                目前人數
                            </td>
                            <td>
                                <asp:Label ID="lbl_OrderQuantity" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <table style="border: double 3px black; width: 100%">
                    <tr>
                        <td>
                            預設物流公司
                        </td>
                        <td>
                            <asp:TextBox ID="tbx_DefaultShipAgent" runat="server"></asp:TextBox>
                        </td>
                        <td>
                            預設出貨日期
                        </td>
                        <td>
                            <asp:TextBox ID="tbx_DefaultShipDate" runat="server"></asp:TextBox>
                            <cc1:CalendarExtender ID="ce_DefaultShipDate" TargetControlID="tbx_DefaultShipDate"
                                runat="server" Format="yyyy/MM/dd">
                            </cc1:CalendarExtender>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                                    <asp:HiddenField ID="hif_CurrentPage" runat="server" />
                                    <asp:HiddenField ID="hif_Bid" runat="server" />
                                    <asp:HiddenField ID="hif_PageCount" runat="server" />
                                    <asp:HiddenField ID="hif_edit" runat="server" />
                                    <asp:GridView ID="gv_Orders" runat="server" OnRowDataBound="gvDataBound" OnRowEditing="gvRowEditing"
                                        OnRowCancelingEdit="gvRowCancelingEdit" OnRowUpdating="gvRowUpdating" AutoGenerateColumns="false"
                                        Width="100%" AlternatingRowStyle-BackColor="White">
                                        <Columns>
                                            <asp:TemplateField HeaderText="編輯">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lbt_Edit" runat="server" CommandName="Edit">編輯</asp:LinkButton>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:LinkButton ID="lbt_Update" runat="server" CommandName="Update">更新</asp:LinkButton><br />
                                                    <asp:LinkButton ID="lbt_Cancel" runat="server" CommandName="Cancel" ForeColor="Red">取消</asp:LinkButton>
                                                </EditItemTemplate>
                                            </asp:TemplateField>
                                            <%--   <asp:TemplateField HeaderText="序號">
                                        <ItemTemplate>
                                            <%# Container.DataItemIndex %>
                                        </ItemTemplate>
                                    </asp:TemplateField>--%>
                                            <asp:TemplateField HeaderText="OrderID">
                                                <ItemTemplate>
                                                    <%# ((ClassForViewPponOrder_OrderStatusLog)(Container.DataItem)).Viewpponorder.OrderId%>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="會員">
                                                <ItemTemplate>
                                                    <%# ((ClassForViewPponOrder_OrderStatusLog)(Container.DataItem)).Viewpponorder.CreateId%>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="訂購時間">
                                                <ItemTemplate>
                                                    <%# ((ClassForViewPponOrder_OrderStatusLog)(Container.DataItem)).Viewpponorder.CreateTime.ToString("yyyy/MM/dd HH:mm")%>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="總額">
                                                <ItemTemplate>
                                                    <%# ((ClassForViewPponOrder_OrderStatusLog)(Container.DataItem)).Viewpponorder.Subtotal.ToString("F0")%>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="出貨狀態">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_ShipStatus" runat="server"></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:RadioButton ID="rbt_Preparing" runat="server" Text="備貨中" GroupName="ShipStatus"
                                                        Checked="true" /><br />
                                                    <asp:RadioButton ID="rbt_Shipped" runat="server" Text="已出貨" GroupName="ShipStatus" />
                                                </EditItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="物流公司">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_ShipAgent" runat="server"></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:HiddenField ID="hif_OrderMemo" runat="server" />
                                                    <asp:HiddenField ID="hif_OrderReason" runat="server" />
                                                    <asp:HiddenField ID="hif_Guid" runat="server" />
                                                    <asp:HiddenField ID="hif_OrderStatus" runat="server" />
                                                    <asp:TextBox ID="tbx_ShipAgent" runat="server" Width="150"></asp:TextBox>
                                                </EditItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="託運單號">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_ShipNo" runat="server"></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="tbx_ShipNo" runat="server" Width="150"></asp:TextBox>
                                                </EditItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="出貨日期">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_ShipDate" runat="server"></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="tbx_ShipDate" runat="server" Width="100"></asp:TextBox>
                                                    <cc1:CalendarExtender ID="ce_ShipDate" TargetControlID="tbx_ShipDate" runat="server"
                                                        Format="yyyy/MM/dd">
                                                    </cc1:CalendarExtender>
                                                </EditItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                    <uc1:Pager ID="gridPager" runat="server" PageSize="10" OnGetCount="RetrieveOrderCount"
                                        OnUpdate="UpdateHandler"></uc1:Pager>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>

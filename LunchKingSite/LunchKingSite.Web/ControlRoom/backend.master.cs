using System;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.WebLib;

public partial class backend : System.Web.UI.MasterPage
{
    private static ISysConfProvider config;
    static backend()
    {
        config = ProviderFactory.Instance().GetConfig();
    }

    protected string SiteUrl
    {
        get
        {
            return config.SiteUrl.TrimEnd('/');
        }
    }
}

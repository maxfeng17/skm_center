<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="backend.master" CodeBehind="Default.aspx.cs" Inherits="LunchKingSite.Web.ControlRoom.Default"
	EnableViewState="false" %>

<%@ Import Namespace="System.Net" %>
<%@ Import Namespace="LunchKingSite.Core" %>
<%@ Import Namespace="LunchKingSite.BizLogic.Facade" %>
<asp:Content ID="Content2" ContentPlaceHolderID="SSC" runat="Server">
	<style type="text/css">
		.btn {
			padding: 5px 15px 5px 15px;
			line-height: 16px;
			cursor: pointer;
		}

		.btn-default {
		}

		.ml-12 {
			margin-left: 12px;
		}

		.form-control {
			padding: 5px;
			font-size: 15px;
		}
	</style>
	<script type="text/javascript">
		$(document).ready(function () {
			$('#btnQuicklunch').click(function () {
				var keyword = $('#keyword').val();
				$('#quickPreview').hide();
				$('#quicklunchLink').attr('href', 'javascript:void(0)');
				$('#discountCodeUsageLink').attr('href', "javascript:void(0)").text('');
				$('#quicklunchHint *').remove();
				$.ajax({
					type: "POST",
					url: "Default.aspx/Quicklunch",
					data: JSON.stringify({ "keyword": keyword }),
					contentType: "application/json; charset=utf-8",
					dataType: "json",
					success: function (msg) {
						//var res = $.parseJSON(msg.d);

						var result = msg.d.result == null ? [] : msg.d.result;

						if (result.length == 1) {
							var item = result[0];
							var link = $('<a></a>').text(item.title == null ? item.url : item.title);
							link.attr('href', item.url).attr('target', '_blank')
								.css('padding', '2px').css('display', 'block');
							$('#quicklunchHint').append(link).show();
							$('#discountCodeUsageLink').attr('href', "/api/system/discountusage?bid=" + item.id).text('折價券資訊');

							if (item.info != null && item.info != '') {
								$('#txtDealInfo').val(item.info);
							}
							if (item.info2 != null && item.info2 != '') {
								$('#txtDealInfo2').val(item.info2);
							}
							if (item.info3 != null && item.info3 != '') {
								$('#txtDealInfo3').val(item.info3);
							}
							if ($('#txtDealInfo').val() != '') {
								$('#quickPreview').data('bid', item.id).show();
							}
						} else if (result.length > 1) {
							for (var key in result) {
								var item = result[key];
								if (item.info != null && item.info != '') {
									console.log('info:' + item.info);
								}
								var link = $('<a></a>').text(item.title == null ? item.url : item.title);
								link.attr('href', item.url).attr('target', '_blank')
									.css('padding', '2px').css('display', 'block');
								$('#quicklunchHint').append(link);
							}
							$('#quicklunchHint').show();
						}
						if (msg.d.success == false || result.length == 0) {
							$('#quicklunchHint').show().text('沒有符合的資料').delay(3000).fadeOut();
						}
					}, error: function (response, q, t) {
						if (t == 'userNotLogin') {
							location.href = loginUrl;
						}
					}
				});
			});
			$('#btnInfo').click(function () {
			});
			$('#btnRefreshDealInfoAtRedis').click(function () {
				var bid = $('#quickPreview').data('bid');
				if (bid == null || bid.length == 0) {
					return;
				}
				$.ajax({
					type: "POST",
					url: "Default.aspx/ReloadCacheAtRedis",
					data: JSON.stringify({ "bid": bid }),
					contentType: "application/json; charset=utf-8",
					dataType: "json",
					success: function (msg) {
						//var res = $.parseJSON(msg.d);
						if (msg.d.success) {
							alert('完成')
							$('#btnQuicklunch').trigger('click');
						} else {
							alert('失敗')
						}
					}, error: function (response, q, t) {
						if (t == 'userNotLogin') {
							location.href = loginUrl;
						}
					}
				});
			});
		});
	</script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
	<%if (User.IsInRole(MemberRoles.Administrator.ToString())) { %>
	<h3>快搜</h3>
	<div>
		<input type="text" class="form-control" style="width: 300px" id="keyword" placeholder="可搜尋檔次ID, 會員ID, 導到設定頁" />
		<button type="button" class="btn btn-default ml-12" id="btnQuicklunch">快速</button>
		<button type="button" class="btn btn-default ml-12" id="btnInfo" style="display: none">資訊</button>
		<div id="quicklunchHint"></div>
		<div class="links">
			<div><a id="quicklunchLink" target="_blank"></a></div>
			<div><a id="discountCodeUsageLink" target="_blank"></a></div>
		</div>
		<div id="quickPreview" style="display: none">
			<div style="display: flex">
				<div>
					<textarea id="txtDealInfo" cols="60" rows="15" wrap="off" title="本機快取">				
					</textarea>
					<div>本機快取</div>
				</div>
				<div>
					<textarea id="txtDealInfo2" cols="60" rows="15" wrap="off" title="Redis快取">

					</textarea>
					<div style="position: relative">Redis快取
						<button id="btnRefreshDealInfoAtRedis" type="button" style="margin-left: 20px">重整</button></div>
				</div>
				<div>
					<textarea id="txtDealInfo3" cols="60" rows="15" wrap="off" title="資料庫">

					</textarea>
					<div>資料庫</div>
				</div>
			</div>
		</div>

	</div>
	<%} %>
	<h2>Database Info</h2>
	<%if (User.IsInRole(MemberRoles.Administrator.ToString())) { %>
    Load-balance Mode: <span class="largeblue"><%=Model.DbMode %></span><br />
	Database: <span class="largeblue"><%=Model.Database %></span><br />
	<% if (Model.CurrentBranchSchemaVersion != 0 || Model.AssemblyBranchRequiredSchemaVersion != 0)
       {
           Response.Write("<fieldset style='width:400px'><legend>Main</legend><div style='margin-left:24px'>");
           Response.Write("Current Schema Version: <span class='largeblue'>" + Model.CurrentSchemaVersion + "</span><br />");
           Response.Write("Assembly Schema Version: <span class='largeblue'>" + Model.AssemblyRequiredSchemaVersion + "</span><br />");
           Response.Write("</fieldset>");
           Response.Write("<fieldset style='width:400px'><legend>Branch</legend><div style='margin-left:24px'>");
           Response.Write("Current Schema Version: <span class='largeblue'>" + Model.CurrentBranchSchemaVersion + "</span><br />");
           Response.Write("Assembly Schema Version: <span class='largeblue'>" + Model.AssemblyBranchRequiredSchemaVersion + "</span><br />");
           Response.Write("</fieldset>");
       }
    else {%>
        Current Schema Version: <span class="largeblue"><%=Model.CurrentSchemaVersion %></span><br />
	Assembly Required Schema Version: <span class="largeblue"><%=Model.AssemblyRequiredSchemaVersion %></span><br />
	<%} %>
	<%} else {%>
    Current Schema Version: <span class="bigblue"><%=Model.CurrentSchemaVersion %></span><br />
	Assembly Required Schema Version: <span class="bigblue"><%=Model.AssemblyRequiredSchemaVersion %></span><br />
	<%} %>

	<div id="dbw" runat="server" style="font-weight: bold; font-size: xx-large; color: red;">
		Warning: Version mismatch!
	</div>
	<%if (User.IsInRole(MemberRoles.Administrator.ToString())) { %>
	<h2>Web Info</h2>
	Last Member created at: <span class="largeblue"><%=Model.MemberCreatedTime %></span><br />
	Last PezMember created at: <span class="largeblue"><%=Model.PezMemberCreatedTime %></span><br />
	Last FbMember created at: <span class="largeblue"><%=Model.FbMemberCreatedTime %></span><br />
	Last 17Member created at: <span class="largeblue"><%=Model.Member17CreatedTime %></span><br />
	Last Order created at: <span class="largeblue"><%=Model.OrderCreatedTime %></span><br />
	Last Creditcard order created at: <span class="largeblue"><%=Model.OrderByCreditCardCreatedTime %></span><br />
	<ul style="padding-left: 20px; margin-top: 3px">
		<li style="list-style-type: none">[10] 網際威信: <%=Model.OrderByCreditCardHiTrustCVV2CreatedTime %></li>
		<li style="list-style-type: none">[12] 藍新: <%=Model.OrderByCreditCardNewebCvv2CreatedTime %></li>
		<li style="list-style-type: none">[14] 藍新分期: <%=Model.OrderByCreditCardNewebInstallmentCreatedTime %></li>
	</ul>
	<h2>Elmah</h2>
	Last Error:<span class="largeblue"><a href="<%=Model.ElmahLink %>" target="_blank"><%=HttpUtility.HtmlEncode(Model.ElmahError) %></a></span><br />
	Last Time:<span class="largeblue"><%=Model.ElmahTime %></span><br />
	<%} %>
	<h2>System Info</h2>
	Server: <span class="largeblue"><%=Model.Server %></span><br />
	.Net Framework Version: <span class="largeblue"><%=Environment.Version %></span><br />
	<%if (User.IsInRole(MemberRoles.Administrator.ToString())) { %>
    User: <span class="largeblue"><%=Model.User %></span><br />
	Start at: <span class="largeblue" title="<%=Model.StartExactTime %>"><%=Model.StartTime %></span><br />
	Session: <span class="largeblue"><%=Model.SessionMode%>, <%=Session.Timeout %>分</span><br />
	MKey: <span class="largeblue"><%=Model.MKey%></span><br />
	SSL: <span class="largeblue"><%=System.Net.ServicePointManager.SecurityProtocol.ToString() %></span><br />
	Integrated Pool: <span class="largeblue"><%=HttpRuntime.UsingIntegratedPipeline ? "是" : "否" %></span><br />
	Release time: <span class="largeblue"><%=Model.DllReleaseTime %></span>
	<h2>Web Limit Info</h2>
	MaxConnection: <span class="largeblue">
		<div>
			<% foreach (KeyValuePair<string,int> pair in Model.ConnectionManagementMaxConnections)
               {
                   Response.Write("<div>Address " + pair.Key + " limit to " + pair.Value + "</div>");
               } %>
		</div>
	</span>
	ProcessorCount: <span class="largeblue"><%=Model.ProcessorCount %></span><br />
	Min/MaxWorkerThreads: <span class="largeblue"><%=Model.ProcessModelMinWorkerThreads %>
        -<%=Model.ProcessModelMaxWorkerThreads %></span><br />
	Min/MaxIoThreads: <span class="largeblue"><%=Model.ProcessModelMinIoThreads %>
        -<%=Model.ProcessModelMaxIoThreads %></span><br />
	MinFreeThreads: <span class="largeblue"><%=Model.HttpRuntimeMinFreeThreads %></span><br />
	MinLocalRequestFreeThreads: <span class="largeblue"><%=Model.MinLocalRequestFreeThreads %></span><br />
	ConcurrentRequests: <span class="largeblue"><%=Model.ConcurrentRequests %></span><br />
	<h2>ServicePointManager</h2>
	DefaultConnectionLimit: <span class="largeblue"><%=ServicePointManager.DefaultConnectionLimit %></span><br />
	MaxServicePointIdleTime: <span class="largeblue"><%=ServicePointManager.MaxServicePointIdleTime %></span>
	<%} %>
	<% if (User.IsInRole(MemberRoles.Administrator.ToString()))
       {
           Response.Write("<h2>Modules Info</h2>");
           HttpApplication httpApps = HttpContext.Current.ApplicationInstance;
           //Get List of modules
           HttpModuleCollection httpModuleCollections = httpApps.Modules;
           Response.Write("<ol>");
           foreach (string activeModule in httpModuleCollections.AllKeys)
           {
               Response.Write("<li>" + activeModule + "</li>");
           }
           Response.Write("</ol>");
       } %>
	<h2>Web Server List</h2>
	Host List:<span class="largeblue"><%=string.Join(", ", CommonFacade.GetWebServerHostList()) %><br />
	</span>
	IP Address List: <span class="largeblue"><%=string.Join(",", CommonFacade.GetWebServerIpList()) %><br />
	</span>
	<h2>Assembly Info</h2>
	<asp:GridView ID="gvAssembly" runat="server" CellPadding="4" Font-Size="9px"
		ForeColor="#333333" GridLines="None" Width="100%" AutoGenerateColumns="False">
		<EmptyDataTemplate>
			No assembly loaded
		</EmptyDataTemplate>
		<Columns>
			<asp:BoundField HeaderText="Assembly" DataField="Assembly" />
			<asp:BoundField HeaderText="Version" DataField="Version" />
			<asp:BoundField HeaderText="File Version" DataField="FileVersion" />
			<asp:BoundField HeaderText="Build Date" DataField="Build Date" />
			<asp:BoundField HeaderText="Size" DataField="Size"
				HeaderStyle-HorizontalAlign="right" ItemStyle-HorizontalAlign="right">
				<HeaderStyle HorizontalAlign="Right"></HeaderStyle>

				<ItemStyle HorizontalAlign="Right"></ItemStyle>
			</asp:BoundField>
			<asp:BoundField HeaderText="Build" DataField="Build Type"
				HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" />
			<asp:BoundField HeaderText="Global" DataField="Global"
				HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" />
			<asp:BoundField HeaderText="NetVersion" DataField="NetVersion"
				HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" />
		</Columns>
		<FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
		<RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
		<SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
		<PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
		<HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White"
			HorizontalAlign="Left" />
		<EditRowStyle BackColor="#999999" />
		<AlternatingRowStyle BackColor="White" ForeColor="#284775" />
	</asp:GridView>
	<asp:Label ID="lbMsg" runat="server" />
</asp:Content>


﻿using System;
using System.Web.UI.WebControls;
using LunchKingSite.Core.UI;

public partial class seller_list : RolePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
            ((GridView)SellerList1.FindControl("gvSeller")).Columns[0].Visible = false;
            ((GridView)SellerList1.FindControl("gvSeller")).Columns[9].Visible = false;
    }
}

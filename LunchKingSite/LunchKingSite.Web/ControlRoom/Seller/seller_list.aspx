﻿<%@ Page Language="C#" MasterPageFile="~/ControlRoom/backend.master" AutoEventWireup="true"
    Codebehind="seller_list.aspx.cs" Inherits="seller_list" %>

<%@ Register Src="../Controls/SellerList.ascx" TagName="SellerList" TagPrefix="uc2" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
    <uc2:SellerList ID="SellerList1" runat="server" ShowSellerName="true"
        ShowSellerCityLevel="true" ShowSellerId="true" ShowSellerTel="true" ShowSellerFax="true"
        ShowSellerStatus="true" ShowCreateTime="true" ShowModifyTime="false" ShowCloseDownStatus="true" />
</asp:Content>

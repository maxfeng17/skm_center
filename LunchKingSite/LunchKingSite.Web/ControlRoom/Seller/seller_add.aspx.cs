﻿using AjaxControlToolkit;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model.Sales;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.I18N;
using LunchKingSite.WebLib.Component;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;
using Newtonsoft.Json.Linq;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using LunchKingSite.Core.Enumeration;
using Seller = LunchKingSite.DataOrm.Seller;
using WebUtility = LunchKingSite.WebLib.WebUtility;
using Microsoft.SqlServer.Types;
using City = LunchKingSite.DataOrm.City;

public partial class seller_add : RolePage, ISellerAddView
{
    public ISysConfProvider Config;
    public IHumanProvider hp;

    #region Event

    public event EventHandler<DataEventArgs<Seller>> UpdateSellerInfo;

    public event EventHandler<DataEventArgs<Dictionary<int, bool>>> UpdateSellerCategory;

    public event EventHandler<DataEventArgs<Guid>> DeleteAccessoryGroup;

    public event EventHandler<DataEventArgs<string>> ImageListChanged;

    public event EventHandler<DataEventArgs<PhotoInfo>> UpdatePhoto;

    public event EventHandler SelectCityChanged;

    public event EventHandler CreateSellerID;

    public event EventHandler<DataEventArgs<string>> ImportBusinessOrderInfo;

    public event EventHandler SellerDetailSave;

    public event EventHandler ImportIntroduct;

    public event EventHandler<DataEventArgs<SellerTempStatus>> SellerApprove;

    public event EventHandler CopySeller;

    #endregion

    #region props

    private SellerAddPresenter _presenter;

    public SellerAddPresenter Presenter
    {
        set
        {
            this._presenter = value;
            if (value != null)
            {
                this._presenter.View = this;
            }
        }
        get
        {
            return this._presenter;
        }
    }

    public Guid SellerGuid
    {
        get
        {
            if (!string.IsNullOrEmpty(Request["sid"]))
            {
                return new Guid(Request.QueryString["sid"]);
            }
            else
            {
                return Guid.Empty;
            }
        }
    }

    public int Tc
    {
        get
        {
            if (!string.IsNullOrEmpty(Request["tc"]))
            {
                return int.Parse(Request["tc"]);
            }
            else
            {
                return 1;
            }
        }
    }

    private Guid _CBizHourGuid;

    public Guid CBizHourGuid
    {
        get
        {
            return _CBizHourGuid;
        }
        set
        {
            _CBizHourGuid = value;
        }
    }

    public int SelectCity
    {
        get
        {
            return int.Parse(ddlCity.SelectedValue);
        }
    }

    public int SelectZone
    {
        get
        {
            return int.Parse(ddlZone.SelectedValue);
        }
    }

    public string SellerID
    {
        get
        {
            return lbSellerID.Text;
        }
    }

    public int? SellerCategory
    {
        get
        {
            int seller_category;
            if (int.TryParse(ddl_Seller_Category.SelectedValue, out seller_category))
            {
                if (seller_category == -1)
                {
                    return null;
                }
                else
                {
                    return seller_category;
                }
            }
            else
            {
                return null;
            }
        }
        set
        {
            if (value == null)
            {
                ddl_Seller_Category.SelectedIndex = -1;
            }
            else
            {
                ddl_Seller_Category.SelectedValue = value.Value.ToString();
            }
        }
    }

    private Dictionary<int, bool> _SellerCategoryList;

    public Dictionary<int, bool> SellerCategoryList
    {
        get
        {
            return _SellerCategoryList;
        }
        set
        {
            _SellerCategoryList = value;
        }
    }

    private string _PhotoPathTemp;

    public string PhotoPathTemp
    {
        get
        {
            return _PhotoPathTemp;
        }
        set
        {
            _PhotoPathTemp = value;
        }
    }

    public string UpdateMessage
    {
        get
        {
            return Presenter.UpdateMessage;
        }
        set
        {
            Presenter.UpdateMessage = value;
        }
    }

    private string _Filter;

    public string Filter
    {
        get
        {
            return _Filter;
        }
        set
        {
            _Filter = value;
        }
    }

    public string UserName
    {
        get
        {
            return User.Identity.Name;
        }
    }

    public HttpPostedFile FileUpload
    {
        get
        {
            if (fuI.FileContent.Length > 0)
            {
                return fuI.PostedFile;
            }
            else
            {
                return null;
            }
        }
    }

    public string ImagePath
    {
        get
        {
            return Request.Form["is"] != null ? (Request.Form["is"]) : string.Empty;
        }
    }

    public string SellerIntroduct
    {
        get
        {
            return HttpUtility.HtmlDecode(tbIntroduct.Text.Trim());
        }
        set
        {
            tbIntroduct.Text = value;
        }
    }

    public int PageSizeOfPpon
    {
        get
        {
            return pagerDeal.PageSize;
        }
    }

    public string BusinessOrderId
    {
        set
        {
            txtBusinessOrderId.Text = value;
        }
        get
        {
            return txtBusinessOrderId.Text;
        }
    }

    private int addressCityId = -1;

    /// <summary>
    /// 地址的城市ID
    /// </summary>
    public int AddressCityId
    {
        get
        {
            if ((Request["selCity"] == null) || (Request["selCity"] == ""))
            {
                return addressCityId;
            }
            else
            {
                return int.Parse(Request["selCity"]);
            }
        }
        set { addressCityId = value; }
    }

    private int? addressTownshipId = null;

    /// <summary>
    /// 地址的鄉鎮市區ID
    /// </summary>
    public int? AddressTownshipId
    {
        get
        {
            if ((Request["selTownship"] == null) || (Request["selTownship"] == ""))
            {
                return addressTownshipId;
            }
            else
            {
                return int.Parse(Request["selTownship"]);
            }
        }
        set { addressTownshipId = value; }
    }

    /// <summary>
    /// 狀態
    /// </summary>
    public StoreStatus StoreStatus
    {
        get
        {
            int selected;
            if (int.TryParse(ddlStoreStatus.SelectedValue, out selected))
            {
                return (StoreStatus)selected;
            }
            else
            {
                return StoreStatus.Available;
            }
        }
        set
        {
            ddlStoreStatus.SelectedValue = ((int)value).ToString();
        }
    }

    /// <summary>
    /// 經緯度
    /// </summary>
    public KeyValuePair<string, string> LongitudeLatitude
    {
        get { return new KeyValuePair<string, string>(txtLatitude.Text, txtLongitude.Text); }
        set
        {
            txtLatitude.Text = value.Key;
            txtLongitude.Text = value.Value;
        }
    }

    /// <summary>
    /// 營業時間
    /// </summary>
    public string BusinessHour
    {
        get { return HttpUtility.HtmlDecode(tbBusinessHour.Text.Trim()); }
        set { tbBusinessHour.Text = value; }
    }
    /// <summary>
    /// 公休日
    /// </summary>
    public string CloseDateInformation
    {
        get { return tbCloseDate.Text.Trim(); }
        set { tbCloseDate.Text = value; }
    }

    public bool IsOpenBooking
    {
        get
        {
            return cbIsOpenBooking.Checked;
        }
        set
        {
            cbIsOpenBooking.Checked = value;
        }

    }

    public bool IsOpenReservationSetting
    {
        get
        {
            int reservationSelected;
            if (int.TryParse(rdlIsOpenReservationSetting.SelectedValue, out reservationSelected))
            {
                return (reservationSelected == 1);
            }
            else
            {
                return false;
            }
        }
        set
        {
            rdlIsOpenReservationSetting.SelectedValue = (value) ? "1" : "0";
        }

    }

    public string CityJSonData { get; set; }

    public List<int> MRTLocationCategory
    {
        get { return new JsonSerializer().Deserialize<List<int>>(hiddenMRTCategoryChecked.Value); }
        set { hiddenMRTCategoryChecked.Value = new JsonSerializer().Serialize(value); }
    }
    public List<KeyValuePair<string, int>> MRTLocationChecked
    {
        get { return new JsonSerializer().Deserialize<List<KeyValuePair<string, int>>>(hiddenMRTCategoryItem.Value); }
        set { hiddenMRTCategoryItem.Value = new JsonSerializer().Serialize(value); }
    }

    public KeyValuePair<string, string> ParentSellerInfo
    {
        get { return new KeyValuePair<string, string>(lbParentSellerId.Text, txtParentSellerName.Text); }
        set
        {
            lbParentSellerId.Text = value.Key;
            txtParentSellerName.Text = value.Value;
        }
    }

    public Guid? ParentSellerGuid
    {
        get
        {
            Guid? parentId = null;
            if (!string.IsNullOrEmpty(hidParentSellerGuid.Value))
            {
                parentId = Guid.Parse(hidParentSellerGuid.Value);
            }

            return parentId;
        }
        set
        {
            if (value.HasValue)
            {
                hidParentSellerGuid.Value = value.ToString();
            }
        }
    }

    public bool IsAgreeHouseNewContractSeller
    {
        get
        {
            return SellerFacade.IsAgreeNewContractSeller(SellerGuid, (int)DeliveryType.ToHouse);
        }
    }

    public bool IsAgreePponNewContractSeller
    {
        get
        {
            return SellerFacade.IsAgreeNewContractSeller(SellerGuid, (int)DeliveryType.ToShop);
        }
    }

    #endregion props

    #region local property

    /// <summary>
    /// 賣家異動的欄位名稱和用來顯示Literal
    /// </summary>
    public Dictionary<string, Literal> SellerChangeItemLiteral
    {
        get
        {
            Dictionary<string, Literal> change_items_literal = new Dictionary<string, Literal>();
            change_items_literal.Add(LunchKingSite.DataOrm.Seller.SellerNameColumn.ColumnName, lit_Seller_SellerName);
            change_items_literal.Add(LunchKingSite.DataOrm.Seller.SellerBossNameColumn.ColumnName, lit_Seller_BossName);
            change_items_literal.Add(LunchKingSite.DataOrm.Seller.SellerTelColumn.ColumnName, lit_Seller_Tel);
            //change_items_literal.Add(LunchKingSite.DataOrm.Seller.SellerFaxColumn.ColumnName, lit_Seller_Fax);
            //change_items_literal.Add(LunchKingSite.DataOrm.Seller.SellerMobileColumn.ColumnName, lit_Seller_Mobile);
            change_items_literal.Add(LunchKingSite.DataOrm.Seller.SellerAddressColumn.ColumnName, lit_Seller_Address);
            //change_items_literal.Add(LunchKingSite.DataOrm.Seller.SellerEmailColumn.ColumnName, lit_Seller_Email);
            //change_items_literal.Add(LunchKingSite.DataOrm.Seller.SellerBlogColumn.ColumnName, lit_Seller_Blog);
            change_items_literal.Add(LunchKingSite.DataOrm.Seller.CompanyNameColumn.ColumnName, lit_Seller_ComapnyName);
            change_items_literal.Add(LunchKingSite.DataOrm.Seller.SignCompanyIDColumn.ColumnName, lit_Seller_CompanySignId);
            change_items_literal.Add(LunchKingSite.DataOrm.Seller.CompanyBossNameColumn.ColumnName, lit_Seller_CompanyBossName);
            //change_items_literal.Add(LunchKingSite.DataOrm.Seller.MessageColumn.ColumnName, lit_Seller_Message);
            //change_items_literal.Add(LunchKingSite.DataOrm.Seller.SellerContactPersonColumn.ColumnName, lit_Seller_ContactPerson);
            //change_items_literal.Add(LunchKingSite.DataOrm.Seller.SellerDescriptionColumn.ColumnName, lit_Seller_Description);
            change_items_literal.Add(LunchKingSite.DataOrm.Seller.OauthClientAppIdColumn.ColumnName, lit_OauthClientAppId);
            return change_items_literal;
        }
    }

    /// <summary>
    /// 賣家異動的欄位名稱和用來輸入的TextBox
    /// </summary>
    public Dictionary<string, TextBox> SellerChangeItemTextBox
    {
        get
        {
            Dictionary<string, TextBox> change_items_textbox = new Dictionary<string, TextBox>();
            change_items_textbox.Add(LunchKingSite.DataOrm.Seller.SellerNameColumn.ColumnName, tbSellerName);
            change_items_textbox.Add(LunchKingSite.DataOrm.Seller.SellerBossNameColumn.ColumnName, tbSellerBossName);
            change_items_textbox.Add(LunchKingSite.DataOrm.Seller.SellerTelColumn.ColumnName, tbSellerTel);
            //change_items_textbox.Add(LunchKingSite.DataOrm.Seller.SellerFaxColumn.ColumnName, tbSellerFax);
            //change_items_textbox.Add(LunchKingSite.DataOrm.Seller.SellerMobileColumn.ColumnName, tbSellerMobile);
            change_items_textbox.Add(LunchKingSite.DataOrm.Seller.SellerAddressColumn.ColumnName, tbSellerAddr);
            //change_items_textbox.Add(LunchKingSite.DataOrm.Seller.SellerEmailColumn.ColumnName, tbSellerEmail);
            //change_items_textbox.Add(LunchKingSite.DataOrm.Seller.SellerBlogColumn.ColumnName, tbSellerBlog);
            change_items_textbox.Add(LunchKingSite.DataOrm.Seller.CompanyNameColumn.ColumnName, tbCompanyName);
            change_items_textbox.Add(LunchKingSite.DataOrm.Seller.SignCompanyIDColumn.ColumnName, tbSignCompanyID);
            change_items_textbox.Add(LunchKingSite.DataOrm.Seller.CompanyBossNameColumn.ColumnName, tbCompanyBossName);
            //change_items_textbox.Add(LunchKingSite.DataOrm.Seller.MessageColumn.ColumnName, tbSellerMessage);
            //change_items_textbox.Add(LunchKingSite.DataOrm.Seller.SellerContactPersonColumn.ColumnName, txtSellerContactPerson);
            //change_items_textbox.Add(LunchKingSite.DataOrm.Seller.SellerDescriptionColumn.ColumnName, tdSellerDescription);
            change_items_textbox.Add(LunchKingSite.DataOrm.Seller.OauthClientAppIdColumn.ColumnName, tbOauthClientAppId);
            return change_items_textbox;
        }
    }


    #endregion local property

    #region Enum

    ///// <summary>
    ///// 店鋪狀態
    ///// </summary>
    //private enum StoreStatus
    //{
    //    /// <summary>
    //    /// 預設顯示
    //    /// </summary>
    //    Initial = 0,

    //    /// <summary>
    //    /// 刪除(作廢)
    //    /// </summary>
    //    Cancel = 1
    //}

    #endregion Enum

    protected void Page_Load(object sender, EventArgs e)
    {
        Config = ProviderFactory.Instance().GetConfig();
        hp = ProviderFactory.Instance().GetProvider<IHumanProvider>();

        if (!Page.IsPostBack)
        {
            ddl_Seller_Category.DataSource = VourcherFacade.GetSellerSampleCategoryDictionary();
            ddl_Seller_Category.DataTextField = "Value";
            ddl_Seller_Category.DataValueField = "Key";
            ddl_Seller_Category.DataBind();
            SetSellerLevel();
            SetWeekly();
            SetShoppingCart();

            if (!CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.SetCloseDown))
            {
                this.formIsCloseDown.Enabled = false;
            }
            if (!Presenter.OnViewInitialized())
            {
                Response.Redirect("seller_list.aspx");
            }
            else
            {
                if (string.IsNullOrEmpty(Request.QueryString["sid"]))
                {
                    Seller.ActiveTabIndex = 0;
                    lbMode.Text = Resources.Localization.AddNew;
                    btnSellerSubmit.Text = Resources.Localization.AddNew;

                    TabPanelSellerDetail.Visible = false;
                    btnImportBusinessOrderInfo.Visible = false;
                    // this is not a proper way of doing MVP, will have to fix it
                    SetupStatusSelections((SellerStatusFlag)0, SellerTransportConfidence.None, DepartmentTypes.Meal, PostCheckoutActionType.NothingToDo);
                }
                else
                {
                    Seller.ActiveTabIndex = 0;
                    lbMode.Text = Resources.Localization.Edit;
                    btnSellerSubmit.Text = Resources.Localization.Update;
                    btnImportBusinessOrderInfo.Visible = true;
                }
            }
            syncBusinessHour.NavigateUrl = "#divBusinessHour";
            syncCloseDate.NavigateUrl = "#divCloseDate";

            if (Config.ShoppingCartEnabled)
            {
                tabShoppingCart.Visible = true;
            }

            //取得二線客服
            SellerMappingEmployee sme = hp.SellerMappingEmployeeGet(SellerGuid);
            if (sme.IsLoaded)
            {
                ViewEmployee emp = hp.ViewEmployeeGet(ViewEmployee.Columns.EmpId, sme.EmpId);
                if (emp.IsLoaded)
                {
                    lblsecond.Text = emp.EmpName + " " + emp.Email + " " + emp.Extension;
                }
                else
                {
                    lblsecond.Text = "";
                }
            }
            else
            {
                lblsecond.Text = "";
            }
        }

        //載入預設citys，供updateCity使用
        var jsonCol = from x in CityManager.Citys.Where(x => x.Code != "SYS")
                      select new
                      {
                          id = x.Id.ToString(),
                          name = x.CityName,
                          Townships = (from y in CityManager.TownShipGetListByCityId(x.Id) select new { id = y.Id.ToString(), name = y.CityName })
                      };
        CityJSonData = new JsonSerializer().Serialize(jsonCol);

        Presenter.OnViewLoaded();

        if (Config.EnableOpenStreetMap)
        {
            ljs.Text = @"<script type=""text/javascript"" src=""" + ResolveUrl("~/Tools/js/osm/OpenLayers.js") + @"""></script>";
        }
        else
        {
            if (Config.EnableGoogleMapApiV2)
            {
                ljs.Text = @"<script src=""https://maps.google.com/maps?file=api&v=2&key=" + LocationFacade.GetGoogleMapsAPIKey() + @""" type=""text/javascript""></script>";
            }
            else
            {
                ljs.Text = @"<script src=""https://maps.googleapis.com/maps/api/js?v=3&sensor=false&libraries=drawing"" type=""text/javascript""></script>";
            }
        }
    }

    public void ReloadSeller()
    {
        Response.Redirect(string.Format("{0}?sid={1}", "seller_add.aspx", SellerGuid.ToString()));
    }

    public void SetTabPanel(int panelNumber)
    {
        Seller.ActiveTabIndex = panelNumber;
    }

    public void SetImageFile(PhotoInfo pi)
    {
        ImageUtility.UploadFileWithResize(pi.PFile.ToAdapter(), pi.Type, pi.DestFilePath, pi.DestFileName, 157, 86);
    }

    #region TabPanel  Seller

    public void SetCityDropDown(List<LunchKingSite.DataOrm.City> city)
    {
        ddlCity.DataSource = city;
        ddlCity.DataBind();
        ddlCity.SelectedValue = "199";//台北市
    }

    public void SetZoneDropDown(Dictionary<int, string> zone)
    {
        ddlZone.DataSource = zone;
        ddlZone.DataBind();
    }

    public void SetSellerID(string SellerID)
    {
        lbSellerID.Text = SellerID;
    }

    private void SetSellerLevel()
    {
        //店家來源
        Dictionary<int, string> sellerFrom = new Dictionary<int, string>();
        foreach (ProposalSellerFrom item in Enum.GetValues(typeof(ProposalSellerFrom)))
        {
            sellerFrom[(int)item] = Helper.GetDescription(item);
        }
        ddlSellerFrom.DataSource = sellerFrom;
        ddlSellerFrom.DataTextField = "Value";
        ddlSellerFrom.DataValueField = "Key";
        ddlSellerFrom.DataBind();

        //店家屬性
        Dictionary<int, string> sellerProperty = new Dictionary<int, string>();
        foreach (ProposalSellerPorperty item in Enum.GetValues(typeof(ProposalSellerPorperty)))
        {
            sellerProperty[(int)item] = Helper.GetDescription(item);
        }
        chkSellerPorperty.DataSource = sellerProperty;
        chkSellerPorperty.DataTextField = "Value";
        chkSellerPorperty.DataValueField = "Key";
        chkSellerPorperty.DataBind();
    }

    private void SetupStatusSelections(SellerStatusFlag status, SellerTransportConfidence confidence, DepartmentTypes dept, PostCheckoutActionType pca)
    {
        ddlRemittanceType.Items.Clear();
        ddlRemittanceType.Items.Add(new ListItem("請選擇", string.Empty));
        var remittanceTypes = WebUtility.GetListItemFromEnum(RemittanceType.Others).OrderByDescending(x => x.Value);

        if (Config.IsRemittanceFortnightly && IsAgreePponNewContractSeller)
        {
            foreach (var type in remittanceTypes)
            {
                switch (int.Parse(type.Value))
                {
                    case (int)RemittanceType.Others:
                    case (int)RemittanceType.Flexible:
                    case (int)RemittanceType.Monthly:
                        ddlRemittanceType.Items.Add(new ListItem(type.Text, type.Value));
                        break;
                    default:
                        break;
                }
            }
        }
        else
        {
            foreach (var type in remittanceTypes)
            {
                switch (int.Parse(type.Value))
                {
                    case (int)RemittanceType.Others:
                    case (int)RemittanceType.Flexible:
                    case (int)RemittanceType.Monthly:
                    case (int)RemittanceType.Weekly:
                        ddlRemittanceType.Items.Add(new ListItem(type.Text, type.Value));
                        break;
                    default:
                        break;
                }
            }
        }
        
    }

    public void SetSellerGet(Seller data, IEnumerable<Audit> auditLogs, List<KeyValuePair<Guid, string>> storeLists)
    {
        // 審核狀態
        liTempStatus.Text = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, (SellerTempStatus)data.TempStatus);
        // 申請核准
        btnSellerApprove.Visible = CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.Completed) && data.TempStatus == (int)SellerTempStatus.Applied;
        // 申請退回
        btnSellerReturned.Visible = CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.Completed) && data.TempStatus == (int)SellerTempStatus.Applied;
        // 申請變更
        btnSellerApply.Visible = CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.Applied) && data.TempStatus != (int)SellerTempStatus.Applied;

        lbSellerName.Text = @"(" + data.SellerName + @")";
        lbSellerID.Text = data.SellerId;
        hfSellerID.Value = data.SellerId;
        tbSellerName.Text = data.SellerName;
        tbSellerBossName.Text = data.SellerBossName;
        tbSellerTel.Text = data.SellerTel;
        hlSellerTree.NavigateUrl = string.Format("../../Sal/SellerTreeView?sid={0}", data.Guid);
        tbOauthClientAppId.Text = data.OauthClientAppId;
        tbStoreRelationCode.Text = data.StoreRelationCode;

        if (LocationUtility.GetParentCityFromCache(data.CityId) != null)
        {
            ddlCity.SelectedValue = LocationUtility.GetParentCityFromCache(data.CityId).Id.ToString();
        }
        ddlZone.SelectedValue = data.CityId.ToString();
        hfZone.Value = data.CityId.ToString();
        tbSellerAddr.Text = data.SellerAddress;

        ViewEmployee emp = new ViewEmployee();
        if (data.SalesId != null)
        {
            emp = hp.ViewEmployeeGet(ViewEmployee.Columns.UserId, data.SalesId);
        }

        //公司匯款帳號
        tbCompanyAccount.Text = data.CompanyAccount;
        tbCompanyAccountName.Text = data.CompanyAccountName;
        //帳務聯絡人
        tbAccountantName.Text = data.AccountantName;
        //帳務聯絡電話
        tbAccountantTel.Text = data.AccountantTel;
        tbCompanyBankCode.Text = data.CompanyBankCode;
        tbCompanyBossName.Text = data.CompanyBossName;
        tbCompanyBranchCode.Text = data.CompanyBranchCode;
        tbCompanyEmail.Text = data.CompanyEmail;
        tbCompanyID.Text = data.CompanyID;
        tbSignCompanyID.Text = data.SignCompanyID;
        tbCompanyName.Text = data.CompanyName;
        tbCompanyNotice.Text = data.CompanyNotice;

        SellerCategory = data.SellerCategory;

        formIsCloseDown.SelectedValue = data.IsCloseDown ? "true" : "false";
        formCloseDownDate.Text = data.CloseDownDate.HasValue ? data.CloseDownDate.Value.ToString("yyyy/MM/dd") : "";

        SetupStatusSelections((SellerStatusFlag)data.SellerStatus,
                              SellerFacade.GetTransportConfidence(data.SellerStatus),
                              (DepartmentTypes)data.Department, (PostCheckoutActionType)data.PostCkoutAction);

        if (!string.IsNullOrEmpty(data.SellerLogoimgPath))
        {
            //imgDefaultSellerPhoto.ImageUrl = ImageFacade.GetMediaPathsFromRawData(data.SellerLogoimgPath, MediaType.SellerPhotoSmall)[0];
            //imgDefaultSellerPhoto.DataBind();
        }

        if (data.SellerInvoice == null)
        {
            data.SellerInvoice = "0";
        }

        lbSellerLevel.Text = data.SellerLevel.HasValue ? Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, (SellerLevel)data.SellerLevel.Value).Split('：')[0] : string.Empty;
        hdSellerLevel.Value = data.SellerLevel.HasValue ? data.SellerLevel.Value.ToString() : string.Empty;
        ddlSellerFrom.SelectedValue = (data.SellerFrom ?? -1).ToString();
        if (!string.IsNullOrEmpty(data.SellerPorperty))
        {
            string[] pros = data.SellerPorperty.Split(",");
            foreach (string pro in pros)
            {
                var listProperty = from i in chkSellerPorperty.Items.Cast<ListItem>()
                                   where pro.Equals(((ListItem)i).Value)
                                   select i;

                listProperty.First().Selected = true;
            }

        }

        //聯絡人
        var contacts = new List<Seller.MultiContracts>();

        if (!string.IsNullOrEmpty(data.Contacts))
        {
            contacts = new JsonSerializer().Deserialize<List<Seller.MultiContracts>>(data.Contacts);
        }
        else
        {
            //一般聯絡人
            var multiContract = new Seller.MultiContracts
            {
                Type = ((int)ProposalContact.Normal).ToString(),
                ContactPersonName = data.SellerContactPerson,
                SellerTel = data.SellerTel,
                SellerMobile = data.SellerMobile,
                SellerFax = data.SellerFax,
                ContactPersonEmail = data.SellerEmail,
                Others = string.Empty
            };

            contacts.Add(multiContract);

            //退換貨聯絡人
            if (!string.IsNullOrEmpty(data.ReturnedPersonTel) || !string.IsNullOrEmpty(data.ReturnedPersonEmail))
            {
                var multiContractR = new Seller.MultiContracts
                {
                    Type = ((int)ProposalContact.ReturnPerson).ToString(),
                    ContactPersonName = data.ReturnedPersonName,
                    SellerTel = data.ReturnedPersonTel,
                    SellerMobile = string.Empty,
                    SellerFax = string.Empty,
                    ContactPersonEmail = data.ReturnedPersonEmail,
                    Others = string.Empty
                };

                contacts.Add(multiContractR);
            }
        }

        repContacts.DataSource = contacts;
        repContacts.DataBind();

        //店鋪營業資料
        StoreStatus = (StoreStatus)data.StoreStatus;
        tbStoreTel.Text = data.StoreTel;
        AddressCityId = data.StoreCityId ?? -1;
        AddressTownshipId = data.StoreTownshipId;
        tbStoreAddress.Text = data.StoreAddress;
        SqlGeography geography = LocationFacade.GetGeographyByCoordinate(data.Coordinate);
        if (geography != null)
        {
            LongitudeLatitude = new KeyValuePair<string, string>(geography.Lat.ToString(), geography.Long.ToString());
        }
        else
        {
            LongitudeLatitude = LocationFacade.GetArea(CityManager.CityTownShopStringGet(AddressTownshipId.Value) + data.StoreAddress);
        }
        SetLocationDropDownList();

        BusinessHour = data.OpenTime;
        tbCloseDate.Text = data.CloseDate;
        tbMrt.Text = data.Mrt;
        tbCar.Text = data.Car;
        tbBus.Text = data.Bus;
        tbOV.Text = data.OtherVehicles;
        tbWebUrl.Text = data.WebUrl;
        tbFBUrl.Text = data.FacebookUrl;
        tbBlogUrl.Text = data.BlogUrl;
        tbOtherUrl.Text = data.OtherUrl;
        tbRemark.Text = data.StoreRemark;
        cbCreditCardAvailable.Checked = data.CreditcardAvailable;
        cbIsOpenBooking.Checked = data.IsOpenBooking;
        rdlIsOpenReservationSetting.SelectedValue = (data.IsOpenReservationSetting) ? "1" : "0";

        //所有分店
        rptBStore.DataSource = storeLists;
        rptBStore.DataBind();

        rptCStore.DataSource = storeLists;
        rptCStore.DataBind();

        //hfPhotoPath.Value = data.SellerLogoimgPath;
        //hfVideoPath.Value = data.SellerVideoPath;

        rpt_AuditLog.DataSource = auditLogs;
        rpt_AuditLog.DataBind();

        if (data.RemittanceType.HasValue)
        {
            ddlRemittanceType.SelectedValue = data.RemittanceType.Value.ToString();
        }

        //cookie還是有問題先改回session觀察看看

        Session["TempSeller"] = data;
    }

    private void SetLocationDropDownList()
    {
        List<City> citys = CityManager.Citys.Where(x => x.Code != "SYS").ToList();
        citys.Insert(0, new City() { Id = -1, CityName = "請選擇" });
        repCity.DataSource = citys;
        repCity.DataBind();

        var townships = CityManager.TownShipGetListByCityId(addressCityId);
        if (addressCityId != -1)
        {
            var nowCitys = CityManager.Citys.Where(x => x.Id == addressCityId).ToList();
            if (nowCitys.Count > 0)
            {
                townships = CityManager.TownShipGetListByCityId(nowCitys.First().Id);
            }
        }
        repTown.DataSource = townships;
        repTown.DataBind();
    }

    private string CheckJTokenEmpty(JToken jtoken)
    {
        if (jtoken != null)
        {
            return jtoken.ToString().Replace("\"", string.Empty);
        }
        else
        {
            return string.Empty;
        }
    }

    protected void ddlCity_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (this.SelectCityChanged != null)
        {
            SelectCityChanged(this, new EventArgs());
        }
    }

    protected void ddlZone_SelectedIndexChanged(object sender, EventArgs e)
    { }

    protected void submit_check(object sender, EventArgs e)
    {
        string msg = string.Empty;
        if (!(
            string.IsNullOrWhiteSpace(tbCompanyID.Text) &&
            string.IsNullOrWhiteSpace(tbCompanyBankCode.Text) &&
            string.IsNullOrWhiteSpace(tbCompanyBranchCode.Text) &&
            string.IsNullOrWhiteSpace(tbCompanyAccountName.Text) &&
            string.IsNullOrWhiteSpace(tbCompanyEmail.Text) &&
            string.IsNullOrWhiteSpace(tbCompanyNotice.Text) &&
            string.IsNullOrWhiteSpace(tbCompanyAccount.Text)))
        {
            Regex r_id = new Regex(@"([A-Z]|[a-z])\d{9}");
            Regex r_foreignId = new Regex(@"([A-Z]|[a-z]){2}\d{8}"); //外籍人士的編號 規則是前兩碼英文後八碼數字
            Regex r_comid = new Regex(@"\d{8}");
            Regex r_account = new Regex(@"\d{14}");
            Regex r_bank = new Regex(@"\d{3}");
            Regex r_branch = new Regex(@"\d{4}");

            if (tbSignCompanyID.Text.Trim().Length == 8 && !r_comid.IsMatch(tbSignCompanyID.Text.Trim()))
            {
                msg = @"簽約公司統一編號格式錯誤";
            }

            if (tbSignCompanyID.Text.Trim().Length == 10 && !(r_id.IsMatch(tbSignCompanyID.Text.Trim()) || r_foreignId.IsMatch(tbSignCompanyID.Text.Trim())))
            {
                msg = @"簽約公司身分證格式錯誤";
            }

            if (tbSignCompanyID.Text.Trim().Length != 10 && tbSignCompanyID.Text.Trim().Length != 8)
            {
                msg = @"簽約公司ID/統一編號格式錯誤";
            }

            if (tbCompanyID.Text.Trim().Length == 8 && !r_comid.IsMatch(tbCompanyID.Text.Trim()))
            {
                msg = @"受款人統一編號格式錯誤";
            }

            if (tbCompanyID.Text.Trim().Length == 10 && !(r_id.IsMatch(tbCompanyID.Text.Trim()) || r_foreignId.IsMatch(tbCompanyID.Text.Trim())))
            {
                msg = @"受款人身分證格式錯誤";
            }

            if (tbCompanyID.Text.Trim().Length != 10 && tbCompanyID.Text.Trim().Length != 8)
            {
                msg = @"受款人ID/統一編號格式錯誤";
            }

            if (tbCompanyAccount.Text.Trim().Length > 14)
            {
                msg = @"帳號長度大於14碼";
            }
            else
            {
                if (!tbCompanyAccount.Text.Equals(""))
                {
                    tbCompanyAccount.Text = "00000000000000".Substring(0, 14 - tbCompanyAccount.Text.Length) + tbCompanyAccount.Text;
                }

                if (tbCompanyAccount.Text.Trim().Length != 14 || !r_account.IsMatch(tbCompanyAccount.Text.Trim()))
                {
                    msg = @"帳號格式錯誤";
                }
            }

            if (tbCompanyBankCode.Text.Trim().Length != 3 || !r_bank.IsMatch(tbCompanyBankCode.Text.Trim()))
            {
                msg = @"銀行代碼格式錯誤";
            }

            if (tbCompanyBranchCode.Text.Trim().Length != 4 || !r_branch.IsMatch(tbCompanyBranchCode.Text.Trim()))
            {
                msg = @"分行代碼格式錯誤";
            }


            if (tbOauthClientAppId.Text.Trim().Length > 0)
            {
                if (tbOauthClientAppId.Text.Trim().Length != 15 || !Helper.IsNumeric(tbOauthClientAppId.Text.Trim()))
                {
                    msg = @"POS串接APP ID格式錯誤";
                }
                else
                {
                    if (OAuthFacade.GetClient(tbOauthClientAppId.Text.Trim()) == null)
                        msg = @"POS串接APP ID不存在";
                }
            }
        }

        if (msg.Length > 0)
        {
            ClientScript.RegisterStartupScript(GetType(), "alert", "alert('" + msg + "');", true);
        }
        else
        {
            btnSellerSubmit_Click(sender, e);
        }
    }

    protected void btnSellerSubmit_Click(object sender, EventArgs e)
    {
        if (UpdateSellerInfo != null)
        {
            string[] IdPhoto = { lbSellerID.Text, "logo.jpg" };
            string[] IdVideo = { lbSellerID.Text, "intro.wmv" };

            Seller info = new Seller(); // user seller model as the data passing between presenter & view

            if (((Button)sender).Text != @"更新")
            {
                if (this.CreateSellerID != null)
                {
                    CreateSellerID(this, new EventArgs());
                }
            }

            info.SellerId = lbSellerID.Text;
            info.SellerName = tbSellerName.Text;
            info.SellerBossName = tbSellerBossName.Text;
            info.SellerTel = tbSellerTel.Text;
            info.CityId = int.Parse(ddlZone.SelectedValue);
            info.SellerAddress = tbSellerAddr.Text;
            info.SellerInvoice = "0";
            info.OauthClientAppId = tbOauthClientAppId.Text;
            info.StoreRelationCode = tbStoreRelationCode.Text;

            //賣家規模
            int level;
            info.SellerLevel = int.TryParse(hdSellerLevel.Value, out level) ? (int?)level : null;

            //賣家來源
            int sellerSource;
            if (!string.IsNullOrWhiteSpace(ddlSellerFrom.SelectedValue) && int.TryParse(ddlSellerFrom.SelectedValue, out sellerSource))
            {
                info.SellerFrom = (short)sellerSource;
            }

            //店家屬性
            var listProperty = from i in chkSellerPorperty.Items.Cast<ListItem>()
                               where ((ListItem)i).Selected
                               select i.Value;


            info.SellerPorperty = string.Join(",", listProperty);

            DateTime verifiedCloseTime = DateTime.MinValue;
            DateTime.TryParse(formCloseDownDate.Text, out verifiedCloseTime);
            if (DateTime.Compare(DateTime.MinValue, verifiedCloseTime) < 0)
            {
                info.CloseDownDate = verifiedCloseTime;
                info.IsCloseDown = true;
            }
            else
            {
                info.IsCloseDown = false;
            }

            if (formIsCloseDown.SelectedValue == "false")  //正常營業 => 忽略營業結束時間
            {
                info.IsCloseDown = false;
                info.CloseDownDate = null;
            }
            info.StoreStatus = (int)StoreStatus;
            info.SellerCategory = SellerCategory;
            //公司匯款帳號
            info.CompanyAccount = tbCompanyAccount.Text.Replace(" ", "");
            info.CompanyAccountName = tbCompanyAccountName.Text;
            //帳務聯絡人
            info.AccountantName = tbAccountantName.Text.Replace(" ", "");
            //帳務聯絡電話
            info.AccountantTel = tbAccountantTel.Text.Replace(" ", "");
            info.CompanyBankCode = tbCompanyBankCode.Text.Replace(" ", "");
            info.CompanyBossName = tbCompanyBossName.Text.Replace(" ", "");
            info.CompanyBranchCode = tbCompanyBranchCode.Text.Replace(" ", "");
            info.CompanyEmail = tbCompanyEmail.Text.Replace(" ", "");
            info.CompanyID = tbCompanyID.Text.Replace(" ", "");
            info.SignCompanyID = tbSignCompanyID.Text.Replace(" ", "");
            info.CompanyName = tbCompanyName.Text.Replace(" ", "");
            info.CompanyNotice = tbCompanyNotice.Text.Replace(" ", "");

            int tempRemittanceType;
            if (int.TryParse(ddlRemittanceType.SelectedValue, out tempRemittanceType))
            {
                info.RemittanceType = tempRemittanceType;
            }

            info.SellerStatus = 0;
            //店鋪營業資料
            var township = CityManager.TownShipGetById(AddressTownshipId == null ? -1 : AddressTownshipId.Value);
            info.StoreTel = tbStoreTel.Text;
            info.StoreTownshipId = AddressTownshipId;
            info.StoreCityId = township == null ? -1 : township.ParentId.Value;
            info.StoreAddress = tbStoreAddress.Text;

            if (info.StoreTownshipId.HasValue)
            {
                KeyValuePair<string, string> latitude_longitude = new KeyValuePair<string, string>();
                Double lat, lont;
                if (!Double.TryParse(LongitudeLatitude.Key, out lat) || !Double.TryParse(LongitudeLatitude.Value, out lont)
                    || Math.Abs(lat) > 90 || Math.Abs(lont) > 180)
                {
                    latitude_longitude = LocationFacade.GetArea(CityManager.CityTownShopStringGet(info.StoreTownshipId.Value) + info.StoreAddress);
                }
                else
                {
                    latitude_longitude = LongitudeLatitude;
                }
                info.Coordinate = LocationFacade.GetGeographyWKT(latitude_longitude.Key, latitude_longitude.Value);
            }
            info.OpenTime = BusinessHour;
            info.CloseDate = CloseDateInformation;
            info.Mrt = tbMrt.Text.Trim();
            info.Bus = tbBus.Text.Trim();
            info.Car = tbCar.Text.Trim();
            info.OtherVehicles = tbOV.Text.Trim();
            info.WebUrl = CheckUrlFormat(tbWebUrl.Text);
            info.FacebookUrl = CheckUrlFormat(tbFBUrl.Text);
            info.BlogUrl = CheckUrlFormat(tbBlogUrl.Text);
            info.OtherUrl = CheckUrlFormat(tbOtherUrl.Text);
            info.StoreRemark = tbRemark.Text;
            //店鋪提供服務
            info.CreditcardAvailable = cbCreditCardAvailable.Checked;
            info.IsOpenBooking = cbIsOpenBooking.Checked;
            info.IsOpenReservationSetting = (rdlIsOpenReservationSetting.SelectedValue == "1");
            info.Department = (int)DepartmentTypes.Ppon;

            Dictionary<int, bool> SellerCategoryList = new Dictionary<int, bool>();

            UpdateSellerInfo(this, new DataEventArgs<Seller>(info));

            if (info.Guid != Guid.Empty && string.IsNullOrEmpty(UpdateMessage))
            {
                if (UpdateSellerCategory != null)
                {
                    UpdateSellerCategory(this, new DataEventArgs<Dictionary<int, bool>>(SellerCategoryList));
                }

                if (ddlZone.SelectedValue != hfZone.Value)
                {
                    hfZone.Value = ddlZone.SelectedValue;
                    hfSellerID.Value = lbSellerID.Text;
                }
                Response.Redirect(string.Format("{0}?sid={1}", "seller_add.aspx", info.Guid));
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.Page.GetType(), "alert", string.Format("alert('{0}');", UpdateMessage), true);
            }
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("seller_list.aspx");
    }

    protected void tbSellerDetail_Click(object sender, EventArgs e)
    {
        if (SellerDetailSave != null)
        {
            SellerDetailSave(sender, e);
        }
    }

    protected void btnImportBusinessOrderInfo_Click(object sender, EventArgs e)
    {
        if (ImportBusinessOrderInfo != null)
        {
            ImportBusinessOrderInfo(this, new DataEventArgs<string>(txtBusinessOrderId.Text.Trim()));
        }
    }

    protected void btnSellerApprove_Click(object sender, EventArgs e)
    {
        if (this.SellerApprove != null)
        {
            this.SellerApprove(this, new DataEventArgs<SellerTempStatus>(SellerTempStatus.Completed));
        }
    }

    protected void btnSellerReturned_Click(object sender, EventArgs e)
    {
        if (this.SellerApprove != null)
        {
            this.SellerApprove(this, new DataEventArgs<SellerTempStatus>(SellerTempStatus.Returned));
        }
    }

    protected void btnSellerApply_Click(object sender, EventArgs e)
    {
        if (this.SellerApprove != null)
        {
            this.SellerApprove(this, new DataEventArgs<SellerTempStatus>(SellerTempStatus.Applied));
        }
    }
    protected void btnCopySeller_Click(object sender, EventArgs e)
    {
        if (this.CopySeller != null)
        {
            this.CopySeller(sender, e);
        }
    }

    public void SetBusinessOrderInfo(BusinessOrder businessOrder)
    {
        if (businessOrder != null)
        {
            txtBusinessOrderId.Text = businessOrder.BusinessOrderId;
            if (businessOrder.BusinessType == BusinessType.Product)
            {
                tbSellerName.Text = businessOrder.CompanyName;
            }
            else
            {
                tbSellerName.Text = businessOrder.SellerName;
            }
            tbSellerBossName.Text = businessOrder.BossName;
            tbSellerTel.Text = businessOrder.BossPhone;
            tbCompanyBossName.Text = businessOrder.BossName;
            tbCompanyName.Text = businessOrder.CompanyName;
            if (businessOrder.IsPrivateContract)
            {
                tbSignCompanyID.Text = businessOrder.PersonalId;
            }
            else
            {
                tbSignCompanyID.Text = businessOrder.GuiNumber;
            }

            // 解析地址城市ID、鄉鎮市區ID
            string addressValue = businessOrder.BossAddress;
            LunchKingSite.DataOrm.City city = CityManager.CityGetByStringAddress(addressValue);
            if (!string.IsNullOrEmpty(addressValue) && city != null)
            {
                LunchKingSite.DataOrm.City township = CityManager.TownshipIdGetByStringAddress(city.Id, addressValue);
                if (township != null)
                {
                    if (ddlCity.Items.FindByValue(city.Id.ToString()) != null)
                    {
                        ddlCity.SelectedValue = city.Id.ToString();
                        addressValue = addressValue.Replace(city.CityName, string.Empty);
                        if (this.SelectCityChanged != null)
                        {
                            SelectCityChanged(this, new EventArgs());
                        }
                    }

                    if (ddlZone.Items.FindByValue(township.Id.ToString()) != null)
                    {
                        ddlZone.SelectedValue = township.Id.ToString();
                        addressValue = addressValue.Replace(township.CityName, string.Empty);
                    }
                }
                tbSellerAddr.Text = addressValue;
            }
            tbCompanyBankCode.Text = businessOrder.AccountingBankId;
            tbCompanyBranchCode.Text = businessOrder.AccountingBranchId;
            tbCompanyAccount.Text = businessOrder.Account;
            tbCompanyAccountName.Text = businessOrder.Accounting;
            tbCompanyID.Text = businessOrder.AccountingId;
            tbAccountantName.Text = businessOrder.AccountingName;
            tbAccountantTel.Text = businessOrder.AccountingTel;
            tbCompanyEmail.Text = businessOrder.AccountingEmail;

        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.Page.GetType(), "alert", "alert('查無此工單編號!!');", true);
        }
    }
    protected void rptBStore_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            var dataItem = (KeyValuePair<Guid, string>)e.Item.DataItem;
            CheckBox chkCheck = (CheckBox)e.Item.FindControl("chkStore");

            chkCheck.InputAttributes["value"] = dataItem.Key.ToString();
        }
    }

    protected void rptCStore_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            var dataItem = (KeyValuePair<Guid, string>)e.Item.DataItem;
            CheckBox chkCheck = (CheckBox)e.Item.FindControl("chkStore");

            chkCheck.InputAttributes["value"] = dataItem.Key.ToString();
        }
    }

    private void SetShoppingCart()
    {
        List<ShoppingCartManageList> items = SellerFacade.ShoppingCartFreightsSetBySellerGuid(SellerGuid);
        if (items == null)
        {
            items = new List<ShoppingCartManageList>();
        }

        rptShoppingCart.DataSource = items;
        rptShoppingCart.DataBind();
    }

    private void SetWeekly()
    {
        Dictionary<string, string> weekly = new Dictionary<string, string>();
        weekly.Add("0", "週一");
        weekly.Add("1", "週二");
        weekly.Add("2", "週三");
        weekly.Add("3", "週四");
        weekly.Add("4", "週五");
        weekly.Add("5", "週六");
        weekly.Add("6", "週日");

        Dictionary<string, string> weeklyHour = new Dictionary<string, string>();
        for (int iHour = 1; iHour <= 24; iHour++)
        {
            weeklyHour.Add(iHour.ToString().PadLeft(2, '0'), iHour.ToString().PadLeft(2, '0'));
        }
        //營業時間
        ddl_WeeklyBeginHour.DataValueField = "Key";
        ddl_WeeklyBeginHour.DataTextField = "Value";
        ddl_WeeklyBeginHour.DataSource = weeklyHour;
        ddl_WeeklyBeginHour.DataBind();

        ddl_WeeklyEndHour.DataValueField = "Key";
        ddl_WeeklyEndHour.DataTextField = "Value";
        ddl_WeeklyEndHour.DataSource = weeklyHour;
        ddl_WeeklyEndHour.DataBind();

        //公休日
        ddl_close_WeeklyBeginHour.DataValueField = "Key";
        ddl_close_WeeklyBeginHour.DataTextField = "Value";
        ddl_close_WeeklyBeginHour.DataSource = weeklyHour;
        ddl_close_WeeklyBeginHour.DataBind();

        ddl_close_WeeklyEndHour.DataValueField = "Key";
        ddl_close_WeeklyEndHour.DataTextField = "Value";
        ddl_close_WeeklyEndHour.DataSource = weeklyHour;
        ddl_close_WeeklyEndHour.DataBind();

        Dictionary<string, string> weeklyMinute = new Dictionary<string, string>();
        for (int iMinute = 0; iMinute < 60; iMinute += 5)
        {
            weeklyMinute.Add(iMinute.ToString().PadLeft(2, '0'), iMinute.ToString().PadLeft(2, '0'));
        }
        //營業時間
        ddl_WeeklyBeginMinute.DataValueField = "Key";
        ddl_WeeklyBeginMinute.DataTextField = "Value";
        ddl_WeeklyBeginMinute.DataSource = weeklyMinute;
        ddl_WeeklyBeginMinute.DataBind();

        ddl_WeeklyEndMinute.DataValueField = "Key";
        ddl_WeeklyEndMinute.DataTextField = "Value";
        ddl_WeeklyEndMinute.DataSource = weeklyMinute;
        ddl_WeeklyEndMinute.DataBind();

        ddl_close_WeeklyBeginMinute.DataValueField = "Key";
        ddl_close_WeeklyBeginMinute.DataTextField = "Value";
        ddl_close_WeeklyBeginMinute.DataSource = weeklyMinute;
        ddl_close_WeeklyBeginMinute.DataBind();

        ddl_close_WeeklyEndMinute.DataValueField = "Key";
        ddl_close_WeeklyEndMinute.DataTextField = "Value";
        ddl_close_WeeklyEndMinute.DataSource = weeklyMinute;
        ddl_close_WeeklyEndMinute.DataBind();

        //公休時間
        Dictionary<int, string> flags = new Dictionary<int, string>();
        foreach (var item in Enum.GetValues(typeof(StoreCloseDate)))
        {
            flags[(int)item] = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, (StoreCloseDate)item);
        }
        ddlCloseDate.DataSource = flags;
        ddlCloseDate.DataTextField = "Value";
        ddlCloseDate.DataValueField = "Key";
        ddlCloseDate.DataBind();



    }

    private string CheckUrlFormat(string url)
    {
        url = HttpUtility.UrlDecode(url);
        if (!string.IsNullOrWhiteSpace(url))
        {
            if (url.StartsWith("http"))
            {
                return url.Trim();
            }
            else
            {
                return "http://" + url.Trim();
            }
        }

        return url;
    }

    #endregion TabPanel  Seller

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////

    private void VisablePanel(int number, bool show)
    {
        if (!show)
        {
            if (number == 0)
            {

            }
        }
        else
        {
            switch (number)
            {
                case 0:
                    break;
            }
        }
    }

    #region TabPanel  ItemMenuList

    protected bool ShowItemStatus(object status)
    {
        if (((int)status & (int)ItemStatusFlag.Online) != 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    protected string SetItemImagePath(object value)
    {
        if (value != null)
        {
            return ImageFacade.GetMediaPath(value.ToString(), MediaType.ItemPhotoSmall);
        }
        else
        {
            return string.Empty;
        }
    }

    #endregion TabPanel  ItemMenuList

    #region TabPanel_AccessoryList

    protected string ShowBizHrType(object type)
    {
        return Helper.GetLocalizedEnum((BusinessHourType)type);
    }

    protected string ShowBizHrOS(object type, string Mode)
    {
        TimeSpan days = Convert.ToDateTime(type).Date.Subtract(Convert.ToDateTime("1900/1/1").Date);

        if (Mode == "Type")
        {
            if (int.Parse(days.Days.ToString()) < 1)
            {
                return Resources.Localization.CarryToday;
            }
            else if (int.Parse(days.Days.ToString()) == 1)
            {
                return Resources.Localization.CarryTomorrow;
            }
            else
            {
                return days.Days + Resources.Localization.CarryFuture;
            }
        }
        else
        {
            return ((DateTime)type).ToString("HH:mm");
        }
    }

    #region AccessoryGroup GridView action class

    protected void gvAccessoryGroup_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        if (DeleteAccessoryGroup != null)
        {
            VisablePanel(0, false);
        }
    }


    #endregion AccessoryGroup GridView action class

    #endregion TabPanel_AccessoryList

    #region TabPanel PponSetup

    public void SetPponList(ViewPponDealCollection orders)
    {
        gvOrder.DataSource = orders;
        gvOrder.DataBind();
    }

    #region PPonOrder GridView action class

    protected void gvOrder_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            HyperLink hlEventName = (HyperLink)e.Row.Cells[1].FindControl("hlEventName");
            Literal ls = (Literal)e.Row.Cells[2].FindControl("ls");
            Literal ld = (Literal)e.Row.Cells[8].FindControl("ld");
            Literal oq = (Literal)e.Row.Cells[4].FindControl("oq");
            Literal oa = (Literal)e.Row.Cells[5].FindControl("oa");
            Label lbFPType = (Label)e.Row.Cells[9].FindControl("lbFPType");

            ViewPponDeal vpol = (ViewPponDeal)e.Row.DataItem;

            string newEventName = PponFacade.EventNameGet(vpol);

            /*企劃確認購買人(份)數不做退貨完成後數量處理, 但要在此修正上線前的檔次都要照原邏輯執行*/
            bool stopRefundQuantityCalculateDate = vpol.BusinessHourOrderTimeS >= Config.StopRefundQuantityCalculateDate;

            hlEventName.Text = newEventName.TrimToMaxLength(40, "...");
            hlEventName.NavigateUrl = string.Format("../ppon/setup.aspx?bid={0}", vpol.BusinessHourGuid);

            oq.Text = Convert.ToString(stopRefundQuantityCalculateDate ? vpol.OrderedIncludeRefundQuantity : vpol.OrderedQuantity);
            oa.Text = string.Format("{0:C}", stopRefundQuantityCalculateDate ? vpol.OrderedIncludeRefundTotal : vpol.OrderedTotal);

            if (vpol.BusinessHourOrderTimeS > DateTime.Now)
            {
                ls.Text = Phrase.NotToBegin;
                ld.Text = Phrase.NotToBegin;
            }
            else if (vpol.BusinessHourOrderTimeS <= DateTime.Now && vpol.BusinessHourOrderTimeE > DateTime.Now)
            {
                ls.Text = Phrase.Proceeding;
                ls.Text += vpol.BusinessHourOrderMinimum <= vpol.OrderedQuantity ? "<br/>" + Phrase.DealIsOn : string.Empty;
                ld.Text = Phrase.ToBeSettled;
                e.Row.BackColor = System.Drawing.Color.Yellow; // determine background
            }
            else if (vpol.BusinessHourOrderTimeE <= DateTime.Now)
            {
                ls.Text = Phrase.EventExpired + "<br/>";
                ls.Text += vpol.BusinessHourOrderMinimum <= vpol.OrderedQuantity ? Phrase.DealIsOn : Phrase.DealIsFailure;

                if (vpol.BusinessHourOrderMinimum <= vpol.OrderedQuantity)
                {
                    e.Row.BackColor = System.Drawing.Color.Red; // determine background
                }
                else
                {
                    ld.Text = Phrase.PaymentReqCancelled;
                    e.Row.BackColor = System.Drawing.Color.Gray; // determine background
                }
            }

            if ((vpol.BusinessHourStatus & (int)BusinessHourStatus.ComboDealMain) > 0)
            {
                lbFPType.Text = "多選項代表檔";
                lbFPType.ForeColor = System.Drawing.Color.Red;
            }
            else if ((vpol.BusinessHourStatus & (int)BusinessHourStatus.ComboDealSub) > 0)
            {
                lbFPType.Text = "多選項子檔";
                lbFPType.ForeColor = System.Drawing.Color.Blue;
            }
        }
    }

    public event EventHandler<DataEventArgs<int>> DealPagerGetCount;

    public event EventHandler<DataEventArgs<int>> DealPagerChanged;

    protected int pagerDealGetCount()
    {
        if (DealPagerGetCount != null)
        {
            var e = new DataEventArgs<int>(0);
            DealPagerGetCount(this, e);
            return e.Data;
        }
        return 0;
    }

    protected void pagerDealUpdate(int pageNumber)
    {
        if (DealPagerChanged != null)
        {
            var e = new DataEventArgs<int>(pageNumber);
            DealPagerChanged(this, e);
        }
    }

    #endregion PPonOrder GridView action class

    #endregion TabPanel PponSetup

    #region TabPanel Store

    public void GoToSellerStoreList()
    {
        Response.Redirect(string.Format("seller_add.aspx?sid={0}&tc=10", SellerGuid));
    }

    public void ShowMessage(string message)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('" + message + "');", true);
    }

    public void CopySellerLink(Guid copySellerGuid)
    {
        SetLocationDropDownList();
        Lit_copydeallink.Text += string.Format("<br/><a style='font-size:10pt' href='seller_add.aspx?sid={0}' target='_blank'>賣家已複製-{0}</a>", copySellerGuid.ToString());
    }

    private StoreCollection readExcel(HttpPostedFile xls)
    {
        StoreCollection storeList = new StoreCollection();
        if (null != xls && xls.ContentLength > 0)
        {
            HSSFWorkbook hssfworkbook = new HSSFWorkbook(xls.InputStream);
            Sheet currentSheet = hssfworkbook.GetSheetAt(0);
            for (int i = 1; i <= currentSheet.LastRowNum; i++)
            {
                var lkStore = new LunchKingSite.DataOrm.Store();
                Row row = currentSheet.GetRow(i);
                if (row.GetCell(0) != null)
                {
                    lkStore.StoreName = @"《" + row.GetCell(0).StringCellValue + @"》";
                    lkStore.SellerGuid = SellerGuid;
                    lkStore.Phone = CommonFacade.GetSheetCellValueString(row.GetCell(1));

                    string addressValue = row.GetCell(2) != null ? row.GetCell(2).StringCellValue : string.Empty;
                    // 解析地址城市ID、鄉鎮市區ID
                    LunchKingSite.DataOrm.City city = CityManager.CityGetByStringAddress(addressValue);
                    if (!string.IsNullOrEmpty(addressValue) && city != null)
                    {
                        LunchKingSite.DataOrm.City township = CityManager.TownshipIdGetByStringAddress(city.Id, addressValue);
                        if (township != null)
                        {
                            lkStore.TownshipId = township.Id;
                            lkStore.AddressString = addressValue.Replace(city.CityName, string.Empty).Replace(township.CityName, string.Empty);
                        }
                        else
                        {
                            continue;
                        }
                    }
                    else
                    {
                        continue;
                    }

                    lkStore.OpenTime = row.GetCell(3) != null ? row.GetCell(3).StringCellValue : string.Empty;
                    lkStore.CloseDate = row.GetCell(4) != null ? row.GetCell(4).StringCellValue : string.Empty;
                    lkStore.Mrt = row.GetCell(5) != null ? row.GetCell(5).StringCellValue : string.Empty;
                    lkStore.Car = row.GetCell(6) != null ? row.GetCell(6).StringCellValue : string.Empty;
                    lkStore.Bus = row.GetCell(7) != null ? row.GetCell(7).StringCellValue : string.Empty;
                    lkStore.OtherVehicles = row.GetCell(8) != null ? row.GetCell(8).StringCellValue : string.Empty;
                    lkStore.WebUrl = row.GetCell(9) != null ? row.GetCell(9).StringCellValue : string.Empty;
                    lkStore.FacebookUrl = row.GetCell(10) != null ? row.GetCell(10).StringCellValue : string.Empty;
                    lkStore.PlurkUrl = row.GetCell(11) != null ? row.GetCell(11).StringCellValue : string.Empty;
                    lkStore.BlogUrl = row.GetCell(12) != null ? row.GetCell(12).StringCellValue : string.Empty;
                    lkStore.OtherUrl = row.GetCell(13) != null ? row.GetCell(13).StringCellValue : string.Empty;
                    lkStore.Remarks = row.GetCell(14) != null ? row.GetCell(14).StringCellValue : string.Empty;

                    lkStore.CompanyName = row.GetCell(15) != null ? row.GetCell(15).StringCellValue : string.Empty;
                    lkStore.CompanyBossName = row.GetCell(16) != null ? row.GetCell(16).StringCellValue : string.Empty;
                    lkStore.CompanyID = CommonFacade.GetSheetCellValueString(row.GetCell(17));
                    lkStore.SignCompanyID = CommonFacade.GetSheetCellValueString(row.GetCell(18));
                    lkStore.CompanyBankCode = CommonFacade.GetSheetCellValueString(row.GetCell(19));
                    lkStore.CompanyBranchCode = CommonFacade.GetSheetCellValueString(row.GetCell(20));
                    lkStore.CompanyAccount = CommonFacade.GetSheetCellValueString(row.GetCell(21));
                    lkStore.CompanyAccountName = row.GetCell(22) != null ? row.GetCell(22).StringCellValue : string.Empty;
                    lkStore.CompanyEmail = row.GetCell(23) != null ? row.GetCell(23).StringCellValue : string.Empty;
                    lkStore.CompanyNotice = row.GetCell(24) != null ? row.GetCell(24).StringCellValue : string.Empty;

                    lkStore.Status = 0;
                    lkStore.IsCloseDown = false;
                    lkStore.CloseDownDate = null;
                    lkStore.CreateId = UserName;
                    lkStore.CreateTime = DateTime.Now;

                    storeList.Add(lkStore);
                }
            }
        }
        return storeList;
    }

    protected void lkStoreExampleDownload_Click(object sender, EventArgs e)
    {
        Response.Clear();
        Response.ContentType = "application/vnd.xls";
        Response.AddHeader("Content-Disposition", "attachment;filename=SalesStore.xls");
        Response.WriteFile(HttpContext.Current.Server.MapPath("~/template/PPon/Controlroom/BranchSample.xls"));
    }

    #endregion TabPanel Store

    #region TabPanel_PhotoUpdate

    public void SetImageGrid(string rawDataPath)
    {
        hi.Value = rawDataPath;
        ListItemCollection lic = new ListItemCollection();
        string[] paths = ImageFacade.GetMediaPathsFromRawData(rawDataPath, MediaType.SellerPhotoLarge);
        foreach (string s in paths)
        {
            lic.Add(new ListItem(s, s));
        }
        rli.DataSource = lic;
        rli.DataBind();
    }

    #region image management panel related functions

    protected void RL_Item_Reorder(object sender, ReorderListItemReorderEventArgs e)
    {
        string[] paths = Helper.GetRawPathsFromRawData(hi.Value);
        ArrayList ary = new ArrayList(paths.Length);
        ary.AddRange(paths);
        string item = (string)ary[e.OldIndex];
        ary.RemoveAt(e.OldIndex);
        int newIdx = (e.OldIndex < e.NewIndex) ? e.NewIndex - 1 : e.NewIndex;
        ary.Insert(newIdx, item);
        paths = (string[])ary.ToArray(typeof(string));
        this.SetImageGrid(Helper.GenerateRawDataFromRawPaths(paths));
    }

    protected void RL_Delete_Command(object sender, ReorderListCommandEventArgs e)
    {
        string[] paths = Helper.GetRawPathsFromRawData(hi.Value);
        ArrayList ary = new ArrayList(paths.Length);
        ary.AddRange(paths);
        ary.RemoveAt(e.Item.ItemIndex);
        paths = (string[])ary.ToArray(typeof(string));
        this.SetImageGrid(Helper.GenerateRawDataFromRawPaths(paths));
    }

    protected void bIS_Save(object sender, EventArgs e)
    {
        string rawData = hi.Value;
        if (!String.IsNullOrEmpty(photoUpload.PostedFile.FileName))
        {
            string tempFile = Path.GetTempFileName();
            HttpPostedFile pFile = photoUpload.PostedFile;
            string destFileName = "logo-" + DateTime.Now.ToString("yyMMddHHmmss");
            if (pFile.ContentType == "image/pjpeg" || pFile.ContentType == "image/jpeg" || pFile.ContentType == "image/x-png" || pFile.ContentType == "image/gif")
            {
                PhotoInfo pi = new PhotoInfo();
                pi.PFile = pFile;
                pi.Type = UploadFileType.SellerPhoto;
                pi.DestFilePath = SellerID;
                pi.DestFileName = destFileName;

                UpdatePhoto(this, new DataEventArgs<PhotoInfo>(pi));

                if (!string.IsNullOrEmpty(rawData))
                {
                    rawData = Helper.GenerateRawDataFromRawPaths(rawData, ImageFacade.GenerateMediaPath(lbSellerID.Text, destFileName + "." + Helper.GetExtensionByContentType(pFile.ContentType)));
                }
                else
                {
                    rawData = ImageFacade.GenerateMediaPath(lbSellerID.Text, destFileName + "." + Helper.GetExtensionByContentType(pFile.ContentType));
                }

                //hfPhotoPath.Value = rawData;
            }
        }

        if (this.ImageListChanged != null)
        {
            this.ImageListChanged(this, new DataEventArgs<string>(rawData));
        }
    }

    #endregion image management panel related functions

    #endregion TabPanel_PhotoUpdate

    #region TabPanel SellerDetail

    public void SetSellerImageDetail(object images)
    {
        ri.DataSource = images;
        ri.DataBind();
    }

    #endregion TabPanel SellerDetail

    protected void ClickImportIntroduct(object sender, EventArgs e)
    {
        if (ImportIntroduct != null)
        {
            this.ImportIntroduct(this, e);
        }
    }

    protected string GetContactTypeDesc(int contactType)
    {
        return Helper.GetDescription((ProposalContact)contactType);
    }

    [WebMethod]
    public static string OpenReservationService(string sellerId)
    {
        string apiKey = BookingSystemFacade.BookingSystemApiCheckeExistAndInsert(sellerId, BookingSystemApiServiceType.ThirdPartyCoupon);
        if (string.IsNullOrEmpty(apiKey))
        {
            return new JsonSerializer().Serialize(new { Success = false, Message = "產生連結失敗!" });
        }
        else
        {
            return new JsonSerializer().Serialize(new { Success = true, Message = System.Web.HttpUtility.UrlEncode(apiKey) });
        }
    }

    [WebMethod]
    public static bool SyncBusinessHour(string storeGuid, string opentime)
    {
        List<Guid> stGuid = new JsonSerializer().Deserialize<List<Guid>>(storeGuid);
        return SellerFacade.StoreOpenTimeUpdate(stGuid, opentime);
    }
    [WebMethod]
    public static bool SyncCloseDate(string storeGuid, string closedate)
    {
        List<Guid> stGuid = new JsonSerializer().Deserialize<List<Guid>>(storeGuid);
        return SellerFacade.StoreCloseDateUpdate(stGuid, closedate);
    }

    [WebMethod]
    public static string GetWeeklyName(string frenquency, string weeklys, string beginTime, string endTime, bool allDay)
    {
        return SellerFacade.GetWeeklyNames(frenquency, weeklys, beginTime, endTime, allDay);
    }

    [WebMethod]
    public static KeyValuePair<string, KeyValuePair<string, string>> CalculateCoordinates(int townshipId, string address)
    {
        return LocationFacade.GetArea2(CityManager.CityTownShopStringGet(townshipId) + address);
    }

    [WebMethod]
    public static IEnumerable<dynamic> CalculateNearMRTLocation(double latitude, double longitude)
    {
        return LocationFacade.StoreNearMRTLocation(latitude, longitude);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.DataOrm;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.UI;
namespace LunchKingSite.Web.ControlRoom
{
    public partial class seller_apply : RolePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                SellerCollection sellers_apply = VourcherFacade.SellerCollectionReturnCase(string.Empty, SellerTempStatus.Applied);
                ViewSellerStoreCollection stores_apply = VourcherFacade.ViewSellerStoreCollectionReturnCase(string.Empty, SellerTempStatus.Applied);
                List<LunchKingSite.Web.Vourcher.ReturnCase> applycases = new List<LunchKingSite.Web.Vourcher.ReturnCase>();
                foreach (var item in sellers_apply)
                {
                    applycases.Add(new LunchKingSite.Web.Vourcher.ReturnCase(item.Guid, item.SellerName, item.SignCompanyID, item.CompanyBossName,
                        item.CompanyName, item.TempStatus, item.Message, item.ReturnTime, item.ApplyTime, LunchKingSite.Web.Vourcher.ReturnCaseType.Seller));
                }
                foreach (var item in stores_apply)
                {
                    applycases.Add(new LunchKingSite.Web.Vourcher.ReturnCase(item.Guid, item.SellerGuid, item.SellerName + ":" + item.StoreName, item.SignCompanyID, item.CompanyBossName,
                        item.CompanyName, item.TempStatus, item.Message, item.ReturnTime, item.ApplyTime, LunchKingSite.Web.Vourcher.ReturnCaseType.Store));
                }
                rpt_ApplyCase.DataSource = applycases;
                rpt_ApplyCase.DataBind();

                SellerCollection sellers_return = VourcherFacade.SellerCollectionReturnCase(string.Empty, SellerTempStatus.Returned);
                ViewSellerStoreCollection stores_return = VourcherFacade.ViewSellerStoreCollectionReturnCase(string.Empty, SellerTempStatus.Returned);
                List<LunchKingSite.Web.Vourcher.ReturnCase> returncases = new List<LunchKingSite.Web.Vourcher.ReturnCase>();
                foreach (var item in sellers_return)
                {
                    returncases.Add(new LunchKingSite.Web.Vourcher.ReturnCase(item.Guid, item.SellerName, item.SignCompanyID, item.CompanyBossName,
                        item.CompanyName, item.TempStatus, item.Message, item.ReturnTime, item.ApplyTime, LunchKingSite.Web.Vourcher.ReturnCaseType.Seller));
                }
                foreach (var item in stores_return)
                {
                    returncases.Add(new LunchKingSite.Web.Vourcher.ReturnCase(item.Guid, item.SellerGuid, item.SellerName + ":" + item.StoreName, item.SignCompanyID, item.CompanyBossName,
                        item.CompanyName, item.TempStatus, item.Message, item.ReturnTime, item.ApplyTime, LunchKingSite.Web.Vourcher.ReturnCaseType.Store));
                }
                rpt_ReturnCase.DataSource = returncases;
                rpt_ReturnCase.DataBind();
            }
        }
    }
}
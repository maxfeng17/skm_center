﻿<%@ Page Language="C#" MasterPageFile="~/ControlRoom/backend.master" AutoEventWireup="true"
    CodeBehind="SellerStoreEdit.aspx.cs" Inherits="LunchKingSite.Web.ControlRoom.SellerStoreEdit" %>

<%@ Import Namespace="LunchKingSite.DataOrm" %>
<%@ Register Src="../Controls/AuditBoard.ascx" TagName="AuditBoard" TagPrefix="uc2" %>
<asp:Content ID="S" ContentPlaceHolderID="SSC" runat="server">
    <script src="<%=ResolveUrl("~/Tools/ckeditor/ckeditor.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Tools/js/json2.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Tools/js/jquery.validate.min.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Tools/js/jquery-ui.1.8.min.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Tools/js/stacktrace-min.js") %>" type="text/javascript"></script>
    <script src="<%=ResolveUrl("~/Tools/js/homecook.js") %>" type="text/javascript"></script>
    <script type="text/javascript" src="../../Tools/js/jquery.ui.datepicker-zh-TW.js"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Tools/ckeditor/ckeditor.js")%>"></script>

    <asp:Literal ID="ljs" runat="server"></asp:Literal>
    <link type="text/css" href="../../Tools/js/css/ui-lightness/jquery-ui-1.8rc3.custom.css"
        rel="stylesheet" />
    <link href="<%=ResolveUrl("~/Tools/js/fancybox/jquery.fancybox.css") %>" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="<%=ResolveUrl("~/Tools/js/fancybox/jquery.fancybox.js") %>"></script>
    <style type="text/css">
        body
        {
            font-size: 10px;
        }
        label
        {
            /*display: block;*/
        }
        fieldset
        {
            margin: 10px 0px;
            border: solid 1px gray;
        }
        fieldset legend
        {
            color: #4F8AFF;
            font-weight: bolder;
        }
        ul
        {
            list-style-type: none;
            margin: 0 0 0 0;
        }
        li
        {
            color: #4D4D4D;
            font-weight: bold;
        }
        ul.status li
        {
            float: left;
        }
        ul.target input
        {
            width: 80px;
        }
        
        input
        {
            border: solid 1px grey;
        }
        textarea
        {
            border: solid 1px grey;
        }
        
        input.date
        {
            width: 80px;
        }
        .mainhead
        {
            background-color: #4F81BD;
            font-size: small;
            color: White;
            font-weight: bold;
            margin-left: 5px;
        }
        .mainrow
        {
            background-color: #D0D8E8;
            border: 1px solid white;
            margin-left: 5px;
        }
        .mainrow2
        {
            background-color: #E9EDF4;
            border: 1px solid white;
            margin-left: 5px;
        }
        .tablehead
        {
            text-align: center;
            background-color: #696969;
            font-weight: bold;
            border: 3px solid #f5f5f5;
            color: White;
        }
        .tablecontent
        {
            width: 100%;
            background-color: #f5f5f5;
        }
        .tablecontent td
        {
            padding-left: 10px;
            font-size: 15px;
        }
        .remove a {
            padding-right:30px;
            background: url(/Themes/PCweb/images/x.png) no-repeat right center;
        }
    </style>
    <script type="text/javascript">
        var frequency = {
            weekly : 1,
            monthly :2,
            special : 3,
            alldayround : 99
        }
        $(document).ready(function () {
            //匯款資料focusout的處理
            //受款人ID/統一編號
            $('#<%=tbStoreID.ClientID%>').focusout(function() {
                var str=$('#<%=tbStoreID.ClientID%>').val();
                var newStr=str.replace(/\ /g,'');            
                $('#<%=tbStoreID.ClientID%>').val(newStr);  
            });

            //簽約公司ID/統一編號
            $('#<%=tbSignStoreID.ClientID%>').focusout(function() {
                var str=$('#<%=tbSignStoreID.ClientID%>').val();
                var newStr=str.replace(/\ /g,'');            
                $('#<%=tbSignStoreID.ClientID%>').val(newStr);  
            });

            //銀行代號
            $('#<%=tbStoreBankCode.ClientID%>').focusout(function() {
                var str=$('#<%=tbStoreBankCode.ClientID%>').val();
                var newStr=str.replace(/\ /g,'');            
                $('#<%=tbStoreBankCode.ClientID%>').val(newStr);  
            });

            //分行代號
            $('#<%=tbStoreBranchCode.ClientID%>').focusout(function() {
                var str=$('#<%=tbStoreBranchCode.ClientID%>').val();
                var newStr=str.replace(/\ /g,'');            
                $('#<%=tbStoreBranchCode.ClientID%>').val(newStr);  
            });

            //帳號
            $('#<%=tbStoreAccount.ClientID%>').focusout(function() {
                var str=$('#<%=tbStoreAccount.ClientID%>').val();
                var newStr=str.replace(/\-/g,'').replace(/\ /g,'');            
                $('#<%=tbStoreAccount.ClientID%>').val(newStr);  
            });

            /*
            * 公休日
            */
            HideOrShowCloseSpan();
            $("#ddlCloseDate").change(function(){
                HideOrShowCloseSpan();
            });

            //公休日(特殊日期)
            setupdatepicker('txtCloseBeginDate', 'txtCloseEndDate');

            $('#selCity').change(function () { updateCity($('#selCity').val()); });
            //CKEDITOR.replace('<%=tbBusinessHour.ClientID%>');
            $('#formCloseDownDate').datepicker({ dateFormat: 'yy/mm/dd' });
            $('#closeDownContainer input:[type="radio"]').change(closeDownDateChange);
            if($('#closeDownContainer input:[type="radio"]').eq(0).filter(':checked').length === 1) {   //營業狀態若是 "正常營業" => 結束營業日期不可輸入東西
                $('#formCloseDownDate').attr('disabled', 'disabled');
            }
            
            //預約系統設定


            $('#<%=rdlIsOpenBooking.ClientID%>').change(function() {
                var rdlBooking = $('#<%=rdlIsOpenBooking.ClientID%> input:radio:checked');
                var rdlRservationSetting = $('#<%=rdlIsOpenReservationSetting.ClientID%> input:radio:checked');
                console.log(rdlBooking.val());
                if (rdlBooking.val() == 0) {
                    $('#<%=rdlIsOpenReservationSetting.ClientID%>').css("display","none");
                } else {
                    $('#<%=rdlIsOpenReservationSetting.ClientID%>').css("display","");
                    $('#<%=rdlIsOpenReservationSetting.ClientID%>_1').attr("checked",true);
                }

            });

            if ($('#<%=rdlIsOpenBooking.ClientID%> input:radio:checked').val() == 0) {
                $('#<%=rdlIsOpenReservationSetting.ClientID%>').css("display","none");
            } else {
                $('#<%=rdlIsOpenReservationSetting.ClientID%>').css("display","");
            }

            $("#chk_close_allday").click(function(){
                $("#ddl_close_WeeklyBeginHour").attr('disabled', $(this).attr("checked"));
                $("#ddl_close_WeeklyBeginMinute").attr('disabled', $(this).attr("checked"));
                $("#ddl_close_WeeklyEndHour").attr('disabled', $(this).attr("checked"));
                $("#ddl_close_WeeklyEndMinute").attr('disabled', $(this).attr("checked"));
            });

            <%----%>
             
            var fragment = "";
            var items = jQuery.parseJSON($('#<%=hiddenMRTCategoryItem.ClientID%>').val());
            $.each(items, function (index, mrtLocation) {
                                            
                var mrtName = mrtLocation.Key;
                var mrtCode = mrtLocation.Value;
                fragment += "<input type='checkbox' name='mrtLocation' id='mrtLocation_"+ mrtCode +"' value='"+ mrtCode + "' checked='checked' /><label for='mrtLocation_"+ mrtCode +"'>"+ mrtName +"</label><br/>";

            });

            $("#mrtLocationContent").append(fragment);
            
              <%--MRTLocationCategorySetting start--%>
            $('#mrtLocationContent input[name="mrtLocation"]').change(function() {
                var selcode = $('input[id="' + this.id + '"]').val();
                var selname = $("label[for='" + this.id + "']").text();
                
                //重設hiddenMRTCategoryChecked
                var mrtCategoryArray = [];
                var checkitems = jQuery.parseJSON($('#<%=hiddenMRTCategoryChecked.ClientID%>').val());
                $.each(checkitems, function(index, tempcode) {
                    if (selcode != tempcode) {
                        mrtCategoryArray.push(tempcode);
                    }
                });
                if ($(this).is(':checked')) {
                    mrtCategoryArray.push(parseInt(selcode));
                }
                $('#<%=hiddenMRTCategoryChecked.ClientID%>').val(JSON.stringify(mrtCategoryArray));

                //重設hiddenMRTCategoryItem
                var mrtItemArray = [];
                var itemList = jQuery.parseJSON($('#<%=hiddenMRTCategoryItem.ClientID%>').val());
                $.each(itemList, function(index, items) {
                    if (selcode != items.Value) {
                        var tempitem = new Object();
                        tempitem.Key = items.Key;
                        tempitem.Value = items.Value;
                        mrtItemArray.push(tempitem);
                    }
                });
                if ($(this).is(':checked')) {
                    var checkitem = new Object();
                    checkitem.Key = selname;
                    checkitem.Value = parseInt(selcode);
                    mrtItemArray.push(checkitem);
                }
                $('#<%=hiddenMRTCategoryItem.ClientID%>').val(JSON.stringify(mrtItemArray));
            });
            <%--MRTLocationCategorySetting end--%>

            /*
            * 營業時間全選
            */
            $("#chk_AllBusinessWeekly").click(function(){
                if($(this).attr("checked")){
                    $("input[name*=chk_BusinessWeekly]").attr("checked",true);
                }else{
                    $("input[name*=chk_BusinessWeekly]").attr("checked",false);
                }
            });

            $("input[name*=chk_BusinessWeekly]").click(function(){
                var flag = true;
                $.each($("input[name*=chk_BusinessWeekly]"),function(){
                    if(!$(this).attr("checked")){
                        flag =false;
                    }
                });
                if(!flag){
                    $("#chk_AllBusinessWeekly").attr("checked",false);
                }else{
                    $("#chk_AllBusinessWeekly").attr("checked",true);
                }
            });

            /*
            * 公休時間全選
            */
            $("#chk_close_AllBusinessWeekly").click(function(){
                if($(this).attr("checked")){
                    $("input[name*=chk_close_BusinessWeekly]").attr("checked",true);
                }else{
                    $("input[name*=chk_close_BusinessWeekly]").attr("checked",false);
                }
            });

            $("input[name*=chk_close_BusinessWeekly]").click(function(){
                var flag = true;
                $.each($("input[name*=chk_close_BusinessWeekly]"),function(){
                    if(!$(this).attr("checked")){
                        flag =false;
                    }
                });
                if(!flag){
                    $("#chk_close_AllBusinessWeekly").attr("checked",false);
                }else{
                    $("#chk_close_AllBusinessWeekly").attr("checked",true);
                }
            });

            $("#syncBusinessHour").fancybox();
            $("#syncCloseDate").fancybox();

            DrawMap();

            BindOpenCloseDate();
        });

        function BindOpenCloseDate() {
            //營業時間
            var _settingTime = "<%=BusinessHour%>";
            var _settingTimes = _settingTime.split("；");
            $.each(_settingTimes, function (idx, value) {
                if (value != "") {
                    appendSettingTime(value, "panBusinessHour");
                }
            });

            //公休日
            var _settingCloseTime = "<%=CloseDateInformation%>";
            var _settingCloseTimes = _settingCloseTime.split("；");
            $.each(_settingCloseTimes, function (idx, value) {
                if (value != "") {
                    appendSettingTime(value, "panCloseDate");
                }
            });
        }

        /*
        * 同步營業時間到其他店鋪
        */
        function syncBusinessHour() {
            var storeGuid = [];
            $.each($("#divBusinessHour input:checked").not("#chkAllStore"), function () {
                var obj = $(this).val();
                storeGuid.push(obj);
            });

            var _settingTimes = "";
            $.each($("#panBusinessHour div"), function (idx, value) {
                _settingTimes += value.innerText + "；";
            });
            if (_settingTimes.length > 0) {
                _settingTimes = _settingTimes.substring(0, _settingTimes.length - 1);
            }
            if (storeGuid.length > 0) {
                $.ajax({
                    type: "POST",
                    url: "SellerStoreEdit.aspx/SyncBusinessHour",
                    data: "{'storeGuid': '" + JSON.stringify(storeGuid) + "','opentime' : '" + _settingTimes + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        alert("資料已同步");
                        window.parent.$.fancybox.close();
                    }
                });
            }
        }

        /*
        * 同步公休時間到其他店鋪
        */
        function syncCloseDate() {
            var storeGuid = [];
            $.each($("#divCloseDate input:checked").not("#chkAllStore"), function () {
                var obj = $(this).val();
                storeGuid.push(obj);
            });

            var _settingTimes = "";
            $.each($("#panCloseDate div"), function (idx, value) {
                _settingTimes += value.innerText + "；";
            });
            if (_settingTimes.length > 0) {
                _settingTimes = _settingTimes.substring(0, _settingTimes.length - 1);
            }
            if (storeGuid.length > 0) {
                $.ajax({
                    type: "POST",
                    url: "SellerStoreEdit.aspx/SyncCloseDate",
                    data: "{'storeGuid': '" + JSON.stringify(storeGuid) + "','closedate' : '" + _settingTimes + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        alert("資料已同步");
                        window.parent.$.fancybox.close();
                    }
                });
            }
        }


        var citys = <%=CityJSonData %>;

        function updateCity(cid) {
            $.each(citys, function(i,item) {
                if(item.id == cid) {
                    var el = $('#selTownship');
                    el.children().remove();
                    el.addItems(eval(item.Townships), function (t) { return t.name; }, function (t) { return t.id; });
                    el.get(0).selectedIndex = 0;
                    //updateArea($('#selTownship').val());
                    return false;
                }
            });
        }

        var closeDownDateChange = function(s, e) {
            var isCloseDown = false;
            if ($(this).attr('id') === 'formIsClosed') {
                $('#formCloseDownDate').attr('disabled', '');
                isCloseDown = true;
            }
            if ($(this).attr('id') === 'formNotClosed') {
                isCloseDown = false;
                $('#formCloseDownDate').attr('disabled', 'disabled');
            }
            if (!isCloseDown)
            {
                $('#formCloseDownDate').val('');
            }
        };

        var validateCloseDownInfo = function() {
            var result = true;

            if($('#formIsClosed').attr('checked')) {
                result = $('#formCloseDownDate').val().length > 0;    //close down must have date
                if(!result) {
                    alert('請設定結束營業日期!');
                    return false;
                }
                if (!window.confirm("您已調整營業狀態，請問是否儲存？"))
                {
                    return false;
                }
            }
            if ($('[id*=txtLatitude]').val().length === 0 || $('[id*=txtLongitude]').val().length === 0) {
                alert('經緯度不可空白!');
                return false;
            }


            $.ajax({
                type: "POST",
                url: "SellerStoreEdit.aspx/ChecekAccount",
                data: "{'tbStoreBankCode':'" + $("#<%=tbStoreBankCode.ClientID%>").val() + "','tbStoreBranchCode':'" + $("#<%=tbStoreBranchCode.ClientID%>").val() + "','tbStoreAccountName':'" + $("#<%=tbStoreAccountName.ClientID%>").val() + "','tbStoreEmail':'" + $("#<%=tbStoreEmail.ClientID%>").val() + "','tbStoreNotice': '" + $("#<%=tbStoreNotice.ClientID%>").val() + "','tbStoreAccount':'" + $("#<%=tbStoreAccount.ClientID%>").val() + "', 'tbStoreID':'" + $("#<%=tbStoreID.ClientID%>").val() + "','tbSignStoreID':'" + $("#<%=tbSignStoreID.ClientID%>").val() + "' }",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function (response) {
                    var c = response.d;
                    if(c != undefined){
                        if(c != ""){
                            alert(c);
                            result= false;
                        }
                    }
                },error: function(error){
                    console.log(error);
                }
            });
            
            //營業時間
            var _settingTimes = "";
            $.each($("#panBusinessHour div"),function(idx, value){
                _settingTimes += value.firstChild.data + " ；";
            });
            if (_settingTimes.length > 0) {
                _settingTimes = _settingTimes.substring(0, _settingTimes.length - 1);
            }
            $("#tbBusinessHour").val(_settingTimes);
            /*
            CKEDITOR.instances.tbBusinessHour.setData(_settingTimes, function()
            {
                this.checkDirty();
            });
            */
            //公休日
            var _settingCloseTimes = "";
            $.each($("#panCloseDate div"),function(idx, value){
                _settingCloseTimes += value.firstChild.data + "；";
            });
            if (_settingCloseTimes.length > 0) {
                _settingCloseTimes = _settingCloseTimes.substring(0, _settingCloseTimes.length - 1);
            }
            $("#<%=tbCloseDate.ClientID%>").val(_settingCloseTimes);
            return result;
            
        };


        function Calculate() {

            $.ajax({
                type: "POST",
                url: "SellerStoreEdit.aspx/CalculateCoordinates",
                data: "{'townshipId':" + $('#selTownship').val() + ",'address':'" + $('[id*=tbStoreAddress]').val() + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    if (response.d != '' && response.d.Key != null) {
                        $('[id*=txtLatitude]').val(response.d.Key);
                        $('[id*=txtLongitude]').val(response.d.Value);
                        DrawMap();
                        CalculateNearMRTLocation();
                    }
                    else {
                        alert('查無準確座標資料，請手動輸入!!');
                    }
                }
            });
        }

        function DrawMap() {

            var latitude = $('[id*=txtLatitude]');
            var longitude = $('[id*=txtLongitude]');

            if ('<%= Config.EnableOpenStreetMap %>'.toLowerCase() == 'true') {
                DrawOpenStreetMap(latitude.val(), longitude.val());
            } else {
                if ('<%= Config.EnableGoogleMapApiV2 %>'.toLowerCase() == 'true') {
                    DrawGoogleMapV2(latitude.val(), longitude.val());
                } else {
                    DrawGoogleMapV3(latitude, longitude);
                }
            }
        }

        function CalculateNearMRTLocation() {
            var latitude = $('[id*=txtLatitude]');
            var longitude = $('[id*=txtLongitude]');            
            $.ajax({
                type: "POST",
                url: "SellerStoreEdit.aspx/CalculateNearMRTLocation",
                data: "{'latitude':" + latitude.val() + ",'longitude':" + longitude.val() + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    if (response.d != '') {
                       // var mrtCategoryArray = $("#<%=hiddenMRTCategoryChecked.ClientID%>").val();
                        var mrtCategoryArray = [];
                        $("#mrtLocationContent").empty();
                        var fragment = "";
                        var items = response.d;
                        $.each(items, function (index, mrtLocation) {
                                            
                            var mrtName = mrtLocation.name;
                            var mrtCode = mrtLocation.code;
                            //var mrtType = mrtLocation.type;
                            //fragment +=   " "+mrtName+" :: "+mrtCode+" - "+mrtType;
                            mrtCategoryArray.push(mrtCode);
                            fragment += "<input type='checkbox' name='mrtLocation' id='mrtLocation_"+ mrtCode +"' value='"+ mrtCode + "' checked='checked' /><label for='mrtLocation_"+ mrtCode +"'>"+ mrtName +"</label><br/>";

                        });
                        $('#<%=hiddenMRTCategoryChecked.ClientID%>').val(JSON.stringify(mrtCategoryArray));
                        $("#mrtLocationContent").append(fragment);
                    }
                    else {
                        //alert('沒有座標資料!!');
                    }
                }
            });
        }

        function DrawOpenStreetMap(latitude, longitude) {

            $('#map').empty(); // 因OpenStreetMap會一直append新的地圖，必須清空
            map = new OpenLayers.Map("map");
            map.addLayer(new OpenLayers.Layer.OSM());

            var lonLat = new OpenLayers.LonLat(longitude, latitude)
                  .transform(
                    new OpenLayers.Projection("EPSG:4326"), // transform from WGS 1984
                    map.getProjectionObject() // to Spherical Mercator Projection
                  );
            var zoom = 16;

            var markers = new OpenLayers.Layer.Markers("Markers");
            map.addLayer(markers);
            markers.addMarker(new OpenLayers.Marker(lonLat));
            map.setCenter(lonLat, zoom);
            $('#map').css('display', '');
        }

        function DrawGoogleMapV2(latitude, longitude) {

            if (GBrowserIsCompatible()) {
                if (document.getElementById("map") != null) {
                    $('#map').css('display', '');
                    var map = new GMap2(document.getElementById("map"));
                    var geocoder = new GClientGeocoder();
                    map.addControl(new GSmallMapControl());

                    geocoder.getLocations(new GLatLng(latitude, longitude), function (point) {
                        if (point) {
                            map.setCenter(new GLatLng(latitude, longitude), 16);
                            var marker = new GMarker(new GLatLng(latitude, longitude));
                            map.addControl(new GSmallMapControl()); // 小型地圖控制項
                            map.addOverlay(marker);
                            AddMoveCoordinateListener(map, marker);
                        }
                    });
                }
            }
        }

        function AddMoveCoordinateListener(map, marker) {
            GEvent.addListener(map, "click", function (overlay, point) {
                if (point) {
                    if (confirm('確定要移動座標位置嗎?')) {
                        //設定標註座標
                        marker.setLatLng(point);
                        $('[id*=txtLongitude]').val(point.x.toString());
                        $('[id*=txtLatitude]').val(point.y.toString());
                    }
                }
            });
        }

        function DrawGoogleMapV3(latitude, longitude) {

            var myLatlng = new google.maps.LatLng(latitude.val(), longitude.val());
            var mapOptions = {
                zoom: 16,
                center: myLatlng,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            }

            var map = new google.maps.Map(document.getElementById('map'),
              mapOptions);

            var marker = new google.maps.Marker({
                position: myLatlng,
                draggable: true,
                map: map
            });

            google.maps.event.addListener(
                marker,
                'drag',
                function() {
                    latitude.val(marker.position.lat());
                    longitude.val(marker.position.lng());
                }
            );

            $('#map').css('display', '');
        }
        
        function addBusinessHour(){
            var chk_BusinessWeekly = $("input[name*=chk_BusinessWeekly]:checked");
            var ddl_WeeklyBeginHour = $("#ddl_WeeklyBeginHour").val();
            var ddl_WeeklyBeginMinute = $("#ddl_WeeklyBeginMinute").val();
            var ddl_WeeklyEndHour = $("#ddl_WeeklyEndHour").val();
            var ddl_WeeklyEndMinute = $("#ddl_WeeklyEndMinute").val();
            var _html = ddl_WeeklyBeginHour + "~" + ddl_WeeklyEndHour;

            var weeklys = chk_BusinessWeekly.map(function() {
                return this.value;
            }).get().join();

            getWeeklyContinue("", weeklys,ddl_WeeklyBeginHour + ":" + ddl_WeeklyBeginMinute, ddl_WeeklyEndHour + ":" + ddl_WeeklyEndMinute, false, "panBusinessHour");                       

            $("input[name*=chk_BusinessWeekly]").removeAttr('checked');
            $("#ddl_WeeklyBeginHour").val("01");
            $("#ddl_WeeklyBeginMinute").val("00");
            $("#ddl_WeeklyEndHour").val("01");
            $("#ddl_WeeklyEndMinute").val("00");

            $("#chk_AllBusinessWeekly").attr("checked", false);
        }

        function addCloseHour(){
            var ddlCloseDate = $("#ddlCloseDate");  //頻率
            var ddlCloseDateText = $('#ddlCloseDate :selected').text();
            var chk_close_BusinessWeekly = $("input[name*=chk_close_BusinessWeekly]:checked");  //每周
            var txtCloseMonthlyDay = $("#txtCloseMonthlyDay");  //每月
            var txtCloseBeginDate = $("#txtCloseBeginDate");  //特殊
            var txtCloseEndDate = $("#txtCloseEndDate");  //特殊
            var chk_close_BusinessWeekly = $("input[name*=chk_close_BusinessWeekly]:checked");
            var ddl_close_WeeklyBeginHour = $("#ddl_close_WeeklyBeginHour").val();
            var ddl_close_WeeklyBeginMinute = $("#ddl_close_WeeklyBeginMinute").val();
            var ddl_close_WeeklyEndHour = $("#ddl_close_WeeklyEndHour").val();
            var ddl_close_WeeklyEndMinute = $("#ddl_close_WeeklyEndMinute").val();
            var chk_close_allday = $("#chk_close_allday").is(':checked');

            var _frequency = GetCloseFrequency();
            switch(_frequency){
                case frequency.weekly:
                    //週
                    var weeklys = chk_close_BusinessWeekly.map(function() {
                        return this.value;
                    }).get().join();

                    getWeeklyContinue(ddlCloseDateText, weeklys,ddl_close_WeeklyBeginHour + ":" + ddl_close_WeeklyBeginMinute, ddl_close_WeeklyEndHour + ":" + ddl_close_WeeklyEndMinute, chk_close_allday, "panCloseDate");
                    break;
                case frequency.monthly:
                    //月
                    var beginTime = ddl_close_WeeklyBeginHour + ":" + ddl_close_WeeklyBeginMinute;
                    var endTime = ddl_close_WeeklyEndHour + ":" + ddl_close_WeeklyEndMinute;
                    $("#panCloseDate").append('<div class="remove">' + ddlCloseDateText + " "  + txtCloseMonthlyDay.val() + '日' + (chk_close_allday ? "全天" : beginTime + "~" + endTime )  + '<a onclick="removeSettingTime(this);"></a></div>');
                    break;
                case frequency.special:
                    //特殊
                    var beginDate = txtCloseBeginDate.val();
                    var endDate = txtCloseEndDate.val();
                    var beginTime = ddl_close_WeeklyBeginHour + ":" + ddl_close_WeeklyBeginMinute;
                    var endTime = ddl_close_WeeklyEndHour + ":" + ddl_close_WeeklyEndMinute;
                    if ($.trim(beginDate) == ""){
                        alert("請輸入至少一個日期");
                        return false;
                    }
                    if($.trim(beginDate) != ""){
                        if(!ValidateDate($.trim(beginDate))){
                            alert("起始日期格式不正確");
                            return false;
                        }
                    }
                    if($.trim(endDate) != ""){
                        if(!ValidateDate($.trim(endDate))){
                            alert("結束日期格式不正確");
                            return false;
                        }
                    }
                    if(!CompareDate(beginDate,endDate)){
                        alert("結束日期需晚於起始日期");
                        return false;
                    }
                    var paddingDatetime = "";
                    if($.trim(endDate) != ""){
                        paddingDatetime = beginDate + "~" + endDate;
                    }else{
                        paddingDatetime = beginDate;
                    }
                    $("#panCloseDate").append('<div class="remove">' + paddingDatetime + " " +  (chk_close_allday ? "全天" : beginTime + "~" + endTime )  + '<a onclick="removeSettingTime(this);"></a></div>');
                    break;
                case frequency.alldayround:
                    //全年無休
                    $("#panCloseDate").append('<div class="remove"><%=LunchKingSite.Core.Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, (LunchKingSite.Core.StoreCloseDate)LunchKingSite.Core.StoreCloseDate.AllYearRound)%><a onclick="removeSettingTime(this);"></a></div>');
                    break;
            }

            $("input[name*=chk_close_BusinessWeekly]:checked").removeAttr('checked');
            $("#ddlCloseDate").val("1");
            $("#txtCloseMonthlyDay").val("");
            $("#txtCloseBeginDate").val("");
            $("#txtCloseEndDate").val("");
            $("input[name*=chk_close_BusinessWeekly]:checked").removeAttr('checked');
            $("#ddl_close_WeeklyBeginHour").val("01");
            $("#ddl_close_WeeklyBeginMinute").val("00");
            $("#ddl_close_WeeklyEndHour").val("01");
            $("#ddl_close_WeeklyEndMinute").val("00");
            $("#chk_close_allday").removeAttr('checked');
            $("#ddl_close_WeeklyBeginHour").attr('disabled',false);
            $("#ddl_close_WeeklyBeginMinute").attr('disabled', false);
            $("#ddl_close_WeeklyEndHour").attr('disabled', false);
            $("#ddl_close_WeeklyEndMinute").attr('disabled', false);

            $("#chk_close_AllBusinessWeekly").attr("checked", false);	

            HideOrShowCloseSpan();
        }

        function ValidateDate(dtValue) {
            var dtRegex = new RegExp(/\b(\d{4})([/])(0[1-9]|1[012])([/])(0[1-9]|[12][0-9]|3[01])\b/);
            return dtRegex.test(dtValue);
        }

        function CompareDate(sd,ed){
            var d1=new Date(sd);
            var d2=new Date(ed);
            if (d2 <= d1){
                return false;
            }
            return true;
        }

        /*
        *營業時間
        */
        function getWeeklyContinue(ofrenquency ,weeklys, beginTime, endTime, allDay, obj){
            if(weeklys == ""){
                alert("請先選擇週幾");
                return false;
            }
            $.ajax({
                type: "POST",
                url: "SellerStoreEdit.aspx/GetWeeklyName",
                data: "{'frenquency':'" + ofrenquency + "','weeklys':'" + weeklys + "','beginTime':'" + beginTime + "','endTime':'" + endTime + "','allDay': '" + allDay + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    var c = response.d;
                    if(c != undefined){
                        appendSettingTime(c,obj);
                    }
                },error: function(error){
                    console.log(error);
                }
            });
        }

        function appendSettingTime(c,obj){
            $("#" + obj + "").append('<div class="remove">' + c + '<a onclick="removeSettingTime(this);"></a></div>');
        }
        /*
        * 移除營業時間div
        */
        function removeSettingTime(obj){
            obj.closest("div").remove();
        }

        /*
        * 公休日欄位顯示或隱藏
        */
        function HideOrShowCloseSpan(){
            var _weekly = false;
            var _monthly = false;
            var _special = false;
            var _time = false;

            var _frequency = GetCloseFrequency();
            switch(_frequency){
                case frequency.weekly:
                    _weekly = true;
                    _monthly = false;
                    _special = false;
                    _time = true;
                    break;
                case frequency.monthly:
                    _weekly = false;
                    _monthly = true;
                    _special = false;
                    _time = true;
                    break;
                case frequency.special:
                    _weekly = false;
                    _monthly = false;
                    _special = true;
                    _time = true;
                    break;
                case frequency.alldayround:
                    _weekly = false;
                    _monthly = false;
                    _special = false;
                    _time = false;
                    break;
            }

            if(_weekly){
                $("#panCloseWeekly").show();
            }else{
                $("#panCloseWeekly").hide();
            }
            if(_monthly){
                $("#panCloseMonthly").show();
            }else{
                $("#panCloseMonthly").hide();
            }
            if(_special){
                $("#panCloseSpecial").show();
            }else{
                $("#panCloseSpecial").hide();
            }
            if(_time){
                $("#panCloseTime").show();
            }else{
                $("#panCloseTime").hide();
            }
        }
        function setupdatepicker(from, to) {
            var dates = $('#' + from + ', #' + to).datepicker({
                changeMonth: true,
                numberOfMonths: 3,
                onSelect: function (selectedDate) {
                    var option = this.id == from ? "minDate" : "maxDate";
                    var instance = $(this).data("datepicker");
                    var date = $.datepicker.parseDate(instance.settings.dateFormat || $.datepicker._defaults.dateFormat, selectedDate, instance.settings);
                    dates.not(this).datepicker("option", option, date);
                },
                onClose: function(selectedDate) {
                    
                }
            });
        }

        function GetCloseFrequency(){
            var _frequency = "";
            switch($("#ddlCloseDate").val()){
                case "<%=(int)LunchKingSite.Core.StoreCloseDate.EveryWeekly%>":
                case "<%=(int)LunchKingSite.Core.StoreCloseDate.SingleWeekly%>":
                case "<%=(int)LunchKingSite.Core.StoreCloseDate.BiWeekly%>":
                case "<%=(int)LunchKingSite.Core.StoreCloseDate.FirstWeekly%>":
                case "<%=(int)LunchKingSite.Core.StoreCloseDate.SecondWeekly%>":
                case "<%=(int)LunchKingSite.Core.StoreCloseDate.ThirdWeekly%>":
                case "<%=(int)LunchKingSite.Core.StoreCloseDate.ForthWeekly%>":
                case "<%=(int)LunchKingSite.Core.StoreCloseDate.FifthWeekly%>":
                    _frequency = frequency.weekly;
                    break;
                case "<%=(int)LunchKingSite.Core.StoreCloseDate.EveryMonthly%>":
                case "<%=(int)LunchKingSite.Core.StoreCloseDate.SingleMonthly%>":
                case "<%=(int)LunchKingSite.Core.StoreCloseDate.BiMonthly%>":
                    _frequency = frequency.monthly;
                    break;
                case "<%=(int)LunchKingSite.Core.StoreCloseDate.SpecialDate%>":
                    _frequency = frequency.special;
                    break;
                case "<%=(int)LunchKingSite.Core.StoreCloseDate.AllYearRound%>":
                    _frequency = frequency.alldayround;
                    break;
            }
            return _frequency;
        }
        function checkAll(obj, div) {
            if ($(obj).is(":checked")) {
                $("#" + div + " > input[type=checkbox]").attr("checked", "checked");
            } else {
                $("#" + div + " > input[type=checkbox]").removeAttr("checked");
            }
        }
    </script>
</asp:Content>
<asp:Content ID="M" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table>
        <tr>
            <td>
                <fieldset>
                    <legend>店鋪資料</legend>
                    <asp:HyperLink ID="hlStoreAdd" runat="server" EnableViewState="false" Text="賣家名稱"></asp:HyperLink>
                    <table>
                        <tr>
                            <td>
                                店 名
                            </td>
                            <td>
                                <asp:TextBox ID="tbBranchName" runat="server" Width="300"></asp:TextBox><span style="color: #FF8C69;
                                    font-size: small;">
                                    <asp:Literal ID="lit_Store_StoreName" runat="server"></asp:Literal></span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                訂位電話
                            </td>
                            <td>
                                <asp:TextBox ID="tbStoreTel" runat="server" Width="300"></asp:TextBox><span style="color: #FF8C69;
                                    font-size: small;">
                                    <asp:Literal ID="lit_Store_Phone" runat="server"></asp:Literal></span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                地 址
                            </td>
                            <td>
                                <asp:Repeater ID="repCity" runat="server">
                                    <HeaderTemplate>
                                        <select name="selCity" id="selCity">
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <option value="<%#Eval("Id")%>" <%# ((City)Container.DataItem).Id == AddressCityId ? "selected='selected'" : "" %>>
                                            <%#Eval("CityName")%></option>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </select>
                                    </FooterTemplate>
                                </asp:Repeater>
                                <asp:Repeater ID="repTown" runat="server">
                                    <HeaderTemplate>
                                        <select name="selTownship" id="selTownship">
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <option value="<%#Eval("Id")%>" <%#((City)Container.DataItem).Id == AddressTownshipId ? "selected='selected'" : "" %>>
                                            <%#Eval("CityName")%></option>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </select>
                                    </FooterTemplate>
                                </asp:Repeater>
                                <asp:TextBox ID="tbStoreAddress" runat="server" Width="270"></asp:TextBox><span style="color: #FF8C69;
                                    font-size: small;">
                                    <asp:Literal ID="lit_Seller_CityId" runat="server"></asp:Literal></span> <span style="color: #FF8C69;
                                        font-size: small;">
                                        <asp:Literal ID="lit_Seller_Address" runat="server"></asp:Literal></span>
                                &nbsp;<input type="button" value="計算經緯度" onclick="Calculate();" />&nbsp; <br />
                                緯度: <asp:TextBox ID="txtLatitude" runat="server" Width="115px"></asp:TextBox>&nbsp; 
                                經度: <asp:TextBox ID="txtLongitude" runat="server" Width="115px"></asp:TextBox>
                                <input type="button" value="畫出地圖" onclick="DrawMap();" /><br />
                                <font color="red">緯度介於正負90, 經度介於正負180; 不想填請填0</font>
                                <div id="map" style="width: 450px; height: 250px;">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-top:30px"></td>
                        </tr>
                        <tr>
                            <td>
                                營業時間
                            </td>
                            <td>
                                <asp:CheckBox ID="chk_AllBusinessWeekly" runat="server" ClientIDMode="Static" />全選
                                <br />
                                <input type="checkbox" id="chk_BusinessWeekly0" name="chk_BusinessWeekly" value="0" />週一
                                <input type="checkbox" id="chk_BusinessWeekly1" name="chk_BusinessWeekly" value="1" />週二
                                <input type="checkbox" id="chk_BusinessWeekly2" name="chk_BusinessWeekly" value="2" />週三
                                <input type="checkbox" id="chk_BusinessWeekly3" name="chk_BusinessWeekly" value="3" />週四
                                <input type="checkbox" id="chk_BusinessWeekly4" name="chk_BusinessWeekly" value="4" />週五
                                <input type="checkbox" id="chk_BusinessWeekly5" name="chk_BusinessWeekly" value="5" />週六
                                <input type="checkbox" id="chk_BusinessWeekly6" name="chk_BusinessWeekly" value="6" />週日
                                <%--<asp:CheckBoxList ID="chk_BusinessWeekly" runat="server" RepeatLayout="Flow" RepeatDirection="Horizontal">
                                    <asp:ListItem Text="週一" Value="0"></asp:ListItem>
                                    <asp:ListItem Text="週二" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="週三" Value="2"></asp:ListItem>
                                    <asp:ListItem Text="週四" Value="3"></asp:ListItem>
                                    <asp:ListItem Text="週五" Value="4"></asp:ListItem>
                                    <asp:ListItem Text="週六" Value="5"></asp:ListItem>
                                    <asp:ListItem Text="週日" Value="6"></asp:ListItem>
                                </asp:CheckBoxList>--%>
                                <br />
                                時間:<asp:DropDownList ID="ddl_WeeklyBeginHour" runat="server" ClientIDMode="Static"></asp:DropDownList>:
                                <asp:DropDownList ID="ddl_WeeklyBeginMinute" runat="server" ClientIDMode="Static"></asp:DropDownList>～
                                <asp:DropDownList ID="ddl_WeeklyEndHour" runat="server" ClientIDMode="Static"></asp:DropDownList>:
                                <asp:DropDownList ID="ddl_WeeklyEndMinute" runat="server" ClientIDMode="Static"></asp:DropDownList>
                                <div>
                                    <input type="button" value="新增" onclick="addBusinessHour();" />
                                </div>
                                營業時間預覽
                                <hr />
                                <asp:Panel ID="panBusinessHour" runat="server" ClientIDMode="Static" style="color:red">
                                   
                                </asp:Panel>
                                <span style="display:none">
                                <asp:TextBox ID="tbBusinessHour" TextMode="MultiLine" runat="server" Width="300" ClientIDMode="Static"></asp:TextBox>
                                </span>
                                <span
                                    style="color: Gray;">
                                    <asp:Literal ID="lit_Store_OpenTime" runat="server"></asp:Literal></span>
                                <div id="divBusinessHour" style="display:none;width:500px;height:300px">
                                    <asp:PlaceHolder ID="phBusinessStores" runat="server">
                                        <asp:Repeater ID="rptBStore" runat="server" OnItemDataBound="rptBStore_ItemDataBound">
                                            <HeaderTemplate>
                                                <asp:CheckBox ID="chkAllStore" runat="server" ClientIDMode="Static" onclick="checkAll(this,'divBusinessStore')" />全選<br />
                                                <hr class="header_hr">
								            </HeaderTemplate>
								            <ItemTemplate>
                                                <div id="divBusinessStore">
                                                <asp:CheckBox ID="chkStore" runat="server" /> <%# ((Store)Container.DataItem).StoreName %>
									            <br />
                                                </div>
								            </ItemTemplate>
								            <FooterTemplate>
                                                <input type="button" id="btnSyncBusinessHour" value="確認同步" class="btn btn-small btn-primary form-inline-btn" onclick="syncBusinessHour()" />
								            </FooterTemplate>
                                        </asp:Repeater>
                                    </asp:PlaceHolder>
                                </div>
                                <span>
                                    <asp:HyperLink ID="syncBusinessHour" runat="server" ClientIDMode="Static" class="btn btn-small btn-primary form-inline-btn openclose">將營業時間同步到其他店鋪</asp:HyperLink></span>
                                <hr />
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-top:30px"></td>
                        </tr>
                        <tr>
                            <td>
                                公休日
                            </td>
                            <td>
                                頻率：<asp:DropDownList ID="ddlCloseDate" runat="server" ClientIDMode="Static"></asp:DropDownList><br />
                                <span id="panCloseWeekly">
                                    <asp:CheckBox ID="chk_close_AllBusinessWeekly" runat="server" ClientIDMode="Static" />全選
                                    <br />
                                    <input type="checkbox" id="chk_close_BusinessWeekly0" name="chk_close_BusinessWeekly" value="0" />週一
                                    <input type="checkbox" id="chk_close_BusinessWeekly1" name="chk_close_BusinessWeekly" value="1" />週二
                                    <input type="checkbox" id="chk_close_BusinessWeekly2" name="chk_close_BusinessWeekly" value="2" />週三
                                    <input type="checkbox" id="chk_close_BusinessWeekly3" name="chk_close_BusinessWeekly" value="3" />週四
                                    <input type="checkbox" id="chk_close_BusinessWeekly4" name="chk_close_BusinessWeekly" value="4" />週五
                                    <input type="checkbox" id="chk_close_BusinessWeekly5" name="chk_close_BusinessWeekly" value="5" />週六
                                    <input type="checkbox" id="chk_close_BusinessWeekly6" name="chk_close_BusinessWeekly" value="6" />週日
                                    <%--<asp:CheckBoxList ID="chk_close_BusinessWeekly" runat="server" RepeatLayout="Flow" RepeatDirection="Horizontal">
                                        <asp:ListItem Text="週一" Value="0"></asp:ListItem>
                                        <asp:ListItem Text="週二" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="週三" Value="2"></asp:ListItem>
                                        <asp:ListItem Text="週四" Value="3"></asp:ListItem>
                                        <asp:ListItem Text="週五" Value="4"></asp:ListItem>
                                        <asp:ListItem Text="週六" Value="5"></asp:ListItem>
                                        <asp:ListItem Text="週日" Value="6"></asp:ListItem>
                                    </asp:CheckBoxList>--%>
                                    <br />
                                </span>
                                <span id="panCloseMonthly">
                                    日子：<asp:TextBox ID="txtCloseMonthlyDay" runat="server" placeholder="填入特定日(只寫數字，以、號隔開，如 「1、15」)" Width="300px" ClientIDMode="Static"></asp:TextBox>日<br />
                                </span>
                                <span id="panCloseSpecial">
                                    日期：<asp:TextBox ID="txtCloseBeginDate" runat="server" Width="100px" ClientIDMode="Static" ></asp:TextBox>～<asp:TextBox ID="txtCloseEndDate" runat="server"  Width="100px" ClientIDMode="Static" ></asp:TextBox><br />
                                </span>
                                <span id="panCloseTime">
                                時間:<asp:DropDownList ID="ddl_close_WeeklyBeginHour" runat="server" ClientIDMode="Static"></asp:DropDownList>:
                                    <asp:DropDownList ID="ddl_close_WeeklyBeginMinute" runat="server" ClientIDMode="Static"></asp:DropDownList>～
                                    <asp:DropDownList ID="ddl_close_WeeklyEndHour" runat="server" ClientIDMode="Static"></asp:DropDownList>:
                                    <asp:DropDownList ID="ddl_close_WeeklyEndMinute" runat="server" ClientIDMode="Static"></asp:DropDownList>
                                    <asp:CheckBox ID="chk_close_allday" runat="server" ClientIDMode="Static" />全天<br />
                                </span>
                                <input type="button" value="新增" onclick="addCloseHour();" /><br />
                                <hr />
                                <asp:Panel ID="panCloseDate" runat="server" ClientIDMode="Static" style="color:red">
                                   
                                </asp:Panel>
                                <div style="display:none">
                                    <asp:TextBox ID="tbCloseDate" runat="server" Width="300"></asp:TextBox>
                                </div>
                                <span style="color: #FF8C69;
                                    font-size: small;">
                                    <asp:Literal ID="lit_Store_CloseDate" runat="server"></asp:Literal></span>
                                <div id="divCloseDate" style="display:none;width:500px;height:300px">
                                    <asp:PlaceHolder ID="phCloseStores" runat="server">
                                        <asp:Repeater ID="rptCStore" runat="server" OnItemDataBound="rptCStore_ItemDataBound">
                                            <HeaderTemplate>
                                                <asp:CheckBox ID="chkAllStore" runat="server" ClientIDMode="Static" onclick="checkAll(this,'divCloseStore')"  />全選<br />
                                                <hr class="header_hr">
								            </HeaderTemplate>
								            <ItemTemplate>
                                                <div id="divCloseStore">
                                                <asp:CheckBox ID="chkStore" runat="server" /> <%# ((Store)Container.DataItem).StoreName %>
									            <br />
                                                </div>
								            </ItemTemplate>
								            <FooterTemplate>
                                                <input type="button" id="btnSyncCloseDate" value="確認同步" class="btn btn-small btn-primary form-inline-btn" onclick="syncCloseDate()" />
								            </FooterTemplate>
                                        </asp:Repeater>
                                    </asp:PlaceHolder>
                                </div>
                                <span >
                                    <asp:HyperLink ID="syncCloseDate" runat="server" ClientIDMode="Static" class="btn btn-small btn-primary form-inline-btn openclose">將公休日同步到其他店鋪</asp:HyperLink></span>
                                <hr />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                刷卡服務
                            </td>
                            <td>
                                <asp:CheckBox ID="cbCreditCardAvailable" runat="server" /><span style="color: #FF8C69;
                                    font-size: small;">
                                    <asp:Literal ID="lit_Store_CreditCardAvailable" runat="server"></asp:Literal></span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                交通資訊
                            </td>
                            <td>
                                捷 運(舊)：
                                <asp:TextBox ID="tbMrt" runat="server" Width="300"></asp:TextBox><span style="color: #FF8C69;
                                    font-size: small;">
                                    <asp:Literal ID="lit_Store_MRT" runat="server"></asp:Literal></span><br />
                                捷 運(新)：
                                <br/>
                                    <asp:HiddenField ID="hiddenMRTCategoryItem" runat="server" Value="[]" />
                                    <asp:HiddenField ID="hiddenMRTCategoryChecked" runat="server" Value="[]" />
                                    <div id="mrtLocationContent">
                                     </div>
                                開 車：
                                <asp:TextBox ID="tbCar" runat="server" Width="300"></asp:TextBox><span style="color: #FF8C69;
                                    font-size: small;">
                                    <asp:Literal ID="lit_Store_Car" runat="server"></asp:Literal></span><br />
                                公 車：
                                <asp:TextBox ID="tbBus" runat="server" Width="300"></asp:TextBox><span style="color: #FF8C69;
                                    font-size: small;">
                                    <asp:Literal ID="lit_Store_Bus" runat="server"></asp:Literal></span><br />
                                其 他：
                                <asp:TextBox ID="tbOV" runat="server" Width="300"></asp:TextBox><span style="color: #FF8C69;
                                    font-size: small;">
                                    <asp:Literal ID="lit_Store_OtherVehicle" runat="server"></asp:Literal></span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                官 網
                            </td>
                            <td>
                                <asp:TextBox ID="tbWebUrl" runat="server" Width="300"></asp:TextBox><span style="color: #FF8C69;
                                    font-size: small;">
                                    <asp:Literal ID="lit_Store_WebUrl" runat="server"></asp:Literal></span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                FaceBook
                            </td>
                            <td>
                                <asp:TextBox ID="tbFBUrl" runat="server" Width="300"></asp:TextBox><span style="color: #FF8C69;
                                    font-size: small;">
                                    <asp:Literal ID="lit_Store_FaceBookUrl" runat="server"></asp:Literal></span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Plurk
                            </td>
                            <td>
                                <asp:TextBox ID="tbPlurkUrl" runat="server" Width="300"></asp:TextBox><span style="color: #FF8C69;
                                    font-size: small;">
                                    <asp:Literal ID="lit_Store_PlurkUrl" runat="server"></asp:Literal></span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                部落格
                            </td>
                            <td>
                                <asp:TextBox ID="tbBlogUrl" runat="server" Width="300"></asp:TextBox><span style="color: #FF8C69;
                                    font-size: small;">
                                    <asp:Literal ID="lit_Store_BlogUrl" runat="server"></asp:Literal></span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                其他網址
                            </td>
                            <td>
                                <asp:TextBox ID="tbOtherUrl" runat="server" Width="300"></asp:TextBox><span style="color: #FF8C69;
                                    font-size: small;">
                                    <asp:Literal ID="lit_Store_OtherUrl" runat="server"></asp:Literal></span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                備 註
                            </td>
                            <td>
                                <asp:TextBox ID="tbRemark" runat="server" Width="300"></asp:TextBox><span style="color: #FF8C69;
                                    font-size: small;">
                                    <asp:Literal ID="lit_Store_Remark" runat="server"></asp:Literal></span>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <b>分店匯款資料</b>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                簽約公司名稱
                            </td>
                            <td>
                                <asp:TextBox ID="tbStoreName" runat="server" Width="300"></asp:TextBox><span style="color: #FF8C69;
                                    font-size: small;">
                                    <asp:Literal ID="lit_Seller_ComapnyName" runat="server"></asp:Literal></span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                負責人名稱
                            </td>
                            <td>
                                <asp:TextBox ID="tbStoreBossName" runat="server" Width="300"></asp:TextBox><span
                                    style="color: Gray;">
                                    <asp:Literal ID="lit_Seller_CompanyBossName" runat="server"></asp:Literal></span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                簽約公司ID/統一編號
                            </td>
                            <td>
                                <asp:TextBox ID="tbSignStoreID" onblur="value=value.replace(/\ /g,'')" onkeydown="value=value.replace(/\ /g,'')"
                                    onkeyup="value=value.replace(/\ /g,'')" runat="server" Width="300" MaxLength="10"></asp:TextBox><span
                                        style="color: Gray;">
                                        <asp:Literal ID="lit_Seller_CompanySignId" runat="server"></asp:Literal></span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                銀行代號
                            </td>
                            <td>
                                <asp:TextBox ID="tbStoreBankCode" onblur="value=value.replace(/\ /g,'')" onkeydown="value=value.replace(/\ /g,'')"
                                    onkeyup="value=value.replace(/\ /g,'')" runat="server" Width="300" MaxLength="3"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                分行代號
                            </td>
                            <td>
                                <asp:TextBox ID="tbStoreBranchCode" onblur="value=value.replace(/\ /g,'')" onkeydown="value=value.replace(/\ /g,'')"
                                    onkeyup="value=value.replace(/\ /g,'')" runat="server" Width="300" MaxLength="4"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                帳 號
                            </td>
                            <td>
                                <p>
                                    <asp:TextBox ID="tbStoreAccount" onblur="value=value.replace(/\-/g,'').replace(/\ /g,'')"
                                        onkeydown="value=value.replace(/\-/g,'').replace(/\ /g,'')" onkeyup="value=value.replace(/\-/g,'').replace(/\ /g,'')"
                                        runat="server" Width="300"></asp:TextBox></p>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                戶 名
                            </td>
                            <td>
                                <asp:TextBox ID="tbStoreAccountName" runat="server" Width="300"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span style="color: Blue">受款人ID/統一編號</span>
                            </td>
                            <td>
                                <asp:TextBox ID="tbStoreID" onblur="value=value.replace(/\ /g,'')" onkeydown="value=value.replace(/\ /g,'')"
                                    onkeyup="value=value.replace(/\ /g,'')" runat="server" Width="300" MaxLength="10"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                帳務聯絡人
                            </td>
                            <td>
                                <asp:TextBox ID="tbAccountantName" runat="server" Width="300" MaxLength="50"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                帳務聯絡電話
                            </td>
                            <td>
                                <asp:TextBox ID="tbAccountantTel" runat="server" Width="300" MaxLength="20"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                帳務連絡人電子信箱
                            </td>
                            <td>
                                <asp:TextBox ID="tbStoreEmail" runat="server" Width="300"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                店家備註
                            </td>
                            <td>
                                <asp:TextBox ID="tbStoreNotice" runat="server" Width="300"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                預約管理 
                            </td>
                            <td>

                                <asp:RadioButtonList ID="rdlIsOpenBooking" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" >
                                    <asp:ListItem Text="關閉" Value="0" Selected="True"></asp:ListItem>
                                    <asp:ListItem Text="開啟" Value="1"></asp:ListItem>
                                </asp:RadioButtonList>
                                
                                
                                <asp:RadioButtonList ID="rdlIsOpenReservationSetting" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" >
                                    <asp:ListItem Text="開放店家自行維護" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="不開放店家自行維護" Value="0" Selected="True"></asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                顯示狀態
                            </td>
                            <td>
                                <asp:RadioButtonList ID="rdlStatus" runat="server" RepeatDirection="Horizontal">
                                    <asp:ListItem Text="顯示" Value="0"></asp:ListItem>
                                    <asp:ListItem Text="隱藏" Value="1"></asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                營業狀態
                            </td>
                            <td>
                                <div id="closeDownContainer">
                                    <input type="radio" name="closeDownStatus" value="false" runat="server" id="formNotClosed"
                                        clientidmode="Static" />
                                    <label for="formNotClosed">
                                        正常營業</label>
                                    <input type="radio" name="closeDownStatus" value="true" runat="server" id="formIsClosed"
                                        clientidmode="Static" />
                                    <label for="formIsClosed">
                                        結束營業</label>
                                    結業營業日
                                    <asp:TextBox runat="server" ID="formCloseDownDate" ClientIDMode="Static" />
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                店家分店代碼
                            </td>
                            <td>
                                <asp:TextBox ID="tbStoreRelationCode" runat="server" Width="300"></asp:TextBox>
                                <span style="color: #FF8C69; font-size: small;">店家分店代碼，有串接POS才需填</span>
                            </td>
                        </tr>
                    </table>
                </fieldset>
                <div>
                    <span style="color: Blue;">退件訊息備註:</span><asp:TextBox ID="tbMessage" runat="server"></asp:TextBox><span
                        style="color: Gray;">
                        <asp:Literal ID="lit_Store_Message" runat="server"></asp:Literal></span>
                    <asp:Button ID="btnReturnApply" runat="server" Text="退回申請" OnClick="btnReturnApply_Click"
                        Enabled="false" />
                    <br />
                    <asp:Label ID="lab_NewCreated" runat="server" Text="此分店為前台新增,'Save'即為審核通過" Visible="false"
                        ForeColor="Red"></asp:Label>
                </div>
                <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" OnClientClick="if( !validateCloseDownInfo() ) { return false; } " />
                <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_Click" />
                <asp:Label runat="server" ID="lbText"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Label ID="lab_Message" runat="server" Text="" ForeColor="Red"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <span style="font-weight: bold">已存檔的店家資料</span><br />
            </td>
        </tr>
    </table>
    <asp:Panel ID="pAudit" runat="server">
        <table style="width: 800px">
            <tr>
                <td class="head2" colspan="2">
                    修改紀錄
                </td>
            </tr>
            <tr>
                <td colspan="2" class="row">
                    <uc2:AuditBoard ID="ab" runat="server" Width="100%" EnableAdd="false" ShowAllRecords="true"
                        ReferenceType="Store" />
                </td>
            </tr>
            <tr>
                <td colspan="2" class="row">
                    <fieldset>
                        <legend>前台異動紀錄</legend>
                        <asp:Repeater ID="rpt_ChangeLog" runat="server">
                            <HeaderTemplate>
                                <table class="tablecontent">
                                    <tr>
                                        <td class="tablehead" style="width: 25%">
                                            日期
                                        </td>
                                        <td class="tablehead" style="width: 60%">
                                            異動資料
                                        </td>
                                        <td class="tablehead" style="width: 15%">
                                            建立者
                                        </td>
                                    </tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td>
                                        <%# ((ChangeLog)(Container.DataItem)).ModifyTime.ToString("MM/dd hh:mm")%>
                                    </td>
                                    <td>
                                        <%#  Regex.Replace(((ChangeLog)(Container.DataItem)).Content,"[{}\"]",string.Empty).Replace(",","<br/>")%>
                                    </td>
                                    <td>
                                        <%# ((ChangeLog)(Container.DataItem)).ModifyUser.Replace("@17life.com",string.Empty)%>
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                        </asp:Repeater>
                    </fieldset>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>

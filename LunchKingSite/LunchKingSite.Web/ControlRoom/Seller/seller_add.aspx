﻿<%@ Page Language="C#" MasterPageFile="~/ControlRoom/backend.master" ValidateRequest="false"
    AutoEventWireup="true" CodeBehind="seller_add.aspx.cs" Inherits="seller_add" %>

<%@ Import Namespace="LunchKingSite.DataOrm" %>
<%@ Register Src="../Controls/Pager.ascx" TagName="Pager" TagPrefix="uc1" %>
<%@ Register Src="../Controls/SellerHiDealList.ascx" TagName="SellerHiDealList" TagPrefix="uc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content0" runat="server" ContentPlaceHolderID="SSC">
</asp:Content>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
    <link href="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.1/themes/ui-lightness/jquery-ui.min.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../Tools/js/jquery.scrollTo.js"></script>
    <script type="text/javascript" src="../../Tools/js/jquery.nav.js"></script>
    <script type="text/javascript" src="../../Tools/js/jquery.ui.datepicker-zh-TW.js"></script>
    <script type="text/javascript" src="../../Tools/ckeditor/ckeditor.js"></script>
    <script type="text/javascript" src="../../Tools/js/json2.js"></script>
    <script type="text/javascript" src="../../Tools/js/jquery.blockUI.js"></script>
    <link rel="stylesheet" type="text/css" href="../../Tools/js/css/ui-lightness/jquery-ui-1.8rc3.custom.css" />
    <script type="text/javascript" src="//www.google.com/jsapi"></script>
    <asp:Literal ID="ljs" runat="server"></asp:Literal>
    <script type="text/javascript" src="<%=ResolveClientUrl("~/Tools/js/jquery-ui.1.8.min.js") %>"></script>
    <script src="<%=ResolveUrl("~/Tools/js/homecook.js") %>" type="text/javascript"></script>
    <link href="<%=ResolveUrl("~/Tools/js/fancybox/jquery.fancybox.css") %>" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="<%=ResolveUrl("~/Tools/js/fancybox/jquery.fancybox.js") %>"></script>
    <style type="text/css">        
        body
        {
            font-size: 10px;
        }
        input
        {
            border: solid 1px grey;
        }
        .rlDH
        {
            width: 10px;
            height: 101px;
            background-color: #5377A9;
            cursor: move;
            border: outset thin black;
        }

        .cbStyle
        {
            border: thin blue inset;
        }

        .cbStyle table
        {
            background-color: #5377A9;
            color: Black;
        }

        .rlM li
        {
            list-style: none;
            margin: 0px;
        }

        .rlM li a
        {
            font-weight: bold;
        }

        .rCue
        {
            border: dashed thin black;
            width: 100%;
            height: 101px;
        }

        .rIA
        {
            margin: 0px;
            font-family: Arial, Verdana, sans-serif;
            font-size: 1em;
            text-align: left;
        }

        .rIA td
        {
            vertical-align: middle;
            margin: 0px;
        }

        .tablehead
        {
            text-align: center;
            background-color: #696969;
            font-weight: bold;
            border: 3px solid #f5f5f5;
            color: White;
        }

        .tablecontent
        {
            width: 100%;
            background-color: #f5f5f5;
            border:1px solid grey;
            border-collapse:collapse;
        }

        .tablecontent td
        {
            padding-left: 10px;
            font-size: 13px;
            border:1px solid grey;
            word-break:break-all;
        }

        .btnSave
        {
            padding: 5px;
            font-size: 15px;
            width: 80px;
            letter-spacing: 5px;
        }
        .watermark
        {
            color:#696969;
        }

        /*側邊anchor*/
        .nav-anchor {
            position: fixed;
            top: 125px;
            margin-left: 970px;
            z-index: 99;
        }
        .nav-anchor li {
            margin-bottom: 2px;
            text-align: center;
            list-style-type: none;
        }

        .nav-anchor a {
            width: 100px;
            height: 30px;
            line-height: 30px;
            background: #ededed;
            color: #666;
            display: block;
            font-size: 11px;
            text-decoration: none;
            text-transform: uppercase;
        }

        .nav-anchor a:hover {
            background: #dedede;
        }

        .nav-anchor .current a {
            background: #3F3F3F;
            color: #e5e5e5;
        }

        .nav-anchor li:first-child a {
            -webkit-border-radius: 3px 3px 0 0;
            -moz-border-radius: 3px 3px 0 0;
            border-radius: 3px 3px 0 0;
        }

        .nav-anchor li:last-child a {
            -webkit-border-radius: 0 0 3px 3px;
            -moz-border-radius: 0 0 3px 3px;
            border-radius: 0 0 3px 3px;
        }

        .checkBoxList
        {
            font-size: 10pt;
        }

        .googlemap
        {
            width: 650px;
            height: 160px;
            background-color: #EBDDC7;
            border: 2px solid #000000;
        }
        .googlemap-Top
        {
            width: 650px;
        }
        .googlemap-X-button
        {
            background-image: url(../../Themes/default/images/17Life/G2/A2-Multi-grade-Setting_xx.png);
            height: 28px;
            width: 27px;
            float: right;
            margin-top: -9px;
            margin-right: -9px;
        }
        .remove a {
            padding-right:30px;
            background: url(../../Themes/PCweb/images/x.png) no-repeat right center;
        }
    </style>
    <script type="text/javascript">
        var frequency = {
            weekly : 1,
            monthly :2,
            special : 3,
            alldayround : 99
        }

        $(document).ready(function () {
            showActivity();
            $('#formCloseDownDate').datepicker({ dateFormat: 'yy/mm/dd' });
            $('#formIsCloseDown').change(closeDownChange);

            if ($('#formIsCloseDown').val() === 'false') {   //營業狀態若是 "正常營業" => 結束營業日期不可輸入東西
                if ($('#formCloseDownDate') !== '輸入結束營業日期') {
                    $('#formCloseDownDate').val('輸入結束營業日期');
                }
                $('#formCloseDownDate').attr('disabled', 'disabled');
            }

            var tbIntroductId = '<%=tbIntroduct.ClientID%>';
            if ($('#' + tbIntroductId).size() > 0) {
                CKEDITOR.replace(tbIntroductId);
            }
            $('#images').sortable();
            $('#images').disableSelection();
            $('.dI').button();
            $('.dI').click(function () {
                var cont = confirm('Are you sure you want to delete it?');
                if (cont) {
                    $(this).parent().remove();
                }
            });

            $('.checkEmailClass').on('blur', function () {
                var chkResult = false;
                var emailText = $(this).val();
                $.each([',', ';', '，', '；'], function (idx, ch) {
                    if (emailText.indexOf(ch) > 0) {
                        $.each(emailText.split(ch), function (i, mail) {
                            checkEmailReg(mail);
                            chkResult = true;
                            return;
                        });
                    }
                });
                if (!chkResult) { checkEmailReg(emailText); }
            });

            appendSlide();
            $('#subMenu').onePageNav();
            
            $("#syncBusinessHour").fancybox();
            $("#syncCloseDate").fancybox();

            DrawMap();

            BindOpenCloseDate();

            //匯款資料focusout的處理
            //受款人ID/統一編號
            $('#<%=tbCompanyID.ClientID%>').focusout(function () {
                var str = $('#<%=tbCompanyID.ClientID%>').val();
                var newStr = str.replace(/\ /g, '');
                $('#<%=tbCompanyID.ClientID%>').val(newStr);
            });

            //簽約公司ID/統一編號
            $('#<%=tbSignCompanyID.ClientID%>').focusout(function () {
                var str = $('#<%=tbSignCompanyID.ClientID%>').val();
                var newStr = str.replace(/\ /g, '');
                $('#<%=tbSignCompanyID.ClientID%>').val(newStr);
            });

            //銀行代號
            $('#<%=tbCompanyBankCode.ClientID%>').focusout(function () {
                var str = $('#<%=tbCompanyBankCode.ClientID%>').val();
                var newStr = str.replace(/\ /g, '');
                $('#<%=tbCompanyBankCode.ClientID%>').val(newStr);
            });

            //分行代號
            $('#<%=tbCompanyBranchCode.ClientID%>').focusout(function () {
                var str = $('#<%=tbCompanyBranchCode.ClientID%>').val();
                var newStr = str.replace(/\ /g, '');
                $('#<%=tbCompanyBranchCode.ClientID%>').val(newStr);
            });

            //帳號
            var str = $('#<%=tbCompanyAccount.ClientID%>').val();
            var newStr = str.replace(/\-/g, '').replace(/\ /g, '');
            $('#<%=tbCompanyAccount.ClientID%>').val(newStr);
            
            /*
            * 公休日
            */
            HideOrShowCloseSpan();
            $("#ddlCloseDate").change(function(){
                HideOrShowCloseSpan();
            });

            //公休日(特殊日期)
            setupdatepicker('txtCloseBeginDate', 'txtCloseEndDate');

            /*
              * 營業時間全選
              */
            $("#chk_AllBusinessWeekly").click(function(){
                if($(this).attr("checked")){
                    $("input[name*=chk_BusinessWeekly]").attr("checked",true);
                }else{
                    $("input[name*=chk_BusinessWeekly]").attr("checked",false);
                }
            });

            $("input[name*=chk_BusinessWeekly]").click(function(){
                var flag = true;
                $.each($("input[name*=chk_BusinessWeekly]"),function(){
                    if(!$(this).attr("checked")){
                        flag =false;
                    }
                });
                if(!flag){
                    $("#chk_AllBusinessWeekly").attr("checked",false);
                }else{
                    $("#chk_AllBusinessWeekly").attr("checked",true);
                }
            });

            /*
            * 公休時間全選
            */
            $("#chk_close_AllBusinessWeekly").click(function(){
                if($(this).attr("checked")){
                    $("input[name*=chk_close_BusinessWeekly]").attr("checked",true);
                }else{
                    $("input[name*=chk_close_BusinessWeekly]").attr("checked",false);
                }
            });

            $("input[name*=chk_close_BusinessWeekly]").click(function(){
                var flag = true;
                $.each($("input[name*=chk_close_BusinessWeekly]"),function(){
                    if(!$(this).attr("checked")){
                        flag =false;
                    }
                });
                if(!flag){
                    $("#chk_close_AllBusinessWeekly").attr("checked",false);
                }else{
                    $("#chk_close_AllBusinessWeekly").attr("checked",true);
                }
            });

            $('#selCity').change(function () { updateCity($('#selCity').val()); });

            var fragment = "";
            var items = jQuery.parseJSON($('#<%=hiddenMRTCategoryItem.ClientID%>').val());
            $.each(items, function (index, mrtLocation) {
                                            
                var mrtName = mrtLocation.Key;
                var mrtCode = mrtLocation.Value;
                //var mrtType = mrtLocation.type;
                //fragment +=   " "+mrtName+" :: "+mrtCode+" - "+mrtType;
                fragment += "<input type='checkbox' name='mrtLocation' id='mrtLocation_"+ mrtCode +"' value='"+ mrtCode + "' checked='checked' disabled='disabled'/><label for=''>"+ mrtName +"</label><br/>";

            });

            $("#mrtLocationContent").append(fragment);


            $('#txtParentSellerName').bind( "input.autocomplete", function(){
                $(this).trigger('keydown.autocomplete');
            })
            $('#txtParentSellerName').autocomplete({
                source: function (request, response) {
                    $.ajax(
                    {
                        type: "POST",
                        url: "../../Sal/SellerContent.aspx/GetSellerNameArray",
                        data: "{ 'sellerName': '" + request.term + "'}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        dataFilter: function (data) { return data; },
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.Label,
                                    value: item.Value
                                }
                            }))
                        }, error: function (res) {
                            console.log(res);
                        }
                    });
                },
                focus: function (event, ui) {
                    $("#txtParentSellerName").val(ui.item.label.substring(ui.item.label.indexOf(' ') + 1));
                    $('#lbParentSellerId').html(ui.item.label.substring(0, ui.item.label.indexOf(' ')));
                    return false;
                },
                select: function (event, ui) {
                    $("#txtParentSellerName").val(ui.item.label.substring(ui.item.label.indexOf(' ') + 1));
                    $('#lbParentSellerId').html(ui.item.label.substring(0, ui.item.label.indexOf(' ')));
                    $("#hidParentSellerGuid").val(ui.item.value);
                    return false;
                }
            });
            $('#txtParentSellerName').blur(function () {
                if ($.trim($(this).val()) == "") {
                    $('#hidParentSellerGuid').val("");
                }
            });

            $.ajax(
            {
                async: false,
                type: "POST",
                url: "../../Sal/SellerContent.aspx/GetMultipleSales",
                data: JSON.stringify({
                    sellerGuid: '<%=SellerGuid%>',
                }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {

                    var result = data.d;
                    var _html = '';

                    $.each(result, function (idx, obj) {
                        
                        _html+="<input id='txtSalesGroup'" + obj.SalesGroup + " type='text' readonly='readonly' value='"  + obj.DevelopeSalesEmpName + "' />&nbsp;";
                        _html+="<input id='txtSalesGroup'" + obj.SalesGroup + " type='text' readonly='readonly' value='"  + obj.OperationSalesEmpName + "' />&nbsp;";
                        
                        if ((idx + 1) % 2 ==0)
                            _html+="<br>";

                    });


                    $('#multipleSales').html(_html);
                        
                }, error: function (res) {
                    console.log(res);
                }
            });


            if ('<%=Config.IsRemittanceFortnightly%>' == "True" && '<%=IsAgreePponNewContractSeller%>' == 'True')
            {
                ShowRemittanceType($('[id*=chkSellerPorperty][value=1]'));
                $('[id*=chkSellerPorperty][value=1]').click(function () {
                    ShowRemittanceType($(this))
                })
            }

            if ('<%=Config.IsRemittanceFortnightly%>' == "True" && '<%=IsAgreeHouseNewContractSeller%>' == 'True' && '<%=IsAgreePponNewContractSeller%>' == 'False')
            {
                $('#ddlRemittanceType').hide();
            }
            
        });

        <%--        function ValidatePhoto() {
            if (document.getElementById("<%=photoUpload.ClientID %>").value != 0) {
                document.getElementById("<%=hfPath.ClientID %>").value = document.getElementById("<%=photoUpload.ClientID %>").value;
            }
        }--%>
        function closeDownChange(s, e) {
            var isCloseDown = $(this).val();
            var tbCloseDown = $('#<%=formCloseDownDate.ClientID%>');
            if (isCloseDown === 'false') {
                if (tbCloseDown.val() !== '輸入結束營業日期') {
                    tbCloseDown.val('輸入結束營業日期');
                }
                tbCloseDown.attr('disabled', 'disabled');
            }
            if (isCloseDown === 'true') {
                tbCloseDown.removeAttr('disabled');
            }
        }

        function appendSlide() {
            if ($("#sellerInfo").length > 0) {
                $("#subMenu").append('<li class="current"><a href="#sellerInfo">基本資料</a></li>');
            }
            if ($("#contactInfo").length > 0) {
                $("#subMenu").append('<li><a href="#contactInfo">通訊錄</a></li>');
            }
            if ($("#financeInfo").length > 0) {
                $("#subMenu").append('<li><a href="#financeInfo">財務資料</a></li>');
            }
            if ($("#storeInfo").length > 0) {
                $("#subMenu").append('<li><a href="#storeInfo">營業資料</a></li>');
            }
            if ($("#serviceStatus").length > 0) {
                $("#subMenu").append('<li><a href="#serviceStatus">提供服務</a></li>');
            }
            if ($("#changeLog").length > 0) {
                $("#subMenu").append('<li><a href="#changeLog">變更記錄</a></li>');
            }
        }

        function BindOpenCloseDate() {
            //營業時間
            var _settingTime = "<%=BusinessHour%>";
            var _settingTimes = _settingTime.split("；");
            $.each(_settingTimes, function (idx, value) {
                if (value != "") {
                    appendSettingTime(value, "panBusinessHour");
                }
            });

            //公休日
            var _settingCloseTime = "<%=CloseDateInformation%>";
            var _settingCloseTimes = _settingCloseTime.split("；");
            $.each(_settingCloseTimes, function (idx, value) {
                if (value != "") {
                    appendSettingTime(value, "panCloseDate");
                }
            });
        }

        /*
        * 同步營業時間到其他店鋪
        */
        function syncBusinessHour() {
            var storeGuid = [];
            $.each($("#divBusinessHour input:checked").not("#chkAllStore"), function () {
                var obj = $(this).val();
                storeGuid.push(obj);
            });

            var _settingTimes = "";
            $.each($("#panBusinessHour div"), function (idx, value) {
                _settingTimes += value.innerText || value.textContent + "；";
            });
            if (_settingTimes.length > 0) {
                _settingTimes = _settingTimes.substring(0, _settingTimes.length - 1);
            }
            if (storeGuid.length > 0) {
                $.ajax({
                    type: "POST",
                    url: "seller_add.aspx/SyncBusinessHour",
                    data: "{'storeGuid': '" + JSON.stringify(storeGuid) + "','opentime' : '" + _settingTimes + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        alert("資料已同步");
                        window.parent.$.fancybox.close();
                    }
                });
            }
        }

        /*
        * 同步公休時間到其他店鋪
        */
        function syncCloseDate() {
            var storeGuid = [];
            $.each($("#divCloseDate input:checked").not("#chkAllStore"), function () {
                var obj = $(this).val();
                storeGuid.push(obj);
            });

            var _settingTimes = "";
            $.each($("#panCloseDate div"), function (idx, value) {
                _settingTimes += value.innerText || value.textContent + "；";
            });
            if (_settingTimes.length > 0) {
                _settingTimes = _settingTimes.substring(0, _settingTimes.length - 1);
            }
            if (storeGuid.length > 0) {
                $.ajax({
                    type: "POST",
                    url: "seller_add.aspx/SyncCloseDate",
                    data: "{'storeGuid': '" + JSON.stringify(storeGuid) + "','closedate' : '" + _settingTimes + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        alert("資料已同步");
                        window.parent.$.fancybox.close();
                    }
                });
            }
        }
        

        function updateCity(cid) {
            var citys = <%=CityJSonData %>;
            $.each(citys, function(i,item) {
                if(item.id == cid) {
                    var el = $('#selTownship');
                    el.children().remove();
                    el.addItems(eval(item.Townships), function (t) { return t.name; }, function (t) { return t.id; });
                    el.get(0).selectedIndex = 0;
                    //updateArea($('#selTownship').val());
                    return false;
                }
            });
        }

        var closeDownDateChange = function(s, e) {
            var isCloseDown = false;
            if ($(this).attr('id') === 'formIsClosed') {
                $('#formCloseDownDate').attr('disabled', '');
                isCloseDown = true;
            }
            if ($(this).attr('id') === 'formNotClosed') {
                isCloseDown = false;
                $('#formCloseDownDate').attr('disabled', 'disabled');
            }
            if (!isCloseDown)
            {
                $('#formCloseDownDate').val('');
            }
        };
        
        function addBusinessHour(){
            var chk_BusinessWeekly = $("input[name*=chk_BusinessWeekly]:checked");
            var ddl_WeeklyBeginHour = $("#ddl_WeeklyBeginHour").val();
            var ddl_WeeklyBeginMinute = $("#ddl_WeeklyBeginMinute").val();
            var ddl_WeeklyEndHour = $("#ddl_WeeklyEndHour").val();
            var ddl_WeeklyEndMinute = $("#ddl_WeeklyEndMinute").val();
            var _html = ddl_WeeklyBeginHour + "~" + ddl_WeeklyEndHour;

            var weeklys = chk_BusinessWeekly.map(function() {
                return this.value;
            }).get().join();

            getWeeklyContinue("", weeklys,ddl_WeeklyBeginHour + ":" + ddl_WeeklyBeginMinute, ddl_WeeklyEndHour + ":" + ddl_WeeklyEndMinute, false, "panBusinessHour");                       

            $("input[name*=chk_BusinessWeekly]").removeAttr('checked');
            $("#ddl_WeeklyBeginHour").val("01");
            $("#ddl_WeeklyBeginMinute").val("00");
            $("#ddl_WeeklyEndHour").val("01");
            $("#ddl_WeeklyEndMinute").val("00");

            $("#chk_AllBusinessWeekly").attr("checked", false);
        }

        function addCloseHour(){
            var ddlCloseDate = $("#ddlCloseDate");  //頻率
            var ddlCloseDateText = $('#ddlCloseDate :selected').text();
            var chk_close_BusinessWeekly = $("input[name*=chk_close_BusinessWeekly]:checked");  //每周
            var txtCloseMonthlyDay = $("#txtCloseMonthlyDay");  //每月
            var txtCloseBeginDate = $("#txtCloseBeginDate");  //特殊
            var txtCloseEndDate = $("#txtCloseEndDate");  //特殊
            var chk_close_BusinessWeekly = $("input[name*=chk_close_BusinessWeekly]:checked");
            var ddl_close_WeeklyBeginHour = $("#ddl_close_WeeklyBeginHour").val();
            var ddl_close_WeeklyBeginMinute = $("#ddl_close_WeeklyBeginMinute").val();
            var ddl_close_WeeklyEndHour = $("#ddl_close_WeeklyEndHour").val();
            var ddl_close_WeeklyEndMinute = $("#ddl_close_WeeklyEndMinute").val();
            var chk_close_allday = $("#chk_close_allday").is(':checked');

            var _frequency = GetCloseFrequency();
            switch(_frequency){
            case frequency.weekly:
                //週
                var weeklys = chk_close_BusinessWeekly.map(function() {
                    return this.value;
                }).get().join();

                getWeeklyContinue(ddlCloseDateText, weeklys,ddl_close_WeeklyBeginHour + ":" + ddl_close_WeeklyBeginMinute, ddl_close_WeeklyEndHour + ":" + ddl_close_WeeklyEndMinute, chk_close_allday, "panCloseDate");
                break;
            case frequency.monthly:
                //月
                var beginTime = ddl_close_WeeklyBeginHour + ":" + ddl_close_WeeklyBeginMinute;
                var endTime = ddl_close_WeeklyEndHour + ":" + ddl_close_WeeklyEndMinute;
                $("#panCloseDate").append('<div class="remove">' + ddlCloseDateText + " "  + txtCloseMonthlyDay.val() + '日' + (chk_close_allday ? "全天" : beginTime + "~" + endTime )  + '<a onclick="removeSettingTime(this);"></a></div>');
                break;
            case frequency.special:
                //特殊
                var beginDate = txtCloseBeginDate.val();
                var endDate = txtCloseEndDate.val();
                var beginTime = ddl_close_WeeklyBeginHour + ":" + ddl_close_WeeklyBeginMinute;
                var endTime = ddl_close_WeeklyEndHour + ":" + ddl_close_WeeklyEndMinute;
                if ($.trim(beginDate) == ""){
                    alert("請輸入至少一個日期");
                    return false;
                }
                if($.trim(beginDate) != ""){
                    if(!ValidateDate($.trim(beginDate))){
                        alert("起始日期格式不正確");
                        return false;
                    }
                }
                if($.trim(endDate) != ""){
                    if(!ValidateDate($.trim(endDate))){
                        alert("結束日期格式不正確");
                        return false;
                    }
                }
                if(!CompareDate(beginDate,endDate)){
                    alert("結束日期需晚於起始日期");
                    return false;
                }
                var paddingDatetime = "";
                if($.trim(endDate) != ""){
                    paddingDatetime = beginDate + "~" + endDate;
                }else{
                    paddingDatetime = beginDate;
                }
                $("#panCloseDate").append('<div class="remove">' + paddingDatetime + " " +  (chk_close_allday ? "全天" : beginTime + "~" + endTime )  + '<a onclick="removeSettingTime(this);"></a></div>');
                break;
            case frequency.alldayround:
                //全年無休
                $("#panCloseDate").append('<div class="remove"><%=LunchKingSite.Core.Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, (LunchKingSite.Core.StoreCloseDate)LunchKingSite.Core.StoreCloseDate.AllYearRound)%><a onclick="removeSettingTime(this);"></a></div>');
                break;
            }

            $("input[name*=chk_close_BusinessWeekly]:checked").removeAttr('checked');
            $("#ddlCloseDate").val("1");
            $("#txtCloseMonthlyDay").val("");
            $("#txtCloseBeginDate").val("");
            $("#txtCloseEndDate").val("");
            $("input[name*=chk_close_BusinessWeekly]:checked").removeAttr('checked');
            $("#ddl_close_WeeklyBeginHour").val("01");
            $("#ddl_close_WeeklyBeginMinute").val("00");
            $("#ddl_close_WeeklyEndHour").val("01");
            $("#ddl_close_WeeklyEndMinute").val("00");
            $("#chk_close_allday").removeAttr('checked');
            $("#ddl_close_WeeklyBeginHour").attr('disabled',false);
            $("#ddl_close_WeeklyBeginMinute").attr('disabled', false);
            $("#ddl_close_WeeklyEndHour").attr('disabled', false);
            $("#ddl_close_WeeklyEndMinute").attr('disabled', false);

            $("#chk_close_AllBusinessWeekly").attr("checked", false);	

            HideOrShowCloseSpan();
        }

        function ValidateDate(dtValue) {
            var dtRegex = new RegExp(/\b(\d{4})([/])(0[1-9]|1[012])([/])(0[1-9]|[12][0-9]|3[01])\b/);
            return dtRegex.test(dtValue);
        }

        function CompareDate(sd,ed){
            var d1=new Date(sd);
            var d2=new Date(ed);
            if (d2 <= d1){
                return false;
            }
            return true;
        }

        /*
        *營業時間
        */
        function getWeeklyContinue(ofrenquency ,weeklys, beginTime, endTime, allDay, obj){
            if(weeklys == ""){
                alert("請先選擇週幾");
                return false;
            }
            $.ajax({
                type: "POST",
                url: "seller_add.aspx/GetWeeklyName",
                data: "{'frenquency':'" + ofrenquency + "','weeklys':'" + weeklys + "','beginTime':'" + beginTime + "','endTime':'" + endTime + "','allDay': '" + allDay + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    var c = response.d;
                    if(c != undefined){
                        appendSettingTime(c,obj);
                    }
                },error: function(error){
                    console.log(error);
                }
            });
        }

        function appendSettingTime(c,obj){
            $("#" + obj + "").append('<div class="remove">' + c + '<a onclick="removeSettingTime(this);"></a></div>');
        }
        /*
        * 移除營業時間div
        */
        function removeSettingTime(obj){
            obj.closest("div").remove();
        }

        /*
        * 公休日欄位顯示或隱藏
        */
        function HideOrShowCloseSpan(){
            var _weekly = false;
            var _monthly = false;
            var _special = false;
            var _time = false;

            var _frequency = GetCloseFrequency();
            switch(_frequency){
            case frequency.weekly:
                _weekly = true;
                _monthly = false;
                _special = false;
                _time = true;
                break;
            case frequency.monthly:
                _weekly = false;
                _monthly = true;
                _special = false;
                _time = true;
                break;
            case frequency.special:
                _weekly = false;
                _monthly = false;
                _special = true;
                _time = true;
                break;
            case frequency.alldayround:
                _weekly = false;
                _monthly = false;
                _special = false;
                _time = false;
                break;
            }

            if(_weekly){
                $("#panCloseWeekly").show();
            }else{
                $("#panCloseWeekly").hide();
            }
            if(_monthly){
                $("#panCloseMonthly").show();
            }else{
                $("#panCloseMonthly").hide();
            }
            if(_special){
                $("#panCloseSpecial").show();
            }else{
                $("#panCloseSpecial").hide();
            }
            if(_time){
                $("#panCloseTime").show();
            }else{
                $("#panCloseTime").hide();
            }
        }
        function setupdatepicker(from, to) {
            var dates = $('#' + from + ', #' + to).datepicker({
                changeMonth: true,
                numberOfMonths: 3,
                onSelect: function (selectedDate) {
                    var option = this.id == from ? "minDate" : "maxDate";
                    var instance = $(this).data("datepicker");
                    var date = $.datepicker.parseDate(instance.settings.dateFormat || $.datepicker._defaults.dateFormat, selectedDate, instance.settings);
                    dates.not(this).datepicker("option", option, date);
                },
                onClose: function(selectedDate) {
                    
                }
            });
        }

        function GetCloseFrequency(){
            var _frequency = "";
            switch($("#ddlCloseDate").val()){
            case "<%=(int)LunchKingSite.Core.StoreCloseDate.EveryWeekly%>":
            case "<%=(int)LunchKingSite.Core.StoreCloseDate.SingleWeekly%>":
            case "<%=(int)LunchKingSite.Core.StoreCloseDate.BiWeekly%>":
            case "<%=(int)LunchKingSite.Core.StoreCloseDate.FirstWeekly%>":
            case "<%=(int)LunchKingSite.Core.StoreCloseDate.SecondWeekly%>":
            case "<%=(int)LunchKingSite.Core.StoreCloseDate.ThirdWeekly%>":
            case "<%=(int)LunchKingSite.Core.StoreCloseDate.ForthWeekly%>":
            case "<%=(int)LunchKingSite.Core.StoreCloseDate.FifthWeekly%>":
                _frequency = frequency.weekly;
                break;
            case "<%=(int)LunchKingSite.Core.StoreCloseDate.EveryMonthly%>":
            case "<%=(int)LunchKingSite.Core.StoreCloseDate.SingleMonthly%>":
            case "<%=(int)LunchKingSite.Core.StoreCloseDate.BiMonthly%>":
                _frequency = frequency.monthly;
                break;
            case "<%=(int)LunchKingSite.Core.StoreCloseDate.SpecialDate%>":
                _frequency = frequency.special;
                break;
            case "<%=(int)LunchKingSite.Core.StoreCloseDate.AllYearRound%>":
                _frequency = frequency.alldayround;
                break;
            }
            return _frequency;
        }

        function checkAll(obj, div) {
            if ($(obj).is(":checked")) {
                $("#" + div + " > input[type=checkbox]").attr("checked", "checked");
            } else {
                $("#" + div + " > input[type=checkbox]").removeAttr("checked");
            }
        }

        function Calculate() {
            $.ajax({
                type: "POST",
                url: "seller_add.aspx/CalculateCoordinates",
                data: "{'townshipId':" + $('#selTownship').val() + ",'address':'" + $('[id*=tbStoreAddress]').val() + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    var coordinate;
                    if(response.d.Key == null || (response.d != '' && response.d.Key != 'ROOFTOP')){ //response.d.Key == null 代表 google api 使用次數爆了
                        $.unblockUI();
                        $.blockUI({ 
                            message: "<div class='googlemap' ><div class='googlemap-Top'><div class='googlemap-X-button' onclick='$.unblockUI();' style='cursor: pointer'></div></div><h2>查無準確的經緯度請前往GoogleMap查詢<br><img src='../../Images/GoogleMapEX.jpg' /><br>確認網頁地址無誤後<br>找到網頁內如圖紅框區塊，左:緯度，右:經度，填入。<br>若還有問題請洽技術人員<br><a style='font-size:12pt' href='https://www.google.com.tw/maps/place/" +  $('#selTownship').val() + $('[id*=tbStoreAddress]').val() + "' onclick='$.unblockUI();' target='_blank'>前往GoogleMap</a></h3></div>",
                            css: { border: 'none', background: 'transparent', width: '0px', height: '0px', cursor: 'default', top: ($(window).height() - 350) / 2 + 'px', left: ($(window).width() - 700) / 2 + 'px' }
                        })
                    }else if(response.d != '' && response.d.Key == 'ROOFTOP') {
                        coordinate = response.d.Value;
                        $('[id*=txtLatitude]').val(coordinate.Key);
                        $('[id*=txtLongitude]').val(coordinate.Value);
                        DrawMap();
                        CalculateNearMRTLocation();
                    }
                    else 
                    {
                        alert('沒有座標資料!!');
                    }
                }
            });
        }

        function DrawMap() {
            var latitude = $('[id*=txtLatitude]');
            var longitude = $('[id*=txtLongitude]');

            if ('<%= Config.EnableOpenStreetMap %>'.toLowerCase() == 'true') {
                DrawOpenStreetMap(latitude.val(), longitude.val());
            } else {
                if ('<%= Config.EnableGoogleMapApiV2 %>'.toLowerCase() == 'true') {
                    DrawGoogleMapV2(latitude.val(), longitude.val());
                } else {
                    DrawGoogleMapV3(latitude, longitude);
                }
            }
        }

        function CalculateNearMRTLocation() {
            var latitude = $('[id*=txtLatitude]');
            var longitude = $('[id*=txtLongitude]');

            if (latitude.val().toLowerCase() != "null" && longitude.val().toLowerCase() != "null" ){
                $.ajax({
                    type: "POST",
                    url: "SellerStoreEdit.aspx/CalculateNearMRTLocation",
                    data: "{'latitude':" + latitude.val() + ",'longitude':" + longitude.val() + "}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        if (response.d != '') {
                            // var mrtCategoryArray = $("#<%=hiddenMRTCategoryChecked.ClientID%>").val();
                            var mrtCategoryArray = [];
                            $("#mrtLocationContent").empty();
                            var fragment = "";
                            var items = response.d;
                            $.each(items, function (index, mrtLocation) {

                                var mrtName = mrtLocation.name;
                                var mrtCode = mrtLocation.code;
                                //var mrtType = mrtLocation.type;
                                //fragment +=   " "+mrtName+" :: "+mrtCode+" - "+mrtType;
                                mrtCategoryArray.push(mrtCode);
                                fragment += "<input type='checkbox' name='mrtLocation' id='mrtLocation_" + mrtCode + "' value='" + mrtCode + "' checked='checked' disabled='disabled'/><label for=''>" + mrtName + "</label><br/>";

                            });
                            $('#<%=hiddenMRTCategoryChecked.ClientID%>').val(JSON.stringify(mrtCategoryArray));
                            $("#mrtLocationContent").append(fragment);
                        }
                        else {
                            //alert('沒有座標資料!!');
                        }
                    }
                });
            }else{
                alert('沒有座標資料!!');
            }
        }

        function DrawOpenStreetMap(latitude, longitude) {

            $('#map').empty(); // 因OpenStreetMap會一直append新的地圖，必須清空
            map = new OpenLayers.Map("map");
            map.addLayer(new OpenLayers.Layer.OSM());

            var lonLat = new OpenLayers.LonLat(longitude, latitude)
                .transform(
                    new OpenLayers.Projection("EPSG:4326"), // transform from WGS 1984
                    map.getProjectionObject() // to Spherical Mercator Projection
                );
            var zoom = 16;

            var markers = new OpenLayers.Layer.Markers("Markers");
            map.addLayer(markers);
            markers.addMarker(new OpenLayers.Marker(lonLat));
            map.setCenter(lonLat, zoom);
            $('#map').css('display', '');
        }

        function DrawGoogleMapV2(latitude, longitude) {

            if (GBrowserIsCompatible()) {
                if (document.getElementById("map") != null) {
                    $('#map').css('display', '');
                    var map = new GMap2(document.getElementById("map"));
                    var geocoder = new GClientGeocoder();
                    map.addControl(new GSmallMapControl());

                    geocoder.getLocations(new GLatLng(latitude, longitude), function (point) {
                        if (point) {
                            map.setCenter(new GLatLng(latitude, longitude), 16);
                            var marker = new GMarker(new GLatLng(latitude, longitude));
                            map.addControl(new GSmallMapControl()); // 小型地圖控制項
                            map.addOverlay(marker);
                            AddMoveCoordinateListener(map, marker);
                        }
                    });
                }
            }
        }

        function AddMoveCoordinateListener(map, marker) {
            GEvent.addListener(map, "click", function (overlay, point) {
                if (point) {
                    if (confirm('確定要移動座標位置嗎?')) {
                        //設定標註座標
                        marker.setLatLng(point);
                        $('[id*=txtLongitude]').val(point.x.toString());
                        $('[id*=txtLatitude]').val(point.y.toString());
                    }
                }
            });
        }

        function DrawGoogleMapV3(latitude, longitude) {

            var myLatlng = new google.maps.LatLng(latitude.val(), longitude.val());
            var mapOptions = {
                zoom: 16,
                center: myLatlng,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            }

            var map = new google.maps.Map(document.getElementById('map'),
                mapOptions);

            var marker = new google.maps.Marker({
                position: myLatlng,
                draggable: true,
                map: map
            });

            google.maps.event.addListener(
                marker,
                'drag',
                function () {
                    latitude.val(marker.position.lat());
                    longitude.val(marker.position.lng());
                }
            );

            $('#map').css('display', '');
        }

        function verifyInput() {
            //result = ValidatePhoto();  //because new seller no updatepanel panel by sam
            //選擇結束營業 => 必須要設定結束營業日
            if ($('#formIsCloseDown').val() === 'true') {
                if ($('#formCloseDownDate').val().length === 0) {
                    alert('請設定結束營業日期!');
                    return false;
                } 
                if (!window.confirm("您已調整營業狀態，請問是否儲存？")) {
                    return false;
                }
            } 

            // tbsellerName must have value
            $(':text[id*=tbSellerName]').val($.trim($(':text[id*=tbSellerName]').val()));
            if ($(':text[id*=tbSellerName]').val() == "賣家名稱不得空白" || $(':text[id*=tbSellerName]').val().length === 0) {
                alert('請設定公司/品牌/店鋪名稱，不得全空白');
                return false;
            } 

            if ($('#ddlSellerFrom').val() === "-1") {
                alert('請選擇店家來源');
                return false;
            } 

            if ($.trim($('#tbSellerBossName').val()).length === 0) {
                alert('請設定負責人');
                return false;
            } 
            
            if ($.trim($('#tbSellerTel').val()).length === 0) {
                alert('請設定電話(代表號)');
                return false;
            } 

            if ($.trim($('#tbSellerAddr').val()).length === 0) {
                alert('請設定地址');
                return false;
            } 
            

            if ($('input:checkbox[id*=chkSellerPorperty]:checked').length == 0) {
                alert('請設定店家屬性');
                return false;
            }

            //營業時間
            var _settingTimes = "";
            $.each($("#panBusinessHour div"),function(idx, value){
                _settingTimes += value.firstChild.data + " ；";
            });
            if (_settingTimes.length > 0) {
                _settingTimes = _settingTimes.substring(0, _settingTimes.length - 1);
            }
            $("#tbBusinessHour").val(_settingTimes);

            //公休日
            var _settingCloseTimes = "";
            $.each($("#panCloseDate div"),function(idx, value){
                _settingCloseTimes += value.firstChild.data + "；";
            });
            if (_settingCloseTimes.length > 0) {
                _settingCloseTimes = _settingCloseTimes.substring(0, _settingCloseTimes.length - 1);
            }
            $("#<%=tbCloseDate.ClientID%>").val(_settingCloseTimes);

            var result = confirm('確定送出？');
            return result;
        }

        function showActivity() {
            if ($('.showActivityList input').attr('checked')) {
                $('#divActivityList').css('display', '');
            }
            else {
                $('#divActivityList').css('display', 'none');
            }
        }

        function checkEmailReg(emailStr) {
            if (emailStr.split('@').length > 1) {
                var maildomain = emailStr.split('@')[1];
                $.each(maildomain, function (i, v) {
                    var result = /[\u4e00-\u9fa5\w\.]/.test(v);
                    if (!result) {
                        alert("信箱可能有誤，請確認。");
                        return;
                    }
                });
            } else {
                alert("信箱可能有誤，請確認。");
                return;
            }
        }

        function NoData() {
            alert("此餐點無任何配料!!");
        }
        function CheckBusinessOrderId(obj) {
            var businessOrderId = $(obj).prev('[id*=txtBusinessOrderId]').val();
            if (businessOrderId == '') {
                alert('請輸入工單編號!!');
                return false;
            } else {
                return confirm('將會帶入賣家基本資料並一併更新分店資訊，確定繼續?');
            }
        }
        function ClearEmptyData(obj) {
            $(obj).val($.trim($(obj).val()));
        }

        function showCopySeller(copySellerGuid) {
            $.blockUI({
                message: "<h3><a  style='font-size:12pt' href='seller_add.aspx?sid=" + copySellerGuid + "' target='_blank'>賣家已複製-" + copySellerGuid + "</a></h3>"
                , css: { border: 'none', background: 'transparent', width: '0px', height: '0px', cursor: 'default', top: ($(window).height() - 350) / 2 + 'px', left: ($(window).width() - 700) / 2 + 'px' }
            });
        }

        function ShowRemittanceType(obj)
        {
            if (obj.is(':checked'))
                $('#ddlRemittanceType').show();
            else
                $('#ddlRemittanceType').hide();
        }

    </script>
    <cc1:TabContainer ID="Seller" runat="server" ActiveTabIndex="1">
        <cc1:TabPanel ID="TabPanelSeller" runat="server" HeaderText="TabPanelSeller">
            <HeaderTemplate>
                <asp:Label ID="lbMode" runat="server" ForeColor="Red"></asp:Label>
                商家<asp:Label ID="lbSellerName" runat="server" Font-Bold="True"></asp:Label>基本資料<asp:Label
                    ID="Umessage" runat="server" ForeColor="Red"></asp:Label>
            </HeaderTemplate>
            <ContentTemplate>
                <table border="0" cellpadding="0" cellspacing="2" width="100%">
                    <tr>
                        <td style="width:10%">賣家編號
                        </td>
                        <td style="width:20%">
                            <asp:Label ID="lbSellerID" runat="server"></asp:Label>
                        </td>
                        <td style="width:10%">功能
                        </td>
                        <td style="width:40%">
                            <asp:Button ID="btnCopySeller" runat="server" Text="複製為新商家" OnClick="btnCopySeller_Click"></asp:Button>
                            <asp:Literal ID="Lit_copydeallink" runat="server"></asp:Literal>
                        </td>
                    </tr>
                    <tr>
                        <td>賣家館別
                        </td>
                        <td>
                            <asp:Label runat="server" ID="lbSellerDepartment" Text="團購網"></asp:Label>
                        </td>
                        <td>狀態</td>
                        <td>
                            <span style="color:red;font-size: 10pt;font-weight: bold"><asp:Literal ID="liTempStatus" runat="server" ></asp:Literal></span>
                            <asp:Button ID="btnSellerApprove" runat="server" Text="申請核准" OnClick="btnSellerApprove_Click" Visible="false" />
                            <asp:Button ID="btnSellerReturned" runat="server" Text="退回申請" OnClick="btnSellerReturned_Click" Visible="false" />
                            <asp:Button ID="btnSellerApply" runat="server" Text="申請變更" OnClick="btnSellerApply_Click" Visible="false" />
                        </td>
                    </tr>
                </table>
                <div id="sellerInfo">
                <h3 style="color:blue">基本資料 & 狀態</h3>
                <table border="0" cellpadding="0" cellspacing="2" width="100%">
                    <tr>
                        <td width="20%"><span style="color: #ff0000; font-weight: bold;">*</span>公司/品牌/店鋪名稱
                        </td>
                        <td width="30%">
                            <asp:TextBox ID="tbSellerName" runat="server" Width="300px" ClientIDMode="Static"></asp:TextBox>
                            <span style="color: #FF8C69; font-size: small;">
                                <asp:Literal ID="lit_Seller_SellerName" runat="server"></asp:Literal></span>
                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender4" runat="server" TargetControlID="tbSellerName"
                                WatermarkText="公司/品牌/店鋪名稱不得空白" WatermarkCssClass="watermark" Enabled="True">
                            </cc1:TextBoxWatermarkExtender>
                        </td>
                        <td width="20%">最新工單編號
                        </td>
                        <td width="30%">
                            <asp:TextBox ID="txtBusinessOrderId" runat="server" onkeyup="ClearEmptyData(this);"></asp:TextBox>
                            <asp:Button ID="btnImportBusinessOrderInfo" runat="server" Text="與工單串接" Visible="False" OnClick="btnImportBusinessOrderInfo_Click"
                                OnClientClick="return CheckBusinessOrderId(this);" /><br />
                        </td>
                    </tr>
                    <tr>
                        <td>上層公司名稱
                        </td>
                        <td>
                            <asp:Label ID="lbParentSellerId" runat="server" ClientIDMode="Static" style="font-size:10pt"></asp:Label>
                            <asp:HiddenField ID="hidParentSellerGuid" runat="server" ClientIDMode="Static" />
                            <asp:TextBox ID="txtParentSellerName" runat="server" ClientIDMode="Static"></asp:TextBox><br/>
                            <asp:HyperLink ID="hlSellerTree" runat="server" Target="_blank" style="font-size:11pt">組織樹狀圖</asp:HyperLink>
                        </td>
                        <td>賣家規模
                        </td>
                        <td>
                            <asp:Label ID="lbSellerLevel" runat="server"></asp:Label>
                            <asp:HiddenField ID="hdSellerLevel" runat="server" ClientIDMode="Static" />
                        </td>
                    </tr>
                    <tr>
                        <td>簽約公司ID/統一編號
                        </td>
                        <td>
                            <asp:TextBox ID="tbSignCompanyID" onkeydown="value=value.replace(/\ /g,'')" onkeyup="value=value.replace(/\ /g,'')"
                                runat="server" MaxLength="10"></asp:TextBox><span style="color: #FF8C69; font-size: small;">
                                    <asp:Literal ID="lit_Seller_CompanySignId" runat="server"></asp:Literal></span>
                        </td>
                        <td><span style="color: #ff0000; font-weight: bold;">*</span>店家來源</td>
                        <td>
                            <asp:DropDownList runat="server" ID="ddlSellerFrom" Width="100px" ClientIDMode="Static" AppendDataBoundItems="true">
                                <asp:ListItem Text="請選擇" Value="-1"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td><span style="color: #ff0000; font-weight: bold;">*</span>負責人
                        </td>
                        <td>
                            <asp:TextBox ID="tbSellerBossName" runat="server" ClientIDMode="Static"></asp:TextBox>
                            <span style="color: #FF8C69; font-size: small;">
                            <asp:Literal ID="lit_Seller_BossName" runat="server"></asp:Literal></span>
                        </td>
                        <td><span style="color: #ff0000; font-weight: bold;">*</span>店家屬性</td>
                        <td>
                            <asp:CheckBoxList ID="chkSellerPorperty" runat="server" RepeatColumns="4" CssClass="checkBoxList" ClientIDMode="Static"></asp:CheckBoxList>
                        </td>
                    </tr>
                    <tr>
                        <td><span style="color: #ff0000; font-weight: bold;">*</span>電話(代表號)
                        </td>
                        <td>
                            <asp:TextBox ID="tbSellerTel" runat="server" MaxLength="100" ClientIDMode="Static"></asp:TextBox>
                            <span style="color: #FF8C69; font-size: small;">
                                <asp:Literal ID="lit_Seller_Tel" runat="server"></asp:Literal></span>
                        </td>
                        <td>店家類別</td>
                        <td>
                            <asp:DropDownList ID="ddl_Seller_Category" runat="server"></asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td rowspan="2"><span style="color: #ff0000; font-weight: bold;">*</span>地址
                        </td>
                        <td rowspan="2">
                            <asp:UpdatePanel ID="UPanelAddress" runat="server">
                                <ContentTemplate>
                                    <asp:DropDownList ID="ddlCity" runat="server" AutoPostBack="True" DataTextField="CityName"
                                        DataValueField="Id" OnSelectedIndexChanged="ddlCity_SelectedIndexChanged">
                                    </asp:DropDownList>
                                    &nbsp;<asp:DropDownList ID="ddlZone" runat="server" DataTextField="Value" DataValueField="Key"
                                        AutoPostBack="True" OnSelectedIndexChanged="ddlZone_SelectedIndexChanged">
                                    </asp:DropDownList>
                                    <asp:HiddenField ID="hfZone" runat="server" />
                                    <asp:TextBox ID="tbSellerAddr" runat="server" Width="300px" ClientIDMode="Static"></asp:TextBox>
                                    <asp:HiddenField ID="hfSellerID" runat="server" />
                                    <asp:UpdateProgress ID="UProgressCity" runat="server" AssociatedUpdatePanelID="UPanelAddress">
                                        <ProgressTemplate>
                                            <img src="../../Themes/PCweb/images/spinner.gif" />
                                        </ProgressTemplate>
                                    </asp:UpdateProgress>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            <span style="color: #FF8C69; font-size: small;">
                                <asp:Literal ID="lit_Seller_CityId" runat="server"></asp:Literal></span> <span style="color: #FF8C69;
                                    font-size: small;">
                                    <asp:Literal ID="lit_Seller_Address" runat="server"></asp:Literal></span>
                        </td>
                        <td>營業狀態
                        </td>
                        <td>
                            <asp:DropDownList runat="server" ID="formIsCloseDown" ClientIDMode="Static">
                                <asp:ListItem Text="正常營業" Value="false"></asp:ListItem>
                                <asp:ListItem Text="結束營業" Value="true"></asp:ListItem>
                            </asp:DropDownList>
                            <asp:TextBox runat="server" ID="formCloseDownDate" ClientIDMode="Static" placeholder="輸入結束營業日期"/>                            
                        </td>
<%--                        <td rowspan="2">
                            <asp:Image ID="imgDefaultSellerPhoto" runat="server" AlternateText="無上傳照片" />
                            <asp:HiddenField ID="hfPath" runat="server" />
                            <asp:HiddenField ID="hfPhotoPath" runat="server" />
                            <asp:HiddenField ID="hfVideoPath" runat="server" />
                        </td>--%>
                    </tr>
                    <tr>
                        <td>顯示狀態</td>
                        <td>
                            <asp:DropDownList runat="server" ID="ddlStoreStatus" Width="100px" ClientIDMode="Static" AppendDataBoundItems="true">
                                <asp:ListItem Text="顯示" Value="0"></asp:ListItem>
                                <asp:ListItem Text="隱藏" Value="1"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>                    
                    <tr>
                        
                        <td><span style="color: #ff0000; font-weight: bold;">*</span>負責業務
                        </td>
                        <td colspan="3">
                            <div id="multipleSales">

                            </div>
                        </td>
                        
                    </tr>
                    <tr>
                        
                        <td><span style="color: #ff0000; font-weight: bold;">*</span>負責二線客服
                        </td>
                        <td colspan="3">
                            <div>
                                <asp:Label ID="lblsecond" runat="server" ClientIDMode="Static" style="font-size:10pt"></asp:Label>
                            </div>
                        </td>
                        
                    </tr>
                </table>
                </div>
                <div id="contactInfo">
                    <h3 style="color:blue">通訊錄</h3>
                    <table class="tablecontent" style="width: 960px;">
                        <tr style="text-align: center">
                            <td>類別</td>
                            <td>姓名</td>
                            <td>連絡電話</td>
                            <td>Email</td>
                            <td>行動電話</td>
                            <td>傳真</td>
                            <td>其他/備註</td>
                        </tr>
                        <asp:Repeater ID="repContacts" runat="server">
                            <ItemTemplate>
                                <tr>
                                    <td style="width:16%;"><%# GetContactTypeDesc(int.Parse(Eval("Type").ToString()))%></td>
                                    <td style="width:9%;"><%# Eval("ContactPersonName")%></td>
                                    <td style="width:12%;"><%# Eval("SellerTel")%></td>
                                    <td style="width:20%;"><%# Eval("ContactPersonEmail")%></td>
                                    <td style="width:15%;"><%# Eval("SellerMobile")%></td>
                                    <td style="width:15%;"><%# Eval("SellerFax")%></td>
                                    <td><%# Eval("Others")%></td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </table>
                </div>
                <div id="financeInfo">
                    <h3 style="color:blue">財務資料 & 帳務聯絡人</h3>
                    <table style="width:100%">
                        <tr>
                            <td>簽約公司名稱
                            </td>
                            <td>
                                <asp:TextBox ID="tbCompanyName" runat="server" MaxLength="50"></asp:TextBox>
                                <span style="color: #FF8C69; font-size: small;">
                                    <asp:Literal ID="lit_Seller_ComapnyName" runat="server"></asp:Literal></span>
                            </td>
                            <td>帳務聯絡人姓名
                            </td>
                            <td>
                                <asp:TextBox ID="tbAccountantName" runat="server" MaxLength="50"></asp:TextBox><br />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <% if (Config.IsRemittanceFortnightly  && IsAgreePponNewContractSeller)
                                    { %>
                            
                                憑證出帳方式
                            <%}
                                else
                                { %>
                            
                                出帳方式
                            <%} %>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlRemittanceType" runat="server" DataTextField="key" DataValueField="value" ClientIDMode="Static">
                                </asp:DropDownList>
                            </td>
                            <td>帳務聯絡電話
                            </td>
                            <td>
                                <asp:TextBox ID="tbAccountantTel" runat="server" MaxLength="100"></asp:TextBox><br />
                            </td>
                        </tr>
                        <tr>
                            <td>負責人名稱
                            </td>
                            <td>
                                <asp:TextBox ID="tbCompanyBossName" runat="server" MaxLength="30"></asp:TextBox>
                                <span style="color: #FF8C69; font-size: small;">
                                    <asp:Literal ID="lit_Seller_CompanyBossName" runat="server"></asp:Literal></span>
                            </td>
                            <td>帳務連絡人電子信箱
                            </td>
                            <td>
                                <asp:TextBox ID="tbCompanyEmail" runat="server" MaxLength="250"　CssClass="checkEmailClass"></asp:TextBox><br />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span>受款人ID/統一編號</span>
                            </td>
                            <td>
                                <asp:TextBox ID="tbCompanyID" onkeydown="value=value.replace(/\ /g,'')" onkeyup="value=value.replace(/\ /g,'')"
                                    runat="server" MaxLength="10"></asp:TextBox><br />
                            </td>
                            <td rowspan="5">備 註
                            </td>
                            <td rowspan="5">
                                <asp:TextBox ID="tbCompanyNotice" runat="server" TextMode="MultiLine" Rows="5" Columns="30"></asp:TextBox><br />
                            </td>
                        </tr>
                        <tr>
                            <td>銀行代號
                            </td>
                            <td>
                                <asp:TextBox ID="tbCompanyBankCode" onkeydown="value=value.replace(/\ /g,'')" onkeyup="value=value.replace(/\ /g,'')"
                                    runat="server" MaxLength="3"></asp:TextBox><br />
                            </td>
                        </tr>
                        <tr>
                            <td>分行代號
                            </td>
                            <td>
                                <asp:TextBox ID="tbCompanyBranchCode" onkeydown="value=value.replace(/\ /g,'')" onkeyup="value=value.replace(/\ /g,'')"
                                    runat="server" MaxLength="4"></asp:TextBox><br />
                            </td>
                        </tr>
                        <tr>
                            <td>帳 號
                            </td>
                            <td>
                                <asp:TextBox ID="tbCompanyAccount" onkeydown="value=value.replace(/\-/g,'').replace(/\ /g,'')"
                                    onkeyup="value=value.replace(/\-/g,'').replace(/\ /g,'')" runat="server"></asp:TextBox><br />
                            </td>
                        </tr>
                        <tr>
                            <td>戶 名
                            </td>
                            <td>
                                <asp:TextBox ID="tbCompanyAccountName" runat="server" MaxLength="50"></asp:TextBox><br />
                            </td>
                        </tr>
                    </table>
                </div>
                <div id="storeInfo">
                    <h3 style="color:blue">店鋪營業資料</h3>
                    <table>
                        <tr>
                            <td>
                                訂位電話
                            </td>
                            <td>
                                <asp:TextBox ID="tbStoreTel" runat="server" Width="300"></asp:TextBox><span style="color: #FF8C69;
                                    font-size: small;">
                                    <asp:Literal ID="lit_Store_Phone" runat="server"></asp:Literal></span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                店鋪地址
                            </td>
                            <td>
                                <asp:Repeater ID="repCity" runat="server">
                                    <HeaderTemplate>
                                        <select name="selCity" id="selCity">
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <option value="<%#Eval("Id")%>" <%# ((City)Container.DataItem).Id == AddressCityId ? "selected='selected'" : "" %>>
                                            <%#Eval("CityName")%></option>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </select>
                                    </FooterTemplate>
                                </asp:Repeater>
                                <asp:Repeater ID="repTown" runat="server">
                                    <HeaderTemplate>
                                        <select name="selTownship" id="selTownship">
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <option value="<%#Eval("Id")%>" <%#((City)Container.DataItem).Id == AddressTownshipId ? "selected='selected'" : "" %>>
                                            <%#Eval("CityName")%></option>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </select>
                                    </FooterTemplate>
                                </asp:Repeater>
                                <asp:TextBox ID="tbStoreAddress" runat="server" Width="270"></asp:TextBox><span style="color: #FF8C69;
                                    font-size: small;">
                                    <asp:Literal ID="Literal1" runat="server"></asp:Literal></span> <span style="color: #FF8C69;
                                        font-size: small;">
                                        <asp:Literal ID="Literal2" runat="server"></asp:Literal></span>
                                &nbsp;<input type="button" value="計算經緯度" onclick="Calculate();" />&nbsp; <br />
                                緯度: <asp:TextBox ID="txtLatitude" runat="server" Width="115px"></asp:TextBox>&nbsp; 
                                經度: <asp:TextBox ID="txtLongitude" runat="server" Width="115px"></asp:TextBox>
                                <input type="button" value="畫出地圖" onclick="DrawMap();" />&nbsp;<input type="button" value="重新計算捷運站" onclick="CalculateNearMRTLocation();" /><br />
                                <font color="red">緯度介於正負90, 經度介於正負180; 不想填請填0</font>
                                <div id="map" style="width: 450px; height: 250px;">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-top:30px"></td>
                        </tr>
                        <tr>
                            <td>
                                營業時間
                            </td>
                            <td>
                                <asp:CheckBox ID="chk_AllBusinessWeekly" runat="server" ClientIDMode="Static" />全選
                                <br />
                                <input type="checkbox" id="chk_BusinessWeekly0" name="chk_BusinessWeekly" value="0" />週一
                                <input type="checkbox" id="chk_BusinessWeekly1" name="chk_BusinessWeekly" value="1" />週二
                                <input type="checkbox" id="chk_BusinessWeekly2" name="chk_BusinessWeekly" value="2" />週三
                                <input type="checkbox" id="chk_BusinessWeekly3" name="chk_BusinessWeekly" value="3" />週四
                                <input type="checkbox" id="chk_BusinessWeekly4" name="chk_BusinessWeekly" value="4" />週五
                                <input type="checkbox" id="chk_BusinessWeekly5" name="chk_BusinessWeekly" value="5" />週六
                                <input type="checkbox" id="chk_BusinessWeekly6" name="chk_BusinessWeekly" value="6" />週日
                                <br />
                                時間:<asp:DropDownList ID="ddl_WeeklyBeginHour" runat="server" ClientIDMode="Static"></asp:DropDownList>:
                                <asp:DropDownList ID="ddl_WeeklyBeginMinute" runat="server" ClientIDMode="Static"></asp:DropDownList>～
                                <asp:DropDownList ID="ddl_WeeklyEndHour" runat="server" ClientIDMode="Static"></asp:DropDownList>:
                                <asp:DropDownList ID="ddl_WeeklyEndMinute" runat="server" ClientIDMode="Static"></asp:DropDownList>                                
                                <div>
                                    <input type="button" value="新增" onclick="addBusinessHour();" />
                                </div>
                                <asp:Panel ID="panBusinessHour" runat="server" ClientIDMode="Static" style="color:red">
                                   
                                </asp:Panel>
                                <span style="display:none">
                                    <asp:TextBox ID="tbBusinessHour" TextMode="MultiLine" runat="server" Width="300" ClientIDMode="Static"></asp:TextBox>
                                </span>
                                <span
                                    style="color: Gray;">
                                    <asp:Literal ID="lit_Store_OpenTime" runat="server"></asp:Literal></span>
                                <div id="divBusinessHour" style="display:none;width:500px;height:300px">
                                    <asp:PlaceHolder ID="phBusinessStores" runat="server">
                                        <asp:Repeater ID="rptBStore" runat="server" OnItemDataBound="rptBStore_ItemDataBound">
                                            <HeaderTemplate>
                                                <asp:CheckBox ID="chkAllStore" runat="server" ClientIDMode="Static" onclick="checkAll(this,'divBusinessStore')" />全選<br />
                                                <hr class="header_hr">
								            </HeaderTemplate>
								            <ItemTemplate>
                                                <div id="divBusinessStore">
                                                <asp:CheckBox ID="chkStore" runat="server" /> <%# ((KeyValuePair<Guid,string>)Container.DataItem).Value %>
									            <br />
                                                </div>
								            </ItemTemplate>
								            <FooterTemplate>
                                                <input type="button" id="btnSyncBusinessHour" value="確認同步" class="btn btn-small btn-primary form-inline-btn" onclick="syncBusinessHour()" />
								            </FooterTemplate>
                                        </asp:Repeater>
                                    </asp:PlaceHolder>
                                </div>
                                <span>
                                    <asp:HyperLink ID="syncBusinessHour" runat="server" ClientIDMode="Static" class="btn btn-small btn-primary form-inline-btn openclose">將營業時間同步到其他店鋪</asp:HyperLink></span>
                                <hr />
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-top:30px"></td>
                        </tr>
                        <tr>
                            <td>
                                公休日
                            </td>
                            <td>
                                頻率：<asp:DropDownList ID="ddlCloseDate" runat="server" ClientIDMode="Static"></asp:DropDownList><br />
                                <span id="panCloseWeekly">
                                    <asp:CheckBox ID="chk_close_AllBusinessWeekly" runat="server" ClientIDMode="Static" />全選
                                    <br />
                                    <input type="checkbox" id="chk_close_BusinessWeekly0" name="chk_close_BusinessWeekly" value="0" />週一
                                    <input type="checkbox" id="chk_close_BusinessWeekly1" name="chk_close_BusinessWeekly" value="1" />週二
                                    <input type="checkbox" id="chk_close_BusinessWeekly2" name="chk_close_BusinessWeekly" value="2" />週三
                                    <input type="checkbox" id="chk_close_BusinessWeekly3" name="chk_close_BusinessWeekly" value="3" />週四
                                    <input type="checkbox" id="chk_close_BusinessWeekly4" name="chk_close_BusinessWeekly" value="4" />週五
                                    <input type="checkbox" id="chk_close_BusinessWeekly5" name="chk_close_BusinessWeekly" value="5" />週六
                                    <input type="checkbox" id="chk_close_BusinessWeekly6" name="chk_close_BusinessWeekly" value="6" />週日
                                    <br />
                                </span>
                                <span id="panCloseMonthly">
                                    日子：<asp:TextBox ID="txtCloseMonthlyDay" runat="server" placeholder="填入特定日(只寫數字，以、號隔開，如 「1、15」)" Width="300px" ClientIDMode="Static"></asp:TextBox>日<br />
                                </span>
                                <span id="panCloseSpecial">
                                    日期：<asp:TextBox ID="txtCloseBeginDate" runat="server" Width="100px" ClientIDMode="Static" ></asp:TextBox>～<asp:TextBox ID="txtCloseEndDate" runat="server"  Width="100px" ClientIDMode="Static" ></asp:TextBox><br />
                                </span>
                                <span id="panCloseTime">
                                時間:<asp:DropDownList ID="ddl_close_WeeklyBeginHour" runat="server" ClientIDMode="Static"></asp:DropDownList>:
                                    <asp:DropDownList ID="ddl_close_WeeklyBeginMinute" runat="server" ClientIDMode="Static"></asp:DropDownList>～
                                    <asp:DropDownList ID="ddl_close_WeeklyEndHour" runat="server" ClientIDMode="Static"></asp:DropDownList>:
                                    <asp:DropDownList ID="ddl_close_WeeklyEndMinute" runat="server" ClientIDMode="Static"></asp:DropDownList>
                                    <asp:CheckBox ID="chk_close_allday" runat="server" ClientIDMode="Static" />全天<br />
                                </span>
                                <input type="button" value="新增" onclick="addCloseHour();" /><br />
                                <hr />
                                <asp:Panel ID="panCloseDate" runat="server" ClientIDMode="Static" style="color:red">
                                   
                                </asp:Panel>
                                <div style="display:none">
                                    <asp:TextBox ID="tbCloseDate" runat="server" Width="300"></asp:TextBox>
                                </div>
                                <span style="color: #FF8C69;
                                    font-size: small;">
                                    <asp:Literal ID="lit_Store_CloseDate" runat="server"></asp:Literal></span>
                                <div id="divCloseDate" style="display:none;width:500px;height:300px">
                                    <asp:PlaceHolder ID="phCloseStores" runat="server">
                                        <asp:Repeater ID="rptCStore" runat="server" OnItemDataBound="rptCStore_ItemDataBound">
                                            <HeaderTemplate>
                                                <asp:CheckBox ID="chkAllStore" runat="server" ClientIDMode="Static" onclick="checkAll(this,'divCloseStore')"  />全選<br />
                                                <hr class="header_hr">
								            </HeaderTemplate>
								            <ItemTemplate>
                                                <div id="divCloseStore">
                                                <asp:CheckBox ID="chkStore" runat="server" /> <%# ((KeyValuePair<Guid,string>)Container.DataItem).Value %>
									            <br />
                                                </div>
								            </ItemTemplate>
								            <FooterTemplate>
                                                <input type="button" id="btnSyncCloseDate" value="確認同步" class="btn btn-small btn-primary form-inline-btn" onclick="syncCloseDate()" />
								            </FooterTemplate>
                                        </asp:Repeater>
                                    </asp:PlaceHolder>
                                </div>
                                <span >
                                    <asp:HyperLink ID="syncCloseDate" runat="server" ClientIDMode="Static" class="btn btn-small btn-primary form-inline-btn openclose">將公休日同步到其他店鋪</asp:HyperLink></span>
                                <hr />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                交通資訊
                            </td>
                            <td>
                                捷運(舊)：
                                <asp:TextBox ID="tbMrt" runat="server" Width="300"></asp:TextBox>
                                <span style="color: #FF8C69;font-size: small;">
                                    <asp:Literal ID="lit_Store_MRT" runat="server"></asp:Literal>
                                </span><br />
                                捷運(新)：
                                <br/>
                                    <asp:HiddenField ID="hiddenMRTCategoryItem" runat="server" Value="[]" />
                                    <asp:HiddenField ID="hiddenMRTCategoryChecked" runat="server" Value="[]" />
                                    <div id="mrtLocationContent">
                                     </div>
                                開車：
                                <asp:TextBox ID="tbCar" runat="server" Width="300"></asp:TextBox>
                                <span style="color: #FF8C69;font-size: small;">
                                    <asp:Literal ID="lit_Store_Car" runat="server"></asp:Literal>
                                </span><br />
                                公車：
                                <asp:TextBox ID="tbBus" runat="server" Width="300"></asp:TextBox>
                                <span style="color: #FF8C69;font-size: small;">
                                    <asp:Literal ID="lit_Store_Bus" runat="server"></asp:Literal>
                                </span><br />
                                其他：
                                <asp:TextBox ID="tbOV" runat="server" Width="300"></asp:TextBox>
                                <span style="color: #FF8C69;font-size: small;">
                                    <asp:Literal ID="lit_Store_OtherVehicle" runat="server"></asp:Literal>
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                網站
                            </td>
                            <td>
                                <asp:TextBox ID="tbWebUrl" runat="server" Width="300"></asp:TextBox><span style="color: #FF8C69;
                                    font-size: small;">
                                    <asp:Literal ID="lit_Store_WebUrl" runat="server"></asp:Literal></span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                FaceBook
                            </td>
                            <td>
                                <asp:TextBox ID="tbFBUrl" runat="server" Width="300"></asp:TextBox><span style="color: #FF8C69;
                                    font-size: small;">
                                    <asp:Literal ID="lit_Store_FaceBookUrl" runat="server"></asp:Literal></span>
                            </td>
                        </tr>
                    	<tr>
                        	<td>
                                部落格
							</td>	
                            <td>
                                <asp:TextBox ID="tbBlogUrl" runat="server" Width="300"></asp:TextBox><span style="color: #FF8C69;
                                    font-size: small;">
                                    <asp:Literal ID="lit_Store_BlogUrl" runat="server"></asp:Literal></span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                其他
                            </td>
                            <td>
                                <asp:TextBox ID="tbOtherUrl" runat="server" Width="300"></asp:TextBox><span style="color: #FF8C69;
                                    font-size: small;">
                                    <asp:Literal ID="lit_Store_OtherUrl" runat="server"></asp:Literal></span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                備 註
                            </td>
                            <td>
                                <asp:TextBox ID="tbRemark" runat="server" Width="300"></asp:TextBox><span style="color: #FF8C69;
                                    font-size: small;">
                                    <asp:Literal ID="lit_Store_Remark" runat="server"></asp:Literal></span>
                            </td>
                        </tr>
                    </table>
                </div>
                <div id="serviceStatus">
                    <h3 style="color:blue">店鋪提供服務</h3>
                    <table>
                        <tr>
                            <td>
                                <asp:CheckBox ID="cbCreditCardAvailable" runat="server" />
                                <span style="color: #FF8C69;font-size: small;">
                                    <asp:Literal ID="lit_Store_CreditCardAvailable" runat="server"></asp:Literal>
                                </span>
                            </td>
                            <td>
                                刷卡服務
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:CheckBox ID="cbIsOpenBooking" runat="server" />
                            </td>
                            <td>
                                預約管理
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                開放店家自行維護：
                                <asp:RadioButtonList ID="rdlIsOpenReservationSetting" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow">
                                    <asp:ListItem Text="開放" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="不開放" Value="0" Selected="True"></asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                    </table>
                </div>
                <div id="other">
                    <h3 style="color:blue">其他</h3>
                    <table>
                        <tr>
                            <td>POS串接APP ID:</td>
                            <td>
                                <asp:TextBox ID="tbOauthClientAppId" runat="server" Width="300px"></asp:TextBox>
                                <span style="color: #FF8C69; font-size: small;">
                                    <asp:Literal ID="lit_OauthClientAppId" runat="server"></asp:Literal>有串接POS才需填，如有需要請聯絡 IT Service</span>
                            </td>
                        </tr>
                        <tr>
                            <td>分店代碼:</td>
                            <td>
                                <asp:TextBox ID="tbStoreRelationCode" runat="server" Width="300px"></asp:TextBox>
                                <span style="color: #FF8C69; font-size: small;">店家分店代碼，有串接POS才需填</span>
                            </td>
                        </tr>
                    </table>
                </div>
                <div style="text-align: center">
                    <asp:Button ID="btnSellerSubmit" runat="server" OnClick="submit_check" OnClientClick="if(verifyInput()){ return true;} else {return false;} "
                        CssClass="btnSave" />&nbsp;
                        <%--return ValidatePhoto();--%>
                        &nbsp;
                    <asp:Button ID="btnCancel" runat="server" Text="取消" OnClick="btnCancel_Click" CssClass="btnSave" />
                </div>
                <div id="changeLog">
                    <h3 style="color:blue">變更紀錄</h3>
                    <table style="width: 960px;" class="tablecontent">   
                        <tr style="text-align: center">
                            <td>異動人員</td>
                            <td>異動內容</td>
                            <td>異動時間</td>
                        </tr>
                        <asp:Repeater ID="rpt_AuditLog" runat="server">
                            <ItemTemplate>
                                <tr>
                                    <td>
                                        <%# string.Format("{0}", ((Audit)(Container.DataItem)).CreateId.Replace("@17life.com", string.Empty))%>
                                    </td>
                                    <td>
                                        <%#  Regex.Replace(((Audit)(Container.DataItem)).Message,"[{}\"]",string.Empty).Replace(",","<br/>")%>
                                    </td>
                                    <td>
                                        <%# ((Audit)(Container.DataItem)).CreateTime.ToString("yyyy/MM/dd hh:mm")%>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </table>
                </div>  
                <!--側邊anchor Start-->
                <ul id="subMenu" class="nav-anchor" style="top: 150px">
            
                </ul> 
            </ContentTemplate>     
        </cc1:TabPanel>
        <cc1:TabPanel runat="server" HeaderText="17Life好康設定" ID="TabPanelPponSetup">
            <ContentTemplate>
            </ContentTemplate>
            <ContentTemplate>
                <asp:UpdatePanel ID="UPanelPponSetup" runat="server">
                    <ContentTemplate>
                        <table width="98%">
                            <tr>
                                <td align="left">
                                    <a href='../ppon/setup.aspx?sid=<%=SellerGuid%>'>新增一起P好康活動</a>
                                </td>
                            </tr>
                        </table>
                        <br />
                        <asp:GridView ID="gvOrder" runat="server" EnableViewState="false" OnRowDataBound="gvOrder_RowDataBound"
                            AllowSorting="True" GridLines="Vertical" ForeColor="Black" CellPadding="4" BorderWidth="1px"
                            BorderStyle="None" BorderColor="#DEDFDE" BackColor="White" AllowPaging="False"
                            EmptyDataText="無符合的資料" AutoGenerateColumns="False" Font-Size="Small">
                            <FooterStyle BackColor="#CCCC99"></FooterStyle>
                            <RowStyle BackColor="#F7F7DE"></RowStyle>
                            <SelectedRowStyle BackColor="#CE5D5A" ForeColor="White" Font-Bold="True"></SelectedRowStyle>
                            <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right"></PagerStyle>
                            <HeaderStyle BackColor="#6B696B" ForeColor="White" Font-Bold="True"></HeaderStyle>
                            <AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
                            <Columns>
                                <asp:BoundField HeaderText="店家名稱" DataField="SellerName" ReadOnly="True">
                                    <ItemStyle Width="95px" Wrap="False" HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField HeaderText="檔號" DataField="UniqueId" ReadOnly="True">
                                    <ItemStyle Width="65px" Wrap="False" HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="活動設定">
                                    <ItemStyle Wrap="False" Width="130px" />
                                    <ItemTemplate>
                                        <asp:HyperLink runat="server" ID="hlEventName"></asp:HyperLink>
                                    </ItemTemplate>
                                    <ItemStyle Width="60px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="活動狀態">
                                    <ItemTemplate>
                                        <asp:Literal ID="ls" runat="server" EnableViewState="false" />
                                    </ItemTemplate>
                                    <ItemStyle Width="60px" />
                                </asp:TemplateField>
                                <asp:BoundField ReadOnly="True" DataField="BusinessHourOrderMinimum" HeaderText="成單門檻"
                                    DataFormatString="{0:N0}">
                                    <ItemStyle Wrap="False" Width="100px" />
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="售出數量">
                                    <ItemTemplate>
                                        <asp:Literal ID="oq" runat="server" EnableViewState="false" />
                                    </ItemTemplate>
                                    <ItemStyle Wrap="False" Width="70px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="目前金額">
                                    <ItemTemplate>
                                        <asp:Literal ID="oa" runat="server" EnableViewState="false" />
                                    </ItemTemplate>
                                    <ItemStyle Width="90px" />
                                </asp:TemplateField>
                                <asp:BoundField ReadOnly="True" DataField="BusinessHourOrderTimeS" DataFormatString="{0:yyyy/MM/dd HH:mm}"
                                    HtmlEncode="False" HeaderText="活動開始時間">
                                    <ItemStyle Width="90px" />
                                </asp:BoundField>
                                <asp:BoundField ReadOnly="True" DataField="BusinessHourOrderTimeE" DataFormatString="{0:yyyy/MM/dd HH:mm}"
                                    HtmlEncode="False" HeaderText="活動結束時間">
                                    <ItemStyle Width="90px" />
                                </asp:BoundField>
                                <asp:TemplateField HeaderText="請款及交易明細">
                                    <ItemTemplate>
                                        <asp:Literal ID="ld" runat="server" EnableViewState="false" />
                                    </ItemTemplate>
                                    <ItemStyle Width="60px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="連鎖好康狀態">
                                    <ItemTemplate>
                                        <asp:Label ID="lbFPType" runat="server"></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="60px" />
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                        <uc1:Pager ID="pagerDeal" runat="server" OnGetCount="pagerDealGetCount" OnUpdate="pagerDealUpdate"
                            PageSize="20" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </ContentTemplate>
        </cc1:TabPanel>
        <cc1:TabPanel runat="server" HeaderText="商家圖檔設定" ID="TabPanelPhotoUpdate">
            <HeaderTemplate>
                商家圖檔設定
            </HeaderTemplate>
            <ContentTemplate>
                <div class="rlM">
                    <asp:UpdatePanel ID="upi" runat="server" RenderMode="Inline" UpdateMode="Conditional"
                        ChildrenAsTriggers="False">
                        <ContentTemplate>
                            <cc1:ReorderList ID="rli" runat="server" PostBackOnReorder="true" CallbackCssStyle="cbStyle"
                                DragHandleAlignment="Left" ItemInsertLocation="Beginning" DataKeyField="Value"
                                SortOrderField="Priority" LayoutType="Table" AllowReorder="true" OnItemReorder="RL_Item_Reorder"
                                OnDeleteCommand="RL_Delete_Command">
                                <ItemTemplate>
                                    <div class="rIA">
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:Image ID="i" runat="server" ImageUrl='<%#Eval("Text")%>' />
                                                </td>
                                                <td>
                                                    <asp:Literal ID="l" runat="server" Text='<%# System.IO.Path.GetFileName(Eval("Text").ToString())%>'></asp:Literal>
                                                </td>
                                                <td>
                                                    <asp:LinkButton ID="ld" runat="server" Text="delete" CommandName="Delete" />
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </ItemTemplate>
                                <ReorderTemplate>
                                    <asp:Panel ID="rlrp" CssClass="rCue" runat="server" />
                                </ReorderTemplate>
                                <DragHandleTemplate>
                                    <div class="rlDH">
                                    </div>
                                </DragHandleTemplate>
                            </cc1:ReorderList>
                            <asp:HiddenField ID="hi" runat="server" />
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="rli" EventName="ItemReorder" />
                            <asp:AsyncPostBackTrigger ControlID="rli" EventName="DeleteCommand" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
                <table width="100%">
                    <tr>
                        <td>上傳照片
                            <asp:FileUpload ID="photoUpload" runat="server" />
                            *jpeg&amp;gif
                        </td>
                        <td>
                            <asp:Button ID="bIS" runat="server" OnClick="bIS_Save" Text="Save" />
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </cc1:TabPanel>
        <cc1:TabPanel runat="server" HeaderText="商家資訊" ID="TabPanelSellerDetail">
            <HeaderTemplate>
                商家資訊
            </HeaderTemplate>
            <ContentTemplate>
                商家描述：<asp:Button ID="btn_ImportIntroduct" runat="server" Text="載入最新" OnClick="ClickImportIntroduct" />
                <br />
                <asp:TextBox ID="tbIntroduct" runat="server" TextMode="MultiLine" Rows="10" Width="700px"></asp:TextBox>
                <br />
                <fieldset style="display:none">
                    <table style="width: 100%">
                        <tr>
                            <td style="border: thin dotted gray; vertical-align: top">
                                <asp:Repeater ID="ri" runat="server">
                                    <HeaderTemplate>
                                        <div style="background-color: LightBlue">
                                            雜誌圖檔
                                        </div>
                                        <ul id="images">
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <li>
                                            <asp:Image ID="i" runat="server" ImageUrl='<%# Eval("ImgPath") %>' ImageAlign="Middle" /><input
                                                type="hidden" name="is" value='<%#Eval("Index")%>' />
                                            <a href="#" class="dI">X</a><hr />
                                        </li>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </ul>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </td>
                        </tr>
                    </table>
                    <br />
                    上傳圖片:
                    <asp:FileUpload ID="fuI" runat="server" />
                </fieldset>
                <asp:Button ID="tbSellerDetail" runat="server" EnableTheming="True" TabIndex="-1"
                    Text="儲存" OnClick="tbSellerDetail_Click" />
            </ContentTemplate>
        </cc1:TabPanel>
        <cc1:TabPanel runat="server" HeaderText="品生活設定" ID="TabPanelHighDealSetup">
            <ContentTemplate>
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <table width="98%">
                            <tr>
                                <td align="left">
                                    <a href='../piinlife/setup.aspx?sid=<%=SellerGuid%>'>新增品生活檔次</a>
                                </td>
                            </tr>
                        </table>
                        <br />
                        <uc2:SellerHiDealList ID="SellerHiDealList1" runat="server" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </ContentTemplate>
        </cc1:TabPanel>
        <cc1:TabPanel runat="server" HeaderText="優惠券設定" ID="TabPanelVourcherEventSetup">
            <ContentTemplate>
                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                    <ContentTemplate>
                        <table width="98%">
                            <tr>
                                <td align="left">
                                    <a href='../event/VourcherEventEdit.aspx?sid=<%= SellerID %>&mode=newadd' target="_blank">新增優惠券</a><br />
                                    <a href='../event/VourcherEventEdit.aspx?sid=<%= SellerID %>&mode=search' target="_blank">查看目前優惠券</a>
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </ContentTemplate>
        </cc1:TabPanel>
        <cc1:TabPanel runat="server" HeaderText="購物車" ID="tabShoppingCart" Visible="false">
            <ContentTemplate>
                <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                    <ContentTemplate>
                        <table width="98%" >
                            <tr>
                                <td align="left">
                                    <asp:Repeater ID="rptShoppingCart" runat="server">
                                        <HeaderTemplate>
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="tablecontent" id="tblShoppingCart">
                                                <thead>
                                                    <tr class="rd-C-Hide">                                                        
                                                        <th class="OrderDate">序號</th>
                                                        <th class="OrderDate">狀態</th>
                                                        <th class="OrderDate">購物車名稱</th>
                                                        <th class="OrderDate">免運門檻</th>
                                                        <th class="OrderDate">未滿門檻收取運費</th>
                                                    </tr>
                                                </thead>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr class="mc-tableContentITEM">
                                                <td style="text-align: left;">
                                                    <%# ((ShoppingCartManageList)Container.DataItem).UniqueId %>
                                                </td>
                                                <td style="text-align: left">
                                                    <span style="display: none">
                                                        <asp:Label ID="litId" runat="server" Text="<%# ((ShoppingCartManageList)Container.DataItem).Id %>"></asp:Label>
                                                    </span>
                                                    <%# ((ShoppingCartManageList)Container.DataItem).FreightsStatusName %>
                                                </td>                                                
                                                <td style="text-align: left;">
                                                    <%# ((ShoppingCartManageList)Container.DataItem).FreightsName %>
                                                </td>
                                                <td style="text-align: center;">
                                                    <%# ((ShoppingCartManageList)Container.DataItem).NoFreightLimit %>
                                                </td>
                                                <td style="text-align: center;">
                                                    <%# ((ShoppingCartManageList)Container.DataItem).Freights %>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </table>
                                        </FooterTemplate>
                                    </asp:Repeater>
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </ContentTemplate>
        </cc1:TabPanel>
    </cc1:TabContainer>
</asp:Content>

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ControlRoom/backend.master" AutoEventWireup="true"
    CodeBehind="seller_apply.aspx.cs" Inherits="LunchKingSite.Web.ControlRoom.seller_apply" %>

<%@ Import Namespace="LunchKingSite.Core" %>
<%@ Import Namespace="LunchKingSite.Web.Vourcher" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <script src="../../Tools/js/jquery-1.7.2.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () { $('.tablecontent tr:odd').addClass('oddtr'); });
    </script>
    <style type="text/css">
        fieldset a:link
        {
            font-weight: bolder;
            color: #cd5c5c;
            text-decoration: none;
        }
        fieldset a:visited
        {
            font-weight: bolder;
            color: #cd5c5c;
            text-decoration: none;
        }
        fieldset
        {
            background-color: #D8D8EB;
            border: 3px solid #778899;
            margin-top: 15px;
            margin-bottom: 10px;
            width: 1000px;
        }
        legend
        {
            background-color: #778899;
            color: White;
            font-weight: bold;
            border: 2px solid #c0c0c0;
            margin: 0px 5px 0px 5px;
            padding: 0px 10px 0px 10px;
        }
        .tablehead
        {
            text-align: center;
            background-color: #696969;
            font-weight: bold;
            border: 3px solid #f5f5f5;
            color: White;
        }
        .tablecontent
        {
            background-color: #f5f5f5;
            width: 100%;
        }
        .tablecontent td
        {
            padding-left: 10px;
            font-size: 15px;
        }
        .oddtr
        {
            background-color: #BEBEBE;
            display: table-row;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <fieldset>
        <legend>賣家分店申請修改</legend>
        <asp:Repeater ID="rpt_ApplyCase" runat="server">
            <HeaderTemplate>
                <table class="tablecontent">
                    <tr>
                        <td class="tablehead">
                            品牌名稱
                        </td>
                        <td class="tablehead">
                            類別
                        </td>
                        <td class="tablehead">
                            負責人
                        </td>
                        <td class="tablehead">
                            簽約公司名稱
                        </td>
                        <td class="tablehead">
                            統一編號
                        </td>
                        <td class="tablehead">
                            申請時間
                        </td>
                    </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td>
                        <asp:HyperLink ID="hyp_Edit" runat="server" Target="_blank" NavigateUrl='<%# ((ReturnCase)(Container.DataItem)).Type == ReturnCaseType.Seller?string.Format("~/ControlRoom/Seller/seller_add.aspx?sid={0}&thin=1",((ReturnCase)(Container.DataItem)).KeyGuid):
                            string.Format("~/ControlRoom/Seller/SellerStoreEdit.aspx?sid={0}&stGuid={1}&t=m", ((ReturnCase)(Container.DataItem)).SubKeyGuid,((ReturnCase)(Container.DataItem)).KeyGuid)%>'><%# ((ReturnCase)(Container.DataItem)).Name%></asp:HyperLink>
                    </td>
                    <td>
                        <%# ((ReturnCase)(Container.DataItem)).Type == ReturnCaseType.Seller ? "賣家" : "分店"%>
                    </td>
                    <td>
                        <%# ((ReturnCase)(Container.DataItem)).CompanyBossName%>
                    </td>
                    <td>
                        <%# ((ReturnCase)(Container.DataItem)).CompanyName%>
                    </td>
                    <td>
                        <%# ((ReturnCase)(Container.DataItem)).SignComapnyId%>
                    </td>
                    <td>
                        <%#((ReturnCase)(Container.DataItem)).ApplyTime.HasValue?((ReturnCase)(Container.DataItem)).ApplyTime.Value.ToString("MM/dd hh:mm"):string.Empty%>
                    </td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
    </fieldset>
    <fieldset>
        <legend>賣家分店退回申請</legend>
        <asp:Repeater ID="rpt_ReturnCase" runat="server">
            <HeaderTemplate>
                <table class="tablecontent">
                    <tr>
                        <td class="tablehead">
                            品牌名稱
                        </td>
                        <td class="tablehead">
                            類別
                        </td>
                        <td class="tablehead">
                            負責人
                        </td>
                        <td class="tablehead">
                            簽約公司名稱
                        </td>
                        <td class="tablehead">
                            統一編號
                        </td>
                        <td class="tablehead">
                            退件時間
                        </td>
                        <td class="tablehead">
                            退件訊息
                        </td>
                    </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td>
                        <asp:HyperLink ID="hyp_Edit" runat="server" Target="_blank" NavigateUrl='<%# ((ReturnCase)(Container.DataItem)).Type == ReturnCaseType.Seller?string.Format("~/ControlRoom/Seller/seller_add.aspx?sid={0}&thin=1",((ReturnCase)(Container.DataItem)).KeyGuid):
                            string.Format("~/ControlRoom/Seller/SellerStoreEdit.aspx?sid={0}&stGuid={1}&t=m", ((ReturnCase)(Container.DataItem)).SubKeyGuid,((ReturnCase)(Container.DataItem)).KeyGuid)%>'><%# ((ReturnCase)(Container.DataItem)).Name%></asp:HyperLink>
                    </td>
                    <td>
                        <%# ((ReturnCase)(Container.DataItem)).Type == ReturnCaseType.Seller ? "賣家" : "分店"%>
                    </td>
                    <td>
                        <%# ((ReturnCase)(Container.DataItem)).CompanyBossName%>
                    </td>
                    <td>
                        <%# ((ReturnCase)(Container.DataItem)).CompanyName%>
                    </td>
                    <td>
                        <%# ((ReturnCase)(Container.DataItem)).SignComapnyId%>
                    </td>
                    <td>
                        <%#((ReturnCase)(Container.DataItem)).ReturnTime.HasValue ? ((ReturnCase)(Container.DataItem)).ReturnTime.Value.ToString("MM/dd hh:mm") : string.Empty%>
                    </td>
                    <td>
                        <%# ((ReturnCase)(Container.DataItem)).Message%>
                    </td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
    </fieldset>
</asp:Content>

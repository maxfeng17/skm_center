﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.UI;
using LunchKingSite.WebLib;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using City = LunchKingSite.DataOrm.City;

namespace LunchKingSite.Web.ControlRoom
{
    public partial class SellerStoreEdit : RolePage, ISellerStoreEditView
    {
        public ISysConfProvider Config = ProviderFactory.Instance().GetConfig();

        public event EventHandler SaveSellerStore;

        public event EventHandler CancelEdit;

        public event EventHandler ReturnApply;

        public Guid SellerGuid
        {
            get
            {
                if (!string.IsNullOrEmpty(Request["sid"]))
                {
                    return new Guid(Request.QueryString["sid"]);
                }
                else
                {
                    return Guid.Empty;
                }
            }
        }

        public Guid StoreGuid
        {
            get
            {
                if (!string.IsNullOrEmpty(Request["stGuid"]))
                {
                    return new Guid(Request.QueryString["stGuid"]);
                }
                else
                {
                    return Guid.Empty;
                }
            }
        }

        public string UserName
        {
            get { return User.Identity.Name; }
        }

        public SellerStoreEditViewWorkType WorkType
        {
            get
            {
                SellerStoreEditViewWorkType rtnType = SellerStoreEditViewWorkType.Add;
                if (!string.IsNullOrEmpty(Request["t"]))
                {
                    if (Request["t"].ToString() == "a")
                    {
                        rtnType = SellerStoreEditViewWorkType.Add;
                    }
                    else if (Request["t"].ToString() == "m")
                    {
                        rtnType = SellerStoreEditViewWorkType.Modify;
                    }
                }
                return rtnType;
            }
        }

        public List<int> MRTLocationCategory
        {
            get { return new JsonSerializer().Deserialize<List<int>>(hiddenMRTCategoryChecked.Value); }
            set { hiddenMRTCategoryChecked.Value = new JsonSerializer().Serialize(value); }
        }
        public List<KeyValuePair<string, int>> MRTLocationChecked
        {
            get { return new JsonSerializer().Deserialize<List<KeyValuePair<string, int>>>(hiddenMRTCategoryItem.Value); }
            set { hiddenMRTCategoryItem.Value = new JsonSerializer().Serialize(value); }
        }


        private SellerStoreEditPresenter _presenter;

        public SellerStoreEditPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get { return this._presenter; }
        }

        /// <summary>
        /// 賣家名稱
        /// </summary>
        public string SellerName
        {
            set { hlStoreAdd.Text = value; }
        }

        /// <summary>
        /// 分店名
        /// </summary>
        public string BranchName
        {
            get
            {
                string branchname = tbBranchName.Text.Replace("《", string.Empty).Replace("》", string.Empty).Trim();
                return "《" + branchname + "》";
            }
            set { tbBranchName.Text = value; }
        }

        /// <summary>
        /// 電話
        /// </summary>
        public string StoreTel
        {
            get { return tbStoreTel.Text.Trim(); }
            set { tbStoreTel.Text = value; }
        }

        private int addressCityId = -1;

        /// <summary>
        /// 地址的城市ID
        /// </summary>
        public int AddressCityId
        {
            get
            {
                if ((Request["selCity"] == null) || (Request["selCity"] == ""))
                {
                    return addressCityId;
                }
                else
                {
                    return int.Parse(Request["selCity"]);
                }
            }
            set { addressCityId = value; }
        }

        private int? addressTownshipId = null;

        /// <summary>
        /// 地址的鄉鎮市區ID
        /// </summary>
        public int? AddressTownshipId
        {
            get
            {
                if ((Request["selTownship"] == null) || (Request["selTownship"] == ""))
                {
                    return addressTownshipId;
                }
                else
                {
                    return int.Parse(Request["selTownship"]);
                }
            }
            set { addressTownshipId = value; }
        }

        /// <summary>
        /// 地址
        /// </summary>
        public string StoreAddress
        {
            get { return tbStoreAddress.Text.Trim(); }
            set { tbStoreAddress.Text = value; }
        }

        /// <summary>
        /// 經緯度
        /// </summary>
        public KeyValuePair<string, string> LongitudeLatitude
        {
            get { return new KeyValuePair<string, string>(txtLatitude.Text, txtLongitude.Text); }
            set
            {
                txtLatitude.Text = value.Key;
                txtLongitude.Text = value.Value;
            }
        }

        /// <summary>
        /// 營業時間
        /// </summary>
        public string BusinessHour
        {
            get { return HttpUtility.HtmlDecode(tbBusinessHour.Text.Trim()); }
            set { tbBusinessHour.Text = value; }
        }

        /// <summary>
        /// 公休日
        /// </summary>
        public string CloseDateInformation
        {
            get { return tbCloseDate.Text.Trim(); }
            set { tbCloseDate.Text = value; }
        }

        /// <summary>
        /// 捷運
        /// </summary>
        public string TrafficMrt
        {
            get { return tbMrt.Text.Trim(); }
            set { tbMrt.Text = value; }
        }

        /// <summary>
        /// 開車
        /// </summary>
        public string TrafficCar
        {
            get { return tbCar.Text.Trim(); }
            set { tbCar.Text = value; }
        }

        /// <summary>
        /// 公車
        /// </summary>
        public string TrafficBus
        {
            get { return tbBus.Text.Trim(); }
            set { tbBus.Text = value; }
        }

        /// <summary>
        /// 其他交通方是
        /// </summary>
        public string TrafficOther
        {
            get { return tbOV.Text.Trim(); }
            set { tbOV.Text = value; }
        }

        /// <summary>
        /// 官網網址
        /// </summary>
        public string WebUrl
        {
            get
            {
                if (!string.IsNullOrWhiteSpace(tbWebUrl.Text))
                {
                    if (tbWebUrl.Text.StartsWith("http://"))
                    {
                        return tbWebUrl.Text.Trim();
                    }
                    else
                    {
                        return "http://" + tbWebUrl.Text.Trim();
                    }
                }
                return tbWebUrl.Text;
            }
            set { tbWebUrl.Text = value; }
        }

        /// <summary>
        /// FaceBook網址
        /// </summary>
        public string FaceBookUrl
        {
            get
            {
                if (!string.IsNullOrWhiteSpace(tbFBUrl.Text))
                {
                    if (tbFBUrl.Text.StartsWith("http://"))
                    {
                        return tbFBUrl.Text.Trim();
                    }
                    else
                    {
                        return "http://" + tbFBUrl.Text.Trim();
                    }
                }
                return tbFBUrl.Text;
            }
            set { tbFBUrl.Text = value; }
        }

        /// <summary>
        /// Plurk網址
        /// </summary>
        public string PlurkUrl
        {
            get
            {
                if (!string.IsNullOrWhiteSpace(tbPlurkUrl.Text))
                {
                    if (tbPlurkUrl.Text.StartsWith("http://"))
                    {
                        return tbPlurkUrl.Text.Trim();
                    }
                    else
                    {
                        return "http://" + tbPlurkUrl.Text.Trim();
                    }
                }
                return tbPlurkUrl.Text;
            }
            set { tbPlurkUrl.Text = value; }
        }

        /// <summary>
        /// 部落格網址
        /// </summary>
        public string BlogUrl
        {
            get
            {
                if (!string.IsNullOrWhiteSpace(tbBlogUrl.Text))
                {
                    if (tbBlogUrl.Text.StartsWith("http://"))
                    {
                        return tbBlogUrl.Text.Trim();
                    }
                    else
                    {
                        return "http://" + tbBlogUrl.Text.Trim();
                    }
                }
                return tbBlogUrl.Text;
            }
            set { tbBlogUrl.Text = value; }
        }

        /// <summary>
        /// tbOtherUrl
        /// </summary>
        public string OtherUrl
        {
            get
            {
                if (!string.IsNullOrWhiteSpace(tbOtherUrl.Text))
                {
                    if (tbOtherUrl.Text.StartsWith("http://"))
                    {
                        return tbOtherUrl.Text.Trim();
                    }
                    else
                    {
                        return "http://" + tbOtherUrl.Text.Trim();
                    }
                }
                return tbOtherUrl.Text;
            }
            set { tbOtherUrl.Text = value; }
        }

        /// <summary>
        /// 備註
        /// </summary>
        public string Remark
        {
            get { return tbRemark.Text.Trim(); }
            set { tbRemark.Text = value; }
        }

        public string CompanyName
        {
            get { return tbStoreName.Text.Trim(); }
            set { tbStoreName.Text = value; }
        }

        public string CompanyBossName
        {
            get { return tbStoreBossName.Text.Trim(); }
            set { tbStoreBossName.Text = value; }
        }

        public string CompanyID
        {
            get { return tbStoreID.Text.Trim(); }
            set { tbStoreID.Text = value; }
        }

        public string SignCompanyID
        {
            get { return tbSignStoreID.Text.Trim(); }
            set { tbSignStoreID.Text = value; }
        }

        public string AccountantName
        {
            get { return tbAccountantName.Text.Trim(); }
            set { tbAccountantName.Text = value; }
        }

        public string AccountantTel
        {
            get { return tbAccountantTel.Text.Trim(); }
            set { tbAccountantTel.Text = value; }
        }

        public string CompanyBankCode
        {
            get { return tbStoreBankCode.Text.Trim(); }
            set { tbStoreBankCode.Text = value; }
        }

        public string CompanyBranchCode
        {
            get { return tbStoreBranchCode.Text.Trim(); }
            set { tbStoreBranchCode.Text = value; }
        }

        public string CompanyAccount
        {
            get { return tbStoreAccount.Text.Trim(); }
            set { tbStoreAccount.Text = value; }
        }

        public string CompanyAccountName
        {
            get { return tbStoreAccountName.Text.Trim(); }
            set { tbStoreAccountName.Text = value; }
        }

        public string CompanyEmail
        {
            get { return tbStoreEmail.Text.Trim(); }
            set { tbStoreEmail.Text = value; }
        }

        public string CompanyNotice
        {
            get { return tbStoreNotice.Text.Trim(); }
            set { tbStoreNotice.Text = value; }
        }

        public string StoreRelationCode
        {
            get { return tbStoreRelationCode.Text.Trim(); }
            set { tbStoreRelationCode.Text = value; }
        }

        /// <summary>
        /// 後台訊息備份
        /// </summary>
        public string Message
        {
            get { return tbMessage.Text.Trim(); }
            set { tbMessage.Text = value; }
        }

        public bool IsOpenBooking
        {
            get
            {
                int openBookingSelected;
                if (int.TryParse(rdlIsOpenBooking.SelectedValue, out openBookingSelected))
                {
                    return (openBookingSelected == 1);
                }
                else
                {
                    return false;
                }
            }
            set
            {
                rdlIsOpenBooking.SelectedValue = (value) ? "1" : "0";

            }

        }

        public bool IsOpenReservationSetting
        {
            get
            {
                int reservationSelected;
                if (int.TryParse(rdlIsOpenReservationSetting.SelectedValue, out reservationSelected))
                {
                    return (reservationSelected == 1);
                }
                else
                {
                    return false;
                }
            }
            set
            {
                rdlIsOpenReservationSetting.SelectedValue = (value) ? "1" : "0";
            }

        }

        /// <summary>
        /// 狀態
        /// </summary>
        public StoreStatus Status
        {
            get
            {
                int selected;
                if (int.TryParse(rdlStatus.SelectedValue, out selected))
                {
                    return (StoreStatus)selected;
                }
                else
                {
                    return StoreStatus.Available;
                }
            }
            set
            {
                rdlStatus.SelectedValue = ((int)value).ToString();
            }
        }

        public bool IsCloseDown
        {
            get
            {
                return formIsClosed.Checked;
            }
            set
            {
                formIsClosed.Checked = value;
                formNotClosed.Checked = !value;
            }
        }

        public DateTime? CloseDownDate
        {
            get
            {
                DateTime dt = DateTime.MinValue;
                DateTime.TryParse(formCloseDownDate.Text, out dt);
                if (dt == DateTime.MinValue)
                {
                    return null;
                }
                else
                {
                    return dt;
                }
            }
            set
            {
                if (value.HasValue)
                {
                    formCloseDownDate.Text = value.Value.ToString("yyyy/MM/dd");
                }
            }
        }

        public bool CreditCardAvailable
        {
            get
            {
                return cbCreditCardAvailable.Checked;
            }
            set
            {
                cbCreditCardAvailable.Checked = value;
            }
        }

        public string CityJSonData { get; set; }

        public IAuditBoardView AuditBoardView
        {
            get
            {
                return ab;
            }
        }

        public void SetLocationDropDownList()
        {
            List<City> citys = CityManager.Citys.Where(x => x.Code != "SYS").ToList();
            citys.Insert(0, new City() { Id = -1, CityName = "請選擇" });
            repCity.DataSource = citys;
            repCity.DataBind();

            var townships = CityManager.TownShipGetListByCityId(addressCityId);
            if (addressCityId != -1)
            {
                var nowCitys = CityManager.Citys.Where(x => x.Id == addressCityId).ToList();
                if (nowCitys.Count > 0)
                {
                    townships = CityManager.TownShipGetListByCityId(nowCitys.First().Id);
                }
            }
            repTown.DataSource = townships;
            repTown.DataBind();

            var jsonCol = from x in CityManager.Citys.Where(x => x.Code != "SYS")
                          select new
                          {
                              id = x.Id.ToString(),
                              name = x.CityName,
                              Townships = (from y in CityManager.TownShipGetListByCityId(x.Id) select new { id = y.Id.ToString(), name = y.CityName })
                          };
            CityJSonData = new JsonSerializer().Serialize(jsonCol);
        }

        #region local property

        /// <summary>
        /// 分店異動的欄位名稱和用來顯示Literal
        /// </summary>
        public Dictionary<string, Literal> StoreChangeItemLiteral
        {
            get
            {
                Dictionary<string, Literal> change_items_literal = new Dictionary<string, Literal>();
                change_items_literal.Add(LunchKingSite.DataOrm.Store.StoreNameColumn.ColumnName, lit_Store_StoreName);
                change_items_literal.Add(LunchKingSite.DataOrm.Store.CompanyBossNameColumn.ColumnName, lit_Seller_CompanyBossName);
                change_items_literal.Add(LunchKingSite.DataOrm.Store.PhoneColumn.ColumnName, lit_Store_Phone);
                change_items_literal.Add(LunchKingSite.DataOrm.Store.AddressStringColumn.ColumnName, lit_Seller_Address);
                change_items_literal.Add(LunchKingSite.DataOrm.Store.OpenTimeColumn.ColumnName, lit_Store_OpenTime);
                change_items_literal.Add(LunchKingSite.DataOrm.Store.CloseDateColumn.ColumnName, lit_Store_CloseDate);
                change_items_literal.Add(LunchKingSite.DataOrm.Store.RemarksColumn.ColumnName, lit_Store_Remark);
                change_items_literal.Add(LunchKingSite.DataOrm.Store.MrtColumn.ColumnName, lit_Store_MRT);
                change_items_literal.Add(LunchKingSite.DataOrm.Store.CarColumn.ColumnName, lit_Store_Car);
                change_items_literal.Add(LunchKingSite.DataOrm.Store.BusColumn.ColumnName, lit_Store_Bus);
                change_items_literal.Add(LunchKingSite.DataOrm.Store.OtherVehiclesColumn.ColumnName, lit_Store_OtherVehicle);
                change_items_literal.Add(LunchKingSite.DataOrm.Store.WebUrlColumn.ColumnName, lit_Store_WebUrl);
                change_items_literal.Add(LunchKingSite.DataOrm.Store.FacebookUrlColumn.ColumnName, lit_Store_FaceBookUrl);
                change_items_literal.Add(LunchKingSite.DataOrm.Store.PlurkUrlColumn.ColumnName, lit_Store_PlurkUrl);
                change_items_literal.Add(LunchKingSite.DataOrm.Store.BlogUrlColumn.ColumnName, lit_Store_BlogUrl);
                change_items_literal.Add(LunchKingSite.DataOrm.Store.OtherUrlColumn.ColumnName, lit_Store_OtherUrl);
                change_items_literal.Add(LunchKingSite.DataOrm.Store.SignCompanyIDColumn.ColumnName, lit_Seller_CompanySignId);
                change_items_literal.Add(LunchKingSite.DataOrm.Store.MessageColumn.ColumnName, lit_Store_Message);
                change_items_literal.Add(LunchKingSite.DataOrm.Store.CompanyNameColumn.ColumnName, lit_Seller_ComapnyName);
                change_items_literal.Add(LunchKingSite.DataOrm.Store.CreditcardAvailableColumn.ColumnName, lit_Store_CreditCardAvailable);
                return change_items_literal;
            }
        }

        /// <summary>
        /// 賣家異動的欄位名稱和用來輸入的TextBox
        /// </summary>
        public Dictionary<string, TextBox> StoreChangeItemTextBox
        {
            get
            {
                Dictionary<string, TextBox> change_items_textbox = new Dictionary<string, TextBox>();
                change_items_textbox.Add(LunchKingSite.DataOrm.Store.StoreNameColumn.ColumnName, tbBranchName);
                change_items_textbox.Add(LunchKingSite.DataOrm.Store.CompanyBossNameColumn.ColumnName, tbStoreBossName);
                change_items_textbox.Add(LunchKingSite.DataOrm.Store.PhoneColumn.ColumnName, tbStoreTel);
                change_items_textbox.Add(LunchKingSite.DataOrm.Store.AddressStringColumn.ColumnName, tbStoreAddress);
                change_items_textbox.Add(LunchKingSite.DataOrm.Store.OpenTimeColumn.ColumnName, tbBusinessHour);
                change_items_textbox.Add(LunchKingSite.DataOrm.Store.CloseDateColumn.ColumnName, tbCloseDate);
                change_items_textbox.Add(LunchKingSite.DataOrm.Store.RemarksColumn.ColumnName, tbRemark);
                change_items_textbox.Add(LunchKingSite.DataOrm.Store.MrtColumn.ColumnName, tbMrt);
                change_items_textbox.Add(LunchKingSite.DataOrm.Store.CarColumn.ColumnName, tbCar);
                change_items_textbox.Add(LunchKingSite.DataOrm.Store.BusColumn.ColumnName, tbBus);
                change_items_textbox.Add(LunchKingSite.DataOrm.Store.OtherVehiclesColumn.ColumnName, tbOV);
                change_items_textbox.Add(LunchKingSite.DataOrm.Store.WebUrlColumn.ColumnName, tbWebUrl);
                change_items_textbox.Add(LunchKingSite.DataOrm.Store.FacebookUrlColumn.ColumnName, tbFBUrl);
                change_items_textbox.Add(LunchKingSite.DataOrm.Store.PlurkUrlColumn.ColumnName, tbPlurkUrl);
                change_items_textbox.Add(LunchKingSite.DataOrm.Store.BlogUrlColumn.ColumnName, tbBlogUrl);
                change_items_textbox.Add(LunchKingSite.DataOrm.Store.OtherUrlColumn.ColumnName, tbOtherUrl);
                change_items_textbox.Add(LunchKingSite.DataOrm.Store.SignCompanyIDColumn.ColumnName, tbSignStoreID);
                change_items_textbox.Add(LunchKingSite.DataOrm.Store.MessageColumn.ColumnName, tbMessage);
                change_items_textbox.Add(LunchKingSite.DataOrm.Store.CompanyNameColumn.ColumnName, tbStoreName);
                change_items_textbox.Add(LunchKingSite.DataOrm.Store.StoreRelationCodeColumn.ColumnName, tbStoreRelationCode);
                return change_items_textbox;
            }
        }

        #endregion local property

        #region

        protected void rptBStore_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is LunchKingSite.DataOrm.Store)
            {
                LunchKingSite.DataOrm.Store dataItem = (LunchKingSite.DataOrm.Store)e.Item.DataItem;
                Panel panStoreName = (Panel)e.Item.FindControl("panStoreName");
                CheckBox chkCheck = (CheckBox)e.Item.FindControl("chkStore");

                chkCheck.InputAttributes["value"] = dataItem.Guid.ToString();
            }
        }
        protected void rptCStore_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is LunchKingSite.DataOrm.Store)
            {
                LunchKingSite.DataOrm.Store dataItem = (LunchKingSite.DataOrm.Store)e.Item.DataItem;
                Panel panStoreName = (Panel)e.Item.FindControl("panStoreName");
                CheckBox chkCheck = (CheckBox)e.Item.FindControl("chkStore");

                chkCheck.InputAttributes["value"] = dataItem.Guid.ToString();
            }
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                SetWeekly();
                if (!StoreGuid.Equals(Guid.Empty))
                {
                    ab.ReferenceId = StoreGuid.ToString();
                }

                hlStoreAdd.NavigateUrl = string.Format("../seller/seller_add.aspx?sid={0}&&thin=1", SellerGuid);

                if (!CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.SetCloseDown))
                {
                    formNotClosed.Disabled = true;
                    formIsClosed.Disabled = true;
                    formCloseDownDate.Enabled = false;
                }
                if (!_presenter.OnViewInitialized())
                {
                    return;
                }

                syncBusinessHour.NavigateUrl = "#divBusinessHour";
                syncCloseDate.NavigateUrl = "#divCloseDate";
            }
            _presenter.OnViewLoaded();

            if (Config.EnableOpenStreetMap)
            {
                ljs.Text = @"<script type=""text/javascript"" src=""" + ResolveUrl("~/Tools/js/osm/OpenLayers.js") + @"""></script>";
            }
            else
            {
                if (Config.EnableGoogleMapApiV2)
                {
                    ljs.Text = @"<script src=""https://maps.google.com/maps?file=api&v=2&key=" + LocationFacade.GetGoogleMapsAPIKey() + @""" type=""text/javascript""></script>";
                }
                else
                {
                    ljs.Text = @"<script src=""https://maps.googleapis.com/maps/api/js?v=3&sensor=false&libraries=drawing"" type=""text/javascript""></script>";
                }
            }
        }


        protected void btnSave_Click(object sender, EventArgs e)
        {
            string msg = string.Empty;

            

            if (msg.Length > 0)
            {
                ShowMessage(msg);
            }
            else
            {
                if (SaveSellerStore != null)
                {
                    SaveSellerStore(this, null);
                }
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            if (CancelEdit != null)
            {
                CancelEdit(this, null);
            }
        }

        protected void btnReturnApply_Click(object sender, EventArgs e)
        {
            lab_Message.Text = string.Empty;
            if (string.IsNullOrEmpty(tbMessage.Text))
            {
                lab_Message.Text = "填入退回申請原因";
            }
            else if (ReturnApply != null)
            {
                ReturnApply(sender, e);
            }
        }

        /// <summary>
        /// 判斷json的欄位是否為null，回傳空字串或其值
        /// </summary>
        /// <param name="jtoken"></param>
        /// <returns></returns>
        private string CheckJTokenEmpty(JToken jtoken)
        {
            if (jtoken != null)
            {
                return jtoken.ToString().Replace("\"", string.Empty);
            }
            else
            {
                return string.Empty;
            }
        }

        #region interface method
        private void SetWeekly()
        {
            Dictionary<string, string> weekly = new Dictionary<string, string>();            
            weekly.Add("0", "週一");
            weekly.Add("1", "週二");
            weekly.Add("2", "週三");
            weekly.Add("3", "週四");
            weekly.Add("4", "週五");
            weekly.Add("5", "週六");
            weekly.Add("6", "週日");
            
            //營業時間
            //chk_BusinessWeekly.DataValueField = "Key";
            //chk_BusinessWeekly.DataTextField = "Value";
            //chk_BusinessWeekly.DataSource = weekly;
            //chk_BusinessWeekly.DataBind();

            ////公休日
            //chk_close_BusinessWeekly.DataValueField = "Key";
            //chk_close_BusinessWeekly.DataTextField = "Value";
            //chk_close_BusinessWeekly.DataSource = weekly;
            //chk_close_BusinessWeekly.DataBind();


            Dictionary<string, string> weeklyHour = new Dictionary<string, string>();
            for (int iHour = 1; iHour <= 24; iHour++)
            {
                weeklyHour.Add(iHour.ToString().PadLeft(2, '0'), iHour.ToString().PadLeft(2, '0'));
            }
            //營業時間
            ddl_WeeklyBeginHour.DataValueField = "Key";
            ddl_WeeklyBeginHour.DataTextField = "Value";
            ddl_WeeklyBeginHour.DataSource = weeklyHour;
            ddl_WeeklyBeginHour.DataBind();

            ddl_WeeklyEndHour.DataValueField = "Key";
            ddl_WeeklyEndHour.DataTextField = "Value";
            ddl_WeeklyEndHour.DataSource = weeklyHour;
            ddl_WeeklyEndHour.DataBind();

            //公休日
            ddl_close_WeeklyBeginHour.DataValueField = "Key";
            ddl_close_WeeklyBeginHour.DataTextField = "Value";
            ddl_close_WeeklyBeginHour.DataSource = weeklyHour;
            ddl_close_WeeklyBeginHour.DataBind();

            ddl_close_WeeklyEndHour.DataValueField = "Key";
            ddl_close_WeeklyEndHour.DataTextField = "Value";
            ddl_close_WeeklyEndHour.DataSource = weeklyHour;
            ddl_close_WeeklyEndHour.DataBind();

            Dictionary<string, string> weeklyMinute = new Dictionary<string, string>();
            for (int iMinute = 0; iMinute < 60; iMinute+=5)
            {
                weeklyMinute.Add(iMinute.ToString().PadLeft(2, '0'), iMinute.ToString().PadLeft(2, '0'));
            }
            //營業時間
            ddl_WeeklyBeginMinute.DataValueField = "Key";
            ddl_WeeklyBeginMinute.DataTextField = "Value";
            ddl_WeeklyBeginMinute.DataSource = weeklyMinute;
            ddl_WeeklyBeginMinute.DataBind();

            ddl_WeeklyEndMinute.DataValueField = "Key";
            ddl_WeeklyEndMinute.DataTextField = "Value";
            ddl_WeeklyEndMinute.DataSource = weeklyMinute;
            ddl_WeeklyEndMinute.DataBind();

            ddl_close_WeeklyBeginMinute.DataValueField = "Key";
            ddl_close_WeeklyBeginMinute.DataTextField = "Value";
            ddl_close_WeeklyBeginMinute.DataSource = weeklyMinute;
            ddl_close_WeeklyBeginMinute.DataBind();

            ddl_close_WeeklyEndMinute.DataValueField = "Key";
            ddl_close_WeeklyEndMinute.DataTextField = "Value";
            ddl_close_WeeklyEndMinute.DataSource = weeklyMinute;
            ddl_close_WeeklyEndMinute.DataBind();

            //公休時間
            Dictionary<int, string> flags = new Dictionary<int, string>();
            foreach (var item in Enum.GetValues(typeof(StoreCloseDate)))
            {
                flags[(int)item] = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, (StoreCloseDate)item);
            }
            ddlCloseDate.DataSource = flags;
            ddlCloseDate.DataTextField = "Value";
            ddlCloseDate.DataValueField = "Key";
            ddlCloseDate.DataBind();


            
        }
        public void GoToSellerStoreList()
        {
            Response.Redirect(string.Format("seller_add.aspx?sid={0}&tc=10", SellerGuid));
        }

        public void ShowMessage(string message)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('" + message + "');", true);
        }

        public void SetStore(LunchKingSite.DataOrm.Store store, LunchKingSite.DataOrm.ChangeLogCollection changelogs, LunchKingSite.DataOrm.StoreCollection sts)
        {
            btnReturnApply.Enabled = store.TempStatus == (int)SellerTempStatus.Applied;
            lab_NewCreated.Visible = store.NewCreated;
            rpt_ChangeLog.DataSource = changelogs;
            rpt_ChangeLog.DataBind();
            string changeitems = changelogs.Where(x => x.Enabled).OrderByDescending(x => x.Id).Select(x => x.Content).DefaultIfEmpty(string.Empty).First();
            if (WorkType == SellerStoreEditViewWorkType.Modify && !string.IsNullOrEmpty(changeitems) && store.TempStatus != (int)SellerTempStatus.Returned)
            {
                btnReturnApply.Enabled = true;
                JObject json = JObject.Parse(changeitems);
                //將異動資料填入textbox，原資料填入literal
                foreach (var item in StoreChangeItemTextBox)
                {
                    if (!string.IsNullOrEmpty(CheckJTokenEmpty(json[item.Key])))
                    {
                        StoreChangeItemLiteral[item.Key].Text = string.IsNullOrEmpty(item.Value.Text) ? "空字串" : item.Value.Text;
                        item.Value.Text = CheckJTokenEmpty(json[item.Key]);
                    }
                }
                int city_id;
                //無法枝節處理的欄位(城市區域、銀行分行代碼和刷卡服務)
                string city_id_jtoken = CheckJTokenEmpty(json[LunchKingSite.DataOrm.Store.TownshipIdColumn.ColumnName]);
                if (int.TryParse(city_id_jtoken, out city_id) && CityManager.TownShipGetById(city_id) != null)
                {
                    if (store.TownshipId != null)
                    {
                        lit_Seller_CityId.Text = CityManager.CityGetById(CityManager.TownShipGetById(store.TownshipId ?? 0).ParentId ?? 0).CityName + CityManager.TownShipGetById(store.TownshipId ?? 0).CityName;
                    }
                    else
                    {
                        lit_Seller_CityId.Text = "空字串";
                    }

                    AddressCityId = CityManager.TownShipGetById(city_id).ParentId ?? 0;
                    addressTownshipId = city_id;
                }
                bool creditcard_available;
                string creditcard_available_string = CheckJTokenEmpty(json[LunchKingSite.DataOrm.Store.CreditcardAvailableColumn.ColumnName]);
                if (!string.IsNullOrEmpty(creditcard_available_string))
                {
                    lit_Store_CreditCardAvailable.Text = store.CreditcardAvailable.ToString();
                    cbCreditCardAvailable.Checked = bool.TryParse(creditcard_available_string, out creditcard_available) ? creditcard_available : false;
                }
            }

            //所有分店
            rptBStore.DataSource = sts;
            rptBStore.DataBind();

            rptCStore.DataSource = sts;
            rptCStore.DataBind();


            //cookie還是有問題,先改回session觀察看看
            Session["TempStore"] = store;
        }

        #endregion

        #region webmethods

        [WebMethod]
        public static KeyValuePair<string, string> CalculateCoordinates(int townshipId, string address)
        {
            return LocationFacade.GetArea(CityManager.CityTownShopStringGet(townshipId) + address);
        }

        [WebMethod]
        public static IEnumerable<dynamic> CalculateNearMRTLocation(double latitude, double longitude)
        {
            return LocationFacade.StoreNearMRTLocation(latitude, longitude);
        }

        [WebMethod]
        public static string GetTownships(string oid)
        {
            try
            {
                var jsonCol = from x in CityManager.TownShipGetListByCityId(int.Parse(oid)) select new { id = x.Id.ToString(), name = x.CityName };

                return new JsonSerializer().Serialize(jsonCol);
            }
            catch (Exception e)
            {
                WebUtility.LogExceptionAnyway(e);
            }

            return null;
        }
        [WebMethod]
        public static string GetWeeklyName(string frenquency,string weeklys, string beginTime, string endTime, bool allDay)
        {
            return SellerFacade.GetWeeklyNames(frenquency, weeklys, beginTime, endTime, allDay);
        }

        [WebMethod]
        public static string ChecekAccount(
            string tbStoreBankCode,
            string tbStoreBranchCode,
            string tbStoreAccountName,
            string tbStoreEmail,
            string tbStoreNotice,
            string tbStoreAccount,
            string tbStoreID,
            string tbSignStoreID
            )
        {
            string msg = "";
            //以下這段檢查只適用於匯款資料為空值時, 其它欄位檢查請勿放進來, 以免被Died誤導
            if (!(
            string.IsNullOrWhiteSpace(tbStoreBankCode) &&
            string.IsNullOrWhiteSpace(tbStoreBranchCode) &&
            string.IsNullOrWhiteSpace(tbStoreAccountName) &&
            string.IsNullOrWhiteSpace(tbStoreEmail) &&
            string.IsNullOrWhiteSpace(tbStoreNotice) &&
            string.IsNullOrWhiteSpace(tbStoreAccount) &&
            string.IsNullOrWhiteSpace(tbStoreID)))
            {
                Regex r_id = new Regex(@"([A-Z]|[a-z])\d{9}");
                Regex r_foreignId = new Regex(@"([A-Z]|[a-z]){2}\d{8}"); //外籍人士的編號 規則是前兩碼英文後八碼數字
                Regex r_comid = new Regex(@"\d{8}");
                Regex r_account = new Regex(@"\d{14}");
                Regex r_bank = new Regex(@"\d{3}");
                Regex r_branch = new Regex(@"\d{4}");

                if (tbSignStoreID.Trim().Length == 8 && !r_comid.IsMatch(tbSignStoreID.Trim()))
                {
                    msg = @"簽約公司統一編號格式錯誤";
                }

                if (tbSignStoreID.Trim().Length == 10 && !(r_id.IsMatch(tbSignStoreID.Trim()) || r_foreignId.IsMatch(tbSignStoreID.Trim())))
                {
                    msg = @"簽約公司身分證格式錯誤";
                }

                if (tbSignStoreID.Trim().Length != 10 && tbSignStoreID.Trim().Length != 8)
                {
                    msg = @"簽約公司ID/統一編號格式錯誤";
                }

                if (tbStoreID.Trim().Length == 8 && !r_comid.IsMatch(tbStoreID.Trim()))
                {
                    msg = @"受款人統一編號格式錯誤";
                }
                else if (tbStoreID.Trim().Length == 10 && !(r_id.IsMatch(tbStoreID.Trim()) || r_foreignId.IsMatch(tbStoreID.Trim())))
                {
                    msg = @"受款人身分證格式錯誤";
                }
                else if (tbStoreID.Trim().Length != 10 && tbStoreID.Trim().Length != 8)
                {
                    msg = @"受款人ID/統一編號格式錯誤";
                }

                if (tbStoreAccount.Trim().Length > 14)
                {
                    msg = @"帳號長度大於14碼";
                }
                else
                {
                    if (!tbStoreAccount.Equals(""))
                    {
                        tbStoreAccount = "00000000000000".Substring(0, 14 - tbStoreAccount.Length) + tbStoreAccount;
                    }

                    if (tbStoreAccount.Trim().Length != 14 || !r_account.IsMatch(tbStoreAccount.Trim()))
                    {
                        msg = @"帳號格式錯誤";
                    }
                }

                if (tbStoreBankCode.Trim().Length != 3 || !r_bank.IsMatch(tbStoreBankCode.Trim()))
                {
                    msg = @"銀行代碼格式錯誤";
                }

                if (tbStoreBranchCode.Trim().Length != 4 || !r_branch.IsMatch(tbStoreBranchCode.Trim()))
                {
                    msg = @"分行代碼格式錯誤";
                }
            }

            return msg;
        }

        [WebMethod]
        public static bool SyncBusinessHour(string storeGuid, string opentime)
        {
            List<Guid> stGuid = new JsonSerializer().Deserialize<List<Guid>>(storeGuid);
            return SellerFacade.StoreOpenTimeUpdate(stGuid, opentime);
        }
        [WebMethod]
        public static bool SyncCloseDate(string storeGuid, string closedate)
        {
            List<Guid> stGuid = new JsonSerializer().Deserialize<List<Guid>>(storeGuid);
            return SellerFacade.StoreCloseDateUpdate(stGuid, closedate);
        }
        #endregion

       

    }
}
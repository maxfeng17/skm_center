﻿using System;
using LunchKingSite.DataOrm;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using System.Linq;
using System.Web.Services;
using System.Web.UI.WebControls;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;
using LunchKingSite.BizLogic.Facade;
using System.Collections.Generic;
using LunchKingSite.Core.UI;

namespace LunchKingSite.Web.ControlRoom.Service
{
    public partial class CustomerCategory : RolePage, ICustomerCategoryView
    {
        #region props
        private static LunchKingSite.Core.IServiceProvider sp = ProviderFactory.Instance().GetProvider<LunchKingSite.Core.IServiceProvider>();

        private CustomerCategoryPresenter _presenter;


        public CustomerCategoryPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        public int PageSize
        {
            get { return ucPager.PageSize; }
            set { ucPager.PageSize = value; }
        }
        public int CurrentPage
        {
            get
            {
                return ucPager.CurrentPage;
            }
        }


        public int UserId
        {
            get
            {
                var userName = User.Identity.Name;

                return MemberFacade.GetUniqueId(userName);
            }
        }

        public string Mode
        {
            get { return mode.Value; }
        }

        public bool IsShowFrontEnd
        {
            get
            {
                var result = Panel3.Controls.OfType<RadioButton>().SingleOrDefault(x => x.GroupName == "isFrontEnd" && x.Checked);
                switch (result.ID)
                {
                    case "rbNotFrontEnd":
                        return false;
                    case "rbFrontEnd":
                        return true;
                    default:
                        return true;
                }
            }
        }

        public bool IsNeedOrderId
        {
            get
            {
                var result = Panel3.Controls.OfType<RadioButton>().SingleOrDefault(x => x.GroupName == "needOrderId" && x.Checked);
                switch (result.ID)
                {
                    case "rbNotOrderId":
                        return false;
                    case "rbOrderId":
                        return true;
                    default:
                        return true;
                }
            }
        }
        public string CategoryName
        {
            get { return txtCategory.Text; }
        }
        public string SubjectContent
        {
            get { return subjectContent.Value; }
        }
        public int MainCategoryId
        {
            get
            {
                int categoryId = 0;
                int.TryParse(mainCategoryId.Value, out categoryId);
                return categoryId;
            }
        }








        #endregion props
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                IntialControls();
                Presenter.OnViewInitialized();
            }
            userId.Value = UserId.ToString();
            _presenter.OnViewLoaded();

        }

        protected void IntialControls()
        {
            rbFrontEnd.Checked = true;
            rbOrderId.Checked = true;
            txtCategory.Text = "";
            subcategory.InnerText = "";
            subjectContent.Value = "";
        }

        #region event
        public event EventHandler AddCustomerCategory;
        public event EventHandler<DataEventArgs<int>> DeleteCustomerCategory;
        public event EventHandler<DataEventArgs<int>> GetCustomerCategoryCount;
        public event EventHandler<DataEventArgs<int>> CategoryPageChanged;
        #endregion
        protected void btnAddCategory(object sender, EventArgs e)
        {
            if (this.AddCustomerCategory != null)
            {
                this.AddCustomerCategory(this, e);
            }
            IntialControls();
            ucPager.ResolvePagerView(CurrentPage, true);
        }

        protected void category_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.DataItem is CustomerServiceCategory)
            {
                CustomerServiceCategory item = (CustomerServiceCategory)e.Row.DataItem;
                Label lblCategory = ((Label)e.Row.FindControl("lblCategory"));
                Label lblFrontEnd = ((Label)e.Row.FindControl("lblFrontEnd"));
                Label lblOrderId = ((Label)e.Row.FindControl("lblOrderId"));
                Label lblCreateDate = ((Label)e.Row.FindControl("lblCreateDate"));
                LinkButton lbtnEdit = ((LinkButton)e.Row.FindControl("lbtnEdit"));
                LinkButton lbtnDelete = ((LinkButton)e.Row.FindControl("lbtnDelete"));

                lblCategory.Text = item.CategoryName;
                lblFrontEnd.Text = item.IsShowFrontEnd == true ? "是" : "否";
                lblOrderId.Text = item.IsNeedOrderId == true ? "是" : "否";
                lblCreateDate.Text = item.ModifyTime==null?item.CreateTime.ToString("yyyy/MM/dd HH:mm:ss"): item.ModifyTime.Value.ToString("yyyy/MM/dd HH:mm:ss");
                lbtnEdit.Text = "編輯";
                lbtnEdit.CommandArgument = item.CategoryId.ToString();
                lbtnDelete.Text = "刪除";
                lbtnDelete.CommandArgument = item.CategoryId.ToString();
            }
        }

        protected void category_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "DEL":

                    if (this.DeleteCustomerCategory != null)
                    {
                        int CommandArgument = 0;
                        int.TryParse(e.CommandArgument.ToString(), out CommandArgument);
                        this.DeleteCustomerCategory(this, new DataEventArgs<int>(CommandArgument));
                    }
                    ucPager.ResolvePagerView(CurrentPage, true);
                    break;

                default:
                    break;
            }
        }

        protected void CategoryUpdateHandler(int pageNumber)
        {
            if (this.CategoryPageChanged != null)
            {
                this.CategoryPageChanged(this, new DataEventArgs<int>(pageNumber));
            }
        }
        protected int GetCategoryCount()
        {
            DataEventArgs<int> e = new DataEventArgs<int>(0);
            if (GetCustomerCategoryCount != null)
            {
                GetCustomerCategoryCount(this, e);
            }
            return e.Data;
        }

        public void CategoryList(CustomerServiceCategoryCollection categoryList)
        {
            divCategory.Visible = true;
            gridCategory.DataSource = categoryList;
            gridCategory.DataBind();
        }

        /// <summary>
        /// 取得問題子分類(主因)
        /// </summary>
        /// <param name="categoryid"></param>
        /// <returns></returns>
        [WebMethod]
        public static string GetCategoryList(int categoryid)
        {
            var category = sp.CustomerServiceCategoryGet(categoryid);
            var subcategory = sp.GetCategoryListByParentId(category.CategoryId);
            CategoryListModel entity = new CategoryListModel();
            entity.categoryId = category.CategoryId;
            entity.categoryName = category.CategoryName;
            entity.isFrontEnd = category.IsShowFrontEnd==true?"1":"0";
            entity.needOrderId = category.IsNeedOrderId == true ? "1" : "0";
            entity.subCategoryList = new List<SubCategoryModel>();
            foreach (var item in subcategory)
            {
                entity.subCategoryList.Add(new SubCategoryModel{
                    subName = item.CategoryName,
                    categoryId=item.CategoryId
                });

            }
        
            return new JsonSerializer().Serialize(entity);
        }

        [WebMethod]
        public static void DeleteSubCategory(int categoryId,int userId)
        {
            CustomerServiceCategory csc = sp.CustomerServiceCategoryGet(categoryId);
            if (csc.IsLoaded)
            {
                CustomerServiceCategory cscMain = sp.CustomerServiceCategoryGet(csc.ParentCategoryId.Value);
                cscMain.ModifyId = userId;
                cscMain.ModifyTime = DateTime.Now;
                sp.CustomerServiceCategorySet(cscMain);
            }
            sp.CustomerServiceCategoryDeleteByCategoryId(categoryId);

            //多砍Sample
            var cscs = sp.CustomerServiceCategorySampleGetByCategoryId(categoryId);
            if (cscs.Count() > 0)
            {
                foreach (var item in cscs)
                {
                    sp.CustomerServiceCategorySampleDelete(item);
                }
            }
        }


        public class CategoryListModel
        {
            public int categoryId { get; set; }
            public string categoryName { get; set; }
            public string isFrontEnd { get; set; }
            public string needOrderId { get; set; }

            public List<SubCategoryModel> subCategoryList { get; set; }
            
        }
        public class SubCategoryModel
        {
            public string subName { get; set; }
            public int categoryId { get; set; }
        }




    }
}
﻿using System;
using LunchKingSite.DataOrm;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using System.Linq;
using System.Web.Services;
using System.Web.UI.WebControls;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;
using LunchKingSite.BizLogic.Facade;
using System.Collections.Generic;
using LunchKingSite.Core.UI;
using LunchKingSite.Core.Enumeration;
using System.Web;
using System.Drawing;
using System.Text.RegularExpressions;
using System.IO;
using LunchKingSite.BizLogic.Model.Refund;
using LunchKingSite.WebLib.Models.Service;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Models.CustomerService;

namespace LunchKingSite.Web.ControlRoom.Service
{
    public partial class ServiceDetail : RolePage, IServiceDetailView
    {
        #region props
        private static LunchKingSite.Core.IServiceProvider sp = ProviderFactory.Instance().GetProvider<LunchKingSite.Core.IServiceProvider>();
        protected static IOrderProvider op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
        protected static IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        protected static IHumanProvider hp = ProviderFactory.Instance().GetProvider<IHumanProvider>();
        protected static ISysConfProvider conf = ProviderFactory.Instance().GetConfig();
        protected static IPponProvider _pp = ProviderFactory.Instance().GetProvider<IPponProvider>();


        private ServiceDetailPresenter _presenter;
        private const string departId = "C001";
        private const string subDepartId = "C002";
        private const string depRoleName = "CustomerCare";


        public ServiceDetailPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        public int UserId
        {
            get
            {
                int userId = 0;
                userId = MemberFacade.GetUniqueId(User.Identity.Name);
                return userId;
            }
        }

        public bool IsCustomerCare
        {
            get
            {
                //檢查有沒有一線客服權限
                ViewEmployee ve = hp.ViewEmployeeByUserIdAndDept(UserId, departId);
                if (ve.IsLoaded)
                {
                    return true;
                }
                else
                {
                    return false;
                }


            }
        }

        public bool IsCustomerCare2
        {
            get
            {
                //檢查有沒有二線客服權限
                ViewEmployee ve = hp.ViewEmployeeByUserIdAndDept(UserId, subDepartId);
                if (ve.IsLoaded)
                {
                    return true;
                }
                else
                {
                    return false;
                }


            }
        }

        public string ServiceNo
        {
            get
            {
                string serviceNo = string.Empty;
                if (HttpContext.Current.Request["serviceNo"] != "")
                {
                    return HttpContext.Current.Request["serviceNo"];
                }
                else
                {
                    return lblServiceNo.Text;
                }
            }
        }

        public string UserData
        {
            get
            {
                CustomerServiceMessage csm = sp.CustomerServiceMessageGet(ServiceNo);
                if (csm.IsLoaded)
                {
                    if (csm.UserId != null)
                    {
                        Member mem = mp.MemberGetbyUniqueId(csm.UserId.Value);
                        if (mem.IsLoaded)
                        {
                            return mem.UserName;
                        }
                        else
                        {
                            return string.Empty;
                        }
                    }
                    else
                    {
                        return string.Empty;
                    }
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        public int Priority
        {

            get
            {
                var result = serviceNoPanel.Controls.OfType<RadioButton>().SingleOrDefault(x => x.GroupName == "questoinLevel" && x.Checked);
                switch (result.ID)
                {
                    case "rbNormal":
                        return 0;
                    case "rbQick":
                        return 1;
                    case "rbFast":
                        return 2;
                    default:
                        return 0;
                }
            }

        }

        public int Status
        {

            get
            {
                var result = serviceNoPanel.Controls.OfType<RadioButton>().SingleOrDefault(x => x.GroupName == "questionDeal" && x.Checked);
                switch (result.ID)
                {
                    case "rbprocess":
                        return 1;
                    case "rbtransfer":
                        return 2;
                    case "rbcompelete":
                        return 4;
                    default:
                        return 1;
                }
            }

        }

        public int MainCategory
        {
            get
            {
                int categoryId = 0;
                int.TryParse(ddCategory.SelectedValue, out categoryId);
                return categoryId;
            }
        }

        public int SubCategory
        {
            get
            {
                int subCategoryId = 0;
                int.TryParse(hiddenSubCategory.Value, out subCategoryId);
                return subCategoryId;
            }
        }

        public string Content
        {
            get
            {
                return serviceContent.InnerText;

            }
        }

        public string ImageUrl
        {
            get
            {
                return txtImageUrl.FileName;
            }
        }
        public FileUpload Image
        {
            get
            {
                return txtImageUrl;
            }
        }


        public string CustomerTxtArea
        {
            get
            {
                return customerTxtArea.InnerText;
            }
        }



        public string ContactTarget
        {
            get
            {
                return ddlsearch.SelectedValue;
            }
        }

        public string alertMessage
        {
            set { AjaxControlToolkit.ToolkitScriptManager.RegisterStartupScript(this, this.GetType(), string.Empty, "alert('" + value + "')", true); }
        }








        #endregion props
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                if (conf.OrderDetailV2LinkVisable)
                {
                    memPanel.Visible = false;
                }
                Presenter.OnViewInitialized();
            }

            _presenter.OnViewLoaded();
        }


        #region event
        public event EventHandler<DataEventArgs<string>> Save;
        public event EventHandler Search;
        public event EventHandler Claim;
        public event EventHandler<DataEventArgs<int>> Join;
        public event EventHandler<DataEventArgs<int>> Notify;
        #endregion

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            if (this.Search != null)
            {
                this.Search(this, e);
            }
        }

        protected void btnClaim_Click(object sender, EventArgs e)
        {
            if (this.Claim != null)
            {
                this.Claim(this, e);
            }
        }

        protected void gvLoginType_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                MemberLink ml = (MemberLink)e.Row.DataItem;
                Label lblExternalOrg = (Label)e.Row.FindControl("lblExternalOrg");
                if ((SingleSignOnSource)ml.ExternalOrg == SingleSignOnSource.ContactDigitalIntegration)
                {
                    lblExternalOrg.Text = "Email";
                }
                else if ((SingleSignOnSource)ml.ExternalOrg == SingleSignOnSource.Mobile17Life)
                {
                    lblExternalOrg.Text = "手機";
                }
                else
                {
                    lblExternalOrg.Text = ((SingleSignOnSource)ml.ExternalOrg).ToString();
                }
            }
        }

        protected void note_Bound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is ViewCustomerServiceInsideLog)
            {
                ViewCustomerServiceInsideLog item = (ViewCustomerServiceInsideLog)e.Item.DataItem;

                Label lblName = (Label)e.Item.FindControl("lblName");
                Label lblStatus = (Label)e.Item.FindControl("lblStatus");
                Label lblInsideMemo = (Label)e.Item.FindControl("lblInsideMemo");
                Label lblCreateTime = (Label)e.Item.FindControl("lblCreateTime");
                Label lblContent = (Label)e.Item.FindControl("lblContent");
                Label lblDe = (Label)e.Item.FindControl("lblDe");
                HyperLink hyFile = (HyperLink)e.Item.FindControl("hyFile");
                lblStatus.Text = Helper.GetEnumDescription((statusConvert)item.Status);
                lblInsideMemo.Text = item.Memo;
                lblCreateTime.Text = item.CreateTime.ToString("yyyy-MM-dd HH:mm");
                if (item.Status == (int)statusConvert.transferback)
                {
                    var vcsi = sp.GetViewCustomerServiceMessageByServiceNo(item.ServiceNo);
                    if (vcsi.IsLoaded)
                    {
                        lblName.Text = vcsi.SellerName;
                    }
                    else
                    {
                        lblName.Text = "";
                    }
                }
                else
                {
                    lblName.Text = item.Name;
                }
                if (!string.IsNullOrEmpty(item.Content))
                {
                    lblContent.Text = item.Content.Replace("\r\n", "<br>");
                }
                if (!string.IsNullOrEmpty(item.File))
                {
                    hyFile.Text = item.File;
                    hyFile.Target = "_blank";
                    hyFile.NavigateUrl = "/Images/Service/InSide/" + item.ServiceNo + "/" + item.File;
                    lblDe.Text = "查看附件";
                }
            }
        }

        protected void gvCustomer_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.DataItem is ViewCustomerServiceOutsideLog)
            {
                ViewCustomerServiceOutsideLog item = (ViewCustomerServiceOutsideLog)e.Row.DataItem;
                Label lblCustomerc = ((Label)e.Row.FindControl("lblCustomerc"));
                Label lblContentc = ((Label)e.Row.FindControl("lblContentc"));
                //Label lblMethodc = ((Label)e.Row.FindControl("lblMethodc"));
                Label lblDec = (Label)e.Row.FindControl("lblDec");
                HyperLink lblhyFile = (HyperLink)e.Row.FindControl("lblhyFile");
                Label lblCreateTimec = ((Label)e.Row.FindControl("lblCreateTimec"));

                lblCustomerc.Text = item.Name + " " + item.CreateName;
                if (!string.IsNullOrEmpty(item.Content))
                {
                    lblContentc.Text = item.Content.Replace("\r\n", "<br>");
                }
                //lblMethodc.Text = Helper.GetEnumDescription((methidConvert)item.Method);
                lblCreateTimec.Text = item.CreateTime.ToString("yyyy-MM-dd HH:mm");
                if (!string.IsNullOrEmpty(item.File))
                {
                    lblhyFile.Text = item.File;
                    lblhyFile.Target = "_blank";
                    lblhyFile.NavigateUrl = "/Images/Service/OutSide/" + item.ServiceNo + "/" + item.File;
                    lblDec.Text = "查看附件";
                }

                if (item.Method == (int)methodConvert.user)
                {
                    e.Row.BackColor = Color.LightGray;
                }
            }
        }

        protected void search_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.DataItem is ViewCustomerServiceList)
            {
                ViewCustomerServiceList item = (ViewCustomerServiceList)e.Row.DataItem;
                HyperLink hyServiceNo = ((HyperLink)e.Row.FindControl("hyServiceNo"));
                Label lblStatus = ((Label)e.Row.FindControl("lblStatus"));
                Label lblPriority = ((Label)e.Row.FindControl("lblPriority"));
                Label lblCreateDate = (Label)e.Row.FindControl("lblCreateDate");
                Label lblUserId = ((Label)e.Row.FindControl("lblUserId"));

                hyServiceNo.Text = item.ServiceNo;
                hyServiceNo.Target = "_blank";
                hyServiceNo.NavigateUrl = "ServiceDetail.aspx?serviceNo=" + item.ServiceNo;
                lblStatus.Text = Helper.GetEnumDescription((statusConvert)item.CustomerServiceStatus);
                lblPriority.Text = Helper.GetEnumDescription((priorityConvert)item.CasePriority);
                if (item.CasePriority == 2)
                {
                    lblPriority.ForeColor = Color.FromName("red");
                }
                lblCreateDate.Text = item.CreateTime.ToString("yyyy-MM-dd HH:mm");
                lblUserId.Text = item.ServicePeopleId.ToString();

            }
        }

        public void SetMemberHide()
        {
            memPanel.Visible = false;
        }
        public void SetMemberData(ViewMemberBuildingCityParentCity m, MobileMember mm)
        {
            memPanel.Visible = true;
            lbRegisteredMobile.Text = string.Empty;

            Member mem = mp.MemberGet(m.UniqueId);

            hkUserName.Text = mem.DisplayName;
            string account = m.UserName;
            if (RegExRules.CheckEmail(account))
            {
                lblEmail.Text = m.UserName;
            }
            else if (mm.IsLoaded)
            {
                lblEmail.Text = mm.MobileNumber;
            }
            else
            {
                lblEmail.Text = "";
            }

            if (mem.IsGuest)
            {
                litGuestMemberMark.Text = string.Format("[{0}]", Member._GUEST_MEMBER);
            }
            else
            {
                litGuestMemberMark.Text = "";
            }
            hkUserName.NavigateUrl = ResolveUrl("~/controlroom/User/users_edit.aspx?username=") + System.Web.HttpUtility.UrlEncode(m.UserName);
            lblMobile.Text = m.Mobile;
            lblUserEmail.Text = mem.UserEmail;
            if (mm.IsLoaded)
            {
                lbRegisteredMobile.Text = mm.MobileNumber;
            }

            hiddenMail.Value = mem.UserEmail;
            hiddenMobile.Value = m.Mobile;
            hiddenUserId.Value = m.UniqueId.ToString();
        }

        public void SetMemberAddress(string address)
        {
            lblAddress.Text = address;
        }

        public void SetMemberLinkList(MemberLinkCollection mlc)
        {
            gvLoginType.DataSource = mlc;
            gvLoginType.DataBind();
        }

        public void SetMemberBonus(ViewMemberPromotionTransactionCollection vmptc)
        {
            lblBCash.Text = (vmptc.Count > 0) ? vmptc[vmptc.Count - 1].RunSum.Value.ToString("N0") : "0";
        }

        public void SetMemberScash(decimal scash, decimal scashE7, decimal scashPez)
        {
            litScash.Text = scash.ToString("N0");
            litScashE7.Text =  scashE7.ToString("N0");
            litScashPez.Text =  scashPez.ToString("N0");
        }

        public void SetServiceDetail(ViewCustomerServiceList vscl, ViewCustomerServiceInsideLogCollection vcsi, ViewCustomerServiceOutsideLogCollection vsco)
        {
            //權益說明
            if (vscl.BusinessHourGuid.HasValue)
            {
                //找出母檔bid,取其權益說明Restrictions才正確
                Guid mainBid = ViewPponDealManager.DefaultManager.MainBusinessHourGuidGetByBid(vscl.BusinessHourGuid.Value);
                var deal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(mainBid);
                var contentLite = ViewPponDealManager.DefaultManager.GetPponContentLite(deal);
                if (deal.IsLoaded)
                {
                    if (deal.DeliveryType == (int)DeliveryType.ToHouse)
                    {
                        lblinterestStatement.Text = contentLite.Restrictions.Replace(
                            "成功x個工作日", "成功" + deal.Shippingdate + "個工作日");
                        bool _isKindDeal = Helper.IsFlagSet(deal.GroupOrderStatus ?? 0, GroupOrderStatus.KindDeal);
                        // 公益檔不顯示無法配送離島訊息
                        if (Helper.IsFlagSet(deal.BusinessHourStatus, BusinessHourStatus.NotDeliveryIslands) && !_isKindDeal)
                        {
                            lblinterestStatement.Text += "<br />●此宅配商品恕不適用離島、外島地區";
                        }

                        hyShip.Text = "出貨資訊";
                        hyShip.NavigateUrl = "javascript:Ship('" + vscl.OrderGuid + "');";
                        hyReturn.Text = "退貨紀錄";
                        hyReturn.NavigateUrl = "javascript:Return('" + vscl.OrderGuid + "');";
                        hyChange.Text = "換貨紀錄";
                        hyChange.NavigateUrl = "javascript:Change('" + vscl.OrderGuid + "');";
                        lblline1.Text = " | ";
                        lblline2.Text = " | ";
                    }
                    else
                    {
                        hyShip.Visible = false;
                        hyChange.Visible = false;
                        lblline1.Visible = false;
                        lblline2.Visible = false;

                        hyReturn.Text = "退貨紀錄";
                        hyReturn.NavigateUrl = "javascript:Return();";
                    }
                }

            }

            //訂購時間
            if (vscl.OrderGuid.HasValue)
            {
                lblOrderTime.Text = op.OrderGet(vscl.OrderGuid.Value).CreateTime.ToString("yyyy/MM/dd HH:mm:ss");
            }

            lblServiceNo.Text = vscl.ServiceNo;
            
            //接續單號
            if ( !string.IsNullOrEmpty(vscl.ParentServiceNo) && vscl.ServiceNo != vscl.ParentServiceNo)
            {
                lblContinue.Visible = true;
            }

            if (vscl.OrderId != null)
            {
                hyOrderId.Text = vscl.OrderId;
                hyOrderId.NavigateUrl = "~/controlroom/Order/order_detail.aspx?oid=" + vscl.OrderGuid.ToString(); ;
                hyOrderId.Target = "_blank";
                lblSupplier.Text = vscl.SellerName;
                var sme = hp.SellerMappingEmployeeGet(vscl.SellerGuid.Value);
                if (sme.IsLoaded)
                {
                    var emp = hp.ViewEmployeeGet(ViewEmployee.Columns.EmpId, sme.EmpId);
                    if (emp.IsLoaded)
                    {
                        lblService.Text = emp.EmpName;
                    }
                }
                hyOrderInfo.Text = "[" + vscl.UniqueId + "]" + vscl.ItemName;
                hyOrderInfo.NavigateUrl = conf.SSLSiteUrl + "/deal/" + vscl.BusinessHourGuid.ToString();
                hyOrderInfo.Target = "_blank";
                ImageBack.PostBackUrl = "~/controlroom/ppon/setup.aspx?bid=" + vscl.BusinessHourGuid.ToString();
                ImageBack.OnClientClick = "target='_blank'";
                lblOrderName.Text = vscl.MemberName;
                lblOrderMobile.Text = vscl.MobileNumber;
                lblOrderAddress.Text = vscl.DeliveryAddress;
                ddCategory.Enabled = false;
                lblIssueFromType.Text = Helper.GetEnumDescription((IssueFromType)vscl.IssueFromType);
                lblPChomeWarn.Visible = vscl.IssueFromType == (int)IssueFromType.PChome;
            }
            else
            {
                ImageBack.Visible = false;
                orderArea.Visible = false;
                orderDetail.Visible = false;
                interestDiv.Visible = false;
                lblIssueFromType.Text = Helper.GetEnumDescription((IssueFromType)vscl.IssueFromType);
            }

            if (string.IsNullOrEmpty(hiddenMail.Value))
            {
                hiddenMail.Value = vscl.Email;
            }

            if (string.IsNullOrEmpty(hiddenMobile.Value))
            {
                hiddenMobile.Value = vscl.Phone;
            }

            #region 下拉選單
            ddCategory.Items.Clear();
            ddCategory.Items.Add(new ListItem("請選擇", "0"));
            ddlCategory.Items.Clear();
            ddlCategory.Items.Add(new ListItem("請選擇", "0"));

            if (vscl.OrderId != null)
            {
                var entityCategory = sp.GetCustomerServiceCategory().Select(x => new { Text = x.CategoryName, Value = x.CategoryId.ToString() });

                foreach (var item in entityCategory)
                {
                    ddCategory.Items.Add(new ListItem(item.Text, item.Value));
                    ddlCategory.Items.Add(new ListItem(item.Text, item.Value));
                }
            }
            else
            {
                var entityCategory = sp.GetCustomerServiceCategory().Where(p => p.IsNeedOrderId == false).Select(x => new { Text = x.CategoryName, Value = x.CategoryId.ToString() });

                foreach (var item in entityCategory)
                {
                    ddCategory.Items.Add(new ListItem(item.Text, item.Value));
                    ddlCategory.Items.Add(new ListItem(item.Text, item.Value));
                }
            }
            ddCategory.SelectedValue = vscl.Category.ToString();
            ddlCategory.SelectedValue = vscl.Category.ToString();

            var entitySubCategory = sp.GetCustomerServiceCategoryListByCategoryId(vscl.Category).Select(x => new { Text = x.CategoryName, Value = x.CategoryId.ToString() });
            ddSubCategory.Items.Clear();
            ddSubCategory.Items.Add(new ListItem("請選擇", "0"));
            ddlSubCategory.Items.Clear();
            ddlSubCategory.Items.Add(new ListItem("請選擇", "0"));
            foreach (var item in entitySubCategory)
            {
                ddSubCategory.Items.Add(new ListItem(item.Text, item.Value));
                ddlSubCategory.Items.Add(new ListItem(item.Text, item.Value));
            }
            ddSubCategory.SelectedValue = vscl.SubCategory.ToString();
            ddlSubCategory.SelectedValue = vscl.SubCategory.ToString();
            hiddenSubCategory.Value = vscl.SubCategory.ToString();
            hiddenRealStatus.Value = vscl.CustomerServiceStatus.ToString();

            var entity = mp.ViewRoleMemberCollectionGet(depRoleName).Where(x => x.UserId != UserId);
            var serviceEmps = entity.Select(x => new KeyValuePair<string, string>(x.UserId.ToString(), x.Name)).Distinct().ToList();

            ddlSecWorkerList.Items.Clear();
            ddlSecWorkerList.Items.Add(new ListItem("請選擇", ""));
            foreach (var emp in serviceEmps)
            {
                ddlSecWorkerList.Items.Add(new ListItem(emp.Value, emp.Key));
            }

            #endregion

            #region radioButton
            switch ((priorityConvert)vscl.CasePriority)
            {
                case priorityConvert.normal:
                    rbNormal.Checked = true;
                    break;
                case priorityConvert.quick:
                    rbQick.Checked = true;
                    break;
                case priorityConvert.fast:
                    rbFast.Checked = true;
                    break;
                default:
                    break;
            }

            switch ((messageConvert)vscl.MessageType)
            {
                case messageConvert.online:
                    rbOnline.Checked = true;
                    break;
                case messageConvert.mobile:
                    rbMobile.Checked = true;
                    break;
                case messageConvert.mail:
                    rbMail.Checked = true;
                    break;
                default:
                    break;
            }

            if (vscl.CustomerServiceStatus != 0)
            {
                btnClaim.Visible = false;
                var member = mp.MemberGetbyUniqueId(vscl.ServicePeopleId.Value);
                if (member.IsLoaded)
                {
                    lblWork.Text = member.LastName + member.FirstName;
                }

                bool worker = vscl.ServicePeopleId == UserId;
                bool secWorker = vscl.SecServicePeopleId == UserId;
                bool hasSecWorker = vscl.SecServicePeopleId > 0;
                if (worker || secWorker)
                {
                    ddlSecWorkerList.Visible = btnJoin.Visible = worker && !hasSecWorker;
                    btnNotify.Visible = secWorker;
                    btnSecNotify.Visible = worker && hasSecWorker;
                }
                if (hasSecWorker)
                {
                    member = mp.MemberGetbyUniqueId(vscl.SecServicePeopleId.Value);
                    lblSecWork.Text = member.LastName + member.FirstName;
                }
            }
            else
            {
                btnClaim.Visible = true;
                lblWork.Text = "";
            }

            switch ((statusConvert)vscl.CustomerServiceStatus)
            {
                case statusConvert.wait:
                    rbprocess.Checked = true;
                    lblStatus.Text = Helper.GetEnumDescription((statusConvert)statusConvert.wait);
                    break;
                case statusConvert.process:
                    rbprocess.Checked = true;
                    lblStatus.Text = Helper.GetEnumDescription((statusConvert)statusConvert.process);
                    break;
                case statusConvert.transfer:
                    rbtransfer.Checked = true;
                    lblStatus.Text = Helper.GetEnumDescription((statusConvert)statusConvert.transfer);
                    break;
                case statusConvert.transferback:
                    rbtransfer.Checked = true;
                    lblStatus.Text = Helper.GetEnumDescription((statusConvert)statusConvert.transferback);
                    break;
                case statusConvert.complete:
                    rbcompelete.Checked = true;
                    lblStatus.Text = Helper.GetEnumDescription((statusConvert)statusConvert.complete);
                    break;
                default:
                    break;
            }
            #endregion



            rep_Note.DataSource = vcsi;
            rep_Note.DataBind();

            gridCustomer.DataSource = vsco;
            gridCustomer.DataBind();


            rbNoSms.Checked = true;


            #region 依據身分決定是否disabled
            if (IsCustomerCare2 && !IsCustomerCare)
            {
                rbNormal.Enabled = false;
                rbQick.Enabled = false;
                rbFast.Enabled = false;
            }
            #endregion

            var csi = sp.CustomerServiceInsideLogGet(vscl.ServiceNo);
            if (csi.Count() > 0)
            {
                foreach(var item in csi.Where(p => p.Status == (int)statusConvert.transferback && p.IsRead==0))
                {
                    item.IsRead = 1;
                    sp.CustomerServiceInsideLogSet(item);
                }
            }
        }

        public void MessageList(ViewCustomerServiceListCollection messageList)
        {
            contactPanel.Visible = true;
            gridContact.DataSource = messageList;
            gridContact.DataBind();
        }
        
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if (this.Save != null)
            {
                string path = Server.MapPath(conf.InSideUrl);
                if (Directory.Exists(path) == false)
                {
                    Directory.CreateDirectory(path);
                }
                bool fileOK = false;
                if (txtImageUrl.HasFile)
                {
                    string fileExtension = System.IO.Path.GetExtension(txtImageUrl.FileName).ToLower();
                    string[] allowedExtensions = { ".png", ".jpg" ,".gif"};
                    for (int i = 0; i < allowedExtensions.Length; i++)
                    {
                        if (fileExtension == allowedExtensions[i])
                        {
                            fileOK = true;
                        }

                    }

                    if (fileOK)
                    {
                        this.Save(this, new DataEventArgs<string>(path));
                    }
                }
                else
                {
                    this.Save(this, new DataEventArgs<string>(path));
                }
            }
            Response.Redirect(Request.Url.ToString());
            serviceContent.InnerText = "";
        }




        [WebMethod]
        public static string GetServiceMessageCategory(int categoryId)
        {
            return CustomerServiceFacade.GetServiceMessageCategory(categoryId);
        }

        [WebMethod]
        public static string GetSampleContent(int id)
        {
            string content = string.Empty;
            content = sp.CustomerServiceCategorySampleGet(id).Content;
            return new JsonSerializer().Serialize(content);
        }


        [WebMethod]
        public static string GetSample(int categoryId)
        {
            var sampleList = sp.CustomerServiceCategorySampleGetByCategoryId(categoryId);
            MainModel main = new MainModel();
            main.sampleList = new List<SampleModel>();
            foreach (var item in sampleList)
            {
                main.sampleList.Add(new SampleModel
                {
                    id = item.Id,
                    content = item.Content.Replace("\r\n", "<br>"),
                    subject = item.Subject

                });
            }
            return new JsonSerializer().Serialize(main);

        }

        /// <summary>
        /// 商品換貨紀錄(宅配)
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public static string GetChangeOrder(string orderGuid)
        {
            Guid orderG = Guid.Empty;
            Guid.TryParse(orderGuid, out orderG);
            ChangeOrder d = new ChangeOrder();
            var cols = op.OrderReturnListGetListByType(orderG, (int)OrderReturnType.Exchange)
                       .OrderByDescending(x => x.CreateTime)
                       .ToList();
            if (cols.Count > 0)
            {
                var changeOrder = cols.LastOrDefault();
                d.ChangeApplicationTime = changeOrder.CreateTime.ToString("yyyy/MM/dd");
                d.ChangeReason = changeOrder.Reason;

                if (changeOrder.VendorProcessTime.HasValue)
                    d.VendorProcessingTime = changeOrder.VendorProcessTime.Value.ToString("yyyy/MM/dd HH:mm");

                d.VendorProcessingProgress = Helper.GetLocalizedEnum(Resources.Localization.ResourceManager, (ExchangeVendorProgressStatus)changeOrder.VendorProgressStatus.Value);
                d.ClosedState = Helper.GetLocalizedEnum(Resources.Localization.ResourceManager, (OrderReturnStatus)changeOrder.Status) ?? "UnKnown";
            }
            else
            {
                d.Success = false;
            }

            return new JsonSerializer().Serialize(d);
        }

        /// <summary>
        /// 顯示出貨資訊(宅配)
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public static string GetShippingOrder(string orderGuid)
        {
            Guid orderG = Guid.Empty;
            Guid.TryParse(orderGuid, out orderG);
            ShippingOrderMethod d = new ShippingOrderMethod();
            ShippingOrder shippingOrder = new ShippingOrder();
            var cols = op.OrderShipGetListByOrderGuid(orderG);
            var orderProductDelivery = op.OrderProductDeliveryGetByOrderGuid(orderG);
            if (cols.Count > 0)
            {
                d.Count = cols.Count;

                foreach (var item in cols)
                {
                    shippingOrder = new ShippingOrder();

                    if (item.ShipTime.HasValue)
                        shippingOrder.ShippingDate = item.ShipTime.Value.ToString("yyyy/MM/dd");
                    shippingOrder.ShippingNo = item.ShipNo;

                    if (!string.IsNullOrEmpty(op.ShipCompanyGetList(item.ShipCompanyId).FirstOrDefault().ShipWebsite))
                    {
                        shippingOrder.LogisticsCompany = string.Format("<p><a target='_blank' style='cursor:pointer;color:blue' onclick='gotoShipUrl(this)'>{0}</a><span id='shipUrl' style='display:none'>{1}</span><span id='shipNo' style='display:none'>{2}</span></p>",
                            op.ShipCompanyGetList(item.ShipCompanyId).FirstOrDefault().ShipCompanyName,
                            op.ShipCompanyGetList(item.ShipCompanyId).FirstOrDefault().ShipWebsite,
                            item.ShipNo
                            );
                    }
                    else
                    {
                        shippingOrder.LogisticsCompany = op.ShipCompanyGetList(item.ShipCompanyId).FirstOrDefault().ShipCompanyName;
                    }

                    if (orderProductDelivery.ProductDeliveryType == (int)ProductDeliveryType.Wms)
                    {
                        shippingOrder.ShippingRemark = "-";
                        shippingOrder.LastModifyDate = "-";
                        shippingOrder.LastModifyAccount = "-";
                    }
                    else
                    {
                        shippingOrder.ShippingRemark = item.ShipMemo;
                        shippingOrder.LastModifyDate = item.ModifyTime.ToString("yyyy/MM/dd  HH:mm");
                        shippingOrder.LastModifyAccount = item.CreateId;
                    }

                    d.ShippingOrder.Add(shippingOrder);
                }
              
            }
            else
            {
                d.Success = false;
                d.Count = cols.Count;
            }


            return new JsonSerializer().Serialize(d);
        }

        /// <summary>
        /// 顯示退貨紀錄(憑證、宅配)
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public static string GetReturnOrder(string orderGuid)
        {
            Guid orderG = Guid.Empty;
            Guid.TryParse(orderGuid, out orderG);
            ReturnOrderModel d = new ReturnOrderModel();

            IList<ReturnFormEntity> returnForms = ReturnFormRepository.FindAllByOrder(orderG);
            if (returnForms.Count > 0)
            {
                ReturnFormEntity form = returnForms.Where(x => x.ProgressStatus.EqualsAny(ProgressStatus.Processing, ProgressStatus.AtmQueueing, ProgressStatus.AtmQueueSucceeded))
                    .OrderByDescending(x => x.VendorProcessTime).FirstOrDefault();
                if (form == null)
                {
                    form = returnForms.OrderByDescending(x => x.VendorProcessTime).FirstOrDefault();
                }

                d.ReturnApplicationTime = form.CreateTime.ToString("yyyy/MM/dd");
                d.DealName = form.GetReturnSpec().Replace(Environment.NewLine, "<br/>");
                d.ReturnReason = form.ReturnReason;
                d.VendorProcessingTime = form.VendorProcessTime.ToString("yyyy/MM/dd HH:mm:ss");

                #region 廠商處理進度
                string vendorStatusDesc = Helper.GetLocalizedEnum(Resources.Localization.ResourceManager, form.VendorProgressStatus) ?? "資料有問題, 請聯絡技術部";

                if (form.VendorProgressStatus == VendorProgressStatus.Unreturnable
                    || form.VendorProgressStatus == VendorProgressStatus.UnreturnableProcessed)
                {
                    vendorStatusDesc += ":<br/>" + form.VendorMemo;
                }
                else if (form.VendorProgressStatus == VendorProgressStatus.CompletedAndRetrievied)
                {
                    vendorStatusDesc += "<br/>" + form.GetCollectedSpec().Replace(Environment.NewLine, "<br/>");
                }
                d.VendorProcessingProgress = vendorStatusDesc;
                #endregion

                #region 退貨進度
                string litProgressDesc = OrderFacade.GetRefundStatus(form);

                string refundDesc;
                switch (form.RefundType)
                {
                    case RefundType.Scash:
                        refundDesc = "退購物金";
                        break;
                    case RefundType.Cash:
                    case RefundType.ScashToCash:
                        refundDesc = "刷退";
                        break;
                    case RefundType.Atm:
                    case RefundType.ScashToAtm:
                        refundDesc = "退ATM";
                        break;
                    case RefundType.Tcash:
                    case RefundType.ScashToTcash:
                        refundDesc = string.Format("退{0}", Helper.GetEnumDescription(form.ThirdPartyPaymentSystem));
                        break;
                    default:
                        refundDesc = "資料有問題, 請聯絡技術部";
                        break;
                }
                d.ReturnSchedule = string.Format("[{0}]{1}{2}", refundDesc, "<br/>", litProgressDesc);

                #endregion
                bool IsPaperAllowance = true;
                var returnForm = op.ReturnFormGetListByOrderGuid(orderG);
                if (returnForm.Count() > 0)
                {
                    if (returnForm.FirstOrDefault().CreditNoteType == (int)AllowanceStatus.ElecAllowance)
                    {
                        IsPaperAllowance = false;
                    }
                }
                d.ReturnComplete = form.GetReturnSpec().Replace(Environment.NewLine, "<br/>");
                d.DiscountDescription = form.CreditNoteType.Equals(0) ? OrderFacade.GetAllowanceDesc((form.ProgressStatus == ProgressStatus.Completed || form.ProgressStatus == ProgressStatus.Canceled) && !form.IsCreditNoteReceived && IsPaperAllowance ? null : (bool?)IsPaperAllowance) : OrderFacade.GetAllowanceDesc(form.CreditNoteType);
                d.ClosedDescription = OrderFacade.GetFinishStatusDescription(form);
            }
            else
            {
                d.Success = false;
            }
            return new JsonSerializer().Serialize(d);
        }

        protected void btnNotify_Click(object sender, EventArgs e)
        {
            if (this.Notify != null)
            {
                Notify(sender, new DataEventArgs<int>((int)statusConvert.ServiceNotify));
            }
        }

        protected void btnSecNotify_Click(object sender, EventArgs e)
        {
            if (this.Notify != null)
            {
                Notify(sender, new DataEventArgs<int>((int)statusConvert.secServiceNotify));
            }
        }

        protected void btnJoin_Click(object sender, EventArgs e)
        {
            int joinUserId = 0;
            if (this.Join != null && int.TryParse(ddlSecWorkerList.SelectedValue,out joinUserId))
            {
                Join(sender, new DataEventArgs<int>(joinUserId));
            }
        }
    }
}
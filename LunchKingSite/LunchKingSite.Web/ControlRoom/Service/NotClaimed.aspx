﻿<%@ Page Language="C#" MasterPageFile="~/ControlRoom/backend.master" AutoEventWireup="true" CodeBehind="NotClaimed.aspx.cs" Inherits="LunchKingSite.Web.ControlRoom.Service.NotClaimed" %>

<%@ Import Namespace="LunchKingSite.BizLogic.Component" %>
<%@ Import Namespace="LunchKingSite.Core" %>
<%@ Import Namespace="LunchKingSite.DataOrm" %>
<%@ Import Namespace="LunchKingSite.WebLib.Views" %>
<%@ Import Namespace="System" %>
<%@ Register Src="../Controls/Pager.ascx" TagName="Pager" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">

    <script type="text/javascript" src="../../Tools/js/jquery-1.7.2.min.js"></script>
    <script type="text/javascript" src="../../Tools/js/jquery-ui.1.8.min.js"></script>
    <script type="text/javascript" src="../../Tools/ckeditor/jquery.query-2.1.7.js"></script>
    <script type="text/javascript" src="../../Tools/js/jquery.maxlength-min.js"></script>
    <script type="text/javascript" src="../../Tools/js/jquery.ui.datepicker-zh-TW.js"></script>
    <script type="text/javascript" src="../../Tools/js/jquery.blockUI.js"></script>
    <link type="text/css" href="../../Tools/js/css/ui-lightness/jquery-ui-1.8rc3.custom.css" rel="stylesheet" />
    <link href="../../Themes/PCweb/css/ppon.css" type="text/css" rel="Stylesheet" />
    <link href="../../Themes/PCweb/plugin/font-awesome/font-awesome.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../Tools/ckeditor/ckeditor.js"></script>
    <script type="text/javascript" src="../../Tools/js/json2.js"></script>
    <script type="text/javascript" src="../../Tools/js/jquery.numeric.js"></script>
    <script type="text/javascript" src="../../Tools/js/jquery.checkboxtree.js"></script>
    <script type="text/javascript" src="//html2canvas.hertzen.com/build/html2canvas.js"></script>
    <link type="text/css" href="../../Tools/js/jquery.checkboxtree.css" rel="stylesheet" />
    <style type="text/css">
        .modalPopup {
            margin-top: -200px;
            margin-left: -200px;
        }

        .searchTable {
            padding: 10px 10px 10px 10px;
            letter-spacing:3px;
            line-height: 24px;
            width:800px;
            background-color:#EFEBEB;
        }
    </style>
    
    <script>
        var b_editor;
        function setupdatepicker(from, to) {
            var dates = $('#' + from + ', #' + to).datepicker({
                changeMonth: true,
                numberOfMonths: 3,
                onSelect: function (selectedDate) {
                    var option = this.id == from ? "minDate" : "maxDate";
                    var instance = $(this).data("datepicker");
                    var date = $.datepicker.parseDate(instance.settings.dateFormat || $.datepicker._defaults.dateFormat, selectedDate, instance.settings);
                    dates.not(this).datepicker("option", option, date);
                }
            });
        }
        $(document).ready(function () {
            $("#hiddenChose").val("");
            setupdatepicker('tbOS', 'tbOE');

            $("#clickAll").click(function () {
                if ($("#clickAll").prop("checked")) {
                    $("input[type=checkbox]").each(function () {
                        //$(this).prop("checked", true);
                        var linkLoc = $(this).prop("onclick");
                        if (linkLoc != null) {
                            if (!$(this).prop("checked")) {
                                $(this).click();
                            }
                        }

                    });
                } else {
                    $("input[type=checkbox]").each(function () {
                        $(this).prop("checked", false);
                        $("#hiddenChose").val("");
                    });
                }
            });
        });

        function chosebox(serviceNo) {
            if ($("#hiddenChose").val().indexOf(serviceNo) > 0) {
                $("#hiddenChose").val(($("#hiddenChose").val()).replace("," + serviceNo ,""));
            }
            else if ($("#hiddenChose").val().indexOf(serviceNo) == 0)
            {
                $("#hiddenChose").val(($("#hiddenChose").val()).replace(serviceNo + ",", "").replace(serviceNo, ""));
            }
            else {
                if ($("#hiddenChose").val() == "") {
                    $("#hiddenChose").val(serviceNo);
                }
                else {
                    $("#hiddenChose").val($("#hiddenChose").val() + "," + serviceNo);
                }
            }
          
        }

        function confirms() {
                       
            if ($("#hiddenChose").val() == "") {
                alert("請先選擇要領取的案件");
                return false;
            }
            else {
                if (confirm("確定要認領案件？")) {
                    return true;
                }
                else {
                    return false;
                }
            }
        }



    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <font size="5"><b>未認領案件</b></font>
    <br>
    <br>
    <asp:HiddenField ID="hiddenChose" ClientIDMode="Static" runat="server"></asp:HiddenField>
    <table class="searchTable">
        <tr>
            <td>
                問題分類：<asp:DropDownList runat="server" ID="ddlCategory" CssClass="adminitem">
                           </asp:DropDownList>
                訂單編號：<asp:TextBox ID="searchOrderId" runat="server" ClientIDMode="Static" />
                帳號：<asp:TextBox ID="searchMail" runat="server" ClientIDMode="Static" />
            </td>
        </tr>
        <tr>
            <td>
                案件編號：<asp:TextBox ID="searchServiceNo" runat="server" ClientIDMode="Static" />
                建立時間：<asp:TextBox ID="tbOS" CssClass="date" runat="server" ClientIDMode="Static" maxlength="10" size="10" placeholder=""/> ～ <asp:TextBox ID="tbOE" CssClass="date" runat="server" ClientIDMode="Static" maxlength="10" size="10" placeholder=""/>
            </td>
            <td>
                <asp:Button ID="btnSearch" runat="server" Text="查詢" OnClick="btnSearch_Click"/>
            </td>
        </tr>
    </table>
    <table width="800px">
        <tr>
            <td align="left">
                <input name="clickAll" id="clickAll" type="checkbox">全選
            </td>
            <td align="right">
                <asp:Button ID="btnClaim" runat="server" Text="確認認領" OnClientClick="return confirms();" OnClick="btnClaimed_Click" />
            </td>
        </tr>
    </table>
    <asp:Panel ID="divNotClaimed" runat="server" Visible="false">
        <asp:GridView ID="gridNotClaimed" runat="server" OnRowDataBound="notClaimed_RowDataBound"
            GridLines="Horizontal" ForeColor="Black" CellPadding="4" BorderWidth="1px" BorderStyle="None"
            BorderColor="#DEDFDE" BackColor="White" EmptyDataText="無符合的資料" AutoGenerateColumns="false"
            Font-Size="Small" Width="800px" >
            <FooterStyle BackColor="#CCCC99"></FooterStyle>
            <RowStyle BackColor="#F7F7DE" Height="20px" HorizontalAlign="Center"></RowStyle>
            <SelectedRowStyle BackColor="#CE5D5A" ForeColor="White" Font-Bold="True"></SelectedRowStyle>
            <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Center"></PagerStyle>
            <HeaderStyle BackColor="#6B696B" ForeColor="White" Font-Bold="True" Height="20px"
                HorizontalAlign="Center"></HeaderStyle>
            <Columns>
                <asp:TemplateField HeaderText="認領" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5%">
                    <ItemTemplate>
                        <asp:CheckBox ID="chbClaim" runat="server" onclick=<%# "chosebox('" + Eval("serviceNo") + "')"%> ></asp:CheckBox>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="案件編號" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="15%">
                    <ItemTemplate>
                        <asp:HyperLink ID="hpServiceNo" runat="server"></asp:HyperLink>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="案件來源" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="15%">
                    <ItemTemplate>
                        <asp:Label ID="lblMessageType" runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="來源平台" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="15%">
                    <ItemTemplate>
                        <asp:Label ID="lblSource" runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="姓名" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="10%">
                    <ItemTemplate>
                        <asp:HyperLink ID="hyName" runat="server"></asp:HyperLink>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="帳號" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="15%">
                    <ItemTemplate>
                        <asp:Label ID="lblEmail" runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="訂單編號" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="15%">
                    <ItemTemplate>
                        <asp:HyperLink  ID="hpOrderId" runat="server"></asp:HyperLink>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="問題分類" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="10%">
                    <ItemTemplate>
                        <asp:Label ID="lblCategory" runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="建立時間" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="20%">
                    <ItemTemplate>
                        <asp:Label ID="lblCreateDate" runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
        <uc1:Pager ID="ucPager" runat="server" PageSize="20" ongetcount="GetClaimedCount" onupdate="ClaimedUpdateHandler"></uc1:Pager>
    </asp:Panel>

</asp:Content>

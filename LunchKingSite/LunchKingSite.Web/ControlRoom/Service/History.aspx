﻿<%@ Page Language="C#" MasterPageFile="~/ControlRoom/backend.master" AutoEventWireup="true" CodeBehind="History.aspx.cs" Inherits="LunchKingSite.Web.ControlRoom.Service.History" %>

<%@ Import Namespace="LunchKingSite.BizLogic.Component" %>
<%@ Import Namespace="LunchKingSite.Core" %>
<%@ Import Namespace="LunchKingSite.DataOrm" %>
<%@ Import Namespace="LunchKingSite.WebLib.Views" %>
<%@ Import Namespace="System" %>
<%@ Register Src="../Controls/Pager.ascx" TagName="Pager" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">

    <script type="text/javascript" src="../../Tools/js/jquery-1.7.2.min.js"></script>
    <script type="text/javascript" src="../../Tools/js/jquery-ui.1.8.min.js"></script>
    <script type="text/javascript" src="../../Tools/ckeditor/jquery.query-2.1.7.js"></script>
    <script type="text/javascript" src="../../Tools/js/jquery.maxlength-min.js"></script>
    <script type="text/javascript" src="../../Tools/js/jquery.ui.datepicker-zh-TW.js"></script>
    <script type="text/javascript" src="../../Tools/js/jquery.blockUI.js"></script>
    <link type="text/css" href="../../Tools/js/css/ui-lightness/jquery-ui-1.8rc3.custom.css" rel="stylesheet" />
    <link href="../../Themes/PCweb/css/ppon.css" type="text/css" rel="Stylesheet" />
    <link href="../../Themes/PCweb/plugin/font-awesome/font-awesome.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../Tools/ckeditor/ckeditor.js"></script>
    <script type="text/javascript" src="../../Tools/js/json2.js"></script>
    <script type="text/javascript" src="../../Tools/js/jquery.numeric.js"></script>
    <script type="text/javascript" src="../../Tools/js/jquery.checkboxtree.js"></script>
    <script type="text/javascript" src="//html2canvas.hertzen.com/build/html2canvas.js"></script>
    <link type="text/css" href="../../Tools/js/jquery.checkboxtree.css" rel="stylesheet" />
    <style type="text/css">
        .modalPopup {
            margin-top: -200px;
            margin-left: -200px;
        }
        .searchTable {
            padding: 10px 10px 10px 10px;
            letter-spacing:2px;
            line-height: 24px;
            background-color:#EFEBEB;
            width: 1300px;
        }
        .table-width {
            width: 1300px;
        }
        .adminitem {
            width: 100px;
        }
    </style>

    <script type="text/javascript">
        var b_editor;
        function setupdatepicker(from, to) {
            var dates = $('#' + from + ', #' + to).datepicker({
                changeMonth: true,
                numberOfMonths: 1,
                onSelect: function (selectedDate) {
                    var option = this.id == from ? "minDate" : "maxDate";
                    var instance = $(this).data("datepicker");
                    var date = $.datepicker.parseDate(instance.settings.dateFormat || $.datepicker._defaults.dateFormat, selectedDate, instance.settings);
                    dates.not(this).datepicker("option", option, date);
                }
            });
        }
        $(document).ready(function () {
            setupdatepicker('tbOS', 'tbOE');
        });


        $(document).ready(function () {
            $("#hiddenChose").val("");
            setupdatepicker('tbOS', 'tbOE');

            $("#clickAll").click(function () {
                if ($("#clickAll").prop("checked")) {
                    $("input[type=checkbox]").each(function () {
                        //$(this).prop("checked", true);
                        var linkLoc = $(this).prop("onclick");
                        if (linkLoc != null) {
                            if (!$(this).prop("checked")) {
                                $(this).click();
                            }
                        }

                    });
                } else {
                    $("input[type=checkbox]").each(function () {
                        $(this).prop("checked", false);
                        $("#hiddenChose").val("");
                    });
                }
            });
        });

        function chosebox(serviceNo) {
            if ($("#hiddenChose").val().indexOf(serviceNo) > 0) {
                $("#hiddenChose").val(($("#hiddenChose").val()).replace("," + serviceNo, ""));
            }
            else if ($("#hiddenChose").val().indexOf(serviceNo) == 0) {
                $("#hiddenChose").val(($("#hiddenChose").val()).replace(serviceNo + ",", "").replace(serviceNo, ""));
            }
            else {
                if ($("#hiddenChose").val() == "") {
                    $("#hiddenChose").val(serviceNo);
                }
                else {
                    $("#hiddenChose").val($("#hiddenChose").val() + "," + serviceNo);
                }
            }

        }

        function confirms() {

            if ($("#hiddenChose").val() == "") {
                alert("請先選擇要領取的案件");
                return false;
            }
            else {
                if (confirm("確定要認領案件？")) {
                    return true;
                }
                else {
                    return false;
                }
            }
        }

        function getSubCategory() {
            var ddlMain = $("#<%=ddlCategory.ClientID%>");
            var ddlMainSelected = $("#<%=ddlCategory.ClientID%> :selected");
            var ddlSub = $("#<%=ddlSubCategory.ClientID%>");

            $.ajax({
                type: "POST",
                url: "History.aspx/GetCustomerServiceCategoryList",
                data: "{categoryId: " + ddlMain.val() + "}",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    var cates = $.parseJSON(data.d);
                    ddlSub.empty();
                    ddlSub.append($('<option selected="selected"></option>').val('-1').html('請選擇'));
                    $.each(cates, function (i, item) {
                        ddlSub.append($('<option></option>').val(item.CategoryId).html(item.CategoryName));
                    });
                },
                error: function () {
                    ddlSub.empty();
                    ddlSub.append($('<option selected="selected"></option>').val('-1').html('請選擇'));
                }
            });
        }
        function setSubCategory() {
            var subCategory = $('select[id*=ddlSubCategory] option:selected').val();
            $('input[id*=hiddenSubCategory]').val(subCategory);
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:HiddenField ID="hiddenSubCategory" ClientIDMode="Static" runat="server" />
    <asp:HiddenField ID="hiddenChose" ClientIDMode="Static" runat="server"></asp:HiddenField>
    <font size="5"><b>客服案件查詢</b></font>
    <br>
    <br>
        <asp:Panel ID="seachPanel" runat="server" Visible="true">
            <table class="searchTable">
                <tr>
                    <td colspan="2">案件編號：<asp:TextBox ID="searchSericeNo" runat="server" ClientIDMode="Static" />　
                        訂單編號：<asp:TextBox ID="searchOrderId" runat="server" ClientIDMode="Static" />　
                        帳號：<asp:TextBox ID="searchMail" runat="server" ClientIDMode="Static" />　
                        處理人員1：<asp:DropDownList runat="server" ID="ddlUserId" CssClass="adminitem" class="ddlUserId" ClientIDMode="Static"></asp:DropDownList>
                        處理人員2：<asp:DropDownList runat="server" ID="ddlSecUserId" CssClass="adminitem" class="ddlUserId" ClientIDMode="Static"></asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">問題等級：<asp:DropDownList runat="server" ID="ddlPriority" CssClass="adminitem">
                        <asp:ListItem Text="請選擇" Value="-1"></asp:ListItem>
                        <asp:ListItem Text="一般" Value="0"></asp:ListItem>
                        <asp:ListItem Text="急" Value="1"></asp:ListItem>
                        <asp:ListItem Text="特急" Value="2"></asp:ListItem>
                    </asp:DropDownList>　
                        問題分類：<asp:DropDownList runat="server" ID="ddlCategory" CssClass="adminitem" onchange="getSubCategory()" OnSelectedIndexChanged="ddlCategory_SelectedIndexChanged"></asp:DropDownList>
                        主因：<asp:DropDownList runat="server" ID="ddlSubCategory" onchange="setSubCategory()"></asp:DropDownList>　
                        案件來源：<asp:DropDownList runat="server" ID="ddlMessageType" CssClass="adminitem"></asp:DropDownList>　
                        裝置平台：<asp:DropDownList runat="server" ID="ddlSource" CssClass="adminitem"></asp:DropDownList>
                        檔號：<asp:TextBox ID="txtUniqueId" runat="server" ClientIDMode="Static" />
                    </td>
                </tr>
                <tr>
                    <td>
                        來源平台：<asp:DropDownList ID="ddlIssueFromType" CssClass="adminitem" runat="server"></asp:DropDownList>
                        <div style="display: none">台新帳號：<asp:TextBox ID="ddlTsMemberNo" CssClass="adminitem" runat="server" ></asp:TextBox></div>
                    </td>
                </tr>
                <tr>
                    <td>
                    案件狀態：<asp:DropDownList runat="server" ID="ddlStatus" CssClass="adminitem">
                        <asp:ListItem Text="全部" Value="-1"></asp:ListItem>
                        <asp:ListItem Text="未認領" Value="0"></asp:ListItem>
                        <asp:ListItem Text="處理中" Value="1"></asp:ListItem>
                        <asp:ListItem Text="轉單" Value="2"></asp:ListItem>
                        <asp:ListItem Text="轉單回覆" Value="3"></asp:ListItem>
                        <asp:ListItem Text="已結案" Value="4"></asp:ListItem>
                    </asp:DropDownList>　
                    供應商：<asp:TextBox ID="txtSupplier" runat="server" ClientIDMode="Static" MaxLength="30" size="15" placeholder="" />　
                        建立日期：<asp:TextBox ID="tbOS" CssClass="date" runat="server" ClientIDMode="Static" MaxLength="10" size="8" placeholder="" />
                        <asp:DropDownList ID="ddlHS" runat="server"></asp:DropDownList>:<asp:DropDownList ID="ddlMS" runat="server"></asp:DropDownList>～<asp:TextBox ID="tbOE" CssClass="date" runat="server" ClientIDMode="Static" MaxLength="10" size="8" placeholder="" />
                        <asp:DropDownList ID="ddlHE" runat="server"></asp:DropDownList>:<asp:DropDownList ID="ddlME" runat="server"></asp:DropDownList>
                        檔次類型：
                        <asp:DropDownList ID="ddlPponDealType" runat="server">
                            <asp:ListItem Text="全部" Value="-1"></asp:ListItem>
                            <asp:ListItem Text="憑證檔次" Value="0"></asp:ListItem>
                            <asp:ListItem Text="一般宅配" Value="1"></asp:ListItem>
                            <asp:ListItem Text="超取訂單" Value="2"></asp:ListItem>
                            <asp:ListItem Text="24H到貨" Value="3"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td width="135px">
                        <asp:Button ID="btnSearch" runat="server" Text="查詢" OnClick="btnSearch_Click" />
                        <asp:Button ID="btnExport" runat="server" Text="匯出Excel" OnClick="btnExport_Click" />
                    </td>
                </tr>
            </table>
            <br />
        </asp:Panel>

        <asp:Panel ID="chosePanel" runat="server" Visible="false">
        <table class="table-width">
            <tr>
                <td align="left">
                    <input name="clickAll" id="clickAll" type="checkbox">全選
                    <span><span style="color:red;padding-left:20px;">☎</span>:商家尚未回覆<span style="color:red;padding-left:20px;">↩</span>:商家回覆<span style="color:blue;padding-left:20px;">✉</span>:客服通知</span>
                </td>
                <td align="right">
                    <asp:Button ID="btnClaim" runat="server" Text="確認認領" OnClientClick="return confirms();" OnClick="btnClaimed_Click" />
                </td>
            </tr>
        </table>
        </asp:Panel>

        <asp:Panel ID="divHistory" runat="server" Visible="false">
            <asp:GridView ID="gridHistory" runat="server" OnRowDataBound="history_RowDataBound"
                GridLines="Horizontal" ForeColor="Black" CellPadding="4" BorderWidth="1px" BorderStyle="None"
                BorderColor="#DEDFDE" BackColor="White" EmptyDataText="無符合的資料" AutoGenerateColumns="false" 
                Font-Size="Small" class="table-width">
                <FooterStyle BackColor="#CCCC99"></FooterStyle>
                <RowStyle BackColor="#F7F7DE" Height="20px" HorizontalAlign="Center"></RowStyle>
                <SelectedRowStyle BackColor="#CE5D5A" ForeColor="White" Font-Bold="True"></SelectedRowStyle>
                <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Center"></PagerStyle>
                <HeaderStyle BackColor="#6B696B" ForeColor="White" Font-Bold="True" Height="20px"
                    HorizontalAlign="Center"></HeaderStyle>
                <Columns>
                    <asp:TemplateField HeaderText="認領" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
                        <ItemTemplate>
                            <asp:CheckBox ID="chbClaim" runat="server" Visible="false" onclick=<%# "chosebox('" + Eval("serviceNo") + "')"%> ></asp:CheckBox>
                            <asp:Label ID="lblDot" runat="server"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="案件編號" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="7%">
                        <ItemTemplate>
                            <asp:HyperLink ID="hpServiceNo" runat="server"></asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="案件狀態" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="6%">
                        <ItemTemplate>
                            <asp:Label ID="lblStatus" runat="server"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="問題等級" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
                        <ItemTemplate>
                            <asp:Label ID="lblPriority" runat="server"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="案件來源" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="6%">
                        <ItemTemplate>
                            <asp:Label ID="lblMessageType" runat="server"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="裝置平台" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
                        <ItemTemplate>
                            <asp:Label ID="lblSource" runat="server"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="來源平台" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
                        <ItemTemplate>
                            <asp:Label ID="lblIssueFromType" runat="server"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="台新帳號" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
                        <ItemTemplate>
                            <asp:Label ID="lblTsMemberNo" runat="server"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="姓名" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5%">
                        <ItemTemplate>
                            <asp:HyperLink ID="hyName" runat="server"></asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="帳號" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5%">
                        <ItemTemplate>
                            <asp:Label ID="lblAccount" runat="server"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="訂單編號" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="12%">
                        <ItemTemplate>
                            <asp:HyperLink ID="hpOrderId" runat="server"></asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="檔次類型" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="7%">
                        <ItemTemplate>
                             <asp:Label ID="lblPponDealType" runat="server"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="供應商" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="7%">
                        <ItemTemplate>
                            <asp:HyperLink ID="hpSupplier" runat="server"></asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="問題分類" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="8%">
                        <ItemTemplate>
                            <asp:Label ID="lblCategory" runat="server"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="主因" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="10%">
                        <ItemTemplate>
                            <asp:Label ID="lblSubject" runat="server"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="建立時間" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="10%">
                        <ItemTemplate>
                            <asp:Label ID="lblCreateDate" runat="server"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="最後異動時間" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="10%">
                        <ItemTemplate>
                            <asp:Label ID="lblModifyDate" runat="server"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="處理人員1" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="8%">
                        <ItemTemplate>
                            <asp:Label ID="lblWorkUser" runat="server"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                     <asp:TemplateField HeaderText="處理人員2" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="8%">
                        <ItemTemplate>
                            <asp:Label ID="lblSecWorkUser" runat="server"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
            <uc1:Pager ID="ucPager" runat="server" PageSize="20" ongetcount="GetHistoryCount" onupdate="HistoryUpdateHandler"></uc1:Pager>
        </asp:Panel>
</asp:Content>

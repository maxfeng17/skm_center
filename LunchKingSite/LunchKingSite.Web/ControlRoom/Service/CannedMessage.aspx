﻿<%@ Page Language="C#" MasterPageFile="~/ControlRoom/backend.master" AutoEventWireup="true" CodeBehind="CannedMessage.aspx.cs" Inherits="LunchKingSite.Web.ControlRoom.Service.CannedMessage" %>

<%@ Import Namespace="LunchKingSite.BizLogic.Component" %>
<%@ Import Namespace="LunchKingSite.Core" %>
<%@ Import Namespace="LunchKingSite.DataOrm" %>
<%@ Import Namespace="LunchKingSite.WebLib.Views" %>
<%@ Import Namespace="System" %>
<%@ Register Src="../Controls/Pager.ascx" TagName="Pager" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">

    <script type="text/javascript" src="../../Tools/js/jquery-1.7.2.min.js"></script>
    <script type="text/javascript" src="../../Tools/js/jquery-ui.1.8.min.js"></script>
    <script type="text/javascript" src="../../Tools/ckeditor/jquery.query-2.1.7.js"></script>
    <script type="text/javascript" src="../../Tools/js/jquery.maxlength-min.js"></script>
    <script type="text/javascript" src="../../Tools/js/jquery.ui.datepicker-zh-TW.js"></script>
    <script type="text/javascript" src="../../Tools/js/jquery.blockUI.js"></script>
    <link type="text/css" href="../../Tools/js/css/ui-lightness/jquery-ui-1.8rc3.custom.css" rel="stylesheet" />
    <link href="../../Themes/PCweb/css/ppon.css" type="text/css" rel="Stylesheet" />
    <link href="../../Themes/PCweb/plugin/font-awesome/font-awesome.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../Tools/ckeditor/ckeditor.js"></script>
    <script type="text/javascript" src="../../Tools/js/json2.js"></script>
    <script type="text/javascript" src="../../Tools/js/jquery.numeric.js"></script>
    <script type="text/javascript" src="../../Tools/js/jquery.checkboxtree.js"></script>
    <script type="text/javascript" src="//html2canvas.hertzen.com/build/html2canvas.js"></script>
    <link type="text/css" href="../../Tools/js/jquery.checkboxtree.css" rel="stylesheet" />
    <script type="text/javascript">
        google.load("jqueryui", "1.8");
    </script>
    <style type="text/css">
        .modalPopup {
            margin-top: -200px;
            margin-left: -200px;
        }
        .searchTable {
            padding: 10px 10px 10px 10px;
            letter-spacing:3px;
            line-height: 24px;
            width:1024px;
            background-color:#EFEBEB;
        }
    </style>

    <script>
        $(document).ready(function () {
            var $tabs = $('#tabs').tabs();
        });
        function buildSubCategory() {
            var ddlMain = $("#<%=ddlCategory.ClientID%>");
            var ddlMainSelected = $("#<%=ddlCategory.ClientID%> :selected");
            var ddlSub = $("#<%=ddlSubject.ClientID%>");

            $.ajax({
                type: "POST",
                url: "CannedMessage.aspx/GetServiceMessageCategory",
                data: "{categoryId: " + ddlMain.val() + "}",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    var cates = $.parseJSON(data.d);
                    ddlSub.empty();
                    ddlSub.append($('<option></option>').val('').html('請選擇'));
                    $.each(cates, function (i, item) {
                        ddlSub.append($('<option></option>').val(item.CategoryId).html(item.CategoryName));
                    });
                },
                error: function () {
                    //alert("error");
                    ddlSub.empty();
                    ddlSub.append($('<option></option>').val('').html('請選擇'));
                }
            });
        }

        function buildInSideSubCategory(categoryId) {
            var ddlMain = $("#<%=ddlInSideCategory.ClientID%>");
            var ddlMainSelected = $("#<%=ddlInSideCategory.ClientID%> :selected");
            var ddlSub = $("#<%=ddlInSideSubject.ClientID%>");
            if (ddlMain.val() == '') {
                ddlSub.empty();
                ddlSub.append($('<option></option>').val('').html('請選擇'));
            }
            else {
                $.ajax({
                    type: "POST",
                    url: "CannedMessage.aspx/GetServiceMessageCategory",
                    data: "{categoryId: " + ddlMain.val() + "}",
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        var cates = $.parseJSON(data.d);
                        ddlSub.empty();
                        ddlSub.append($('<option></option>').val('').html('請選擇'));
                        if (categoryId == "") {
                            $.each(cates, function (i, item) {
                                ddlSub.append($('<option></option>').val(item.CategoryId).html(item.CategoryName));
                            });
                        }
                        else {
                            $.each(cates, function (i, item) {
                                if (item.CategoryId != categoryId) {
                                    ddlSub.append($('<option></option>').val(item.CategoryId).html(item.CategoryName));
                                }
                                else {
                                    ddlSub.append($('<option selected></option>').val(item.CategoryId).html(item.CategoryName));
                                }
                            });
                        }
                    },
                    error: function () {
                        //alert("error");
                        ddlSub.empty();
                        ddlSub.append($('<option></option>').val('').html('請選擇'));
                    }
                });
            }
        }

        function getSubValue() {
            $("#outSideSubject").val($("#ddlSubject").val());
        }
        function getInSubValue() {
            $("#inSideSubject").val($("#ddlInSideSubject").val());
        }

        function searchCheck() {
            if ($("#ddlCategory").val() == "") {
                alert("請選擇問題類別");
                return false;
            }
            else if ($("#ddlSubject").val() == "") {
                alert("請選擇問題主因");
                return false;
            }
            else {
                return true;
            }
        }

        function addCheck() {
            if ($("#ddlInSideCategory").val() == "") {
                alert("請選擇問題類別");
                return false;
            }
            else if ($("#ddlInSideSubject").val() == "") {
                alert("請選擇問題主因");
                return false;
            }
            else {
                return true;
            }
        }

        function confirmDel() {
            return confirm("確定要刪除該筆訊息？");
        }

        function clear() {
            $("#txtSubject").val("");
            $("#ddlInSideCategory").val("");
            $("#outSideSubject").val("");
            $("#txtContent").val("");
            buildInSideSubCategory('');
        }

        function show() {
            $("#txtSubject").val("");
            $("#txtContent").val("");
            $("#ddlInSideCategory").val("");
            buildInSideSubCategory('');
            $find("mpe").show();
            $("#PopAddServiceMsg").show();
            $("#mode").val("Add");
           
            return false;
        }



        function edit(id) {
            
            $.ajax({
                type: "POST",
                url: "CannedMessage.aspx/GetSampleList",
                data: "{id: " + id + "}",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    var cates = $.parseJSON(data.d);
                    $("#txtSubject").val(cates.subject)
                    $("#txtContent").val(cates.content);
                    $("#ddlInSideCategory").val(cates.parent_categoryId);
                    buildInSideSubCategory(cates.categoryId);
                    $("#ddlInSideSubject").val(cates.categoryId);
                    $("#mode").val("Edit");
                    $("#sampleId").val(cates.id);
                    $("#inSideSubject").val(cates.categoryId);
                    $find("mpe").show();
                    $("#PopAddServiceMsg").show();
               
                },
                error: function () {
                    alert("error");
             
                }
            });
            
            return false;
        }

        function redirect() {
            location.href = "CustomerCategory.aspx";
            return false;
        }

        function showFine() {
            $("#txtFineContent").val("");
            $("#ddlFineType").val("");
            $find("mpe2").show();
            $("#PopAddFine").show();
            $("#fineMode").val("Add");
            return false;
        }

        function fineClear() {
            $("#txtFineContent").val("");
            $("#ddlFineType").val("");
        }

        function addFineCheck() {
            if ($("#txtFineContent").val() == "") {
                alert("請輸入原因");
                return false;
            }
            else if ($("#ddlFineType option:selected").val() == "") {
                alert("請輸入異動對帳單明細類型");
                return false;
            }
            else {
                return true;
            }
        }

        function fineEdit(id) {

            $.ajax({
                type: "POST",
                url: "CannedMessage.aspx/GetVendorFineCategoryList",
                data: "{id: " + id + "}",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    var cates = $.parseJSON(data.d);
                    $("#txtFineContent").val(cates.fineContent);
                    $("#ddlFineType").val(cates.fineType);
                    $("#ddlFineType").prop('disabled', true);
                    $("#fineMode").val("Edit");
                    $("#fineId").val(cates.id);
                    $find("mpe2").show();
                    $("#PopAddFine").show();
                },
                error: function () {
                    alert("error");

                }
            });

            return false;
        }


    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:HiddenField runat="server" ID="inSideSubject" ClientIDMode="Static"></asp:HiddenField>
    <asp:HiddenField runat="server" ID="outSideSubject" ClientIDMode="Static"></asp:HiddenField>
    <asp:HiddenField runat="server" ID="mode" ClientIDMode="Static"></asp:HiddenField>
    <asp:HiddenField runat="server" ID="fineMode" ClientIDMode="Static"></asp:HiddenField>
    <asp:HiddenField runat="server" ID="sampleId" ClientIDMode="Static"></asp:HiddenField>
    <asp:HiddenField runat="server" ID="fineId" ClientIDMode="Static"></asp:HiddenField>
    <asp:LinkButton ID="lnkDummy" runat="server"></asp:LinkButton>
    <cc1:ModalPopupExtender ID="ModalPopupExtender" runat="server" BehaviorID="mpe" TargetControlID="lnkDummy"
        PopupControlID="divAddServiceMsg" BackgroundCssClass="modalBackground"
        CancelControlID="btnAddServiceMsgClose" PopupDragHandleControlID="Panel3" Enabled="True" />
    <cc1:ModalPopupExtender ID="ModalPopupExtender1" runat="server" BehaviorID="mpe2" TargetControlID="lnkDummy"
        PopupControlID="divAddFine" BackgroundCssClass="modalBackground"
        CancelControlID="btnAddFineClose" PopupDragHandleControlID="palAddFine" Enabled="True" />

    <font size="5"><b>罐頭訊息管理</b></font>
    <br>
    <br>
    <div id="tabs" style="width: 1200px">
            <ul>
                <li><a href="#tabs-1">客服案件問題分類/主因</a></li>
                <li><a href="#tabs-2">廠商扣款/異動原因管理</a></li>
            </ul>
        <div id="tabs-1">
            對應到客服案件，問題分類以及主因
            <table class="searchTable">
            <tr>
                <td>問題類別：<asp:DropDownList runat="server" ID="ddlCategory" ClientIDMode="Static" onchange="buildSubCategory();">
                    <asp:ListItem Text="請選擇" Value=""></asp:ListItem>
                </asp:DropDownList>
                    問題主因：<asp:DropDownList runat="server" ID="ddlSubject" ClientIDMode="Static" onchange="getSubValue();">
                        <asp:ListItem Text="請選擇" Value=""></asp:ListItem>
                    </asp:DropDownList>
                    <br>
                </td>
            </tr>
            <tr>
                <td align="right">
                    <asp:Button ID="btnSearch" runat="server" Text="查詢" OnClientClick="return searchCheck();" OnClick="btnSearch_Click" />
                </td>
            </tr>

        </table>
            <table width="1024px">
            <tr>
                <td align="right">
                    <asp:Button ID="btnManagerCategory" runat="server" Text="問題類別管理" OnClientClick="return redirect()" />
                    <asp:Button ID="btnAddServiceMsg" runat="server" Text="新增罐頭訊息" OnClientClick="return show();" />
                </td>
            </tr>
        </table>
            <br />
            <br />
            <asp:Panel ID="divSearch" runat="server" Visible="false">
            <asp:GridView ID="gridSearch" runat="server" OnRowCommand="search_RowCommand" OnRowDataBound="search_RowDataBound"
                GridLines="Horizontal" ForeColor="Black" CellPadding="4" BorderWidth="1px" BorderStyle="None"
                BorderColor="#DEDFDE" BackColor="White" EmptyDataText="無符合的資料" AutoGenerateColumns="false"
                Font-Size="Small" Width="1024px">
                <FooterStyle BackColor="#CCCC99"></FooterStyle>
                <RowStyle BackColor="#F7F7DE" Height="20px" HorizontalAlign="Center"></RowStyle>
                <SelectedRowStyle BackColor="#CE5D5A" ForeColor="White" Font-Bold="True"></SelectedRowStyle>
                <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Center"></PagerStyle>
                <HeaderStyle BackColor="#6B696B" ForeColor="White" Font-Bold="True" Height="20px"
                    HorizontalAlign="Center"></HeaderStyle>
                <Columns>
                    <asp:TemplateField HeaderText="問題類別" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="10%">
                        <ItemTemplate>
                            <asp:Label ID="lblCategory" runat="server"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="主因" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="10%">
                        <ItemTemplate>
                            <asp:Label ID="lblCategoryName" runat="server"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="標題" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="20%">
                        <ItemTemplate>
                            <asp:Label ID="lblSubject" runat="server"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="內容" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="25%">
                        <ItemTemplate>
                            <asp:Label ID="lblContent" runat="server"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="更新日期" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="15%">
                        <ItemTemplate>
                            <asp:Label ID="lblCreateDate" runat="server" CommandName="Show"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="管理" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="10%">
                        <ItemTemplate>
                            <asp:LinkButton ID="lbtnEdit" runat="server"  OnClientClick='<%# "return edit(" + Eval("Id") + ");" %>' ></asp:LinkButton>
                            <asp:LinkButton ID="lbtnDelete" runat="server" CommandName="DEL" OnClientClick="return confirmDel();" Visible="False"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
            <uc1:Pager ID="ucPager" runat="server" PageSize="10" ongetcount="GetMessageCount" onupdate="CannedMessageUpdateHandler"></uc1:Pager>
        </asp:Panel>



            <asp:Panel ID="divAddServiceMsg" runat="server">
            <div id="PopAddServiceMsg" class="modalPopup" style="display: none; width: 500px;">
                <asp:Panel ID="Panel3" runat="server" Width="500px" Style="cursor: move; background-color: #DDDDDD; border: solid 1px Gray; color: Black">

                    <table width="100%" style="align-content: center">
                        <tr >
                            <td width="40%" height="40px" style='vertical-align:middle;'>編輯訊息</td>
                            <td><asp:ImageButton ID="ImageButton1" ImageUrl="~/images/close.gif" ImageAlign="Right" runat="server"></asp:ImageButton></td>
                        </tr>
                        <tr><td colspan="2"><hr /></td></tr>
                        <tr>
                            <td>問題類別
                                            <asp:DropDownList runat="server" ID="ddlInSideCategory" ClientIDMode="Static" onchange="buildInSideSubCategory('');">
                                                <asp:ListItem Text="請選擇" Value=""></asp:ListItem>
                                            </asp:DropDownList>
                            </td>
                            <td>問題主因
                                            <asp:DropDownList runat="server" ID="ddlInSideSubject" ClientIDMode="Static" onchange="getInSubValue();">
                                                <asp:ListItem Text="請選擇" Value=""></asp:ListItem>
                                            </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">標題<asp:TextBox runat="server" ID="txtSubject" ClientIDMode="Static" Width="85%"/></td>
                        </tr>
                        <tr>
                            <td colspan="2">內容<textarea id="txtContent" clientidmode="Static" cols="60" rows="8" runat="server"></textarea></td>
                        </tr>
                        <tr>
                            <td colspan="2" align="center">
                                <asp:Button ID="btnAddServiceMsgSubmit" runat="server" Text="確定" OnClientClick="return addCheck();" OnClick="btnAddService" />&nbsp;
                                <asp:Button ID="btnAddServiceMsgClose" runat="server" Text="退出" OnClientClick="clear();" />
                            </td>
                        </tr>

                    </table>
                </asp:Panel>
            </div>
        </asp:Panel>
        </div>
        <div id="tabs-2">
            對應到訂單頁上，商家扣款的異動原因，以及商家的對帳單明細
            <table style="width:1024px">
                <tr>
                    <td align="right">
                        <asp:Button ID="brnAddFine" runat="server" Text="新增商家扣款/異動原因" OnClientClick="return showFine();"/>
                    </td>
                </tr>
            </table>
            
            <asp:GridView ID="grvFine" runat="server" OnRowDataBound="fine_RowDataBound"
                GridLines="Horizontal" ForeColor="Black" CellPadding="4" BorderWidth="1px" BorderStyle="None"
                BorderColor="#DEDFDE" BackColor="White" EmptyDataText="無符合的資料" AutoGenerateColumns="false"
                Font-Size="Small" Width="1024px">
                <FooterStyle BackColor="#CCCC99"></FooterStyle>
                <RowStyle BackColor="#F7F7DE" Height="20px" HorizontalAlign="Center"></RowStyle>
                <SelectedRowStyle BackColor="#CE5D5A" ForeColor="White" Font-Bold="True"></SelectedRowStyle>
                <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Center"></PagerStyle>
                <HeaderStyle BackColor="#6B696B" ForeColor="White" Font-Bold="True" Height="20px"
                    HorizontalAlign="Center"></HeaderStyle>
                <Columns>
                    <asp:TemplateField HeaderText="商家扣款異動/原因" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="10%">
                        <ItemTemplate>
                            <asp:Label ID="lblFineContent" runat="server"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="異動對帳單明細" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="10%">
                        <ItemTemplate>
                            <asp:Label ID="lblFineType" runat="server"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="管理" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="10%">
                        <ItemTemplate>
                            <asp:LinkButton ID="lbtnFineEdit" runat="server"  OnClientClick='<%# "return fineEdit(" + Eval("Id") + ");" %>' ></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>

            <asp:Panel ID="divAddFine" runat="server">
                <div id="PopAddFine" class="modalPopup" style="display: none; width: 500px;">
                    <asp:Panel ID="palAddFine" runat="server" Width="500px" Style="cursor: move; background-color: #DDDDDD; border: solid 1px Gray; color: Black">

                        <table width="100%" style="align-content: center">
                            <tr >
                                <td style='vertical-align:middle;'>【新增】商家扣款/異動原因</td>
                                <td><asp:ImageButton ID="imgFineClose" ImageUrl="~/images/close.gif" ImageAlign="Right" runat="server"></asp:ImageButton></td>
                            </tr>
                            <tr><td colspan="2"><hr /></td></tr>
                            <tr>
                                <td>商家扣款/異動原因：
                                                
                                </td>
                                <td>
                                    <asp:TextBox runat="server" ID="txtFineContent" ClientIDMode="Static"  />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    異動對帳單明細：
                                </td>
                                <td>
                                    <asp:DropDownList runat="server" ID="ddlFineType" ClientIDMode="Static" onchange="getInSubValue();">
                                        <asp:ListItem Text="請選擇" Value=""></asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" align="center">
                                    <asp:Button ID="btnAddFineSubmit" runat="server" Text="確定" OnClientClick="return addFineCheck();" OnClick="btnAddFine" />&nbsp;
                                    <asp:Button ID="btnAddFineClose" runat="server" Text="退出" OnClientClick="fineClear();" />
                                </td>
                            </tr>

                        </table>
                    </asp:Panel>
                </div>
            </asp:Panel>

        </div>
        

    </div>

</asp:Content>

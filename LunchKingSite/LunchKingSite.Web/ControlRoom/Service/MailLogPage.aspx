﻿<%@ Page Language="C#" MasterPageFile="~/ControlRoom/backend.master" AutoEventWireup="true" CodeBehind="MailLogPage.aspx.cs" 
    Inherits="LunchKingSite.Web.ControlRoom.Service.MailLogPage" EnableViewState="false" ClientIDMode="Static" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">

    <script type="text/javascript" src="../../Tools/js/jquery-1.7.2.min.js"></script>
    <script type="text/javascript" src="../../Tools/js/jquery-ui.1.8.min.js"></script>
    <script type="text/javascript" src="../../Tools/ckeditor/jquery.query-2.1.7.js"></script>
    <script type="text/javascript" src="../../Tools/js/jquery.maxlength-min.js"></script>
    <script type="text/javascript" src="../../Tools/js/jquery.ui.datepicker-zh-TW.js"></script>
    <script type="text/javascript" src="../../Tools/js/jquery.blockUI.js"></script>
    <link type="text/css" href="../../Tools/js/css/ui-lightness/jquery-ui-1.8rc3.custom.css" rel="stylesheet" />
    <link href="../../Themes/PCweb/css/ppon.css" type="text/css" rel="Stylesheet" />
    <link href="../../Themes/PCweb/plugin/font-awesome/font-awesome.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../Tools/ckeditor/ckeditor.js"></script>
    <script type="text/javascript" src="../../Tools/js/json2.js"></script>
    <script type="text/javascript" src="../../Tools/js/jquery.numeric.js"></script>
    <script type="text/javascript" src="../../Tools/js/jquery.checkboxtree.js"></script>
    <script type="text/javascript" src="//html2canvas.hertzen.com/build/html2canvas.js"></script>
    <link type="text/css" href="../../Tools/js/jquery.checkboxtree.css" rel="stylesheet" />
    <style type="text/css">
        .modalPopup {
            margin-top: -200px;
            margin-left: -200px;
        }

        .searchTable {
            padding: 10px 10px 10px 10px;
            line-height: 24px;
            width: 1060px;
            background-color: #EFEBEB;
        }

        #datalist {
            display: table;
            width: 1060px;
            background-color: #EFEBEB;
        }

        #datalist .row {
            border-top:1px solid silver;
            font-size:13px;
        }

        #datalist .cell {
            display: table-cell;
            padding:2px 3px 1px 3px;
            vertical-align: middle;
        }

        #datalist .cell1 {
            display: table-cell;
            width: 60px;
            vertical-align: middle;
            font-family: Arial;
        }

        #datalist .cell2 {
            display: table-cell;
            width: 240px;
            font-family: Arial;
        }

        #datalist .cell3 {
            display: table-cell;
            width: 400px;
        }

        #datalist .cell4 {
            display: table-cell;
            width: 60px;
            text-align: right;
        }

        #datalist .cell5 {
            display: table-cell;
            width: 40px;
            text-align: right;
        }

        #datalist .cell6 {
            display: table-cell;
            width: 120px;
            font-family: Arial;
            text-align: right;
        }

        #datalist .cell7 {
            display: table-cell;
            width: 40px;
            text-align: right;
        }
        #datalist .cell8 {
            display: table-cell;
            width: 40px;
            text-align: right;
        }

        #datalist span.createTime {
            display: block;
        }

        #datalist span.resendTime {
            display: block;
            color: blue;
        }

        #datalist .templateRow {
            display: none;
        }

        #datalist .noData {
            text-align: center;
            font-size:18px;
            color: rgb(44, 87, 242);
            display:none;
        }

        .wrapword {
            white-space: -moz-pre-wrap !important; /* Mozilla, since 1999 */
            white-space: -webkit-pre-wrap; /*Chrome & Safari */
            white-space: -pre-wrap; /* Opera 4-6 */
            white-space: -o-pre-wrap; /* Opera 7 */
            white-space: pre-wrap; /* css-3 */
            word-wrap: break-word; /* Internet Explorer 5.5+ */
            word-break: break-all;
            white-space: normal;
        }


    </style>

    <script type="text/javascript" language="javascript">
        var b_editor;
        function setupdatepicker(from, to) {
            var dates = $('#' + from + ', #' + to).datepicker({
                changeMonth: true,
                numberOfMonths: 3,
                onSelect: function (selectedDate) {
                    var option = this.id == from ? "minDate" : "maxDate";
                    var instance = $(this).data("datepicker");
                    var date = $.datepicker.parseDate(instance.settings.dateFormat || $.datepicker._defaults.dateFormat, selectedDate, instance.settings);
                    dates.not(this).datepicker("option", option, date);
                }
            });
        }
        function parse17LifeDateTimeString(str) {
            if (str != null && str.length === 21) {
                var iso8601Str = '';
                iso8601Str += str.substr(0, 4) + "-";
                iso8601Str += str.substr(4, 2) + "-";
                iso8601Str += str.substr(6, 2) + "T";
                iso8601Str += str.substr(9, 2) + ":";
                iso8601Str += str.substr(11, 2) + ":";
                iso8601Str += str.substr(13, 2);
                iso8601Str += str.substr(16, 5);
                return new Date(Date.parse(iso8601Str));
            }
            return null;
        }
        function toDateTimeString(dt, pattern) {
            if (dt == null) {
                return null;
            }
            if (pattern == null) {
                pattern = "yyyy/MM/dd HH:mm:ss";
            }
            var yyyy = dt.getFullYear();
            var mm = dt.getMonth() < 9 ? "0" + (dt.getMonth() + 1) : (dt.getMonth() + 1); // getMonth() is zero-based
            var dd  = dt.getDate() < 10 ? "0" + dt.getDate() : dt.getDate();
            var hh = dt.getHours() < 10 ? "0" + dt.getHours() : dt.getHours();
            var min = dt.getMinutes() < 10 ? "0" + dt.getMinutes() : dt.getMinutes();
            var ss = dt.getSeconds() < 10 ? "0" + dt.getSeconds() : dt.getSeconds();
            //return "".concat(yyyy).concat('/').concat(mm).concat('/').concat(dd).concat(' ')
                //.concat(hh).concat(':').concat(min).concat(':').concat(ss);

            return pattern.replace("yyyy", yyyy).replace("MM", mm).replace("dd", dd)
                .replace("HH", hh).replace("mm", min).replace("ss", ss);
        };

        $(document).ready(function () {
            setupdatepicker('tbStartDate', 'tbEndDate');

            window.templateMap = {}
            $('option', '#ddlTemplate').each(function() {
                if ($(this).attr('value') !== '') {
                    templateMap[$(this).attr('value')] = $(this).text();
                }
            });            

            $(document).ajaxStart(function() {
                $('#btnSubmit').attr('disabled', true);
            }).ajaxStop(function() {
                $('#btnSubmit').removeAttr('disabled');
            });

            $('#btnSubmit').click(function() {
                var dataBody = $('.dataBody');
                dataBody.empty();
                $('.noData').hide();
                var email = $('#tbEmail').val();
                var subject = $('#tbSubject').val();
                var startDate = $('#tbStartDate').val() +"T00:00:00+0800";
                var endDate = $('#tbEndDate').val() +"T00:00:00+0800";
                var template = $('#ddlTemplate').val() === '' ? null : parseInt($('#ddlTemplate').val(), 10);
                var parameters = {
                    "email": email,
                    "subject": subject,
                    "startDate": startDate,
                    "endDate": endDate,
                    "template": template
                };
                if ($('#chkTop200').is(':checked')) {
                    parameters.top = 200;
                } else {
                    parameters.top = null;
                }
                $.ajax({
                    type: "POST",
                    url: "/service/mailLog/query",
                    data: JSON.stringify(parameters),
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function(res) {                    
                        if (res.Code === 1000) {
                            if (res.Data.length === 0) {
                                $('.noData').show();
                                return;
                            }
                            for (var i in res.Data) {
                                var item = res.Data[i];
                                var newRow = $('.templateRow').find('.row').clone();
                                newRow.find('.cell1').text(item.Id);
                                newRow.find('.cell2').text(item.Receiver);
                                newRow.find('.cell3').text(item.Subject);
                                newRow.find('.cell4').text(templateMap[item.Category.toString()]);
                                newRow.find('.cell5').html('<a href="?action=view&id=' + item.Id + '" target="_blank" class="view" refId="' + item.Id + '">檢視</a>');
                                var createTimeSpan = $('<span></span>').addClass('createTime').html(toDateTimeString(parse17LifeDateTimeString(item.CreateTime), "yyyy/MM/dd HH:mm"));
                                var resendTimeText = toDateTimeString(parse17LifeDateTimeString(item.ResendTime), "yyyy/MM/dd HH:mm") || "";
                                var resendTimeSpan = $('<span></span>').addClass('resendTime').html(resendTimeText);
                                newRow.find('.cell6').append(createTimeSpan);
                                newRow.find('.cell6').append(resendTimeSpan);
                                var resultSpan = $('<span></span>').text(item.Result ? "成功" : "失敗");
                                if (item.Result === false) {
                                    resultSpan.attr('title', item.LastError);
                                }
                                newRow.find('.cell7').append(resultSpan);
                                newRow.find('.cell8').html($('<a href="javascript:void(0)" class="resend" refId="' + item.Id + '">重送</a>'));

                                dataBody.append(newRow);
                            }
                        }
                    }, error: function(err) {
                        
                    }, complete: function() {
                    }
                });
            });

            $('#datalist').on('click', '.resend', function() {
                if (confirm('確定要重新發送此信件？') !== true) {
                    return;
                }
                var parameters = {
                    id: parseInt($(this).attr('refId'), 10)
                };
                $.ajax({
                    type: "POST",
                    url: "/service/mailLog/resend",
                    data: JSON.stringify(parameters),
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function(res) {
                        if (res.Code === 1000) {
                            if (res.Data) {
                                alert('重送完成');
                            } else {
                                alert('重送失敗');
                            }
                        }
                    },
                    error: function(err) {

                    },
                    complete: function() {
                    }
                });            
            });
        });


    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <h2 style="font-size:24px"><b>信件查詢</b></h2>
    <div style="margin-top: 20px">
        <table class="searchTable">
            <tr>
                <td>信箱: <asp:TextBox ID="tbEmail" runat="server" ClientIDMode="Static" style="width:200px" /></td>
                <td>
              範本分類: 
                    <asp:DropDownList runat="server" ID="ddlTemplate" CssClass="adminitem">
                        <asp:ListItem Text="請選擇" Value="-1"></asp:ListItem>
                        <asp:ListItem Text="範本1" Value="0"></asp:ListItem>
                        <asp:ListItem Text="範本2" Value="1"></asp:ListItem>
                        <asp:ListItem Text="範本3" Value="2"></asp:ListItem>
                        <asp:ListItem Text="範本4" Value="3"></asp:ListItem>
                        <asp:ListItem Text="範本5" Value="4"></asp:ListItem>
                        <asp:ListItem Text="範本6" Value="5"></asp:ListItem>
                    </asp:DropDownList>
                    
                </td>
                <td>
                    發送日期: <asp:TextBox ID="tbStartDate" CssClass="date" runat="server" ClientIDMode="Static" MaxLength="10" size="10" placeholder="" />
                    ～
                    <asp:TextBox ID="tbEndDate" CssClass="date" runat="server" ClientIDMode="Static" MaxLength="10" size="10" placeholder="" />
                </td>
                <td>
                    最多200筆: <input type="checkbox" id="chkTop200" runat="server" checked="checked" />
                </td>
                <td>
                    <button id="btnSubmit" type="button">確定</button>
                </td>
            </tr>
            <tr>
                <td>主旨: <asp:TextBox ID="tbSubject" runat="server" ClientIDMode="Static" style="width:200px" /></td>
            </tr>
           
        </table>
    </div>

    <div id="datalist">
        <div class="noData">
            沒有符合的資料
        </div>
        <div class="dataBody">
        </div>
        <div class="templateRow">
            <div class="row">
                <div class="cell cell1">###</div>
                <div class="cell cell2 wrapword">vita_chang+kimi_chou.17life.com@17life.com.tw</div>
                <div class="cell cell3 wrapword">付款成功通知(【SOLUDOS】美國紐約懶人鞋草編鞋 2雙/組)</div>
                <div class="cell cell4">範本1</div>
                <div class="cell cell5">2017/07/17 12:33:22</div>
                <div class="cell cell6"></div>
                <div class="cell cell7"></div>
                <div class="cell cell8"></div>
            </div>
        </div>
    </div>


</asp:Content>

﻿<%@ Page Language="C#" MasterPageFile="~/ControlRoom/backend.master" AutoEventWireup="true" CodeBehind="CustomerCategory.aspx.cs" Inherits="LunchKingSite.Web.ControlRoom.Service.CustomerCategory" %>

<%@ Import Namespace="LunchKingSite.BizLogic.Component" %>
<%@ Import Namespace="LunchKingSite.Core" %>
<%@ Import Namespace="LunchKingSite.DataOrm" %>
<%@ Import Namespace="LunchKingSite.WebLib.Views" %>
<%@ Import Namespace="System" %>
<%@ Register Src="../Controls/Pager.ascx" TagName="Pager" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">

    <script type="text/javascript" src="../../Tools/js/jquery-1.7.2.min.js"></script>
    <script type="text/javascript" src="../../Tools/js/jquery-ui.1.8.min.js"></script>
    <script type="text/javascript" src="../../Tools/ckeditor/jquery.query-2.1.7.js"></script>
    <script type="text/javascript" src="../../Tools/js/jquery.maxlength-min.js"></script>
    <script type="text/javascript" src="../../Tools/js/jquery.ui.datepicker-zh-TW.js"></script>
    <script type="text/javascript" src="../../Tools/js/jquery.blockUI.js"></script>
    <link type="text/css" href="../../Tools/js/css/ui-lightness/jquery-ui-1.8rc3.custom.css" rel="stylesheet" />
    <link href="../../Themes/PCweb/css/ppon.css" type="text/css" rel="Stylesheet" />
    <link href="../../Themes/PCweb/plugin/font-awesome/font-awesome.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../Tools/ckeditor/ckeditor.js"></script>
    <script type="text/javascript" src="../../Tools/js/json2.js"></script>
    <script type="text/javascript" src="../../Tools/js/jquery.numeric.js"></script>
    <script type="text/javascript" src="../../Tools/js/jquery.checkboxtree.js"></script>
    <script type="text/javascript" src="//html2canvas.hertzen.com/build/html2canvas.js"></script>
    <link type="text/css" href="../../Tools/js/jquery.checkboxtree.css" rel="stylesheet" />
    <style type="text/css">
        .modalPopup {
            margin-top: -200px;
            margin-left: -200px;
        }
    </style>

    <script>

        function confirmDel() {
            return confirm("確定要刪除該筆問題類別？\r\n(刪除後，該類別下的主因也會一併刪除)");
        }

        function show() {
            initial()
            $find("mpe").show();
            $("#PopAddCategory").show();
            
            return false;
        }

        function addSubCate() {
            var Count = parseInt($("#subjectCount").val());
            $("#subjectCount").val(Count + 1);
            $("#subcategory").append("<div style='height:30px' id='div" + $("#subjectCount").val() + "'><input type='text' id='subject" + $("#subjectCount").val() + "'><a href='javascript:void(0)' onclick='remove(" + $("#subjectCount").val() + ",0);'><img src='/images/close.gif' style='vertical-align:middle;' ></a></div>")
            return false;
        }

        function addCheck() {
            if (confirm("確定儲存資料？")) {
                if ($("#txtCategory").val() == "") {
                    alert("請填寫問題類別！");
                    return false;
                }
                else {
                    var total = $("#subjectCount").val();
                    for (i = 1; i <= total ; i++) {
                        if ($("#subjectContent").val() != "") {
                            if ($("#subject" + i + "").val() != undefined) {
                                $("#subjectContent").val($("#subjectContent").val() + "," + $("#subject" + i + "").val());
                            }
                        }
                        else {
                            if ($("#subject" + i + "").val() != undefined) {
                                $("#subjectContent").val($("#subject" + i + "").val());
                            }
                        }
                    }
                }
            }
            else {
                return false;
            }
        }

        function initial() {
            $("#subcategory").html("");
            $("#txtCategory").val("");
            $('#rbFrontEnd').prop('checked', true);
            $('#rbOrderId').prop('checked', true);
            $("#mode").val("Add");
            $("#subjectCount").val(0);
            $("#subjectContent").val("");
        }
        

        function remove(id, categoryId)
        {
            if (categoryId != 0)
            {
                if (confirm("您確定要刪除此筆資料嗎？"))
                {
                    del(id,categoryId);
                    return true;
                }
                else
                {
                    return false;
                };
                
            }


        }

        function del(id,categoryId,userId) {
            $.ajax({
                type: "POST",
                url: "CustomerCategory.aspx/DeleteSubCategory",
                data: "{categoryId: " + categoryId + ",userId:"+$("#userId").val()+"}",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function () {
                    $("#div" + id + "").remove();
                    //var Count = parseInt($("#subjectCount").val());
                    //$("#subjectCount").val(Count - 1);
                },
                error: function () {
                    alert("error");

                }
            });
        }

        function edit(categoryId) {
            $.ajax({
                type: "POST",
                url: "CustomerCategory.aspx/GetCategoryList",
                data: "{categoryid: " + categoryId + "}",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    var cates = $.parseJSON(data.d);
                    $("#mainCategoryId").val(cates.categoryId);
                    $("#txtCategory").val(cates.categoryName);
                    if (cates.isFrontEnd == "1") {
                        $('#rbFrontEnd').prop('checked', true);
                    }
                    else {
                        $('#rbNotFrontEnd').prop('checked', true);
                    }
                    if (cates.needOrderId == "1") {
                        $('#rbOrderId').prop('checked', true);
                    }
                    else {
                        $('#rbNotOrderId').prop('checked', true);
                    }
                     var count = 0;
                    $.each(cates.subCategoryList, function (i, item) {
                        count = count + 1;
                        $("#subjectCount").val(count);
                        $("#subcategory").append("" +
                            "<div style='height:30px' id='div" + $("#subjectCount").val() + "'>" +
                            "<input type='text' id='subject" + $("#subjectCount").val() + "' value='" + item.subName + "''>" +
                            "<a style='display:none;' href='javascript:void(0)' onclick='remove(" + $("#subjectCount").val() + "," + item.categoryId + ");'>" +
                            "<img src='/images/close.gif' style='vertical-align:middle;' ></a></div>");
                    });
                    $("#mode").val("Edit");
                    $find("mpe").show();
                    $("#PopAddCategory").show();

                },
                error: function () {
                    alert("error");

                }
            });
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:HiddenField runat="server" ID="mode" ClientIDMode="Static"></asp:HiddenField>
    <asp:HiddenField runat="server" ID="mainCategoryId" ClientIDMode="Static"></asp:HiddenField>
    <asp:HiddenField runat="server" ID="subjectCount" Value="0" ClientIDMode="Static"></asp:HiddenField>
    <asp:HiddenField runat="server" ID="subjectContent" ClientIDMode="Static"></asp:HiddenField>
    <asp:HiddenField runat="server" ID="userId" ClientIDMode="Static"></asp:HiddenField>
    <asp:LinkButton ID="lnkDummy" runat="server"></asp:LinkButton>
    <cc1:ModalPopupExtender ID="ModalPopupExtender" runat="server" BehaviorID="mpe" TargetControlID="lnkDummy"
        PopupControlID="divAddCategory" BackgroundCssClass="modalBackground"
        CancelControlID="btnAddCategoryClose" PopupDragHandleControlID="Panel3" Enabled="True" />

    <font size="6"><b>問題類別管理</b></font>
    <br>
    <br>
    <table width="800px">
        <tr>
            <td align="right">
                <asp:Button ID="btnAddServiceMsg" runat="server" Text="新增問題類別" OnClientClick="return show();" />
            </td>
        </tr>
    </table>
    <br />
    <br />
    <asp:Panel ID="divCategory" runat="server" Visible="false">
        <asp:GridView ID="gridCategory" runat="server" OnRowCommand="category_RowCommand" OnRowDataBound="category_RowDataBound"
            GridLines="Horizontal" ForeColor="Black" CellPadding="4" BorderWidth="1px" BorderStyle="None"
            BorderColor="#DEDFDE" BackColor="White" EmptyDataText="無符合的資料" AutoGenerateColumns="false"
            Font-Size="Small" Width="800px">
            <FooterStyle BackColor="#CCCC99"></FooterStyle>
            <RowStyle BackColor="#F7F7DE" Height="20px" HorizontalAlign="Center"></RowStyle>
            <SelectedRowStyle BackColor="#CE5D5A" ForeColor="White" Font-Bold="True"></SelectedRowStyle>
            <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Center"></PagerStyle>
            <HeaderStyle BackColor="#6B696B" ForeColor="White" Font-Bold="True" Height="20px"
                HorizontalAlign="Center"></HeaderStyle>
            <Columns>
                <asp:TemplateField HeaderText="問題類別" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="20%">
                    <ItemTemplate>
                        <asp:Label ID="lblCategory" runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="是否顯示於前台" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="20%">
                    <ItemTemplate>
                        <asp:Label ID="lblFrontEnd" runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="是否需要訂單編號" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="20%">
                    <ItemTemplate>
                        <asp:Label ID="lblOrderId" runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="更新日期" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="20%">
                    <ItemTemplate>
                        <asp:Label ID="lblCreateDate" runat="server" CommandName="Show"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="管理" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="20%">
                    <ItemTemplate>
                        <asp:LinkButton ID="lbtnEdit" runat="server"  OnClientClick='<%# "return edit(" + Eval("CategoryId") + ");" %>' ></asp:LinkButton>
                        <asp:LinkButton ID="lbtnDelete" runat="server" CommandName="DEL" OnClientClick="return confirmDel();" Visible="False"></asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
        <uc1:Pager ID="ucPager" runat="server" PageSize="10" ongetcount="GetCategoryCount" onupdate="CategoryUpdateHandler"></uc1:Pager>
    </asp:Panel>



    <asp:Panel ID="divAddCategory" runat="server">
        <div id="PopAddCategory" class="modalPopup" style="display: none; width: 300px;">
            <asp:Panel ID="Panel3" runat="server" Width="300px" Style="cursor: move; background-color: #DDDDDD; border: solid 1px Gray; color: Black">

                <table width="100%" style="align-content: center" ">
                    <tr >
                        <td width="40%" height="40px" style='vertical-align:middle;'>編輯問題類別</td>
                        <td><asp:ImageButton ID="imageClose" ImageUrl="~/images/close.gif" ImageAlign="Right" runat="server"></asp:ImageButton></td>
                    </tr>
                    <tr><td colspan="2"><hr /></td></tr>
                    <tr>
                        <td>問題類別</td>
                        <td><asp:TextBox runat="server" ID="txtCategory" ClientIDMode="Static" /></td>                        
                    </tr>
                    <tr>
                        <td>顯示於前台</td>
                        <td> 
                            <asp:RadioButton ID="rbNotFrontEnd" ClientIDMode="Static" GroupName="isFrontEnd" runat="server" />否
                            <asp:RadioButton ID="rbFrontEnd" ClientIDMode="Static" GroupName="isFrontEnd" runat="server" />是
                        </td>
                    </tr>
                    <tr>
                        <td>需要訂單編號</td>
                        <td>
                            <asp:RadioButton ID="rbNotOrderId" ClientIDMode="Static" GroupName="needOrderId" runat="server" />否
                            <asp:RadioButton ID="rbOrderId" ClientIDMode="Static" GroupName="needOrderId" runat="server" />是
                        </td>
                    </tr>
                    <tr>
                         <td colspan="2">
                            主因<asp:Button ID="btn" runat="server" Text="新增" OnClientClick="return addSubCate();" />
                         </td>                  
                    </tr>
                    <tr>
                        <td colspan="2"><div id="subcategory" clientidmode="Static" runat="server"></div></td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center">
                            <asp:Button ID="btnAddCategorySubmit" runat="server" Text="確定" OnClientClick="return addCheck();"  onclick="btnAddCategory"/>&nbsp;
                            <asp:Button ID="btnAddCategoryClose" runat="server" Text="退出" OnClientClick="initial();" />
                        </td>
                    </tr>

                </table>
            </asp:Panel>
        </div>
    </asp:Panel>



</asp:Content>

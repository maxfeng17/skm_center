﻿<%@ Page Language="C#" MasterPageFile="~/ControlRoom/backend.master" AutoEventWireup="true" CodeBehind="Process.aspx.cs" Inherits="LunchKingSite.Web.ControlRoom.Service.Process" %>

<%@ Import Namespace="LunchKingSite.BizLogic.Component" %>
<%@ Import Namespace="LunchKingSite.Core" %>
<%@ Import Namespace="LunchKingSite.DataOrm" %>
<%@ Import Namespace="LunchKingSite.WebLib.Views" %>
<%@ Import Namespace="System" %>
<%@ Register Src="../Controls/Pager.ascx" TagName="Pager" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">

    <script type="text/javascript" src="../../Tools/js/jquery-1.7.2.min.js"></script>
    <script type="text/javascript" src="../../Tools/js/jquery-ui.1.8.min.js"></script>
    <script type="text/javascript" src="../../Tools/ckeditor/jquery.query-2.1.7.js"></script>
    <script type="text/javascript" src="../../Tools/js/jquery.maxlength-min.js"></script>
    <script type="text/javascript" src="../../Tools/js/jquery.ui.datepicker-zh-TW.js"></script>
    <script type="text/javascript" src="../../Tools/js/jquery.blockUI.js"></script>
    <link type="text/css" href="../../Tools/js/css/ui-lightness/jquery-ui-1.8rc3.custom.css" rel="stylesheet" />
    <link href="../../Themes/PCweb/css/ppon.css" type="text/css" rel="Stylesheet" />
    <link href="../../Themes/PCweb/plugin/font-awesome/font-awesome.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../Tools/ckeditor/ckeditor.js"></script>
    <script type="text/javascript" src="../../Tools/js/json2.js"></script>
    <script type="text/javascript" src="../../Tools/js/jquery.numeric.js"></script>
    <script type="text/javascript" src="../../Tools/js/jquery.checkboxtree.js"></script>
    <script type="text/javascript" src="//html2canvas.hertzen.com/build/html2canvas.js"></script>
    <link type="text/css" href="../../Tools/js/jquery.checkboxtree.css" rel="stylesheet" />
    <style type="text/css">
        .modalPopup {
            margin-top: -200px;
            margin-left: -200px;
        }
        .searchTable {
            padding: 10px 10px 10px 10px;
            letter-spacing:3px;
            line-height: 24px;
            width:1200px;
            background-color:#EFEBEB;
        }
    </style>

    <script>
        var b_editor;
        function setupdatepicker(from, to) {
            var dates = $('#' + from + ', #' + to).datepicker({
                changeMonth: true,
                numberOfMonths: 3,
                onSelect: function (selectedDate) {
                    var option = this.id == from ? "minDate" : "maxDate";
                    var instance = $(this).data("datepicker");
                    var date = $.datepicker.parseDate(instance.settings.dateFormat || $.datepicker._defaults.dateFormat, selectedDate, instance.settings);
                    dates.not(this).datepicker("option", option, date);
                }
            });
        }
        $(document).ready(function () {
            setupdatepicker('tbOS', 'tbOE');
        });


        function Remind(serviceNo) {
            if (confirm("是否已經聯絡到廠商？")) {
                $.ajax({
                    type: "POST",
                    url: "Process.aspx/RemindPhone",
                    data: "{serviceNo: '" + serviceNo + "'}",
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if (data.d == true) {
                            $("#" + serviceNo).hide();
                        }
                    },
                    error: function () {

                    }
                });
            }

        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <font size="5"><b>處理中案件</b></font>
    <br>
    <br>
    <asp:HiddenField ID="hiddenChose" ClientIDMode="Static" runat="server"></asp:HiddenField>
    <asp:Panel ID="seachPanel" runat="server" Visible="true">
        <table class="searchTable">
            <tr>
                <td>案件編號：<asp:TextBox ID="searchSericeNo" runat="server" ClientIDMode="Static" />
                    訂單編號：<asp:TextBox ID="searchOrderId" runat="server" ClientIDMode="Static" />
                    帳號：<asp:TextBox ID="searchMail" runat="server" ClientIDMode="Static" />
                    <asp:RadioButton ID="rbUserId" GroupName="searchType" runat="server" />
                    <asp:DropDownList runat="server" ID="ddlUserId" CssClass="adminitem">
                    </asp:DropDownList>
                    <asp:RadioButton ID="rbAll" GroupName="searchType" runat="server" />全部
                </td>
            </tr>
            <tr>
                <td>問題等級：
                <asp:DropDownList runat="server" ID="ddlPriority" CssClass="adminitem">
                    <asp:ListItem Text="請選擇" Value="-1"></asp:ListItem>
                    <asp:ListItem Text="一般" Value="0"></asp:ListItem>
                    <asp:ListItem Text="急" Value="1"></asp:ListItem>
                    <asp:ListItem Text="特急" Value="2"></asp:ListItem>
                </asp:DropDownList>
                    主要問題：
                <asp:DropDownList runat="server" ID="ddlCategory" CssClass="adminitem">
                </asp:DropDownList>
                    建立日期：<asp:TextBox ID="tbOS" CssClass="date" runat="server" ClientIDMode="Static" MaxLength="10" size="10" placeholder="" />
                    ～
                    <asp:TextBox ID="tbOE" CssClass="date" runat="server" ClientIDMode="Static" MaxLength="10" size="10" placeholder="" />
                </td>
                <td>
                    <asp:Button ID="btnSearch" runat="server" Text="查詢" OnClick="btnSearch_Click" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <br />
    <asp:Panel ID="divProcess" runat="server" Visible="false">
        <asp:GridView ID="gridProcess" runat="server" OnRowDataBound="process_RowDataBound"
            GridLines="Horizontal" ForeColor="Black" CellPadding="4" BorderWidth="1px" BorderStyle="None"
            BorderColor="#DEDFDE" BackColor="White" EmptyDataText="無符合的資料" AutoGenerateColumns="false" 
            Font-Size="Small" Width="1200px">
            <FooterStyle BackColor="#CCCC99"></FooterStyle>
            <RowStyle BackColor="#F7F7DE" Height="20px" HorizontalAlign="Center"></RowStyle>
            <SelectedRowStyle BackColor="#CE5D5A" ForeColor="White" Font-Bold="True"></SelectedRowStyle>
            <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Center"></PagerStyle>
            <HeaderStyle BackColor="#6B696B" ForeColor="White" Font-Bold="True" Height="20px"
                HorizontalAlign="Center"></HeaderStyle>
            <Columns>
                <asp:TemplateField HeaderText="案件編號" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="12%">
                    <ItemTemplate>
                        <asp:Label ID="lblDot" runat="server"></asp:Label>
                        <asp:HyperLink ID="hpServiceNo" runat="server"></asp:HyperLink>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="案件狀態" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="6%">
                    <ItemTemplate>
                        <asp:Label ID="lblStatus" runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="問題等級" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="6%">
                    <ItemTemplate>
                        <asp:Label ID="lblPriority" runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="案件來源" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="6%">
                    <ItemTemplate>
                        <asp:Label ID="lblMessageType" runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="來源平台" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="6%">
                    <ItemTemplate>
                        <asp:Label ID="lblSource" runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="姓名" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5%">
                    <ItemTemplate>
                        <asp:HyperLink ID="hyName" runat="server"></asp:HyperLink>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="帳號" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5%">
                    <ItemTemplate>
                        <asp:Label ID="lblAccount" runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="訂單編號" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="10%">
                    <ItemTemplate>
                        <asp:HyperLink ID="hpOrderId" runat="server"></asp:HyperLink>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="供應商" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5%">
                    <ItemTemplate>
                        <asp:HyperLink ID="hpSupplier" runat="server"></asp:HyperLink>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="問題分類" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="8%">
                    <ItemTemplate>
                        <asp:Label ID="lblCategory" runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="主因" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="10%">
                    <ItemTemplate>
                        <asp:Label ID="lblSubject" runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="建立時間" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="10%">
                    <ItemTemplate>
                        <asp:Label ID="lblCreateDate" runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="最後異動時間" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="10%">
                    <ItemTemplate>
                        <asp:Label ID="lblModifyDate" runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="處理人員" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="8%">
                    <ItemTemplate>
                        <asp:Label ID="lblWorkUser" runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
        <uc1:Pager ID="ucPager" runat="server" PageSize="20" ongetcount="GetProcessCount" onupdate="ProcessUpdateHandler"></uc1:Pager>
    </asp:Panel>

</asp:Content>

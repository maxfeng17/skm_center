﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using com.google.zxing;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Models.Entities;
using LunchKingSite.Core.UI;
using LunchKingSite.WebApi.Core;

namespace LunchKingSite.Web.ControlRoom.Service
{
    public partial class MailLogPage : Page
    {
        static private ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        protected void Page_Load(object sender, EventArgs e)
        {
            tbStartDate.Text = DateTime.Now.AddMonths(-1).ToString("yyyy/MM/dd");
            tbEndDate.Text = DateTime.Now.ToString("yyyy/MM/dd");

            var templateSource = Helper.GetEnumKeyValueList<MailTemplateType>();
            ddlTemplate.Items.Clear();
            ddlTemplate.Items.Add(new ListItem("全部", ""));
            foreach (var pair in templateSource)
            {
                ddlTemplate.Items.Add(new ListItem(pair.Key, pair.Value.ToString()));
            }

            if (Request["action"] == "view")
            {
                int id;
                if (int.TryParse(Request["id"], out id))
                {
                    ProcessView(id);
                }
                else
                {
                    HandleError("ERROR, no id.");
                    return;
                }
            }
        }

        private void HandleError(string msg)
        {
            Response.Clear();
            Response.Write(msg);
            Response.End();
        }

        private void ProcessView(int id)
        {
            HttpClient client = new HttpClient();
            dynamic parameters = new {Id = id};
            string apiUrl = Helper.CombineUrl(config.SiteUrl, "/service/mailLog/getContent");
            HttpResponseMessage response = client.PostAsync(apiUrl, new HttpJsonContent(parameters)).Result;
            if (response.IsSuccessStatusCode)
            {
                ApiResult result = response.Content.ReadAsAsync<ApiResult>().Result;
                Response.Clear();
                Response.Write(result.Data == null ? string.Empty : result.Data.ToString());
                Response.End();
            }            
        }
    }
}
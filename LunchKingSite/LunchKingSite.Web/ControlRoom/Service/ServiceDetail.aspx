﻿<%@ Page Language="C#" MasterPageFile="~/ControlRoom/backend.master" AutoEventWireup="true" CodeBehind="ServiceDetail.aspx.cs" Inherits="LunchKingSite.Web.ControlRoom.Service.ServiceDetail" %>

<%@ Import Namespace="LunchKingSite.BizLogic.Component" %>
<%@ Import Namespace="LunchKingSite.Core" %>
<%@ Import Namespace="LunchKingSite.DataOrm" %>
<%@ Import Namespace="LunchKingSite.WebLib.Views" %>
<%@ Import Namespace="System" %>
<%@ Register Src="../Controls/Pager.ascx" TagName="Pager" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">

    <script type="text/javascript" src="../../Tools/js/jquery-1.7.2.min.js"></script>
    <script type="text/javascript" src="../../Tools/js/jquery-ui.1.8.min.js"></script>
    <script type="text/javascript" src="../../Tools/ckeditor/jquery.query-2.1.7.js"></script>
    <script type="text/javascript" src="../../Tools/js/jquery.maxlength-min.js"></script>
    <script type="text/javascript" src="../../Tools/js/jquery.ui.datepicker-zh-TW.js"></script>
    <script type="text/javascript" src="../../Tools/js/jquery.blockUI.js"></script>
    <link type="text/css" href="../../Tools/js/css/ui-lightness/jquery-ui-1.8rc3.custom.css" rel="stylesheet" />
    <link href="../../Themes/PCweb/css/ppon.css" type="text/css" rel="Stylesheet" />
    <link href="../../Themes/PCweb/plugin/font-awesome/font-awesome.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../Tools/ckeditor/ckeditor.js"></script>
    <script type="text/javascript" src="../../Tools/js/json2.js"></script>
    <script type="text/javascript" src="../../Tools/js/jquery.numeric.js"></script>
    <script type="text/javascript" src="../../Tools/js/jquery.checkboxtree.js"></script>
    <script type="text/javascript" src="//html2canvas.hertzen.com/build/html2canvas.js"></script>
    <link type="text/css" href="../../Tools/js/jquery.checkboxtree.css" rel="stylesheet" />
    <!-- 彈窗jquery和它的功能宣告 -->
    <link href="<%= ResolveUrl("../../Tools/js/css/jquery-confirm.min.css") %>" rel="stylesheet" type="text/css" />
    <script src="<%= ResolveUrl("../../Tools/js/jquery-confirm.min.js") %>" type="text/javascript"></script>
    <style type="text/css">
        .modalPopup {
            margin-top: -200px;
            margin-left: -200px;
        }

        .service {
            padding: 10px;
        }



        .title {
            padding-bottom: 10px;
            font-size: x-large;
            font-weight: bolder;
            text-align: left;
            /*padding-left: 100px;*/
        }


        .case-info-sub {
            border-style: solid;
            border-width: 1px;
            border-color: #999898;
            width: 1220px;
            background-color: white;
        }

        .case-column {
            padding: 4px 5px 3px 10px;
            border-bottom: solid 1px #999898;
            text-align: left;
        }

        .log-content {
            border-style: solid;
            border-width: 1px;
            padding: 10px;
            margin-bottom: 20px;
            width: 100%;
        }

        .tag_place {
            border-radius: 20px;
            background: #999;
            color: white;
            padding: 0px 7px;
        }
        .inside_memo{
            color: red;
        }
        .content-word {
            padding-top: 10px;
        }

        a:link {
            text-decoration: underline;
        }

        .service-button {
            border: 1px solid #cbcbcb;
            color: #ffffff;
            cursor: pointer;
            background: linear-gradient(to bottom, #ff7f6a 0%,#f2553b 100%);
            padding: 0px 17px;
            height: 28px;
            line-height: 28px;
            overflow: visible;
            -webkit-border-radius: 4px;
            float: left;
            text-align: center;
            font-size: 15px;
            box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
            margin: 0px 15px 0px 15px;
        }

        .case-footer {
            padding-left: 200px;
            padding-top: 10px;
            height: 40px;
        }

        .image{
            padding-right: 15px;
            float:right;

        }

        .information{
            width:400px;
            height:auto;
            border-spacing:0;
            border-collapse:collapse;
            word-break: break-all;
        }
        .inforleft{
            width:30%;
            background-color:#AAAAAA;
            padding:10px;
        }
        .inforright{
            width:70%;
            background-color:#F5F5F5;
            padding:10px;
        }
    </style>
    <script>
        $(document).ready(function () {
            $("#noteButton").html("<a href='javascript:void(0)' onclick='showNote()'>檢視完整備註紀錄</a>(" + $(".log-content").length + ")");
            //$("#noteArea").hide();
            if ($(".log-content").length > 0) {
                $(".log-content:lt(" + ($(".log-content").length - 1) + ")").hide();
            }
            else {
                $("#noteButton").hide();
            }

            $("#imageClose").click(function () {
                $.unblockUI();
            })

            $.ajax({
                type: "POST",
                url: "ServiceDetail.aspx/GetSample",
                data: "{categoryId: '" + $("#ddlSubCategory").val() + "'}",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    //var result = "";
                    var cates = $.parseJSON(data.d);
                    $.each(cates.sampleList, function (i, item) {
                        $('#choseArea tbody:last').append("<tr><td align='center'>" + item.subject + "</td><td>" + item.content + "</td><td align='center'><a href='javascript:void(0)' onclick=getSample(" + item.id + ");>選擇</a></td></tr>");
                    });
                },
                error: function () {

                }
            });

            $(".tr_empty").each(function () {
                $(this).parent().find("tr").first().hide();
            });

            
            if($("#rbNoSms").is(":checked")) {
                $("#hiddenMethod").val("1");
            }

            $("#rbSms").change(
                function () {
                    if ($(this).is(':checked')) {
                        $("#hiddenMethod").val("3");
                    }
                });
            $("#rbNoSms").change(function () {
                if ($(this).is(':checked')) {
                    $("#hiddenMethod").val("1");
                }
            });


        });

        function check() {
            if ($("#hiddenRealStatus").val() == "0") {
                alert("此案件尚未認領，無法變更資訊");
                return false;
            }
            else {
                if (confirm("確定要更新案件？")) {
                    if ($("#rbtransfer").is(':checked') || $("#rbcompelete").is(':checked')) {
                        if ($("#ddCategory").val() == "" || $("#ddCategory").val()=="0") {
                            alert("請填寫問題分類！");
                            return false;
                        } else if ($("#ddSubCategory").val() == "0" || $("#ddSubCategory").val() == "") {
                            alert("請填寫問題主因！");
                            return false;
                        }
                        else if ($("#rbtransfer").is(':checked') && $("#serviceContent").val() == "") {
                            alert("請填寫備註資訊");
                            return false;
                        }
                        else {
                            return true;
                        }
                    }
                    else {
                        return true;
                    }
                    
                }
                else {
                    return false;
                }
            }
        }

        function getSubCategory() {
            var ddlMain = $("#<%=ddCategory.ClientID%>");
            var ddlMainSelected = $("#<%=ddCategory.ClientID%> :selected");
            var ddlSub = $("#<%=ddSubCategory.ClientID%>");

            $.ajax({
                type: "POST",
                url: "ServiceDetail.aspx/GetServiceMessageCategory",
                data: "{categoryId: " + ddlMain.val() + "}",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    var cates = $.parseJSON(data.d);
                    ddlSub.empty();
                    ddlSub.append($('<option></option>').val('').html('請選擇'));
                    $.each(cates, function (i, item) {
                        ddlSub.append($('<option></option>').val(item.CategoryId).html(item.CategoryName));
                    });
                },
                error: function () {
                    ddlSub.empty();
                    ddlSub.append($('<option></option>').val('').html('請選擇'));
                }
            });
        }

        function getddlSubCategory() {
            var ddlMain = $("#<%=ddlCategory.ClientID%>");
            var ddlMainSelected = $("#<%=ddlCategory.ClientID%> :selected");
            var ddlSub = $("#<%=ddlSubCategory.ClientID%>");

            $.ajax({
                type: "POST",
                url: "ServiceDetail.aspx/GetServiceMessageCategory",
                data: "{categoryId: " + ddlMain.val() + "}",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    var cates = $.parseJSON(data.d);
                    ddlSub.empty();
                    ddlSub.append($('<option></option>').val('').html('請選擇'));
                    $.each(cates, function (i, item) {
                        ddlSub.append($('<option></option>').val(item.CategoryId).html(item.CategoryName));
                    });
                },
                error: function () {
                    ddlSub.empty();
                    ddlSub.append($('<option></option>').val('').html('請選擇'));
                }
            });
        }
        function getSubValue() {
            $("#hiddenSubCategory").val($("#ddSubCategory").val());
        }


        function getSample(id) {
            //取資料Content
            $.ajax({
                type: "POST",
                url: "ServiceDetail.aspx/GetSampleContent",
                data: "{id: " + id + "}",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    var cates = $.parseJSON(data.d);

                    $("#customerTxtArea").val(cates);
                },
                error: function () {
                }
            });
            $.unblockUI();
            $("#PopCannedMessage").hide();
        }

        function getSampleList() {
            $.ajax({
                type: "POST",
                url: "ServiceDetail.aspx/GetSample",
                data: "{categoryId: '" + $("#ddlSubCategory").val() + "'}",
                //data: "{serviceNo: '" + $("#lblServiceNo").val() + "'}",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    var cates = $.parseJSON(data.d);
                    $('#choseArea tbody:last').empty();
                    $('#choseArea tbody:last').append("<th width='30%'>訊息標題</th><th>訊息內容</th><th width='20%'></th>")
                    $.each(cates.sampleList, function (i, item) {
                        $('#choseArea tbody:last').append("<tr><td align='center'>" + item.subject + "</td><td>" + item.content + "</td><td align='center'><a href='javascript:void(0)' onclick=getSample(" + item.id + ");>選擇</a></td></tr>");
                    });
                },
                error: function () {

                }
            });
        }



        function sendMessage() {
            if ($("#hiddenRealStatus").val() == "0") {
                alert("此案件尚未被認領，無法回復客服紀錄");
                return false;
            }
            else {
                if ($("#customerTxtArea").val() == "" && $('#customerfile').val() == "") {
                    alert("請輸入回覆內容或是上傳檔案！");
                    return false;
                }
                else if ($("#hiddenPost").val() != "" && $('#hiddenPost').val() == $("#customerTxtArea").val()) {
                    initial();
                    return false;
                }
                else {
                    var formData = new FormData();
                    formData.append("serviceNo", $("#lblServiceNo").text());
                    formData.append("customerTxtArea", $("#customerTxtArea").val());
                    formData.append("method", $("#hiddenMethod").val());
                    formData.append("images", $('#customerfile')[0].files[0]);
                    formData.append("mailTo", $("#hiddenMail").val());
                    formData.append("mobileTo", $("#hiddenMobile").val());
                    formData.append("userId", $("#hiddenUserId").val());


                    $.ajax({
                        type: "POST",
                        //url:'/ControlRoom/vbs/membership/VendorDetailDataImgSet',
                        url: "/ControlRoom/api/CustomerService/Upload",
                        data: formData,
                        contentType: false,
                        processData: false,
                        success: function (result, status, xhr) {
                            if (xhr.status == 200) {
                                if (result.Code == 1000) {
                                    //alert('儲存成功。');
                                    $("#hiddenPost").val($("#customerTxtArea").val());
                                    if (result.Data.image != null) {
                                        $("#gridCustomer tr:last").after("<tr><td align='center'><span>" + result.Data.name + "<br />" + result.Data.account + "</span><br /><span>" + result.Data.date + "</span></td><td align='left' style='word-break: break-all;'>" + result.Data.content.replace(/\r\n/g, "<br />") + "<br />查看附件<a href='" + result.Data.link + "' target='_blank'>" + result.Data.image + "</a></td>");
                                    }
                                    else {
                                        $("#gridCustomer tr:last").after("<tr><td align='center'><span>" + result.Data.name + "<br />" + result.Data.account + "</span><br /><span>" + result.Data.date + "</span></td><td align='left' style='word-break: break-all;'>" + result.Data.content.replace(/\r\n/g, "<br />") + "</td>");
                                    }
                                    initial();

                                }
                                else if (result.Code == 1140) {
                                    alert(result.Message);
                                    initial();
                                }
                                else {
                                    alert('儲存失敗，請稍後在試。');
                                    initial();

                                }
                            }
                        },
                        error: function (error) {
                            alert('儲存失敗，請稍後在試。');
                            initial();

                        }
                    });


                    return false;
                }
            }
        }

        function initial() {
            $("#customerTxtArea").val("");
            $("#receiveArea").hide();
            $("#receiveArea").val("");
            $("#customerfile").val("");

        }

        function showNote() {
            if ($(".log-content").length > 0) {
                if ($(".log-content:gt(0)").css('display') == 'none') {
                    $("#noteButton").html("<a href='javascript:void(0)' onclick='showNote()'>隱藏備註紀錄</a>");
                }
                else {
                    $("#noteButton").html("<a href='javascript:void(0)' onclick='showNote()'>檢視完整備註紀錄</a>(" + $(".log-content").length + ")");
                }
                $(".log-content:lt(" + ($(".log-content").length - 1) + ")").toggle("slow");
            }
            //$("#noteArea").toggle("slow");
        }

        function claim()
        {
            if (confirm("確定要認領案件？")) {
                return true;
            }
            else {
                return false;
            }
        }

        function Ship(orderGuid) {
            $.ajax({
                type: "POST",
                url: "ServiceDetail.aspx/GetShippingOrder",
                data: "{orderGuid: '" + orderGuid + "'}",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    var cates = $.parseJSON(data.d);
                    if (cates.Success == true) {
                        //如果這是單筆OrderShip時，跳過選擇shipNo畫面
                        if (cates.Count == 1) {
                            for (var i = 0; i < cates.ShippingOrder.length; i++) {
                                html = "<table class='information'>";
                                html = html + "<tr><td class='inforleft'></td><td class='inforright' style='text-align:right'><a href='javascript:void(0)' onclick='ClosePop()'><img src='/images/close.gif'></a></td></tr>";
                                html = html + "<tr><td class='inforleft'>出貨日期</td><td class='inforright'>" + cates.ShippingOrder[i].ShippingDate + "</td></tr>";
                                html = html + "<tr><td class='inforleft'>運單編號</td><td class='inforright'>" + cates.ShippingOrder[i].ShippingNo + "</td></tr>";
                                html = html + "<tr><td class='inforleft'>物流公司</td><td class='inforright'>" + cates.ShippingOrder[i].LogisticsCompany + "</td></tr>";
                                html = html + "<tr><td class='inforleft'>出貨備註</td><td class='inforright'>" + cates.ShippingOrder[i].ShippingRemark + "</td></tr>";
                                html = html + "<tr><td class='inforleft'>最後修改日期</td><td class='inforright'>" + cates.ShippingOrder[i].LastModifyDate + "</td></tr>";
                                html = html + "<tr><td class='inforleft'>最後修改人</td><td class='inforright'>" + cates.ShippingOrder[i].LastModifyAccount + "</td></tr>";
                                html = html + "</table>";
                            }
                            $.blockUI({ message: html, css: { width: '400px', cursor: 'default' }, overlaycss: { cursor: 'default' }, onOverlayClick: $.unblockUI });
                        } else {
                            html = "<table align='center'>";
                            html = html + "<tr><td align='center'>運單編號</td></tr>";
                            target = "";
                            for (var i = 0; i < cates.ShippingOrder.length; i++) {

                                target = target + "<div id='target_"+cates.ShippingOrder[i].ShippingNo+"' style='display:none'><table  class='information'>";
                                target = target + "<tr><td class='inforleft'></td><td class='inforright' style='text-align:right'><a href='javascript:void(0)' onclick='ClosePop()'><img src='/images/close.gif'></a></td></tr>";
                                target = target + "<tr><td class='inforleft'>出貨日期</td><td class='inforright'>" + cates.ShippingOrder[i].ShippingDate + "</td></tr>";
                                target = target + "<tr><td class='inforleft'>運單編號</td><td class='inforright'>" + cates.ShippingOrder[i].ShippingNo + "</td></tr>";
                                target = target + "<tr><td class='inforleft'>物流公司</td><td class='inforright'>" + cates.ShippingOrder[i].LogisticsCompany + "</td></tr>";
                                target = target + "<tr><td class='inforleft'>出貨備註</td><td class='inforright'>" + cates.ShippingOrder[i].ShippingRemark + "</td></tr>";
                                target = target + "<tr><td class='inforleft'>最後修改日期</td><td class='inforright'>" + cates.ShippingOrder[i].LastModifyDate + "</td></tr>";
                                target = target + "<tr><td class='inforleft'>最後修改人</td><td class='inforright'>" + cates.ShippingOrder[i].LastModifyAccount + "</td></tr>";
                                target = target + "</table></div>";  
                                
                                html = html + "<tr><td align='center'><input type='button' value='"+cates.ShippingOrder[i].ShippingNo+"' onclick=Test('"+cates.ShippingOrder[i].ShippingNo+"')></td></tr>";
                                                            
                            }
                            html = html + "</table>";
                            $.blockUI({ message: target+html, css: { width: '400px', cursor: 'default' }, overlaycss: { cursor: 'default' }, onOverlayClick: $.unblockUI });
                        }
                    } else {
                        alert("查無出貨資訊！");
                    }

                },
                error: function () {

                }
            });
        }
        function Test(shipNo) {
            var html = $("#target_" + shipNo).html();
            $.unblockUI();
            
            $.blockUI({ message: html, css: { width: '400px', cursor: 'default' }, overlaycss: { cursor: 'default' }, onOverlayClick: $.unblockUI });
        }

        function gotoShipUrl(obj) {
            var shipNo = $(obj).parent().find("span#shipNo");
            copy(shipNo.html());
            var shipUrl = $(obj).parent().find("span#shipUrl");
            //window.open(shipUrl.html());
            $.confirm({
                type: 'red',
                title: '訊息 ',
                content: '物流單號已複製，前往網站查詢物流進度。',
                boxWidth: 300,
                useBootstrap: false,
                animation: 'zoom',
                closeAnimation: 'left',
                buttons: {
                    cancel: {
                        text: '取消'
                    }, specialKey: {
                        text: '開啟',
                        btnClass: 'btn-red any-other-class',
                        action: function () {
                            window.open(shipUrl.html());
                        }
                    }
                }
            });
        }
        function copy(s) {
            $('body').append('<textarea id="clip_area"></textarea>');

            var clip_area = $('#clip_area');

            clip_area.text(s);
            clip_area.select();

            document.execCommand('copy');

            clip_area.remove();
        }
        function Return(orderGuid) {
            $.ajax({
                type: "POST",
                url: "ServiceDetail.aspx/GetReturnOrder",
                data: "{orderGuid: '" + orderGuid + "'}",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    var cates = $.parseJSON(data.d);
                    if (cates.Success == true) {
                        html = "<table class='information'>";
                        html = html + "<tr><td class='inforleft'></td><td class='inforright' style='text-align:right'><a href='javascript:void(0)' onclick='ClosePop()'><img src='/images/close.gif'></a></td></tr>";
                        html = html + "<tr><td class='inforleft'>申請日期</td><td class='inforright'>" + cates.ReturnApplicationTime + "</td></tr>";
                        html = html + "<tr><td class='inforleft'>申請退貨</td><td class='inforright'>" + cates.DealName + "</td></tr>";
                        html = html + "<tr><td class='inforleft'>退貨原因</td><td class='inforright'>" + cates.ReturnReason + "</td></tr>";
                        html = html + "<tr><td class='inforleft'>廠商處理時間</td><td class='inforright'>" + cates.VendorProcessingTime + "</td></tr>";
                        html = html + "<tr><td class='inforleft'>廠商處理進度</td><td class='inforright'>" + cates.VendorProcessingProgress + "</td></tr>";
                        html = html + "<tr><td class='inforleft'>退貨進度</td><td class='inforright'>" + cates.ReturnSchedule + "</td></tr>";
                        html = html + "<tr><td class='inforleft'>完成退貨</td><td class='inforright'>" + cates.ReturnComplete + "</td></tr>";
                        html = html + "<tr><td class='inforleft'>折讓單狀態</td><td class='inforright'>" + cates.DiscountDescription + "</td></tr>";
                        html = html + "<tr><td class='inforleft'>結案狀態</td><td class='inforright'>" + cates.ClosedDescription + "</td></tr>";
                        html = html + "</table>";
                        $.blockUI({ message: html, css: { width: '400px', cursor: 'default' }, overlaycss: { cursor: 'default' }, onOverlayClick: $.unblockUI });
                    } else {
                        alert("查無退貨紀錄！");
                    }
                },
                error: function () {

                }
            });
        }

        function Change(orderGuid) {
            $.ajax({
                type: "POST",
                url: "ServiceDetail.aspx/GetChangeOrder",
                data: "{orderGuid: '" + orderGuid + "'}",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    var cates = $.parseJSON(data.d);
                    if (cates.Success == true) {
                        html = "<table class='information'>";
                        html = html + "<tr><td class='inforleft'></td><td class='inforright' style='text-align:right'><a href='javascript:void(0)' onclick='ClosePop()'><img src='/images/close.gif'></a></td></tr>";
                        html = html + "<tr><td class='inforleft'>申請日期</td><td class='inforright'>" + cates.ChangeApplicationTime + "</td></tr>";
                        html = html + "<tr><td class='inforleft'>換貨原因退貨</td><td class='inforright'>" + cates.ChangeReason + "</td></tr>";
                        html = html + "<tr><td class='inforleft'>廠商處理時間</td><td class='inforright'>" + cates.VendorProcessingTime + "</td></tr>";
                        html = html + "<tr><td class='inforleft'>廠商處理進度</td><td class='inforright'>" + cates.VendorProcessingProgress + "</td></tr>";
                        html = html + "<tr><td class='inforleft'>結案狀態</td><td class='inforright'>" + cates.ClosedState + "</td></tr>";
                        html = html + "</table>";
                        $.blockUI({ message: html, css: { width: '400px' }, overlaycss: { cursor: 'default' }, onOverlayClick: $.unblockUI });
                    } else {
                        alert("查無換貨紀錄！");
                    }

                },
                error: function () {

                }
            });

        }

        function CanMessage()
        {
            $.blockUI({ message: $("#PopCannedMessage"), css: { width: '600px', top: '20%', cursor: 'default' }, overlaycss: { cursor: 'default' }, onOverlayClick: $.unblockUI });
        }
        
        function ClosePop() {
            $.unblockUI();
        }
       


    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%--會員基本資料--%>
    <% if(!conf.OrderDetailV2LinkVisable) {%>
    <asp:Panel ID="memPanel" runat="server" Visible="true" HorizontalAlign="Center" Width="1200px">
        <table align="center">
            <tr>
                <td class="style4">
                    <fieldset>
                        <legend>會員基本資料</legend>
                        <table style="width: 467px">
                            <tr>
                                <td class="style5" colspan="2">帳號:&nbsp;&nbsp;
                                    <asp:Label ID="lblEmail" runat="server"></asp:Label>
                                    <asp:Literal ID="litGuestMemberMark" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td class="style7">姓名:&nbsp;&nbsp;
                                                <asp:HyperLink ID="hkUserName" runat="server" Target="_blank"></asp:HyperLink>
                                </td>
                                <td class="style6">連絡電話:&nbsp;&nbsp;
                                                <asp:Label ID="lblMobile" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="style5" colspan="2">連絡Email:&nbsp;&nbsp;
                                                <asp:Literal ID="lblUserEmail" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td class="style5" colspan="2">地址:&nbsp;&nbsp;
                                                <asp:Label ID="lblAddress" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="style5" colspan="2">
									<div style="width: 640px; display: flex; flex-direction: row">
										<div style="padding-right: 20px">
											登入方式:
										</div>
										<div style="flex: 1">
											<asp:GridView ID="gvLoginType" runat="server" GridLines="Horizontal" BorderWidth="1"
												ShowFooter="false" BorderColor="Silver"
												ShowHeader="true" AutoGenerateColumns="false" Width="80%" CellPadding="1" CellSpacing="1"
												OnRowDataBound="gvLoginType_RowDataBound">
												<Columns>
													<asp:TemplateField HeaderText="登入類型" HeaderStyle-Width="35%" ItemStyle-HorizontalAlign="Center">
														<ItemTemplate>
															<asp:Label ID="lblExternalOrg" runat="server"></asp:Label>
														</ItemTemplate>
													</asp:TemplateField>
													<asp:BoundField HeaderText="編號" DataField="ExternalUserId" HeaderStyle-Width="65%"
														HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" />
												</Columns>
												<EmptyDataTemplate>
													無串接資訊
												</EmptyDataTemplate>
											</asp:GridView>
										</div>
									</div>
                                </td>
                            </tr>
                            <tr>
                                <td class="style5" colspan="2">認證手機:&nbsp;&nbsp;
                                                <asp:Label ID="lbRegisteredMobile" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td class="style7">紅利:&nbsp;&nbsp;
                                                <asp:Label ID="lblBCash" runat="server" Text="0"></asp:Label>
                                </td>
                                <td class="style6" style="display: none;">會員卡別:&nbsp;&nbsp;
                                                <asp:Label ID="lblMemberCard" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="style7">購物金: 共<asp:Literal ID="litScash" runat="server" Text="0" />
                                    <div style="padding:5px 0px 0px 20px">17Life: <asp:Literal ID="litScashE7" runat="server" /></div>
                                    <div style="padding:5px 0px 0px 20px">由Payeasy兌換: <asp:Literal ID="litScashPez" runat="server" /></div>
                                </td>
                                <td class="style6">
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <br />
    <%}%>

    
    <%--關聯案件--%>
    <asp:Panel ID="contactPanel" runat="server" Visible="true" Width="100%" align="left">
        <table width="100%">
            <tr>
                <td align="left"><font size="5"><b>關聯案件</b></font></td>
            </tr>
        </table>
        <table width="1220px" style="background: white; border-collapse: collapse; border: 1px solid #cbcbcb;" >
            <tr>
                <td align="left">
                    <asp:DropDownList runat="server" ID="ddlsearch" CssClass="adminitem" ClientIDMode="Static">
                        <asp:ListItem Text="同訂單" Value="orderId"></asp:ListItem>
                        <asp:ListItem Text="同會員" Value="mem"></asp:ListItem>
                        <asp:ListItem Text="一般接續問題" Value="generalContinue"></asp:ListItem>
                    </asp:DropDownList>
                    <asp:Button ID="btnSearch" runat="server" Text="查詢" OnClick="btnSearch_Click" />
                </td>
            </tr>
            <tr>
                <asp:GridView ID="gridContact" runat="server" OnRowDataBound="search_RowDataBound"
                    GridLines="Horizontal" ForeColor="Black" CellPadding="4" BorderWidth="1px"
                    BorderColor="#DEDFDE" BackColor="White" AutoGenerateColumns="false"
                    Font-Size="Small" Width="100%">
                    <RowStyle BackColor="White" Height="20px" HorizontalAlign="Center"></RowStyle>
                    <HeaderStyle BackColor="#6B696B" ForeColor="White" Font-Bold="True" Height="20px"
                        HorizontalAlign="Center"></HeaderStyle>
                    <Columns>
                        <asp:TemplateField HeaderText="案件編號" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="10%">
                            <ItemTemplate>
                                <asp:HyperLink ID="hyServiceNo" runat="server"></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="案件處理狀態" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="20%">
                            <ItemTemplate>
                                <asp:Label ID="lblStatus" runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="優先度" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="20%">
                            <ItemTemplate>
                                <asp:Label ID="lblPriority" runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="建立時間" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="20%">
                            <ItemTemplate>
                                <asp:Label ID="lblCreateDate" runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="處理人員" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="10%">
                            <ItemTemplate>
                                <asp:Label ID="lblUserId" runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </tr>
        </table>
    </asp:Panel>
    <br />

    <div style="margin-left: 2px;">
    <%--案件資訊--%>
    <asp:Panel ID="serviceNoPanel" runat="server" Visible="true" Width="100%" align="left">
        <asp:HiddenField ID="hiddenSubCategory" ClientIDMode="Static" runat="server" />
        <asp:HiddenField ID="hiddenRealStatus" ClientIDMode="Static" runat="server" />
        <div>
            <div class="title">
                案件資訊
            </div>
            <div class="case-info-sub">
                <div class="case-column">
                    <div style="float: left; width: 50%;">
                    <label>案件編號：</label>
                        <asp:Label ID="lblServiceNo" ClientIDMode="Static" runat="server" />
                        <asp:Label ID="lblContinue" ClientIDMode="Static" runat="server" Text="(接)" Visible="false" ForeColor="#CC0000" Font-Size="13px"></asp:Label>
                    </div>
                    <div>
                          <label>案件狀態：</label><asp:Label ID="lblStatus" ClientIDMode="Static" runat="server" />
                          <asp:Button ID="btnClaim" runat="server" Text="領取案件" OnClientClick="return claim();" OnClick="btnClaim_Click" />
                          
                    </div>
                </div>
                <div class="case-column">
                    <div style="float: left; width: 50%;">
                        <label>處理人員1：</label>
                        <span><asp:Label ID="lblWork" ClientIDMode="Static" runat="server" /></span>
                        <asp:Button runat="server" ID="btnNotify" Text="通知我" Visible="false" OnClick="btnNotify_Click" />
                    </div>
                    <div>
                        <label>處理人員2：</label>
                        <asp:DropDownList runat="server" ID="ddlSecWorkerList" ClientIDMode="Static" Visible="false">
                        </asp:DropDownList>
                        <asp:Button runat="server" ID="btnJoin" Text="加入此同仁" Visible="false" OnClick="btnJoin_Click" />
                        <span><asp:Label ID="lblSecWork" ClientIDMode="Static" runat="server" /></span>
                        <asp:Button runat="server" ID="btnSecNotify" Text="通知我" Visible="false" OnClick="btnSecNotify_Click" />
                    </div>

                </div>
                <div class="case-column">
                    <div style="float: left; width: 50%;">
                        <label>問題分類：</label>
                        <asp:DropDownList runat="server" ID="ddCategory" ClientIDMode="Static" onchange="getSubCategory();">
                        </asp:DropDownList>
                    </div>
                    <div>
                        <label>主因：</label>
                        <asp:DropDownList runat="server" ID="ddSubCategory" ClientIDMode="Static" onchange="getSubValue();">
                        </asp:DropDownList>
                    </div>

                </div>

                <% if(!conf.OrderDetailV2LinkVisable) {%>
                <div id="orderArea" runat="server" class="case-column">
                    <div style="float: left; width: 50%; height: 48px; line-height: 30px;">
                        <label>訂單編號：</label><asp:HyperLink ID="hyOrderId" runat="server" ClientIDMode="Static" /><br />
                        <asp:HyperLink ID="hyShip" runat="server" ClientIDMode="Static" />
                        <asp:Label ID="lblline1" runat="server" ClientIDMode="Static" />
                        <asp:HyperLink ID="hyReturn" runat="server" ClientIDMode="Static" />
                        <asp:Label ID="lblline2" runat="server" ClientIDMode="Static" />
                        <asp:HyperLink ID="hyChange" runat="server" ClientIDMode="Static" />
                    </div>
                    <div style="line-height: 30px; ">
                        <label>供應商：</label><asp:Label ID="lblSupplier" ClientIDMode="Static" runat="server" /><br />
                        <label>二線客服：</label><asp:Label ID="lblService" ClientIDMode="Static" runat="server" />
                    </div>
                </div>
               <div id="interestDiv" runat="server" class="case-column">
                    <div style="line-height: 30px;"> 
                        <label>訂購時間：</label><asp:HyperLink ID="lblOrderTime" runat="server" ClientIDMode="Static" /> <br />
                        <asp:Label ID="lblinterestStatement" runat="server" ClientIDMode="Static" />
                    </div>
                </div>
                <div id="orderDetail" runat="server" class="case-column" style="height: 90px">
                    <div style="float: left; width: 100%; line-height: 30px;">
                        <label>商品資訊：</label><asp:HyperLink ID="hyOrderInfo" runat="server" ClientIDMode="Static" />
                        <asp:ImageButton ID="ImageBack" ImageUrl="~/images/icons/icon_find.gif" runat="server"></asp:ImageButton>
                    </div>
                    <div style="float: left; width: 50%; height: 30px; line-height: 30px;">
                        <label>訂購人姓名：</label><asp:Label ID="lblOrderName" ClientIDMode="Static" runat="server" />
                    </div>
                    <div style="float: left; line-height: 30px; ">
                        <label>訂購人電話：</label><asp:Label ID="lblOrderMobile" runat="server" ClientIDMode="Static" />
                    </div>
                    <div style="float: left; width: 100%; height: 30px; line-height: 30px;">
                        <label>訂購人地址：</label><asp:Label ID="lblOrderAddress" ClientIDMode="Static" runat="server" />
                    </div>
                </div>
                <%}%>

                <div class="case-column">
                    <div>
                        <label>來源平台：</label><asp:Label ID="lblIssueFromType" ClientIDMode="Static" runat="server" />
                        <font color="red"><asp:Label ID="lblPChomeWarn" ClientIDMode="Static" runat="server" Text="(注意：此案件來自PChome，僅能回復一次)" Visible="false"/></font>
                    </div>
                </div>
                <div class="case-column">
                    <div>
                        <label>問題等級：</label>
                        <asp:RadioButton ID="rbNormal" ClientIDMode="Static" GroupName="questoinLevel" runat="server" />一般
                        <asp:RadioButton ID="rbQick" ClientIDMode="Static" GroupName="questoinLevel" runat="server" />急
                        <asp:RadioButton ID="rbFast" ClientIDMode="Static" GroupName="questoinLevel" runat="server" /><font color="red">特急</font>
                    </div>
                </div>
                <div class="case-column">
                    <div>
                        <label>案件來源：</label>
                        <asp:RadioButton ID="rbOnline" ClientIDMode="Static" GroupName="questionFrom" runat="server" Enabled="false" />線上詢問
                        <asp:RadioButton ID="rbMobile" ClientIDMode="Static" GroupName="questionFrom" runat="server" Enabled="false" />來電
                        <asp:RadioButton ID="rbMail" ClientIDMode="Static" GroupName="questionFrom" runat="server" Enabled="false" />mail
                    </div>
                </div>
                <div class="case-column">
                    <div>
                        <label>案件處理：</label>
                        <asp:RadioButton ID="rbprocess" ClientIDMode="Static" GroupName="questionDeal" runat="server" />處理中
                        <asp:RadioButton ID="rbtransfer" ClientIDMode="Static" GroupName="questionDeal" runat="server" />轉單
                        <asp:RadioButton ID="rbcompelete" ClientIDMode="Static" GroupName="questionDeal" runat="server" />已結案<span style="color:#cbcbcb"> (案件結案後客戶即無法再留言) </span>
                    </div>
                </div>

            </div>
        </div>
    </asp:Panel>
    <br />
    
    <div style="float:left;width: 550px">
    <%--案件備註紀錄--%>
    <asp:Panel ID="InsideMessagePanel" runat="server" Visible="true" Width="100%" align="center">
        <table style="width: 100%; background: white; border-collapse: collapse; border: 1px solid #cbcbcb;">
            <tr><td>
                <div style="padding-left:10px;">
                    <span style="float: left; padding: 10px 5px 10px 0;">案件備註紀錄</span>
                    <br/><br/>
                    <div id="noteArea" style="padding: 0 30px 5px 0px;">
                     <asp:Repeater ID="rep_Note" runat="server" Visible="true" OnItemDataBound="note_Bound">
                         <ItemTemplate>
                             <div class="log-content">
                                 <span style="padding-right: 10px">
                                     <asp:Label ID="lblName" runat="server" /></span>
                                 <span class="tag_place">
                                     <asp:Label ID="lblStatus" runat="server" /></span>
                                 <span style="float: right;">
                                     <asp:Label ID="lblCreateTime" runat="server" /></span>
                                 <div class="content-word">
                                     <asp:Label ID="lblContent" runat="server" style="word-break: break-all;"/>
                                 </div>
                                 <asp:Label ID="lblDe" runat="server" /><asp:HyperLink ID="hyFile" runat="server" />
                                 <span class="inside_memo">
                                     <asp:Label ID="lblInsideMemo" runat="server" /></span>
                             </div>
                         </ItemTemplate>
                     </asp:Repeater>
                    </div>
                     <span id="noteButton" style="display:block;float:right"><a href="javascript:void(0)" onclick="showNote()">檢視完整備註紀錄</a></span>
                    <br/><br/>
                    <div style="padding: 10px 0px 5px 0px; float: left;">回覆備註(供內部人員及供應商查看)</div>
                    <div>
                        <asp:FileUpload ID="txtImageUrl" runat="server" />
                    </div>
                    <div style="padding-bottom: 15px;">
                        <textarea id="serviceContent" clientidmode="Static" rows="10" runat="server" style="width:98%"></textarea>
                    </div>
                    <div align="center" style="margin-bottom: 20px;">
                        <asp:Button ID="btnSubmit" runat="server" Text="確定" OnClientClick="return check();" OnClick="btnSubmit_Click" />
                    </div>
                </div>
            </td></tr>
        </table>
    </asp:Panel>
    </div>
    
    <div style="float:left;width: 650px; margin-left:20px;">
    <%-- 客服回覆 --%>
    <asp:Panel ID="customerPanel" runat="server" Visible="true" Width="100%" align="center">
        <table style="width: 100%; background: white; border-collapse: collapse; border: 1px solid #cbcbcb;">
            <asp:HiddenField ID="hiddenMobile" ClientIDMode="Static" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hiddenMail" ClientIDMode="Static" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hiddenUserId" ClientIDMode="Static" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hiddenMethod" ClientIDMode="Static" runat="server"></asp:HiddenField>
            <input type="hidden" ID="hiddenPost">
            <tr>
                <td align="center">
                    <span style="float: left; padding: 10px 5px 10px 10px;">客服回覆紀錄</span>
                    <asp:GridView ID="gridCustomer" ClientIDMode="Static" runat="server" ForeColor="Black" CellPadding="4" BorderWidth="1px"
                        BorderColor="#DEDFDE" BackColor="White" EmptyDataText="無符合的資料" AutoGenerateColumns="false" Width="95%"
                        OnRowDataBound="gvCustomer_RowDataBound">
                        <Columns>
                            <asp:TemplateField HeaderText="回覆者" HeaderStyle-Width="20%" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:Label ID="lblCustomerc" runat="server"></asp:Label>
                                    <asp:Label ID="lblCreateTimec" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="內容" HeaderStyle-Width="80%">
                                <ItemTemplate>
                                    <asp:Label ID="lblContentc" runat="server" style="word-break: break-all;"></asp:Label>
                                    <br />
                                    <asp:Label ID="lblDec" runat="server" ItemStyle-HorizontalAlign="Center"></asp:Label>
                                    <asp:HyperLink ID="lblhyFile" runat="server"></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>
                        <EmptyDataTemplate>
                            <tr class="tr_empty">
                                <th scope="col" style="width: 20%">回覆者</th>
                                <th scope="col" style="width: 80%">內容</th>
                            </tr>
                        </EmptyDataTemplate>
                    </asp:GridView>
                    <span style="float: left; padding: 10px 5px 10px 15px; height: 20px"></span>
                </td>
            </tr>
            <tr>
                <td style="float: left; padding: 5px 5px 5px 10px;">是否發送簡訊
                <asp:RadioButton ID="rbSms" ClientIDMode="Static" GroupName="sms" value="3" runat="server" />是
                <asp:RadioButton ID="rbNoSms" ClientIDMode="Static" GroupName="sms" value="1" runat="server" />否
                </td>
            </tr>
            <tr>
                <td width="97%" style="float: left; padding: 5px 5px 5px 10px;">回覆內容&nbsp;<input type="button" value="罐頭訊息範例" 
                    onclick="CanMessage();">&nbsp;<input type="file" id="customerfile" value="上傳附件"><br />
                    <textarea id="customerTxtArea" clientidmode="Static" rows="10" runat="server" style="width: 100%"></textarea>
                </td>
            </tr>
            <tr>
                <td class="service" align="center">
                    <asp:Button ID="btnCustomer" runat="server" Text="送出" OnClientClick="return sendMessage()"  style="margin-bottom: 10px;"></asp:Button>
                </td>
            </tr>
        </table>
    </asp:Panel>
    </div>

    <%-- 罐頭訊息範例 --%>
        <div id="PopCannedMessage" style="display: none; width: 600px;max-height:600px;overflow-y: auto !important;overflow-x:hidden">
            <asp:Panel ID="Panel3" runat="server" Width="600px" Style="background-color: #DDDDDD; border: solid 1px Gray; color: Black">

                <table width="100%" style="align-content: center;">
                    <tr>
                        <td>
                            <asp:ImageButton ID="imageClose" CssClass="image" ClientIDMode="Static" ImageUrl="~/images/close.gif" runat="server" ></asp:ImageButton>
                        </td>
                    </tr>

                    <tr>
                        <td><span style="display:inline-block;width:20px"></span>問題分類：
                            <asp:DropDownList runat="server" ID="ddlCategory" ClientIDMode="Static" onchange="getddlSubCategory();">
                            </asp:DropDownList>
                            主因：
                            <asp:DropDownList runat="server" ID="ddlSubCategory" ClientIDMode="Static" onchange="getSampleList();">
                            </asp:DropDownList>

                        </td>
                    </tr>

                    <tr>
                        <td>
                            <table width="95%" align="center" border="1" id="choseArea" style="border-collapse: collapse; border: 1px solid #00;">
                                <tr>
                                    <th width="30%">訊息標題</th>
                                    <th width="50%">訊息內容</th>
                                    <th width="20%"></th>
                                </tr>
                            </table>
                        </td>
                    </tr>

                </table>
            </asp:Panel>
        </div>

    </div>

</asp:Content>

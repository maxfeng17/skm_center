﻿using System;
using LunchKingSite.DataOrm;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using System.Linq;
using System.Web.Services;
using System.Web.UI.WebControls;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;
using LunchKingSite.BizLogic.Facade;
using System.Collections.Generic;
using LunchKingSite.Core.UI;
using System.Web;
using LunchKingSite.Core.Enumeration;

namespace LunchKingSite.Web.ControlRoom.Service
{
    public partial class NotClaimed : RolePage, INotClaimedView
    {
        #region props
        private static LunchKingSite.Core.IServiceProvider sp = ProviderFactory.Instance().GetProvider<LunchKingSite.Core.IServiceProvider>();
        protected IOrderProvider op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
        protected IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        private NotClaimedPresenter _presenter;


        public NotClaimedPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        public int PageSize
        {

            get { return ucPager.PageSize; }
            set { ucPager.PageSize = value; }

        }
        public int CurrentPage
        {
            get
            {
                return ucPager.CurrentPage;
            }
        }

        public int SearchCategory
        {
            get
            {
                int category = 0;
                int.TryParse(ddlCategory.SelectedValue, out category);
                return category;
            }
        }

        public Guid SearchOrderGuid
        {
            get
            {
                Guid orderGuid = Guid.Empty;
                if (!string.IsNullOrEmpty(searchOrderId.Text))
                {
                    orderGuid = op.OrderGuidGetById(searchOrderId.Text.Trim());
                }
                return orderGuid;
            }
        }

        public string SearchServiceNo
        {
            get
            {
                return searchServiceNo.Text;
            }
        }


        public string Mail
        {
            get
            {
                return searchMail.Text.Trim();
            }
        }

        public string SDate
        {
            get
            {
                return tbOS.Text;
            }
        }

        public string EDate
        {
            get
            {
                return tbOE.Text;
            }
        }

        public string ChoseBox
        {
            get
            {
                return hiddenChose.Value;
            }
        }
        public int UserId
        {
            get { return MemberFacade.GetUniqueId(User.Identity.Name); }
        }







        #endregion props
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                Presenter.OnViewInitialized();
                //綁定dropdownlist參數
                var entityCategory = sp.GetCustomerServiceCategory().Select(x => new { Text = x.CategoryName, Value = x.CategoryId.ToString() });
                ddlCategory.Items.Clear();
                ddlCategory.Items.Add(new ListItem("請選擇", ""));
                foreach (var item in entityCategory)
                {
                    ddlCategory.Items.Add(new ListItem(item.Text, item.Value));
                }
            }

            _presenter.OnViewLoaded();

        }


        #region event
        public event EventHandler AddClaimed;
        public event EventHandler Search;
        public event EventHandler<DataEventArgs<int>> ClaimedCount;
        public event EventHandler<DataEventArgs<int>> ClaimedPageChanged;
        #endregion

        protected void initial()
        {
            ddlCategory.SelectedValue = "";
            tbOS.Text = "";
            tbOE.Text = "";
            searchMail.Text = "";
            searchOrderId.Text = "";


        }

        protected void btnClaimed_Click(object sender, EventArgs e)
        {
            initial();
            if (this.AddClaimed != null)
            {
                this.AddClaimed(this, e);
            }

            ucPager.ResolvePagerView(CurrentPage, true);
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            if (this.Search != null)
            {
                this.Search(this, e);
            }
            ucPager.ResolvePagerView(CurrentPage, true);
        }

        protected void notClaimed_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.DataItem is ViewCustomerServiceList)
            {
                ViewCustomerServiceList item = (ViewCustomerServiceList)e.Row.DataItem;
                HyperLink hpServiceNo = ((HyperLink)e.Row.FindControl("hpServiceNo"));
                Label lblMessageType= ((Label)e.Row.FindControl("lblMessageType"));
                Label lblSource = ((Label)e.Row.FindControl("lblSource"));
                HyperLink hyName = ((HyperLink)e.Row.FindControl("hyName"));
                Label lblEmail = ((Label)e.Row.FindControl("lblEmail"));
                HyperLink hpOrderId = ((HyperLink)e.Row.FindControl("hpOrderId"));
                Label lblCategory = ((Label)e.Row.FindControl("lblCategory"));
                Label lblCreateDate = ((Label)e.Row.FindControl("lblCreateDate"));

                hpServiceNo.Text = item.ServiceNo;
                hpServiceNo.NavigateUrl = "ServiceDetail.aspx?serviceNo=" + item.ServiceNo;
                hpServiceNo.Target = "_blank";
                lblMessageType.Text= Helper.GetEnumDescription((messageConvert)item.MessageType);
                lblSource.Text= Helper.GetEnumDescription((Source)item.Source);
                lblCategory.Text = item.ServiceNo;
                hyName.Text = item.Name;
                if (item.UserId != null)
                {
                    hyName.NavigateUrl = "~/controlroom/User/users_edit.aspx?username=" + HttpUtility.UrlEncode(MemberFacade.GetUserEmail(item.UserId.Value));
                    hyName.Target = "_blank";

                    Member mem = mp.MemberGetbyUniqueId(item.UserId.Value);
                    if (mem.IsLoaded)
                    {
                        if (mem.UserName.ToLower().Contains("@mobile"))
                        {
                            MobileMember mobilemem = mp.MobileMemberGet(item.UserId.Value);
                            if (mobilemem.IsLoaded)
                            {
                                lblEmail.Text = mobilemem.MobileNumber;
                            }
                        }
                        else
                        {
                            lblEmail.Text = mem.UserName;
                        }
                    }
                }
                else
                {
                    lblEmail.Text = item.Email;
                }
                string deliveryType = string.Empty;
                if (item.OrderGuid != null)
                {

                    if (item.DeliveryType == (int)DeliveryType.ToShop)
                    {
                        deliveryType = "(憑證)";
                    }
                    else
                    {
                        deliveryType = "(宅配)";
                    }
                    hpOrderId.Text = item.OrderId;
                    hpOrderId.NavigateUrl = "~/controlroom/Order/order_detail.aspx?oid=" + item.OrderGuid.Value.ToString();
                    hpOrderId.Target = "_blank";
                }

                lblCategory.Text = item.MainCategoryName + "<br />" + deliveryType;
                lblCreateDate.Text = item.CreateTime.ToString("yyyy/MM/dd HH:mm:ss");

            }
        }

        protected void ClaimedUpdateHandler(int pageNumber)
        {
            if (this.ClaimedPageChanged != null)
            {
                this.ClaimedPageChanged(this, new DataEventArgs<int>(pageNumber));
            }
        }
        protected int GetClaimedCount()
        {
            DataEventArgs<int> e = new DataEventArgs<int>(0);
            if (ClaimedCount != null)
            {
                ClaimedCount(this, e);
                hiddenChose.Value = "";
            }
            return e.Data;
        }

        public void MessageList(ViewCustomerServiceListCollection messageList)
        {
            divNotClaimed.Visible = true;
            gridNotClaimed.DataSource = messageList.OrderBy(p=>p.CreateTime);
            gridNotClaimed.DataBind();
        }

    }
}
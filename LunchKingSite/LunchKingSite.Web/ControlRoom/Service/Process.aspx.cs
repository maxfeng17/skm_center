﻿using System;
using LunchKingSite.DataOrm;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using System.Linq;
using System.Web.Services;
using System.Web.UI.WebControls;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;
using LunchKingSite.BizLogic.Facade;
using System.Collections.Generic;
using LunchKingSite.Core.UI;
using LunchKingSite.Core.Enumeration;
using System.Web;
using System.Drawing;

namespace LunchKingSite.Web.ControlRoom.Service
{
    public partial class Process : RolePage, IProcessView
    {
        #region props
        protected static LunchKingSite.Core.IServiceProvider sp = ProviderFactory.Instance().GetProvider<LunchKingSite.Core.IServiceProvider>();
        protected static IOrderProvider op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
        protected static IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        protected static IHumanProvider hp = ProviderFactory.Instance().GetProvider<IHumanProvider>();
        protected static ISellerProvider sellerProv = ProviderFactory.Instance().GetProvider<ISellerProvider>();


        private ProcessPresenter _presenter;
        private const string departId = "C001";
        private const string subDepartId = "C002";


        public ProcessPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        public int PageSize
        {
            get { return ucPager.PageSize; }
            set { ucPager.PageSize = value; }
        }
        public int CurrentPage
        {
            get
            {
                return ucPager.CurrentPage;
            }
        }

        public string SearchServiceNo
        {
            get
            {
                return searchSericeNo.Text.Trim();
            }
        }

        public Guid SearchOrderGuid
        {
            get
            {
                Guid orderGuid = Guid.Empty;
                if (!string.IsNullOrEmpty(searchOrderId.Text))
                {
                    orderGuid = op.OrderGuidGetById(searchOrderId.Text.Trim());
                }
                return orderGuid;
            }
        }

        public string Mail
        {
            get
            {
                return searchMail.Text.Trim();
            }
        }

        public int SearchType
        {
            get
            {
                var result = seachPanel.Controls.OfType<RadioButton>().SingleOrDefault(x => x.GroupName == "searchType" && x.Checked);

                switch (result.ID)
                {
                    case "rbUserId":
                        return 0;
                    case "rbAll":
                        return 1;
                    default:
                        return -1;
                }
            }
        }


        public int SearchUserId
        {
            get
            {
                int userId = 0;
                int.TryParse(ddlUserId.SelectedValue, out userId);
                return userId;
            }
        }

        public int SearchPriority
        {
            get
            {
                int priority = 0;
                int.TryParse(ddlPriority.SelectedValue, out priority);
                return priority;
            }
        }
        public int SearchCategory
        {
            get
            {
                int categoryId = 0;
                int.TryParse(ddlCategory.SelectedValue, out categoryId);
                return categoryId;
            }
        }

        public string SDate
        {
            get
            {
                return tbOS.Text;
            }
        }

        public string EDate
        {
            get
            {
                return tbOE.Text;
            }
        }

        public int UserId
        {
            get
            {
                int userId = 0;
                userId = MemberFacade.GetUniqueId(User.Identity.Name);
                return userId;
            }
        }



        public bool IsCustomerCare
        {
            get
            {
                //檢查有沒有一線客服權限
                ViewEmployee ve = hp.ViewEmployeeByUserIdAndDept(UserId, departId);
                if (ve.IsLoaded)
                {
                    return true;
                }
                else
                {
                    return false;
                }


            }
        }

        public bool IsCustomerCare2
        {
            get
            {
                //檢查有沒有二線客服權限
                ViewEmployee ve = hp.ViewEmployeeByUserIdAndDept(UserId, subDepartId);
                if (ve.IsLoaded)
                {
                    return true;
                }
                else
                {
                    return false;
                }


            }
        }





        #endregion props
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                rbUserId.Checked = true;
                
                var entityCategory = sp.GetCustomerServiceCategory().Select(x => new { Text = x.CategoryName, Value = x.CategoryId.ToString() });
                ddlCategory.Items.Clear();
                ddlCategory.Items.Add(new ListItem("請選擇", ""));
                foreach (var item in entityCategory)
                {
                    ddlCategory.Items.Add(new ListItem(item.Text, item.Value));
                }

                //如果是二線客服的話              
                if(IsCustomerCare2 && !IsCustomerCare)
                {
                    var entity = hp.ViewEmployeeCollectionGetByFilter("dept_id", subDepartId, false);
                    ddlUserId.Items.Clear();
                    ddlUserId.Items.Add(new ListItem("請選擇", ""));
                    foreach (var item in entity)
                    {
                        ddlUserId.Items.Add(new ListItem(item.Name, item.UserId.ToString()));
                    }
                }
                else
                {
                    var entity = hp.ViewEmployeeCollectionGetByFilter("dept_id", departId, false);
                    ddlUserId.Items.Clear();
                    ddlUserId.Items.Add(new ListItem("請選擇", ""));
                    foreach (var item in entity)
                    {
                        ddlUserId.Items.Add(new ListItem(item.Name, item.UserId.ToString()));
                    }

                }

                ddlUserId.SelectedValue = UserId.ToString();

                Presenter.OnViewInitialized();

            }


            _presenter.OnViewLoaded();

        }


        #region event
        public event EventHandler Search;
        public event EventHandler<DataEventArgs<int>> ProcessCount;
        public event EventHandler<DataEventArgs<int>> ProcessPageChanged;
        #endregion



        protected void btnSearch_Click(object sender, EventArgs e)
        {
            if (this.Search != null)
            {
                this.Search(this, e);
            }
            ucPager.ResolvePagerView(CurrentPage, true);
        }

        protected void process_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.DataItem is ViewCustomerServiceList)
            {
                ViewCustomerServiceList item = (ViewCustomerServiceList)e.Row.DataItem;
                Label lblDot = ((Label)e.Row.FindControl("lblDot"));
                HyperLink hpServiceNo = ((HyperLink)e.Row.FindControl("hpServiceNo"));
                Label lblStatus = ((Label)e.Row.FindControl("lblStatus"));
                Label lblPriority = ((Label)e.Row.FindControl("lblPriority"));
                Label lblMessageType = ((Label)e.Row.FindControl("lblMessageType"));
                Label lblSource = ((Label)e.Row.FindControl("lblSource"));
                HyperLink hyName = ((HyperLink)e.Row.FindControl("hyName"));
                Label lblAccount = ((Label)e.Row.FindControl("lblAccount"));
                HyperLink hpOrderId = ((HyperLink)e.Row.FindControl("hpOrderId"));
                HyperLink hpSupplier = ((HyperLink)e.Row.FindControl("hpSupplier"));
                Label lblCategory = ((Label)e.Row.FindControl("lblCategory"));
                Label lblSubject = ((Label)e.Row.FindControl("lblSubject"));
                Label lblCreateDate = ((Label)e.Row.FindControl("lblCreateDate"));
                Label lblModifyDate = ((Label)e.Row.FindControl("lblModifyDate"));
                Label lblWorkUser = ((Label)e.Row.FindControl("lblWorkUser"));

                int csi = sp.CustomerServiceInsideLogByServiceNoAndStatus(item.ServiceNo, (int)statusConvert.transferback,null);
                //客服有回覆
                if (csi > 0)
                {
                    lblDot.Text = "<span style='color:red;margin-right:5px;font-size:18px;'>•</span>";
                }
                else
                {
                    //一天前
                    DateTime time = DateTime.Now.AddDays(-1);
                    int notRead = sp.CustomerServiceInsideLogByServiceNoAndStatus(item.ServiceNo, (int)statusConvert.transfer, time);
                    if (notRead > 0)
                    {
                        lblDot.Text = "<span id='"+ item.ServiceNo + "' style='color:red;margin-right:5px'><a href='javascript:void(0)' style='text-decoration:none;color:red' onclick='Remind(\"" + item.ServiceNo+"\")'>☎</a></span>";
                    }
                }
                hpServiceNo.Text = item.ServiceNo;
                hpServiceNo.NavigateUrl = "ServiceDetail.aspx?serviceNo=" + item.ServiceNo;
                hpServiceNo.Target = "_blank";
                lblStatus.Text = Helper.GetEnumDescription((statusConvert)item.CustomerServiceStatus);
                lblPriority.Text = Helper.GetEnumDescription((priorityConvert)item.CasePriority);
                if (item.CasePriority == 2)
                {
                    lblPriority.ForeColor = Color.FromName("red");
                }
                lblMessageType.Text = Helper.GetEnumDescription((messageConvert)item.MessageType);
                lblSource.Text = Helper.GetEnumDescription((Source)item.Source); 
                hyName.Text = item.Name;
                if (item.UserId != null)
                {
                    hyName.NavigateUrl = "~/controlroom/User/users_edit.aspx?username=" + HttpUtility.UrlEncode(MemberFacade.GetUserEmail(item.UserId.Value));
                    hyName.Target = "_blank";

                    Member mem = mp.MemberGetbyUniqueId(item.UserId.Value);
                    if (mem.IsLoaded)
                    {
                        if (mem.UserName.ToLower().Contains("@mobile"))
                        {
                            MobileMember mobilemem = mp.MobileMemberGet(item.UserId.Value);
                            if (mobilemem.IsLoaded)
                            {
                                lblAccount.Text = mobilemem.MobileNumber;
                            }
                        }
                        else
                        {
                            lblAccount.Text = mem.UserName;
                        }
                    }
                }
                else
                {
                    lblAccount.Text = item.Email;
                }


                string deliveryType = string.Empty;
                if (item.OrderGuid != null)
                {
                    if (item.DeliveryType == (int)DeliveryType.ToShop)
                    {
                        deliveryType = "(憑證)";
                    }
                    else
                    {
                        deliveryType = "(宅配)";
                    }

                    LunchKingSite.DataOrm.Order order = op.OrderGet(item.OrderGuid.Value);
                    if (order.IsLoaded)
                    {
                        hpOrderId.Text = order.OrderId;
                        hpOrderId.NavigateUrl = "~/controlroom/Order/order_detail.aspx?oid=" + item.OrderGuid.Value.ToString();
                        hpOrderId.Target = "_blank";
                        hpSupplier.Text = order.SellerName;
                        hpSupplier.NavigateUrl = "~/controlroom/Seller/seller_add.aspx?sid=" + order.SellerGuid;
                        hpSupplier.Target = "_blank";
                    }
                }
                lblCategory.Text = sp.CustomerServiceCategoryGet(item.Category).CategoryName + "<br />" + deliveryType;
                lblSubject.Text = sp.CustomerServiceCategoryGet(item.SubCategory).CategoryName;
                lblCreateDate.Text = item.CreateTime.ToString("yyyy/MM/dd HH:mm:ss");
                lblModifyDate.Text = item.ModifyTime == null ? "" : item.ModifyTime.Value.ToString("yyyy/MM/dd HH:mm:ss");
                if (item.ServicePeopleId!=null)
                {
                    var mem= mp.MemberGetbyUniqueId(item.ServicePeopleId.Value);
                    lblWorkUser.Text = mem.LastName + mem.FirstName;
                }

            }
        }

        protected void ProcessUpdateHandler(int pageNumber)
        {
            if (this.ProcessPageChanged != null)
            {
                this.ProcessPageChanged(this, new DataEventArgs<int>(pageNumber));
            }
        }
        protected int GetProcessCount()
        {
            DataEventArgs<int> e = new DataEventArgs<int>(0);
            if (ProcessCount != null)
            {
                ProcessCount(this, e);
            }
            return e.Data;
        }

        public void ProcessList(ViewCustomerServiceListCollection messageList)
        {
            divProcess.Visible = true;
            gridProcess.DataSource = messageList.OrderByDescending(p=>p.CasePriority).ThenBy(p=>p.CreateTime);
            gridProcess.DataBind();
        }

        /// <summary>
        /// 顯示出貨資訊(宅配)
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public static bool RemindPhone(string serviceNo)
        {
            string msg = string.Empty;
            var csl = sp.CustomerServiceInsideLogGet(serviceNo).Where(p=>p.Status==(int)statusConvert.transfer && p.IsRead==0);
            try
            {
                if (csl.Count() > 0)
                {
                    foreach (var item in csl)
                    {
                        item.IsRead = 1;
                        sp.CustomerServiceInsideLogSet(item);
                    }
                }
            }
            catch (Exception ex)
            {
                msg = ex.Message;
            }

            return msg.Length == 0;
        }
    }
}
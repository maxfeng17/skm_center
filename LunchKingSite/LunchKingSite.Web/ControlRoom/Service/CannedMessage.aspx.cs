﻿using System;
using LunchKingSite.DataOrm;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using System.Linq;
using System.Web.Services;
using System.Web.UI.WebControls;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core.UI;

namespace LunchKingSite.Web.ControlRoom.Service
{
    public partial class CannedMessage : RolePage, ICannedMessageView
    {
        #region props
        private static LunchKingSite.Core.IServiceProvider sp = ProviderFactory.Instance().GetProvider<LunchKingSite.Core.IServiceProvider>();
        private static IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        private static IAccountingProvider ap = ProviderFactory.Instance().GetProvider<IAccountingProvider>();
        private CannedMessagePresenter _presenter;


        public CannedMessagePresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        public int PageSize
        {
            get { return ucPager.PageSize; }
            set { ucPager.PageSize = value; }
        }
        public int CurrentPage
        {
            get
            {
                return ucPager.CurrentPage;
            }
        }

        public int CategoryId
        {
            get
            {
                int categoryId = 0;
                int.TryParse(outSideSubject.Value, out categoryId);
                return categoryId;
            }
        }

        public int InCategoryId
        {
            get
            {
                int incategoryId = 0;
                int.TryParse(inSideSubject.Value, out incategoryId);
                return incategoryId;
            }
        }

        public string Content
        {
            get { return txtContent.Value; }

        }

        public int UserId
        {
            get
            {
                var userName = User.Identity.Name;
                
                return MemberFacade.GetUniqueId(userName);
            }
        }

        public string UserName
        {
            get
            {
                return User.Identity.Name;
            }
        }

        public string Mode
        {
            get { return mode.Value; }
        }

        public int SampleId
        {
            get
            {
                int sample_Id = 0;
                int.TryParse(sampleId.Value, out sample_Id);
                return sample_Id;
            }
        }

        public string Subject
        {
            get
            {
                return txtSubject.Text; 
            }
        }



        public int FineId
        {
            get
            {
                int fine_Id = 0;
                int.TryParse(fineId.Value, out fine_Id);
                return fine_Id;
            }
        }

        public string FineContent
        {
            get
            {
                return txtFineContent.Text;
            }
        }

        public int FineType
        {
            get
            {
                return Convert.ToInt32(ddlFineType.SelectedValue);
            }
        }

        public string FineMode
        {
            get { return fineMode.Value; }
        }


        #endregion props
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                Presenter.OnViewInitialized();
                
                //綁定dropdownlist參數
                var entityCategory = sp.GetCustomerServiceCategory().Select(x => new { Text = x.CategoryName, Value = x.CategoryId.ToString() });
                foreach (var item in entityCategory)
                {
                    ddlCategory.Items.Add(new ListItem(item.Text, item.Value));
                }

                foreach (var item in entityCategory)
                {
                    ddlInSideCategory.Items.Add(new ListItem(item.Text, item.Value));
                }

                foreach (VendorFineType item in Enum.GetValues(typeof(VendorFineType)))
                {
                    ddlFineType.Items.Add(new ListItem(Helper.GetDescription(item), ((int)item).ToString()));
                }
            }
            
            _presenter.OnViewLoaded();

        }

        protected void Initial()
        {
            txtContent.Value = "";
            ddlInSideCategory.SelectedValue = "";
            ddlInSideSubject.SelectedValue = "";
        }

        #region event
        public event EventHandler Search;
        public event EventHandler AddService;
        public event EventHandler AddFine;
        public event EventHandler<DataEventArgs<int>> EditCannedMessage;
        public event EventHandler<DataEventArgs<int>> DeleteCannedMessage;
        public event EventHandler<DataEventArgs<int>> GetCannedMessageCount;
        public event EventHandler<DataEventArgs<int>> CannedMessagePageChanged;
        #endregion
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            if (this.Search != null)
            {
                this.Search(this, e);
            }
            
            var entityCategory = sp.GetCustomerServiceCategoryListByCategoryId(int.Parse(ddlCategory.SelectedValue)).Select(x => new { Text = x.CategoryName, Value = x.CategoryId.ToString() });
            ddlSubject.Items.Clear();
            ddlSubject.Items.Add(new ListItem("請選擇", ""));
            foreach (var item in entityCategory)
            {
                ddlSubject.Items.Add(new ListItem(item.Text, item.Value));
            }
            
            outSideSubject.Value = CategoryId.ToString();
            ddlSubject.SelectedValue = CategoryId.ToString();

            ucPager.ResolvePagerView(CurrentPage, true);

            divSearch.Visible = true;
        }

        protected void btnAddService(object sender, EventArgs e)
        {
            if (this.AddService != null)
            {
                this.AddService(this, e);
            }
            Initial();
            ucPager.ResolvePagerView(CurrentPage, true);
            divSearch.Visible = true;
        }

        protected void search_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.DataItem is CustomerServiceCategorySample)
            {
                CustomerServiceCategorySample item = (CustomerServiceCategorySample)e.Row.DataItem;
                Label lblCategory = ((Label)e.Row.FindControl("lblCategory"));
                Label lblCategoryName = ((Label)e.Row.FindControl("lblCategoryName"));
                Label lblSubject = ((Label)e.Row.FindControl("lblSubject"));
                Label lblContent = ((Label)e.Row.FindControl("lblContent"));
                Label lblCreateDate = ((Label)e.Row.FindControl("lblCreateDate"));
                LinkButton lbtnEdit = ((LinkButton)e.Row.FindControl("lbtnEdit"));
                LinkButton lbtnDelete = ((LinkButton)e.Row.FindControl("lbtnDelete"));

                lblCategory.Text = sp.GetCustomerNameByCategoryIdMain(item.CategoryId);
                lblCategoryName.Text= sp.GetCustomerNameByCategoryId(item.CategoryId);
                lblSubject.Text = item.Subject;
                lblContent.Text = item.Content;
                lblCreateDate.Text = item.ModifyTime==null?item.CreateTime.ToString("yyyy/MM/dd HH:mm:ss"): item.ModifyTime.Value.ToString("yyyy/MM/dd HH:mm:ss");
                lbtnEdit.Text = "編輯";
                lbtnEdit.CommandArgument = item.Id.ToString();
                lbtnDelete.Text = "刪除";
                lbtnDelete.CommandArgument = item.Id.ToString();
            }
        }

        protected void search_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "UPD":
                    
                    if (this.EditCannedMessage != null)
                    {
                        int CommandArgument = 0;
                        int.TryParse(e.CommandArgument.ToString(), out CommandArgument);
                        this.EditCannedMessage(this, new DataEventArgs<int>(CommandArgument));
                    }
                    ucPager.ResolvePagerView(CurrentPage, true);
                    break;

                case "DEL":
                    
                    if (this.DeleteCannedMessage != null)
                    {
                        int CommandArgument = 0;
                        int.TryParse(e.CommandArgument.ToString(),out CommandArgument);
                        this.DeleteCannedMessage(this, new DataEventArgs<int>(CommandArgument));
                    }
                    ucPager.ResolvePagerView(CurrentPage, true);
                    break;

                default:
                    break;
            }
        }

        protected void fine_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.DataItem is VendorFineCategory)
            {
                VendorFineCategory item = (VendorFineCategory)e.Row.DataItem;
                Label lblFineContent = ((Label)e.Row.FindControl("lblFineContent"));
                Label lblFineType = ((Label)e.Row.FindControl("lblFineType"));
                LinkButton lbtnFineEdit = ((LinkButton)e.Row.FindControl("lbtnFineEdit"));

                lblFineContent.Text = item.Content;
                lblFineType.Text = Helper.GetDescription((VendorFineType)item.Type);
                lbtnFineEdit.Text = "編輯";
                lbtnFineEdit.CommandArgument = item.Id.ToString();
            }
        }
        protected void btnAddFine(object sender, EventArgs e)
        {
            if (this.AddFine != null)
            {
                this.AddFine(this, e);
            }
        }

        protected void CannedMessageUpdateHandler(int pageNumber)
        {
            if (this.CannedMessagePageChanged != null)
            {
                this.CannedMessagePageChanged(this, new DataEventArgs<int>(pageNumber));
            }
        }
        protected int GetMessageCount()
        {
            DataEventArgs<int> e = new DataEventArgs<int>(0);
            if (GetCannedMessageCount != null)
            {
                GetCannedMessageCount(this, e);
            }
            return e.Data;
        }

        public void CannedMessageList(CustomerServiceCategorySampleCollection categorySampleList)
        {
            divSearch.Visible = true;
            foreach (var item in categorySampleList)
            {
                item.Content = item.Content.Replace("vbCrLf", "<br>").Replace("\r\n","<br>");
            }
            gridSearch.DataSource = categorySampleList;
            gridSearch.DataBind();
        }

        public void CustomerServiceCategorySampleEdit(CustomerServiceCategorySample entity)
        {
            int categoryId = sp.CustomerServiceCategoryGet(entity.CategoryId).ParentCategoryId.Value;
            //取得subject下拉選單
            var entityCategory = sp.GetCustomerServiceCategoryListByCategoryId(categoryId).Select(x => new { Text = x.CategoryName, Value = x.CategoryId.ToString() });
            foreach (var item in entityCategory)
            {
                ddlInSideSubject.Items.Add(new ListItem(item.Text, item.Value));
            }
            ddlInSideCategory.SelectedValue = categoryId.ToString();
            ddlInSideSubject.SelectedValue = entity.CategoryId.ToString();
            txtContent.Value = entity.Content;
        }

        public void VendorFineCategoryGetList(VendorFineCategoryCollection fineCategory)
        {
            grvFine.DataSource = fineCategory;
            grvFine.DataBind();
        }


        [WebMethod]
        public static string GetServiceMessageCategory(int categoryId)
        {
            return CustomerServiceFacade.GetServiceMessageCategory(categoryId);
        }

        [WebMethod]
        public static string GetSampleList(int id)
        {
            var sample = sp.CustomerServiceCategorySampleGet(id);
            var parentId = sp.CustomerServiceCategoryGet(sample.CategoryId).ParentCategoryId;
            SampleModel sampleList = new SampleModel();
            sampleList.id = sample.Id;
            sampleList.categoryId = sample.CategoryId;
            sampleList.content = sample.Content;
            sampleList.subject = sample.Subject;
            sampleList.parent_categoryId = parentId.Value;
            return new JsonSerializer().Serialize(sampleList);
        }

        [WebMethod]
        public static string GetVendorFineCategoryList(int id)
        {
            var fine = ap.VendorFineCategoryGetList(id);
            FineModel fineList = new FineModel();
            fineList.id = fine.Id;
            fineList.fineContent = fine.Content;
            fineList.fineType = fine.Type;
            return new JsonSerializer().Serialize(fineList);
        }

        public class SampleModel
        {
            public int id { get; set; }
            public int categoryId { get; set; }
            public int parent_categoryId { get; set; }
            public string content { get; set; }
            public string subject { get; set; }

        }

        public class FineModel
        {
            public int id { get; set; }
            public string fineContent { get; set; }
            public int fineType { get; set; }

        }

    }
}
﻿using System;
using LunchKingSite.DataOrm;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using System.Linq;
using System.Web.Services;
using System.Web.UI.WebControls;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;
using LunchKingSite.BizLogic.Facade;
using System.Collections.Generic;
using LunchKingSite.Core.UI;
using LunchKingSite.Core.Enumeration;
using System.Web;
using System.Drawing;

namespace LunchKingSite.Web.ControlRoom.Service
{
    public partial class History : RolePage, IHistoryView
    {
        #region props

        protected static Core.IServiceProvider sp = ProviderFactory.Instance().GetProvider<Core.IServiceProvider>();
        protected static IOrderProvider op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
        protected static IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        protected static IHumanProvider hp = ProviderFactory.Instance().GetProvider<IHumanProvider>();
        protected static ISellerProvider sellerProv = ProviderFactory.Instance().GetProvider<ISellerProvider>();
        protected static ISysConfProvider config = ProviderFactory.Instance().GetConfig();

        private HistoryPresenter _presenter;
        private const string departId = "C001";
        private const string subDepartId = "C002";
        private const string depRoleName = "CustomerCare";
        
        public HistoryPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        public int PageSize
        {
            get { return ucPager.PageSize; }
            set { ucPager.PageSize = value; }
        }

        public int CurrentPage
        {
            get
            {
                return ucPager.CurrentPage;
            }
        }

        public string SearchServiceNo
        {
            get
            {
                return searchSericeNo.Text.Trim();
            }
        }
        
        public Guid SearchOrderGuid
        {
            get
            {
                Guid orderGuid = Guid.Empty;
                if (!string.IsNullOrEmpty(searchOrderId.Text))
                {
                    orderGuid = op.OrderGuidGetById(searchOrderId.Text.Trim());
                }
                return orderGuid;
            }
        }
        
        public string Mail
        {
            get
            {
                return searchMail.Text.Trim();
            }
        }
        
        public int SearchUserId
        {
            get
            {
                int userId = 0;
                int.TryParse(ddlUserId.SelectedValue, out userId);
                return userId;
            }
        }

        public int SearchSecUserId
        {
            get
            {
                int userId = 0;
                int.TryParse(ddlSecUserId.SelectedValue, out userId);
                return userId;
            }
        }

        public int SearchPriority
        {
            get
            {
                int priority = 0;
                int.TryParse(ddlPriority.SelectedValue, out priority);
                return priority;
            }
        }

        public int SearchCategory
        {
            get
            {
                int categoryId = 0;
                int.TryParse(ddlCategory.SelectedValue, out categoryId);
                return categoryId;
            }
        }

        public string SDate
        {
            get
            {
                return tbOS.Text;
            }
        }

        public string EDate
        {
            get
            {
                return tbOE.Text;
            }
        }

        public string SHour
        {
            get
            {
                return ddlHS.SelectedValue;
            }
        }

        public string SMinute
        {
            get
            {
                return ddlMS.SelectedValue;
            }
        }

        public string EHour
        {
            get
            {
                return ddlHE.SelectedValue;
            }
        }

        public string EMinute
        {
            get
            {
                return ddlME.SelectedValue;
            }
        }

        public int SearchStatus
        {
            get
            {
                int status = -1;
                int.TryParse(ddlStatus.SelectedValue, out status);
                return status;
            }
        }

        public int SearchPponDealType
        {
            get
            {
                int status = -1;
                int.TryParse(ddlPponDealType.SelectedValue, out status);
                return status;
            }
        }

        public int MessageType
        {
            get
            {
                int type = -1;
                int.TryParse(ddlMessageType.SelectedValue, out type);
                return type;
            }
        }

        public int CaseSource
        {
            get
            {
                int source = -1;
                int.TryParse(ddlSource.SelectedValue, out source);
                return source;
            }
        }

        public string UniqueId
        {
            get
            {
                return txtUniqueId.Text.Trim();
            }
        }

        public string Supplier
        {
            get
            {
                return txtSupplier.Text.Trim();
            }
        }

        public int SubCategory
        {
            get
            {
                int subCategory = -1;
                int.TryParse(ddlSubCategory.SelectedValue, out subCategory);
                return subCategory;
            }
        }

        public int UserId
        {
            get
            {
                int userId = 0;
                userId = MemberFacade.GetUniqueId(User.Identity.Name);
                return userId;
            }
        }
        
        public bool IsCustomerCare
        {
            get
            {
                //檢查有沒有一線客服權限
                ViewEmployee ve = hp.ViewEmployeeByUserIdAndDept(UserId, departId);
                if (ve.IsLoaded)
                {
                    return true;
                }
                else
                {
                    return false;
                }


            }
        }

        public bool IsCustomerCare2
        {
            get
            {
                //檢查有沒有二線客服權限
                ViewEmployee ve = hp.ViewEmployeeByUserIdAndDept(UserId, subDepartId);
                if (ve.IsLoaded)
                {
                    return true;
                }
                else
                {
                    return false;
                }


            }
        }

        public string ChoseBox
        {
            get
            {
                return hiddenChose.Value;
            }
        }

        public int IssueFromType
        {
            get { return int.Parse(ddlIssueFromType.SelectedValue); }
        }

        public string TsMemberNo
        {
            get { return ddlTsMemberNo.Text.Trim(); }
        }

        #endregion props

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                IntialControls();
                Presenter.OnViewInitialized();
            }
            _presenter.OnViewLoaded();

        }

        private void IntialControls()
        {
            //處理人員
            var entity = mp.ViewRoleMemberCollectionGet(depRoleName);
            var serviceEmps = entity.Select(x => new KeyValuePair<string, string>(x.UserId.ToString(), x.Name)).Distinct().ToList();
            ddlUserId.Items.Clear();
            ddlSecUserId.Items.Clear();
            ddlUserId.Items.Add(new ListItem("全部", ""));
            foreach (var emp in serviceEmps)
            {
                ddlUserId.Items.Add(new ListItem(emp.Value, emp.Key));
            }
            ddlUserId.SelectedValue = UserId.ToString();
            ddlSecUserId.Items.Add(new ListItem("全部", ""));
            foreach (var emp in serviceEmps)
            {
                ddlSecUserId.Items.Add(new ListItem(emp.Value, emp.Key));
            }

            //案件來源
            Dictionary<int, string> messageType = new Dictionary<int, string>();
            messageType[-1] = "全部";
            foreach (var item in Enum.GetValues(typeof(messageConvert)))
            {
                messageType[(int)item] = Helper.GetEnumDescription((messageConvert)item);
            }

            ddlMessageType.DataSource = messageType;
            ddlMessageType.DataTextField = "Value";
            ddlMessageType.DataValueField = "Key";
            ddlMessageType.DataBind();

            //裝置平台
            Dictionary<int, string> caseSource = new Dictionary<int, string>();
            caseSource[-1] = "全部";
            foreach (var item in Enum.GetValues(typeof(Source)))
            {
                caseSource[(int)item] = Helper.GetEnumDescription((Source)item);
            }

            ddlSource.DataSource = caseSource;
            ddlSource.DataTextField = "Value";
            ddlSource.DataValueField = "Key";
            ddlSource.DataBind();

            //來源平台
            Dictionary<int, string> issueFromType = new Dictionary<int, string>();
            issueFromType[-1] = "全部";
            foreach (var item in Enum.GetValues(typeof(IssueFromType)))
            {
                issueFromType[(int)item] = Helper.GetEnumDescription((IssueFromType)item);
            }

            ddlIssueFromType.DataSource = issueFromType;
            ddlIssueFromType.DataTextField = "Value";
            ddlIssueFromType.DataValueField = "Key";
            ddlIssueFromType.DataBind();

            //問題類別
            var entityCategory = sp.GetCustomerServiceCategory().Select(x => new { Text = x.CategoryName, Value = x.CategoryId.ToString() });
            ddlCategory.Items.Clear();
            ddlCategory.Items.Add(new ListItem("請選擇", "-1"));
            foreach (var item in entityCategory)
            {
                ddlCategory.Items.Add(new ListItem(item.Text, item.Value));
            }

            //主因
            ddlSubCategory.Items.Clear();
            ddlSubCategory.Items.Add(new ListItem("請選擇", "-1"));
            
            //時間
            Dictionary<string, string> hourList = new Dictionary<string, string>();
            for(int h = 0; h <= 23; h++)
            {
                hourList.Add(h.ToString().PadLeft(2, '0'), h.ToString().PadLeft(2, '0'));
            }
            ddlHS.DataSource = hourList;
            ddlHS.DataTextField = "Value";
            ddlHS.DataValueField = "Key";
            ddlHS.DataBind();

            ddlHE.DataSource = hourList;
            ddlHE.DataTextField = "Value";
            ddlHE.DataValueField = "Key";
            ddlHE.DataBind();

            ddlHE.SelectedValue = "23";
            
            Dictionary<string, string> minuteList = new Dictionary<string, string>();
            for (int h = 0; h <= 60; h+=5)
            {
                minuteList.Add(h.ToString().PadLeft(2, '0'), h.ToString().PadLeft(2, '0'));
            }
            minuteList.Add("59", "59");

            ddlMS.DataSource = minuteList;
            ddlMS.DataTextField = "Value";
            ddlMS.DataValueField = "Key";
            ddlMS.DataBind();

            ddlME.DataSource = minuteList;
            ddlME.DataTextField = "Value";
            ddlME.DataValueField = "Key";
            ddlME.DataBind();

            ddlME.SelectedValue = "59";
        }

        #region event
        public event EventHandler Search;
        public event EventHandler Export;
        public event EventHandler AddClaimed;
        public event EventHandler<DataEventArgs<int>> HistoryCount;
        public event EventHandler<DataEventArgs<int>> HistoryPageChanged;
        #endregion
        
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            if (this.Search != null)
            {
                this.Search(this, e);
            }
            ucPager.ResolvePagerView(CurrentPage, true);
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            if (this.Export != null)
            {
                this.Export(this, e);
            }
        }

        protected void history_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.DataItem is ViewCustomerServiceList)
            {
                ViewCustomerServiceList item = (ViewCustomerServiceList)e.Row.DataItem;
                CheckBox chbClaim = (CheckBox)e.Row.FindControl("chbClaim");
                Label lblDot = ((Label)e.Row.FindControl("lblDot"));
                HyperLink hpServiceNo = ((HyperLink)e.Row.FindControl("hpServiceNo"));
                Label lblStatus = ((Label)e.Row.FindControl("lblStatus"));
                Label lblPriority = ((Label)e.Row.FindControl("lblPriority"));
                Label lblMessageType = ((Label)e.Row.FindControl("lblMessageType"));
                Label lblSource = ((Label)e.Row.FindControl("lblSource"));
                HyperLink hyName = ((HyperLink)e.Row.FindControl("hyName"));
                Label lblAccount = ((Label)e.Row.FindControl("lblAccount"));
                HyperLink hpOrderId = ((HyperLink)e.Row.FindControl("hpOrderId"));
                Label lblPponDealType = ((Label)e.Row.FindControl("lblPponDealType"));
                HyperLink hpSupplier = ((HyperLink)e.Row.FindControl("hpSupplier"));
                Label lblCategory = ((Label)e.Row.FindControl("lblCategory"));
                Label lblSubject = ((Label)e.Row.FindControl("lblSubject"));
                Label lblCreateDate = ((Label)e.Row.FindControl("lblCreateDate"));
                Label lblModifyDate = ((Label)e.Row.FindControl("lblModifyDate"));
                Label lblWorkUser = ((Label)e.Row.FindControl("lblWorkUser"));
                Label lblSecWorkUser = ((Label)e.Row.FindControl("lblSecWorkUser"));
                Label lblIssueFromType = ((Label)e.Row.FindControl("lblIssueFromType"));
                Label lblTsMemberNo = ((Label)e.Row.FindControl("lblTsMemberNo"));

                if ((statusConvert)item.CustomerServiceStatus == statusConvert.wait)
                {
                    chbClaim.Visible = true;
                }

                int csi = sp.CustomerServiceInsideLogByServiceNoAndStatus(item.ServiceNo, (int)statusConvert.transferback, null);
                //客服有回覆
                if (csi > 0)
                {
                    lblDot.Text = "<span title='商家回覆' style='color:red;margin-right:5px;font-size:20px;'>↩</span>";
                }
                else
                {
                    //一天前
                    DateTime time = DateTime.Now.AddDays(-1);
                    int transferNotRead = sp.CustomerServiceInsideLogByServiceNoAndStatus(item.ServiceNo, (int)statusConvert.transfer, time);
                    
                    if (transferNotRead > 0)
                    {
                        lblDot.Text = "<span title='商家尚未回覆' id='" + item.ServiceNo + "' style='color:red;margin-right:5px'><a href='javascript:void(0)' style='text-decoration:none;color:red' onclick='Remind(\"" + item.ServiceNo + "\")'>☎</a></span>";
                    }

                    if (UserId == item.ServicePeopleId || UserId == item.SecServicePeopleId)
                    {
                        int status = UserId == item.ServicePeopleId ? (int)statusConvert.ServiceNotify : (int)statusConvert.secServiceNotify;
                        int notifyNotRead = sp.CustomerServiceInsideLogByServiceNoAndStatus(item.ServiceNo, status, null);
                        lblDot.Text += notifyNotRead > 0 ? "<br/><span title='客服通知' style='color:blue;margin-right:5px;font-size:20px;'>✉</span>" : string.Empty;
                    }
                }

                hpServiceNo.Text = item.ServiceNo;
                hpServiceNo.NavigateUrl = config.OrderDetailV2LinkVisable && item.OrderGuid != null
                    ? "~/controlroom/Member/OrderDetail?orderGuid=" + item.OrderGuid + "#CustomerServiceArea"
                    : "ServiceDetail.aspx?serviceNo=" + item.ServiceNo;
                lblStatus.Text = Helper.GetEnumDescription((statusConvert)item.CustomerServiceStatus);
                lblPriority.Text = Helper.GetEnumDescription((priorityConvert)item.CasePriority);
                if (item.CasePriority == 2)
                {
                    lblPriority.ForeColor = Color.FromName("red");
                }
                lblMessageType.Text = Helper.GetEnumDescription((messageConvert)item.MessageType);
                lblSource.Text = Helper.GetEnumDescription((Source)item.Source); 
                hyName.Text = item.Name;
                if (item.UserId != null)
                {
                    hyName.NavigateUrl = "~/controlroom/User/users_edit.aspx?username=" + HttpUtility.UrlEncode(MemberFacade.GetUserName(item.UserId.Value));
                    hyName.Target = "_blank";

                    Member mem = mp.MemberGetbyUniqueId(item.UserId.Value);
                    if (mem.IsLoaded)
                    {
                        if (mem.UserName.ToLower().Contains("@mobile"))
                        {
                            MobileMember mobilemem = mp.MobileMemberGet(item.UserId.Value);
                            if (mobilemem.IsLoaded)
                            {
                                lblAccount.Text = mobilemem.MobileNumber;
                            }
                        }
                        else
                        {
                            lblAccount.Text = mem.UserName;
                        }
                    }
                }
                else
                {
                    lblAccount.Text = item.Email;
                }

                string deliveryType = string.Empty;
                if (item.OrderGuid != null)
                {
                    if (item.DeliveryType == (int)DeliveryType.ToShop)
                    {
                        deliveryType = "(憑證)";
                        lblPponDealType.Text = "憑證檔次";
                    }
                    else
                    {
                        if (item.ProductDeliveryType == (int)ProductDeliveryType.Wms)
                        {
                            lblPponDealType.Text = "24H到貨";
                        }
                        else if (item.IspOrderType > 0)
                        {
                            lblPponDealType.Text = "超取訂單";
                        }
                        else
                        {
                            lblPponDealType.Text = "一般宅配";
                        }
                    }

                    LunchKingSite.DataOrm.Order order = op.OrderGet(item.OrderGuid.Value);
                    if (order.IsLoaded)
                    {
                        hpOrderId.Text = order.OrderId;
                        hpOrderId.NavigateUrl = config.OrderDetailV2LinkVisable
                            ? "~/controlroom/Member/OrderDetail?orderGuid=" + item.OrderGuid
                            : "~/controlroom/Order/order_detail.aspx?oid=" + item.OrderGuid;
                        hpOrderId.Target = "_blank";
                        hpSupplier.Text = order.SellerName;
                        hpSupplier.NavigateUrl = "~/controlroom/Seller/seller_add.aspx?sid=" + order.SellerGuid;
                        hpSupplier.Target = "_blank";
                    }
                }
                
                lblCategory.Text = sp.CustomerServiceCategoryGet(item.Category).CategoryName + "<br />" + deliveryType;
                lblSubject.Text = sp.CustomerServiceCategoryGet(item.SubCategory).CategoryName;
                lblCreateDate.Text = item.CreateTime.ToString("yyyy/MM/dd HH:mm:ss");
                lblModifyDate.Text = item.ModifyTime == null ? "" : item.ModifyTime.Value.ToString("yyyy/MM/dd HH:mm:ss");
                if (item.ServicePeopleId != null)
                {
                    var mem = mp.MemberGetbyUniqueId(item.ServicePeopleId.Value);
                    lblWorkUser.Text = mem.LastName + mem.FirstName;
                }
                if (item.SecServicePeopleId != null)
                {
                    var mem = mp.MemberGetbyUniqueId(item.SecServicePeopleId.Value);
                    lblSecWorkUser.Text = mem.LastName + mem.FirstName;
                }

                lblIssueFromType.Text = Helper.GetEnumDescription((IssueFromType) item.IssueFromType);
                lblTsMemberNo.Text = item.TsMemberNo;

            }
        }

        protected void HistoryUpdateHandler(int pageNumber)
        {
            if (this.HistoryPageChanged != null)
            {
                this.HistoryPageChanged(this, new DataEventArgs<int>(pageNumber));
            }
        }

        protected int GetHistoryCount()
        {
            DataEventArgs<int> e = new DataEventArgs<int>(0);
            if (HistoryCount != null)
            {
                HistoryCount(this, e);
            }
            return e.Data;
        }

        public void HistoryList(ViewCustomerServiceListCollection messageList)
        {
            gridHistory.DataSource = messageList.OrderBy(p=>p.CreateTime);
            gridHistory.DataBind();
            divHistory.Visible = true;
            chosePanel.Visible = true;
        }

        protected void btnClaimed_Click(object sender, EventArgs e)
        {
            initial();
            if (this.AddClaimed != null)
            {
                this.AddClaimed(this, e);
            }

            ucPager.ResolvePagerView(CurrentPage, true);
        }

        protected void initial()
        {
            //ddlCategory.SelectedValue = "-1";
            tbOS.Text = "";
            tbOE.Text = "";
            searchMail.Text = "";
            searchOrderId.Text = "";
        }

        protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            int categoryId;
            if (int.TryParse(ddlCategory.SelectedValue, out categoryId))
            {
                if (categoryId > 0)
                {
                    var subCategoryList = sp.GetCustomerServiceCategoryListByCategoryId(categoryId).ToList();
                    foreach (var subCategory in subCategoryList)
                    {
                        ddlSubCategory.Items.Add(new ListItem(subCategory.CategoryName, subCategory.CategoryId.ToString()));
                    }

                    var selectedItem = ddlSubCategory.Items.FindByValue(hiddenSubCategory.Value);
                    if (selectedItem != null)
                    {
                        selectedItem.Selected = true;
                    }
                }
            }

        }

        [WebMethod]
        public static string GetCustomerServiceCategoryList(int categoryId)
        {
            var serviceCates = sp.GetCustomerServiceCategoryListByCategoryId(categoryId).ToList();
            return new JsonSerializer().Serialize(serviceCates);
        }

        
    }
}
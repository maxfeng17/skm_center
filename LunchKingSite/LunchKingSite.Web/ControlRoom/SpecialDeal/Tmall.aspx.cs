﻿using LunchKingSite.Core;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.BizLogic.Facade.Models;

namespace LunchKingSite.Web.ControlRoom.SpecialDeal
{
    public partial class Tmall : RolePage, ITmallView
    {
        #region 簡繁轉換
        private const int LocaleSystemDefault = 0x0800;
        private const int LcmapSimplifiedChinese = 0x02000000;
        private const int LcmapTraditionalChinese = 0x04000000;
        [DllImport("kernel32", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern int LCMapString(int locale, int dwMapFlags, string lpSrcStr, int cchSrc,
                                              [Out] string lpDestStr, int cchDest);
        #endregion

        #region property
        private TmallPresenter _presenter;
        public TmallPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        public string UserMemo
        {
            get;
            set;
        }

        public Guid SelectedStoreGuid
        {
            get
            {
                var selectedStr = ddlStore.SelectedValue;
                if (!string.IsNullOrWhiteSpace(selectedStr))
                {
                    return new Guid(selectedStr);
                }
                else if (!string.IsNullOrWhiteSpace(HiddenSingleStoreGuid.Text))
                {
                    return new Guid(HiddenSingleStoreGuid.Text);
                }
                return Guid.Empty;
            }
        }

        private IList<OptionItemLimit> itemLimits
        {
            get;
            set;
        }

        public Guid BusinessHourGuid
        {
            get
            {
                Guid bid;
                if (!string.IsNullOrEmpty(hidBusinessHourGuid.Value) && Guid.TryParse(hidBusinessHourGuid.Value, out bid))
                {
                    return bid;
                }
                else
                {
                    return Guid.Empty;
                }
            }
            set
            {
                hidBusinessHourGuid.Value = value.ToString();
            }
        }

        public string SessionId
        {
            get { return Session.SessionID; }
        }

        public string UserName { get { return User.Identity.Name; } }

        public int PageCount
        {
            get
            {
                int pagecount;
                return int.TryParse((ViewState["pagecount"] == null ? string.Empty : ViewState["pagecount"].ToString()), out pagecount) ? pagecount : 0;
            }
            set
            {
                ViewState["pagecount"] = value;
            }
        }

        public int SearchFilter
        {
            get
            {
                int key = 0;
                if (int.TryParse(ddl_SearchKey.SelectedValue, out key))
                {
                    return key;
                }
                else
                {
                    return 0;
                }
            }
        }

        public string SearchKey
        {
            get
            {
                return tbx_SearchKey.Text;
            }
        }

        public int SmsFilter
        {
            get
            {
                int key = 0;
                if (int.TryParse(ddl_SmsStatus.SelectedValue, out key))
                {
                    return key;
                }
                else
                {
                    return 0;
                }
            }
        }

        public bool IsSearchByKey
        {
            get
            {
                return cbx_IsSearchByKey.Checked;
            }
            set
            {
                cbx_IsSearchByKey.Checked = value;
            }
        }

        public DateTime SmsStart
        {
            get
            {
                DateTime date;
                if (DateTime.TryParse(tbx_SmsStart.Text, out date))
                {
                    return date;
                }
                else
                {
                    return DateTime.Now;
                }
            }
        }

        public DateTime SmsEnd
        {
            get
            {
                DateTime date;
                if (DateTime.TryParse(tbx_SmsEnd.Text, out date))
                {
                    return date;
                }
                else
                {
                    return DateTime.Now;
                }
            }
        }

        public string SmsMemo
        {
            get
            {
                return tbx_SmsMemo.Text;
            }
        }

        public int PageSize
        {
            get
            {
                return gridPager.PageSize;
            }
        }
        #region Tmall property
        public string DefaultDeliveryAddress
        {
            get
            {
                return
                    tbx_DeliveryAddress.Text;
            }
        }
        public int DefaultQuanty
        {
            get
            {
                int quantity;
                if (int.TryParse(ddl_Quantity.SelectedValue, out quantity))
                {
                    return quantity;
                }
                else
                {
                    return 1;
                }
            }
        }
        public int DefaultTotal { get { return 0; } }
        public int DefaultDeliveryCharge { get { return 0; } }
        public EntrustSellType DefaultEntrustSell { get { return EntrustSellType.No; } }
        public DepartmentTypes DefaultSellerType { get { return DepartmentTypes.Ppon; } }
        public DeliveryType DefaultDeliveryType { get { return DeliveryType.ToShop; } }
        public string DefaultUserName { get; set; }
        public string DefaultWriteName
        {
            get
            {
                return tbx_DeliveryName.Text;
            }
        }
        #endregion
        #endregion

        #region event
        public event EventHandler<DataEventArgs<int>> SearchDealByUid;
        public event EventHandler<DataEventArgs<Guid>> SearchDealByBid;
        public event EventHandler<DataEventArgs<KeyValuePair<string, string>>> MakeTmallOrder;
        public event EventHandler SearchList;
        public event EventHandler<DataEventArgs<int>> PageChange;
        public event EventHandler<DataEventArgs<int>> ReSendCouponSms;
        #endregion

        #region page
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (!_presenter.OnViewInitialized())
                    return;
            }
            _presenter.OnViewLoaded();
        }

        /// <summary>
        /// 查詢檔次(檔號或bid)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SearchDeal(object sender, EventArgs e)
        {
            int uid;
            Guid bid;
            ClearData();
            if (string.IsNullOrEmpty(tbx_Id.Text))
            {
                ShowAlert("請輸入檔號或Bid");
            }
            else
            {
                cbx_Tab2.Checked = false;
                if (ddl_DealIDType.SelectedValue == "uid" && int.TryParse(tbx_Id.Text, out uid))
                {
                    if (this.SearchDealByUid != null)
                    {
                        SearchDealByUid(this, new DataEventArgs<int>(uid));
                    }
                }
                else if (ddl_DealIDType.SelectedValue == "bid" && Guid.TryParse(tbx_Id.Text, out bid))
                {
                    if (this.SearchDealByBid != null)
                    {
                        SearchDealByBid(this, new DataEventArgs<Guid>(bid));
                    }
                }
                else
                {
                    ShowAlert("格式錯誤");
                }
            }
        }

        protected void rpOption_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            int count = 0;
            if (e.Item.DataItem is AccessoryGroup)
            {
                List<ViewItemAccessoryGroup> viagc = ((AccessoryGroup)e.Item.DataItem).members;
                DropDownList ddl = (DropDownList)e.Item.FindControl("ddlPayMultOption");
                ddl.Items.Add("請選擇");
                foreach (ViewItemAccessoryGroup v in viagc)
                {
                    ListItem li = new ListItem(v.AccessoryName, v.AccessoryGroupMemberGuid.ToString());
                    if (v.Quantity == null || v.Quantity > 0)
                    {
                        li.Attributes.Remove("disabled");
                    }
                    else
                    {
                        li.Attributes["disabled"] = "disabled";
                        li.Text += "(已售完)";
                    }

                    ddl.Items.Add(li);
                    count++;
                }
                if (count == 0)
                {
                    ddl.Visible = false;
                    Label lbl = (Label)e.Item.FindControl("lblPayMultOption");
                    lbl.Visible = false;
                }
            }
        }

        protected void MakeOrderSendSms(object sender, EventArgs e)
        {
            cbx_Tab2.Checked = false;
            string errormessage;
            if (!CheckBranch())
            {
                ShowAlert("請選擇還有數量的分店");
            }
            // check option
            else if (!CheckOptions())
            {
                ShowAlert("請選擇還有數量的選項");
            }
            else if (!CheckTmallInfo(out errormessage))
            {
                ShowAlert(errormessage);
            }
            else
            {
                #region MergePay

                foreach (RepeaterItem ri in rpPayOption.Items)
                {
                    DropDownList ddl = (DropDownList)ri.FindControl("ddlPayMultOption");
                    if (string.IsNullOrEmpty(UserMemo))
                    {
                        UserMemo = ddl.SelectedItem.Text + "|^|" + ddl.SelectedItem.Value;
                    }
                    else
                    {
                        UserMemo += "," + ddl.SelectedItem.Text + "|^|" + ddl.SelectedItem.Value;
                    }
                }
                UserMemo += "|#|" + DefaultQuanty;

                if (this.MakeTmallOrder != null)
                {
                    MakeTmallOrder(this, new DataEventArgs<KeyValuePair<string, string>>(new KeyValuePair<string, string>(tbx_TmallOrder.Text, tbx_Mobile.Text)));
                }

                #endregion MergePay
            }
        }

        protected void ResetOrderInfo(object sender, EventArgs e)
        {
            cbx_Tab2.Checked = false;
            tbx_TmallOrder.Text = tbx_Mobile.Text = string.Empty;
            HiddenSingleStoreGuid.Text = string.Empty;
            if (pStore.Visible)
            {
                ddlStore.SelectedIndex = 0;
            }
            if (phPayOption.Visible)
            {
                foreach (RepeaterItem ri in rpPayOption.Items)
                {
                    DropDownList ddl = (DropDownList)ri.FindControl("ddlPayMultOption");
                    ddl.SelectedIndex = 0;
                }
            }
        }

        protected void SearchByKey(object sender, EventArgs e)
        {
            if (SearchList != null)
            {
                cbx_Tab2.Checked = true;
                if (sender is Button)
                {
                    cbx_IsSearchByKey.Checked = ((Button)sender).ID == "btn_SearchByKey";
                }
                SearchList(this, e);
            }
        }

        protected int RetrieveOrderCount()
        {
            return PageCount;
        }

        protected void UpdateHandler(int pageNumber)
        {
            if (this.PageChange != null)
            {
                this.PageChange(this, new DataEventArgs<int>(pageNumber));
            }
        }

        public void SetPageCount()
        {
            gridPager.ResolvePagerView(PageCount > 0 ? 1 : 0, true);
        }

        public void TransText(object sender, EventArgs e)
        {
            if (fu_TransText.HasFile)
            {
                if (fu_TransText.FileName.EndsWith(".csv"))
                {
                    StringBuilder builder = new StringBuilder();
                    using (StreamReader reader = new StreamReader(fu_TransText.PostedFile.InputStream, Encoding.GetEncoding("GB2312")))
                    {
                        string line = string.Empty;
                        while ((line = reader.ReadLine()) != null)
                        {
                            builder.AppendLine(ToTraditional(line));
                        }
                    }
                    Response.Clear();
                    Response.ContentType = "text/csv";
                    Response.ContentEncoding = Encoding.GetEncoding("Big5");
                    Response.AddHeader("Content-Disposition", "attachment;filename=" + fu_TransText.FileName.Replace(".csv", "_new.csv"));
                    Response.Write(builder.ToString());
                    Response.End();
                }
                else
                {
                    ShowAlert("請傳入csv檔");
                }
            }
            else
            {
                ShowAlert("查無檔案");
            }
        }

        public void ReSendSms(object sender, GridViewCommandEventArgs e)
        {
            int coupon_id;
            if (e.CommandName == "ReSend" && !string.IsNullOrEmpty(e.CommandArgument.ToString()) && int.TryParse(e.CommandArgument.ToString(), out coupon_id))
            {
                if (ReSendCouponSms != null)
                {
                    ReSendCouponSms(this, new DataEventArgs<int>(coupon_id));
                }
            }
        }
        #endregion

        #region private function
        private void ShowAlert(string message)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "$(function () {$.unblockUI();alert('" + message + "');});", true);
        }

        private void ClearData()
        {
            pan_DealSummary.Visible = false;
            lab_DeliveryTime.Text = lab_Name.Text = lab_OrderTimeEnd.Text = lab_Uid.Text
                = tbx_Mobile.Text = tbx_TmallOrder.Text = string.Empty;
        }

        private void ClearDetail()
        {
            phPayOption.Visible = pStore.Visible
                = phPayOption.Visible = pStore.Visible = pan_Delivery.Visible = false;
            rpPayOption.DataBind();
            tbx_DeliveryAddress.Text = tbx_DeliveryName.Text = tbx_Mobile.Text
               = tbx_SearchKey.Text = tbx_SmsEnd.Text = tbx_SmsStart.Text = tbx_TmallOrder.Text = tbx_SmsMemo.Text = string.Empty;
            ddl_Quantity.SelectedIndex = 0;
        }

        /// <summary>
        /// 簡訊狀態顯示成字串
        /// </summary>
        /// <param name="status"></param>
        /// <returns></returns>
        protected string SmsStatusString(int? status)
        {
            if (!status.HasValue)
            {
                return "無紀錄";
            }
            else
            {
                switch (status)
                {
                    case (int)SmsStatus.Success:
                        return Resources.Localization.SmsSuccess;
                    case (int)SmsStatus.Fail:
                        return Resources.Localization.SmsFail;
                    case (int)SmsStatus.Queue:
                        return Resources.Localization.SmsQueue;
                    case (int)SmsStatus.Reset:
                        return Resources.Localization.SmsReset;
                    case (int)SmsStatus.Ppon:
                        return Resources.Localization.SmsPpon;
                    case (int)SmsStatus.Cancel:
                        return Resources.Localization.Cancel;
                    default:
                        return "error";
                }
            }
        }

        /// <summary>
        /// 顯示核銷狀態文字
        /// </summary>
        /// <param name="status">狀態</param>
        /// <returns></returns>
        protected string TrustStatusString(int status)
        {
            switch (status)
            {
                case (int)TrustStatus.Initial:
                    return Resources.Localization.TrustStatus_Initial;
                case (int)TrustStatus.Trusted:
                    return Resources.Localization.TrustStatus_Trusted;
                case (int)TrustStatus.Verified:
                    return Resources.Localization.TrustStatus_Verified;
                case (int)TrustStatus.Refunded:
                    return Resources.Localization.TrustStatus_Refunded;
                case (int)TrustStatus.Returned:
                    return Resources.Localization.TrustStatus_Returned;
                case (int)TrustStatus.ATM:
                    return Resources.Localization.TrustStatus_ATM;
                default:
                    return "error";
            }
        }

        /// <summary>
        /// 分店名稱+地址
        /// </summary>
        /// <param name="store"></param>
        /// <returns></returns>
        protected string GetStoreOptionTitle(ViewPponStore store)
        {
            return string.Format("{0} {1}{2}{3}",
                store.StoreName, store.CityName, store.TownshipName, store.AddressString);
        }

        /// <summary>
        /// 檢查多選項是否有選擇完整
        /// </summary>
        /// <returns></returns>
        private bool CheckOptions()
        {
            #region Pay
            string Selected = string.Empty;
            foreach (RepeaterItem ri in rpPayOption.Items)
            {
                Selected = ((DropDownList)ri.FindControl("ddlPayMultOption")).SelectedItem.Text;

                if (Selected.IndexOf("請選擇") > -1 || Selected.IndexOf("已售完") > -1)
                {
                    return false;
                }
            }

            #endregion Pay
            return true;
        }

        private bool CheckBranch()
        {
            if (ddlStore.SelectedItem != null && ddlStore.SelectedItem.Text == "請選擇")
            {
                return false;
            }

            return true;
        }

        private bool CheckTmallInfo(out string errormessage)
        {
            Regex r_order = new Regex("[0-9]{15}");
            Regex r_mobile = new Regex("[0-9]{11}");
            errormessage = string.Empty;
            if (!r_order.IsMatch(tbx_TmallOrder.Text))
            {
                errormessage += "天貓訂單為15碼數字。";
            }
            if (r_mobile.IsMatch(tbx_Mobile.Text))
            {
                if (!(tbx_Mobile.Text.StartsWith("1") || tbx_Mobile.Text.StartsWith("8525") ||
                    tbx_Mobile.Text.StartsWith("8529") || tbx_Mobile.Text.StartsWith("8526")
                    || tbx_Mobile.Text.StartsWith("853")))
                {
                    errormessage += "手機號碼開頭為1、852(5、6、9)或853。";
                }
            }
            else
            {
                errormessage += "大陸手機號碼只需要填1開頭的11碼，港澳手機號碼則必須連同國碼一起填入，同樣是填11碼。";
            }
            if (pan_Delivery.Visible && (string.IsNullOrEmpty(tbx_DeliveryAddress.Text) || string.IsNullOrEmpty(tbx_DeliveryName.Text)))
            {
                errormessage += "天貓宅配請填入地址和收件人。";
            }
            return string.IsNullOrEmpty(errormessage);
        }

        private void SetStore(ViewPponStoreCollection stores)
        {
            var count = 0;
            pStore.Visible = true;

            //若有一家以上分店；以下拉式選單方式顯示供使用者點選
            //但分店若只有一家；畫面上直接顯示該店家資料，不須以下拉式選單方式呈現
            if (stores.Count > 1)
            {
                #region 多家分店

                ddlStore.Visible = true;
                ddlStore.Items.Clear();
                ddlStore.Items.Add("請選擇");
                divSingleStore.Visible = false;
                lblSingleStore.Visible = false;
                lblStoreChioce.Visible = true;
                foreach (var store in stores.OrderBy(x => x.SortOrder))
                {
                    ListItem li = new ListItem(GetStoreOptionTitle(store), store.StoreGuid.ToString());
                    if (store.TotalQuantity <= 0 || store.OrderedQuantity >= store.TotalQuantity)
                    {
                        li.Attributes["disabled"] = "disabled";
                        li.Text += "(已售完)";
                    }
                    else
                    {
                        li.Attributes.Remove("disabled");
                    }

                    ddlStore.Items.Add(li);
                    count++;
                }

                #endregion 多家分店
            }
            else if (stores.Count == 1)
            {
                #region 一家分店

                ddlStore.Visible = false;
                divSingleStore.Visible = true;
                lblSingleStore.Visible = true;
                lblStoreChioce.Visible = false;
                lblSingleStore.Text = GetStoreOptionTitle(stores[0]);
                HiddenSingleStoreGuid.Text = stores[0].StoreGuid.ToString();
                count++;

                #endregion 一家分店
            }

            if (count == 0)
            {
                #region 無分店，宅配檔
                ddlStore.DataBind();
                ddlStore.Visible = false;
                lblSingleStore.Visible = false;
                lblStoreChioce.Visible = false;
                HiddenSingleStoreGuid.Text = string.Empty;
                #endregion 無分店，宅配檔
            }
        }

        private static string ToTraditional(string argSource)
        {
            var t = new String(' ', argSource.Length);
            LCMapString(LocaleSystemDefault, LcmapTraditionalChinese, argSource, argSource.Length, t, argSource.Length);
            return t;
        }
        #endregion

        #region view method
        public void SearchDealById(ViewPponDeal deal, VendorDealSalesCount verify_summary, AccessoryGroupCollection multOption, ViewPponStoreCollection stores, int sms_fali_total, int sms_success_total, int sms_queue_total)
        {
            ClearDetail();
            if (deal.BusinessHourGuid == Guid.Empty)
            {
                ShowAlert("查無檔次");
            }
            else if ((deal.BusinessHourStatus & (int)BusinessHourStatus.TmallDeal) == 0)
            {
                ShowAlert("此檔非天貓檔次");
            }
            else if ((deal.BusinessHourStatus & (int)BusinessHourStatus.ComboDealMain) > 0)
            {
                ShowAlert("此檔為多檔次母檔，請重新輸入子檔");
            }
            else
            {
                pan_SmsFailList.Visible = false;
                pan_DealSummary.Visible = true;
                lab_Uid.Text = string.Format("[{0}]", deal.UniqueId);
                lab_Name.Text = deal.EventName;
                lab_OrderTimeEnd.Text = deal.BusinessHourOrderTimeE.ToString("yyyy/MM/dd");
                lab_DeliveryTime.Text = deal.BusinessHourDeliverTimeS.Value.ToString("yyyy/MM/dd ~ ") + deal.BusinessHourDeliverTimeE.Value.ToString("yyyy/MM/dd");
                if (deal.DeliveryType.HasValue && int.Equals((int)DeliveryType.ToHouse, deal.DeliveryType.Value))
                {
                    pan_Delivery.Visible = true;
                }
                else
                {
                    tbx_DeliveryAddress.Text = "天貓憑證訂單";
                    tbx_DeliveryName.Text = "天貓";
                }
                lab_Summary.Text =
                    verify_summary == null ? string.Format("總銷售：0　| 已核銷：0　|　已退貨：0<br/>發送成功：0　|　發送失敗：0　|　等待發送：0") :
                    string.Format("總銷售：{0}　| 已核銷：{1}　|　已退貨：{2}<br/>發送成功：{3}　|　發送失敗：{4}　|　等待發送：{5}",
                    verify_summary.UnverifiedCount + verify_summary.VerifiedCount + verify_summary.ForceVerifyCount + verify_summary.ReturnedCount + verify_summary.ForceReturnCount
                    , verify_summary.VerifiedCount + verify_summary.ForceVerifyCount, verify_summary.ReturnedCount + verify_summary.ForceReturnCount, sms_success_total, sms_fali_total, sms_queue_total);
                if (multOption.Count > 0)
                {
                    rpPayOption.DataSource = multOption;
                    rpPayOption.DataBind();
                    phPayOption.Visible = true;
                }
                if (stores.Count > 0)
                {
                    SetStore(stores);
                }
            }
        }

        public void ShowErrorMessage(string message)
        {
            ShowAlert(message);
        }

        public void GetSearchList(ViewOrderCorrespondingSmsLogCollection list)
        {
            gv_List.DataSource = list;
            gv_List.DataBind();
        }

        public void OrderMakeSuccess(string order_id)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "$(function () {$.unblockUI();alert('" + string.Format("訂單成立成功 ! 訂單號碼:{0}", order_id) + "');});", true);
        }

        public void GetFailSmsList(ViewOrderCorrespondingSmsLogCollection list)
        {
            lab_FailDuration.Text = string.Format("(發送簡訊期間:{0}~{1})", DateTime.Today.AddDays(-3).ToString("yyyy/MM/dd HH:mm"), DateTime.Today.AddDays(1).ToString("yyyy/MM/dd HH:mm"));
            gv_FailList.DataSource = list;
            gv_FailList.DataBind();
        }
        #endregion

    }
}
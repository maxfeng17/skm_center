﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ControlRoom/backend.master" AutoEventWireup="true" CodeBehind="Tmall.aspx.cs" Inherits="LunchKingSite.Web.ControlRoom.SpecialDeal.Tmall" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/ControlRoom/Controls/Pager.ascx" TagName="Pager" TagPrefix="uc1" %>
<%@ Import Namespace="LunchKingSite.DataOrm" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <link type="text/css" href="../../Tools/js/css/ui-lightness/jquery-ui-1.8rc3.custom.css"
        rel="stylesheet" />
    <script type="text/javascript" src="//www.google.com/jsapi"></script>
    <script type="text/javascript" src="//code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
    <script type="text/javascript" src="../../Tools/js/jquery.blockUI.js"></script>
    <style type="text/css">
        .tmall_cell {
            padding-left: 10px;
            padding-right: 10px;
        }
    </style>
    <script type="text/javascript">
        $(function () {
            if ($('#<%=cbx_Tab2.ClientID %>').prop("checked")) {
                $("#tabs").tabs({ active: 1 });
            }
            else {
                $("#tabs").tabs({ active: 0 });
            }
        });

        function block_postback() {
            if (Page_ClientValidate('Send')) {
                $.blockUI({ message: "<h2>執行中...</h2>", css: { width: '150px' } });
            }
            else {
                $.unblockUI();
            }
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1>天貓發碼系統</h1>
    <asp:HiddenField ID="hidMultiBranch" runat="server" />
    <asp:HiddenField ID="hidAllItems" runat="server" />
    <asp:HiddenField ID="hidItemLimits" runat="server" />
    <asp:HiddenField ID="hidBusinessHourGuid" runat="server" />
    <table>
        <tr>
            <td>檔次查詢 : 
            </td>
            <td>
                <asp:DropDownList ID="ddl_DealIDType" runat="server">
                    <asp:ListItem Text="檔號" Value="uid"></asp:ListItem>
                    <asp:ListItem Text="Bid" Value="bid"></asp:ListItem>
                </asp:DropDownList>
                &nbsp;
    <asp:TextBox ID="tbx_Id" runat="server" Width="300"></asp:TextBox>
                &nbsp;
    <asp:Button ID="btn_SearchDeal" runat="server" Text="確認" OnClick="SearchDeal" /></td>
        </tr>
        <tr>
            <td>報表簡繁轉換
            </td>
            <td>
                <asp:FileUpload ID="fu_TransText" runat="server" /><asp:Button ID="btn_TransText" runat="server" Text="轉換" OnClick="TransText" />
            </td>
        </tr>
    </table>
    <br />
    <asp:Panel ID="pan_SmsFailList" runat="server">
        <table>
            <tr>
                <td>發送失敗紀錄&nbsp;<asp:Label ID="lab_FailDuration" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="gv_FailList" runat="server" AutoGenerateColumns="false" EmptyDataRowStyle-BackColor="White" EmptyDataText="查無資料" OnRowCommand="ReSendSms">
                        <Columns>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <table style="width: 100%; background-color: white;">
                                        <tr style="background-color: lightpink; text-align: center;">
                                            <td>檔次名稱</td>
                                            <td>17Life訂單編號</td>
                                            <td>憑證編號</td>
                                            <td>發送時間</td>
                                            <td>天貓訂單編號</td>
                                            <td>手機號碼</td>
                                            <td>簡訊備註</td>
                                            <td>憑證狀態</td>
                                            <td>重新發送</td>
                                        </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td class="tmall_cell">
                                            <a href="../ppon/setup.aspx?bid=<%# ((ViewOrderCorrespondingSmsLog)(Container.DataItem)).BusinessHourGuid %>" target="_blank" style="color: blue;">
                                                <%# ((ViewOrderCorrespondingSmsLog)(Container.DataItem)).ItemName %></a></td>
                                        <td class="tmall_cell">
                                            <a href="../Order/order_detail.aspx?oid=<%# ((ViewOrderCorrespondingSmsLog)(Container.DataItem)).OrderGuid %>" target="_blank" style="color: blue;">
                                                <%# ((ViewOrderCorrespondingSmsLog)(Container.DataItem)).OrderId %></a></td>
                                        <td class="tmall_cell"><%# ((ViewOrderCorrespondingSmsLog)(Container.DataItem)).SequenceNumber %></td>
                                        <td class="tmall_cell"><%# ((ViewOrderCorrespondingSmsLog)(Container.DataItem)).SmsCreateTime.Value.ToString("yyyy/MM/dd <br/>HH:mm:ss") %></td>
                                        <td class="tmall_cell"><%# ((ViewOrderCorrespondingSmsLog)(Container.DataItem)).RelatedOrderId %></td>
                                        <td class="tmall_cell"><%# ((ViewOrderCorrespondingSmsLog)(Container.DataItem)).Mobile %></td>
                                        <td class="tmall_cell"><%# ((ViewOrderCorrespondingSmsLog)(Container.DataItem)).Memo %></td>
                                        <td class="tmall_cell"><%# SmsStatusString(((ViewOrderCorrespondingSmsLog)(Container.DataItem)).SmsStatus.Value)  %></td>
                                        <td class="tmall_cell">
                                            <asp:Button ID="btn_ReSend" runat="server" Text="重新發送" CommandArgument="<%# ((ViewOrderCorrespondingSmsLog)(Container.DataItem)).CouponId %>" CommandName="ReSend" /></td>
                                    </tr>
                                </ItemTemplate>
                                <AlternatingItemTemplate>
                                    <tr style="background-color: pink;">
                                        <td class="tmall_cell">
                                            <a href="../ppon/setup.aspx?bid=<%# ((ViewOrderCorrespondingSmsLog)(Container.DataItem)).BusinessHourGuid %>" target="_blank" style="color: blue;">
                                                <%# ((ViewOrderCorrespondingSmsLog)(Container.DataItem)).ItemName %></a></td>
                                        <td class="tmall_cell">
                                            <a href="../Order/order_detail.aspx?oid=<%# ((ViewOrderCorrespondingSmsLog)(Container.DataItem)).OrderGuid %>" target="_blank" style="color: blue;">
                                                <%# ((ViewOrderCorrespondingSmsLog)(Container.DataItem)).OrderId %></a></td>
                                        <td class="tmall_cell"><%# ((ViewOrderCorrespondingSmsLog)(Container.DataItem)).SequenceNumber %></td>
                                        <td class="tmall_cell"><%# ((ViewOrderCorrespondingSmsLog)(Container.DataItem)).SmsCreateTime.Value.ToString("yyyy/MM/dd <br/>HH:mm:ss") %></td>
                                        <td class="tmall_cell"><%# ((ViewOrderCorrespondingSmsLog)(Container.DataItem)).RelatedOrderId %></td>
                                        <td class="tmall_cell"><%# ((ViewOrderCorrespondingSmsLog)(Container.DataItem)).Mobile %></td>
                                        <td class="tmall_cell"><%# ((ViewOrderCorrespondingSmsLog)(Container.DataItem)).Memo %></td>
                                        <td class="tmall_cell"><%# SmsStatusString(((ViewOrderCorrespondingSmsLog)(Container.DataItem)).SmsStatus.Value)  %></td>
                                        <td class="tmall_cell">
                                            <asp:Button ID="btn_ReSend" runat="server" Text="重新發送" CommandArgument="<%# ((ViewOrderCorrespondingSmsLog)(Container.DataItem)).CouponId %>" CommandName="ReSend" /></td>
                                    </tr>
                                </AlternatingItemTemplate>
                                <FooterTemplate>
                                    </table>
                                </FooterTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
        </table>

    </asp:Panel>
    <asp:Panel ID="pan_DealSummary" runat="server" Visible="false">
        <table>
            <tr>
                <td colspan="2" style="font-weight: bolder; font-size: large;">
                    <asp:Label ID="lab_Uid" runat="server"></asp:Label>
                    <asp:Label ID="lab_Name" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>結檔時間：</td>
                <td>
                    <asp:Label ID="lab_OrderTimeEnd" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>兌換期間：</td>
                <td>
                    <asp:Label ID="lab_DeliveryTime" runat="server"></asp:Label>

                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Label ID="lab_Summary" runat="server"></asp:Label>
                </td>
            </tr>
        </table>
        <br />
        <div id="tabs">
            <ul>
                <li><a href="#tabs-1">憑證發送</a></li>
                <li><a href="#tabs-2">紀錄查詢</a></li>
            </ul>
            <div id="tabs-1">
                <table>
                    <tr>
                        <td>天貓訂單編號：</td>
                        <td>
                            <asp:TextBox ID="tbx_TmallOrder" runat="server" MaxLength="15"></asp:TextBox>
                        </td>
                        <td>手機號碼：</td>
                        <td>
                            <asp:TextBox ID="tbx_Mobile" runat="server" MaxLength="11"></asp:TextBox>
                        </td>
                        <td rowspan="2" style="font-size: small">- 大陸，1開頭，11位數<br />
                            - 香港國碼 852，852 + 8位數（5、9、6開頭）<br />
                            - 澳門國碼 853，853 + 8位數（6開頭）
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <asp:Panel ID="pan_Delivery" runat="server" Visible="false">
                                <table style="width: 100%">
                                    <tr>
                                        <td>宅配收件人：</td>
                                        <td>
                                            <asp:TextBox ID="tbx_DeliveryName" runat="server"></asp:TextBox>
                                        </td>
                                        <td>宅配地址：</td>
                                        <td>
                                            <asp:TextBox ID="tbx_DeliveryAddress" runat="server"></asp:TextBox>

                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <asp:RequiredFieldValidator ID="rfv_TmallOrder" runat="server" ErrorMessage="請填入天貓訂單15碼數字" ControlToValidate="tbx_TmallOrder" Display="Dynamic"
                                ValidationGroup="Send"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="rev_TmallOrder" runat="server" ErrorMessage="天貓訂單為15碼數字"
                                ControlToValidate="tbx_TmallOrder" Display="Dynamic" ValidationExpression="[0-9]{15}" ValidationGroup="Send"></asp:RegularExpressionValidator>
                        </td>
                        <td></td>
                        <td>
                            <asp:RequiredFieldValidator ID="rfv_Mobile" runat="server" ErrorMessage="請填入手機號碼11碼數字" ControlToValidate="tbx_Mobile" Display="Dynamic"
                                ValidationGroup="Send"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="rev_Mobile" runat="server" ErrorMessage="手機號碼為11碼數字"
                                ControlToValidate="tbx_Mobile" Display="Dynamic" ValidationExpression="[0-9]{11}" ValidationGroup="Send"></asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>簡訊備註 : </td>
                        <td colspan="4">
                            <asp:TextBox ID="tbx_SmsMemo" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5">
                            <asp:Panel ID="pnlOption" runat="server">
                                <asp:UpdatePanel ID="upxy" runat="server" ChildrenAsTriggers="true" RenderMode="Inline">
                                    <ContentTemplate>
                                        <div class="form-unit">
                                            <asp:Label ID="lblStoreChioce" runat="server" CssClass="unit-label" Text="分店選擇" Visible="false"></asp:Label>
                                            <div class="data-input">
                                                <asp:PlaceHolder ID="pStore" runat="server">
                                                    <asp:PlaceHolder ID="divStore" runat="server">
                                                        <asp:DropDownList ID="ddlStore" runat="server">
                                                        </asp:DropDownList>
                                                        <span id="branchErr" class="error-text" style="display: none;">請選擇</span>
                                                    </asp:PlaceHolder>
                                                    <asp:TextBox ID="HiddenSingleStoreGuid" runat="server" Visible="false"></asp:TextBox>
                                                </asp:PlaceHolder>
                                            </div>
                                            <div id="divSingleStore" runat="server" class="data-input">
                                                <p>
                                                    <asp:Label ID="lblSingleStore" runat="server"></asp:Label>
                                                </p>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                                <asp:PlaceHolder ID="phPayOption" runat="server">
                                    <asp:Repeater ID="rpPayOption" runat="server" OnItemDataBound="rpOption_ItemDataBound">
                                        <HeaderTemplate>
                                            <table>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <%# Container.ItemIndex % 2 == 0 ? "<tr>" : string.Empty%>
                                            <td>
                                                <div class="form-unit">
                                                    <label id="MultOptions" class="unit-label" for="">
                                                        <asp:Label ID="lblPayMultOption" runat="server" Text='<%# Eval("accessorygroupname")%>'></asp:Label>
                                                    </label>
                                                    <div class="data-input">
                                                        <asp:DropDownList ID="ddlPayMultOption" runat="server">
                                                        </asp:DropDownList>
                                                        <span class="error-text" style="display: none;">請選擇</span>
                                                    </div>
                                                </div>
                                            </td>
                                        </ItemTemplate>
                                        <AlternatingItemTemplate>
                                            <td>
                                                <div class="form-unit">
                                                    <label id="MultOptions" class="unit-label" for="">
                                                        <asp:Label ID="lblPayMultOption" runat="server" Text='<%# Eval("accessorygroupname")%>'></asp:Label>
                                                    </label>
                                                    <div class="data-input">
                                                        <asp:DropDownList ID="ddlPayMultOption" runat="server">
                                                        </asp:DropDownList>
                                                        <span class="error-text" style="display: none;">請選擇</span>
                                                    </div>
                                                </div>
                                            </td>
                                            <%# Container.ItemIndex % 2 == 1 ? "</tr>" : string.Empty%>
                                        </AlternatingItemTemplate>
                                        <FooterTemplate>
                                            </table>
                                        </FooterTemplate>
                                    </asp:Repeater>
                                </asp:PlaceHolder>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5">
                            <br />
                            數量選擇
                               <br />
                            <asp:DropDownList ID="ddl_Quantity" runat="server">
                                <asp:ListItem Text="1" Value="1"></asp:ListItem>
                                <asp:ListItem Text="2" Value="2"></asp:ListItem>
                                <asp:ListItem Text="3" Value="3"></asp:ListItem>
                                <asp:ListItem Text="4" Value="4"></asp:ListItem>
                                <asp:ListItem Text="5" Value="5"></asp:ListItem>
                                <asp:ListItem Text="6" Value="6"></asp:ListItem>
                                <asp:ListItem Text="7" Value="7"></asp:ListItem>
                                <asp:ListItem Text="8" Value="8"></asp:ListItem>
                                <asp:ListItem Text="9" Value="9"></asp:ListItem>
                                <asp:ListItem Text="10" Value="10"></asp:ListItem>
                                <asp:ListItem Text="11" Value="11"></asp:ListItem>
                                <asp:ListItem Text="12" Value="12"></asp:ListItem>
                                <asp:ListItem Text="13" Value="13"></asp:ListItem>
                                <asp:ListItem Text="14" Value="14"></asp:ListItem>
                                <asp:ListItem Text="15" Value="15"></asp:ListItem>
                                <asp:ListItem Text="16" Value="16"></asp:ListItem>
                                <asp:ListItem Text="17" Value="17"></asp:ListItem>
                                <asp:ListItem Text="18" Value="18"></asp:ListItem>
                                <asp:ListItem Text="19" Value="19"></asp:ListItem>
                                <asp:ListItem Text="20" Value="20"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5">
                            <br />
                            <asp:Button ID="btn_SendOrder" runat="server" Text="發送" OnClick="MakeOrderSendSms" ValidationGroup="Send" OnClientClick="block_postback();" />&nbsp;&nbsp;
                    <asp:Button ID="btn_Reset" runat="server" Text="重設" OnClick="ResetOrderInfo" />
                        </td>
                    </tr>
                </table>
            </div>
            <div id="tabs-2">
                <table>
                    <tr>
                        <td>選擇查詢項目:</td>
                        <td>
                            <asp:DropDownList ID="ddl_SearchKey" runat="server">
                                <asp:ListItem Text="17Life訂單編號" Value="0"></asp:ListItem>
                                <asp:ListItem Text="天貓訂單編號" Value="1"></asp:ListItem>
                                <asp:ListItem Text="手機號碼" Value="2"></asp:ListItem>
                                <asp:ListItem Text="憑證編號" Value="3"></asp:ListItem>
                            </asp:DropDownList>
                            &nbsp;&nbsp;
                        <asp:TextBox ID="tbx_SearchKey" runat="server"></asp:TextBox>
                        </td>
                        <td>
                            <asp:Button ID="btn_SearchByKey" runat="server" Text="查詢" OnClick="SearchByKey" ValidationGroup="Search1" />
                        </td>
                    </tr>
                    <tr>
                        <td>憑證狀態:</td>
                        <td>
                            <asp:DropDownList ID="ddl_SmsStatus" runat="server">
                                <asp:ListItem Text="全部" Value="0"></asp:ListItem>
                                <asp:ListItem Text="等待發送" Value="1"></asp:ListItem>
                                <asp:ListItem Text="發送成功" Value="3"></asp:ListItem>
                                <asp:ListItem Text="發送失敗" Value="2"></asp:ListItem>
                            </asp:DropDownList>
                            &nbsp;&nbsp;
                        <asp:TextBox ID="tbx_SmsStart" runat="server"></asp:TextBox>
                            <cc1:CalendarExtender ID="ce_SmsStart" TargetControlID="tbx_SmsStart"
                                runat="server">
                            </cc1:CalendarExtender>
                            至
                        <asp:TextBox ID="tbx_SmsEnd" runat="server"></asp:TextBox>
                            <cc1:CalendarExtender ID="ce_SmsEnd" TargetControlID="tbx_SmsEnd"
                                runat="server">
                            </cc1:CalendarExtender>
                        </td>
                        <td>
                            <asp:Button ID="btn_SearchBySms" runat="server" Text="查詢" ValidationGroup="Search2" OnClick="SearchByKey" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <span style="display: none;">
                                <asp:CheckBox ID="cbx_Tab2" runat="server" /></span>
                            <asp:CheckBox ID="cbx_IsSearchByKey" runat="server" Visible="false" />
                            <asp:RequiredFieldValidator ID="rfv_Search_1" runat="server" ErrorMessage="請輸入搜尋條件" Display="Dynamic" ControlToValidate="tbx_SearchKey" ValidationGroup="Search1"></asp:RequiredFieldValidator>
                            <asp:RequiredFieldValidator ID="rfv_SmsStart" runat="server" ErrorMessage="請輸入起始時間" Display="Dynamic" ControlToValidate="tbx_SmsStart" ValidationGroup="Search2"></asp:RequiredFieldValidator>
                            <asp:RequiredFieldValidator ID="rfv_SmsEnd" runat="server" ErrorMessage="請輸入終止時間" Display="Dynamic" ControlToValidate="tbx_SmsEnd" ValidationGroup="Search2"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <asp:Panel ID="pan_List" runat="server">
                                <asp:GridView ID="gv_List" runat="server" AutoGenerateColumns="false" EmptyDataRowStyle-BackColor="White" EmptyDataText="查無資料">
                                    <Columns>
                                        <asp:TemplateField>
                                            <HeaderTemplate>
                                                <table style="width: 100%; background-color: white;">
                                                    <tr style="background-color: lightblue; text-align: center;">
                                                        <td>17Life訂單編號</td>
                                                        <td>憑證編號</td>
                                                        <td>確認碼</td>
                                                        <td>核銷狀態</td>
                                                        <td>憑證狀態</td>
                                                        <td>發送時間</td>
                                                        <td>天貓訂單資料</td>
                                                    </tr>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <td class="tmall_cell">
                                                        <a href="../Order/order_detail.aspx?oid=<%# ((ViewOrderCorrespondingSmsLog)(Container.DataItem)).OrderGuid %>" target="_blank" style="color: blue;">
                                                            <%# ((ViewOrderCorrespondingSmsLog)(Container.DataItem)).OrderId %></a></td>
                                                    <td class="tmall_cell"><%# ((ViewOrderCorrespondingSmsLog)(Container.DataItem)).SequenceNumber %></td>
                                                    <td class="tmall_cell"><%# ((ViewOrderCorrespondingSmsLog)(Container.DataItem)).Code %></td>
                                                    <td class="tmall_cell"><%# TrustStatusString(((ViewOrderCorrespondingSmsLog)(Container.DataItem)).TrustStatus) %></td>
                                                    <td class="tmall_cell"><%# SmsStatusString(((ViewOrderCorrespondingSmsLog)(Container.DataItem)).SmsStatus)  %></td>
                                                    <td class="tmall_cell"><%# ((ViewOrderCorrespondingSmsLog)(Container.DataItem)).SmsCreateTime.HasValue?((ViewOrderCorrespondingSmsLog)(Container.DataItem)).SmsCreateTime.Value.ToString("yyyy/MM/dd <br/>HH:mm:ss"):string.Empty %></td>
                                                    <td class="tmall_cell">訂單編號:<%# ((ViewOrderCorrespondingSmsLog)(Container.DataItem)).RelatedOrderId %><br />
                                                        手機號碼:<%# ((ViewOrderCorrespondingSmsLog)(Container.DataItem)).Mobile %>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                            <AlternatingItemTemplate>
                                                <tr style="background-color: lightyellow;">
                                                    <td class="tmall_cell">
                                                        <a href="../Order/order_detail.aspx?oid=<%# ((ViewOrderCorrespondingSmsLog)(Container.DataItem)).OrderGuid %>" target="_blank" style="color: blue;">
                                                            <%# ((ViewOrderCorrespondingSmsLog)(Container.DataItem)).OrderId %></a></td>
                                                    <td class="tmall_cell"><%# ((ViewOrderCorrespondingSmsLog)(Container.DataItem)).SequenceNumber %></td>
                                                    <td class="tmall_cell"><%# ((ViewOrderCorrespondingSmsLog)(Container.DataItem)).Code %></td>
                                                    <td class="tmall_cell"><%# TrustStatusString(((ViewOrderCorrespondingSmsLog)(Container.DataItem)).TrustStatus) %></td>
                                                    <td class="tmall_cell"><%# SmsStatusString(((ViewOrderCorrespondingSmsLog)(Container.DataItem)).SmsStatus)  %></td>
                                                    <td class="tmall_cell"><%# ((ViewOrderCorrespondingSmsLog)(Container.DataItem)).SmsCreateTime.HasValue?((ViewOrderCorrespondingSmsLog)(Container.DataItem)).SmsCreateTime.Value.ToString("yyyy/MM/dd <br/>HH:mm:ss"):string.Empty %></td>
                                                    <td class="tmall_cell">訂單編號:<%# ((ViewOrderCorrespondingSmsLog)(Container.DataItem)).RelatedOrderId %><br />
                                                        手機號碼:<%# ((ViewOrderCorrespondingSmsLog)(Container.DataItem)).Mobile %>
                                                    </td>
                                                </tr>
                                            </AlternatingItemTemplate>
                                            <FooterTemplate>
                                                </table>
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                                <br />
                                <uc1:Pager ID="gridPager" runat="server" PageSize="15" OnGetCount="RetrieveOrderCount"
                                    OnUpdate="UpdateHandler" />
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </asp:Panel>
</asp:Content>

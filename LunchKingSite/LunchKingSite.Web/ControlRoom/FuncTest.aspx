﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FuncTest.aspx.cs" Inherits="LunchKingSite.Web.ControlRoom.FuncTest" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <fieldset>
                <legend>Ite2 簡訊    
                </legend>
                服務位置:<asp:Literal ID="litIte2Url" runat="server" />
                <br />
                
                手機號碼: <asp:TextBox ID="txtIte2Mobile" runat="server" />
                <br />
                <asp:Button ID="btnIte2Test" runat="server" Text="開始測試" OnClick="btnIte2Test_Click" />
                <br />
                <asp:Label ID="lbIte2Message" runat="server" ForeColor="Red" EnableViewState="false" />
            </fieldset>
        </div>
        <div style="margin-top:10px">
            <fieldset>
                <legend>Sybase 簡訊    
                </legend>
                服務位置:<asp:Literal ID="litSybaseUrl" runat="server" />
                <br />
                
                手機號碼: <asp:TextBox ID="txtSybaseMobile" runat="server" />
                <br />
                <asp:Button ID="btnSybaseTest" runat="server" Text="開始測試" OnClick="btnSybaseTest_Click" />
                <br />
                <asp:Label ID="lbSybaseMessage" runat="server" ForeColor="Red" EnableViewState="false" />
            </fieldset>
        </div>
    </form>
</body>
</html>

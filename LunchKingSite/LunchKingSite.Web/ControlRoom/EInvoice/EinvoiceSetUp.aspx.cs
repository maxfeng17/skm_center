﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.Core.Component;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using System.Data;
using System.Text;
using System.IO;
using System.Text.RegularExpressions;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core.UI;
using LunchKingSite.WebLib.Component;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using System.Collections;
namespace LunchKingSite.Web.ControlRoom.EInvoice
{
    public partial class EinvoiceSetUp : RolePage
    {
        IOrderProvider op;
        IMemberProvider mp;
        ISysConfProvider cp;
        protected void Page_Load(object sender, EventArgs e)
        {
            op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
            mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
            cp = ProviderFactory.Instance().GetConfig();
            if (!IsPostBack)
                PageInit();
        }
        protected void PageInit()
        {
            ListItem[] hours = new ListItem[] { new ListItem("0", "0"), new ListItem("1", "1"), new ListItem("2", "2"), new ListItem("3", "3"), new ListItem("4", "4"), new ListItem("5", "5"), new ListItem("6", "6") ,
            new ListItem("7", "7") ,new ListItem("8", "8") ,new ListItem("9", "9"), new ListItem("10", "10") ,new ListItem("11", "11") ,new ListItem("12", "12"),new ListItem("13", "13"),new ListItem("14", "14"),new ListItem("15", "15")
            ,new ListItem("16", "16"),new ListItem("17", "17"),new ListItem("18", "18"),new ListItem("19", "19"),new ListItem("20", "20"),new ListItem("21", "21"),new ListItem("22", "22"),new ListItem("23", "23")};
            ListItem[] minutes = new ListItem[] { new ListItem("0", "0"), new ListItem("30", "30") };
            int imonth = DateTime.Now.Month;
            int iyear = DateTime.Now.Year;
            List<ListItem> invoiceYearList = new List<ListItem>();
            for (int year = iyear; year <= iyear + 6; year++)
            {
                invoiceYearList.Add(new ListItem(year.ToString(), year.ToString()));
            }
            ddl_invoiceYear.DataSource = ddl_invoiceYear2.DataSource = ddl_invoiceYear3.DataSource = invoiceYearList;
            ddl_MailHourStart.DataSource = ddl_MailHourEnd.DataSource = hours;
            ddl_MailMiunteStart.DataSource = ddl_MailMiunteEnd.DataSource = minutes;
            List<ListItem> month = new List<ListItem>();
            for (int i = 1; i < 13; i = i + 2)
            {
                month.Add(new ListItem(i.ToString().PadLeft(2, '0') + "~" + (i + 1).ToString().PadLeft(2, '0'), i.ToString().PadLeft(2, '0')));
            }

            ddlInvoiceTimeOption.Items.Add(new ListItem { Text = "開立發票時間", Value = ((int)InvoiceQueryOption.InvoiceNumberTime).ToString() });
            ddlInvoiceTimeOption.Items.Add(new ListItem { Text = "申請印製時間", Value = ((int)InvoiceQueryOption.RequestTime).ToString() });

            ddl_invoiceMonth.DataSource = ddl_invoiceMonth2.DataSource = ddl_invoiceMonth3.DataSource = month;

            ddl_InvoiceType.DataSource = new List<ListItem>() { new ListItem(Resources.Localization.A0401, ((int)EinvoiceType.C0401).ToString()), new ListItem(Resources.Localization.B0301, ((int)EinvoiceType.D0401).ToString()) };
            ddl_Cancel_Type.DataSource = new List<ListItem>() { new ListItem(Resources.Localization.A0501, ((int)EinvoiceType.C0501).ToString()), new ListItem(Resources.Localization.B0401, ((int)EinvoiceType.D0501).ToString()) };
            rbl_PaperInvoiceType.DataSource = new List<ListItem>() { new ListItem(Resources.Localization.Paper2Einvoice, ((int)InvoiceMode2.Duplicate).ToString()), new ListItem(Resources.Localization.Paper3Einvoice, ((int)InvoiceMode2.Triplicate).ToString()) };
            rbl_InvoiceUserType.DataSource = new List<ListItem>() { new ListItem(Helper.GetDescription(InvoiceUserType.Customer), ((int)InvoiceUserType.Customer).ToString()), new ListItem(Helper.GetDescription(InvoiceUserType.Vendor), ((int)InvoiceUserType.Vendor).ToString()) };
            ddl_Cancel_Type.DataTextField = ddl_InvoiceType.DataTextField = ddl_invoiceYear.DataTextField = ddl_invoiceYear3.DataTextField = ddl_invoiceMonth.DataTextField = ddl_invoiceMonth2.DataTextField = ddl_invoiceMonth3.DataTextField = rbl_PaperInvoiceType.DataTextField = ddl_MailMiunteStart.DataTextField = ddl_MailMiunteEnd.DataTextField = ddl_MailHourStart.DataTextField = ddl_MailHourEnd.DataTextField = rbl_InvoiceUserType.DataTextField = "Text";
            ddl_Cancel_Type.DataValueField = ddl_InvoiceType.DataValueField = ddl_invoiceYear.DataValueField = ddl_invoiceYear3.DataValueField = ddl_invoiceMonth.DataValueField = ddl_invoiceMonth2.DataValueField = ddl_invoiceMonth3.DataValueField = rbl_PaperInvoiceType.DataValueField = ddl_MailMiunteStart.DataValueField = ddl_MailMiunteEnd.DataValueField = ddl_MailHourStart.DataValueField = ddl_MailHourEnd.DataValueField = rbl_InvoiceUserType.DataValueField = "Value";
            ddl_invoiceMonth2.SelectedValue = (imonth % 2) == 0 ? (imonth - 1).ToString().PadLeft(2, '0') : imonth.ToString().PadLeft(2, '0');
            ddl_invoiceMonth.SelectedValue = (imonth % 2) == 0 ? ((imonth + 1) % 12).ToString().PadLeft(2, '0') : ((imonth + 2) % 12).ToString().PadLeft(2, '0');
            ddl_invoiceMonth3.SelectedValue = (imonth % 2) == 0 ? ((imonth - 3) <= 0 ? (imonth + 9) : (imonth - 3)).ToString().PadLeft(2, '0') : ((imonth - 2) <= 0 ? (imonth + 10) : (imonth - 2)).ToString().PadLeft(2, '0');
            ddl_invoiceYear2.SelectedValue = ddl_invoiceYear.SelectedValue = ddl_invoiceYear3.SelectedValue = iyear.ToString();
            ddl_invoiceYear.DataBind(); ddl_invoiceYear2.DataBind(); ddl_invoiceYear3.DataBind();
            ddl_invoiceMonth.DataBind(); ddl_invoiceMonth2.DataBind(); ddl_invoiceMonth3.DataBind();
            ddl_Cancel_Type.DataBind();
            rbl_PaperInvoiceType.DataBind();
            ddl_InvoiceType.DataBind();
            rbl_PaperInvoiceType.SelectedIndex = 0;
            rbl_InvoiceUserType.DataBind();
            rbl_InvoiceUserType.SelectedIndex = 0;
            ddl_MailHourStart.DataBind(); ddl_MailHourEnd.DataBind();
            ddl_MailMiunteStart.DataBind(); ddl_MailMiunteEnd.DataBind();

        }
        /// <summary>
        /// 產生發票序號檔
        /// </summary>
        protected void GenerateInvoice(object sender, EventArgs e)
        {
            int istart, iend;
            //起迄票軌
            if (int.TryParse(tbx_invoiceStart.Text, out istart) && int.TryParse(tbx_invoiceEnd.Text, out iend) && iend >= istart)
            {
                string dateCode = new DateTime(int.Parse(ddl_invoiceYear.SelectedValue), int.Parse(ddl_invoiceMonth.SelectedValue), 1).ToString("yyyyMM");
                if (op.EinvoiceSerialCollectionGetByDate(dateCode).Any())
                {
                    tbx_invoiceMessage.Text = tbx_invoiceHead.Text = tbx_invoiceStart.Text = tbx_invoiceEnd.Text = string.Empty;
                    lab_Message.Text = "新增失敗，發票期別已存在";
                    return;
                }
                int half = (iend - istart + 1) / 2;
                if (half - cp.VendorInvoiceNumberCount > 0)
                {
                    EinvoiceSerial data = new EinvoiceSerial();
                    data.HeadCode = tbx_invoiceHead.Text;   //字軌碼前兩碼英文
                    data.MinSerial = istart;                       //最小序號
                    data.NextSerial = istart;                      //目前已開立至號碼  
                    data.MaxSerial = istart + half - 1;                //最大序號  
                    data.StartDate = new DateTime(int.Parse(ddl_invoiceYear.SelectedValue), int.Parse(ddl_invoiceMonth.SelectedValue), 1);          //發票有效日期(單月1號)
                    data.EndDate = new DateTime(int.Parse(ddl_invoiceYear.SelectedValue), (int.Parse(ddl_invoiceMonth.SelectedValue) + 1), 1);      //雙月最後一號  
                    data.DateCode = dateCode;    //年月碼
                    data.CreateTime = DateTime.Now;
                    data.Creator = User.Identity.Name;
                    data.Message = tbx_invoiceMessage.Text;
                    data.VendorMinSerial = data.VendorNextSerial = data.MaxSerial - (cp.VendorInvoiceNumberCount - 1);
                    data.WinnerMailSendStatus = (int)WinnerMailSendStatus.Initial;
                    op.EinvoiceSetSerial(data);

                    data = new EinvoiceSerial();
                    data.HeadCode = tbx_invoiceHead.Text;   //字軌碼前兩碼英文
                    data.MinSerial = istart + half;              //最小序號
                    data.NextSerial = istart + half;             //目前已開立至號碼  
                    data.MaxSerial = iend;                            //最大序號  
                    data.StartDate = new DateTime(int.Parse(ddl_invoiceYear.SelectedValue), int.Parse(ddl_invoiceMonth.SelectedValue) + 1, 1);          //發票有效日期(單月1號)
                    data.EndDate = new DateTime(((int.Parse(ddl_invoiceMonth.SelectedValue) + 2) < 12) ? int.Parse(ddl_invoiceYear.SelectedValue) : int.Parse(ddl_invoiceYear.SelectedValue) + 1, (int.Parse(ddl_invoiceMonth.SelectedValue) + 2) % 12, 1);      //雙月最後一號  
                    data.DateCode = dateCode;    //年月碼
                    data.CreateTime = DateTime.Now;
                    data.Creator = User.Identity.Name;
                    data.Message = tbx_invoiceMessage.Text;
                    data.VendorMinSerial = data.VendorNextSerial = data.MaxSerial - (cp.VendorInvoiceNumberCount - 1);
                    data.WinnerMailSendStatus = (int)WinnerMailSendStatus.Initial;
                    op.EinvoiceSetSerial(data);

                    tbx_invoiceMessage.Text = tbx_invoiceHead.Text = tbx_invoiceStart.Text = tbx_invoiceEnd.Text = string.Empty;
                    lab_Message.Text = "發票字軌已設定";
                }
                else
                {
                    ClientScript.RegisterStartupScript(GetType(), "error", "alert('廠商發票組數超過總發票數，請聯絡IT！');", true);
                }
            }
        }
        protected void GetEinvoiceSerialCollection(object sender, EventArgs e)
        {
            EinvoiceSerialCollection data = op.EinvoiceSerialCollectionGetByDate(ddl_invoiceYear2.SelectedValue + ddl_invoiceMonth2.SelectedValue);
            if (data.FirstOrDefault().VendorMinSerial > 0)
            { 
                data.AddRange(data);
            }
            gv_EinvoiceSerialCollection.DataSource = data.OrderBy(x => x.Id);
            gv_EinvoiceSerialCollection.DataBind();
        }

        //protected void Einvoice_DataBound(object sender, GridViewRowEventArgs e)
        //{
        //    if (e.Row.DataItem is EinvoiceMain)
        //    {
        //        EinvoiceMain detail = (EinvoiceMain)e.Row.DataItem;
        //        Label invoicemode = (Label)e.Row.FindControl("lbl_InvoiceMode");
        //        Button paperrequest = (Button)e.Row.FindControl("lbt_PaperRequest");
        //        GridView gv = (GridView)e.Row.FindControl("gv_EinvoiceDetail");
        //        paperrequest.Enabled = detail.InvoicePaperedTime == new DateTime(1900, 1, 1) && !detail.InvoicePapered;
        //        switch ((EinvoiceMode)detail.InvoiceMode)
        //        {
        //            case EinvoiceMode.DonationA:
        //                paperrequest.Enabled = false;
        //                invoicemode.Text = Resources.Localization.DonationA;
        //                break;
        //            case EinvoiceMode.DonationB:
        //                paperrequest.Enabled = false;
        //                invoicemode.Text = Resources.Localization.DonationB;
        //                break;
        //            case EinvoiceMode.Einvoice2:
        //                invoicemode.Text = Resources.Localization.Einvoice2;
        //                break;
        //            case EinvoiceMode.Einvoice3:
        //                invoicemode.Text = Resources.Localization.Einvoice3;
        //                break;
        //            case EinvoiceMode.PaperInvoice2:
        //                invoicemode.Text = Resources.Localization.PaperInvoice2;
        //                break;
        //            case EinvoiceMode.PaperInvoice3:
        //                invoicemode.Text = Resources.Localization.PaperInvoice3;
        //                break;
        //        }
        //        gv.DataSource = op.EinvoiceDetailCollectionGet(detail.Id);
        //        gv.DataBind();
        //    }
        //    else if (e.Row.DataItem is EinvoiceDetail)
        //    {
        //        EinvoiceDetail detail = (EinvoiceDetail)e.Row.DataItem;
        //        Label invoicetype = (Label)e.Row.FindControl("lbl_InvoiceType");
        //        GetEinvoiceTypeString(detail.InvoiceType, invoicetype);
        //    }
        //    else if (e.Row.DataItem is EinvoiceLog)
        //    {
        //        EinvoiceLog log = (EinvoiceLog)e.Row.DataItem;
        //        Label invoicetype = (Label)e.Row.FindControl("lbl_InvoiceType");
        //        GetEinvoiceTypeString(log.InvoiceType, invoicetype);
        //    }
        //}
        protected void GetEinvoiceTypeString(int type, Label lab_Type)
        {
            switch ((EinvoiceType)type)
            {
                case EinvoiceType.C0401:
                    lab_Type.Text = Resources.Localization.A0401;
                    break;
                case EinvoiceType.D0401:
                    lab_Type.Text = Resources.Localization.B0301;
                    break;
                case EinvoiceType.D0501:
                    lab_Type.Text = Resources.Localization.B0401;
                    break;
                case EinvoiceType.C0501:
                    lab_Type.Text = Resources.Localization.A0501;
                    break;
                case EinvoiceType.Paper2Einvoice:
                    lab_Type.Text = Resources.Localization.Paper2Einvoice;
                    break;
                case EinvoiceType.Paper3Einvoice:
                    lab_Type.Text = Resources.Localization.Paper3Einvoice;
                    break;
                case EinvoiceType.Paper3EinvoiceMedia:
                    lab_Type.Text = Resources.Localization.Paper3EinvoiceMedia;
                    break;
                case EinvoiceType.EinvoiceJobMessage:
                    lab_Type.Text = Resources.Localization.EinvoiceJobMessage;
                    break;
            }
        }

        protected void GenerateMediaInvoice(object sender, EventArgs e)
        {
            DateTime d1, d2;
            if (DateTime.TryParse(tbx_TxtDateS.Text, out d1) && DateTime.TryParse(tbx_TxtDateE.Text, out d2))
            {
                string txt = EinvoiceFacade.GenerateInvoiceMedia(d1, d2);
                Context.Response.Clear();
                Context.Response.ContentEncoding = Encoding.GetEncoding(0);
                Context.Response.ContentType = "text/octet-stream";
                Context.Response.AddHeader("Content-Disposition", "attachment; filename=" + EinvoiceType.Paper3EinvoiceMedia + d1.ToString("-MMdd") + DateTime.Now.ToString("-yyyyMMdd-HHmmss") + ".txt");
                Response.BinaryWrite(Encoding.UTF8.GetBytes(txt));
                Response.End();
            }
        }
        protected void GenerateTxtInvoice(object sender, EventArgs e)
        {
            DateTime d1, d2;
            InvoiceMode2 mode2 = (InvoiceMode2)int.Parse(rbl_PaperInvoiceType.SelectedValue);
            InvoiceUserType userType = (InvoiceUserType)int.Parse(rbl_InvoiceUserType.SelectedValue);

            bool isCopy = ckb_EivCopy.Checked;
            bool hasWinning = ckb_EivWinning.Checked;
            if (DateTime.TryParse(tbx_TxtDateS.Text, out d1) && DateTime.TryParse(tbx_TxtDateE.Text, out d2))
            {
                string filename = mode2 + d1.ToString("-MMdd") + DateTime.Now.ToString("-yyyyMMdd-HHmmss") + ".zip";
                string createId = User.Identity.Name;
                Dictionary<string, string> txt = userType == InvoiceUserType.Customer ? EinvoiceFacade.GenerateInvoiceForPrint(d1, d2, mode2, isCopy,hasWinning, createId, filename, (InvoiceQueryOption)int.Parse(ddlInvoiceTimeOption.SelectedValue)) : EinvoiceFacade.GenerateVendorInvoiceForPrint(d1, d2, mode2, isCopy, createId, filename);

                byte[] zipFile = FileZipUtility.ZipFile(txt, ZipFileType.Xml);
                if (zipFile != null && txt.Count > 0)
                {
                    //非副本則將下載的正本存備份在server
                    if (!isCopy)
                    {
                        string filePath = @"C:\eInvoiceText\";
                        if (!Directory.Exists(filePath))
                        {
                            Directory.CreateDirectory(filePath);
                        }
                        using (var fs = new FileStream(filePath + filename, FileMode.Create, FileAccess.Write))
                        {
                            fs.Write(zipFile, 0, zipFile.Length);
                        }
                    }
                    
                    Context.Response.Clear();
                    Context.Response.ContentType = "application/zip";
                    Context.Response.AddHeader("Content-Disposition", "attachment; filename=" + filename);
                    Context.Response.BufferOutput = false;
                    Response.BinaryWrite(zipFile);
                    Response.End();
                    Response.Flush();
                }
            }
        }
        protected void ReEinvoiceUpload(object sender, EventArgs e)
        {
            DateTime now = DateTime.Now;
            string[] str_list = tbx_ReEinvoiceUpload.Text.Replace("\n", ",").Replace("\r", ",").Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
            
            List<int> list_id = new List<int>();
            foreach (var item in str_list)
            {
                int id;
                if (int.TryParse(item, out id))
                    list_id.Add(id);
            }
            EinvoiceFacade.GenerateUploadFilesById(list_id, now);
        }
        protected void VoidEinvoiceUpload(object sender, EventArgs e)
        {
            string[] str_list = tbx_EinvoiceVoidUpload.Text.Replace("\n", ",").Replace("\r", ",").Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
            List<int> list_id = new List<int>();
            foreach (var item in str_list)
            {
                int id;
                if (int.TryParse(item, out id))
                {
                    list_id.Add(id);
                }
            }
            EinvoiceFacade.GenerateVoidUploadFileById(list_id);
        }

        protected void CancelEinvoiceUpload(object sender, EventArgs e)
        {
            string[] str_list = tbx_EinvoiceCancelUpload.Text.Replace("\n", ",").Replace("\r", ",").Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
            List<int> list_id = new List<int>();
            foreach (var item in str_list)
            {
                int id;
                if (int.TryParse(item, out id))
                {
                    list_id.Add(id);
                }
            }
            EinvoiceFacade.GenerateEinvoiceUploadFileById(list_id, EinvoiceType.C0501);
        }

        protected void AllownanceEinvoiceUpload(object sender, EventArgs e)
        {
            string[] str_list = tbx_EinvoiceAllownanceUpload.Text.Replace("\n", ",").Replace("\r", ",").Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
            List<int> list_id = new List<int>();
            foreach (var item in str_list)
            {
                int id;
                if (int.TryParse(item, out id))
                {
                    list_id.Add(id);
                }
            }
            EinvoiceFacade.GenerateEinvoiceUploadFileById(list_id, EinvoiceType.D0401);
        }

        protected void ReEinvoiceUploadByDate(object sender, EventArgs e)
        {
            DateTime dstart, dend;
            DateTime now = DateTime.Now;
            //請款日期起迄
            if (DateTime.TryParse(tbx_ReEinvoiceUploadStart.Text, out dstart) && DateTime.TryParse(tbx_ReEinvoiceUploadEnd.Text, out dend))
            {
                if (ddl_InvoiceType.SelectedValue == ((int)EinvoiceType.C0401).ToString())
                {
                    EinvoiceFacade.ReGenerateUploadFilesByDate(dstart, dend);
                }
            }
        }

        protected void EinvoiceProcess(object sender, EventArgs e)
        {
            DateTime dstart, dend, dindex;
            DateTime now = DateTime.Now;
            //請款日期起迄
            if (DateTime.TryParse(tbx_SatrtDay.Text, out dstart) && DateTime.TryParse(tbx_EndDay.Text, out dend))
            {
                if (ddl_InvoiceType.SelectedValue == ((int)EinvoiceType.C0401).ToString())
                {
                    if (DateTime.TryParse(tbx_IndexDay.Text, out dindex))
                    {
                        lbl_ProcessMessage.Text = EinvoiceFacade.GenerateEinvoiceNumber(dstart, dend, dindex);
                        EinvoiceFacade.GenerateUploadFiles(dstart, dend, dindex);
                        if (cp.EnbaledTaishinEcPayApi)
                        {
                            EinvoiceFacade.GeneratePartnerEinvoiceNumber(dstart, dend, dindex);
                        }
                    }
                    else
                    {
                        lbl_ProcessMessage.Text = EinvoiceFacade.GenerateEinvoiceNumber(dstart, dend, null);
                        EinvoiceFacade.GenerateUploadFiles(dstart, dend, now);
                        if (cp.EnbaledTaishinEcPayApi)
                        {
                            EinvoiceFacade.GeneratePartnerEinvoiceNumber(dstart, dend, now);
                        }
                    }
                }
                else
                {
                    lbl_ProcessMessage.Text = EinvoiceFacade.AddEinvoiceAllowance(dstart, dend);
                    if (DateTime.TryParse(tbx_IndexDay.Text, out dindex))
                    {
                        EinvoiceFacade.GenerateAllowanceUploadFiles(dstart, dend, dindex);
                        EinvoiceFacade.GenerateCancelUploadFiles(dstart, dend, dindex);
                    }
                    else
                    {
                        EinvoiceFacade.GenerateAllowanceUploadFiles(dstart, dend, now);
                        EinvoiceFacade.GenerateCancelUploadFiles(dstart, dend, now);
                    }
                }
            }
        }

        protected void GenerateEinvoicePaperByNumber(object sender, EventArgs e)
        {
            string[] str_list = tbx_EinvoiceNumForPaper.Text.Replace("\n", ",").Replace("\r", ",").Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
            bool isCopy = ckb_EivCopy2.Checked;
            if (str_list.Length > 0)
            {
                string txt = EinvoiceFacade.GenerateInvoiceForPrintByNumber(str_list, true, User.Identity.Name, isCopy);
                Context.Response.Clear();
                Context.Response.ContentEncoding = Encoding.GetEncoding(0);
                Context.Response.ContentType = "text/octet-stream";
                Context.Response.AddHeader("Content-Disposition", "attachment; filename=RePeperEinvoice.xml");
                Response.BinaryWrite(Encoding.UTF8.GetBytes(txt));
                Response.End();
            }
        }

        protected void WithdrawInvoicePaper(object sender, EventArgs e)
        {
            string[] str_list = tbWithdrawNumber.Text.Replace("\n", ",").Replace("\r", ",").Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
            if (str_list.Length > 0)
            {
                lbWithdrawMessage.Text = EinvoiceFacade.WithdrawInvoicePaperByNumber(str_list, User.Identity.Name);
            }
        }

        protected void CancelEinvoice(object sender, EventArgs e)
        {
            
        }
        protected void EinvoiceMailSend(object sender, EventArgs e)
        {
            lbl_MailCount.Text = string.Empty;
            DateTime dstart, dend;
            DateTime now = DateTime.Now;
            EinvoiceMailType type;
            if (rbl_EinvoiceMailType.SelectedValue == "1")
                type = EinvoiceMailType.EinvoiceInfoMail;
            else
                type = EinvoiceMailType.EvincoeWinnerMail;
            //請款日期起迄
            if (DateTime.TryParse((tbx_MailStartDay.Text + " " + ddl_MailHourStart.SelectedValue + ":" + ddl_MailMiunteStart.SelectedValue + ":00"), out dstart) && DateTime.TryParse((tbx_MailEndDay.Text + " " + ddl_MailHourEnd.SelectedValue + ":" + ddl_MailMiunteEnd.SelectedValue + ":00"), out dend))
            {
                string oldVersion = EinvoiceFacade.SendEinvoiceWinnerEmail(dstart, dend, cbx_MailToFtp.Checked, type, cbx_MailHasConfirmed.Checked, InvoiceVersion.Old);
                string carrierVersion = EinvoiceFacade.SendEinvoiceWinnerEmail(dstart, dend, cbx_MailToFtp.Checked, type, cbx_MailHasConfirmed.Checked, InvoiceVersion.CarrierEra);
                lbl_MailCount.Text = string.Format("舊制電子發票- {0}；新制發票載具- {1}", oldVersion, carrierVersion);
            }
        }
        protected void SetUpWinningNumber(object sender, EventArgs e)
        {
            List<int> superspecial_list = new List<int>();
            List<int> special_list = new List<int>();
            List<int> first_list = new List<int>();
            List<int> additional_list = new List<int>();
            int number;
            if (!string.IsNullOrEmpty(tbx_WinningNumber_superspecial_1.Text) && int.TryParse(tbx_WinningNumber_superspecial_1.Text, out number))
                superspecial_list.Add(number);
            if (!string.IsNullOrEmpty(tbx_WinningNumber_superspecial_2.Text) && int.TryParse(tbx_WinningNumber_superspecial_2.Text, out number))
                superspecial_list.Add(number);
            if (!string.IsNullOrEmpty(tbx_WinningNumber_special_1.Text) && int.TryParse(tbx_WinningNumber_special_1.Text, out number))
                special_list.Add(number);
            if (!string.IsNullOrEmpty(tbx_WinningNumber_special_2.Text) && int.TryParse(tbx_WinningNumber_special_2.Text, out number))
                special_list.Add(number);
            if (!string.IsNullOrEmpty(tbx_WinningNumber_first_1.Text) && int.TryParse(tbx_WinningNumber_first_1.Text, out number))
                first_list.Add(number);
            if (!string.IsNullOrEmpty(tbx_WinningNumber_first_2.Text) && int.TryParse(tbx_WinningNumber_first_2.Text, out number))
                first_list.Add(number);
            if (!string.IsNullOrEmpty(tbx_WinningNumber_first_3.Text) && int.TryParse(tbx_WinningNumber_first_3.Text, out number))
                first_list.Add(number);
            if (!string.IsNullOrEmpty(tbx_WinningNumber_first_4.Text) && int.TryParse(tbx_WinningNumber_first_4.Text, out number))
                first_list.Add(number);
            if (!string.IsNullOrEmpty(tbx_WinningNumber_first_5.Text) && int.TryParse(tbx_WinningNumber_first_5.Text, out number))
                first_list.Add(number);
            if (!string.IsNullOrEmpty(tbx_WinningNumber_additional_1.Text) && int.TryParse(tbx_WinningNumber_additional_1.Text, out number))
                additional_list.Add(number);
            if (!string.IsNullOrEmpty(tbx_WinningNumber_additional_2.Text) && int.TryParse(tbx_WinningNumber_additional_2.Text, out number))
                additional_list.Add(number);
            if (!string.IsNullOrEmpty(tbx_WinningNumber_additional_3.Text) && int.TryParse(tbx_WinningNumber_additional_3.Text, out number))
                additional_list.Add(number);
            int year, month;
            if (int.TryParse(ddl_invoiceYear3.SelectedValue, out year) && int.TryParse(ddl_invoiceMonth3.SelectedValue, out month))
            {
                DateTime d_start = new DateTime(year, month, 1);
                DateTime d_end = d_start.AddMonths(2);

                //特別獎 8碼號碼相同
                foreach (var item in superspecial_list)
                {
                    EinvoiceFacade.SetEinvoiceWinningStatus(d_start, d_end, item.ToString().PadLeft(8, '0'));
                }
                //特獎 8碼號碼相同
                foreach (var item in special_list)
                {
                    EinvoiceFacade.SetEinvoiceWinningStatus(d_start, d_end, item.ToString().PadLeft(8, '0'));
                }
                //頭獎
                foreach (var item in first_list)
                {
                    //8碼
                    EinvoiceFacade.SetEinvoiceWinningStatus(d_start, d_end, item.ToString().PadLeft(8, '0'));
                    //末7碼
                    EinvoiceFacade.SetEinvoiceWinningStatus(d_start, d_end, item.ToString().PadLeft(8, '0').Substring(1, 7));
                    //末6碼
                    EinvoiceFacade.SetEinvoiceWinningStatus(d_start, d_end, item.ToString().PadLeft(8, '0').Substring(2, 6));
                    //末5碼
                    EinvoiceFacade.SetEinvoiceWinningStatus(d_start, d_end, item.ToString().PadLeft(8, '0').Substring(3, 5));
                    //末4碼
                    EinvoiceFacade.SetEinvoiceWinningStatus(d_start, d_end, item.ToString().PadLeft(8, '0').Substring(4, 4));
                    //末3碼
                    EinvoiceFacade.SetEinvoiceWinningStatus(d_start, d_end, item.ToString().PadLeft(8, '0').Substring(5, 3));
                }
                //增開六獎  末3碼
                foreach (var item in additional_list)
                {
                    EinvoiceFacade.SetEinvoiceWinningStatus(d_start, d_end, item.ToString().PadLeft(3, '0'));
                }
                hyp_WinningList.Visible = true;
            }
        }

        protected void Import_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(InvoiceWinnerListUpload.FileName))
            {
                ClientScript.RegisterStartupScript(GetType(), "lockedOut", "alert('請選擇檔案！');", true);
            }
            else
            {
                if (null != InvoiceWinnerListUpload.PostedFile && InvoiceWinnerListUpload.PostedFile.ContentLength > 0)
                {
                    HSSFWorkbook hssfworkbook = new HSSFWorkbook(InvoiceWinnerListUpload.PostedFile.InputStream);
                    Sheet currentSheet = hssfworkbook.GetSheetAt(0);
                    Row row;
                    EinvoiceMainCollection emc = new EinvoiceMainCollection();
                    DateTime now = DateTime.Now;
                    Dictionary<string, DateTime> success = new Dictionary<string, DateTime>();
                    for (int k = 0; k < currentSheet.PhysicalNumberOfRows; k++)
                    {
                        row = currentSheet.GetRow(k);
                        if (null != row.GetCell(0) && row.GetCell(0).ToString().Trim() != "" && row.GetCell(0).ToString().Length == 10)
                        {
                            EinvoiceMain em = op.EinvoiceMainGet(row.GetCell(0).ToString().Trim());
                            if (null != em && em.IsLoaded && !em.InvoiceWinning)
                            {
                                EinvoiceWinner ew = mp.EinvoiceWinnerGetByUserid(em.UserId);
                                em.InvoiceWinning = true;
                                if (ew != null)
                                {
                                    if ( !string.IsNullOrEmpty(ew.UserName)&& !string.IsNullOrEmpty(ew.Adress))
                                    {
                                        em.InvoiceBuyerName = ew.UserName;
                                        em.InvoiceBuyerAddress = ew.Adress;
                                        em.InvoiceWinnerresponseTime = DateTime.Now;
                                        em.Message = "系統帶入會員中獎管理" + DateTime.Now.ToString("MMddHHmm");
                                    }       
                                }
                                if (!emc.Any(x => x == em))
                                {
                                    emc.Add(em);
                                }
                                success.Add(em.InvoiceNumber, em.InvoiceNumberTime.Value);
                            }
                            else
                            {
                                PartnerEinvoiceMain pem = op.PartnerEinvoiceMainGet(row.GetCell(0).ToString().Trim());
                                if (pem != null && pem.IsLoaded && !pem.InvoiceWinning)
                                {
                                    pem.InvoiceWinning = true;
                                    op.PartnerEinvoiceMainSet(pem);
                                    success.Add(pem.InvoiceNumber, pem.InvoiceNumberTime.Value);
                                }
                            }
                        }
                    }
                    EinvoiceFacade.SetEinvoiceMainCollectionWithLog(emc, User.Identity.Name, now);

                    string result = string.Format(@"成功匯入{0}筆，有{1}筆失敗，總共{2}筆，", success.Count.ToString().Trim(), Convert.ToString(currentSheet.PhysicalNumberOfRows - success.Count), Convert.ToString(currentSheet.PhysicalNumberOfRows));

                    if (success.Count > 0)
                    {
                        var dateCode = op.EinvoiceSerialCollectionGetByInvInfo(success.FirstOrDefault().Key, success.FirstOrDefault().Value).DateCode;
                        var esc = op.EinvoiceSerialCollectionGetByDate(dateCode);
                        if (esc.Where(x => x.WinnerMailSendStatus == (int)WinnerMailSendStatus.Initial).Count() > 0)
                        {
                            esc.Where(x => x.WinnerMailSendStatus == (int)WinnerMailSendStatus.Initial).ForEach(x => {
                                x.WinnerMailSendStatus = (int)WinnerMailSendStatus.Ready;
                                op.EinvoiceSetSerial(x);
                            });
                            result += string.Format("{0}-{1}月中獎通知信已進入排程。", esc.Min(x => x.StartDate).ToString("MM"), esc.Max(x => x.StartDate).ToString("MM"));
                        }
                        else
                        {
                            result += "中獎通知排程未啟用或已在排程上，詳請洽技術。";
                        }

                        EinvoiceFacade.SetJobMessage(dateCode, result, (int)EinvoiceType.Winning, User.Identity.Name);
                    }

                    ClientScript.RegisterStartupScript(GetType(), "lockedOut", "alert('" + result + "');", true);

                }
            }
        }

        protected void btn_VendorSerialEdit_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            var row = (GridViewRow)btn.NamingContainer;
            if (btn.CommandName == "EditRow")
            {
                btn.Text = "更新";
                btn.CommandName = "UpdateRow";
                ((TextBox)row.FindControl("tb_VendorMinSerial")).Enabled = true;
            }
            else if (btn.CommandName == "UpdateRow")
            {
                btn.Text = "編輯";
                btn.CommandName = "EditRow";
                ((TextBox)row.FindControl("tb_VendorMinSerial")).Enabled = false;
            }
        }
    }
}
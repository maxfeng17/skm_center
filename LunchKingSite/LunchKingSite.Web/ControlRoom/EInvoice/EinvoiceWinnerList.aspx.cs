﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.Core.Component;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using System.Data;
using System.Text;
using System.IO;
using System.Text.RegularExpressions;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core.UI;
using LunchKingSite.WebLib.Views;
using LunchKingSite.WebLib.Presenters;
namespace LunchKingSite.Web.ControlRoom.EInvoice
{
    public partial class EinvoiceWinnerList : RolePage, IEinvoiceWinnerList
    {
        public event EventHandler<DataEventArgs<int>> PageChange = null;
        public event EventHandler GetWinnerList = null;
        public event EventHandler<DataEventArgs<string>> GetEinvoice = null;
        public event EventHandler<DataEventArgs<KeyValuePair<string, KeyValuePair<string, string>>>> ChangeInfo = null;
        public event EventHandler<DataEventArgs<string>> GeneratePrint = null;
        public event EventHandler<DataEventArgs<string>> ExportEinvoice = null;
        #region property
        private EinvoiceWinnerListPresenter _presenter;
        public EinvoiceWinnerListPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }
        public DateTime InvoiceWinnerDate
        {
            get
            {
                int year, month;
                if (int.TryParse(ddl_invoiceYear2.SelectedValue, out year) && int.TryParse(ddl_invoiceMonth2.SelectedValue, out month))
                {
                    return new DateTime(year, month, 1);
                }
                else
                    return new DateTime();
            }
        }
        public string Filter
        {
            get
            {
                string result = string.Empty;
                switch (rbl_invoiceMode.SelectedValue)
                {
                    case "17":
                        result = " and " + ViewEinvoiceWinner.Columns.InvoiceMode2 + " = " + (int)InvoiceMode2.Duplicate + " and " + ViewEinvoiceWinner.Columns.InvoicePaperedTime + " is not null ";
                        break;
                    case "18":
                        result = " and " + ViewEinvoiceWinner.Columns.InvoiceMode2 + " = " + (int)InvoiceMode2.Triplicate + " and " + ViewEinvoiceWinner.Columns.InvoicePaperedTime + " is not null ";
                        break;
                    case "1":
                        result = " and " + ViewEinvoiceWinner.Columns.LoveCode + " = 23981";
                        break;
                    case "2":
                        result = " and " + ViewEinvoiceWinner.Columns.LoveCode + " = 919";
                        break;
                    case "0":
                        //result = " in (1,2,17,18,21) ";
                        break;
                    default:
                    case "21":
                        result = " and " + ViewEinvoiceWinner.Columns.InvoiceMode2 + " = " + (int)InvoiceMode2.Duplicate + " and " + ViewEinvoiceWinner.Columns.CarrierType + " = " + (int)CarrierType.Member;
                        break;
                    case "22":
                        result = " and " + ViewEinvoiceWinner.Columns.InvoiceMode2 + " = " + (int)InvoiceMode2.Duplicate + " and " + ViewEinvoiceWinner.Columns.InvoiceWinnerresponseTime + " is not null and " + ViewEinvoiceWinner.Columns.CarrierType + " = " + (int)CarrierType.Member;
                        break;
                    case "23":
                        result = " and " + ViewEinvoiceWinner.Columns.InvoiceMode2 + " = " + (int)InvoiceMode2.Duplicate + " and " + ViewEinvoiceWinner.Columns.InvoiceWinnerresponseTime + " is null and " + ViewEinvoiceWinner.Columns.CarrierType + " = " + (int)CarrierType.Member;
                        break;
                }
                return result;
            }
        }
        public int PageCount
        {
            get
            {
                int pagecount;
                return int.TryParse((ViewState["pagecount"] == null ? string.Empty : ViewState["pagecount"].ToString()), out pagecount) ? pagecount : 0;
            }
            set
            {
                lab_Count.Text = value.ToString();
                ViewState["pagecount"] = value;
            }
        }
        public bool? IsSingleSearch
        {
            get
            {
                return (bool?)ViewState["isSingleSearch"];
            }
            set
            {
                ViewState["isSingleSearch"] = value;
            }
        }
        #endregion
        #region mothod
        public void GetEinvoiceWinnerList(ViewEinvoiceWinnerCollection data)
        {            
            gv_EinvoiceWinners.DataSource = data;
            gv_EinvoiceWinners.DataBind();
            btn_Export.Visible = (data != null && data.Count > 0); 
        }
        public void SetUpPageCount()
        {
            gridPager.ResolvePagerView(PageCount > 0 ? 1 : 0, true);
        }
        public void ResponsePrintInfo(string printinfo)
        {
            Context.Response.Clear();
            Context.Response.ContentEncoding = Encoding.GetEncoding(0);
            Context.Response.ContentType = "text/octet-stream";
            Context.Response.AddHeader("Content-Disposition", "attachment; filename=einvoicewinnerPrint" + DateTime.Now.ToString("-yyyyMMdd-HHmmss") + ".txt");
            Response.BinaryWrite(Encoding.UTF8.GetBytes(printinfo));
            Response.End();
        }
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                _presenter.OnViewInitialized();
                PageInit();
            }
            _presenter.OnViewLoaded();
        }
        protected void PageInit()
        {
            int imonth = DateTime.Now.Month;
            List<ListItem> year = new List<ListItem>();
            for (int i = DateTime.Now.Year - 3; i <= DateTime.Now.Year; i++)
            {
                year.Add(new ListItem(i.ToString(), i.ToString()));
            }
            ddl_invoiceYear2.DataSource = year; 
            List<ListItem> month = new List<ListItem>();
            for (int i = 1; i < 13; i = i + 2)
            {
                month.Add(new ListItem(i.ToString().PadLeft(2, '0') + "~" + (i + 1).ToString().PadLeft(2, '0'), i.ToString().PadLeft(2, '0')));
            }
            ddl_invoiceMonth2.DataSource = month;
            ddl_invoiceMonth2.DataTextField = "Text";
            ddl_invoiceMonth2.DataValueField = "Value";
            ddl_invoiceMonth2.SelectedValue = (imonth % 2) == 0 ? Math.Abs((DateTime.Now.Month - 3)).ToString().PadLeft(2, '0') : Math.Abs((DateTime.Now.Month - 2)).ToString().PadLeft(2, '0');
            ddl_invoiceYear2.SelectedValue = DateTime.Now.Year.ToString();
            ddl_invoiceYear2.DataBind();
            ddl_invoiceMonth2.DataBind();
            btn_Export.Visible = false;
        }
        protected void GetEinvoiceWinners(object sender, EventArgs e)
        {
            pan_List.Visible = true;
            pan_Edit.Visible = false;
            if (GetWinnerList != null)
                this.GetWinnerList(sender, e);
        }
        protected void GetSingleEinvoice(object sender, EventArgs e)
        {
            pan_List.Visible = true;
            pan_Edit.Visible = false;
            if (GetEinvoice != null)
                this.GetEinvoice(sender, new DataEventArgs<string>(tbx_InvoiceNum.Text));
        }
        protected void ExportEinvoiceList(object sender, EventArgs e)
        {
            pan_List.Visible = true;
            pan_Edit.Visible = false;

            if (ExportEinvoice != null)
                this.ExportEinvoice(sender, new DataEventArgs<string>(tbx_InvoiceNum.Text));
        }

        protected void EditInfo(object sender, GridViewCommandEventArgs e)
        {
            lab_pInvoiceNumber.Text = lab_pInvoiceBuyerAddress.Text = lab_pInvoiceBuyerName.Text =
                tbx_pInvoiceBuyerAddress.Text = tbx_pInvoiceBuyerName.Text = string.Empty;
            if (e.CommandName == "EditInfo")
            {
                LinkButton lbt = (LinkButton)(e.CommandSource);
                GridViewRow row = ((GridViewRow)(lbt.NamingContainer));
                lab_pInvoiceNumber.Text = e.CommandArgument.ToString();
                tbx_pInvoiceBuyerAddress.Text = lab_pInvoiceBuyerAddress.Text = ((Label)(row.FindControl("lab_gInvoiceBuyerAddress"))).Text;
                tbx_pInvoiceBuyerName.Text = lab_pInvoiceBuyerName.Text = ((Label)(row.FindControl("lab_gInvoiceBuyerName"))).Text;
                pan_List.Visible = false;
                pan_Edit.Visible = true;
            }
            if (e.CommandName == "PrintInfo" && User.IsInRole("Financial"))
            {
                if (GeneratePrint != null)
                    this.GeneratePrint(sender, new DataEventArgs<string>(e.CommandArgument.ToString()));
            }
        }
        protected void ConfirmChange(object sender, EventArgs e)
        {
            pan_List.Visible = true;
            pan_Edit.Visible = false;
            if (ChangeInfo != null)
                this.ChangeInfo(sender, new DataEventArgs<KeyValuePair<string, KeyValuePair<string, string>>>(new KeyValuePair<string, KeyValuePair<string, string>>(lab_pInvoiceNumber.Text, new KeyValuePair<string, string>(tbx_pInvoiceBuyerName.Text, tbx_pInvoiceBuyerAddress.Text))));
        }
        protected void CancelChange(object sender, EventArgs e)
        {
            pan_List.Visible = true;
            pan_Edit.Visible = false;
        }
        protected int RetrieveOrderCount()
        {
            return PageCount;
        }
        protected void UpdateHandler(int pageNumber)
        {
            if (this.PageChange != null)
                this.PageChange(this, new DataEventArgs<int>(pageNumber));
        }

        public string GetRefundInfo(string invNum) 
        {
            CashTrustLog trust = EinvoiceFacade.CashTrustLogGetByInvoiceNumber(invNum);
            if (trust.IsLoaded)
            {
                if (((TrustStatus)trust.Status).Equals(TrustStatus.Refunded)) 
                {
                    return I18N.Phrase.TrustStatus_Returned;
                }            
            }
            return string.Empty;
        }
    }
}
﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="../backend.master"
    CodeBehind="EinvoiceWinnerList.aspx.cs" Inherits="LunchKingSite.Web.ControlRoom.EInvoice.EinvoiceWinnerList" %>

<%@ Register Src="../Controls/Pager.ascx" TagName="Pager" TagPrefix="uc1" %>
<%@ Import Namespace="LunchKingSite.DataOrm" %>
<%@ Import Namespace="LunchKingSite.Core" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table style="width: 1600">
        <tr>
            <td style="text-align: center; font-size: large; font-weight: bolder; background-color: #F0AF13;
                color: #688E45;" colspan="3">
                統一發票中獎報表
            </td>
        </tr>
        <tr>
            <td style="background-color: #688E45; font-weight: bolder; color: White;">
                發票年月、種類
            </td>
            <td style="background-color: #688E45; font-weight: bolder; color: White;">
                <asp:Button ID="btn_EinvoiceSearch" runat="server" Text="查詢" OnClick="GetEinvoiceWinners" />
            </td>
            <td style="background-color: #B3D5F0">
                <asp:DropDownList ID="ddl_invoiceYear2" runat="server">
                </asp:DropDownList>
                <asp:DropDownList ID="ddl_invoiceMonth2" runat="server">
                </asp:DropDownList>
                <asp:RadioButtonList ID="rbl_invoiceMode" runat="server" RepeatColumns="3" RepeatDirection="Horizontal">
                    <asp:ListItem Text="電子二聯(全部)" Value="21" Selected="True"></asp:ListItem>
                    <asp:ListItem Text="電子二聯(已確認)" Value="22"></asp:ListItem>
                    <asp:ListItem Text="電子二聯(未確認)" Value="23"></asp:ListItem>
                    <asp:ListItem Text="紙本二聯" Value="17"></asp:ListItem>
                    <asp:ListItem Text="紙本三聯" Value="18"></asp:ListItem>
                    <asp:ListItem Text="空手道" Value="1"></asp:ListItem>
                    <asp:ListItem Text="創世" Value="2"></asp:ListItem>
                    <asp:ListItem Text="全部" Value="0"></asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td style="background-color: #688E45; font-weight: bolder; color: White;">
                搜尋中獎發票
            </td>
            <td style="background-color: #688E45; font-weight: bolder; color: White;">
                <asp:Button ID="btn_EinvoiceText" runat="server" Text="搜尋" OnClick="GetSingleEinvoice"
                    ValidationGroup="t" />
            </td>
            <td style="background-color: #B3D5F0">
                <asp:TextBox ID="tbx_InvoiceNum" runat="server" MaxLength="10"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="請填入發票號碼"
                    ControlToValidate="tbx_InvoiceNum" Display="Dynamic" ValidationGroup="t"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:Panel ID="pan_List" runat="server">
                    總筆數:<asp:Label ID="lab_Count" runat="server"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Button ID="btn_Export" runat="server" Text="匯出" Width="60" OnClick="ExportEinvoiceList" />
                    <asp:GridView ID="gv_EinvoiceWinners" runat="server" AutoGenerateColumns="false"
                        OnRowCommand="EditInfo">
                        <Columns>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <table style="width: 1600; background-color: White; font-size: small">
                                        <tr>
                                            <td style="background-color: #CDCDCD">
                                                序號
                                            </td>
                                            <td style="background-color: #CDCDCD">
                                                訂單編號
                                            </td>
                                            <td style="background-color: #CDCDCD">
                                                發票號碼
                                            </td>
                                            <td style="background-color: #CDCDCD">
                                                開立日期
                                            </td>
                                            <td style="background-color: #CDCDCD">
                                                訂購日期
                                            </td>
                                            <td style="background-color: #CDCDCD">
                                                發票種類
                                            </td>
                                            <td style="background-color: #CDCDCD">
                                                使用載具
                                            </td>
                                            <td style="background-color: #CDCDCD">
                                                印製紙本
                                            </td>
                                            <td style="background-color: #CDCDCD">
                                                退貨狀態
                                            </td>
                                            <td style="background-color: #CDCDCD">
                                                會員姓名
                                            </td>
                                            <td style="background-color: #CDCDCD">
                                                Email
                                            </td>
                                            <td style="background-color: #CDCDCD">
                                                會員電話
                                            </td>
                                            <td style="background-color: #CDCDCD">
                                                中獎人
                                            </td>
                                            <td style="background-color: #CDCDCD">
                                                連絡電話
                                            </td>
                                            <td style="background-color: #CDCDCD">
                                                發票地址
                                            </td>
                                            <td style="background-color: #CDCDCD">
                                                中獎確認時間
                                            </td>
                                        </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td>
                                            <%# (((ViewEinvoiceWinner)Container.DataItem).InvoicePaperedTime==null)?string.Empty:(Container.DataItemIndex + 1).ToString() %>
                                            <asp:LinkButton ID="lbt_Edit" runat="server" CommandArgument="<%# ((ViewEinvoiceWinner)Container.DataItem).InvoiceNumber%>"
                                                CommandName="EditInfo"> <%# (((ViewEinvoiceWinner)Container.DataItem).InvoicePaperedTime==null)?(Container.DataItemIndex + 1).ToString():string.Empty %></asp:LinkButton>
                                            <span style="display: none">
                                                <%# ((ViewEinvoiceWinner)Container.DataItem).InvoiceSumAmount.ToString("F0")%></span>
                                        </td>
                                        <td>
                                            <%# ((ViewEinvoiceWinner)Container.DataItem).OrderId%>
                                        </td>
                                        <td>
                                            <%# ((ViewEinvoiceWinner)Container.DataItem).InvoiceNumber%>
                                        </td>
                                        <td>
                                            <%# (((ViewEinvoiceWinner)Container.DataItem).InvoiceNumberTime==null)?string.Empty:((ViewEinvoiceWinner)Container.DataItem).InvoiceNumberTime.Value.ToString("yyyy/MM/dd")%>
                                        </td>
                                        <td>
                                            <%# ((ViewEinvoiceWinner)Container.DataItem).OrderTime.ToString("yyyy/MM/dd")%>
                                        </td>
                                        <td>
                                            <%# ((ViewEinvoiceWinner)Container.DataItem).InvoiceType%>
                                        </td>
                                        <td>
                                            <%# ((ViewEinvoiceWinner)Container.DataItem).CarrierTypeDescription%>
                                        </td>
                                        <td>
                                            <%# (((ViewEinvoiceWinner)Container.DataItem).InvoicePaperedTime==null)?string.Empty:((ViewEinvoiceWinner)Container.DataItem).InvoicePaperedTime.Value.ToString("yyyy/MM/dd")%>
                                        </td>
                                        <td>
                                            <%# GetRefundInfo(((ViewEinvoiceWinner)Container.DataItem).InvoiceNumber) %>
                                        </td>
                                        <td>
                                            <%# ((ViewEinvoiceWinner)Container.DataItem).Membername%>
                                        </td>
                                        <td>
                                            <%# ((ViewEinvoiceWinner)Container.DataItem).UserEmail%>
                                        </td>
                                        <td>
                                            <%# ((ViewEinvoiceWinner)Container.DataItem).Mobile%>
                                        </td>
                                        <td>
                                            <asp:Label ID="lab_gInvoiceBuyerName" runat="server" Text=" <%# ((ViewEinvoiceWinner)Container.DataItem).InvoiceBuyerName%>"></asp:Label>
                                        </td>
                                        <td>
                                            <%# ((ViewEinvoiceWinner)Container.DataItem).InvoiceWinnerresponsePhone%>
                                        </td>
                                        <td>
                                            <asp:Label ID="lab_gInvoiceBuyerAddress" runat="server" Text="<%# ((ViewEinvoiceWinner)Container.DataItem).InvoiceBuyerAddress%>"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:LinkButton ID="lbt_Print" runat="server" CommandArgument="<%# ((ViewEinvoiceWinner)Container.DataItem).InvoiceNumber%>"
                                                CommandName="PrintInfo" Enabled=" <%# (((ViewEinvoiceWinner)Container.DataItem).InvoiceWinnerresponseTime != null) && string.IsNullOrEmpty(((ViewEinvoiceWinner)Container.DataItem).Refund) && (((ViewEinvoiceWinner)Container.DataItem).InvoicePaperedTime == null)%>">
                                                <%# (((ViewEinvoiceWinner)Container.DataItem).InvoiceWinnerresponseTime==null)?string.Empty:((ViewEinvoiceWinner)Container.DataItem).InvoiceWinnerresponseTime.Value.ToString("yyyy/MM/dd HH:mm")%>
                                            </asp:LinkButton>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <AlternatingItemTemplate>
                                    <tr>
                                        <td style="background-color: #DBEDFA">
                                            <%# (((ViewEinvoiceWinner)Container.DataItem).InvoicePaperedTime==null)?string.Empty:(Container.DataItemIndex + 1).ToString() %>
                                            <asp:LinkButton ID="lbt_Edit" runat="server" CommandArgument="<%# ((ViewEinvoiceWinner)Container.DataItem).InvoiceNumber%>"
                                                CommandName="EditInfo"> <%# (((ViewEinvoiceWinner)Container.DataItem).InvoicePaperedTime==null)?(Container.DataItemIndex + 1).ToString():string.Empty %> </asp:LinkButton>
                                            <span style="display: none">
                                                <%# ((ViewEinvoiceWinner)Container.DataItem).InvoiceSumAmount.ToString("F0")%></span>
                                        </td>
                                        <td style="background-color: #DBEDFA">
                                            <%# ((ViewEinvoiceWinner)Container.DataItem).OrderId%>
                                        </td>
                                        <td style="background-color: #DBEDFA">
                                            <%# ((ViewEinvoiceWinner)Container.DataItem).InvoiceNumber%>
                                        </td>
                                        <td style="background-color: #DBEDFA">
                                            <%# (((ViewEinvoiceWinner)Container.DataItem).InvoiceNumberTime==null)?string.Empty:((ViewEinvoiceWinner)Container.DataItem).InvoiceNumberTime.Value.ToString("yyyy/MM/dd")%>
                                        </td>
                                        <td style="background-color: #DBEDFA">
                                            <%# ((ViewEinvoiceWinner)Container.DataItem).OrderTime.ToString("yyyy/MM/dd")%>
                                        </td>
                                        <td style="background-color: #DBEDFA">
                                            <%# ((ViewEinvoiceWinner)Container.DataItem).InvoiceType%>
                                        </td>
                                        <td style="background-color: #DBEDFA">
                                            <%# ((ViewEinvoiceWinner)Container.DataItem).CarrierTypeDescription%>
                                        </td>
                                        <td style="background-color: #DBEDFA">
                                            <%# (((ViewEinvoiceWinner)Container.DataItem).InvoicePaperedTime==null)?string.Empty:((ViewEinvoiceWinner)Container.DataItem).InvoicePaperedTime.Value.ToString("yyyy/MM/dd")%>
                                        </td>
                                        <td style="background-color: #DBEDFA">
                                            <%# GetRefundInfo(((ViewEinvoiceWinner)Container.DataItem).InvoiceNumber) %>
                                        </td>
                                        <td style="background-color: #DBEDFA">
                                            <%# ((ViewEinvoiceWinner)Container.DataItem).Membername%>
                                        </td>
                                        <td style="background-color: #DBEDFA">
                                            <%# ((ViewEinvoiceWinner)Container.DataItem).UserEmail%>
                                        </td>
                                        <td style="background-color: #DBEDFA">
                                            <%# ((ViewEinvoiceWinner)Container.DataItem).Mobile%>
                                        </td>
                                        <td style="background-color: #DBEDFA">
                                            <asp:Label ID="lab_gInvoiceBuyerName" runat="server" Text=" <%# ((ViewEinvoiceWinner)Container.DataItem).InvoiceBuyerName%>"></asp:Label>
                                        </td>
                                        <td style="background-color: #DBEDFA">
                                            <%# ((ViewEinvoiceWinner)Container.DataItem).InvoiceWinnerresponsePhone%>
                                        </td>
                                        <td style="background-color: #DBEDFA">
                                            <asp:Label ID="lab_gInvoiceBuyerAddress" runat="server" Text="<%# ((ViewEinvoiceWinner)Container.DataItem).InvoiceBuyerAddress%>"></asp:Label>
                                        </td>
                                        <td style="background-color: #DBEDFA">
                                            <asp:LinkButton ID="lbt_Print" runat="server" CommandArgument="<%# ((ViewEinvoiceWinner)Container.DataItem).InvoiceNumber%>"
                                                CommandName="PrintInfo" Enabled=" <%# (((ViewEinvoiceWinner)Container.DataItem).InvoiceWinnerresponseTime != null) && string.IsNullOrEmpty(((ViewEinvoiceWinner)Container.DataItem).Refund) && (((ViewEinvoiceWinner)Container.DataItem).InvoicePaperedTime == null)%>">
                                                <%# (((ViewEinvoiceWinner)Container.DataItem).InvoiceWinnerresponseTime==null)?string.Empty:((ViewEinvoiceWinner)Container.DataItem).InvoiceWinnerresponseTime.Value.ToString("yyyy/MM/dd HH:mm")%>
                                            </asp:LinkButton>
                                        </td>
                                    </tr>
                                </AlternatingItemTemplate>
                                <FooterTemplate>
                                    </table>
                                </FooterTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                    <uc1:Pager ID="gridPager" runat="server" PageSize="50" ongetcount="RetrieveOrderCount"
                        onupdate="UpdateHandler" />
                </asp:Panel>
                <asp:Panel ID="pan_Edit" runat="server" Visible="false">
                    <table style="background-color: White; font-size: small">
                        <tr>
                            <td style="background-color: #688E45" colspan="3">
                                發票修改
                            </td>
                        </tr>
                        <tr>
                            <td style="background-color: #CDCDCD">
                                發票號碼
                            </td>
                            <td colspan="2" style="background-color: #DBEDFA">
                                <asp:Label ID="lab_pInvoiceNumber" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="background-color: #CDCDCD">
                                中獎人
                            </td>
                            <td style="background-color: #DBEDFA">
                                <asp:Label ID="lab_pInvoiceBuyerName" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="tbx_pInvoiceBuyerName" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="background-color: #CDCDCD">
                                發票地址
                            </td>
                            <td style="background-color: #DBEDFA">
                                <asp:Label ID="lab_pInvoiceBuyerAddress" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="tbx_pInvoiceBuyerAddress" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" style="text-align: center">
                                <asp:Button ID="btn_Confirm" runat="server" Text="確認" OnClick="ConfirmChange" />&nbsp;&nbsp;&nbsp;&nbsp;
                                <asp:Button ID="btn_Cancel" runat="server" Text="取消" ForeColor="Red" OnClick="CancelChange" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
    </table>
</asp:Content>

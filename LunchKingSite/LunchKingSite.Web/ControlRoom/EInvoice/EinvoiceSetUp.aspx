﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="../backend.master"
    CodeBehind="EinvoiceSetUp.aspx.cs" Inherits="LunchKingSite.Web.ControlRoom.EInvoice.EinvoiceSetUp" %>

<%@ Import Namespace="LunchKingSite.DataOrm" %>
<%@ Import Namespace="LunchKingSite.Core" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h1 style="background-color: Aqua; color: Red">查詢</h1>
    <table style="width: 1200px">
        <tr>
            <td style="text-align: center; font-size: large; font-weight: bolder; background-color: #F0AF13; color: #688E45;"
                colspan="2">查詢字軌記錄
            </td>
        </tr>
        <tr>
            <td style="background-color: #688E45; font-weight: bolder; color: White; width: 20%;">發票年月
            </td>
            <td style="background-color: #B3D5F0">
                <asp:DropDownList ID="ddl_invoiceYear2" runat="server">
                </asp:DropDownList>
                <asp:DropDownList ID="ddl_invoiceMonth2" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td style="background-color: #688E45; font-weight: bolder; color: White" colspan="2">
                <asp:Button ID="btn_EinvoiceSerialDay" runat="server" Text="查詢字軌紀錄" OnClick="GetEinvoiceSerialCollection" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:GridView ID="gv_EinvoiceSerialCollection" runat="server" AutoGenerateColumns="false">
                    <Columns>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <table style="width: 1200px; background-color: White">
                                    <tr>
                                        <td style="background-color: #CDCDCD">發票年月
                                        </td>
                                        <td style="background-color: #CDCDCD">發票起號
                                        </td>
                                        <td style="background-color: #CDCDCD">發票迄號
                                        </td>
                                        <td style="background-color: #CDCDCD">已開立至
                                        </td>
                                        <td style="background-color: #CDCDCD">剩餘數
                                        </td>
                                        <td style="background-color: #CDCDCD">建檔者
                                        </td>
                                        <td style="background-color: #CDCDCD">建檔日
                                        </td>
                                    </tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td>
                                        <%# ((EinvoiceSerial)Container.DataItem).StartDate.ToString("yyyyMM")%>
                                    </td>
                                    <td>
                                        <%# ((EinvoiceSerial)Container.DataItem).HeadCode + ((EinvoiceSerial)Container.DataItem).MinSerial.ToString().PadLeft(8, '0') %>
                                    </td>
                                    <td>
                                        <%#  ((EinvoiceSerial)Container.DataItem).HeadCode+(((EinvoiceSerial)Container.DataItem).VendorMinSerial > 0 ? ((EinvoiceSerial)Container.DataItem).VendorMinSerial -1 : ((EinvoiceSerial)Container.DataItem).MaxSerial).ToString().PadLeft(8, '0')%>
                                    </td>
                                    <td>
                                        <%# ((EinvoiceSerial)Container.DataItem).HeadCode + ((EinvoiceSerial)Container.DataItem).NextSerial.ToString().PadLeft(8, '0')%>
                                    </td>
                                    <td>
                                        <%# (((EinvoiceSerial)Container.DataItem).VendorMinSerial ?? ((EinvoiceSerial)Container.DataItem).MaxSerial) - ((EinvoiceSerial)Container.DataItem).NextSerial%>
                                    </td>
                                    <td>
                                        <%# ((EinvoiceSerial)Container.DataItem).Creator%>
                                    </td>
                                    <td>
                                        <%# ((EinvoiceSerial)Container.DataItem).CreateTime.ToString("yyyyMMdd")%>
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <AlternatingItemTemplate>
                                <tr>
                                    <td style="background-color: #DBEDFA">
                                        <%# ((EinvoiceSerial)Container.DataItem).StartDate.ToString("yyyyMM")%>
                                    </td>
                                    <td style="background-color: #DBEDFA">
                                        <%# ((EinvoiceSerial)Container.DataItem).HeadCode + ((EinvoiceSerial)Container.DataItem).VendorMinSerial.ToString().PadLeft(8, '0') %>
                                    </td>
                                    <td style="background-color: #DBEDFA">
                                        <%#  ((EinvoiceSerial)Container.DataItem).HeadCode+((EinvoiceSerial)Container.DataItem).MaxSerial.ToString().PadLeft(8, '0')%>
                                    </td>
                                    <td style="background-color: #DBEDFA">
                                        <%# ((EinvoiceSerial)Container.DataItem).HeadCode + ((EinvoiceSerial)Container.DataItem).VendorNextSerial.ToString().PadLeft(8, '0')%>
                                    </td>
                                    <td style="background-color: #DBEDFA">
                                        <%# ((EinvoiceSerial)Container.DataItem).MaxSerial - ((EinvoiceSerial)Container.DataItem).VendorNextSerial + 1 %>
                                    </td>
                                    <td style="background-color: #DBEDFA">
                                        <%# ((EinvoiceSerial)Container.DataItem).Creator%>
                                    </td>
                                    <td style="background-color: #DBEDFA">
                                        <%# ((EinvoiceSerial)Container.DataItem).CreateTime.ToString("yyyyMMdd")%>
                                    </td>
                                </tr>
                            </AlternatingItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </td>
        </tr>
    </table>
    <table style="width: 1200px">
        <tr>
            <td colspan="2" style="text-align: center; font-size: large; font-weight: bolder; background-color: #F0AF13; color: #688E45;">匯出紙本發票檔
            </td>
        </tr>
        <tr>
            <td style="background-color: #688E45; font-weight: bolder; color: White; width: 20%;">
                <asp:DropDownList runat="server" ID="ddlInvoiceTimeOption" />
            </td>
            <td style="background-color: #B3D5F0">
                <asp:TextBox ID="tbx_TxtDateS" runat="server"></asp:TextBox>
                <cc1:CalendarExtender ID="ceTxtDateS" TargetControlID="tbx_TxtDateS" runat="server">
                </cc1:CalendarExtender>
                ~<asp:TextBox ID="tbx_TxtDateE" runat="server"></asp:TextBox>
                <cc1:CalendarExtender ID="ceTxtDateE" TargetControlID="tbx_TxtDateE" runat="server">
                </cc1:CalendarExtender><span style="color:blue;font-size:10pt">(資料不含迄日)</span>
            </td>
        </tr>
        <tr>
            <td style="background-color: #688E45; font-weight: bolder; color: White; width: 20%;">開立對象
            </td>
            <td style="background-color: #B3D5F0">
               <asp:RadioButtonList ID="rbl_InvoiceUserType" runat="server" RepeatDirection="Horizontal"
                    ForeColor="Black" >
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td style="background-color: #688E45; font-weight: bolder; color: White; width: 20%;">發票類型
            </td>
            <td style="background-color: #B3D5F0">
               <asp:RadioButtonList ID="rbl_PaperInvoiceType" runat="server" RepeatDirection="Horizontal"
                    ForeColor="Black" >
                </asp:RadioButtonList>
            </td>
        </tr>
         <tr>
            <td style="background-color: #688E45; font-weight: bolder; color: White; width: 20%;">匯出類型
            </td>
            <td style="background-color: #B3D5F0">
                <asp:CheckBox ID="ckb_EivCopy" runat="server" Text="副本"/>
                <asp:CheckBox ID="ckb_EivWinning" runat="server" Text="含中獎"/>
            </td>
        </tr>
        <tr>
            <td style="background-color: #688E45; font-weight: bolder; color: White" colspan="2">
                <asp:Button ID="btn_TxtInvoice" runat="server" Text="產生紙本發票檔" OnClick="GenerateTxtInvoice" />
                <asp:Button ID="btn_MediaInvoice" runat="server" Text="產生三聯電子媒體檔" OnClick="GenerateMediaInvoice"
                    Visible="false" />
            </td>
        </tr>
    </table>
    <table style="width: 1200px">
        <tr>
            <td colspan="2" style="text-align: center; font-size: large; font-weight: bolder; background-color: #F0AF13; color: #688E45;">重印發票
            </td>
        </tr>
        <tr>
            <td style="background-color: #688E45; font-weight: bolder; color: White; width: 20%;">發票號碼(逗號或斷行)
            </td>
            <td style="background-color: #B3D5F0">
                <asp:TextBox ID="tbx_EinvoiceNumForPaper" runat="server" TextMode="MultiLine" Rows="10"></asp:TextBox>
                <asp:CheckBox ID="ckb_EivCopy2" runat="server" Text="副本"/>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="background-color: #688E45; font-weight: bolder; color: White">
                <asp:Button ID="btn_GenerateEinvoicePaper" runat="server" Text="產生" OnClick="GenerateEinvoicePaperByNumber" />
            </td>
        </tr>
    </table>
    <table style="width: 1200px">
        <tr>
            <td colspan="2" style="text-align: center; font-size: large; font-weight: bolder; background-color: #F0AF13; color: #688E45;">發票退回未印狀態(抽出發票)<img title="此功能用以支援三聯式發票匯出之後，客人想要進行退貨，而導致發票雖抽出未寄回，但系統上仍需要點擊折讓已回的操作；財務確認發票抽出、不寄送之後，需要來此進行退回未印狀態。" src="../../Themes/PCweb/images/lmQuestion21x21.png" />
            </td>
        </tr>
        <tr>
            <td style="background-color: #688E45; font-weight: bolder; color: White; width: 20%;">發票退回未印製狀態(逗號或斷行)
            </td>
            <td style="background-color: #B3D5F0">
                <asp:TextBox ID="tbWithdrawNumber" runat="server" TextMode="MultiLine" Rows="10" ClientIDMode="Static"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="background-color: #688E45; font-weight: bolder; color: White">
                <asp:Button runat="server" ID="btnWithdrawInvoice" Text="已抽回" OnClick="WithdrawInvoicePaper" />
            </td>
        </tr>
        <tr>
            <td colspan="2" style="background-color: #B3D5F0">
                <asp:Label ID="lbWithdrawMessage" runat="server"></asp:Label>
            </td>
        </tr>
    </table>
    <h1 style="background-color: Aqua; color: Red">設定</h1>
    <table style="width: 1200px">
        <tr>
            <td colspan="2" style="text-align: center; font-size: large; font-weight: bolder; background-color: #F0AF13; color: #688E45;">發票字軌設定
            </td>
        </tr>
        <tr>
            <td style="width: 20%; background-color: #688E45; font-weight: bolder; color: White; width: 20%;">字軌編碼
            </td>
            <td style="background-color: #B3D5F0">
                <asp:TextBox ID="tbx_invoiceHead" runat="server" MaxLength="2" onKeyUp='this.value=this.value.toUpperCase();'></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfd_invoiceHead" runat="server" ErrorMessage="請填入兩碼字軌代號"
                    ControlToValidate="tbx_invoiceHead" ValidationGroup="a" Display="Dynamic"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="rev_invoiceHead" runat="server" ErrorMessage="兩碼英文字"
                    ControlToValidate="tbx_invoiceHead" ValidationGroup="a" ValidationExpression="[A-Z]{2}"
                    Display="Dynamic"></asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr>
            <td style="background-color: #688E45; font-weight: bolder; color: White">號碼起訖
            </td>
            <td style="background-color: #B3D5F0">
                <asp:TextBox ID="tbx_invoiceStart" runat="server" MaxLength="8"></asp:TextBox>~
                <asp:TextBox ID="tbx_invoiceEnd" runat="server" MaxLength="8"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfv_invoiceStart" runat="server" ErrorMessage="請輸入八碼起始數字"
                    ControlToValidate="tbx_invoiceStart" ValidationGroup="a" Display="Dynamic"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="rev_invoiceStart" runat="server" ErrorMessage="八碼數字"
                    ControlToValidate="tbx_invoiceStart" ValidationGroup="a" Display="Dynamic" ValidationExpression="[0-9]{8}"></asp:RegularExpressionValidator>
                <asp:RequiredFieldValidator ID="rfv_invoiceEnd" runat="server" ErrorMessage="請輸入八碼結束數字"
                    ControlToValidate="tbx_invoiceEnd" ValidationGroup="a" Display="Dynamic"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="rev_invoiceEnd" runat="server" ErrorMessage="八碼數字"
                    ControlToValidate="tbx_invoiceEnd" ValidationGroup="a" Display="Dynamic" ValidationExpression="[0-9]{8}"></asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr>
            <td style="background-color: #688E45; font-weight: bolder; color: White">發票年月
            </td>
            <td style="background-color: #B3D5F0">
                <asp:DropDownList ID="ddl_invoiceYear" runat="server">
                </asp:DropDownList>
                <asp:DropDownList ID="ddl_invoiceMonth" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td style="background-color: #688E45; font-weight: bolder; color: White">訊息
            </td>
            <td style="background-color: #B3D5F0">
                <asp:TextBox ID="tbx_invoiceMessage" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="background-color: #688E45; font-weight: bolder; color: White" colspan="2">
                <asp:Button ID="btn_Generate" runat="server" Text="產生" OnClick="GenerateInvoice"
                    ValidationGroup="a" />
            </td>
        </tr>
        <tr>
            <td colspan="2" style="width: 10%; background-color: #688E45; font-weight: bolder; color: Red">
                <asp:Label ID="lab_Message" runat="server"></asp:Label>
            </td>
        </tr>
    </table>
    <table style="width: 1200px">
        <tr>
            <td colspan="2" style="text-align: center; font-size: large; font-weight: bolder; background-color: #F0AF13; color: #688E45;">產生發票檔
            </td>
        </tr>
        <tr>
            <td style="background-color: #688E45; font-weight: bolder; color: White; width: 20%;">訂單交易起日
            </td>
            <td style="background-color: #B3D5F0">
                <asp:TextBox ID="tbx_SatrtDay" runat="server"></asp:TextBox>
                <cc1:CalendarExtender ID="ceES" TargetControlID="tbx_SatrtDay" runat="server">
                </cc1:CalendarExtender>
                <span style="color:red;font-size:10pt">(>= 輸入日期)</span>
            </td>
        </tr>
        <tr>
            <td style="background-color: #688E45; font-weight: bolder; color: White">訂單交易迄日
            </td>
            <td style="background-color: #B3D5F0">
                <asp:TextBox ID="tbx_EndDay" runat="server"></asp:TextBox>
                <cc1:CalendarExtender ID="ceEE" TargetControlID="tbx_EndDay" runat="server">
                </cc1:CalendarExtender>
                <span style="color:red;font-size:10pt">(< 輸入日期)</span>
            </td>
        </tr>
        <tr>
            <td style="background-color: #688E45; font-weight: bolder; color: White">指定發票日
            </td>
            <td style="background-color: #B3D5F0">
                <asp:TextBox ID="tbx_IndexDay" runat="server"></asp:TextBox>
                <cc1:CalendarExtender ID="ceEI" TargetControlID="tbx_IndexDay" runat="server">
                </cc1:CalendarExtender>
            </td>
        </tr>
        <tr>
            <td style="background-color: #688E45; font-weight: bolder; color: White">交易類別
            </td>
            <td style="background-color: #B3D5F0">
                <asp:DropDownList ID="ddl_InvoiceType" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td style="background-color: #688E45; font-weight: bolder; color: White">
                <asp:Button ID="btn_EinvoiceProcess" runat="server" Text="產生" OnClick="EinvoiceProcess" />
            </td>
            <td style="background-color: #688E45; font-weight: bolder; color: White; text-align: right"></td>
        </tr>
        <tr>
            <td style="background-color: #688E45; font-weight: bolder; color: White">補產電子發票上傳檔(輸入Detail ID)
            </td>
            <td style="background-color: #688E45; font-weight: bolder; color: White;">
                <asp:TextBox ID="tbx_ReEinvoiceUpload" runat="server" TextMode="MultiLine" Rows="2"></asp:TextBox>
                <asp:Button ID="btn_ReEinvoiceUpload" runat="server" Text="補產" OnClick="ReEinvoiceUpload" />
            </td>
        </tr>
        <tr>
           <td style="background-color: #688E45; font-weight: bolder; color: White">補產電子發票上傳檔(日期 EinvoiceDetail InvoiceType的Status要先更新為0)
            </td>
            <td style="background-color: #688E45; font-weight: bolder; color: White;">
                <asp:TextBox ID="tbx_ReEinvoiceUploadStart" runat="server"></asp:TextBox>
                <cc1:CalendarExtender ID="ce_ReEinvoiceUploadStart" TargetControlID="tbx_ReEinvoiceUploadStart" runat="server">
                </cc1:CalendarExtender>
                ~
                 <asp:TextBox ID="tbx_ReEinvoiceUploadEnd" runat="server"></asp:TextBox>
                <cc1:CalendarExtender ID="ce_ReEinvoiceUploadEnd" TargetControlID="tbx_ReEinvoiceUploadEnd" runat="server">
                </cc1:CalendarExtender>
                <asp:Button ID="btn_ReEinvoiceUploadByDate" runat="server" Text="補產" OnClick="ReEinvoiceUploadByDate" />
            </td>
        </tr>
        <tr>
            <td style="background-color: #688E45; font-weight: bolder; color: White">註銷電子發票上傳檔(輸入Detail ID)
            </td>
            <td style="background-color: #688E45; font-weight: bolder; color: White;">
                <asp:TextBox ID="tbx_EinvoiceVoidUpload" runat="server" TextMode="MultiLine" Rows="2"></asp:TextBox>
                <asp:Button ID="btn_EinvoiceVoidUpload" runat="server" Text="註銷" OnClick="VoidEinvoiceUpload" />
            </td>
        </tr>
        <tr>
            <td style="background-color: #688E45; font-weight: bolder; color: White">補產作廢電子發票上傳檔(輸入Detail ID)
            </td>
            <td style="background-color: #688E45; font-weight: bolder; color: White;">
                <asp:TextBox ID="tbx_EinvoiceCancelUpload" runat="server" TextMode="MultiLine" Rows="2"></asp:TextBox>
                <asp:Button ID="btn_EinvoiceCancelUpload" runat="server" Text="補產" OnClick="CancelEinvoiceUpload" />
            </td>
        </tr>
         <tr>
            <td style="background-color: #688E45; font-weight: bolder; color: White">補產折讓電子發票上傳檔(輸入Detail ID)
            </td>
            <td style="background-color: #688E45; font-weight: bolder; color: White;">
                <asp:TextBox ID="tbx_EinvoiceAllownanceUpload" runat="server" TextMode="MultiLine" Rows="2"></asp:TextBox>
                <asp:Button ID="btn_EinvoiceAllownanceUpload" runat="server" Text="補產" OnClick="AllownanceEinvoiceUpload" />
            </td>
        </tr>
        <tr>
            <td colspan="2" style="background-color: #B3D5F0">
                <asp:Label ID="lbl_ProcessMessage" runat="server"></asp:Label>
            </td>
        </tr>
    </table>
    <table style="width: 1200px">
        <tr>
            <td colspan="2" style="text-align: center; font-size: large; font-weight: bolder; background-color: #F0AF13; color: #688E45;">作廢、作廢折讓發票
            </td>
        </tr>
        <tr>
            <td style="background-color: #688E45; font-weight: bolder; color: White; width: 20%;">作廢種類
            </td>
            <td style="background-color: #B3D5F0">
                <asp:DropDownList ID="ddl_Cancel_Type" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td style="background-color: #688E45; font-weight: bolder; color: White">發票ID
            </td>
            <td style="background-color: #B3D5F0">
                <asp:TextBox ID="tbx_Cancel_EinvoiceNum" runat="server" TextMode="MultiLine" Rows="5"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="background-color: #688E45; font-weight: bolder; color: White" colspan="2">
                <asp:Button ID="btn_CancelInvoice" runat="server" Text="產生作廢發票" OnClick="CancelEinvoice" />
            </td>
        </tr>
    </table>
    <table style="width: 1200px">
        <tr>
            <td colspan="2" style="text-align: center; font-size: large; font-weight: bolder; background-color: #F0AF13; color: #688E45;">中獎發票設定
            </td>
        </tr>
        <tr>
            <td style="background-color: #688E45; font-weight: bolder; color: White; width: 20%;">中獎發票開立區間
            </td>
            <td style="background-color: #B3D5F0">
                <asp:DropDownList ID="ddl_invoiceYear3" runat="server">
                </asp:DropDownList>
                <asp:DropDownList ID="ddl_invoiceMonth3" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td style="background-color: #688E45; font-weight: bolder; color: White; width: 20%;">特別獎
            </td>
            <td style="background-color: #B3D5F0">
                <asp:TextBox ID="tbx_WinningNumber_superspecial_1" runat="server"></asp:TextBox>
                <asp:TextBox ID="tbx_WinningNumber_superspecial_2" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="background-color: #688E45; font-weight: bolder; color: White; width: 20%;">特獎
            </td>
            <td style="background-color: #B3D5F0">
                <asp:TextBox ID="tbx_WinningNumber_special_1" runat="server"></asp:TextBox>
                <asp:TextBox ID="tbx_WinningNumber_special_2" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="background-color: #688E45; font-weight: bolder; color: White; width: 20%;">頭獎
            </td>
            <td style="background-color: #B3D5F0">
                <asp:TextBox ID="tbx_WinningNumber_first_1" runat="server"></asp:TextBox>
                <asp:TextBox ID="tbx_WinningNumber_first_2" runat="server"></asp:TextBox>
                <asp:TextBox ID="tbx_WinningNumber_first_3" runat="server"></asp:TextBox>
                <asp:TextBox ID="tbx_WinningNumber_first_4" runat="server"></asp:TextBox>
                <asp:TextBox ID="tbx_WinningNumber_first_5" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="background-color: #688E45; font-weight: bolder; color: White; width: 20%;">增開六獎
            </td>
            <td style="background-color: #B3D5F0">
                <asp:TextBox ID="tbx_WinningNumber_additional_1" runat="server"></asp:TextBox>
                <asp:TextBox ID="tbx_WinningNumber_additional_2" runat="server"></asp:TextBox>
                <asp:TextBox ID="tbx_WinningNumber_additional_3" runat="server"></asp:TextBox>
            </td>
        </tr>
                <tr>
            <td style="background-color: #688E45; font-weight: bolder; color: White; width: 20%;">匯入中獎清冊
            </td>
            <td style="background-color: #B3D5F0">
                <asp:FileUpload ID="InvoiceWinnerListUpload" runat="server"></asp:FileUpload>
        <asp:Button ID="Import" runat="server" Text="Excel表匯入" OnClick="Import_Click" /><br />
        請確認檔案中只有發票號，並先另存為97~2000格式(.xls)再點選『Excel表匯入』進行匯入。
            </td>
        </tr>
        <tr>
            <td style="background-color: #688E45; font-weight: bolder; color: White" colspan="2">
                <asp:Button ID="btn_WinningNumber" runat="server" Text="確定" OnClick="SetUpWinningNumber" />
                <asp:HyperLink ID="hyp_WinningList" runat="server" NavigateUrl="~/ControlRoom/EInvoice/EinvoiceWinnerList.aspx"
                    Visible="false" Target="_self">中獎發票列表</asp:HyperLink>
            </td>
        </tr>
    </table>
    <table style="width: 1200px">
        <tr>
            <td colspan="2" style="text-align: center; font-size: large; font-weight: bolder; background-color: #F0AF13; color: #688E45;">電子發票通知信
            </td>
        </tr>
        <tr>
            <td style="background-color: #688E45; font-weight: bolder; color: White; width: 20%;">發票開立起日
            </td>
            <td style="background-color: #B3D5F0">
                <asp:TextBox ID="tbx_MailStartDay" runat="server"></asp:TextBox>
                <cc1:CalendarExtender ID="CalendarExtender1" TargetControlID="tbx_MailStartDay" runat="server">
                </cc1:CalendarExtender>
                <asp:DropDownList ID="ddl_MailHourStart" runat="server">
                </asp:DropDownList>
                <asp:DropDownList ID="ddl_MailMiunteStart" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td style="background-color: #688E45; font-weight: bolder; color: White">發票開立迄日
            </td>
            <td style="background-color: #B3D5F0">
                <asp:TextBox ID="tbx_MailEndDay" runat="server"></asp:TextBox>
                <cc1:CalendarExtender ID="CalendarExtender2" TargetControlID="tbx_MailEndDay" runat="server">
                </cc1:CalendarExtender>
                <asp:DropDownList ID="ddl_MailHourEnd" runat="server">
                </asp:DropDownList>
                <asp:DropDownList ID="ddl_MailMiunteEnd" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td style="background-color: #688E45; font-weight: bolder; color: White">
                <asp:RadioButtonList ID="rbl_EinvoiceMailType" runat="server" RepeatDirection="Horizontal"
                    ForeColor="White">
                    <asp:ListItem Text="開立" Value="1" Selected="True"></asp:ListItem>
                    <asp:ListItem Text="中獎" Value="2"></asp:ListItem>
                </asp:RadioButtonList>
            </td>
            <td style="background-color: #688E45; font-weight: bolder; color: White">
                <asp:CheckBox ID="cbx_MailHasConfirmed" runat="server" Text="未確認中獎發票" Checked="true" />
                <asp:CheckBox ID="cbx_MailToFtp" runat="server" Text="上傳FTP" Checked="true" />
                <asp:Button ID="btn_MailSend" runat="server" Text="產生" OnClick="EinvoiceMailSend" />
            </td>
        </tr>
        <tr>
            <td colspan="2" style="background-color: #B3D5F0">
                <asp:Label ID="lbl_MailCount" runat="server"></asp:Label>
            </td>
        </tr>
    </table>
</asp:Content>

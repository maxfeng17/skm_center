﻿using System;
using System.Text;
using System.Web.UI.WebControls;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using System.Collections.Generic;
using System.Web.UI;

namespace LunchKingSite.Web.ControlRoom.Controls
{
    public partial class PponDealTimeSlotItemForDay : UserControl
    {
        protected string DivId;

        private HashSet<Guid> _newDeals = new HashSet<Guid>();

        public void showData(ViewPponDealTimeSlotCollection DealCollection, string divId, IList<Guid> newDeals)
        {
            foreach (Guid newDealGuid in newDeals)
            {
                if (_newDeals.Contains(newDealGuid) == false)
                {
                    _newDeals.Add(newDealGuid);
                }
            }
            rList.DataSource = DealCollection;
            rList.DataBind();
            DivId = divId;
        }

        protected void rList_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            ViewPponDealTimeSlot data = (ViewPponDealTimeSlot)e.Item.DataItem;
            if (data != null)
            {
                Literal litName = (Literal)e.Item.FindControl("litName");
                litName.Text = data.ItemName.TrimToMaxLength(40);
            }
        }

        // 標記 visa、隱藏檔、昨日上檔、今日上檔
        protected string GetUnitColor(ViewPponDealTimeSlot vpdts)
        {
            StringBuilder sb = new StringBuilder();

            Guid bid = vpdts.BusinessHourGuid;

            IViewPponDeal vpd = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(bid, true);
            if (vpd.IsVisa(vpdts.EffectiveStart))
            {
                if (sb.Length > 0)
                {
                    sb.Append(' ');
                }
                sb.Append("unit-visa");
            }
            else if (vpd.IsVisa())
            {
                if (sb.Length > 0)
                {
                    sb.Append(' ');
                }
                sb.Append("unit-visa2");
            }

            if (vpdts.Status == (int)DealTimeSlotStatus.NotShowInPponDefault)
            {
                if (sb.Length > 0)
                {
                    sb.Append(' ');
                }
                sb.Append("unit-hidden");
            }

            var now = DateTime.Now;

            if (vpdts.EffectiveStart.Hour != 12)
            {
                if (sb.Length > 0)
                {
                    sb.Append(' ');
                }
                if (vpdts.EffectiveStart.Date < now.Date)
                {
                    sb.Append("unit-yesterday-new");
                }
                else if (vpdts.EffectiveStart.Date == now.Date) //上午的今日上檔
                {
                    sb.Append("unit-today-new");
                }
                else
                {
                    sb.Append("unit-tomorrow-new");
                }
            }
            else
            {
                if (_newDeals.Contains(bid)) //下午的今日上檔
                {
                    if (sb.Length > 0)
                    {
                        sb.Append(' ');
                    }
                    sb.Append("unit-today-new");
                }
            }
            
            return sb.ToString();
        }

        protected string GetDataId(ViewPponDealTimeSlot vpdts)
        {
            return string.Format("{0}|{1}|{2}", vpdts.BusinessHourGuid, vpdts.CityId, vpdts.EffectiveStart.ToString("yyyy/MM/dd HH:mm:ss"));
        }

        protected int GetSpanSeq(ViewPponDealTimeSlot vpdts)
        {
            return ((ViewPponDealTimeSlotCollection)rList.DataSource).IndexOf(vpdts) + 1;
        }
    }
}
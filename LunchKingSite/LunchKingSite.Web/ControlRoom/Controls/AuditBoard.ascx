<%@ Control Language="C#" AutoEventWireup="true" Codebehind="AuditBoard.ascx.cs"
    Inherits="LunchKingSite.Web.ControlRoom.Controls.AuditBoard" %>
<asp:Panel ID="pe" runat="server">
    <asp:Literal ID="l2" runat="server" Text="<%$Resources:Localization,AddNew%>" />
    <asp:TextBox ID="tN" runat="server" Columns="30" />&nbsp;&nbsp;
    <asp:Literal ID="l1" runat="server" Text="<%$Resources:Localization,VisibleToUser%>" />
    <asp:DropDownList ID="dV" runat="server">
        <asp:ListItem Text="<%$Resources:Localization,No%>" Value="false" />
        <asp:ListItem Text="<%$Resources:Localization,Yes%>" Value="true" />
    </asp:DropDownList>
    <asp:Button ID="bS" runat="server" Text="<%$Resources:Localization,Submit%>" OnClick="bS_Click" />
</asp:Panel>
<asp:GridView ID="gvm" runat="server" DataKeyNames="Id" CellPadding="4" ForeColor="#333333" GridLines="None"
    Width="100%" AutoGenerateColumns="false" OnRowEditing="gvm_RowEditing" OnRowCancelingEdit="gvm_RowCancelingEdit" OnRowUpdating="gvm_RowUpdating">
    <Columns>
        <asp:BoundField DataField="CreateTime" HeaderText="<%$ Resources:Localization,Date %>"
            ItemStyle-Width="15%" ReadOnly="true" DataFormatString="{0:yyyy/MM/dd HH:mm}" />
        <asp:BoundField DataField="Message" HeaderText="<%$ Resources:Localization,Message %>"
            ItemStyle-Width="70%" ItemStyle-CssClass="wrapper-text" ReadOnly="true" HtmlEncode="false" />
        <asp:BoundField DataField="CreateId" HeaderText="<%$ Resources:Localization,CreateId %>" ItemStyle-Width="7%" ReadOnly="true" />
        <asp:CheckBoxField DataField="CommonVisible" HeaderText="V" ItemStyle-Width="3%" />
        <asp:CommandField ShowEditButton="true" ShowDeleteButton="false" />
    </Columns>
    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
    <RowStyle BackColor="#EFF3FB" HorizontalAlign="Left" />
    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
    <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" HorizontalAlign="Left" />
    <EditRowStyle BackColor="#2461BF" />
    <AlternatingRowStyle BackColor="White" HorizontalAlign="Left" />
</asp:GridView>
﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SellerHiDealList.ascx.cs" Inherits="LunchKingSite.Web.ControlRoom.SellerHiDealList" %>
<asp:ListView ID="lvHiDealInfo" runat="server">
    <LayoutTemplate>
        <table>
            <thead>
                <tr>
                    <th>DID(檔號)</th>
                    <th>店家名稱</th>
                    <th>檔次設定</th>
                    <th>狀態</th>
                    <th>目前銷售</th>
                    <th>目前銷售金額</th>
                    <th>開始時間</th>
                    <th>截止時間</th>
                </tr>
            </thead>
            <tbody>
                <tr runat="server" id="itemPlaceHolder" />
            </tbody>
        </table>
    </LayoutTemplate>
    <ItemTemplate>
        <tr>
            <td style="text-align: right;"><%# Eval("DealId")%></td>
            <td style="text-align: center;"><%# Eval("SellerName")%></td>
            <td style="text-align: right;"><a href='../piinlife/setup.aspx?did=<%# Eval("DealId")%>'><%# Eval("DealName")%></a></td>
            <td style="text-align: right;"><%# ((bool)Eval("IsOpen")) ? "上檔" : "下檔"%></td>
            <td style="text-align: right;"><%# Eval("CurrentSoldItems")%></td>
            <td style="text-align: right;"><%# Eval("CurrentSoldAmount") == null ? "" : ((Decimal)Eval("CurrentSoldAmount")).ToString("$##,###,##0")  %></td>
            <td style="text-align: right;"><%# Eval("DealStartTime") == null ? "" : ((DateTime)Eval("DealStartTime")).ToString("yyyy/MM/dd hh:mm")%></td>
            <td style="text-align: right;"><%# Eval("DealEndTime") == null ? "" : ((DateTime)Eval("DealEndTime")).ToString("yyyy/MM/dd hh:mm")%></td>
        </tr>
    </ItemTemplate>        
</asp:ListView>
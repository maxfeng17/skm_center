﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EinvoiceControl.ascx.cs" 
    Inherits="LunchKingSite.Web.ControlRoom.Controls.EinvoiceControl" ClientIDMode="Static" %>
<%@ Import Namespace="LunchKingSite.Core" %>
<script type="text/javascript">
    function init() {
        var elements = document.getElementsByClassName('invoice-readonly');
        for (var i = 0; i < elements.length; i++) {
            var tagName = elements[i].tagName;
            if (tagName == 'SELECT') {
                var text = document.createElement('span');
                text.textContent = elements[i].options[elements[i].selectedIndex].text;
                elements[i].parentNode.appendChild(text);
            } else if (tagName == 'INPUT') {
                var text = document.createElement('span');
                text.textContent = elements[i].value;
                elements[i].parentNode.appendChild(text);
            }
            elements[i].style.display = 'none'; // Hide all elements.
        }
    }

    $(document).ready(function () {
        $('#ddlEinvoiceMode').live('change', function () {
            ResetInvoiceInfoError();
            if ($('#ddlEinvoiceMode').val() == '2') {
                $('#ddlCarrierType').removeAttr('disabled');
                if ($('#ddlCarrierType').val() != '0') {
                    $('#txtCarrierId').removeAttr('disabled');
                }
                //$('#txtBuyerName').removeAttr("disabled");
                //$('#txtInvoiceAddress').removeAttr("disabled");
                $('#txtInvoiceCompanyId').val('').attr("disabled", true);
                $('#txtInvoiceTitle').val('').attr("disabled", true);
            } else if ($('#ddlEinvoiceMode').val() == '1') { // 3聯
                $('#ddlCarrierType').val('0').attr('disabled', true);
                $('#txtCarrierId').val('').attr('disabled', true);
                //$('#txtBuyerName').removeAttr("disabled");
                //$('#txtInvoiceAddress').removeAttr("disabled");
                $('#txtInvoiceCompanyId').removeAttr('disabled');
                $('#txtInvoiceTitle').removeAttr('disabled');
                $('#divDonateMark').hide();
                $('#divDonateMark').next().show();
            } else if ($('#ddlEinvoiceMode').val() == '') { // 捐贈only
                $('#txtInvoiceCompanyId').val('').attr("disabled", true);
                $('#txtInvoiceTitle').val('').attr("disabled", true);
                $('#divDonateMark').show();
                $('#divDonateMark').next().hide();
            }
        });

        $('#txtCarrierId').live('keyup', function () {
            if (window.currKeyisLowerCase) {
                $(this).val($(this).val().toUpperCase());
            }
        });
        $('#txtCarrierId').live('keydown', function (e) {
            if (e.which >= 65 && e.which <= 90) {
                window.currKeyisLowerCase = true;
            } else {
                window.currKeyisLowerCase = false;
            }
        });
        $('#ddlCarrierType').live('change', function () {
            ResetInvoiceInfoError();
            $('#txtCarrierId').val('');
            //if ($(this).val() == '0') {
            //    $('#txtBuyerName').removeAttr('disabled');;
            //    $('#txtInvoiceAddress').removeAttr('disabled');
            //} else {
            //    $('#txtBuyerName').val('').attr("disabled", true);
            //    $('#txtInvoiceAddress').val('').attr("disabled", true);
            //}
            if ($(this).val() == '0' || $(this).val() == '1') {
                $('#txtCarrierId').attr("disabled", true);
            } else if ($(this).val() == '2') {
                $('#txtCarrierId').removeAttr("disabled").attr('maxLength', 8);
            } else if ($(this).val() == '3') {
                $('#txtCarrierId').removeAttr("disabled").attr('maxLength', 16);
            }
        });
        if ($('#ddlCarrierType').is(":disabled") == false) {
            if ($('#ddlCarrierType').val() == '2') {
                $('#txtCarrierId').removeAttr("disabled").attr('maxLength', 8);
            } else if ($('#ddlCarrierType').val() == '3') {
                $('#txtCarrierId').removeAttr("disabled").attr('maxLength', 16);
            }
        }

        $('.invoice-readonly').each(function () {
            var tagName = this.tagName.toUpperCase();
            if (tagName == 'SELECT') {
                var text = $('option:selected', this).text() + " ";
            } else if (tagName == 'INPUT') {
                var text = $(this).val();
            }
            $(this).after($('<span></span>').text(text)).remove();
        });
    });

    function ResetInvoiceInfoError() {
        $('.invoice-error').hide();
    }

    function CheckInvoiceInfo() {
        ResetInvoiceInfoError();
        var result = true;
        if ($('#txtInvoiceTitle').is(':enabled') && jQuery.trim($("#txtInvoiceTitle").val()) == "") {
            $('#InvoiceTitleError').show();
            result = false;
        }
        //3聯才檢查
        if ($('#ddlEinvoiceMode').val() == '1') {
            if ($('#txtInvoiceAddress').is(':enabled') && jQuery.trim($("#txtInvoiceAddress").val()) == "") {
                $('#InvoiceAddressError').show();
                result = false;
            }
            if ($('#txtInvoiceCompanyId').is(':enabled') && jQuery.trim($("#txtInvoiceCompanyId").val()) == "") {
                $('#InvoiceCompanyIdError').show();
                $('#InvoiceCompanyIdError').text('請填入統編');
                result = false;
            } else if ($('#txtInvoiceCompanyId').is(':enabled') && jQuery.trim($("#txtInvoiceCompanyId").val()).length != 8 || isNaN(jQuery.trim($("#txtInvoiceCompanyId").val()))) {
                $('#InvoiceCompanyIdError').show();
                $('#InvoiceCompanyIdError').text('請填入八碼數字');
                result = false;
            }
        } else if ($('#ddlEinvoiceMode').val() == '2') {
            if ($('#ddlCarrierType').val() == '0') {
                if ($.trim($('#txtBuyerName').val()) == '') {
                    $('#BuyerNameError').show();
                    result = false;
                }
                if ($.trim($('#txtInvoiceAddress').val()) == '') {
                    $('#InvoiceAddressError').show();
                    result = false;
                }
            }
            if ($('#ddlCarrierType').val() == '2') {
                var phoneCarrier = $.trim($('#txtCarrierId').val());
                if (phoneCarrier == '') {
                    $('#CarrierIdError').show();
                    $('#CarrierIdError').html("請輸入手機條碼。");
                    result = false;
                } else {
                    var phone_code39 = /^\/[0-9A-Z\-.$/\+%\*\s]*$/;
                    if (!phone_code39.test(phoneCarrier) || phoneCarrier.length != 8) {
                        $('#CarrierIdError').html("共8碼。限數字與大寫英文，並以/開頭。");
                        $('#CarrierIdError').show();
                        result = false;
                    } else {
                        $('#CarrierIdError').hide();
                    }
                }
            } else if ($('#ddlCarrierType').val() == '3') {
                if ($.trim($('#txtCarrierId').val()) == '') {
                    $('#CarrierIdError').show();
                    result = false;
                }
                var PersonalCertificateCarrier = $.trim($('#txtCarrierId').val());
                if (PersonalCertificateCarrier == '') {
                    $('#CarrierIdError').show();
                    $('#CarrierIdError').html("請輸入自然人憑證條碼。");
                    result = false;
                } else {
                    var PersonalCertificate = /^[A-Z][A-Z]\d{14}$/;
                    if (!PersonalCertificate.test(PersonalCertificateCarrier)) {
                        $('#CarrierIdError').html("共16碼：2碼大寫英文+14碼數字。");
                        $('#CarrierIdError').show();
                        result = false;
                    } else {
                        $('#CarrierIdError').hide();
                    }
                }

            }
        }
        return result;
    }
</script>
<style>
    .invoice-error
    {
        color: Red;
        font-weight: normal;
        display:none;
    }
    .invoice-readonly
    {
        color: black;
        background-color: transparent;
        border: 0px;
    }
</style>
                            <tr style="height:26px">
                                <td style="width:80px; text-align:right">發票類型：</td>
                                <td><asp:DropDownList ID="ddlEinvoiceMode" runat="server" /></td>
                            </tr>
                            <tr style="height:26px">
                                <td style="width:80px; text-align:right">是否捐贈：</td>
                                <td>
                                    <div id="divDonateMark">
                                        <asp:Literal ID="litInvoiceDonateMark" runat="server" />
                                    </div>
                                    <span hidden="hidden">否</span>
                                </td>
                            </tr>
                            <tr style="height:26px" id="rowInvoicePaperMark" runat="server">
                                <td style="width:80px; text-align:right">索取紙本：</td>
                                <td><asp:Literal ID="litInvoicePaperMark" runat="server" /></td>
                            </tr>
                            <tr style="height:26px">
                                <td style="width:80px; text-align:right">使用載具：</td>
                                <td>
                                    <asp:Literal ID="litCarrierInfo" runat="server" />
                                    <asp:DropDownList ID="ddlCarrierType"  runat="server" style="height:24px">
                                        <asp:ListItem Text="會員" Value="1" />
                                        <asp:ListItem Text="手機條碼" Value="2" />
                                        <asp:ListItem Text="自然人憑證" Value="3" />
                                        <asp:ListItem Text="無(紙本)" Value="0" />
                                    </asp:DropDownList><asp:TextBox ID="txtCarrierId" runat="server" style="height:24px"/>
                                    <span id="CarrierIdError" class="invoice-error">
                                        請填入載具號碼</span>
                                    <asp:HiddenField ID="hfCarrierInfo" runat="server" />
                                    <asp:HiddenField ID="hfComInfo" runat="server" />
                                </td>
                            </tr>
                            <tr style="height:26px">
                                <td style="width:80px; text-align:right">
                                    收件人：
                                </td>
                                <td>
                                    <asp:TextBox ID="txtBuyerName" runat="server" Width="100" MaxLength="50" Enabled="True" />
                                    <span id="BuyerNameError" class="invoice-error">
                                        請填入收件者</span>
                                </td>
                            </tr>
                            <tr style="height:26px">
                                <td style="text-align:right">
                                    收件地址：
                                </td>
                                <td>
                                    <asp:TextBox ID="txtInvoiceAddress" runat="server" style="width:250px" MaxLength="250" Enabled="True" />
                                    <span id="InvoiceAddressError" class="invoice-error">
                                        請填入收件地址</span>
                                </td>
                            </tr>
                            <tr style="height:26px">
                                <td style="text-align:right">
                                    統編：
                                </td>
                                <td>
                                    <asp:TextBox ID="txtInvoiceCompanyId" runat="server" Width="100" MaxLength="8" Enabled="false" />
                                    <span id="InvoiceCompanyIdError" class="invoice-error">
                                        請填入統編</span>
                                </td>
                            </tr>
                            <tr style="height:26px">
                                <td style="text-align:right">
                                    抬頭：
                                </td>
                                <td>
                                    <asp:TextBox ID="txtInvoiceTitle" runat="server" Width="100" Enabled="false" />
                                    <span id="InvoiceTitleError" class="invoice-error">
                                        請填入抬頭</span>
                                </td>
                            </tr>
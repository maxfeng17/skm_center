﻿using System;
using LunchKingSite.Core.Component;
using LunchKingSite.WebLib.Views;
using LunchKingSite.Core;

namespace LunchKingSite.Web.ControlRoom
{
    public partial class SellerHiDealList : System.Web.UI.UserControl
    {
        private IHiDealProvider hdp = ProviderFactory.Instance().GetProvider<IHiDealProvider>();
        protected void Page_Load(object sender, EventArgs e)
        {
            if(Page is ISellerAddView)
            {
                ISellerAddView pg = Page as ISellerAddView;
                lvHiDealInfo.DataSource = hdp.ViewSellerHiDealGetList(pg.SellerGuid);
                lvHiDealInfo.DataBind();
            }
        }
    }    
}
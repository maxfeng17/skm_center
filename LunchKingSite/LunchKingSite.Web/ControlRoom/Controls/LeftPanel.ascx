<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LeftPanel.ascx.cs" Inherits="LunchKingSite.Web.ControlRoom.Controls.LeftPanel" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:HyperLink ID="HyperLink3" runat="server" NavigateUrl="~">前台首頁</asp:HyperLink>&nbsp;&nbsp;
<asp:HyperLink ID="HyperLink4" runat="server" NavigateUrl="~/controlroom">後台首頁</asp:HyperLink>
<br />
<br />
<div id="FullFunctions" runat="server">
    <asp:Panel ID="pMembershipHead" CssClass="collapsePanelHeader" runat="server">
        <div style="padding: 5px; cursor: pointer; vertical-align: middle;">
            <div style="float: left;">
                會員管理</div>
            <div style="float: right; vertical-align: middle;">
                <asp:ImageButton ID="Image1" runat="server" ImageUrl="~/images/icons/expand.jpg"
                    AlternateText="(展開...)" />
            </div>
        </div>
    </asp:Panel>
    <asp:Panel ID="pMembership" runat="server" Wrap="False" CssClass="collapsePanel">
        <asp:HyperLink ID="hlIntegrate" runat="server" NavigateUrl="~/ControlRoom/User/ServiceIntegrate.aspx">客服整合系統</asp:HyperLink><br />
        <asp:HyperLink ID="hlUser" runat="server" NavigateUrl="~/ControlRoom/User/Users.aspx">會員列表</asp:HyperLink><br />
        <asp:HyperLink ID="hlRoles" runat="server" NavigateUrl="~/ControlRoom/User/Roles.aspx">群組列表</asp:HyperLink><br />
        <asp:HyperLink ID="hlBonusBack" runat="server" NavigateUrl="~/ControlRoom/User/bonus_back.aspx">紅利兌換</asp:HyperLink><br />
        <asp:HyperLink ID="hlMassEmailer" runat="server" NavigateUrl="~/ControlRoom/User/MassEmailer.aspx">大量寄會員信</asp:HyperLink><br />
        <asp:HyperLink ID="hlPponMail" runat="server" NavigateUrl="~/ControlRoom/User/SendPponOrderEmail.aspx">寄會員信(好康)</asp:HyperLink><br />
        <asp:HyperLink ID="hlservice" runat="server" NavigateUrl="~/ControlRoom/serviceworker.aspx">會員回覆系統</asp:HyperLink><br />
    </asp:Panel>
    <asp:Panel ID="pVerificationHead" CssClass="collapsePanelHeader" runat="server">
        <div style="padding: 5px; cursor: pointer; vertical-align: middle;">
            <div style="float: left;">
                核銷管理</div>
            <div style="float: right; vertical-align: middle;">
                <asp:ImageButton ID="ibVerificationHead" runat="server" ImageUrl="~/images/icons/expand.jpg"
                    AlternateText="(展開...)" />
            </div>
        </div>
    </asp:Panel>
    <asp:Panel ID="pVerification" runat="server" Wrap="False" CssClass="collapsePanel">
        <asp:HyperLink ID="hlStatistics" runat="server" NavigateUrl="~/ControlRoom/Verification/VerificationStatistics.aspx">核銷統計</asp:HyperLink><br />
        <asp:HyperLink ID="hlInventory" runat="server" NavigateUrl="~/ControlRoom/Verification/SellerVerificationSearch.aspx">清冊核銷</asp:HyperLink><br />
        <asp:HyperLink ID="hlCouponVerify" runat="server" NavigateUrl="~/ControlRoom/Verification/CouponVerification.aspx">憑證查詢</asp:HyperLink><br />
        <asp:HyperLink ID="hlVbsBalanceSheetMgmt" runat="server" NavigateUrl="~/ControlRoom/vbs/searchbalancesheet">對帳單管理</asp:HyperLink><br />
        <asp:HyperLink ID="hlVbsEmployee" runat="server" NavigateUrl="~/ControlRoom/vbs/membership/Search">內部核銷人員管理</asp:HyperLink><br />
        <asp:HyperLink ID="hlVbsVendor" runat="server" NavigateUrl="~/ControlRoom/vbs/membership/SearchVendor">商家帳號權限管理</asp:HyperLink><br />
        <asp:HyperLink ID="hlVbsBulletin" runat="server" NavigateUrl="~/ControlRoom/Verification/VbsBulletinBoardSetUp.aspx">商家公佈欄設定</asp:HyperLink>
    </asp:Panel>
    <asp:Panel ID="pEmpHead" CssClass="collapsePanelHeader" runat="server">
        <div style="padding: 5px; cursor: pointer; vertical-align: middle;">
            <div style="float: left;">
                員工資料管理</div>
            <div style="float: right; vertical-align: middle;">
                <asp:ImageButton ID="ibEmpHead" runat="server" ImageUrl="~/images/icons/expand.jpg"
                    AlternateText="(展開...)" />
            </div>
        </div>
    </asp:Panel>
    <asp:Panel ID="pEmp" runat="server" Wrap="False" CssClass="collapsePanel">
        <asp:HyperLink ID="hlEmp" runat="server" NavigateUrl="~/ControlRoom/hr/EmpSet.aspx">員工資料維護</asp:HyperLink><br />
        <asp:HyperLink ID="hkMailNotify" runat="server" NavigateUrl="~/ControlRoom/hr/MailNotifySet.aspx">通知信管理</asp:HyperLink><br />
    </asp:Panel>
    <asp:Panel ID="pItemMgtHead" CssClass="collapsePanelHeader" runat="server">
        <div style="padding: 5px; cursor: pointer; vertical-align: middle;">
            <div style="float: left;">
                菜單管理</div>
            <div style="float: right; vertical-align: middle;">
                <asp:ImageButton ID="ibItemMgtHead" runat="server" ImageUrl="~/images/icons/expand.jpg"
                    AlternateText="(展開...)" />
            </div>
        </div>
    </asp:Panel>
    <asp:Panel ID="pItemMgt" Wrap="false" CssClass="collapsePanel" runat="server">
        <asp:HyperLink ID="hlAccessoryCategory" runat="server" NavigateUrl="~/ControlRoom/Item/accessory_category.aspx">內部配料類別管理</asp:HyperLink><br />
        <asp:HyperLink ID="hlAccessoryGroup" runat="server" NavigateUrl="~/ControlRoom/Item/accessory_group.aspx">賣家配料類別管理</asp:HyperLink><br />
    </asp:Panel>
    <asp:Panel ID="pSellerMgtHead" CssClass="collapsePanelHeader" runat="server">
        <div style="padding: 5px; cursor: pointer; vertical-align: middle;">
            <div style="float: left;">
                賣家管理</div>
            <div style="float: right; vertical-align: middle;">
                <asp:ImageButton ID="ibSellerMgtHead" runat="server" ImageUrl="~/images/icons/expand.jpg"
                    AlternateText="(展開...)" />
            </div>
        </div>
    </asp:Panel>
    <asp:Panel ID="pSellerMgt" Wrap="false" CssClass="collapsePanel" runat="server">
        <asp:HyperLink ID="hlSellerAdd" runat="server" NavigateUrl="~/ControlRoom/Seller/seller_add.aspx">新增賣家</asp:HyperLink><br />
        <asp:HyperLink ID="hlSellerList" runat="server" NavigateUrl="~/ControlRoom/Seller/seller_list.aspx">賣家列表</asp:HyperLink><br />
    </asp:Panel>
    <asp:Panel ID="pBuildingMgtHead" CssClass="collapsePanelHeader" runat="server">
        <div style="padding: 5px; cursor: pointer; vertical-align: middle;">
            <div style="float: left;">
                位置管理</div>
            <div style="float: right; vertical-align: middle;">
                <asp:ImageButton ID="ibBuildingMgtHead" runat="server" ImageUrl="~/images/icons/expand.jpg"
                    AlternateText="(展開...)" />
            </div>
        </div>
    </asp:Panel>
    <asp:Panel ID="pBuildingMgt" Wrap="false" CssClass="collapsePanel" runat="server">
        <asp:HyperLink ID="hlCityAdd" runat="server" NavigateUrl="~/ControlRoom/City/city_add.aspx">區域調整</asp:HyperLink><br />
        <asp:HyperLink ID="hlBuildingAdd" runat="server" NavigateUrl="~/ControlRoom/Building/building_add.aspx">新增路段</asp:HyperLink><br />
        <asp:HyperLink ID="hlBuildingList" runat="server" NavigateUrl="~/ControlRoom/Building/building_list.aspx">路段列表</asp:HyperLink></asp:Panel>
    <asp:Panel ID="pGrouponHeat" CssClass="collapsePanelHeader" runat="server">
        <div style="padding: 5px; cursor: pointer; vertical-align: middle;">
            <div style="float: left;">
                17P好康</div>
            <div style="float: right; vertical-align: middle;">
                <asp:ImageButton ID="ibGroupon" runat="server" ImageUrl="~/images/icons/expand.jpg"
                    AlternateText="(展開...)" />
            </div>
        </div>
    </asp:Panel>
    <asp:Panel ID="pGroupon" Wrap="false" CssClass="collapsePanel" runat="server">
        <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/ControlRoom/ppon/pponorderlist.aspx">活動列表頁</asp:HyperLink><br />
        <asp:HyperLink ID="hlPponDealTimeSlot" runat="server" NavigateUrl="~/ControlRoom/ppon/PponDealTimeSlot.aspx">活動時程表</asp:HyperLink><br />
        <asp:HyperLink ID="hlPponPostList" runat="server" NavigateUrl="~/ControlRoom/ppon/QueryPost.aspx">討論區列表</asp:HyperLink>
        <br />
        <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="~/ControlRoom/ppon/Subscription.aspx">訂閱管理</asp:HyperLink><br />
        <asp:HyperLink ID="hlExportCouponList" runat="server" NavigateUrl="~/ControlRoom/ppon/ExportCouponList.aspx">匯出憑證</asp:HyperLink><br />
        <asp:HyperLink ID="hlEventList" runat="server" NavigateUrl="~/ControlRoom/event/eventlist.aspx">行銷活動列表</asp:HyperLink><br />
        <asp:HyperLink ID="HyperLink27" runat="server" NavigateUrl="~/ControlRoom/ppon/pponinsertserialkey.aspx">廠商序號上傳</asp:HyperLink><br />
        <asp:HyperLink ID="hlProvisionSet" runat="server" NavigateUrl="~/ControlRoom/Ppon/ProvisionDescSet.aspx">工單好康條款設定</asp:HyperLink><br />
        <asp:HyperLink ID="hlMagazineInfo" runat="server" NavigateUrl="~/ControlRoom/Ppon/MagazineInfo.aspx">雜誌店家查詢</asp:HyperLink><br />
        <asp:HyperLink ID="HyperLink34" runat="server" NavigateUrl="~/ControlRoom/Ppon/VacationSetup.aspx">國定假日設定</asp:HyperLink><br />
        <asp:HyperLink ID="HyperLink38" runat="server" NavigateUrl="~/ControlRoom/Ppon/FamiPortManager.aspx">全家FamiPort設定</asp:HyperLink><br />
    </asp:Panel>
    <asp:Panel ID="pPiinLifeHead" CssClass="collapsePanelHeader" runat="server">
        <div style="padding: 5px; cursor: pointer; vertical-align: middle;">
            <div style="float: left;">
                品生活</div>
            <div style="float: right; vertical-align: middle;">
                <asp:ImageButton ID="ibPiinLifeHead" runat="server" ImageUrl="~/images/icons/expand.jpg"
                    AlternateText="(展開...)" />
            </div>
        </div>
    </asp:Panel>
    <asp:Panel ID="pPiinLife" Wrap="false" CssClass="collapsePanel" runat="server">
        <asp:HyperLink ID="hkHidealOrderList" runat="server" NavigateUrl="~/ControlRoom/piinlife/HidealOrderList.aspx">PiinLife訂單列表</asp:HyperLink><br />
        <asp:HyperLink ID="hkHiDealDealTimeSlot" runat="server" NavigateUrl="~/ControlRoom/piinlife/DealTimeSlot.aspx">PiinLife活動時程表</asp:HyperLink><br />
        <asp:HyperLink ID="hkHiDealPromoBanner" runat="server" NavigateUrl="~/ControlRoom/piinlife/promobanner.aspx">PiinLife首頁Banner</asp:HyperLink><br />
        <asp:HyperLink ID="hkHiDealFaq" runat="server" NavigateUrl="~/ControlRoom/ppon/pponfaq.aspx">PiinLife-Faq設定</asp:HyperLink><br />
        <asp:HyperLink ID="hkHiDealList" runat="server" NavigateUrl="~/ControlRoom/piinlife/DealList.aspx">PiinLife檔次列表</asp:HyperLink><br />
        <asp:HyperLink ID="hkHiDealReturnedList" runat="server" NavigateUrl="~/ControlRoom/piinlife/returnedList.aspx">PiinLife退換貨列表</asp:HyperLink><br />
    </asp:Panel>
    <asp:Panel ID="pVourcherHead" CssClass="collapsePanelHeader" runat="server">
        <div style="padding: 5px; cursor: pointer; vertical-align: middle;">
            <div style="float: left;">
                優惠券</div>
            <div style="float: right; vertical-align: middle;">
                <asp:ImageButton ID="ibVourcherHead" runat="server" ImageUrl="~/images/icons/expand.jpg"
                    AlternateText="(展開...)" />
            </div>
        </div>
    </asp:Panel>
    <asp:Panel ID="pVourcher" Wrap="false" CssClass="collapsePanel" runat="server">
        <asp:HyperLink ID="HyperLink36" runat="server" NavigateUrl="~/ControlRoom/seller/seller_apply.aspx">賣家申請管理</asp:HyperLink><br />
        <asp:HyperLink ID="HyperLink35" runat="server" NavigateUrl="~/ControlRoom/event/vourchereventedit.aspx">優惠券管理</asp:HyperLink><br />
        <asp:HyperLink ID="HyperLink37" runat="server" NavigateUrl="~/ControlRoom/event/vourcherpromoedit.aspx">Top賣家管理</asp:HyperLink><br />
    </asp:Panel>
    <asp:Panel ID="divEdmHead" CssClass="collapsePanelHeader" runat="server">
        <div style="padding: 5px; cursor: pointer; vertical-align: middle;">
            <div style="float: left;">
                EDM管理</div>
            <div style="float: right; vertical-align: middle;">
                <asp:ImageButton ID="ImageButton6" runat="server" ImageUrl="~/images/icons/expand.jpg"
                    AlternateText="(展開...)" />
            </div>
        </div>
    </asp:Panel>
    <asp:Panel ID="divEdm" Wrap="false" CssClass="collapsePanel" runat="server">
        <asp:HyperLink ID="hsd" runat="server" NavigateUrl="~/ControlRoom/ppon/SubscriptionFrame.aspx">編輯訂閱信檔次</asp:HyperLink><br />
        <asp:HyperLink ID="hss" runat="server" NavigateUrl="~/ControlRoom/ppon/SubscriptionSubject.aspx">編輯訂閱信主旨</asp:HyperLink><br />
        <asp:HyperLink ID="hsb" runat="server" NavigateUrl="~/ControlRoom/ppon/SubscriptionDealId.aspx">訂閱信BID產生</asp:HyperLink><br />
        <asp:HyperLink ID="hpsd" runat="server" NavigateUrl="~/ControlRoom/piinlife/SubscriptionFrame.aspx">PL檔次編輯</asp:HyperLink><br />
        <asp:HyperLink ID="hpsp" runat="server" NavigateUrl="~/ControlRoom/piinlife/SubscriptionPromo.aspx">PL廣告編輯</asp:HyperLink><br />
        <asp:HyperLink ID="hpss" runat="server" NavigateUrl="~/ControlRoom/piinlife/SubscriptionSubject.aspx">PL主旨編輯</asp:HyperLink><br />
        <asp:HyperLink ID="HyperLink29" runat="server" NavigateUrl="~/ControlRoom/ppon/edmadsetup.aspx">新版-Top Banner編輯</asp:HyperLink><br />
        <asp:HyperLink ID="HyperLink30" runat="server" NavigateUrl="~/ControlRoom/ppon/edmsetup.aspx">新版-訂閱信編輯</asp:HyperLink><br />
        <asp:HyperLink ID="HyperLink31" runat="server" NavigateUrl="~/ControlRoom/ppon/edmtitlesetup.aspx">新版-訂閱信主旨編輯</asp:HyperLink><br />
        <asp:HyperLink ID="HyperLink32" runat="server" NavigateUrl="~/ControlRoom/ppon/edmpromosetup.aspx">新版-非訂閱信編輯</asp:HyperLink><br />
    </asp:Panel>
    <asp:Panel ID="divMarketingHeader" CssClass="collapsePanelHeader" runat="server">
        <div style="padding: 5px; cursor: pointer; vertical-align: middle;">
            <div style="float: left;">
                行銷管理</div>
            <div style="float: right; vertical-align: middle;">
                <asp:ImageButton ID="ImageButton5" runat="server" ImageUrl="~/images/icons/expand.jpg"
                    AlternateText="(展開...)" />
            </div>
        </div>
    </asp:Panel>
    <asp:Panel ID="divMarketing" Wrap="false" CssClass="collapsePanel" runat="server">
        <asp:HyperLink ID="hpm" runat="server" NavigateUrl="~/ControlRoom/system/conman.aspx">編輯器內容</asp:HyperLink><br />
        <asp:HyperLink ID="cpa" runat="server" NavigateUrl="~/controlroom/finance/referrer.aspx">CPA管理</asp:HyperLink><br />
        <asp:HyperLink ID="newcpa" runat="server" NavigateUrl="~/controlroom/ppon/cpasetup.aspx">新版CPA管理</asp:HyperLink><br />
        <asp:HyperLink ID="hprmo" runat="server" NavigateUrl="~/ControlRoom/system/promoManager.aspx">行銷Promo頁管理</asp:HyperLink><br />
        <asp:HyperLink ID="HyperLink25" runat="server" NavigateUrl="~/ControlRoom/System/PEZHtmlCombiner.aspx">Pez行銷用Html編輯器</asp:HyperLink><br />
        <asp:HyperLink ID="hrm" runat="server" NavigateUrl="~/ControlRoom/system/randomcms.aspx">行銷Banner</asp:HyperLink><br />
        <asp:HyperLink ID="eventpromo" runat="server" NavigateUrl="~/ControlRoom/event/eventpromosetup.aspx">商品主題活動</asp:HyperLink><br />
        <asp:HyperLink ID="hdc" runat="server" NavigateUrl="~/ControlRoom/discount/campaignmain.aspx">coupon code</asp:HyperLink><br />
        <asp:HyperLink ID="HyperLink26" runat="server" NavigateUrl="~/ControlRoom/event/EventActivitySetUp.aspx">跳窗EDM設定</asp:HyperLink><br />
        <asp:HyperLink runat="server" NavigateUrl="~/ControlRoom/piinlife/ManageExchangeBanner.aspx">VBO Banner</asp:HyperLink><br />
        <asp:HyperLink ID="HyperLink39" runat="server" NavigateUrl="~/ControlRoom/Ppon/PushAppSetup.aspx">App推播管理列表</asp:HyperLink><br />
    </asp:Panel>
    <asp:Panel ID="pMiscH" CssClass="collapsePanelHeader" runat="server">
        <div style="padding: 5px; cursor: pointer; vertical-align: middle;">
            <div style="float: left;">
                其他</div>
            <div style="float: right; vertical-align: middle;">
                <asp:ImageButton ID="ibpMisc" runat="server" ImageUrl="~/images/icons/expand.jpg"
                    AlternateText="(展開...)" />
            </div>
        </div>
    </asp:Panel>
    <asp:Panel ID="pMisc" Wrap="false" CssClass="collapsePanel" runat="server">
        <asp:HyperLink ID="hj" runat="server" NavigateUrl="~/ControlRoom/system/jobs.aspx">工作排程</asp:HyperLink><br />
        <asp:HyperLink ID="hcpf" runat="server" NavigateUrl="~/ControlRoom/ppon/pponcouponfixtool.aspx">憑證修復</asp:HyperLink><br />
        <asp:HyperLink ID="hcr" runat="server" NavigateUrl="~/ControlRoom/ppon/pponcouponrefund.aspx">憑證/商品批次退貨</asp:HyperLink><br />
        <asp:HyperLink ID="hfaq" runat="server" NavigateUrl="~/ControlRoom/ppon/pponfaq.aspx">Faq設定</asp:HyperLink><br />
        <asp:HyperLink ID="hdmrs" runat="server" NavigateUrl="~/ControlRoom/system/DataManagerReset.aspx">暫存資料重設</asp:HyperLink><br />
        <asp:HyperLink ID="HyperLink19" runat="server" NavigateUrl="~/ControlRoom/Order/ImportBankInfo.aspx">匯入銀行資料</asp:HyperLink><br />
        <asp:HyperLink ID="HyperLink33" runat="server" NavigateUrl="~/ControlRoom/System/CreditCardTest.aspx">信用卡測試</asp:HyperLink><br />
    </asp:Panel>
    <asp:Panel ID="pOrderMgtHead" CssClass="collapsePanelHeader" runat="server">
        <div style="padding: 5px; cursor: pointer; vertical-align: middle;">
            <div style="float: left;">
                訂單管理</div>
            <div style="float: right; vertical-align: middle;">
                <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/images/icons/expand.jpg"
                    AlternateText="(展開...)" />
            </div>
        </div>
    </asp:Panel>
    <asp:Panel ID="pOrderMgt" Wrap="false" CssClass="collapsePanel" runat="server">
        <asp:HyperLink ID="hlOrderMonitor" runat="server" NavigateUrl="~/ControlRoom/Order/order_monitor.aspx">本日訂單監控</asp:HyperLink><br />
        <asp:HyperLink ID="hlOrderList" runat="server" NavigateUrl="~/ControlRoom/Order/order_list.aspx">訂單列表</asp:HyperLink><br />
        <asp:HyperLink ID="hlReviewList" runat="server" NavigateUrl="~/ControlRoom/Order/review_list.aspx">評鑑列表</asp:HyperLink><br />
        <asp:HyperLink ID="hlCreditcardTrans" runat="server" NavigateUrl="~/ControlRoom/Ppon/QueryCreditcardTransaction.aspx">交易列表</asp:HyperLink><br />
        <asp:HyperLink ID="hlReturnList" runat="server" NavigateUrl="~/ControlRoom/Ppon/PponReturnList.aspx">退換貨訂單列表</asp:HyperLink><br />
        <asp:HyperLink ID="hlShipStatus" runat="server" NavigateUrl="~/ControlRoom/Order/order_shipstatus.aspx">出貨狀態調整</asp:HyperLink><br />
        <asp:HyperLink ID="HyperLink40" runat="server" NavigateUrl="~/ControlRoom/Order/OrderShipBatchClear.aspx">出貨批次刪除</asp:HyperLink>
    </asp:Panel>
    <asp:Panel ID="pReportsMgtHead" CssClass="collapsePanelHeader" runat="server">
        <div style="padding: 5px; cursor: pointer; vertical-align: middle;">
            <div style="float: left;">
                檢視報表</div>
            <div style="float: right; vertical-align: middle;">
                <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/images/icons/expand.jpg"
                    AlternateText="(展開...)" />
            </div>
        </div>
    </asp:Panel>
    <asp:Panel ID="pReportsMgt" Wrap="false" CssClass="collapsePanel" runat="server">
        <asp:HyperLink ID="HyperLink7" runat="server" NavigateUrl="http://batman.17life.com/Reports/Pages/Report.aspx?ItemPath=/SellerOrderReport"
            Target="_blank">商家別銷售報表</asp:HyperLink><br />
        <asp:HyperLink ID="HyperLink8" runat="server" NavigateUrl="http://batman.17life.com/Reports/Pages/Report.aspx?ItemPath=/SellerOrderReport_total"
            Target="_blank">商家別銷售總表</asp:HyperLink><br />
        <asp:HyperLink ID="HyperLink15" runat="server" NavigateUrl="http://batman.17life.com/Reports/Pages/Report.aspx?ItemPath=/DistrictSelling"
            Target="_blank">館別分區報表</asp:HyperLink><br />
        <asp:HyperLink ID="HyperLink16" runat="server" NavigateUrl="http://batman.17life.com/Reports/Pages/Report.aspx?ItemPath=/CreditcardCharging"
            Target="_blank">信用卡請款報表</asp:HyperLink><br />
        <asp:HyperLink ID="HyperLink17" runat="server" NavigateUrl="http://batman.17life.com/Reports/Pages/Report.aspx?ItemPath=/AccountPayable"
            Target="_blank">應付帳款報表</asp:HyperLink><br />
        <asp:HyperLink ID="HyperLink18" runat="server" NavigateUrl="http://batman.17life.com/Reports/Pages/Report.aspx?ItemPath=/SalesBonus"
            Target="_blank">業務獎金報表</asp:HyperLink><br />
        <asp:HyperLink ID="HyperLink6" runat="server" NavigateUrl="~/ControlRoom/Verification/VerificationPrint.aspx">信託核銷報表</asp:HyperLink><br />
        <asp:HyperLink ID="HyperLink9" runat="server" NavigateUrl="~/ControlRoom/Verification/VerificationSearch.aspx">信託核銷查詢</asp:HyperLink><br />
        <asp:HyperLink ID="HyperLink10" runat="server" NavigateUrl="~/ControlRoom/Einvoice/EinvoiceWinnerList.aspx">中獎發票報表</asp:HyperLink>
    </asp:Panel>
    <asp:Panel ID="Panel1" CssClass="collapsePanelHeader" runat="server">
        <div style="padding: 5px; cursor: pointer; vertical-align: middle;">
            <div style="float: left;">
                常用連結</div>
            <div style="float: right; vertical-align: middle;">
                <asp:ImageButton ID="ImageButton3" runat="server" ImageUrl="~/images/icons/expand.jpg"
                    AlternateText="(展開...)" />
            </div>
        </div>
    </asp:Panel>
    <asp:Panel ID="Panel2" Wrap="false" CssClass="collapsePanel" runat="server">
        <asp:HyperLink ID="HyperLink11" runat="server" NavigateUrl="http://192.168.1.4:8080/webfax/servlet/index?sel=1"
            Target="_blank">Webfax Server</asp:HyperLink><br />
        <asp:HyperLink ID="HyperLink12" runat="server" NavigateUrl="http://lunchkingsrc.dnsalias.net/lksite/"
            Target="_blank">午餐王測試站</asp:HyperLink><br />
        <asp:HyperLink ID="HyperLink13" runat="server" NavigateUrl="http://tw.myblog.yahoo.com/lunchkingblog"
            Target="_blank">午餐王部落格</asp:HyperLink><br />
        <asp:HyperLink ID="HyperLink14" runat="server" NavigateUrl="http://lkcollaboration/Pages/Default.aspx"
            Target="_blank">午餐王共通平台</asp:HyperLink><br />
        <asp:HyperLink ID="HyperLink5" runat="server" NavigateUrl="http://lkcollaboration/Sites/LunchKingSite/Wiki/%E9%A6%96%E9%A0%81.aspx"
            Target="_blank">午餐王專用Wiki</asp:HyperLink></asp:Panel>
    <asp:Panel ID="Panel3" CssClass="collapsePanelHeader" runat="server">
        <div style="padding: 5px; cursor: pointer; vertical-align: middle;">
            <div style="float: left;">
                財務管理</div>
            <div style="float: right; vertical-align: middle;">
                <asp:ImageButton ID="ImageButton4" runat="server" ImageUrl="~/images/icons/expand.jpg"
                    AlternateText="(展開...)" />
            </div>
        </div>
    </asp:Panel>
    <asp:Panel ID="Panel4" Wrap="false" CssClass="collapsePanel" runat="server">
        <asp:HyperLink ID="HyperLink20" runat="server" NavigateUrl="~/ControlRoom/finance/atmach.aspx">中信ACH</asp:HyperLink><br />
        <asp:HyperLink ID="HyperLink21" runat="server" NavigateUrl="~/ControlRoom/Finance/AtmList.aspx">ATM列表</asp:HyperLink><br />
        <asp:HyperLink ID="HyperLink22" runat="server" NavigateUrl="~/ControlRoom/Finance/AtmRefundList.aspx">ATM退貨列表</asp:HyperLink><br />
        <asp:HyperLink ID="HyperLink28" runat="server" NavigateUrl="~/ControlRoom/Finance/AntiCreditcardFraud.aspx">防盜刷管理</asp:HyperLink><br />
        <asp:HyperLink ID="HyperLink23" runat="server" NavigateUrl="~/ControlRoom/Verification/WeeklyPayAccountSetUp.aspx">檔次核銷出帳設定</asp:HyperLink><br />
        <asp:HyperLink ID="HyperLink24" runat="server" NavigateUrl="~/ControlRoom/Verification/WeeklyPayReport.aspx">檔次核銷出帳報表</asp:HyperLink><br />
        <a href="<%= GetRouteUrl("HiDealVerification", new {action="WeeklyPayReport"}) %>">品生活每週ACH報表</a><br />
        <asp:HyperLink runat="server" NavigateUrl="~/ControlRoom/verification/PaymentReceiptManage.aspx">發票收據檢核</asp:HyperLink><br />
        <asp:HyperLink runat="server" NavigateUrl="~/ControlRoom/HiDealVerification/FundsTransferMgmt">匯款金額管理</asp:HyperLink>  <br />      
        <%--    <asp:HyperLink ID="HyperLink6" runat="server" NavigateUrl="../finance/PponDealReports.aspx"
        Target="_blank">館別分區報表</asp:HyperLink><br />--%>
    </asp:Panel>
</div>
<div id="RestrictFunctions" runat="server">
</div>
<br />

using LunchKingSite.Core.UI;
using System;
using System.ComponentModel;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LunchKingSite.Web.ControlRoom.Controls
{
    public partial class Pager : BaseUserControl
    {
        public delegate int GetCountHandler();

        public delegate void UpdateHandler(int pageNumber);

        private event GetCountHandler _getCountFunc = null;

        [Browsable(true), Category("Behavior"), Description("Handler for getting record count"), DesignOnly(true)]
        public event GetCountHandler GetCount
        {
            add { _getCountFunc += new GetCountHandler(value); }
            remove { _getCountFunc = null; }
        }

        private event UpdateHandler _updateFunc = null;

        [Browsable(true), Category("Behavior"), Description("Handler for getting record count"), DesignOnly(true)]
        public event UpdateHandler Update
        {
            add { _updateFunc += new UpdateHandler(value); }
            remove { _updateFunc = null; }
        }

        private int _pageCount = 0;

        public int PageCount
        {
            get { return _pageCount; }
        }

        //用來設定是否於PageLoad時啟動SetPaging的動作. 因為有些資料量龐大的Page不希望於PageLoad時就去抓資料, 故設此屬性以利設定
        public Pager()
        {
            SetPagingWhenPageLoad = true;
        }

        public bool SetPagingWhenPageLoad { get; set; }

        public int PageSize
        {
            get { return ViewState["ps"] != null ? (int)ViewState["ps"] : 15; }
            set { ViewState["ps"] = value; }
        }

        private int _curPage = 1;

        public int CurrentPage
        {
            get { return _curPage; }
            set { ResolvePagerView(value, false); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (SetPagingWhenPageLoad)
                {
                    SetupPaging();
                    ResolvePagerView(1);
                }
                else
                {
                    lFirst.Enabled = lLast.Enabled = lPrev.Enabled = lNext.Enabled = false;
                }
            }
        }

        public void SetupPaging()
        {
            int totalRecords = 0;
            int pageSize = this.PageSize;

            if (_getCountFunc != null)
            {
                totalRecords = _getCountFunc();
            }
            else
            {
                totalRecords = 0;
            }

            _pageCount = totalRecords / pageSize;
            if (totalRecords % pageSize > 0)
            {
                _pageCount++;
            }

            lblPageCount.Text = _pageCount.ToString();

            //load up the list items
            ddlPages.Items.Clear();
            for (int i = 1; i <= _pageCount; i++)
            {
                ddlPages.Items.Add(new ListItem(i.ToString(), i.ToString()));
            }
            lrc.Text = totalRecords.ToString();
        }

        public void ResolvePagerView(int currentPage, bool recount)
        {
            int pageCount = 1;

            if (recount)
            {
                SetupPaging();
            }

            int.TryParse(lblPageCount.Text, out pageCount);

            if (currentPage > pageCount)
            {
                currentPage = 1;
            }

            int nextPage = currentPage + 1;
            int prevPage = currentPage - 1;

            btnPrev.Enabled = true;
            btnNext.Enabled = true;
            btnLast.Enabled = true;
            btnFirst.Enabled = true;
            lPrev.Enabled = true;
            lNext.Enabled = true;
            lLast.Enabled = true;
            lFirst.Enabled = true;

            if (currentPage >= pageCount)
            {
                btnNext.Enabled = false;
                btnLast.Enabled = false;
                lNext.Enabled = false;
                lLast.Enabled = false;
            }
            if (currentPage == 1)
            {
                btnPrev.Enabled = false;
                btnFirst.Enabled = false;
                lPrev.Enabled = false;
                lFirst.Enabled = false;
            }
            ddlPages.SelectedValue = currentPage.ToString();
            _curPage = currentPage;
        }

        public void ResolvePagerView(int currentPage)
        {
            ResolvePagerView(currentPage, false);
        }

        protected void pager_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton btn = (ImageButton)sender;
            string pageCommand = btn.CommandArgument;
            int currentPage = ddlPages.SelectedIndex + 1;

            switch (pageCommand)
            {
                case "First":
                    currentPage = 1;
                    break;

                case "Prev":
                    currentPage--;
                    break;

                case "Next":
                    currentPage++;
                    break;

                case "Last":
                    currentPage = int.Parse(lblPageCount.Text);
                    break;
            }

            //reload the grid
            if (_updateFunc != null)
                _updateFunc(currentPage);
            ResolvePagerView(currentPage);
        }

        protected void ddlPages_SelectedIndexChanged(object sender, EventArgs e)
        {
            //reload the grid
            if (_updateFunc != null)
                _updateFunc(ddlPages.SelectedIndex + 1);
            ResolvePagerView(ddlPages.SelectedIndex + 1);
        }

        protected void link_Click(object sender, EventArgs e)
        {
            LinkButton btn = (LinkButton)sender;
            string pageCommand = btn.CommandArgument;
            int currentPage = ddlPages.SelectedIndex + 1;

            switch (pageCommand)
            {
                case "First":
                    currentPage = 1;
                    break;

                case "Prev":
                    currentPage--;
                    break;

                case "Next":
                    currentPage++;
                    break;

                case "Last":
                    currentPage = int.Parse(lblPageCount.Text);
                    break;
            }

            //reload the grid
            if (_updateFunc != null)
                _updateFunc(currentPage);
            ResolvePagerView(currentPage);
        }
    }
}
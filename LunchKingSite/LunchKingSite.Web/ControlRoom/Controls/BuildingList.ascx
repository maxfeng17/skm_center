﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BuildingList.ascx.cs" Inherits="LunchKingSite.Web.ControlRoom.Controls.BuildingList" %>
<%@ Register Assembly="System.Web.Extensions"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Src="Pager.ascx" TagName="Pager" TagPrefix="uc1" %>
            <asp:UpdatePanel ID="UBuildingPanel" runat="server">
                <ContentTemplate>
                        <table width="1100">
                            <tr>
                                <td>
                                    <asp:DropDownList ID="SearchDropDownList" runat="server" DataTextField="Value"
                                        DataValueField="Key">
                                    </asp:DropDownList></td>
                                <td>
                                    <asp:TextBox ID="tbSearch" runat="server"></asp:TextBox>
                                </td>
                                <td>
                                    區域:</td>
                                <td>
                                    <asp:DropDownList ID="ddlCity" runat="server" AutoPostBack="True" DataTextField="CityName"
                                        DataValueField="Id" OnSelectedIndexChanged="ddlCity_SelectedIndexChanged">
                                    </asp:DropDownList>&nbsp;
                                    <asp:DropDownList ID="ddlZone" runat="server" DataTextField="CityName" DataValueField="Id">
                                    </asp:DropDownList></td>
                                <td>
                                    狀態:</td>
                                <td>
                                    <asp:RadioButtonList ID="rbl" runat="server" DataTextField="value" DataValueField="key"
                                        Font-Size="10pt" RepeatDirection="Horizontal">
                                    </asp:RadioButtonList></td>
                                <td>
                                    與商家距離</td>
                                <td>
                                    <asp:TextBox ID="tbd" runat="server"></asp:TextBox></td>
                                <td>
                                    公尺以內</td>
                                <td>
                        <asp:Button ID="bSearchBtn" runat="server" OnClick="bSearchBtn_Click" Text="搜尋" /></td>
                                <td>
                                    顯示筆數</td>
                                <td>
                                    <asp:DropDownList ID="ddlPageSize" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
                                        <asp:ListItem Value="20">20筆</asp:ListItem>
                                        <asp:ListItem Value="40">40筆</asp:ListItem>
                                        <asp:ListItem Value="60">60筆</asp:ListItem>
                                        <asp:ListItem Value="80">80筆</asp:ListItem>
                                        <asp:ListItem Value="100">100筆</asp:ListItem>
                                        <asp:ListItem Selected="True" Value="150">150筆</asp:ListItem>
                                        <asp:ListItem Value="300">300筆</asp:ListItem>
                                        <asp:ListItem Value="500">500筆</asp:ListItem>
                                    </asp:DropDownList></td>
                                <td>
                                    <aspajax:UpdateProgress id="UProgress" runat="server" AssociatedUpdatePanelID="UBuildingPanel">
                                        <progresstemplate>
<IMG src="../../Themes/PCweb/images/spinner.gif" __designer:dtid="1125899906842670" /> 
</progresstemplate>
                                    </aspajax:UpdateProgress></td>
                            </tr>
                        </table>
            <asp:GridView ID="gvBuilding" runat="server" AllowPaging="True" AllowSorting="True"
                AutoGenerateColumns="False" CellPadding="2" DataKeyNames="GUID" EmptyDataText="無符合的資料"
                Font-Size="Small" ForeColor="Black" GridLines="None" OnSorting="gvBuilding_Sorting"
                PageSize="150" Width="100%" BackColor="LightGoldenrodYellow" BorderColor="Tan" BorderWidth="1px">
                <FooterStyle BackColor="Tan" />
                <Columns>
                    <asp:TemplateField HeaderText="選擇">
                        <ItemTemplate>
                            <asp:CheckBox ID="bcb" runat="server" />
                        </ItemTemplate>
                        <HeaderTemplate>
                            <asp:CheckBox ID="cbSelectAll" runat="server" AutoPostBack="True" OnCheckedChanged="cbSelectAll_CheckedChanged"
                                Text="全選" />
                        </HeaderTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="BuildingId" HeaderText="編號" ReadOnly="True" Visible="False" />
                    <asp:CheckBoxField DataField="BuildingOnline" HeaderText="上線" ReadOnly="True" />
                    <asp:HyperLinkField DataNavigateUrlFields="GUID" DataNavigateUrlFormatString="~/ControlRoom/Building/Building_Add.aspx?bid={0}"
                        DataTextField="BuildingName" HeaderText="名稱" >
                        <ItemStyle Width="150px" />
                    </asp:HyperLinkField>
                    <asp:BoundField DataField="BuildingAddressNumber" HeaderText="路段別" >
                        <ItemStyle Width="200px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="CityName" HeaderText="區域" ReadOnly="True" />
                    <asp:BoundField DataField="BuildingRank" HeaderText="順序" />
                    <asp:CommandField ButtonType="Button" HeaderText="Delete" ShowDeleteButton="True"
                        Visible="False" />
                </Columns>
                <SelectedRowStyle BackColor="DarkSlateBlue" ForeColor="GhostWhite" />
                <PagerStyle BackColor="PaleGoldenrod" ForeColor="DarkSlateBlue" HorizontalAlign="Center" />
                <HeaderStyle BackColor="Tan" Font-Bold="True" Wrap="False" />
                <AlternatingRowStyle BackColor="PaleGoldenrod" />
            </asp:GridView>
                    <table width="100%">
                        <tr>
                            <td>
            <uc1:Pager ID="gridPager" runat="server" PageSize="150" OnGetCount="RetrieveBuildingCount" OnUpdate="UpdateHandler" />
                            </td>
                            <td>
                                總筆數 :
                                <asp:Label ID="TotalConut" runat="server" OnLoad="TotalConut_Load"></asp:Label></td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>
&nbsp;

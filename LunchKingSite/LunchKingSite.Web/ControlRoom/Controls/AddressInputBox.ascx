<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AddressInputBox.ascx.cs"
    Inherits="LunchKingSite.Web.ControlRoom.Controls.AddressInputBox" %>
<asp:Literal ID="lsn" runat="server" />
<asp:TextBox ID="tbAddr" runat="server" Columns="50" MaxLength="50" />
<asp:RequiredFieldValidator ID="v1" runat="server" ControlToValidate="tbAddr" Display="None"
    ErrorMessage="<%$Resources:Localization,ReqFullAddress%>" />
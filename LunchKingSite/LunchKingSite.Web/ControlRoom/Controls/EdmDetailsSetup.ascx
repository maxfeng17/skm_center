﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EdmDetailsSetup.ascx.cs"
    Inherits="LunchKingSite.Web.ControlRoom.Ppon.EdmDetailsSetup" %>
<%@ Import Namespace="LunchKingSite.Core" %>
<script type="text/javascript">

    $(document).ready(
    function () {
        $('option').css('width', '650px');

    }
    );

    function showdetail(item) {
        $(item).show();
    }

</script>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
        <asp:PlaceHolder ID="PlaceHolder1" runat="server" EnableViewState="false">
            <script type="text/javascript">
                function chineseCount(word, d) {
                    v = 0
                    for (cc = 0; cc < word.length; cc++) {
                        c = word.charCodeAt(cc);
                        if (!(c >= 32 && c <= 126)) v++;
                    }
                    total = Math.ceil((word.length - v) / 2);
                    $(d).text(v + total);
                }
                Sys.Application.add_load(WireEvent);
                function WireEvent() {
                  <%if (MainDetailType == (int)EdmDetailType.PiinLife_1 || MainDetailType == (int)EdmDetailType.PiinLife_2)
                    { %>
                    $('.<%="bid"+this.ClientID %>').text('Did : ');
                     <%} %>
                    $('#<%=ddl_AllDeals_1.ClientID %>').change(function () {
                        var t = $('#<%=ddl_AllDeals_1.ClientID %> option:selected').text();
                        $('#<%=tbx_Bid_1.ClientID %>').val('');
                        $('#td<%=ddl_AllDeals_1.ClientID %>').find('input:text').val(t);
                        chineseCount(t, $('#div<%=tbx_Title_1.ClientID %>'));
                    });
                    $('#<%=ddl_AllDeals_2.ClientID %>').change(function () {
                        var t = $('#<%=ddl_AllDeals_2.ClientID %> option:selected').text();
                        $('#<%=tbx_Bid_2.ClientID %>').val('');
                        $('#td<%=ddl_AllDeals_2.ClientID %>').find('input:text').val(t);
                        chineseCount(t, $('#div<%=tbx_Title_2.ClientID %>'));
                    });
                    $('#<%=ddl_AllDeals_3.ClientID %>').change(function () {
                        var t = $('#<%=ddl_AllDeals_3.ClientID %> option:selected').text();
                        $('#<%=tbx_Bid_3.ClientID %>').val('');
                        $('#td<%=ddl_AllDeals_3.ClientID %>').find('input:text').val(t);
                        chineseCount(t, $('#div<%=tbx_Title_3.ClientID %>'));
                    });
                    $('#<%=ddl_AllDeals_4.ClientID %>').change(function () {
                        var t = $('#<%=ddl_AllDeals_4.ClientID %> option:selected').text();
                        $('#<%=tbx_Bid_4.ClientID %>').val('');
                        $('#td<%=ddl_AllDeals_4.ClientID %>').find('input:text').val(t);
                        chineseCount(t, $('#div<%=tbx_Title_4.ClientID %>'));
                    });
                    $('#<%=ddl_AllDeals_5.ClientID %>').change(function () {
                        var t = $('#<%=ddl_AllDeals_5.ClientID %> option:selected').text();
                        $('#<%=tbx_Bid_5.ClientID %>').val('');
                        $('#td<%=ddl_AllDeals_5.ClientID %>').find('input:text').val(t);
                        chineseCount(t, $('#div<%=tbx_Title_5.ClientID %>'));
                    });
                    $('#<%=ddl_AllDeals_6.ClientID %>').change(function () {
                        var t = $('#<%=ddl_AllDeals_6.ClientID %> option:selected').text();
                        $('#<%=tbx_Bid_6.ClientID %>').val('');
                        $('#td<%=ddl_AllDeals_6.ClientID %>').find('input:text').val(t);
                        chineseCount(t, $('#div<%=tbx_Title_6.ClientID %>'));
                    });
                    $('#<%=ddl_AllDeals_7.ClientID %>').change(function () {
                        var t = $('#<%=ddl_AllDeals_7.ClientID %> option:selected').text();
                        $('#<%=tbx_Bid_7.ClientID %>').val('');
                        $('#td<%=ddl_AllDeals_7.ClientID %>').find('input:text').val(t);
                        chineseCount(t, $('#div<%=tbx_Title_7.ClientID %>'));
                    });
                    $('#<%=ddl_AllDeals_8.ClientID %>').change(function () {
                        var t = $('#<%=ddl_AllDeals_8.ClientID %> option:selected').text();
                        $('#<%=tbx_Bid_8.ClientID %>').val('');
                        $('#td<%=ddl_AllDeals_8.ClientID %>').find('input:text').val(t);
                        chineseCount(t, $('#div<%=tbx_Title_8.ClientID %>'));
                    });
                    $('#<%=ddl_AllDeals_9.ClientID %>').change(function () {
                        var t = $('#<%=ddl_AllDeals_9.ClientID %> option:selected').text();
                        $('#<%=tbx_Bid_9.ClientID %>').val('');
                        $('#td<%=ddl_AllDeals_9.ClientID %>').find('input:text').val(t);
                        chineseCount(t, $('#div<%=tbx_Title_9.ClientID %>'));
                    });
                    $('#<%=ddl_AllDeals_10.ClientID %>').change(function () {
                        var t = $('#<%=ddl_AllDeals_10.ClientID %> option:selected').text();
                        $('#<%=tbx_Bid_10.ClientID %>').val('');
                        $('#td<%=ddl_AllDeals_10.ClientID %>').find('input:text').val(t);
                        chineseCount(t, $('#div<%=tbx_Title_10.ClientID %>'));
                    });
                    $('#<%=ddl_AllDeals_11.ClientID %>').change(function () {
                        var t = $('#<%=ddl_AllDeals_11.ClientID %> option:selected').text();
                        $('#<%=tbx_Bid_11.ClientID %>').val('');
                        $('#td<%=ddl_AllDeals_11.ClientID %>').find('input:text').val(t);
                        chineseCount(t, $('#div<%=tbx_Title_11.ClientID %>'));
                    });
                    $('#<%=ddl_AllDeals_12.ClientID %>').change(function () {
                        var t = $('#<%=ddl_AllDeals_12.ClientID %> option:selected').text();
                        $('#<%=tbx_Bid_12.ClientID %>').val('');
                        $('#td<%=ddl_AllDeals_12.ClientID %>').find('input:text').val(t);
                        chineseCount(t, $('#div<%=tbx_Title_12.ClientID %>'));
                    });
                    $('#<%=tbx_Title_1.ClientID %>').keyup(function (event) {
                        chineseCount($(this).val(), $('#div<%=tbx_Title_1.ClientID %>'));
                    });
                    $('#<%=tbx_Title_2.ClientID %>').keyup(function (event) {
                        chineseCount($(this).val(), $('#div<%=tbx_Title_2.ClientID %>'));
                    });
                    $('#<%=tbx_Title_3.ClientID %>').keyup(function (event) {
                        chineseCount($(this).val(), $('#div<%=tbx_Title_3.ClientID %>'));
                    });
                    $('#<%=tbx_Title_4.ClientID %>').keyup(function (event) {
                        chineseCount($(this).val(), $('#div<%=tbx_Title_4.ClientID %>'));
                    });
                    $('#<%=tbx_Title_5.ClientID %>').keyup(function (event) {
                        chineseCount($(this).val(), $('#div<%=tbx_Title_5.ClientID %>'));
                    });
                    $('#<%=tbx_Title_6.ClientID %>').keyup(function (event) {
                        chineseCount($(this).val(), $('#div<%=tbx_Title_6.ClientID %>'));
                    });
                    $('#<%=tbx_Title_7.ClientID %>').keyup(function (event) {
                        chineseCount($(this).val(), $('#div<%=tbx_Title_7.ClientID %>'));
                    });
                    $('#<%=tbx_Title_8.ClientID %>').keyup(function (event) {
                        chineseCount($(this).val(), $('#div<%=tbx_Title_8.ClientID %>'));
                    });
                    $('#<%=tbx_Title_9.ClientID %>').keyup(function (event) {
                        chineseCount($(this).val(), $('#div<%=tbx_Title_9.ClientID %>'));
                    });
                    $('#<%=tbx_Title_10.ClientID %>').keyup(function (event) {
                        chineseCount($(this).val(), $('#div<%=tbx_Title_10.ClientID %>'));
                    });
                    $('#<%=tbx_Title_11.ClientID %>').keyup(function (event) {
                        chineseCount($(this).val(), $('#div<%=tbx_Title_11.ClientID %>'));
                    });
                    $('#<%=tbx_Title_12.ClientID %>').keyup(function (event) {
                        chineseCount($(this).val(), $('#div<%=tbx_Title_12.ClientID %>'));
                    });
                    $('#<%=ddl_MD_City.ClientID %> option[value="444"]').hide();
                }
            </script>
            <asp:HiddenField ID="hif_EdmSetupCityId" runat="server" />
            <asp:HiddenField ID="hif_DeliveryDate" runat="server" />
            <asp:HiddenField ID="hif_DealDate" runat="server" />
            <asp:HiddenField ID="hif_Pid" runat="server" />
            <asp:HiddenField ID="hif_MainDetailType" runat="server" />
            <asp:HiddenField ID="hif_MainDetailItemType" runat="server" />
        </asp:PlaceHolder>
        <fieldset class="fieldset" style="margin-top: 20px">
            <legend class="title">
                <asp:Literal ID="lit_Name" runat="server"></asp:Literal></legend>
            <table style="background-color: gray; width: 950px">
                <tr>
                    <td class="bgOrange" style="width: 100px">類別 :
                    </td>
                    <td class="bgBlue">
                        <asp:DropDownList ID="ddl_MD_City" runat="server" OnSelectedIndexChanged="ChangeCity"
                            AutoPostBack="true" Width="150">
                        </asp:DropDownList>
                        <asp:CheckBox ID="cbx_Display_CityName" runat="server" Text="顯示區域名稱" />

                        <asp:PlaceHolder ID="ph_MD_SortType" runat="server" Visible="False">&nbsp;&nbsp;<span>排序方式：</span>
                            <asp:DropDownList ID="ddl_MD_SortType" runat="server" AutoPostBack="true" Width="150" OnSelectedIndexChanged="ChangeSortType"> 
                                <asp:ListItem Text="依前台推薦" Value="0"></asp:ListItem>
                                <asp:ListItem Text="依銷售份數" Value="1"></asp:ListItem>
                                <asp:ListItem Text="依銷售總額" Value="2"></asp:ListItem>
                                <asp:ListItem Text="依上檔日期" Value="3"></asp:ListItem>
                            </asp:DropDownList>
                        </asp:PlaceHolder>
                    </td>
                </tr>
                <tr>
                    <td class="bgOrange">選擇版型 :
                    </td>
                    <td class="bgBlue">行:
                        <asp:DropDownList ID="ddl_Row" runat="server" OnSelectedIndexChanged="ChangeTotal"
                            AutoPostBack="true" Width="150">
                            <asp:ListItem Text="0" Value="0"></asp:ListItem>
                            <asp:ListItem Text="1" Value="1"></asp:ListItem>
                            <asp:ListItem Text="2" Value="2"></asp:ListItem>
                            <asp:ListItem Text="3" Value="3"></asp:ListItem>
                            <asp:ListItem Text="4" Value="4"></asp:ListItem>
                        </asp:DropDownList>
                        列:
                        <asp:DropDownList ID="ddl_Column" runat="server" OnSelectedIndexChanged="ChangeTotal"
                            AutoPostBack="true" Width="150">
                            <asp:ListItem Text="0" Value="0"></asp:ListItem>
                            <asp:ListItem Text="1" Value="1"></asp:ListItem>
                            <asp:ListItem Text="1(旅遊專屬)" Value="4"></asp:ListItem>
                            <asp:ListItem Text="2" Value="2"></asp:ListItem>
                            <asp:ListItem Text="3" Value="3"></asp:ListItem>
                            <asp:ListItem Text="4" Value="4"></asp:ListItem>
                            <asp:ListItem Text="5" Value="5"></asp:ListItem>
                            <asp:ListItem Text="6" Value="6"></asp:ListItem>
                            <asp:ListItem Text="7" Value="7"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:Panel ID="pan_1" runat="server">
                            <table style="background-color: gray; width: 950px">
                                <tr>
                                    <td class="title1" colspan="4">Deal 1
                                    </td>
                                </tr>
                                <tr>
                                    <td class="bgLOrange" style="width: 100px">檔次 :
                                    </td>
                                    <td class="bgLBlue" style="width: 350px">
                                        <asp:DropDownList ID="ddl_AllDeals_1" runat="server" Width="400">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="bgLOrange" style="width: 40px">
                                        <span class='<%="bid"+this.ClientID %>'>Bid :</span>
                                    </td>
                                    <td class="bgLBlue">
                                        <asp:TextBox ID="tbx_Bid_1" runat="server" Width="250"></asp:TextBox>
                                        <asp:Button ID="btn_SearchBid_1" runat="server" Text="搜尋" OnClick="SearchBid" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="bgLOrange" style="width: 100px">標題(APP標) :<br />
                                        <asp:Label ID="lab_Limit_1" runat="server"></asp:Label>
                                    </td>
                                    <td colspan="3" class="bgLBlue">
                                        <table>
                                            <tr>
                                                <td id='<%="td"+ddl_AllDeals_1.ClientID %>'>
                                                    <asp:TextBox ID="tbx_Title_1" runat="server" Width="750"></asp:TextBox>
                                                    <asp:Image ID="img_Valid_1" runat="server" Height="21" />
                                                </td>
                                                <td>
                                                    <div id='<%="div"+tbx_Title_1.ClientID %>'>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <asp:Panel ID="pan_2" runat="server">
                            <table style="background-color: gray; width: 950px">
                                <tr>
                                    <td class="title2" colspan="4">Deal 2
                                    </td>
                                </tr>
                                <tr>
                                    <td class="bgLOrange" style="width: 100px">檔次 :
                                    </td>
                                    <td class="bgLBlue" style="width: 350px">
                                        <asp:DropDownList ID="ddl_AllDeals_2" runat="server" Width="400">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="bgLOrange" style="width: 40px">
                                        <span class='<%="bid"+this.ClientID %>'>Bid :</span>
                                    </td>
                                    <td class="bgLBlue">
                                        <asp:TextBox ID="tbx_Bid_2" runat="server" Width="250"></asp:TextBox>
                                        <asp:Button ID="btn_SearchBid_2" runat="server" Text="搜尋" OnClick="SearchBid" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="bgLOrange" style="width: 100px">標題(APP標) :<br />
                                        <asp:Label ID="lab_Limit_2" runat="server"></asp:Label>
                                    </td>
                                    <td colspan="3" class="bgLBlue">
                                        <table>
                                            <tr>
                                                <td id='<%="td"+ddl_AllDeals_2.ClientID %>'>
                                                    <asp:TextBox ID="tbx_Title_2" runat="server" Width="750"></asp:TextBox>
                                                    <asp:Image ID="img_Valid_2" runat="server" Height="21" />
                                                </td>
                                                <td>
                                                    <div id='<%="div"+tbx_Title_2.ClientID %>'>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <asp:Panel ID="pan_3" runat="server">
                            <table style="background-color: gray; width: 950px">
                                <tr>
                                    <td class="title1" colspan="4">Deal 3
                                    </td>
                                </tr>
                                <tr>
                                    <td class="bgLOrange" style="width: 100px">檔次 :
                                    </td>
                                    <td class="bgLBlue" style="width: 350px">
                                        <asp:DropDownList ID="ddl_AllDeals_3" runat="server" Width="400">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="bgLOrange" style="width: 40px">
                                        <span class='<%="bid"+this.ClientID %>'>Bid :</span>
                                    </td>
                                    <td class="bgLBlue">
                                        <asp:TextBox ID="tbx_Bid_3" runat="server" Width="250"></asp:TextBox>
                                        <asp:Button ID="btn_SearchBid_3" runat="server" Text="搜尋" OnClick="SearchBid" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="bgLOrange" style="width: 100px">標題(APP標) :<br />
                                        <asp:Label ID="lab_Limit_3" runat="server"></asp:Label>
                                    </td>
                                    <td colspan="3" class="bgLBlue">
                                        <table>
                                            <tr>
                                                <td id='<%="td"+ddl_AllDeals_3.ClientID %>'>
                                                    <asp:TextBox ID="tbx_Title_3" runat="server" Width="750"></asp:TextBox>
                                                    <asp:Image ID="img_Valid_3" runat="server" Height="21" />
                                                </td>
                                                <td>
                                                    <div id='<%="div"+tbx_Title_3.ClientID %>'>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <asp:Panel ID="pan_4" runat="server">
                            <table style="background-color: gray; width: 950px">
                                <tr>
                                    <td class="title2" colspan="4">Deal 4
                                    </td>
                                </tr>
                                <tr>
                                    <td class="bgLOrange" style="width: 100px">檔次 :
                                    </td>
                                    <td class="bgLBlue" style="width: 350px">
                                        <asp:DropDownList ID="ddl_AllDeals_4" runat="server" Width="400">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="bgLOrange" style="width: 40px">
                                        <span class='<%="bid"+this.ClientID %>'>Bid :</span>
                                    </td>
                                    <td class="bgLBlue">
                                        <asp:TextBox ID="tbx_Bid_4" runat="server" Width="250"></asp:TextBox>
                                        <asp:Button ID="btn_SearchBid_4" runat="server" Text="搜尋" OnClick="SearchBid" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="bgLOrange" style="width: 100px">標題(APP標) :<br />
                                        <asp:Label ID="lab_Limit_4" runat="server"></asp:Label>
                                    </td>
                                    <td colspan="3" class="bgLBlue">
                                        <table>
                                            <tr>
                                                <td id='<%="td"+ddl_AllDeals_4.ClientID %>'>
                                                    <asp:TextBox ID="tbx_Title_4" runat="server" Width="750"></asp:TextBox>
                                                    <asp:Image ID="img_Valid_4" runat="server" Height="21" />
                                                </td>
                                                <td>
                                                    <div id='<%="div"+tbx_Title_4.ClientID %>'>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <asp:Panel ID="pan_5" runat="server">
                            <table style="background-color: gray; width: 950px">
                                <tr>
                                    <td class="title1" colspan="4">Deal 5
                                    </td>
                                </tr>
                                <tr>
                                    <td class="bgLOrange" style="width: 100px">檔次 :
                                    </td>
                                    <td class="bgLBlue" style="width: 350px">
                                        <asp:DropDownList ID="ddl_AllDeals_5" runat="server" Width="400">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="bgLOrange" style="width: 40px">Bid :
                                    </td>
                                    <td class="bgLBlue">
                                        <asp:TextBox ID="tbx_Bid_5" runat="server" Width="250"></asp:TextBox>
                                        <asp:Button ID="btn_SearchBid_5" runat="server" Text="搜尋" OnClick="SearchBid" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="bgLOrange" style="width: 100px">標題(APP標) :<br />
                                        <asp:Label ID="lab_Limit_5" runat="server"></asp:Label>
                                    </td>
                                    <td colspan="3" class="bgLBlue">
                                        <table>
                                            <tr>
                                                <td id='<%="td"+ddl_AllDeals_5.ClientID %>'>
                                                    <asp:TextBox ID="tbx_Title_5" runat="server" Width="750"></asp:TextBox>
                                                    <asp:Image ID="img_Valid_5" runat="server" Height="21" />
                                                </td>
                                                <td>
                                                    <div id='<%="div"+tbx_Title_5.ClientID %>'>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <asp:Panel ID="pan_6" runat="server">
                            <table style="background-color: gray; width: 950px">
                                <tr>
                                    <td class="title2" colspan="4">Deal 6
                                    </td>
                                </tr>
                                <tr>
                                    <td class="bgLOrange" style="width: 100px">檔次 :
                                    </td>
                                    <td class="bgLBlue" style="width: 350px">
                                        <asp:DropDownList ID="ddl_AllDeals_6" runat="server" Width="400">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="bgLOrange" style="width: 40px">
                                        <span class='<%="bid"+this.ClientID %>'>Bid :</span>
                                    </td>
                                    <td class="bgLBlue">
                                        <asp:TextBox ID="tbx_Bid_6" runat="server" Width="250"></asp:TextBox>
                                        <asp:Button ID="btn_SearchBid_6" runat="server" Text="搜尋" OnClick="SearchBid" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="bgLOrange" style="width: 100px">標題(APP標) :<br />
                                        <asp:Label ID="lab_Limit_6" runat="server"></asp:Label>
                                    </td>
                                    <td colspan="3" class="bgLBlue">
                                        <table>
                                            <tr>
                                                <td id='<%="td"+ddl_AllDeals_6.ClientID %>'>
                                                    <asp:TextBox ID="tbx_Title_6" runat="server" Width="750"></asp:TextBox>
                                                    <asp:Image ID="img_Valid_6" runat="server" Height="21" />
                                                </td>
                                                <td>
                                                    <div id='<%="div"+tbx_Title_6.ClientID %>'>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <asp:Panel ID="pan_7" runat="server">
                            <table style="background-color: gray; width: 950px">
                                <tr>
                                    <td class="title1" colspan="4">Deal 7
                                    </td>
                                </tr>
                                <tr>
                                    <td class="bgLOrange" style="width: 100px">檔次 :
                                    </td>
                                    <td class="bgLBlue" style="width: 350px">
                                        <asp:DropDownList ID="ddl_AllDeals_7" runat="server" Width="400">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="bgLOrange" style="width: 40px">
                                        <span class='<%="bid"+this.ClientID %>'>Bid :</span>
                                    </td>
                                    <td class="bgLBlue">
                                        <asp:TextBox ID="tbx_Bid_7" runat="server" Width="250"></asp:TextBox>
                                        <asp:Button ID="btn_SearchBid_7" runat="server" Text="搜尋" OnClick="SearchBid" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="bgLOrange" style="width: 100px">標題(APP標) :<br />
                                        <asp:Label ID="lab_Limit_7" runat="server"></asp:Label>
                                    </td>
                                    <td colspan="3" class="bgLBlue">
                                        <table>
                                            <tr>
                                                <td id='<%="td"+ddl_AllDeals_7.ClientID %>'>
                                                    <asp:TextBox ID="tbx_Title_7" runat="server" Width="750"></asp:TextBox>
                                                    <asp:Image ID="img_Valid_7" runat="server" Height="21" />
                                                </td>
                                                <td>
                                                    <div id='<%="div"+tbx_Title_7.ClientID %>'>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <asp:Panel ID="pan_8" runat="server">
                            <table style="background-color: gray; width: 950px">
                                <tr>
                                    <td class="title2" colspan="4">Deal 8
                                    </td>
                                </tr>
                                <tr>
                                    <td class="bgLOrange" style="width: 100px">檔次 :
                                    </td>
                                    <td class="bgLBlue" style="width: 350px">
                                        <asp:DropDownList ID="ddl_AllDeals_8" runat="server" Width="400">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="bgLOrange" style="width: 40px">
                                        <span class='<%="bid"+this.ClientID %>'>Bid :</span>
                                    </td>
                                    <td class="bgLBlue">
                                        <asp:TextBox ID="tbx_Bid_8" runat="server" Width="250"></asp:TextBox>
                                        <asp:Button ID="btn_SearchBid_8" runat="server" Text="搜尋" OnClick="SearchBid" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="bgLOrange" style="width: 100px">標題(APP標) :<br />
                                        <asp:Label ID="lab_Limit_8" runat="server"></asp:Label>
                                    </td>
                                    <td colspan="3" class="bgLBlue">
                                        <table>
                                            <tr>
                                                <td id='<%="td"+ddl_AllDeals_8.ClientID %>'>
                                                    <asp:TextBox ID="tbx_Title_8" runat="server" Width="750"></asp:TextBox>
                                                    <asp:Image ID="img_Valid_8" runat="server" Height="21" />
                                                </td>
                                                <td>
                                                    <div id='<%="div"+tbx_Title_8.ClientID %>'>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <asp:Panel ID="pan_9" runat="server">
                            <table style="background-color: gray; width: 950px">
                                <tr>
                                    <td class="title1" colspan="4">Deal 9
                                    </td>
                                </tr>
                                <tr>
                                    <td class="bgLOrange" style="width: 100px">檔次 :
                                    </td>
                                    <td class="bgLBlue" style="width: 350px">
                                        <asp:DropDownList ID="ddl_AllDeals_9" runat="server" Width="400">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="bgLOrange" style="width: 40px">
                                        <span class='<%="bid"+this.ClientID %>'>Bid :</span>
                                    </td>
                                    <td class="bgLBlue">
                                        <asp:TextBox ID="tbx_Bid_9" runat="server" Width="250"></asp:TextBox>
                                        <asp:Button ID="btn_SearchBid_9" runat="server" Text="搜尋" OnClick="SearchBid" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="bgLOrange" style="width: 100px">標題(APP標) :<br />
                                        <asp:Label ID="lab_Limit_9" runat="server"></asp:Label>
                                    </td>
                                    <td colspan="3" class="bgLBlue">
                                        <table>
                                            <tr>
                                                <td id='<%="td"+ddl_AllDeals_9.ClientID %>'>
                                                    <asp:TextBox ID="tbx_Title_9" runat="server" Width="750"></asp:TextBox>
                                                    <asp:Image ID="img_Valid_9" runat="server" Height="21" />
                                                </td>
                                                <td>
                                                    <div id='<%="div"+tbx_Title_9.ClientID %>'>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <asp:Panel ID="pan_10" runat="server">
                            <table style="background-color: gray; width: 950px">
                                <tr>
                                    <td class="title2" colspan="4">Deal 10
                                    </td>
                                </tr>
                                <tr>
                                    <td class="bgLOrange" style="width: 100px">檔次 :
                                    </td>
                                    <td class="bgLBlue" style="width: 350px">
                                        <asp:DropDownList ID="ddl_AllDeals_10" runat="server" Width="400">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="bgLOrange" style="width: 40px">
                                        <span class='<%="bid"+this.ClientID %>'>Bid :</span>
                                    </td>
                                    <td class="bgLBlue">
                                        <asp:TextBox ID="tbx_Bid_10" runat="server" Width="250"></asp:TextBox>
                                        <asp:Button ID="btn_SearchBid_10" runat="server" Text="搜尋" OnClick="SearchBid" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="bgLOrange" style="width: 100px">標題(APP標) :<br />
                                        <asp:Label ID="lab_Limit_10" runat="server"></asp:Label>
                                    </td>
                                    <td colspan="3" class="bgLBlue">
                                        <table>
                                            <tr>
                                                <td id='<%="td"+ddl_AllDeals_10.ClientID %>'>
                                                    <asp:TextBox ID="tbx_Title_10" runat="server" Width="750"></asp:TextBox>
                                                    <asp:Image ID="img_Valid_10" runat="server" Height="21" />
                                                </td>
                                                <td>
                                                    <div id='<%="div"+tbx_Title_10.ClientID %>'>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <asp:Panel ID="pan_11" runat="server">
                            <table style="background-color: gray; width: 950px">
                                <tr>
                                    <td class="title1" colspan="4">Deal 11
                                    </td>
                                </tr>
                                <tr>
                                    <td class="bgLOrange" style="width: 100px">檔次 :
                                    </td>
                                    <td class="bgLBlue" style="width: 350px">
                                        <asp:DropDownList ID="ddl_AllDeals_11" runat="server" Width="400">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="bgLOrange" style="width: 40px">
                                        <span class='<%="bid"+this.ClientID %>'>Bid :</span>
                                    </td>
                                    <td class="bgLBlue">
                                        <asp:TextBox ID="tbx_Bid_11" runat="server" Width="250"></asp:TextBox>
                                        <asp:Button ID="btn_SearchBid_11" runat="server" Text="搜尋" OnClick="SearchBid" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="bgLOrange" style="width: 100px">標題(APP標) :<br />
                                        <asp:Label ID="lab_Limit_11" runat="server"></asp:Label>
                                    </td>
                                    <td colspan="3" class="bgLBlue">
                                        <table>
                                            <tr>
                                                <td id='<%="td"+ddl_AllDeals_11.ClientID %>'>
                                                    <asp:TextBox ID="tbx_Title_11" runat="server" Width="750"></asp:TextBox>
                                                    <asp:Image ID="img_Valid_11" runat="server" Height="21" />
                                                </td>
                                                <td>
                                                    <div id='<%="div"+tbx_Title_11.ClientID %>'>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <asp:Panel ID="pan_12" runat="server">
                            <table style="background-color: gray; width: 950px">
                                <tr>
                                    <td class="title2" colspan="4">Deal 12
                                    </td>
                                </tr>
                                <tr>
                                    <td class="bgLOrange" style="width: 100px">檔次 :
                                    </td>
                                    <td class="bgLBlue" style="width: 350px">
                                        <asp:DropDownList ID="ddl_AllDeals_12" runat="server" Width="400">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="bgLOrange" style="width: 40px">
                                        <span class='<%="bid"+this.ClientID %>'>Bid :</span>
                                    </td>
                                    <td class="bgLBlue">
                                        <asp:TextBox ID="tbx_Bid_12" runat="server" Width="250"></asp:TextBox>
                                        <asp:Button ID="btn_SearchBid_12" runat="server" Text="搜尋" OnClick="SearchBid" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="bgLOrange" style="width: 100px">標題(APP標) :<br />
                                        <asp:Label ID="lab_Limit_12" runat="server"></asp:Label>
                                    </td>
                                    <td colspan="3" class="bgLBlue">
                                        <table>
                                            <tr>
                                                <td id='<%="td"+ddl_AllDeals_12.ClientID %>'>
                                                    <asp:TextBox ID="tbx_Title_12" runat="server" Width="750"></asp:TextBox>
                                                    <asp:Image ID="img_Valid_12" runat="server" Height="21" />
                                                </td>
                                                <td>
                                                    <div id='<%="div"+tbx_Title_12.ClientID %>'>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                </tr>
            </table>
        </fieldset>
    </ContentTemplate>
</asp:UpdatePanel>

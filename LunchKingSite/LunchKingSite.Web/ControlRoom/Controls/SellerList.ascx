<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SellerList.ascx.cs"
    Inherits="LunchKingSite.Web.ControlRoom.Controls.SellerList" %>
<%@ Register Src="Pager.ascx" TagName="Pager" TagPrefix="uc1" %>
<asp:Panel ID="divSearchPanel" runat="server" DefaultButton="bSearchBtn">
    <table>
        <tr>
            <td>
                <asp:DropDownList ID="SearchDropDownList" runat="server" DataTextField="Value" DataValueField="Key">
                </asp:DropDownList>
            </td>
            <td>
                <asp:TextBox ID="tbSearch" runat="server"></asp:TextBox>
            </td>
            <td>
                <asp:Button ID="bSearchBtn" runat="server" OnClick="bSearchBtn_Click" Text="搜尋" />
            </td>
            <td>
                <asp:RadioButtonList ID="rbl" runat="server" DataTextField="value" DataValueField="key"
                    Font-Size="10pt" RepeatDirection="Horizontal" Visible="False">
                </asp:RadioButtonList>
            </td>
            <td>
                顯示筆數
            </td>
            <td>
                <asp:DropDownList ID="ddlPageSize" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
                    <asp:ListItem Value="15">15筆</asp:ListItem>
                    <asp:ListItem Value="50">50筆</asp:ListItem>
                    <asp:ListItem Value="100">100筆</asp:ListItem>
                    <asp:ListItem Value="200">200筆</asp:ListItem>
                    <asp:ListItem Value="400">400筆</asp:ListItem>
                    <asp:ListItem Value="800">800筆</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
    </table>
</asp:Panel>
<asp:GridView ID="gvSeller" runat="server" AllowSorting="True" AutoGenerateColumns="False"
    CellPadding="2" DataKeyNames="GUID" EmptyDataText="無符合的資料" Font-Size="10pt" ForeColor="Black"
    GridLines="None" OnSorting="gvSeller_Sorting" Width="100%" BackColor="LightGoldenrodYellow"
    BorderColor="Tan" PageSize="15" OnRowDataBound="BindCloseDownData">
    <FooterStyle BackColor="Tan" />
    <Columns>
        <asp:TemplateField HeaderText="選擇">
            <%--0--%>
            <ItemTemplate>
                <asp:CheckBox ID="bcb" runat="server" />
            </ItemTemplate>
            <HeaderTemplate>
                <asp:CheckBox ID="cbSelectAll" runat="server" AutoPostBack="True" OnCheckedChanged="cbSelectAll_CheckedChanged"
                    Text="全選" />
            </HeaderTemplate>
            <HeaderStyle Wrap="False" />
            <ItemStyle Wrap="False" />
        </asp:TemplateField>
        <asp:TemplateField HeaderText="館別">
            <ItemTemplate>
             <%# ReSetDepartment(Eval("Department")) %>
            </ItemTemplate>
            <HeaderStyle Wrap="False" />
            <ItemStyle Wrap="False" />
        </asp:TemplateField>
        <asp:BoundField HeaderText="賣家編號    " DataField="SellerId">
            <%--4--%>
            <HeaderStyle Wrap="False" />
            <ItemStyle Wrap="False" />
        </asp:BoundField>
        <asp:TemplateField HeaderText="賣家名稱">
            <%--1--%>
            <ItemTemplate>
                <asp:HyperLink ID="hlName" runat="server" NavigateUrl='<%# Eval("Guid", "~/Controlroom//Seller/seller_add.aspx?sid={0}&thin=1") %>'
                    Text='<%# Eval("SellerName") %>'></asp:HyperLink>
            </ItemTemplate>
            <HeaderStyle Wrap="False" />
            <ItemStyle Wrap="False" />
        </asp:TemplateField>
        <asp:TemplateField HeaderText="上層賣家名稱">
            <ItemTemplate>
                <asp:HyperLink ID="parentSellerLink" runat="server"></asp:HyperLink>
            </ItemTemplate>
            <HeaderStyle Wrap="False" />
            <ItemStyle Wrap="False" />
        </asp:TemplateField>
        <asp:BoundField DataField="CompanyName" HeaderText="簽約公司名稱" />
        <asp:BoundField DataField="SignCompanyID" HeaderText="簽約公司ID/統一編號" />
        <asp:TemplateField HeaderText="賣家狀態">
            <ItemTemplate>
                <asp:Literal runat="server" Text='<%# bool.Parse(Eval("IsCloseDown").ToString()) ? "結束營業  " : "" %>' />
                <asp:Literal runat="server" ID="uiStoresCloseDown" />
            </ItemTemplate>
            <HeaderStyle Width="180px" />
        </asp:TemplateField>
        <asp:TemplateField HeaderText="完整設定">
            <%--2--%>
            <ItemTemplate>
                <asp:HyperLink ID="hlComplete" runat="server" NavigateUrl='<%# Eval("Guid", "~/Controlroom//Seller/seller_add.aspx?sid={0}") %>'
                    Text='完整設定'></asp:HyperLink>
            </ItemTemplate>
            <HeaderStyle Wrap="False" />
            <ItemStyle Wrap="False" />
        </asp:TemplateField>
        <asp:BoundField HeaderText="區域  " DataField="SellerRemark" Visible="False">
            <%--3--%>
            <HeaderStyle Wrap="False" />
            <ItemStyle Wrap="False" />
        </asp:BoundField>
        <asp:BoundField DataField="SellerTel" HeaderText="電話" Visible="False">
            <%--5--%>
            <HeaderStyle Wrap="False" />
            <ItemStyle Wrap="False" />
        </asp:BoundField>
        <asp:BoundField DataField="SellerFax" HeaderText="傳真" Visible="False">
            <%--6--%>
            <HeaderStyle Wrap="False" />
            <ItemStyle Wrap="False" />
        </asp:BoundField>
        <asp:TemplateField HeaderText="上線" Visible="False">
            <%--7--%>
            <ItemTemplate>
                <asp:Label ID="lbSellerStatus" runat="server" Text='<%# ReSetSellerStatus(Eval("SellerStatus")) %>'></asp:Label>
            </ItemTemplate>
            <HeaderStyle Wrap="False" />
            <ItemStyle Wrap="False" />
        </asp:TemplateField>
        <asp:BoundField DataField="CreateTime" DataFormatString="{0:yyyy/MM/dd HH:mm:ss}"
            HeaderText="建立時間">
            <%--8--%>
            <HeaderStyle Wrap="False" />
            <ItemStyle Wrap="False" />
        </asp:BoundField>
        <asp:BoundField DataField="ModifyTime" DataFormatString="{0:yyyy/MM/dd HH:mm:ss}"
            HeaderText="最後修改時間">
            <%--    9--%>
            <HeaderStyle Wrap="False" />
            <ItemStyle Wrap="False" />
        </asp:BoundField>
        <asp:TemplateField HeaderText="連鎖顯示名稱">
            <%--10--%>
            <ItemTemplate>
                <asp:TextBox ID="tbDisplayName" runat="server"></asp:TextBox>
            </ItemTemplate>
            <HeaderStyle Wrap="False" />
            <ItemStyle Wrap="False" />
        </asp:TemplateField>
    </Columns>
    <SelectedRowStyle BackColor="DarkSlateBlue" ForeColor="GhostWhite" />
    <PagerStyle BackColor="PaleGoldenrod" ForeColor="DarkSlateBlue" HorizontalAlign="Center" />
    <HeaderStyle BackColor="Tan" Font-Bold="True" Wrap="False" />
    <AlternatingRowStyle BackColor="PaleGoldenrod" />
</asp:GridView>
<table width="100%">
    <tr>
        <td>
            <uc1:Pager ID="gridPager" runat="server" PageSize="15" OnGetCount="RetrieveDataCount"
                OnUpdate="UpdateHandler" />
        </td>
    </tr>
</table>
&nbsp; 
using System.ComponentModel;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.Core;
using LunchKingSite.Core.UI;

namespace LunchKingSite.Web.ControlRoom.Controls
{
    public partial class AddressInputBox : BaseUserControl
    {
        /// <summary>
        /// The address of the building for the input box
        /// </summary>
        public string BuildingAddress { get; set; }

        public ZoneType BuildingZoneType { get; set; }

        /// <summary>
        /// Preset address, the control will fill text boxes according to this string
        /// </summary>
        public string PresetAddress { get; set; }

        /// <summary>
        /// City name of the building
        /// </summary>
        public string CityName
        {
            get { return (string)ViewState["cn"]; }
            set { ViewState["cn"] = value; }
        }

        /// <summary>
        /// It will return the street name portion of the building address
        /// </summary>
        public string StreetName
        {
            get { return lsn.Text; }
        }

        /// <summary>
        /// It will return the formatted full address according to the input from the user to this control
        /// </summary>
        public string FullAddress
        {
            get { return lsn.Text + tbAddr.Text.Trim(); }
        }

        public string ValidationGroup
        {
            get { return v1.ValidationGroup; }
            set { v1.ValidationGroup = value; }
        }

        public ValidatorDisplay ValidationDisplay
        {
            get { return v1.Display; }
            set { v1.Display = value; }
        }

        public string ValidationMessage
        {
            get { return v1.ErrorMessage; }
            set { v1.ErrorMessage = value; }
        }
        public string ValidationCssclass
        {
            get { return v1.CssClass; }
            set { v1.CssClass = value; }
        }

        public bool ShowStreetName
        {
            get { return lsn.Visible; }
            set { lsn.Visible = value; }
        }

        public Unit Width
        {
            get { return tbAddr.Width; }
            set { tbAddr.Width = value; }
        }

        public string CssClass
        {
            get { return tbAddr.CssClass; }
            set { tbAddr.CssClass = value; }
        }
        /// <summary>
        /// This function will setup the control. It should be called to update the control display
        /// </summary>
        public override void DataBind()
        {
            if (string.IsNullOrEmpty(BuildingAddress))
                return;

            BuildingAddress = BuildingAddress.Trim();
            int i = BuildingAddress.IndexOf(' ');
            if (i > 0)
                BuildingAddress = BuildingAddress.Substring(0, i);
            lsn.Text = (BuildingZoneType != ZoneType.Company ? CityName : string.Empty);
            if (string.IsNullOrEmpty(PresetAddress))
                return;

            // now fill the content according to the preset value
            tbAddr.Text = PresetAddress.Replace(CityName, string.Empty);
            /*
            Regex regex = new Regex(@".*[段|街|路]((?<ln>\d+)巷)*((?<an>\d+)弄)*(?<sn>\d+((-|之)*\d+)*)號(之(?<son>\d+))*,* *((?<fn>[\d\w]+)(樓|F))*(之(?<of>[\w\d-]+))*,* *((?<rn>.+)室)*", RegexOptions.Compiled);
            Match m = regex.Match(_preAddr);
            if (m.Groups["ln"].Success)
                tbL.Text = m.Groups["ln"].Value;
            if (m.Groups["an"].Success)
                tbA.Text = m.Groups["an"].Value;
            if (m.Groups["sn"].Success) // address number
            {
                string num = m.Groups["sn"].Value;
                num = num.Replace("號", "").Replace("之", "-");
                tbAN.Text = num;
            }
            if (m.Groups["son"].Success) // address number of
                tbANO.Text = m.Groups["son"].Value;
            if (m.Groups["fn"].Success) // floor number
                tbFloor.Text = m.Groups["fn"].Value;
            if (m.Groups["of"].Success) // floor sub address number
                tbFloorSub.Text = m.Groups["of"].Value;
            if (m.Groups["rn"].Success) // room number
                tbRoom.Text = m.Groups["rn"].Value;
            */
        }

        public void ClearAddress()
        {
            tbAddr.Text = string.Empty;
        }
    }
}
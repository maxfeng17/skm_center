<%@ Control Language="C#" AutoEventWireup="true" Codebehind="Pager.ascx.cs" Inherits="LunchKingSite.Web.ControlRoom.Controls.Pager" %>

<table border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td align="center">
            <table border="0" align="center" cellpadding="1" cellspacing="1"style="white-space: nowrap; word-break: keep-all">
                <tr align="center" valign="middle">
                    <td>
                        
                            <asp:ImageButton ID="btnFirst" OnClick="pager_Click" runat="server" ImageUrl="~/Themes/default/images/17Life/A8/A8_Icon_01.jpg"
                                Text="<< 第一頁" CommandArgument="First"></asp:ImageButton>
                    </td>
                    <td>
                        
                            <asp:LinkButton ID="lFirst" CssClass="ablack4" Font-Bold="true" runat="server" CommandArgument="First" OnClick="link_Click">第一頁</asp:LinkButton>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        <asp:LinkButton ID="lPrev" CssClass="ablack4" Font-Bold="true" runat="server" CommandArgument="Prev" OnClick="link_Click">上一頁</asp:LinkButton>
                    </td>
                    <td>
                        
                            <asp:ImageButton ID="btnPrev" OnClick="pager_Click" runat="server" ImageUrl="~/Themes/default/images/17Life/A8/A8_Icon_02.jpg"
                                Text="< 上一頁" CommandArgument="Prev"></asp:ImageButton>
                    </td>
                    <td>
                        
                            <asp:ImageButton ID="btnNext" OnClick="pager_Click" runat="server" Text="下一頁 >" ImageUrl="~/Themes/default/images/17Life/A8/A8_Icon_03.jpg"
                                CommandArgument="Next"></asp:ImageButton>
                    </td>
                    <td>
                        <asp:LinkButton ID="lNext" CssClass="ablack4" Font-Bold="true" runat="server" CommandArgument="Next" OnClick="link_Click">下一頁</asp:LinkButton>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        <asp:LinkButton ID="lLast" CssClass="ablack4" Font-Bold="true" runat="server" 
                            CommandArgument="Last" OnClick="link_Click">最後頁</asp:LinkButton>
                    </td>
                    <td>
                        
                            <asp:ImageButton ID="btnLast" OnClick="pager_Click" runat="server" Text="最後頁 >>"
                                ImageUrl="~/Themes/default/images/17Life/A8/A8_Icon_04.jpg" CommandArgument="Last">
                            </asp:ImageButton>
                    </td>
                </tr>
            </table>
        </td>
        <td align="center">
            <table border="0" cellspacing="1" cellpadding="1">
                <tr align="center" valign="middle">
                    <td class="ablack4">
                        <b>頁數：</b>
                    </td>
                    <td valign="bottom">
                        <asp:DropDownList ID="ddlPages" runat="server"  OnSelectedIndexChanged="ddlPages_SelectedIndexChanged"
                            AutoPostBack="True">
                        </asp:DropDownList>
                        
                    </td>
                    <td class="ablack4" colspan="2">
                        <asp:Label ID="lblPageCount"  Font-Bold="true" runat="server"></asp:Label>
            &nbsp;&nbsp;(<asp:Label ID="lrc" runat="server" /> 筆資料)
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>






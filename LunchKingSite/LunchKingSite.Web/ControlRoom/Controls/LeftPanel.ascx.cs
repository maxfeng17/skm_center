using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.Web.ControlRoom.Controls
{
    public partial class LeftPanel : System.Web.UI.UserControl
    {
        private ISysConfProvider config;

        private string parentNode = @"<div id='#id#' class='collapsePanelHeader'><div style='padding:5px;cursor:pointer;vertical-align:middle;'>
                                            <div style='float:left;'>#parent_name#</div><div style='float:right;vertical-align:middle;'>
                                            <input type='image' src='#siteurl#/images/icons/expand.jpg' alt='(展開...)' style='border-width:0px;'></div></div></div>";

        private const string childNode = @"<div class='collapsePanel' style='white-space:nowrap;'>#child_nodes#</div>";

        protected void Page_Load(object sender, EventArgs e)
        {
            config = ProviderFactory.Instance().GetConfig();
            parentNode = parentNode.Replace("#siteurl#", config.SiteUrl);

            if (config.EnablePageAccessControl)
            {
                // 取出路徑包含controlroom，並排除父權限名稱為空者
                IEnumerable<FunctionPrivilegeData> privileges = FunctionPrivilegeManager.GetFunctionPrivilegeDataById(Page.User.Identity.Name)
                    .Where(x => !string.IsNullOrWhiteSpace(x.ParentFunction));

                StringBuilder menu = new StringBuilder();
                StringBuilder list;
                foreach (IGrouping<KeyValuePair<int, string>, FunctionPrivilegeData> dataGroup in privileges.Where(x => x.Visible).GroupBy(x => new KeyValuePair<int, string>(x.ParentSortOrder, x.ParentFunction)).OrderBy(y => y.Key.Key))
                {
                    // add parent node
                    menu.Append(parentNode.Replace("#parent_name#", dataGroup.Key.Value).Replace("#id#",
                        Convert.ToBase64String(Encoding.UTF8.GetBytes(dataGroup.Key.Value))));

                    // add child nodes
                    list = new StringBuilder();
                    foreach (FunctionPrivilegeData data in dataGroup.OrderBy(x => x.SortOrder))
                    {
                        list.Append("<a href='" + config.SiteUrl + data.Link + "'>" + data.DisplayName + "</a><br>");
                    }
                    menu.Append(childNode.Replace("#child_nodes#", list.ToString()));
                }

                // append to page
                RestrictFunctions.InnerHtml = menu.ToString();

                RestrictFunctions.Visible = true;
                FullFunctions.Visible = false;
            }
            else
            {
                RestrictFunctions.Visible = false;
                FullFunctions.Visible = true;
            }
        }
    }
}
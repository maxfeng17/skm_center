﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EinvoiceFooterControl.ascx.cs" Inherits="LunchKingSite.Web.ControlRoom.Controls.EinvoiceFooterControl" %>
<asp:MultiView ID="muView" runat="server">
    <asp:View runat="server" ID="viewLkSiteOld">
        <div class="OrDetinvoicedetail">
        <ol style="margin-top:12px">
            <li>依財政部令此副本僅供參考，不可直接兌獎，系統會自動對獎並寄出中獎發票。</li>
            <li>根據財政部令台財稅字第0952400194號「<a href="<%=SysConf.InvoiceKeyPointUrl%> %>"
                target="_blank">電子發票實施作業要點</a>」，我們將為您的發票檔案儲存至您的訂單資料中，並上傳至「電子發票整合服務平台」。</li>
            <li>有關發票問題請見說明：<a href="<%=SysConf.SiteUrl %>/ppon/newbieguide.aspx?s=88&q=89" target="_blank">17Life如何保存發票及對獎</a>、<a
                href="https://www.einvoice.nat.gov.tw/wSite/mp?mp=1" target="_blank">電子發票整合服務平台</a>。</li>
        </ol>
            </div>
    </asp:View>
    <asp:View runat="server" ID="viewLkSiteCarrierEra">
        <div class="OrDetinvoicedetail">
        <ol style="margin-top:12px">
            <li>依財政部令此副本僅供參考，不可直接兌獎。</li>
            <li>使用會員載具（未歸戶），17Life將依財政部提供之名單，主動進行中獎通知以及中獎發票交付。</li>
            <li>使用手機條碼以及自然人憑證條碼載具，或已歸戶者，由財政部通知中獎。消費者可至Kiosk列印中獎發票，或由財政部通知，並自動匯款。</li>
            <li>Kiosk列舉: 7-11的ibon; 全家的FarmiPort; 萊爾富的Life-ET。</li>
            <li>有關發票詳細說明:
                <a href="https://www.einvoice.nat.gov.tw/APMEMBERVAN/GeneralCarrier/generalCarrier?CSRT=5861599567505696271" target="_blank">電子發票整合服務平台
                </a>、
                <a href="https://www.einvoice.nat.gov.tw/home/DownLoad?fileName=1377486728934_0.pdf" target="_blank">電子發票實施作業要點
                </a>。
            </li>
        </ol>
            </div>
    </asp:View>
    <asp:View runat="server" ID="viewPiinlifeOld">
        <p>
            ※依財政部令此副本僅供參考，不可直接兌獎，系統會自動對獎並寄出中獎發票。<br />
            ※根據財政部令台財稅字第0952400194號「<a href="<%=SysConf.InvoiceKeyPointUrl%>"
                target="_blank">電子發票實施作業要點</a>」，我們將為您的<br>
            &nbsp;&nbsp;&nbsp;&nbsp;發票檔案儲存至您的訂單資料中，並上傳至「電子發票整合服務平台」。<br />
            ※有關發票問題請見說明：<a href="../InvoiceHosting.aspx" target="_blank">電子發票</a>、<a href="https://www.einvoice.nat.gov.tw/wSite/mp?mp=1"
                target="_blank">電子發票整合服務平台</a>。
        </p>
    </asp:View>
    <asp:View runat="server" ID="viewPiinlifeCarrierEra">
    <p>
※依財政部令此副本僅供參考，不可直接兌獎。<br />
※使用會員載具(未歸戶)，17Life會自動進行中獎通知以及中獎發票交付。<br />
※使用手機條碼以及自然人憑證條碼載具，或已歸戶者，由財政部通知中獎。<br />
　消費者可至Kiosk列印中獎發票，或由財政部通知，並自動匯款。<br />
※Kiosk列舉: 7-11的ibon; 全家的FarmiPort; 萊爾富的Life-ET。<br />
※有關發票詳細說明:
          <a href="https://www.einvoice.nat.gov.tw/APMEMBERVAN/GeneralCarrier/generalCarrier?CSRT=5861599567505696271" target="_blank">
            電子發票整合服務平台
          </a>、
          <a href="https://www.einvoice.nat.gov.tw/home/DownLoad?fileName=1377486728934_0.pdf" target="_blank">
            電子發票實施作業要點
          </a>。
    </p>
    </asp:View>
</asp:MultiView>
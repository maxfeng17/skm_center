using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.Core;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;

namespace LunchKingSite.Web.ControlRoom.Controls
{
    public partial class AuditBoard : BaseUserControl, IAuditBoardView
    {
        public event EventHandler<DataEventArgs<AuditEntry>> AddClick;
        public event EventHandler<DataEventArgs<KeyValuePair<int, bool>>> EditClick;

        #region props
        private AuditBoardPresenter _presenter;
        public AuditBoardPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        public string ReferenceId 
        {
            get { return (ViewState != null && ViewState["rf"] != null) ? (string)ViewState["rf"] : null; }
            set { ViewState["rf"] = value; }
        }

        public AuditType ReferenceType 
        {
            get { return (ViewState != null && ViewState["rt"] != null) ? (AuditType)ViewState["rt"] : AuditType.Order; }
            set { ViewState["rt"] = value; }
        }

        public bool ShowAllRecords 
        {
            get { return gvm.Columns[3].Visible; }
            set { gvm.Columns[2].Visible = gvm.Columns[3].Visible = gvm.Columns[4].Visible = value; }
        }

        public bool EnableAdd 
        {
            get { return pe.Visible; }
            set { pe.Visible = value; }
        }

        public bool HideView
        {
            get { return !this.Visible; }
            set { this.Visible = !value; }
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                this._presenter.OnViewInitialized();
            this._presenter.OnViewLoaded();
        }

        #region interface methods
        public void SetRecordGrid(AuditCollection data)
        {
            gvm.DataSource = data;
            gvm.DataBind();
        }
        #endregion

        protected void bS_Click(object sender, EventArgs e)
        {
            if (this.AddClick != null)
                this.AddClick(null, new DataEventArgs<AuditEntry>(new AuditEntry(Page.User.Identity.Name, tN.Text.Trim(), Convert.ToBoolean(dV.SelectedValue))));
        }

        protected void gvm_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gvm.EditIndex = e.NewEditIndex;
            this._presenter.OnViewInitialized();
        }

        protected void gvm_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gvm.EditIndex = -1;
            this._presenter.OnViewInitialized();
        }

        protected void gvm_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            if (this.EditClick != null)
            {
                gvm.Columns[3].ExtractValuesFromCell(e.NewValues, (DataControlFieldCell)gvm.Rows[e.RowIndex].Cells[3], DataControlRowState.Edit, true);
                this.EditClick(this, new DataEventArgs<KeyValuePair<int, bool>>(new KeyValuePair<int, bool>((int)gvm.DataKeys[e.RowIndex].Value, (bool)e.NewValues[0])));
            }
            gvm.EditIndex = -1;
            this._presenter.OnViewInitialized();
        }
    }
}
﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LunchKingSite.Web.ControlRoom.Ppon
{
    public partial class EdmDetailsSetup : BaseUserControl, IEdmDetailsSetupView
    {
        #region property

        private EdmDetailsSetupPresenter _presenter;

        public EdmDetailsSetupPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        public List<EdmDetail> EdmDetailData { get; set; }

        public List<EdmDetalControls> ItemControlList;

        public ViewPponDealTimeSlotCollection VPDTcollection { get; set; }
        
        //區塊名稱
        public string FieldName { get; set; }


        #region view property

        public int CityId
        {
            get
            {
                int cityid;
                if (int.TryParse(ddl_MD_City.SelectedValue, out cityid))
                {
                    return cityid;
                }
                else
                {
                    return 0;
                }
            }
            set
            {
                ddl_MD_City.SelectedValue = value.ToString();
            }
        }

        public bool IsShowSortType { get; set; }

        public int EdmSetupCityId
        {
            get
            {
                int edmSetupCityId;
                if (int.TryParse(hif_EdmSetupCityId.Value, out edmSetupCityId))
                {
                    return edmSetupCityId;
                }
                else
                {
                    return CityId;
                }
            }
            set { hif_EdmSetupCityId.Value = value.ToString(); }
        }

        public int SortType
        {
            get
            {
                int sortType;
                if (int.TryParse(ddl_MD_SortType.SelectedValue, out sortType))
                {
                    return sortType;
                }
                else
                {
                    return 0;
                }
            }
            set
            {
                ddl_MD_SortType.SelectedValue = value.ToString();
            }
        }

        public DateTime DeliveryDate
        {
            get
            {
                DateTime deliverydate;
                if (DateTime.TryParse(hif_DeliveryDate.Value, out deliverydate))
                {
                    return deliverydate;
                }
                else
                {
                    return DateTime.MaxValue;
                }
            }
            set
            {
                hif_DeliveryDate.Value = value.ToString("yyyy/MM/dd HH:mm");
            }
        }

        public DateTime? DealDate
        {
            get
            {
                DateTime dealdate;
                if (DateTime.TryParse(hif_DealDate.Value, out dealdate))
                {
                    return dealdate;
                }
                else
                {
                    return null;
                }
            }
            set
            {
                if (value.HasValue)
                {
                    hif_DealDate.Value = value.Value.ToString("yyyy/MM/dd HH:mm");
                }
                else
                {
                    hif_DealDate.Value = string.Empty;
                }
            }
        }

        public int Pid
        {
            get
            {
                int pid;
                if (int.TryParse(hif_Pid.Value, out pid))
                {
                    return pid;
                }
                else
                {
                    return 0;
                }
            }
            set
            {
                hif_Pid.Value = value.ToString();
            }
        }

        public bool DisplayCityName
        {
            get
            {
                return cbx_Display_CityName.Checked;
            }
            set
            {
                cbx_Display_CityName.Checked = value;
            }
        }

        public int MainDetailType
        {
            get
            {
                int maindetailtype;
                if (int.TryParse(hif_MainDetailType.Value, out maindetailtype))
                {
                    return maindetailtype;
                }
                else
                {
                    return 0;
                }
            }
            set
            {
                hif_MainDetailType.Value = value.ToString();
            }
        }

        public int MainDetailItemType
        {
            get
            {
                int maindetailitemtype;
                if (int.TryParse(hif_MainDetailItemType.Value, out maindetailitemtype))
                {
                    return maindetailitemtype;
                }
                else
                {
                    return 0;
                }
            }
            set
            {
                hif_MainDetailItemType.Value = value.ToString();
            }
        }

        public int RowNumber
        {
            get
            {
                int rownumber;
                if (int.TryParse(ddl_Row.SelectedValue, out rownumber))
                {
                    return rownumber;
                }
                else
                {
                    return 0;
                }
            }
            set
            {
                ddl_Row.SelectedValue = value.ToString();
            }
        }

        public int ColumnNumber
        {
            get
            {
                int columnnumber;
                if (int.TryParse(ddl_Column.SelectedValue, out columnnumber))
                {
                    return columnnumber;
                }
                else
                {
                    return 0;
                }
            }
            set
            {
                ddl_Column.SelectedValue = value.ToString();
            }
        }

        #endregion view property

        #endregion property

        #region event

        public event EventHandler GetViewPponDealTimeSlotCollection = null;

        public event EventHandler<DataEventArgs<KeyValuePair<Guid, int>>> GetViewPponDealByBid = null;

        #endregion event

        #region method

        //變更區域後下拉rebind
        //P好康
        public void GetViewPponDealTimeSlotCollectionByCityDate(ViewPponDealTimeSlotCollection vpdts)
        {
            VPDTcollection = vpdts;
            int i = 0;
            foreach (EdmDetalControls item in ItemControlList)
            {
                if (string.IsNullOrEmpty(item.Bid.Text))
                {
                    item.Title.Text = string.Empty;
                }

                List<Guid> soldOutList = new List<Guid>();
                VPDTcollection.ForEach(x =>
                {
                    var deal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(x.BusinessHourGuid);
                    if (string.IsNullOrEmpty(x.SubjectName))
                    {
                        x.SubjectName = (deal!=null&&deal.IsLoaded)? ((string.IsNullOrEmpty(deal.AppTitle))?deal.ItemName:deal.AppTitle) :x.EventName;
                    }

                    if (deal.OrderTotalLimit > 0 && deal.OrderedQuantity > 0 && deal.OrderTotalLimit - deal.OrderedQuantity <= 0)
                    {
                        soldOutList.Add(x.BusinessHourGuid);
                    }
                });

                //排除已售完檔次
                var onsaleList = VPDTcollection.Where(x => !soldOutList.Contains(x.BusinessHourGuid)).ToList();

                List<ViewPponDealTimeSlot> ddlList;
                if (onsaleList.Count > 250) //減少預帶檔次
                {
                    ddlList = onsaleList.GetRange(0, 250);
                }
                else
                {
                    ddlList = onsaleList;
                }

                item.AllDeals.DataSource = ddlList;
                item.AllDeals.DataTextField = "ItemName";
                item.AllDeals.DataValueField = "BusinessHourGuid";
                item.AllDeals.DataBind();

                item.AllDeals.SelectedIndex = -1;
                item.Title.Text = "";
                item.Bid.Text = "";
                item.Valid.ImageUrl = ""; //檢查BID和標題是否相符

                if (ddlList.Count > 0 && i < ddlList.Count)
                {
                    item.AllDeals.SelectedIndex = i;
                    if (string.IsNullOrEmpty(item.Bid.Text))
                    {
                        item.Title.Text = item.AllDeals.SelectedItem.Text;
                    }
                }

                if (EdmDetailData != null)
                {
                    var items = EdmDetailData.Where(x => x.Type == MainDetailItemType);
                    if (items.Count() > 0 && i < items.Count())
                    {
                        EdmDetail firstitem = items.Skip(i).FirstOrDefault();
                        item.Title.Text = firstitem.Title;
                        if (ddlList.Any(x => x.BusinessHourGuid == firstitem.Bid))
                        {
                            item.AllDeals.SelectedValue = firstitem.Bid.ToString();
                            if (item.AllDeals.SelectedItem.Text == item.Title.Text)
                            {
                                item.Valid.ImageUrl = "/Themes/default/images/17Life/G3/CheckoutPass.png";
                            }
                            else
                            {
                                item.Valid.ImageUrl = "/Themes/default/images/17Life/G3/CheckoutError.png";
                            }
                        }
                        else
                        {
                            item.AllDeals.SelectedIndex = -1;
                            item.Bid.Text = firstitem.Bid.ToString();
                            
                            //檢查儲存資料Bid和標題是否相符
                            var checkdeal = onsaleList.Where(x => x.BusinessHourGuid == firstitem.Bid).FirstOrDefault();
                            if (checkdeal != null && checkdeal.ItemName == firstitem.Title)
                            {
                                item.Valid.ImageUrl = "/Themes/default/images/17Life/G3/CheckoutPass.png";
                            }
                            else
                            {
                                item.Valid.ImageUrl = "/Themes/default/images/17Life/G3/CheckoutError.png";
                            }
                        }
                    }

                }
                i++;
            }
        }
        
        //輸入bid後回傳的viewppondeal資料
        //P好康
        public void RetrieveViewPponDealByBid(ViewPponDeal deal, int control_sequence)
        {
            if (ItemControlList.Any(x => x.Sequence == control_sequence))
            {
                EdmDetalControls controls = ItemControlList.First(x => x.Sequence == control_sequence);
                if (deal.BusinessHourGuid == Guid.Empty)
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "alert", "alert('查無資料!!');", true);
                    controls.Bid.Text = string.Empty;
                    controls.Title.Text = controls.AllDeals.SelectedItem.Text;
                }
                else
                {
                    controls.Bid.Text = deal.BusinessHourGuid.ToString();
                    controls.Title.Text = (string.IsNullOrEmpty(deal.AppTitle))?deal.ItemName:deal.AppTitle;
                }
                VisibleItems();
            }
        }
        
        #endregion method

        #region page

        protected void Page_Load(object sender, EventArgs e)
        {
            //各小區塊下拉選單、bid輸入、簡介輸入和按鈕
            ItemControlList = new List<EdmDetalControls>() {
                new EdmDetalControls(ddl_AllDeals_1, tbx_Bid_1, tbx_Title_1,btn_SearchBid_1,1,img_Valid_1),
                new EdmDetalControls(ddl_AllDeals_2, tbx_Bid_2, tbx_Title_2,btn_SearchBid_2,2,img_Valid_2),
                new EdmDetalControls(ddl_AllDeals_3, tbx_Bid_3, tbx_Title_3,btn_SearchBid_3,3,img_Valid_3),
                new EdmDetalControls(ddl_AllDeals_4, tbx_Bid_4, tbx_Title_4,btn_SearchBid_4,4,img_Valid_4),
                new EdmDetalControls(ddl_AllDeals_5, tbx_Bid_5, tbx_Title_5,btn_SearchBid_5,5,img_Valid_5),
                new EdmDetalControls(ddl_AllDeals_6, tbx_Bid_6, tbx_Title_6,btn_SearchBid_6,6,img_Valid_6),
                new EdmDetalControls(ddl_AllDeals_7, tbx_Bid_7, tbx_Title_7,btn_SearchBid_7,7,img_Valid_7),
                new EdmDetalControls(ddl_AllDeals_8, tbx_Bid_8, tbx_Title_8,btn_SearchBid_8,8,img_Valid_8),
                new EdmDetalControls(ddl_AllDeals_9, tbx_Bid_9, tbx_Title_9,btn_SearchBid_9,9,img_Valid_9),
                new EdmDetalControls(ddl_AllDeals_10, tbx_Bid_10, tbx_Title_10,btn_SearchBid_10,10,img_Valid_10),
                new EdmDetalControls(ddl_AllDeals_11, tbx_Bid_11, tbx_Title_11,btn_SearchBid_11,11,img_Valid_11),
                new EdmDetalControls(ddl_AllDeals_12, tbx_Bid_12, tbx_Title_12,btn_SearchBid_12,12,img_Valid_12)
            };
            if (!IsPostBack)
            {
                this._presenter.OnViewInitialized();
            }
            this._presenter.OnViewLoaded();
        }

        public override void DataBind()
        {
            EventArgs e = new EventArgs();
            //設定城市區域
            if (GetViewPponDealTimeSlotCollection != null)
            {
                ddl_MD_City.DataSource = PponCityGroup.DefaultPponCityGroup.Where(x => x.CityName != "福利網" && x.CityName != "台北2" && x.CityName != "天貓");
                ddl_MD_City.DataTextField = "CityName";
                ddl_MD_City.DataValueField = "CityId";
                ddl_MD_City.DataBind();
            }
            //主檔
            EdmDetail firstitem;
            if ((firstitem = EdmDetailData.FirstOrDefault(x => x.Type == MainDetailType)) != null)
            {
                ddl_MD_City.SelectedValue = firstitem.CityId.ToString();
                RowNumber = firstitem.RowNumber;
                ColumnNumber = firstitem.ColumnNumber;
                cbx_Display_CityName.Checked = firstitem.DisplayCityName;

            }
            else
            {
                RowNumber = 0;
                ColumnNumber = 0;
            }
            lit_Name.Text = FieldName;
            //依據所選行、欄顯示小區塊
            VisibleItems();

            if (Pid == 0 && firstitem != null)
            {
                SortType = firstitem.SortType.HasValue ? firstitem.SortType.Value : (int) EDMDetailsSortType.BySequence;
            }


            //檔次綁定
            if (GetViewPponDealTimeSlotCollection != null)
            {   
                GetViewPponDealTimeSlotCollection(ddl_MD_City, e);
            }



            ph_MD_SortType.Visible = IsShowSortType;
        }

        protected void ChangeTotal(object sender, EventArgs e)
        {
            VisibleItems();
        }

        protected void ChangeCity(object sender, EventArgs e)
        {
            if (GetViewPponDealTimeSlotCollection != null)
            {
                GetViewPponDealTimeSlotCollection(sender, e);
            }
        }

        protected void ChangeSortType(object sender, EventArgs e)
        {
            if (GetViewPponDealTimeSlotCollection != null)
            {
                GetViewPponDealTimeSlotCollection(sender, e);
            }
        }
        
        protected void SearchBid(object sender, EventArgs e)
        {
            if (sender is Button)
            {
                Button btn = (Button)sender;
                EdmDetalControls controls = ItemControlList.First(x => x.BtnBid == btn);
                if (GetViewPponDealByBid != null && ItemControlList.Any(x => x.BtnBid == btn))
                {
                    Guid bid;
                    if (Guid.TryParse(controls.Bid.Text, out bid))
                    {
                        GetViewPponDealByBid(sender, new DataEventArgs<KeyValuePair<Guid, int>>(new KeyValuePair<Guid, int>(bid, controls.Sequence)));
                        controls.Valid.ImageUrl = "/Themes/default/images/17Life/G3/CheckoutPass.png";
                    }
                    else
                    {
                        controls.Bid.Text = string.Empty;
                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "alertbid", "alert('Bid格式錯誤!!');", true);
                        controls.Valid.ImageUrl = "/Themes/default/images/17Life/G3/CheckoutError.png";
                    }
                }
                else
                {
                    controls.Valid.ImageUrl = "/Themes/default/images/17Life/G3/CheckoutError.png";
                }
            }
        }

        #endregion page

        #region other methods
        
        protected void VisibleItems()
        {
            List<Panel> panel_list = new List<Panel>() { pan_1, pan_2, pan_3, pan_4, pan_5, pan_6, pan_7, pan_8, pan_9, pan_10, pan_11, pan_12 };
            List<Label> lab_list = new List<Label>() { lab_Limit_1, lab_Limit_2, lab_Limit_3, lab_Limit_4, lab_Limit_5, lab_Limit_6, lab_Limit_7, lab_Limit_8, lab_Limit_9, lab_Limit_10, lab_Limit_11, lab_Limit_12 };
            int row, column;
            int tempColumn;
            if (int.TryParse(ddl_Row.SelectedValue, out row) && int.TryParse(ddl_Column.SelectedValue, out column))
            {
                int textlimit = 52;
                if (column == 1)
                {
                    textlimit = 52;
                }
                else if (column == 2)
                {
                    textlimit = 50;
                }
                else if (column == 3)
                {
                    textlimit = 30;
                }
                else if (column == 4)
                {
                    textlimit = 52;
                }

                tempColumn = (column == 4) ? 1 : column;

                for (int i = 1; i <= panel_list.Count; i++)
                {

                    if (i <= row * tempColumn)
                    {
                        panel_list[i - 1].Visible = true;
                        lab_list[i - 1].Text = "頁面字數限制:" + textlimit;
                    }
                    else
                    {
                        panel_list[i - 1].Visible = false;
                        lab_list[i - 1].Text = string.Empty;
                    }
                }
            }
        }

        //回傳設定資料
        public List<EdmDetail> ReturnEdmDetails()
        {
            List<EdmDetail> edmdetail_list = new List<EdmDetail>();
            if (RowNumber * ColumnNumber > 0)
            {
                edmdetail_list.Add(new EdmDetail() { Pid = Pid, Type = MainDetailType, DisplayCityName = DisplayCityName, CityId = CityId, CityName = ddl_MD_City.SelectedItem.Text, RowNumber = RowNumber, ColumnNumber = ColumnNumber ,SortType = SortType});
                int i = 0;
                int tempColumnNumber;
                tempColumnNumber = (ColumnNumber == 4) ? 1 : ColumnNumber;
                foreach (EdmDetalControls controls in ItemControlList)
                {

                    if (i < RowNumber * tempColumnNumber)
                    {
                        Guid bid;
                        if (!string.IsNullOrEmpty(controls.Bid.Text))
                        {
                            if (Guid.TryParse(controls.Bid.Text, out bid))
                            {
                                edmdetail_list.Add(new EdmDetail() { Pid = Pid, Type = MainDetailItemType, Bid = bid, Title = controls.Title.Text, Sequence = i });
                            }
                        }
                        else if (Guid.TryParse(controls.AllDeals.SelectedValue, out bid))
                        {
                            edmdetail_list.Add(new EdmDetail() { Pid = Pid, Type = MainDetailItemType, Bid = bid, Title = controls.Title.Text, Sequence = i });
                        }
                    }
                    i++;
                }
            }
            return edmdetail_list;
        }

        #endregion other methods
    }

    public class EdmDetalControls
    {
        public DropDownList AllDeals { get; set; }

        public TextBox Bid { get; set; }

        public TextBox Title { get; set; }

        public Button BtnBid { get; set; }

        public int Sequence { get; set; }

        public Image Valid { get; set; }

        public EdmDetalControls(DropDownList alldeals, TextBox bid, TextBox title, Button btnbid, int sequence, Image valid)
        {
            AllDeals = alldeals;
            Bid = bid;
            Title = title;
            BtnBid = btnbid;
            Sequence = sequence;
            Valid = valid;
        }
    }

    internal static class UserControlExtensionMethod
    {
        public static List<EdmDetail> GetData(this EdmDetailsSetup usercontrol)
        {
            return usercontrol.ReturnEdmDetails();
        }
    }
}
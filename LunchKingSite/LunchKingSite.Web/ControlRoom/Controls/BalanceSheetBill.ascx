﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BalanceSheetBill.ascx.cs" Inherits="LunchKingSite.Web.ControlRoom.Controls.BalanceSheetBill" %>
    <%-- 單據本身的資料 --%>    
    <asp:HiddenField ID="hidReceiptType" runat="server" ClientIDMode="Static"/>
    <asp:HiddenField ID="hidReceiptTypeOtherName" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hidIsInvoiceTaxRequired" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hidMode" runat="server" />
    <asp:HiddenField ID="hidBillId" runat="server" />     
    <table>
        <tr>
            <td>單據開立方式：</td>
            <td><asp:Label ID="lblVendorReceiptType" runat="server" ></asp:Label></td>
        </tr>
        <tr>
            <td>買  受  人：</td>
            <td>
                <asp:RadioButtonList ID="rbCompanyName" runat="server">
                    <asp:ListItem Value="2" text='康太（24317014）' Selected="True"/>
<%--                    <asp:ListItem Value="1" text='康迅（70553216）' />
                    <asp:ListItem Value="3" text='康迅旅遊（27734293）'/>   --%>
                    <asp:ListItem Value="4" text='康朋（24949807）'/>                        
                </asp:RadioButtonList> 
            </td>
        </tr>
        <tr>
            <td>發票號碼：</td>
            <td>
                <asp:TextBox ID="tbInvoiceNumber" runat="server" ClientIDMode="Static"></asp:TextBox>&nbsp &nbsp
                <asp:Label ID="Label2" runat="server" Text="若為發票對開，請務必變更出帳方式。不可使用ACH。" ForeColor="Red"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>開立人統編：</td>
            <td>
                <asp:TextBox ID="tbInvoiceComId" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>發票日期：</td>
            <td>
                <asp:TextBox ID="tbInvoiceDate" runat="server" CssClass="conditional_datepicker" />              
            </td>
        </tr>
        <tr>
            <td>預計付款日期：</td>
            <td>
                <asp:TextBox ID="tbDefaultPaymentDay" runat="server" CssClass="conditional_datepicker" />              
            </td>
        </tr>
        <tr>
            <td>檢核確認：</td>
            <td>
                <ol style="color:red">
                    <li>買受人與統編資訊</li>
                    <li>品名檔號</li>
                    <li>發票章</li>
                    <li>公司負責人章（收據必需）</li>
                </ol>
            </td>
        </tr>
        <tr>
            <td colspan = "2">-----------------------發票實際金額-----------------------</td>
        </tr>
        <tr>
            <td>未稅金額：</td>
            <td><asp:TextBox ID="tbSubTotal" runat="server" onkeyup="numberCheck(this.value);" ClientIDMode="Static"></asp:TextBox></td>
        </tr>
        <tr>
            <td>稅　　額：</td>
            <td><asp:TextBox ID="tbGstTotal" runat="server" onkeyup="numberCheck(this.value);" ClientIDMode="Static"></asp:TextBox></td>
        </tr>
        <tr>
            <td>發票總額：</td>
            <td><asp:TextBox ID="tbInvoiceTotal" runat="server" onchange="tbInvoiceTotal_onchange()" onkeyup="numberCheck(this.value);" ClientIDMode="Static"></asp:TextBox></td>
        </tr>
        <tr>
            <td>單據備註：</td>
            <td>
                <asp:TextBox ID="tbMemo" runat="server" TextMode="MultiLine" Width="50%"></asp:TextBox>                
            </td>
        </tr>
    </table>
    
    <asp:ListView ID="lvDistribution" runat="server" ItemPlaceholderID="repeatMe" 
        OnItemDataBound="lvDistribution_ItemDataBound">
        <LayoutTemplate>
            <table>
                <tr id="repeatMe" runat="server"/>                
            </table>
        </LayoutTemplate>
        <ItemTemplate>
            <tr>
                <td>
                    <asp:HiddenField ID="BsId" runat="server" Value='<%# ((dynamic)Container.DataItem).BalanceSheetId %>' />
                    <asp:HiddenField ID="BalanceSheetUseType" runat="server" Value='<%# ((dynamic)Container.DataItem).BalanceSheetUseType %>' />
                    <table style="border:1px solid grey">
                        <thead>
                            <tr>
                                <th colspan="2">                                    
                                    <asp:Literal runat="server" 
                                        Text='<%#                                     
                                    string.Format("{0}/{1}{2} - {3} {4}"
                                        , ((dynamic)Container.DataItem).Year
                                        , ((dynamic)Container.DataItem).Month
                                        , ((dynamic)Container.DataItem).Day != null ? "/" + ((dynamic)Container.DataItem).Day : string.Empty
                                        ,((dynamic)Container.DataItem).DealName
                                        ,((dynamic)Container.DataItem).StoreName
                                    ) %>' />
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>未稅金額：</td>
                                <td>
                                    <asp:TextBox ID="DistSubtotal" runat="server" Text='<%# ((dynamic)Container.DataItem).Subtotal %>'                                        
                                        CssClass="subTotalDetail" onkeyup="numberCheck(this.value);" />                                                           
                                </td>
                            </tr>
                            <tr>
                                <td>稅　　額：</td>
                                <td>
                                    <asp:TextBox ID="DistTax" runat="server" Text='<%# ((dynamic)Container.DataItem).Tax %>'                                          
                                        CssClass="gstTotalDetail" onkeyup="numberCheck(this.value);" />                                    
                                </td>                                
                            </tr>
                            <tr>
                                <td>發票總額：</td>
                                <td>
                                    <asp:TextBox ID="DistTotal" runat="server" Text='<%# ((dynamic)Container.DataItem).Total %>'                                         
                                        CssClass="totalDetail" onkeyup="numberCheck(this.value);" />                                    
                                </td>
                            </tr>
                            <tr>
                                <td>對帳單備註：</td>
                                <td><asp:TextBox ID="DistRemark" runat="server" Text='<%# ((dynamic)Container.DataItem).Remark %>' TextMode="MultiLine" Width="100%"/></td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </ItemTemplate>
    </asp:ListView>
    
    <asp:Button ID="btnCancel" runat="server" Text="取消" OnClick="Cancel_Click" OnClientClick="javascript:return confirm('取消將不會儲存此單據資料，您確定要取消此單據的編輯嗎?')" />    
    <asp:Button ID="SaveDistribution" runat="server" Text="儲存" OnClientClick="if (formDataCheck() == false){ return false; }" OnClick="SaveDistribution_Click"/>
    

    <style type="text/css">
        .ui-datepicker { width: 17em; padding: .2em .2em 0; display: none; font-size: 80.5%; }
    </style>
<script type="text/javascript" src="../../Tools/js/jquery.ui.datepicker-zh-TW.js"></script>
<script type="text/javascript">
    window.tax = <%= GetBusinessTax()%>;
    $(document).ready(function () {
        $('.conditional_datepicker:not([readonly])').datepicker();
        $('input[readonly]').css('color', 'gray');
        if($("#<%= tbDefaultPaymentDay.ClientID %>").val()==""){
            getDefaultPaymentDay();
        }
        //當開立方式為收據,未稅金額由系統帶入總額的數字
        $(".totalDetail").change(function () {
            var receiptType = $("#<%= hidReceiptType.ClientID %>").val();
            var invoiceTaxRequired = $("#<%= hidIsInvoiceTaxRequired.ClientID %>").val();
            var hasTax = receiptType === "2" && invoiceTaxRequired === "True";
            var src = this;
            var target = $(this).closest('table').find(".subTotalDetail");
            if (hasTax) {
                var invoiceTotal = parseInt($(src).val(), 10); //總額
                var subTotal = Math.round(invoiceTotal / (1 + tax)); //未稅金額
                var gstTotal = invoiceTotal - subTotal; //稅額
                target.val(subTotal);
                $(this).closest('table').find(".gstTotalDetail").val(gstTotal);
            } else {
                target.val($(src).val());
            }
        });

        //單據備註會被預帶到下面的各筆對帳單對應單據資料的備註中
        $("#<%= tbMemo.ClientID %>").change(function () {
            var src = this;
            var target = $(".memoDetail");
            //若對帳單對應單據備註為空才預帶單據備註
            if (target.val() == "") {
                target.val($(src).val());
            } else {
                target.val($(src).val());
            }
        });

        

        function getDefaultPaymentDay(){


            var today=new Date();
            if ('<%=config.IsRemittanceFortnightly%>' == 'True')
            {
                //新的&同意雙周結算法
                
                if ('<%=deliveryType%>'=='1')
                {
                    //憑證算法
                    if ('<%=hidIsAgreePponNewContractSeller%>' == 'True')
                    {
                        //同意走新的
                        var nowWeek = today.getDay();
                        var dafeultFriday=new Date();
                        if (nowWeek == 6)
                        {
                            //dafeultFriday =  (- 1) + 7;
                            dafeultFriday.setDate(dafeultFriday.getDate() -1 + 7);
                        }
                        else
                        {
                            //dafeultFriday = (5 - nowWeek) + 7;
                            dafeultFriday.setDate(dafeultFriday.getDate() + ((5 - nowWeek) + 7));
                        }

                        $("#<%= tbDefaultPaymentDay.ClientID %>").val(dafeultFriday.getFullYear()+"/"+(dafeultFriday.getMonth()+1)+"/"+dafeultFriday.getDate());
                    }
                    else if ('<%=hidIsAgreePponNewContractSeller%>' == 'False')
                    {
                        //不同意走舊的
                        //週3~6:隔週2 周日~2:本周五
                        var weekly=today.getDay();//取得星期幾
                        if(weekly==5){today.setDate(today.getDate() + 7);}
                        if(weekly==6){today.setDate(today.getDate() + 6);}
                        if(weekly==0){today.setDate(today.getDate() + 5);}
                        if(weekly==1){today.setDate(today.getDate() + 4);}
                        if(weekly==2){today.setDate(today.getDate() + 3);}
                        if(weekly==3){today.setDate(today.getDate() + 6);}
                        if(weekly==4){today.setDate(today.getDate() + 5);}
                        $("#<%= tbDefaultPaymentDay.ClientID %>").val(today.getFullYear()+"/"+(today.getMonth()+1)+"/"+today.getDate());
                    }                    
                }
                else if ('<%=deliveryType%>'=='2')
                {

                    //宅配算法
                    //同意走新的
                    if ('<%=hidIsAgreeHouseNewContractSeller%>' == 'True')
                    {
                        //本月的1號
                        var FirstDay  = new Date(today.getFullYear(),today.getMonth(),1);
                        //本月1號星期幾//0為星期天
                        var FirstDayWeek = FirstDay.getDay();
                        if (FirstDayWeek==0)
                            FirstDayWeek = 7;//x

                        //預計付款日為禮拜五
                        var defaultWeek=5;//w
                        var defaultSecondFriday=new Date();
                        var defaultFourthFriday=new Date();

                        var defaultSecondFridayRangeStart=new Date();
                        var defaultSecondFridayRangeEnd=new Date();
                        var defaultFourthFridayRangeStart=new Date();
                        var defaultFourthFridayRangeEnd=new Date();
                        if (defaultWeek >= FirstDayWeek)
                        {
                            //f=1+(w-x)
                            defaultSecondFriday.setDate(FirstDay.getDate()+(defaultWeek - FirstDayWeek) + 7 * (2-1));
                            defaultFourthFriday.setDate(FirstDay.getDate()+(defaultWeek - FirstDayWeek) + 7 * (4-1));


                        }
                        else
                        {
                            //f=1+[(7-x)+w]
                            //f+7*(n-1)
                            defaultSecondFriday.setDate(FirstDay.getDate()+(7-FirstDayWeek + defaultWeek) + 7 * (2-1));
                            defaultFourthFriday.setDate(FirstDay.getDate()+(7-FirstDayWeek + defaultWeek) + 7 * (4-1));
                        }


                        defaultSecondFridayRangeStart.setDate(defaultSecondFriday.getDate() - 19);
                        defaultSecondFridayRangeEnd.setDate(defaultSecondFriday.getDate() - 6);

                        defaultFourthFridayRangeStart.setDate(defaultFourthFriday.getDate() - 19);
                        defaultFourthFridayRangeEnd.setDate(defaultFourthFriday.getDate() - 6);


                        if ((Date.parse(defaultSecondFridayRangeStart)).valueOf() <= (Date.parse(today)).valueOf() && (Date.parse(defaultSecondFridayRangeEnd)).valueOf() >= (Date.parse(today)).valueOf())
                        {
                        
                            $("#<%= tbDefaultPaymentDay.ClientID %>").val(defaultSecondFriday.getFullYear()+"/"+(defaultSecondFriday.getMonth()+1)+"/"+defaultSecondFriday.getDate());
                        }
                        else if ((Date.parse(defaultFourthFridayRangeStart)).valueOf() <= (Date.parse(today)).valueOf() && (Date.parse(defaultFourthFridayRangeEnd)).valueOf() >= (Date.parse(today)).valueOf())
                        {
                            $("#<%= tbDefaultPaymentDay.ClientID %>").val(defaultFourthFriday.getFullYear()+"/"+(defaultFourthFriday.getMonth()+1)+"/"+defaultFourthFriday.getDate());
                        }
                        else
                        {
                            //付款日為下個月
                            //本月的1號
                            //getMonth 1月為0
                            FirstDay = new Date(FirstDay.getFullYear(),today.getMonth() + 1,1);
                            if (today.getMonth() + 1 == 12 )
                            {
                                defaultSecondFriday = new Date(FirstDay.getFullYear() - 1,today.getMonth() + 1,1);
                            }
                            else
                            {
                                defaultSecondFriday = new Date(FirstDay.getFullYear(),today.getMonth() + 1,1);
                            }
                            
                            //本月1號星期幾//0為星期天
                            var FirstDayWeek = FirstDay.getDay();
                            if (FirstDayWeek==0)
                                FirstDayWeek = 7;//x

                            if (defaultWeek >= FirstDayWeek)
                            {
                            
                                defaultSecondFriday.setDate(FirstDay.getDate()+(defaultWeek - FirstDayWeek) + 7 * (2-1));
                            }
                            else
                            {
                                defaultSecondFriday.setDate(FirstDay.getDate()+(7-FirstDayWeek + defaultWeek) + 7 * (2-1));

                            }

                            $("#<%= tbDefaultPaymentDay.ClientID %>").val(defaultSecondFriday.getFullYear()+"/"+(defaultSecondFriday.getMonth()+1)+"/"+defaultSecondFriday.getDate());
                        }
                    }
                    else
                    {
                        //不同意走舊的
                        //週3~6:隔週2 周日~2:本周五
                        var weekly=today.getDay();//取得星期幾
                        if(weekly==5){today.setDate(today.getDate() + 7);}
                        if(weekly==6){today.setDate(today.getDate() + 6);}
                        if(weekly==0){today.setDate(today.getDate() + 5);}
                        if(weekly==1){today.setDate(today.getDate() + 4);}
                        if(weekly==2){today.setDate(today.getDate() + 3);}
                        if(weekly==3){today.setDate(today.getDate() + 6);}
                        if(weekly==4){today.setDate(today.getDate() + 5);}
                        $("#<%= tbDefaultPaymentDay.ClientID %>").val(today.getFullYear()+"/"+(today.getMonth()+1)+"/"+today.getDate());
                    } 
                }
            }
            else
            {
                //舊的&不同意雙週結
                //週3~6:隔週2 周日~2:本周五
                var weekly=today.getDay();//取得星期幾
                if(weekly==5){today.setDate(today.getDate() + 7);}
                if(weekly==6){today.setDate(today.getDate() + 6);}
                if(weekly==0){today.setDate(today.getDate() + 5);}
                if(weekly==1){today.setDate(today.getDate() + 4);}
                if(weekly==2){today.setDate(today.getDate() + 3);}
                if(weekly==3){today.setDate(today.getDate() + 6);}
                if(weekly==4){today.setDate(today.getDate() + 5);}
                $("#<%= tbDefaultPaymentDay.ClientID %>").val(today.getFullYear()+"/"+(today.getMonth()+1)+"/"+today.getDate());
            }
            
            

            
        }
    });

    //當開立方式為收據,未稅金額無效,不可輸入,由系統帶入總額的數字
    function tbInvoiceTotal_onchange() {
        var receiptType = $("#<%= hidReceiptType.ClientID %>").val();
        var invoiceTaxRequired = $("#<%= hidIsInvoiceTaxRequired.ClientID %>").val();
        var hasTax = receiptType === "2" && invoiceTaxRequired === "True";
        if (hasTax) {
            var invoiceTotal = parseInt($("#tbInvoiceTotal").val(), 10);
            var subTotal = Math.round(invoiceTotal / (1 + tax)); // 未稅金額
            var gstTotal = invoiceTotal - subTotal; // 稅額
            $("#tbSubTotal").val(subTotal);
            $("#tbGstTotal").val(gstTotal);
        } else {
            $("#tbSubTotal").val($("#tbInvoiceTotal").val());
        }
    }

    function numberCheck(value) {
        return value.replace(/\D/g,'');
    }

</script>

using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.Web.ControlRoom.Controls
{
    public partial class LeftPanel2 : System.Web.UI.UserControl
    {
        private ISysConfProvider config;

        private string parentNode = @"
        <div id='#id#' class='collapsePanelHeader'>
            <div class='pull-left'>#parent_name#</div>
            <div class='pull-right'>
                <input type='image' src='#siteurl#/controlroom/images/icons/expand2.png' alt='(展開...)' />
            </div>
        </div>";        

        protected void Page_Load(object sender, EventArgs e)
        {
            config = ProviderFactory.Instance().GetConfig();
            parentNode = parentNode.Replace("#siteurl#", config.SiteUrl);


            // 取出路徑包含controlroom，並排除父權限名稱為空者
            IEnumerable<FunctionPrivilegeData> privileges = FunctionPrivilegeManager.GetFunctionPrivilegeDataById(Page.User.Identity.Name)
                .Where(x => !string.IsNullOrWhiteSpace(x.ParentFunction));

            StringBuilder menu = new StringBuilder();
            var itemGroups = privileges.Where(t => t.Visible)
                    .GroupBy(t => new KeyValuePair<int, string>(t.ParentSortOrder, t.ParentFunction))
                    .OrderBy(t => t.Key.Key);
            foreach (IGrouping<KeyValuePair<int, string>, FunctionPrivilegeData> dataGroup in itemGroups)
            {
                // add parent node
                menu.Append(parentNode.Replace("#parent_name#", dataGroup.Key.Value).Replace("#id#",
                    Convert.ToBase64String(Encoding.UTF8.GetBytes(dataGroup.Key.Value))));

                // add child nodes
                menu.Append("\n\t<div class='collapsePanel'>\n");
                foreach (FunctionPrivilegeData data in dataGroup.OrderBy(x => x.SortOrder))
                {
                    string url = Helper.CombineUrl(config.SiteUrl, data.Link);
                    menu.AppendFormat("\t\t<a class='funcLink' href='{0}'>{1}</a>\n", url, data.DisplayName);
                }
                menu.Append("\t</div>");
            }

            // append to page
            RestrictFunctions.InnerHtml = menu.ToString();

            RestrictFunctions.Visible = true;

        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using LunchKingSite.Core.Component;

namespace LunchKingSite.Web.ControlRoom.Controls
{
    public partial class EdmTitleItem : System.Web.UI.UserControl
    {
        #region property
        public string UserName { get; set; }

        public int CityId
        {
            get
            {
                int city_id;
                if (int.TryParse(hif_CityId.Value, out city_id))
                    return city_id;
                else
                    return 0;
            }
            set
            {
                hif_CityId.Value = value.ToString();
            }
        }

        public DateTime DeliveryDate
        {
            get
            {
                DateTime date_now = (DateTime.Now > DateTime.Today.AddHours(12)) ? DateTime.Today.AddDays(1) : DateTime.Today;
                return EmailFacade.GetEdmSendTime(CityId, date_now);
            }
        }

        public int EdmMainId
        {
            get
            {
                int id;
                if (int.TryParse(hif_Id.Value, out id))
                    return id;
                else
                    return 0;
            }
            set
            {
                hif_Id.Value = value.ToString();
            }
        }

        ISysConfProvider sp;
        #endregion
        #region page
        public EdmTitleItem()
        {
            sp = ProviderFactory.Instance().GetConfig();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
           
        }

        public override void DataBind()
        {
            PponCity city = PponCityGroup.DefaultPponCityGroup.GetPponCityByCityId(CityId);
            lab_CityName.Text = city.EDMCityName;
            EdmMain edm_main = EmailFacade.GetNewEdmMainByCityIdDate(city, DeliveryDate, EdmMainType.Daily, true);
            if (edm_main.Id == 0)
            {
                EdmDetail first_item;
                List<EdmDetail> details = EmailFacade.GetNewEdmDetailsByCityIdDate(city, DeliveryDate);
                if ((first_item = details.FirstOrDefault(x => x.Type == (int)EdmDetailType.MainDeal_1_Items)) != null)
                {
                    tbx_Title.Text = lab_Title.Text = first_item.Title;
                }
                else
                {
                    tbx_Title.Text = lab_Title.Text = string.Empty;
                }
                EdmMainId = 0;
            }
            else
            {
                lblDeliveryDate.Text = edm_main.DeliveryDate.ToString("yyyy/MM/dd HH:mm");
                tbx_Title.Text = lab_Title.Text = edm_main.Subject;
                EdmMainId = edm_main.Id;
            }
        }
        #endregion
        #region method
        //回傳設定資料
        public EdmMain ReturnEdmMain()
        {
            EdmMain main;
            if (EdmMainId == 0)
                main = new EdmMain() { Subject = tbx_Title.Text, DeliveryDate = DeliveryDate, CityId = CityId, CityName = PponCityGroup.DefaultPponCityGroup.GetPponCityByCityId(CityId).CityName, Creator = UserName, CreateDate = DateTime.Now, Type = (int)EdmMainType.Daily, Status = true, Cpa = EmailFacade.GetEdmCpa(CityId) };
            else
            {
                main = EmailFacade.GetNewEdmMainById(EdmMainId);
                main.Subject = tbx_Title.Text;
            }
            return main;
        }
        #endregion
    }

    static class UserControlExtensionMethod
    {
        public static EdmMain GetData(this EdmTitleItem usercontrol)
        {
            return usercontrol.ReturnEdmMain();
        }
    }
}
﻿using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Vbs;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace LunchKingSite.Web.ControlRoom.Controls
{
    public partial class BalanceSheetBill : BaseUserControl
    {
        public static ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        public event EventHandler Saved;
        public event EventHandler Canceled;
        
        public BillUserControlMode RequestMode { get; set; }
        public int RequestBillId { get; set; }
        public Dictionary<int, BalanceSheetUseType> RequestBalanceSheetIds { get; set; }        

        private BillUserControlMode CurrentMode
        {
            get
            {
                int result;
                if (int.TryParse(hidMode.Value, out result))
                {
                    return (BillUserControlMode)result;
                }
                return BillUserControlMode.Unknown;
            }
            set
            {
                hidMode.Value = ((int)value).ToString();
            }
        }        
        private int? CurrentBillId
        {
            get 
            {
                int billId;
                if (int.TryParse(hidBillId.Value, out billId))
                {
                    return billId;
                }
                return null;                
            }            
        }                   
        public int deliveryType { get; set; }
        public bool hidIsAgreePponNewContractSeller { get; set; }
        public bool hidIsAgreeHouseNewContractSeller { get; set; }

        public void ReLoad()
        {
            ClearControlValues();       
            
            BillInfo billInfo;
            List<dynamic> exDist;
            bool taxEditable;
            bool addRelationError = false;
            bool updateModeBillEditable = true;
            string defaultPayment="";
            switch (this.RequestMode)
            {                
                case BillUserControlMode.AddRelationToBill:
                    var billAdd = BillRepository.BillOfId(this.RequestBillId);
                    if (billAdd.GetDistributedDistribution().Count == 0)
                    {
                        addRelationError = true;
                        ShowMessage("查無此單據!"); 
                    }
                    List<BillingStmtDistribution> distributionsAdd;
                    List<BillingStmtDistribution> distributionsWmsAdd;
                    if (!billAdd.TryCalculateDistribution(this.RequestBalanceSheetIds, out distributionsAdd,out distributionsWmsAdd))
                    {
                        addRelationError = true;
                        distributionsAdd = billAdd.GetDistributedDistribution();
                        ShowMessage("單據開立方式與勾選的對帳單開立方式不同!");                        
                    }
                    
                    exDist = billAdd.ToExpandedInfo(distributionsAdd, distributionsWmsAdd);
                    billInfo = billAdd.Info;
                    taxEditable =
                        billInfo.BillType == VendorReceiptType.Invoice
                        && billInfo.IsInvoiceTaxRequired;

                    foreach (var dist in exDist)
                    {
                        if (RequestBalanceSheetIds.ContainsKey(dist.BalanceSheetId))
                        {
                            dist.TaxReadOnly = !taxEditable;
                            dist.TotalAmountReadOnly = false;
                        }
                        else
                        {
                            dist.TaxReadOnly = true;
                            dist.TotalAmountReadOnly = true;                            
                        }
                    }
                    
                    hidBillId.Value = this.RequestBillId.ToString();
                    this.CurrentMode = BillUserControlMode.AddRelationToBill;
                    break;
                case BillUserControlMode.UpdateBill:
                    var billUpdate = BillRepository.BillOfId(this.RequestBillId);

                    List<BillingStmtDistribution> distributionsUpdate = new List<BillingStmtDistribution>();
                    List<BillingStmtDistribution> distributionsWmsUpdate = new List<BillingStmtDistribution>();
                    if (this.RequestBalanceSheetIds.First().Value == BalanceSheetUseType.Deal)
                    {
                        var bsModel = new LunchKingSite.BizLogic.Vbs.BS.BalanceSheetModel(this.RequestBalanceSheetIds.First().Key);
                        distributionsUpdate = billUpdate.GetDistributedDistribution();
                        distributionsUpdate = distributionsUpdate.Where(x => x.Type == (int)BalanceSheetUseType.Deal).ToList();
                        defaultPayment = bsModel.GetDefaultPaymentDate();
                    }
                    else
                    {
                        var bsModel = new LunchKingSite.BizLogic.Vbs.BS.BalanceSheetWmsModel(this.RequestBalanceSheetIds.First().Key);
                        distributionsWmsUpdate = billUpdate.GetDistributedDistribution();
                        distributionsWmsUpdate = distributionsWmsUpdate.Where(x => x.Type == (int)BalanceSheetUseType.Wms).ToList();
                        defaultPayment = bsModel.GetDefaultPaymentDate();
                    }
                    
                    
                  //  IList<BillingStmtDistribution> distributionsUpdate = billUpdate.GetDistributedDistribution();
                  //  IList<BillingStmtDistribution> distributionsWmsUpdate = billUpdate.GetDistributedDistribution();

                    exDist = billUpdate.ToExpandedInfo(distributionsUpdate, distributionsWmsUpdate);//在確認??
                    billInfo = billUpdate.Info;

                    taxEditable = 
                        billInfo.BillType == VendorReceiptType.Invoice
                        && billInfo.IsInvoiceTaxRequired;
                   
                    foreach (var dist in exDist)
                    {
                        if (dist.IsConfirmed &&
                            !CommonFacade.IsInSystemFunctionPrivilege(HttpContext.Current.User.Identity.Name, SystemFunctionType.UpdateConfirmedBsBill))
                        {
                            updateModeBillEditable = false;
                            dist.TaxReadOnly = true;
                            dist.TotalAmountReadOnly = true; 
                        }
                        else
                        {
                            dist.TaxReadOnly = !taxEditable;
                            dist.TotalAmountReadOnly = false;
                        }             
                    }
                    
                    hidBillId.Value = this.RequestBillId.ToString();
                    this.CurrentMode = BillUserControlMode.UpdateBill;
                    break;
                case BillUserControlMode.CreateBill:
                default:
                    var billNew = BillEntity.New();
                    List<BillingStmtDistribution> distributions;
                    List<BillingStmtDistribution> distributionsWms;

                    if (!billNew.TryCalculateNewBillDistribution(this.RequestBalanceSheetIds, out distributions, out distributionsWms, out billInfo))
                    {
                        SaveDistribution.Attributes.Add("readonly", "readonly");
                        ShowMessage("所選對帳單發生應稅/免稅設定不同問題!請分開維護單據!");
                        //throw new Exception("something is wrong !?");
                        return;
                    }
                    exDist = billNew.ToExpandedInfo(distributions, distributionsWms);
                    taxEditable =
                    billInfo.BillType == VendorReceiptType.Invoice
                    && billInfo.IsInvoiceTaxRequired;

                    foreach (var dist in exDist)
                    {
                        dist.TaxReadOnly = !taxEditable;
                        dist.TotalAmountReadOnly = false;
                    }

                    this.CurrentMode = BillUserControlMode.CreateBill;
                    break;
            }

            lvDistribution.DataSource = exDist;
            lvDistribution.DataBind();
            if (exDist.Count > 0)
            {
                tbInvoiceTotal.Text = billInfo.Total.ToString();
                tbSubTotal.Text = billInfo.Subtotal.ToString();
                tbGstTotal.Text = billInfo.Tax.ToString();
                tbMemo.Text = billInfo.Remark;
                lblVendorReceiptType.Text = GetReceiptIssueTypeDescription(billInfo.BillType, billInfo.BillTypeOtherName);
                hidReceiptType.Value = ((int)billInfo.BillType).ToString();
                hidIsInvoiceTaxRequired.Value = billInfo.IsInvoiceTaxRequired.ToString();
                hidReceiptTypeOtherName.Value = billInfo.BillTypeOtherName;
                rbCompanyName.SelectedValue = ((int)billInfo.BuyerType).ToString();
                tbInvoiceComId.Text = billInfo.BillingCompanyId;
                tbInvoiceNumber.Text = billInfo.BillNumber;
                tbInvoiceDate.Text = billInfo.BillDate.HasValue ? billInfo.BillDate.Value.ToString("yyyy/MM/dd") : string.Empty;
                tbDefaultPaymentDay.Text = defaultPayment;
            }
            bool hasTax = (billInfo.BillType == VendorReceiptType.Invoice && billInfo.IsInvoiceTaxRequired);
            switch (this.CurrentMode)
            {
                case BillUserControlMode.CreateBill:
                    ConfigureBillInfoEditability(true, hasTax);
                    break;
                case BillUserControlMode.UpdateBill:
                    ConfigureBillInfoEditability(updateModeBillEditable, hasTax);
                    break;
                case BillUserControlMode.AddRelationToBill:
                default:
                    ConfigureBillInfoEditability(false);
                    break;
            }
            if (addRelationError)
            {
                SaveDistribution.Attributes.Add("readonly", "readonly");
            }
            else
            {
                SaveDistribution.Attributes.Remove("readonly");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="editable"></param>
        /// <param name="hasTax">not required when editable == false</param>
        private void ConfigureBillInfoEditability(bool editable, bool hasTax = false)
        {
            switch (editable)
            {
                case true:
                    if (hasTax)
                    {
                        tbGstTotal.Attributes.Remove("readonly");
                        tbSubTotal.Attributes.Remove("readonly");
                        tbInvoiceTotal.Attributes.Remove("readonly");                    
                    }
                    else
                    {
                        tbGstTotal.Attributes.Add("readonly", "readonly");
                        tbSubTotal.Attributes.Add("readonly", "readonly");
                        tbInvoiceTotal.Attributes.Remove("readonly");
                    }
                    tbMemo.Attributes.Remove("readonly");
                    foreach (ListItem item in rbCompanyName.Items)
                    {
                        item.Enabled = true;
                    }
                    tbInvoiceComId.Attributes.Remove("readonly");
                    tbInvoiceNumber.Attributes.Remove("readonly");
                    tbInvoiceDate.Attributes.Remove("readonly");
                    break;
                case false:
                    tbGstTotal.Attributes.Add("readonly", "readonly");
                    tbSubTotal.Attributes.Add("readonly", "readonly");
                    tbInvoiceTotal.Attributes.Add("readonly", "readonly");
                    tbMemo.Attributes.Add("readonly", "readonly");                
                    foreach (ListItem item in rbCompanyName.Items)
                    {
                        if (!item.Selected)
                        {
                            item.Enabled = false;
                        }                    
                    }
                    tbInvoiceComId.Attributes.Add("readonly", "readonly");
                    tbInvoiceNumber.Attributes.Add("readonly", "readonly");
                    tbInvoiceDate.Attributes.Add("readonly", "readonly");      
                    break;
            }
        }
        
        protected void lvDistribution_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType == ListViewItemType.DataItem)
            {
                if (((dynamic)e.Item.DataItem).TaxReadOnly)
                {
                    ((TextBox)e.Item.FindControl("DistSubtotal")).Attributes.Add("readonly", "readonly");
                    ((TextBox)e.Item.FindControl("DistTax")).Attributes.Add("readonly", "readonly");
                }
                if (((dynamic)e.Item.DataItem).TotalAmountReadOnly)
                {
                    ((TextBox)e.Item.FindControl("DistTotal")).Attributes.Add("readonly", "readonly");
                }
            }
        }

        protected void SaveDistribution_Click(object sender, EventArgs e)
        {
            bool parsingError = false;
            List<BillingStmtDistribution> modDistribution = new List<BillingStmtDistribution>();
            foreach (var lvItem in lvDistribution.Items)
            {
                int bsId;
                int total;
                int subtotal;
                int tax;
                int useType = 0;
                string remark = ((TextBox)lvItem.FindControl("DistRemark")).Text;

                if (int.TryParse(((HiddenField)lvItem.FindControl("BsId")).Value, out bsId)
                    && int.TryParse(((TextBox)lvItem.FindControl("DistTotal")).Text, out total)
                    && int.TryParse(((TextBox)lvItem.FindControl("DistSubtotal")).Text, out subtotal)
                    && int.TryParse(((TextBox)lvItem.FindControl("DistTax")).Text, out tax)
                    && int.TryParse(((HiddenField)lvItem.FindControl("BalanceSheetUseType")).Value, out useType))
                {
                    modDistribution.Add(new BillingStmtDistribution(bsId, total, subtotal, tax, remark, useType));
                }
                else
                {
                    parsingError = true;
                    break;
                }
            }

            BillInfo modInfo = null;
            
            VendorReceiptType type;                
            if (!VendorReceiptType.TryParse(hidReceiptType.Value, out type))
            {
                throw new Exception("開立方式有問題!");
            }
            BillBuyerType buyerType;
            if (!BillBuyerType.TryParse(rbCompanyName.SelectedValue, out buyerType))
            {
                throw new Exception("買受人有問題!");
            }
            DateTime? billDate = null;
            DateTime dummy;
            if (DateTime.TryParse(tbInvoiceDate.Text, out dummy))
            {
                billDate = dummy;
            }
            bool isInvoiceTaxRequired;
            if (!bool.TryParse(hidIsInvoiceTaxRequired.Value, out isInvoiceTaxRequired))
            {
                throw new Exception("開立方式有問題!");
            }

            modInfo = new BillInfo
            {
                BillType = type,
                BillTypeOtherName = hidReceiptTypeOtherName.Value,
                IsInvoiceTaxRequired = isInvoiceTaxRequired,
                BillNumber = tbInvoiceNumber.Text,
                BillingCompanyId = tbInvoiceComId.Text,
                BillDate = billDate,
                BuyerType = buyerType,
                Total = int.Parse(tbInvoiceTotal.Text),
                Subtotal = int.Parse(tbSubTotal.Text),
                Tax = int.Parse(tbGstTotal.Text),
                Remark = tbMemo.Text
            };            

            if (!parsingError)
            {
                BillEntity bill;
                if (this.CurrentBillId.HasValue)
                {
                    bill = BillRepository.BillOfId(this.CurrentBillId.Value);
                }
                else
                {
                    bill = BillEntity.New();
                }
                string invalidReason;
                if (bill.ValidateModRequest(modInfo, modDistribution, out invalidReason))
                {
                    BillRepository.Save(bill, this.Context.User.Identity.Name);
                    if (this.CurrentMode == BillUserControlMode.CreateBill ||
                        this.CurrentMode == BillUserControlMode.AddRelationToBill)
                    {
                        //改為財務作業，存檔後立即確認，節省作業時間
                        foreach (var mod in modDistribution)
                        {
                            if (mod.Type == (int)BalanceSheetUseType.Deal)
                            {
                                var bsModel = new LunchKingSite.BizLogic.Vbs.BS.BalanceSheetModel(mod.BalanceSheetId);
                                var confirmResult = bsModel.ConfirmBillingStatement(HttpContext.Current.User.Identity.Name);
                                bsModel.UpdateDefaultPaymentTime(tbDefaultPaymentDay.Text.Trim());
                            }
                            else
                            {
                                var bsModel = new LunchKingSite.BizLogic.Vbs.BS.BalanceSheetWmsModel(mod.BalanceSheetId);
                                var confirmResult = bsModel.ConfirmBillingStatement(HttpContext.Current.User.Identity.Name);
                                bsModel.UpdateDefaultPaymentTime(tbDefaultPaymentDay.Text.Trim());
                            }
                            
                        }

                        ShowMessage("新增成功");
                    }
                    else if (this.CurrentMode == BillUserControlMode.UpdateBill)
                    {
                        foreach (var mod in modDistribution)
                        {
                            if (mod.Type == (int)BalanceSheetUseType.Deal)
                            {
                                var bsModel = new LunchKingSite.BizLogic.Vbs.BS.BalanceSheetModel(mod.BalanceSheetId);
                                bsModel.UpdateDefaultPaymentTime(tbDefaultPaymentDay.Text.Trim());
                            }
                            else
                            {
                                var bsModel = new LunchKingSite.BizLogic.Vbs.BS.BalanceSheetWmsModel(mod.BalanceSheetId);
                                bsModel.UpdateDefaultPaymentTime(tbDefaultPaymentDay.Text.Trim());
                            }
                            
                        }
                        ShowMessage("修改成功");
                    }

                    if (this.Saved != null)
                    {
                        this.Saved(this, EventArgs.Empty);
                    }
                }
                else
                {
                    ShowMessage(invalidReason);
                }
            }
        }

        protected void Cancel_Click(object sender, EventArgs e)
        {
            this.Canceled(this, EventArgs.Empty);
        }
        

        protected decimal GetBusinessTax()
        {
            return ProviderFactory.Instance().GetConfig().BusinessTax;
        }
        
        private void ClearControlValues()
        {
            lblVendorReceiptType.Text = string.Empty;            
            rbCompanyName.SelectedValue = "2";
            tbInvoiceNumber.Text = string.Empty;
            tbInvoiceComId.Text = string.Empty;
            tbInvoiceDate.Text = string.Empty;
            tbSubTotal.Text = string.Empty;
            tbGstTotal.Text = string.Empty;
            tbInvoiceTotal.Text = string.Empty;
            tbMemo.Text = string.Empty;
            hidReceiptType.Value = string.Empty;
            hidIsInvoiceTaxRequired.Value = string.Empty;
            hidReceiptTypeOtherName.Value = string.Empty;
            hidBillId.Value = string.Empty;            
            hidMode.Value = string.Empty;
        }
        
        private string GetReceiptIssueTypeDescription(VendorReceiptType receiptIssueType, string receiptIssueTypeOtherRemark)
        {
            switch (receiptIssueType)
            {
                case VendorReceiptType.Other:
                    return string.Format("{0}：{1}", Helper.GetLocalizedEnum(Resources.Localization.ResourceManager, receiptIssueType), receiptIssueTypeOtherRemark);
                default:
                    return Helper.GetLocalizedEnum(Resources.Localization.ResourceManager, receiptIssueType);
            }
        }

        private void ShowMessage(string msg)
        {
            System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('" + msg + "');", true);
        }
        
    }

    public enum BillUserControlMode
    {
        Unknown,
        CreateBill,
        AddRelationToBill,
        UpdateBill
    }
}
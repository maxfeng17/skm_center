﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.Core;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;

namespace LunchKingSite.Web.ControlRoom.Controls
{
    public partial class BuildingList : LocalizedBaseUserControl, IBuildingListView
    {
        public event EventHandler<DataEventArgs<int>> GetBuildingCount;
        public event EventHandler<DataEventArgs<int>> PageChanged;
        public event EventHandler SortClicked;
        public event EventHandler SearchClicked;
        public event EventHandler SelectCityChanged;

        #region props
        private BuildingListPresenter _presenter;
        public BuildingListPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        public string SortExpression
        {
            get { return (ViewState != null && ViewState["se"] != null) ? (string)ViewState["se"] : ViewBuildingCity.Columns.BuildingRank + " asc"; }
            set { ViewState["se"] = value; }
        }
        public string Filter
        {
            get
            {
                return tbSearch.Text;
            }
            set
            {
                tbSearch.Text = value;
            }
        }
        public bool Status
        {
            get
            {
                if (string.IsNullOrEmpty(rbl.SelectedValue))
                {
                    return true;
                }
                else
                {
                    return Convert.ToBoolean(rbl.SelectedValue);
                }
            }
            set
            {
                value = Convert.ToBoolean(rbl.SelectedValue);
            }
        }
        public string FilterType
        {
            get { return SearchDropDownList.SelectedValue; }
            set { SearchDropDownList.SelectedValue = value; }
        }
        public int PageSize
        {
            get { return int.Parse(ddlPageSize.SelectedValue); }
            set { value = int.Parse(ddlPageSize.SelectedValue); }
        }
        public int CurrentPage
        {
            get { return gridPager.CurrentPage; }
        }

        public KeyValuePair<string, int>? SelectCity
        {
            get
            {
                if (ddlCity.SelectedValue != "0")
                    if (ddlZone.SelectedValue == "0")
                        return new KeyValuePair<string, int>(ViewBuildingCity.Columns.ParentId, int.Parse(ddlCity.SelectedValue));
                    else if (ddlZone.SelectedValue != "")
                        return new KeyValuePair<string, int>(ViewBuildingCity.Columns.CityId, int.Parse(ddlZone.SelectedValue));
                return null;
            }
        }

        public Guid BusinessHourGuid
        {
            get
            {
                if (!string.IsNullOrEmpty(Request["bzid"]))
                {
                    try
                    {
                        return new Guid(Request.QueryString["bzid"]);
                    }
                    catch
                    {
                        return Guid.Empty;
                    }
                }
                else
                    return Guid.Empty;
            }
        }

        public ShowListMode BusinessHourBindMode { get; set; }

        public Guid SellerGuid
        {
            get
            {
                if (!string.IsNullOrEmpty(Request["sid"]))
                {
                    try
                    {
                        return new Guid(Request.QueryString["sid"]);
                    }
                    catch
                    {
                        return Guid.Empty;
                    }
                }
                else
                    return Guid.Empty;
            }
        }

        public int Distance
        {
            get
            {
                if (!String.IsNullOrEmpty(tbd.Text))
                    return Convert.ToInt32(tbd.Text);
             
                return -1;
            }
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                gvBuilding.Columns[2].SortExpression = ViewBuildingCity.Columns.BuildingOnline;
                gvBuilding.Columns[3].SortExpression = ViewBuildingCity.Columns.BuildingName;
                gvBuilding.Columns[4].SortExpression = ViewBuildingCity.Columns.BuildingAddressNumber;
                gvBuilding.Columns[5].SortExpression = ViewBuildingCity.Columns.CityName;
                gvBuilding.Columns[6].SortExpression = ViewBuildingCity.Columns.BuildingRank;

                this.Presenter.OnViewInitialized();
            }

            this.Presenter.OnViewLoaded();
        }

        public void SetZoneDropDown(List<City> zone)
        {
            ddlZone.DataSource = zone;
            ddlZone.DataBind();
        }

        public void SetCityDropDown(List<City> city)
        {
            ddlCity.DataSource = city;
            ddlCity.DataBind();
        }

        public void SetBuildingList(ViewBuildingCityCollection BuildingCityLists)
        {
            gvBuilding.DataSource = BuildingCityLists;
            gvBuilding.DataBind();
        }

        protected void UpdateHandler(int pageNumber)
        {
            DataEventArgs<int> e = new DataEventArgs<int>(pageNumber);
            if (this.PageChanged != null)
                this.PageChanged(this, e);
        }

        protected int RetrieveBuildingCount()
        {
            DataEventArgs<int> e = new DataEventArgs<int>(0);
            if (GetBuildingCount != null)
                GetBuildingCount(this, e);
            return e.Data;
        }

        protected void gvBuilding_Sorting(object sender, GridViewSortEventArgs e)
        {
            ViewState["sd"] = ((string)ViewState["sd"] == " ASC") ? " DESC" : " ASC";
            this.SortExpression = e.SortExpression + (string)ViewState["sd"];
            if (this.SortClicked != null)
                SortClicked(this, new EventArgs());
            gridPager.ResolvePagerView(1);
        }

        public void SetFilterTypeDropDown(Dictionary<string, string> types)
        {
            SearchDropDownList.DataSource = types;
            SearchDropDownList.DataBind();
        }

        public void SetStatusFilter(Dictionary<bool, string> status)
        {
            rbl.DataSource = status;
            rbl.DataBind();
        }

        protected void bSearchBtn_Click(object sender, EventArgs e)
        {
            if (this.SearchClicked != null)
                this.SearchClicked(this, e);
            gridPager.ResolvePagerView(1, true);
        }

        protected void ddlCity_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlZone.SelectedValue = "0";
            if (this.SelectCityChanged != null)
                SelectCityChanged(this, new EventArgs());
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            gvBuilding.PageSize = int.Parse(ddlPageSize.SelectedValue);
            gridPager.PageSize = int.Parse(ddlPageSize.SelectedValue);

            if (SearchClicked != null)
                SearchClicked(this, e);

            gridPager.ResolvePagerView(1, true);
        }

        protected void TotalConut_Load(object sender, EventArgs e)
        {
            TotalConut.Text = RetrieveBuildingCount().ToString();
        }

        protected void cbSelectAll_CheckedChanged(object sender, EventArgs e)
        {
            foreach (GridViewRow gd in gvBuilding.Rows)
            {
                if (((CheckBox)gvBuilding.HeaderRow.FindControl("cbSelectAll")).Checked)
                    ((CheckBox)gd.Cells[0].FindControl("bcb")).Checked = true;
                else
                    ((CheckBox)gd.Cells[0].FindControl("bcb")).Checked = false;
            }
        }
    }
}
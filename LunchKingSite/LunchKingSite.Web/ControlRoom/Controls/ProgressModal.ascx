<%@ Control Language="C#" AutoEventWireup="true" Codebehind="ProgressModal.ascx.cs"
    Inherits="LunchKingSite.Web.ControlRoom.Controls.ProgressModal" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="System.Web.Extensions"
    Namespace="System.Web.UI" TagPrefix="aspajax" %>
<aspajax:UpdateProgress ID="mup" runat="server">
    <ProgressTemplate>
        <div class="ldnmpu" style="width: 150px">
            <img src="~/Themes/PCweb/images/spinner.gif" align="middle" runat="server" />
            資料處理中, 請稍候
        </div>
    </ProgressTemplate>
</aspajax:UpdateProgress>
<cc1:ModalPopupExtender ID="mpe" runat="server" TargetControlID="mup" BackgroundCssClass="ldnmbg"
    DropShadow="false" PopupControlID="mup" />

<script language="javascript" type="text/javascript">  
    Sys.Application.add_load(pm_alh);
    function pm_alh(sender, args)
    {
        var prm = Sys.WebForms.PageRequestManager.getInstance();
		prm.add_initializeRequest(mpe_i);
		prm.add_endRequest(mpe_e);
    }
    
    function mpe_i(sender, args)
    {  
        var pop = $find("<%=mpe.ClientID%>");
        pop.show();
    }
    
    function mpe_e(sender, args)
    {
        var pop = $find("<%=mpe.ClientID%>");
        pop.hide();
    }
</script>


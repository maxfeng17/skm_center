﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EdmTitleItem.ascx.cs"
    Inherits="LunchKingSite.Web.ControlRoom.Controls.EdmTitleItem" %>
<asp:HiddenField ID="hif_CityId" runat="server" />
<asp:HiddenField ID="hif_Id" runat="server" />
<table style="background-color: gray; width: 950px">
    <tr>
        <td class="bgOrange">
            <asp:Label ID="lab_CityName" runat="server" Text="Label"></asp:Label>
            (發送時間: <asp:Label ID="lblDeliveryDate" runat="server"></asp:Label>)
        </td>
    </tr>
    <tr>
        <td class="bgBlue">
            <asp:Label ID="lab_Title" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td class="bgLBlue">
            <asp:TextBox ID="tbx_Title" runat="server" Width="930" TextMode="MultiLine" Rows="2"></asp:TextBox>
        </td>
    </tr>
</table>

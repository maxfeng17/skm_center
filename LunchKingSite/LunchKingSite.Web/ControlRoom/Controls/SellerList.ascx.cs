﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;

namespace LunchKingSite.Web.ControlRoom.Controls
{
    public partial class SellerList : LocalizedBaseUserControl, ISellerListView
    {
        public event EventHandler<DataEventArgs<int>> GetDataCount;
        public event EventHandler<DataEventArgs<int>> PageChanged;
        public event EventHandler SortClicked;
        public event EventHandler SearchClicked;

        #region props
        private SellerListPresenter _presenter;
        public SellerListPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        public string SortExpression
        {
            get { return (ViewState != null && ViewState["se"] != null) ? (string)ViewState["se"] : Seller.Columns.CreateTime + " Desc"; }
            set { ViewState["se"] = value; }
        }
        public string Filter
        {
            get
            {
                return tbSearch.Text;
            }
            set
            {
                tbSearch.Text = value;
            }
        }
        public string FilterType
        {
            get { return SearchDropDownList.SelectedValue; }
            set { SearchDropDownList.SelectedValue = value; }
        }
        public int PageSize
        {
            get { return int.Parse(ddlPageSize.SelectedValue); }
            set { value = int.Parse(ddlPageSize.SelectedValue); }
        }
        public int CurrentPage
        {
            get { return gridPager.CurrentPage; }
        }

        private bool _showSellerName;
        public bool ShowSellerName
        {
            get { return _showSellerName; }
            set
            {
                _showSellerName = value;
                foreach ( DataControlField dcf in gvSeller.Columns)
                {
                    if(string.Equals("賣家名稱", dcf.HeaderText))
                    {
                        dcf.Visible = value;
                        dcf.SortExpression = Seller.Columns.SellerName;
                    }
                }
            }
        }

        private bool _showSellerCityLevel;
        public bool ShowSellerCityLevel
        {
            get { return _showSellerCityLevel; }
            set
            {
                _showSellerCityLevel = value;
                foreach (DataControlField dcf in gvSeller.Columns)
                {
                    if (string.Equals("區域", dcf.HeaderText))
                    {
                        dcf.Visible = value;
                        dcf.SortExpression = Seller.Columns.CityId;
                    }
                }
            }
        }

        private bool _showSellerId;
        public bool ShowSellerId
        {
            get { return _showSellerId; }
            set
            {
                _showSellerId = value;
                foreach (DataControlField dcf in gvSeller.Columns)
                {
                    if (string.Equals("賣家編號", dcf.HeaderText))
                    {
                        dcf.Visible = value;
                        dcf.SortExpression = Seller.Columns.SellerId;
                    }
                }
            }
        }

        private bool _showSellerTel;
        public bool ShowSellerTel	                             
        {
            get { return _showSellerTel; }
            set
            {
                _showSellerTel = value;
                foreach (DataControlField dcf in gvSeller.Columns)
                {
                    if (string.Equals("電話", dcf.HeaderText))
                    {
                        dcf.Visible = value;
                        dcf.SortExpression = Seller.Columns.SellerTel;
                    }
                }
            }
        }

        private bool _showSellerFax;
        public bool ShowSellerFax		                               
        {
            get { return _showSellerFax; }
            set
            {
                _showSellerFax = value;
                foreach (DataControlField dcf in gvSeller.Columns)
                {
                    if (string.Equals("傳真", dcf.HeaderText))
                    {
                        dcf.Visible = false;
                        dcf.SortExpression = Seller.Columns.SellerFax;
                    }
                }
            }
        }

        private bool _showSellerStatus;
        public bool ShowSellerStatus
        {
            get { return _showSellerStatus; }
            set
            {
                _showSellerStatus = value;
                foreach (DataControlField dcf in gvSeller.Columns)
                {
                    if (string.Equals("上線", dcf.HeaderText))
                    {
                        dcf.Visible = false;
                        dcf.SortExpression = Seller.Columns.SellerStatus;
                    }
                }
            }
        }

        private bool _showCreateTime;
        public bool ShowCreateTime
        {
            get { return _showCreateTime; }
            set
            {
                _showCreateTime = value;
                foreach (DataControlField dcf in gvSeller.Columns)
                {
                    if (string.Equals("建立時間", dcf.HeaderText))
                    {
                        dcf.Visible = value;
                        dcf.SortExpression = Seller.Columns.CreateTime;
                    }
                }
            }
        }

        private bool _showModifyTime;
        public bool ShowModifyTime
        {
            get { return _showModifyTime; }
            set
            {
                _showModifyTime = value;
                foreach (DataControlField dcf in gvSeller.Columns)
                {
                    if (string.Equals("最後修改時間", dcf.HeaderText))
                    {
                        dcf.Visible = value;
                        dcf.SortExpression = Seller.Columns.ModifyTime;
                    }
                    if(string.Equals("連鎖顯示名稱", dcf.HeaderText))
                    {
                        dcf.Visible = value;
                    }
                }
            }
        }

        private bool showCloseDownStatus;
        public bool ShowCloseDownStatus
        {
            get { return showCloseDownStatus; }
            set 
            { 
                showCloseDownStatus = value;
                foreach (DataControlField dcf in gvSeller.Columns)
                {
                    if (string.Equals("賣家狀態", dcf.HeaderText))
                    {
                        dcf.Visible = value;
                    }
                }
            }
        }
	
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                this.Presenter.OnViewInitialized();
            }

            this.Presenter.OnViewLoaded();
        }

        public void SetSellerList(SellerCollection SellerLists)
        {
            gvSeller.DataSource = SellerLists;
            gvSeller.DataBind();
        }

        protected void UpdateHandler(int pageNumber)
        {
            DataEventArgs<int> e = new DataEventArgs<int>(pageNumber);
            if (this.PageChanged != null)
                this.PageChanged(this, e);
        }

        protected int RetrieveDataCount()
        {
            DataEventArgs<int> e = new DataEventArgs<int>(0);
            if (GetDataCount != null)
                GetDataCount(this, e);
            return e.Data;
        }

        protected void gvSeller_Sorting(object sender, GridViewSortEventArgs e)
        {
            ViewState["sd"] = ((string)ViewState["sd"] == " ASC") ? " DESC" : " ASC";
            this.SortExpression = e.SortExpression + (string)ViewState["sd"];
            if (this.SortClicked != null)
                SortClicked(this, new EventArgs());
            gridPager.ResolvePagerView(1);
        }

        public void SetFilterTypeDropDown(Dictionary<string, string> types)
        {
            SearchDropDownList.DataSource = types;
            SearchDropDownList.DataBind();
        }

        public void SetStatusFilter(Dictionary<bool, string> status)
        {
            rbl.DataSource = status;
            rbl.DataBind();
        }

        protected void bSearchBtn_Click(object sender, EventArgs e)
        {
            if (this.SearchClicked != null)
                this.SearchClicked(this, e);
            gridPager.ResolvePagerView(1, true);
        }

        protected void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            gvSeller.PageSize = int.Parse(ddlPageSize.SelectedValue);
            gridPager.PageSize = int.Parse(ddlPageSize.SelectedValue);

            if (this.SearchClicked != null)
                this.SearchClicked(this, e);

            gridPager.ResolvePagerView(1, true);
        }

        protected void cbSelectAll_CheckedChanged(object sender, EventArgs e)
        {
            foreach (GridViewRow gd in gvSeller.Rows)
            {
                if (((CheckBox)gvSeller.HeaderRow.FindControl("cbSelectAll")).Checked)
                    ((CheckBox)gd.Cells[0].FindControl("bcb")).Checked = true;
                else
                    ((CheckBox)gd.Cells[0].FindControl("bcb")).Checked = false;
            }
        }

        protected string ReSetSellerStatus(object status)
        {
            if (SellerFacade.GetOnlineType((int)status) == SellerOnlineType.Online)
                return Resources.Localization.Online;
            else
                return Resources.Localization.Offline;
        }
        protected string ReSetDepartment(object department)
        {
            DepartmentTypes dep;
            if (Enum.TryParse<DepartmentTypes>(department.ToString(), out dep))
            {
                return Helper.GetLocalizedEnum(Localization, dep);
            }
            else
                return "unknown";
        }
        protected void BindCloseDownData(object sender, GridViewRowEventArgs e)
        {
            if (!ShowCloseDownStatus)
                return;

            if (e.Row.RowType != DataControlRowType.DataRow)
                return;

            Guid sellerGuid = ((Seller) e.Row.DataItem).Guid;
            
            var parentSellerLink = (HyperLink)e.Row.FindControl("parentSellerLink");
            var parentSellerGuid = Presenter.GetParentSellerGuid(sellerGuid);
            parentSellerLink.Text = Presenter.GetParentSellerName(parentSellerGuid);
            parentSellerLink.NavigateUrl = parentSellerGuid.HasValue ? string.Format("~/Controlroom//Seller/seller_add.aspx?sid={0}&thin=1", parentSellerGuid.Value) : string.Empty;

            StoreCollection stores = Presenter.GetStores(sellerGuid);
            if (stores.Count.Equals(0))
                return;

            Literal uiStoresCloseDown = (Literal) e.Row.FindControl("uiStoresCloseDown");
            StringBuilder sb = new StringBuilder();

            foreach(Store store in stores)
            {
                if (store.IsCloseDown)
                    sb.Append(store.StoreName + "結束營業  ");
            }

            uiStoresCloseDown.Text = sb.ToString();
        }
    }
}
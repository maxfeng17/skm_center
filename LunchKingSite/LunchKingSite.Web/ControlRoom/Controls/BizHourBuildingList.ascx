﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BizHourBuildingList.ascx.cs" Inherits="LunchKingSite.Web.ControlRoom.Controls.BizHourBuildingList" %>
<%@ Register Assembly="System.Web.Extensions"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Src="Pager.ascx" TagName="Pager" TagPrefix="uc1" %>
<asp:UpdatePanel id="UPanel" runat="server">
    <contenttemplate>
<table>
    <tr>
        <td>
            搜尋 :
            <asp:DropDownList ID="dplSellerCityBizHour" runat="server" DataTextField="key" DataValueField="value">
            </asp:DropDownList>
            <asp:TextBox ID="tbSearch" runat="server"></asp:TextBox>
            <asp:Button ID="bSearchBtn" runat="server" OnClick="bSearchBtn_Click" Text="搜尋" />
            &nbsp;<asp:Label ID="lblResult" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
        </td>
        <td>
            <aspajax:UpdateProgress id="UProgress1" runat="server" AssociatedUpdatePanelID="UPanel">
                <progresstemplate>
&nbsp;讀取中 <IMG src="../../Themes/PCweb/images/spinner.gif" __designer:dtid="844424930132024" /> 
</progresstemplate>
            </aspajax:UpdateProgress>
        </td>
    </tr>
</table>
<table width="500">
    <tr>
        <td colspan="3">
            <asp:GridView ID="gvSellerInBuildingCity" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                BackColor="LightGoldenrodYellow" BorderColor="Tan" BorderWidth="1px" CellPadding="2"
                DataKeyNames="BusinessHourGuid" Font-Size="Small" ForeColor="Black" GridLines="None"
                OnSorting="gvSellerInBuildingCity_Sorting" PageSize="15" Width="900px">
                <FooterStyle BackColor="Tan" />
                <RowStyle HorizontalAlign="Center" Wrap="False" />
                <Columns>
                    <asp:TemplateField HeaderText="選擇">
                        <HeaderTemplate>
                            <asp:CheckBox ID="cbSellerAddAll" runat="server" AutoPostBack="True" OnCheckedChanged="cbSellerAddAll_CheckedChanged"
                                Text="全選" />
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:CheckBox ID="cbSellerAdd" runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:HyperLinkField DataNavigateUrlFields="SellerGuid" DataNavigateUrlFormatString="~/ControlRoom/Seller/seller_add.aspx?sid={0}"
                        DataTextField="SellerName" HeaderText="賣家名稱" />
                    <asp:TemplateField HeaderText="商品類型">
                        <ItemTemplate>
                            <asp:Label ID="Label1" runat="server" Text='<%# ShowBizHrType(Eval("BusinessHourTypeId")) %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="BuildingName" HeaderText="地區" Visible="False" />
                    <asp:BoundField DataField="SellerAddress" HeaderText="賣家地址" />
                    <asp:TemplateField HeaderText="時間">
                        <ItemTemplate>
                            <asp:Label ID="Label6" runat="server" Text='<%# ShowBizHrOS(Eval("BusinessHourOrderTimeS"), "Type") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="接單開始">
                        <ItemTemplate>
                            <asp:Label ID="Label2" runat="server" Text='<%# ShowBizHrOS(Eval("BusinessHourOrderTimeS"), "Time") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="接單結束">
                        <ItemTemplate>
                            <asp:Label ID="Label3" runat="server" Text='<%# ShowBizHrOS(Eval("BusinessHourOrderTimeE"), "Time") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="外送開始">
                        <ItemTemplate>
                            <asp:Label ID="Label4" runat="server" Text='<%# ShowBizHrOS(Eval("BusinessHourDeliverTimeS"), "Time") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="外送結束">
                        <ItemTemplate>
                            <asp:Label ID="Label5" runat="server" Text='<%# ShowBizHrOS(Eval("BusinessHourDeliverTimeE"), "Time") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <PagerStyle BackColor="PaleGoldenrod" ForeColor="DarkSlateBlue" HorizontalAlign="Center" />
                <SelectedRowStyle BackColor="DarkSlateBlue" ForeColor="GhostWhite" />
                <HeaderStyle BackColor="Tan" Font-Bold="True" HorizontalAlign="Center" Wrap="False" />
                <AlternatingRowStyle BackColor="PaleGoldenrod" />
            </asp:GridView>
        </td>
    </tr>
    <tr>
        <td colspan="3">
            <uc1:Pager ID="gridPager" runat="server" OnGetCount="RetrieveBuildingCount" OnUpdate="UpdateHandler"
                        PageSize="15" />
        </td>
    </tr>
</table></contenttemplate>
</asp:UpdatePanel>

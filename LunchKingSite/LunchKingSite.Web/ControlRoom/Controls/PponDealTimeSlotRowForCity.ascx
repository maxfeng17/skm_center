﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PponDealTimeSlotRowForCity.ascx.cs"
	Inherits="LunchKingSite.Web.ControlRoom.Controls.PponDealTimeSlotRowForCity" %>
<%@ Register Src="PponDealTimeSlotItemForDay.ascx" TagName="PponDealTimeSlotItemForDay"
	TagPrefix="uc1" %>
<asp:Panel ID="PTitle" runat="server" Visible="false">
	<table border="1">
		<tr>
			<td>
				<div style="width: 100px; display: block; text-align: center;">
					<asp:Label class="lbTitle" ID="lbTitle" runat="server"></asp:Label>
				</div>
			</td>
			<td>
				<div style="width: 210px; display: block;">
				    <div class="today">
				        <asp:Label class="lbCol" ID="lbCol0" runat="server"></asp:Label>
                        <input type="button" value="儲存今日排序" style="float:left;display:none" class="saveTodaySeq" refBox="day0"/>
				    </div>
				</div>
			</td>
			<td>
				<div style="width: 210px; display: block;">
					<asp:Label class="lbCol" ID="lbCol1" runat="server"></asp:Label>
                    <input type="button" value="儲存今日排序" style="float:left;display:none" class="saveTodaySeq" refBox="day1" />
                    <input type="button" value="參考昨天" style="float:right; display:none" class="smart" refBoxL="day0" refBoxR="day1" />
				</div>
			</td>
			<td>
				<div style="width: 210px; display: block;">
					<asp:Label class="lbCol" ID="lbCol2" runat="server"></asp:Label>
                    <input type="button" value="儲存今日排序" style="float:left;display:none" class="saveTodaySeq" refBox="day2" />
                    <input type="button" value="參考昨天" style="float:right; display:none" class="smart" refBoxL="day1" refBoxR="day2" />
				</div>
			</td>
			<td>
				<div style="width: 210px; display: block;">
					<asp:Label class="lbCol" ID="lbCol3" runat="server"></asp:Label>
                    <input type="button" value="儲存今日排序" style="float:left;display:none" class="saveTodaySeq" refBox="day3" />
                    <input type="button" value="參考昨天" style="float:right; display:none" class="smart" refBoxL="day2" refBoxR="day3" />
				</div>
			</td>
			<td>
				<div style="width: 210px; display: block;">
					<asp:Label class="lbCol" ID="lbCol4" runat="server"></asp:Label>
                    <input type="button" value="儲存今日排序" style="float:left;display:none" class="saveTodaySeq" refBox="day4" />
                    <input type="button" value="參考昨天" style="float:right; display:none" class="smart" refBoxL="day3" refBoxR="day4" />
				</div>
			</td>
			<td>
				<div style="width: 210px; display: block;">
					<asp:Label class="lbCol" ID="lbCol5" runat="server"></asp:Label>
                    <input type="button" value="儲存今日排序" style="float:left;display:none" class="saveTodaySeq" refBox="day5" />
                    <input type="button" value="參考昨天" style="float:right; display:none" class="smart" refBoxL="day4" refBoxR="day5" />
				</div>
			</td>
			<td>
				<div style="width: 210px; display: block;">
					<asp:Label class="lbCol" ID="lbCol6" runat="server"></asp:Label>
                    <input type="button" value="儲存今日排序" style="float:left;display:none" class="saveTodaySeq" refBox="day6" />
                    <input type="button" value="參考昨天" style="float:right; display:none" class="smart" refBoxL="day5" refBoxR="day6" />
				</div>
			</td>
		</tr>
	</table>
</asp:Panel>
<asp:Panel ID="PData" runat="server" Visible="true">
	<table border="1">
		<tr>
			<td valign="top">
				<div style="width: 100px; display: block; vertical-align: top;">
					<asp:Label ID="lbCity" runat="server"></asp:Label>
				</div>
			</td>
			<td valign="top">
				<div style="width: 210px; display: block; vertical-align: top;" class="day0">
					<uc1:PponDealTimeSlotItemForDay ID="day0" runat="server" />
				</div>
			</td>
			<td valign="top">
				<div style="width: 210px; display: block; vertical-align: top;" class="day1">
					<uc1:PponDealTimeSlotItemForDay ID="day1" runat="server" />
				</div>
			</td>
			<td valign="top">
				<div style="width: 210px; display: block; vertical-align: top;" class="day2">
					<uc1:PponDealTimeSlotItemForDay ID="day2" runat="server" />
				</div>
			</td>
			<td valign="top">
				<div style="width: 210px; display: block; vertical-align: top;" class="day3">
					<uc1:PponDealTimeSlotItemForDay ID="day3" runat="server" />
				</div>
			</td>
			<td valign="top">
				<div style="width: 210px; display: block; vertical-align: top;" class="day4">
					<uc1:PponDealTimeSlotItemForDay ID="day4" runat="server" />
				</div>
			</td>
			<td valign="top">
				<div style="width: 210px; display: block; vertical-align: top;" class="day5">
					<uc1:PponDealTimeSlotItemForDay ID="day5" runat="server" />
				</div>
			</td>
			<td valign="top">
				<div style="width: 210px; display: block; vertical-align: top;" class="day6">
					<uc1:PponDealTimeSlotItemForDay ID="day6" runat="server" />
				</div>
			</td>
		</tr>
	</table>
</asp:Panel>

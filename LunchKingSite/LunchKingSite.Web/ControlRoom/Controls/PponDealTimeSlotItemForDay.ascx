﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PponDealTimeSlotItemForDay.ascx.cs"
    Inherits="LunchKingSite.Web.ControlRoom.Controls.PponDealTimeSlotItemForDay" %>
<%@ Import Namespace="LunchKingSite.Core" %>
<%@ Import Namespace="LunchKingSite.DataOrm" %>
<div id="<%=DivId%>" class="slotItem" >
    <asp:Repeater ID="rList" runat="server" OnItemDataBound="rList_ItemDataBound">
        <ItemTemplate>
                <div class="<%#((ViewPponDealTimeSlot)Container.DataItem).IsLockSeq ? "fixed" : "" %>">
                    <span id="no" style="float:left;width:35px; text-align:center;"><%#Container.ItemIndex + 1%></span>
                    <ul class="unit <%#GetUnitColor((ViewPponDealTimeSlot)Container.DataItem)%>" dataId="<%#GetDataId((ViewPponDealTimeSlot)Container.DataItem)%>" >
		                <li><a class="url" target="_blank"><asp:Literal ID="litName" runat="server" /></a></li>
		                <li>
		                    <label><input type="checkbox" class="noShow" <%#((ViewPponDealTimeSlot)Container.DataItem).Status == (int)DealTimeSlotStatus.NotShowInPponDefault ? "checked" : "" %> <%# (((ViewPponDealTimeSlot)Container.DataItem).BusinessHourStatus&(int)BusinessHourStatus.TmallDeal)>0?"disabled":"" %>/>隱藏</label>
                            <label><input type="checkbox" class="lockSeq" <%#((ViewPponDealTimeSlot)Container.DataItem).IsLockSeq ? "checked" : "" %> />
                                <span class="<%#((ViewPponDealTimeSlot)Container.DataItem).IsLockSeq ? "lockSpan lock_seq_lb" : "lockSpan"%>">鎖定排序</span></label>
                            <label><%#((ViewPponDealTimeSlot)Container.DataItem).IsHotDeal ? "<span class='hotSpan'>HOT</span>" : "" %></label>
		                </li>
	                </ul>
                </div>
        </ItemTemplate>
    </asp:Repeater>
</div>

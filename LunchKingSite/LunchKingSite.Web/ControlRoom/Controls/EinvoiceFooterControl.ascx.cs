﻿using System;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;

namespace LunchKingSite.Web.ControlRoom.Controls
{
    public partial class EinvoiceFooterControl : System.Web.UI.UserControl
    {

        protected ISysConfProvider SysConf = ProviderFactory.Instance().GetConfig();
        public OrderClassification DealType { get; set; }
        public InvoiceVersion Version { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.PreRender += EinvoiceFooterControl_PreRender;
        }

        private void EinvoiceFooterControl_PreRender(object sender, EventArgs e)
        {
            if (DealType == OrderClassification.LkSite)
            {
                if (Version == InvoiceVersion.Old)
                {
                    muView.SetActiveView(viewLkSiteOld);
                }
                else if (Version == InvoiceVersion.CarrierEra)
                {
                    muView.SetActiveView(viewLkSiteCarrierEra);
                }
            }
            else if (DealType == OrderClassification.HiDeal)
            {
                if (Version == InvoiceVersion.Old)
                {
                    muView.SetActiveView(viewPiinlifeOld);
                }
                else if (Version == InvoiceVersion.CarrierEra)
                {
                    muView.SetActiveView(viewPiinlifeCarrierEra);
                }
            }        
        }
    }
}
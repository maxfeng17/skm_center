﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.Core;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;

namespace LunchKingSite.Web.ControlRoom.Controls
{
    public partial class BizHourBuildingList : LocalizedBaseUserControl, IBizHourBuildingListView
    {
        public event EventHandler<DataEventArgs<int>> GetSellerCount;
        public event EventHandler<CommandEventArgs> PageChanged;
        public event EventHandler<CommandEventArgs> SortClicked;
        public event EventHandler<CommandEventArgs> SearchClicked;

        #region props
        private BizHourBuildingListPresenter.BizHourBuildingListType bblpType;
        private BizHourBuildingListPresenter _presenter;
        public BizHourBuildingListPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }
        public Guid BuildingGuid
        {
            get
            {
                if (!string.IsNullOrEmpty(Request["bid"]))
                {
                    try
                    {
                        return new Guid(Request.QueryString["bid"]);
                    }
                    catch
                    {
                        return Guid.Empty;
                    }
                }
                else
                    return Guid.Empty;
            }
        }
        public string SortExpression
        {
            get
            {
                return (ViewState != null && ViewState["sebc"] != null) ? (string)ViewState["sebc"] : ViewSellerCityBizHour.Columns.SellerId + " ASC";
            }
            set
            {
                ViewState["sebc"] = value;
            }
        }
        public int PageSize
        {
            get { return (ViewState != null && ViewState["ps"] != null) ? (int)ViewState["ps"] : BizHourBuildingListPresenter.DEFAULT_PAGE_SIZE; }
            set { ViewState["ps"] = value; }
        }
        public int CurrentPage
        {
            get { return (!string.IsNullOrEmpty(((DropDownList)gridPager.FindControl("ddlPages")).SelectedValue)) ? int.Parse(((DropDownList)gridPager.FindControl("ddlPages")).SelectedValue) : gridPager.CurrentPage; }
        }
        public string FilterType
        {
            get { return ViewSellerCityBizHour.Columns.SellerName; }
            set { dplSellerCityBizHour.SelectedValue = value; }
        }
        public string Filter
        {
            get
            {
                return tbSearch.Text;
            }
            set
            {
                tbSearch.Text = value;
            }
        }
        public BizHourBuildingListPresenter.BizHourBuildingListType QueryType
        {
            get { return bblpType; }
            set { bblpType = value; }
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                gvSellerInBuildingCity.Columns[1].SortExpression = ViewBuildingSellerDeliveryHour.Columns.SellerName;
                gvSellerInBuildingCity.Columns[2].SortExpression = ViewBuildingSellerDeliveryHour.Columns.BusinessHourTypeId;
                gvSellerInBuildingCity.Columns[3].SortExpression = ViewBuildingSellerDeliveryHour.Columns.SellerAddress;
                gvSellerInBuildingCity.Columns[4].SortExpression = ViewBuildingSellerDeliveryHour.Columns.SellerTel;
                gvSellerInBuildingCity.Columns[6].SortExpression = ViewBuildingSellerDeliveryHour.Columns.BusinessHourOrderTimeS;
                gvSellerInBuildingCity.Columns[7].SortExpression = ViewBuildingSellerDeliveryHour.Columns.BusinessHourOrderTimeE;
                gvSellerInBuildingCity.Columns[8].SortExpression = ViewBuildingSellerDeliveryHour.Columns.BusinessHourDeliverTimeS;
                gvSellerInBuildingCity.Columns[9].SortExpression = ViewBuildingSellerDeliveryHour.Columns.BusinessHourDeliverTimeE;

                Presenter.OnViewInitialized();
            }
            Presenter.OnViewLoaded();
        }

        protected string ShowBizHrType(object type)
        {
            return Helper.GetLocalizedEnum(Resources.Localization.ResourceManager, (BusinessHourType)type);
        }

        protected string ShowBizHrOS(object type, string Mode)
        {
            string carrytype = string.Empty;
            string ordertime = ((DateTime)type).ToString("HH:mm");

            if (((DateTime)type).Day <= 1)
                carrytype = Resources.Localization.CarryToday;
            else
                carrytype = Resources.Localization.CarryTomorrow;

            if (Mode == "Type")
                return carrytype;
            else
                return ordertime;
        }

        public void SetSellerCityBizHourList(ViewSellerCityBizHourCollection SellerCityBizHourList)
        {
            gvSellerInBuildingCity.DataSource = SellerCityBizHourList;
            gvSellerInBuildingCity.DataBind();
        }

        protected void gvSellerInBuildingCity_Sorting(object sender, GridViewSortEventArgs e)
        {
            ViewState["sdbc"] = ((string)ViewState["sdbc"] == " ASC") ? " DESC" : " ASC";
            this.SortExpression = e.SortExpression + (string)ViewState["sdbc"];
            if (this.SortClicked != null)
                this.SortClicked(this, new CommandEventArgs("gvSellerInBuildingCity", null));
        }

        public void SetSearchDropDown(Dictionary<string, string> SearchType)
        {
            dplSellerCityBizHour.DataSource = SearchType;
            dplSellerCityBizHour.DataBind();
        }

        protected int RetrieveBuildingCount()
        {
            DataEventArgs<int> e = new DataEventArgs<int>(0);
            if (GetSellerCount != null)
            {
                GetSellerCount(this, e);
            }
            return e.Data;
        }

        protected void UpdateHandler(int pageNumber)
        {
            if (this.PageChanged != null)
                this.PageChanged(this, new CommandEventArgs("gvSellerInBuildingCity", pageNumber)); ;
        }

        protected void bSearchBtn_Click(object sender, EventArgs e)
        {
            if (dplSellerCityBizHour.SelectedItem.Value == "build")
                this.QueryType = BizHourBuildingListPresenter.BizHourBuildingListType.HasGUID;
            else
                this.QueryType = BizHourBuildingListPresenter.BizHourBuildingListType.JustSeller;
            
            if (this.SearchClicked != null)
                this.SearchClicked(this, new CommandEventArgs("gvSellerInBuildingCity", ViewSellerCityBizHour.Columns.SellerName));
            gridPager.ResolvePagerView(1, true);
        }

        protected void cbSellerAddAll_CheckedChanged(object sender, EventArgs e)
        {
            foreach (GridViewRow gd in gvSellerInBuildingCity.Rows)
            {
                if (((CheckBox)gvSellerInBuildingCity.HeaderRow.FindControl("cbSellerAddAll")).Checked)
                    ((CheckBox)gd.Cells[0].FindControl("cbSellerAdd")).Checked = true;
                else
                    ((CheckBox)gd.Cells[0].FindControl("cbSellerAdd")).Checked = false;
            }
        }
    }
}
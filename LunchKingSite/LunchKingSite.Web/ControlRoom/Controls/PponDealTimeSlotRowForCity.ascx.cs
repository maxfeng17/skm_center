﻿using System;
using System.Collections.Generic;
using LunchKingSite.DataOrm;
using System.Web.UI;

namespace LunchKingSite.Web.ControlRoom.Controls
{
    public partial class PponDealTimeSlotRowForCity : UserControl
    {

        private bool _isTitle = false;
        public bool IsTitle
        {
            get
            {
                return _isTitle;
            }
            set
            {
                _isTitle = value;
                if (_isTitle)
                {
                    PTitle.Visible = true;
                    PData.Visible = false;
                }
                else
                {
                    PTitle.Visible = false;
                    PData.Visible = true;
                }
            }
        }


        public void SetDayData(ViewPponDealTimeSlotCollection dealCollection, int dateIndex, string divId, IList<Guid> newDeals)
        {
            switch (dateIndex)
            {
                case 0:
                    day0.showData(dealCollection, divId, newDeals);
                    break;
                case 1:
                    day1.showData(dealCollection, divId, newDeals);
                    break;
                case 2:
                    day2.showData(dealCollection, divId, newDeals);
                    break;
                case 3:
                    day3.showData(dealCollection, divId, newDeals);
                    break;
                case 4:
                    day4.showData(dealCollection, divId, newDeals);
                    break;
                case 5:
                    day5.showData(dealCollection, divId, newDeals);
                    break;
                case 6:
                    day6.showData(dealCollection, divId, newDeals);
                    break;
            }
        }
        public void SetCityName(string cityName)
        {
            lbCity.Text = cityName;
        }

        public void SetTitle(string title, int dateIndex)
        {
            switch (dateIndex)
            {
                case 0:
                    lbCol0.Text = title;
                    break;
                case 1:
                    lbCol1.Text = title;
                    break;
                case 2:
                    lbCol2.Text = title;
                    break;
                case 3:
                    lbCol3.Text = title;
                    break;
                case 4:
                    lbCol4.Text = title;
                    break;
                case 5:
                    lbCol5.Text = title;
                    break;
                case 6:
                    lbCol6.Text = title;
                    break;

            }
        }




        protected void Page_Load(object sender, EventArgs e)
        {
            lbTitle.Text = string.Format("{0} 年",DateTime.Now.Year);
        }
    }
}
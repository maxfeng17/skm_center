﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;

namespace LunchKingSite.Web.ControlRoom.Controls
{
    public partial class EinvoiceControl : System.Web.UI.UserControl
    {
        protected static ISysConfProvider conf = LunchKingSite.Core.Component.ProviderFactory.Instance().GetConfig();
        /// <summary>
        /// 發票類型
        /// </summary>
        public string InvoiceMode
        {
            get { return ddlEinvoiceMode.SelectedValue; }
            set { ddlEinvoiceMode.SelectedValue = value; }
        }
        /// <summary>
        /// 是否捐贈
        /// </summary>
        public string InvoiceDonateMark
        {
            get { return litInvoiceDonateMark.Text; }
            set { litInvoiceDonateMark.Text = value; }
        }
        /// <summary>
        /// 索取紙本
        /// </summary>
        public string InvoicePaperMark
        {
            get { return litInvoiceDonateMark.Text; }
            set
            {
                litInvoicePaperMark.Text = value;
                if (_einvoice.Version == (int)InvoiceVersion.CarrierEra)
                {
                    rowInvoicePaperMark.Visible = false;
                }
            }
        }
        /// <summary>
        /// 使用載具
        /// </summary>
        public string CarrierInfo
        {
            get { return litCarrierInfo.Text; }
            set
            {
                litCarrierInfo.Text = value;
                ddlCarrierType.Visible = false;
                txtCarrierId.Visible = false;
            }
        }

        public string CarrierId
        {
            get { return txtCarrierId.Text; }
            set { txtCarrierId.Text = value; }
        }

        private EinvoiceMain _einvoice;

        public CarrierType CarrierType
        {
            get
            {
                CarrierType val;
                Enum.TryParse(ddlCarrierType.SelectedValue, out val);
                return val;
            }
            set
            {
                if (value == CarrierType.None && _einvoice.Version == (int)InvoiceVersion.Old)
                {
                    litCarrierInfo.Visible = true;
                    ddlCarrierType.Visible = false;
                }
                else
                {
                    ddlCarrierType.Visible = true;
                    litCarrierInfo.Visible = false;
                    ddlCarrierType.SelectedValue = ((int)value).ToString();
                }
            }
        }

        /// <summary>
        /// 收件人
        /// </summary>
        public string BuyerName
        {
            get { return txtBuyerName.Text.Trim(); }
            set { txtBuyerName.Text = value; }
        }

        /// <summary>
        /// 收件地址
        /// </summary>
        public string BuyerAddress
        {
            get { return txtInvoiceAddress.Text.Trim(); }
            set { txtInvoiceAddress.Text = value; }
        }

        /// <summary>
        /// 統編
        /// </summary>
        public string CompanyId
        {
            get { return txtInvoiceCompanyId.Text.Trim(); }
            set { txtInvoiceCompanyId.Text = value; }
        }

        /// <summary>
        /// 抬頭
        /// </summary>
        public string CompanyName
        {
            get { return txtInvoiceTitle.Text.Trim(); }
            set { txtInvoiceTitle.Text = value; }
        }


        public InvoiceVersion Version
        {
            get
            {
                //return (InvoiceVersion)_einvoice.Version;
                if (ViewState["InvoiceVersion"] == null)
                {
                    throw new Exception("InvoiceVersion is null");
                }
                InvoiceVersion val;
                Enum.TryParse(ViewState["InvoiceVersion"].ToString(), out val);
                return val;
            }
            set
            {
                ViewState["InvoiceVersion"] = value;
            }
        }

        public int UserId
        {
            get
            {
                //return (InvoiceVersion)_einvoice.Version;
                if (ViewState["UserId"] == null)
                {
                    throw new Exception("UserId is null");
                }
                int val;
                int.TryParse(ViewState["UserId"].ToString(), out val);
                return val;
            }
            set
            {
                ViewState["UserId"] = value;
            }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
        }

        private void BuildDdlEinvoiceMode()
        {
            ddlEinvoiceMode.Items.Clear();

            ListItem item = new ListItem("請選擇", string.Empty);
            item.Attributes.Add("disabled", "disabled");
            ddlEinvoiceMode.Items.Add(item);

            ListItem item2 = new ListItem("二聯式發票", ((int)InvoiceMode2.Duplicate).ToString());
            ddlEinvoiceMode.Items.Add(item2);

            ListItem item3 = new ListItem("三聯式發票", ((int)InvoiceMode2.Triplicate).ToString());
            ddlEinvoiceMode.Items.Add(item3);
        }

        private void SetEditMode(EinvoiceControlEditMode range, bool setTitle = false)
        {
            if (range == EinvoiceControlEditMode.ReadOnly)
            {
                ddlEinvoiceMode.Enabled = false;
                ddlEinvoiceMode.CssClass = "invoice-readonly";
                //txtBuyerName.Enabled = false;
                //txtBuyerName.CssClass = "invoice-readonly";
                //txtInvoiceTitle.Enabled = false;
                //txtInvoiceTitle.CssClass = "invoice-readonly";

                txtInvoiceCompanyId.Enabled = false;
                txtInvoiceCompanyId.CssClass = "invoice-readonly";
                //txtInvoiceAddress.Enabled = true;
                //txtInvoiceAddress.CssClass = "invoice-readonly";
                ddlCarrierType.Enabled = false;
                ddlCarrierType.CssClass = "invoice-readonly";
                txtCarrierId.Enabled = false;
                txtCarrierId.CssClass = "invoice-readonly";
            }
            else if (range == EinvoiceControlEditMode.DuplicateMember)
            {
                ddlEinvoiceMode.Enabled = true;
                //txtBuyerName.Enabled = false;
                //txtInvoiceAddress.Enabled = false;
                txtInvoiceTitle.Enabled = false;

                txtInvoiceCompanyId.Enabled = false;
                ddlCarrierType.Enabled = true;
                txtCarrierId.Enabled = false;
            }
            else if (range == EinvoiceControlEditMode.DuplicateCarrier)
            {
                ddlEinvoiceMode.Enabled = true;
                //txtBuyerName.Enabled = false;
                //txtInvoiceAddress.Enabled = false;
                txtInvoiceTitle.Enabled = false;

                txtInvoiceCompanyId.Enabled = false;
                ddlCarrierType.Enabled = true;
                txtCarrierId.Enabled = true;
            }
            else if (range == EinvoiceControlEditMode.Triplicate)
            {
                ddlEinvoiceMode.Enabled = false;
                ddlEinvoiceMode.CssClass = "invoice-readonly";

                txtInvoiceTitle.Enabled = true;

                txtInvoiceCompanyId.Enabled = true;
                //txtInvoiceCompanyId.CssClass = "invoice-readonly";
                ddlCarrierType.Enabled = false;
                ddlCarrierType.CssClass = "invoice-readonly";
                txtCarrierId.Enabled = false;
                txtCarrierId.CssClass = "invoice-readonly";
            }
            else if (range == EinvoiceControlEditMode.DuplicatePaper)
            {
                ddlEinvoiceMode.Enabled = true;
                //txtBuyerName.Enabled = true;
                //txtInvoiceAddress.Enabled = true;
                txtInvoiceTitle.Enabled = false;

                txtInvoiceCompanyId.Enabled = false;
                ddlCarrierType.Enabled = true;
                txtCarrierId.Enabled = false;
            }
        }

        public void SetData(EinvoiceMain einvoice, IList<EinvoiceMain> einvCol)
        {
            this._einvoice = einvoice;

            this.Version = (InvoiceVersion)einvoice.Version;
            this.UserId = einvoice.UserId;

            this.BuildDdlEinvoiceMode();

            this.InvoicePaperMark = einvoice.IsPrintMark ? "是" : "否";
            this.InvoiceDonateMark = einvoice.IsDonateMark ? "是" : "否";

            if (einvoice.IsDonateMark)
            {
                ddlEinvoiceMode.Items.Clear();
                ListItem item = new ListItem("請選擇", string.Empty);
                ddlEinvoiceMode.Items.Add(item);
                ListItem item3 = new ListItem("三聯式發票", ((int)InvoiceMode2.Triplicate).ToString());
                ddlEinvoiceMode.Items.Add(item3);

                this.InvoiceDonateMark += ", " + EinvoiceFacade.GetDonateCodeName(einvoice);
                this.CarrierInfo = "否";
                this.SetEditMode(EinvoiceControlEditMode.Donate);
                return;
            }

            if (einvoice.IsEinvoice3)
            {
                this.CompanyId = einvoice.InvoiceComId;
                this.CompanyName = einvoice.InvoiceComName;
                this.BuyerAddress = einvoice.InvoiceBuyerAddress;
                this.BuyerName = einvoice.InvoiceBuyerName;
                this.InvoiceMode = ((int)InvoiceMode2.Triplicate).ToString();
                this.SetEditMode(EinvoiceControlEditMode.Triplicate);
            }
            else if (einvoice.IsEinvoice2)
            {
                this.BuyerAddress = einvoice.InvoiceBuyerAddress;
                this.BuyerName = einvoice.InvoiceBuyerName;
                this.InvoiceMode = ((int)InvoiceMode2.Duplicate).ToString();
                int nextMon = einvoice.InvoiceNumberTime != null ? (einvoice.InvoiceNumberTime.Value.Month % 2 == 0 ? 1 : 2) : 0;
                if (einvoice.IsIntertemporal && einvoice.InvoiceNumberTime.Value.GetFirstDayOfMonth().AddMonths(nextMon).AddDays(conf.InvoiceBufferDays) < DateTime.Now)
                {
                    this.SetEditMode(EinvoiceControlEditMode.ReadOnly);
                }
                else if (einvoice.CarrierType == (int)CarrierType.None)
                {
                    this.SetEditMode(EinvoiceControlEditMode.DuplicatePaper);
                }
                else if (einvoice.CarrierType == (int)CarrierType.Member)
                {
                    this.SetEditMode(EinvoiceControlEditMode.DuplicateMember);
                }
                else
                {
                    this.SetEditMode(EinvoiceControlEditMode.DuplicateCarrier);
                }
            }

            if (einvoice.Version == (int)InvoiceVersion.Old)
            {
                this.CarrierInfo = "否";
            }
            else if (einvoice.Version == (int)InvoiceVersion.CarrierEra)
            {
                this.CarrierType = (CarrierType)einvoice.CarrierType;
                if (this.CarrierType == CarrierType.Phone || this.CarrierType == CarrierType.PersonalCertificate)
                {
                    this.CarrierId = einvoice.CarrierId;
                }
            }
            hfCarrierInfo.Value = (einvoice.InvoiceStatus == (int)EinvoiceType.C0701 || einvoice.InvoiceStatus == (int)EinvoiceType.Initial || einvoice.InvoiceMode2 == (int)InvoiceMode2.Triplicate) ? string.Empty : (int)CarrierType + "|" + CarrierId;
            hfComInfo.Value = einvoice.InvoiceMode2 == (int)InvoiceMode2.Triplicate && !string.IsNullOrEmpty(einvoice.InvoiceNumber) ? einvoice.InvoiceComId + "|" + einvoice.InvoiceComName : string.Empty;
        }
    }
    public enum EinvoiceControlEditMode
    {
        ReadOnly,
        DuplicateCarrier,
        DuplicatePaper,
        DuplicateMember,
        Triplicate,
        Donate
    }
}
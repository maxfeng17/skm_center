﻿<%@ Page Language="C#" MasterPageFile="~/ControlRoom/backend.master" AutoEventWireup="true" CodeBehind="ATMTransfer.aspx.cs" Inherits="LunchKingSite.Web.ControlRoom.Atm.ATMTransfer" %>

<%@ Import Namespace="LunchKingSite.DataOrm" %>
<%@ Import Namespace="LunchKingSite.Core.ModelPartial" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript">
        function check() {
            if ($("#txtVirtualAccount").val() == "") {
                alert("請輸入虛擬帳號");
                return false;
            }
            if ($("#txtAmount").val() == "") {
                alert("請輸入交易金額");
                return false;
            }
            if (isNaN($("#txtAmount").val())) {
                alert("交易金額須為數字");
                return false;
            }
        }
    </script>
    <fieldset>
        <asp:Label ID="lblVirtualAccount" runat="server" Text="虛擬帳號："></asp:Label><asp:TextBox ID="txtVirtualAccount" runat="server" ClientIDMode="Static" placeholder="95948725107149，不能有-"></asp:TextBox><br />
        <asp:Label ID="lblAmount" runat="server" Text="交易金額："></asp:Label><asp:TextBox ID="txtAmount" runat="server" ClientIDMode="Static"></asp:TextBox><br />
        <asp:Button ID="btnSearch" runat="server" Text="模擬轉帳" OnClick="btnTransfer_Click" OnClientClick="return check()" /><br />
        <asp:Label ID="lblResult" runat="server" Text="結果"></asp:Label>
    </fieldset>
</asp:Content>

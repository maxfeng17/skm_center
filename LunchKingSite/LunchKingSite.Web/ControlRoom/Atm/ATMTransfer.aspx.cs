﻿using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.UI;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Transactions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LunchKingSite.Web.ControlRoom.Atm
{
    public partial class ATMTransfer : RolePage
    {
        ISysConfProvider config = ProviderFactory.Instance().GetConfig();

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnTransfer_Click(object sender, EventArgs e)
        {
            string virtualAccount = txtVirtualAccount.Text.Trim();
            int amount = 0;
            int.TryParse(txtAmount.Text, out amount);
            string transferDate = (Convert.ToInt32(DateTime.Now.ToString("yyyyMMdd")) - 19110000).ToString();
            if (!string.IsNullOrEmpty(virtualAccount))
            {
                string transactionNo = DateTime.Now.ToString("yyyyMMddHHmmss00");
                string msgID = "2";
                string body = "";

                body = string.Format("{0}{1}{2}{3}{4}{5}{6}{7}{8}{9}{10}{11}{12}{13}{14}{15}{16}{17}{18}",
                    "624540095223",//轉入實體帳號 0-12
                    msgID,//帳務別 12-1
                    transferDate,//記帳日期 13-7
                    virtualAccount.Substring(5).PadRight(14, ' '),//繳款人識別碼 20-14
                    amount.ToString().PadLeft(13, '0'),//交易金額 34-13
                    "00", //47-2
                    transferDate,//交易日期 49-7
                    DateTime.Now.ToString("HHmmss"),//交易時間 56-6
                    "8227715525", //跨行序號 62-10
                    "C", //借貸別 72-1
                    "+", //正負別 73-1
                    "98925", //櫃員代號 74-5
                    "04045", //交易代碼 79-5
                    "4", //交易設備種類 84-1
                    "000000001151**5145*", //轉出帳號 85-19
                    "".PadLeft(11, ' '), //註記二 104-11
                    "95931000000000000000000000000000   ", //前五碼為企業識碼，其餘為保留欄位 115-35
                    "".PadLeft(80, ' '), //付款人姓名 150-80
                    "".PadLeft(80, ' ') //匯款附言 230-80
                    );

                int returncode = 543;

                using (TransactionScope ts = new TransactionScope())
                {
                    returncode = PaymentFacade.CtAtmLogSave(transactionNo, msgID, body);
                    if (returncode != 543)
                        ts.Complete();
                }

                if (returncode != 543)
                {
                    lblResult.Text = "轉帳成功";
                }
                else
                {
                    lblResult.Text = "轉帳失敗";
                }


                //Dictionary<string, string> dic = new Dictionary<string, string>();
                //dic.Add("TransactionNo", transactionNo);
                //dic.Add("MsgID", msgID);
                //dic.Add("body", body);

                //string param = "";
                //string retVal = "";
                //foreach (string key in dic.Keys)
                //{
                //    param += key + "=" + dic[key] + "&";
                //}
                //if (param.Length > 0)
                //{
                //    param = param.Substring(0, param.Length - 1);
                //}

                //byte[] bs = Encoding.UTF8.GetBytes(param);
                //HttpWebRequest req = WebRequest.Create(config.SSLSiteUrl + "/ATMNotify.aspx") as HttpWebRequest;
                //req.Method = "POST";
                //req.ContentType = "application/x-www-form-urlencoded";
                //req.ContentLength = bs.Length;
                //ServicePointManager.ServerCertificateValidationCallback =
                //        delegate { return true; };

                //using (Stream reqStream = req.GetRequestStream())
                //{
                //    reqStream.Write(bs, 0, bs.Length);
                //}
                //try
                //{
                //    using (HttpWebResponse response = req.GetResponse() as HttpWebResponse)
                //    {
                //        StreamReader reader = new StreamReader(response.GetResponseStream());
                //        retVal = reader.ReadToEnd();
                //    }
                //}
                //catch
                //{
                //    throw;
                //}

            }
        }
    }
}
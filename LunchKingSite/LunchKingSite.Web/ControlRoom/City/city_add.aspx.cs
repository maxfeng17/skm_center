﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Component;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;

public partial class city_add : RolePage, ICityAddView
{
    public event EventHandler SearchCity;
    public event EventHandler<DataEventArgs<CityInfo>> AddCity;
    public event EventHandler<DataEventArgs<City>> UpdateCity;
    public event EventHandler<DataEventArgs<int>> DeleteCity;

    #region props
    private CityAddPresenter _presenter;
    public CityAddPresenter Presenter
    {
        set
        {
            this._presenter = value;
            if (value != null)
                this._presenter.View = this;
        }
        get
        {
            return this._presenter;
        }
    }
    public int SelectCity
    {
        get { return int.Parse(ddlCity.SelectedValue); }
    }

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
            this.Presenter.OnViewInitialized();
        this.Presenter.OnViewLoaded();
    }

    public void SetCityDropDown(Dictionary<int, string> city)
    {
        ddlCity.DataSource = city;
        ddlCity.DataBind();
    }

    public void SetUpdatePanel(bool b)
    {
        UpdateCityPanel.Visible = b;
    }

    public void SetImageFile(HttpPostedFile pFile, string destFileName)
    {
        ImageUtility.UploadFile(pFile.ToAdapter(), UploadFileType.CityPhoto, "city", destFileName);
    }

    protected void btnCityAdd_Click(object sender, EventArgs e)
    {
        City c = new City();
        CityInfo ci = new CityInfo();

        c.ParentId = int.Parse(ddlCity.SelectedValue);
        c.CityName = tbZone.Text;
        c.Code = tbCode.Text;
        ci.City = c;

        if (newPhotoUpload.HasFile)
            ci.PFile = newPhotoUpload.PostedFile;

        AddCity(this, new DataEventArgs<CityInfo>(ci));
    }

    protected void btnZoneSearch_Click(object sender, EventArgs e)
    {
        if (this.SearchCity != null)
            this.SearchCity(this, e);
    }

    #region gvZone
    public void SetCityList(CityCollection c)
    {
        gvZone.DataSource = c;
        gvZone.DataBind();
    }

    protected string SetImagePath(object value)
    {
        if (value != null)
            return ImageFacade.GetMediaPath(value.ToString(), MediaType.CityPhoto);
        else
            return string.Empty;
    }

    protected void gvZone_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Footer)
            e.Row.Cells[5].Text = "共" + gvZone.Rows.Count.ToString() + "筆資料";
    }

    protected void gvZone_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        if (DeleteCity != null)
            DeleteCity(this, new DataEventArgs<int>(int.Parse(gvZone.DataKeys[e.RowIndex].Value.ToString())));
    }

    protected void gvZone_RowEditing(object sender, GridViewEditEventArgs e)
    {
        SetUpdatePanel(true);
        tbUpdateZone.Text = ((Label)gvZone.Rows[e.NewEditIndex].FindControl("lbZone")).Text;
        tbUpdateCode.Text = ((Label)gvZone.Rows[e.NewEditIndex].FindControl("lbCode")).Text;
        hid.Value = gvZone.DataKeys[e.NewEditIndex].Value.ToString();
    }
    #endregion

    protected void btnCityUpdate_Click(object sender, EventArgs e)
    {
        City info = new City();

        info.Id = int.Parse(hid.Value);
        info.CityName = tbUpdateZone.Text;
        info.Code = tbUpdateCode.Text;
        if (photoUpload.HasFile)
        {
            HttpPostedFile pFile = photoUpload.PostedFile;
            info.ImgPath = string.Format("city,{0}.{1}", info.Id, Helper.GetExtensionByContentType(pFile.ContentType));
            SetImageFile(pFile, info.Id.ToString());
        }
        else
            info.ImgPath = null;

        UpdateCity(this, new DataEventArgs<City>(info));
        gvZone.EditIndex = -1;
    }
}

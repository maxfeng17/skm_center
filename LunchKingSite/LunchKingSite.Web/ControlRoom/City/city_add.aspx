﻿<%@ Page Language="C#" MasterPageFile="~/ControlRoom/backend.master" AutoEventWireup="true"
    CodeBehind="city_add.aspx.cs" Inherits="city_add" %>

<%@ Register Src="../Controls/BizHourBuildingList.ascx" TagName="BizHourBuildingList"
    TagPrefix="uc3" %>
<%@ Register Src="../Controls/BuildingList.ascx" TagName="BuildingList" TagPrefix="uc2" %>
<%@ Register Src="../Controls/Pager.ascx" TagName="Pager" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="System.Web.Extensions" Namespace="System.Web.UI" TagPrefix="aspajax" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="ContentPlaceHolder1">

    <script language="javascript" type="text/javascript">
        function delete_confirm(e) {
            if (event.srcElement.outerText == "刪除")
                event.returnValue = confirm("確定刪除?");
        }
        document.onclick = delete_confirm;         
    </script>

    <table>
        <tr>
            <td colspan="7">
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td rowspan="2" style="font-size: x-large" nowrap="nowrap" valign="top">
                所屬City:
            </td>
            <td rowspan="2" valign="top">
                <aspajax:UpdatePanel ID="uPanel3" runat="server">
                    <contenttemplate>
                        <asp:DropDownList ID="ddlCity" runat="server" AutoPostBack="True" DataTextField="Value"
                            DataValueField="Key" Font-Size="X-Large">
                        </asp:DropDownList>
                    </contenttemplate>
                </aspajax:UpdatePanel>
            </td>
            <td nowrap="nowrap">
                名稱 :
            </td>
            <td>
                <asp:TextBox ID="tbZone" runat="server" Width="100px"></asp:TextBox>
            </td>
            <td nowrap="nowrap">
                編碼
            </td>
            <td colspan="2">
                <asp:TextBox ID="tbCode" runat="server" Width="50px"></asp:TextBox>
            </td>
            <td>
                上傳照片
                <asp:FileUpload ID="newPhotoUpload" runat="server" />
                *jpeg&amp;gif
            </td>
            <td>
                <asp:Button ID="btnCityAdd" runat="server" Text="新增" OnClick="btnCityAdd_Click" />
            </td>
        </tr>
        <tr>
            <td colspan="6" style="text-align: right" class="style1">
                <asp:Label ID="lbNewLevelAddress" runat="server" Visible="False"></asp:Label>
                先選取左方City, 即可查詢底下所有Zone
            </td>
            <td class="style1">
                <asp:Button ID="btnZoneSearch" runat="server" Text="查詢" OnClick="btnZoneSearch_Click" />
            </td>
        </tr>
        <tr>
            <td colspan="6" valign="top">
                <asp:GridView ID="gvZone" runat="server" AutoGenerateColumns="False" CellPadding="6"
                    EmptyDataText="無符合的資料" Font-Size="Small" GridLines="None" PageSize="10" OnRowDeleting="gvZone_RowDeleting"
                    OnRowEditing="gvZone_RowEditing" ShowFooter="True" OnRowDataBound="gvZone_RowDataBound"
                    DataKeyNames="Id">
                    <FooterStyle BackColor="#FFFFCC" Font-Bold="True" ForeColor="Black" BorderColor="Black" />
                    <Columns>
                        <asp:TemplateField HeaderText="名稱">
                            <EditItemTemplate>
                                <asp:TextBox ID="tbZone" runat="server" Text='<%# Bind("CityName") %>'></asp:TextBox>
                                <br />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="tbZone"
                                    ErrorMessage="名稱" ValidationGroup="vg1" runat="server" />
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbZone" runat="server" Text='<%# Bind("CityName") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="20%" Wrap="False" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="編碼">
                            <EditItemTemplate>
                                <asp:TextBox ID="tbCode" runat="server" Text='<%# Bind("Code") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbCode" runat="server" Text='<%# Bind("Code") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="20%" />
                        </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="圖片">
                                                        <ItemTemplate>
                                                            <asp:Image ID="iPhoto" runat="server" ImageUrl='<%# SetImagePath(Eval("ImgPath")) %>' />
                                                            <asp:HiddenField ID="hfPhoto" runat="server" Value='<%# Eval("ImgPath") %>' />
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <asp:FileUpload ID="EditUpload" runat="server" /><asp:HiddenField ID="hfPhoto"
                                                                runat="server" Value='<%# Eval("ItemPath") %>' />
                                                            <asp:HiddenField ID="hfRowPath" runat="server" />
                                                        </EditItemTemplate>
                                                        <HeaderStyle Wrap="False" />
                                                        <ItemStyle Wrap="False" />
                                                    </asp:TemplateField>
                        <asp:TemplateField HeaderText="Status" Visible="false">
                            <EditItemTemplate>
                                <asp:TextBox ID="tbStatus" runat="server" Text='<%# Eval("Code") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lbStatus" runat="server" Text='<%# Eval("Code" ) %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ShowHeader="False">
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkButton3" runat="server" CausesValidation="False" CommandName="Edit"
                                    Text="編輯"></asp:LinkButton>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CommandName="Update"
                                    Text="更新" OnClientClick="return confirm('確定更新這筆記錄?');" ValidationGroup="vg1">    
                                                                                                           
                                </asp:LinkButton>
                                <asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CommandName="Cancel"
                                    Text="取消"></asp:LinkButton>
                            </EditItemTemplate>
                            <ItemStyle Wrap="True" />
                        </asp:TemplateField>
                        <asp:TemplateField ShowHeader="False">
                            <ItemTemplate>
                                <asp:LinkButton ID="LinkButton4" runat="server" CausesValidation="False" CommandName="Delete"
                                    Text="刪除">
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
                    <SelectedRowStyle BackColor="#FFCC66" ForeColor="Navy" Font-Bold="True" />
                    <HeaderStyle BackColor="#990000" Font-Bold="True" Wrap="False" ForeColor="White" />
                    <AlternatingRowStyle BackColor="White" />
                    <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
                </asp:GridView>
            </td>
            <td colspan="3" valign="top">
                <asp:Panel ID="UpdateCityPanel" runat="server" HorizontalAlign="Right">
                    <table>
                        <tr>
                            <td align="left">
                                名稱 :&nbsp;
                            </td>
                            <td align="left">
                                <asp:TextBox ID="tbUpdateZone" runat="server" Width="124px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                編碼:
                            </td>
                            <td align="left">
                                <asp:TextBox ID="tbUpdateCode" runat="server" Width="124px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                上傳照片
                            </td>
                            <td align="left">
                                <asp:FileUpload ID="photoUpload" runat="server" />
                                *jpeg&amp;gif
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td align="right">
                                <asp:HiddenField ID="hid" runat="server" />
                                <asp:Button ID="btnCityUpdate" runat="server" OnClick="btnCityUpdate_Click" Text="修改" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="SSC">
    <style type="text/css">
        .style1
        {
            height: 17px;
        }
    </style>
</asp:Content>

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ControlRoom/backend.master" AutoEventWireup="true"
    CodeBehind="WeeklyPayReport.aspx.cs" Inherits="LunchKingSite.Web.ControlRoom.Verification.WeeklyPayReport" %>

<%@ Import Namespace="LunchKingSite.DataOrm" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <style>
        .head
        {
            background-color: #688E45;
            font-weight: bolder;
            color: White;
        }
        .head2
        {
            background-color: #F0AF13;
            color: #688E45;
            font-weight: bolder;
            vertical-align: top;
        }
        .row
        {
            background-color: #B3D5F0;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table style="width: 1000px">
        <tr>
            <td colspan="3" class="head" style="text-align: center">
                檔次出帳報表
            </td>
        </tr>
        <tr>
            <td class="head2" colspan="3" align="right">
                <asp:Button ID="btn_PayByBillAchReport" runat="server" Text="預產ACH匯款檔(先收單據後付款)" OnClick="GenPayByBillAchReport" />
            </td>
        </tr>
        <tr>
            <td class="head2">
                報表起始時間
            </td>
            <td class="row">
                <asp:TextBox ID="tbx_DateStart" runat="server"></asp:TextBox>&nbsp;00:00:00
                <cc1:CalendarExtender ID="ce_Start" TargetControlID="tbx_DateStart" runat="server">
                </cc1:CalendarExtender>
                ~
                <asp:TextBox ID="tbx_DateEnd" runat="server"></asp:TextBox>&nbsp;23:59:59
                <cc1:CalendarExtender ID="ce_End" TargetControlID="tbx_DateEnd" runat="server">
                </cc1:CalendarExtender>
            </td>
            <td class="row">
                <asp:Button ID="btn_SearchReport" runat="server" Text="查詢統計" OnClick="SearchReport" />
            </td>
        </tr>
        <tr>
            <td class="head2">
                <asp:DropDownList ID="ddlSelect" runat="server">
                    
                </asp:DropDownList>
            </td>
            <td class="row">
                <asp:TextBox ID="tbx_Bid" runat="server"></asp:TextBox>
            </td>
            <td class="row">
                <asp:Button ID="btn_Bid" runat="server" Text="查詢檔次" OnClick="SearchBidReport" />
            </td>
        </tr>
        <tr>
            <td class="head2">
                匯款日
            </td>
            <td class="row" colspan="2">
                <asp:TextBox ID="tbOS" runat="server"></asp:TextBox>&nbsp;15:00:00
                <cc1:CalendarExtender ID="CalendarExtender1" TargetControlID="tbOS" runat="server">
                </cc1:CalendarExtender>
            </td>
        </tr>
        <tr>
            <td colspan="3" class="head" style="text-align: center">
                <asp:Label ID="lab_Message" runat="server" ForeColor="Red"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:GridView ID="gv_Report" runat="server" OnRowCommand="GetInnerReport" AutoGenerateColumns="false">
                    <Columns>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <table style="width: 1000px">
                                    <tr>
                                        <td class="head2">
                                            Id<br />
                                            ACH
                                        </td>
                                        <td class="head2">
                                            檔名<br />
                                        </td>
                                        <td class="head2">
                                            分店<br />
                                        </td>
                                        <td class="head2">
                                            簽約公司
                                            <br />
                                            負責人
                                        </td>
                                        <td class="head2">
                                            統編/身分證
                                            <br />
                                            簽約公司統編
                                            <br />
                                            匯款戶名
                                        </td>
                                        <td class="head2">
                                            匯款行代號
                                            <br />
                                            匯款帳號
                                        </td>
                                        <td class="head2">
                                            核銷起訖
                                        </td>
                                        <td class="head2">
                                            核銷數<br />
                                            進貨價
                                        </td>
                                        <td class="head2">
                                            應付<br />
                                        </td>
                                        <%--<td class="head2">
                                            實付<br />
                                        </td>--%>
                                        <td class="head2">
                                            誤付<br />
                                            誤收
                                        </td>
                                        <td class="head2">
                                            最後一期<br />
                                            該期
                                        </td>
                                        <td class="head2">
                                            匯款結果
                                        </td>
                                    </tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td class="row">
                                        <%# ((ViewWeeklyPayReport)(Container.DataItem)).Id%>
                                        <br />
                                        <asp:LinkButton ID="lbt_G_PrintBid" runat="server" CommandName="PrintBid" CommandArgument=' <%# ((ViewWeeklyPayReport)(Container.DataItem)).Id%>'
                                            Text="ACH" Visible='<%# ((ViewWeeklyPayReport)(Container.DataItem)).BusinessHourGuid != ((ViewWeeklyPayReport)(Container.DataItem)).ReportGuid%>'></asp:LinkButton>
                                        <asp:LinkButton ID="lbt_G_PrintRid" runat="server" CommandName="PrintRid" CommandArgument=' <%# ((ViewWeeklyPayReport)(Container.DataItem)).BusinessHourGuid%>'
                                            Text="ACH" Visible='<%# ((ViewWeeklyPayReport)(Container.DataItem)).BusinessHourGuid == ((ViewWeeklyPayReport)(Container.DataItem)).ReportGuid%>'></asp:LinkButton>

                                        <asp:LinkButton ID="lbt_BK_PrintBid" runat="server" CommandName="PrintBKBid" CommandArgument=' <%# ((ViewWeeklyPayReport)(Container.DataItem)).Id%>'
                                            Text="網銀" Visible='<%# (((ViewWeeklyPayReport)(Container.DataItem)).BusinessHourGuid != ((ViewWeeklyPayReport)(Container.DataItem)).ReportGuid) %>'></asp:LinkButton>
                                        <asp:LinkButton ID="lbt_BK_PrintRid" runat="server" CommandName="PrintBKRid" CommandArgument=' <%# ((ViewWeeklyPayReport)(Container.DataItem)).BusinessHourGuid%>'
                                            Text="網銀" Visible='<%# (((ViewWeeklyPayReport)(Container.DataItem)).BusinessHourGuid == ((ViewWeeklyPayReport)(Container.DataItem)).ReportGuid) %>'></asp:LinkButton>
                                        </td>

                                    <td class="row" style="vertical-align: bottom">
                                        <asp:HyperLink ID="hyp_G_Link" runat="server" Target="_blank" NavigateUrl='<%# "../../Ppon/default.aspx?bid="+((ViewWeeklyPayReport)(Container.DataItem)).BusinessHourGuid%>'
                                            Visible='<%# ((ViewWeeklyPayReport)(Container.DataItem)).BusinessHourGuid != ((ViewWeeklyPayReport)(Container.DataItem)).ReportGuid%>'
                                            Text='<%# ((ViewWeeklyPayReport)(Container.DataItem)).EventName.TrimToMaxLength(20,"...")%>'></asp:HyperLink>
                                        <asp:Label ID="lab_G_ItemName" runat="server" Text='<%# ((ViewWeeklyPayReport)(Container.DataItem)).EventName.TrimToMaxLength(20,"...")%>'
                                            Visible='<%# ((ViewWeeklyPayReport)(Container.DataItem)).BusinessHourGuid == ((ViewWeeklyPayReport)(Container.DataItem)).ReportGuid%>'></asp:Label>
                                        <br />
                                    </td>
                                    <td class="row">
                                        <%# ((ViewWeeklyPayReport)(Container.DataItem)).StoreName%>
                                        <br />
                                    </td>
                                    <td class="row">
                                        <%# (((ViewWeeklyPayReport)(Container.DataItem)).Paytocompany==1)?((ViewWeeklyPayReport)(Container.DataItem)).CompanyName:((ViewWeeklyPayReport)(Container.DataItem)).StoreCompanyName%>
                                        <br />
                                        <%# (((ViewWeeklyPayReport)(Container.DataItem)).Paytocompany==1)?((ViewWeeklyPayReport)(Container.DataItem)).SellerName:((ViewWeeklyPayReport)(Container.DataItem)).CompanyBossName%>
                                    </td>
                                    <td class="row">
                                        <%# (((ViewWeeklyPayReport)(Container.DataItem)).Paytocompany==1)?((ViewWeeklyPayReport)(Container.DataItem)).AccountId:((ViewWeeklyPayReport)(Container.DataItem)).CompanyID%>
                                        <br />
                                        <%# (((ViewWeeklyPayReport)(Container.DataItem)).Paytocompany == 1) ? ((ViewWeeklyPayReport)(Container.DataItem)).SignCompanyID : ((ViewWeeklyPayReport)(Container.DataItem)).StoreSignCompanyID%>
                                        <br />
                                        <%# (((ViewWeeklyPayReport)(Container.DataItem)).Paytocompany==1)?((ViewWeeklyPayReport)(Container.DataItem)).AccountName:((ViewWeeklyPayReport)(Container.DataItem)).CompanyAccountName%>
                                    </td>
                                    <td class="row">
                                        <%# (((ViewWeeklyPayReport)(Container.DataItem)).Paytocompany==1)?((ViewWeeklyPayReport)(Container.DataItem)).BankNo:((ViewWeeklyPayReport)(Container.DataItem)).CompanyBankCode%>
                                        <%# (((ViewWeeklyPayReport)(Container.DataItem)).Paytocompany==1)?((ViewWeeklyPayReport)(Container.DataItem)).BranchNo:((ViewWeeklyPayReport)(Container.DataItem)).CompanyBranchCode%>
                                        <br />
                                        <%# (((ViewWeeklyPayReport)(Container.DataItem)).Paytocompany == 1) ? ((ViewWeeklyPayReport)(Container.DataItem)).AccountNo : ((ViewWeeklyPayReport)(Container.DataItem)).CompanyAccount%>
                                    </td>
                                    <td class="row">
                                        <%# ((ViewWeeklyPayReport)(Container.DataItem)).IntervalStart.ToString("yyyy/MM/dd")%>~<br />
                                        <%# ((ViewWeeklyPayReport)(Container.DataItem)).IntervalEnd.ToString("yyyy/MM/dd")%>
                                    </td>
                                    <td class="row">
                                        <%# ((ViewWeeklyPayReport)(Container.DataItem)).TotalCount%>
                                        <br />
                                        <%# ((ViewWeeklyPayReport)(Container.DataItem)).Cost.ToString("F0")%>
                                    </td>
                                    <td class="row">
                                        <%# ((ViewWeeklyPayReport)(Container.DataItem)).TotalSum.ToString("F0")%>
                                        <br />
                                    </td>
                                    <%--<td class="row">
                                        <%# ((((ViewWeeklyPayReport)(Container.DataItem)).TransferAmount) ?? ((ViewWeeklyPayReport)(Container.DataItem)).TotalSum).ToString("F0")%>
                                        <br />
                                    </td>--%>
                                    <td class="row">
                                        <%# ((ViewWeeklyPayReport)(Container.DataItem)).ErrorIn.ToString("F0")%><br />
                                        <%# ((ViewWeeklyPayReport)(Container.DataItem)).ErrorOut.ToString("F0")%>
                                    </td>
                                    <td class="row">
                                        <%# ((ViewWeeklyPayReport)(Container.DataItem)).IsLastWeek?"V":string.Empty%><br />
                                        <asp:LinkButton ID="lbt_G_ReportGuid" runat="server" CommandName="ReportGuid" CommandArgument='<%# ((ViewWeeklyPayReport)(Container.DataItem)).ReportGuid%>'
                                            Text='子檔' Visible='<%# ((ViewWeeklyPayReport)(Container.DataItem)).BusinessHourGuid == ((ViewWeeklyPayReport)(Container.DataItem)).ReportGuid%>'></asp:LinkButton>
                                        <asp:LinkButton ID="lbt_G_ReportMainId" runat="server" CommandName="ReportMainId"
                                            CommandArgument='<%# ((ViewWeeklyPayReport)(Container.DataItem)).ReportGuid%>'
                                            Text='主檔' Visible='<%# ((ViewWeeklyPayReport)(Container.DataItem)).BusinessHourGuid != ((ViewWeeklyPayReport)(Container.DataItem)).ReportGuid%>'></asp:LinkButton>
                                    </td>
                                    <td class="row">
                                        <%# GetAtmResult(Eval("ResponseTime"), Eval("Result")) %>
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <AlternatingItemTemplate>
                                <tr>
                                    <td class="row">
                                        <%# ((ViewWeeklyPayReport)(Container.DataItem)).Id%>
                                        <br />
                                        <asp:LinkButton ID="lbt_G_PrintBid" runat="server" CommandName="PrintBid" CommandArgument=' <%# ((ViewWeeklyPayReport)(Container.DataItem)).Id%>'
                                            Text="ACH" Visible='<%# ((ViewWeeklyPayReport)(Container.DataItem)).BusinessHourGuid != ((ViewWeeklyPayReport)(Container.DataItem)).ReportGuid%>'></asp:LinkButton>
                                        <asp:LinkButton ID="lbt_G_PrintRid" runat="server" CommandName="PrintRid" CommandArgument=' <%# ((ViewWeeklyPayReport)(Container.DataItem)).BusinessHourGuid%>'
                                            Text="ACH" Visible='<%# ((ViewWeeklyPayReport)(Container.DataItem)).BusinessHourGuid == ((ViewWeeklyPayReport)(Container.DataItem)).ReportGuid%>'></asp:LinkButton>

                                        <asp:LinkButton ID="lbt_BK_PrintBid" runat="server" CommandName="PrintBKBid" CommandArgument=' <%# ((ViewWeeklyPayReport)(Container.DataItem)).Id%>'
                                            Text="網銀" Visible='<%# (((ViewWeeklyPayReport)(Container.DataItem)).BusinessHourGuid != ((ViewWeeklyPayReport)(Container.DataItem)).ReportGuid) %>'></asp:LinkButton>
                                        <asp:LinkButton ID="lbt_BK_PrintRid" runat="server" CommandName="PrintBKRid" CommandArgument=' <%# ((ViewWeeklyPayReport)(Container.DataItem)).BusinessHourGuid%>'
                                            Text="網銀" Visible='<%# (((ViewWeeklyPayReport)(Container.DataItem)).BusinessHourGuid == ((ViewWeeklyPayReport)(Container.DataItem)).ReportGuid) %>'></asp:LinkButton>
                                    </td>
                                    <td class="row" style="vertical-align: bottom">
                                        <asp:HyperLink ID="hyp_G_Link" runat="server" Target="_blank" NavigateUrl='<%# "../../Ppon/default.aspx?bid="+((ViewWeeklyPayReport)(Container.DataItem)).BusinessHourGuid%>'
                                            Visible='<%# ((ViewWeeklyPayReport)(Container.DataItem)).BusinessHourGuid != ((ViewWeeklyPayReport)(Container.DataItem)).ReportGuid%>'
                                            Text='<%# ((ViewWeeklyPayReport)(Container.DataItem)).EventName.TrimToMaxLength(20,"...")%>'></asp:HyperLink>
                                        <asp:Label ID="lab_G_ItemName" runat="server" Text='<%# ((ViewWeeklyPayReport)(Container.DataItem)).EventName.TrimToMaxLength(20,"...")%>'
                                            Visible='<%# ((ViewWeeklyPayReport)(Container.DataItem)).BusinessHourGuid == ((ViewWeeklyPayReport)(Container.DataItem)).ReportGuid%>'></asp:Label>
                                        <br />
                                    </td>
                                    <td class="row">
                                        <%# ((ViewWeeklyPayReport)(Container.DataItem)).StoreName%>
                                        <br />
                                    </td>
                                    <td class="row">
                                        <%# (((ViewWeeklyPayReport)(Container.DataItem)).Paytocompany==1)?((ViewWeeklyPayReport)(Container.DataItem)).CompanyName:((ViewWeeklyPayReport)(Container.DataItem)).StoreCompanyName%>
                                        <br />
                                        <%# (((ViewWeeklyPayReport)(Container.DataItem)).Paytocompany==1)?((ViewWeeklyPayReport)(Container.DataItem)).SellerName:((ViewWeeklyPayReport)(Container.DataItem)).CompanyBossName%>
                                    </td>
                                    <td class="row">
                                        <%# (((ViewWeeklyPayReport)(Container.DataItem)).Paytocompany==1)?((ViewWeeklyPayReport)(Container.DataItem)).AccountId:((ViewWeeklyPayReport)(Container.DataItem)).CompanyID%>
                                        <br />
                                        <%# (((ViewWeeklyPayReport)(Container.DataItem)).Paytocompany == 1) ? ((ViewWeeklyPayReport)(Container.DataItem)).SignCompanyID : ((ViewWeeklyPayReport)(Container.DataItem)).StoreSignCompanyID%>
                                        <br />
                                        <%# (((ViewWeeklyPayReport)(Container.DataItem)).Paytocompany==1)?((ViewWeeklyPayReport)(Container.DataItem)).AccountName:((ViewWeeklyPayReport)(Container.DataItem)).CompanyAccountName%>
                                    </td>
                                    <td class="row">
                                        <%# (((ViewWeeklyPayReport)(Container.DataItem)).Paytocompany==1)?((ViewWeeklyPayReport)(Container.DataItem)).BankNo:((ViewWeeklyPayReport)(Container.DataItem)).CompanyBankCode%>
                                        <%# (((ViewWeeklyPayReport)(Container.DataItem)).Paytocompany==1)?((ViewWeeklyPayReport)(Container.DataItem)).BranchNo:((ViewWeeklyPayReport)(Container.DataItem)).CompanyBranchCode%>
                                        <br />
                                        <%# (((ViewWeeklyPayReport)(Container.DataItem)).Paytocompany == 1) ? ((ViewWeeklyPayReport)(Container.DataItem)).AccountNo : ((ViewWeeklyPayReport)(Container.DataItem)).CompanyAccount%>
                                    </td>
                                    <td class="row">
                                        <%# ((ViewWeeklyPayReport)(Container.DataItem)).IntervalStart.ToString("yyyy/MM/dd")%>~<br />
                                        <%# ((ViewWeeklyPayReport)(Container.DataItem)).IntervalEnd.ToString("yyyy/MM/dd")%>
                                    </td>
                                    <td class="row">
                                        <%# ((ViewWeeklyPayReport)(Container.DataItem)).TotalCount%>
                                        <br />
                                        <%# ((ViewWeeklyPayReport)(Container.DataItem)).Cost.ToString("F0")%>
                                    </td>
                                    <td class="row">
                                        <%# ((ViewWeeklyPayReport)(Container.DataItem)).TotalSum.ToString("F0")%>
                                        <br />
                                    </td>
                                    <%--<td class="row">
                                        <%# ((((ViewWeeklyPayReport)(Container.DataItem)).TransferAmount) ?? ((ViewWeeklyPayReport)(Container.DataItem)).TotalSum).ToString("F0")%>
                                        <br />
                                    </td>--%>
                                    <td class="row">
                                        <%# ((ViewWeeklyPayReport)(Container.DataItem)).ErrorIn.ToString("F0")%>
                                        <br />
                                        <%# ((ViewWeeklyPayReport)(Container.DataItem)).ErrorOut.ToString("F0")%>
                                    </td>
                                    <td class="row">
                                        <%# ((ViewWeeklyPayReport)(Container.DataItem)).IsLastWeek?"V":string.Empty%><br />
                                        <asp:LinkButton ID="lbt_G_ReportGuid" runat="server" CommandName="ReportGuid" CommandArgument='<%# ((ViewWeeklyPayReport)(Container.DataItem)).ReportGuid%>'
                                            Text='子檔' Visible='<%# ((ViewWeeklyPayReport)(Container.DataItem)).BusinessHourGuid == ((ViewWeeklyPayReport)(Container.DataItem)).ReportGuid%>'></asp:LinkButton>
                                        <asp:LinkButton ID="lbt_G_ReportMainId" runat="server" CommandName="ReportMainId"
                                            CommandArgument='<%# ((ViewWeeklyPayReport)(Container.DataItem)).ReportGuid%>'
                                            Text='主檔' Visible='<%# ((ViewWeeklyPayReport)(Container.DataItem)).BusinessHourGuid != ((ViewWeeklyPayReport)(Container.DataItem)).ReportGuid%>'></asp:LinkButton>
                                    </td>
                                    <td class="row">
                                        <%# GetAtmResult(Eval("ResponseTime"), Eval("Result"))%>
                                    </td>
                                </tr>
                            </AlternatingItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </td>
        </tr>
    </table>
</asp:Content>

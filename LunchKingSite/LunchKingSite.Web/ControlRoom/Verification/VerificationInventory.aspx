﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ControlRoom/backend.master" AutoEventWireup="true"
    CodeBehind="VerificationInventory.aspx.cs" Inherits="LunchKingSite.Web.ControlRoom.Verification.VerificationInventory" %>

<%@ Register Src="~/ControlRoom/Controls/Pager.ascx" TagName="Pager" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <script type="text/javascript" src="../../Tools/js/jquery-1.4.2.min.js"></script>
    <script type="text/javascript">
        var ogvCount;
        $(document).ready(function () {
            $('table [id*=gvInventory]').find('td:first-child input[type=checkbox]').filter(function () {
                OringinRowCheck(this, 'A')
            })
            ogvCount = $('table [id*=gvInventory]').find('td:first-child input[type=checkbox]').filter(function () { return $(this).attr('checked') }).length;
        });

        function CheckVerification() {
            var count = 0;
            count = $('table [id*=gvInventory]').find('td:first-child input[type=checkbox]').filter(function () { return $(this).attr('checked') }).length;
            var itemsColl = document.getElementById('<%=hidSelectCount.ClientID %>').value
            var gvCount = $('table [id*=gvInventory]').find('td:first-child input[type=checkbox]').filter(function () { return $(this).attr('checked') }).length;
            if (itemsColl !=0)
            {
                if (ogvCount != gvCount)
                {
                    itemsColl = itemsColl - (ogvCount - gvCount);
                }
            }
            else {
                itemsColl = count;
            }
            
            if (itemsColl == 0) {
                alert('您沒有勾選憑證!');
                return false;
            } else {
                return confirm('確定核銷此 ' + itemsColl + '張憑證?');
            }
        }
        
        function SelectAll(oCheckAll, type) {
            var $objChkAll = $(oCheckAll);
            var $objParentTable = $objChkAll.parent();
            while ($objParentTable.attr('tagName') != 'TABLE') {
                $objParentTable = $objParentTable.parent();
            }
            var IsCheck = oCheckAll.checked;
            if (IsCheck)
            {
                $objParentTable.find('td:first-child input[type=checkbox]').each(function () {
                    RowCheck(this, type, $objChkAll.attr('checked'));
                });
            }
            else {
                $objParentTable.find('td:first-child input[type=checkbox]').each(function () {
                    RowCheck(this, false);
                });
            }
            
        }

        function ReverseCheck() {
            $('table [id*=gvInventory]').find('td:first-child input[type=checkbox]').each(function () {
                RowCheck($(this), !$(this).attr('checked'));
            });
            return false;
        }

        function SelectAllRow() {

            var subcheckboxes = document.getElementById('Verify');
            var count = subcheckboxes.length;
            for (var i = 0; i < subcheckboxes.length; ++i) {
                subcheckboxes[i].checked = checkbox.checked;
            }
            
            alert(count);
            return false;
        }

        function RowCheck(oCheck, check) {
            var chk = $(oCheck);
            chk.attr('checked', check);
            var color = '#EEC591';
            var $objParentRow = chk.parent().parent();
            
            if (chk.attr('checked')) {
                $objParentRow.css('background', color);
            }
            else {
                $objParentRow.css('background', 'white');
            }
        }

        function OringinRowCheck(oCheck) {
            RowCheck(oCheck, $(oCheck).attr('checked'));
        }

        
    </script>
    <style type="text/css">
        .style1
        {
            width: 78px;
        }
        .style2
        {
            width: 79px;
        }
        .style3
        {
            width: 86px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>
        <asp:Label ID="lblItemName" runat="server"></asp:Label>
    </h2>
    <table>
        <tr>
            <td>
                <table style="font-size: 21px;">
                    <tr>
                        <td>
                            店家名稱:
                        </td>
                        <td>
                            <asp:Label ID="lblSellerName" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            結檔時間:
                        </td>
                        <td>
                            <asp:Label ID="lblEndTime" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            使用期限:
                        </td>
                        <td>
                            <asp:Label ID="lblDeliverTiem" runat="server"></asp:Label>
                            <asp:Literal ID="expireDateMessage" runat="server" />
                        </td>
                    </tr>
                </table>
            </td>
            <td valign="top">
                <table>
                    <tr>
                        <td>
                            &nbsp;&nbsp;&nbsp;&nbsp;<asp:LinkButton ID="lkDealSearch" runat="server" Text="查找其他檔次"
                                PostBackUrl="~/ControlRoom/Verification/SellerVerificationSearch.aspx"></asp:LinkButton>
                        </td>
                        <td>
                            &nbsp;&nbsp;&nbsp;&nbsp;<asp:Button ID="btnInventoryLost" runat="server" Text="清冊遺失"
                                OnClick="btnInventoryLost_Click" OnClientClick="return confirm('確定清冊已遺失?')" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <hr size="1" align="left" width="90%" color="black" />
    <table style="display:none;">
        <tr>
            <td colspan="2">
                <asp:Panel ID="Panel1" runat="server">
                    <table style="font-size: 20px">
                        <tr>
                            <td>
                                店家清冊清點：
                            </td>
                            <td style="width: 400px">
                                <table style="background-color: White; border: 1; border-collapse: collapse;" cellspacing="1"
                                    rules="all" cellpadding="3" width="100%">
                                    <tr>
                                        <td align="center" style="width: 25%">
                                            結檔銷售
                                        </td>
                                        <td align="center" style="width: 25%">
                                            已核銷
                                        </td>
                                        <td align="center" style="width: 50%">
                                            未核銷 + 結檔後退貨
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" style="width: 25%">
                                            <asp:Label ID="lblSellerSlug" runat="server"></asp:Label>
                                        </td>
                                        <td align="center" style="width: 25%">
                                            <asp:TextBox ID="txtSellerVerifiedQuantity" Style="text-align: right" runat="server"
                                                Width="90px"></asp:TextBox>
                                            <asp:Label ID="lblSellerVerifiedQuantity" runat="server"></asp:Label>
                                        </td>
                                        <td align="center" style="width: 50%">
                                            <asp:Label ID="lblSellerNonVeri" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                線上核銷結果：
                            </td>
                            <td style="width: 400px">
                                <table style="background-color: White; border: 1; border-collapse: collapse;" cellspacing="1"
                                    rules="all" cellpadding="3" width="100%">
                                    <tr>
                                        <td align="center" style="width: 25%">
                                            結檔銷售
                                        </td>
                                        <td align="center" style="width: 25%">
                                            已核銷
                                        </td>
                                        <td align="center" style="width: 25%">
                                            未核銷
                                        </td>
                                        <td align="center" style="width: 25%">
                                            結檔後退貨
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" style="width: 25%">
                                            <asp:Label ID="lblSlug" runat="server"></asp:Label>
                                        </td>
                                        <td align="center" style="width: 25%">
                                            <asp:Label ID="lblVeri" runat="server"></asp:Label>
                                        </td>
                                        <td align="center" style="width: 25%">
                                            <asp:Label ID="lblNonVeri" runat="server"></asp:Label>
                                        </td>
                                        <td align="center" style="width: 25%">
                                            <asp:Label ID="lblReturned" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
    </table>
    <asp:Panel ID="divInventory" runat="server">
        <table width="500px">
            <tr>
                <td>
                    <asp:GridView ID="gvInventory" runat="server" OnRowDataBound="gvInventory_RowDataBound"
                        EnableViewState="true" EmptyDataText="無符合的資料" AutoGenerateColumns="False" OnDataBinding="gvInventory_DataBinding">
                        <FooterStyle BackColor="#CCCC99"></FooterStyle>
                        <RowStyle BackColor="White" Height="30px"></RowStyle>
                        <SelectedRowStyle BackColor="#CE5D5A" ForeColor="White" Font-Bold="True"></SelectedRowStyle>
                        <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right"></PagerStyle>
                        <HeaderStyle BackColor="#6B696B" ForeColor="White" Font-Bold="True" Height="30px">
                        </HeaderStyle>
                        <AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
                        <Columns>
                            <asp:TemplateField ItemStyle-Width="50px" ItemStyle-HorizontalAlign="Center">
                                <HeaderTemplate>
                                    <asp:CheckBox ID="chkCheck" CssClass="Original" runat="server" onclick="SelectAll(this,'A');" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:CheckBox ID="Verify" runat="server" Visible="false" onclick="OringinRowCheck(this,'A')" />
                                    <asp:HiddenField ID="hidCouponId" runat="server" />
                                    <asp:HiddenField ID="hidCouponCode" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="SequenceNumber" HeaderText="憑證編號" ItemStyle-HorizontalAlign="Center"
                                ItemStyle-Width="150px" />
                            <asp:BoundField DataField="CouponCode" HeaderText="確認碼" ItemStyle-HorizontalAlign="Center"
                                ItemStyle-Width="150px" />
                            <asp:TemplateField HeaderText="憑證狀況" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="150px">
                                <ItemTemplate>
                                    <asp:Label ID="lblStatus" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="處理時間" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="200px">
                                <ItemTemplate>
                                    <asp:Label ID="lblModifyTime" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                    <br />
                    <asp:LinkButton ID="lnkAll" runat="server" Text="跨頁全選" 
                         OnClick="lnkAll_Click" Font-Size="Smaller"></asp:LinkButton>
                    <asp:HiddenField ID="hidSelectCount" runat="server" />
                    <asp:LinkButton ID="lnkReverse" runat="server" Text="反向選取" OnClientClick="return ReverseCheck();"
                        Font-Size="Smaller"></asp:LinkButton>
                    <asp:Button ID="btnVerification" runat="server" Text="核銷勾選的憑證" OnClick="btnVerification_Click"
                        OnClientClick="return CheckVerification();" />
                    <br />
                    <br />
                    <uc1:Pager ID="gridPager" runat="server" PageSize="20" ongetcount="InventoryCount"
                        onupdate="UpdateHandler" EnableViewState="true"></uc1:Pager>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="divRecovery" runat="server" Visible="false">
        <table>
            <tr>
                <td>
                    憑證編號：
                </td>
                <td>
                    <asp:TextBox ID="txtSequenceNumber" runat="server"></asp:TextBox>
                </td>
                <td>
                    <%--<asp:Button ID="btnSubmit" runat="server" OnClick="btnSubmit_Click"/>--%>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>

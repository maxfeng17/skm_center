﻿using LunchKingSite.Core;
using LunchKingSite.Core.UI;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;
using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LunchKingSite.Web.ControlRoom.Verification
{
    public partial class VbsBulletinBoardSetUp : RolePage, IVbsBulletinBoardSetUpView
    {
        #region Event

        public event EventHandler<PublishingArgs> SetPublishing;
        public event EventHandler<DataEventArgs<int>> StickPublishing;
        public event EventHandler<DataEventArgs<int>> DeletePublishing;
        public event EventHandler<BulletinBoardListGetEventArgs> BulletinBoardListGet;
        public event EventHandler<DataEventArgs<int>> PageChanged;
        public event EventHandler<DataEventArgs<int>> GetPublishCount;

        #endregion

        #region Property

        private VbsBulletinBoardSetUpPresenter _presenter;

        public VbsBulletinBoardSetUpPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        public int ContentMaxLength {
            get {
                return 4000;
            }
        }

        public int FilterSellerType 
        {
            get {
                return Convert.ToInt32(ddlSellerType.SelectedValue);
            }
        }

        /// <summary>
        /// 發布的內容（跟發布的長度是不同的屬性！原因是，長度必須要排除換行字元）
        /// </summary>
        public string PublishContent {
            get 
            {
                return this.tbPublishContent.Text;
            }
            set {
                this.tbPublishContent.Text = value;
            }
        }

        /// <summary>
        /// 發布的內容長度（跟發布的內容是不同的屬性！原因是，長度必須要排除換行字元）
        /// </summary>
        public int PublishContentLength 
        {
            get 
            {
                return this.tbPublishContent.Text.Replace(Environment.NewLine, string.Empty).Length;
            }
        }

        public bool IsPublishToShop 
        {
            get 
            {
                return cbToShop.Checked;
            }
        }

        public bool IsPublishToHouse
        {
            get
            {
                return cbToHouse.Checked;
            }
        }

        public DateTime PublishTime
        {
            get
            {
                DateTime dt;
                if (DateTime.TryParse(txtPublishedDate.Text, out dt))
                {
                    return dt;
                }
                return DateTime.Now;
            }
        }

        public string LinkTitle
        {
            get
            {
                return txtLinkTitle.Text;
            }
            set
            {
                txtLinkTitle.Text = value;
            }
        }

        public string LinkUrl
        {
            get
            {
                return txtLinkUrl.Text;
            }
            set
            {
                txtLinkUrl.Text = value;
            }
        }

        public string UserName
        {
            get
            {
                return User.Identity.Name;
            }
        }

        #region GridPager

        public int PageSize
        {
            get { return gridPager.PageSize; }
            set { gridPager.PageSize = value; }
        }

        public int CurrentPage
        {
            get { return gridPager.CurrentPage; }
        }

        #endregion

        #endregion

        #region Method

        public void Alert(string msg) 
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), string.Empty, "alert('" + msg + "');", true);
        }

        public void BuildCbPublishSellerType() 
        {
            cbToShop.Text = I18N.Phrase.ToShopSeller;
            cbToHouse.Text = I18N.Phrase.ToHouseSeller;
        }

        public void BuildDdlSellerType() 
        {
            ddlSellerType.Items.Add(new ListItem(I18N.Phrase.All, ((int)VbsBulletinBoardPublishSellerType.All).ToString() ));
            ddlSellerType.Items.Add(new ListItem(I18N.Phrase.ToShop, ((int)VbsBulletinBoardPublishSellerType.ToShop).ToString()));
            ddlSellerType.Items.Add(new ListItem(I18N.Phrase.ToHouse, ((int)VbsBulletinBoardPublishSellerType.ToHouse).ToString()));
        }

        public void AddContentMaxLength() 
        {
            tbPublishContent.Attributes.Add("onkeypress", "return isMaxLength(this, " + ContentMaxLength + ");");
        }

        //更新完公布欄訊息後回應給消費者
        public void PublishSetDone(bool isSuccess, UpdateMode mode, string message) 
        {
            if (isSuccess) 
            {
                string msg;
                switch (mode) 
                {
                    case UpdateMode.Add:
                        msg = I18N.Phrase.AddNew;
                        break;
                    case UpdateMode.Update:
                        msg = I18N.Phrase.Update;
                        break;
                    case UpdateMode.Delete:
                        msg = I18N.Phrase.Delete;
                        break;
                    case UpdateMode.Stick:
                        msg = message + I18N.Phrase.Stick;
                        break;
                    default:
                        msg = I18N.Phrase.AddNew;
                        break;
                }
                msg += I18N.Phrase.Success;
                Alert(msg);
            }
            
        }

        //將抓出的公布欄資料繫結到GridView
        public void SetBulletinBoardList(IEnumerable<VbsBulletinBoard> boardCols) 
        {
            gvBoard.DataSource = boardCols;
            gvBoard.DataBind();
            gridPager.ResolvePagerView(CurrentPage, true);
        }

        protected string ShowSellerType(int type) 
        {
            string result = string.Empty;
            result += Helper.IsFlagSet(type, (int)VbsBulletinBoardPublishSellerType.ToShop) ? I18N.Phrase.ToShop + "／" : string.Empty;
            result += Helper.IsFlagSet(type, (int)VbsBulletinBoardPublishSellerType.ToHouse) ? I18N.Phrase.ToHouse + "／" : string.Empty;
            result = result.Substring(0, result.Length - 1);
            return result;
        }

        protected string ReplaceHtmlNewLine(string input) 
        {
            return input.Replace(Environment.NewLine, I18N.Phrase.HtmlNewLine);
        }

        protected void UpdateHandler(int pageNumber)
        {
            DataEventArgs<int> e = new DataEventArgs<int>(pageNumber);
            gridPager.CurrentPage = pageNumber;
            if (this.PageChanged != null)
                this.PageChanged(this, e);
        }

        protected int PagerRetrieveCounts() 
        {
            DataEventArgs<int> e = new DataEventArgs<int>(0);
            if (GetPublishCount != null)
                GetPublishCount(this, e);
            return e.Data;
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (!_presenter.OnViewInitialized())
                {
                    return;
                }
                this._presenter.OnViewLoaded();
                this.BulletinBoardListGet(this, new BulletinBoardListGetEventArgs { CurrentPage = 1, SellerType = this.FilterSellerType });
            }
            else 
            {
                this._presenter.OnViewLoaded();
            }
        }

        protected void btnPublish_Click(object sender, EventArgs e)
        {
            #region Data Check

            if (string.IsNullOrEmpty(PublishContent)) 
            {
                Alert("請輸入公告內容"); return;
            }

            if (PublishContentLength > ContentMaxLength) 
            {
                Alert("公告內容請勿超過 " + ContentMaxLength + " 字"); return;
            }

            if (!IsPublishToHouse && !IsPublishToShop) 
            {
                Alert("請至少選擇一個公告對象"); return;
            }

            #endregion

            if (this.SetPublishing != null)
            {
                SetPublishing(this, new PublishingArgs { Mode = UpdateMode.Add });
            }
        }

        protected void gvBoard_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals(RowCommand.Stick.ToString())) 
            {
                //置頂
                StickPublishing(this, new DataEventArgs<int>(int.Parse((string)e.CommandArgument)));
            } else if (e.CommandName.Equals(RowCommand.Del.ToString()))
            {
                //刪除訊息
                DeletePublishing(this, new DataEventArgs<int>(int.Parse((string)e.CommandArgument)));
            }
        }

        protected void ddlSellerType_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.BulletinBoardListGet(this, new BulletinBoardListGetEventArgs { CurrentPage = this.CurrentPage, SellerType = this.FilterSellerType });
        }

        protected string RenderLink(string title, string url)
        {
            if (string.IsNullOrEmpty(title) == false && string.IsNullOrEmpty(url) == false)
            {
                return string.Format("<a href='{0}'>{1}</a>", url, title);
            }
            return string.Empty;
        }
    }
}
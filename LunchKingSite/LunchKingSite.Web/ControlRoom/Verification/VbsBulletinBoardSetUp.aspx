﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ControlRoom/backend.master" AutoEventWireup="true" CodeBehind="VbsBulletinBoardSetUp.aspx.cs" Inherits="LunchKingSite.Web.ControlRoom.Verification.VbsBulletinBoardSetUp" %>
<%@ Register Src="../Controls/Pager.ascx" TagName="Pager" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <script type="text/javascript" src="//www.google.com/jsapi"></script>
    <script type="text/javascript">
        google.load("jquery", "1.4.2");
        google.load("jqueryui", "1.8");
    </script>
    <script type="text/javascript" src="../../Tools/js/jquery.ui.datepicker-zh-TW.js"></script>
    <script type="text/javascript" src="../../Tools/js/jquery.blockUI.js"></script>
    <link type="text/css" href="../../Tools/js/css/ui-lightness/jquery-ui-1.8rc3.custom.css"
        rel="stylesheet" />
    <script language="javascript" type="text/javascript">
        $(document).ready(function () {

            $('#<%=btnPublish.ClientID%>').click(function () {
                var cbToShop = $('#<%=cbToShop.ClientID%>');
                var cbToHouse = $('#<%=cbToHouse.ClientID%>');
                var tbContent = $('#<%=tbPublishContent.ClientID%>');
                
                if (!tbContent.val().length > 0) {
                    alert('請輸入公告內容'); return false;
                }
                
                if (!cbToShop.attr('checked') && !cbToHouse.attr('checked')) {
                    alert('請至少選擇一個公告對象'); return false;
                }

                if (!window.confirm("公告將即時發佈到商家系統，確定新增嗎？")) {
                    return false;
                }
            });

            $('#<%=tbPublishContent.ClientID%>').keyup(function() {

                limits($(this), <% =ContentMaxLength%>);
            });
            
            $('.datePicker').datepicker({
                changeMonth: true,
                numberOfMonths: 1,
                minDate: new Date()
            });
            

            function limits(obj, limit) {

                var text = $(obj).val();
                var length = text.length;
                if (length > limit) {
                    $(obj).val(text.substr(0, limit));
                } else {
                    //limit - length
                    $('#nowWorldCount').data('11');
                }
            }
        });

        function isMaxLength(tb, len) {
            return tb.value.length < len;
        }
    </script>
    <style>
        .datePicker
        {
            width: 100px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>商家系統公布欄設定</h2>
    <table width="800">
        <tr>
            <td width="15%">公告對象：</td>
            <td width="*">
                <asp:CheckBox ID="cbToHouse" runat="server"></asp:CheckBox>
                <asp:CheckBox ID="cbToShop" runat="server"></asp:CheckBox>
            </td>
        </tr>
        <tr>
            <td valign="top">公告時間：</td>
            <td>
                <asp:TextBox ID="txtPublishedDate" runat="server" CssClass="datePicker" />
            </td>
        </tr>
        <tr>
            <td valign="top">公告內容：</td>
            <td>
                <asp:TextBox ID="tbPublishContent" runat="server" Rows="10" TextMode="MultiLine" Width="80%"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td valign="top">連結名稱：</td>
            <td>
                <asp:TextBox ID="txtLinkTitle" runat="server" Width="80%" />
            </td>
        </tr>
        <tr>
            <td valign="top">連結網址：</td>
            <td>
                <asp:TextBox ID="txtLinkUrl" runat="server" Width="80%" />
            </td>
        </tr>
        <tr>
            <td colspan="2"><asp:Button ID="btnPublish" runat="server" Text="新增公告" OnClick="btnPublish_Click" />
            </td>
        </tr>
    </table>
    <br />
    <table width="1000">
        <tr>
            <td>
                <uc1:Pager ID="gridPager" runat="server" PageSize="10" OnGetCount="PagerRetrieveCounts" SetPagingWhenPageLoad="true" OnUpdate="UpdateHandler"></uc1:Pager><br />
                顯示篩選：<asp:DropDownList ID="ddlSellerType" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlSellerType_SelectedIndexChanged" />
                <asp:GridView ID="gvBoard" runat="server" ForeColor="#333333" Width="100%" AutoGenerateColumns="False" OnRowCommand="gvBoard_RowCommand" 
                    EnableViewState="true" PageSize="10" >
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                        <asp:TemplateField HeaderText="對象" ItemStyle-Width="10%">
                            <ItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text='<%# ShowSellerType((int)Eval("PublishSellerType")) %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="內容" ItemStyle-Width="55%">
                            <ItemTemplate>
                                <asp:Label ID="Label2" runat="server" Text='<%# ReplaceHtmlNewLine(Eval("PublishContent").ToString()) %>'></asp:Label>
                                <br /><%#RenderLink(Eval("LinkTitle") as string, Eval("LinkUrl") as string) %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="公告時間" ItemStyle-Width="10%">
                            <ItemTemplate>
                                <asp:Label ID="Label3" runat="server" Text='<%# ((DateTime)Eval("PublishTime")).ToString("yyyy/MM/dd") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="動作" ItemStyle-Width="25%">
                            <ItemTemplate>
                                <asp:Button ID="btnStick" runat="server" Text='<%# (bool)Eval("IsSticky") ? "取消置頂" : "置頂" %>' CommandName="Stick" CommandArgument='<%# Eval("Id") %>' OnClientClick="return true;" />&nbsp;
                                <asp:Button ID="btnDelete" runat="server" Text="刪除" CommandName="Del" CommandArgument='<%# Eval("Id") %>' OnClientClick="return confirm('確定要刪除此公告內容？')" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
    </table>
</asp:Content>

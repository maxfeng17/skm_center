﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PaymentReceiptManage.aspx.cs" MasterPageFile="~/ControlRoom/backend.master"  Inherits="LunchKingSite.Web.ControlRoom.Verification.PaymentReceiptManage" %>
<%@ Register Src="../Controls/BalanceSheetBill.ascx" TagName="BalanceSheetBill" TagPrefix="uc2" %>
<%@ Register Src="../Controls/Pager.ascx" TagName="Pager" TagPrefix="uc1" %>
<%@ Import Namespace="LunchKingSite.BizLogic.Facade" %>
<%@ Import Namespace="LunchKingSite.Core" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
<script type="text/javascript" src="../../Tools/js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="../../Tools/js/jquery.ui.datepicker-zh-TW.js"></script>
<script type="text/javascript" src="../../Tools/js/jquery-ui.1.8.min.js"></script>
<link rel="stylesheet" type="text/css" href="../../Tools/js/css/ui-lightness/jquery-ui-1.8rc3.custom.css"/> 
<style type="text/css">        
    /*設定table裡字的大小*/
    .tblBillInfo
    {
        border:1px solid grey;
    }   
    .tblBillInfo td
    {
        border:1px solid grey;
        text-align:center; 
    }    
       
        
    .tblBsInfo 
    {
        border:1px solid grey;
        border-collapse:collapse;
    }       
    
    .tblBsInfo td
    {
        border:1px solid grey;
    }       
    
    .ui-datepicker { width: 17em; padding: .2em .2em 0; display: none; font-size: 80.5%; }

</style>
<script type="text/javascript">
    $(document).ready(function () {
        setupdatepicker('<%=tbSheetDS.ClientID%>');
        setupdatepicker('<%=tbSheetDE.ClientID%>');
        $('.date').datepicker();

        var tabIndex = $("#<%= hidCurrentTab.ClientID %>").val();
        $("#tabs").tabs({
            select: function (event, ui) {
                $("#<%= hidCurrentTab.ClientID %>").val(ui.index);
                $('input[id*=hidScrollPositionFlag]').val('');
                $('input[id*=hidUpdateBillRegionFlag]').val('');
            },
            selected: tabIndex
        });

        var repInvoiceQuery = document.getElementById('<%= repInvoiceQuery.ClientID %>');
        $('input:checkbox[id$=chkBillSentDateSelectedAll]', repInvoiceQuery).click(function (e) {
            if (this.checked) {
                $('input:checkbox[id$=chkBillSentDate]', repInvoiceQuery).attr('checked', true);
            }
            else {
                $('input:checkbox[id$=chkBillSentDate]', repInvoiceQuery).removeAttr('checked');
            }
        });
        $('input:checkbox[id$=chkFinanceGetDateSelectedAll]', repInvoiceQuery).click(function (e) {
            if (this.checked) {
                $('input:checkbox[id$=chkFinanceGetDate]', repInvoiceQuery).attr('checked', true);
            }
            else {
                $('input:checkbox[id$=chkFinanceGetDate]', repInvoiceQuery).removeAttr('checked');
            }
        });

        $('input:checkbox[id$=chkBillSentDate]', repInvoiceQuery).click(function (e) {
            //To uncheck the header checkbox when there are no selected checkboxes in itemtemplate
            if ($('input:checkbox[id$=chkBillSentDate]:checked', repInvoiceQuery).length == 0) {
                $('input:checkbox[id$=chkBillSentDateSelectedAll]', repInvoiceQuery).removeAttr('checked');
            }
                //To check the header checkbox when there are all selected checkboxes in itemtemplate
            else if ($('input:checkbox[id$=chkBillSentDate]:checked', repInvoiceQuery).length == $('input:checkbox[id$=CheckSelect]', repInvoiceQuery).length) {
                $('input:checkbox[id$=chkBillSentDateSelectedAll]', repInvoiceQuery).attr('checked', true);
            }
        });

        $('input:checkbox[id$=chkFinanceGetDate]', repInvoiceQuery).click(function (e) {
            //To uncheck the header checkbox when there are no selected checkboxes in itemtemplate
            if ($('input:checkbox[id$=chkFinanceGetDate]:checked', repInvoiceQuery).length == 0) {
                $('input:checkbox[id$=chkFinanceGetDateSelectedAll]', repInvoiceQuery).removeAttr('checked');
            }
                //To check the header checkbox when there are all selected checkboxes in itemtemplate
            else if ($('input:checkbox[id$=chkFinanceGetDate]:checked', repInvoiceQuery).length == $('input:checkbox[id$=CheckSelect]', repInvoiceQuery).length) {
                $('input:checkbox[id$=chkFinanceGetDateSelectedAll]', repInvoiceQuery).attr('checked', true);
            }
        });

        var repSellerStore = document.getElementById('<%= repSellerStore.ClientID %>');
        $('input:checkbox[id$=chkSellerStoreSelectedAll]', repSellerStore).click(function (e) {
            if (this.checked) {
                $('input:checkbox[id$=chkSellerStore]', repSellerStore).attr('checked', true);
            }
            else {
                $('input:checkbox[id$=chkSellerStore]', repSellerStore).removeAttr('checked');
            }
        });

        var repBalanceSheet = document.getElementById('<%= repBalanceSheet.ClientID %>');
        $('input:checkbox[id$=chkBalanceSheetSelectedAll]', repBalanceSheet).click(function (e) {
            if (this.checked) {
                $('input:checkbox[id$=chkBalanceSheet]', repBalanceSheet).each(function () {
                    if ($(this).attr('disabled') == false) {
                        $(this).attr('checked', true);
                    }
                });
            }
            else {
                $('input:checkbox[id$=chkBalanceSheet]', repBalanceSheet).removeAttr('checked');
            }
        });

        if ($('input[id*=hidScrollPositionFlag]').val() != "")
            window.scrollTo(0, $('input[id*=hidScrollPositionFlag]').val());
        //若為修改單據時 則按下runat server按鈕 都需跳回修改單據畫面 故鎖定scroll top位置
        if ($('input[id*=hidUpdateBillRegionFlag]').val() != "")
            window.scrollTo(0, $('input[id*=hidUpdateBillRegionFlag]').val());


        /////////////////////////////編輯單據資料時, 其他東西不可用///////////////////////////////////////////////////////////////
        var mode = $('#hidManagementMode').val();
        if (mode === '2') {
            $('input:not(".manageModeControlFreeZone input")').attr('onclick', '');
            $('select:not(".manageModeControlFreeZone select")').attr('disabled', 'disabled');
            //disabled filed wouldn't pass value when submit.
            $('form').submit(function () {
                $('select:not(".manageModeControlFreeZone select")').removeAttr('disabled');
            });
            $('input:not(".manageModeControlFreeZone input")').click(function () {
                alert('您正在新增單據，請完成/取消單據的新增後再做其他的動作。');
                return false;
            });
        }
        if (mode === '3') {
            $('input:not(".manageModeControlFreeZone input")').attr('onclick', '');
            $('select:not(".manageModeControlFreeZone select")').attr('disabled', 'disabled');
            //disabled filed wouldn't pass value when submit.
            $('form').submit(function () {
                $('select:not(".manageModeControlFreeZone select")').removeAttr('disabled');
            });
            $('input:not(".manageModeControlFreeZone input")').click(function () {
                alert('您正在修改單據，請完成/取消單據的修改後再做其他的動作。');
                return false;
            });
        }
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////

        $('input[id*=txtAddBillId]').keyup(function (e) {
            if (/\D/g.test(this.value)) {
                // Filter non-digits from input value.
                this.value = this.value.replace(/\D/g, '');
            }
        });
    });

    function setupdatepicker(date) {
        $('#' + date).datepicker({
            dateFormat: "yy/mm"
        });
    }

    function btnUpdateBillInfo_Click() {
        $('input[id*=hidScrollPositionFlag]').val('');
        //按下單據[修改]按鈕 須保持視窗停留於單據修改畫面(於畫面最下方)
        $('input[id*=hidUpdateBillRegionFlag]').val(document.body.clientHeight);
    }

    function recordScrollPosition() {
        $('input[id*=hidUpdateBillRegionFlag]').val('');
        //按下server object 因跳轉導致scroll reset 故紀錄目前scroll position位置
        $('input[id*=hidScrollPositionFlag]').val(document.body.scrollTop);
    }

    function updateBsRemarkRequest(ele) {
        var parent = $(ele).parent();
        parent.find('.editMode').show();
        parent.find('.viewMode').hide();
    }

    function updateBillRemarkRequest(ele) {
        var parent = $(ele).parent();
        parent.find('.editMode').show();
        parent.find('.viewMode').hide();
    }

</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        
        <asp:HiddenField ID="hidCurrentTab" runat="server" />
        <asp:HiddenField ID="hidScrollPositionFlag" runat="server" />
        <asp:HiddenField ID="hidManagementMode" runat="server" ClientIDMode="Static" />

    <div id="tabs">  
        <ul>
            <li><a href="#tabs1">單據檢核建檔</a></li>
            <li><a href="#tabs4">餘額單據列表</a></li>
            <li><a href="#tabs2">單據資料查詢管理</a></li>
            <li><a href="#tabs3">未回單據總覽</a></li>
        </ul>

 <!------------------------------------------------頁籤  單據檢核建檔------------------------------------------------------>
    <div id="tabs1">
        
    <table>
        <tr>
            <td><asp:DropDownList ID="ddlST" runat="server" DataTextField="Value" DataValueField="Key" Width="103px" /></td>
            <td><asp:TextBox ID="tbSearch" runat="server" Width="265px" onkeypress="en(event)"></asp:TextBox></td>
        </tr>
        <tr>
            <td>
                <asp:DropDownList ID="ddlDate" runat="server" DataTextField="Value" DataValueField="Key" Width="103px">
                    <asp:ListItem Value="ddlOrderTime">銷售期間</asp:ListItem> 
                    <asp:ListItem Value="ddlDeliverTime">兌換期間</asp:ListItem> 
                </asp:DropDownList>
            </td>
            <td>
                <asp:TextBox ID="tbDS" runat="server" Columns="12" CssClass="date"/>
                至
                <asp:TextBox ID="tbDE" runat="server" Columns="12" CssClass="date"/>
                <asp:Button ID="btnSearch" runat="server" Text="查詢" OnClick="btnSearch_Click" />
            </td>
        </tr>
      </table>

    <!--賣家分店 -->   
      <asp:Label ID="lbSellerStore" runat="server" Visible="false"></asp:Label>
      <asp:Panel ID="setSellerStore" runat="server" Visible="false">
        <table style="width:600px" class="tblBsInfo">
            <tr style="font-weight:bold">
                <td width="6%" align="center"><asp:Checkbox ID="chkSellerStoreSelectedAll" runat="server"/></td>
                <td width="62%"><strong>賣家名稱</strong></td>
                <td width="32%">分店</td>
            </tr>
            <asp:Repeater ID="repSellerStore" runat="server">        
                <ItemTemplate>
                    <tr>          
                        <td align="center"><asp:CheckBox ID="chkSellerStore" runat="server"  value='<%# Eval("SellerGuid") + "/" + Eval("StoreGuid")%>'/></td>
                        <td><asp:Literal runat="server" Text='<%# Eval("SellerName")%>' /></td>
                        <td><asp:Literal runat="server" Text='<%# Eval("StoreName")%>' /></td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
        </table>
        <asp:button ID="btnSellerStore" runat="server" OnClick="btnSellerStore_Click" Text="送出"></asp:button>
      </asp:Panel>
      <br /><br />

      <!--對帳單 -->    
      <asp:Label ID="lbBalanceSheet" runat="server" Visible="false"></asp:Label>
      <asp:Panel ID="setBalanceSheetList" runat="server" Visible="false">
        <table style="width:1100px" class="tblBsInfo">
            <tr style="font-weight:bold">
                <td width="2%" align="center"><asp:Checkbox ID="chkBalanceSheetSelectedAll" runat="server" AutoPostBack="true" OnCheckedChanged="GetgvInvoiceInfo"/></td>
                <td width="3%"><asp:Button ID="confirmed" runat="server" Text='確認匯款' OnClick="confirmed_Click" Width="60px" Font-size="12px"/></td>                
                <td width="4%">對帳單ID</td>
                <td width="6%">對帳單月份</td>
                <td width="6%">檔號/PID</td>
                <td width="5%">檔次類型</td>
                <td width="10%">檔名／商品名稱</td>
                <td width="7%">分店</td>
                <td width="7%">賣家名稱</td>
                <td width="5%">未稅金額</td>
                <td width="5%">稅額</td>
                <td width="5%">發票總額</td>
                <td width="5%">運費</td>
                <td width="5%">異動金額</td>
                <td width="5%">逾期出貨補款</td>
                <td width="5%">逾期出貨扣款</td>
                <td width="5%">物流處理費(全家)</td>
                <td width="5%">物流處理費(7-11)</td>
                <td width="5%">倉儲費用(PChome)</td>
                <td width="5%">物流處理費(PChome)</td>
                <td width="5%" style="display:none;">剩餘未稅金額</td>
                <td width="5%" style="display:none;">剩餘稅額</td>
                <td width="5%">剩餘總額</td>
                <td width="8%">開立方式</td>
                <td width="8%">出帳方式</td>
            </tr>
            <asp:Repeater ID="repBalanceSheet" runat="server" OnItemCommand="repBalanceSheet_ItemCommand" OnItemDataBound="repBalanceSheet_ItemDataBound">        
                <ItemTemplate>
                    <tr id="tdRepBalanceSheet" runat="server">
                            <td align="center">
                                <asp:CheckBox ID="chkBalanceSheet" runat="server" AutoPostBack="True" OnCheckedChanged="GetgvInvoiceInfo" Enabled='<%#GetChkBalanceSheetState(bool.Parse(Eval("IsConfirmedReadyToPay").ToString()), int.Parse(Eval("residualInvoiceTotal").ToString()), int.Parse(Eval("IspFamilyAmount").ToString()), int.Parse(Eval("IspSevenAmount").ToString()), (GetOverdueAmount(Convert.ToInt32(Eval("Id")), true) + GetOverdueAmount(Convert.ToInt32(Eval("Id")), false)),int.Parse(Eval("WmsOrderAmount").ToString()), 0)%>' ClientIDMode="Static" />
                            </td>
                            <td align="center">
                                <asp:CheckBox ID="chkConfirmed" runat="server" AutoPostBack="True" OnCheckedChanged="GetgvInvoiceInfo" ClientIDMode="Static" Enabled='<%#GetconfirmedState(int.Parse(Eval("Id").ToString()),(int)BalanceSheetUseType.Deal)%>' />
                            </td>
                            <td>
                                <asp:Label ID="sid" runat="server" Text='<%# Eval("Id")%>' />
                            </td>
                            <td>
                                <asp:Literal runat="server" Text='<%# GetBalanceSheetDesc(string.Format("{0}/{1}",Eval("year").ToString(), Eval("month").ToString()), DateTime.Parse(Eval("IntervalEnd").ToString()), int.Parse(Eval("GenerationFrequency").ToString()))%>' />
                            </td>
                            <td>
                                <asp:Label ID="uniqueId" runat="server" Text='<%# Eval("uniqueId")%>' />
                            </td>
                            <td>
                                <asp:Label ID="deliveryTypeDesc" runat="server" Text='<%# GetDeliveryTypeDesc(int.Parse(Eval("deliveryType").ToString()))%>' />
                                <asp:HiddenField ID="deptId" runat="server" Value='<%# Eval("deptId")%>' />
                            </td>
                            <td>
                                <asp:Label ID="title" runat="server" Text='<%# Eval("name")%>' /></td>
                            <td>
                                <asp:Label ID="storeName" runat="server" Text='<%# Eval("storeName")%>' />
                                <asp:HiddenField ID="sellerGuid" runat="server" Value='<%# Eval("sellerGuid")%>' />
                            </td>
                            <td>
                                <asp:Literal runat="server" Text='<%# Eval("sellerName")%>' /></td>
                            <td>
                                <asp:Label ID="subTotal" runat="server" Text='<%# Eval("subTotal")%>' />
                            </td>
                            <td>
                                <asp:Label ID="gstTotal" runat="server" Text='<%# Eval("gstTotal")%>' />
                            </td>
                            <td>
                                <asp:Label ID="invoiceTotal" runat="server" Text='<%# Eval("invoiceTotal")%>' />
                            </td>
                            <td>
                                <asp:Label ID="freightAmounts" runat="server" Text='<%# Eval("freightAmounts")%>' />
                            </td>
                            <td>
                                <asp:Label ID="adjustmentAmount" runat="server" Text='<%# Eval("adjustmentAmount")%>' />
                            </td>
                            <td>
                                <asp:Label ID="positivePaymentOverdueAmount" runat="server" Text='<%# GetOverdueAmount(Convert.ToInt32(Eval("Id")), true)%>' />
                            </td>
                            <td>
                                <asp:Label ID="negativePaymentOverdueAmount" runat="server" Text='<%# GetOverdueAmount(Convert.ToInt32(Eval("Id")), false)%>' />
                            </td>
                            <td>
                                <asp:Label ID="ispFamilyAmount" runat="server" Text='<%# Eval("ispFamilyAmount")%>' />
                            </td>
                            <td>
                                <asp:Label ID="ispSevenAmount" runat="server" Text='<%# Eval("ispSevenAmount")%>' />
                            </td>
                            <td>
                                <asp:Label ID="wmsAmount" runat="server" Text='0' />
                            </td>
                            <td>
                                <asp:Label ID="wmsOrderAmount" runat="server" Text='<%# Eval("wmsOrderAmount")%>' />
                            </td>
                            <td style="display:none;">
                                <asp:Label ID="residualSubTotal" runat="server" Text='<%# Eval("residualSubTotal")%>' />
                            </td>
                            <td style="display:none;">
                                <asp:Label ID="residualGstTotal" runat="server" Text='<%# Eval("residualGstTotal")%>' />
                            </td>
                            <td>
                                <asp:Label ID="residualInvoiceTotal" runat="server" Text='<%# Eval("residualInvoiceTotal")%>' />
                            </td>
                            <td>
                                <asp:Label ID="receiptTypeName" runat="server" Text='<%# GetTheReceiptType(int.Parse(Eval("VendorReceiptType").ToString()), (Eval("message") != null) ? Eval("message").ToString() : string.Empty)%>' />
                                <asp:HiddenField ID="receiptType" runat="server" Value='<%# Eval("VendorReceiptType")%>' />
                                <asp:HiddenField ID="id" runat="server" Value='<%# Eval("Id")%>' />
                                <asp:HiddenField ID="productGuid" runat="server" Value='<%# Eval("ProductGuid")%>' />
                                <asp:HiddenField ID="remittanceType" runat="server" Value='<%# Eval("RemittanceType")%>' />
                                <asp:HiddenField ID="companyId" runat="server" Value='<%# Eval("CompanyId")%>' />
                                <asp:HiddenField ID="bsYear" runat="server" Value='<%# Eval("Year")%>' />
                                <asp:HiddenField ID="bsMonth" runat="server" Value='<%# Eval("Month")%>' />
                                <asp:HiddenField ID="bsIntervalEnd" runat="server" Value='<%# Eval("IntervalEnd")%>' />
                                <asp:HiddenField ID="bsGenerationFrequency" runat="server" Value='<%# Eval("GenerationFrequency")%>' />
                                <asp:HiddenField ID="deliveryType" runat="server" Value='<%# Eval("DeliveryType")%>' />
                                <asp:HiddenField ID="isTaxRequire" runat="server" Value='<%# Eval("IsTaxRequire")%>' />
                            </td>
                            <td>
                                <asp:Label ID="remittanceTypeDesc" runat="server" Text='<%# GetRemittanceType(int.Parse(Eval("RemittanceType").ToString()))%>' />
                            </td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>

            <!--倉儲對帳單 -->    
            <asp:Repeater ID="repBalanceSheetWms" runat="server" OnItemCommand="repBalanceSheetWms_ItemCommand" OnItemDataBound="repBalanceSheetWms_ItemDataBound">        
                <ItemTemplate>
                    <tr id="tdRepBalanceSheetWms" runat="server">
                            <td align="center">
                                <asp:CheckBox ID="chkBalanceSheet" runat="server" AutoPostBack="True" OnCheckedChanged="GetgvInvoiceInfo" Enabled='<%#GetChkBalanceSheetState(bool.Parse(Eval("IsConfirmedReadyToPay").ToString()), 0, 0, 0, 0,0,int.Parse(Eval("wmsAmount").ToString()))%>' ClientIDMode="Static" />
                            </td>
                            <td align="center">
                                <asp:CheckBox ID="chkConfirmed" runat="server" AutoPostBack="True" OnCheckedChanged="GetgvInvoiceInfo" ClientIDMode="Static" Enabled='<%#GetconfirmedState(int.Parse(Eval("Id").ToString()),(int)BalanceSheetUseType.Wms)%>' />
                            </td>
                            <td>
                                <asp:Label ID="sid" runat="server" Text='<%# Eval("Id")%>' />
                            </td>
                            <td>
                                <asp:Literal runat="server" Text='<%# GetBalanceSheetDesc(string.Format("{0}/{1}",Eval("year").ToString(), Eval("month").ToString()), DateTime.Parse(Eval("IntervalEnd").ToString()), int.Parse(Eval("GenerationFrequency").ToString()))%>' />
                            </td>
                            <td>
                                <asp:Label ID="uniqueId" runat="server" Text='-' />
                            </td>
                            <td>
                                <asp:Label ID="deliveryTypeDesc" runat="server" Text='<%# GetDeliveryTypeDesc(int.Parse(Eval("deliveryType").ToString()))%>' />
                            </td>
                            <td>
                                <asp:Label ID="title" runat="server" Text='<%# Eval("name")%>' />
                            </td>
                            <td>
                                <asp:Label ID="storeName" runat="server" Text='-' />
                            </td>
                            <td>
                                <asp:Literal runat="server" Text='<%# Eval("sellerName")%>' />
                                <asp:HiddenField ID="sellerGuid" runat="server" Value='<%# Eval("sellerGuid")%>' />
                            </td>
                            <td>
                                <asp:Label ID="subTotal" runat="server" Text='0' />
                            </td>
                            <td>
                                <asp:Label ID="gstTotal" runat="server" Text='0' />
                            </td>
                            <td>
                                <asp:Label ID="invoiceTotal" runat="server" Text='0' />
                            </td>
                            <td>
                                <asp:Label ID="freightAmounts" runat="server" Text='0' />
                            </td>
                            <td>
                                <asp:Label ID="adjustmentAmount" runat="server" Text='0' />
                            </td>
                            <td>
                                <asp:Label ID="positivePaymentOverdueAmount" runat="server" Text='0' />
                            </td>
                            <td>
                                <asp:Label ID="negativePaymentOverdueAmount" runat="server" Text='0' />
                            </td>
                            <td>
                                <asp:Label ID="ispFamilyAmount" runat="server" Text='0' />
                            </td>
                            <td>
                                <asp:Label ID="ispSevenAmount" runat="server" Text='0' />
                            </td>
                            <td>
                                <asp:Label ID="wmsAmount" runat="server" Text='<%# Eval("wmsAmount")%>' />
                            </td>
                            <td>
                                <asp:Label ID="Label2" runat="server" Text='0' />
                            </td>
                            <td style="display:none;">
                                <asp:Label ID="residualSubTotal" runat="server" Text='<%# Eval("residualSubTotal")%>' />
                            </td>
                            <td style="display:none;">
                                <asp:Label ID="residualGstTotal" runat="server" Text='<%# Eval("residualGstTotal")%>' />
                            </td>
                            <td>
                                <asp:Label ID="residualInvoiceTotal" runat="server" Text='<%# Eval("residualInvoiceTotal")%>' />
                            </td>
                            <td>
                                <asp:Label ID="receiptTypeName" runat="server" Text='<%# GetTheReceiptType(int.Parse(Eval("VendorReceiptType").ToString()), string.Empty)%>' />
                            </td>
                            <td>
                                <asp:Label ID="remittanceTypeDesc" runat="server" Text='<%# GetRemittanceType(int.Parse(Eval("RemittanceType").ToString()))%>' />
                                <asp:HiddenField ID="receiptType" runat="server" Value='<%# Eval("VendorReceiptType")%>' />
                                <asp:HiddenField ID="id" runat="server" Value='<%# Eval("Id")%>' />
                                <asp:HiddenField ID="remittanceType" runat="server" Value='<%# Eval("RemittanceType")%>' />
                                <asp:HiddenField ID="bsYear" runat="server" Value='<%# Eval("Year")%>' />
                                <asp:HiddenField ID="bsMonth" runat="server" Value='<%# Eval("Month")%>' />
                                <asp:HiddenField ID="bsIntervalEnd" runat="server" Value='<%# Eval("IntervalEnd")%>' />
                                <asp:HiddenField ID="bsGenerationFrequency" runat="server" Value='<%# Eval("GenerationFrequency")%>' />
                                <asp:HiddenField ID="deliveryType" runat="server" Value='<%# Eval("DeliveryType")%>' />
                            </td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
        </table>
      </asp:Panel>

        <!--合併對帳單 -->   
        <asp:GridView ID="gvSheetListMerger" runat="server" EmptyDataText="無符合的資料" AutoGenerateColumns="False" 
            ShowFooter="True" FooterStyle-HorizontalAlign="Right">
            <Columns>
                <asp:TemplateField HeaderText="分店">
                    <ItemTemplate>
                        <asp:Label ID="storeName" runat="server" Text='<%# Eval("storeName")%>' /> 
                    </ItemTemplate>
                </asp:TemplateField>                    
                <asp:TemplateField HeaderText="對帳單月份">
                    <ItemTemplate>
                        <asp:Label ID="date" runat="server" Text='<%# (int.Parse(Eval("year").ToString()) == 0) ? "" : GetBalanceSheetDesc(string.Format("{0}/{1}", Eval("year").ToString(), Eval("month").ToString()), DateTime.Parse(Eval("IntervalEnd").ToString()), int.Parse(Eval("GenerationFrequency").ToString()))%>' /> 
                    </ItemTemplate>
                </asp:TemplateField> 
                <asp:TemplateField HeaderText="檔號/PID">
                    <ItemTemplate>
                        <asp:Label ID="uniqueId" runat="server" Text='<%# (int.Parse(Eval("uniqueId").ToString()) == 0)  ? "-" : Eval("uniqueId")%>' /> 
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="檔次類型">
                    <ItemTemplate>
                        <asp:Label ID="deliveryTypeDesc" runat="server" Text='<%# GetDeliveryTypeDesc(int.Parse(Eval("deliveryType").ToString()))%>' /> 
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="檔名／商品名稱">
                    <ItemTemplate>
                        <asp:Label ID="title" runat="server" Text='<%# Eval("name")%>' /> 
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:Label ID="lbTitle" runat="server" Text="合計"></asp:Label>
                    </FooterTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="出帳方式">
                    <ItemTemplate>
                        <asp:Label ID="remittanceType" runat="server" Text='<%# GetRemittanceType(int.Parse(Eval("RemittanceType").ToString()))%>' /> 
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="金額"  ItemStyle-HorizontalAlign="right">
                    <ItemTemplate>
                        <asp:Label ID="residualSubTotal" runat="server" Text='<%# Eval("residualSubTotal")%>' /> 
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:Label ID="subTotalDiff" runat="server"></asp:Label>
                        <asp:Label ID="lbResidualSubTotal" runat="server" Text=""></asp:Label><br />
                    </FooterTemplate>
                </asp:TemplateField>                     
                <asp:TemplateField HeaderText="稅額"  ItemStyle-HorizontalAlign="right">
                    <ItemTemplate>
                        <asp:Label ID="residualGstTotal" runat="server" Text='<%# Eval("residualGstTotal")%>' /> 
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:Label ID="gstTotalDiff" runat="server"></asp:Label>
                        <asp:Label ID="lbResidualGstTotal" runat="server" Text=""></asp:Label>
                    </FooterTemplate>
                </asp:TemplateField>                      
                <asp:TemplateField HeaderText="發票總額"  ItemStyle-HorizontalAlign="right">
                    <ItemTemplate>
                        <asp:Label ID="residualInvoiceTotal" runat="server" Text='<%# Eval("residualInvoiceTotal")%>' /> 
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:Label ID="lbResidualInvoiceTotal" runat="server" Text=""></asp:Label>
                    </FooterTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="運費"  ItemStyle-HorizontalAlign="right">
                    <ItemTemplate>
                        <asp:Label ID="freightAmounts" runat="server" Text='<%# Eval("freightAmounts")%>' /> 
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:Label ID="lbFreightAmountsTotal" runat="server" Text=""></asp:Label>
                    </FooterTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="異動金額"  ItemStyle-HorizontalAlign="right">
                    <ItemTemplate>
                        <asp:Label ID="adjustmentAmountTotal" runat="server" Text='<%# Eval("adjustmentAmount")%>' /> 
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:Label ID="lbAdjustmentAmount" runat="server" Text=""></asp:Label>
                    </FooterTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="逾期出貨補款"  ItemStyle-HorizontalAlign="right">
                    <ItemTemplate>
                        <asp:Label ID="positivePaymentOverdueAmountTotal" runat="server" Text='<%# Eval("positivePaymentOverdueAmount")%>' /> 
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:Label ID="lbPositivePaymentOverdue" runat="server" Text=""></asp:Label>
                    </FooterTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="逾期出貨扣款"  ItemStyle-HorizontalAlign="right">
                    <ItemTemplate>
                        <asp:Label ID="negativePaymentOverdueAmountTotal" runat="server" Text='<%# Eval("negativePaymentOverdueAmount")%>' /> 
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:Label ID="lbNegativePaymentOverdue" runat="server" Text=""></asp:Label>
                    </FooterTemplate>
                </asp:TemplateField>
                 <asp:TemplateField HeaderText="物流處理費(全家)"  ItemStyle-HorizontalAlign="right">
                    <ItemTemplate>
                        <asp:Label ID="ispFamilyAmount" runat="server" Text='<%# Eval("ispFamilyAmount")%>' /> 
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:Label ID="lbIspFamilyAmount" runat="server" Text=""></asp:Label>
                    </FooterTemplate>
                </asp:TemplateField>
                 <asp:TemplateField HeaderText="物流處理費(7-11)"  ItemStyle-HorizontalAlign="right">
                    <ItemTemplate>
                        <asp:Label ID="ispSevenAmount" runat="server" Text='<%# Eval("ispSevenAmount")%>' /> 
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:Label ID="lbIspSevenAmount" runat="server" Text=""></asp:Label>
                    </FooterTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="倉儲費用(PChome)"  ItemStyle-HorizontalAlign="right">
                    <ItemTemplate>
                        <asp:Label ID="wmsAmount" runat="server" Text='<%# Eval("wmsAmount")%>' /> 
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:Label ID="lbWmsAmount" runat="server" Text=""></asp:Label>
                    </FooterTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="物流處理費(PChome))"  ItemStyle-HorizontalAlign="right">
                    <ItemTemplate>
                        <asp:Label ID="wmsOrderAmount" runat="server" Text='<%# Eval("wmsOrderAmount")%>' /> 
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:Label ID="lbWmsOrderAmount" runat="server" Text=""></asp:Label>
                    </FooterTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>    
        <br /><br />
        <asp:Panel ID="addBillRegion" runat="server" Visible="false">
            <asp:Label runat="server" Text="請輸入單據ID：" />
            <asp:TextBox ID="txtAddBillId" runat="server" Width="60px" ClientIDMode="Static"/>
            <asp:Button ID="add" runat="server" Text="新增單據資料" onclick="Add_Click" OnClientClick="recordScrollPosition()" />        
        </asp:Panel>
        <br /><br />
        <asp:Panel ID="addBill" runat="server" Visible="false" BorderWidth="1" BorderColor="#999999" Width="800px">
        <div class="manageModeControlFreeZone">
        <uc2:BalanceSheetBill ID="AddBalanceSheetBill" runat="server"/>
        </div>
        <br />
        </asp:Panel>
    
    </div>
    

 <!------------------------------------------------頁籤  單據資料查詢管理-------------------------------------------------->   
     <div id="tabs2">
         
         <table>
             <tr>
                 <td><asp:DropDownList ID="ddlDepartment1" runat="server" DataTextField="Value"  
                         AutoPostBack="true"  DataValueField="Key" Width="125px" /></td>
                 <td><asp:DropDownList ID="ddlRemittanceType" runat="server" DataTextField="Value"  AutoPostBack="true"  DataValueField="Key" Width="120px"/></td>
             </tr>
             <tr>
                 <td>單據開立方式：</td>
                 <td><asp:DropDownList ID="ddlReceiptType" runat="server" DataTextField="Value"  AutoPostBack="true"  DataValueField="Key" Width="150px"/></td>
             </tr>
             <tr>
                 <td>買受人：</td>
                 <td><asp:DropDownList ID="ddlBuyerType" runat="server" DataTextField="Value"  AutoPostBack="true"  DataValueField="Key" Width="100px"/></td>
             </tr>
             <tr>
                 <td><asp:DropDownList ID="ddlSTQuery" runat="server" DataTextField="Value"  AutoPostBack="true"  DataValueField="Key" Width="125px" /></td>
                 <td><asp:TextBox ID="tbSearchQuery" runat="server" Width="265px" onkeypress="en(event)"></asp:TextBox></td>
             </tr>
<%--             <tr>
                 <td>專員寄出單據日：</td>
                 <td>
                    <asp:TextBox ID="tbSentDS" runat="server" Columns="12" CssClass="date"/>
                    至
                    <asp:TextBox ID="tbSentDE" runat="server" Columns="12" CssClass="date"/>
                </td>
             </tr>--%>
             <tr>
                 <td>財務收到單據日：</td>
                 <td>
                    <asp:TextBox ID="tbFinanceGetDateS" runat="server" Columns="12" CssClass="date"/>
                    至
                    <asp:TextBox ID="tbFinanceGetDateE" runat="server" Columns="12" CssClass="date"/>
                </td>
             </tr>
             <tr>
                 <td>單據建檔日：</td>
                 <td>
                    <asp:TextBox ID="tbCreateTimeS" runat="server" Columns="12" CssClass="date"/>
                    至
                    <asp:TextBox ID="tbCreateTimeE" runat="server" Columns="12" CssClass="date"/>
                </td>
             </tr>
            <tr>
                 <td>預計付款日：</td>
                 <td>
                    <asp:TextBox ID="tbDefaultPaymentTimeS" runat="server" Columns="12" CssClass="date"/>
                    至
                    <asp:TextBox ID="tbDefaultPaymentTimeE" runat="server" Columns="12" CssClass="date"/>
                </td>
             </tr>
             <tr>
                 <td>對帳單月份：</td>
                 <td>
                    <asp:TextBox ID="tbSheetDS" runat="server" Columns="8" CssClass="date"/>
                    至
                    <asp:TextBox ID="tbSheetDE" runat="server" Columns="8" CssClass="date"/>
                </td>
             </tr>
             <tr>
                 <td>檔次類型：</td>
                 <td><asp:DropDownList ID="ddlDeliveryType" runat="server" DataTextField="Value"  AutoPostBack="true"  DataValueField="Key" Width="90px"/></td>
             </tr>
             <tr>
                 <td>開立發票日期：</td>
                 <td>
                     <asp:TextBox ID="tbVendorInvoiceNumberTimeS" runat="server" Columns="10" CssClass="date"/>
                    至
                    <asp:TextBox ID="tbVendorInvoiceNumberTimeE" runat="server" Columns="10" CssClass="date"/>
                 </td>
             </tr>
         </table>
          <asp:Button ID="btnQueryBillInfo" runat="server" Text="查詢" OnClick="btnQueryBillInfo_Click" />
         
         <br /><br />
    <table id="tblRegionSelect" runat="server" border="0" cellspacing="0" cellpadding="0" width="100px" visible="false">
        <tr>
            <td>
                <asp:button ID="btnExportBsBillInfo" runat="server" Text="匯出Excel" OnClick="btnExportBsBillInfo_OnClick" />
            </td>
        </tr>
    </table><br/><br/>
    <asp:Label ID="lbBillInfoCount" runat="server" Visible="false"></asp:Label>
    <asp:Panel ID="setBillInfo" runat="server" Visible="false">
        <table style="width:2095px" class= "tblBillInfo">
            <tr>
<%--                <td style="width: 40px">
                    <asp:Checkbox ID="chkBillSentDateSelectedAll" runat="server"/>
                    專員寄出單據日
                    <asp:button ID="btnUpdateBillSentDate" runat="server" Text="寄出" Width="45px" OnClick="btnUpdateBillSentDate_Click"/>
                </td>--%>
                <td style="width: 40x">
                    <asp:Checkbox ID="chkFinanceGetDateSelectedAll" runat="server"/>
                    財務收到單據日
                    <asp:button ID="btnUpdateFinanceGetDate" runat="server" Text="收到" Width="45px" OnClick="btnUpdateFinanceGetDate_Click"/>
                </td>
                <td style="width: 80px">
                    對帳單類型
                </td>
                <td style="width: 80px">
                    出帳方式
                </td>
                <td style="width: 50px">
                    對帳單ID
                </td>
                <td style="width: 80px">
                    單據建檔日
                </td>
                <td style="width: 80px">
                    預計付款日
                </td>
                <td style="width: 80px">
                    對帳單月份
                </td>
                <td style="width: 50px">
                    對帳單金額
                </td>
                <td style="width: 80px">
                    單據開立方式
                </td>
                <td style="width: 45px">
                    買受人
                </td>
                <td style="width: 80px">
                    結檔日
                </td>
                <td style="width: 60px">
                    檔號
                </td>
                <td style="width: 80px">
                    檔次類型
                </td>
                <td style="width: 120px">
                    檔名
                </td>
                <td style="width: 90px">
                    分店
                </td>
                <td style="width: 90px">
                    簽約公司
                </td>
                <td style="width: 60px">
                    發票對應統編
                </td>
                <td style="width: 80px">
                    單據開立日期
                </td>
                <td style="width: 90px">
                    發票號碼
                </td>
                <td style="width: 60px">
                    未稅金額
                </td>
                <td style="width: 60px">
                    稅額
                </td>
                <td style="width: 60px">
                    發票總額
                </td>
                <td style="width: 60px">
                    運費
                </td>
                <td style="width: 60px">
                    異動金額
                </td>
                <td style="width: 60px">
                    逾期出貨補款
                </td>
                <td style="width: 60px">
                    逾期出貨扣款
                </td>
                <td style="width: 60px">
                    物流處理費(全家)
                </td>
                <td style="width: 60px">
                    物流處理費(7-11)
                </td>
                <td style="width: 60px">
                    倉儲費用(PChome)
                </td>
                <td style="width: 60px">
                    物流處理費(PChome)
                </td>
                <td style="width: 60px">
                    開立發票號碼 
                </td>
                <td style="width: 60px">
                    開立發票日期
                </td>
                <td style="width: 90px">
                    受款戶名
                </td>
                <td style="width: 60px">
                    受款ID
                </td>
                <td style="width: 120px">
                    受款帳號
                </td>
                <td style="width: 45px">
                    銀行代號
                </td>
                <td style="width: 45px">
                    分行代號
                </td>
                <td style="width: 120px">
                    單據備註
                </td>
                <td style="width: 120px">
                    對帳單備註
                </td>
                <td style="width: 90px">
                    業務
                </td>
                <td style="width: 100px">
                    建檔人
                </td>
            </tr>
            <asp:Repeater ID="repInvoiceQuery" runat="server" OnItemCommand="repInvoiceQuery_ItemCommand" OnItemDataBound="repInvoiceQuery_ItemDataBound">        
                <ItemTemplate>
                    <tr>      
<%--                        <td>
                            <asp:Checkbox ID="chkBillSentDate" runat="server" Visible='<%# (bool.Parse(Eval("IsConfirmedReadyToPay").ToString()) && Eval("BillSentDate") == null)%>' value='<%# Eval("BillId")%>'/>
                            <asp:Literal runat="server" Text='<%# Eval("BillSentDate") != null ? GetTheDate(DataBinder.Eval(Container, "DataItem.BillSentDate").ToString()) :"" %>' /> 
                        </td>--%>
                        <td>
                            <asp:Checkbox ID="chkFinanceGetDate" runat="server" Visible='<%# (bool.Parse(Eval("IsConfirmedReadyToPay").ToString()) && Eval("FinanceGetDate") == null)%>' value='<%# Eval("BillId")%>' />
                            <asp:Literal runat="server" Text='<%# Eval("FinanceGetDate") != null ? GetTheDate(DataBinder.Eval(Container, "DataItem.FinanceGetDate").ToString()) :"" %>' /> 
                        </td>
                        <td>
                            <asp:Literal runat="server" Text='<%# GetBalanceSheetUseType(int.Parse(Eval("BalanceSheetUseType").ToString())) %>' /> 
                        </td>
                        <td>
                            <asp:Literal runat="server" Text='<%# GetRemittanceType(int.Parse(Eval("RemittanceType").ToString())) %>' /> 
                        </td>
                        <td>
                            <asp:Literal runat="server" Text='<%# Eval("Id").ToString() %>' /> 
                        </td>
                        <td>
                            <asp:Literal runat="server" Text='<%# Eval("CreateTime")!=null ? GetTheDate(DataBinder.Eval(Container, "DataItem.CreateTime").ToString()) :"" %>' /> 
                        </td>
                        <td>
                            <asp:Literal runat="server" Text='<%# Eval("DefaultPaymentTime")!=null ? GetTheDate(DataBinder.Eval(Container, "DataItem.DefaultPaymentTime").ToString()) :"" %>' /> 
                        </td>
                        <td>
                            <asp:Literal runat="server" Text='<%# GetBalanceSheetDesc(string.Format("{0}/{1}", Eval("year").ToString(), Eval("month").ToString()), DateTime.Parse(Eval("IntervalEnd").ToString()), int.Parse(Eval("GenerationFrequency").ToString()))%>' /> 
                        </td>
                        <td>
                            <asp:Literal runat="server" Text='<%# Eval("EstAmount")%>' />
                        </td>
                        <td>
                            <asp:Literal ID="Literal1" runat="server" Text='<%# GetTheReceiptType(int.Parse(Eval("VendorReceiptType").ToString()), (Eval("Message") != null) ? Eval("Message").ToString() : string.Empty) %>' /> 
                        </td>
                        <td>
                            <asp:Literal ID="Literal2" runat="server" Text='<%# (Eval("buyerType") != null) ? GetTheBuyerName(int.Parse(Eval("buyerType").ToString())) : Eval("buyerType")%>' /> 
                        </td>
                        <td>
                            <asp:Literal runat="server" Text='<%# GetTheDate(DataBinder.Eval(Container, "DataItem.DealEndTime").ToString()) %>'/> 
                        </td>
                        <td>
                            <asp:Literal runat="server" Text='<%# Eval("uniqueId")%>' /> 
                        </td>
                        <td>
                            <asp:Literal runat="server" Text='<%# GetDeliveryTypeDesc(int.Parse(Eval("deliveryType").ToString()))%>' /> 
                        </td>
                        <td>
                            <asp:Literal runat="server" Text='<%# Eval("name")%>' /> 
                        </td>
                        <td>
                            <asp:Literal runat="server" Text='<%# Eval("storeName")%>' />
                        </td>
                        <td>
                            <asp:Literal runat="server" Text='<%# Eval("companyName")%>' />
                        </td>
                        <td>
                            <asp:Literal ID="Literal3" runat="server" Text='<%# Eval("InvoiceComId")%>'  /> 
                        </td>
                        <td>
                            <asp:Literal runat="server" Text='<%# (Eval("InvoiceDate") != null) ? GetTheDate(DataBinder.Eval(Container, "DataItem.InvoiceDate").ToString()) : Eval("InvoiceDate") %>'  /> 
                        </td>
                        <td>
                            <asp:Literal runat="server" Text='<%# Eval("billNumber")%>' /> 
                            <asp:button ID="btnUpdateBillInfo" runat="server" Text="修改" Visible='<%# (Eval("BillId") != null)%>' Enabled='<%# (!bool.Parse(Eval("IsConfirmedReadyToPay").ToString()) || CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.UpdateConfirmedBsBill))%>' CommandName="UpdateBill" CommandArgument='<%# Eval("Id") + "/" + Eval("BillId") %>' OnClientClick="btnUpdateBillInfo_Click()"/>
                            <asp:button ID="btnDeleteBill" runat="server" Text="刪除" Visible='<%# (Eval("BillId") != null)%>' Enabled='<%# (!bool.Parse(Eval("IsConfirmedReadyToPay").ToString()) || CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.UpdateConfirmedBsBill))%>' CommandName="DeleteBill" CommandArgument='<%# Eval("Id") + "/" + Eval("BillId") + "/" + (int)BalanceSheetUseType.Deal %>'/>
                        </td>
                        <td>
                            <asp:Literal runat="server" Text='<%# Eval("billMoneyNotaxed")%>'/>
                        </td>
                        <td>
                            <asp:Literal runat="server" Text='<%# Eval("billTax")%>' />
                        </td>
                        <td>
                            <asp:Literal runat="server" Text='<%# Eval("billMoney")%>' />
                        </td>
                        <td>
                            <asp:Literal runat="server" Text='<%# Eval("FreightAmounts")%>' />
                        </td>
                        <td>
                            <asp:Literal runat="server" Text='<%# Eval("AdjustmentAmount")%>' />
                        </td>
                        <td>
                            <asp:Label runat="server" Text='<%# GetOverdueAmount(Convert.ToInt32(Eval("Id")), true)%>' />
                        </td>
                        <td>
                            <asp:Label runat="server" Text='<%# GetOverdueAmount(Convert.ToInt32(Eval("Id")), false)%>' />
                        </td>
                        <td>
                            <asp:Literal runat="server" Text='<%# Eval("ispFamilyAmount")%>' />
                        </td>
                        <td>
                            <asp:Literal runat="server" Text='<%# Eval("ispSevenAmount")%>' />
                        </td>
                         <td>
                            <asp:Literal runat="server" Text='0' />
                        </td>
                        <td>
                            <asp:Literal runat="server" Text='<%# Eval("wmsOrderAmount")%>' />
                        </td>
                        <td>
                            <asp:Literal runat="server" Text='<%# Eval("vendorInvoiceNumber")%>'/>
                        </td>
                        <td>
                            <asp:Literal runat="server" Text='<%# Eval("vendorInvoiceNumberTime")!=null ? GetTheDate(DataBinder.Eval(Container, "DataItem.vendorInvoiceNumberTime").ToString()) :"" %>' />
                        </td>
                        <td>
                            <asp:Literal runat="server" Text='<%# Eval("companyAccountName")%>' />
                        </td>
                        <td>
                            <asp:Literal runat="server" Text='<%# Eval("companyID")%>' />
                        </td>
                        <td>
                            <asp:Literal runat="server" Text='<%# Eval("companyAccount")%>' />
                        </td>
                        <td>
                            <asp:Literal runat="server" Text='<%# Eval("companyBankCode")%>' /> 
                        </td>
                        <td>
                            <asp:Literal runat="server" Text='<%# Eval("companyBranchCode")%>' /> 
                        </td>
                        <td>
                            <asp:TextBox style="display:none;" ID="txtBillRemark" class="editMode" 
                                runat="server" ClientIDMode="Static" Text='<%# Eval("BlRemark")%>' />
                            <asp:Literal ID="Literal4" runat="server" Text='<%# Eval("BlRemark")%>' /> 
                            <input type="button" value="修改" class="viewMode" onclick="updateBillRemarkRequest(this)"/>
                            <asp:button ID="Button1" class="editMode" runat="server" 
                                Text="儲存" style="display:none;"                                
                                CommandName="UpdateBillRemark" 
                                CommandArgument='<%#Eval("BillId") %>' />
                        </td>
                        <td>
                            <asp:TextBox style="display:none;" ID="txtBsRemark" class="editMode" 
                                runat="server" ClientIDMode="Static" Text='<%# Eval("remark")%>' />
                            <asp:Label ID="Label1" runat="server" class="viewMode" Text='<%# Eval("remark")%>' /> 
                            <input type="button" value="修改" class="viewMode" onclick="updateBsRemarkRequest(this)"/>
                            <asp:button ID="Button2" class="editMode" runat="server" 
                                Text="儲存" style="display:none;"                                
                                CommandName="UpdateBsRemark" 
                                CommandArgument='<%# Eval("Id") + "/" + Eval("BillId") %>' />
                        </td>
                        <td>
                            <asp:Literal runat="server" Text='<%# Eval("empName")%>' /> 
                        </td>
                        <td>
                            <asp:Literal runat="server" Text='<%# Eval("createId")%>' />
                        </td>
                    </tr>            
                </ItemTemplate>
            </asp:Repeater>
            <asp:Repeater ID="repInvoiceWmsQuery" runat="server" OnItemCommand="repInvoiceWmsQuery_ItemCommand" OnItemDataBound="repInvoiceWmsQuery_ItemDataBound">        
                <ItemTemplate>
                    <tr>      
<%--                        <td>
                            <asp:Checkbox ID="chkBillSentDate" runat="server" Visible='<%# (bool.Parse(Eval("IsConfirmedReadyToPay").ToString()) && Eval("BillSentDate") == null)%>' value='<%# Eval("BillId")%>'/>
                            <asp:Literal runat="server" Text='<%# Eval("BillSentDate") != null ? GetTheDate(DataBinder.Eval(Container, "DataItem.BillSentDate").ToString()) :"" %>' /> 
                        </td>--%>
                        <td>
                            <asp:Checkbox ID="chkFinanceGetDate" runat="server" Visible='<%# (bool.Parse(Eval("IsConfirmedReadyToPay").ToString()) && Eval("FinanceGetDate") == null)%>' value='<%# Eval("BillId")%>' />
                            <asp:Literal runat="server" Text='<%# Eval("FinanceGetDate") != null ? GetTheDate(DataBinder.Eval(Container, "DataItem.FinanceGetDate").ToString()) :"" %>' /> 
                        </td>
                        <td>
                            <asp:Literal runat="server" Text='<%# GetBalanceSheetUseType(int.Parse(Eval("BalanceSheetUseType").ToString())) %>' /> 
                        </td>
                        <td>
                            <asp:Literal runat="server" Text='<%# GetRemittanceType(int.Parse(Eval("RemittanceType").ToString())) %>' /> 
                        </td>
                        <td>
                            <asp:Literal runat="server" Text='<%# Eval("Id").ToString() %>' /> 
                        </td>
                        <td>
                            <asp:Literal runat="server" Text='<%# Eval("CreateTime")!=null ? GetTheDate(DataBinder.Eval(Container, "DataItem.CreateTime").ToString()) :"" %>' /> 
                        </td>
                        <td>
                            <asp:Literal runat="server" Text='<%# Eval("DefaultPaymentTime")!=null ? GetTheDate(DataBinder.Eval(Container, "DataItem.DefaultPaymentTime").ToString()) :"" %>' /> 
                        </td>
                        <td>
                            <asp:Literal runat="server" Text='<%# GetBalanceSheetDesc(string.Format("{0}/{1}", Eval("year").ToString(), Eval("month").ToString()), DateTime.Parse(Eval("IntervalEnd").ToString()), int.Parse(Eval("GenerationFrequency").ToString()))%>' /> 
                        </td>
                        <td>
                            <asp:Literal runat="server" Text='<%# Eval("EstAmount")%>' />
                        </td>
                        <td>
                            <asp:Literal ID="Literal1" runat="server" Text='<%# GetTheReceiptType(int.Parse(Eval("VendorReceiptType").ToString()), (Eval("Message") != null) ? Eval("Message").ToString() : string.Empty) %>' /> 
                        </td>
                        <td>
                            <asp:Literal ID="Literal2" runat="server" Text='<%# (Eval("buyerType") != null) ? GetTheBuyerName(int.Parse(Eval("buyerType").ToString())) : Eval("buyerType")%>' /> 
                        </td>
                        <td>
                            <asp:Literal runat="server" Text='-'/> 
                        </td>
                        <td>
                            <asp:Literal runat="server" Text='-' /> 
                        </td>
                        <td>
                            <asp:Literal runat="server" Text='<%# GetDeliveryTypeDesc(int.Parse(Eval("deliveryType").ToString()))%>' /> 
                        </td>
                        <td>
                            <asp:Literal runat="server" Text='<%# Eval("name")%>' /> 
                        </td>
                        <td>
                            <asp:Literal runat="server" Text='' />
                        </td>
                        <td>
                            <asp:Literal runat="server" Text='<%# Eval("companyName")%>' />
                        </td>
                        <td>
                            <asp:Literal ID="Literal3" runat="server" Text='<%# Eval("InvoiceComId")%>'  /> 
                        </td>
                        <td>
                            <asp:Literal runat="server" Text='<%# (Eval("InvoiceDate") != null) ? GetTheDate(DataBinder.Eval(Container, "DataItem.InvoiceDate").ToString()) : Eval("InvoiceDate") %>'  /> 
                        </td>
                        <td>
                            <asp:Literal runat="server" Text='<%# Eval("billNumber")%>' /> 
                            <asp:button ID="btnUpdateBillInfo" runat="server" Text="修改" Visible='<%# (Eval("BillId") != null)%>' Enabled='<%# (!bool.Parse(Eval("IsConfirmedReadyToPay").ToString()) || CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.UpdateConfirmedBsBill))%>' CommandName="UpdateBill" CommandArgument='<%# Eval("Id") + "/" + Eval("BillId") %>' OnClientClick="btnUpdateBillInfo_Click()"/>
                            <asp:button ID="btnDeleteBill" runat="server" Text="刪除" Visible='<%# (Eval("BillId") != null)%>' Enabled='<%# (!bool.Parse(Eval("IsConfirmedReadyToPay").ToString()) || CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.UpdateConfirmedBsBill))%>' CommandName="DeleteBill" CommandArgument='<%# Eval("Id") + "/" + Eval("BillId")  + "/" + (int)BalanceSheetUseType.Wms%>'/>
                        </td>
                        <td>
                            <asp:Literal runat="server" Text='0'/>
                        </td>
                        <td>
                            <asp:Literal runat="server" Text='0' />
                        </td>
                        <td>
                            <asp:Literal runat="server" Text='0' />
                        </td>
                        <td>
                            <asp:Literal runat="server" Text='0' />
                        </td>
                        <td>
                            <asp:Literal runat="server" Text='0' />
                        </td>
                        <td>
                            <asp:Label runat="server" Text='0' />
                        </td>
                        <td>
                            <asp:Label runat="server" Text='0' />
                        </td>
                        <td>
                            <asp:Literal runat="server" Text='0' />
                        </td>
                        <td>
                            <asp:Literal runat="server" Text='0' />
                        </td>
                         <td>
                            <asp:Literal runat="server" Text='<%# Eval("wmsAmount")%>' />
                        </td>
                        <td>
                            <asp:Literal runat="server" Text='0' />
                        </td>
                        <td>
                            <asp:Literal runat="server" Text='-'/>
                        </td>
                        <td>
                            <asp:Literal runat="server" Text='-' />
                        </td>
                        <td>
                            <asp:Literal runat="server" Text='<%# Eval("companyAccountName")%>' />
                        </td>
                        <td>
                            <asp:Literal runat="server" Text='<%# Eval("companyID")%>' />
                        </td>
                        <td>
                            <asp:Literal runat="server" Text='<%# Eval("companyAccount")%>' />
                        </td>
                        <td>
                            <asp:Literal runat="server" Text='<%# Eval("companyBankCode")%>' /> 
                        </td>
                        <td>
                            <asp:Literal runat="server" Text='<%# Eval("companyBranchCode")%>' /> 
                        </td>
                        <td>
                            <asp:TextBox style="display:none;" ID="txtBillRemark" class="editMode" 
                                runat="server" ClientIDMode="Static" Text='<%# Eval("BlRemark")%>' />
                            <asp:Literal ID="Literal4" runat="server" Text='<%# Eval("BlRemark")%>' /> 
                            <input type="button" value="修改" class="viewMode" onclick="updateBillRemarkRequest(this)"/>
                            <asp:button ID="Button1" class="editMode" runat="server" 
                                Text="儲存" style="display:none;"                                
                                CommandName="UpdateBillRemark" 
                                CommandArgument='<%#Eval("BillId") %>' />
                        </td>
                        <td>
                            <asp:TextBox style="display:none;" ID="txtBsRemark" class="editMode" 
                                runat="server" ClientIDMode="Static" Text='<%# Eval("remark")%>' />
                            <asp:Label ID="Label1" runat="server" class="viewMode" Text='<%# Eval("remark")%>' /> 
                            <input type="button" value="修改" class="viewMode" onclick="updateBsRemarkRequest(this)"/>
                            <asp:button ID="Button2" class="editMode" runat="server" 
                                Text="儲存" style="display:none;"                                
                                CommandName="UpdateBsRemark" 
                                CommandArgument='<%# Eval("Id") + "/" + Eval("BillId") %>' />
                        </td>
                        <td>
                            <asp:Literal runat="server" Text='<%# Eval("empName")%>' /> 
                        </td>
                        <td>
                            <asp:Literal runat="server" Text='<%# Eval("createId")%>' />
                        </td>
                    </tr>            
                </ItemTemplate>
            </asp:Repeater>
        </table>
        <uc1:Pager ID="gridPager" runat="server" PageSize="15" OnGetCount="RetrieveDataCount" SetPagingWhenPageLoad="false"
            OnUpdate="UpdateHandler"></uc1:Pager>
    </asp:Panel>   
    <br/><br/>
    <asp:HiddenField ID="hidUpdateBillRegionFlag" runat="server" /><%--維護單據資料時 須記錄目前scroll-top位置以保持視窗停留於修改單據畫面位置--%>     
    <asp:HiddenField ID="hidDeliveryType" runat="server" />       
    <asp:HiddenField ID="hidIsAgreePponNewContractSeller" runat="server" />
    <asp:HiddenField ID="hidIsAgreeHouseNewContractSeller" runat="server" />
         
               
    <asp:Panel ID="updateBillInfo" runat="server" Visible="false" BorderWidth="1" BorderColor="#999999" Width="800px">
    <div class="manageModeControlFreeZone">
        <uc2:BalanceSheetBill ID="UpdateBalanceSheetBill" runat="server" />    
    </div>
    </asp:Panel>
     </div>
 
 <!------------------------------------------------頁籤  未回單據總覽------------------------------------------------------>   
     <div id="tabs3">
        <table border="0" cellspacing="0" cellpadding="0" width="350px">
            <tr>
                <td>
                    <asp:DropDownList ID="ddlDepartment2" runat="server" DataTextField="Value"  AutoPostBack="true"  DataValueField="Key" Width="200px" OnSelectedIndexChanged="ddlDepartment2_SelectedIndexChanged" />
                </td>
                <td width="60">
                    <asp:button ID="btnExportUnReceiptReceivedInfo" runat="server" Text="匯出Excel" OnClick="btnExportUnReceiptReceivedInfo_OnClick" />
                </td>
            </tr>
        </table>
        <br/>
        <asp:GridView ID="gvUnReceiptReceived" ClientIDMode="Static" runat="server" EmptyDataText="無符合的資料" 
			AutoGenerateColumns="False" Width="1500px" EnableViewState="false">
            <Columns>
                <asp:TemplateField HeaderText="單據狀態">
                    <ItemTemplate>
                        <asp:Literal runat="server" Text='未建檔' />                         
                    </ItemTemplate>
                    <ItemStyle Width="80px" />
                </asp:TemplateField> 
                <asp:TemplateField HeaderText="對帳單月份">
                    <ItemTemplate>
                        <asp:Literal runat="server" Text='<%# GetBalanceSheetDesc(string.Format("{0}/{1}", Eval("Year").ToString(), Eval("Month").ToString()), DateTime.Parse(Eval("IntervalEnd").ToString()), int.Parse(Eval("GenerationFrequency").ToString()))%>' />
                    </ItemTemplate>
                </asp:TemplateField> 
                <asp:TemplateField HeaderText="月對帳單" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="60px">
                    <ItemTemplate>
                        <a href='<%# ResolveUrl("~/vbs/") + (int.Parse(Eval("GenerationFrequency").ToString()) == 2 ? "BalanceSheetsMonth/" : "BalanceSheetsManual/") + Eval("ProductGuid") + "/" + (int.Parse(Eval("ProductType").ToString()) == 0 ? "PPon" : "PiinLife") + "/" + (Eval("StoreGuid") == null ? "null" : Eval("StoreGuid").ToString()) + "?balanceSheetId=" + Eval("BalanceSheetId").ToString()%>'  style="text-decoration:  none;	color: #1E90FF;"  target="_blank">
                            <asp:Literal runat="server" Text='查看' />
                        </a>
                    </ItemTemplate>
                </asp:TemplateField> 
                <asp:TemplateField HeaderText="檔號/PID">
                    <ItemTemplate>
                        <asp:Literal runat="server" Text='<%# Eval("UniqueId")%>' />                         
                    </ItemTemplate>
                    <ItemStyle Width="80px" />
                </asp:TemplateField> 
                <asp:BoundField ReadOnly="True" DataField="Name" HeaderText="檔名/商品名稱"/>
                <asp:BoundField ReadOnly="True" DataField="StoreName" HeaderText="分店"/>
                <asp:BoundField ReadOnly="True" DataField="BalanceSheetTotal" HeaderText="對帳單總額" ItemStyle-Width="80px" ItemStyle-HorizontalAlign="Right"/>
                <asp:BoundField ReadOnly="True" DataField="ResidualBalanceSheetTotal" HeaderText="單據未回總額" ItemStyle-Width="80px" ItemStyle-HorizontalAlign="Right"/>
                <asp:TemplateField HeaderText="開立方式" >
                    <ItemTemplate>
                        <asp:Literal runat="server" Text='<%# GetTheReceiptType(int.Parse(Eval("VendorReceiptType").ToString()), (Eval("Message") != null) ? Eval("Message").ToString() : string.Empty)%>' /> 
                    </ItemTemplate>
                    <ItemStyle Width="100px" />
                </asp:TemplateField>                    
                <asp:TemplateField HeaderText="出帳方式" >
                    <ItemTemplate>
                        <asp:Literal runat="server" Text='<%# GetRemittanceType(int.Parse(Eval("RemittanceType").ToString()))%>' /> 
                    </ItemTemplate>
                    <ItemStyle Width="100px" />
                </asp:TemplateField>                  
                <asp:TemplateField HeaderText="帳務聯絡人" ItemStyle-Width="80px">
                    <ItemTemplate>
                        <asp:Literal runat="server" Text='<%# Eval("AccountantName")%>' /> 
                    </ItemTemplate>
                </asp:TemplateField>                           
                <asp:TemplateField HeaderText="帳務連絡電話" ItemStyle-Width="80px">
                    <ItemTemplate>
                        <asp:Literal runat="server" Text='<%# Eval("AccountantTel")%>' /> 
                    </ItemTemplate>
                </asp:TemplateField>                
                <asp:TemplateField HeaderText="帳務連絡Email" ItemStyle-Width="100px">
                    <ItemTemplate>
                        <asp:Literal runat="server" Text='<%# Eval("AccountantEmail")%>' /> 
                    </ItemTemplate>
                </asp:TemplateField>        
                <asp:BoundField ReadOnly="True" DataField="EmpName" HeaderText="所屬業務"/>
            </Columns>
        </asp:GridView>
     </div>

 <!------------------------------------------------頁籤  餘額單據列表------------------------------------------------------>   
     <div id="tabs4">
        <table border="0" cellspacing="0" cellpadding="0" width="300px">
            <tr>
                <td>
                    <asp:DropDownList ID="ddlDepartment3" runat="server" DataTextField="Value"  AutoPostBack="true"  DataValueField="Key" Width="120px" />
                </td>
                <td colspan="2">&nbsp;</td>
            </tr>
            <tr>
                <td>                    
                    <asp:Literal runat="server" Text="開立人統編："/>
                </td>
                <td>
                    <asp:TextBox ID="txtInvoiceComId" runat="server" Width="100px"></asp:TextBox>
                </td>
                <td>
                    <asp:button ID="btnQueryRemainderBill" runat="server" Text="查詢" OnClick="btnQueryRemainderBill_OnClick" />
                </td>
            </tr>
        </table>
        <br/>
        <asp:GridView ID="gvRemainderBill" runat="server" EmptyDataText="無符合的資料" AutoGenerateColumns="False" Width="900px">
             <Columns>
                <asp:BoundField ReadOnly="True" DataField="Id" HeaderText="單據ID">
                    <ItemStyle HorizontalAlign="Center" Width="60px" />
                </asp:BoundField>
                <asp:BoundField ReadOnly="True" DataField="SellerName" HeaderText="賣家名稱">
                    <ItemStyle HorizontalAlign="Center" Width="200px" />
                </asp:BoundField>
                <asp:BoundField ReadOnly="True" DataField="InvoiceComId" HeaderText="開立人統編">
                    <ItemStyle HorizontalAlign="Center" Width="90px" />
                </asp:BoundField>
                <asp:TemplateField HeaderText="單據建檔日">
                    <ItemTemplate>
                        <asp:Literal runat="server" Text='<%# GetTheDate(Eval("CreateTime").ToString())%>' /> 
                    </ItemTemplate>
                    <ItemStyle Width="100px" />
                </asp:TemplateField>              
                <asp:BoundField ReadOnly="True" DataField="BillNumber" HeaderText="單據號碼">  
                    <ItemStyle Width="100px" /> 
                </asp:BoundField>                                             
                <asp:BoundField ReadOnly="True" DataField="Remainder" HeaderText="單據剩餘金額">  
                    <ItemStyle HorizontalAlign="Center" Width="100px" /> 
                </asp:BoundField>                              
                <asp:BoundField ReadOnly="True" DataField="Remark" HeaderText="單據備註">                
                    <ItemStyle Width="250px" />
                </asp:BoundField>       
                <asp:TemplateField HeaderText="單據類型">
                    <ItemTemplate>
                        <asp:Literal runat="server" Text='<%# GetTheReceiptType(int.Parse(Eval("ReceiptType").ToString()), Eval("Message").ToString())%>' /> 
                    </ItemTemplate>
                    <ItemStyle Width="100px" />
                </asp:TemplateField>        
            </Columns>
        </asp:GridView>
     </div>




 </div>
  </div>
</asp:Content>
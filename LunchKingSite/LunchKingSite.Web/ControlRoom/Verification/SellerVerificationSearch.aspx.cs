﻿using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.I18N;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text.RegularExpressions;
using System.Web.UI.WebControls;

namespace LunchKingSite.Web.ControlRoom.Verification
{
    public partial class SellerVerificationSearch : RolePage, ISellerVerificationSearchView
    {
        #region event

        public event EventHandler<EventArgs> OkButtonClick;

        #endregion event

        #region prop

        private SellerVerificationSearchPresenter _presenter;

        public SellerVerificationSearchPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        public Dictionary<string, string> FilterTypes
        {
            get
            {
                Dictionary<string, string> data = new Dictionary<string, string>();

                data.Add(ViewPponDeal.Columns.UniqueId, Phrase.BusinessHourUniqueId);
                data.Add(ViewPponDeal.Columns.BusinessHourGuid, Phrase.BusinessHourGuid);

                return data;
            }
        }

        public string FilterType
        {
            get
            {
                return ddlSearch.SelectedValue;
            }
        }

        public string FilterText
        {
            get
            {
                return txtFilterText.Text.Trim();
            }
        }

        #endregion prop

        #region page

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                this.Presenter.OnViewInitialized();
            }
            this.Presenter.OnViewLoaded();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            if (Validation(FilterType, FilterText))
            {
                if (this.OkButtonClick != null)
                {
                    this.OkButtonClick(null, e);
                }
            }
        }

        protected void repSellerVerificationSearch_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                PponDealSalesInfo ds = (PponDealSalesInfo)e.Item.DataItem;
                Literal ltlSaleCount = (Literal)e.Item.FindControl("ltlSaleCount");
                Literal ltlAfterDealCloseSaleCount = (Literal)e.Item.FindControl("ltlAfterDealCloseSaleCount");
                HyperLink hkItemName = (HyperLink)e.Item.FindControl("hkItemName");
                hkItemName.Text = ds.ItemName;
                hkItemName.NavigateUrl = "../Verification/VerificationInventory.aspx?bid=" + ds.MerchandiseGuid;

                ltlSaleCount.Text = (ds.UnverifiedCount + ds.VerifiedCount + ds.ReturnedCount + ds.ForceReturnCount + ds.ForceVerifyCount).ToString();
                
                //結檔銷售數:已結檔之檔次才需顯示
                if (ds.IsDealClose)
                {
                    ltlAfterDealCloseSaleCount.Text = (Convert.ToInt32(ltlSaleCount.Text) - ds.BeforeDealEndReturnedCount).ToString();
                }
                else
                {
                    ltlAfterDealCloseSaleCount.Text = "未結檔";
                }

                ltlUnVerifyCount.Text = Convert.ToString(ds.UnverifiedCount);
                ltlTotalVerifiedCount.Text = (ds.VerifiedCount + ds.ForceReturnCount + ds.ForceVerifyCount).ToString();
                ltlServiceReturnCount.Text = (ds.ReturnedCount + ds.ForceReturnCount + ds.ForceVerifyCount).ToString();
            }
        }

        #endregion page

        #region method

        public void SetFilterTypeDropDown(Dictionary<string, string> types)
        {
            ddlSearch.DataSource = types;
            ddlSearch.DataBind();
        }

        /// <summary>
        /// 店家核銷統計資料
        /// </summary>
        /// <param name="obj"></param>
        public void SetVerificationData(List<PponDealSalesInfo> dealSalesInfo)
        {
            if (dealSalesInfo.Count > 0)
            {
                lblCount.Visible = false;
                setSellerVerificationSearch.Visible = true;

                repSellerVerificationSearch.DataSource = dealSalesInfo;
                repSellerVerificationSearch.DataBind();
            }
            else
            {
                lblCount.Text = "查無資料";
                lblCount.Visible = true;
                setSellerVerificationSearch.Visible = false;
            }
        }

        #endregion method

        #region pirvate method

        private bool Validation(string column, string value)
        {
            lblErrorMsg.Text = string.Empty;
            if (column.Equals(ViewPponDeal.Columns.BusinessHourGuid))
            {
                Regex guidRegEx = new Regex(@"^(\{{0,1}([0-9a-fA-F]){8}-([0-9a-fA-F]){4}-([0-9a-fA-F]){4}-([0-9a-fA-F]){4}-([0-9a-fA-F]){12}\}{0,1})$");
                if (guidRegEx.IsMatch(value))
                {
                    return true;
                }
                else
                {
                    lblErrorMsg.Text = "bid輸入錯誤!!";
                    return false;
                }
            }
            else if (column.Equals(ViewPponDeal.Columns.UniqueId))
            {
                int text;
                if (int.TryParse(value, out text))
                {
                    return true;
                }
                else
                {
                    lblErrorMsg.Text = "檔號輸入錯誤!!";
                    return false;
                }
            }
            else
            {
                return true;
            }
        }

        #endregion pirvate method
    }
}
﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Views;
using System;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LunchKingSite.Web.ControlRoom.Verification
{
    public partial class WeeklyPayAccountSetUp : RolePage
    {
        #region Property

        public string UserName
        {
            get
            {
                return User.Identity.Name;
            }
        }

        public int PayToCompany
        {
            get { return (rbPayToCompany.Checked) ? 1 : 0; }
            set
            {
                if (value == 1)
                {
                    rbPayToCompany.Checked = true;
                    rbPayToStore.Checked = false;
                }
                else
                {
                    rbPayToCompany.Checked = false;
                    rbPayToStore.Checked = true;
                }
            }
        }

        public bool ShowStore { get; set; }

        public IAuditBoardView AuditBoardView
        {
            get
            {
                return ab;
            }
        }

        private IOrderProvider op;
        private IPponProvider _pp;
        private ISellerProvider _sp;
        private IHiDealProvider _hp;
        private ISysConfProvider _cp;
        private IAccountingProvider _ap;

        protected struct SearchType
        {
            public const int Ppon = 0;
            public const int Money = 1;
            public const int HiDeal = 2;
        }

        protected struct SearchColumn
        {
            public const int Bid = 0;
            public const int Pid = 1;
            public const int DealId = 2;
            public const int HiDealId = 3;
        }

        public RemittanceType PaidType
        {
            get
            {
                if (rbAchWeekly.Checked)
                {
                    return RemittanceType.AchWeekly;
                }
                if (rbAchMonthly.Checked)
                {
                    return RemittanceType.AchMonthly;
                }
                if (rbAchPartially.Checked)
                {
                    return RemittanceType.ManualPartially;
                }
                if (rbManualWeekly.Checked)
                {
                    return RemittanceType.ManualWeekly;
                }
                if (rbManualMonthly.Checked)
                {
                    return RemittanceType.ManualMonthly;
                }
                if (rbFlexible.Checked)
                {
                    return RemittanceType.Flexible;
                }
                if (rbMonthly.Checked)
                {
                    return RemittanceType.Monthly;
            }
                if (rbWeekly.Checked)
                {
                    return RemittanceType.Weekly;
                }
                if (rbFortnightly.Checked)
                {
                    return RemittanceType.Fortnightly;
                }
                return RemittanceType.Others;
            }
            set
            {
                rbAchMonthly.Checked = rbAchWeekly.Checked = rbAchPartially.Checked = rbManualWeekly.Checked = rbManualMonthly.Checked = rbAchOthers.Checked =
                rbFlexible.Checked = rbMonthly.Checked = rbWeekly.Checked = rbFortnightly.Checked = false;

                switch (value)
                {
                    case RemittanceType.AchWeekly:
                        rbAchWeekly.Checked = true;
                        break;

                    case RemittanceType.AchMonthly:
                        rbAchMonthly.Checked = true;
                        break;

                    case RemittanceType.ManualPartially:
                        rbAchPartially.Checked = true;
                        break;

                    case RemittanceType.ManualWeekly:
                        rbManualWeekly.Checked = true;
                        break;

                    case RemittanceType.ManualMonthly:
                        rbManualMonthly.Checked = true;
                        break;

                    case RemittanceType.Flexible:
                        rbFlexible.Checked = true;
                        break;

                    case RemittanceType.Monthly:
                        rbMonthly.Checked = true;
                        break;

                    case RemittanceType.Weekly:
                        rbWeekly.Checked = true;
                        break;

                    case RemittanceType.Fortnightly:
                        rbFortnightly.Checked = true;
                        break;

                    default:
                        rbAchOthers.Checked = true;
                        break;
                }
            }
        }

        public RemittanceType PaidTypeC
        {
            get
            {
                if (rbAchWeeklyC.Checked)
                {
                    return RemittanceType.AchWeekly;
                }
                if (rbAchMonthlyC.Checked)
                {
                    return RemittanceType.AchMonthly;
                }
                if (rbAchPartiallyC.Checked)
                {
                    return RemittanceType.ManualPartially;
                }
                if (rbManualWeeklyC.Checked)
                {
                    return RemittanceType.ManualWeekly;
                }
                if (rbManualMonthlyC.Checked)
                {
                    return RemittanceType.ManualMonthly;
                }
                if (rbFlexibleC.Checked)
                {
                    return RemittanceType.Flexible;
                }
                if (rbMonthlyC.Checked)
                {
                    return RemittanceType.Monthly;
                }
                if (rbWeeklyC.Checked)
                {
                    return RemittanceType.Weekly;
                }
                if (rbFortnightlyC.Checked)
                {
                    return RemittanceType.Fortnightly;
                }

                return RemittanceType.Others;
                }
            set
            {
                rbAchMonthlyC.Checked = rbAchWeeklyC.Checked = rbAchPartiallyC.Checked = rbManualWeeklyC.Checked = rbManualMonthlyC.Checked = rbAchOthersC.Checked =
                rbFlexibleC.Checked = rbMonthlyC.Checked = rbWeeklyC.Checked = rbFortnightlyC.Checked = false;

                switch (value)
                {
                    case RemittanceType.AchWeekly:
                        rbAchWeeklyC.Checked = true;
                        break;

                    case RemittanceType.AchMonthly:
                        rbAchMonthlyC.Checked = true;
                        break;

                    case RemittanceType.ManualPartially:
                        rbAchPartiallyC.Checked = true;
                        break;

                    case RemittanceType.ManualWeekly:
                        rbManualWeeklyC.Checked = true;
                        break;

                    case RemittanceType.ManualMonthly:
                        rbManualMonthlyC.Checked = true;
                        break;

                    case RemittanceType.Flexible:
                        rbFlexibleC.Checked = true;
                        break;

                    case RemittanceType.Monthly:
                        rbMonthlyC.Checked = true;
                        break;

                    case RemittanceType.Weekly:
                        rbWeeklyC.Checked = true;
                        break;

                    case RemittanceType.Fortnightly:
                        rbFortnightlyC.Checked = true;
                        break;

                    default:
                        rbAchOthersC.Checked = true;
                        break;
                }
            }
        }

        #endregion Property

        public WeeklyPayAccountSetUp()
        {
            op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
            _pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            _sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
            _hp = ProviderFactory.Instance().GetProvider<IHiDealProvider>();
            _cp = ProviderFactory.Instance().GetConfig();
            _ap = ProviderFactory.Instance().GetProvider<IAccountingProvider>();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                SetDdl(SearchType.Ppon);
                ShowStore = false;
            }
        }

        protected void SearchPidInfo(object sender, EventArgs e)
        {
            btn_AddAccount.Visible = false;
            lab_Message.Text = string.Empty;
            ClearText(tbx_AccountID, tbx_Sign_C_ID, tbx_AccountName, tbx_AccountNo, tbx_BankNo, tbx_BranchNo, tbx_CompanyName, tbx_Email, tbx_Message, tbx_SellerName);
            ClearText(lab_C_AccountID, lab_C_SignID, lab_C_AccountName, lab_C_AccountNo, lab_C_BankNo, lab_C_Bid, lab_C_BranchNo, lab_C_CompanyName,
                lab_C_Email, lab_C_EventName, lab_C_SellerName, lab_DealDeliveryE, lab_DealDeliveryS, lab_DealName, lab_DealOrderE, lab_DealOrderS, lab_Message, lab_C_OS, lbDeliveryType);

            rbAchWeekly.Checked = rbAchMonthly.Checked = rbManualWeekly.Checked = rbManualMonthly.Checked = rbAchPartially.Checked = rbAchOthers.Checked =
            rbFlexible.Checked = rbMonthly.Checked = rbWeekly.Checked = rbFortnightly.Checked = false;
            rbAchWeeklyC.Checked = rbAchMonthlyC.Checked = rbManualWeeklyC.Checked = rbManualMonthlyC.Checked = rbAchPartiallyC.Checked = rbAchOthersC.Checked =
            rbFlexibleC.Checked = rbMonthlyC.Checked = rbWeeklyC.Checked = rbFortnightlyC.Checked = false; 

            ViewHiDeal deal = new ViewHiDeal();
            HiDealProduct product = new HiDealProduct();
            switch (Convert.ToInt32(ddlSelect.SelectedValue))
            {
                case SearchColumn.Pid:
                    int pid;
                    if (int.TryParse(tbx_Pid.Text, out pid))
                    {
                        product = _hp.HiDealProductGet(pid);
                        if (product.Guid != Guid.Empty)
                        {
                            deal = _hp.ViewHiDealGetByDealId(product.DealId);
                        }
                        else
                        {
                            lab_Message.Text = "查無品生活檔次";
                        }
                    }
                    else
                    {
                        lab_Message.Text = "查無品生活檔次";
                    }
                    break;

                case SearchColumn.HiDealId:
                    int did;
                    if (int.TryParse(tbx_Pid.Text, out did))
                    {
                        deal = _hp.ViewHiDealGetByDealId(did);
                        if (deal.DealGuid != Guid.Empty)
                        {
                            HiDealProductCollection hdpc = _hp.HiDealProductCollectionGet(deal.DealId);
                            if (hdpc.Count > 0)
                            {
                                product = hdpc[0];
                            }
                            else
                            {
                                lab_Message.Text = "找不到產品資訊";
                            }
                        }
                        else
                        {
                            lab_Message.Text = "查無品生活檔次";
                        }
                    }
                    break;
            }

            if (deal.DealGuid != Guid.Empty && product.Guid != Guid.Empty)
            {
                lab_DealName.Text = string.Format("{0} - {1}", deal.DealName, product.Name);
                lab_DealOrderS.Text = deal.DealStartTime == null ? "" : deal.DealStartTime.Value.ToString("yyyy/MM/dd");
                lab_DealOrderE.Text = deal.DealEndTime == null ? "" : deal.DealEndTime.Value.ToString("yyyy/MM/dd");
                lab_DealDeliveryS.Text = product.UseStartTime == null ? "" : product.UseStartTime.Value.ToString("yyyy/MM/dd");
                lab_DealDeliveryE.Text = product.UseEndTime == null ? "" : product.UseEndTime.Value.ToString("yyyy/MM/dd");
                lbDeliveryType.Text = ((product.IsInStore) ? "到店; " : string.Empty) +
                                      ((product.IsHomeDelivery) ? "宅配; " : string.Empty);
                PayToCompany = product.PayToCompany ? 1 : 0;
                PaidType = (RemittanceType)product.RemittanceType;

                hlStoreAdd.NavigateUrl = string.Format("../seller/SellerStoreEdit.aspx?sid={0}&t=a", deal.SellerGuid);

                if (PayToCompany == 1)
                {
                    gvSeller.DataSource = _sp.SellerGetList(0, 0, null, Seller.Columns.Guid + " = " + deal.SellerGuid);
                    gvSeller.DataBind();
                    gvSeller.Visible = true;
                    gvStore.Visible = false;
                }
                else
                {
                    gvStore.DataSource = _sp.StoreGetListBySellerGuid(deal.SellerGuid);
                    gvStore.DataBind();
                    gvStore.Visible = true;
                    gvSeller.Visible = false;
                }
                ShowStore = true;

                Seller seller = _sp.SellerGet(deal.SellerGuid);
                btn_AddAccount.Visible = true;
                if (seller.IsLoaded)
                {
                    tbx_CompanyName.Text = seller.CompanyName;
                    tbx_SellerName.Text = seller.SellerName;
                    tbx_AccountID.Text = seller.CompanyID;
                    tbx_Sign_C_ID.Text = seller.SignCompanyID;
                    tbx_BankNo.Text = seller.CompanyBankCode;
                    tbx_BranchNo.Text = seller.CompanyBranchCode;
                    tbx_AccountNo.Text = seller.CompanyAccount;
                    tbx_AccountName.Text = seller.CompanyAccountName;
                    tbx_Email.Text = seller.CompanyEmail;
                    tbx_Message.Text = seller.CompanyNotice;
                    btn_AddAccount.Text = @"修改";
                    btn_AddAccount.Visible = CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.Update);
                }
                else
                {
                    btn_AddAccount.Text = @"新增";
                    btn_AddAccount.Visible = CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.Create);
                }
            }
        }

        protected void SearchBidInfo(object sender, EventArgs e)
        {
            btn_AddAccount.Visible = false;
            lab_Message.Text = string.Empty;
            ClearText(tbx_AccountID, tbx_Sign_C_ID, tbx_AccountName, tbx_AccountNo, tbx_BankNo, tbx_BranchNo, tbx_CompanyName, tbx_Email, tbx_Message, tbx_SellerName);
            ClearText(lab_C_AccountID, lab_C_SignID, lab_C_AccountName, lab_C_AccountNo, lab_C_BankNo, lab_C_Bid, lab_C_BranchNo, lab_C_CompanyName,
                lab_C_Email, lab_C_EventName, lab_C_SellerName, lab_DealDeliveryE, lab_DealDeliveryS, lab_DealName, lab_DealOrderE, lab_DealOrderS, lab_Message, lab_C_OS, lbDeliveryType);

            rbAchWeekly.Checked = rbAchMonthly.Checked = rbManualWeekly.Checked = rbManualMonthly.Checked = rbAchPartially.Checked = rbAchOthers.Checked =
            rbFlexible.Checked = rbMonthly.Checked = rbWeekly.Checked = rbFortnightly.Checked = false;
            rbAchWeeklyC.Checked = rbAchMonthlyC.Checked = rbManualWeeklyC.Checked = rbManualMonthlyC.Checked = rbAchPartiallyC.Checked = rbAchOthersC.Checked =
            rbFlexibleC.Checked = rbMonthlyC.Checked = rbWeeklyC.Checked = rbFortnightlyC.Checked= false;

            IViewPponDeal deal = new ViewPponDeal();
            DealAccounting accounting = new DealAccounting();

            switch (Convert.ToInt32(ddlSelect.SelectedValue))
            {
                case SearchColumn.Bid:
                    Guid bid;
                    if (Guid.TryParse(tbx_Bid.Text, out bid))
                    {
                        deal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(bid);
                        accounting = _pp.DealAccountingGet(bid);
                    }
                    else
                    {
                        lab_Message.Text = @"查無檔次";
                    }
                    break;

                case SearchColumn.DealId:
                    int uid;
                    if (int.TryParse(tbx_Bid.Text, out uid))
                    {
                        ViewPponDealCollection vpd = _pp.ViewPponDealGetListPaging(0, 0, string.Empty, ViewPponDeal.Columns.UniqueId + " = " + uid);
                        if (vpd.Count > 0)
                        {
                            deal = vpd[0];
                            accounting = _pp.DealAccountingGet(deal.BusinessHourGuid);
                        }
                    }
                    else
                    {
                        lab_Message.Text = @"查無檔次";
                    }
                    break;

                default:
                    lab_Message.Text = @"請選擇搜尋欄位";
                    break;
            }

            if (deal.BusinessHourGuid == Guid.Empty)
            {
                lab_Message.Text = @"無檔次";
            }
            else
            {
                ab.ReferenceId = deal.BusinessHourGuid.ToString();
                AuditBoardView.Presenter.OnViewInitialized();
                btn_AddAccount.Visible = true;
                lab_DealName.Text = deal.EventName;
                lab_DealOrderS.Text = deal.BusinessHourOrderTimeS.ToString("yyyy/MM/dd");
                lab_DealOrderE.Text = deal.BusinessHourOrderTimeE.ToString("yyyy/MM/dd");
                lab_DealDeliveryS.Text = deal.BusinessHourDeliverTimeS == null ? string.Empty : deal.BusinessHourDeliverTimeS.Value.ToString("yyyy/MM/dd");
                lab_DealDeliveryE.Text = deal.BusinessHourDeliverTimeE == null ? string.Empty : deal.BusinessHourDeliverTimeE.Value.ToString("yyyy/MM/dd");
                lbDeliveryType.Text = (deal.DeliveryType == (int)DeliveryType.ToShop) ? "到店" : "宅配";
                PayToCompany = _pp.DealAccountingGet(deal.BusinessHourGuid).Paytocompany;
                PaidType = (RemittanceType)accounting.RemittanceType;

                hlStoreAdd.NavigateUrl = string.Format("../seller/SellerStoreEdit.aspx?sid={0}&t=a", deal.SellerGuid);

                if (PayToCompany == 1)
                {
                    gvSeller.DataSource = op.WeeklyPayAccountGetList(deal.BusinessHourGuid).Where(x => x.StoreGuid == deal.SellerGuid);
                    gvSeller.DataBind();
                    gvSeller.Visible = true;
                    gvStore.Visible = false;
                }
                else
                {
                    gvStore.DataSource = (from sa in op.WeeklyPayAccountGetList(deal.BusinessHourGuid)
                                                    .Where(x => x.StoreGuid != deal.SellerGuid)
                                          join st in SellerFacade.GetPponStoreSellersByBId(deal.BusinessHourGuid, VbsRightFlag.Accouting) on sa.StoreGuid equals st.Guid
                                          select new
                                          {
                                              StoreName = st.SellerName,
                                              st.CompanyEmail,
                                              st.CompanyNotice,
                                              CompanyBossName = sa.SellerName,
                                              CompanyID = sa.AccountId,
                                              CompanyBankCode = sa.BankNo,
                                              CompanyBranchCode = sa.BranchNo,
                                              CompanyAccount = sa.AccountNo,
                                              CompanyAccountName = sa.AccountName,
                                              sa.CompanyName,
                                              sa.SignCompanyID
                                          });
                    gvStore.DataBind();
                    gvStore.Visible = true;
                    gvSeller.Visible = false;
                }
                ShowStore = true;

                WeeklyPayAccount accountinfo = op.GetWeeklyPayAccount(deal.BusinessHourGuid, deal.SellerGuid);

                if (accountinfo.BusinessHourGuid != Guid.Empty)
                {
                    tbx_CompanyName.Text = accountinfo.CompanyName;
                    tbx_SellerName.Text = accountinfo.SellerName;
                    tbx_AccountID.Text = accountinfo.AccountId;
                    tbx_Sign_C_ID.Text = accountinfo.SignCompanyID;
                    tbx_BankNo.Text = accountinfo.BankNo;
                    tbx_BranchNo.Text = accountinfo.BranchNo;
                    tbx_AccountNo.Text = accountinfo.AccountNo;
                    tbx_AccountName.Text = accountinfo.AccountName;
                    tbx_Email.Text = accountinfo.Email;
                    tbx_Message.Text = accountinfo.Message;
                    btn_AddAccount.Text = @"修改";
                    btn_AddAccount.Visible = CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.Update);
                }
                else
                {
                    btn_AddAccount.Text = @"新增";
                    btn_AddAccount.Visible = CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.Create);
                }

                #region 開檔後無法修改出帳方式

                if (DateTime.Now > deal.BusinessHourOrderTimeS && 
                    !CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.UpdateRemittanceType))
                {
                    rbAchWeekly.Enabled = false;
                    rbAchMonthly.Enabled = false;
                    rbManualWeekly.Enabled = false;
                    rbManualMonthly.Enabled = false;
                    rbAchPartially.Enabled = false;
                    rbAchOthers.Enabled = false;
                    rbFlexible.Enabled = false;
                    rbMonthly.Enabled = false;
                    rbWeekly.Enabled = false;
                    rbFortnightly.Enabled = false;
                    rbAchWeeklyC.Enabled = false;
                    rbAchMonthlyC.Enabled = false;
                    rbManualWeeklyC.Enabled = false;
                    rbManualMonthlyC.Enabled = false;
                    rbAchPartiallyC.Enabled = false;
                    rbAchOthersC.Enabled = false;
                    rbFlexibleC.Enabled = false;
                    rbMonthlyC.Enabled = false;
                    rbWeeklyC.Enabled = false;
                    rbFortnightlyC.Enabled = false;
                }

                #endregion 開檔後無法修改出帳方式
            }
        }

        protected void ModifyAccount(object sender, CommandEventArgs e)
        {
            pan_C_Bid.Visible = pan_C_Money.Visible = false;
            Regex r_comid = new Regex(@"\d{8}");
            Regex r_bank = new Regex(@"\d{3}");
            Regex r_branch = new Regex(@"\d{4}");
            string auditMsg = string.Empty;

            if (tbx_AccountID.Text.Trim().Length == 8 && !r_comid.IsMatch(tbx_AccountID.Text.Trim()) && (PayToCompany == 1))
            {
                pan_Search.Visible = true;
                pan_Confirm.Visible = false;
                lab_Message.Text = @"統編為格式錯誤";
            }
            else if (tbx_AccountID.Text.Trim().Length != 10 && tbx_AccountID.Text.Trim().Length != 8 && (PayToCompany == 1))
            {
                pan_Search.Visible = true;
                pan_Confirm.Visible = false;
                lab_Message.Text = @"受款人ID/統編格式錯誤";
            }
            else if (!(tbx_BankNo.Text.Trim().Length == 3 && r_bank.IsMatch(tbx_BankNo.Text.Trim())) && (PayToCompany == 1))
            {
                pan_Search.Visible = true;
                pan_Confirm.Visible = false;
                lab_Message.Text = @"銀行代碼格式錯誤";
            }
            else if (!(tbx_BranchNo.Text.Trim().Length == 4 && r_branch.IsMatch(tbx_BranchNo.Text.Trim())) && (PayToCompany == 1))
            {
                pan_Search.Visible = true;
                pan_Confirm.Visible = false;
                lab_Message.Text = @"分行代碼格式錯誤";
            }
            else
            {
                if (rbl_Mode.SelectedValue == "0")
                {
                    lab_Message.Text = string.Empty;
                    Guid bid = new Guid();
                    int uid;
                    bool hasBid = false;
                    if (Convert.ToInt32(ddlSelect.SelectedValue) == SearchColumn.DealId)
                    {
                        if (int.TryParse(tbx_Bid.Text, out uid))
                        {
                            ViewPponDealCollection vpd = _pp.ViewPponDealGetListPaging(0, 0, string.Empty, ViewPponDeal.Columns.UniqueId + " = " + uid);
                            if (vpd.Count > 0)
                            {
                                bid = vpd.First().BusinessHourGuid;
                                hasBid = true;
                            }
                        }
                    }
                    else
                    {
                        if (Guid.TryParse(tbx_Bid.Text, out bid))
                        {
                            hasBid = true;
                        }
                    }

                    if (hasBid)
                    {
                        IViewPponDeal deal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(bid);
                        WeeklyPayAccount account = op.GetWeeklyPayAccount(bid, deal.SellerGuid);
                        DealAccounting dAccount = _pp.DealAccountingGet(bid);
                        Seller s = _sp.SellerGet(deal.SellerGuid);
                        if (deal.BusinessHourGuid != Guid.Empty)
                        {
                            if (e.CommandName == "ToConfirm")
                            {
                                ClearText(lab_C_AccountID, lab_C_SignID, lab_C_AccountName, lab_C_AccountNo, lab_C_BankNo, lab_C_Bid, lab_C_BranchNo, lab_C_CompanyName,
                                    lab_C_Email, lab_C_EventName, lab_C_SellerName, lab_DealDeliveryE, lab_DealDeliveryS, lab_DealName, lab_DealOrderE, lab_DealOrderS, lab_Message, lab_C_Message, lab_C_OS);
                                PaidTypeC = PaidType;
                                pan_Search.Visible = false;
                                pan_Confirm.Visible = true;
                                pAudit.Visible = true;
                                pan_C_Bid.Visible = true;
                                btn_Confirm.Text = string.IsNullOrEmpty(s.CompanyAccount) ? "確認新增" : "確認更新";
                                lab_IdTitle.Text = "Bid";
                                lab_C_Bid.Text = bid.ToString();
                                lab_C_EventName.Text = deal.EventName;
                                lab_C_CompanyName.Text = tbx_CompanyName.Text;
                                lab_C_SellerName.Text = tbx_SellerName.Text;
                                lab_C_AccountID.Text = tbx_AccountID.Text;
                                lab_C_SignID.Text = tbx_Sign_C_ID.Text;
                                lab_C_AccountName.Text = tbx_AccountName.Text;
                                lab_C_AccountNo.Text = tbx_AccountNo.Text.Replace("-", string.Empty).Replace(" ", string.Empty);
                                lab_C_Email.Text = tbx_Email.Text;
                                lab_C_BankNo.Text = tbx_BankNo.Text;
                                lab_C_BranchNo.Text = tbx_BranchNo.Text;
                                lab_C_Message.Text = tbx_Message.Text;
                                lab_C_PayTo.Text = (PayToCompany == 1) ? "統一匯至賣家匯款資料" : "匯至各分店";
                            }
                            else if (e.CommandName == "ToModify")
                            {
                                bool IsExistPartiallyPaymentDate;
                                bool IsExistBalanceSheet;
                                pAudit.Visible = false;
                                IsExistPartiallyPaymentDate = (dAccount.PartiallyPaymentDate != null) ? true : false;
                                BalanceSheetCollection bsc = _ap.BalanceSheetGetListByProductGuid(bid); //是否開立對帳單
                                IsExistBalanceSheet = (bsc.ToList().Count > 0) ? true : false;

                                Guid? main_bid = _pp.GetComboDeal(bid).MainBusinessHourGuid;
                                DealAccounting mainDeal = (main_bid !=null) ? _pp.DealAccountingGet(main_bid.Value) : null;
                                ComboDealCollection vcdc = new ComboDealCollection();
                                if (main_bid != null && main_bid == bid)
                                {
                                    vcdc = _pp.GetComboDealByBid(bid, false);
                                    foreach (ComboDeal cd in vcdc)
                                    {
                                        if (_pp.DealAccountingGet(cd.BusinessHourGuid).PartiallyPaymentDate != null)
                                        {
                                            IsExistPartiallyPaymentDate = true;
                                            break;
                                        }
                                        if (_ap.BalanceSheetGetListByProductGuid(cd.BusinessHourGuid).ToList().Count > 0)
                                        {
                                            IsExistBalanceSheet = true;
                                            break;
                                        }
                                    }
                                }

                                if (main_bid != null && (dAccount.BusinessHourGuid != main_bid.Value) && ((int)PaidTypeC != mainDeal.RemittanceType))
                                {
                                    pan_Search.Visible = true;
                                    pan_Confirm.Visible = false;
                                    lab_Message.Text = @"不可與母檔不同出帳日";
                                }
                                else if (IsExistPartiallyPaymentDate)
                                {
                                    pan_Search.Visible = true;
                                    pan_Confirm.Visible = false;
                                    lab_Message.Text = @"已暫付七成";
                                }
                                else if (IsExistBalanceSheet)
                                {
                                    pan_Search.Visible = true;
                                    pan_Confirm.Visible = false;
                                    lab_Message.Text = @"已開立對帳單";
                                }
                                else
                                {
                                    if (account.BusinessHourGuid == Guid.Empty)
                                    {
                                        account = new WeeklyPayAccount();
                                        account.BusinessHourGuid = bid;
                                        account.Message = tbx_Message.Text;
                                        account.CreateDate = DateTime.Now;
                                        account.Creator = UserName;
                                        lab_Message.Text = @"新增成功";
                                        auditMsg = "建立出帳日:{0};建立匯款資料:{1}";
                                    }
                                    else
                                    {
                                        account.Message = tbx_Message.Text + "(" + DateTime.Now.ToString("MM/dd") + UserName + "修改)";
                                        lab_Message.Text = @"更新成功";
                                        auditMsg = "變更出帳日:{0};變更匯款資料:{1}";
                                    }
                                    lab_C_Bid.Text = bid.ToString();
                                    lab_C_EventName.Text = account.EventName = deal.EventName.TrimToMaxLength(100, "...");

                                    if (PayToCompany == 1)
                                    {
                                        account.CompanyName = tbx_CompanyName.Text;
                                        account.SellerName = tbx_SellerName.Text;
                                        account.AccountId = tbx_AccountID.Text;
                                        account.SignCompanyID = tbx_Sign_C_ID.Text;
                                        account.AccountName = tbx_AccountName.Text;
                                        account.AccountNo = tbx_AccountNo.Text;
                                        account.Email = tbx_Email.Text;
                                        account.BankNo = tbx_BankNo.Text;
                                        account.BranchNo = tbx_BranchNo.Text;
                                        account.Message = tbx_Message.Text;

                                        //update seller info
                                        s.CompanyAccount = tbx_AccountNo.Text;
                                        s.CompanyAccountName = tbx_AccountName.Text;
                                        s.CompanyBankCode = tbx_BankNo.Text;
                                        s.CompanyBossName = tbx_SellerName.Text;
                                        s.CompanyBranchCode = tbx_BranchNo.Text;
                                        s.CompanyEmail = tbx_Email.Text;
                                        s.CompanyID = tbx_AccountID.Text;
                                        s.SignCompanyID = tbx_Sign_C_ID.Text;
                                        s.CompanyName = tbx_CompanyName.Text;
                                        s.CompanyNotice = tbx_Message.Text;
                                        _sp.SellerSet(s);
                                    }
                                    
                                    op.SetWeeklyPayAccount(account);

                                    DealAccountingCollection dAccountC = new DealAccountingCollection();
                                    if (vcdc.Count > 0)
                                    {
                                        foreach (ComboDeal cd in vcdc)
                                        {
                                            DealAccounting da = _pp.DealAccountingGet(cd.BusinessHourGuid);
                                            dAccountC.Add(da);
                                        }
                                    }
                                    dAccountC.Add(dAccount);

                                    foreach (DealAccounting da in dAccountC)
                                    {
                                        da.Paytocompany = PayToCompany;
                                        da.RemittanceType = (int)PaidTypeC;
                                        _pp.DealAccountingSet(da);
                                        op.UpdateBusinessHourStatusForWeeklyPay((PaidTypeC == RemittanceType.AchWeekly), da.BusinessHourGuid);
                                        auditMsg = string.Format(auditMsg, PaidTypeC, ((PayToCompany == 1) ? "統一匯至賣家匯款資料" : "匯至各分店"));
                                        CommonFacade.AddAudit(da.BusinessHourGuid.ToString(), AuditType.DealAccounting, auditMsg, User.Identity.Name, true);
                                    }

                                    //新宅配提案單也一併調整
                                    Proposal pro = _sp.ProposalGet(bid);
                                    if (pro.ProposalSourceType == (int)ProposalSourceType.House)
                                    {
                                        pro.RemittanceType = (int)PaidTypeC;
                                        _sp.ProposalSet(pro);

                                        ProposalFacade.ProposalLog(pro.Id, "變更 出帳方式 為 " + Helper.GetLocalizedEnum(I18N.Phrase.ResourceManager, (RemittanceType)PaidTypeC), User.Identity.Name);

                                    }

                                    AuditBoardView.SetRecordGrid(null);

                                    pan_Search.Visible = true;
                                    pan_Confirm.Visible = btn_AddAccount.Visible = false;
                                    ClearText(tbx_AccountID, tbx_Sign_C_ID, tbx_AccountName, tbx_AccountNo, tbx_BankNo, tbx_BranchNo, tbx_CompanyName, tbx_Email, tbx_Message, tbx_SellerName, tbx_Bid, tbx_Pid);
                                    ClearText(lab_C_AccountID, lab_C_SignID, lab_C_AccountName, lab_C_AccountNo, lab_C_BankNo, lab_C_Bid, lab_C_BranchNo, lab_C_CompanyName,
                                        lab_C_Email, lab_C_EventName, lab_C_SellerName, lab_DealDeliveryE, lab_DealDeliveryS, lab_DealName, lab_DealOrderE, lab_DealOrderS, lab_C_Message, lab_C_OS);
                                }
                            } 
                        }
                        else
                        {
                            pan_Search.Visible = true;
                            pan_Confirm.Visible = false;
                            lab_Message.Text = @"查無檔次";
                        }
                    }
                    else
                    {
                        pan_Search.Visible = true;
                        pan_Confirm.Visible = false;
                        lab_Message.Text = @"查無檔次";
                    }
                }
                else if (rbl_Mode.SelectedValue == "1")
                {
                    DateTime now = DateTime.Now;
                    DateTime target = new DateTime();
                    if (DateTime.TryParse(tbOS.Text, out target))
                    {
                        target = target.AddHours(17);
                        if (target.DayOfWeek == DayOfWeek.Sunday || target.DayOfWeek == DayOfWeek.Saturday)
                        {
                            lab_Message.Text = @"退款日不得為假日";
                        }
                        else if ((target - now).Days < 2)
                        {
                            lab_Message.Text = @"退款日需比發動日大兩天，" + (target - now).Days + @"天";
                        }
                        else if (target.DayOfWeek == DayOfWeek.Monday && (target - now).Days < 4)
                        {
                            lab_Message.Text = @"處理日不得為假日";
                        }
                        else
                        {
                            int money;
                            if (int.TryParse(tbx_Money.Text, out money))
                            {
                                if (e.CommandName == "ToConfirm")
                                {
                                    ClearText(lab_C_AccountID, lab_C_SignID, lab_C_AccountName, lab_C_AccountNo, lab_C_BankNo, lab_C_Bid, lab_C_BranchNo, lab_C_CompanyName,
                                                lab_C_Email, lab_C_EventName, lab_C_SellerName, lab_DealDeliveryE, lab_DealDeliveryS, lab_DealName, lab_DealOrderE, lab_DealOrderS, lab_Message, lab_C_Message, lab_C_OS);
                                    pan_Search.Visible = false;
                                    pan_Confirm.Visible = true;
                                    pAudit.Visible = true;
                                    pan_C_Money.Visible = true;
                                    btn_Confirm.Text = "確認";
                                    lab_C_Money.Text = money.ToString();
                                    lab_C_OS.Text = target.ToString("yyyy/MM/dd");
                                    lab_C_CompanyName.Text = tbx_CompanyName.Text.Trim();
                                    lab_C_SellerName.Text = tbx_SellerName.Text.Trim();
                                    lab_C_AccountID.Text = tbx_AccountID.Text.Trim();
                                    lab_C_SignID.Text = tbx_Sign_C_ID.Text.Trim();
                                    lab_C_AccountName.Text = tbx_AccountName.Text.Trim();
                                    lab_C_AccountNo.Text = tbx_AccountNo.Text.Trim();
                                    lab_C_Email.Text = tbx_Email.Text.Trim();
                                    lab_C_BankNo.Text = tbx_BankNo.Text.Trim();
                                    lab_C_BranchNo.Text = tbx_BranchNo.Text.Trim();
                                }
                                else if (e.CommandName == "ToModify")
                                {
                                    DateTime t = new DateTime();
                                    if (target.DayOfWeek == DayOfWeek.Monday && (target - now).Days > 3)
                                    {
                                        t = target.AddDays(-3);
                                    }
                                    else
                                    {
                                        t = target.AddDays(-1);
                                    }

                                    string result = AchP1Head(t);
                                    int P1Count = 0;
                                    int P1Total = money;
                                    result += "NSC405" + (P1Count + 1).ToString().PadLeft(6, '0') + string.Format("{0}{1}", _cp.TriggerBankAndBranchNo, _cp.TriggerAccountNo) +
                                                      (lab_C_BankNo.Text + lab_C_BranchNo.Text).PadLeft(7, '0') + lab_C_AccountNo.Text.PadLeft(14, '0') +
                                                      (money).ToString().PadLeft(10, '0') + string.Format("00B{0}  ", _cp.TriggerCompanyId) +
                                                     lab_C_AccountID.Text.ToUpper().PadRight(10, ' ') + "      00000000000000 " +
                                                      User.Identity.Name.Replace("@", string.Empty).Replace(".", string.Empty).PadRight(20, ' ') +
                                                      string.Empty.PadRight(32, ' ') + "\r\n";
                                    result += AchP1Foot(t, 1, P1Total);
                                    Context.Response.Clear();
                                    Context.Response.ContentEncoding = Encoding.GetEncoding(0);
                                    Context.Response.ContentType = "text/octet-stream";
                                    Context.Response.AddHeader("Content-Disposition", "attachment; filename=ACHP01_" + string.Format("{0}_{1}_01.txt", now.ToString("yyyyMMdd"), _cp.TriggerCompanyId));
                                    Response.BinaryWrite(Encoding.UTF8.GetBytes(result));
                                    Response.End();
                                }
                            }
                            else
                            {
                                pan_Search.Visible = true;
                                pan_Confirm.Visible = false;
                                lab_Message.Text = @"請輸入數字金額";
                            }
                        }
                    }
                    else
                    {
                        lab_Message.Text = @"匯款時間格式錯誤";
                    }
                }
                else if (rbl_Mode.SelectedValue == "2")
                {
                    if (e.CommandName == "ToConfirm")
                    {
                        string dealName = lab_DealName.Text;
                        ClearText(lab_C_AccountID, lab_C_SignID, lab_C_AccountName, lab_C_AccountNo, lab_C_BankNo, lab_C_Bid, lab_C_BranchNo, lab_C_CompanyName,
                            lab_C_Email, lab_C_EventName, lab_C_SellerName, lab_DealDeliveryE, lab_DealDeliveryS, lab_DealName, lab_DealOrderE, lab_DealOrderS, lab_Message, lab_C_Message, lab_C_OS);
                        PaidTypeC = PaidType;
                        pan_Search.Visible = false;
                        pan_Confirm.Visible = true;
                        pAudit.Visible = true;
                        pan_C_Bid.Visible = true;
                        btn_Confirm.Text = "確認更新";
                        lab_IdTitle.Text = "Pid";
                        lab_C_Bid.Text = tbx_Pid.Text;
                        lab_C_EventName.Text = dealName;
                        lab_C_CompanyName.Text = tbx_CompanyName.Text;
                        lab_C_SellerName.Text = tbx_SellerName.Text;
                        lab_C_AccountID.Text = tbx_AccountID.Text;
                        lab_C_SignID.Text = tbx_Sign_C_ID.Text;
                        lab_C_AccountName.Text = tbx_AccountName.Text;
                        lab_C_AccountNo.Text = tbx_AccountNo.Text.Replace("-", string.Empty).Replace(" ", string.Empty);
                        lab_C_Email.Text = tbx_Email.Text;
                        lab_C_BankNo.Text = tbx_BankNo.Text;
                        lab_C_BranchNo.Text = tbx_BranchNo.Text;
                        lab_C_Message.Text = tbx_Message.Text;
                        lab_C_PayTo.Text = (PayToCompany == 1) ? "統一匯至賣家匯款資料" : "匯至各分店";
                    }
                    else if (e.CommandName == "ToModify")
                    {
                        //更新 PayToCompany (1.付給賣家還是0.付給分店)
                        // IsWeeklyPay 是否週付
                        int pid = int.Parse(tbx_Pid.Text);
                        HiDealProduct product = _hp.HiDealProductGet(pid);
                        product.RemittanceType = (int)PaidType;
                        product.PayToCompany = PayToCompany == 1;
                        _hp.HiDealProductSet(product);
                        auditMsg = "變更出帳日:" + PaidType + ";";
                        auditMsg += "變更匯款資料:" + ((PayToCompany == 1) ? "統一匯至賣家匯款資料" : "匯至各分店");
                        CommonFacade.AddAudit(pid.ToString(), AuditType.DealAccounting, auditMsg, User.Identity.Name, true);

                        pan_Search.Visible = true;
                        pan_Confirm.Visible = btn_AddAccount.Visible = false;
                        ClearText(tbx_AccountID, tbx_Sign_C_ID, tbx_AccountName, tbx_AccountNo, tbx_BankNo, tbx_BranchNo, tbx_CompanyName, tbx_Email, tbx_Message, tbx_SellerName, tbx_Bid, tbx_Pid);
                        ClearText(lab_C_AccountID, lab_C_SignID, lab_C_AccountName, lab_C_AccountNo, lab_C_BankNo, lab_C_Bid, lab_C_BranchNo, lab_C_CompanyName,
                            lab_C_Email, lab_C_EventName, lab_C_SellerName, lab_DealDeliveryE, lab_DealDeliveryS, lab_DealName, lab_DealOrderE, lab_DealOrderS, lab_C_Message, lab_C_OS);
                    }
                }
            }
        }

        protected void CancelAction(object sender, EventArgs e)
        {
            ClearText(tbx_AccountID, tbx_Sign_C_ID, tbx_AccountName, tbx_AccountNo, tbx_BankNo, tbx_BranchNo, tbx_CompanyName, tbx_Email, tbx_Message, tbx_SellerName, tbx_Bid, tbx_Pid, tbx_Money);
            ClearText(lab_C_AccountID, lab_C_SignID, lab_C_AccountName, lab_C_AccountNo, lab_C_BankNo, lab_C_Bid, lab_C_BranchNo,
                lab_C_CompanyName, lab_C_Email, lab_C_EventName, lab_C_SellerName, lab_DealDeliveryE, lab_DealDeliveryS, lab_DealName, lab_DealOrderE, lab_DealOrderS, lab_Message, lab_C_Message, lab_C_OS);
            pan_Search.Visible = true;
            pan_Confirm.Visible = false;
            pAudit.Visible = false;
            ab.ReferenceId = string.Empty;
            AuditBoardView.SetRecordGrid(null);
            AuditBoardView.Presenter.OnViewInitialized();
        }

        protected void ClearText(params object[] items)
        {
            foreach (var item in items)
            {
                if (item is TextBox)
                {
                    ((TextBox)item).Text = string.Empty;
                }
                else if (item is Label)
                {
                    ((Label)item).Text = string.Empty;
                }
            }
        }

        protected void ChangeMode(object sender, EventArgs e)
        {
            ClearText(tbx_AccountID, tbx_Sign_C_ID, tbx_AccountName, tbx_AccountNo, tbx_BankNo, tbx_BranchNo, tbx_CompanyName, tbx_Email, tbx_Message, tbx_SellerName, tbx_Bid, tbx_Pid, tbx_Money);
            ClearText(lab_C_AccountID, lab_C_SignID, lab_C_AccountName, lab_C_AccountNo, lab_C_BankNo, lab_C_Bid, lab_C_BranchNo,
                lab_C_CompanyName, lab_C_Email, lab_C_EventName, lab_C_SellerName, lab_DealDeliveryE, lab_DealDeliveryS, lab_DealName, lab_DealOrderE, lab_DealOrderS, lab_Message, lab_C_Message, lab_C_OS);
            rbPayToStore.Checked = rbPayToCompany.Checked = false;
            if (rbl_Mode.SelectedValue == Convert.ToString(SearchType.Ppon))
            {
                SetDdl(SearchType.Ppon);
                pan_Money.Visible = pan_Bid.Visible = pan_Pid.Visible = false;
                btn_AddAccount.Visible = false;
                pan_Bid.Visible = true;

                tbx_CompanyName.Enabled = false;
                tbx_SellerName.Enabled = false;
                tbx_AccountID.Enabled = false;
                tbx_Sign_C_ID.Enabled = false;
                tbx_BankNo.Enabled = false;
                tbx_BranchNo.Enabled = false;
                tbx_AccountNo.Enabled = false;
                tbx_AccountName.Enabled = false;
                tbx_Email.Enabled = false;
                tbx_Message.Enabled = false;
            }
            else if (rbl_Mode.SelectedValue == Convert.ToString(SearchType.Money))
            {
                lab_ModeName.Visible = true;
                lab_ModeName.Text = @"匯款金額<br/>匯款日";
                ddlSelect.Visible = false;
                pan_Money.Visible = pan_Bid.Visible = pan_Pid.Visible = false;
                btn_AddAccount.Visible = pan_Money.Visible = true;

                tbx_CompanyName.Enabled = true;
                tbx_SellerName.Enabled = true;
                tbx_AccountID.Enabled = true;
                tbx_Sign_C_ID.Enabled = true;
                tbx_BankNo.Enabled = true;
                tbx_BranchNo.Enabled = true;
                tbx_AccountNo.Enabled = true;
                tbx_AccountName.Enabled = true;
                tbx_Email.Enabled = true;
                tbx_Message.Enabled = true;
                btn_Confirm.Text = "新增";
            }
            else if (rbl_Mode.SelectedValue == Convert.ToString(SearchType.HiDeal))
            {
                SetDdl(SearchType.HiDeal);
                pan_Money.Visible = pan_Bid.Visible = pan_Pid.Visible = false;
                btn_AddAccount.Visible = false;
                pan_Pid.Visible = true;

                tbx_CompanyName.Enabled = false;
                tbx_SellerName.Enabled = false;
                tbx_AccountID.Enabled = false;
                tbx_Sign_C_ID.Enabled = false;
                tbx_BankNo.Enabled = false;
                tbx_BranchNo.Enabled = false;
                tbx_AccountNo.Enabled = false;
                tbx_AccountName.Enabled = false;
                tbx_Email.Enabled = false;
                tbx_Message.Enabled = false;
            }
        }

        private string AchP1Head(DateTime time)
        {
            return "BOFACHP01" + (DateTime.Now.Year - 1911).ToString().PadLeft(4, '0')  //13
                   + time.Month.ToString().PadLeft(2, '0')                              //2
                   + time.Day.ToString().PadLeft(2, '0')                                //2
                   + time.Hour.ToString().PadLeft(2, '0')                               //2
                   + time.Minute.ToString().PadLeft(2, '0')                             //2
                   + time.Second.ToString().PadLeft(2, '0')                             //2
                   + string.Format("{0}{1}", _cp.SendUnitNo, _cp.ReceiveUnitNo)         //14
                   + new string(' ', 123)
                   + "\r\n";
        }

        private string AchP1Foot(DateTime time, int count, int total)
        {
            return "EOFACHP01" + (DateTime.Now.Year - 1911).ToString().PadLeft(4, '0')  //13
                   + time.Month.ToString().PadLeft(2, '0')                              //2
                   + time.Day.ToString().PadLeft(2, '0')                                //2
                   + string.Format("{0}{1}", _cp.SendUnitNo, _cp.ReceiveUnitNo)         //14
                   + count.ToString().PadLeft(8, '0')                                   //8
                   + total.ToString().PadLeft(16, '0')                                  //16
                   + new string(' ', 105)
                   + "\r\n";
        }

        //設定搜尋下拉式選單
        private void SetDdl(int searchType)
        {
            ddlSelect.Visible = true;
            lab_ModeName.Visible = false;
            ddlSelect.Items.Clear();
            switch (searchType)
            {
                case SearchType.Ppon:
                    ddlSelect.Items.Add(new ListItem("檔次Bid", Convert.ToString(SearchColumn.Bid)));
                    ddlSelect.Items.Add(new ListItem("檔號", Convert.ToString(SearchColumn.DealId)));
                    break;

                case SearchType.HiDeal:
                    ddlSelect.Items.Add(new ListItem(Resources.Localization.ProductId, Convert.ToString(SearchColumn.Pid)));
                    break;
            }
        }
    }
}
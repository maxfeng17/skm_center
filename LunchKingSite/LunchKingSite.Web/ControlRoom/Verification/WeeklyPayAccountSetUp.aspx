﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WeeklyPayAccountSetUp.aspx.cs"
    Inherits="LunchKingSite.Web.ControlRoom.Verification.WeeklyPayAccountSetUp" MasterPageFile="~/ControlRoom/backend.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="../Controls/AuditBoard.ascx" TagName="AuditBoard" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <style type="text/css">
        .head
        {
            background-color: #688E45;
            font-weight: bolder;
            color: White;
        }
        .head2
        {
            background-color: #F0AF13;
            color: #688E45;
            font-weight: bolder;
            width: 100px;
        }
        .row
        {
            background-color: #B3D5F0;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Panel ID="pan_Search" runat="server">
        <table style="width: 950px">
            <tr>
                <td class="head" colspan="2" style="text-align: center">
                    每週核銷銀行匯款帳號設定
                </td>
            </tr>
            <tr>
                <td class="head2">
                    設定選項
                </td>
                <td class="row">
                    <asp:RadioButtonList ID="rbl_Mode" runat="server" OnSelectedIndexChanged="ChangeMode"
                        AutoPostBack="true" RepeatDirection="Horizontal">
                        <asp:ListItem Text="每週檔次核銷" Value="0" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="品生活檔次核銷" Value="2"></asp:ListItem>
                        <asp:ListItem Text="輸入金額" Value="1"></asp:ListItem>                        
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td class="head2">
                    <asp:Label runat="server" ID="lab_ModeName"></asp:Label>
                    <asp:DropDownList ID="ddlSelect" runat="server">
                    </asp:DropDownList>
                </td>
                <td class="row">
                    <asp:Panel ID="pan_Bid" runat="server">
                        <asp:TextBox ID="tbx_Bid" runat="server" Width="300"></asp:TextBox>
                        <asp:Button ID="btn_SearchBid" runat="server" Text="搜尋" OnClick="SearchBidInfo" />
                    </asp:Panel>
                    <asp:Panel ID="pan_Money" runat="server" Visible="false">
                        <asp:TextBox ID="tbx_Money" runat="server"></asp:TextBox><br />
                        <asp:TextBox ID="tbOS" runat="server"></asp:TextBox>&nbsp;17:00:00
                        <cc1:CalendarExtender ID="ce_Start" TargetControlID="tbOS" runat="server">
                        </cc1:CalendarExtender>
                        <span style="font-size: small; color: Green">需比今日晚兩天且不為假日。</span>
                    </asp:Panel>
                    <asp:Panel ID="pan_Pid" runat="server" Visible="false">
                        <asp:TextBox ID="tbx_Pid" runat="server" Width="300"></asp:TextBox>
                        <asp:Button ID="btn_SearchPid" runat="server" Text="搜尋" OnClick="SearchPidInfo" />
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td class="head2">
                    檔次資訊
                </td>
                <td>
                    <table style="width: 100%">
                        <tr>
                            <td class="head2">
                                檔次名稱
                            </td>
                            <td class="row">
                                <asp:Label ID="lab_DealName" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="head2">
                                訂購起訖
                            </td>
                            <td class="row">
                                <asp:Label ID="lab_DealOrderS" runat="server"></asp:Label>~
                                <asp:Label ID="lab_DealOrderE" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="head2">
                                使用起訖
                            </td>
                            <td class="row">
                                <asp:Label ID="lab_DealDeliveryS" runat="server"></asp:Label>~
                                <asp:Label ID="lab_DealDeliveryE" runat="server"></asp:Label>
                            </td>
                        </tr>                
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2" class="head2">店家資訊</td>
            </tr>
            <tr>
                <td class="head2">
                    消費方式
                </td>
                <td class="row">
                    <asp:Label ID="lbDeliveryType" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="head2">
                    匯款資料
                </td>
                <td class="row">
                    <asp:RadioButton ID="rbPayToCompany" GroupName="payto" runat="server" Enabled="false" />統一匯至賣家匯款資料
                    <asp:RadioButton ID="rbPayToStore" GroupName="payto" runat="server" Enabled="false"/>匯至各分店匯款資料
                </td>
            </tr>
            <tr>
                <td class="head2" style="width: 150px">
                    出帳日
                </td>
                <td class="row">
                    <asp:RadioButton ID="rbAchWeekly" GroupName="achType" runat="server" />ACH每週出帳
                    <asp:RadioButton ID="rbAchMonthly" GroupName="achType" runat="server" />ACH每月出帳
                    <asp:RadioButton ID="rbManualWeekly" GroupName="achType" runat="server" />人工每週出帳
                    <asp:RadioButton ID="rbManualMonthly" GroupName="achType" runat="server" />人工每月出帳
                    <asp:RadioButton ID="rbAchPartially" GroupName="achType" runat="server" />商品(暫付70%)
                    <asp:RadioButton ID="rbAchOthers" GroupName="achType" runat="server" />其他付款方式<br/>
                    <asp:RadioButton ID="rbWeekly" GroupName="achType" runat="server" />新每週出帳
                    <asp:RadioButton ID="rbFortnightly" GroupName="achType" runat="server" />雙周結出帳
                    <asp:RadioButton ID="rbMonthly" GroupName="achType" runat="server" />新每月出帳
                    <asp:RadioButton ID="rbFlexible" GroupName="achType" runat="server" />彈性選擇出帳
                </td>
            </tr>
            <% if (ShowStore) { %>

            <tr><td colspan="2">
            <asp:HyperLink ID="hlStoreAdd" runat="server" EnableViewState="false" Text="新增店鋪" ></asp:HyperLink>
            
            <asp:GridView ID="gvStore" runat="server" EmptyDataText="無資料" 
                    AutoGenerateColumns="False" Width="950px" Font-Size="Small">
                <Columns>
                    <asp:BoundField AccessibleHeaderText="店名" DataField="StoreName" HeaderText="店名" />
                    <asp:BoundField AccessibleHeaderText="簽約公司名稱" DataField="CompanyName" 
                        HeaderText="簽約公司名稱" />
                    <asp:BoundField AccessibleHeaderText="負責人名稱" DataField="CompanyBossName" 
                        HeaderText="負責人名稱" />
                    <asp:BoundField AccessibleHeaderText="受款人ID/統編" DataField="CompanyID" 
                        HeaderText="受款人ID/統編" />
                    <asp:BoundField AccessibleHeaderText="簽約公司統編" DataField="SignCompanyID" 
                        HeaderText="簽約公司統編" />
                    <asp:BoundField AccessibleHeaderText="受款銀行代號" DataField="CompanyBankCode" 
                        HeaderText="受款銀行代號" />
                    <asp:BoundField AccessibleHeaderText="受款分行代號" DataField="CompanyBranchCode" 
                        HeaderText="受款分行代號" />
                    <asp:BoundField AccessibleHeaderText="受款人帳號" DataField="CompanyAccount" 
                        HeaderText="受款人帳號" />
                    <asp:BoundField AccessibleHeaderText="受款人戶名" DataField="CompanyAccountName" 
                        HeaderText="受款人戶名" />
                    <asp:BoundField AccessibleHeaderText="店家email" DataField="CompanyEmail" 
                        HeaderText="店家email" />
                    <asp:BoundField AccessibleHeaderText="備註" DataField="CompanyNotice" 
                        HeaderText="備註" />
                </Columns>
                </asp:GridView>
                <asp:GridView ID="gvSeller" runat="server" EmptyDataText="無資料" 
                    AutoGenerateColumns="False" Width="950px" Font-Size="Small">
                <Columns>
                    <asp:BoundField AccessibleHeaderText="簽約公司名稱" DataField="CompanyName"
                        HeaderText="簽約公司名稱" />
<%--                    <asp:BoundField AccessibleHeaderText="負責人名稱" DataField="CompanyBossName" 
                        HeaderText="負責人名稱" />--%>
                    <asp:BoundField AccessibleHeaderText="受款人ID/統編" DataField="AccountId" 
                        HeaderText="受款人ID/統編" />
                    <asp:BoundField AccessibleHeaderText="簽約公司統編" DataField="SignCompanyID" 
                        HeaderText="簽約公司統編" />
                    <asp:BoundField AccessibleHeaderText="受款銀行代號" DataField="BankNo" 
                        HeaderText="受款銀行代號" />
                    <asp:BoundField AccessibleHeaderText="受款分行代號" DataField="BranchNo" 
                        HeaderText="受款分行代號" />
                    <asp:BoundField AccessibleHeaderText="受款人帳號" DataField="AccountNo" 
                        HeaderText="受款人帳號" />
                    <asp:BoundField AccessibleHeaderText="受款人戶名" DataField="AccountName" 
                        HeaderText="受款人戶名" />
                    <asp:BoundField AccessibleHeaderText="店家email" DataField="Email" 
                        HeaderText="店家email" />
                    <asp:BoundField AccessibleHeaderText="備註" DataField="Message" 
                        HeaderText="備註" />
                </Columns>
                </asp:GridView>
            </td></tr>
            <% } else { %>
            <asp:Panel ID="Panel1" runat="server" Visible="False">
            <tr>
                <td class="head2">
                    簽約公司名稱
                </td>
                <td class="row">
                    <asp:TextBox ID="tbx_CompanyName" runat="server" Width="300" Enabled="false"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfv_CompanyName" runat="server" ErrorMessage="*"
                        Display="Dynamic" ValidationGroup="A" ControlToValidate="tbx_CompanyName"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="head2">
                    負責人名稱
                </td>
                <td class="row">
                    <asp:TextBox ID="tbx_SellerName" runat="server" Width="300" Enabled="false"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfv_SellerName" runat="server" ErrorMessage="*" Display="Dynamic"
                        ValidationGroup="A" ControlToValidate="tbx_SellerName"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="head2">
                    受款人ID/統編
                </td>
                <td class="row">
                    <asp:TextBox ID="tbx_AccountID" runat="server" MaxLength="10" Enabled="false"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfv_AccountID" runat="server" ErrorMessage="*" Display="Dynamic"
                        ValidationGroup="A" ControlToValidate="tbx_AccountID"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="head2">
                    簽約公司統編
                </td>
                <td class="row">
                    <asp:TextBox ID="tbx_Sign_C_ID" runat="server" MaxLength="10" Enabled="false"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="head2">
                    受款行銀行代號
                </td>
                <td class="row">
                    <asp:TextBox ID="tbx_BankNo" runat="server" MaxLength="3" Enabled="false"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfv_BankNo" runat="server" ErrorMessage="*" Display="Dynamic"
                        ValidationGroup="A" ControlToValidate="tbx_BankNo"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="rev_BankNo" runat="server" ErrorMessage="數字"
                        ControlToValidate="tbx_BankNo" Display="Dynamic" ValidationGroup="A" ValidationExpression="\d{3}"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td class="head2">
                    受款行分行代號
                </td>
                <td class="row">
                    <asp:TextBox ID="tbx_BranchNo" runat="server" MaxLength="4" Enabled="false"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfv_BranchNo" runat="server" ErrorMessage="*" Display="Dynamic"
                        ValidationGroup="A" ControlToValidate="tbx_BranchNo"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="rev_BranchNo" runat="server" ErrorMessage="數字"
                        ControlToValidate="tbx_BranchNo" Display="Dynamic" ValidationGroup="A" ValidationExpression="\d{4}"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td class="head2">
                    受款人帳號
                </td>
                <td class="row">
                    <asp:TextBox ID="tbx_AccountNo" runat="server" Width="300" Enabled="false"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfv_AccountNo" runat="server" ErrorMessage="*" Display="Dynamic"
                        ValidationGroup="A" ControlToValidate="tbx_AccountNo"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="head2">
                    受款人戶名
                </td>
                <td class="row">
                    <asp:TextBox ID="tbx_AccountName" runat="server" Width="300" Enabled="false"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfv_AccountName" runat="server" ErrorMessage="*"
                        Display="Dynamic" ValidationGroup="A" ControlToValidate="tbx_AccountName"></asp:RequiredFieldValidator>
                </td>
            </tr>

            <tr>
                <td class="head2">
                    店家Email
                </td>
                <td class="row">
                    <asp:TextBox ID="tbx_Email" runat="server" Width="300" Enabled="false"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="head2">
                    備註
                </td>
                <td class="row">
                    <asp:TextBox ID="tbx_Message" runat="server" Width="300" TextMode="MultiLine" Rows="5" Enabled="false"></asp:TextBox>
                </td>
            </tr>
            </asp:Panel>
            <% } %>
            <tr>
                <td colspan="2" class="head" style="text-align: center">
                    <asp:Button ID="btn_AddAccount" runat="server" Text="新增" OnCommand="ModifyAccount"
                        CommandName="ToConfirm" Visible="false" ValidationGroup="A" />
                    <asp:Label ID="lab_Message" runat="server" ForeColor="Red"></asp:Label>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="pan_Confirm" runat="server" Visible="false">
        <asp:Panel ID="pan_C_Bid" runat="server">
            <table style="width: 950px;">
                <tr>
                    <td class="head2">
                        <asp:label ID="lab_IdTitle" runat="server">Bid</asp:label>
                    </td>
                    <td class="row">
                        <asp:Label ID="lab_C_Bid" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="head2">
                        檔次名稱
                    </td>
                    <td class="row">
                        <asp:Label ID="lab_C_EventName" runat="server"></asp:Label>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <asp:Panel ID="pan_C_Money" runat="server" Visible="false">
            <table style="width: 950px;">
                <tr>
                    <td class="head2">
                        匯款金額
                    </td>
                    <td class="row">
                        <asp:Label ID="lab_C_Money" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="head2">
                        匯款日
                    </td>
                    <td class="row">
                        <asp:Label ID="lab_C_OS" runat="server"></asp:Label>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <table style="width: 950px;">
            <tr>
                <td colspan="2" class="head" style="text-align: center">
                    確認輸入資料
                </td>
            </tr>
            <% if (PayToCompany == 1) {%>
            <asp:Panel ID="Panel2" runat="server" Visible="False">
            <tr>
                <td class="head2">
                    簽約公司名稱
                </td>
                <td class="row">
                    <asp:Label ID="lab_C_CompanyName" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="head2">
                    負責人名稱
                </td>
                <td class="row">
                    <asp:Label ID="lab_C_SellerName" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="head2">
                    受款人ID/統編
                    簽約公司統編
                </td>
                <td class="row">
                    <asp:Label ID="lab_C_AccountID" runat="server"></asp:Label><br />
                    <asp:Label ID="lab_C_SignID" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="head2">
                    受款人戶名
                </td>
                <td class="row">
                    <asp:Label ID="lab_C_AccountName" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="head2">
                    受款人帳號
                </td>
                <td class="row">
                    <asp:Label ID="lab_C_BankNo" runat="server"></asp:Label>
                    <asp:Label ID="lab_C_BranchNo" runat="server"></asp:Label>
                    <asp:Label ID="lab_C_AccountNo" runat="server"></asp:Label>
                </td>
            </tr>

            <tr>
                <td class="head2">
                    店家Email
                </td>
                <td class="row">
                    <asp:Label ID="lab_C_Email" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="head2">
                    備註
                </td>
                <td class="row">
                    <asp:Label ID="lab_C_Message" runat="server"></asp:Label>
                </td>
            </tr>
            </asp:Panel>
            <% } %>
            <tr>
                <td class="head2">匯款資料
                </td>
                <td class="row">
                     <asp:Label ID="lab_C_PayTo" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="head2">
                    出帳日
                </td>
                <td class="row">
                    <asp:RadioButton ID="rbAchWeeklyC" GroupName="achTypeC" runat="server"  />ACH每週出帳
                    <asp:RadioButton ID="rbAchMonthlyC" GroupName="achTypeC" runat="server" />ACH每月出帳
                    <asp:RadioButton ID="rbManualWeeklyC" GroupName="achTypeC" runat="server" />人工每週出帳
                    <asp:RadioButton ID="rbManualMonthlyC" GroupName="achTypeC" runat="server" />人工每月出帳
                    <asp:RadioButton ID="rbAchPartiallyC" GroupName="achTypeC" runat="server" />商品(暫付70%)
                    <asp:RadioButton ID="rbAchOthersC" GroupName="achTypeC" runat="server" />其他付款方式<br/>
                    <asp:RadioButton ID="rbWeeklyC" GroupName="achTypeC" runat="server" />新每週出帳
                    <asp:RadioButton ID="rbFortnightlyC" GroupName="achTypeC" runat="server" />雙周結出帳
                    <asp:RadioButton ID="rbMonthlyC" GroupName="achTypeC" runat="server" />新每月出帳
                    <asp:RadioButton ID="rbFlexibleC" GroupName="achTypeC" runat="server" />彈性選擇出帳
                </td>
            </tr>
            <tr>
                <td colspan="2" class="head" style="text-align: center">
                    <asp:Button ID="btn_Confirm" runat="server" Text="確認" OnCommand="ModifyAccount" CommandName="ToModify" />
                    <asp:Button ID="btn_Cancel" runat="server" Text="取消" ForeColor="Red" OnClick="CancelAction" />
                </td>
            </tr>

        </table>
    </asp:Panel>
    <br />
    <asp:Panel ID="pAudit" runat="server" Visible="false">
    <table style="width: 950px">
        <tr>
            <td class="head2" colspan="2">
                修改紀錄
            </td>
        </tr>
        <tr>
            <td colspan="2" class="row">
                <uc2:AuditBoard ID="ab" runat="server" Width="100%" EnableAdd="false" ShowAllRecords="true" ReferenceType="Seller"  />
            </td>
        </tr>
    </table>
    </asp:Panel>
</asp:Content>

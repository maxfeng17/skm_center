﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ControlRoom/backend.master" AutoEventWireup="true" CodeBehind="CouponVerification.aspx.cs" Inherits="LunchKingSite.Web.ControlRoom.Verification.CouponVerification" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <script type="text/javascript" src="../../Tools/js/jquery-1.4.2.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {

            $('#divCoupon div table tbody tr td div input[type=checkbox]').filter(':visible').change(function () {
                var isChecked = $(this).is(':checked');
                if (isChecked) {
                    var message1 = $(this).parent().children(':first-child').attr('msg');
                    $(this).parent().children(':first-child').attr('msg', message1.replace('？', '(只退購物金)？'));
                }
                else {
                    var message2 = $(this).parent().children(':first-child').attr('msg');
                    $(this).parent().children(':first-child').attr('msg', message2.replace('(只退購物金)？', '？'));
                }
            });

            $('.clrTextBox').focus(function () {
                if (this.value == '請輸入憑證編號 (不含確認碼)  Ex: 1001-9647') {
                    $(this).val('');
                } else if (this.value == '請輸入憑證確認碼 Ex: 189866') {
                    $(this).val('');
                }
            });

        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>查詢憑證</h2>
    <div>
        <fieldset style="width: 600px; padding: 10px;">
            <asp:RadioButtonList ID="rblSearchType" runat="server"
                RepeatDirection="Horizontal" OnSelectedIndexChanged="rblSearchType_SelectedIndexChanged" AutoPostBack="True">
                <asp:ListItem Selected="True" Value="1">P好康</asp:ListItem>
                <asp:ListItem Value="2">品生活</asp:ListItem>
                <asp:ListItem Value="4">天貓</asp:ListItem>

            </asp:RadioButtonList>
            <div style="margin: 5px 0px 10px 0px;">
                <asp:DropDownList ID="ddlST" runat="server" DataTextField="Value" DataValueField="Key" Width="23%" AutoPostBack="True" OnSelectedIndexChanged="ddlST_SelectedIndexChanged"/>
                <asp:TextBox runat="server" ID="tbCouponSn" CssClass="clrTextBox" Width="75%">請輸入憑證編號 (不含確認碼)  Ex: 1001-9647</asp:TextBox>
            </div>
            <asp:Button ID="btnSearch" runat="server" Text="查詢" OnClick="btnSearch_Click" />
            <span>查詢結果為目前最新的憑證狀態</span>
        </fieldset>
    </div>

    <div id="divCoupon" style="padding: 10px;">

        <asp:GridView ID="gvCoupons" runat="server" AutoGenerateColumns="False"
            OnRowDataBound="gvCoupons_RowDataBound" OnRowCommand="gvCoupons_RowCommand">
            <Columns>
                <asp:BoundField DataField="pid" HeaderText="檔號(PID)">
                    <HeaderStyle BackColor="White" />
                    <ItemStyle BackColor="White" />
                </asp:BoundField>
                <asp:BoundField HeaderText="憑證編號" DataField="CouponSn">
                    <HeaderStyle BackColor="White" />
                    <ItemStyle BackColor="White" />
                </asp:BoundField>
                <asp:BoundField HeaderText="確認碼" DataField="CouponCode">
                    <HeaderStyle BackColor="White" />
                    <ItemStyle BackColor="White" />
                </asp:BoundField>
                <asp:TemplateField HeaderText="訂單編號">
                    <ItemTemplate>
                        <asp:HyperLink ID="HyperLink1" runat="server"
                            NavigateUrl='<%# (int)Eval("OrderClassification") == 1 ? Eval("OrderGuid", "~/ControlRoom/Order/order_detail.aspx?oid={0}") : Eval("OrderGuid", "~/ControlRoom/piinlife/hidealorderdetaillist.aspx?oid={0}") %>'
                            Text='<%# Eval("OrderId") %>'></asp:HyperLink>
                    </ItemTemplate>
                    <HeaderStyle BackColor="White" HorizontalAlign="Center" />
                    <ItemStyle BackColor="White" HorizontalAlign="Center" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="憑證狀態">
                    <ItemTemplate>
                        <asp:Literal ID="Literal1" runat="server"
                            Text='<%# UsageStatusToString((int) Eval("CouponUseStatus")) %>'></asp:Literal>
                    </ItemTemplate>
                    <HeaderStyle BackColor="White" />
                    <ItemStyle BackColor="White" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="操作">
                    <ItemTemplate>
                        <div id="divVerified" runat="server" style="display: none;">
                            <div>
                                <asp:Button ID="btnForceRefund" runat="server" Text="強制退貨"
                                CommandName="ForceReturn" CommandArgument='<%# Eval("CouponId") %>'
                                OnClientClick="var s = $(this).attr('msg'); if (!confirm(s)) { return false; };" />
                            </div>
                            
                            <div>
                                <asp:CheckBox ID="ckForceRefundSCash" runat="server" Text="只退購物金" />
                            </div>
                            <div>
                                <asp:Button ID="btnUnverify" runat="server" Text="取消核銷"
                                    CommandName="Unverify" CommandArgument='<%# Eval("CouponId") %>'
                                    OnClientClick="var s = $(this).attr('msg'); if (!confirm(s)) { return false; };" />
                            </div>
                        </div>
                        <div id="divRefunded" runat="server" style="display: none;">
                            <asp:Button ID="btnForceVerify" runat="server" Text="強制核銷"
                                CommandName="ForceVerify" CommandArgument='<%# Eval("CouponId") %>'
                                OnClientClick="var s = $(this).attr('msg'); if (!confirm(s)) { return false; };" />
                            <asp:Literal ID="litCannotForceVerify" runat="server" Text="欲強制核銷，<br/>請至清冊核銷頁" />
                        </div>
                        <div id="divForcedVerified" runat="server" style="display: none;">
                            <asp:Literal ID="Literal2" runat="server" Text="--" />
                        </div>
                        <div id="divForcedRefunded" runat="server" style="display: none;">
                            <asp:Literal ID="Literal3" runat="server" Text="--" />
                        </div>
                        <div id="divLostVerified" runat="server" style="display: none;">
                            <asp:Literal ID="Literal4" runat="server" Text="--" />
                        </div>
                        <div id="divRefundBeforeClose" runat="server" style="display: none;">
                            <asp:Literal ID="Literal5" runat="server" Text="--" />
                        </div>
                        <div id="divUnverified" runat="server" style="display: none;">
                            <div>
                                <asp:Button ID="btnVerify" runat="server" Text="核銷"
                                    CommandName="Verify" CommandArgument='<%# Eval("CouponId") %>'
                                    OnClientClick="var s = $(this).attr('msg'); if (!confirm(s)) { return false; };" />
                            </div>
                            <div>
                                <asp:Literal ID="litNoRefund" runat="server" Text="不接受退貨" /></div>
                            <div>
                                <asp:Literal ID="litExpireNoRefund" runat="server" Text="憑證過期後<br/>不接受退貨" /></div>
                        </div>
                        <div id="divDealVerified" runat="server" style="display: none;">
                            <asp:Literal ID="Literal7" runat="server" Text="所屬檔次<br/>已核銷完畢<br/>無法更動狀態" />
                        </div>
                    </ItemTemplate>
                    <HeaderStyle BackColor="White" />
                    <ItemStyle BackColor="White" />
                </asp:TemplateField>
            </Columns>
        </asp:GridView>

    </div>

    <div>
        <asp:Literal ID="litSearchWarningMessage" runat="server" />
        <asp:Literal ID="litProcessResultMessage" runat="server" />
    </div>
</asp:Content>

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ControlRoom/backend.master" AutoEventWireup="true"
    CodeBehind="SellerVerificationSearch.aspx.cs" Inherits="LunchKingSite.Web.ControlRoom.Verification.SellerVerificationSearch" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <style type="text/css">
        .tbSellerVerificationSearch
        {
            width:1200px;
            border-collapse:collapse
        }
        .header_tr
        {
            background-color:#6B696B;
            font-size:14px;
            font-weight:bold;
            color:White;
            text-align:center
        }
        .content_tr
        {
            background-color:#F7F7DE
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2>
        核銷檔次查詢</h2>
    <table>
        <tr>
            <td>
                <asp:DropDownList ID="ddlSearch" runat="server" DataTextField="Value" DataValueField="Key">
                </asp:DropDownList>
                <asp:TextBox ID="txtFilterText" runat="server" Width="250px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvFilterText" runat="server" ControlToValidate="txtFilterText"
                    Display="Dynamic" ErrorMessage="請輸入欲查詢內容。" ValidationGroup="Search"></asp:RequiredFieldValidator>
            </td>
            <td>
                <asp:Button ID="btnSearch" runat="server" Text="搜尋" OnClick="btnSearch_Click" ValidationGroup="Search"/>
            </td>
        </tr>
    </table>
    <br />
    <asp:Label ID="lblErrorMsg" runat="server" ForeColor="Red"></asp:Label>
    <asp:Label ID="lblCount" runat="server" Visible="false"></asp:Label>
    <asp:Panel ID="setSellerVerificationSearch" runat="server" Visible="false">
        <div id="divRemark">
            【說明】<br/>
            <ul>
                <li>結檔銷售：即結檔當下的份數，故結檔後才會顯示。</li>
                <li>強制核銷：先退貨又核銷（最終狀態：核銷）的憑證份數。</li>
                <li>強制退貨：先核銷又退貨（最終狀態：退貨）的憑證份數。</li>
                <li>商家系統核銷數（店家看到的核銷數） ＝ 已核銷＋強制核銷＋強制退貨。</li>
                <li>客服查看退貨數 ＝ 強制核銷＋退貨＋強制退貨。</li>
                <li>結檔前退貨 ＝ 總銷售數－結檔銷售。</li>
            </ul>
        </div>
        <table class="tbSellerVerificationSearch" cellspacing="0" rules="all" border="1">
            <tr class="header_tr">
                <td>商家系統核銷數</td>
                <td>客服查看退貨數</td>
                <td>未核銷</td>
            </tr>
            <tr class="content_tr">
                <td align="center"><asp:Literal ID="ltlTotalVerifiedCount" runat="server" /></td>
                <td align="center"><asp:Literal ID="ltlServiceReturnCount" runat="server" /></td>
                <td align="center"><asp:Literal ID="ltlUnVerifyCount" runat="server" /></td>
            </tr>
        </table><br/>
        <table class="tbSellerVerificationSearch" cellspacing="0" rules="all" border="1">
            <tr class="header_tr">
                <td rowspan="2" style="width:10%">店家名稱</td>
                <td rowspan="2" style="width:7%">檔號</td>
                <td rowspan="2" style="width:20%">商品名稱</td>
                <td rowspan="2" style="width:7%">總銷售數</td>
                <td rowspan="2" style="width:7%">結檔銷售</td>
                <td colspan="2" style="width:14%">核銷</td>
                <td colspan="2" style="width:21%">退貨</td>
                <td rowspan="2" style="width:7%">未核銷</td>
            </tr>
            <tr class="header_tr">
                <td style="width:7%">已核銷</td>
                <td style="width:7%">強制核銷</td>
                <td style="width:7%">已退貨</td>
                <td style="width:7%">強制退貨</td>
            </tr>            
            <asp:Repeater ID="repSellerVerificationSearch" runat="server" OnItemDataBound="repSellerVerificationSearch_ItemDataBound">     
                <ItemTemplate>
                    <tr class="content_tr">
                        <td><asp:Literal runat="server" Text='<%# Eval("SellerName")%>' /></td>
                        <td align="center"><asp:Literal runat="server" Text='<%# Eval("UniqueId")%>' /></td>
                        <td><asp:HyperLink ID="hkItemName" runat="server" Target="_blank"></asp:HyperLink></td>
                        <td align="center"><asp:Literal ID="ltlSaleCount" runat="server" /></td>
                        <td align="center"><asp:Literal ID="ltlAfterDealCloseSaleCount" runat="server" /></td>
                        <td align="center"><asp:Literal runat="server" Text='<%# Eval("VerifiedCount")%>' /></td>
                        <td align="center"><asp:Literal runat="server" Text='<%# Eval("ForceVerifyCount")%>' /></td>
                        <td align="center"><asp:Literal runat="server" Text='<%# Eval("ReturnedCount")%>' /></td>
                        <td align="center"><asp:Literal runat="server" Text='<%# Eval("ForceReturnCount")%>' /></td>
                        <td align="center"><asp:Literal runat="server" Text='<%# Eval("UnverifiedCount")%>' /></td>
                    </tr>
                </ItemTemplate>    
            </asp:Repeater>
        </table>
    </asp:Panel>
</asp:Content>

﻿using LunchKingSite.BizLogic.Model.BalanceSheet;
using LunchKingSite.BizLogic.Vbs.BS;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.I18N;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.BizLogic.Facade;

namespace LunchKingSite.Web.ControlRoom.Verification
{
    public partial class PaymentReceiptManage : RolePage, IPaymentReceiptManageView
    {
        public PaymentReceiptManageMode ManagementMode { get; set; }

        public event EventHandler SearchClicked;
        public event EventHandler QueryClicked;
        public event EventHandler QueryUnReceiptReceivedInfoClicked;
        public event EventHandler QueryRemainderBillClicked;
        public event EventHandler DeleteBill;
        public event EventHandler ExportBsBillInfoClicked;
        public event EventHandler GetConfirmed;
        public event EventHandler<DataEventArgs<string[]>> GetSheet;
        public event EventHandler<DataEventArgs<Dictionary<int, DateTime>>> UpdateBillSentDate;
        public event EventHandler<DataEventArgs<Dictionary<int, DateTime>>> UpdateFinanceGetDate;
        public event EventHandler<DataEventArgs<int>> PageChanged;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                this.Presenter.OnViewInitialized();
            }
            this.Presenter.OnViewLoaded();

            AddBalanceSheetBill.Saved += OnQueryBalanceSheetBillInfo;
            AddBalanceSheetBill.Canceled += OnBalanceSheetBillHide;
            UpdateBalanceSheetBill.Saved += OnQueryBalanceSheetBillInfo;
            UpdateBalanceSheetBill.Canceled += OnBalanceSheetBillHide;
        }

        private PaymentReceiptManagePresenter _presenter;
        public PaymentReceiptManagePresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        public string FilterBillId
        {
            get { return txtAddBillId.Text; }
        }

        //單據建檔中輸入餘額單據編號帶出資料前須先判斷單據開立方式是否一致
        public int FilterAddBillReceiptType
        {
            get
            {
                int receiptType = 0;
                foreach (RepeaterItem row in repBalanceSheet.Items)
                {
                    if (((CheckBox)row.FindControl("chkBalanceSheet")).Checked)
                    {
                        receiptType = int.Parse(((HiddenField)row.FindControl("receiptType")).Value);
                    }
                }
                return receiptType;
            }
        }

        public void SetFilterTypeDropDown(Dictionary<string, string> types)
        {
            ddlST.DataSource = types;
            ddlST.DataBind();
        }

        public void SetFilterRemittanceType(Dictionary<int, string> types)
        {
            ddlRemittanceType.DataSource = types;
            ddlRemittanceType.DataBind();
        }

        public void SetFilterReceiptType(Dictionary<int, string> types)
        {
            ddlReceiptType.DataSource = types;
            ddlReceiptType.DataBind();
        }

        public void SetFilterBuyerType(Dictionary<string, string> types)
        {
            ddlBuyerType.DataSource = types;
            ddlBuyerType.DataBind();
        }

        public void SetFilterQueryTypes(Dictionary<string, string> types)
        {
            ddlSTQuery.DataSource = types;
            ddlSTQuery.DataBind();
        }

        public void SetFilterDepartment(Dictionary<string, string> types)
        {
            ddlDepartment1.DataSource = ddlDepartment2.DataSource = ddlDepartment3.DataSource = types;
            ddlDepartment1.DataBind();
            ddlDepartment2.DataBind();
            ddlDepartment3.DataBind();
        }

        public void SetFilterDeliveryType(Dictionary<int, string> types)
        {
            ddlDeliveryType.DataSource = types;
            ddlDeliveryType.DataBind();
        }

        public string FilterType
        {
            get { return ddlST.SelectedValue; }
            set { ddlST.SelectedValue = value; }
        }

        public string FilterUser
        {
            get { return tbSearch.Text; }
            set { tbSearch.Text = value; }
        }

        public string FilterQueryType
        {
            get { return ddlSTQuery.SelectedValue; }
            set { ddlSTQuery.SelectedValue = value; }
        }

        public string FilterQueryUser
        {
            get { return tbSearchQuery.Text; }
            set { tbSearchQuery.Text = value; }
        }

        public void ShowMessage(string msg)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('" + msg + "');", true);
        }

        public Dictionary<int, string> DealTypeInfos
        {
            get
            {
                Dictionary<int, string> data = new Dictionary<int, string>();
                data.Add((int)DepartmentTypes.Ppon, Phrase.SDT_Ppon);
                data.Add((int)DepartmentTypes.HiDeal, Phrase.SDT_HiDeal);
                return data;
            }
        }

        public Dictionary<string, string> FilterTypes
        {
            get
            {
                Dictionary<string, string> data = new Dictionary<string, string>();
                data.Add(ViewBalanceSheetBillList.Columns.UniqueId, Resources.Localization.UniqueId + "/PID");
                data.Add(ViewBalanceSheetBillList.Columns.Name, Resources.Localization.DealName);
                data.Add(ViewBalanceSheetBillList.Columns.SellerName, Resources.Localization.SellerName);
                data.Add(ViewBalanceSheetBillList.Columns.CompanyID, Resources.Localization.CompanyID);
                data.Add(ViewBalanceSheetBillList.Columns.SignCompanyID, "簽約公司統編");
                data.Add(ViewBalanceSheetBillList.Columns.ProductGuid, "BID");

                return data;
            }
        }


        public Dictionary<int, string> FilterRemittanceType
        {
            get
            {
                Dictionary<int, string> data = new Dictionary<int, string>();
                data.Add(-1, "出帳方式");
                foreach (int remittanceType in Enum.GetValues(typeof(RemittanceType)))
                {
                    data.Add(remittanceType, Helper.GetLocalizedEnum((RemittanceType)remittanceType));
                }
                return data;
            }
        }


        public Dictionary<int, string> FilterReceiptType
        {
            get
            {
                Dictionary<int, string> data = new Dictionary<int, string>();
                data.Add(-1, Resources.Localization.All);
                foreach (var receiptType in Enum.GetValues(typeof(VendorReceiptType)))
                {
                    data.Add((int)receiptType, Helper.GetLocalizedEnum(Resources.Localization.ResourceManager, (VendorReceiptType)receiptType));
                }
                return data;
            }
        }

        public Dictionary<string, string> FilterBuyerType
        {
            get
            {
                Dictionary<string, string> data = new Dictionary<string, string>();
                data.Add("All", Resources.Localization.All);
                data.Add("1", Resources.Localization.PayEasy);
                data.Add("2", Resources.Localization._17Life);
                data.Add("4", Resources.Localization._17LifeTravel);
                return data;
            }
        }

        public Dictionary<string, string> FilterQueryTypes
        {
            get
            {
                Dictionary<string, string> data = new Dictionary<string, string>();
                data.Add(ViewBalanceSheetBillList.Columns.UniqueId, Resources.Localization.UniqueId + "/PID");
                data.Add(ViewBalanceSheetBillList.Columns.CreateId, "建檔人");
                data.Add(ViewBalanceSheetBillList.Columns.InvoiceComId, "開立人統編");
                data.Add(ViewBalanceSheetBillList.Columns.Name, Resources.Localization.DealName);
                data.Add(ViewBalanceSheetBillList.Columns.BillNumber, Resources.Localization.InvoiceNumber);
                data.Add(ViewBalanceSheetBillList.Columns.VendorInvoiceNumber, "開立發票號碼");

                return data;
            }
        }

        public Dictionary<int, string> FilterDeliveryType
        {
            get
            {
                Dictionary<int, string> data = new Dictionary<int, string>();
                data.Add(-1, Resources.Localization.All);
                foreach (var deliveryType in Enum.GetValues(typeof(DeliveryType)))
                {
                    data.Add((int)deliveryType, Helper.GetLocalizedEnum((DeliveryType)deliveryType));
                }
                return data;
            }
        }

        public int PageSize
        {
            get { return gridPager.PageSize; }
            set { gridPager.PageSize = value; }
        }

        public int CurrentPage
        {
            get { return gridPager.CurrentPage; }
        }
        public string UserName
        {
            get
            {
                return User.Identity.Name;
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            if (this.SearchClicked != null)
            {
                //重設未回單據的選項，且將其gridview的viewstate關掉，以達到操作其它功能時，未回單據頁將回到未查詢的狀態
                ddlDepartment2.SelectedValue = "";

                this.SearchClicked(this, e);
            }
        }


        protected void btnQueryBillInfo_Click(object sender, EventArgs e)
        {
            if (this.QueryClicked != null)
            {
                //重設未回單據的選項，且將其gridview的viewstate關掉，以達到操作其它功能時，未回單據頁將回到未查詢的狀態
                ddlDepartment2.SelectedValue = "";

                this.QueryClicked(this, e);
            }

            gridPager.ResolvePagerView(1, true);
        }

        public void SetStoreList(DataTable bsLists)
        {
            setBalanceSheetList.Visible = false;
            gvSheetListMerger.Visible = false;
            addBillRegion.Visible = false;
            addBill.Visible = false;

            if (bsLists != null && bsLists.Rows.Count > 0)
            {
                setSellerStore.Visible = true;
                lbSellerStore.Visible = false;
                repSellerStore.DataSource = bsLists;
                repSellerStore.DataBind();
            }
            else
            {
                setSellerStore.Visible = false;
                lbSellerStore.Visible = true;
                lbSellerStore.Text = "查無資料";
            }
        }

        public void SetSheetList(List<BalanceSheetBillListInfo> sheet, List<BalanceSheetBillListInfo> wmsSheet)
        {
            repBalanceSheet.DataSource = sheet;
            repBalanceSheet.DataBind();

            repBalanceSheetWms.DataSource = wmsSheet;
            repBalanceSheetWms.DataBind();


            if (sheet.Count > 0)
            {
                setBalanceSheetList.Visible = true;
                lbBalanceSheet.Visible = false;
                repBalanceSheet.Visible = true;
            }
            if (wmsSheet.Count > 0)
            {
                setBalanceSheetList.Visible = true;
                lbBalanceSheet.Visible = false;
                repBalanceSheetWms.Visible = true;
            }

            if (sheet.Count == 0 && wmsSheet.Count == 0)
            {
                setBalanceSheetList.Visible = false;
                lbBalanceSheet.Visible = true;
                lbBalanceSheet.Text = "無符合的資料";
            }
            else if (sheet.Count == 0)
            {
                repBalanceSheet.Visible = false;
            }
            else if (wmsSheet.Count == 0)
            {
                repBalanceSheetWms.Visible = false;
            }


            gvSheetListMerger.Visible = false;
            addBillRegion.Visible = false;
            addBill.Visible = false;
        }

        protected void GetSheetList(string[] filter)
        {
            if (this.GetSheet != null)
                this.GetSheet(this, new DataEventArgs<string[]>(filter));
        }

        public void SetInvoiceQuery(List<BalanceSheetBillListInfo> sheet, List<BalanceSheetBillListInfo> wmsheet)
        {
            if (btnExportBsBillInfo.CommandName == "ExportToExcel")
            {
                ExportBsBillInfo(sheet, wmsheet);
            }
            else
            {
                setBillInfo.Visible = false;
                updateBillInfo.Visible = false;
                tblRegionSelect.Visible = false;

                if (sheet != null)
                {
                    repInvoiceQuery.DataSource = sheet;
                    
                }
                repInvoiceQuery.DataBind();

                if (wmsheet != null)
                {
                    repInvoiceWmsQuery.DataSource = wmsheet;
                }
                repInvoiceWmsQuery.DataBind();

                lbBillInfoCount.Text = wmsheet.Count.ToString() + sheet.Count.ToString();



                if (int.Parse(lbBillInfoCount.Text) == 0) //查無符合的資料
                {
                    lbBillInfoCount.Visible = true;
                    lbBillInfoCount.Text = "查無符合的資料";
                }
                else
                {
                    lbBillInfoCount.Visible = false;
                    setBillInfo.Visible = true;
                    tblRegionSelect.Visible = true;
                }
            }
            btnExportBsBillInfo.CommandName = "";
        }
        
        //勾選的對帳單月份，尚未有單據金額的對帳單List
        protected void btnSellerStore_Click(object sender, EventArgs e)
        {
            int checkCount = 0;
            foreach (RepeaterItem row in repSellerStore.Items)
            {
                if (row.ItemType == ListItemType.Item || row.ItemType == ListItemType.AlternatingItem)
                {
                    CheckBox cb = (CheckBox)row.FindControl("chkSellerStore");
                    if (cb.Checked)
                    {
                        checkCount++;
                    }
                }
            }
            if (checkCount > 0)
            {
                GetSheetList(FilterSellerStore);
            }
            else
            {
                setBalanceSheetList.Visible = false;
            }
        }

        //準備要key入發票的相關內容
        protected void GetgvInvoiceInfo(object sender, EventArgs e)
        {
            var vstInfo = new List<BalanceSheetBillListInfo>();
            var args = new SheetListMergerArgs();

            var tempReceiptType = string.Empty;
            var tempDeliveryType = string.Empty;
            bool? tempIsTax = null;

            foreach (RepeaterItem row in repBalanceSheet.Items)
            {
                if (row.ItemType == ListItemType.Item || row.ItemType == ListItemType.AlternatingItem)
                {
                    CheckBox cb = (CheckBox)row.FindControl("chkBalanceSheet");
                    if (cb.Checked)
                    {
                        var info = new BalanceSheetBillListInfo();
                        info.Year = int.Parse(((HiddenField)row.FindControl("bsYear")).Value);
                        info.Month = int.Parse(((HiddenField)row.FindControl("bsMonth")).Value);
                        info.IntervalEnd = DateTime.Parse(((HiddenField)row.FindControl("bsIntervalEnd")).Value);
                        info.Name = ((Label)row.FindControl("title")).Text;
                        info.ResidualSubTotal = int.Parse(((Label)row.FindControl("residualSubTotal")).Text);//金額
                        info.ResidualGstTotal = int.Parse(((Label)row.FindControl("residualGstTotal")).Text);//稅額
                        info.ResidualInvoiceTotal = int.Parse(((Label)row.FindControl("residualInvoiceTotal")).Text);//總額
                        info.FreightAmounts = int.Parse(((Label)row.FindControl("freightAmounts")).Text);
                        info.AdjustmentAmount = int.Parse(((Label)row.FindControl("adjustmentAmount")).Text);
                        info.PositivePaymentOverdueAmount = int.Parse(((Label)row.FindControl("positivePaymentOverdueAmount")).Text);
                        info.NegativePaymentOverdueAmount = int.Parse(((Label)row.FindControl("negativePaymentOverdueAmount")).Text);
                        info.IspFamilyAmount = int.Parse(((Label)row.FindControl("ispFamilyAmount")).Text);
                        info.IspSevenAmount = int.Parse(((Label)row.FindControl("ispSevenAmount")).Text);
                        info.WmsOrderAmount = int.Parse(((Label)row.FindControl("wmsOrderAmount")).Text);
                        info.Id = int.Parse(((HiddenField)row.FindControl("id")).Value);
                        info.VendorReceiptType = int.Parse(((HiddenField)row.FindControl("receiptType")).Value);
                        info.StoreName = ((Label)row.FindControl("storeName")).Text;
                        info.SellerGuid = Guid.Parse(((HiddenField)row.FindControl("sellerGuid")).Value);
                        info.RemittanceType = int.Parse(((HiddenField)row.FindControl("remittanceType")).Value);
                        info.UniqueId = int.Parse(((Label)row.FindControl("uniqueId")).Text);
                        info.GenerationFrequency = int.Parse(((HiddenField)row.FindControl("bsGenerationFrequency")).Value);
                        info.DeliveryType = int.Parse(((HiddenField)row.FindControl("deliveryType")).Value);
                        info.DeptId = ((HiddenField)row.FindControl("deptId")).Value;
                        info.IsTaxRequire = bool.Parse(((HiddenField)row.FindControl("isTaxRequire")).Value);
                        args.sumSubTotal += info.ResidualSubTotal;
                        args.sumGstTotal += info.ResidualGstTotal;
                        args.sumInvoiceTotal += info.ResidualInvoiceTotal;
                        args.sumfreightAmountsTotal += info.FreightAmounts;
                        args.sumadjustmentAmountTotal += info.AdjustmentAmount;
                        args.sumPositivePaymentOverdueAmountTotal += info.PositivePaymentOverdueAmount;
                        args.sumNegativePaymentOverdueAmountTotal += info.NegativePaymentOverdueAmount;
                        args.sumispFamilyAmountTotal += info.IspFamilyAmount;
                        args.sumispSevenAmountTotal += info.IspSevenAmount;
                        args.sumWmsOrderAmountTotal += info.WmsOrderAmount;

                        if (string.IsNullOrEmpty(tempReceiptType) && string.IsNullOrEmpty(tempDeliveryType))
                        {
                            tempReceiptType = ((HiddenField)row.FindControl("receiptType")).Value;
                            tempDeliveryType = ((HiddenField)row.FindControl("deliveryType")).Value;
                        }
                        else
                        {
                            if (int.Parse(tempReceiptType) != info.VendorReceiptType)
                            {
                                ShowMessage("單據開立方式不同，不得合開一張發票或收據！");
                                cb.Checked = false;
                                return;
                            }
                            if (int.Parse(tempDeliveryType) != info.DeliveryType)
                            {
                                ShowMessage("憑證檔次與宅配檔次不能合併建單，請重新確認！");
                                cb.Checked = false;
                                return;
                            }
                        }

                        if (!tempIsTax.HasValue)
                        {
                            tempIsTax = bool.Parse(((HiddenField)row.FindControl("isTaxRequire")).Value);
                        }
                        else
                        {
                            if (tempIsTax != info.IsTaxRequire)
                            {
                                ShowMessage("應稅/免稅設定不同，不得合開一張發票或收據！");
                                cb.Checked = false;
                                return;
                            }
                        }

                        hidDeliveryType.Value = info.DeliveryType.ToString();

                        //該檔次是否同意新合約,用來計算預期預期付款日
                        bool IsAgreePponNewContractDeal = false;
                        bool IsAgreeHouseNewContractDeal = false;
                        if (info.DeliveryType == (int)DeliveryType.ToShop || (info.DeliveryType == (int)DeliveryType.ToHouse && info.DeptId != EmployeeChildDept.S010.ToString()))
                            IsAgreePponNewContractDeal = SellerFacade.IsAgreeNewContractSeller(info.SellerGuid, (int)DeliveryType.ToShop);
                        else
                            IsAgreeHouseNewContractDeal = SellerFacade.IsAgreeNewContractSeller(info.SellerGuid, (int)DeliveryType.ToHouse);

                        hidIsAgreePponNewContractSeller.Value = IsAgreePponNewContractDeal.ToString();
                        hidIsAgreeHouseNewContractSeller.Value = IsAgreeHouseNewContractDeal.ToString();
                        vstInfo.Add(info);

                    }
                }
            }


            foreach (RepeaterItem row in repBalanceSheetWms.Items)
            {
                if (row.ItemType == ListItemType.Item || row.ItemType == ListItemType.AlternatingItem)
                {
                    CheckBox cb = (CheckBox)row.FindControl("chkBalanceSheet");
                    if (cb.Checked)
                    {
                        var info = new BalanceSheetBillListInfo();
                        info.Id = int.Parse(((HiddenField)row.FindControl("id")).Value);
                        info.Name = ((Label)row.FindControl("title")).Text;
                        info.Year = int.Parse(((HiddenField)row.FindControl("bsYear")).Value);
                        info.Month = int.Parse(((HiddenField)row.FindControl("bsMonth")).Value);
                        info.DeliveryType = int.Parse(((HiddenField)row.FindControl("deliveryType")).Value);
                        info.RemittanceType = int.Parse(((HiddenField)row.FindControl("remittanceType")).Value);
                        info.VendorReceiptType = int.Parse(((HiddenField)row.FindControl("receiptType")).Value);
                        info.IntervalEnd = DateTime.Parse(((HiddenField)row.FindControl("bsIntervalEnd")).Value);
                        info.SellerGuid = Guid.Parse(((HiddenField)row.FindControl("sellerGuid")).Value);
                        info.GenerationFrequency = int.Parse(((HiddenField)row.FindControl("bsGenerationFrequency")).Value);
                        info.WmsAmount = int.Parse(((Label)row.FindControl("wmsAmount")).Text);

                        args.sumWmsAmountTotal += info.WmsAmount;

                        if (string.IsNullOrEmpty(tempDeliveryType))
                        {
                            tempDeliveryType = ((HiddenField)row.FindControl("deliveryType")).Value;
                        }
                        else
                        {
                            if (int.Parse(tempDeliveryType) != info.DeliveryType)
                            {
                                ShowMessage("憑證檔次與宅配檔次不能合併建單，請重新確認！");
                                cb.Checked = false;
                                return;
                            }
                        }

                        

                        hidDeliveryType.Value = info.DeliveryType.ToString();

                        //該檔次是否同意新合約,用來計算預期預期付款日
                        bool IsAgreePponNewContractDeal = false;
                        bool IsAgreeHouseNewContractDeal = false;
                        //倉儲只有一班的宅配材有
                        if (info.DeliveryType == (int)DeliveryType.ToHouse)
                            IsAgreeHouseNewContractDeal = SellerFacade.IsAgreeNewContractSeller(info.SellerGuid, (int)DeliveryType.ToHouse);


                        hidIsAgreePponNewContractSeller.Value = IsAgreePponNewContractDeal.ToString();
                        hidIsAgreeHouseNewContractSeller.Value = IsAgreeHouseNewContractDeal.ToString();

                        vstInfo.Add(info);

                    }
                }
            }

            #region Calculations

            var config = ProviderFactory.Instance().GetConfig();
            var taxRate = tempIsTax.GetValueOrDefault(false) ? config.BusinessTax : 0;
            args.accSubTotal = (int)Math.Round(args.sumInvoiceTotal / (1 + taxRate), 0);
            args.accSubTotalDiff = args.accSubTotal - args.sumSubTotal;
            args.accGstTotal = args.sumInvoiceTotal - args.accSubTotal;
            args.accGstTotalDiff = args.accGstTotal - args.sumGstTotal;

            #endregion

            if (vstInfo.Count > 0)
            {
                addBillRegion.Visible = true;
                gvSheetListMerger.Visible = true;
                addBill.Visible = false;

                //勾選的對帳單月份，尚未有單據金額的對帳單List
                gvSheetListMerger.DataSource = vstInfo;
                gvSheetListMerger.DataBind();

                ((Label)gvSheetListMerger.FooterRow.FindControl("lbResidualSubTotal")).Text = args.accSubTotal.ToString();
                ((Label)gvSheetListMerger.FooterRow.FindControl("lbResidualGstTotal")).Text = args.accGstTotal.ToString();
                ((Label)gvSheetListMerger.FooterRow.FindControl("lbResidualInvoiceTotal")).Text = args.sumInvoiceTotal.ToString();
                ((Label)gvSheetListMerger.FooterRow.FindControl("lbFreightAmountsTotal")).Text = args.sumfreightAmountsTotal.ToString();
                ((Label)gvSheetListMerger.FooterRow.FindControl("lbAdjustmentAmount")).Text = args.sumadjustmentAmountTotal.ToString();
                ((Label)gvSheetListMerger.FooterRow.FindControl("lbPositivePaymentOverdue")).Text = args.sumPositivePaymentOverdueAmountTotal.ToString();
                ((Label)gvSheetListMerger.FooterRow.FindControl("lbNegativePaymentOverdue")).Text = args.sumNegativePaymentOverdueAmountTotal.ToString();
                ((Label)gvSheetListMerger.FooterRow.FindControl("lbIspFamilyAmount")).Text = args.sumispFamilyAmountTotal.ToString();
                ((Label)gvSheetListMerger.FooterRow.FindControl("lbIspSevenAmount")).Text = args.sumispSevenAmountTotal.ToString();
                ((Label)gvSheetListMerger.FooterRow.FindControl("lbWmsAmount")).Text = args.sumWmsAmountTotal.ToString();
                ((Label)gvSheetListMerger.FooterRow.FindControl("lbWmsOrderAmount")).Text = args.sumWmsOrderAmountTotal.ToString();

                ((Label)gvSheetListMerger.FooterRow.FindControl("subTotalDiff")).Text = "<div style='color:#FB0505'>差額:" + args.accSubTotalDiff.ToString() + "</div>";
                ((Label)gvSheetListMerger.FooterRow.FindControl("gstTotalDiff")).Text = "<div style='color:#FB0505'>差額:" + args.accGstTotalDiff.ToString() + "</div>";
            }
            else
            {
                gvSheetListMerger.Visible = false;
                addBillRegion.Visible = false;
            }
        }

        protected void Add_Click(object sender, EventArgs e)
        {
            setBalanceSheetList.Visible = repBalanceSheet.Items.Count < 200;
            hidManagementMode.Value = ((int)PaymentReceiptManageMode.CreateRelationship).ToString();

            addBill.Visible = true;
            AddBalanceSheetBill.RequestBalanceSheetIds = GetSelectedBalanceSheetIds();

            //未輸入餘額單據編號 單純為新增畫面
            if (txtAddBillId.Text == "")
            {
                AddBalanceSheetBill.RequestMode = ControlRoom.Controls.BillUserControlMode.CreateBill;
                AddBalanceSheetBill.deliveryType = Convert.ToInt32(hidDeliveryType.Value);
                AddBalanceSheetBill.hidIsAgreePponNewContractSeller = Convert.ToBoolean(hidIsAgreePponNewContractSeller.Value);
                AddBalanceSheetBill.hidIsAgreeHouseNewContractSeller = Convert.ToBoolean(hidIsAgreeHouseNewContractSeller.Value);
                AddBalanceSheetBill.ReLoad();
            }
            //輸入餘額單據編號 視同修改單據模式 須根據單據編號帶出單據內容
            else
            {
                int billId;
                if (int.TryParse(txtAddBillId.Text, out billId))
                {
                    AddBalanceSheetBill.RequestBillId = billId;
                    AddBalanceSheetBill.RequestMode = ControlRoom.Controls.BillUserControlMode.AddRelationToBill;
                    AddBalanceSheetBill.ReLoad();
                }
            }
        }

        private Dictionary<int, BalanceSheetUseType> GetSelectedBalanceSheetIds()
        {
            Dictionary<int, BalanceSheetUseType> result = new Dictionary<int, BalanceSheetUseType>();

            foreach (RepeaterItem row in repBalanceSheet.Items)
            {
                if (row.ItemType == ListItemType.Item || row.ItemType == ListItemType.AlternatingItem)
                {
                    CheckBox cb = (CheckBox)row.FindControl("chkBalanceSheet");
                    if (cb.Checked)
                    {
                        result.Add(int.Parse(((HiddenField)row.FindControl("id")).Value), BalanceSheetUseType.Deal);
                    }
                }
            }

            foreach (RepeaterItem row in repBalanceSheetWms.Items)
            {
                if (row.ItemType == ListItemType.Item || row.ItemType == ListItemType.AlternatingItem)
                {
                    CheckBox cb = (CheckBox)row.FindControl("chkBalanceSheet");
                    if (cb.Checked)
                    {
                        result.Add(int.Parse(((HiddenField)row.FindControl("id")).Value), BalanceSheetUseType.Wms);
                    }
                }
            }

            return result;
        }



        protected void repBalanceSheet_ItemCommand(object sender, RepeaterCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "Confirmed":
                    if (GetConfirmed != null)
                    {
                        GetConfirmed(this, e);
                    }

                    break;
            }
        }

        protected void repBalanceSheetWms_ItemCommand(object sender, RepeaterCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "Confirmed":
                    if (GetConfirmed != null)
                    {
                        GetConfirmed(this, e);
                    }

                    break;
            }
        }

        protected void repBalanceSheet_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (GetconfirmedState(int.Parse(DataBinder.Eval(e.Item.DataItem, "Id").ToString()), (int)BalanceSheetUseType.Deal))
                {
                    //Button移到外面去了
                    //Button confirmed = (Button)e.Item.FindControl("confirmed");
                    //string confirmMsg = "";
                    //if (int.Parse(DataBinder.Eval(e.Item.DataItem, "residualInvoiceTotal").ToString()) == 0)
                    //    confirmMsg = "確認鎖定後將無法再異動對帳單，並確定單據與帳款無誤，可進行匯款？";
                    //else
                    //    confirmMsg = "您的單據總額與對帳單金額不符。若強制執行此動作，將無法再異動對帳單，並且會執行此筆對帳單的全額匯款。您是否還要執行此動作？";
                    //confirmed.Attributes.Add("onclick", "javascript:return " + "confirm('" + confirmMsg + "')");
                }
                System.Web.UI.HtmlControls.HtmlTableRow tr = (System.Web.UI.HtmlControls.HtmlTableRow)e.Item.FindControl("tdRepBalanceSheet"); //Where TD1 is the ID of the Table Cell
                if (int.Parse(((Label)e.Item.FindControl("residualInvoiceTotal")).Text) < 0 || (int.Parse(((Label)e.Item.FindControl("residualInvoiceTotal")).Text) == 0 && (int.Parse(((Label)e.Item.FindControl("IspFamilyAmount")).Text) != 0 || int.Parse(((Label)e.Item.FindControl("IspSevenAmount")).Text) != 0 || int.Parse(((Label)e.Item.FindControl("negativePaymentOverdueAmount")).Text) != 0 || int.Parse(((Label)e.Item.FindControl("wmsOrderAmount")).Text) != 0)))
                {
                    tr.Attributes.Add("style", "color:red;");
                }
                CheckBox chkBalanceSheet = (CheckBox)e.Item.FindControl("chkBalanceSheet");
                chkBalanceSheet.Attributes.Add("onclick", "javascript:recordScrollPosition()");
            }
        }

        protected void repBalanceSheetWms_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (GetconfirmedState(int.Parse(DataBinder.Eval(e.Item.DataItem, "Id").ToString()), (int)BalanceSheetUseType.Wms))
                {

                }
                System.Web.UI.HtmlControls.HtmlTableRow tr = (System.Web.UI.HtmlControls.HtmlTableRow)e.Item.FindControl("tdRepBalanceSheetWms"); //Where TD1 is the ID of the Table Cell
                if (int.Parse(((Label)e.Item.FindControl("residualInvoiceTotal")).Text) < 0 || (int.Parse(((Label)e.Item.FindControl("residualInvoiceTotal")).Text) == 0 && (int.Parse(((Label)e.Item.FindControl("wmsAmount")).Text) != 0)))
                {
                    tr.Attributes.Add("style", "color:red;");
                }
                CheckBox chkBalanceSheet = (CheckBox)e.Item.FindControl("chkBalanceSheet");
                chkBalanceSheet.Attributes.Add("onclick", "javascript:recordScrollPosition()");
            }
        }

        protected void repInvoiceQuery_ItemCommand(object sender, RepeaterCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "UpdateBill":
                    updateBillInfo.Visible = true;
                    UpdateBalanceSheetBill.RequestMode = ControlRoom.Controls.BillUserControlMode.UpdateBill;
                    UpdateBalanceSheetBill.RequestBillId = int.Parse(e.CommandArgument.ToString().Split("/")[1]);
                    Dictionary<int, BalanceSheetUseType> balanceSheetIDs = new Dictionary<int, BalanceSheetUseType>();
                    balanceSheetIDs.Add(int.Parse(e.CommandArgument.ToString().Split("/")[0]),BalanceSheetUseType.Deal);
                    UpdateBalanceSheetBill.RequestBalanceSheetIds = balanceSheetIDs;
                    UpdateBalanceSheetBill.ReLoad();
                    break;
                case "DeleteBill":
                    if (DeleteBill != null)
                    {
                        DeleteBill(this, e);
                    }
                    break;
                case "UpdateBillRemark":
                    string newBillRemark = ((TextBox)e.Item.FindControl("txtBillRemark")).Text;
                    int billId1;
                    if (int.TryParse((string)e.CommandArgument, out billId1))
                    {
                        this.Presenter.UpdateBillRemark(billId1, newBillRemark);
                        Presenter.OnQueryClicked(sender, e);
                    }
                    break;
                case "UpdateBsRemark":
                    string newBsRemark = ((TextBox)e.Item.FindControl("txtBsRemark")).Text;
                    string[] ids = ((string)e.CommandArgument).Split('/');
                    int balanceSheetId;
                    int billId;
                    if (int.TryParse(ids[0], out balanceSheetId)
                        && int.TryParse(ids[1], out billId))
                    {
                        this.Presenter.UpdateBsRemark(balanceSheetId, billId, newBsRemark, BalanceSheetUseType.Deal);
                        Presenter.OnQueryClicked(sender, e);
                    }
                    break;
            }
        }

        protected void repInvoiceQuery_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            string confirmMsg = "";
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (DataBinder.Eval(e.Item.DataItem, "BillId") != null)
                {
                    Button deleteBill = (Button)e.Item.FindControl("btnDeleteBill");
                    confirmMsg = "您確定要刪除此單據，單據編號[" + DataBinder.Eval(e.Item.DataItem, "BillId").ToString() + "]，刪除後將無法回復，確定刪除嗎？";
                    deleteBill.Attributes.Add("onclick", "javascript:return " + "confirm('" + confirmMsg + "')");
                }
            }
        }

        protected void repInvoiceWmsQuery_ItemCommand(object sender, RepeaterCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "UpdateBill":
                    updateBillInfo.Visible = true;
                    UpdateBalanceSheetBill.RequestMode = ControlRoom.Controls.BillUserControlMode.UpdateBill;
                    UpdateBalanceSheetBill.RequestBillId = int.Parse(e.CommandArgument.ToString().Split("/")[1]);
                    Dictionary<int, BalanceSheetUseType> balanceSheetIDs = new Dictionary<int, BalanceSheetUseType>();
                    balanceSheetIDs.Add(int.Parse(e.CommandArgument.ToString().Split("/")[0]), BalanceSheetUseType.Wms);
                    UpdateBalanceSheetBill.RequestBalanceSheetIds = balanceSheetIDs;
                    UpdateBalanceSheetBill.ReLoad();
                    break;
                case "DeleteBill":
                    if (DeleteBill != null)
                    {
                        DeleteBill(this, e);
                    }
                    break;
                case "UpdateBillRemark":
                    string newBillRemark = ((TextBox)e.Item.FindControl("txtBillRemark")).Text;
                    int billId1;
                    if (int.TryParse((string)e.CommandArgument, out billId1))
                    {
                        this.Presenter.UpdateBillRemark(billId1, newBillRemark);
                        Presenter.OnQueryClicked(sender, e);
                    }
                    break;
                case "UpdateBsRemark":
                    string newBsRemark = ((TextBox)e.Item.FindControl("txtBsRemark")).Text;
                    string[] ids = ((string)e.CommandArgument).Split('/');
                    int balanceSheetId;
                    int billId;
                    if (int.TryParse(ids[0], out balanceSheetId)
                        && int.TryParse(ids[1], out billId))
                    {
                        this.Presenter.UpdateBsRemark(balanceSheetId, billId, newBsRemark, BalanceSheetUseType.Wms);
                        Presenter.OnQueryClicked(sender, e);
                    }
                    break;
            }
        }

        protected void repInvoiceWmsQuery_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            string confirmMsg = "";
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (DataBinder.Eval(e.Item.DataItem, "BillId") != null)
                {
                    Button deleteBill = (Button)e.Item.FindControl("btnDeleteBill");
                    confirmMsg = "您確定要刪除此單據，單據編號[" + DataBinder.Eval(e.Item.DataItem, "BillId").ToString() + "]，刪除後將無法回復，確定刪除嗎？";
                    deleteBill.Attributes.Add("onclick", "javascript:return " + "confirm('" + confirmMsg + "')");
                }
            }
        }

        public string[] FilterInternal
        {
            get
            {
                List<string> filter = new List<string>();

                if (ddlST.SelectedValue == ViewBalanceSheetBillList.Columns.Name ||
                    ddlST.SelectedValue == ViewBalanceSheetBillList.Columns.SellerName)
                {
                    if (!string.IsNullOrEmpty(tbSearch.Text))
                    {
                        filter.Add(ddlST.SelectedValue + " like N'%" + tbSearch.Text.Replace("'","''") + "%'");
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(tbSearch.Text))
                    {
                        filter.Add(ddlST.SelectedValue + " like '" + tbSearch.Text.Replace("'", "''") + "'");
                    }
                }

                if (ddlDate.SelectedValue == "ddlOrderTime")//銷售期間
                {
                    if (!string.IsNullOrEmpty(tbDS.Text))
                    {
                        filter.Add(ViewBalanceSheetBillList.Columns.DealStartTime + " >= '" + tbDS.Text.Replace("'", "''") + "'");
                    }

                    if (!string.IsNullOrEmpty(tbDE.Text))
                    {
                        filter.Add(ViewBalanceSheetBillList.Columns.DealEndTime + " <= '" + tbDE.Text.Replace("'", "''") + "'");
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(tbDS.Text))
                    {
                        filter.Add(ViewBalanceSheetBillList.Columns.UseStartTime + " >= '" + tbDS.Text.Replace("'", "''") + "'");
                    }

                    if (!string.IsNullOrEmpty(tbDE.Text))
                    {
                        filter.Add(ViewBalanceSheetBillList.Columns.UseEndTime + " <= '" + tbDE.Text.Replace("'", "''") + "'");
                    }
                }

                return filter.ToArray();
            }
        }

        public string[] FilterSellerStore
        {
            get
            {
                string sellerGuidList = "", storeGuidList = "";
                foreach (RepeaterItem row in repSellerStore.Items)
                {
                    if (row.ItemType == ListItemType.Item || row.ItemType == ListItemType.AlternatingItem)
                    {
                        CheckBox cb = (CheckBox)row.FindControl("chkSellerStore");
                        string[] sArray = cb.Attributes["value"].Split("/");
                        if (cb.Checked)
                        {
                            if (!string.IsNullOrEmpty(sellerGuidList))
                                sellerGuidList += "','" + sArray[0];
                            else
                                sellerGuidList = sArray[0];

                            if (!string.IsNullOrEmpty(storeGuidList))
                            {
                                if (!string.IsNullOrEmpty(sArray[1]))
                                    storeGuidList += "','" + sArray[1];
                            }
                            else
                                storeGuidList = sArray[1];
                        }
                    }
                }
                List<string> filter = FilterInternal.ToList();

                if (!string.IsNullOrEmpty(sellerGuidList))
                {
                    filter.Add(ViewBalanceSheetBillList.Columns.SellerGuid + " in ('" + sellerGuidList + "')");
                }

                if (!string.IsNullOrEmpty(storeGuidList))
                {
                    filter.Add(ViewBalanceSheetBillList.Columns.StoreGuid + " in ('" + storeGuidList + "')");
                }


                return filter.ToArray();
            }
        }

        public string[] FilterInfo
        {
            get
            {
                List<string> filter = new List<string>();

                if (ddlReceiptType.SelectedValue != "-1")
                    filter.Add(ViewBalanceSheetBillList.Columns.VendorReceiptType + " = " + ddlReceiptType.SelectedValue);

                if (ddlBuyerType.SelectedValue != "All")
                    filter.Add(ViewBalanceSheetBillList.Columns.BuyerType + " = " + ddlBuyerType.SelectedValue);

                ////專員寄出單據日
                //if (!string.IsNullOrEmpty(tbSentDS.Text))
                //    filter.Add(ViewBalanceSheetBillList.Columns.BillSentDate + " >= " + tbSentDS.Text);

                //if (!string.IsNullOrEmpty(tbSentDE.Text))
                //    filter.Add(ViewBalanceSheetBillList.Columns.BillSentDate + " <= " + tbSentDE.Text);

                //對帳月份
                if (!string.IsNullOrEmpty(tbSheetDS.Text))
                {
                    string[] sArray = tbSheetDS.Text.Split("/");
                    filter.Add(ViewBalanceSheetBillList.Columns.IntervalStart + " >= " + new DateTime(int.Parse(sArray[0]), int.Parse(sArray[1]), 1, 0, 0, 0));
                }

                if (!string.IsNullOrEmpty(tbSheetDE.Text))
                {
                    string[] sArray = tbSheetDE.Text.Split("/");
                    filter.Add(ViewBalanceSheetBillList.Columns.IntervalStart + " < " + new DateTime(int.Parse(sArray[0]), int.Parse(sArray[1]) + 1, 1, 0, 0, 0));
                }

                //財務收到單據日
                if (!string.IsNullOrEmpty(tbFinanceGetDateS.Text))
                    filter.Add(ViewBalanceSheetBillList.Columns.FinanceGetDate + " >= " + tbFinanceGetDateS.Text);

                if (!string.IsNullOrEmpty(tbFinanceGetDateE.Text))
                    filter.Add(ViewBalanceSheetBillList.Columns.FinanceGetDate + " <= " + tbFinanceGetDateE.Text);

                //單據建檔日
                if (!string.IsNullOrEmpty(tbCreateTimeS.Text))
                    filter.Add(ViewBalanceSheetBillList.Columns.CreateTime + " >= " + tbCreateTimeS.Text);

                if (!string.IsNullOrEmpty(tbCreateTimeE.Text))
                    filter.Add(ViewBalanceSheetBillList.Columns.CreateTime + " <= " + tbCreateTimeE.Text);

                //預計付款日
                if (!string.IsNullOrEmpty(tbDefaultPaymentTimeS.Text))
                    filter.Add(ViewBalanceSheetBillList.Columns.DefaultPaymentTime + " >= " + tbDefaultPaymentTimeS.Text);

                if (!string.IsNullOrEmpty(tbDefaultPaymentTimeE.Text))
                    filter.Add(ViewBalanceSheetBillList.Columns.DefaultPaymentTime + " <= " + tbDefaultPaymentTimeE.Text);


                //排除週結對帳單
                filter.Add(ViewBalanceSheetBillList.Columns.BalanceSheetType + " <> " + (int)BalanceSheetType.WeeklyPayWeekBalanceSheet);
                filter.Add(ViewBalanceSheetBillList.Columns.BalanceSheetType + " <> " + (int)BalanceSheetType.ManualWeeklyPayWeekBalanceSheet);

                //出帳方式
                if (ddlRemittanceType.SelectedValue != "-1")
                    filter.Add(ViewBalanceSheetBillList.Columns.RemittanceType + " = " + ddlRemittanceType.SelectedValue);

                if (!string.IsNullOrEmpty(ddlDepartment1.SelectedValue) && ddlDepartment1.SelectedValue != "All")
                    filter.Add(ViewBalanceSheetBillList.Columns.DeptId + " = " + ddlDepartment1.SelectedValue);

                //檔次類型
                if (ddlDeliveryType.SelectedValue != "-1")
                    filter.Add(ViewBalanceSheetBillList.Columns.DeliveryType + " = " + ddlDeliveryType.SelectedValue);

                //對開發票開立日期
                if (!string.IsNullOrEmpty(tbVendorInvoiceNumberTimeS.Text))
                    filter.Add(ViewBalanceSheetBillList.Columns.VendorInvoiceNumberTime + " >= " + tbVendorInvoiceNumberTimeS.Text);

                if (!string.IsNullOrEmpty(tbVendorInvoiceNumberTimeE.Text))
                    filter.Add(ViewBalanceSheetBillList.Columns.VendorInvoiceNumberTime + " <= " + tbVendorInvoiceNumberTimeE.Text);

                filter.Add(ViewBalanceSheetBillList.Columns.BillId + " is not null");


                return filter.ToArray();
            }
        }

        public bool IsFilterUnReceiptReceivedInfoFilled
        {
            get { return ddlDepartment2.SelectedValue != ""; }
        }

        public string[] FilterUnReceiptReceivedInfo
        {
            get
            {
                List<string> filter = new List<string>();

                //filter.Add(ViewBalanceSheetBillList.Columns.GenerationFrequency + " in ( " + (int)BalanceSheetGenerationFrequency.MonthBalanceSheet + "," + 
                //                                                                             (int)BalanceSheetGenerationFrequency.ManualTriggeredBalanceSheet + "," +
                //                                                                             (int)BalanceSheetGenerationFrequency.LumpSumBalanceSheet + " )");
                //排除週結對帳單
                filter.Add(ViewBalanceSheetBillList.Columns.BalanceSheetType + " <> " + (int)BalanceSheetType.WeeklyPayWeekBalanceSheet);
                filter.Add(ViewBalanceSheetBillList.Columns.BalanceSheetType + " <> " + (int)BalanceSheetType.ManualWeeklyPayWeekBalanceSheet);

                filter.Add(ViewBalanceSheetBillList.Columns.IsConfirmedReadyToPay + " = " + false);

                if (!string.IsNullOrEmpty(ddlDepartment2.SelectedValue) && ddlDepartment2.SelectedValue != "All")
                {
                    filter.Add(ViewBalanceSheetBillList.Columns.DeptId + " = " + ddlDepartment2.SelectedValue);
                }

                return filter.ToArray();
            }
        }

        public string[] FilterRemainderBillInfo
        {
            get
            {
                List<string> filter = new List<string>();
                
                if (!string.IsNullOrEmpty(txtInvoiceComId.Text))
                    filter.Add(ViewRemainderBillList.Columns.InvoiceComId + " = " + txtInvoiceComId.Text);

                if (!string.IsNullOrEmpty(ddlDepartment3.SelectedValue) && ddlDepartment3.SelectedValue != "All")
                    filter.Add(ViewRemainderBillList.Columns.DeptId + " = " + ddlDepartment3.SelectedValue);

                return filter.ToArray();
            }
        }

        public int PageDataCount
        {
            get;
            set;
        }

        protected string GetTheDate(string time)
    {
            DateTime date;
            return DateTime.TryParse(time, out date) ? date.ToString("yyyy/MM/dd") : "";
        }

        protected string GetTheReceiptType(int receiptType, string receiptTypeRemark)
        {
            string receiptTypeDesc = "";
            receiptTypeDesc = Helper.GetLocalizedEnum(Resources.Localization.ResourceManager, (VendorReceiptType)receiptType);

            //若單據開立方式選擇為其他 需顯示備註內容
            if (receiptType == (int)VendorReceiptType.Other)
                receiptTypeDesc += "：" + receiptTypeRemark;

            return receiptTypeDesc;
        }

        protected string GetBalanceSheetUseType(int useType)
        {
            string useTypeDesc = "";
            useTypeDesc = Helper.GetDescription((BalanceSheetUseType)useType);

            return useTypeDesc;
        }

        protected string GetTheBuyerName(int buyerType)
        {
            string buyerName = string.Empty;
            switch (buyerType)
            {
                case (int)BillBuyerType.PayEasy:
                    buyerName = Resources.Localization.PayEasy;
                    break;
                case (int)BillBuyerType.Contact:
                    buyerName = Resources.Localization._17Life;
                    break;
                case (int)BillBuyerType.PayEasyTravel:
                    buyerName = Resources.Localization.PayEasyTravel;
                    break;
                case (int)BillBuyerType.ContactTravel:
                    buyerName = Resources.Localization._17LifeTravel;
                    break;
                default:
                    break;
            }
            return buyerName;
        }

        protected string GetBalanceSheetDesc(string bsYearMonth, DateTime bsIntervalEnd, int bsGenerationFrequency)
        {
            string bsDesc = bsYearMonth;
            switch (bsGenerationFrequency)
            {
                case (int)BalanceSheetGenerationFrequency.ManualTriggeredBalanceSheet:
                    bsDesc = string.Format("{0:yyyy/MM/dd}<br/>人工對帳單", bsIntervalEnd);
                    break;
                case (int)BalanceSheetGenerationFrequency.LumpSumBalanceSheet:
                    bsDesc = string.Format("{0}<br/>商品對帳單", bsYearMonth);
                    break;
                case (int)BalanceSheetGenerationFrequency.FlexibleBalanceSheet:
                    bsDesc = string.Format("{0:yyyy/MM/dd}", bsIntervalEnd);
                    break;
                case (int)BalanceSheetGenerationFrequency.WeekBalanceSheet:
                    bsDesc = string.Format("{0:yyyy/MM/dd}", bsIntervalEnd.AddDays(-1));
                    break;
                case (int)BalanceSheetGenerationFrequency.FortnightlyBalanceSheet:
                    bsDesc = string.Format("{0:yyyy/MM/dd}", bsIntervalEnd.AddDays(-1));
                    break;
                default:
                    break;
            }
            return bsDesc;
        }

        protected int GetOverdueAmount(int bsId, bool positive)
        {
            var bsModel = new BalanceSheetModel(bsId);
            int OverdueAmount = bsModel.GetVendorPaymentOverdueAmount(positive);

            return OverdueAmount;
        }

        protected void ddlDepartment2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.QueryUnReceiptReceivedInfoClicked != null)
            {
                this.QueryUnReceiptReceivedInfoClicked(this, e);
            }
        }

        protected string GetRemittanceType(int remittanceType)
        {
            return Helper.GetLocalizedEnum((RemittanceType)remittanceType);
        }

        protected string GetDeliveryTypeDesc(int deliveryType)
        {
            return Helper.GetLocalizedEnum((DeliveryType)deliveryType);
        }


        /// <summary>
        /// 單據可否建檔
        /// </summary>
        /// <param name="IsConfirmedReadyToPay"></param>
        /// <param name="residualInvoiceTotal"></param>
        /// <param name="IspFamilyAmount"></param>
        /// <param name="IspSevenAmount"></param>
        /// <param name="overdueAmount"></param>
        /// <returns></returns>
        protected bool GetChkBalanceSheetState(bool IsConfirmedReadyToPay, int residualInvoiceTotal, int IspFamilyAmount, int IspSevenAmount, int overdueAmount, int wmsOrderAmount,int wmsAmount)
        {
            if (IsConfirmedReadyToPay)
            {
                return false;
            }
            else
            {
                if (residualInvoiceTotal != 0 || IspFamilyAmount != 0 || IspSevenAmount != 0 || overdueAmount != 0 || wmsAmount != 0 || wmsOrderAmount != 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        /// <summary>
        /// 單據可否確認付款
        /// </summary>
        /// <param name="bsId"></param>
        /// <returns></returns>
        protected bool GetconfirmedState(int bsId,int useType)
        {
            if (useType == 1)
            {
                BalanceSheetModel balanceSheet = new BalanceSheetModel(bsId);
                return balanceSheet.IsBillingConfirmable && balanceSheet.GetAccountsPayable().ISPFamilyAmount != 0 && balanceSheet.GetAccountsPayable().ISPSevenAmount != 0 && balanceSheet.GetAccountsPayable().WmsOrderAmount != 0;
            }
            else if (useType == 2)
            {
                return false;
            }
            else
                return false;
            
        }

        //更新專員寄出單據日
        protected void btnUpdateBillSentDate_Click(object sender, EventArgs e)
        {
            CheckBox ckb = new CheckBox();
            List<int> billIds = new List<int>();
            Dictionary<int, DateTime> bsIdBillSentDates = new Dictionary<int, DateTime>();
            foreach (RepeaterItem item in repInvoiceQuery.Items)
            {
                ckb = (CheckBox)item.FindControl("chkBillSentDate");
                if (ckb.Visible == true && ckb.Checked == true)
                {
                    billIds.Add(int.Parse(ckb.Attributes["value"]));
                }
            }
            if (billIds.Any())
            {
                foreach (var billId in billIds.Where(billId => !bsIdBillSentDates.ContainsKey(billId)))
                {
                    bsIdBillSentDates.Add(billId, DateTime.Now.Date);
                }
                this.UpdateBillSentDate(this, new DataEventArgs<Dictionary<int, DateTime>>(bsIdBillSentDates));
            }
        }

        //更新財務收到單據日
        protected void btnUpdateFinanceGetDate_Click(object sender, EventArgs e)
        {
            CheckBox ckb = new CheckBox();
            List<int> billIds = new List<int>();
            Dictionary<int, DateTime> bsIdFinanceGetDates = new Dictionary<int, DateTime>();
            foreach (RepeaterItem item in repInvoiceQuery.Items)
            {
                ckb = (CheckBox)item.FindControl("chkFinanceGetDate");
                if (ckb.Visible == true && ckb.Checked == true)
                {
                    billIds.Add(int.Parse(ckb.Attributes["value"]));
                }
            }
            if (billIds.Count > 0)
            {
                foreach (var billId in billIds.Where(billId => !bsIdFinanceGetDates.ContainsKey(billId)))
                {
                    bsIdFinanceGetDates.Add(billId, DateTime.Now.Date);
                }
                this.UpdateFinanceGetDate(this, new DataEventArgs<Dictionary<int, DateTime>>(bsIdFinanceGetDates));
            }
        }

        public void SetUnConfirmBalanceSheetInfo(List<UnConfirmBalanceSheet> bsInfos)
        {
            if (btnExportUnReceiptReceivedInfo.CommandName == "ExportToExcel")
            {
                ExportUnReceiptReceivedInfo(bsInfos);
            }
            else
            {
                gvUnReceiptReceived.DataSource = bsInfos;
                gvUnReceiptReceived.DataBind();
            }

            btnExportUnReceiptReceivedInfo.CommandName = "";
        }

        public void SetRemainderBill(ViewRemainderBillListCollection bills)
        {
            gvRemainderBill.DataSource = bills;
            gvRemainderBill.DataBind();
        }

        protected void btnExportUnReceiptReceivedInfo_OnClick(object sender, EventArgs e)
        {
            btnExportUnReceiptReceivedInfo.CommandName = "ExportToExcel";
            OnQueryUnReceiptReceivedInfo(this, e);
        }

        protected void btnExportBsBillInfo_OnClick(object sender, EventArgs e)
        {
            btnExportBsBillInfo.CommandName = "ExportToExcel";
            if (this.ExportBsBillInfoClicked != null)
            {
                this.ExportBsBillInfoClicked(this, e);
            }
        }

        protected void btnQueryRemainderBill_OnClick(object sender, EventArgs e)
        {
            ddlDepartment2.SelectedValue = "";
            //抓取餘額單據列表資料
            OnQueryRemainderBill(this, e);
        }

        //匯出單據查詢資料列表
        public void ExportBsBillInfo(List<BalanceSheetBillListInfo> bsBillCol, List<BalanceSheetBillListInfo> bsBillWmsCol)
        {
            if (bsBillCol.Any() || bsBillWmsCol.Any())
            {
                DataTable dt = new DataTable();
                DataRow drHeader = dt.NewRow();
                int counter = 0;

                string[] headers = new string[]
                {
                    "財務收到單據日", "對帳單類型(1:檔次2:倉儲)","出帳方式", "對帳單ID","單據建檔日","預計付款日", "對帳單月份", "對帳單金額", "單據開立方式",
                    "買受人", "結檔日", "檔號", "檔次類型", "檔名", "分店", "簽約公司", "發票對應統編", "單據開立日期", "發票號碼",
                    "未稅金額", "稅額", "總額","運費" ,"異動金額","逾期出貨補款","逾期出貨扣款","物流處理費(全家)","物流處理費(7-11)",
                    "倉儲費用(PChome)", "物流處理費(PChome)", "開立發票號碼","開立發票日期","受款戶名", "受款ID", "受款帳號", "銀行代號", "分行代號", "單據備註",
                    "對帳單備註", "業務", "建檔人"
                };

                foreach (string s in headers)
                {
                    DataColumn dc = new DataColumn(s);
                    dt.Columns.Add(dc);
                    drHeader[counter] = dc.ColumnName;
                    counter++;
                }

                dt.Rows.Add(drHeader);
                bsBillCol.AddRange(bsBillWmsCol);
                bsBillCol = bsBillCol.OrderBy(x => x.CreateTime).ToList();
                foreach (var bill in bsBillCol)
                {
                    DataRow dr = dt.NewRow();
                    dr[0] = (bill.FinanceGetDate.HasValue) ? GetTheDate(bill.FinanceGetDate.ToString()) : "";
                    dr[1] = bill.BalanceSheetUseType;
                    dr[2] = GetRemittanceType(bill.RemittanceType);
                    dr[3] = bill.Id;
                    dr[4] = (bill.CreateTime.HasValue) ? GetTheDate(bill.CreateTime.ToString()) : "";
                    dr[5] = (bill.DefaultPaymentTime.HasValue) ? GetTheDate(bill.DefaultPaymentTime.ToString()) : "";
                    dr[6] = GetBalanceSheetDesc(string.Format("{0}/{1}", bill.Year, bill.Month), bill.IntervalEnd, bill.GenerationFrequency).Replace("<br/>", " ");
                    dr[7] = bill.EstAmount;
                    dr[8] = GetTheReceiptType(bill.VendorReceiptType, (!string.IsNullOrEmpty(bill.Message)) ? bill.Message : string.Empty);
                    dr[9] = GetTheBuyerName(bill.BuyerType);
                    dr[10] = GetTheDate(bill.DealEndTime.ToString());
                    dr[11] = bill.UniqueId;
                    dr[12] = GetDeliveryTypeDesc(bill.DeliveryType);
                    dr[13] = bill.Name;
                    dr[14] = bill.StoreName;
                    dr[15] = bill.CompanyName;
                    dr[16] = bill.InvoiceComId;
                    dr[17] = (bill.InvoiceDate.HasValue) ? GetTheDate(bill.InvoiceDate.ToString()) : "";
                    dr[18] = bill.BillNumber;
                    dr[19] = bill.BillMoneyNotaxed;
                    dr[20] = bill.BillTax;
                    dr[21] = bill.BillMoney;
                    dr[22] = bill.FreightAmounts;
                    dr[23] = bill.AdjustmentAmount;
                    dr[24] = bill.PositivePaymentOverdueAmount;
                    dr[25] = bill.NegativePaymentOverdueAmount;
                    dr[26] = bill.IspFamilyAmount;
                    dr[27] = bill.IspSevenAmount;
                    dr[28] = bill.WmsAmount;
                    dr[29] = bill.WmsOrderAmount;
                    dr[30] = bill.VendorInvoiceNumber;
                    dr[31] = (bill.VendorInvoiceNumberTime.HasValue) ? GetTheDate(bill.VendorInvoiceNumberTime.ToString()) : "";
                    dr[32] = bill.CompanyAccountName;
                    dr[33] = bill.CompanyID;
                    dr[34] = bill.CompanyAccount;
                    dr[35] = bill.CompanyBankCode;
                    dr[36] = bill.CompanyBranchCode;
                    dr[37] = bill.BlRemark;
                    dr[38] = bill.Remark;
                    dr[39] = bill.EmpName;
                    dr[40] = bill.CreateId;
                    dt.Rows.Add(dr);

                }

                

                Workbook workbook = new HSSFWorkbook();

                // 新增試算表。
                Sheet sheet = workbook.CreateSheet("單據查詢列表");

                Cell c = null;
                int rowCount = 0;
                int rowIndex = 0;
                foreach (DataRow row in dt.Rows)
                {
                    Row dataRow = sheet.CreateRow(rowIndex);
                    foreach (DataColumn column in dt.Columns)
                    {
                        c = dataRow.CreateCell(column.Ordinal);
                        c.SetCellValue(row[column].ToString());
                    }
                    rowIndex++;
                    rowCount++;
                }
                //adjust column width
                for (int i = 0; i < sheet.GetRow(sheet.FirstRowNum).LastCellNum; i++)
                {
                    sheet.AutoSizeColumn(i);
                    int width = sheet.GetColumnWidth(i);
                    sheet.SetColumnWidth(i, width + 500); //personal fu.
                }

                HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment; filename={0}", HttpUtility.UrlEncode("BsbillInfoList.xls")));
                workbook.Write(HttpContext.Current.Response.OutputStream);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), string.Empty, "沒有資料可供匯出!", true);
            }
        }

        //匯出未回單據資料列表
        public void ExportUnReceiptReceivedInfo(List<UnConfirmBalanceSheet> bsInfo)
        {

            DataTable dt = new DataTable();
            DataRow drHeader = dt.NewRow();
            int counter = 0;

            string[] headers = new string[] { "單據狀態", "對帳單月份", "檔號/PID", "檔名/商品名稱", "分店", 
                "對帳單總額", "單據未回總額", "開立方式", "出帳方式", "帳務聯絡人", "帳務連絡電話", "帳務連絡Email", "所屬業務" };

            foreach (string s in headers)
            {
                DataColumn dc = new DataColumn(s);
                dt.Columns.Add(dc);
                drHeader[counter] = dc.ColumnName;
                counter++;
            }

            dt.Rows.Add(drHeader);

            if (bsInfo != null)
            {
                foreach (var bs in bsInfo)
                {
                    DataRow dr = dt.NewRow();
                    dr[0] = "未建檔";
                    dr[1] = GetBalanceSheetDesc(string.Format("{0}/{1}", bs.Year.Value, bs.Month.Value), bs.IntervalEnd, bs.GenerationFrequency);
                    dr[2] = bs.UniqueId;
                    dr[3] = bs.Name;
                    dr[4] = bs.StoreName;
                    dr[5] = bs.BalanceSheetTotal;
                    dr[6] = bs.ResidualBalanceSheetTotal;
                    dr[7] = GetTheReceiptType(bs.VendorReceiptType, (!string.IsNullOrEmpty(bs.Message)) ? bs.Message : string.Empty);
                    dr[8] = GetRemittanceType(bs.RemittanceType);
                    dr[9] = bs.AccountantName;
                    dr[10] = bs.AccountantTel;
                    dr[11] = bs.AccountantEmail;
                    dr[12] = bs.EmpName;
                    dt.Rows.Add(dr);
                }

                Workbook workbook = new HSSFWorkbook();

                // 新增試算表。
                Sheet sheet = workbook.CreateSheet("未回單據查詢列表");

                Cell c = null;
                int rowCount = 0;
                int rowIndex = 0;
                foreach (DataRow row in dt.Rows)
                {
                    Row dataRow = sheet.CreateRow(rowIndex);
                    foreach (DataColumn column in dt.Columns)
                    {
                        c = dataRow.CreateCell(column.Ordinal);
                        c.SetCellValue(row[column].ToString());
                    }
                    rowIndex++;
                    rowCount++;
                }

                HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment; filename={0}", HttpUtility.UrlEncode("UnReceiptReceivedInfoList.xls")));
                workbook.Write(HttpContext.Current.Response.OutputStream);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), string.Empty, "沒有資料可供匯出!", true);
            }
        }

        protected void OnQueryUnReceiptReceivedInfo(object sender, EventArgs e)
        {
            if (this.QueryUnReceiptReceivedInfoClicked != null)
                this.QueryUnReceiptReceivedInfoClicked(this, e);
        }

        protected void OnQueryRemainderBill(object sender, EventArgs e)
        {
            if (this.QueryRemainderBillClicked != null)
                this.QueryRemainderBillClicked(this, e);
        }

        protected void OnQueryBalanceSheetBillInfo(object sender, EventArgs e)
        {
            hidManagementMode.Value = ((int)PaymentReceiptManageMode.NotEditing).ToString();
            //新增單據後更新
            if (((HiddenField)UpdateBalanceSheetBill.FindControl("hidBillId")).Value == "")
            {
                txtAddBillId.Text = "";
                btnSellerStore_Click(this, e);
            }
            //修改單據後更新
            else
            {
                btnQueryBillInfo_Click(this, e);
            }
        }

        protected void OnBalanceSheetBillHide(object sender, EventArgs e)
        {
            hidManagementMode.Value = ((int)PaymentReceiptManageMode.NotEditing).ToString();
            //新增單據畫面狀態
            if (((HiddenField)UpdateBalanceSheetBill.FindControl("hidBillId")).Value == "")
            {
                txtAddBillId.Text = "";
                addBill.Visible = false;
            }
            //修改單據畫面狀態
            else
            {
                ((HiddenField)UpdateBalanceSheetBill.FindControl("hidBillId")).Value = "";
                updateBillInfo.Visible = false;
            }
        }

        protected void UpdateHandler(int pageNumber)
        {
            DataEventArgs<int> e = new DataEventArgs<int>(pageNumber);
            if (this.PageChanged != null)
                this.PageChanged(this, e);
        }

        protected int RetrieveDataCount()
        {
            return PageDataCount;
        }

        protected void confirmed_Click(object sender, EventArgs e)
        {
            foreach (RepeaterItem row in repBalanceSheet.Items)
            {
                if (row.ItemType == ListItemType.Item || row.ItemType == ListItemType.AlternatingItem)
                {
                    CheckBox cb = (CheckBox)row.FindControl("chkConfirmed");
                    Label sheetId = (Label)row.FindControl("sid");//ID
                    Label uniqueId = (Label)row.FindControl("uniqueId");//檔號/PID

                    if (cb.Checked)
                    {
                        this.Presenter.GetDataConfirmed(sheetId.Text, uniqueId.Text);
                        break;
                    }
                }
            }
        }

    }
}
﻿using LunchKingSite.BizLogic.Jobs;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Configuration;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Component;
using System;
using System.Collections.Specialized;
using System.Linq;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using LunchKingSite.BizLogic.Facade;


namespace LunchKingSite.Web.ControlRoom.Verification
{
    public partial class WeeklyPayReport : BasePage
    {
        public ISysConfProvider _config = ProviderFactory.Instance().GetConfig();

        #region Property

        private IOrderProvider op;
        private IPponProvider pp;
        private ISysConfProvider sp;

        protected struct SearchColumn
        {
            public const int Bid = 0;
            public const int DealId = 1;
        }

        #endregion Property

        public WeeklyPayReport()
        {
            op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
            pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            sp = ProviderFactory.Instance().GetConfig();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                SetDdl();
            }
        }

        protected void SetDdl()
        {
            ddlSelect.Items.Add(new ListItem("Bid", Convert.ToString(SearchColumn.Bid)));
            ddlSelect.Items.Add(new ListItem("Deal檔號", Convert.ToString(SearchColumn.DealId)));
        }

        protected void SearchReport(object sender, EventArgs e)
        {
            lab_Message.Text = string.Empty;
            DateTime d_start, d_end;
            if (DateTime.TryParse(tbx_DateStart.Text, out d_start) && DateTime.TryParse(tbx_DateEnd.Text, out d_end))
            {
                ViewWeeklyPayReportCollection reports = op.GetViewWeeklyPayReportByDate(d_start, d_end.AddDays(1));
                if (reports.Count == 0)
                {
                    lab_Message.Text = @"查無資料";
                }
                gv_Report.DataSource = reports;
                gv_Report.DataBind();
            }
            else
            {
                lab_Message.Text = @"日期格式錯誤!!";
            }
        }

        protected void SearchBidReport(object sender, EventArgs e)
        {
            lab_Message.Text = string.Empty;
            ViewWeeklyPayReportCollection reports = new ViewWeeklyPayReportCollection();
            switch (Convert.ToInt32(ddlSelect.SelectedValue))
            {
                case SearchColumn.Bid:
                    Guid bid;
                    if (Guid.TryParse(tbx_Bid.Text, out bid))
                    {
                        reports = op.GetViewWeeklyPayReportByBid(bid);
                        if (reports.Count == 0)
                        {
                            lab_Message.Text = @"查無資料";
                        }
                    }
                    else
                    {
                        lab_Message.Text = @"Bid格式錯誤!!";
                    }
                    break;

                case SearchColumn.DealId:
                    reports = op.GetViewWeeklyPayReportByUniqueId(tbx_Bid.Text);
                    if (reports.Count == 0)
                    {
                        lab_Message.Text = @"查無資料";
                    }
                    break;
            }
            gv_Report.DataSource = reports;
            gv_Report.DataBind();
        }

        protected string GetAtmResult(Object dt, Object code)
        {
            string finalResult = "尚無資料";
            const string resultFail = "匯款失敗";

            if (dt == null || code == null)
            {
                return finalResult;
            }
            DateTime theDt = (DateTime)dt;
            string strCode = code.ToString();

            if (!strCode.IsEmpty())
            {
                switch (strCode)
                {
                    case AtmAchProps.AtmAchResultCode.Success:
                        finalResult = theDt.ToString("yyyy/MM/dd HH:mm:ss") + " " + code + " " + AtmAchProps.AtmAchResultDescription.Success;
                        break;

                    case AtmAchProps.AtmAchResultCode.DepositNotEnough:
                        finalResult = theDt.ToString("yyyy/MM/dd HH:mm:ss") + " " + resultFail + " " + code + AtmAchProps.AtmAchResultDescription.DepositNotEnough;
                        break;

                    case AtmAchProps.AtmAchResultCode.UnauthorizedUser:
                        finalResult = theDt.ToString("yyyy/MM/dd HH:mm:ss") + " " + resultFail + " " + code + AtmAchProps.AtmAchResultDescription.UnauthorizedUser;
                        break;

                    case AtmAchProps.AtmAchResultCode.TerminatedUser:
                        finalResult = theDt.ToString("yyyy/MM/dd HH:mm:ss") + " " + resultFail + " " + code + AtmAchProps.AtmAchResultDescription.TerminatedUser;
                        break;

                    case AtmAchProps.AtmAchResultCode.NoSuchAccount:
                        finalResult = theDt.ToString("yyyy/MM/dd HH:mm:ss") + " " + resultFail + " " + code + AtmAchProps.AtmAchResultDescription.NoSuchAccount;
                        break;

                    case AtmAchProps.AtmAchResultCode.WrongVATNumber:
                        finalResult = theDt.ToString("yyyy/MM/dd HH:mm:ss") + " " + resultFail + " " + code + AtmAchProps.AtmAchResultDescription.WrongVATNumber;
                        break;

                    case AtmAchProps.AtmAchResultCode.WrongUserNumber:
                        finalResult = theDt.ToString("yyyy/MM/dd HH:mm:ss") + " " + resultFail + " " + code + AtmAchProps.AtmAchResultDescription.WrongUserNumber;
                        break;

                    case AtmAchProps.AtmAchResultCode.UserNumberNotMatch:
                        finalResult = theDt.ToString("yyyy/MM/dd HH:mm:ss") + " " + resultFail + " " + code + AtmAchProps.AtmAchResultDescription.UserNumberNotMatch;
                        break;

                    case AtmAchProps.AtmAchResultCode.AccountCleared:
                        finalResult = theDt.ToString("yyyy/MM/dd HH:mm:ss") + " " + resultFail + " " + code + AtmAchProps.AtmAchResultDescription.AccountCleared;
                        break;

                    case AtmAchProps.AtmAchResultCode.SilentAccount:
                        finalResult = theDt.ToString("yyyy/MM/dd HH:mm:ss") + " " + resultFail + " " + code + AtmAchProps.AtmAchResultDescription.SilentAccount;
                        break;

                    case AtmAchProps.AtmAchResultCode.FrozenAccount:
                        finalResult = theDt.ToString("yyyy/MM/dd HH:mm:ss") + " " + resultFail + " " + code + AtmAchProps.AtmAchResultDescription.FrozenAccount;
                        break;

                    case AtmAchProps.AtmAchResultCode.CompulsoryEnforcement:
                        finalResult = theDt.ToString("yyyy/MM/dd HH:mm:ss") + " " + resultFail + " " + code + AtmAchProps.AtmAchResultDescription.CompulsoryEnforcement;
                        break;

                    case AtmAchProps.AtmAchResultCode.UntradeOrWrongImportData:
                        finalResult = theDt.ToString("yyyy/MM/dd HH:mm:ss") + " " + resultFail + " " + code + AtmAchProps.AtmAchResultDescription.UntradeOrWrongImportData;
                        break;

                    case AtmAchProps.AtmAchResultCode.Others:
                        finalResult = theDt.ToString("yyyy/MM/dd HH:mm:ss") + " " + resultFail + " " + code + AtmAchProps.AtmAchResultDescription.Others;
                        break;
                }
            }
            return finalResult;
        }

        protected void GetInnerReport(object sender, GridViewCommandEventArgs e)
        {
            lab_Message.Text = string.Empty;
            var reports = new ViewWeeklyPayReportCollection();
            DateTime now = DateTime.Now;
            DateTime target;
            int id;
            Guid rid;
            Guid gid;
            switch (e.CommandName)
            {
                case "ReportGuid":
                    if (Guid.TryParse(e.CommandArgument.ToString(), out rid))
                    {
                        reports = op.GetViewWeeklyPayReportByRid(rid);
                        if (reports.Count == 0)
                        {
                            lab_Message.Text = @"查無資料";
                        }
                        gv_Report.DataSource = reports;
                        gv_Report.DataBind();
                    }
                    else
                    {
                        lab_Message.Text = @"Bid格式錯誤!!";
                    }
                    break;

                case "ReportMainId":
                    if (Guid.TryParse(e.CommandArgument.ToString(), out rid))
                    {
                        reports = op.GetViewWeeklyPayReportByBid(rid);
                        if (reports.Count == 0)
                        {
                            lab_Message.Text = @"查無資料";
                        }
                        gv_Report.DataSource = reports;
                        gv_Report.DataBind();
                    }
                    else
                    {
                        lab_Message.Text = @"Bid格式錯誤!!";
                    }
                    break;

                case "PrintBid":
                    if (int.TryParse(e.CommandArgument.ToString(), out id))
                    {
                        if (DateTime.TryParse(tbOS.Text, out target))
                        {
                            target = target.AddHours(17);
                            if (target.DayOfWeek == DayOfWeek.Sunday || target.DayOfWeek == DayOfWeek.Saturday)
                            {
                                lab_Message.Text = @"退款日不得為假日";
                            }
                            else if ((target - now).Days < 2)
                            {
                                lab_Message.Text = @"退款日需比發動日大兩天，" + (target - now).Days + @"天";
                            }
                            else if (target.DayOfWeek == DayOfWeek.Monday && (target - now).Days < 4)
                            {
                                lab_Message.Text = @"處理日不得為假日";
                            }
                            else
                            {
                                reports = op.GetViewWeeklyPayReportById(id);

                                //請款凍結檔次不產出ACH匯款檔案 TODO:網銀匯款檔須記得加上凍結請款檢查
                                var dealFreezeInfos = pp.DealAccountingGet(reports.Select(x=>x.BusinessHourGuid))
                                    .ToDictionary(x => x.BusinessHourGuid,
                                                       x => Helper.IsFlagSet(x.Flag, (int)AccountingFlag.Freeze));

                                IEnumerable<ViewWeeklyPayReport> _reports = reports.Where(x => dealFreezeInfos.ContainsKey(x.BusinessHourGuid) && !dealFreezeInfos[x.BusinessHourGuid]);

                                if (_reports.Count() == 0)
                                {
                                    lab_Message.Text = @"查無資料";
                                }
                                else if (_reports.Count(x => string.IsNullOrEmpty(x.AccountNo) && x.Paytocompany == 1) > 0)
                                {
                                    lab_Message.Text = @"有未填寫匯款帳戶的檔次Bid=";
                                    foreach (var item in _reports.Where(x => string.IsNullOrEmpty(x.AccountNo) && x.Paytocompany == 1))
                                    {
                                        lab_Message.Text += (@"," + item.BusinessHourGuid);
                                    }
                                }
                                else if (_reports.Count(x => x.StoreGuid != null && string.IsNullOrEmpty(x.CompanyAccount) && x.Paytocompany == 0) > 0)
                                {
                                    lab_Message.Text = @"有未填寫匯款帳戶的分店Bid=";
                                    foreach (var item in _reports.Where(x => x.StoreGuid != null && string.IsNullOrEmpty(x.CompanyAccount) && x.Paytocompany == 0))
                                    {
                                        lab_Message.Text += (@"," + item.BusinessHourGuid);
                                    }
                                }
                                else if (_reports.Count(x => x.StoreGuid == null && x.Paytocompany == 0) > 0)
                                {
                                    lab_Message.Text = @"有匯款設定錯誤的檔次Bid=";
                                    foreach (var item in _reports.Where(x => x.StoreGuid == null && x.Paytocompany == 0))
                                    {
                                        lab_Message.Text += (@"," + item.BusinessHourGuid);
                                    }
                                }
                                else
                                {
                                    DateTime t = new DateTime();
                                    if (target.DayOfWeek == DayOfWeek.Monday && (target - now).Days > 3)
                                    {
                                        t = target.AddDays(-3);
                                    }
                                    else
                                    {
                                        t = target.AddDays(-1);
                                    }
                                    AchP1Body(_reports, t);
                                }
                            }
                        }
                        else
                        {
                            lab_Message.Text = @"匯款時間格式錯誤";
                        }
                    }
                    else
                    {
                        lab_Message.Text = @"Id格式錯誤!!";
                    }
                    break;
                case "PrintBKBid":
                    if (int.TryParse(e.CommandArgument.ToString(), out id))
                    {
                        if (DateTime.TryParse(tbOS.Text, out target))
                        {
                            target = target.AddHours(17);
                            if (target.DayOfWeek == DayOfWeek.Sunday || target.DayOfWeek == DayOfWeek.Saturday)
                            {
                                lab_Message.Text = @"退款日不得為假日";
                            }
                            else if (target.DayOfWeek == DayOfWeek.Monday && (target - now).Days < 4)
                            {
                                lab_Message.Text = @"處理日不得為假日";
                            }
                            else
                            {
                                reports = op.GetViewWeeklyPayReportById(id);

                                //請款凍結檔次不產出ACH匯款檔案 TODO:網銀匯款檔須記得加上凍結請款檢查
                                var dealFreezeInfos = pp.DealAccountingGet(reports.Select(x => x.BusinessHourGuid))
                                    .ToDictionary(x => x.BusinessHourGuid,
                                                       x => Helper.IsFlagSet(x.Flag, (int)AccountingFlag.Freeze));
                               
                                IEnumerable<ViewWeeklyPayReport> _reports = reports.Where(x => dealFreezeInfos.ContainsKey(x.BusinessHourGuid) && !dealFreezeInfos[x.BusinessHourGuid]);


                                if (_reports.Count() == 0)
                                {
                                    lab_Message.Text = @"查無資料";
                                }
                                else if (_reports.Count(x => string.IsNullOrEmpty(x.AccountNo) && x.Paytocompany == 1) > 0)
                                {
                                    lab_Message.Text = @"有未填寫匯款帳戶的檔次Bid=";
                                    foreach (var item in _reports.Where(x => string.IsNullOrEmpty(x.AccountNo) && x.Paytocompany == 1))
                                    {
                                        lab_Message.Text += (@"," + item.BusinessHourGuid);
                                    }
                                }
                                else if (_reports.Count(x => x.StoreGuid != null && string.IsNullOrEmpty(x.CompanyAccount) && x.Paytocompany == 0) > 0)
                                {
                                    lab_Message.Text = @"有未填寫匯款帳戶的分店store_guid=";
                                    foreach (var item in _reports.Where(x => x.StoreGuid != null && string.IsNullOrEmpty(x.CompanyAccount) && x.Paytocompany == 0))
                                    {
                                        lab_Message.Text += (@"," + item.StoreGuid);
                                    }
                                }
                                else if (_reports.Count(x => x.StoreGuid == null && x.Paytocompany == 0) > 0)
                                {
                                    lab_Message.Text = @"有匯款設定錯誤的檔次Bid=";
                                    foreach (var item in _reports.Where(x => x.StoreGuid == null && x.Paytocompany == 0))
                                    {
                                        lab_Message.Text += (@"," + item.StoreGuid);
                                    }
                                }
                                else
                                {
                                    DateTime t = new DateTime();
                                    if (target.DayOfWeek == DayOfWeek.Monday && (target - now).Days > 3)
                                    {
                                        t = target.AddDays(-3);
                                    }
                                    else
                                    {
                                        t = target.AddDays(-1);
                                    }
                                    ManualP1Body(_reports, t);
                                }
                            }
                        }
                        else
                        {
                            lab_Message.Text = @"匯款時間格式錯誤";
                        }
                    }
                    else
                    {
                        lab_Message.Text = @"Id格式錯誤!!";
                    }
                    break;
                case "PrintRid":
                    if (Guid.TryParse(e.CommandArgument.ToString(), out gid))
                    {
                        if (DateTime.TryParse(tbOS.Text, out target))
                        {
                            target = target.AddHours(17);
                            if (target.DayOfWeek == DayOfWeek.Sunday || target.DayOfWeek == DayOfWeek.Saturday)
                            {
                                lab_Message.Text = @"退款日不得為假日";
                            }
                            else if ((target - now).Days < 2)
                            {
                                lab_Message.Text = @"退款日需比發動日大兩天，" + (target - now).Days + @"天";
                            }
                            else if (target.DayOfWeek == DayOfWeek.Monday && (target - now).Days < 4)
                            {
                                lab_Message.Text = @"處理日不得為假日";
                            }
                            else
                            {
                                
                                
                                reports = op.GetViewWeeklyPayReportByRid(gid);

                                //請款凍結檔次不產出ACH匯款檔案 TODO:網銀匯款檔須記得加上凍結請款檢查                                
                                var dealFreezeInfos = pp.DealAccountingGet(reports.Select(x=>x.BusinessHourGuid))
                                    .ToDictionary(x => x.BusinessHourGuid,
                                                       x => Helper.IsFlagSet(x.Flag, (int)AccountingFlag.Freeze));
                                    
                                IEnumerable<ViewWeeklyPayReport> _reports = reports.Where(x => dealFreezeInfos.ContainsKey(x.BusinessHourGuid) && !dealFreezeInfos[x.BusinessHourGuid]);


                                if (_reports.Count() == 0)
                                {
                                    lab_Message.Text = @"查無資料";
                                }
                                else if (_reports.Count(x => string.IsNullOrEmpty(x.AccountNo) && x.Paytocompany == 1) > 0)
                                {
                                    lab_Message.Text = @"有未填寫匯款帳戶的檔次Bid=";
                                    foreach (var item in _reports.Where(x => string.IsNullOrEmpty(x.AccountNo) && x.Paytocompany == 1))
                                    {
                                        lab_Message.Text += (@"," + item.BusinessHourGuid);
                                    }
                                }
                                else if (_reports.Count(x => x.StoreGuid != null && string.IsNullOrEmpty(x.CompanyAccount) && x.Paytocompany == 0) > 0)
                                {
                                    lab_Message.Text = @"有未填寫匯款帳戶的分店store_guid=";
                                    foreach (var item in _reports.Where(x => x.StoreGuid != null && string.IsNullOrEmpty(x.CompanyAccount) && x.Paytocompany == 0))
                                    {
                                        lab_Message.Text += (@"," + item.StoreGuid);
                                    }
                                }
                                else if (_reports.Count(x => x.StoreGuid == null && x.Paytocompany == 0) > 0)
                                {
                                    lab_Message.Text = @"有匯款設定錯誤的檔次Bid=";
                                    foreach (var item in _reports.Where(x => x.StoreGuid == null && x.Paytocompany == 0))
                                    {
                                        lab_Message.Text += (@"," + item.BusinessHourGuid);
                                    }
                                }
                                else
                                {
                                    DateTime t = new DateTime();
                                    if (target.DayOfWeek == DayOfWeek.Monday && (target - now).Days > 3)
                                    {
                                        t = target.AddDays(-3);
                                    }
                                    else
                                    {
                                        t = target.AddDays(-1);
                                    }
                                    AchP1Body(_reports, t);
                                }
                            }
                        }
                        else
                        {
                            lab_Message.Text = @"匯款時間格式錯誤";
                        }
                    }
                    else
                    {
                        lab_Message.Text = @"Rid格式錯誤!!";
                    }

                    break;
                case "PrintBKRid":
                    if (Guid.TryParse(e.CommandArgument.ToString(), out gid))
                    {
                        if (DateTime.TryParse(tbOS.Text, out target))
                        {
                            target = target.AddHours(17);
                            if (target.DayOfWeek == DayOfWeek.Sunday || target.DayOfWeek == DayOfWeek.Saturday)
                            {
                                lab_Message.Text = @"退款日不得為假日";
                            }
                            else if (target.DayOfWeek == DayOfWeek.Monday && (target - now).Days < 4)
                            {
                                lab_Message.Text = @"處理日不得為假日";
                            }
                            else
                            {
                                reports = op.GetViewWeeklyPayReportByRid(gid);

                                //請款凍結檔次不產出ACH匯款檔案 TODO:網銀匯款檔須記得加上凍結請款檢查
                                var dealFreezeInfos = pp.DealAccountingGet(reports.Select(x=>x.BusinessHourGuid))
                                    .ToDictionary(x => x.BusinessHourGuid,
                                                       x => Helper.IsFlagSet(x.Flag, (int)AccountingFlag.Freeze));
                                
                                IEnumerable<ViewWeeklyPayReport> _reports = reports.Where(x => dealFreezeInfos.ContainsKey(x.BusinessHourGuid) && !dealFreezeInfos[x.BusinessHourGuid]);
                                
                                if (_reports.Count() == 0)
                                {
                                    lab_Message.Text = @"查無資料";
                                }
                                else if (_reports.Count(x => string.IsNullOrEmpty(x.AccountNo) && x.Paytocompany == 1) > 0)
                                {
                                    lab_Message.Text = @"有未填寫匯款帳戶的檔次Bid=";
                                    foreach (var item in _reports.Where(x => string.IsNullOrEmpty(x.AccountNo) && x.Paytocompany == 1))
                                    {
                                        lab_Message.Text += (@"," + item.BusinessHourGuid);
                                    }
                                }
                                else if (_reports.Count(x => x.StoreGuid != null && string.IsNullOrEmpty(x.CompanyAccount) && x.Paytocompany == 0) > 0)
                                {
                                    lab_Message.Text = @"有未填寫匯款帳戶的分店store_guid=";
                                    foreach (var item in _reports.Where(x => x.StoreGuid != null && string.IsNullOrEmpty(x.CompanyAccount) && x.Paytocompany == 0))
                                    {
                                        lab_Message.Text += (@"," + item.StoreGuid);
                                    }
                                }
                                else if (_reports.Count(x => x.StoreGuid == null && x.Paytocompany == 0) > 0)
                                {
                                    lab_Message.Text = @"有匯款設定錯誤的檔次Bid=";
                                    foreach (var item in _reports.Where(x => x.StoreGuid == null && x.Paytocompany == 0))
                                    {
                                        lab_Message.Text += (@"," + item.BusinessHourGuid);
                                    }
                                }
                                else
                                {
                                    DateTime t = new DateTime();
                                    if (target.DayOfWeek == DayOfWeek.Monday && (target - now).Days > 3)
                                    {
                                        t = target.AddDays(-3);
                                    }
                                    else
                                    {
                                        t = target.AddDays(-1);
                                    }
                                    ManualP1Body(_reports, t);
                                }
                            }
                        }
                        else
                        {
                            lab_Message.Text = @"匯款時間格式錯誤";
                        }
                    }
                    else
                    {
                        lab_Message.Text = @"Rid格式錯誤!!";
                    }

                    break;
            }
        }

        protected void GenPayByBillAchReport(object sender, EventArgs e)
        {
            var js = new JobSetting(typeof(GenAchReport), null);
            js.Parameters = new NameValueCollection
            {
                { "mode", "PayByBill" },
            };
            var job = new GenAchReport();
            job.Execute(js);
        }

        #region AchReport

        private string AchP1Head(DateTime time)
        {
            return "BOFACHP01" + (DateTime.Now.Year - 1911).ToString().PadLeft(4, '0')  //13
                   + time.Month.ToString().PadLeft(2, '0')                              //2
                   + time.Day.ToString().PadLeft(2, '0')                                //2
                   + time.Hour.ToString().PadLeft(2, '0')                               //2
                   + time.Minute.ToString().PadLeft(2, '0')                             //2
                   + time.Second.ToString().PadLeft(2, '0')                             //2
                   + string.Format("{0}{1}", sp.SendUnitNo, sp.ReceiveUnitNo)           //14
                   + new string(' ', 123) + "\r\n";
        }

        private string AchP1Foot(DateTime time, int count, int total)
        {
            return "EOFACHP01" + (DateTime.Now.Year - 1911).ToString().PadLeft(4, '0')  //13
                   + time.Month.ToString().PadLeft(2, '0')                              //2
                   + time.Day.ToString().PadLeft(2, '0')                                //2
                   + string.Format("{0}{1}", sp.SendUnitNo, sp.ReceiveUnitNo)           //14
                   + count.ToString().PadLeft(8, '0')                                   //8
                   + total.ToString().PadLeft(16, '0')                                  //16
                   + new string(' ', 105) + "\r\n";
        }

        private void AchP1Body(IEnumerable<ViewWeeklyPayReport> reports, DateTime t)
        {
            string result = string.Empty;
            int pageSize = sp.WeeklyPayPageSize;
            int p1Count = 0;
            int p1Total = 0;
            int rowCount = 0;
            string tempstr = "";
            //匯出報表須先排序
            var sortReprots = reports.OrderBy(x => x.UniqueId)
                                     .ThenBy(x => x.Id);
          
            Dictionary<string, IEnumerable<ViewWeeklyPayReport>> data = new Dictionary<string, IEnumerable<ViewWeeklyPayReport>>();
            data.Add("(自行)ACHP01_" + string.Format("{0}_{1}_01.txt", DateTime.Now.ToString("yyyyMMdd"), sp.TriggerCompanyId), sortReprots.Where(x => (x.BankNo == sp.TriggerBankAndBranchNo.Substring(0, 3) && x.Paytocompany == (int)DealAccountingPayType.PayToCompany) ||
                                                        (x.CompanyBankCode == sp.TriggerBankAndBranchNo.Substring(0, 3) && x.Paytocompany != (int)DealAccountingPayType.PayToCompany)));
            data.Add("(他行)ACHP01_" + string.Format("{0}_{1}_01.txt", DateTime.Now.ToString("yyyyMMdd"), sp.TriggerCompanyId), sortReprots.Where(x => (x.BankNo != sp.TriggerBankAndBranchNo.Substring(0, 3) && x.Paytocompany == (int)DealAccountingPayType.PayToCompany) ||
                                                        (x.CompanyBankCode != sp.TriggerBankAndBranchNo.Substring(0, 3) && x.Paytocompany != (int)DealAccountingPayType.PayToCompany)));

            Dictionary<string, string> d2 = new Dictionary<string, string>();
            foreach (KeyValuePair<string, IEnumerable<ViewWeeklyPayReport>> b_item in data)
            {
                foreach (ViewWeeklyPayReport item in b_item.Value)
                {
                    rowCount++;

                    if ((item.TransferAmount ?? item.TotalSum) > 0)
                    {
                        if (item.Paytocompany == 1)
                        {
                            if (tempstr.IndexOf(item.BusinessHourGuid.ToString()) >= 0)
                            {
                                if (b_item.Value.ToList().Count == rowCount)
                                {
                                    result += AchP1Foot(t, p1Count, p1Total) + "\r\n\r\n";
                                    if (b_item.Value.ToList().Count > rowCount)
                                    {
                                        p1Total = 0;
                                        p1Count = 0;
                                        result += "\r\n\r\n";
                                    }
                                }
                                continue;
                            }

                            if (p1Count == 0)
                            {
                                result += AchP1Head(t);
                            }

                            p1Count++;
                            if (item.StoreGuid == null)
                            {
                                result += "NSC405" + (p1Count).ToString().PadLeft(6, '0') + string.Format("{0}{1}", sp.TriggerBankAndBranchNo, sp.TriggerAccountNo) +
                                                  (item.BankNo + item.BranchNo).PadLeft(7, '0') + item.AccountNo.PadLeft(14, '0') +
                                                  ((int)(item.TransferAmount ?? item.TotalSum)).ToString().PadLeft(10, '0') + string.Format("00B{0}  ", sp.TriggerCompanyId) +
                                                 item.AccountId.ToUpper().PadRight(10, ' ') + "      00000000000000 " +
                                                  item.Id.ToString().PadRight(20, ' ') +
                                                  item.UniqueId.ToString().PadRight(20, ' ') +
                                                  string.Empty.PadRight(12, ' ') + "\r\n";

                                p1Total += (int)(item.TransferAmount ?? item.TotalSum);
                            }
                            else
                            {   //有分店但是匯給本店
                                tempstr += item.BusinessHourGuid.ToString();
                                int total = op.GetViewWeeklyPayReportSumByBidRid(item.BusinessHourGuid, item.ReportGuid);
                                result += "NSC405" + (p1Count).ToString().PadLeft(6, '0') + string.Format("{0}{1}", sp.TriggerBankAndBranchNo, sp.TriggerAccountNo) +
                                          (item.BankNo + item.BranchNo).PadLeft(7, '0') + item.AccountNo.PadLeft(14, '0') +
                                          (total).ToString().PadLeft(10, '0') + string.Format("00B{0}  ", sp.TriggerCompanyId) +
                                         item.AccountId.ToUpper().PadRight(10, ' ') + "      00000000000000 " +
                                          item.Id.ToString().PadRight(20, ' ') +
                                          item.UniqueId.ToString().PadRight(20, ' ') +
                                          string.Empty.PadRight(12, ' ') + "\r\n";

                                p1Total += total;
                            }
                        }
                        else
                        {
                            if (p1Count == 0)
                            {
                                result += AchP1Head(t);
                            }

                            p1Count++;
                            result += "NSC405" + (p1Count).ToString().PadLeft(6, '0') + string.Format("{0}{1}", sp.TriggerBankAndBranchNo, sp.TriggerAccountNo) +
                                              (item.CompanyBankCode + item.CompanyBranchCode).PadLeft(7, '0') + item.CompanyAccount.PadLeft(14, '0') +
                                              ((int)(item.TransferAmount ?? item.TotalSum)).ToString().PadLeft(10, '0') + string.Format("00B{0}  ", sp.TriggerCompanyId) +
                                              item.CompanyID.ToUpper().PadRight(10, ' ') + "      00000000000000 " +
                                              item.Id.ToString().PadRight(20, ' ') +
                                              item.UniqueId.ToString().PadRight(20, ' ') +
                                              string.Empty.PadRight(12, ' ') + "\r\n";

                            p1Total += (int)(item.TransferAmount ?? item.TotalSum);
                        }
                    }

                    if ((p1Count % pageSize == -1 || b_item.Value.ToList().Count == rowCount) && p1Count > 0)
                    {
                        result += AchP1Foot(t, p1Count, p1Total) + "\r\n\r\n";
                        if (b_item.Value.ToList().Count > rowCount)
                        {
                            p1Total = 0;
                            p1Count = 0;
                            result += "\r\n\r\n";
                        }
                    }
                }
                d2.Add(b_item.Key, result);
                result = string.Empty;
                rowCount = 0;
                p1Total = 0;
                p1Count = 0;
            }
            byte[] zipFile = FileZipUtility.ZipFile(d2);
            if (zipFile != null && zipFile.Length > 0)
            {
                Context.Response.Clear();
                Context.Response.ContentType = "application/zip";
                Context.Response.AddHeader("Content-Disposition", "attachment; filename=ACHP01_" + string.Format("{0}_{1}_01.zip", DateTime.Now.ToString("yyyyMMdd"), sp.TriggerCompanyId));
                Context.Response.BufferOutput = false;
                Response.BinaryWrite(zipFile);
                Response.End();
            }
        }


        private void ManualP1Body(IEnumerable<ViewWeeklyPayReport> reports, DateTime t)
        {
            string result = string.Empty;
            var target = DateTime.Parse(tbOS.Text);

            //匯出報表須先排序
            var sortReprots = reports.OrderBy(x => x.Id);

            var data = new Dictionary<string, IEnumerable<ViewWeeklyPayReport>>();
            data.Add("(自行)ManualP01_" + string.Format("{0}_{1}_01.txt", DateTime.Now.ToString("yyyyMMdd"), sp.TriggerCompanyId), sortReprots.Where(x => (x.BankNo == sp.TriggerBankAndBranchNo.Substring(0, 3) && x.Paytocompany == (int)DealAccountingPayType.PayToCompany) ||
                                                        (x.CompanyBankCode == sp.TriggerBankAndBranchNo.Substring(0, 3) && x.Paytocompany != (int)DealAccountingPayType.PayToCompany)));
            data.Add("(他行)ManualP01_" + string.Format("{0}_{1}_01.txt", DateTime.Now.ToString("yyyyMMdd"), sp.TriggerCompanyId), sortReprots.Where(x => (x.BankNo != sp.TriggerBankAndBranchNo.Substring(0, 3) && x.Paytocompany == (int)DealAccountingPayType.PayToCompany) ||
                                                        (x.CompanyBankCode != sp.TriggerBankAndBranchNo.Substring(0, 3) && x.Paytocompany != (int)DealAccountingPayType.PayToCompany)));

            var d2 = new Dictionary<string, string>();
            foreach (var bItem in data)
            {
                foreach (ViewWeeklyPayReport item in bItem.Value)
                {
                    var identityId = "58";
                    var reportId = item.Id.ToString();
                    if (reportId.Length > 7)
                    {
                        reportId = reportId.Substring(reportId.Length - 7, 7);
                    }

                    if ((item.TransferAmount ?? item.TotalSum) > 0)
                    {
                        if (item.Paytocompany == 1)
                        {
                            if (item.StoreGuid == null)
                            {
                                //檢查是否為身分證
                                string companyId = item.AccountId;
                                if (string.IsNullOrEmpty(companyId))
                                {
                                    companyId = item.SignCompanyID;
                                }
                                string validateId = RegExRules.PersonalIdCheck(companyId);
                                if (validateId == "")
                                {
                                    identityId = "174";
                                }

                                result += reportId.PadLeft(7, '0') + 
                                        target.ToString("yyyyMMdd") +
                                        ((int)(item.TransferAmount ?? item.TotalSum)).ToString().PadLeft(16, '0') + "00" +
                                        sp.TriggerAccountNo.PadRight(17, ' ') +
                                        chtPadRight("康太數位整合股份有限公司",60) +
                                        item.AccountNo.PadRight(17, ' ') +
                                        chtPadRight(item.AccountName, 60) +
                                        sp.TaiShinPaymentBankAndBranchNo + 
                                        item.BankNo.PadLeft(3,'0').Substring(0,3) + 
                                        item.BranchNo.PadLeft(4,'0').Substring(0,4) +
                                        chtPadRight("", 100) +
                                        companyId.PadRight(17, ' ') +
                                        identityId.PadRight(3,' ') +
                                        sp.TriggerCompanyId.PadRight(17, ' ') +
                                        "58 " + 
                                        "15 " +
                                        item.Id.ToString().PadLeft(30, ' ') + 
                                        "".PadLeft(35, ' ') + "".PadLeft(25, ' ') + "".PadLeft(25, ' ') + "".PadLeft(35, ' ') + "".PadLeft(25, ' ') + "".PadLeft(25, ' ') + "".PadLeft(50, ' ') + 
                                        "\r\n";
                            }
                            else
                            {   //有分店但是匯給本店
                                int total = op.GetViewWeeklyPayReportSumByBidRid(item.BusinessHourGuid, item.ReportGuid);

                                //檢查是否為身分證
                                string companyId = item.AccountId;
                                if (string.IsNullOrEmpty(companyId))
                                {
                                    companyId = item.SignCompanyID;
                                }
                                string validateId = RegExRules.PersonalIdCheck(companyId);
                                if (validateId == "")
                                {
                                    identityId = "174";
                                }


                                result += reportId.PadLeft(7, '0') +
                                        target.ToString("yyyyMMdd") +
                                        (total).ToString().PadLeft(16, '0') + "00" +
                                        sp.TriggerAccountNo.PadRight(17, ' ') +
                                        chtPadRight("康太數位整合股份有限公司", 60) +
                                        item.AccountNo.PadRight(17, ' ') +
                                        chtPadRight(item.AccountName, 60) +
                                        sp.TaiShinPaymentBankAndBranchNo + 
                                        item.BankNo.PadLeft(3, '0').Substring(0, 3) +
                                        item.BranchNo.PadLeft(4, '0').Substring(0, 4) +
                                        chtPadRight("", 100) +
                                        companyId.PadRight(17, ' ') +
                                        identityId.PadRight(3, ' ') +
                                        sp.TriggerCompanyId.PadRight(17, ' ') +
                                        "58 " +
                                        "15 " +
                                        item.Id.ToString().PadLeft(30, ' ') + 
                                        "".PadLeft(35, ' ') + "".PadLeft(25, ' ') + "".PadLeft(25, ' ') + "".PadLeft(35, ' ') + "".PadLeft(25, ' ') + "".PadLeft(25, ' ') + "".PadLeft(50, ' ') + 
                                        "\r\n";
                            }
                        }
                        else
                        {
                            //檢查是否為身分證
                            string companyId = item.CompanyID;
                            if (string.IsNullOrEmpty(companyId))
                            {
                                companyId = item.SignCompanyID;
                            }
                            string validateID = RegExRules.PersonalIdCheck(companyId);
                            if (validateID == "")
                            {
                                identityId = "174";
                            }

                            result += reportId.PadLeft(7, '0') +
                                        target.ToString("yyyyMMdd") +
                                        ((int)(item.TransferAmount ?? item.TotalSum)).ToString().PadLeft(16, '0') + "00" +
                                        sp.TriggerAccountNo.PadRight(17, ' ') +
                                        chtPadRight("康太數位整合股份有限公司", 60) +
                                        item.CompanyAccount.PadRight(17, ' ') +
                                        chtPadRight(item.CompanyAccountName, 60) +
                                        sp.TaiShinPaymentBankAndBranchNo + 
                                        item.CompanyBankCode.PadLeft(3, '0').Substring(0, 3) +
                                        item.CompanyBranchCode.PadLeft(4, '0').Substring(0, 4) +
                                        chtPadRight("", 100) +
                                        companyId.PadRight(17, ' ') +
                                        identityId.PadRight(3, ' ') +
                                        sp.TriggerCompanyId.PadRight(17, ' ') +
                                        "58 " +
                                        "15 " +
                                        item.Id.ToString().PadLeft(30, ' ') + 
                                        "".PadLeft(35, ' ') + "".PadLeft(25, ' ') + "".PadLeft(25, ' ') + "".PadLeft(35, ' ') + "".PadLeft(25, ' ') + "".PadLeft(25, ' ') + "".PadLeft(50, ' ') + 
                                        "\r\n";
                        }
                    }

                }
                d2.Add(bItem.Key, result);
                result = string.Empty;
            }
            byte[] zipFile = FileZipUtility.ZipFile(d2);
            if (zipFile != null && zipFile.Length > 0)
            {
                Context.Response.Clear();
                Context.Response.ContentType = "application/zip";
                Context.Response.AddHeader("Content-Disposition", "attachment; filename=ManualP01_" + string.Format("{0}_{1}_01.zip", DateTime.Now.ToString("yyyyMMdd"), sp.TriggerCompanyId));
                Context.Response.BufferOutput = false;
                Response.BinaryWrite(zipFile);
                Response.End();
            }
        }

        #endregion AchReport

        private string chtPadRight(string org, int sLen)
        {
            int totalLength = 0;
            string strWords = "";
            for (int i = 0; i < org.Length; i++)
            {
                string s = org.Substring(i, 1);
                int currentLength = 0;
                if (Convert.ToInt32(s[0]) > 128 || Convert.ToInt32(s[0]) < 0)
                {
                    currentLength = 2;
                }
                else {
                    currentLength = 1;
                }
                strWords += s[0];
                totalLength += currentLength;
                if (totalLength >= sLen)
                {
                    break;
                }
            }
            if (totalLength >= sLen)
            {
                return strWords;
            }
            return org + " ".PadRight(sLen - totalLength);
        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.WebLib.Views;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using System.Web.Security;
using System.Collections;
using System.Transactions;
using System.Data;

namespace LunchKingSite.Web.ControlRoom.Verification
{
    public partial class VerificationInventory : RolePage, IVerificationInventoryView
    {
        IMemberProvider memProv;
        IOrderProvider odrProv;

        #region EventHandler
        public event EventHandler<DataEventArgs<int>> GetInventoryCount;
        public event EventHandler<DataEventArgs<int>> PageChanged;
        public event EventHandler<EventArgs> OkButtonClick;
        public event EventHandler<EventArgs> InventoryLostClick;
        public event EventHandler<EventArgs> SelectAllClick;
        #endregion

        private VerificationInventoryPresenter _presenter;
        public VerificationInventoryPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        public Dictionary<int,string> SelectedItems
        {
            get
            {
                return (ViewState["mySelectedItems"] != null) ? (Dictionary<int, string>)ViewState["mySelectedItems"] : null;
            }
            set
            {
                ViewState["mySelectedItems"] = value;
            }
        }

        #region Enum
        private enum VerifyType
        {
            /// <summary>
            /// 核銷
            /// </summary>
            Verify,
            /// <summary>
            /// 強制核銷
            /// </summary>
            Enforce
        }
        #endregion

        #region prop
        /// <summary>
        /// SortExpression
        /// </summary>
        public string SortExpression
        {
            get { return (ViewState != null && ViewState["se"] != null) ? (string)ViewState["se"] : ViewPponCoupon.Columns.SequenceNumber + " asc"; }
            set { ViewState["se"] = value; }
        }

        /// <summary>
        /// PageSize
        /// </summary>
        public int PageSize
        {
            get { return gridPager.PageSize; }
            set { gridPager.PageSize = value; }
        }

        /// <summary>
        /// CurrentPage
        /// </summary>
        public int CurrentPage
        {
            get { return (!string.IsNullOrEmpty(((DropDownList)gridPager.FindControl("ddlPages")).SelectedValue)) ? int.Parse(((DropDownList)gridPager.FindControl("ddlPages")).SelectedValue) : gridPager.CurrentPage; }
        }

        /// <summary>
        /// BusinessHourGuid
        /// </summary>
        public Guid BusinessHourGuid
        {
            get
            {
                if (!string.IsNullOrEmpty(Request["bid"]))
                {
                    try
                    {
                        return new Guid(Request.QueryString["bid"]);
                    }
                    catch
                    {
                        return Guid.Empty;
                    }
                }
                else
                    return Guid.Empty;
            }
        }

        /// <summary>
        /// SellerGuid
        /// </summary>
        public Guid SellerGuid
        {
            get { return new Guid(ViewState["SellerGuid"].ToString()); }
            set { ViewState["SellerGuid"] = value; }
        }

        /// <summary>
        /// 使用者Id
        /// </summary>
        public string LoginUserId
        {
            get { return this.Page.User.Identity.Name; }
        }

        /// <summary>
        /// Client IP
        /// </summary>
        public string ClientIp
        {
            get { return Helper.GetClientIP(); }
        }

        /// <summary>
        /// 店家清點核銷數
        /// </summary>
        public string SellerVerifiedQuantity
        {
            get { return txtSellerVerifiedQuantity.Text; }
            set { txtSellerVerifiedQuantity.Text = value; }
        }

        /// <summary>
        /// 核銷 Coupon List
        /// </summary>
        public Dictionary<int, string> VerifyCouponList
        {
            get { return GetCouponList(VerifyType.Verify); }
        }

        /// <summary>
        /// 強制核銷 Coupon List
        /// </summary>
        public Dictionary<int, string> EnforceCouponList
        {
            get { return GetCouponList(VerifyType.Enforce); }
        }

        /// <summary>
        /// 核銷狀態
        /// </summary>
        public int DealAccountingFlag
        {
            get { return Convert.ToInt32(ViewState["DealAccountingFlag"]); }
            set { ViewState["DealAccountingFlag"] = value; }
        }

        /// <summary>
        /// 總銷售數
        /// </summary>
        public int SalesNumber
        {
            get { return Convert.ToInt32(ViewState["SalesNumber"]); }
            set { ViewState["SalesNumber"] = value; }
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            memProv = ProviderFactory.Instance().GetProvider<IMemberProvider>();
            odrProv = ProviderFactory.Instance().GetProvider<IOrderProvider>();

            if (!Page.IsPostBack)
            {
                // Authorize Control
                btnInventoryLost.Visible = (Roles.IsUserInRole("OperationAssistant") || Roles.IsUserInRole("Administrator"));
               
                this.Presenter.OnViewInitialized();
            }
            this.Presenter.OnViewLoaded();
        }

        #region Button Event
        /// <summary>
        /// 核銷
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnVerification_Click(object sender, EventArgs e)
        {
            CollectSelected();
            if (this.OkButtonClick != null)
                this.OkButtonClick(null, e);

            System.Web.UI.ScriptManager.RegisterStartupScript(this, typeof(Button), "alert", "alert('核銷成功!!');", true);
        }

        protected void lnkAll_Click(object sender, EventArgs e)
        {
            if (this.SelectAllClick != null)
                this.SelectAllClick(null, e);
            
            for (int i = 0; i < gvInventory.Rows.Count; i++)
            {
                string SequenceNumber = gvInventory.Rows[i].Cells[1].Text;
                CheckBox cb = gvInventory.Rows[i].FindControl("Verify") as CheckBox;
                cb.Checked = true;
            }

            hidSelectCount.Value = SelectedItems.Count.ToString();
        }

        /// <summary>
        /// 註記清冊遺失
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnInventoryLost_Click(object sender, EventArgs e)
        {
            if (this.InventoryLostClick != null)
                this.InventoryLostClick(null, e);
        }

        
        #endregion

        /// <summary>
        /// 店家清冊清點
        /// </summary>
        /// <param name="da"></param>
        public void SetDealAccountingData(DealAccounting da)
        {
            DealAccountingFlag = da.Flag;

            if (da != null)
            {
                if (Helper.IsFlagSet((long)da.Flag, (long)AccountingFlag.VerificationLost))
                {
                    lblSellerSlug.Text = lblSellerVerifiedQuantity.Text = lblSellerNonVeri.Text = "清冊已遺失";
                    lblSellerSlug.ForeColor = lblSellerVerifiedQuantity.ForeColor = lblSellerNonVeri.ForeColor = System.Drawing.Color.Red;
                    SetControlLock();
                }
                else
                {
                    if (da.SellerVerifiedQuantity != null)
                    {
                        txtSellerVerifiedQuantity.Visible = false;
                        lblSellerVerifiedQuantity.Text = da.SellerVerifiedQuantity.ToString();
                        lblSellerNonVeri.Text = (Convert.ToInt32(lblSlug.Text.Replace(",", string.Empty)) - da.SellerVerifiedQuantity).ToString();
                    }
                    else
                    {
                        lblSellerNonVeri.Text = string.Empty;
                        txtSellerVerifiedQuantity.Visible = true;
                    }

                    lblSellerSlug.Text = lblSlug.Text;

                    // 設定是否為鎖定
                    if (Helper.IsFlagSet((long)da.Flag, (long)AccountingFlag.VerificationLocked))
                    {
                        SetControlLock();
                    }
                }
            }
        }

        /// <summary>
        /// 店家基本資料
        /// </summary>
        /// <param name="vpd"></param>
        public void SetViewPponDealData(ViewPponDeal vpd)
        {
            if (vpd != null)
            {
                lblItemName.Text = vpd.ItemName;
                lblSellerName.Text = vpd.EventName;
                if (vpd.ItemName.IndexOf('-') < 0)
                {
                    lblSellerName.Text = vpd.SellerName;
                }
                else
                {
                    lblSellerName.Text = vpd.ItemName.Substring(0, vpd.ItemName.IndexOf('-'));
                }
                SellerGuid = vpd.SellerGuid;
                lblEndTime.Text = vpd.BusinessHourOrderTimeE.ToShortDateString();
                lblDeliverTiem.Text = vpd.BusinessHourDeliverTimeE != null ? Convert.ToDateTime(vpd.BusinessHourDeliverTimeE).ToShortDateString() : string.Empty;
                expireDateMessage.Text = GetExpireDateMessage(vpd);

                lblSlug.Text = Convert.ToInt32(vpd.Slug).ToString("N0");
            }
        }

        private string GetExpireDateMessage(ViewPponDeal vpd)
        {
            if (vpd == null)
                return string.Empty;

            Dictionary<string, DateTime> storeCloseDownDates = Presenter.GetStoreCloseDownDates(vpd.SellerGuid);
            Dictionary<string, DateTime> storeExpireDates = Presenter.GetStoreExpireDates(vpd.BusinessHourGuid);

            List<string> info = new List<string>();
            
            if(vpd.IsCloseDown)
            {
                if(vpd.CloseDownDate.HasValue)
                {
                    info.Add(string.Format("結束營業 {0}", vpd.CloseDownDate.Value.ToString("yyyy/MM/dd")));
                }
            }
            
            foreach (KeyValuePair<string, DateTime> pair in storeCloseDownDates)
            {
                info.Add(string.Format("{0}結束營業 {1}", pair.Key, pair.Value.ToString("yyyy/MM/dd")));
            }
            
            if(vpd.ChangedExpireDate.HasValue && vpd.BusinessHourDeliverTimeE.HasValue )
            {
                if (vpd.BusinessHourDeliverTimeE.Value > vpd.ChangedExpireDate.Value)
                {
                    info.Add(string.Format("停止使用 {0}", vpd.ChangedExpireDate.Value.ToString("yyyy/MM/dd")));
                }
                else
                {
                    info.Add(string.Format("延長 {0}", vpd.ChangedExpireDate.Value.ToString("yyyy/MM/dd")));
                }
            }

            if (vpd.BusinessHourDeliverTimeE.HasValue)
            {
                foreach (KeyValuePair<string, DateTime> pair in storeExpireDates)
                {
                    if(vpd.BusinessHourDeliverTimeE.Value > pair.Value)
                    {
                        info.Add( string.Format("{0}停止使用 {1}",pair.Key,pair.Value.ToString("yyyy/MM/dd")));
                    }
                    else
                    {
                        info.Add( string.Format("{0}延長 {1}",pair.Key,pair.Value.ToString("yyyy/MM/dd")));
                    }
                }
            }

            string msg = string.Join("；", info);
            if (string.IsNullOrEmpty(msg))
            {
                return string.Empty;
            }
            else
            {
                return string.Format("({0})", msg);
            }
        }

        /// <summary>
        /// 線上核銷結果(尚未鎖定)
        /// </summary>
        /// <param name="obj"></param>
        public void SetVerificationData(PponDealSalesInfo obj)
        {
            if (obj != null)
            {
                lblVeri.Text = (obj.VerifiedCount + obj.ForceReturnCount).ToString("N0");
                lblNonVeri.Text = obj.UnverifiedCount.ToString("N0");
                lblReturned.Text = (obj.ReturnedCount + obj.ForceVerifyCount - obj.BeforeDealEndReturnedCount).ToString("N0");

                if (obj.IsDealClose)
                {
                    lblSlug.Text = (obj.UnverifiedCount + obj.VerifiedCount + obj.ReturnedCount + obj.ForceReturnCount + obj.ForceVerifyCount - obj.BeforeDealEndReturnedCount).ToString("N0");
                }
            }
        }

        /// <summary>
        /// 線上核銷結果(已鎖定)
        /// </summary>
        /// <param name="obj"></param>
        public void SetVerificationRecordData(VerificationStatisticsLog obj)
        {
            if (obj != null)
            {
                lblVeri.Text = (Convert.ToInt32(obj.Verified) + Convert.ToInt32(obj.ForcedReturned)).ToString("N0");
                lblNonVeri.Text = Convert.ToInt32(obj.Unverified).ToString("N0");
                lblReturned.Text = (Convert.ToInt32(obj.Returned) + Convert.ToInt32(obj.ForcedVerified) - (Convert.ToInt32(SalesNumber) - Convert.ToInt32(lblSlug.Text.Replace(",", "")))).ToString("N0");
            }
        }

        #region GridView Event
        /// <summary>
        /// 憑證清冊
        /// </summary>
        /// <param name="vpc"></param>
        public void SetInventoryList(ViewPponCouponCollection vpc)
        {
            gvInventory.DataSource = vpc;
            gvInventory.DataBind();
        }

        /// <summary>
        /// UpdateHandler
        /// </summary>
        /// <param name="pageNumber"></param>
        protected void UpdateHandler(int pageNumber)
        {
            DataEventArgs<int> e = new DataEventArgs<int>(pageNumber);
            if (this.PageChanged != null)
                this.PageChanged(this, e);
        }

        /// <summary>
        /// GridView Data Count
        /// </summary>
        /// <returns></returns>
        protected int InventoryCount()
        {
            DataEventArgs<int> e = new DataEventArgs<int>(0);
            if (GetInventoryCount != null)
                GetInventoryCount(this, e);
            return e.Data;
        }

        protected void gvInventory_DataBinding(object sender, EventArgs e)
        {
            CollectSelected();
            hidSelectCount.Value = SelectedItems.Count.ToString();
        }

        /// <summary>
        /// RowDataBound
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvInventory_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowIndex > -1 && this.SelectedItems != null)
            {
                DataRowView row = e.Row.DataItem as DataRowView;
                CheckBox cb = e.Row.FindControl("Verify") as CheckBox;
                string CouponCode = e.Row.Cells[2].Text.ToString();
                if (this.SelectedItems.Values.Contains(CouponCode))
                    cb.Checked = true;
                else
                    cb.Checked = false;
            }

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ViewPponCoupon vpcc = (ViewPponCoupon)e.Row.DataItem;
                CheckBox Verify = (CheckBox)e.Row.Cells[0].FindControl("Verify");
                Label lblStatus = (Label)e.Row.Cells[3].FindControl("lblStatus");
                Label lblModifyTime = (Label)e.Row.Cells[4].FindControl("lblModifyTime");
                CashTrustLog cash_trust_log = new CashTrustLog();
                OldCashTrustLog old_cash_trust_log = new OldCashTrustLog();
                
                string strStatus = string.Empty;
                string strModifyTime = string.Empty;
                bool boolVerify = false;
                bool boolEnforced = false;
                System.Drawing.Color color = System.Drawing.Color.Black;

                if (string.IsNullOrEmpty(vpcc.SequenceNumber))
                {
                    strStatus = "無此Coupon";
                    color = System.Drawing.Color.Red;
                }
                else
                {
                    HiddenField hidCouponId = (HiddenField)e.Row.Cells[0].FindControl("hidCouponId");
                    hidCouponId.Value = vpcc.CouponId.ToString();
                    HiddenField hidCouponCode = (HiddenField)e.Row.Cells[0].FindControl("hidCouponCode");
                    hidCouponCode.Value = vpcc.CouponCode.ToString();

                    cash_trust_log = memProv.CashTrustLogGetByCouponId((int)vpcc.CouponId);
                    if (cash_trust_log.TrustId == Guid.Empty)
                    {
                        old_cash_trust_log = memProv.OldCashTrustLogGetByCouponId((int)vpcc.CouponId);
                        if (old_cash_trust_log.Id == 0)
                        {
                            strStatus = "憑證號碼錯誤";
                            color = System.Drawing.Color.Red;
                        }
                        else
                        {
                            SetData(vpcc, ref boolVerify, ref boolEnforced, ref strStatus, ref strModifyTime, old_cash_trust_log.Status, ref color);
                        }
                    }
                    else
                    {
                        // 強制退貨/核銷 查詢
                        if (!cash_trust_log.SpecialStatus.EqualsAny(new int[] { 0, (int)TrustSpecialStatus.Giveaway, (int)TrustSpecialStatus.SpecialDiscount }) &&
                            !Helper.IsFlagSet((long)cash_trust_log.SpecialStatus, (long)TrustSpecialStatus.ReturnCanceled))
                        {
                            if (Helper.IsFlagSet((long)cash_trust_log.SpecialStatus, (long)TrustSpecialStatus.ReturnForced))
                            {
                                strStatus = "已強制退貨";
                            }
                            else if (Helper.IsFlagSet((long)cash_trust_log.SpecialStatus, (long)TrustSpecialStatus.VerificationForced) || 
                                     Helper.IsFlagSet((long)cash_trust_log.SpecialStatus, (long)TrustSpecialStatus.VerificationUnrecovered))
                            {
                                strStatus = "已強制核銷";
                            }
                            else if (Helper.IsFlagSet((long)cash_trust_log.SpecialStatus, (long)TrustSpecialStatus.VerificationLost))
                            {
                                strStatus = "已核銷-清冊已遺失";
                                color = System.Drawing.Color.Red;
                            }
                            strModifyTime = cash_trust_log.SpecialOperatedTime.ToString();
                        }
                        else
                        {
                            SetData(vpcc, ref boolVerify, ref boolEnforced, ref strStatus, ref strModifyTime, cash_trust_log.Status, ref color);
                        }
                        
                    }
                }

                lblStatus.Text = strStatus;
                lblStatus.ForeColor = color;
                lblModifyTime.Text = strModifyTime;
                Verify.Visible = boolVerify;

            }
        }
        #endregion

        #region Private Event
        /// <summary>
        /// 設定憑證狀態
        /// </summary>
        /// <param name="vpcc">ViewPponCoupon</param>
        /// <param name="chkCheck">核銷</param>
        /// <param name="chkEnforceCheck">強制核銷</param>
        /// <param name="lblStatus">憑證狀況</param>
        /// <param name="lblModifyTime">處理時間</param>
        /// <param name="status">Status</param>
        private void SetData(ViewPponCoupon vpcc, ref bool boolVerify, ref bool boolEnforce, ref string strStatus, ref string strModifyTime, int status, ref System.Drawing.Color color)
        {
            if (status == (int)TrustStatus.Verified)
            {
                VerificationLog vl = memProv.VerificationLogVerifyCouponGet(vpcc.CouponId);
                strModifyTime = vl.CreateTime != null ? Convert.ToDateTime(vl.CreateTime).ToString() : string.Empty;
                strStatus = "已核銷";
            }
            else if (status > (int)TrustStatus.Verified)
            {
                PaymentTransaction pt = odrProv.PaymentTransactionGetList(vpcc.OrderGuid).Where(x => x.TransType == (int)PayTransType.Refund).FirstOrDefault();
                if (pt != null)
                {
                    strModifyTime = pt.TransTime.ToString();
                    if (pt.TransTime < vpcc.BusinessHourOrderTimeE)
                    {
                        strStatus = "結檔前已退貨";
                        color = System.Drawing.Color.Green;
                        boolEnforce = true;
                    }
                    else
                    {
                        strStatus = "已退貨";
                        boolEnforce = true;
                    }
                }
            }
            else
            {
                boolVerify = true;
            }
        }

        /// <summary>
        /// 取得勾選憑證資料
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        private Dictionary<int, string> GetCouponList(VerifyType type)
        {
            Dictionary<int, string> list = new Dictionary<int, string>();
            list = SelectedItems;
            return list;
        }
        
        private void SetControlLock()
        {
            gvInventory.Columns[0].Visible = false;
            gvInventory.Columns[4].Visible = false;
            txtSellerVerifiedQuantity.Visible = false;
            btnInventoryLost.Visible = lnkReverse.Visible = btnVerification.Visible = false;
        }

        private void CollectSelected()
        {
            Dictionary<int, string> selectedItems = null;
            if (SelectedItems == null)
                selectedItems = new Dictionary<int, string>();
            else
                selectedItems = SelectedItems;

            for (int i = 0; i < gvInventory.Rows.Count; i++)
            {
                CheckBox cb = gvInventory.Rows[i].FindControl("Verify") as CheckBox;
                HiddenField hidCouponId = (HiddenField)gvInventory.Rows[i].FindControl("hidCouponId");
                HiddenField hidCouponCode = (HiddenField)gvInventory.Rows[i].FindControl("hidCouponCode");
                string couponId = hidCouponId.Value;
                string couponCode = hidCouponCode.Value;

                if (selectedItems.Values.Contains(couponCode) && (!cb.Checked || !cb.Visible))
                    selectedItems.Remove(Convert.ToInt32(hidCouponId.Value));
                if (!selectedItems.Values.Contains(couponCode) && (cb.Checked && cb.Visible))
                    selectedItems.Add(Convert.ToInt32(hidCouponId.Value), hidCouponCode.Value);
            }
            this.SelectedItems = selectedItems;
        }
        #endregion

     
    }
}
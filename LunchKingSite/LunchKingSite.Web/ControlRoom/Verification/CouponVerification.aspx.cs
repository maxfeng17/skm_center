﻿using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.UI;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace LunchKingSite.Web.ControlRoom.Verification
{
    public partial class CouponVerification : RolePage, ICouponVerificationView
    {
        #region events

        public event EventHandler<DataEventArgs<string>> SearchCoupon = delegate { };

        public event EventHandler<DataEventArgs<string>> SearchTmallCoupon = delegate { };

        public event EventHandler<DataEventArgs<string>> SearchHiDealCoupon = delegate { };

        public event EventHandler<DataEventArgs<KeyValuePair<int, bool>>> ForceRefund;
        
        public event EventHandler<DataEventArgs<int>> ForceVerify = delegate { };

        public event EventHandler<DataEventArgs<int>> Verify = delegate { };

        public event EventHandler<DataEventArgs<int>> Unverify = delegate { };

        public event EventHandler<EventArgs> SearchCouponByType = delegate { };

        #endregion events

        #region properties

        public string CouponSn { get; private set; }

        public string UserName
        {
            get
            {
                return this.User.Identity.Name;
            }
        }

        public int orderClassification
        {
            get
            {
                return int.Parse(this.rblSearchType.SelectedValue);
            }
        }

        private CouponVerificationPresenter _presenter;

        public CouponVerificationPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        #endregion properties

        #region methods

        public void SetCouponList(Collection<VerifyingCoupon> couponInfo)
        {
            gvCoupons.DataSource = couponInfo;
            gvCoupons.DataBind();

            if (couponInfo.Count == 0)
            {
                litSearchWarningMessage.Text =
                    (rblSearchType.SelectedValue == "4") ? "查無憑證！請檢查是否為天貓訂單。" : "查無憑證！ 若為舊訂單，請洽技術部。";
            }
            else
            {
                litSearchWarningMessage.Text = null;
            }
        }

        public void SetFilterTypeDropDown(Dictionary<string, string> types)
        {
            ddlST.DataSource = types;
            ddlST.DataBind();
        }

        public string GetFilterTypeDropDown()
        {
            return ddlST.SelectedValue;
        }

        #endregion methods

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Request.QueryString["csn"] != null)
                {
                    CouponSn = Request.QueryString["csn"];
                    this.Presenter.OnViewLoaded();
                    SearchCoupon(this, new DataEventArgs<string>(Request.QueryString["csn"]));
                }
                this.Presenter.OnViewInitialized();
            }
            else
            {
                CouponSn = tbCouponSn.Text;
                this.Presenter.OnViewLoaded();
            }
        }

        public void SetProcessResultMessage(string msg)
        {
            litProcessResultMessage.Text = msg;
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            QueryData(e);
        }

        private void QueryData(EventArgs e)
        {
            CouponSn = tbCouponSn.Text;
            switch (rblSearchType.SelectedValue)
            {
                case "1": //P好康
                    SearchCouponByType(this, e);
                    //SearchCoupon(this, new DataEventArgs<string>(CouponSn));
                    break;

                case "2": //品生活

                    SearchHiDealCoupon(this, new DataEventArgs<string>(CouponSn));
                    break;

                case "4"://天貓
                    SearchTmallCoupon(this, new DataEventArgs<string>(CouponSn));
                    break;
            }
        }

        protected void gvCoupons_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                string msg1 = string.Format("確定將憑證 {0} 強制退貨？", ((VerifyingCoupon)e.Row.DataItem).CouponSn);
                string msg2 = string.Format("確定將憑證 {0} 強制核銷？", ((VerifyingCoupon)e.Row.DataItem).CouponSn);
                string msg3 = string.Format("確定將憑證 {0} 核銷？", ((VerifyingCoupon)e.Row.DataItem).CouponSn);
                string msg4 = string.Format("確定將憑證 {0} 退貨？", ((VerifyingCoupon)e.Row.DataItem).CouponSn);
                string msg5 = string.Format("確定將憑證 {0} 回復成未核銷？", ((VerifyingCoupon)e.Row.DataItem).CouponSn);

                //((CheckBox)e.Row.FindControl("ckRefundSCash")).Enabled = !(((VerifyingCoupon)e.Row.DataItem).IsRefundCashOnly);
                ((CheckBox)e.Row.FindControl("ckForceRefundSCash")).Enabled = !(((VerifyingCoupon)e.Row.DataItem).IsRefundCashOnly);

                switch ((((VerifyingCoupon)e.Row.DataItem).CouponUseStatus))
                {
                    case CouponUsageStatus.Verified:
                    case CouponUsageStatus.LostVerified:    //2011.12.09 清冊遺失依然可以強制退貨

                        ((HtmlGenericControl)e.Row.FindControl("divVerified")).Attributes.CssStyle["display"] = "inline";
                        ((Button)e.Row.FindControl("btnForceRefund")).Attributes["msg"] = msg1;
                        ((Button)e.Row.FindControl("btnUnverify")).Attributes["msg"] = msg5;
                        ((HtmlGenericControl)e.Row.FindControl("divForcedVerified")).Disabled = true;
                        ((HtmlGenericControl)e.Row.FindControl("divForcedRefunded")).Disabled = true;
                        ((HtmlGenericControl)e.Row.FindControl("divLostVerified")).Disabled = true;
                        ((HtmlGenericControl)e.Row.FindControl("divUnverified")).Disabled = true;
                        if (orderClassification == (int)OrderClassification.HiDeal) //品生活憑證核銷時不給退貨
                        {
                            ((Button)e.Row.FindControl("btnForceRefund")).Visible = false;
                            ((CheckBox)e.Row.FindControl("ckForceRefundSCash")).Visible = false;
                        }
                        break;

                    case CouponUsageStatus.Refunded:
                        if (((VerifyingCoupon)e.Row.DataItem).DealVerified)
                        {
                            ((HtmlGenericControl)e.Row.FindControl("divRefunded")).Attributes.CssStyle["display"] = "inline";
                            ((Button)e.Row.FindControl("btnForceVerify")).Visible = false;

                            (e.Row.FindControl("litCannotForceVerify")).Visible = true;
                        }
                        else
                        {
                            ((HtmlGenericControl)e.Row.FindControl("divRefunded")).Attributes.CssStyle["display"] = "inline";
                            ((Button)e.Row.FindControl("btnForceVerify")).Attributes["msg"] = msg2;
                            ((Button)e.Row.FindControl("btnForceVerify")).Visible = orderClassification == (int)OrderClassification.HiDeal ? false : true;
                            (e.Row.FindControl("litCannotForceVerify")).Visible = false;
                        }
                        break;

                    case CouponUsageStatus.ForcedVerified:
                        ((HtmlGenericControl)e.Row.FindControl("divForcedVerified")).Attributes.CssStyle["display"] = "inline";
                        break;

                    case CouponUsageStatus.ForcedRefunded:
                        ((HtmlGenericControl)e.Row.FindControl("divForcedRefunded")).Attributes.CssStyle["display"] = "inline";
                        break;

                    case CouponUsageStatus.RefundedBeforeClose:
                        ((HtmlGenericControl)e.Row.FindControl("divRefunded")).Attributes.CssStyle["display"] = "inline";
                        ((Button)e.Row.FindControl("btnForceVerify")).Attributes["msg"] = msg2;
                        ((Button)e.Row.FindControl("btnForceVerify")).Visible = orderClassification == (int)OrderClassification.HiDeal ? false : true;
                        (e.Row.FindControl("litCannotForceVerify")).Visible = false;
                        break;

                    case CouponUsageStatus.Unverified:
                        if (((VerifyingCoupon)e.Row.DataItem).DealVerified)
                        {
                            ((Button)e.Row.FindControl("btnVerify")).Visible = false;
                        }
                        ((HtmlGenericControl)e.Row.FindControl("divUnverified")).Attributes.CssStyle["display"] = "inline";
                        ((Button)e.Row.FindControl("btnVerify")).Attributes["msg"] = msg3;
                        e.Row.FindControl("litNoRefund").Visible = ((VerifyingCoupon)e.Row.DataItem).DealNoRefund;
                        e.Row.FindControl("litExpireNoRefund").Visible = ((VerifyingCoupon)e.Row.DataItem).DealExpireNoRefund;
                        break;
                }

                //權限控管 (強制核銷和強制退貨)
                Button fr = (Button)e.Row.FindControl("btnForceRefund"); Button fv = (Button)e.Row.FindControl("btnForceVerify");
                Button Verify = (Button)e.Row.FindControl("btnVerify");
                if (Verify != null)
                {
                    Verify.Enabled = CommonFacade.IsInSystemFunctionPrivilege(User.Identity.Name, SystemFunctionType.Verify);
                }

                if (fr != null)
                {
                    fr.Enabled = CommonFacade.IsInSystemFunctionPrivilege(User.Identity.Name, SystemFunctionType.ForceRefund);
                }

                if (fv != null)
                {
                    fv.Enabled = CommonFacade.IsInSystemFunctionPrivilege(User.Identity.Name, SystemFunctionType.ForceVerify);
                }
            }
        }

        protected string UsageStatusToString(int status)
        {
            switch ((CouponUsageStatus)status)
            {
                case CouponUsageStatus.Verified:
                    return "已核銷";

                case CouponUsageStatus.Refunded:
                    return "已退貨";

                case CouponUsageStatus.ForcedVerified:
                    return "已強制核銷";

                case CouponUsageStatus.ForcedRefunded:
                    return "已強制退貨";

                case CouponUsageStatus.LostVerified:
                    return "已核銷--清冊遺失";

                case CouponUsageStatus.Unverified:
                    return "未核銷";

                case CouponUsageStatus.RefundedBeforeClose:
                    return "結檔前退貨";

                default:
                    return "unidentified status...";
            }
        }

        protected void gvCoupons_RowCommand(Object sender, GridViewCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "ForceReturn":
                    GridViewRow row = (GridViewRow)((Button)e.CommandSource).Parent.Parent.Parent;
                    bool toForceReturnScash = ((CheckBox)row.FindControl("ckForceRefundSCash")).Checked;
                    ForceRefund(this, new DataEventArgs<KeyValuePair<int, bool>>(new KeyValuePair<int, bool>(int.Parse((string)e.CommandArgument), toForceReturnScash)));
                    break;

                case "ForceVerify":
                    ForceVerify(this, new DataEventArgs<int>(int.Parse((string)e.CommandArgument)));
                    break;

                case "Verify":
                    Verify(this, new DataEventArgs<int>(int.Parse((string)e.CommandArgument)));
                    break;

                case "Unverify":
                    Unverify(this, new DataEventArgs<int>(int.Parse((string)e.CommandArgument)));
                    break;
            }

            QueryData(e);
        }

        protected void rblSearchType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rblSearchType.SelectedValue.Equals("4"))
            {
                ddlST.Visible = false;
            }
            else
            {
                ddlST.Visible = true;
            }
        }

        protected void ddlST_SelectedIndexChanged(object sender, EventArgs e)
        {
            //請輸入憑證編號 (不含確認碼)  Ex: 1001-9647
            //請輸入憑證確認碼 Ex: 189866
            if (string.IsNullOrWhiteSpace(tbCouponSn.Text) ||
                tbCouponSn.Text.Equals("請輸入憑證編號 (不含確認碼)  Ex: 1001-9647") ||
                tbCouponSn.Text.Equals("請輸入憑證編號 (不含確認碼)  Ex: 1001-9647"))
            {
                switch (GetFilterTypeDropDown())
                {
                    case "sequence":
                        tbCouponSn.Text = "請輸入憑證編號 (不含確認碼)  Ex: 1001-9647";
                        break;
                    case "code":
                        tbCouponSn.Text = "請輸入憑證確認碼 Ex: 189866";
                        break;
                    default:
                        break;
                }
            }
        }
    }
}
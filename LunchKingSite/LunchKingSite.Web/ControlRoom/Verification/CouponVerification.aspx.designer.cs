﻿//------------------------------------------------------------------------------
// <自動產生的>
//     這段程式碼是由工具產生的。
//
//     對這個檔案所做的變更可能會造成錯誤的行為，而且如果重新產生程式碼，
//     所做的變更將會遺失。 
// </自動產生的>
//------------------------------------------------------------------------------

namespace LunchKingSite.Web.ControlRoom.Verification {
    
    
    public partial class CouponVerification {
        
        /// <summary>
        /// rblSearchType 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.RadioButtonList rblSearchType;
        
        /// <summary>
        /// ddlST 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.DropDownList ddlST;
        
        /// <summary>
        /// tbCouponSn 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox tbCouponSn;
        
        /// <summary>
        /// btnSearch 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button btnSearch;
        
        /// <summary>
        /// gvCoupons 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.GridView gvCoupons;
        
        /// <summary>
        /// litSearchWarningMessage 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal litSearchWarningMessage;
        
        /// <summary>
        /// litProcessResultMessage 控制項。
        /// </summary>
        /// <remarks>
        /// 自動產生的欄位。
        /// 將移動欄位宣告從設計檔案修改為程式碼後置檔案。
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal litProcessResultMessage;
    }
}

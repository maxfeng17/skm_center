﻿<%@ Page Language="C#" MasterPageFile="~/ControlRoom/backend.master" AutoEventWireup="true" 
    CodeBehind="GiftBanner.aspx.cs" Inherits="LunchKingSite.Web.ControlRoom.Mgm.GiftBanner" %>

<%@ Import Namespace="LunchKingSite.BizLogic.Component" %>
<%@ Import Namespace="LunchKingSite.Core" %>
<%@ Import Namespace="LunchKingSite.DataOrm" %>
<%@ Import Namespace="LunchKingSite.WebLib.Views" %>
<%@ Register Src="../Controls/Pager.ascx" TagName="Pager" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <link rel="stylesheet" type="text/css" href="../../Tools/colorpicker/css/colorpicker.css" />
    <script type="text/javascript" src="../../Tools/colorpicker/js/colorpicker.js"></script>

    <script type="text/javascript">


        function Check() {
            if ($('[id*=txtTitle]').val() == '') {
                alert('請輸入Banner標題名稱!!');
                return false;
            }else if ($('[id*=txtContent]').val() == '') {
                alert('請輸入Banner內容!!');
                return false;
            }else if ($('[id*=txtColor]').val() == '') {
                alert('請輸入字型顏色!!');
                return false;
            } else if ($('#<%=rblShowType.ClientID %> input[type=radio]:checked').val() == 0 && $("#<%=txtImageUrl.ClientID%>").val()!="" && $("#width").val()!="640" ||
                       $('#<%=rblShowType.ClientID %> input[type=radio]:checked').val() == 0 && $("#<%=txtImageUrl.ClientID%>").val() != "" && $("#height").val() != "560") { 
                alert('圖片尺寸應為640x560，不符規定');
                return false;
            } else if ($('#<%=rblShowType.ClientID %> input[type=radio]:checked').val() == 1 && $("#<%=txtImageUrl.ClientID%>").val()!="" && $("#width").val()!="640" ||
                       $('#<%=rblShowType.ClientID %> input[type=radio]:checked').val() == 1 && $("#<%=txtImageUrl.ClientID%>").val() != "" && $("#height").val() != "152") {     
                alert('圖片尺寸640x152，不符規定');
                return false;
            }
            else {

                if ($("#<%=mode.ClientID%>").val() == "Add") {

                    if ($("#<%=txtImageUrl.ClientID%>").val() != "") {
                        //沒上線皆可新增
                        if ($('#<%=rblOnLine.ClientID %> input[type=radio]:checked').val() == 0) {
                            return true;
                        }
                        else {
                            if ($('#<%=rblShowType.ClientID %> input[type=radio]:checked').val() == 0) {
                                if ($("#<%=noGiftOn.ClientID%>").val() != "0") {
                                    if (confirm("目前已有顯示狀況為'無禮物'的圖檔在線上，若確定儲存會自動將原有設定下線")) {
                                        return true;
                                    }
                                    else {
                                        return false;
                                    };
                                }
                                else {
                                    return true;
                                }
                            }
                            else {
                                if ($("#<%=hasGiftOn.ClientID%>").val() != "0") {
                                    if (confirm("目前已有顯示狀況為'有禮物'的圖檔在線上，若確定儲存會自動將原有設定下線")) {
                                        return true;
                                    }
                                    else {
                                        return false;
                                    };
                                }
                                else {
                                    return true;
                                }
                            }
                        }
                    }
                    else {
                        alert("請上傳圖片");
                        return false;
                    }
                }
                else {
                        //沒上線皆可新增
                        if ($('#<%=rblOnLine.ClientID %> input[type=radio]:checked').val() == 0) {
                            return true;
                        }
                        else {
                            if ($('#<%=rblShowType.ClientID %> input[type=radio]:checked').val() == 0) {
                                if ($("#<%=noGiftOn.ClientID%>").val() != "0" && $("#<%=noGiftOn.ClientID%>").val() != "" && $("#<%=noGiftOn.ClientID%>").val()!=$("#<%=txtId.ClientID%>").val()) {
                                    if (confirm("目前已有顯示狀況為'無禮物'的圖檔在線上，若確定儲存會自動將原有設定下線")) {
                                        return true;
                                    }
                                    else {
                                        return false;
                                    };
                                }
                                else {
                                    return true;
                                }
                            }
                            else {
                                if ($("#<%=hasGiftOn.ClientID%>").val() != "0" && $("#<%=hasGiftOn.ClientID%>").val()!="" && $("#<%=hasGiftOn.ClientID%>").val()!=$("#<%=txtId.ClientID%>").val()) {
                                    if (confirm("目前已有顯示狀況為'有禮物'的圖檔在線上，若確定儲存會自動將原有設定下線")) {
                                        return true;
                                    }
                                    else {
                                        return false;
                                    };
                                }
                                else {
                                    return true;
                                }
                            }
                        }
                }

            }
        }

        function Delete()
        {
            if (confirm("確定要刪除此筆資料")) {
                return true;
            }
            else {
                return false;
            }
        }

        $(function () {          

            $("#<%=txtImageUrl.ClientID%>").change(function () {
                if (this.files && this.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $("#<%=blah.ClientID%>").attr('src', e.target.result);
                        var image = new Image();
                        image.onload = function () {
                            $("#width").val(image.width);
                            $("#height").val(image.height);
                        }
                        image.src = e.target.result;
                    }


                    reader.readAsDataURL(this.files[0]);
                }
            });

            $("#<%=rblShowType.ClientID%>").change(function () {
                if ($('#<%=rblShowType.ClientID %> input[type=radio]:checked').val() == 0) {
                    $("#msg").html("尺寸限 640 x 560");
                }
                else {
                    $("#msg").html("尺寸限 640 x 152");
                }
            })



            $('#<%=txtColor.ClientID%>').ColorPicker({
                onSubmit: function (hsb, hex, rgb, el) {
                    $(el).val(hex);
                    $(el).ColorPickerHide();
                },
                onBeforeShow: function () {
                    $(this).ColorPickerSetColor(this.value);
                }
            })
            .bind('keyup', function () {
                $(this).ColorPickerSetColor(this.value);
            });
        });

    </script>
</asp:Content>


<asp:Content ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <asp:Panel ID="divMgmGiftHeaderSearch" runat="server">
        <fieldset id="searchArea" runat="server">
            <legend>送禮區Banner搜尋</legend>
            <table>
                <tr>
                    <td>標題</td>
                    <td>
                        <asp:TextBox ID="searchTitle" runat="server" MaxLength="50" size="50"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>顯示狀況</td>
                    <td>
                        <asp:RadioButton ID="rbAllGift" GroupName="searchShowType" runat="server" />全部
                        <asp:RadioButton ID="rbNone" GroupName="searchShowType" runat="server" />無禮物
                        <asp:RadioButton ID="rbOne" GroupName="searchShowType" runat="server" />有禮物
                    </td>
                </tr>
                <tr>
                    <td>上線與否</td>
                    <td>
                        <asp:RadioButton ID="rbAll" GroupName="searchOnline" runat="server" />全部
                        <asp:RadioButton ID="rbNoOnline" GroupName="searchOnline" runat="server" />關閉
                        <asp:RadioButton ID="rbOnline" GroupName="searchOnline" runat="server" />開啟
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:Button ID="btnSearch" runat="server" Text="搜尋" OnClick="btnSearch_Click"/>&nbsp;
                        <asp:Button ID="btnAdd" runat="server" Text="新增" OnClick="btnAdd_Click"/>
                    </td>
                </tr>
            </table>
        </fieldset>
    </asp:Panel>

    <br />
    <hr />
    <br /> 

    <asp:Panel ID="divGiftBannerList" runat="server" Visible="false">
        <asp:GridView ID="giftBanner" runat="server" OnRowCommand="giftBanner_RowCommand" OnRowDataBound="giftBanner_RowDataBound"
            GridLines="Horizontal" ForeColor="Black" CellPadding="4" BorderWidth="1px" BorderStyle="None"
            BorderColor="#DEDFDE" BackColor="White" EmptyDataText="無符合的資料" AutoGenerateColumns="false"
            Font-Size="Small" Width="1029px">
            <FooterStyle BackColor="#CCCC99"></FooterStyle>
            <RowStyle BackColor="#F7F7DE" Height="20px" HorizontalAlign="Center"></RowStyle>
            <SelectedRowStyle BackColor="#CE5D5A" ForeColor="White" Font-Bold="True"></SelectedRowStyle>
            <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Center"></PagerStyle>
            <HeaderStyle BackColor="#6B696B" ForeColor="White" Font-Bold="True" Height="20px"
                HorizontalAlign="Center"></HeaderStyle>
            <Columns>
                <asp:TemplateField HeaderText="標題" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="10%">
                    <ItemTemplate>
                        <asp:Label ID="lblTitle" runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="內容" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="30%">
                    <ItemTemplate>
                        <asp:Label ID="lblContent" runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="圖片" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="10%">
                    <ItemTemplate>
                        <asp:Label ID="lblImageUrl" runat="server" ></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="文字顏色" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="6%">
                    <ItemTemplate>
                        <asp:Label ID="lblTxtColor" runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="顯示狀況" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="6%">
                    <ItemTemplate>
                        <asp:Label ID="lblShowType" runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="上線與否" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="6%">
                    <ItemTemplate>
                        <asp:Label ID="lblOnLine" runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="建立時間" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="10%">
                    <ItemTemplate>
                        <asp:Label ID="lblCreateTime" runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="編輯" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5%">
                    <ItemTemplate>
                        <asp:Button ID="btnEdit" runat="server" CommandName="UPD" Text="編輯"></asp:Button>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="刪除" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5%">
                    <ItemTemplate>
                        <asp:Button ID="btnDelete" runat="server" Text="刪除" CommandName="DEL" OnClientClick="return Delete()"/>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
            <uc1:Pager ID="ucPager" runat="server" PageSize="50" ongetcount="GetBannerCount" onupdate="BannerUpdateHandler"></uc1:Pager>
    </asp:Panel>


    <asp:Panel ID="divGiftBannerEdit" runat="server" Visible="false">
        <fieldset>
            <legend>送禮區Banner編輯</legend>
            <asp:HiddenField ID="hasGiftOn" runat="server" />
            <asp:HiddenField ID="noGiftOn" runat="server" />
            <asp:HiddenField ID="mode" runat="server" />
            <asp:HiddenField ID="width" runat="server" ClientIDMode="Static"/>
            <asp:HiddenField ID="height" runat="server" ClientIDMode="Static" />
            <table>
                <tr>
                    <td colspan="2"> <asp:ImageButton ID="imageClose" ImageUrl="~/images/close.gif" ImageAlign="Right" OnClick="btnClose_Click" runat="server"></asp:ImageButton></td>
                </tr>
                <tr>
                    <td style="width: 100px">標題</td>
                    <td>
                        <asp:HiddenField ID="txtId" runat="server" />
                        <asp:TextBox ID="txtTitle" runat="server" size="50"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>內容</td>
                    <td>
                        <TextArea ID="txtContent" cols="50" rows="5" maxlength="100" runat="server"></TextArea>
                        <br />
                        <font color="red" size="1">內容限100個字</font>    
                    </td>
                </tr>
                <tr>
                    <td>圖片</td>
                    <td>
                          <asp:FileUpload ID="txtImageUrl"  runat="server" /><font color="red" size="1"><span id="msg">尺寸限 640 x 560</span></font>            
                    </td>
                </tr>
                <tr>
                    <td>預覽圖片</td>
                    <td>
                        <asp:Image ID="blah" runat="server"></asp:Image>
                    </td>
                </tr>
                <tr>
                    <td>文字顏色</td>
                    <td>
                        <asp:TextBox ID="txtColor" runat="server" size="5"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>顯示狀況</td>
                    <td>
                        <asp:RadioButtonList ID="rblShowType" runat="server" RepeatLayout="Flow" RepeatDirection="Horizontal"></asp:RadioButtonList>
                    </td>
                </tr>
                <tr>
                    <td>上線與否:</td>
                    <td>
                        <asp:RadioButtonList ID="rblOnLine" runat="server" RepeatLayout="Flow" RepeatDirection="Horizontal"></asp:RadioButtonList>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="center">
                        <asp:Button ID="btnSave" runat="server" Text="儲存" OnClick="btnSave_Click" OnClientClick="return Check()"/>&nbsp;
                        <asp:Button ID="btnClose" runat="server" Text="取消" OnClick="btnClose_Click"/>
                    </td>
                </tr>
            </table>
            <br />

        </fieldset>
    </asp:Panel>


</asp:Content>

﻿using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.Core.Enumeration;

namespace LunchKingSite.Web.ControlRoom.Mgm
{
    public partial class Gift_Search : RolePage, IGiftSearchView
    {
        #region props
        private static IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        private static IMGMProvider mgmp = ProviderFactory.Instance().GetProvider<IMGMProvider>();

        private GiftSearchPresenter _presenter;
        public GiftSearchPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        public int PageSize
        {
            get { return ucPager.PageSize; }
            set { ucPager.PageSize = value; }
        }

        public int CurrentPage
        {
            get
            {
                return ucPager.CurrentPage;
            }
        }

        public string MemberInfo
        {
            get { return txtMemberInfo.Text; }

        }

        public string SequenceNumber
        {
            get { return txtSequenceNumber.Text; }
        }

        public string Mobile
        {
            get { return txtMobile.Text; }
        }


        public string ProductName
        {
            get { return txtProductName.Text; }
        }

        public string TbOS
        {
            get { return tbOS.Text; }
        }
        public string TbOE
        {
            get { return tbOE.Text; }
        }



        public int UserId
        {
            get
            {
                if (!string.IsNullOrEmpty(MemberInfo))
                {
                    Member memberByUserName = mp.MemberGetByUserName(MemberInfo);
                    if (memberByUserName.IsLoaded)
                    {
                        return memberByUserName.UniqueId;
                    }
                    else
                    {
                        Member memberByMobile = mp.MemberGetByMobile(MemberInfo);
                        if (memberByMobile.IsLoaded)
                        {
                            return memberByMobile.UniqueId;
                        }
                        else
                        {
                            return -1;
                        }

                    }
                }
                else
                {
                    return 0;
                }
            }
        }


        #endregion

        #region event
        public event EventHandler Search;
        public event EventHandler<DataEventArgs<string>> ViewMgmGiftListGet;
        public event EventHandler<DataEventArgs<int>> ViewMgmGiftListGetCount;
        public event EventHandler<DataEventArgs<int>> ViewMgmGiftListPageChanged;
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Presenter.OnViewInitialized();
            }
            _presenter.OnViewLoaded();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            if (this.Search != null)
            {
                this.Search(this, e);
            }
            ucPager.ResolvePagerView(CurrentPage, true);
            
            giftViewArea.Visible = false;
        }

        protected void giftSearch_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.DataItem is ViewMgmGiftBacklist)
            {
                ViewMgmGiftBacklist item = (ViewMgmGiftBacklist)e.Row.DataItem;
                LinkButton lblGiftId = ((LinkButton)e.Row.FindControl("lblGiftId"));
                HyperLink lblMemberAccount = ((HyperLink)e.Row.FindControl("lblMemberAccount"));
                Label lblName = ((Label)e.Row.FindControl("lblName"));
                Label lblMobile = ((Label)e.Row.FindControl("lblMobile"));
                Label lblTitle = ((Label)e.Row.FindControl("lblTitle"));
                Label lblCount = ((Label)e.Row.FindControl("lblCount"));
                HyperLink lblOrderId = ((HyperLink)e.Row.FindControl("lblOrderId"));
                Label lblSendTime = ((Label)e.Row.FindControl("lblSendTime"));
                lblGiftId.Text = item.SequenceNumber;
                lblGiftId.CommandArgument = item.SequenceNumber+","+item.Account;
                lblMemberAccount.Text = item.Account;
                lblMemberAccount.NavigateUrl = "~/controlroom/User/users_edit.aspx?username="+ HttpUtility.UrlEncode(item.Account);
                lblMemberAccount.Target = "_blank";
                lblName.Text = item.Name;
                lblMobile.Text = item.Mobile;
                lblCount.Text = item.Count.ToString();
                lblTitle.Text = item.Title;
                lblOrderId.Text = item.OrderId;
                lblOrderId.NavigateUrl = "~/controlroom/Order/order_detail.aspx?oid="+item.Guid;
                lblOrderId.Target = "_blank";
                lblSendTime.Text = item.SendTime.ToString("yyyy/MM/dd HH:mm");
            }
        }

        protected void giftView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.DataItem is ViewMgmGift)
            {
                ViewMgmGift item = (ViewMgmGift)e.Row.DataItem;
                HyperLink lblReceiveAccount = ((HyperLink)e.Row.FindControl("lblReceiveAccount"));
                Label lblReceiveName = ((Label)e.Row.FindControl("lblReceiveName"));
                Label lblAccept = ((Label)e.Row.FindControl("lblAccept"));
                Label lblReceiveTime = ((Label)e.Row.FindControl("lblReceiveTime"));
                HyperLink lblCouponNumber = ((HyperLink)e.Row.FindControl("lblCouponNumber"));
                Label lblStatus = ((Label)e.Row.FindControl("lblStatus"));
                Label lblCardStatus = ((Label)e.Row.FindControl("lblCardStatus"));
                Label lblCardLink = ((Label)e.Row.FindControl("lblCardLink"));
                if (item.Accept == 6)
                {
                    lblReceiveAccount.Text = GetQuickInfo(item.Id);
                }
                else
                {
                    lblReceiveAccount.Text = GetReceiveAccount(item.ReceiverId);
                    lblReceiveAccount.NavigateUrl = "~/controlroom/User/users_edit.aspx?username=" + HttpUtility.UrlEncode(GetReceiveAccount(item.ReceiverId));
                    lblReceiveAccount.Target = "_blank";
                }
                lblReceiveName.Text = item.ReceiveName;
                lblAccept.Text = Helper.GetEnumDescription((MgmAcceptGiftType)item.Accept);
                lblCouponNumber.Text = item.CouponSequenceNumber;
                lblCouponNumber.NavigateUrl = "~/controlroom/Verification/CouponVerification.aspx?csn="+item.CouponSequenceNumber;
                lblCouponNumber.Target = "_blank";
                lblStatus.Text = item.IsUsed == false ? "未使用" : "已使用";
                lblCardStatus.Text = item.ReplyMessageId == null ? "未回覆" : "已回覆";
                lblReceiveTime.Text = item.ReceiveTime == null ? "" : item.ReceiveTime.Value.ToString("yyyy/MM/dd HH:mm");
                lblCardLink.Text = "https://gift.17life.com/g/"+item.AccessCode.ToString().ToLower();
            }
        }

        protected void giftView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "Show":
                    if (this.ViewMgmGiftListGet != null)
                    {
                        string CommandArgument = e.CommandArgument.ToString();
                        this.ViewMgmGiftListGet(this, new DataEventArgs<string>(CommandArgument));
                    }
                    break;
                default:
                    break;
            }
        }

        protected void GiftUpdateHandler(int pageNumber)
        {
            if (this.ViewMgmGiftListPageChanged != null)
            {
                this.ViewMgmGiftListPageChanged(this, new DataEventArgs<int>(pageNumber));
            }
        }

        protected int GetGiftCount()
        {
            DataEventArgs<int> e = new DataEventArgs<int>(0);
            if (ViewMgmGiftListGetCount != null)
            {
                ViewMgmGiftListGetCount(this, e);
            }
            return e.Data;
        }

        #region method
        public string GetReceiveAccount(int? receive_id)
        {
            string account = string.Empty;
            int receiveId = receive_id.HasValue ? receive_id.Value : 0;
            if (receiveId != 0)
            {
                //先取得Email
                Member MemberGetbyUniqueId= mp.MemberGetbyUniqueId(receiveId);
                //有資料時
                if (MemberGetbyUniqueId.IsLoaded)
                {
                    //username包含@mobile ->帳號查mobile_member
                    if (MemberGetbyUniqueId.UserName.Contains("@mobile"))
                    {
                        MobileMember MobileMember = mp.MobileMemberGet(receiveId);
                        if (MobileMember.IsLoaded)
                        {
                            account = MobileMember.MobileNumber;
                        }
                    }
                    else
                    {
                        account = MemberGetbyUniqueId.UserName;
                    }
                }
            }
            return account;
        }

        public static string GetQuickInfo(int giftId)
        {

            string result = string.Empty;
            var mgmGiftVersion = mgmp.GiftVersionGetByGiftiId(giftId);

            if (mgmGiftVersion.IsLoaded)
            {
                result = mgmGiftVersion.QuickInfo;
            }

            return result;
        }

        public void SetMgmGiftList(ViewMgmGiftBacklistCollection viewMgmGiftBacklist)
        {
            divgiftSearchList.Visible = true;
            //giftSearch.Visible = false;
            giftSearch.DataSource = viewMgmGiftBacklist;
            giftSearch.DataBind();
        }

        public void SetViewMgmGiftBacklist(ViewMgmGiftCollection viewMgmGiftList,string giftId,string account)
        {
            lbldMemberAccount.Text = account;
            lbldGiftId.Text = giftId;
            divgiftViewList.Visible = true;
            giftViewArea.Visible = true;
            giftView.DataSource = viewMgmGiftList;
            giftView.DataBind();
        }

        #endregion
    }
}
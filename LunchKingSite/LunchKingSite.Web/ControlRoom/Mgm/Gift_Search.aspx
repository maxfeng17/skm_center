﻿<%@ Page Language="C#" MasterPageFile="~/ControlRoom/backend.master" AutoEventWireup="true" 
    CodeBehind="Gift_Search.aspx.cs" Inherits="LunchKingSite.Web.ControlRoom.Mgm.Gift_Search" %>

<%@ Import Namespace="LunchKingSite.BizLogic.Component" %>
<%@ Import Namespace="LunchKingSite.Core" %>
<%@ Import Namespace="LunchKingSite.DataOrm" %>
<%@ Import Namespace="LunchKingSite.WebLib.Views" %>
<%@ Register Src="../Controls/Pager.ascx" TagName="Pager" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <script type="text/javascript" src="../../Tools/js/jquery-1.7.2.min.js"></script>
    <script type="text/javascript" src="../../Tools/js/jquery-ui.1.8.min.js"></script>
    <script type="text/javascript" src="../../Tools/ckeditor/jquery.query-2.1.7.js"></script>
    <script type="text/javascript" src="../../Tools/js/jquery.maxlength-min.js"></script>
    <script type="text/javascript" src="../../Tools/js/jquery.ui.datepicker-zh-TW.js"></script>
    <script type="text/javascript" src="../../Tools/js/jquery.blockUI.js"></script>
    <link type="text/css" href="../../Tools/js/css/ui-lightness/jquery-ui-1.8rc3.custom.css" rel="stylesheet" />
    <link href="../../Themes/PCweb/css/ppon.css" type="text/css" rel="Stylesheet" />
    <link href="../../Themes/PCweb/plugin/font-awesome/font-awesome.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../Tools/ckeditor/ckeditor.js"></script>
    <script type="text/javascript" src="../../Tools/js/json2.js"></script>
    <script type="text/javascript" src="../../Tools/js/jquery.numeric.js"></script>
    <script type="text/javascript" src="../../Tools/js/jquery.checkboxtree.js"></script>
    <script type="text/javascript" src="//html2canvas.hertzen.com/build/html2canvas.js"></script>
    <link type="text/css" href="../../Tools/js/jquery.checkboxtree.css" rel="stylesheet" />
    <style type="text/css">
        .drag-drop-upload {
            position: relative;
            border: 2px dashed #a89f9f;
        }
        .drag-drop-upload-hover {
            border-color: #4D4D4D;
        }
        .drag-drop-upload-ok {
            border-color: transparent;
        }
        .drag-drop-upload-ok p {
            visibility: hidden;
        }
        .tab-option label {
            font-size: small;
        }
    </style>
    <script type="text/javascript">
        var b_editor;
        function setupdatepicker(from, to) {
            var dates = $('#' + from + ', #' + to).datepicker({
                changeMonth: true,
                numberOfMonths: 3,
                onSelect: function (selectedDate) {
                    var option = this.id == from ? "minDate" : "maxDate";
                    var instance = $(this).data("datepicker");
                    var date = $.datepicker.parseDate(instance.settings.dateFormat || $.datepicker._defaults.dateFormat, selectedDate, instance.settings);
                    dates.not(this).datepicker("option", option, date);
                }
            });
        }

        $(document).ready(function () {
            setupdatepicker('tbOS', 'tbOE');
        });

        function Close() {
            $("#<%=giftViewArea.ClientID%>").hide();
            return false;
        }
     </script>
</asp:Content>

<asp:Content ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   <fieldset id="searchArea" runat="server">
    <legend>送禮查詢</legend>
    會員帳號：<asp:TextBox ID="txtMemberInfo" runat="server" ClientIDMode="Static" maxlength="30" size="30" placeholder="會員帳號"/>
    禮物編號：<asp:TextBox ID="txtSequenceNumber" runat="server" ClientIDMode="Static" maxlength="6" size="6" placeholder="禮物編號"/>
    訂單短標：<asp:TextBox ID="txtProductName" runat="server" ClientIDMode="Static" maxlength="50" size="50" placeholder="訂單短標"/><br /><br />
    聯絡電話：<asp:TextBox ID="txtMobile" runat="server" ClientIDMode="Static" maxlength="10" size="10" placeholder="聯絡電話"/> 送禮時間：<asp:TextBox ID="tbOS" CssClass="date" runat="server" ClientIDMode="Static" maxlength="10" size="10" placeholder=""/> ~ <asp:TextBox ID="tbOE" CssClass="date" runat="server" ClientIDMode="Static" maxlength="10" size="10" placeholder=""/>
    <asp:Button ID="btnSearch" runat="server" Text="查詢" OnClick="btnSearch_Click"/>
    <hr>
    <asp:Panel ID="divgiftSearchList" runat="server" Visible="false">
        <asp:GridView ID="giftSearch" runat="server"  OnRowCommand="giftView_RowCommand" OnRowDataBound="giftSearch_RowDataBound"
            GridLines="Horizontal" ForeColor="Black" CellPadding="4" BorderWidth="1px" BorderStyle="None"
            BorderColor="#DEDFDE" BackColor="White" EmptyDataText="無符合的資料" AutoGenerateColumns="false"
            Font-Size="Small" Width="1400px">
            <FooterStyle BackColor="#CCCC99"></FooterStyle>
            <RowStyle BackColor="#F7F7DE" Height="20px" HorizontalAlign="Center"></RowStyle>
            <SelectedRowStyle BackColor="#CE5D5A" ForeColor="White" Font-Bold="True"></SelectedRowStyle>
            <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Center"></PagerStyle>
            <HeaderStyle BackColor="#6B696B" ForeColor="White" Font-Bold="True" Height="20px"
                HorizontalAlign="Center"></HeaderStyle>
            <Columns>
                <asp:TemplateField HeaderText="送禮會員帳號" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="10%">
                    <ItemTemplate>
                        <asp:HyperLink ID="lblMemberAccount" runat="server"></asp:HyperLink>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="聯絡電話" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="10%">
                    <ItemTemplate>
                        <asp:Label ID="lblMobile" runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="送禮會員名稱" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="8%">
                    <ItemTemplate>
                        <asp:Label ID="lblName" runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="禮物編號" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5%">
                    <ItemTemplate>
                        <asp:LinkButton ID="lblGiftId" runat="server" CommandName="Show"></asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="訂單短標" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="30%">
                    <ItemTemplate>
                        <asp:Label ID="lblTitle" runat="server" ForeColor="Red"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="送禮憑證數量" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="10%">
                    <ItemTemplate>
                        <asp:Label ID="lblCount" runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="訂單編號" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="15%">
                    <ItemTemplate>
                        <asp:HyperLink ID="lblOrderId" runat="server"></asp:HyperLink>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="送禮時間" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="20%">
                    <ItemTemplate>
                        <asp:Label ID="lblSendTime" runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
            <uc1:Pager ID="ucPager" runat="server" PageSize="10" ongetcount="GetGiftCount" onupdate="GiftUpdateHandler"></uc1:Pager>
     </asp:Panel>
    </fieldset>

   <br /><br />

   <fieldset id="giftViewArea" runat="server" Visible="false">
    <legend>送禮詳細資訊</legend>
     送禮者帳號:<asp:Label ID="lbldMemberAccount" runat="server"></asp:Label> <asp:ImageButton ID="imageClose" ImageUrl="~/images/close.gif" ImageAlign="Right" OnClientClick="return Close()" runat="server"></asp:ImageButton>
     <br />
     禮物編號:<asp:Label ID="lbldGiftId" runat="server"></asp:Label>
    <asp:Panel ID="divgiftViewList" runat="server" Visible="false">

        <asp:GridView ID="giftView" runat="server" OnRowDataBound="giftView_RowDataBound"
            GridLines="Horizontal" ForeColor="Black" CellPadding="4" BorderWidth="1px" BorderStyle="None"
            BorderColor="#DEDFDE" BackColor="White" EmptyDataText="無符合的資料" AutoGenerateColumns="false"
            Font-Size="Small" Width="1400px">
            <FooterStyle BackColor="#CCCC99"></FooterStyle>
            <RowStyle BackColor="#F7F7DE" Height="20px" HorizontalAlign="Center"></RowStyle>
            <SelectedRowStyle BackColor="#CE5D5A" ForeColor="White" Font-Bold="True"></SelectedRowStyle>
            <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Center"></PagerStyle>
            <HeaderStyle BackColor="#6B696B" ForeColor="White" Font-Bold="True" Height="20px"
                HorizontalAlign="Center"></HeaderStyle>
            <Columns>
                <asp:TemplateField HeaderText="收禮者帳號" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="10%">
                    <ItemTemplate>
                        <asp:HyperLink ID="lblReceiveAccount" runat="server"></asp:HyperLink>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="收禮會員名稱" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="10%">
                    <ItemTemplate>
                        <asp:Label ID="lblReceiveName" runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="禮物狀態" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="10%">
                    <ItemTemplate>
                        <asp:Label ID="lblAccept" runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="領取時間" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="15%">
                    <ItemTemplate>
                        <asp:Label ID="lblReceiveTime" runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="憑證編號" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="10%">
                    <ItemTemplate>
                        <asp:HyperLink ID="lblCouponNumber" runat="server"></asp:HyperLink>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="憑證狀態" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="7%">
                    <ItemTemplate>
                        <asp:Label ID="lblStatus" runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="卡片狀態" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="7%">
                    <ItemTemplate>
                        <asp:Label ID="lblCardStatus" runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="卡片連結" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="41%">
                    <ItemTemplate>
                        <asp:Label ID="lblCardLink" runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
     </asp:Panel>   
    </fieldset>

</asp:Content>
﻿using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.Core.Enumeration;
using System.Text.RegularExpressions;

namespace LunchKingSite.Web.ControlRoom.Mgm
{
    public partial class GiftBanner : RolePage, IGiftBannerView
    {
        #region props
        private GiftBannerPresenter _presenter;

        private static IMGMProvider mgm = ProviderFactory.Instance().GetProvider<IMGMProvider>();
        public GiftBannerPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        public int PageSize
        {
            get { return ucPager.PageSize; }
            set { ucPager.PageSize = value; }
        }

        public int CurrentPage
        {
            get
            {
                return ucPager.CurrentPage;
            }
        }

        public int Id
        {
            get
            {
                int id = int.TryParse(txtId.Value, out id) ? id : 0;
                return id;
            }
        }

        public string TxtTitle
        {
            get
            {
                return txtTitle.Text;
            }
        }

        public string Content
        {
            get
            {
                return txtContent.Value;
            }
        }

        public string ImageUrl
        {
            get
            {
                return txtImageUrl.FileName;
            }
        }
        public FileUpload Image
        {
            get
            {
                return txtImageUrl;
            }
        }


        public string TextColor
        {
            get
            {
                return txtColor.Text;
            }
        }

        public int ShowType
        {
            get
            {
                return rblShowType.SelectedIndex;
            }
        }

        public bool Online
        {
            get
            {
                return rblOnLine.SelectedIndex == 0 ? false : true;
            }
        }

        public string SearchTitle
        {
            get
            {
                return searchTitle.Text;
            }
        }

        public int SearchShowType
        {
            get
            {
                var result = searchArea.Controls.OfType<RadioButton>().SingleOrDefault(x => x.GroupName == "searchShowType" && x.Checked);

                switch (result.ID)
                {
                    case "rbNone":
                        return 0;
                    case "rbOne":
                        return 1;
                    default:
                        return -1;
                }

            }

        }

        public int SearchOnline
        {
            get
            {
                var result = searchArea.Controls.OfType<RadioButton>().SingleOrDefault(x => x.GroupName == "searchOnline" && x.Checked);

                switch (result.ID)
                {
                    case "rbNoOnline":
                        return 0;
                    case "rbOnline":
                        return 1;
                    default:
                        return -1;
                }

            }
        }

        public int Searchbtn
        {
            get;
            set;
        }

        public string HasGiftOn
        {
            get
            {
                var result = mgm.MgmGiftHeaderByShowType(1);
                if (result.IsLoaded)
                {
                    return result.Id.ToString();
                }
                else
                {
                    return "0";
                }
            }
            set
            {
                hasGiftOn.Value = value;
            }
        }

        
        public string NoGiftOn
        {
            get
            {
                var result = mgm.MgmGiftHeaderByShowType(0);
                if (result.IsLoaded)
                {
                    return result.Id.ToString();
                }
                else
                {
                    return "0";
                }
            }
            set
            {
                noGiftOn.Value = value;
            }
        }

    




        #endregion

        #region event
        public event EventHandler Search;
        public event EventHandler<DataEventArgs<string>> Save;
        public event EventHandler<DataEventArgs<int>> MgmGiftHeaderListGet;
        public event EventHandler<DataEventArgs<int>> MgmGiftHeaderListGetCount;
        public event EventHandler<DataEventArgs<int>> MgmGiftHeaderListPageChanged;
        public event EventHandler<DataEventArgs<int>> MgmGiftHeaderListDelete;
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                IntialControls();
                Presenter.OnViewInitialized();
            }
            _presenter.OnViewLoaded();
        }

        protected void giftBanner_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.DataItem is MgmGiftHeader)
            {
                int OnLine = 0;

                MgmGiftHeader item = (MgmGiftHeader)e.Row.DataItem;
                Button btnEdit = ((Button)e.Row.FindControl("btnEdit"));
                Label lblTitle = ((Label)e.Row.FindControl("lblTitle"));
                Label lblContent = ((Label)e.Row.FindControl("lblContent"));
                Label lblImageUrl = ((Label)e.Row.FindControl("lblImageUrl"));
                Label lblTxtColor = ((Label)e.Row.FindControl("lblTxtColor"));
                Label lblShowType = ((Label)e.Row.FindControl("lblShowType"));
                Label lblOnLine = ((Label)e.Row.FindControl("lblOnLine"));
                Label lblCreateTime = ((Label)e.Row.FindControl("lblCreateTime"));
                Button btnDelete = ((Button)e.Row.FindControl("btnDelete"));
                btnEdit.CommandArgument = item.Id.ToString();
                lblTitle.Text = item.Title;
                lblContent.Text = item.Content;
                lblImageUrl.Text = item.ImageUrl;
                lblTxtColor.Text = item.TextColor;
                lblShowType.Text = CheckType(item.ShowType);

                OnLine = item.OnLine == true ? 1 : 0;

                lblOnLine.Text = Helper.GetEnumDescription((MgmGiftHeaderOnline)OnLine);
                lblCreateTime.Text = item.CreatedTime.ToString("yyyy/MM/dd HH:mm");
                btnDelete.CommandArgument = item.Id.ToString();

            }
        }

        protected void giftBanner_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "UPD":
                    if (this.MgmGiftHeaderListGet != null)
                    {
                        int id = int.TryParse(e.CommandArgument.ToString(), out id) ? id : 0;
                        this.MgmGiftHeaderListGet(this, new DataEventArgs<int>(id));
                    }
                    break;
                case "DEL":
                    if (this.MgmGiftHeaderListGet != null)
                    {
                        int id = int.TryParse(e.CommandArgument.ToString(), out id) ? id : 0;
                        this.MgmGiftHeaderListDelete(this, new DataEventArgs<int>(id));
                    }
                    break;
                default:
                    break;
            }
        }


        public void MgmGiftHeaderEdit(MgmGiftHeader item)
        {
            divGiftBannerList.Visible = false;
            divGiftBannerEdit.Visible = true;

            txtId.Value = item.Id.ToString();
            txtTitle.Text = item.Title;
            txtContent.Value = item.Content;
            blah.ImageUrl = "/Images/MGM/header/"+item.ImageUrl;
            txtColor.Text = item.TextColor;
            rblShowType.SelectedValue = item.ShowType.ToString();
            rblOnLine.SelectedValue = item.OnLine == true ? "1" : "0";
            noGiftOn.Value = NoGiftOn;
            hasGiftOn.Value = HasGiftOn;
            mode.Value = "Edit";
        }

        private void IntialControls()
        {
            Dictionary<int, string> onLine = new Dictionary<int, string>();
            foreach (var item in Enum.GetValues(typeof(MgmGiftHeaderOnline)))
            {
                onLine[(int)item] = Helper.GetEnumDescription((MgmGiftHeaderOnline)item);
            }

            rblOnLine.DataSource = onLine;
            rblOnLine.DataTextField = "Value";
            rblOnLine.DataValueField = "Key";
            rblOnLine.DataBind();
            rblOnLine.SelectedIndex = 0;

            Dictionary<int, string> showType = new Dictionary<int, string>();
            foreach (var item in Enum.GetValues(typeof(MgmGiftHeaderShowType)))
            {
                showType[(int)item] = Helper.GetEnumDescription((MgmGiftHeaderShowType)item);
            }

            rblShowType.DataSource = showType;
            rblShowType.DataTextField = "Value";
            rblShowType.DataValueField = "Key";
            rblShowType.DataBind();
            rblShowType.SelectedIndex = 0;


            rbAllGift.Checked = true;
            rbAll.Checked = true;
        }
        //搜尋
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            if (this.Search != null)
            {
                Searchbtn = 1;
                this.Search(this, e);
            }
            ucPager.ResolvePagerView(CurrentPage, true);
        }

        //新增
        protected void btnAdd_Click(object sender, EventArgs e)
        {
            divGiftBannerEdit.Visible = true;
            divGiftBannerList.Visible = false;
            txtId.Value = string.Empty;
            txtTitle.Text = string.Empty;
            txtContent.Value = string.Empty;
            txtColor.Text = string.Empty;
            blah.ImageUrl = "";
            rblOnLine.SelectedIndex = 0;
            rblShowType.SelectedIndex = 0;
            noGiftOn.Value = NoGiftOn;
            hasGiftOn.Value = HasGiftOn;
            mode.Value = "Add";
        }

        //取消
        protected void btnClose_Click(object sender, EventArgs e)
        {
            divGiftBannerEdit.Visible = false;
            divGiftBannerList.Visible = true;
        }

        //儲存
        protected void btnSave_Click(object sender, EventArgs e)
        { 
            if (this.Save != null)
            {
                string path = Server.MapPath("~/Images/MGM/header/");
                int id = int.TryParse(txtId.Value, out id) ? id : 0;
                string Data = id + "," + path;
                int width = 0;
                int height = 0;
                if (id != 0)
                {                    
                    bool fileOK = false;
                    if (txtImageUrl.HasFile)
                    {
                        const string rule = "^[0-9a-zA-Z_]*$";
                        string fileExtension = System.IO.Path.GetExtension(txtImageUrl.FileName).ToLower();
                        string[] allowedExtensions = { ".png", ".jpg" };
                        for (int i = 0; i < allowedExtensions.Length; i++)
                        {
                            if (fileExtension == allowedExtensions[i])
                            {
                                fileOK = true;
                            }
                        }
                        if (!Regex.IsMatch(txtImageUrl.FileName.Replace(".png", "").Replace(".jpg", ""), rule))
                        {
                            fileOK = false;
                            ClientScript.RegisterStartupScript(GetType(), "alerterR", "alert('檔案名稱僅限英數字或\"_\"，不得包含空白字元');", true);
                        }
                        //取得圖片寬度
                        using (var image = System.Drawing.Image.FromStream(txtImageUrl.PostedFile.InputStream))
                        {
                            width = image.Width;
                            height = image.Height;
                        }

                        if (ShowType == 0 && width != 640 || ShowType == 0 && height != 560)
                        {
                            fileOK = false;
                            ClientScript.RegisterStartupScript(GetType(), "alerterR", "alert('圖片尺寸應為640x560，不符規定');", true);
                        }
                        else if (ShowType == 1 && width != 640 || ShowType == 1 && height != 152)
                        {
                            fileOK = false;
                            ClientScript.RegisterStartupScript(GetType(), "alerterR", "alert('圖片尺寸640x152，不符規定');", true);
                        }

                        if (fileOK)
                        {
                            this.Save(this, new DataEventArgs<string>(Data));
                        }
                    }
                    else
                    {
                        this.Save(this, new DataEventArgs<string>(Data));
                    }
                }
                else
                {
                    bool fileOK = false;
                    if (txtImageUrl.HasFile)
                    {
                        const string rule = "^[0-9a-zA-Z_]*$";
                        string fileExtension = System.IO.Path.GetExtension(txtImageUrl.FileName).ToLower();
                        string[] allowedExtensions = { ".png", ".jpg" };
                        for (int i = 0; i < allowedExtensions.Length; i++)
                        {
                            if (fileExtension == allowedExtensions[i])
                            {
                                fileOK = true;
                            }
                        }
                        if (!Regex.IsMatch(txtImageUrl.FileName.Replace(".png", "").Replace(".jpg", ""), rule))
                        {
                            fileOK = false;
                            ClientScript.RegisterStartupScript(GetType(), "alerterR", "alert('檔案名稱僅限英數字或\"_\"，不得包含空白字元');", true);                        
                        }

                        //取得圖片寬度
                        using (var image = System.Drawing.Image.FromStream(txtImageUrl.PostedFile.InputStream))
                        {
                            width = image.Width;
                            height = image.Height;
                        }

                        if (ShowType == 0 && width != 640 || ShowType == 0 && height != 560)
                        {
                            fileOK = false;
                            ClientScript.RegisterStartupScript(GetType(), "alerterR", "alert('圖片尺寸應為640x560，不符規定');", true);
                        }
                        else if (ShowType == 1 && width != 640 || ShowType == 1 && height != 152)
                        {
                            fileOK = false;
                            ClientScript.RegisterStartupScript(GetType(), "alerterR", "alert('圖片尺寸640x152，不符規定');", true);
                        }

                        if (fileOK)
                        { 
                            this.Save(this, new DataEventArgs<string>(Data));
                        }
                    }
                    else
                    {
                        ClientScript.RegisterStartupScript(GetType(), "alerterR", "alert('請選擇上傳圖檔');", true);
                    }
                }
            }
        }


        protected void BannerUpdateHandler(int pageNumber)
        {
            if (this.MgmGiftHeaderListPageChanged != null)
            {
                this.MgmGiftHeaderListPageChanged(this, new DataEventArgs<int>(pageNumber));
            }
        }

        protected int GetBannerCount()
        {
            DataEventArgs<int> e = new DataEventArgs<int>(0);
            if (MgmGiftHeaderListGetCount != null)
            {
                MgmGiftHeaderListGetCount(this, e);
            }
            return e.Data;
        }

        #region method
        public void SetMgmHeaderList(MgmGiftHeaderCollection mgmGiftBannerList)
        {
            divGiftBannerList.Visible = true;
            divGiftBannerEdit.Visible = false;
            giftBanner.DataSource = mgmGiftBannerList;
            giftBanner.DataBind();
            ucPager.ResolvePagerView(CurrentPage, true);
        }

        public string CheckType(int showType)
        {
            switch (showType)
            {
                case 0:
                    return "無禮物";
                case 1:
                    return "有禮物";
                default:
                    return "例外狀況";
            }
        }

        #endregion
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.DataOrm;
using LunchKingSite.Core.Component;
using LunchKingSite.Core;
using LunchKingSite.Core.Enumeration;
using LunchKingSite.WebLib.Views;
using System.Data;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using LunchKingSite.Core.UI;

namespace LunchKingSite.Web.ControlRoom
{
    public partial class serviceworker : RolePage
    {
        #region enum
        /// <summary>
        /// 轉件項目
        /// </summary>
        private enum ServiceWorkType
        {
            /// <summary>
            /// 商品
            /// </summary>
            Production = 0,
            /// <summary>
            /// 好康
            /// </summary>
            Ppon = 1,
            /// <summary>
            /// 退貨
            /// </summary>
            Returned = 2,
        }

        #endregion

        #region Property

        IMemberProvider im = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        IOrderProvider op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
        IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        private string all = "全部";
        public string SalesManNameList = string.Empty;
        public string[] SalesmanNameArray
        {
            set
            {
                var rtn = string.Empty;
                foreach (var s in value)
                {
                    if (rtn == string.Empty)
                    {
                        rtn = string.Format("'{0}'", s);
                    }
                    else
                    {
                        rtn = string.Format("{0},'{1}'", rtn, s);
                    }
                }
                SalesManNameList = rtn;
            }
        }

        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request["name"]))
            {
                txtEmail.Text = Request["name"];
                LoadData(1);
            }
            if (!Page.IsPostBack)
            {
                //建構UI
                SetUI();
                
                //因為預設並未抓資料, 故Pager 也不要SetPaging
                gridPager.SetPagingWhenPageLoad = false;
            }
        }

        protected void btnSend_Click(object sender, EventArgs e)
        {
            #region Data Check

            if (ddlCategoryA.SelectedIndex == 0 || ddlSubCategoryA.SelectedIndex == 0) 
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), string.Empty, "alert('請選擇分類！')", true);
                return;
            }
            if (rdlWorkType.SelectedIndex < 0)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), string.Empty, "alert('嘿！尚未點選轉件項目！')", true);
                return;
            }

            #endregion

            #region 新增 ServiceMessage

            ServiceMessage sm = new ServiceMessage();
            sm.Email = txtIEmail.Text;
            sm.Phone = txtIPhone.Text;
            sm.Name = txtIName.Text;
            //原本 Type=0 是好康, 1是品生活; 整併完以後要設什麼? 可能得討論..
            sm.Type = 0;
            sm.Category = Convert.ToInt32(ddlCategoryA.SelectedValue);
            sm.SubCategory = Convert.ToInt32(ddlSubCategoryA.SelectedValue);
            sm.OrderId = txtIOrder.Text;
            if (!string.IsNullOrEmpty(rdlWorkType.SelectedValue))
                sm.WorkType = Convert.ToInt32(rdlWorkType.SelectedValue);
            if (!string.IsNullOrEmpty(rblPriority.SelectedValue)) { sm.Priority = Convert.ToInt32(rblPriority.SelectedValue); }

            sm.Message = (txtIMessage.Text.Trim().Replace("\t", string.Empty).Replace("\r", string.Empty).Replace("\n", string.Empty) == string.Empty) ? string.Empty : txtIMessage.Text.Trim();
            sm.MessageType = ddlItype.SelectedItem.Text;
            sm.Status = ddlIStatus.SelectedItem.Value;
            sm.WorkUser = Page.User.Identity.Name;
            sm.CreateId = Page.User.Identity.Name;
            sm.CreateTime = DateTime.Now;
            sm.ModifyId = Page.User.Identity.Name;
            sm.ModifyTime = DateTime.Now;
            sm.RemarkSeller = (txtSellerRemark.Text.Trim().Replace("\t", string.Empty).Replace("\r", string.Empty).Replace("\n", string.Empty) == string.Empty) ? string.Empty : txtSellerRemark.Text.Trim();
            im.ServiceMessageSet(sm);

            #endregion

            #region 新增ServiceLog

            ServiceLog _serviceLog = new ServiceLog();
            _serviceLog.SendTo = sm.Email;           //對應到ServiceMessage的Email
            _serviceLog.RefX = sm.Id;     //FK, 對應到ServiceMessage的ID
            _serviceLog.RemarkSeller = sm.RemarkSeller;          //回報廠商備註
            _serviceLog.CreateId = sm.CreateId;
            _serviceLog.CreateTime = sm.CreateTime;
            im.ServiceLogSet(_serviceLog);

            #endregion

            #region clean layout
            txtIEmail.Text = "";
            txtIPhone.Text = "";
            txtIName.Text = "";
            txtIOrder.Text = "";
            txtSellerRemark.Text = "";
            ddlCategoryA.SelectedIndex = 0;
            ddlSubCategoryA.SelectedIndex = 0;
            txtIMessage.Text = "";

            #endregion

            ScriptManager.RegisterStartupScript(this, this.GetType(), string.Empty, "alert('已經新增一筆客服')", true);

            LoadData(1);
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            LoadData(1);
        }

        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ViewServiceMessageDetail RowView = (ViewServiceMessageDetail)e.Row.DataItem;
                Member mm = im.MemberGetByUserEmail(RowView.Email);

                // 狀態
                HyperLink hkStatus = (HyperLink)e.Row.FindControl("hkStatus");
                hkStatus.Text = this.getStatus(RowView.Status);
                hkStatus.NavigateUrl = string.Format("servicedetail.aspx?id={0}", RowView.Id);

                //緊急程度
                Label lbPriority = (Label)e.Row.FindControl("lbPriority");
                switch (RowView.Priority)
                {
                    case (int)ServiceConfig.replyPriority.Normal:
                        lbPriority.Text = Resources.Localization.PriorityNormal;
                        break;
                    case (int)ServiceConfig.replyPriority.High:
                        lbPriority.Text = Resources.Localization.PriorityHigh;
                        break;
                    case (int)ServiceConfig.replyPriority.Extreme:
                        lbPriority.Text = "<font color='red'>" + Resources.Localization.PriorityExtreme + "</font>";
                        break;
                    default:
                        break;
                }

                //姓名
                HyperLink hkName = (HyperLink)e.Row.FindControl("hkName");
                hkName.Text = RowView.Name;
                hkName.NavigateUrl = "../controlroom/User/ServiceIntegrate.aspx?name=" + mm.UserName;

                //Email
                HyperLink hkEmail = (HyperLink)e.Row.FindControl("hkEmail");
                hkEmail.Text = RowView.Email;
                hkEmail.NavigateUrl = "../controlroom/user/users_edit.aspx?username=" + mm.UserName;

                // 加入訂單編號連結
                Guid order_guid = ProviderFactory.Instance().GetProvider<IOrderProvider>().OrderGet(DataOrm.Order.Columns.OrderId, RowView.OrderId).Guid;
                HyperLink hkOrderId = (HyperLink)e.Row.FindControl("hkOrderId");
                hkOrderId.Text = RowView.OrderId;
                hkOrderId.NavigateUrl = "../controlroom/Order/order_detail.aspx?oid=" + order_guid;

                //檔名
                HyperLink itemname = (HyperLink) e.Row.FindControl("itemname");
                itemname.Text = RowView.ItemName;
                itemname.NavigateUrl = "../controlroom/ppon/setup.aspx?bid=" + RowView.Guid;

                // 處理人員
                Label lblModifyId = (Label)e.Row.FindControl("lblModifyId");
                if (!string.IsNullOrEmpty(RowView.WorkUser))
                    lblModifyId.Text = RowView.WorkUser.Substring(0, RowView.WorkUser.IndexOf('@'));

                // 館別
                Label lblType = (Label)e.Row.FindControl("lblType");
                switch ((ServiceMessageType)RowView.Type)
                {
                    case ServiceMessageType.Ppon:
                        lblType.Text = "P好康";
                        break;
                    case ServiceMessageType.Hideal:
                        lblType.Text = "HiDeal";
                        break;
                    default:
                        break;
                }

                // 轉件項目
                Label lblWorkType = (Label)e.Row.FindControl("lblWorkType");
                if (RowView.WorkType != null)
                {
                    switch ((ServiceWorkType)RowView.WorkType)
                    {
                        case ServiceWorkType.Production:
                            lblWorkType.Text = "商品";
                            break;
                        case ServiceWorkType.Ppon:
                            lblWorkType.Text = "好康";
                            break;
                        case ServiceWorkType.Returned:
                            lblWorkType.Text = "退貨";
                            break;
                        default:
                            break;
                    }
                }

                //業務姓名
                Label lblSalesId = (Label)e.Row.FindControl("lblSalesId");
                if (RowView.DealEmpName != null)
                {
                    lblSalesId.Text = RowView.DealEmpName;
                }
            }
        }

        protected void btnbump_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtIEmail.Text))
            {
                Member m = MemberFacade.GetMember(txtIEmail.Text);
                txtIName.Text = m.DisplayName;
                txtIPhone.Text = m.Mobile;
            }
        }

        protected void exportExcel_Click(object sender, EventArgs e)
        {
            DropDownList ddl = (DropDownList)gridPager.FindControl("ddlPages");
            if(ddl.SelectedIndex > -1)
                ExportData(Convert.ToInt32(ddl.SelectedValue));
        }

        protected void ddlCategoryS_SelectedIndexChanged(object sender, EventArgs e)
        {
            BuildServiceCategory(ddlSubCategoryS, Convert.ToInt32(ddlCategoryS.SelectedValue));
        }

        protected void ddlCategoryA_SelectedIndexChanged(object sender, EventArgs e)
        {
            BuildServiceCategory(ddlSubCategoryA, Convert.ToInt32(ddlCategoryA.SelectedValue));
        }

        #endregion

        private string getStatus(string status) {
            switch (status)
            {
                case ServiceConfig.statusCode.Wait:
                    return ServiceConfig.statusString.Wait;
                case ServiceConfig.statusCode.Process:
                    return ServiceConfig.statusString.Process;
                case ServiceConfig.statusCode.Complete:
                    return ServiceConfig.statusString.Complete;
                default:
                    return string.Empty;
            }
        }

        private string getPriority(int pri) {
            switch (pri) { 
                case (int)ServiceConfig.replyPriority.Normal:
                    return ServiceConfig.replyPriorityString.Normal;
                case (int)ServiceConfig.replyPriority.High:
                    return ServiceConfig.replyPriorityString.High;
                case (int)ServiceConfig.replyPriority.Extreme:
                    return ServiceConfig.replyPriorityString.Extreme;
                default:
                    return string.Empty;
            }
        }

        protected void UpdateHandler(int pageNumber)
        {

            LoadData(pageNumber);
        }

        protected int RetrieveTransCount()
        {
            return ProviderFactory.Instance().GetProvider<IMemberProvider>().ViewServiceMessageDetailGetCountForFilter(GetFilter());
        }

        private void LoadData(int pagenumber)
        {
            ViewServiceMessageDetailCollection smcCol = ProviderFactory.Instance().GetProvider<IMemberProvider>().ViewServiceMessageDetailGetListForFilter
                (pagenumber, 50, ViewServiceMessageDetail.Columns.CreateTime + " desc", GetFilter());

            FilterItemName(smcCol);

            GridView1.DataSource = smcCol;
            GridView1.DataBind();
            gridPager.ResolvePagerView(pagenumber, true);
        }

        private void FilterItemName(ViewServiceMessageDetailCollection Detail)
        {
            foreach (var o in Detail.Where(x => !string.IsNullOrEmpty(x.LabelIconList)))
            {
                string fastget = "";
                foreach (var s in o.LabelIconList.Split(','))
                {
                    if (s == ((int)DealLabelSystemCode.ArriveIn24Hrs).ToString() ||
                        s == ((int)DealLabelSystemCode.ArriveIn72Hrs).ToString())
                    {
                        fastget += string.Format("[{0}]", SystemCodeManager.GetDealLabelCodeName(Convert.ToInt32(s)));
                    }
                }
                o.ItemName = fastget + o.ItemName;
            }
        }

        private void ExportData(int pagenumber)
        {
            ViewServiceMessageDetailCollection smcCol = ProviderFactory.Instance().GetProvider<IMemberProvider>().ViewServiceMessageDetailGetListForFilter
                (pagenumber, 0, ViewServiceMessageDetail.Columns.CreateTime + " desc", GetFilter());
            if (smcCol.Count() > 0)
            {
                #region 新增試算表

                string changeLine = "\n";
                Workbook workbook = new HSSFWorkbook();
                Sheet sheet = workbook.CreateSheet("會員回覆報表");

                #region 設定欄位名稱

                List<string> headers = new List<string>(new string[]
                {
                    string.Empty, "檔號", "訂單編號", "申請時間", "結檔時間", "廠商名稱", "商品名稱", "收件人", "收件人電話", "配送地址", "數量", "金額"
                    , "問題", "回報廠商備註", "廠商回覆日", "廠商回覆欄", "出件人員", "物流公司", "運單編號", "退換貨連絡電子信箱", "業務"
                });
                Row headRow = sheet.CreateRow(0);
                for (int i = 0; i < headers.Count(); i++) 
                {
                    headRow.CreateCell(i).SetCellValue(headers[i]);
                }

                #endregion

                #region 設定內容

                int rowIdx = 1;
                foreach (ViewServiceMessageDetail smc in smcCol) 
                {
                    ViewOrderDetailItemCollection odCols = !string.IsNullOrEmpty(smc.OrderGuid.ToString()) ? (ViewOrderDetailItemCollection)op.ViewOrderDetailItemGetList(
                        (Guid)smc.OrderGuid) : new ViewOrderDetailItemCollection();
                    //排除運費/ 購物金折抵等等的項目 (OrderDetailStatus==0)
                    IEnumerable<ViewOrderDetailItem> odCol = odCols.Where(x => x.OrderDetailStatus.Equals((int)OrderDetailStatus.None));

                    string value = string.Empty;    //多筆資料時用的暫存字串
                    int columnIdx = 0;

                    Row itemRow = sheet.CreateRow(rowIdx);
                    //首行留空白
                    itemRow.CreateCell(columnIdx).SetCellValue(string.Empty); columnIdx++;
                    //檔號
                    itemRow.CreateCell(columnIdx).SetCellValue(smc.UniqueId.ToString()); columnIdx++;
                    //訂單編號
                    itemRow.CreateCell(columnIdx).SetCellValue(smc.OrderId); columnIdx++;
                    //申請時間(來信時間)
                    itemRow.CreateCell(columnIdx).SetCellValue(((DateTime)smc.CreateTime).ToString("yyyy-MM-dd")); columnIdx++;
                    //結檔時間
                    itemRow.CreateCell(columnIdx).SetCellValue(smc.OrderTimeE.Equals(null) ? string.Empty : ((DateTime)smc.OrderTimeE).ToString("yyyy-MM-dd")); columnIdx++;
                    //廠商名稱
                    itemRow.CreateCell(columnIdx).SetCellValue(smc.SellerName); columnIdx++;
                    //商品名稱
                    //fastget如果快速運貨則提示，否則空字串
                    string fastget = "";
                    if (!string.IsNullOrEmpty(smc.LabelIconList))
                    {
                        foreach (var s in smc.LabelIconList.Split(','))
                        {
                            if (s == ((int)DealLabelSystemCode.ArriveIn24Hrs).ToString() ||
                                s == ((int)DealLabelSystemCode.ArriveIn72Hrs).ToString())
                            {
                                fastget += string.Format("[{0}]", SystemCodeManager.GetDealLabelCodeName(Convert.ToInt32(s)));
                            }
                        }
                    }
                    value = fastget;
                    foreach (ViewOrderDetailItem od in odCol)
                    {
                        value += od.OrderDetailItemName.Replace("&nbsp;", " ").Replace("&nbsp", " ");
                        //加上換行字元. 但是如果是最後一個元素, 則不要加
                        value += od != odCol.Last() ? changeLine : string.Empty;
                    }
                    itemRow.CreateCell(columnIdx).SetCellValue(value); columnIdx++;
                    //收件人
                    itemRow.CreateCell(columnIdx).SetCellValue(smc.MemberName); columnIdx++;
                    //收件人電話
                    itemRow.CreateCell(columnIdx).SetCellValue(smc.Mobile); columnIdx++;
                    //配送地址
                    itemRow.CreateCell(columnIdx).SetCellValue(smc.DeliveryAddress); columnIdx++;
                    //數量
                    value = string.Empty;
                    foreach (ViewOrderDetailItem od in odCol)
                    {
                        value += od.OrderDetailQuantity.ToString();
                        //加上換行字元. 但是如果是最後一個元素, 則不要加
                        value += od != odCol.Last() ? changeLine : string.Empty;
                    }
                    itemRow.CreateCell(columnIdx).SetCellValue(value); columnIdx++;

                    //金額
                    itemRow.CreateCell(columnIdx).SetCellValue(smc.Amount != null ? ((int)smc.Amount).ToString() : string.Empty); columnIdx++;
                    //問題
                    itemRow.CreateCell(columnIdx).SetCellValue(smc.Message); columnIdx++;
                    //回報廠商備註
                    itemRow.CreateCell(columnIdx).SetCellValue(smc.RemarkSeller); columnIdx++;
                    //廠商回覆日
                    itemRow.CreateCell(columnIdx).SetCellValue(string.Empty); columnIdx++;
                    //廠商回覆欄
                    itemRow.CreateCell(columnIdx).SetCellValue(string.Empty); columnIdx++;
                    //出件人員
                    itemRow.CreateCell(columnIdx).SetCellValue(!string.IsNullOrEmpty(smc.ModifyId) ? smc.ModifyId.Substring(0, smc.ModifyId.IndexOf('@')) : string.Empty); columnIdx++;
                    //物流公司
                    itemRow.CreateCell(columnIdx).SetCellValue(smc.CompanyName); columnIdx++;
                    //運單編號
                    itemRow.CreateCell(columnIdx).SetCellValue(smc.ShipNo); columnIdx++;
                    //退換貨聯絡電子信箱
                    itemRow.CreateCell(columnIdx).SetCellValue(smc.ReturnedPersonEmail); columnIdx++;
                    //業務
                    itemRow.CreateCell(columnIdx).SetCellValue(smc.DealEmpName); columnIdx++;
                    //換行
                    CellStyle cs = workbook.CreateCellStyle();
                    cs.WrapText = true;
                    for (int i = 0; i < headers.Count(); i++) { 
                        if (itemRow.GetCell(i).ToString().Contains(changeLine)) {
                            itemRow.GetCell(i).CellStyle = cs;
                            int rows = itemRow.ToString().Split(changeLine).Count();
                            itemRow.HeightInPoints = rows * sheet.DefaultRowHeight / 20;
                        }                    
                    }
                    rowIdx++;
                }

                #endregion

                #region 調整欄寬

                for (int i = 0; i < headers.Count(); i++)
                {
                    sheet.AutoSizeColumn(i);
                }
                //首行無資料，欄寬會變很窄，故隨便給個值，勿鞭
                sheet.SetColumnWidth(0, 2560);

                #endregion

                HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment; filename={0}", HttpUtility.UrlEncode("ServiceReport.xls")));
                workbook.Write(HttpContext.Current.Response.OutputStream);

                #endregion
            }
        }

        private string[] GetFilter()
        {
            List<string> ll = new List<string>();
            if (ddlStatus.SelectedItem.Text != all)
            {
                ll.Add(ViewServiceMessageDetail.Columns.Status + "=" + ddlStatus.SelectedItem.Value);
            }

            if (ddltype.SelectedItem.Text != all)
            {
                ll.Add(ViewServiceMessageDetail.Columns.MessageType + "=" + ddltype.SelectedItem.Value);
            }

            if (!string.IsNullOrEmpty(txtName.Text))
            {
                ll.Add(ViewServiceMessageDetail.Columns.Name + "=" + txtName.Text);
            }

            if (!string.IsNullOrEmpty(txtSellerName.Text)) {
                ll.Add(ViewServiceMessageDetail.Columns.SellerName + "=" + txtSellerName.Text);
            }

            if (!string.IsNullOrEmpty(txtorder.Text))
            {
                ll.Add(ViewServiceMessageDetail.Columns.OrderId + "=" + txtorder.Text);
            }

            if (!string.IsNullOrEmpty(txtEmail.Text))
            {
                ll.Add(ViewServiceMessageDetail.Columns.Email + "=" + txtEmail.Text);
            }

            if (!string.IsNullOrEmpty(txtPhone.Text))
            {
                ll.Add(ViewServiceMessageDetail.Columns.Phone + "=" + txtPhone.Text);
            }

            if (!string.IsNullOrEmpty(rdlTypeS.SelectedValue)) 
            {
                ll.Add(ViewServiceMessageDetail.Columns.Type + "=" + rdlTypeS.SelectedValue);
            }

            if (ddlCategoryS.SelectedIndex.EqualsNone(0))
            {
                ll.Add(ViewServiceMessageDetail.Columns.Category + "=" + ddlCategoryS.SelectedItem.Value);
            }

            if (ddlSubCategoryS.SelectedIndex.EqualsNone(0))
            {
                ll.Add(ViewServiceMessageDetail.Columns.SubCategory + "=" + ddlSubCategoryS.SelectedValue);
            }

            if (!string.IsNullOrEmpty(ddlWorkType.SelectedValue))
            {
                ll.Add(ViewServiceMessageDetail.Columns.WorkType + "=" + ddlWorkType.SelectedValue);
            }

            if (!string.IsNullOrEmpty(txtModify.Text))
            {
                ll.Add(ViewServiceMessageDetail.Columns.WorkUser + "=" + txtModify.Text);
            }

            DateTime dt;
            if (DateTime.TryParse(tbOS.Text, out dt) && DateTime.TryParse(tbOE.Text, out dt))
            {
                ll.Add(ViewServiceMessageDetail.Columns.CreateTime + " between " + tbOS.Text + " and " + tbOE.Text);
            }
            if (DateTime.TryParse(tbDealTime1.Text, out dt) && DateTime.TryParse(tbDealTime2.Text, out dt))
            {
                ll.Add(ViewServiceMessageDetail.Columns.ModifyTime + " between " + tbDealTime1.Text + " and " + tbDealTime2.Text);
            }

            if (!string.IsNullOrEmpty(rblSelectPriority.SelectedValue) && rblSelectPriority.SelectedValue != all) {
                ll.Add(ViewServiceMessageDetail.Columns.Priority + "=" + rblSelectPriority.SelectedValue);
            }

            if (cbRemarkSeller.Checked)
            {
                ll.Add(ViewServiceMessageDetail.Columns.RemarkSeller + " <> " + string.Empty);
            }

            return ll.ToArray();
        }

        protected void SetSalesmanNameArray()
        {
            IHumanProvider _humanProv = ProviderFactory.Instance().GetProvider<IHumanProvider>();
            var viewEmpCol = _humanProv.ViewEmployeeCollectionGetByDepartment(EmployeeDept.C000);
            SalesmanNameArray = viewEmpCol.Select(emp => emp.Email).ToArray();
        }

        #region UI Setup

        private void SetUI()
        {
            BuildPriorityRbl(false, ref rblPriority);
            BuildPriorityRbl(true, ref rblSelectPriority);
            BuildServiceCategory(ddlCategoryS, null);
            BuildServiceCategory(ddlCategoryA, null);
            SetSalesmanNameArray();
        }

        private void BuildPriorityRbl(bool isSelectAll, ref RadioButtonList rbl)
        {
            rbl.Items.Clear();
            //篩選條件用的下拉選單, 不需加入 "全部" 的選項
            if (isSelectAll) { rbl.Items.Add(new ListItem(all, all)); }
            rbl.Items.Add(new ListItem(ServiceConfig.replyPriorityString.Normal, ((int)ServiceConfig.replyPriority.Normal).ToString()));
            rbl.Items.Add(new ListItem(ServiceConfig.replyPriorityString.High, ((int)ServiceConfig.replyPriority.High).ToString()));
            rbl.Items.Add(new ListItem("<font color='red'>" + ServiceConfig.replyPriorityString.Extreme + "</font>", ((int)(ServiceConfig.replyPriority.Extreme)).ToString()));
            //非篩選條件用的下拉選單, 需附上預設值
            if (!isSelectAll) { rbl.SelectedValue = ((int)ServiceConfig.replyPriority.Normal).ToString(); }
        }

        protected void BuildServiceCategory(DropDownList ddl, int? categoryId)
        {
            ServiceMessageCategoryCollection serviceCates = im.ServiceMessageCategoryCollectionGetListByParentId(categoryId);
            ddl.Items.Clear();
            ddl.Items.Add(new ListItem("請選擇", "0"));
            foreach (var cate in serviceCates) 
            {
                ddl.Items.Add(new ListItem(cate.CategoryName, cate.CategoryId.ToString()));
            }
        }

        #endregion

    }
}
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Diagnostics;
using System.IO;
using System.Net.Configuration;
using System.Reflection;
using System.Web.Configuration;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.WebModules;
using log4net;
using System.Linq;
using System.Runtime.Versioning;
using System.Web.Services;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using System.Globalization;
using System.Net;
using Newtonsoft.Json;

namespace LunchKingSite.Web.ControlRoom
{
    public partial class Default : RolePage
    {
        private ViewModel _model = new ViewModel();

        protected ViewModel Model
        {
            get { return _model; }
        }

        DataTable asmTbl;
        private static ILog logger = LogManager.GetLogger("controlroom");

        #region props

        static ISysConfProvider cp = ProviderFactory.Instance().GetConfig();

        public ISysConfProvider SystemConfig
        {
            get { return cp; }
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                SetupDbInfo();
                if (User.IsInRole(MemberRoles.Administrator.ToString()))
                {
                    SetupDbInfo2();
                    try
                    {
                        SetupWebInfo();
                    }
                    catch (Exception ex)
                    {
                        logger.Warn("SetupWebInfo 失敗" + ex);
                    }
                    try
                    {
                        SetupElmahInfo();
                    }
                    catch (Exception ex)
                    {
                        logger.Warn("SetupElmahInfo 失敗" + ex);
                    }
                }
                SetupSystemInfo();
                SetupHttpRiuntimeInfo();
                AllAssemblyDetail();
            }
        }

        private void SetupDbInfo()
        {
            Model.CurrentSchemaVersion = ProviderFactory.Instance().GetProvider<ICmsProvider>().SchemaGetVersion();
            AssemblyDatabaseSchemaAttribute attrib =
                (AssemblyDatabaseSchemaAttribute)
                    AssemblyDatabaseSchemaAttribute.GetCustomAttribute(Assembly.GetAssembly(typeof(AssemblyDatabaseSchemaAttribute)),
                        typeof(AssemblyDatabaseSchemaAttribute));
            Model.AssemblyRequiredSchemaVersion = attrib.SchemaVersion;
            Model.CurrentBranchSchemaVersion = ProviderFactory.Instance().GetProvider<ICmsProvider>().SchemaGetBranchVersion();
            AssemblyBranchDatabaseSchemaAttribute attrib2 =
                (AssemblyBranchDatabaseSchemaAttribute)
                    AssemblyDatabaseSchemaAttribute.GetCustomAttribute(Assembly.GetAssembly(typeof(AssemblyBranchDatabaseSchemaAttribute)),
                        typeof(AssemblyBranchDatabaseSchemaAttribute));
            if (attrib2 != null)
            {
                Model.AssemblyBranchRequiredSchemaVersion = attrib2.SchemaVersion;
            }

            dbw.Visible =
                new Version(Model.CurrentSchemaVersion).Equals(new Version(Model.AssemblyRequiredSchemaVersion)) == false ||
                String.Equals(Model.CurrentBranchSchemaVersion, Model.AssemblyBranchRequiredSchemaVersion) == false;
        }

        private void SetupDbInfo2()
        {
            var connsb = new SqlConnectionStringBuilder(ConfigurationManager.ConnectionStrings["Default"].ConnectionString);
            Model.Database = connsb.InitialCatalog + "(" + connsb.DataSource + ")";
            Model.DbMode = "";
        }

        private void SetupWebInfo()
        {
            {
                string sql = "select top 1 create_time from member with(nolock) order by create_time desc";
                DateTime? time = DB.Query().ExecuteScalar<DateTime?>(sql);
                if (time != null)
                {
                    Model.MemberCreatedTime = DateTimeHelper.DateStringFromNow(time.Value);
                }
            }
            {
                string sql = @"
                    select top 1 member.create_time from member with(nolock) inner join member_link with(nolock)
                    on member_link.user_id=member.unique_id and member_link.external_org=0 order by unique_id desc                
                ";
                DateTime? time = DB.Query().ExecuteScalar<DateTime?>(sql);
                if (time != null)
                {
                    Model.PezMemberCreatedTime = DateTimeHelper.DateStringFromNow(time.Value);
                }
            }
            {
                string sql = @"
                    select top 1 member.create_time from member with(nolock) inner join member_link with(nolock)
                    on member_link.user_id=member.unique_id and member_link.external_org=1 order by unique_id desc                
                ";
                DateTime? time = DB.Query().ExecuteScalar<DateTime?>(sql);
                if (time != null)
                {
                    Model.FbMemberCreatedTime = DateTimeHelper.DateStringFromNow(time.Value);
                }
            }
            {
                string sql = @"
                    select top 1 member.create_time from member with(nolock) inner join member_link with(nolock)
                    on member_link.user_id=member.unique_id and member_link.external_org=2 order by unique_id desc                
                ";
                DateTime? time = DB.Query().ExecuteScalar<DateTime?>(sql);
                if (time != null)
                {
                    Model.Member17CreatedTime = DateTimeHelper.DateStringFromNow(time.Value);
                }
            }
            //最後一次信用卡交易完成時間
            {
                string sql = @"
                    select top 1 create_time from [order] with(nolock)
                    where [order].order_status & 8 > 0 and [order].order_status & 512 = 0
                    order by create_time desc
                ";
                DateTime? time = DB.Query().ExecuteScalar<DateTime?>(sql);
                if (time != null)
                {
                    Model.OrderCreatedTime = DateTimeHelper.DateStringFromNow(time.Value);
                }
            }
            //最後一次信用卡交易完成時間 網際威信
            {
                //string sql = @"
                //    select top 1 pt.trans_time from payment_transaction pt with(nolock)
                //    where pt.payment_type = 1 and trans_type = 0 and result = 0 and amount > 0
                //    and pt.api_provider = 10
                //    order by pt.trans_time desc
                //";
                //DateTime? time = DB.Query().ExecuteScalar<DateTime?>(sql);
                //if (time != null)
                //{
                //    Model.OrderByCreditCardHiTrustCVV2CreatedTime = DateTimeHelper.DateStringFromNow(time.Value);
                //}
                Model.OrderByCreditCardHiTrustCVV2CreatedTime = "...";
            }
            //最後一次信用卡交易完成時間 藍星
            {
                //string sql = @"
                //    select top 1 pt.trans_time from payment_transaction pt with(nolock)
                //    where pt.payment_type = 1 and trans_type = 0 and result = 0 and amount > 0
                //    and pt.api_provider = 12
                //    order by pt.trans_time desc
                //";
                //DateTime? time = DB.Query().ExecuteScalar<DateTime?>(sql);
                //if (time != null)
                //{
                //    Model.OrderByCreditCardNewebCvv2CreatedTime = DateTimeHelper.DateStringFromNow(time.Value);
                //}
                Model.OrderByCreditCardNewebCvv2CreatedTime = "...";
            }
            //最後一次信用卡交易完成時間 藍星分期
            {
                //string sql = @"
                //    select top 1 pt.trans_time from payment_transaction pt with(nolock)
                //    where pt.payment_type = 1 and trans_type = 0 and result = 0 and amount > 0
                //    and pt.api_provider = 14
                //    order by pt.trans_time desc
                //";
                //DateTime? time = DB.Query().ExecuteScalar<DateTime?>(sql);
                //if (time != null)
                //{
                //    Model.OrderByCreditCardNewebInstallmentCreatedTime = DateTimeHelper.DateStringFromNow(time.Value);
                //}
                Model.OrderByCreditCardNewebInstallmentCreatedTime = "...";
            }

            {
                string sql = @"
                    select top 1 create_time from cash_trust_log ctl with(nolock)
                    where ctl.credit_card > 0
                    order by ctl.create_time desc
                ";
                DateTime? time = DB.Query().ExecuteScalar<DateTime?>(sql);
                if (time != null)
                {
                    Model.OrderByCreditCardCreatedTime = DateTimeHelper.DateStringFromNow(time.Value);
                }
            }
        }

        private void SetupElmahInfo()
        {
            ElmahError elmahError = ProviderFactory.Instance().GetProvider<IElmahProvider>()
                .ElmahGet(SqlDateTime.MinValue.Value, SqlDateTime.MaxValue.Value);
            if (elmahError.IsLoaded)
            {
                Model.ElmahError = elmahError.Message;
                Model.ElmahLink = ResolveUrl("~/oops.axd/detail?id=" + elmahError.ErrorId);
                Model.ElmahTime = DateTimeHelper.DateStringFromNow(elmahError.TimeUtc.ToLocalTime());
            }
        }

        private void SetupSystemInfo()
        {
            Model.Server = Environment.MachineName;

            if (User.Identity.IsAuthenticated == false)
            {
                Model.User = "未登入";
            }
            else
            {
                Model.User = string.Format("{0}({1})", User.Identity.Name, Helper.GetClientIP());
            }

            Model.StartTime = DateTimeHelper.DateStringFromTimeSpan(DateTime.Now - StartupModule.StartTime);
            Model.StartExactTime = StartupModule.StartTime.ToString("yyyy:MM/dd HH:mm:ss");
            Model.SessionMode = Session.Mode.ToString();

            MachineKeySection section = (MachineKeySection)
                ConfigurationManager.GetSection("system.web/machineKey");
            Model.MKey = String.Format("{0}|{1}|{2}|{3}...|{4}...",
                section.CompatibilityMode,
                section.Decryption,
                section.ValidationAlgorithm,
                section.ValidationKey.Substring(0, 4),
                section.DecryptionKey.Substring(0, 4));
            Model.DllReleaseTime =
                File.GetLastWriteTime(Path.Combine(System.Web.HttpRuntime.BinDirectory, "LunchKingSite.Web.dll"))
                    .ToString("yyyy/MM/dd HH:mm:ss");
        }

        private void SetupHttpRiuntimeInfo()
        {
            {
                HttpRuntimeSection section = (HttpRuntimeSection)
                    ConfigurationManager.GetSection("system.web/httpRuntime");

                Model.HttpRuntimeMinFreeThreads = section.MinFreeThreads;
                Model.MinLocalRequestFreeThreads = section.MinLocalRequestFreeThreads;
            }
            {
                ProcessModelSection section = (ProcessModelSection)
                    ConfigurationManager.GetSection("system.web/processModel");
                Model.ProcessModelMaxIoThreads = section.MaxIOThreads;
                Model.ProcessModelMinIoThreads = section.MinIOThreads;

                Model.ProcessModelMaxWorkerThreads = section.MaxWorkerThreads;
                Model.ProcessModelMinWorkerThreads = section.MinWorkerThreads;
                Model.ProcessorCount = Environment.ProcessorCount;
                Model.ConcurrentRequests = Model.ProcessModelMaxIoThreads * Environment.ProcessorCount -
                    Model.HttpRuntimeMinFreeThreads;
            }
            {
                ConnectionManagementSection section = (ConnectionManagementSection)
                    ConfigurationManager.GetSection("system.net/connectionManagement");
                Model.ConnectionManagementMaxConnections = new List<KeyValuePair<string, int>>();

                foreach (ConnectionManagementElement item in section.ConnectionManagement)
                {
                    Model.ConnectionManagementMaxConnections.Add(
                        new KeyValuePair<string, int>(item.Address, item.MaxConnection));
                }
            }
        }


        protected void AllAssemblyDetail()
        {
            asmTbl = new DataTable("Assembly");
            asmTbl.Columns.Add("Assembly", typeof(string));
            asmTbl.Columns.Add("Version", typeof(string));
            asmTbl.Columns.Add("FileVersion", typeof(string));
            asmTbl.Columns.Add("Build Date", typeof(string));
            asmTbl.Columns.Add("Size", typeof(string));
            asmTbl.Columns.Add("Build Type", typeof(string));
            asmTbl.Columns.Add("Global", typeof(string));
            asmTbl.Columns.Add("NetVersion", typeof(string));

            foreach (System.Reflection.Assembly a in AppDomain.CurrentDomain.GetAssemblies())
            {

                if (a.IsDynamic || a is System.Reflection.Emit.AssemblyBuilder || a.GetName().Version.ToString() == "0.0.0.0" || a.CodeBase.IndexOf("WINDOWS") > 0)
                    continue;
                DataRow row = asmTbl.NewRow();
                try
                {
                    row["Assembly"] = System.IO.Path.GetFileName(a.CodeBase.Replace("file:///", ""));
                    row["FileVersion"] = FileVersionInfo.GetVersionInfo(a.Location).FileVersion;
                    row["Version"] = a.GetName().Version;
                    row["Build Date"] = System.IO.File.GetLastWriteTime(a.Location).ToString("yyyy/MM/dd hh:MM:ss");
                    row["Size"] = String.Format("{0:n0}", new System.IO.FileInfo(a.Location).Length);
                    row["Build Type"] = GetAssemblyDebugOrRelease(a).Substring(0, 1);
                    row["Global"] = a.GlobalAssemblyCache ? "X" : "";

                    TargetFrameworkAttribute frameworkAttr = null;
                    try
                    {
                        frameworkAttr = (TargetFrameworkAttribute)
                        a.GetCustomAttributes(typeof(TargetFrameworkAttribute)).First();
                    }
                    catch (Exception ex)
                    {
                        row["NetVersion"] = ex.Message;
                    }
                    row["NetVersion"] = frameworkAttr == null ? "" : frameworkAttr.FrameworkDisplayName;
                }
                catch (Exception ex)
                {
                    //TODO 如果沒機會攔到，此攔截應刪除
                    lbMsg.Text = String.Format("載入 dll {0} 發生問題, ex= {1}", a.Location, ex.Message);
                }
                asmTbl.Rows.Add(row);
            }

            asmTbl.DefaultView.Sort = "Assembly";

            gvAssembly.DataSource = asmTbl;
            gvAssembly.DataBind();
        }

        protected string GetAssemblyDebugOrRelease(Assembly a)
        {
            string result = "Release";
            object[] attribs = a.GetCustomAttributes(typeof(DebuggableAttribute), false);
            if (attribs.Length > 0)
            {
                DebuggableAttribute debuggableAttribute = attribs[0] as DebuggableAttribute;
                if (debuggableAttribute != null)
                {
                    result = debuggableAttribute.IsJITOptimizerDisabled ? "Debug" : "Release";
                }
            }
            return result;
        }

        public class ViewModel
        {
            public ViewModel()
            {
                ElmahError = "";
                ElmahTime = "";

                MemberCreatedTime = "";
                PezMemberCreatedTime = "";
                FbMemberCreatedTime = "";
                Member17CreatedTime = "";
                OrderCreatedTime = "";
                OrderByCreditCardCreatedTime = "";

                User = "";
                Server = "";
                StartTime = "";
                StartExactTime = "";

                DbMode = "";
                Database = "";
                CurrentSchemaVersion = "";
                AssemblyRequiredSchemaVersion = "";
            }

            public string ElmahError { get; set; }
            public string ElmahLink { get; set; }
            public string ElmahTime { get; set; }

            public string MemberCreatedTime { get; set; }
            public string PezMemberCreatedTime { get; set; }
            public string FbMemberCreatedTime { get; set; }
            public string Member17CreatedTime { get; set; }
            public string OrderCreatedTime { get; set; }
            public string OrderByCreditCardCreatedTime { get; set; }
            public string OrderByCreditCardHiTrustCVV2CreatedTime { get; set; }
            public string OrderByCreditCardNewebCvv2CreatedTime { get; set; }
            public string OrderByCreditCardNewebInstallmentCreatedTime { get; set; }

            public string User { get; set; }
            public string Server { get; set; }
            public string StartTime { get; set; }
            public string StartExactTime { get; set; }

            public string DbMode { get; set; }
            public string Database { get; set; }
            public string CurrentSchemaVersion { get; set; }
            public string AssemblyRequiredSchemaVersion { get; set; }
            public int CurrentBranchSchemaVersion { get; set; }
            public int AssemblyBranchRequiredSchemaVersion { get; set; }
            public string DllReleaseTime { get; set; }

            public string SessionMode { get; set; }
            public string MKey { get; set; }
            //HttpRuntime
            public int HttpRuntimeMinFreeThreads { get; set; }
            public int ProcessModelMinWorkerThreads { get; set; }
            public int ProcessModelMaxWorkerThreads { get; set; }
            public int ProcessModelMinIoThreads { get; set; }
            public int ProcessModelMaxIoThreads { get; set; }
            public int MinLocalRequestFreeThreads { get; set; }
            public List<KeyValuePair<string, int>> ConnectionManagementMaxConnections { get; set; }
            public int ConcurrentRequests { get; set; }
            public int ProcessorCount { get; set; }
        }

        internal class DateTimeHelper
        {
            public static string DateStringFromTimeSpan(TimeSpan span)
            {
                return Helper.GetSmartTimeSpanString(span);
            }

            public static string DateStringFromNow(DateTime dt)
            {
                TimeSpan span = DateTime.Now - dt;
                return DateStringFromTimeSpan(span);
            }


        }

        public static bool IsChinese(string text)
        {
            var charArray = text.ToCharArray();
            bool result = false;

            foreach (var character in charArray)
            {
                var cat = char.GetUnicodeCategory(character);


                if (cat != UnicodeCategory.OtherLetter)
                {
                    continue;
                }


                result = true;
                break;
            }
            return result;
        }
        [WebMethod]
        public static dynamic Quicklunch(string keyword)
        {
            if (keyword != null)
            {
                keyword = keyword.Trim();
            }
            Guid guid;
            int id;
            List<dynamic> result = new List<dynamic>();
            if (Helper.IsNumeric(keyword))
            {
                id = int.Parse(keyword);
                if (keyword.Length == 8)
                {
                    IViewPponDeal deal = ViewPponDealManager.DefaultManager.ViewPponDealGetByUniqueId(id);
                    if (deal != null && deal.BusinessHourGuid != Guid.Empty)
                    {
                        result.Add(new
                        {
                            title = deal.UniqueId + " " + deal.ItemName,
                            url = string.Concat("/ControlRoom/ppon/setup.aspx?bid=", deal.BusinessHourGuid.ToString()),
                            info = JsonConvert.SerializeObject(deal, Formatting.Indented)
                        });
                    }
                }
                else if (keyword.Length == 10)
                {
                    if (RegExRules.CheckMobile(keyword))
                    {
                        MobileMember mm = MemberFacade.GetMobileMember(keyword);
                        if (mm.IsLoaded)
                        {
                            Member mem = MemberFacade.GetMember(keyword);
                            if (mem.IsLoaded)
                            {
                                result.Add(new
                                {
                                    title = "會員編輯:" + mem.UserName,
                                    url = string.Concat("/controlroom/User/users_edit.aspx?username=", mem.UserName)
                                });
                            }
                        }
                    }
                    else
                    {
                        Member mem = MemberFacade.GetMember(id);
                        if (mem.IsLoaded)
                        {
                            result.Add(new
                            {
                                title = "會員編輯:" + mem.UserName,
                                url = string.Concat("/controlroom/User/users_edit.aspx?username=", mem.UserName)
                            });
                        }
                    }
                }
            }

            if (RegExRules.CheckEmail(keyword))
            {
                Member mem = MemberFacade.GetMember(keyword);
                if (mem.IsLoaded)
                {
                    result.Add(new
                    {
                        title = "會員編輯:" + mem.UserName,
                        url = string.Concat("/controlroom/User/users_edit.aspx?username=", mem.UserName)
                    });
                }
            }


            if (Guid.TryParse(keyword, out guid))
            {
                IViewPponDeal deal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(guid, true);
                IViewPponDeal dealInRedis = ProviderFactory.Instance().GetProvider<ICacheProvider>().Get<ViewPponDeal>("vpd::" + guid, cp.CacheItemCompress);
                IViewPponDeal dealInDB = ProviderFactory.Instance().GetDefaultProvider<IPponProvider>().ViewPponDealGetByBusinessHourGuid(guid);
                if (deal != null && deal.BusinessHourGuid != Guid.Empty)
                {
                    result.Add(new
                    {
                        id = deal.BusinessHourGuid,
                        title = deal.UniqueId + " " + deal.ItemName,
                        url = string.Concat("/ControlRoom/ppon/setup.aspx?bid=", deal.BusinessHourGuid.ToString()),
                        info = JsonConvert.SerializeObject(deal, Formatting.Indented),
                        info2 = JsonConvert.SerializeObject(dealInRedis, Formatting.Indented),
                        info3 = JsonConvert.SerializeObject(dealInDB, Formatting.Indented),
                    });
                }
            }

            if (result.Count == 0 && IsChinese(keyword))
            {
                // 試著從短標找檔次
                var deals = ViewPponDealManager.DefaultManager.ViewPponDealGetList(true, true);
                foreach (var deal in deals)
                {
                    if (deal.ItemName.Contains(keyword))
                    {
                        result.Add(new
                        {
                            title = deal.UniqueId + " " + deal.ItemName,
                            url = string.Concat("/ControlRoom/ppon/setup.aspx?bid=", deal.BusinessHourGuid.ToString())
                        });
                    }
                }
            }

            IOrderProvider op = ProviderFactory.Instance().GetDefaultProvider<IOrderProvider>();
            if (result.Count == 0)
            {
                var o = op.OrderGetByOrderId(keyword);
                if (o.IsLoaded)
                {
                    result.Add(new
                    {
                        title = o.OrderId,
                        url = string.Concat("/controlroom/Order/order_detail.aspx?oid=", o.Guid)
                    });
                }
            }
            if (result.Count == 0 && Guid.TryParse(keyword, out guid))
            {
                var o = op.OrderGet(guid);
                if (o.IsLoaded)
                {
                    result.Add(new
                    {
                        title = o.OrderId,
                        url = string.Concat("/controlroom/Order/order_detail.aspx?oid=", o.Guid)
                    });
                }
            }

            if (result.Count > 20)
            {
                result = result.Take(20).ToList();
            }

            if (result.Count > 0)
            {
                return new
                {
                    success = true,
                    result
                };
            }
            else
            {
                return new
                {
                    success = false
                };
            }
        }

        [WebMethod]
        public static dynamic ReloadCacheAtRedis(string bid)
        {
            var cache = ProviderFactory.Instance().GetDefaultProvider<ICacheProvider>();
            var pp = ProviderFactory.Instance().GetDefaultProvider<IPponProvider>();
            Guid guid;
            if (Guid.TryParse(bid, out guid) == false)
            {
                return new
                {
                    success = false
                };
            }
            var deal = pp.ViewPponDealGetByBusinessHourGuid(guid);
            if (deal.IsLoaded == false)
            {
                return new
                {
                    success = false
                };
            }
            {
                string cacheKey = "vpd::" + bid;
                cache.Set(cacheKey, deal, new TimeSpan(24, 0, 0), cp.CacheItemCompress);
            }

            foreach (var vc in pp.GetViewComboDealByBid(guid, false))
            {
                var subDeal = pp.ViewPponDealGetByBusinessHourGuid(vc.BusinessHourGuid);
                string cacheKey = "vpd::" + subDeal.BusinessHourGuid;
                cache.Set(cacheKey, subDeal, new TimeSpan(24, 0, 0), cp.CacheItemCompress);
            }

            return new
            {
                success = true
            };
        }
    }
}

﻿using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib;
using System;

namespace LunchKingSite.Web.ControlRoom
{
    public partial class AtmPostPage : RolePage
    {
        private IOrderProvider op;

        protected string NotifyUrl {
            get { return Helper.CombineUrl(ProviderFactory.Instance().GetConfig().SSLSiteUrl, "atmnotify.aspx"); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            CtAtm atm = op.CtAtmGetByOrderId(TextBox1.Text);
            if (atm != null && atm.Si>0)
            {
                string seqno = (Convert.ToInt64(op.CtAtmMessageInformGetMaxSeqno()) + 1).ToString().PadLeft(16, '0');
                string date = (atm.CreateTime.Year - 1911).ToString() + atm.CreateTime.Date.ToString("MMdd");
                CommonFacade.AddAudit(atm.OrderGuid, AuditType.Payment, string.Format("執行模擬訂單：{0}ATM付款成功", TextBox1.Text), User.Identity.Name, true);

                Response.Redirect(WebUtility.GetSiteRoot() + "/controlroom/AtmPostPage.aspx?seqno=" + seqno
                                                                                                    + "&date=" + date
                                                                                                    + "&senq=" + atm.Senq
                                                                                                    + "&amount=" + atm.Amount.ToString().PadLeft(13, '0'));
            }
            else
            {
                Label1.Text = "查無此單ATM紀錄";
            }
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            SimulateRefundResult(TextBox2.Text, true);
        }

        protected void btnRefundFailed_Click(object sender, EventArgs e)
        {
            SimulateRefundResult(TextBox2.Text, false);
        }

        private void SimulateRefundResult(string orderId, bool refundResult)
        {
            DataOrm.Order o = op.OrderGet(DataOrm.Order.Columns.OrderId, orderId);

            if (o != null)
            {
                CtAtmRefund atmRefund = op.CtAtmRefundGetLatest(o.Guid);
                
                if (atmRefund != null && atmRefund.Si > 0)
                {
                    if (refundResult)
                    {
                        atmRefund.Status = (int) AtmRefundStatus.RefundSuccess;
                    }
                    else
                    {
                        atmRefund.Status = (int)AtmRefundStatus.RefundFail;
                    }
                    
                    atmRefund.RefundResponseTime = DateTime.Now;
                    op.CtAtmRefundSet(atmRefund);
                    CommonFacade.AddAudit(atmRefund.OrderGuid, AuditType.Payment, string.Format("執行模擬訂單：{0}ATM退款{1}資料匯入", o.OrderId, refundResult ? "成功" : "失敗"), User.Identity.Name, true);
                    Label2.Text = string.Format("已模擬{0}ATM退款{1}資料匯入，記得去執行AtmClean",
                                                o.OrderId, refundResult ? "成功" : "失敗");

                }
                else
                {
                    Label2.Text = "查無此單ATM退款紀錄";
                }
            }
            else
            {
                Label2.Text = "查無此單";
            }
        }
    }
}
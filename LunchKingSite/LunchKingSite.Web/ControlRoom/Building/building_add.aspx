﻿<%@ Page Language="C#" MasterPageFile="~/ControlRoom/backend.master" AutoEventWireup="true"
    Codebehind="building_add.aspx.cs" Inherits="building_add" %>

<%@ Register Src="../Controls/BizHourBuildingList.ascx" TagName="BizHourBuildingList"
    TagPrefix="uc3" %>
<%@ Register Src="../Controls/BuildingList.ascx" TagName="BuildingList" TagPrefix="uc2" %>
<%@ Register Src="../Controls/Pager.ascx" TagName="Pager" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="System.Web.Extensions"
    Namespace="System.Web.UI" TagPrefix="aspajax" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
    <asp:Literal ID="ljs" runat="server"></asp:Literal>
    <script type="text/javascript">

        function Calculate() {

            $.ajax({
                type: "POST",
                url: "building_add.aspx/CalculateCoordinates",
                data: "{'address':'" + $('[id*=txtAddress]').val() + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    if (response.d != '' && response.d.Key != null) {
                        $('#<%=txtB.ClientID %>').val(response.d.Key);
                        $('#<%=txtA.ClientID %>').val(response.d.Value);
                        DrawMap();
                    }
                    else {
                        alert('沒有座標資料!!');
                    }
                }
            });
        }

        function DrawMap() {

            var latitude = $('#<%=txtB.ClientID %>');
            var longitude = $('#<%=txtA.ClientID %>');

            if ('<%= Config.EnableOpenStreetMap %>'.toLowerCase() == 'true') {
                DrawOpenStreetMap(latitude.val(), longitude.val());
            } else {
                if ('<%= Config.EnableGoogleMapApiV2 %>'.toLowerCase() == 'true') {
                    DrawGoogleMapV2(latitude.val(), longitude.val());
                } else {
                    DrawGoogleMapV3(latitude, longitude);
                }
            }
        }

        function DrawOpenStreetMap(latitude, longitude) {

            $('#map').empty(); // 因OpenStreetMap會一直append新的地圖，必須清空
            map = new OpenLayers.Map("map");
            map.addLayer(new OpenLayers.Layer.OSM());

            var lonLat = new OpenLayers.LonLat(longitude, latitude)
                  .transform(
                    new OpenLayers.Projection("EPSG:4326"), // transform from WGS 1984
                    map.getProjectionObject() // to Spherical Mercator Projection
                  );
            var zoom = 16;

            var markers = new OpenLayers.Layer.Markers("Markers");
            map.addLayer(markers);
            markers.addMarker(new OpenLayers.Marker(lonLat));
            map.setCenter(lonLat, zoom);
            $('#map').css('display', '');
        }

        function DrawGoogleMapV2(latitude, longitude) {

            if (GBrowserIsCompatible()) {
                if (document.getElementById("map") != null) {
                    $('#map').css('display', '');
                    var map = new GMap2(document.getElementById("map"));
                    var geocoder = new GClientGeocoder();
                    map.addControl(new GSmallMapControl());

                    geocoder.getLocations(new GLatLng(latitude, longitude), function (point) {
                        if (point) {
                            map.setCenter(new GLatLng(latitude, longitude), 16);
                            var marker = new GMarker(new GLatLng(latitude, longitude));
                            map.addControl(new GSmallMapControl()); // 小型地圖控制項
                            map.addOverlay(marker);
                            AddMoveCoordinateListener(map, marker);
                        }
                    });
                }
            }
        }

        function AddMoveCoordinateListener(map, marker) {
            GEvent.addListener(map, "click", function (overlay, point) {
                if (point) {
                    if (confirm('確定要移動座標位置嗎?')) {
                        //設定標註座標
                        marker.setLatLng(point);
                        $('#<%=txtA.ClientID %>').val(point.x.toString());
                        $('#<%=txtB.ClientID %>').val(point.y.toString());
                    }
                }
            });
        }

        function DrawGoogleMapV3(latitude, longitude) {

            var myLatlng = new google.maps.LatLng(latitude.val(), longitude.val());
            var mapOptions = {
                zoom: 16,
                center: myLatlng,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            }

            var map = new google.maps.Map(document.getElementById('map'),
              mapOptions);

            var marker = new google.maps.Marker({
                position: myLatlng,
                draggable: true,
                map: map
            });

            google.maps.event.addListener(
                marker,
                'drag',
                function () {
                    latitude.val(marker.position.lat());
                    longitude.val(marker.position.lng());
                }
            );

            $('#map').css('display', '');
        }

        function delete_confirm(e) {
            if (event.srcElement.outerText == "刪除")
                event.returnValue = confirm("確定刪除?");
        }

        document.onclick = delete_confirm;
    </script>
    <table>
        <tr>
            <td colspan="2">
                您正在
                <asp:Label ID="lblMode" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
                路段
                <asp:Label ID="lblBuildingName" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label></td>
            <td colspan="2">
                <asp:Button ID="btnBuildingDelete" runat="server" OnClick="btnBuildingDelete_Click"
                    Text="刪除該路段" /><cc1:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server"
                        ConfirmText="確定刪除該路段？" TargetControlID="btnBuildingDelete">
                    </cc1:ConfirmButtonExtender>
            </td>
        </tr>
        <tr>
            <td>
                所屬區碼</td>
            <td>
                <aspajax:UpdatePanel ID="uPanel3" runat="server">
                    <ContentTemplate>
                        <asp:DropDownList ID="ddlCity" runat="server" AutoPostBack="True" DataTextField="Value"
                            DataValueField="Key" OnSelectedIndexChanged="ddlCity_SelectedIndexChanged">
                        </asp:DropDownList>&nbsp;
                        <asp:DropDownList ID="ddlZone" runat="server" DataTextField="Value" DataValueField="Key">
                        </asp:DropDownList>
                    </ContentTemplate>
                </aspajax:UpdatePanel>
            </td>
            <td>
                路段狀態</td>
            <td>
                <asp:RadioButtonList ID="rbl" runat="server" RepeatDirection="Horizontal" Font-Size="10pt">
                    <asp:ListItem Selected="True" Value="True">上線</asp:ListItem>
                    <asp:ListItem Value="False">下線</asp:ListItem>
                </asp:RadioButtonList></td>
        </tr>
        <tr>
            <td>
                路段名稱</td>
            <td colspan="3">
                <aspajax:UpdatePanel ID="uPanel4" runat="server">
                    <ContentTemplate>
                        <asp:TextBox ID="tbBuildingName" runat="server"></asp:TextBox>
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp; 路段編號&nbsp;
                        <asp:TextBox ID="tbBuildingRank" runat="server"></asp:TextBox>
                    </ContentTemplate>
                </aspajax:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td>
                路段地址</td>
            <td>
                <asp:TextBox ID="tbAddr" runat="server" Width="200px"></asp:TextBox></td>
            <td>
                路段別</td>
            <td>
                <asp:TextBox ID="tbBuildingAddrNum" runat="server" Width="200px"></asp:TextBox></td>
        </tr>
        <tr>
            <td align="center" colspan="4">
                <asp:Button ID="btnBuildingAdd" runat="server" Text="送出" OnCommand="UpdateClick" /></td>
        </tr>
    </table>
    <table>
            <tr>
                <td>
                    用此地址查詢：</td>
                <td>
                    <asp:TextBox ID="txtAddress" runat="server" Width="376px"></asp:TextBox>
                    <input type="button" value="計算座標" onclick="Calculate();" />
                </td>
            </tr>
            <tr>
                <td>
                    經度：</td>
                <td>
                    <asp:TextBox ID="txtA" runat="server" Width="248px"></asp:TextBox></td>
            </tr>
            <tr>
                <td>
                    緯度：</td>
                <td>
                    <asp:TextBox ID="txtB" runat="server" Width="248px"></asp:TextBox>
                    <input type="button" value="畫出地圖" onclick="DrawMap();" />
                </td>
            </tr>
            <tr>
                <td>
                    地圖：
                </td>
                <td>
                    <div id="map" style="width: 600px; height: 500px"></div> <!--此為地圖顯示大小-->
                </td>
            </tr>
        </table>
    <br />
    賣家列表<table>
        <tr>
            <td>
                <asp:Button ID="btnAddSellerInBuilding" runat="server" Text="新增賣家於此路段" />
                <cc1:ModalPopupExtender ID="ModalPopupExtender1" runat="server" BackgroundCssClass="ldnmbg"
                    CancelControlID="btCancelBizHourInBuilding" PopupControlID="PAddBizHourInBuilding"
                    PopupDragHandleControlID="PAddBizHourInBuilding" TargetControlID="btnAddSellerInBuilding"
                    CacheDynamicResults="True" BehaviorID="ModalPopupExtender1" X="10" Y="10">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="PAddBizHourInBuilding" runat="server">
                    <table class="modalPopup">
                        <tr>
                            <td>
                                <aspajax:UpdatePanel ID="UpdatePanel1" runat="server">
                                    <ContentTemplate>
                                        <uc3:BizHourBuildingList ID="BizHourBuildingList2" runat="server" ComparisonType="NOT IN" QueryType="JustSeller" />
                                    </ContentTemplate>
                                </aspajax:UpdatePanel>
                            </td>
                        </tr>
                    </table>
                    <asp:Button ID="btAddBizHourInBuilding" runat="server" Text="增加" OnClick="btAddBizHourInBuilding_Click" />
                    <asp:Button ID="btCancelBizHourInBuilding" runat="server" Text="取消" />
                    <cc1:ConfirmButtonExtender ID="ConfirmButtonExtender3" runat="server" ConfirmText="確定新增？"
                        TargetControlID="btAddBizHourInBuilding">
                    </cc1:ConfirmButtonExtender>
                </asp:Panel>
                <asp:Button ID="btDeleteBizHour" runat="server" OnClick="btDeleteBizHour_Click" Text="刪除賣家於此路段" />
                <cc1:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" ConfirmText="確定刪除？"
                    TargetControlID="btDeleteBizHour">
                </cc1:ConfirmButtonExtender>
                <uc3:BizHourBuildingList ID="BizHourBuildingList1" runat="server" />
            </td>
        </tr>
    </table>
</asp:Content>

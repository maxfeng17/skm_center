﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;
using System.Net;
using System.IO;
using Microsoft.SqlServer.Types;
using System.Web.Services;
using LunchKingSite.Core.Component;
using LunchKingSite.BizLogic.Component;

public partial class building_add : RolePage, IBuildingAddView
{
    public ISysConfProvider Config = ProviderFactory.Instance().GetConfig();
    public event EventHandler<DataEventArgs<UpdatableBuildingInfo>> UpdataBuildingInfo;
    public event EventHandler SelectCityChanged;
    public event EventHandler<DataEventArgs<BuildingDelivery>> UpdateBuildingDelivery;
    public event EventHandler<DataEventArgs<BuildingDelivery>> DeleteBuildingDelivery;
    public event EventHandler DeleteBuilding;

    protected ILocationProvider ilp;

    #region props
    private BuildingAddPresenter _presenter;
    public BuildingAddPresenter Presenter
    {
        set
        {
            this._presenter = value;
            if (value != null)
            {
                this._presenter.View = this;
            }
        }
        get
        {
            return this._presenter;
        }
    }
    public Guid BuildingGuid
    {
        get
        {
            if (!string.IsNullOrEmpty(Request["bid"]))
            {
                try
                {
                    return new Guid(Request.QueryString["bid"]);
                }
                catch
                {
                    return Guid.Empty;
                }
            }
            else
                return Guid.Empty;
        }
    }
    public int SelectCity
    {
        get { return int.Parse(ddlCity.SelectedValue); }
    }
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (BuildingGuid != Guid.Empty)
            {
                btnBuildingAdd.Text = "修改";
                lblMode.Text = "修改";
                btnBuildingAdd.CommandArgument = "Update";
                btnAddSellerInBuilding.Enabled = true;
            }
            else
            {
                btnBuildingAdd.Text = "新增";
                lblMode.Text = "新增";
                btnBuildingAdd.CommandArgument = "Insert";
                btnAddSellerInBuilding.Enabled = false;
            }

            Presenter.OnViewInitialized();
        }
        Presenter.OnViewLoaded();

        if (Config.EnableOpenStreetMap)
        {
            ljs.Text = @"<script type=""text/javascript"" src=""" + ResolveUrl("~/Tools/js/osm/OpenLayers.js") + @"""></script>";
        }
        else
        {
            if (Config.EnableGoogleMapApiV2)
            {
                ljs.Text = @"<script src=""https://maps.google.com/maps?file=api&v=2&key=" + LocationFacade.GetGoogleMapsAPIKey() + @""" type=""text/javascript""></script>";
            }
            else
            {
                ljs.Text = @"<script src=""https://maps.googleapis.com/maps/api/js?v=3&sensor=false&libraries=drawing"" type=""text/javascript""></script>";
            }
        }
    }

    public void SetCityDropDown(Dictionary<int, string> city)
    {
        ddlCity.DataSource = city;
        ddlCity.DataBind();
    }

    public void SetZoneDropDown(Dictionary<int, string> zone)
    {
        ddlZone.DataSource = zone;
        ddlZone.DataBind();
    }

    public void SetBuildingBind(ViewBuildingCity theBuilding)
    {
        lblBuildingName.Text = (!string.IsNullOrEmpty(theBuilding.BuildingName)) ? theBuilding.BuildingName : string.Empty;
        tbBuildingName.Text = (!string.IsNullOrEmpty(theBuilding.BuildingName)) ? theBuilding.BuildingName : string.Empty;
        tbAddr.Text = theBuilding.BuildingStreetName;
        if (theBuilding.ParentId != null)
        {
            ddlCity.SelectedValue = theBuilding.ParentId.Value.ToString();
            ddlZone.SelectedValue = theBuilding.CityId.ToString();
        }
        else
            ddlCity.SelectedValue = theBuilding.CityId.ToString();
        tbBuildingAddrNum.Text = (!string.IsNullOrEmpty(theBuilding.BuildingAddressNumber)) ? theBuilding.BuildingAddressNumber : string.Empty;
        tbBuildingRank.Text = (!string.IsNullOrEmpty(theBuilding.BuildingRank.ToString())) ? theBuilding.BuildingRank.ToString() : string.Empty;
        rbl.Items.FindByValue(theBuilding.BuildingOnline.ToString()).Selected = true;
        string[] s = theBuilding.Coordinate.Split(' ');
        if (s.Length > 2)
        {
            txtB.Text = s[1].Replace("(", "");
            txtA.Text = s[2].Replace(")", "");
        }
    }

    protected string ShowBizHrType(object type)
    {
        return Helper.GetLocalizedEnum(Resources.Localization.ResourceManager, (BusinessHourType)type);
    }

    protected string ShowBizHrOS(object type)
    {
        string carrytype = string.Empty;
        string ordertime = ((DateTime)type).ToShortTimeString();
        switch (((DateTime)type).Day)
        {
            case 1:
                carrytype = Resources.Localization.CarryToday;
                break;
            case 2:
                carrytype = Resources.Localization.CarryTomorrow;
                break;
            default:
                break;
        }
        return carrytype + ordertime;
    }

    protected void UpdateClick(object sender, CommandEventArgs e)
    {
        UpdatableBuildingInfo buildingInfo = new UpdatableBuildingInfo();

        if (e.CommandArgument.ToString() == "Insert")
            buildingInfo.BuildingGuid = Helper.GetNewGuid(buildingInfo);
        else
            buildingInfo.BuildingGuid = BuildingGuid;

        buildingInfo.BuildingName = tbBuildingName.Text.Trim();
        buildingInfo.BuildingStreetName = tbAddr.Text.Trim();
        buildingInfo.BuildingAddressNumber = (!string.IsNullOrEmpty(tbBuildingAddrNum.Text.Trim())) ? tbBuildingAddrNum.Text.Trim() : null;
        buildingInfo.BuildingRank = int.Parse(tbBuildingRank.Text);
        buildingInfo.CityId = !string.IsNullOrEmpty(ddlZone.SelectedValue) ? int.Parse(ddlZone.SelectedValue) : int.Parse(ddlCity.SelectedValue);
        buildingInfo.BuildingOnline = Convert.ToBoolean(rbl.SelectedValue);
        buildingInfo.BuildingAtitude = txtA.Text;
        buildingInfo.BuildingLongitude = txtB.Text;

        if (UpdataBuildingInfo != null)
        {
            UpdataBuildingInfo(this, new DataEventArgs<UpdatableBuildingInfo>(buildingInfo));
            Response.Redirect(string.Format("{0}?bid={1}", "building_add.aspx", buildingInfo.BuildingGuid.ToString()));
        }
    }

    protected void ddlCity_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (this.SelectCityChanged != null)
            SelectCityChanged(this, new EventArgs());
    }

    protected void btnBuildingDelete_Click(object sender, EventArgs e)
    {
        if (((GridView)BizHourBuildingList1.FindControl("gvSellerInBuildingCity")).Rows.Count <= 0)
        {
            if (DeleteBuilding != null)
            {
                DeleteBuilding(this, new EventArgs());
                Response.Redirect("building_list.aspx");
            }
        }
        else
            Response.Write("<script>window.alert('此路段目前有設定賣家，無法刪除!!')</script>");
    }

    protected void btDeleteBizHour_Click(object sender, EventArgs e)
    {
        if (DeleteBuildingDelivery != null)
        {
            CheckBox bhcbAdd = new CheckBox();
            CheckBox bhcbDelete = new CheckBox();

            foreach (GridViewRow gd in ((GridView)BizHourBuildingList1.FindControl("gvSellerInBuildingCity")).Rows)
            {
                Guid BusinessHourGuid = (Guid)((GridView)BizHourBuildingList1.FindControl("gvSellerInBuildingCity")).DataKeys[gd.RowIndex].Value;

                BuildingDelivery bd = new BuildingDelivery();
                bd.BusinessHourGuid = BusinessHourGuid;
                bd.BuildingGuid = BuildingGuid;

                bhcbAdd = (CheckBox)gd.Cells[0].FindControl("cbSellerAdd");

                if (bhcbAdd.Checked)
                {
                    bd.BusinessHourGuid = BusinessHourGuid;
                    bd.BuildingGuid = BuildingGuid;

                    DeleteBuildingDelivery(this, new DataEventArgs<BuildingDelivery>(bd));
                }
            }
            Response.Redirect(string.Format("{0}?bid={1}", "Building_Add.aspx", BuildingGuid));
        }
    }

    protected void btAddBizHourInBuilding_Click(object sender, EventArgs e)
    {
        if (UpdateBuildingDelivery != null)
        {
            CheckBox bhcbAdd = new CheckBox();
            CheckBox bhcbDelete = new CheckBox();

            foreach (GridViewRow gd in ((GridView)BizHourBuildingList2.FindControl("gvSellerInBuildingCity")).Rows)
            {
                string BusinessHourGuid = ((GridView)BizHourBuildingList2.FindControl("gvSellerInBuildingCity")).DataKeys[gd.RowIndex].Value.ToString();

                BuildingDelivery bd = new BuildingDelivery();
                bd.BusinessHourGuid = new Guid(BusinessHourGuid);
                bd.BuildingGuid = BuildingGuid;

                bhcbAdd = (CheckBox)gd.Cells[0].FindControl("cbSellerAdd");

                if (bhcbAdd.Checked)
                {
                    UpdateBuildingDelivery(this, new DataEventArgs<BuildingDelivery>(bd));
                }
            }
        }
        Response.Redirect(string.Format("{0}?bid={1}", "Building_Add.aspx", BuildingGuid));
    }
    [WebMethod]
    public static KeyValuePair<string, string> CalculateCoordinates(string address)
    {
        return LocationFacade.GetArea(address);
    }
}
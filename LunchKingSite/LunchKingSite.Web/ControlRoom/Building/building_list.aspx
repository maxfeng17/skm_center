﻿<%@ Page Language="C#" MasterPageFile="~/ControlRoom/backend.master" AutoEventWireup="true" CodeBehind="building_list.aspx.cs" Inherits="building_list" %>

<%@ Register Src="../Controls/BuildingList.ascx" TagName="BuildingList" TagPrefix="uc2" %>

<%@ Register Src="../Controls/Pager.ascx" TagName="Pager" TagPrefix="uc1" %>

<%@ Register Assembly="System.Web.Extensions"
    Namespace="System.Web.UI" TagPrefix="aspajax" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
    <uc2:BuildingList ID="BuildingList1" runat="server" />
</asp:Content>
 
﻿using System;
using System.Web.UI.WebControls;
using LunchKingSite.Core.UI;

public partial class building_list : RolePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ((GridView)BuildingList1.FindControl("gvBuilding")).Columns[0].Visible = false;
        }
    }
}


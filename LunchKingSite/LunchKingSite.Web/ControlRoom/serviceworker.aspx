﻿<%@ Page Title="會員回覆系統" Language="C#" MasterPageFile="~/ControlRoom/backend.master"
    AutoEventWireup="true" CodeBehind="serviceworker.aspx.cs" Inherits="LunchKingSite.Web.ControlRoom.serviceworker" %>

<%@ Register Src="~/ControlRoom/Controls/Pager.ascx" TagName="Pager" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <script type="text/javascript" src="//www.google.com/jsapi"></script>
    <script type="text/javascript">
        google.load("jquery", "1.4.2");
        google.load("jqueryui", "1.8");
    </script>
    <script type="text/javascript" src="../Tools/js/jquery.ui.datepicker-zh-TW.js"></script>
    <script type="text/javascript" src="../Tools/js/jquery-ui-timepicker-addon.js"></script>
    <link type="text/css" href="../Tools/js/css/ui-lightness/jquery-ui-1.8rc3.custom.css"
        rel="stylesheet" />
    <script type="text/javascript" language="javascript">
        function $$(id, context) {
            var el = $("#" + id, context);
            if (el.length < 1)
                el = $("*[id$=" + id + "]", context);
            return el;
        }

        $(document).ready(function () {
            $$('tbOS').datetimepicker({ dateFormat: 'yy/mm/dd', stepMinute: 1 });
            $$('tbOE').datetimepicker({ dateFormat: 'yy/mm/dd', stepMinute: 1 });
			$$('tbDealTime1').datetimepicker({ dateFormat: 'yy/mm/dd', stepMinute: 1 });
            $$('tbDealTime2').datetimepicker({ dateFormat: 'yy/mm/dd', stepMinute: 1 });

            var availableTags = [<%=SalesManNameList%>];
            $('#<%=txtModify.ClientID %>').autocomplete({
                source: availableTags
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style type="text/css">
        /*body
        {
            font-size: 10px;
        }*/
        .ui-timepicker-div .ui-widget-header
        {
            margin-bottom: 8px;
        }
        .ui-timepicker-div dl
        {
            text-align: left;
        }
        .ui-timepicker-div dl dt
        {
            height: 25px;
        }
        .ui-timepicker-div dl dd
        {
            margin: -25px 0 10px 65px;
        }
        .ui-timepicker-div td
        {
            font-size: 90%;
        }
        label.error
        {
            float: none;
            color: red;
            padding-left: .5em;
            vertical-align: top;
        }
        .style1
        {
            width: 414px;
        }
    </style>
    <table style="width: 622px">
        <tr>
            <td valign="top" class="style1">
                <fieldset style="width: 410px">
                    <legend>查詢條件</legend>
                    <table style="height: 111px;">
                        <tr>
                            <td>
                                狀態
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlStatus" runat="server">
                                    <asp:ListItem>全部</asp:ListItem>
                                    <asp:ListItem Value="wait">待處理</asp:ListItem>
                                    <asp:ListItem Value="process">處理中</asp:ListItem>
                                    <asp:ListItem Value="complete">完成</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td>
                                形式
                            </td>
                            <td>
                                <asp:DropDownList ID="ddltype" runat="server">
                                    <asp:ListItem>全部</asp:ListItem>
                                    <asp:ListItem>來信</asp:ListItem>
                                    <asp:ListItem>來電</asp:ListItem>
                                    <asp:ListItem>去信</asp:ListItem>
                                    <asp:ListItem>外撥</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                姓名
                            </td>
                            <td>
                                <asp:TextBox ID="txtName" runat="server"></asp:TextBox>
                            </td>
                            <td>
                                電話
                            </td>
                            <td>
                                <asp:TextBox ID="txtPhone" runat="server" Width="120px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                訂單編號
                            </td>
                            <td>
                                <asp:TextBox ID="txtorder" runat="server"></asp:TextBox>
                            </td>
                            <td>
                                email
                            </td>
                            <td>
                                <asp:TextBox ID="txtEmail" runat="server" Width="128px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                館別
                            </td>
                            <td colspan="3">
                                <asp:RadioButtonList ID="rdlTypeS" runat="server" RepeatDirection="Horizontal">
                                    <asp:ListItem Value="" Text="全選" Selected="True"></asp:ListItem>
                                    <asp:ListItem Value="0" Text="P好康"></asp:ListItem>
                                    <asp:ListItem Value="1" Text="HiDeal"></asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                分類
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlCategoryS" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlCategoryS_SelectedIndexChanged">
                                    <asp:ListItem Text="請選擇" Value="0"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td>
                                細項
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlSubCategoryS" runat="server">
                                    <asp:ListItem Text="請選擇" Value="0"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                出件人員
                            </td>
                            <td colspan="3">
                                <asp:TextBox ID="txtModify" runat="server" Width="200px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                來信日期
                            </td>
                            <td colspan="3">
                                <table>
                                    <tr>
                                        <td>
                                            從
                                            <asp:TextBox ID="tbOS" runat="server" class="required" Width="139px"></asp:TextBox>
                                        </td>
                                        <td>
                                            至
                                            <asp:TextBox ID="tbOE" runat="server" class="required" Width="139px"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
						<tr>
                            <td>
                                處理時間
                            </td>
                            <td colspan="3">
                                <table>
                                    <tr>
                                        <td>
                                            從
                                            <asp:TextBox ID="tbDealTime1" runat="server" class="required" Width="139px"></asp:TextBox>
                                        </td>
                                        <td>
                                            至
                                            <asp:TextBox ID="tbDealTime2" runat="server" class="required" Width="139px"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                轉件項目
                            </td>
                            <td colspan="3">
                                <asp:DropDownList ID="ddlWorkType" runat="server">
                                    <asp:ListItem Text="請選擇" Value=""></asp:ListItem>
                                    <asp:ListItem Text="商品" Value="0"></asp:ListItem>
                                    <asp:ListItem Text="好康" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="退貨" Value="2"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                賣家名稱
                            </td>
                            <td colspan="3">
                                <asp:TextBox ID="txtSellerName" runat="server" Width="150"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                緊急程度
                            </td>
                            <td colspan="3">
                                <asp:RadioButtonList ID="rblSelectPriority" runat="server" RepeatDirection="Horizontal"
                                    RepeatLayout="Table">
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                回報廠商備註
                            </td>
                            <td colspan="3">
                                <asp:CheckBox ID="cbRemarkSeller" runat="server" Text="回報廠商備註" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <asp:Button ID="btnSearch" runat="server" Text="查詢" OnClick="btnSearch_Click" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <asp:Label ID="lbCounts" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </td>
            <td>
                <div style="display:none">
                <fieldset style="width: 605px;">
                    <legend>自行新增客服</legend>
                    <table style="font-size: smaller;">
                        <tr>
                            <td align="center">
                                <table>
                                    <tr>
                                        <td align="right">
                                            姓名
                                        </td>
                                        <td align="left">
                                            <asp:TextBox ID="txtIName" runat="server"></asp:TextBox>
                                        </td>
                                        <td align="right">
                                            Email
                                        </td>
                                        <td align="left">
                                            <asp:TextBox ID="txtIEmail" runat="server"></asp:TextBox>
                                            <asp:Button ID="btnbump" runat="server" Text="帶值" OnClick="btnbump_Click" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            電話
                                        </td>
                                        <td align="left">
                                            <asp:TextBox ID="txtIPhone" runat="server"></asp:TextBox>
                                        </td>
                                        <td align="right">
                                            OrderId
                                        </td>
                                        <td align="left">
                                            <asp:TextBox ID="txtIOrder" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            形式
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlItype" runat="server">
                                                <asp:ListItem Selected>來電</asp:ListItem>
                                                <asp:ListItem>外撥</asp:ListItem>
                                                <asp:ListItem>去信</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td align="right">
                                            分類
                                        </td>
                                        <td align="left">
                                            <asp:DropDownList ID="ddlCategoryA" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlCategoryA_SelectedIndexChanged">
                                                <asp:ListItem Text="請選擇" Value="0"></asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:DropDownList ID="ddlSubCategoryA" runat="server">
                                                <asp:ListItem Text="請選擇" Value="0"></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            狀態
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlIStatus" runat="server">
                                                <asp:ListItem Value="wait">待處理</asp:ListItem>
                                                <asp:ListItem Value="process">處理中</asp:ListItem>
                                                <asp:ListItem Value="complete">完成</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td colspan="2"></td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            轉件項目
                                        </td>
                                        <td align="left" colspan="3">
                                            <asp:RadioButtonList ID="rdlWorkType" runat="server" RepeatDirection="Horizontal">
                                                <asp:ListItem Text="商品" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="好康" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="退貨" Value="2"></asp:ListItem>
                                            </asp:RadioButtonList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            緊急程度
                                        </td>
                                        <td align="left" colspan="3">
                                            <asp:RadioButtonList ID="rblPriority" runat="server" RepeatDirection="Horizontal"
                                                RepeatLayout="Table">
                                            </asp:RadioButtonList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            問題說明
                                        </td>
                                        <td align="left" colspan="3">
                                            <asp:TextBox ID="txtIMessage" runat="server" TextMode="MultiLine" Height="130px"
                                                Width="520px"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            回報廠商備註
                                        </td>
                                        <td align="left" colspan="3">
                                            <asp:TextBox ID="txtSellerRemark" runat="server" TextMode="MultiLine" Height="50px"
                                                Width="520px"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" align="center" style="text-align: left">
                                            <asp:Button ID="btnSend" runat="server" Text="送出" OnClick="btnSend_Click" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </fieldset>
                </div>
            </td>
        </tr>
    </table>
    <table>
        <tr>
            <td>
                <asp:Button ID="exportExcel" runat="server" Text="匯出Excel" 
                    onclick="exportExcel_Click" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:GridView ID="GridView1" runat="server" EnableViewState="False" OnRowDataBound="GridView1_RowDataBound"
                    GridLines="Horizontal" ForeColor="Black" CellPadding="4" BorderWidth="1px" BorderStyle="None" Width="1650"
                    BorderColor="#DEDFDE" BackColor="White" EmptyDataText="無符合的資料" AutoGenerateColumns="False">
                    <FooterStyle BackColor="#CCCC99"></FooterStyle>
                    <RowStyle BackColor="#F7F7DE" Height="50px"></RowStyle>
                    <SelectedRowStyle BackColor="#CE5D5A" ForeColor="White" Font-Bold="True"></SelectedRowStyle>
                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right"></PagerStyle>
                    <HeaderStyle BackColor="#6B696B" ForeColor="White" Font-Bold="True" Height="30px">
                    </HeaderStyle>
                    <AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
                    <Columns>
                        <asp:BoundField DataField="id" HeaderText="id" Visible="false" />
                        <asp:TemplateField HeaderText="狀態" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:HyperLink ID="hkStatus" runat="server" Target="_blank" Width="40px"></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="緊急程度" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:Label ID="lbPriority" runat="server" Width="40px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="姓名">
                            <ItemTemplate>
                                <asp:HyperLink ID="hkName" runat="server" Target="_blank"></asp:HyperLink>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" Width="60px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Email">
                            <ItemTemplate>
                                <asp:HyperLink ID="hkEmail" runat="server" Target="_blank"></asp:HyperLink>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" Width="60px" />
                        </asp:TemplateField>
                        <asp:BoundField DataField="messagetype" HeaderText="型式" ItemStyle-Width="50px" ItemStyle-HorizontalAlign="Center" />
                        <asp:TemplateField HeaderText="問題">
                            <ItemTemplate>
                                <asp:Label ID="lblmessage" runat="server" Text='<%# HttpUtility.HtmlDecode(Eval("message").ToString().Replace("\n","<br>")) %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" Width="120px" />
                        </asp:TemplateField>
                        <asp:BoundField DataField="uniqueid" HeaderText="檔號" ItemStyle-Width="50px" ItemStyle-HorizontalAlign="Left" />
                        <%--<asp:BoundField DataField="itemname" HeaderText="檔名" ItemStyle-Width="120px" ItemStyle-HorizontalAlign="Left" />--%>
                        <asp:TemplateField HeaderText="檔名" ItemStyle-Width="80px" ItemStyle-HorizontalAlign="Center"
                            ItemStyle-Font-Size="Medium">
                            <ItemTemplate>
                                <asp:HyperLink ID="itemname" runat="server" Target="_blank" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="訂單編號" ItemStyle-Width="80px" ItemStyle-HorizontalAlign="Center"
                            ItemStyle-Font-Size="Medium">
                            <ItemTemplate>
                                <asp:HyperLink ID="hkOrderId" runat="server" Target="_blank" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="回報廠商備註">
                            <ItemTemplate>
                                <asp:Label ID="lblRemark" runat="server" Text='<%# (Eval("remarkSeller")== null ? "" : Eval("remarkSeller").ToString().Replace("\n","<br>")) %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" Width="200px" />
                        </asp:TemplateField>
                        <asp:BoundField DataField="createid" HeaderText="出信人" Visible="false" />
                        <asp:BoundField DataField="createtime" HeaderText="來信時間" DataFormatString="{0:yyyy/MM/dd HH:mm:ss}"
                            ItemStyle-Width="100px" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Size="Medium" />
                        <asp:TemplateField HeaderText="出件人員" ItemStyle-Width="80px" ItemStyle-HorizontalAlign="Center"
                            ItemStyle-Font-Size="Medium">
                            <ItemTemplate>
                                <asp:Label ID="lblModifyId" runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="modifytime" HeaderText="最後處理時間" DataFormatString="{0:yyyy/MM/dd HH:mm:ss}"
                            ItemStyle-Width="100px" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Size="Medium" />
                        <asp:TemplateField HeaderText="館別" ItemStyle-Width="5%" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:Label ID="lblType" runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="轉件項目" ItemStyle-Width="100px" ItemStyle-HorizontalAlign="Center"
                            ItemStyle-Font-Size="Medium">
                            <ItemTemplate>
                                <asp:Label ID="lblWorkType" runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="業務" ItemStyle-Width="150px" ItemStyle-HorizontalAlign="Center"
                            ItemStyle-Font-Size="Medium">
                            <ItemTemplate>
                                <asp:Label ID="lblSalesId" runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td>
                <uc1:Pager ID="gridPager" runat="server" PageSize="50" OnGetCount="RetrieveTransCount"
                    OnUpdate="UpdateHandler"></uc1:Pager>
            </td>
        </tr>
    </table>
</asp:Content>

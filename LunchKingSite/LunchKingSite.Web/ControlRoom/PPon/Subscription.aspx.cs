﻿using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.UI;
using System;
using System.Web.UI.WebControls;

namespace LunchKingSite.Web.ControlRoom.Ppon
{
    public partial class Subscription : RolePage
    {
        protected static IPponProvider pp;

        protected void Page_Load(object sender, EventArgs e)
        {
            pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            if (txtemail.Text != "")
            {
                GetData();
            }
        }

        protected void btnQuery_Click(object sender, EventArgs e)
        {
            GetData();
        }

        private void GetData()
        {
            gvS.DataSource = pp.SubscriptionGetList(txtemail.Text);
            gvS.DataBind();
        }

        public void Gvs_RowCommand(Object sender, GridViewCommandEventArgs e)
        {
            GridViewRow row = ((LinkButton)e.CommandSource).Parent.Parent as GridViewRow;
            int id = int.Parse(((Label)row.FindControl("lblId")).Text);
            pp.SubscriptionDelete(id);
            GetData();
        }
    }
}
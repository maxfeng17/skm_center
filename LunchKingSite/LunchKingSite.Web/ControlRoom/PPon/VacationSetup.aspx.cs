﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.Core.UI;

namespace LunchKingSite.Web.ControlRoom.Ppon
{
    public partial class VacationSetup : RolePage
    {
        private IPponProvider _pponProv = ProviderFactory.Instance().GetProvider<IPponProvider>();
        private DateTime DateTimeStart
        {
            get
            {
                DateTime dateTime;
                DateTime.TryParse(txtDateTimeS.Text, out dateTime);
                return dateTime;
            }
            set
            {
                txtDateTimeS.Text = value.ToString();
            }
        }
        private DateTime DateTimeEnd
        {
            get
            {
                DateTime dateTime;
                DateTime.TryParse(txtDateTimeE.Text, out dateTime);
                return dateTime;
            }
            set
            {
                txtDateTimeE.Text = value.ToString();
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                txtDateTimeS.Text = DateTime.Now.ToString("yyyy/MM/dd");
                txtDateTimeE.Text = DateTime.Now.AddYears(1).ToString("yyyy/MM/dd");
                LoadVacationList();
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            LoadVacationList();
        }

        protected void btnVacationAdd_Click(object sender, EventArgs e)
        {
            DateTime holiday;
            DateTime.TryParse(txtHoliday.Text, out holiday);
            Vacation vacation = _pponProv.GetVacationByDate(holiday);
            if (!vacation.IsLoaded)
            {
                vacation.Holiday = holiday;
                vacation.Name = txtName.Text.Trim();
                _pponProv.SaveVacation(vacation);
                txtHoliday.Text = string.Empty;
                txtName.Text = string.Empty;
                LoadVacationList();
            }
            else
                ScriptManager.RegisterStartupScript(this, this.Page.GetType(), "alert", "alert('節日已存在!!');", true);
        }

        protected void gvVacationList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "DEL")
            {
                int id;
                int.TryParse(e.CommandArgument.ToString(), out id);
                _pponProv.DeleteVacation(id);
                LoadVacationList();
            }
        }

        private void LoadVacationList()
        {
            if (!DateTimeStart.Equals(DateTime.MinValue) && !DateTimeEnd.Equals(DateTime.MinValue))
            {
                gvVacationList.DataSource = _pponProv.GetVacationListByPeriod(DateTimeStart, DateTimeEnd);
                gvVacationList.DataBind();
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Text;
using LunchKingSite.Core.Component;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using LunchKingSite.Core.UI;

namespace LunchKingSite.Web.ControlRoom.Ppon
{
    public partial class PponInsertSerialKey : RolePage
    {
        IPponProvider pp;
        protected void Page_Load(object sender, EventArgs e)
        {
            pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            if (!IsPostBack)
            {
                foreach (var item in Enum.GetValues(typeof(SellerSerialKeyDealType)))
                {
                    rbl_SerialKeyDealType.Items.Add(new ListItem(Helper.GetLocalizedEnum(Resources.Localization.ResourceManager, (SellerSerialKeyDealType)item), ((int)item).ToString()));
                }
                if (rbl_SerialKeyDealType.Items.Count > 0)
                    rbl_SerialKeyDealType.SelectedIndex = 0;
            }
        }

        protected void SearchCurrentCount(object sender, EventArgs e)
        {
            Guid bid;
            StringBuilder builder = new StringBuilder();
            lab_Message.Text = lab_Info.Text = string.Empty;
            if (Guid.TryParse(tbx_Bid.Text, out bid))
            {
                ViewPponDeal deal = pp.ViewPponDealGetByBusinessHourGuid(bid);
                PeztempCollection pc = pp.PeztempGetListByBid(bid);
                string strCoupons = "";
                int iCount = 0;
                foreach (Peztemp p in pc)
                {
                    strCoupons += p.PezCode+"<br/>";
                    iCount++;
                    if (iCount == 5)
                        break;
                }
                builder.AppendLine("檔名 :     " + deal.EventName + "<br/>");
                builder.AppendLine("已設定廠商序號檔次 :     " + (((deal.GroupOrderStatus ?? 0) & (int)GroupOrderStatus.PEZevent) > 0).ToString() + "<br/>");
                builder.AppendLine("此檔設定販賣數量 :     "+(deal.OrderTotalLimit==null?"0": deal.OrderTotalLimit.Value.ToString("f0")+"<br/>"));
                builder.AppendLine("已存在序號量 :     " + pp.PeztempGetCount(bid, false) + "<br/>");
                builder.AppendLine("已使用序號量 :     " + pp.PeztempGetCount(bid, true)+"<br/>");
                builder.AppendLine("前五筆序號   :<br/>     " + strCoupons);
                lab_Info.Text = builder.ToString();
            }
            else
                lab_Message.Text = "請輸入正確Bid";
        }

        protected void InsertSerialKey(object sender, EventArgs e)
        {
            int total = 0;
            Guid bid;
            string errormessage = lab_Message.Text = lab_Info.Text = string.Empty;
            SellerSerialKeyDealType serialkey_type;
            if (FileUpload1.HasFile && Guid.TryParse(tbx_Bid.Text, out bid) && Enum.TryParse<SellerSerialKeyDealType>(rbl_SerialKeyDealType.SelectedValue, out serialkey_type))
            {
                if (Path.GetExtension(FileUpload1.FileName).ToLower() != ".txt")
                    lab_Message.Text = "僅可上傳txt檔";
                else
                {
                    HttpPostedFile file = FileUpload1.PostedFile;
                    using (Stream filestream = file.InputStream)
                    {
                        using (StreamReader reader = new StreamReader(filestream, Encoding.GetEncoding(950)))
                        {
                            string[] list_row = reader.ReadToEnd().Replace("\n", ";").Replace("\r", ";").Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
                            List<string> list_bid = new List<string>();
                            Dictionary<string, string> list_biduseridentity = new Dictionary<string, string>();
                            bool check_reliable = true;
                            switch (serialkey_type)
                            {
                                case SellerSerialKeyDealType.BidWithUserId:
                                    foreach (var item in list_row)
                                    {
                                        var splititem = item.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                                        try
                                        {
                                            list_biduseridentity.Add(splititem.Skip(0).DefaultIfEmpty(string.Empty).FirstOrDefault(), splititem.Skip(1).DefaultIfEmpty(string.Empty).FirstOrDefault());
                                        }
                                        catch (Exception error)
                                        {
                                            errormessage += error.Message;
                                        }
                                    }
                                    if (list_biduseridentity.Any(x => string.IsNullOrEmpty(x.Key) || string.IsNullOrEmpty(x.Value)))
                                        check_reliable = false;
                                    else
                                    {
                                        list_biduseridentity.ForEach(x =>
                                        {
                                            Peztemp pez = new Peztemp();
                                            pez.Bid = bid;
                                            pez.PezCode = x.Key;
                                            pez.UserId = x.Value;
                                            pp.PeztempSet(pez);
                                            total++;
                                        });
                                    }
                                    break;
                                default:
                                    foreach (var item in list_row)
                                    {
                                        var splititem = item.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                                        foreach (string serial in splititem)
                                        {
                                            list_bid.Add(serial);
                                        }
                                    }
                                    if (list_bid.Any(x => string.IsNullOrEmpty(x)))
                                        check_reliable = false;
                                    else
                                    {
                                        list_bid.ForEach(x =>
                                        {
                                            Peztemp pez = new Peztemp();
                                            pez.Bid = bid;
                                            pez.PezCode = x;
                                            pp.PeztempSet(pez);
                                            total++;
                                        });
                                    }
                                    break;
                            }
                            if (!check_reliable)
                                lab_Message.Text = "發現資料空白或欄位不符";
                            else
                            {
                                int blank_count;
                                if (!string.IsNullOrEmpty(tbx_Blank.Text) && int.TryParse(tbx_Blank.Text, out blank_count))
                                {
                                    for (int i = 0; i < blank_count; i++)
                                    {
                                        Peztemp pez = new Peztemp();
                                        pez.Bid = bid;
                                        pez.PezCode = string.Empty;
                                        pp.PeztempSet(pez);
                                    }
                                    total += blank_count;
                                }
                                
                                
                                lab_Message.Text = "新增總筆數 :" + total.ToString("F0") + (string.IsNullOrEmpty(errormessage) ? string.Empty : ("<br/>" + errormessage))+"<br/>";
                                
                            }
                        }
                    }
                }
            }
            else
                lab_Message.Text = "請填入正確Bid";
        }

    }

}
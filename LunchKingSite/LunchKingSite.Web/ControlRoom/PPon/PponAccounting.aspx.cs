﻿using LunchKingSite.Core;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;
using System;
using System.Web.UI;

namespace LunchKingSite.Web.ControlRoom.Ppon
{
    public partial class PponAccounting : RolePage, IPponAccountingView
    {
        public event EventHandler QueryCost;

        public event EventHandler<DataEventArgs<UpdatableAccountingInfo>> UpdateAccounting;

        public event EventHandler<DataEventArgs<DealCost>> CreateCost;

        public event EventHandler DeleteCost;

        #region props

        public Guid BusinessHourGuid
        {
            get
            {
                if (string.IsNullOrEmpty(Request.QueryString["bid"]))
                {
                    return Guid.Empty;
                }
                
                try
                {
                    return new Guid(Request.QueryString["bid"]);
                }
                catch
                {
                    return Guid.Empty;
                }
            }
        }

        private PponAccountingPresenter _presenter;

        public PponAccountingPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        #endregion props

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                ddlAPFormula.Items.AddRange(WebUtility.GetListItemFromEnum(AccountsPayableFormula.Default));
                _presenter.OnViewInitialized();
            }
            _presenter.OnViewLoaded();
        }

        public void SetData(DealAccounting dAccounting, DealCostCollection dCost)
        {
            tbSales.Text = dAccounting.SalesId;
            tbCommission.Text = dAccounting.SalesCommission.ToString();
            tbBonus.Text = Convert.ToInt32(dAccounting.SalesBonus).ToString();
            if (dAccounting.ChargingDate != null)
            {
                tbChargingDate.Text = dAccounting.ChargingDate.Value.ToString("MM/dd/yyyy");
            }

            if (dAccounting.EnterDate != null)
            {
                tbEnterDate.Text = dAccounting.EnterDate.Value.ToString("MM/dd/yyyy");
            }

            if (dAccounting.InvoiceDate != null)
            {
                tbInvoiceDate.Text = dAccounting.InvoiceDate.Value.ToString("MM/dd/yyyy");
            }

            ddlAPFormula.SelectedIndex = (int)dAccounting.Status;

            if (dCost != null)
            {
                if (dCost.Count == 1) tbCost.Text = dCost[0].Cost.ToString();
                else if (dCost.Count > 1)
                {
                    p2.Visible = false;
                    btnMultiCost.Visible = false;
                    pCost.Visible = true;
                }
            }
            else
            {
                tbCost.Text = string.Empty;
            }

            SetGrid(dCost);
        }

        public void SetGrid(DealCostCollection dCost)
        {
            gCost.DataSource = dCost;
            gCost.DataBind();
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            UpdatableAccountingInfo ai = new UpdatableAccountingInfo();
            ai.SalesId = tbSales.Text;
            ai.Commission = tbCommission.Text;
            ai.Bonus = tbBonus.Text;
            ai.ChargingDate = tbChargingDate.Text;
            ai.EnterDate = tbEnterDate.Text;
            ai.InvoiceDate = tbInvoiceDate.Text;
            ai.APFormula = ddlAPFormula.SelectedIndex.ToString();
            ai.Cost = tbCost.Text;
            if (UpdateAccounting != null)
            {
                UpdateAccounting(this, new DataEventArgs<UpdatableAccountingInfo>(ai));
            }
        }

        protected void btnMultiCost_Click(object sender, EventArgs e)
        {
            pCost.Visible = true;
            p2.Visible = false;
            btnMultiCost.Visible = false;
            if (QueryCost != null)
            {
                QueryCost(this, e);
            }
        }

        protected void btnCreateCost_Click(object sender, EventArgs e)
        {
            DealCost newCost = new DealCost();
            newCost.BusinessHourGuid = BusinessHourGuid;
            newCost.Cost = decimal.Parse(tbMultiCost.Text);
            newCost.Quantity = int.Parse(tbQuantity.Text);
            if (CreateCost != null)
            {
                CreateCost(this, new DataEventArgs<DealCost>(newCost));
            }

            if (QueryCost != null)
            {
                QueryCost(this, e);
            }
        }

        protected void btnDeleteCost_Click(object sender, EventArgs e)
        {
            if (DeleteCost != null)
            {
                DeleteCost(this, e);
            }

            if (QueryCost != null)
            {
                QueryCost(this, e);
            }
        }
    }
}
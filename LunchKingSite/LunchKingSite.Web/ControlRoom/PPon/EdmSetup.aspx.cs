﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LunchKingSite.Web.ControlRoom.Ppon
{
    public partial class EdmSetup : RolePage
    {
        #region property

        public string UserName
        {
            get
            {
                return User.Identity.Name;
            }
        }

        public DateTime Delivery_Date
        {
            get
            {
                DateTime deliverydate;
                if (DateTime.TryParse(tbx_Main_DeliveryDate.Text, out deliverydate))
                {
                    int selectedEdmCityId = int.Parse(ddl_Main_City.SelectedValue);
                    return EmailFacade.GetEdmSendTime(selectedEdmCityId, deliverydate);
                }
                else
                {
                    return DateTime.MaxValue;
                }
            }
            set
            {
                tbx_Main_DeliveryDate.Text = value.ToString("MM/dd/yyyy");
            }
        }

        public EdmMainType EdmType
        {
            get
            {
                return EdmMainType.Daily;
            }
        }

        public int CityId
        {
            get
            {
                int cityid;
                if (int.TryParse(ddl_Main_City.SelectedValue, out cityid))
                {
                    return cityid;
                }
                else
                {
                    return 0;
                }
            }
            set
            {
                ddl_Main_City.SelectedValue = value.ToString();
            }
        }

        public int Id
        {
            get
            {
                int id;
                if (int.TryParse(hif_Id.Value, out id))
                {
                    return id;
                }
                else
                {
                    return 0;
                }
            }
            set
            {
                hif_Id.Value = value.ToString();
            }
        }

        public string UploadDate { get; set; }

        #endregion property

        #region page

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                List<PponCity> edm_cities = new List<PponCity>{ PponCityGroup.DefaultPponCityGroup.TaipeiCity
                                                              , PponCityGroup.DefaultPponCityGroup.Taoyuan
                                                              , PponCityGroup.DefaultPponCityGroup.Hsinchu
                                                              , PponCityGroup.DefaultPponCityGroup.Taichung
                                                              , PponCityGroup.DefaultPponCityGroup.Tainan
                                                              , PponCityGroup.DefaultPponCityGroup.Kaohsiung
                                                              };

                List<PponCity> edm_other_cities = new List<PponCity> { PponCityGroup.DefaultPponCityGroup.PBeautyLocation
                                                                     , PponCityGroup.DefaultPponCityGroup.Travel 
                                                                     , PponCityGroup.DefaultPponCityGroup.AllCountry
                                                                     , PponCityGroup.DefaultPponCityGroup.Piinlife
                                                                     , PponCityGroup.DefaultPponCityGroup.PponSelect
                                                                     };
                edm_cities.AddRange(edm_other_cities);
                ddl_Main_City.DataSource = edm_cities;
                ddl_Main_City.DataTextField = "EdmCityName";
                ddl_Main_City.DataValueField = "CityId";
                ddl_Main_City.DataBind();
                tbx_Main_DeliveryDate.Text = DateTime.Now.ToString("MM/dd/yyyy");
                ddl_Main_City.SelectedValue = PponCityGroup.DefaultPponCityGroup.TaipeiCity.CityId.ToString();
            }
        }

        protected void ddl_Main_City_DataBinding(object sender, EventArgs e)
        {

        }



        protected void GetEdmContent(object sender, EventArgs e)
        {
            tbx_Subject.Text = lab_ID.Text = tbx_Cpa.Text = string.Empty;
            lblDeliveryTime.Text = Delivery_Date.ToString("HH:mm:ss");
            hyp_ID.Visible = false;
            int cityid;
            List<EdmDetail> edmdetaildata;
            if (int.TryParse(ddl_Main_City.SelectedValue, out cityid) && PponCityGroup.DefaultPponCityGroup.Any(x => x.CityId == cityid))
            {
                PponCity city = PponCityGroup.DefaultPponCityGroup.First(x => x.CityId == cityid);
                EdmMain edm_main = EmailFacade.GetNewEdmMainByCityIdDate(city, Delivery_Date, EdmType, true);
                if (edm_main.Id == 0)
                {
                    lab_ID.Text = "新增編輯";
                    edmdetaildata = EmailFacade.GetNewEdmDetailsByCityIdDate(city, Delivery_Date);
                    EdmDetail first_deal;
                    if ((first_deal = edmdetaildata.Where(x => x.Type == (int)EdmDetailType.MainDeal_1_Items).FirstOrDefault()) != null)
                    {
                        tbx_Subject.Text = first_deal.Title;
                    }
                    else if ((first_deal = edmdetaildata.Where(x => x.Type == (int)EdmDetailType.PponPiinLife_Item_1).FirstOrDefault()) != null)
                    {
                        tbx_Subject.Text = first_deal.Title;
                    }
                    btn_Delete.Visible = btn_Send.Visible = btn_TestSend.Visible = btn_Delete2.Visible = btn_Send2.Visible = btn_TestSend2.Visible = cbx_Compress.Visible = false;
                    //預設cpa
                    tbx_Cpa.Text = EmailFacade.GetEdmCpa(cityid);
                    cbx_Pause.Checked = false;
                }
                else
                {
                    lab_ID.Text = "編輯 ID : " + edm_main.Id.ToString();
                    if (edm_main.UploadDate != null)
                    {
                        lab_ID.Text += " (已於" + edm_main.UploadDate.Value.ToString("MM/dd HH:mm") + "上傳發送)";
                    }
                    hyp_ID.Visible = true;
                    hyp_ID.NavigateUrl = "~/Ppon/OnlineEDM.aspx?eid=" + edm_main.Id;
                    edmdetaildata = EmailFacade.GetNewEdmDetailsByPid(edm_main.Id);
                    //只變更主旨，尚未編輯任何內容

                    if (edmdetaildata.Count == 0)
                    {
                        edmdetaildata = EmailFacade.GetNewEdmDetailsByCityIdDate(city, Delivery_Date);
                    }
                    tbx_Subject.Text = edm_main.Subject;
                    tbx_Cpa.Text = edm_main.Cpa;
                    cbx_Pause.Checked = edm_main.Pause;
                    UploadDate = edm_main.UploadDate.HasValue ? edm_main.UploadDate.Value.ToString("MM/dd HH:mm") : string.Empty;
                    btn_Delete.Visible = btn_Send.Visible = btn_TestSend.Visible = btn_Delete2.Visible = btn_Send2.Visible = btn_TestSend2.Visible = cbx_Compress.Checked = true;
                }
                Id = edm_main.Id;

                SetUpEdmDetailSetupContol(edmuc_main_1, EdmDetailType.MainDeal_1, EdmDetailType.MainDeal_1_Items, edmdetaildata, true, edm_main.Id);
                SetUpEdmDetailSetupContol(edmuc_main_2, EdmDetailType.MainDeal_2, EdmDetailType.MainDeal_2_Items, edmdetaildata, true, edm_main.Id);
                SetUpEdmDetailSetupContol(edmuc_Area_1, EdmDetailType.Area_1, EdmDetailType.Area_1_Items, edmdetaildata, true, edm_main.Id);
                SetUpEdmDetailSetupContol(edmuc_Area_2, EdmDetailType.Area_2, EdmDetailType.Area_2_Items, edmdetaildata, true, edm_main.Id);
                SetUpEdmDetailSetupContol(edmuc_Area_3, EdmDetailType.Area_3, EdmDetailType.Area_3_Items, edmdetaildata, true, edm_main.Id);
                SetUpEdmDetailSetupContol(edmuc_PponPiinLife_1, EdmDetailType.PponPiinLife_1, EdmDetailType.PponPiinLife_Item_1, edmdetaildata, true, edm_main.Id);

                #region AD

                ViewCmsRandomCollection ad_list = EmailFacade.GetNewEdmADByCityIdDate(city, Delivery_Date);
                ddl_AD_Content.DataSource = ddl_AD.DataSource = ad_list;
                ddl_AD.DataTextField = "Title";
                ddl_AD_Content.DataTextField = "Body";
                ddl_AD_Content.DataValueField = ddl_AD.DataValueField = "Pid";
                ddl_AD.DataBind();
                ddl_AD_Content.DataBind();
                if (edmdetaildata.Any(x => x.Type == (int)EdmDetailType.AD) && ad_list.Any(x => x.Pid == edmdetaildata.First(y => y.Type == (int)EdmDetailType.AD).AdId))
                {
                    cbx_AD.Checked = true;
                    ddl_AD_Content.SelectedValue = ddl_AD.SelectedValue = edmdetaildata.First(y => y.Type == (int)EdmDetailType.AD).AdId.ToString();
                }
                else
                {
                    cbx_AD.Checked = false;
                }
                ChangeAD(sender, e);

                #endregion AD

                #region PEZ

                CmsContent article = EmailFacade.GetPEZCmsContent("/edm/promote_pez");
                lit_PEZ.Text = HttpUtility.HtmlDecode(article.Body);
                if (edmdetaildata.Any(x => x.Type == (int)EdmDetailType.PEZ))
                {
                    cbx_PEZ.Checked = true;
                }
                else
                {
                    cbx_PEZ.Checked = false;
                }

                #endregion PEZ
            }
            pan_Details.Visible = true;
        }

        protected void ChangeAD(object sender, EventArgs e)
        {
            ListItem item;
            if ((item = ddl_AD_Content.Items.FindByValue(ddl_AD.SelectedValue)) != null)
            {
                lit_AD.Text = HttpUtility.HtmlDecode(item.Text);
            }
        }

        protected void SaveEdm(object sender, EventArgs e)
        {
            string errormessage_count = string.Empty;
            string errormessage_repeat = string.Empty;
            bool check_count = true;
            bool check_repeat = true;
            bool check_ad = true;
            bool check_duplicate = true;
            Dictionary<EdmDetailType, EdmDetailType> type_list = EmailFacade.GetNewEdmTypePair();

            EdmMain edmmain;
            List<EdmDetail> edmdetail_list = edmuc_main_1.GetData();
            edmdetail_list.AddRange(edmuc_main_2.GetData());
            edmdetail_list.AddRange(edmuc_Area_1.GetData());
            edmdetail_list.AddRange(edmuc_Area_2.GetData());
            edmdetail_list.AddRange(edmuc_Area_3.GetData());
            edmdetail_list.AddRange(edmuc_PponPiinLife_1.GetData());
            //檢查遺漏設定和同區域檔次重複
            foreach (var item in type_list)
            {
                if ((edmdetail_list.Count(x => x.Type == (int)item.Key) == 0 && edmdetail_list.Count(x => x.Type == (int)item.Value) > 0) ||
                    (edmdetail_list.Count(x => x.Type == (int)item.Value) == 0 && edmdetail_list.Count(x => x.Type == (int)item.Key) > 0))
                {
                    errormessage_count = item.Key.ToString() + ",";
                    check_count = false;
                }
                else if ((edmdetail_list.Count(x => x.Type == (int)item.Key) > 0 && edmdetail_list.Count(x => x.Type == (int)item.Value) > 0)
                    && (edmdetail_list.Where(x => x.Type == (int)item.Value).GroupBy(x => x.Bid).Count() != edmdetail_list.Count(x => x.Type == (int)item.Value)))
                {
                    errormessage_repeat = item.Key.ToString() + ",";
                    check_repeat = false;
                }
            }
            //檢查不同區域檔次重複
            List<string> duplicate_item = new List<string>();
            foreach (var item in edmdetail_list.Where(x => type_list.Any(y => (int)y.Value == x.Type)).GroupBy(x => x.Bid))
            {
                if (item.Count() > 1)
                {
                    string duplicate = string.Empty;
                    check_duplicate = false;
                    duplicate += "(";
                    foreach (var innitem in item)
                    {
                        duplicate += type_list.First(x => (int)x.Value == innitem.Type).Key.ToString() + ",";
                    }
                    duplicate += ")";
                    duplicate_item.Add(duplicate);
                }
            }
            int ad_id;
            if (!int.TryParse(ddl_AD.SelectedValue, out ad_id) && cbx_AD.Checked)
            {
                check_ad = false;
            }

            if (check_count && check_repeat && check_ad && check_duplicate)
            {
                #region main

                if (Id != 0)
                {
                    edmmain = EmailFacade.GetNewEdmMainById(Id);
                    edmmain.Subject = tbx_Subject.Text;
                    edmmain.Cpa = tbx_Cpa.Text;
                    edmmain.Pause = cbx_Pause.Checked;
                    edmmain.Message += UserName + ":edit " + DateTime.Now.ToString("MMdd HH:mm") + ";";
                }
                else
                {
                    edmmain = new EdmMain()
                    {
                        Subject = tbx_Subject.Text,
                        Cpa = tbx_Cpa.Text,
                        DeliveryDate = Delivery_Date,
                        CityId = CityId,
                        CityName = ddl_Main_City.SelectedItem.Text,
                        Creator = UserName,
                        CreateDate = DateTime.Now,
                        Type = (int)EdmType,
                        Status = true,
                        Pause = cbx_Pause.Checked
                    };
                }

                #endregion main

                #region detail

                int pid = EmailFacade.SaveNewEdmMain(edmmain);
                EmailFacade.DeleteNewEdmDetailByPid(pid);
                foreach (EdmDetail item in edmdetail_list)
                {
                    item.Pid = pid;
                    EmailFacade.SaveNewEdmDetail(item);
                }

                #endregion detail

                #region ad

                if (cbx_AD.Checked)
                {
                    EdmDetail edmdetail_ad = new EdmDetail() { Pid = pid, AdId = ad_id, Type = (int)EdmDetailType.AD, CityName = "AD", DisplayCityName = false };
                    EmailFacade.SaveNewEdmDetail(edmdetail_ad);
                }

                #endregion ad

                #region pez

                if (cbx_PEZ.Checked)
                {
                    EdmDetail edmdetail_pez = new EdmDetail() { Pid = pid, Type = (int)EdmDetailType.PEZ, CityName = "PEZ", DisplayCityName = false };
                    EmailFacade.SaveNewEdmDetail(edmdetail_pez);
                }

                #endregion pez

                GetEdmContent(sender, e);
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "alertsuccess", "alert('存檔成功!!');", true);
            }
            else
            {
                string errormessage = string.Empty;
                if (!check_count)
                {
                    errormessage += "內容檔次有缺:" + errormessage_count + "。\\n";
                }

                if (!check_repeat)
                {
                    errormessage += "同區域檔次重複:" + errormessage_repeat + "。\\n";
                }

                if (!check_duplicate)
                {
                    errormessage += "不同區域檔次重複:" + duplicate_item.Distinct().Aggregate((x, y) => x + "," + y) + "。\\n";
                }

                if (!check_ad)
                {
                    errormessage += "AD Banner未選!\\n";
                }
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "alertsave", "alert('" + errormessage + "請檢查後存檔!!');", true);
            }
        }

        protected void CancelAction(object sende, EventArgs e)
        {
            pan_Details.Visible = false;
        }

        protected void DeleteEdmSetting(object sender, EventArgs e)
        {
            if (Id != 0)
            {
                EmailFacade.DisableNewEdm(Id);
                CancelAction(sender, e);
            }
        }

        protected void SetUpEdmDetailSetupContol(EdmDetailsSetup usercontrol, EdmDetailType maintype, EdmDetailType detailtype, List<EdmDetail> data, bool isShowSortType, int pid = 0)
        {
            usercontrol.MainDetailType = (int)maintype;
            usercontrol.MainDetailItemType = (int)detailtype;
            usercontrol.FieldName = maintype.ToString();
            usercontrol.EdmDetailData = data.Where(x => x.Type == ((int)maintype) || x.Type == ((int)detailtype)).ToList();
            usercontrol.DeliveryDate = Delivery_Date;
            usercontrol.Pid = pid;
            usercontrol.IsShowSortType = isShowSortType;
            usercontrol.EdmSetupCityId = CityId;
            usercontrol.DataBind();
        }

        protected void TestSendEdm(object sender, EventArgs e)
        {
            if (Id != 0)
            {
                string[] emails = tbx_TestEmail.Text.Replace("\n", ",").Replace("\r", ",").Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                if (emails.Length > 0)
                {
                    string message = EmailFacade.GenerateNewEdmFile(Id, UserName, cbx_Compress.Checked, emails);
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "alertsuccess", "alert('" + message + "');", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "alertsuccess", "alert('請填入測試發送Email!!');", true);
                }
            }
        }

        protected void SendEdm(object sender, EventArgs e)
        {
            if (Id != 0)
            {
                string message = EmailFacade.GenerateNewEdmFile(Id, UserName, true);
                UploadDate = DateTime.Now.ToString("MM/dd HH:mm");
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "alertsuccess", "alert('" + message + "');", true);
            }
        }

        protected void TestGenerateDailyEdm(object sender, EventArgs e)
        {
            string[] emails = tbx_TestEmail.Text.Replace("\n", ",").Replace("\r", ",").Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
            if (emails.Length > 0)
            {
                EmailFacade.GenerateDailyEdm(UserName, emails);
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "alertsuccess", "alert('已上傳!!');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "alertsuccess", "alert('請填入測試發送Email!!');", true);
            }
        }

        #endregion page
    }
}
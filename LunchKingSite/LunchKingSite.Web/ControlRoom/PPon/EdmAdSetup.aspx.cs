﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core.UI;

namespace LunchKingSite.Web.ControlRoom.Ppon
{
    public partial class EdmAdSetup : RolePage
    {
        #region property
        public string UserName
        {
            get { return User.Identity.Name; }
        }
        private ICmsProvider cp;
        public static ISysConfProvider Config = ProviderFactory.Instance().GetConfig();
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            cp = ProviderFactory.Instance().GetProvider<ICmsProvider>();
                    
            if (!IsPostBack)
            {
                tbx_Start.Text = DateTime.Now.ToString("MM/dd/yyyy");
                tbx_End.Text = DateTime.Now.AddMonths(2).ToString("MM/dd/yyyy");
                cbx_d_Area.DataSource = PponCityGroup.DefaultPponCityGroup;
                cbx_d_Area.DataTextField = "CityName";
                cbx_d_Area.DataValueField = "CityId";
                cbx_d_Area.DataBind();
            }
        }

        protected void SearchAd(object sender, EventArgs e)
        {
            lab_SearchMessage.Text = string.Empty;
            ViewCmsRandomCollection data = new ViewCmsRandomCollection();
            List<ViewCmsRandom> groupby_data = new List<ViewCmsRandom>();
            DateTime startdate, enddate;
            if (DateTime.TryParse(tbx_Start.Text, out startdate) && DateTime.TryParse(tbx_End.Text, out enddate))
                data = cp.GetViewCmsRandomCollectionByTitleAndDate(tbx_Name.Text, startdate, enddate, RandomCmsType.NewEdmAd);
            else 
                data = cp.GetViewCmsRandomCollectionByTitleAndDate(tbx_Name.Text, null, null, RandomCmsType.NewEdmAd);

            if (data.Count > 0)
            {
                //一對多
                foreach (var item in data.GroupBy(x => x.Pid))
                {
                    //彙總同PID的選定城市
                    string city_list = item.Select(x => x.CityName).Aggregate((x, y) => x + "," + y);
                    ViewCmsRandom first_item = item.First();
                    first_item.CityName = city_list;
                    //選取單一代表
                    groupby_data.Add(first_item);
                }
                rpt_List.DataSource = groupby_data.OrderByDescending(x => x.StartTime);
                rpt_List.DataBind();
                pan_List.Visible = true;
            }
            else
            {
                lab_SearchMessage.Text = "查無資料!!";
                rpt_List.DataBind();
            }
        }

        protected void rptItemCommand(object sender, RepeaterCommandEventArgs e)
        {
            int id;
            if (e.CommandName == "EditItem" && int.TryParse(e.CommandArgument.ToString(), out id))
            {
                pan_List.Visible = pan_Search.Visible = pan_ImgUpData.Visible = false;
                pan_Detail.Visible = btn_d_Delete.Visible = true;
                lab_Type.Text = "修改";
                //ScriptManager.RegisterStartupScript(this, typeof(Button), "ckeditor", "$('#" + tbx_d_Html.ClientID + "').ckeditor();", true);
                ViewCmsRandomCollection data = cp.GetViewCmsRandomCollectionByPid(id, RandomCmsType.NewEdmAd);
                //勾選城市
                if (data.Count > 0)
                {
                    for (int i = 0; i < cbx_d_Area.Items.Count; i++)
                    {
                        if (data.Any(x => x.CityId.ToString() == cbx_d_Area.Items[i].Value))
                            cbx_d_Area.Items[i].Selected = true;
                        else
                            cbx_d_Area.Items[i].Selected = false;
                    }
                    //設定起始時間、名稱和內容
                    ViewCmsRandom cms = data.First();
                    tbx_d_Start.Text = cms.StartTime.ToString("MM/dd/yyyy");
                    tbx_d_End.Text = cms.EndTime.ToString("MM/dd/yyyy");
                    tbx_d_Title.Text = cms.Title;
                    tbx_d_Html.Text = HttpUtility.HtmlDecode(cms.Body);
                    hif_Id.Value = id.ToString();
                }
            }
        }

        protected void AddNew(object sender, EventArgs e)
        {
            ClearText();
            pan_List.Visible = pan_Search.Visible = pan_ImgUpData.Visible = false;
            pan_Detail.Visible = true;
            lab_Type.Text = "新增";
            // ScriptManager.RegisterStartupScript(this, typeof(Button), "ckeditor", "$('#" + tbx_d_Html.ClientID + "').ckeditor();", true);
        }

        protected void SaveDetail(object sender, EventArgs e)
        {
            int id, cityid;
            DateTime d_start, d_end;
            CmsRandomContent content;
            //城市必選
            if (cbx_d_Area.SelectedIndex != -1)
            {
                if (DateTime.TryParse(tbx_d_Start.Text, out d_start) && DateTime.TryParse(tbx_d_End.Text, out d_end))
                {
                    string message = string.Empty;
                    if (int.TryParse(hif_Id.Value, out id))
                    {
                        message = "更新";
                        content = cp.CmsRandomContentGetById(id);
                        content.ModifiedOn = DateTime.Now;
                        content.ModifiedBy = UserName;
                        //將所有一對多城市選擇刪除
                        cp.CmsRandomCityDelete(id, (int)RandomCmsType.NewEdmAd);
                        btn_d_Delete.Visible = true;
                    }
                    else
                    {
                        message = "新增";
                        content = new CmsRandomContent();
                        content.CreatedBy = UserName;
                        content.CreatedOn = DateTime.Now;
                    }
                    content.Title = tbx_d_Title.Text;
                    content.ContentTitle = content.ContentName = "EDM AD";
                    content.Locale = "zh_TW";
                    content.Status = true;
                    content.Body = tbx_d_Html.Text;
                    content.Type = (int)RandomCmsType.NewEdmAd;
                    cp.CmsRandomContentSet(content);
                    //加入對應選擇城市
                    for (int i = 0; i < cbx_d_Area.Items.Count; i++)
                    {
                        if (cbx_d_Area.Items[i].Selected && int.TryParse(cbx_d_Area.Items[i].Value, out cityid))
                        {
                            CmsRandomCity city = new CmsRandomCity();
                            city.Pid = content.Id;
                            city.CityId = cityid;
                            city.CityName = cbx_d_Area.Items[i].Text;
                            city.StartTime = d_start;
                            city.EndTime = d_end;
                            city.Status = true;
                            city.Ratio = 1;
                            city.Type = (int)RandomCmsType.NewEdmAd;
                            cp.CmsRandomCitySet(city);
                        }
                    }
                    lab_Message.Text = message + "成功!!";


                }
                else
                    lab_Message.Text = "日期格式錯誤!!";
            }
            else
                lab_Message.Text = "請選擇區域!!";
            tbx_d_Html.Text = HttpUtility.HtmlDecode(tbx_d_Html.Text);
            //ScriptManager.RegisterStartupScript(this, typeof(Button), "ckeditor", "$('#" + tbx_d_Html.ClientID + "').ckeditor();", true);
        }

        protected void DeleteAll(object sender, EventArgs e)
        {
            int id;
            if (int.TryParse(hif_Id.Value, out id))
            {
                cp.CmsRandomContentDelete(id);
                cp.CmsRandomCityDelete(id, (int)RandomCmsType.NewEdmAd);
                rpt_List.DataBind();
                pan_List.Visible = pan_Detail.Visible = false;
                pan_Search.Visible = true;
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "alertsave", "alert('刪除成功!!');", true);
            }
        }

        protected void CancelAction(object sender, EventArgs e)
        {
            pan_Search.Visible = true;
            pan_List.Visible = pan_Detail.Visible = false;
            ClearText();
        }

        private void ClearText()
        {
            tbx_d_Start.Text = tbx_d_End.Text = tbx_d_Html.Text = tbx_d_Title.Text =
                hif_Id.Value = lab_Message.Text = lab_SearchMessage.Text = string.Empty;
            cbx_d_Area.ClearSelection();
            btn_d_Delete.Visible = false;
        }

        protected void btn_UpDataImg_Click(object sender, EventArgs e)
        {
            pan_ImgUpData.Visible = !pan_ImgUpData.Visible;
        }

        protected void btn_UpData_Click(object sender, EventArgs e)
        {
            Boolean fileOK = false;
            String path = Server.MapPath("~/images/17P/17topbn/");

            if (filMyFile.PostedFile != null)
            {
                const string rule = "^[0-9a-zA-Z_]*$";
                String Msg = "檔案格式錯誤";
                String fileExtension =System.IO.Path.GetExtension(filMyFile.FileName).ToLower();
                String[] allowedExtensions = { ".png",".jpg" };
                for (int i = 0; i < allowedExtensions.Length; i++)
                {
                    if (fileExtension == allowedExtensions[i])
                    {
                        fileOK = true;
                    }
                }
                if (!Regex.IsMatch(filMyFile.FileName.Replace(".png", "").Replace(".jpg", ""), rule))
                {
                    fileOK = false;
                    Msg = "檔案名稱僅限英數字或\"_\"，不得包含空白字元";
                }
                if (fileOK)
                {
                    try
                    {
                        filMyFile.PostedFile.SaveAs(path + filMyFile.FileName);

                        String cms = "&lt;a href=\"" + tbx_upUrl.Text + "\" target=\"_blank\"&gt;&lt;img alt=\"" + tbx_upTitle.Text + 
                                     "\" height=\"80\" src=\"" + Config.SiteUrl + "/images/17P/17topbn/" + filMyFile.FileName +
                                     "\" width=\"740\" /&gt;&lt;/a&gt;";

                        tbx_d_Title.Text = tbx_upTitle.Text;
                        tbx_d_Html.Text = HttpUtility.HtmlDecode(cms);

                        pan_List.Visible = pan_Search.Visible = pan_ImgUpData.Visible = false;
                        pan_Detail.Visible = true;
                        lab_Type.Text = "新增";

                    }
                    catch (Exception)
                    {
                        pan_ImgUpData.Visible = true;
                        lab_UpdataMessage.Text = "檔案上傳失敗！";
                    }
                }
                else
                {
                    pan_ImgUpData.Visible = true;
                    lab_UpdataMessage.Text =  Msg + "！";
                }

            }
            else
            {
                pan_ImgUpData.Visible = true;
                lab_UpdataMessage.Text = "請選擇上傳圖檔！";
            }        
            
        }
        
    }
}
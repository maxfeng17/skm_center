﻿using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LunchKingSite.Web.ControlRoom.Ppon
{
    public partial class ExportCouponList : RolePage
    {
        protected static ISysConfProvider config = ProviderFactory.Instance().GetConfig();

        protected struct SearchColumn
        {
            public const string Bid = "Bid";
            public const string DealId = "DealId";
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                ddlSelect.Items.Add(new ListItem("Bid", SearchColumn.Bid));
                ddlSelect.Items.Add(new ListItem("檔號", SearchColumn.DealId));
            }
        }

        protected void ExportToExcel_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(BusinessHourGuid.Text))
            {
                ScriptManager.RegisterStartupScript(Page, typeof(ImageButton), "alert", "alert('請先輸入想匯入的bid，謝謝。');", true);
            }
            else
            {
                try
                {
                    Guid bid = new Guid();
                    switch (ddlSelect.SelectedValue)
                    {
                        case SearchColumn.Bid:
                            bid = new Guid(BusinessHourGuid.Text);
                            break;

                        case SearchColumn.DealId:
                            string DealId = BusinessHourGuid.Text;
                            ViewPponDealCollection vpdc = ProviderFactory.Instance().GetProvider<IPponProvider>().ViewPponDealGetListPaging
                                    (0, 0, ViewPponDeal.Columns.UniqueId, ViewPponDeal.Columns.UniqueId + " = " + DealId);
                            if (vpdc.Count > 0)
                            {
                                bid = vpdc[0].BusinessHourGuid;
                            }
                            break;
                    }

                    DataTable dt = ProviderFactory.Instance().GetProvider<IPponProvider>().GetSellerCouponList(bid);
                    if (dt.Rows.Count > 0)
                    {
                        IPponProvider pponProvider = ProviderFactory.Instance().GetProvider<IPponProvider>();
                        ViewPponCoupon vpc = pponProvider.ViewPponCouponGetCouponEventContent(ViewPponCoupon.Columns.BusinessHourGuid, bid);
                        DealProperty dp = pponProvider.DealPropertyGet(bid);

                        string couponusage = string.Empty;
                        couponusage = vpc.CouponUsage;

                        if (!string.IsNullOrEmpty(couponusage) && couponusage.Length > 50)
                        {
                            couponusage = couponusage.Substring(0, 49) + "...";
                        }

                        OrderFacade.ExportCouponList(bid, vpc.CouponUsage, vpc.Introduction, vpc.BusinessHourOrderTimeS, vpc.BusinessHourOrderTimeE,
                            (DateTime)vpc.BusinessHourDeliverTimeS, (DateTime)vpc.BusinessHourDeliverTimeE, dt, dp.UniqueId, vpc.SellerName, vpc.StoreName, 
                            dt.Rows.Count.ToString());
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(Page, typeof(ImageButton), "alert", "alert('無憑證資料。');", true);
                    }
                }
                catch 
                {
                    ScriptManager.RegisterStartupScript(Page, typeof(ImageButton), "alert", "alert('bid輸入錯誤，請檢查後重新輸入，謝謝。');", true);
                }
            }
        }

    }
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ControlRoom/backend.master" AutoEventWireup="true"
    CodeBehind="EdmTitleSetUp.aspx.cs" Inherits="LunchKingSite.Web.ControlRoom.Ppon.EdmTitleSetUp" %>

<%@ Register Src="../Controls/EdmTitleItem.ascx" TagName="EdmTitleItem" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <style type="text/css">
        .bgOrange
        {
            background-color: #FFC690;
            text-align: center;
        }
        .bgBlue
        {
            background-color: #B3D5F0;
        }
        .bgLBlue
        {
            background-color: #B2DFEE;
        }
        .bgLOrange
        {
            background-color: #FFEC8B;
            font-size: smaller;
        }
        .title
        {
            text-align: center;
            background-color: #688E45;
            font-weight: bolder;
            color: White;
        }
        .title1
        {
            text-align: center;
            background-color: #8B8970;
            font-weight: bolder;
            color: White;
        }
        .title2
        {
            text-align: center;
            background-color: #778899;
            font-weight: bolder;
            color: White;
        }
        .header
        {
            text-align: center;
            font-size: large;
            font-weight: bolder;
            background-color: #F0AF13;
            color: Blue;
        }
        .fieldset
        {
            border: #688E45 solid 2px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <fieldset class="fieldset">
        <legend class="title">訂閱信主旨編輯</legend>
        <uc1:EdmTitleItem ID="EdmTitleItem1" runat="server" />
        <uc1:EdmTitleItem ID="EdmTitleItem2" runat="server" />
        <uc1:EdmTitleItem ID="EdmTitleItem3" runat="server" />
        <uc1:EdmTitleItem ID="EdmTitleItem4" runat="server" />
        <uc1:EdmTitleItem ID="EdmTitleItem5" runat="server" />
        <uc1:EdmTitleItem ID="EdmTitleItem6" runat="server" />
        <uc1:EdmTitleItem ID="EdmTitleItem7" runat="server" />
        <uc1:EdmTitleItem ID="EdmTitleItem8" runat="server" />
        <uc1:EdmTitleItem ID="EdmTitleItem9" runat="server" />
        <uc1:EdmTitleItem ID="EdmTitleItem10" runat="server" />
        <asp:Button ID="btn_Save" runat="server" Text="存檔" OnClick="SaveItem" />
    </fieldset>
</asp:Content>

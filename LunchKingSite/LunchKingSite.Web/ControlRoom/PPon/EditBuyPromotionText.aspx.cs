﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using log4net;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Models.Ppon;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;

namespace LunchKingSite.Web.ControlRoom.Ppon
{
    public partial class EditBuyPromotionText : Page
    {
        private ISerializer serializer = ProviderFactory.Instance().GetSerializer();
        private ISystemProvider ss = ProviderFactory.Instance().GetProvider<ISystemProvider>();

        private ILog logger = LogManager.GetLogger(typeof(EditBuyPromotionText));
        private TextBox[] textControls = new TextBox[5];
        protected void Page_Load(object sender, EventArgs e)
        {
            textControls[0] = txtLine1;
            textControls[1] = txtLine2;
            textControls[2] = txtLine3;
            textControls[3] = txtLine4;
            textControls[4] = txtLine5;
            if (this.IsPostBack == false)
            {
                InitForm();
            }
        }

        private void InitForm()
        {
            var data = ss.SystemDataGet(SystemDataManager._BUY_PROMOTION_TEXT);
            if (data.IsLoaded)
            {
                List<BuyPromotionText> lines = 
                    ProviderFactory.Instance().GetSerializer().Deserialize<List<BuyPromotionText>>(data.Data);
                for (int i = 0; i < lines.Count; i++)
                {
                    textControls[i].Text = lines[i].Text;
                }
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            List<BuyPromotionText> lines = new List<BuyPromotionText>();
            for (int i = 0; i < textControls.Length; i++)
            {
                if (textControls[i].Text != string.Empty)
                {
                    lines.Add(new BuyPromotionText {Text = textControls[i].Text});
                }
            }
            var data = ss.SystemDataGet(SystemDataManager._BUY_PROMOTION_TEXT);
            if (data.IsLoaded == false)
            {
                string msg = "SystemData 找不到 " + SystemDataManager._BUY_PROMOTION_TEXT;
                logger.Error(msg);
                throw new Exception(msg);
            }
            data.Data = serializer.Serialize(lines);
            ss.SystemDataSet(data);
            SystemDataManager.ClearBuyPromotionTextCache();
            ShowMessage("更新完成");
        }
    
        private void ShowMessage(string s)
        {
            divMessage.InnerHtml = s;
        }
    }
}
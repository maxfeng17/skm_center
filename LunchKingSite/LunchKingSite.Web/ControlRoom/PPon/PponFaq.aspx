﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PponFaq.aspx.cs" Inherits="LunchKingSite.Web.ControlRoom.Ppon.PponFaq"
    MasterPageFile="~/ControlRoom/backend.master" %>

<%@ Import Namespace="LunchKingSite.DataOrm" %>
<%@ Import Namespace="LunchKingSite.Core" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit.HTMLEditor"
    TagPrefix="HTMLEditor" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
    <style>
        .slotItem ul {
            margin-bottom: 3px;
            border: dashed 1px gray;
            padding: 2px 2px 2px 5px;
        }

            .slotItem ul.hover {
                background-color: #E5ECF9;
            }

        .slotItem li {
            font-weight: normal;
            font-size: smaller;
            margin: 0px;
        }

            .slotItem li.eb, .slotItem li.db, .slotItem li.cb {
                display: none;
                float: left;
                padding-right: 15px;
                text-decoration: underline;
                color: blue;
                cursor: pointer;
            }
    </style>
    <script type="text/javascript" src="//www.google.com/jsapi"></script>
    <script type="text/javascript" src="../../Tools/js/json2.js"></script>
    <script src="../../Tools/js/jquery-1.7.2.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        google.load("jqueryui", "1.8");
        function toggleAvailMode(dom, on) {
            if (on) {
                $(dom).addClass('hover');
            } else {
                $(dom).removeClass('hover');
            }
        }
        function UserTrackSet(cellId) {
            var dataStr = "";
            $(cellId + ' > ul').each(function () {
                var lis = $(this).children('li');
                var id = $(lis.get(0)).text();
                if (id != '') {
                    dataStr = dataStr + id + ",";
                }
            });
            $.ajax({
                type: "POST",
                url: "PponFaq.aspx/SetSlotData",
                data: '{"data":"' + dataStr + '"}',
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (msg) {
                    if (msg.d != '') {
                        alert(msg.d);
                    }
                    else {
                        if (confirm('全部排序完後請重整，是否要重整頁面?')) {
                            location.reload();
                        }
                    }
                }
            });
            return true;
        }
        function SetCellMethod(cellId) {
            $(cellId + '> ul').live('mouseover mouseout', function (event) {
                if (event.type == 'mouseover') {

                    toggleAvailMode(this, true);
                } else {

                    toggleAvailMode(this, false);
                }
            });

            $(cellId).sortable();
            $(cellId).bind("sortupdate", function (event, ui) {
                UserTrackSet(cellId);
            }
        );
        }

    </script>

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table style="width: 800px">
                <tr>
                    <td colspan="4" style="text-align: center; font-size: large; font-weight: bolder; background-color: #F0AF13; color: White">FAQ
                    </td>
                </tr>
                <tr>
                    <td style="background-color: #688E45; font-weight: bolder; color: White;">館別</td>
                    <td style="background-color: #B3D5F0" colspan="3">
                        <asp:RadioButtonList ID="rbl_Department" runat="server" RepeatDirection="Horizontal" OnSelectedIndexChanged="Department_SelectedIndexChanged" AutoPostBack="true">
                            <asp:ListItem Text="P好康" Value="Ppon" Selected="True"></asp:ListItem>
                            <asp:ListItem Text="品生活" Value="HiDeal"></asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr>
                    <td rowspan="2" style="width: 10%; background-color: #688E45; font-weight: bolder; color: White">項目
                    </td>
                    <td style="width: 25%; background-color: #688E45; font-weight: bolder; color: White">大標題
                    </td>
                    <td style="background-color: #688E45; font-weight: bolder; color: White" colspan="2">問題
                    </td>
                </tr>
                <tr>
                    <td style="background-color: #B3D5F0">
                        <asp:DropDownList ID="ddl_Sequence1" runat="server" OnSelectedIndexChanged="BindControl"
                            AutoPostBack="true" Width="100%">
                        </asp:DropDownList>
                    </td>
                    <td style="background-color: #B3D5F0" colspan="2">
                        <asp:DropDownList ID="ddl_Sequence2" runat="server" OnSelectedIndexChanged="BindControl"
                            AutoPostBack="true" Width="100%">
                        </asp:DropDownList>
                        <asp:DropDownList ID="ddl_Sequence3" runat="server" Visible="false">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td style="background-color: #688E45; font-weight: bolder; color: White">解答
                    </td>
                    <td colspan="3" style="background-color: white">
                        <asp:Literal ID="lit_Contents" runat="server"></asp:Literal>
                    </td>
                </tr>
                <tr>
                    <td style="width: 10%; background-color: #688E45; font-weight: bolder; color: White">編輯
                    </td>
                    <td colspan="2'" style="background-color: #B3D5F0">
                        <asp:RadioButtonList ID="rbl_Edit" runat="server" RepeatDirection="Horizontal" OnSelectedIndexChanged="CheckAddEnable"
                            AutoPostBack="true">
                            <asp:ListItem Text="大標題" Value="0" Selected="True"></asp:ListItem>
                            <asp:ListItem Text="問題" Value="1"></asp:ListItem>
                            <asp:ListItem Text="解答" Value="2"></asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                    <td style="background-color: #B3D5F0">
                        <asp:Button ID="btn_Add" runat="server" Text="新增" OnClick="FaqEdit" />
                        <asp:Button ID="btn_Edit" runat="server" Text="修改" OnClick="FaqEdit" />
                        <asp:Button ID="btn_Delete" runat="server" Text="刪除" OnClick="FaqDelete" ForeColor="Red" OnClientClick="return confirm('確定刪除此項目和其子項?');" />
                        <asp:Button ID="btn_Sort" runat="server" Text="排序" OnClick="FaqEdit" />
                    </td>
                </tr>
                <tr>
                    <td colspan="4'" style="font-weight: bolder; background-color: #B3D5F0;">
                        <asp:Label ID="plbl_Type" runat="server" Visible="false" Font-Bold="true" Font-Size="Large"
                            ForeColor="Blue"></asp:Label>
                        <asp:Label ID="plbl_Title" runat="server" Visible="false"></asp:Label>
                        <asp:Panel ID="pan_Edit" runat="server" Visible="false">
                            <asp:HiddenField ID="hid_Id" runat="server" />
                            <asp:HiddenField ID="hid_type" runat="server" />
                            <br />
                            <asp:RadioButtonList ID="prbl_Align" runat="server" RepeatDirection="Horizontal"
                                Visible="false">
                                <asp:ListItem Text="靠左" Value="L" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="靠右" Value="R"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:Button ID="pbtn_OK" runat="server" Text="確認" OnClick="ConfirmEdit" ForeColor="Blue" />
                            <asp:Button ID="pbtn_Cancel" runat="server" Text="取消" OnClick="CancelEdit" />
                            <br />
                            <HTMLEditor:Editor ID="tbx_editor" runat="server" Height="250px" Width="650px" AutoFocus="true" />
                        </asp:Panel>
                        <asp:Panel ID="pan_Sort" runat="server" Visible="false">
                            <asp:Button ID="btn_Close" runat="server" Text="關閉" OnClick="CancelEdit" />
                            <div id="sortdata" class="slotItem" style="text-align: left">
                                <asp:Repeater ID="rList" runat="server">
                                    <ItemTemplate>
                                        <ul style="list-style: none">
                                            <li style="display: none;">
                                                <%#((Faq)(Container.DataItem)).Id %>
                                            </li>
                                            <li>內容:
                                                <%#((Faq)(Container.DataItem)).Contents%>
                                            </li>
                                            <li>建檔日:
                                                <%#((Faq)(Container.DataItem)).CreateTime.ToString("yyyy/MM/dd") %>   建檔人: <%#((Faq)(Container.DataItem)).Creator %>
                                            </li>
                                            <li>版面位置:
                                                <%#
                                                !string.IsNullOrEmpty(((Faq)(Container.DataItem)).Mark)?
                                                ((Faq)(Container.DataItem)).Mark.Equals("H")?string.Empty:((Faq)(Container.DataItem)).Mark:string.Empty %>
                                            </li>
                                        </ul>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </div>
                        </asp:Panel>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

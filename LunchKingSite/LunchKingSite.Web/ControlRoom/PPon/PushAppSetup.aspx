﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ControlRoom/backend.master" AutoEventWireup="true"
    CodeBehind="PushAppSetup.aspx.cs" Inherits="LunchKingSite.Web.ControlRoom.Ppon.PushAppSetup" %>

<%@ Import Namespace="LunchKingSite.DataOrm" %>
<%@ Import Namespace="LunchKingSite.Core" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Ppon/Pager.ascx" TagName="Pager" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <link type="text/css" href="../../Tools/js/css/ui-lightness/jquery-ui-1.8rc3.custom.css"
        rel="stylesheet" />
    <style type="text/css">
        .pushDialog {
            padding:3px;
        }
        .pushDialog .title {
            font-size:16px;
            font-weight:bold;
        }
        .pushDialog .body {
            margin-top:12px;
        }
        .pushDialog .bottom {
            margin-top:12px;
            text-align:center;
        }
        .pushDialog .tab {
            display: none;
        }
        .pushDialg .tab-actived {
            display:block;
        }
    </style>
    <script type="text/javascript" src="../../Tools/js/jquery-1.7.2.min.js"></script>
    <script type="text/javascript" src="../../Tools/js/jquery.blockUI.js"></script>
    <script type="text/javascript" src="../../Tools/js/jquery-ui-1.9.2.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            PushTypeChange($('[id*=rdlPushAppType]'));
            //HiedBrand();


            //region pushTest
            $('.pushAction').click(function(evt) {
                var refId = $(this).attr('ref-id');
                $('#popupPush2').dialog(
                    {
                        title: "推播測試",
                        width: 720,
                        height: 360
                    }
                );
                $('#popupPush2').find('.title').text("Push Id = " + refId);
                
                $('#dPushId').val(refId);

                return;                
            });

            $('#btnPush').click(function() {
                var tokens = $('#ttTokens').val().split('\n');                
                var data = { "pushId": $('#dPushId').val(), "tokens": tokens };
                if (tokens == null || tokens.length == 0) {
                    alert('沒有任何裝置Token');
                    return;
                }
                if (tokens.length > 10 && window.confirm("要推的裝置有點多，請再次確認") == false) {
                    return;
                }
                $.ajax({
                    type: "POST",
                    url: "PushAppSetup.aspx/PushAppAsync",
                    data: JSON.stringify(data),
                    async: false,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function(res) {
                        alert(res.d);
                    },
                    error: function(res) {
                        alert('error. try again.');
                    }
                });
            });

            $('#ttTokens').bind('input propertychange', function() {
                var tokens = $('#ttTokens').val().split('\n');
                var ios = 0;
                var android = 0;
                for (var key in tokens) {
                    if (tokens[key].length == 64) {
                        ios++;
                    } else if (tokens[key].length>64){
                        android++;
                    }
                }

                if (ios > 0 || android > 0) {
                    $('#spPushDesc').text('iOS*' + ios + ', Android*' + android);
                } else {
                    $('#spPushDesc').text('');
                }
            });
            $('.tab-actived').show();
            $('.linkTab', '.pushDialog').click(function() {
                console.log('linkTab was clicked');
                $('.tab').hide();
                var tabId = $(this).attr('tabId');
                console.log('tab: ' + tabId);
                $('#' + tabId).show();
                qdMgr.showMessage('沒有符合的資料');
            });

            function QueryDeviceManager() {
                this.showMessage = function(msg) {
                    $('#tbDevices').find('tbody').find('tr').not('.message').remove();
                    $('#tbDevices').find('tbody').find('tr.message').text(msg).show();
                }
                this.appendRow = function(s1,s2, s3,s4,s5) {
                    $('#tbDevices').find('tbody').find('.message').hide();
                    var row = $('<tr></tr>').appendTo($('#tbDevices').find('tbody'));
                    var cell0 = $('<td></td>').appendTo(row);
                    var cell1 = $('<td></td>').appendTo(row);
                    var cell2 = $('<td></td>').appendTo(row);
                    var cell3 = $('<td></td>').appendTo(row);
                    var cell4 = $('<td></td>').appendTo(row);
                    cell0.append($('<input type=checkbox />').attr('checked', true).val(s1));
                    cell1.text(s2);
                    cell2.text(s3);
                    cell3.text(s4);
                    cell4.text(s5);
                }
                this.queryDevices = function(account) {
                    var data = { "account": account };
                    $.ajax({
                        type: "POST",
                        url: "PushAppSetup.aspx/QueryDevices",
                        data: JSON.stringify(data),
                        async: true,
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        success: function(res) {
                            if (res.d == null) {
                                qdMgr.showMessage('沒有符合的資料');
                                return;
                            }
                            for (var key in res.d) {
                                qdMgr.appendRow(res.d[key].Token,
                                    res.d[key].ShortToken,
                                    res.d[key].Model,
                                    res.d[key].OsVer,
                                    res.d[key].AppVer);
                            }
                        },
                        error: function(res) {
                            alert('error. try again.');
                        }
                    });
                };
            };
            var qdMgr = new QueryDeviceManager();

            //query mode
            $('#btnQueryDevices').click(function() {
                qdMgr.showMessage('資料載入中');
                var account = $('#txtQueryAccount').val();
                if (account == '') {
                    $('#txtQueryAccount').focus();
                    return;
                }
                qdMgr.queryDevices(account);
            });
 
            $('#btnQueryMyDevices').click(function() {
                qdMgr.showMessage('資料載入中');
                qdMgr.queryDevices('');
            });

            $('#btnAddToken').click(function() {
                var tokens = $('#ttTokens').val().split('\n').filter(t=>t!='');;
                var skipCount = 0;
                var addCount = 0;
                $('#tbDevices').find('tbody').find(':checked').each(function() {
                    var token = $(this).val();
                    if (jQuery.inArray(token, tokens) == -1) {
                        addCount++;
                        console.log('push:' + token);
                        tokens.push(token);
                    } else {
                        skipCount++;
                    }
                });
                $('#ttTokens').val(tokens.join('\n'));
                $('#ttTokens').trigger('propertychange');
                var msg = '';
                if (addCount > 0) {
                    msg += '新增了 ' + addCount + ' 筆資料. ';
                }
                if (skipCount > 0) {
                    msg += skipCount + ' 筆資料因重複略過';
                }
                $('#txtQueryAccount').val('').focus();
                qdMgr.showMessage(msg);
            });

            //endregion pushTest

        });

        function HiedBrand()
        {
            var isShowBrand = '<%=config.EnableControlroomPushBrand%>';
            if (isShowBrand == 'False')
            {
                $(".BrandPromo").hide();
                $('[id*=rdlPushAppType_4]').hide();
                $('[for*=rdlPushAppType_4]').hide();
            }
        }

        function SaveCheck() {
            if ($('[id*=txtSendDate]').val() == '') {
                alert('請輸入推播時間!!!');
                return false;
            }
            if ($('[id*=chklPushArea] input:checked').length == 0) {
                alert('請選擇推播訂閱地區!!!');
                return false;
            }
            if ($('[id*=rdPushAppBid]').is(':checked') && $('[id*=txtBid]').val() == '') {
                alert('請輸入檔次bid');
                return false;
            }
            if ($('[id*=rdPushAppPponCity]').is(':checked') && $('[id*=rdlPponCityGroup] input:checked').length == 0) {
                alert('請選擇頻道');
                return false;
            }
            if ($('[id*=rdPushAppVourcherId]').is(':checked') && $('[id*=txtVourcherId]').val() == '') {
                alert('請輸入優惠券編號');
                return false;
            }
            if ($('[id*=rdPushAppSellerId]').is(':checked') && $('[id*=txtSellerId]').val() == '') {
                alert('請輸入優惠券店家編號');
                return false;
            }
            
            if ($('[id*=rdPushAppEventPromo]').is(':checked') && $('[id*=txtEventPromoId]').val() == '') {
                alert('請輸入商品主題活動編號');
                return false;
            }

            if ($('[id*=rdPushAppBrandPromo]').is(':checked') && $('[id*=txtBrandPromoId]').val() == '') {
                alert('請輸入策展2.0活動編號');
                return false;
            }

            if ($('[id*=rdPushAppCustomUrl]').is(':checked') && $('[id*=txtCustomUrl]').val() == '') {
                alert('請輸入自訂連結');
                return false;
            }

            
            if ($('[id*=rdPushAppCustomUrl]').is(':checked') && $('[id*=txtCustomUrl]').val().indexOf("https://www.17life.com") < 0 && $('[id*=txtCustomUrl]').val().indexOf("http://www.17life.com") < 0) {
                alert('自訂連結內請包含 http://www.17life.com 或是 https://www.17life.com');
                return false;
            }

            
            if ($.trim($('[id*=txtDescription]').val()) == '') {
                alert('請輸入推播文字內容');
                return false;
            }
            
        }

        function PushItemControl() {
            if ($('[id*=rdPushAppBid]').is(':checked')) {
                $('[id*=rdPushAppBid]').next().next('span').css('display', '');
            } else {
                $('[id*=rdPushAppBid]').next().next('span').css('display', 'none');
                $('[id*=txtBid]').val('');
            }
            if ($('[id*=rdPushAppPponCity]').is(':checked')) {
                $('[id*=rdPushAppPponCity]').next().next('span').css('display', '');
            } else {
                $('[id*=rdPushAppPponCity]').next().next('span').css('display', 'none');
                $('input[id*=rdlPponCityGroup]').eq(0).attr('checked', true);
            }
            if ($('[id*=rdPushAppVourcherId]').is(':checked')) {
                $('[id*=rdPushAppVourcherId]').next().next('span').css('display', '');
            } else {
                $('[id*=rdPushAppVourcherId]').next().next('span').css('display', 'none');
                $('[id*=txtVourcherId]').val('');
            }
            if ($('[id*=rdPushAppSellerId]').is(':checked')) {
                $('[id*=rdPushAppSellerId]').next().next('span').css('display', '');
            } else {
                $('[id*=rdPushAppSellerId]').next().next('span').css('display', 'none');
                $('[id*=txtSellerId]').val('');
            }
            
            if ($('[id*=rdPushAppEventPromo]').is(':checked')) {
                $('[id*=rdPushAppEventPromo]').next().next('span').css('display', '');
                $('[id*=txtBrandPromoId]').val('');
            } else {
                $('[id*=rdPushAppEventPromo]').next().next('span').css('display', 'none');
                $('[id*=txtEventPromoId]').val('');
            }
            if ($('[id*=rdPushAppBrandPromo]').is(':checked')) {
                $('[id*=rdPushAppBrandPromo]').next().next('span').css('display', '');
                $('[id*=txtEventPromoId]').val('');
            } else {
                $('[id*=rdPushAppBrandPromo]').next().next('span').css('display', 'none');
                $('[id*=txtBrandPromoId]').val('');
            }
            if ($('[id*=rdPushAppCustomUrl]').is(':checked')) {
                $('[id*=rdPushAppCustomUrl]').next().next('span').css('display', '');
            } else {
                $('[id*=rdPushAppCustomUrl]').next().next('span').css('display', 'none');
                $('[id*=txtCustomUrl]').val('');
            }


        }

        function PushTypeChange(obj) {
            var checkVal = $(obj).find(':checked').val();
            var rdPushAppBid = $('[id*=rdPushAppBid]');
            var rdPushAppPponCity = $('[id*=rdPushAppPponCity]');
            var rdPushAppVourcherId = $('[id*=rdPushAppVourcherId]');
            var rdPushAppSellerId = $('[id*=rdPushAppSellerId]');
            var rdPushAppEventPromo = $('[id*=rdPushAppEventPromo]');
            var rdPushAppBrandPromo = $('[id*=rdPushAppBrandPromo]');
            var rdPushAppCustomUrl = $('[id*=rdPushAppCustomUrl]');
            //$(".rdPushApp").hide();
            $(".rdPushApp").css("color", "#A9A9A9");

            if (checkVal == '<%= (int)PushAppType.PponDeal %>') {
                //$(".PushBid").show();
                //$(".PponCity").show();
                $(".PushBid").css("color", "Black");
                $(".PponCity").css("color", "Black");
                rdPushAppBid.attr('checked', 'checked');
                rdPushAppPponCity.removeAttr('checked');
                rdPushAppVourcherId.removeAttr('checked');
                rdPushAppSellerId.removeAttr('checked');
                rdPushAppEventPromo.removeAttr('checked');
                rdPushAppBrandPromo.removeAttr('checked');
                rdPushAppCustomUrl.removeAttr('checked');

                rdPushAppBid.removeAttr('disabled');
                rdPushAppPponCity.removeAttr('disabled');
                rdPushAppVourcherId.attr('disabled', 'disabled');
                rdPushAppSellerId.attr('disabled', 'disabled');
                rdPushAppEventPromo.attr('disabled', 'disabled');
                rdPushAppBrandPromo.attr('disabled', 'disabled');
                rdPushAppCustomUrl.attr('disabled', 'disabled');
            }
            else if (checkVal == '<%= (int)PushAppType.Vourcher %>') {
                //$(".VourcherId").show();
                //$(".SellerId").show();
                $(".VourcherId").css("color", "Black");
                $(".SellerId").css("color", "Black");
                rdPushAppBid.removeAttr('checked');
                rdPushAppPponCity.removeAttr('checked');
                rdPushAppVourcherId.attr('checked', 'checked');
                rdPushAppSellerId.removeAttr('checked');
                rdPushAppEventPromo.removeAttr('checked');
                rdPushAppBrandPromo.removeAttr('checked');
                rdPushAppCustomUrl.removeAttr('checked');
                
                rdPushAppBid.attr('disabled', 'disabled');
                rdPushAppPponCity.attr('disabled', 'disabled');
                rdPushAppVourcherId.removeAttr('disabled');
                rdPushAppSellerId.removeAttr('disabled');
                rdPushAppEventPromo.attr('disabled', 'disabled');
                rdPushAppBrandPromo.attr('disabled', 'disabled');
                rdPushAppCustomUrl.attr('disabled', 'disabled');
            }
            else if (checkVal == '<%= (int)PushAppType.EventPromo %>') {
                //$(".EventPromo").show();
                $(".EventPromo").css("color", "Black");
                rdPushAppBid.removeAttr('checked');
                rdPushAppPponCity.removeAttr('checked');
                rdPushAppVourcherId.removeAttr('checked');
                rdPushAppSellerId.removeAttr('checked');
                rdPushAppEventPromo.attr('checked', 'checked');
                rdPushAppBrandPromo.removeAttr('checked');
                rdPushAppCustomUrl.removeAttr('checked');

                rdPushAppBid.attr('disabled', 'disabled');
                rdPushAppPponCity.attr('disabled', 'disabled');
                rdPushAppVourcherId.attr('disabled', 'disabled');
                rdPushAppSellerId.attr('disabled', 'disabled');
                rdPushAppEventPromo.removeAttr('disabled');
                rdPushAppBrandPromo.attr('disabled', 'disabled');
                rdPushAppCustomUrl.attr('disabled', 'disabled');
            }
            else if (checkVal == '<%= (int)PushAppType.BrandPromo %>') {
                //$(".BrandPromo").show();
                $(".BrandPromo").css("color", "Black");
                rdPushAppBid.removeAttr('checked');
                rdPushAppPponCity.removeAttr('checked');
                rdPushAppVourcherId.removeAttr('checked');
                rdPushAppSellerId.removeAttr('checked');
                rdPushAppEventPromo.removeAttr('checked');
                rdPushAppBrandPromo.attr('checked', 'checked');
                rdPushAppCustomUrl.removeAttr('checked');

                rdPushAppBid.attr('disabled', 'disabled');
                rdPushAppPponCity.attr('disabled', 'disabled');
                rdPushAppVourcherId.attr('disabled', 'disabled');
                rdPushAppSellerId.attr('disabled', 'disabled');
                rdPushAppEventPromo.attr('disabled', 'disabled');
                rdPushAppBrandPromo.removeAttr('disabled');
                rdPushAppCustomUrl.attr('disabled', 'disabled');
            }
            else if (checkVal == '<%= (int)PushAppType.CustomUrl %>') {
                //$(".CustomUrl").show();
                $(".CustomUrl").css("color", "Black");
                rdPushAppBid.removeAttr('checked');
                rdPushAppPponCity.removeAttr('checked');
                rdPushAppVourcherId.removeAttr('checked');
                rdPushAppSellerId.removeAttr('checked');
                rdPushAppEventPromo.removeAttr('checked');
                rdPushAppBrandPromo.removeAttr('checked');
                rdPushAppCustomUrl.attr('checked', 'checked');

                rdPushAppBid.attr('disabled', 'disabled');
                rdPushAppPponCity.attr('disabled', 'disabled');
                rdPushAppVourcherId.attr('disabled', 'disabled');
                rdPushAppSellerId.attr('disabled', 'disabled');
                rdPushAppEventPromo.attr('disabled', 'disabled');
                rdPushAppBrandPromo.attr('disabled', 'disabled');
                rdPushAppCustomUrl.removeAttr('disabled');


            } else {
                <%--- 不該有這樣的狀況 ---%>
            }
            PushItemControl();
        }

        function LimitTextArea(field) {
            maxlimit = 80;
            if (field.value.length > maxlimit)
                field.value = field.value.substring(0, maxlimit);

        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Panel ID="divPushAppSearch" runat="server">
        <h1>
            App推播管理列表</h1>
        推播日期：<asp:TextBox ID="txtSendDateSearchS" runat="server" Columns="8" />
        <cc1:CalendarExtender ID="ceSendDateSearchS" TargetControlID="txtSendDateSearchS"
            runat="server" Format="yyyy/MM/dd">
        </cc1:CalendarExtender>
        ～<asp:TextBox ID="txtSendDateSearchE" runat="server" Columns="8" />
        <cc1:CalendarExtender ID="ceSendDateSearchE" TargetControlID="txtSendDateSearchE"
            runat="server" Format="yyyy/MM/dd">
        </cc1:CalendarExtender>
        <br />
        推播內容：<asp:TextBox ID="txtDescriptionSearch" runat="server" Width="200px"></asp:TextBox>
        <br />
        類型：<asp:RadioButtonList ID="rdlPushTypeSearch" runat="server" RepeatLayout="Flow"
            RepeatDirection="Horizontal">
        </asp:RadioButtonList>
        <br />
        <asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_Click" Text="搜尋" />
        <asp:Button ID="btnAdd" runat="server" OnClick="btnAdd_Click" Text="新增推播" />
        <br />
        <asp:GridView ID="gvPushAppList" runat="server" OnRowDataBound="gvPushAppList_RowDataBound"
            OnRowCommand="gvPushAppList_OnRowCommand" GridLines="Horizontal" ForeColor="Black"
            CellPadding="4" BorderWidth="1px" BorderStyle="None" BorderColor="#DEDFDE" BackColor="White"
            EmptyDataText="無符合的資料" AutoGenerateColumns="false" Font-Size="Small" EnableViewState="true"
            Width="1600px">
            <FooterStyle BackColor="#CCCC99"></FooterStyle>
            <RowStyle BackColor="#F7F7DE" Height="20px" HorizontalAlign="Center"></RowStyle>
            <SelectedRowStyle BackColor="#CE5D5A" ForeColor="White" Font-Bold="True"></SelectedRowStyle>
            <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Center"></PagerStyle>
            <HeaderStyle BackColor="#6B696B" ForeColor="White" Font-Bold="True" Height="20px"
                HorizontalAlign="Center"></HeaderStyle>
            <Columns>
                <asp:BoundField DataField="Id" HeaderText="Id">
                    <ItemStyle HorizontalAlign="Center" Width="60" />
                </asp:BoundField>
                <asp:BoundField DataField="PushDate" DataFormatString="{0:yyyy-MM-dd HH:mm}" HtmlEncode="False"
                    HeaderText="推播時間">
                    <ItemStyle Width="200" HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:TemplateField HeaderText="訂閱地區" ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                        <asp:Label ID="lblPushArea" runat="server"></asp:Label></ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" Width="200" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="推播類型" ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                        <asp:Label ID="lblPushType" runat="server"></asp:Label></ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" Width="200" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="推播項目" ItemStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <asp:Label ID="lblPushItem" runat="server"></asp:Label></ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" Width="300" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="推播內文">
                    <ItemTemplate>
                        <asp:LinkButton ID="lkEdit" runat="server" CommandName="UPD" CommandArgument="<%# ((PushApp)Container.DataItem).Id %>"></asp:LinkButton>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left" Width="20%" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="執行時間">
                    <ItemTemplate>
                        <asp:Label ID="lblRealPushTime" runat="server"></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" Width="140" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="訂閱數<br />(點擊率)">
                    <ItemTemplate>
                        <asp:Label ID="lblSubscribeCount" runat="server"></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" Width="100" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="iOS推播<br />(點擊率)">
                    <ItemTemplate>
                        <asp:Label ID="lblIosPush" runat="server"></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" Width="100" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Andriod推播<br />(點擊率)">
                    <ItemTemplate>
                        <asp:Label ID="lblAndroidPush" runat="server"></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" Width="100" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="訂單數">
                    <ItemTemplate>
                        <asp:Label ID="lblOrderCount" runat="server"></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Right" Width="100" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="營業額">
                    <ItemTemplate>
                        <asp:Label ID="lblTurnover" runat="server"></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Right" Width="100" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="轉換率">
                    <ItemTemplate>
                        <asp:Label ID="lblConversionRate" runat="server"></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Right" Width="100" />
                </asp:TemplateField>
                <asp:TemplateField>
                    <ItemTemplate >
                        <asp:Button ID="btnPush" runat="server" Width="60px" Text="推播" CommandName="PUSH"  
                            CommandArgument="<%# ((PushApp)Container.DataItem).Id %>" OnClientClick="return confirm('確定要推播??');" />
                        <asp:PlaceHolder ID="phPushTest" runat="server">
                            <br />
                        <input type="button" value="試推" style="margin-top:8px; width:60px"
                            ref-id="<%# ((PushApp)Container.DataItem).Id %>" class="pushAction" />    
                        </asp:PlaceHolder>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" Width="3%" />
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
        <uc1:Pager ID="ucPager" runat="server" PageSize="10" ongetcount="GetCount" onupdate="UpdateHandler">
        </uc1:Pager>
    </asp:Panel>
    <asp:Panel ID="divPushAppAdd" runat="server" Visible="false">
        <h1>
            新增App推播</h1>
        <ul style="list-style-type: decimal; line-height: 30px;">
            <li>設定發送時間：<asp:TextBox ID="txtSendDate" runat="server" Columns="8" />
                <cc1:CalendarExtender ID="ceSendDate" TargetControlID="txtSendDate" runat="server"
                    Format="yyyy/MM/dd">
                </cc1:CalendarExtender>
                <asp:DropDownList ID="ddlSendTimeHour" runat="server">
                </asp:DropDownList>
                <asp:DropDownList ID="ddlSendTimeMinute" runat="server">
                    <asp:ListItem Text="00" Value="0"></asp:ListItem>
                    <asp:ListItem Text="15" Value="15"></asp:ListItem>
                    <asp:ListItem Text="30" Value="30"></asp:ListItem>
                    <asp:ListItem Text="45" Value="45"></asp:ListItem>
                </asp:DropDownList>
                <asp:HiddenField ID="hidPushId" runat="server" />
            </li>
            <li>選擇推播訂閱地區：
                <asp:CheckBoxList ID="chklPushArea" runat="server" RepeatLayout="Flow" RepeatDirection="Horizontal">
                </asp:CheckBoxList>
            </li>
            <li>選擇推播App連結：<br />
                類型：<asp:RadioButtonList ID="rdlPushAppType" runat="server" RepeatLayout="Flow" RepeatDirection="Horizontal"
                    onclick="PushTypeChange(this);">
                </asp:RadioButtonList>
                <br />
                <ul style="list-style-type: none;">
                    <li class="rdPushApp PushBid">
                        <asp:RadioButton ID="rdPushAppBid" runat="server" Text="開啟單一團購檔次" GroupName="PushAppItem"
                            Checked="true" onclick="PushItemControl();" />
                        <span>，請輸入bid：<asp:TextBox ID="txtBid" runat="server" Width="200px"></asp:TextBox></span>
                    </li>
                    <li class="rdPushApp PponCity">
                        <asp:RadioButton ID="rdPushAppPponCity" runat="server" Text="開啟單一團購頻道" GroupName="PushAppItem"
                            onclick="PushItemControl();" />
                        <span>，請選擇頻道：<asp:RadioButtonList ID="rdlPponCityGroup" runat="server" RepeatLayout="Flow"
                            RepeatDirection="Horizontal">
                        </asp:RadioButtonList>
                        </span></li>
                    <li class="rdPushApp VourcherId">
                        <asp:RadioButton ID="rdPushAppVourcherId" runat="server" Text="開啟單一優惠" GroupName="PushAppItem"
                            onclick="PushItemControl();" />
                        <span>，請輸入優惠券編號：<asp:TextBox ID="txtVourcherId" runat="server" Width="200px"></asp:TextBox></span>
                    </li>
                    <li class ="rdPushApp SellerId">
                        <asp:RadioButton ID="rdPushAppSellerId" runat="server" Text="開啟店家所有優惠" GroupName="PushAppItem"
                            onclick="PushItemControl();" />
                        <span>，請輸入優惠券店家編號：<asp:TextBox ID="txtSellerId" runat="server" Width="200px"></asp:TextBox></span>
                    </li>
                    <li class ="rdPushApp EventPromo">
                        <asp:RadioButton ID="rdPushAppEventPromo" runat="server" Text="開啟商品主題活動頁/策展1.0" GroupName="PushAppItem"
                            onclick="PushItemControl();" />
                        <span>，請輸入商品主題活動：<asp:TextBox ID="txtEventPromoId" runat="server" Width="200px"></asp:TextBox></span>
                    </li>                    
                    <li class="rdPushApp CustomUrl">
                        <asp:RadioButton ID="rdPushAppCustomUrl" runat="server" Text="開啟指定連結" GroupName="PushAppItem"
                            onclick="PushItemControl();" />
                        <span>，請輸入網址：<asp:TextBox ID="txtCustomUrl" runat="server" Width="400px"></asp:TextBox></span>                       
                    </li>
                    <li class="rdPushApp BrandPromo">
                        <asp:RadioButton ID="rdPushAppBrandPromo" runat="server" Text="開啟策展2.0" GroupName="PushAppItem"
                            onclick="PushItemControl();" />
                        <span>，請輸入策展2.0活動編號：<asp:TextBox ID="txtBrandPromoId" runat="server" Width="200px"></asp:TextBox></span>
                    </li>
                </ul>
            </li>
            <li>編輯推播文字&nbsp;&nbsp;<span class="ui-state-highlight" style="font-size: small">限定80個字元，40個中文字</span><br />
                <asp:TextBox ID="txtDescription" runat="server" TextMode="MultiLine" Width="400px"
                    Height="60px" onKeyDown="LimitTextArea(this)" onKeyUp="LimitTextArea(this)" onkeypress="LimitTextArea(this)"></asp:TextBox>
            </li>
        </ul>
        <asp:Button ID="btnSave" runat="server" OnClick="btnSave_Click" Text="儲存" OnClientClick="return SaveCheck();" />
        <asp:Button ID="btnDelete" runat="server" OnClick="btnDelete_Click" Text="刪除" OnClientClick="return confirm('確認刪除此筆推播?');" />
        <asp:Button ID="btnUpdate" runat="server" OnClick="btnUpdate_Click" Text="更新" OnClientClick="return SaveCheck();" Visible="False" />
        <asp:Button ID="btnBack" runat="server" OnClick="btnBack_Click" Text="返回列表" />
    </asp:Panel>
    <div id="popupPush2" style="display:none">
        <div class="pushDialog">
            <input type="hidden" id="dPushId" />
            <div>
                <span style="display:inline-block" class="title"></span>
                <span style="display:inline-block; margin-left:24px;"><a href="javascript:void(0)" class="linkTab" tabId="tab-1">試推</a></span>
                <span style="display:inline-block; margin-left:24px;"><a href="javascript:void(0)" class="linkTab" tabId="tab-2">查詢</a></span>
            </div>
            <div id="tab-1" class="tab tab-actived">
                <div class="body">
                    <textarea id="ttTokens" cols="200" rows="4" style="width: 90%; height: 160px; overflow-x: hidden; white-space: nowrap;"
                        placeholder="填入要推播的裝置Token"></textarea>
                </div>
                <div class="bottom">
                    <span style="float:left"></span>
                    <input type="button" id="btnPush" value="我要試推" /><span id="spPushDesc" style="margin-left: 20px"></span>
                </div>
            </div>
            <div id="tab-2" class="tab">
                <div class="body">
                    <input type="text" id="txtQueryAccount" placeholder="輸入會員帳號，查詢手機裝置" style="width: 60%" />
                    <input type="button" value="查詢" id="btnQueryDevices" style="margin-left:12px" />
                    <input type="button" value="我的" id="btnQueryMyDevices" style="margin-left:24px" />
                    <table style="width:90%;" class="table" id="tbDevices">
                        <thead>
                        <tr>
                            <td style="border-bottom:1px solid silver; text-align:left"></td>
                            <td style="border-bottom:1px solid silver; text-align:left">代號</td>
                            <td style="border-bottom:1px solid silver; text-align:left">手機</td>
                            <td style="border-bottom:1px solid silver; text-align:left">OS版號</td>
                            <td style="border-bottom:1px solid silver; text-align:left">APP版本</td>
                        </tr>
                        </thead>
                        <tbody>
<%--                            <tr>
                                <td><input type="checkbox" /></td>
                                <td>##########</td>
                                <td>####</td>
                                <td>####</td>
                                <td>####</td>
                            </tr>--%>
                            <tr class="message">
                                <td colspan="5">沒有符合的資料
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="bottom">
                    <input type="button" id="btnAddToken" value="加入" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>

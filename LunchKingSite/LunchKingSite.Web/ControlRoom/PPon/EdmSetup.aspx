﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ControlRoom/backend.master" AutoEventWireup="true"
    CodeBehind="EdmSetup.aspx.cs" Inherits="LunchKingSite.Web.ControlRoom.Ppon.EdmSetup" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="../Controls/EdmDetailsSetup.ascx" TagName="EdmDetailsSetup" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <script type="text/javascript">
        function isupload() {
            if ('<%=UploadDate %>' == '') {
                return confirm('請確定是否要上傳正式發送?');
            }
            else {
                return confirm('此EDM已於' + '<%=UploadDate %>' + "上傳，請確定是否要重複上傳發送?");
            }
        }
    </script>
    <style type="text/css">
        .bgOrange {
            background-color: #FFC690;
            text-align: center;
        }

        .bgBlue {
            background-color: #B3D5F0;
        }

        .bgLBlue {
            background-color: #B2DFEE;
        }

        .bgLOrange {
            background-color: #FFEC8B;
            font-size: smaller;
        }

        .title {
            text-align: center;
            background-color: #688E45;
            font-weight: bolder;
            color: White;
        }

        .title1 {
            text-align: center;
            background-color: #8B8970;
            font-weight: bolder;
            color: White;
        }

        .title2 {
            text-align: center;
            background-color: #778899;
            font-weight: bolder;
            color: White;
        }

        .header {
            text-align: center;
            font-size: large;
            font-weight: bolder;
            background-color: #F0AF13;
            color: Blue;
        }

        .fieldset {
            border: #688E45 solid 2px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:HiddenField ID="hif_Id" runat="server" />
    <fieldset class="fieldset">
        <legend class="title">訂閱信編輯</legend>
        <table style="background-color: gray; width: 950px">
            <tr>
                <td class="bgOrange" style="width: 15%">地區選擇
                </td>
                <td class="bgBlue" style="width: 35%">
                    <asp:DropDownList ID="ddl_Main_City" runat="server" Width="150">
                    </asp:DropDownList>
                </td>
                <td class="bgOrange" style="width: 15%">發送日期
                </td>
                <td class="bgBlue" style="width: 35%">
                    <asp:TextBox ID="tbx_Main_DeliveryDate" runat="server"></asp:TextBox>
                    <cc1:CalendarExtender ID="ce_DeliveryDate" TargetControlID="tbx_Main_DeliveryDate"
                        runat="server">
                    </cc1:CalendarExtender>
                    <asp:RequiredFieldValidator ID="rfv_DeliveryDate" runat="server" ErrorMessage="*"
                        ValidationGroup="A" ControlToValidate="tbx_Main_DeliveryDate" Display="Dynamic"
                        ForeColor="Red"></asp:RequiredFieldValidator>
                    <asp:Label ID="lblDeliveryTime" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="bgOrange">測試發送Email:<br />
                    逗號或斷行區隔
                </td>
                <td class="bgBlue" colspan="3">
                    <asp:TextBox ID="tbx_TestEmail" runat="server" TextMode="MultiLine" Rows="2" Width="700"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td colspan="4" class="title2">
                    <asp:Button ID="btn_SetDefaultEdm" runat="server" Text="確定" OnClick="GetEdmContent"
                        ValidationGroup="A" />
                    <asp:Button ID="btn_TestGenerateDailyEdm" runat="server" Text="測試產生今日各區EDM" OnClick="TestGenerateDailyEdm" />
                </td>
            </tr>
        </table>
    </fieldset>
    <asp:Panel ID="pan_Details" runat="server" Visible="false">
        <br />
        <div class="title">
            <asp:Label ID="lab_ID" runat="server"></asp:Label>&nbsp;&nbsp;&nbsp;
            <asp:HyperLink ID="hyp_ID" runat="server" Target="_blank">預覽</asp:HyperLink>
            <br />
            <asp:Button ID="btn_Save2" runat="server" Text="儲存" OnClick="SaveEdm" ValidationGroup="B" />&nbsp;&nbsp;
            <asp:Button ID="btn_Cancel2" runat="server" Text="取消" ForeColor="Blue" OnClick="CancelAction" />&nbsp;&nbsp;
            <asp:Button ID="btn_Delete2" runat="server" Text="刪除" ForeColor="Red" OnClientClick="return(confirm('確定刪除??'));"
                OnClick="DeleteEdmSetting" Visible="false" />&nbsp;&nbsp;
            <asp:Button ID="btn_TestSend2" runat="server" Text="測試發送本區域EDM" OnClick="TestSendEdm"
                Visible="false" />
            &nbsp;&nbsp;
            <asp:Button ID="btn_Send2" runat="server" Text="發送本區域EDM" OnClick="SendEdm" Visible="false"
                OnClientClick="return(isupload());" ForeColor="Blue" />
        </div>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <script type="text/javascript">
                    function chineseCount(word, d) {
                        v = 0
                        for (cc = 0; cc < word.length; cc++) {
                            c = word.charCodeAt(cc);
                            if (!(c >= 32 && c <= 126)) v++;
                        }
                        total = Math.ceil((word.length - v) / 2);
                        $(d).text(v + total);
                    }
                    Sys.Application.add_load(WireEvent);
                    function WireEvent() {
                        $('#<%=tbx_Subject.ClientID %>').keyup(function (event) {
                            chineseCount($(this).val(), $('#div<%=tbx_Subject.ClientID %>'));
                        });
                    }
                </script>
                <fieldset class="fieldset" style="margin-top: 20px">
                    <legend class="title">Subject</legend>
                    <table style="background-color: gray; width: 950px">
                        <tr>
                            <td class="bgLOrange" style="width: 100px">郵件主旨 :<br />
                                頁面字數限制:45
                            </td>
                            <td class="bgLBlue" colspan="3">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="tbx_Subject" runat="server" TextMode="MultiLine" Rows="2" Width="750"></asp:TextBox>
                                        </td>
                                        <td>
                                            <div id='<%="div"+tbx_Subject.ClientID %>'>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                                <asp:RequiredFieldValidator ID="rfv_Subject" runat="server" ErrorMessage="*" ValidationGroup="B"
                                    ControlToValidate="tbx_Subject" Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="bgLOrange" style="width: 100px">CPA
                            </td>
                            <td class="bgLBlue">
                                <asp:TextBox ID="tbx_Cpa" runat="server"></asp:TextBox>
                            </td>
                            <td class="bgLOrange" style="width: 100px">暫停
                            </td>
                            <td class="bgLBlue">
                                <asp:CheckBox ID="cbx_Pause" runat="server" />
                            </td>
                        </tr>
                    </table>
                </fieldset>
                <fieldset class="fieldset" style="margin-top: 20px">
                    <legend class="title">AD</legend>
                    <table style="background-color: gray; width: 950px">
                        <tr>
                            <td class="bgLOrange" style="width: 100px">AD :
                            </td>
                            <td class="bgLBlue">
                                <asp:DropDownList ID="ddl_AD" runat="server" OnSelectedIndexChanged="ChangeAD" AutoPostBack="true"
                                    Width="150">
                                </asp:DropDownList>
                                <asp:DropDownList ID="ddl_AD_Content" runat="server" Visible="false">
                                </asp:DropDownList>
                                &nbsp;&nbsp;<asp:CheckBox ID="cbx_AD" runat="server" Text="是否加入AD" Checked="true" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" class="bgLBlue">
                                <asp:Literal ID="lit_AD" runat="server"></asp:Literal>
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </ContentTemplate>
        </asp:UpdatePanel>
        <uc1:EdmDetailsSetup ID="edmuc_main_1" runat="server" />
        <uc1:EdmDetailsSetup ID="edmuc_main_2" runat="server" />
        <uc1:EdmDetailsSetup ID="edmuc_Area_1" runat="server" />
        <uc1:EdmDetailsSetup ID="edmuc_Area_2" runat="server" />
        <uc1:EdmDetailsSetup ID="edmuc_Area_3" runat="server" />
        <uc1:EdmDetailsSetup ID="edmuc_PponPiinLife_1" runat="server"/>
        <fieldset class="fieldset" style="margin-top: 20px;display: none" >
            <legend class="title">PEZ</legend>
            <table style="background-color: gray; width: 950px">
                <tr>
                    <td class="bgOrange" style="width: 15%">CPA
                    </td>
                    <td class="bgBlue">
                        <asp:CheckBox ID="cbx_PEZ" runat="server" Text="是否加入PEZ每日一物" Checked="false" />
                        <asp:RadioButtonList ID="rbl_Pez_Cpa" runat="server" RepeatDirection="Horizontal"
                            Visible="false">
                            <asp:ListItem Text="Ezmail" Value="Ezmail" Selected="True"></asp:ListItem>
                            <asp:ListItem Text="Cellopoint" Value="Cellopoint"></asp:ListItem>
                            <asp:ListItem Text="abc" Value="Ezmail"></asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr>
                    <td class="bgLBlue" colspan="2">
                        <asp:Literal ID="lit_PEZ" runat="server"></asp:Literal>
                    </td>
                </tr>
            </table>
        </fieldset>
        <div class="title">
            <asp:Button ID="btn_Save" runat="server" Text="儲存" OnClick="SaveEdm" ValidationGroup="B" />&nbsp;&nbsp;
            <asp:Button ID="btn_Cancel" runat="server" Text="取消" ForeColor="Blue" OnClick="CancelAction" />&nbsp;&nbsp;
            <asp:Button ID="btn_Delete" runat="server" Text="刪除" ForeColor="Red" OnClientClick="return(confirm('確定刪除??'));"
                OnClick="DeleteEdmSetting" Visible="false" />&nbsp;&nbsp;
            <asp:CheckBox ID="cbx_Compress" runat="server" Text="是否壓縮" Checked="true" Visible="false" /><asp:Button
                ID="btn_TestSend" runat="server" Text="測試發送本區域EDM" OnClick="TestSendEdm" Visible="false" />
            &nbsp;&nbsp;
            <asp:Button ID="btn_Send" runat="server" Text="發送本區域EDM" OnClick="SendEdm" Visible="false"
                OnClientClick="return(isupload());" ForeColor="Blue" />
        </div>
    </asp:Panel>
</asp:Content>

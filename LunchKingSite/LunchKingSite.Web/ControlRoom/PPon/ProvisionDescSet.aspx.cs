﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.Core;
using LunchKingSite.Core.UI;
using System.Web.Security;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;
using LunchKingSite.DataOrm;
using LunchKingSite.BizLogic.Model.Sales;
using LunchKingSite.Core.Component;
using LunchKingSite.BizLogic.Component;
using System.Reflection;
using LunchKingSite.BizLogic.Model;
using MongoDB.Bson;

namespace LunchKingSite.Web.ControlRoom.Ppon
{
    public partial class ProvisionDescSet : RolePage, IProvisionDescSetView
    {
        #region props
        private ProvisionDescSetPresenter _presenter;
        public ProvisionDescSetPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        public string UserName
        {
            get
            {
                return Page.User.Identity.Name; 
            }
        }

        public ProvisionType Type
        {
            get
            {
                ProvisionType type = ProvisionType.None;
                ProvisionType.TryParse(rdlType.SelectedValue, out type);
                return type;
            }
        }

        public ObjectId Id
        {
            get
            {
                ObjectId id;
                ObjectId.TryParse(hidObjectId.Value, out id);
                return id;
            }
        }

        public ObjectId DepartmentId
        {
            get
            {
                ObjectId id;
                ObjectId.TryParse(hidDepartmentId.Value, out id);
                if (!id.Equals(ObjectId.Empty))
                    return id;
                else
                {
                    ObjectId.TryParse(ddlDepartment.SelectedValue, out id);
                    DepartmentId = id;
                    return id;
                }
            }
            set
            {
                hidDepartmentId.Value = value.ToString();
            }
        }

        public ObjectId CategoryId
        {
            get
            {
                ObjectId id;
                ObjectId.TryParse(hidCategoryId.Value, out id);
                if (!id.Equals(ObjectId.Empty))
                    return id;
                else
                {
                    ObjectId.TryParse(ddlCategory.SelectedValue, out id);
                    CategoryId = id;
                    return id;
                }
            }
            set
            {
                hidCategoryId.Value = value.ToString();
            }
        }

        public ObjectId ItemId
        {
            get
            {
                ObjectId id;
                ObjectId.TryParse(hidItemId.Value, out id);
                if (!id.Equals(ObjectId.Empty))
                    return id;
                else
                {
                    ObjectId.TryParse(ddlItem.SelectedValue, out id);
                    ItemId = id;
                    return id;
                }
            }
            set
            {
                hidItemId.Value = value.ToString();
            }
        }

        public ObjectId ContentId
        {
            get
            {
                ObjectId id;
                ObjectId.TryParse(hidContentId.Value, out id);
                if (!id.Equals(ObjectId.Empty))
                    return id;
                else
                {
                    ObjectId.TryParse(ddlContent.SelectedValue, out id);
                    ContentId = id;
                    return id;
                }
            }
            set
            {
                hidContentId.Value = value.ToString();
            }
        }

        public ObjectId DescId
        {
            get
            {
                ObjectId id;
                ObjectId.TryParse(hidDescId.Value, out id);
                return id;
            }
            set
            {
                hidDescId.Value = value.ToString();
            }
        }

        public string EditText
        {
            get
            {
                return txtEdit.Text.Trim();
            }
        }

        public ProvisionDescStatus Status
        {
            get
            {
                ProvisionDescStatus status;
                ProvisionDescStatus.TryParse(rdlStatus.SelectedValue, out status);
                return status;
            }
        }
        #endregion

        #region event
        public event EventHandler<EventArgs> ProvisionSelectedIndexChanged;
        public event EventHandler<EventArgs> SaveClicked;
        public event EventHandler<EventArgs> SortClicked;
        public event EventHandler<EventArgs> MongoSyncSqlClicked;
        public event EventHandler<DataEventArgs<Dictionary<ObjectId, int>>> SaveSortClicked;
        public event EventHandler<EventArgs> DeleteClicked;
        #endregion

        #region method
        public void SetProvisionDropDownListData(IEnumerable<ProvisionDepartment> departments)
        {
            ddlDepartment.Items.Clear();
            ddlCategory.Items.Clear();
            ddlItem.Items.Clear();
            ddlContent.Items.Clear();
            lblDesc.Text = string.Empty;

            if (departments.Count() > 0)
            {
                ddlDepartment.DataSource = departments;
                ddlDepartment.DataBind();

                ProvisionDepartment department = departments.Where(x => x.Id.Equals(DepartmentId)).FirstOrDefault();
                if (department != null)
                {
                    if (!DepartmentId.Equals(ObjectId.Empty))
                        ddlDepartment.SelectedValue = DepartmentId.ToString();

                    ddlCategory.DataSource = department.Categorys.OrderBy(x => x.Rank);
                    ddlCategory.DataBind();

                    ProvisionCategory category = department.Categorys.Where(x => x.Id.Equals(CategoryId)).FirstOrDefault();
                    if (category != null && !category.Id.Equals(ObjectId.Empty))
                    {
                        if (!CategoryId.Equals(ObjectId.Empty))
                            ddlCategory.SelectedValue = CategoryId.ToString();

                        ddlItem.DataSource = category.Items.OrderBy(x => x.Rank);
                        ddlItem.DataBind();

                        ProvisionItem item = category.Items.Where(x => x.Id.Equals(ItemId)).FirstOrDefault();
                        if (item != null && !item.Id.Equals(ObjectId.Empty))
                        {
                            if (!ItemId.Equals(ObjectId.Empty))
                                ddlItem.SelectedValue = ItemId.ToString();

                            ddlContent.DataSource = item.Contents.OrderBy(x => x.Rank);
                            ddlContent.DataBind();

                            ProvisionContent content = item.Contents.Where(x => x.Id.Equals(ContentId)).FirstOrDefault();
                            if (content != null && !content.Id.Equals(ObjectId.Empty))
                            {
                                if (!ContentId.Equals(ObjectId.Empty))
                                    ddlContent.SelectedValue = ContentId.ToString();

                                ProvisionDesc desc = content.ProvisionDescs.FirstOrDefault();
                                if (desc != null && !desc.Id.Equals(ObjectId.Empty))
                                {
                                    lblDesc.Text = desc.Description;
                                    DescId = desc.Id;
                                    hidStatus.Value = desc.Status.ToString();
                                }
                            }
                        }
                    }
                }
            }
        }

        public void SetMsProvisionDropDownListData(IEnumerable<ProvisionDepartmentModel> departments)
        {
            ddlDepartment.Items.Clear();
            ddlCategory.Items.Clear();
            ddlItem.Items.Clear();
            ddlContent.Items.Clear();
            lblDesc.Text = string.Empty;

            if (departments.Count() > 0)
            {
                ddlDepartment.DataSource = departments;
                ddlDepartment.DataBind();

                ProvisionDepartmentModel department = departments.Where(x => x.Id.Equals(DepartmentId.ToString())).FirstOrDefault();
                if (department != null)
                {
                    if (!DepartmentId.Equals(ObjectId.Empty))
                        ddlDepartment.SelectedValue = DepartmentId.ToString();

                    ddlCategory.DataSource = department.Categorys.OrderBy(x => x.Rank);
                    ddlCategory.DataBind();

                    ProvisionCategoryModel category = department.Categorys.Where(x => x.Id.Equals(CategoryId.ToString())).FirstOrDefault();
                    if (category != null && !category.Id.Equals(ObjectId.Empty))
                    {
                        if (!CategoryId.Equals(ObjectId.Empty))
                            ddlCategory.SelectedValue = CategoryId.ToString();

                        ddlItem.DataSource = category.Items.OrderBy(x => x.Rank);
                        ddlItem.DataBind();

                        ProvisionItemModel item = category.Items.Where(x => x.Id.Equals(ItemId.ToString())).FirstOrDefault();
                        if (item != null && !item.Id.Equals(ObjectId.Empty.ToString()))
                        {
                            if (!ItemId.Equals(ObjectId.Empty.ToString()))
                                ddlItem.SelectedValue = ItemId.ToString();

                            ddlContent.DataSource = item.Contents.OrderBy(x => x.Rank);
                            ddlContent.DataBind();

                            ProvisionContentModel content = item.Contents.Where(x => x.Id.Equals(ContentId.ToString())).FirstOrDefault();
                            if (content != null && !content.Id.Equals(ObjectId.Empty.ToString()))
                            {
                                if (!ContentId.Equals(ObjectId.Empty.ToString()))
                                    ddlContent.SelectedValue = ContentId.ToString();

                                ProvisionDescModel desc = content.ProvisionDescs.FirstOrDefault();
                                if (desc != null && !desc.Id.Equals(ObjectId.Empty.ToString()))
                                {
                                    lblDesc.Text = desc.Description;
                                    DescId = ObjectId.Parse(desc.Id);
                                    hidStatus.Value = desc.Status.ToString();
                                }
                            }
                        }
                    }
                }
            }
        }


        public void SetProvisionSortRank(Dictionary<ObjectId, string[]> dataList)
        {
            int value;
            repSort.DataSource = dataList.OrderBy(x => int.TryParse(x.Value[0], out value) ? value : 0);
            repSort.DataBind();
        }
        #endregion

        #region page
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Presenter.OnViewInitialized();
                hidDepartmentId.Value = DepartmentId.ToString();
                hidCategoryId.Value = CategoryId.ToString();
                hidItemId.Value = ItemId.ToString();
                hidContentId.Value = ContentId.ToString();
                hidDescId.Value = DescId.ToString();
                SetEditButtonVisibleControl();
            }
            _presenter.OnViewLoaded();
        }

        protected void ddlDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            ObjectId departmentId;
            ObjectId.TryParse(ddlDepartment.SelectedValue, out departmentId);
            DepartmentId = departmentId;
            CategoryId = ItemId = ContentId = DescId = ObjectId.Empty;

            if (this.ProvisionSelectedIndexChanged != null)
                this.ProvisionSelectedIndexChanged(this, e);
        }

        protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            ObjectId categoryId;
            ObjectId.TryParse(ddlCategory.SelectedValue, out categoryId);
            CategoryId = categoryId;
            ItemId = ContentId = DescId = ObjectId.Empty;

            if (this.ProvisionSelectedIndexChanged != null)
                this.ProvisionSelectedIndexChanged(this, e);
        }

        protected void ddlItem_SelectedIndexChanged(object sender, EventArgs e)
        {
            ObjectId itemId;
            ObjectId.TryParse(ddlItem.SelectedValue, out itemId);
            ItemId = itemId;
            ContentId = DescId = ObjectId.Empty;

            if (this.ProvisionSelectedIndexChanged != null)
                this.ProvisionSelectedIndexChanged(this, e);
        }

        protected void ddlContent_SelectedIndexChanged(object sender, EventArgs e)
        {
            ObjectId contentId;
            ObjectId.TryParse(ddlContent.SelectedValue, out contentId);
            ContentId = contentId;
            DescId = ObjectId.Empty;

            if (this.ProvisionSelectedIndexChanged != null)
                this.ProvisionSelectedIndexChanged(this, e);
        }

        protected void rdlType_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetEditButtonVisibleControl();
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            divEdit.Visible = true;
            hidObjectId.Value = ObjectId.GenerateNewId().ToString();
            ddlDepartment.Enabled = ddlCategory.Enabled = ddlItem.Enabled = ddlContent.Enabled = rdlType.Enabled = false;
            rdlStatus.Visible = Type.Equals(ProvisionType.Description);
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            divEdit.Visible = true;
            ddlDepartment.Enabled = ddlCategory.Enabled = ddlItem.Enabled = ddlContent.Enabled = rdlType.Enabled = false;
            rdlStatus.Visible = Type.Equals(ProvisionType.Description);
            switch (Type)
            {
                case ProvisionType.Department:
                    hidObjectId.Value = ddlDepartment.SelectedValue;
                    txtEdit.Text = ddlDepartment.SelectedItem.Text;
                    break;
                case ProvisionType.Category:
                    hidObjectId.Value = ddlCategory.SelectedValue;
                    txtEdit.Text = ddlCategory.SelectedItem.Text;
                    break;
                case ProvisionType.Item:
                    hidObjectId.Value = ddlItem.SelectedValue;
                    txtEdit.Text = ddlItem.SelectedItem.Text;
                    break;
                case ProvisionType.Content:
                    hidObjectId.Value = ddlContent.SelectedValue;
                    txtEdit.Text = ddlContent.SelectedItem.Text;
                    break;
                case ProvisionType.Description:
                    hidObjectId.Value = hidDescId.Value;
                    txtEdit.Text = lblDesc.Text;
                    ProvisionDescStatus status = ProvisionDescStatus.Default;
                    ProvisionDescStatus.TryParse(hidStatus.Value, out status);
                    rdlStatus.SelectedValue = status.ToString();
                    break;
                case ProvisionType.None:
                default:
                    break;
            }
        }

        protected void btnSort_Click(object sender, EventArgs e)
        {
            if (this.SortClicked != null)
            {
                ddlDepartment.Enabled = ddlCategory.Enabled = ddlItem.Enabled = ddlContent.Enabled = rdlType.Enabled = false;
                this.SortClicked(this, e);
                divSort.Visible = true;
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (this.SaveClicked != null)
            {
                this.SaveClicked(this, e);

                divEdit.Visible = false;
                txtEdit.Text = string.Empty;
                rdlStatus.SelectedIndex = 0;
                ddlDepartment.Enabled = ddlCategory.Enabled = ddlItem.Enabled = ddlContent.Enabled = rdlType.Enabled = true;
            }
        }

        protected void btnSaveSort_Click(object sender, EventArgs e)
        {
            if (this.SaveClicked != null)
            {
                this.SaveSortClicked(this, new DataEventArgs<Dictionary<ObjectId, int>>(GetProvisionRank()));

                divSort.Visible = false;
                ddlDepartment.Enabled = ddlCategory.Enabled = ddlItem.Enabled = ddlContent.Enabled = rdlType.Enabled = true;
            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            if (this.DeleteClicked != null)
            {
                this.DeleteClicked(this, e);

                ddlDepartment.Enabled = ddlCategory.Enabled = ddlItem.Enabled = ddlContent.Enabled = rdlType.Enabled = true;
                divEdit.Visible = false;
                txtEdit.Text = string.Empty;
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            if (this.SaveClicked != null)
            {
                ddlDepartment.Enabled = ddlCategory.Enabled = ddlItem.Enabled = ddlContent.Enabled = rdlType.Enabled = true;
                divEdit.Visible = false;
                txtEdit.Text = string.Empty;
                divSort.Visible = false;
                rdlStatus.SelectedIndex = 0;
            }
        }

        protected void btnMongoSyncSql_Click(object sender, EventArgs e)
        {
            if (this.MongoSyncSqlClicked != null)
            {
                this.MongoSyncSqlClicked(this, e);
            }
        }

        protected void repSort_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem is KeyValuePair<ObjectId, string[]>)
            {
                KeyValuePair<ObjectId, string[]> item = (KeyValuePair<ObjectId, string[]>)e.Item.DataItem;
                HiddenField hidSortObjectId = ((HiddenField)e.Item.FindControl("hidSortObjectId"));
                Label lblDescription = ((Label)e.Item.FindControl("lblDescription"));
                TextBox txtRank = ((TextBox)e.Item.FindControl("txtRank"));

                hidSortObjectId.Value = item.Key.ToString();
                txtRank.Text = item.Value[0];
                lblDescription.Text = item.Value[1];
            }
        }
        #endregion

        #region private method
        private void SetEditButtonVisibleControl()
        {
            switch (Type)
            {
                case ProvisionType.Department:
                    btnEdit.Visible = !ddlDepartment.Items.Count.Equals(0);
                    btnSort.Visible = false;
                    // 鎖定館別異動
                    break;
                case ProvisionType.Category:
                    btnAdd.Visible = !ddlDepartment.Items.Count.Equals(0);
                    btnEdit.Visible = !ddlCategory.Items.Count.Equals(0);
                    btnSort.Visible = true;
                    break;
                case ProvisionType.Item:
                    btnAdd.Visible = !ddlCategory.Items.Count.Equals(0);
                    btnEdit.Visible = !ddlItem.Items.Count.Equals(0);
                    btnSort.Visible = true;
                    break;
                case ProvisionType.Content:
                    btnAdd.Visible = !ddlItem.Items.Count.Equals(0);
                    btnEdit.Visible = !ddlContent.Items.Count.Equals(0);
                    btnSort.Visible = true;
                    break;
                case ProvisionType.Description:
                    btnAdd.Visible = !ddlContent.Items.Count.Equals(0);
                    btnEdit.Visible = !string.IsNullOrEmpty(lblDesc.Text);
                    btnSort.Visible = false;
                    break;
                case ProvisionType.None:
                default:
                    btnEdit.Visible = false;
                    btnSort.Visible = false;
                    break;
            }
        }
        private Dictionary<ObjectId, int> GetProvisionRank()
        {
            Dictionary<ObjectId, int> list = new Dictionary<ObjectId, int>();
            for (int i = 0; i < repSort.Items.Count; i++)
            {
                HiddenField hidSortObjectId = (HiddenField)repSort.Items[i].FindControl("hidSortObjectId");
                TextBox txtRank = (TextBox)repSort.Items[i].FindControl("txtRank");
                ObjectId id;
                ObjectId.TryParse(hidSortObjectId.Value, out id);
                int rank;
                int.TryParse(txtRank.Text, out rank);
                if (!id.Equals(ObjectId.Empty))
                    list.Add(id, rank);
            }
            return list;
        }
        #endregion
    }
}
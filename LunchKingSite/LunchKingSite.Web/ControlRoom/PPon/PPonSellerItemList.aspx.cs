﻿using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;

namespace LunchKingSite.Web.ControlRoom.Ppon
{
    public partial class PponSellerItemList : RolePage
    {
        private IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        private IOrderProvider op = ProviderFactory.Instance().GetProvider<IOrderProvider>();

        private Guid bid
        {
            get
            {
                Guid busId;
                return Guid.TryParse(Request["bid"], out busId) ? busId : Guid.Empty;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (bid != Guid.Empty)
                {
                    SetGrid(bid);
                    SetShippedDate(bid);

                    DealProperty dp = pp.DealPropertyGet(bid);
                    if (dp.AncestorBusinessHourGuid.HasValue)
                    {
                        ViewPponDeal anc_deal = pp.ViewPponDealGetByBusinessHourGuid(dp.AncestorBusinessHourGuid.Value);
                        lit_Continued.Text = string.Format("(接續檔號：{0}，", anc_deal.UniqueId);
                        if (dp.IsContinuedSequence)
                        {
                            int oCount = OrderFacade.GetAncestorCouponCount(bid);
                            lit_Continued.Text += string.Format("接續憑證號碼，接續號碼：{0};", (oCount + 1));
                        }
                        if (dp.IsContinuedQuantity)
                        {
                            ViewPponDeal deal = pp.ViewPponDealGetByBusinessHourGuid(bid);
                            lit_Continued.Text += string.Format("接續銷售數量，接續數量：{0};", ((deal.ContinuedQuantity ?? 0) + 1));
                        }
                        lit_Continued.Text += ")";
                    }

                    //是否已核對對帳單
                    DealAccounting da = pp.DealAccountingGet(bid);
                    if (da.FinalBalanceSheetDate.HasValue)
                    {
                        btnFinalBalanceSheetDate.Text = FinalBalanceSheetDateGetString((DateTime)da.FinalBalanceSheetDate);
                        btnFinalBalanceSheetDate.Enabled = false;
                        btSaveTime.Enabled = false;
                        btDeleteTime.Enabled = false;
                    }
                }
                else
                {
                    ShowMessage("Bid錯誤");
                }
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            if (Request["bid"] != "")
                SetGrid(new Guid(Request["bid"]));
        }

        protected void btnFinalBalanceSheetDate_Click(object sender, EventArgs e)
        {
            if (bid == Guid.Empty) return;
            BusinessHour bh = pp.BusinessHourGet(bid);
            DealAccounting da = pp.DealAccountingGet(bid);
            
            //商品檔對帳單系統化機制 只限於2014-08-01後開檔的檔次 才開始運作
            if (DateTime.Compare(bh.BusinessHourOrderTimeS, new DateTime(2014, 8, 1)) >= 0 &&
                IsPartiallyPaymentDateNeeded(bid, (RemittanceType)da.RemittanceType) &&
                !da.PartiallyPaymentDate.HasValue)
            {
                ShowMessage("尚未設定暫付七成付款日，無法更新");
                return;
            }

            if (!da.FinalBalanceSheetDate.HasValue)
            {
                var now = DateTime.Now;
                var actionDesc = string.Format("UpdateFinalBalanceSheetDate : {0} -> {1}", da.FinalBalanceSheetDate, now);
                da.MarkOld();
                da.FinalBalanceSheetDate = now;
                pp.DealAccountingSet(da);
                CommonFacade.AddAudit(da.BusinessHourGuid, AuditType.DealAccounting, actionDesc, User.Identity.Name, true);
                btnFinalBalanceSheetDate.Text = FinalBalanceSheetDateGetString((DateTime)da.FinalBalanceSheetDate);
                btnFinalBalanceSheetDate.Enabled = false;
            }
        }

        private string FinalBalanceSheetDateGetString(DateTime dt)
        {
            return "已於 " + dt.ToString("yyyy-MM-dd HH:mm:ss") + " 核對對帳單";
        }

        protected void btnExportToExcel_Click(object sender, EventArgs e)
        {
            bool isShoppingCart = false;
            Guid bid = new Guid(Request["bid"]);
            DataTable dt = GetSellerItemList(bid);
            ViewPponDeal vp = ViewPponDealGet(bid);
            if (!(vp.ShoppingCart.HasValue && vp.ShoppingCart.Value))
            {
                dt.Columns.Remove("數量統計");
            }
            else
            {
                isShoppingCart = true;
            }
            string itemDetail = string.Format("※清冊名單共 {0} 份    檔號：{1}", dt.Compute("Sum(數量)", string.Empty).ToString(), vp.UniqueId);
            Dictionary<string, int> quantityDesc = dt.AsEnumerable().GroupBy(item => new { Option = item["選項"].ToString() }).Select(group => new { Option = group.Key.Option, QuantityCount = group.Sum(s => int.Parse(s["數量"].ToString())) }).ToDictionary(y => y.Option, y => y.QuantityCount);
            Export(vp.CouponUsage + (string.IsNullOrEmpty(Request["itemname"]) ? "商家名冊.xls" : "P完美.xls"), dt, vp.EventName, itemDetail, string.Join("\n", quantityDesc.Select(x => string.Format("{0}：{1}", x.Key, x.Value))), isShoppingCart);
        }

        protected void btSaveTime_Click(object sender, EventArgs e)
        {
            DateTime now = DateTime.Now;
            DateTime selectedDate;

            if (!DateTime.TryParse(tbShippedDate.Text, out selectedDate))
            {
                ShowMessage("請輸入出貨回覆日");
                return;
            }
            else
            {
                if (!(now.DayOfWeek == DayOfWeek.Monday && now.AddDays(-3).Date <= selectedDate) && now.AddDays(-1).Date > selectedDate)
                {
                    ShowMessage("選取的日期不可小於昨天; \\n 若今天是星期一則選取日期不可小於星期五");
                    return;
                }
                else
                {
                    #region 紀錄出貨回覆日

                    /* 檢查 未出貨數 = 0 方能壓記出貨回覆日
                       先前品生活有未回填出貨資訊 直接壓記出貨回覆日的狀況 
                       因目前出貨回覆日會影響商品對帳單產出 
                       故不允許未有出貨數仍壓記出貨回覆日的狀況發生
                       (多檔次母檔需加總子檔未出貨數) */
                    DealAccounting accounting = pp.DealAccountingGet(new Guid(Request["bid"]));
                    var actionDesc = string.Format("UpdateShippedDate : {0} -> {1}", accounting.ShippedDate, selectedDate);
                    accounting.MarkOld();
                    accounting.ShippedDate = selectedDate;
                    pp.DealAccountingSet(accounting);
                    CommonFacade.AddAudit(accounting.BusinessHourGuid, AuditType.DealAccounting, actionDesc, User.Identity.Name, true);
                    lbMessage.Text = string.Format("出貨回覆日已記錄為{0}", tbShippedDate.Text);

                    #endregion 紀錄出貨回覆日
                }
            }
        }

        protected void btDeleteTime_Click(object sender, EventArgs e)
        {
            #region 清除出貨回覆日

            DealAccounting accounting = pp.DealAccountingGet(new Guid(Request["bid"]));
            var actionDesc = string.Format("UpdateShippedDate : {0} -> {1}", accounting.ShippedDate, string.Empty);
            accounting.MarkOld();
            accounting.ShippedDate = null;
            pp.DealAccountingSet(accounting);
            CommonFacade.AddAudit(accounting.BusinessHourGuid, AuditType.DealAccounting, actionDesc, User.Identity.Name, true);
            lbMessage.Text = "已清除出貨回覆日";
            tbShippedDate.Text = string.Empty;

            #endregion 清除出貨回覆日
        }

        private void SetGrid(Guid bid)
        {
            ViewPponDeal vpd = ViewPponDealGet(bid);
            DataTable dt = GetSellerItemList(bid);
            if (!(vpd.ShoppingCart.HasValue && vpd.ShoppingCart.Value))
            {
                dt.Columns.Remove("數量統計");
            }

            GridView1.DataSource = dt;
            GridView1.DataBind();

            lblItemName.Text = vpd.ItemName;
            lblTotalCount.Text = dt.Compute("Sum(數量)", string.Empty).ToString();
            if (!string.IsNullOrEmpty(txtCreateTimeS.Text) && !string.IsNullOrEmpty(txtCreateTimeE.Text))
            {
                lblOrderTime.Text = string.Format("※ {0} 00:00 至 {1} 00:00 清冊名單：{2} 份", Convert.ToDateTime(txtCreateTimeS.Text).ToShortDateString(), Convert.ToDateTime(txtCreateTimeE.Text).ToShortDateString(), dt.Rows.Count.ToString());
            }
            else
            {
                lblOrderTime.Text = string.Empty;
            }
        }

        private void SetShippedDate(Guid bid)
        {
            DealAccounting accounting = pp.DealAccountingGet(new Guid(Request["bid"]));
            if (accounting != null && accounting.ShippedDate != null)
            {
                tbShippedDate.Text = accounting.ShippedDate.Value.ToString("MM/dd/yyyy");
            }
            else
            {
                tbShippedDate.Text = string.Empty;
            }
        }

        private ViewPponDeal ViewPponDealGet(Guid bid)
        {
            ViewPponDeal vp = pp.ViewPponDealGetByBusinessHourGuid(bid);
            return vp;
        }

        private DataTable GetSellerItemList(Guid bid)
        {
            if (string.IsNullOrEmpty(Request["itemname"]))
            {
                pp.UpdateGetSellerItemListExportLog(bid, txtCreateTimeS.Text, txtCreateTimeE.Text, User.Identity.Name);
                return pp.GetSellerItemList(bid, txtCreateTimeS.Text, txtCreateTimeE.Text);
            }
            else
            {
                return pp.GetPeautySellerItemList(bid, Request["itemname"]);
            }
        }

        private static void Export(string fileName, DataTable sdt, string title, string itemDetail, string quantityDesc, bool isShoppingCart)
        {
            if (sdt.Rows.Count > 0)
            {
                Workbook workbook = new HSSFWorkbook();

                // 新增試算表。
                Sheet sheet = workbook.CreateSheet("店家清冊名單");

                //to enable newlines you need set a cell styles with wrap=true
                CellStyle cellWrapStyle = workbook.CreateCellStyle();

                // set font bold
                CellStyle cellHeaderStyle = workbook.CreateCellStyle();
                Font cellHeaderFont = workbook.CreateFont();
                cellHeaderFont.Boldweight = (short)FontBoldWeight.BOLD;
                cellHeaderStyle.SetFont(cellHeaderFont);
                cellHeaderStyle.BorderBottom = cellHeaderStyle.BorderLeft = cellHeaderStyle.BorderRight = cellHeaderStyle.BorderTop = CellBorderType.THIN;
                cellHeaderStyle.BottomBorderColor = cellHeaderStyle.LeftBorderColor = cellHeaderStyle.RightBorderColor = cellHeaderStyle.TopBorderColor = IndexedColors.BLACK.Index;

                // Set content the border and border colors.
                CellStyle cellBorderStyle = workbook.CreateCellStyle();
                cellBorderStyle.BorderBottom = cellBorderStyle.BorderLeft = cellBorderStyle.BorderRight = cellBorderStyle.BorderTop = CellBorderType.THIN;
                cellBorderStyle.BottomBorderColor = cellBorderStyle.LeftBorderColor = cellBorderStyle.RightBorderColor = cellBorderStyle.TopBorderColor = IndexedColors.BLACK.Index;

                // add title
                Row titleRow = sheet.CreateRow(0);
                titleRow.CreateCell(0).SetCellValue(title);
                // Merging cells
                NPOI.SS.Util.CellRangeAddress titleColspan = new NPOI.SS.Util.CellRangeAddress(0, 0, 0, sdt.Columns.Count - 1);
                sheet.AddMergedRegion(titleColspan);

                // add itemDetail
                Row itemDetailRow = sheet.CreateRow(1);
                itemDetailRow.CreateCell(0).SetCellValue(itemDetail);
                // Merging cells
                NPOI.SS.Util.CellRangeAddress itemDetailColspan = new NPOI.SS.Util.CellRangeAddress(1, 1, 0, sdt.Columns.Count - 1);
                sheet.AddMergedRegion(itemDetailColspan);

                // add quantityDescription
                Row quantityDescriptionRow = sheet.CreateRow(2);
                Cell quantityDescription = quantityDescriptionRow.CreateCell(0);
                quantityDescription.SetCellValue(quantityDesc);
                // Merging cells
                NPOI.SS.Util.CellRangeAddress quantityDescColspan = new NPOI.SS.Util.CellRangeAddress(2, 2, 0, sdt.Columns.Count - 1);
                sheet.AddMergedRegion(quantityDescColspan);
                // Using newlines in cells
                quantityDescription.CellStyle = cellWrapStyle;

                // add Content Header
                Row contentRow = sheet.CreateRow(3);
                foreach (DataColumn column in sdt.Columns)
                {
                    Cell cell = contentRow.CreateCell(column.Ordinal);
                    cell.SetCellValue(column.ColumnName);
                    cell.CellStyle = cellHeaderStyle;
                }

                Cell c = null;
                int rowCount = 0;
                int rowIndex = 4;
                if (isShoppingCart)
                {
                    CellStyle cellColorStyle = workbook.CreateCellStyle();
                    cellColorStyle.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.GREY_25_PERCENT.index;
                    cellColorStyle.FillPattern = FillPatternType.SOLID_FOREGROUND;
                    cellColorStyle.BorderBottom = cellColorStyle.BorderLeft = cellColorStyle.BorderRight = cellColorStyle.BorderTop = CellBorderType.THIN;
                    cellColorStyle.BottomBorderColor = cellColorStyle.LeftBorderColor = cellColorStyle.RightBorderColor = cellColorStyle.TopBorderColor = IndexedColors.BLACK.Index;
                    int back_color = 0;
                    foreach (var item in sdt.AsEnumerable().GroupBy(x => x.Field<string>("訂單編號")))
                    {
                        foreach (var row in item)
                        {
                            Row dataRow = sheet.CreateRow(rowIndex);

                            foreach (DataColumn column in sdt.Columns)
                            {
                                c = dataRow.CreateCell(column.Ordinal);
                                if (column.ColumnName.EqualsAny("數量"))
                                {
                                    int quantity = int.TryParse(row[column].ToString(), out quantity) ? quantity : 0;
                                    c.SetCellValue(quantity);
                                }
                                else
                                {
                                    c.SetCellValue(row[column].ToString());
                                }

                                if (back_color % 2 == 0)
                                {
                                    c.CellStyle = cellColorStyle;
                                }
                                else
                                {
                                    c.CellStyle = cellBorderStyle;
                                }
                            }
                            rowIndex++;
                            rowCount++;
                        }
                        back_color++;
                    }
                }
                else
                {
                    foreach (DataRow row in sdt.Rows)
                    {
                        Row dataRow = sheet.CreateRow(rowIndex);
                        foreach (DataColumn column in sdt.Columns)
                        {
                            c = dataRow.CreateCell(column.Ordinal);
                            if (column.ColumnName.EqualsAny("數量"))
                            {
                                int quantity = int.TryParse(row[column].ToString(), out quantity) ? quantity : 0;
                                c.SetCellValue(quantity);
                            }
                            else
                            {
                                c.SetCellValue(row[column].ToString());
                            }

                            c.CellStyle = cellBorderStyle;
                        }

                        rowIndex++;
                        rowCount++;
                    }
                }
                HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment; filename={0}", HttpUtility.UrlEncode(fileName)));
                workbook.Write(HttpContext.Current.Response.OutputStream);
            }
        }

        private bool IsPartiallyPaymentDateNeeded(Guid dealGuid, RemittanceType remittanceType)
        {
            if (remittanceType != RemittanceType.ManualPartially)
            {
                return false;
            }
            var isExceedReturnLimit = Helper.IsFlagSet(op.GroupOrderGetByBid(dealGuid).Status.GetValueOrDefault((int)GroupOrderStatus.None), 
                                                        GroupOrderStatus.PartialFail);
            return !isExceedReturnLimit;
        }

        public void ShowMessage(string msg)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('" + msg + "');", true);
        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using LunchKingSite.Core.Component;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using System.IO;
using System.Text;
using LunchKingSite.Core.UI;

namespace LunchKingSite.Web.ControlRoom.Ppon
{
    public partial class PponSellerInvoiceList : RolePage
    {
        protected static IOrderProvider _op;

        protected void Page_Load(object sender, EventArgs e)
        {
            _op = ProviderFactory.Instance().GetProvider<IOrderProvider>();

            if (!IsPostBack)
            {
                if (Request["bid"] != "")
                    SetGrid(new Guid(Request["bid"]));
            }
        }

        private void SetGrid(Guid bid)
        {
            DataTable dt = ProviderFactory.Instance().GetProvider<IPponProvider>().GetSellerInvoiceList(bid);
            GridView1.DataSource = dt;
            GridView1.DataBind();
        }

        protected void btnExportToExcel_Click(object sender, EventArgs e)
        {
            ViewPponDeal vp = ProviderFactory.Instance().GetProvider<IPponProvider>().ViewPponDealGet(ViewPponDeal.Columns.BusinessHourGuid, Request["bid"]);
            GridViewExportUtil.Export(vp.CouponUsage + "發票清冊.xls", this.GridView1);
        }

        protected void btnImport_Click(object sender, EventArgs e)
        {
            if (FileUpload1.HasFile)
            {
                string strContent = null;
                StreamReader rdr = new StreamReader(FileUpload1.PostedFile.InputStream, Encoding.Default);
                while ((strContent = rdr.ReadLine()) != null)
                {
                    string[] stringSeparators = new string[] {"^^"};
                    string invoiceNo = strContent.Split(stringSeparators, StringSplitOptions.None)[0];
                    string orderId = strContent.Split(stringSeparators, StringSplitOptions.None)[14];
                    DataOrm.Order o = _op.OrderGet(DataOrm.Order.OrderIdColumn.ColumnName, orderId);
                    if (o != null)
                    {
                        o.InvoiceNo = invoiceNo;
                        _op.OrderSet(o);
                    }
                }
            }
        }

        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                //If the first template field of gridview contains numeric or floting point data then use this code
                e.Row.Cells[4].Attributes.Add("style", @"mso-number-format:\@;");
            }
        }
    }
}
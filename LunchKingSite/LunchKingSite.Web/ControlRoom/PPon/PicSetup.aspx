﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ControlRoom/backend.master" AutoEventWireup="true" CodeBehind="PicSetup.aspx.cs" Inherits="LunchKingSite.Web.ControlRoom.Ppon.PicSetup" %>

<%@ Import Namespace="LunchKingSite.BizLogic.Component" %>
<%@ Import Namespace="LunchKingSite.Core" %>
<%@ Import Namespace="LunchKingSite.DataOrm" %>
<%@ Import Namespace="LunchKingSite.WebLib.Views" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="expires" content="Fri, Jun 12 1981 08:20:00 GM">
    <meta http-equiv="Cache-Control" content="no-cache, must-revalidate">
    <link type="text/css" href="../../Tools/js/css/ui-lightness/jquery-ui-1.8rc3.custom.css"
        rel="stylesheet" />
    <link href="../../Themes/PCweb/css/ppon.css" type="text/css" rel="Stylesheet" />
    <script type="text/javascript" src="//www.google.com/jsapi"></script>
    <script type="text/javascript">
        google.load("jqueryui", "1.8");
    </script>
    <script type="text/javascript">
        $(document).ready(function () {

            $('#images').sortable();
            $('#images').disableSelection();
            $('.dI').button();
            $('.dI').click(function () {
                var cont = confirm('Are you sure you want to delete it?');
                if (cont) {
                    $(this).parent().remove();
                }
            });
            var moveLeft = 20;
            var moveDown = 10;
            $('.productpopimg').hover(function (e) {
                $(this).parent().find('.popupimg').show();
            }, function () {
                $(this).parent().find('.popupimg').hide();
            });
            $('.productpopimg').mousemove(function (e) {
                $(this).parent().find(".popupimg").css('top', e.pageY + moveDown).css('left', e.pageX + moveLeft);
            });
            if ($('#lab_Message').text() != '') {
                $('#lab_Message').fadeOut(5000);
                $('#lab_Message2').fadeOut(5000);
            }
        });
    </script>
    <style type="text/css">
        li {
            list-style: none;
        }

        .popupimg {
            display: none;
            position: absolute;
            z-index: 9999;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div style="background-color: LightBlue">
        檔次圖片
    </div>
    <fieldset>
        <legend>
            <asp:Label ID="lab_Name" runat="server" Font-Bold="true"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:Label ID="lab_Message" runat="server" ForeColor="Red" Font-Bold="true" ClientIDMode="Static"></asp:Label>
        </legend>
        <table style="width: 100%">
            <tr>
                <td colspan="2">檔次Bid:
                    <asp:TextBox ID="tbx_Bid" runat="server" Width="300"></asp:TextBox>
                    <asp:Button ID="btn_Search" runat="server" Text="確認" OnClick="SearchCouponEventContent" />
                </td>
            </tr>
            <tr>
                <td style="border: thin dotted gray; vertical-align: top;">
                    <asp:Repeater ID="ri" runat="server">
                        <HeaderTemplate>
                            <div style="background-color: LightBlue">
                                1. 輪播大圖(請固定大小->480px * 267px)
                            </div>
                            <ul id="images">
                        </HeaderTemplate>
                        <ItemTemplate>
                            <li>
                                <asp:Image ID="i" runat="server" ImageUrl='<%# Eval("ImgPath")+"?"+(DateTime.Now.ToString("ss")) %>'
                                    ImageAlign="Middle" Style="width: 20%;" CssClass="productpopimg" /><input type="hidden" name="is" value='<%#Eval("Index")%>' />
                                <div class="popupimg">
                                    <asp:Image ID="io" runat="server" ImageUrl='<%# Eval("ImgPath")+"?"+(DateTime.Now.ToString("ss")) %>'
                                        ImageAlign="Middle" />
                                </div>
                                <a href="#" class="dI">X</a><hr />
                            </li>
                        </ItemTemplate>
                        <FooterTemplate>
                            </ul>
                        </FooterTemplate>
                    </asp:Repeater>
                </td>
                <td style="border: thin dotted gray; vertical-align: top;">
                    <asp:Repeater ID="ri_2" runat="server">
                        <HeaderTemplate>
                            <div style="background-color: LightGreen">
                                2. SideDeal小圖
                            </div>
                            <br />
                            <ul id="images">
                        </HeaderTemplate>
                        <ItemTemplate>
                            <li>
                                <asp:Image ID="i" runat="server" ImageUrl='<%# Container.DataItem %>' ImageAlign="Middle" Style="width: 40%;" /><input
                                    type="hidden" name="is" value="1" />
                                <a href="#" class="dI">X</a><hr />
                            </li>
                        </ItemTemplate>
                        <FooterTemplate>
                            </ul>
                        </FooterTemplate>
                    </asp:Repeater>
                    <asp:Repeater ID="ri_3" runat="server">
                        <HeaderTemplate>
                            <div style="background-color: Olive">
                                3. Im小圖
                            </div>
                            <br />
                            <ul id="images">
                        </HeaderTemplate>
                        <ItemTemplate>
                            <li>
                                <asp:Image ID="i" runat="server" ImageUrl='<%# Container.DataItem %>' ImageAlign="Middle" Style="width: 40%;" /><input
                                    type="hidden" name="is" value="2" />
                                <a href="#" class="dI">X</a><hr />
                            </li>
                        </ItemTemplate>
                        <FooterTemplate>
                            </ul>
                        </FooterTemplate>
                    </asp:Repeater>
                </td>
            </tr>
        </table>
        <table style="width: 100%; display:none">
            <tr>
                <td style="border: thin dotted gray; vertical-align: top">
                    <asp:Repeater ID="rpSpImg" runat="server">
                        <HeaderTemplate>
                            <div style="background-color: LightBlue">
                                特殊規格圖片(APP或其他用途)
                            </div>
                            <ul id="images">
                        </HeaderTemplate>
                        <ItemTemplate>
                            <li>
                                <asp:Image ID="i" runat="server" ImageUrl='<%# Eval("ImgPath")+"?"+(DateTime.Now.ToString("ss")) %>'
                                    ImageAlign="Middle" Style="width: 40%;" /><input type="hidden" name="spIs" value='<%#Eval("Index")%>' />
                                <a href="#" class="dI">X</a><hr />
                            </li>
                        </ItemTemplate>
                        <FooterTemplate>
                            </ul>
                        </FooterTemplate>
                    </asp:Repeater>
                </td>
            </tr>
        </table>
        <table style="width: 100%">
            <tr>
                <td style="border: thin dotted gray; vertical-align: top">
                    <asp:Repeater ID="rpTravelEDMSpImg" runat="server">
                        <HeaderTemplate>
                            <div style="background-color: LightBlue">
                                旅遊eDM專屬大圖
                            </div>
                            <ul id="images">
                        </HeaderTemplate>
                        <ItemTemplate>
                            <li>
                                <asp:Image ID="i" runat="server" ImageUrl='<%# Eval("ImgPath")+"?"+(DateTime.Now.ToString("ss")) %>'
                                    ImageAlign="Middle" Style="width: 40%;" /><input type="hidden" name="trspIs" value='<%#Eval("Index")%>' />
                                <a href="#" class="dI">X</a><hr />
                            </li>
                        </ItemTemplate>
                        <FooterTemplate>
                            </ul>
                        </FooterTemplate>
                    </asp:Repeater>
                </td>
            </tr>
        </table>
        <asp:RadioButtonList ID="rbl_Type" runat="server" RepeatDirection="Horizontal" RepeatColumns="2">
        </asp:RadioButtonList>
        <asp:CheckBox ID="chkIsPrintWatermark" runat="server" Checked="true" /><font style="color: Brown;">自動壓Logo</font>
        <br />
        上傳圖片:<asp:FileUpload ID="fuI" runat="server" multiple="true" />
        <br />
        <br />
        <asp:Button ID="btn_Update" runat="server" Text="更新" OnClick="Save" Font-Size="Large" />
        <asp:Label ID="lab_Message2" runat="server" ForeColor="Red" Font-Bold="true" Font-Size="Large" ClientIDMode="Static" Style="float: right;"></asp:Label>
    </fieldset>
</asp:Content>

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ControlRoom/backend.master" AutoEventWireup="true"
    CodeBehind="EdmAdSetup.aspx.cs" Inherits="LunchKingSite.Web.ControlRoom.Ppon.EdmAdSetup" %>

<%@ Import Namespace="LunchKingSite.DataOrm" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <script src="../../Tools/js/jquery-1.7.2.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="../../Tools/ckeditor/ckeditor.js"></script>
    <script type="text/javascript" src="../../Tools/ckeditor/adapters/jquery.js"></script>
    <script type="text/javascript">
        $(document).ready(
        function () {
            if ($('#<%=pan_Detail.ClientID%>').is(':visible')) {
                CKEDITOR.replace('<%=tbx_d_Html.ClientID%>');
            }
        }
        );
        $(function () {
            var moveLeft = 20;
            var moveDown = 10;
            $('.trigger').hover(function (e) {
                $(this).find('.pop-up').show();
            }, function () {
                $(this).find('.pop-up').hide();
            });
            $('.trigger').mousemove(function (e) {
                $(this).find(".pop-up").css('top', e.pageY + moveDown).css('left', e.pageX + moveLeft);
            });
        });

    </script>
    <style type="text/css">
        .bgOrange
        {
            background-color: #FFC690;
            text-align: center;
        }
        .bgBlue
        {
            background-color: #B3D5F0;
        }
        .title
        {
            text-align: center;
            background-color: #688E45;
            font-weight: bolder;
            color: White;
        }
        .header
        {
            text-align: center;
            font-size: large;
            font-weight: bolder;
            background-color: #F0AF13;
            color: Blue;
        }
        .fieldset
        {
            border: #688E45 solid 2px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Panel ID="pan_Search" runat="server">
        <fieldset class="fieldset">
            <legend class="title">Top Banner搜尋 </legend>
            <table style="background-color: gray; width: 950px">
                <tr>
                    <td class="bgOrange">
                        訊息名稱
                    </td>
                    <td class="bgBlue">
                        <asp:TextBox ID="tbx_Name" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="bgOrange">
                        訊息期間
                    </td>
                    <td class="bgBlue">
                        <asp:TextBox ID="tbx_Start" runat="server"></asp:TextBox>~<asp:TextBox ID="tbx_End"
                            runat="server"></asp:TextBox>
                        <cc1:CalendarExtender ID="ce_Start" TargetControlID="tbx_Start" runat="server">
                        </cc1:CalendarExtender>
                        <cc1:CalendarExtender ID="ce_End" TargetControlID="tbx_End" runat="server">
                        </cc1:CalendarExtender>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" class="bgBlue" style="text-align: center">
                        <asp:Button ID="btn_Search" runat="server" Text="搜尋" OnClick="SearchAd" />&nbsp;
                        <asp:Button ID="btn_Add" runat="server" Text="新增" ForeColor="Blue" OnClick="AddNew" />&nbsp;
                        <asp:Button ID="btn_UpDataImg" runat="server" Text="上傳圖檔" OnClick="btn_UpDataImg_Click"  />
                    </td>
                </tr>
                <tr>
                    <td colspan="3" class="bgBlue" style="text-align: center">
                        <asp:Label ID="lab_SearchMessage" runat="server" ForeColor="Blue"></asp:Label>
                    </td>
                </tr>
            </table>
        </fieldset>
    </asp:Panel>
    
    <asp:Panel ID="pan_ImgUpData" runat="server" Visible="false">
        <fieldset class="fieldset">
            <legend class="title">Top Banner圖檔上傳 </legend>
            <table style="background-color: gray; width: 950px">
                <tr>
                    <td class="bgOrange">
                        替代文字
                    </td>
                    <td class="bgBlue">
                        <asp:TextBox ID="tbx_upTitle" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="bgOrange">
                        連結
                    </td>
                    <td class="bgBlue">
                        <asp:TextBox ID="tbx_upUrl" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="bgBlue" >
                        <asp:FileUpload runat="server" ID="filMyFile" />&nbsp;
                        <asp:Button Text="上傳" ID="updata" UseSubmitBehavior="false" runat="server" OnClick="btn_UpData_Click" />&nbsp;
                        <asp:Label ID="lab_UpdataMessage" runat="server" ForeColor="Red"></asp:Label>
                    </td>
                </tr>
            </table>
        </fieldset>
    </asp:Panel>

    <asp:Panel ID="pan_List" runat="server" Visible="false">
        <fieldset class="fieldset">
            <legend class="title">列表</legend>
            <asp:Repeater ID="rpt_List" runat="server" OnItemCommand="rptItemCommand">
                <HeaderTemplate>
                    <table style="background-color: gray; width: 950px">
                        <tr>
                            <td class="bgOrange">
                                Id
                            </td>
                            <td class="bgOrange">
                                訊息名稱
                            </td>
                            <td class="bgOrange">
                                起日
                            </td>
                            <td class="bgOrange">
                                迄日
                            </td>
                            <td class="bgOrange">
                                區域
                            </td>
                        </tr>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td class="bgBlue" style="text-align: center">
                            <div class="trigger">
                                <div class="pop-up" style="display: none; position: absolute">
                                    <%# HttpUtility.HtmlDecode(((ViewCmsRandom)(Container.DataItem)).Body)%>
                                </div>
                                <asp:LinkButton ID="lib_Id" runat="server" CommandArgument='<%# ((ViewCmsRandom)(Container.DataItem)).Pid %>'
                                    CommandName='EditItem'><%# ((ViewCmsRandom)(Container.DataItem)).Pid %></asp:LinkButton>
                            </div>
                        </td>
                        <td class="bgBlue">
                            <%# ((ViewCmsRandom)(Container.DataItem)).Title %>
                        </td>
                        <td class="bgBlue">
                            <%# ((ViewCmsRandom)(Container.DataItem)).StartTime.ToString("yyyy/MM/dd") %>
                        </td>
                        <td class="bgBlue">
                            <%# ((ViewCmsRandom)(Container.DataItem)).EndTime.ToString("yyyy/MM/dd") %>
                        </td>
                        <td class="bgBlue">
                            <%# ((ViewCmsRandom)(Container.DataItem)).CityName%>
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </table>
                </FooterTemplate>
            </asp:Repeater>
        </fieldset>
    </asp:Panel>
    <asp:Panel ID="pan_Detail" runat="server" Visible="false">
        <fieldset class="fieldset">
            <legend class="title">Top Banner<asp:Label ID="lab_Type" runat="server"></asp:Label>設定
                <asp:HiddenField ID="hif_Id" runat="server" />
            </legend>
            <table style="background-color: gray; width: 950px">
                <tr>
                    <td class="bgOrange">
                        名稱
                    </td>
                    <td class="bgBlue">
                        <asp:TextBox ID="tbx_d_Title" runat="server"></asp:TextBox><asp:RequiredFieldValidator
                            ID="rfv_d_Title" runat="server" ErrorMessage="*" ForeColor="Red" Display="Dynamic"
                            ControlToValidate="tbx_d_Title" ValidationGroup="A"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="bgOrange">
                        期間
                    </td>
                    <td class="bgBlue">
                        <asp:TextBox ID="tbx_d_Start" runat="server"></asp:TextBox><asp:RequiredFieldValidator
                            ID="rfv_d_Start" runat="server" ErrorMessage="*" Display="Dynamic" ControlToValidate="tbx_d_Start"
                            ValidationGroup="A"></asp:RequiredFieldValidator>
                        ~
                        <asp:TextBox ID="tbx_d_End" runat="server"></asp:TextBox><asp:RequiredFieldValidator
                            ID="rfv_d_End" runat="server" ErrorMessage="*" Display="Dynamic" ControlToValidate="tbx_d_End"
                            ValidationGroup="A"></asp:RequiredFieldValidator>
                        <cc1:CalendarExtender ID="ce_d_Start" TargetControlID="tbx_d_Start" runat="server">
                        </cc1:CalendarExtender>
                        <cc1:CalendarExtender ID="ce_d_End" TargetControlID="tbx_d_End" runat="server">
                        </cc1:CalendarExtender>
                    </td>
                </tr>
                <tr>
                    <td class="bgOrange">
                        區域
                    </td>
                    <td class="bgBlue">
                        <asp:CheckBoxList ID="cbx_d_Area" runat="server" RepeatDirection="Horizontal">
                        </asp:CheckBoxList>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="bgOrange">
                        訊息內容 (固定寬高740px*80px)
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="bgBlue">
                        <asp:TextBox ID="tbx_d_Html" runat="server" TextMode="MultiLine" Width="900" Height="250"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="bgBlue" style="text-align: center">
                        <asp:Button ID="btn_d_Save" runat="server" Text="確定" ForeColor="Blue" OnClick="SaveDetail"
                            ValidationGroup="A" />&nbsp;&nbsp;<asp:Button ID="btn_d_Cancel" runat="server" Text="取消/返回"
                                ForeColor="Red" OnClick="CancelAction" />&nbsp;&nbsp;
                        <asp:Button ID="btn_d_Delete" runat="server" Text="刪除" OnClientClick="return confirm('確定刪除??');"
                            OnClick="DeleteAll" Visible="false" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="bgBlue" style="text-align: center">
                        <asp:Label ID="lab_Message" runat="server" ForeColor="Blue"></asp:Label>
                    </td>
                </tr>
            </table>
        </fieldset>
    </asp:Panel>
</asp:Content>

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.Services;
using LunchKingSite.BizLogic.Component;
using log4net;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;
using LunchKingSite.Core.UI;
using Newtonsoft.Json;

namespace LunchKingSite.Web.ControlRoom.Ppon
{
    public partial class PponDealTimeSlot2 : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        [WebMethod]
        public static PponDealTimeSlotData GetSlotData(int cityId)
        {
            PponDealTimeSlotData SlotData = PponDealTimeSlotCore.QueryData(cityId);
            File.WriteAllText("c:\\temp\\1.txt", JsonConvert.SerializeObject(SlotData, Formatting.Indented));
            return SlotData;
        }
    }

    public class PponDealTimeSlotCore
    {
        private static IPponProvider _pponProv = ProviderFactory.Instance().GetProvider<IPponProvider>();
        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        public static PponDealTimeSlotData QueryData(int cityId)
        {
            PponDealTimeSlotData pageData = new PponDealTimeSlotData(6);

            PponCity showCity = PponCityGroup.DefaultPponCityGroup.GetPponCityByCityId(cityId);
            pageData.title = showCity.CityName;

            DateTime firstDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 12, 0, 0);
            if (DateTime.Now.Hour < 12)
            {
                firstDate = firstDate.AddDays(-1);
            }

            //取得七天內的新上檔次
            ViewPponBusinessHourGuidCollection newDeals = _pponProv.ViewPponBusinessHourGuidGetList(firstDate, firstDate.AddDays(7));

            //目前共產生七天的表格資料
            for (int i = 0; i < pageData.daySlots.Length; i++)
            {
                DateTime workDate = firstDate.AddDays(i);
                //設定日期用於顯示
                
                var daySlot = pageData.daySlots[i];                

                daySlot.title = workDate.ToString("MM/dd hh:mm") + " ~ " + workDate.AddDays(1).AddMinutes(-1).ToString("MM/dd hh:mm");

                //設定每天的新檔次
                foreach (ViewPponBusinessHourGuid vpbhg in newDeals.Where(x => x.BusinessHourOrderTimeS.HasValue
                    && int.Equals(workDate.Day, x.BusinessHourOrderTimeS.Value.Day)))
                {
                    daySlot.newDealIds.Add(vpbhg.Guid.Value);
                }

                //依據查詢所有程式的資料
                List<string> searchWhere = new List<string>();
                searchWhere.Add(ViewPponDealTimeSlot.Columns.EffectiveStart + " >= " + workDate.ToString("yyyy/MM/dd HH:mm:ss"));
                searchWhere.Add(ViewPponDealTimeSlot.Columns.EffectiveStart + " < " + workDate.AddDays(1).ToString("yyyy/MM/dd HH:mm:ss"));
                searchWhere.Add(ViewPponDealTimeSlot.Columns.CityId + " = " + cityId);

                ViewPponDealTimeSlotCollection dataColl =
                    _pponProv.ViewPponDealTimeSlotGetList(
                        ViewPponDealTimeSlot.Columns.CityId + "," + ViewPponDealTimeSlot.Columns.Sequence,
                        searchWhere.ToArray());

                foreach (ViewPponDealTimeSlot item in dataColl.Where(x => !((x.BusinessHourStatus & (int)BusinessHourStatus.ComboDealSub) > 0)))
                {
                    daySlot.slots.Add(new SlotUnit
                    {
                        dealId = item.BusinessHourGuid.ToString(),
                        title = item.ItemName
                    });
                }
            }

            return pageData;
        }
    }

    #region models

    public class SlotUnit
    {
        public string dealId;
        public string title;
    }

    public class DaySlot
    {
        public string title;
        public List<SlotUnit> slots = new List<SlotUnit>();
        public List<Guid> newDealIds = new List<Guid>();
    }


    public class PponDealTimeSlotData
    {
        public DaySlot[] daySlots;
        public PponDealTimeSlotData(int day)
        {
            daySlots = new DaySlot[day];
            for (int i = 0; i < daySlots.Length; i++)
            {
                daySlots[i] = new DaySlot();
            }
        }
        public string title;
    }

    #endregion
}
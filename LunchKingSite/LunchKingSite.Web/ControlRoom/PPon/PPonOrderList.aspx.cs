﻿using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using LunchKingSite.I18N;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class PponOrderList : RolePage, IPponOrderListView
{
    public event EventHandler<DataEventArgs<int>> GetOrderCount;

    public event EventHandler<DataEventArgs<Guid>> GetCurrentQuantity;

    public event EventHandler SortClicked;

    public event EventHandler<DataEventArgs<int>> PageChanged;

    public event EventHandler SearchClicked;

    protected static IOrderProvider op = ProviderFactory.Instance().GetProvider<IOrderProvider>();

    #region props

    private PponOrderListPresenter _presenter;

    public PponOrderListPresenter Presenter
    {
        set
        {
            this._presenter = value;
            if (value != null)
            {
                this._presenter.View = this;
            }
        }
        get
        {
            return this._presenter;
        }
    }

    public string SortExpression
    {
        get
        {
            return (ViewState != null && ViewState["se"] != null) ? (string)ViewState["se"] : ViewPponDeal.Columns.BusinessHourOrderTimeS + " desc";
        }
        set
        {
            ViewState["se"] = value;
        }
    }

    public string FilterUser
    {
        get
        {
            return tbSearch.Text;
        }
        set
        {
            tbSearch.Text = value;
        }
    }

    public string[] FilterInternal
    {
        get
        {
            string[] filter = new string[5];

            filter[0] = !string.IsNullOrEmpty(tbDS.Text) ? ViewPponDeal.Columns.BusinessHourOrderTimeS + " >= " + tbDS.Text : null;
            filter[1] = !string.IsNullOrEmpty(tbDE.Text) ? ViewPponDeal.Columns.BusinessHourOrderTimeS + " < " + tbDE.Text : null;
            filter[2] = !string.IsNullOrEmpty(tbES.Text) ? ViewPponDeal.Columns.BusinessHourOrderTimeE + " >= " + tbES.Text : null;
            filter[3] = !string.IsNullOrEmpty(tbEE.Text) ? ViewPponDeal.Columns.BusinessHourOrderTimeE + " < " + tbEE.Text : null;
            filter[4] = ViewPponDeal.Columns.UniqueId + " is not null";
            return filter;
        }
    }

    public string FilterType
    {
        get
        {
            return ddlST.SelectedValue;
        }
        set
        {
            ddlST.SelectedValue = value;
        }
    }

    public bool FilterPiinlife
    {
        get
        {
            return chkFilterPiinlife.Checked;
        }
    }

    public List<String> SelectedStatusFilter
    {
        get
        {
            List<int> selected = new List<int>();
            List<String> selStatus = new List<string>();

            foreach (DataListItem dlI in dL.Items)
            {
                foreach (Control ctl in dlI.Controls)
                {
                    RadioButtonList r = ctl as RadioButtonList;
                    if (r != null)
                    {
                        selected.Add(int.Parse(r.Text));
                    }
                }
            }

            if (selected == null || selected.Count == 0)
            {
                return selStatus;
            }

            String[] trueStatus = {ViewPponDeal.Columns.BusinessHourOrderTimeS + " > " + DateTime.Now,
                                   ViewPponDeal.Columns.BusinessHourOrderTimeS + " <= " + DateTime.Now + " AND " + ViewPponDeal.Columns.BusinessHourOrderTimeE + " > " + DateTime.Now,
                                   ViewPponDeal.Columns.BusinessHourOrderTimeE + " <= " + DateTime.Now,
                                   ViewPponDeal.Columns.OrderedQuantity + " >= " + ViewPponDeal.Columns.BusinessHourOrderMinimum,
                                   ViewPponDeal.Columns.OrderedQuantity + " < " + ViewPponDeal.Columns.BusinessHourOrderMinimum + " AND " + ViewPponDeal.Columns.BusinessHourOrderTimeE + " <= " + DateTime.Now};
            String[] falseStatus = {ViewPponDeal.Columns.BusinessHourOrderTimeS + " <= " + DateTime.Now,
                                    ViewPponDeal.Columns.BusinessHourOrderTimeS + " > " + DateTime.Now,
                                    ViewPponDeal.Columns.BusinessHourOrderTimeE + " > " + DateTime.Now,
                                    ViewPponDeal.Columns.OrderedQuantity + " < " + ViewPponDeal.Columns.BusinessHourOrderMinimum,
                                    ViewPponDeal.Columns.OrderedQuantity + " >= " + ViewPponDeal.Columns.BusinessHourOrderMinimum};

            for (int i = 0; i < selected.Count; i++)
            {
                switch (selected[i])
                {
                    case 0:
                        selStatus.Add(trueStatus[i]);
                        break;

                    case 1:
                        selStatus.Add(falseStatus[i]);
                        break;

                    default:
                        break;
                }
            }

            return selStatus;
        }
    }

    public int PageSize
    {
        get
        {
            return gridPager.PageSize;
        }
        set
        {
            gridPager.PageSize = value;
        }
    }

    public int CurrentPage
    {
        get
        {
            return gridPager.CurrentPage;
        }
    }

    public Dictionary<int, string> StatusFilter
    {
        get
        {
            Dictionary<int, string> data = new Dictionary<int, string>();
            data.Add(0, Phrase.NotToBegin);
            data.Add(1, Phrase.Proceeding);
            data.Add(2, Phrase.EventExpired);
            data.Add(3, Phrase.DealIsOn);
            data.Add(4, Phrase.DealIsFailure);

            return data;
        }
    }

    public Dictionary<string, string> FilterTypes
    {
        get
        {
            Dictionary<string, string> data = new Dictionary<string, string>();

            data.Add(ViewPponDeal.Columns.SellerName, Phrase.SellerName);
            data.Add(ViewPponDeal.Columns.EventName, Phrase.EventName);
            data.Add(ViewPponDeal.Columns.UniqueId, Phrase.BusinessHourUniqueId);
            data.Add(ViewPponDeal.Columns.BusinessHourGuid, Phrase.BusinessHourGuid);

            return data;
        }
    }

    public string OrderByInternal
    {
        get
        {
            return ViewPponDeal.Columns.BusinessHourOrderTimeS + " desc";
        }
    }

    public int CurrentQuantity
    {
        get
        {
            return (ViewState != null && ViewState["quantity"] != null) ? (int)ViewState["quantity"] : 0;
        }
        set
        {
            ViewState["quantity"] = value;
        }
    }

    #endregion props

    protected void Page_Load(object sender, EventArgs e)
    {
        gvOrder.Columns[0].SortExpression = ViewPponDeal.Columns.SellerName;
        gvOrder.Columns[1].SortExpression = ViewPponDeal.Columns.EventName;
        gvOrder.Columns[3].SortExpression = ViewPponDeal.Columns.BusinessHourOrderMinimum;
        gvOrder.Columns[4].SortExpression = ViewPponDeal.Columns.OrderedQuantity;
        gvOrder.Columns[5].SortExpression = ViewPponDeal.Columns.OrderedTotal;
        gvOrder.Columns[6].SortExpression = ViewPponDeal.Columns.BusinessHourOrderTimeS;
        gvOrder.Columns[7].SortExpression = ViewPponDeal.Columns.BusinessHourOrderTimeE;

        if (!Page.IsPostBack)
        {
            this.Presenter.OnViewInitialized();
        }

        this.Presenter.OnViewLoaded();
    }

    public void SetPponList(ViewPponDealCollection orders)
    {
        gvOrder.DataSource = orders;
        gvOrder.DataBind();
    }

    public void SetStatusFilter(Dictionary<int, string> status)
    {
        dL.DataSource = status;
        dL.DataBind();
    }

    public void SetFilterTypeDropDown(Dictionary<string, string> types)
    {
        ddlST.DataSource = types;
        ddlST.DataBind();
    }

    protected void bSearchBtn_Click(object sender, EventArgs e)
    {
        if (Validation(FilterType, tbSearch.Text.Trim()))
        {
            if (this.SearchClicked != null)
            {
                this.SearchClicked(this, e);
            }

            gridPager.ResolvePagerView(1, true);
        }
    }

    protected void gvOrder_Sorting(object sender, GridViewSortEventArgs e)
    {
        ViewState["sd"] = ((string)ViewState["sd"] == " ASC") ? " DESC" : " ASC";
        this.SortExpression = e.SortExpression + (string)ViewState["sd"];
        if (this.SortClicked != null)
        {
            SortClicked(this, new EventArgs());
        }

        gridPager.ResolvePagerView(1);
    }

    protected void UpdateHandler(int pageNumber)
    {
        DataEventArgs<int> e = new DataEventArgs<int>(pageNumber);
        if (this.PageChanged != null)
        {
            this.PageChanged(this, e);
        }
    }

    protected int RetrieveOrderCount()
    {
        DataEventArgs<int> e = new DataEventArgs<int>(0);
        if (GetOrderCount != null)
        {
            GetOrderCount(this, e);
        }

        return e.Data;
    }

    protected int RetrieveCurrentQuantity(Guid bid)
    {
        if (GetCurrentQuantity != null)
        {
            GetCurrentQuantity(this, new DataEventArgs<Guid>(bid));
        }

        return CurrentQuantity;
    }

    protected void ddlTmr_SIC(object sender, EventArgs e)
    {
        if (ddlTmr.SelectedValue == "0")
        {
            tmr1.Enabled = false;
        }

        else
        {
            tmr1.Enabled = true;
            tmr1.Interval = int.Parse(ddlTmr.SelectedValue) * 1000;
        }
    }

    protected void ddlPS_SIC(object sender, EventArgs e)
    {
        this.PageSize = int.Parse(ddlPS.SelectedValue);
        if (this.PageChanged != null)
        {
            this.PageChanged(this, new DataEventArgs<int>(gridPager.CurrentPage));
        }
        gridPager.ResolvePagerView(gridPager.CurrentPage, true);
    }

    protected void tmr1_Tick(object sender, EventArgs e)
    {
        if (this.PageChanged != null)
        {
            this.PageChanged(this, new DataEventArgs<int>(gridPager.CurrentPage));
        }
        gridPager.ResolvePagerView(gridPager.CurrentPage, true);
    }

    protected void gvOrder_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            HyperLink hs = (HyperLink)e.Row.Cells[1].FindControl("hs");
            Literal ls = (Literal)e.Row.Cells[2].FindControl("ls");
            Literal ld = (Literal)e.Row.Cells[8].FindControl("ld");
            ViewPponDeal vpol = (ViewPponDeal)e.Row.DataItem;

            if (vpol.OrderGuid == null || vpol.OrderGuid == Guid.Empty)
            {
                hs.Text = "請先將活動" + vpol.EventName + "執行start deal!";
            }
            else
            {
                hs.NavigateUrl = "pponorderdetail.aspx?pid=" + vpol.OrderGuid;
                string newEventName = PponFacade.EventNameGet(vpol);
                hs.Text = newEventName.TrimToMaxLength(40, "...");
            }

            if (vpol.BusinessHourOrderTimeS > DateTime.Now)
            {
                ls.Text = Phrase.NotToBegin;
                ld.Text = Phrase.NotToBegin;
            }
            else if (vpol.BusinessHourOrderTimeS <= DateTime.Now && vpol.BusinessHourOrderTimeE > DateTime.Now)
            {
                ls.Text = Phrase.Proceeding;
                ld.Text = Phrase.ToBeSettled;
                e.Row.BackColor = System.Drawing.Color.Yellow; // determine background
            }
            else if (vpol.BusinessHourOrderTimeE <= DateTime.Now)
            {
                int slug;
                int.TryParse(vpol.Slug, out slug);
                ls.Text = Phrase.EventExpired + "<br/>";
                ls.Text += vpol.BusinessHourOrderMinimum <= slug ? Phrase.DealIsOn : Phrase.DealIsFailure;

                if (vpol.BusinessHourOrderMinimum <= slug)
                {
                    if (Helper.IsFlagSet(vpol.GroupOrderStatus ?? 0, (long)GroupOrderStatus.UnderPaymentRequest))
                    {
                        ld.Text = Phrase.UnderPaymentRequest;
                    }
                    else if (Helper.IsFlagSet(vpol.GroupOrderStatus ?? 0, (long)GroupOrderStatus.PaymentReqSucceeded))
                    {
                        ld.Text = Phrase.PaymentReqSucceeded;
                    }
                    else if (Helper.IsFlagSet(vpol.GroupOrderStatus ?? 0, (long)GroupOrderStatus.PaymentReqPartiallySucceeded))
                    {
                        ld.Text = Phrase.PaymentReqPartiallySucceeded;
                    }

                    e.Row.BackColor = System.Drawing.Color.Red; // determine background
                }
                else
                {
                    ld.Text = Phrase.PaymentReqCancelled;
                    e.Row.BackColor = System.Drawing.Color.Gray; // determine background
                }
            }
        }
    }

    private bool Validation(string column, string value)
    {
        lblErrorMsg.Text = string.Empty;

        if (column.Equals(ViewPponDeal.Columns.BusinessHourGuid))
        {
            Regex guidRegEx = new Regex(@"^(\{{0,1}([0-9a-fA-F]){8}-([0-9a-fA-F]){4}-([0-9a-fA-F]){4}-([0-9a-fA-F]){4}-([0-9a-fA-F]){12}\}{0,1})$");
            if (guidRegEx.IsMatch(value))
            {
                return true;
            }
            else
            {
                lblErrorMsg.Text = "bid輸入錯誤!!";
                return false;
            }
        }
        else if (column.Equals(ViewPponDeal.Columns.UniqueId))
        {
            int text;
            if (int.TryParse(value, out text))
            {
                return true;
            }
            else
            {
                lblErrorMsg.Text = "檔號輸入錯誤!!";
                return false;
            }
        }
        else
        {
            return true;
        }
    }
}
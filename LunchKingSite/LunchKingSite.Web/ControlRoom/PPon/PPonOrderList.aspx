﻿<%@ Page Language="C#" MasterPageFile="~/ControlRoom/backend.master" AutoEventWireup="true"
    CodeBehind="PponOrderList.aspx.cs" Inherits="PponOrderList" %>

<%@ Import Namespace="LunchKingSite.I18N" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="../Controls/Pager.ascx" TagName="Pager" TagPrefix="uc1" %>
<%@ Register Assembly="System.Web.Extensions" Namespace="System.Web.UI" TagPrefix="aspajax" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
    <table width="920">
        <tr>
            <td style="width: 79%; height: 31px;">
                <table style="border: double 3px black">
                    <tr>
                        <th style="width: 714px;">
                            活動列表頁
                        </th>
                    </tr>
                    <tr>
                        <td style="width: 714px;">
                            <asp:DropDownList ID="ddlST" runat="server" DataTextField="Value" DataValueField="Key"
                                Width="103px" />
                            <asp:TextBox ID="tbSearch" runat="server" Width="250px"></asp:TextBox>
                            <br />
                            活動開始時間 :
                            <asp:TextBox ID="tbDS" runat="server" Columns="8" />
                            <cc1:CalendarExtender ID="ceDS" TargetControlID="tbDS" runat="server">
                            </cc1:CalendarExtender>
                            至
                            <asp:TextBox ID="tbDE" runat="server" Columns="8" />
                            <cc1:CalendarExtender ID="ceDE" TargetControlID="tbDE" runat="server">
                            </cc1:CalendarExtender>
                            <br />
                            活動截止時間 :
                            <asp:TextBox ID="tbES" runat="server" Columns="8" />
                            <cc1:CalendarExtender ID="ceES" TargetControlID="tbES" runat="server">
                            </cc1:CalendarExtender>
                            至
                            <asp:TextBox ID="tbEE" runat="server" Columns="8" />
                            <cc1:CalendarExtender ID="ceEE" TargetControlID="tbEE" runat="server">
                            </cc1:CalendarExtender>
                            <asp:CheckBox ID="chkFilterPiinlife" runat="server" Text="只顯示品生活" />
                            <br />
                            狀態:
                            <asp:DataList ID="dL" runat="server" RepeatColumns="3" Width="710px" RepeatDirection="Horizontal"
                                ShowFooter="False" ShowHeader="False" Height="1px" BorderColor="#DEBA84" BackColor="#DEBA84"
                                BorderStyle="None" BorderWidth="1px" CellPadding="3" CellSpacing="2" GridLines="Both">
                                <ItemTemplate>
                                    &nbsp;<asp:Label ID="Label1" runat="server" Text='<%# Bind("value") %>' Font-Bold="True"
                                        Font-Size="Medium" Width="104px"></asp:Label>&nbsp;<asp:RadioButtonList ID="cbS"
                                            runat="server" RepeatDirection="Horizontal" DataTextField="Value" DataValueField="Key"
                                            RepeatLayout="Flow" HorizontalAlign="right" Font-Size="Smaller">
                                            <asp:ListItem Value="0">是</asp:ListItem>
                                            <asp:ListItem Value="1">否</asp:ListItem>
                                            <asp:ListItem Value="2" Selected="True">忽略</asp:ListItem>
                                        </asp:RadioButtonList>
                                </ItemTemplate>
                                <FooterStyle BackColor="#F7DFB5" ForeColor="#8C4510" />
                                <ItemStyle BackColor="#FFF7E7" ForeColor="#8C4510" />
                                <SelectedItemStyle BackColor="#738A9C" Font-Bold="True" ForeColor="White" />
                                <HeaderStyle BackColor="#A55129" Font-Bold="True" ForeColor="White" />
                            </asp:DataList>&nbsp;<asp:Button ID="bSearchBtn" OnClick="bSearchBtn_Click" runat="server"
                                Text="搜尋" ValidationGroup="Search"></asp:Button>&nbsp;
                        </td>
                    </tr>
                </table>
            </td>
            <td style="width: 25%; height: 31px;" valign="top">
                <table style="border: double 3px black">
                    <tr>
                        <th style="width: 174px">
                            列表狀態
                        </th>
                    </tr>
                    <tr>
                        <td style="width: 174px">
                            自動更新 (秒):
                            <asp:DropDownList ID="ddlTmr" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlTmr_SIC">
                                <asp:ListItem Text="無" Value="0" Selected="True" />
                                <asp:ListItem Text="10" Value="10" />
                                <asp:ListItem Text="30" Value="30" />
                                <asp:ListItem Text="60" Value="60" />
                            </asp:DropDownList>
                            <aspajax:Timer ID="tmr1" runat="server" Enabled="false" OnTick="tmr1_Tick" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 174px">
                            每頁顯示:
                            <asp:DropDownList ID="ddlPS" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlPS_SIC">
                                <asp:ListItem Text="15" Value="15" Selected="true" />
                                <asp:ListItem Text="30" Value="30" />
                                <asp:ListItem Text="60" Value="60" />
                                <asp:ListItem Text="100" Value="100" />
                                <asp:ListItem Text="200" Value="200" />
                            </asp:DropDownList>
                            筆資料
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <aspajax:UpdatePanel runat="server" ID="UpdatePanel1" ChildrenAsTriggers="true" UpdateMode="Conditional">
        <contenttemplate>
            <asp:Label ID="lblErrorMsg" runat="server" ForeColor="Red"></asp:Label>
            <asp:GridView ID="gvOrder" runat="server" EnableViewState="False" OnSorting="gvOrder_Sorting"
                OnRowDataBound="gvOrder_RowDataBound" AllowSorting="True" GridLines="Vertical"
                ForeColor="Black" CellPadding="4" BorderWidth="1px" BorderStyle="None" BorderColor="#DEDFDE"
                BackColor="White" EmptyDataText="無符合的資料" AutoGenerateColumns="False"
                Font-Size="Small" EnableModelValidation="True">
                <FooterStyle BackColor="#CCCC99"></FooterStyle>
                <RowStyle BackColor="#F7F7DE"></RowStyle>
                <SelectedRowStyle BackColor="#CE5D5A" ForeColor="White" Font-Bold="True"></SelectedRowStyle>
                <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right"></PagerStyle>
                <HeaderStyle BackColor="#6B696B" ForeColor="White" Font-Bold="True"></HeaderStyle>
                <AlternatingRowStyle BackColor="White"></AlternatingRowStyle>
                <Columns>
                    <asp:BoundField HeaderText="店家名稱" DataField="SellerName" ReadOnly="True">
                        <ItemStyle Width="95px" Wrap="False" HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:BoundField HeaderText="檔號" DataField="UniqueId" ReadOnly="True">
                        <ItemStyle Width="95px" Wrap="False" HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:TemplateField HeaderText="活動名稱">
                        <ItemTemplate>
                            <asp:HyperLink ID="hs" runat="server"></asp:HyperLink>
                        </ItemTemplate>
                        <ItemStyle Wrap="False" Width="130px" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="活動狀態">
                        <ItemTemplate>
                            <asp:Literal ID="ls" runat="server" EnableViewState="false"  />
                        </ItemTemplate>
                        <ItemStyle Width="60px" />
                    </asp:TemplateField>
                    <asp:BoundField ReadOnly="True" DataField="BusinessHourOrderMinimum" HeaderText="成單門檻">
                        <ItemStyle Wrap="False" Width="100px" />
                    </asp:BoundField>
                    <asp:TemplateField HeaderText="目前人數" ItemStyle-HorizontalAlign="Right">
                        <ItemTemplate>
                            <asp:Label  Wrap="False" Width="70px" ID="lbQuantity" runat="server" 
                                Text='<%# (Convert.ToDateTime(Eval("BusinessHourOrderTimeS"))>Convert.ToDateTime("2011-08-01")) 
                                            ? RetrieveCurrentQuantity(new Guid(Eval("BusinessHourGuid").ToString())) 
                                            : Eval("OrderedQuantity") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="目前金額" ItemStyle-HorizontalAlign="Right">
                        <ItemTemplate>
                            <asp:Label Width="90px" ID="lbAmount" runat="server" 
                                Text='<%# (Convert.ToDateTime(Eval("BusinessHourOrderTimeS"))>Convert.ToDateTime("2011-08-01")) 
                                        ? (Convert.ToDecimal(Eval("ItemPrice")) * RetrieveCurrentQuantity(new Guid(Eval("BusinessHourGuid").ToString()))).ToString("C")
                                        : Convert.ToDecimal(Eval("OrderedTotal")).ToString("C") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField ReadOnly="True" DataField="BusinessHourOrderTimeS" DataFormatString="{0:yyyy/MM/dd HH:mm}"
                        HtmlEncode="False" HeaderText="活動開始時間">
                        <ItemStyle Width="90px" />
                    </asp:BoundField>
                    <asp:BoundField ReadOnly="True" DataField="BusinessHourOrderTimeE" DataFormatString="{0:yyyy/MM/dd HH:mm}"
                        HtmlEncode="False" HeaderText="活動結束時間">
                        <ItemStyle Width="90px" />
                    </asp:BoundField>
                    <asp:TemplateField HeaderText="請款及交易明細">
                        <ItemTemplate>
                            <asp:Literal ID="ld" runat="server" EnableViewState="false"  />
                        </ItemTemplate>
                        <ItemStyle Width="60px" />
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
            <uc1:Pager ID="gridPager" runat="server" PageSize="15" OnGetCount="RetrieveOrderCount"
                OnUpdate="UpdateHandler"></uc1:Pager>
        </contenttemplate>
        <triggers>
<asp:AsyncPostBackTrigger ControlID="bSearchBtn" EventName="Click"></asp:AsyncPostBackTrigger>
<asp:AsyncPostBackTrigger ControlID="ddlPS" EventName="SelectedIndexChanged"></asp:AsyncPostBackTrigger>
<asp:AsyncPostBackTrigger ControlID="tmr1" EventName="Tick"></asp:AsyncPostBackTrigger>
        </triggers>
    </aspajax:UpdatePanel>
    <script>
        $(document).ready(function () {
            document.onkeypress = function () {
                if (window.event && window.event.keyCode == 13) {
                    $('#<%=bSearchBtn.ClientID %>').click();
                }
            }
        });
    </script>
</asp:Content>

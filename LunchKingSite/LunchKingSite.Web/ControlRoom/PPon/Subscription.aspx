﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ControlRoom/backend.master" AutoEventWireup="true" CodeBehind="Subscription.aspx.cs" Inherits="LunchKingSite.Web.ControlRoom.Ppon.Subscription" %>
<%@ Import Namespace="LunchKingSite.BizLogic.Component" %>
<%@ Import Namespace="LunchKingSite.DataOrm" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<table>
    <tr>
        <td>
            email
        </td>
        <td>
            <asp:TextBox ID="txtemail" runat="server"></asp:TextBox>
        </td>
    </tr>
     <tr>
        <td>
        </td>
        <td>
            <asp:Button ID="btnQuery" runat="server" Text="查詢" onclick="btnQuery_Click" />
        </td>
    </tr>
     <tr>
        <td colspan="2">
            <asp:GridView ID="gvS" runat="server" AutoGenerateColumns="False" 
                EnableModelValidation="True" OnRowCommand="Gvs_RowCommand">
                <Columns>
                    <asp:TemplateField HeaderText="id">
                        <EditItemTemplate>
                            <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("id") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblId" runat="server" Text='<%# Bind("id") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="email">
                        <EditItemTemplate>
                            <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("email") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label2" runat="server" Text='<%# Bind("email") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="city">
                        <EditItemTemplate>
                            <asp:TextBox ID="TextBox3" runat="server" Text='<%#CategoryManager.CategoryGetById(((Subscription)Container.DataItem).CategoryId)%>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label3" runat="server" Text='<%#CategoryManager.CategoryGetById(((Subscription)Container.DataItem).CategoryId)%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="time">
                        <EditItemTemplate>
                            <asp:TextBox ID="TextBox4" runat="server" Text='<%# Bind("createtime") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label4" runat="server" Text='<%# Bind("createtime") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ShowHeader="False">
                        <ItemTemplate>
                            <asp:LinkButton ID="LinkButton1" runat="server" 
                                 Text="Delete" OnClientClick="javasrcipt:return confirm('確定刪除嗎?')"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </td>
    </tr>
</table>
</asp:Content>

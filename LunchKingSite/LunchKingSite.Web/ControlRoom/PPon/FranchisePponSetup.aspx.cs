﻿using LunchKingSite.Core.UI;
using System;

namespace LunchKingSite.Web.ControlRoom.Ppon
{
    public partial class FranchisePponSetup : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Write("此功能已停用，如有疑問請洽IT");
        }
    }
}
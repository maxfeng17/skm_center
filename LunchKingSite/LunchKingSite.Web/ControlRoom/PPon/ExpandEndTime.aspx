﻿<%@ Page Language="C#" MasterPageFile="../backend.master" AutoEventWireup="true" CodeBehind="ExpandEndTime.aspx.cs" Inherits="LunchKingSite.Web.ControlRoom.Ppon.ExpandEndTime" %>
<%@ Import Namespace="LunchKingSite.Core.Component" %>
<%@ Import Namespace="LunchKingSite.Core" %>


<asp:Content ContentPlaceHolderID="SSC" runat="server">
    <link type="text/css" href="../../Tools/js/css/ui-lightness/jquery-ui-1.8rc3.custom.css"
        rel="stylesheet" />
    <script type="text/javascript" src="//www.google.com/jsapi"></script>
    <script type="text/javascript">
        google.load("jqueryui", "1.8");
    </script>
    <style>
        .frmbutton {
            letter-spacing: 3px;
            padding: 3px;
            text-align: center;
            font-size: 13px;
        }
        .error {
            color:red;
        }
        #resultTable {
            width:600px;
            font-family:monospace;
        }
        #resultTable th {
            padding:3px;
            text-align:left;
        }
        #resultTable td {
             padding:3px;
        }
        .main-content input {
            margin-top:5px;
        }
        .main-content textarea {
            margin-top:5px;
            display:block;
        }
        #infoCell1, #infoCell2 {
            margin-right:12px;
        }
        .tab
        {
            font-size: smaller;
        }
    </style>
    <script type="text/javascript">        
        window.siteUrl = '<%=ProviderFactory.Instance().GetConfig().SiteUrl%>';
        window.ajaxTimeout = <%=Request["t"] == null ? 30000 : int.Parse(Request["t"])*1000%>;
        window.ajaxPagesize = <%=Request["p"] == null ? 1 : int.Parse(Request["p"])%>;
        $(document).ready(function () {

            var $tabs = $('#tabs').tabs();
            loadDatePicker();

            //延長檔次
            $('#txtBids').keyup(function() {
                if (QueryManager.GetArrayFromTextArea('txtBids').length == 0) {
                    $('#btnQuery').attr('disabled', true);
                } else {
                    $('#btnQuery').removeAttr('disabled');
                }
            });

            function resetRow() {
                $('#noDataRows').hide();
                $('#resultRows').show();
                $('#resultRows').find('tr').remove();
                $('#btnExecute').attr('disabled', true);
            }

            function appendRows(data) {
                var i = $('#resultRows').find('tr').size();
                for (var key in data) {
                    var item = data[key];
                    i++;
                    appendRow(i, item.Bid, item.Result, item.Message);
                }
            }

            function appendRow(idx, bid, result, message) {
                var row = $('<tr></tr>').appendTo($('#resultRows'));
                if (result) {
                    row.append('<td><input type="checkbox" checked="checked" value="' + bid +  '" /> ' + idx + '</td>');
                } else {
                    row.append('<td style="padding-left:32px">' + idx + '</td>');
                }
                row.append('<td><a href="/'+ bid+'" target="_blank">' + bid + '</a></td>');
                if (result) {
                    row.append('<td class="status">Ready</td><td></td>');
                    row.append('<td class="error"></td>');
                } else {
                    row.append('<td class="status">Error</td>');
                    row.append('<td class="error">' + message + '</td>');
                }
            }

            function setRowsStatus(data)
            {
                for (var key in data) {
                    var item = data[key];
                    setRowStatus(item);
                }
            }

            function setRowStatus(item) {
                $('#resultRows').find(':checkbox').each(function() {
                    if ($(this).val() == item.Bid) {
                        if (item.Result) {
                            $(this).closest('tr').find('.status').text('Done');
                        } else {
                            $(this).closest('tr').find('.status').text('Error');
                            $(this).closest('tr').find('.error').text(item.Message);
                        }                        
                        $(this).closest('tr').find(':checkbox').removeAttr('checked').attr('disabled', true);
                    }
                });
            }

            function resetRowsStatus() {
                $('#resultRows').find(':checked').closest('tr').find('.status').text('Queue');
            }

            function showProcessStatus(processStatus, count) {
                if (processStatus == showProcessStatus._SHOW_PENDING_COUNT) {
                    $('#infoCell1').text(count);
                }
                if (processStatus == showProcessStatus._SHOW_WORKING_COUNT) {
                    $('#infoCell2').text(count);
                }
                if (processStatus == showProcessStatus._SHOW_FINISH_COUNT) {
                    $('#infoCell3').text(count);
                }
                if (processStatus == showProcessStatus._RESET_ALL) {
                    $('#infoCell1').text('0');
                    $('#infoCell2').text('0');
                    $('#infoCell3').text('0');
                }
            }


            //批次結檔
            $('#txtBidsClose').keyup(function() {
                if (QueryManager.GetArrayFromTextArea('txtBidsClose').length == 0) {
                    $('#btnQueryClose').attr('disabled', true);
                } else {
                    $('#btnQueryClose').removeAttr('disabled');
                }
            });

            function resetRowClose() {
                $('#noDataRowsClose').hide();
                $('#resultRowsClose').show();
                $('#resultRowsClose').find('tr').remove();
                $('#btnExecuteClose').attr('disabled', true);
            }

            function appendRowsClose(data) {
                var i = $('#resultRowsClose').find('tr').size();
                for (var key in data) {
                    var item = data[key];
                    i++;
                    appendRowClose(i, item.Bid, item.Result, item.Message);
                }
            }

            function appendRowClose(idx, bid, result, message) {
                var row = $('<tr></tr>').appendTo($('#resultRowsClose'));
                if (result) {
                    row.append('<td><input type="checkbox" checked="checked" value="' + bid +  '" /> ' + idx + '</td>');
                } else {
                    row.append('<td style="padding-left:32px">' + idx + '</td>');
                }
                row.append('<td><a href="/'+ bid+'" target="_blank">' + bid + '</a></td>');
                if (result) {
                    row.append('<td class="status">Ready</td><td></td>');
                    row.append('<td class="error"></td>');
                } else {
                    row.append('<td class="status">Error</td>');
                    row.append('<td class="error">' + message + '</td>');
                }
            }

            function setRowsStatusClose(data)
            {
                for (var key in data) {
                    var item = data[key];
                    setRowStatusClose(item);
                }
            }

            function setRowStatusClose(item) {
                $('#resultRowsClose').find(':checkbox').each(function() {
                    if ($(this).val() == item.Bid) {
                        if (item.Result) {
                            $(this).closest('tr').find('.status').text('Done');
                        } else {
                            $(this).closest('tr').find('.status').text('Error');
                            $(this).closest('tr').find('.error').text(item.Message);
                        }                        
                        $(this).closest('tr').find(':checkbox').removeAttr('checked').attr('disabled', true);
                    }
                });
            }

            function resetRowsStatusClose() {
                $('#resultRowsClose').find(':checked').closest('tr').find('.status').text('Queue');
            }

            function showProcessStatusClose(processStatus, count) {
                if (processStatus == showProcessStatus._SHOW_PENDING_COUNT) {
                    $('#infoCell1Close').text(count);
                }
                if (processStatus == showProcessStatus._SHOW_WORKING_COUNT) {
                    $('#infoCell2Close').text(count);
                }
                if (processStatus == showProcessStatus._SHOW_FINISH_COUNT) {
                    $('#infoCell3Close').text(count);
                }
                if (processStatus == showProcessStatus._RESET_ALL) {
                    $('#infoCell1Close').text('0');
                    $('#infoCell2Close').text('0');
                    $('#infoCell3Close').text('0');
                }
            }
            showProcessStatus._RESET_ALL = 4;
            showProcessStatus._SHOW_PENDING_COUNT = 1;
            showProcessStatus._SHOW_WORKING_COUNT = 2;
            showProcessStatus._SHOW_FINISH_COUNT = 3;

            //setInterval(function() {
            //    console.log(jQuery.active);
            //},500);

            function QueryManager() {
                this.workItems = null;
                this.doneItems = null;
                this.endDate = null;
                this.setData = function() {
                    var lines = $('#txtBids').val().split(/\n/);
                    this.workItems = [];
                    this.doneItems = [];
                    for (var i = 0; i < lines.length; i++) {
                        var val = $.trim(lines[i]);
                        if (val == '') {
                            continue;
                        }
                        this.workItems.push(val);
                    }
                    this.endDate = $('#txtEndDate').val();
                    showProcessStatus(showProcessStatus._SHOW_PENDING_COUNT, this.workItems.length);
                };

                //region query
                this.startQuery = function() {
                    resetRow();
                    showProcessStatus(showProcessStatus._RESET_ALL);
                    this.pagedQuery();
                }
                this.pagedQuery = function() {    
                    var bids = [];
                    while (this.workItems.length > 0) {
                        var bid = this.workItems[0];
                        bids.push(bid);
                        this.workItems.splice(0, 1);
                        if (bids.length >= ajaxPagesize) {
                            break;
                        }
                    }
                    showProcessStatus(showProcessStatus._SHOW_PENDING_COUNT, this.workItems.length);
                    showProcessStatus(showProcessStatus._SHOW_WORKING_COUNT, bids.length);
                    if (bids.length > 0) {
                        this.query(bids, this.endDate);
                    } else {
                        this.onQueryDone();
                    }
                }
                this.onQueryDone = function() {
                    $('#btnExecute').removeAttr('disabled');
                };
                this.query = function(bids, endDate) {
                    var model = {
                        bids: bids,
                        endDate: endDate
                    };
                    var self = this;
                    $.ajax({
                        type: "POST",
                        url: 'ExpandEndTime.aspx/QueryStatus',
                        data: JSON.stringify(model),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function(msg) {
                            if (msg.d != '') {
                                appendRows(msg.d.Data);
                                for (var i in msg.d.Data) {
                                    self.doneItems.push(msg.d.Data[i]);
                                }
                                showProcessStatus(showProcessStatus._SHOW_FINISH_COUNT, self.doneItems.length);
                                self.pagedQuery();
                            } else {
                                alert(msg);
                            }
                        },
                        timeout:ajaxTimeout,
                        error: function(x, y, z) {
                            alert('程式執行發生問題，網頁重整.' + y + z);
                            location.reload(true);
                        }
                    });
                };
                //endregion query

            };

            

            

            function ExecuteManager() {
                this.workItems = null;
                this.doneItems = null;
                this.endDate = null;
                this.setData = function() {
                    this.workItems = [];
                    this.doneItems = [];
                    var self = this;
                    $('#resultRows').find(':checked').each(function() {
                        self.workItems.push($(this).val());
                    });                    
                    this.endDate = $('#txtEndDate').val();
                };                
                this.startExecute = function() {
                    $('#btnQuery').attr('disabled', true);
                    $('#btnExecute').attr('disabled', true);
                    resetRowsStatus();
                    showProcessStatus(showProcessStatus._RESET_ALL);
                    this.pagedExecute();
                }
                this.pagedExecute = function() {
                    var bids = [];
                    while (this.workItems.length > 0) {
                        var bid = this.workItems[0];
                        bids.push(bid);
                        this.workItems.splice(0, 1);
                        if (bids.length >= ajaxPagesize) {
                            break;
                        }
                    }
                    showProcessStatus(showProcessStatus._SHOW_PENDING_COUNT, this.workItems.length);
                    showProcessStatus(showProcessStatus._SHOW_WORKING_COUNT, bids.length);
                    if (bids.length > 0) {
                        this.execute(bids, this.endDate);
                    } else {
                        this.onExecuteDone();
                    }
                }
                this.onExecuteDone = function() {
                    $('#btnQuery').removeAttr('disabled', true);
                };
                this.execute = function(bids, endDate) {
                    var model = {
                        bids: bids,
                        endDate: endDate
                    };
                    var self = this;
                    
                    $.ajax({
                        type: "POST",
                        url: 'ExpandEndTime.aspx/ExecuteExpand',
                        data: JSON.stringify(model),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function(msg) {
                            if (msg.d != '') {
                                setRowsStatus(msg.d.Data);
                                for (var i in msg.d.Data) {
                                    self.doneItems.push(msg.d.Data[i]);
                                }
                                showProcessStatus(showProcessStatus._SHOW_FINISH_COUNT, self.doneItems.length);
                                self.pagedExecute();
                            } else {
                                alert(msg);
                            }
                        },
                        timeout:ajaxTimeout,
                        error: function(x, y, z) {
                            alert('程式執行發生問題，網頁重整.' + y + z);
                            location.reload(true);
                        }
                    });
                }
            }

            function QueryManagerClose() {
                this.workItems = null;
                this.doneItems = null;
                this.setData = function() {
                    var lines = $('#txtBidsClose').val().split(/\n/);
                    this.workItems = [];
                    this.doneItems = [];
                    for (var i = 0; i < lines.length; i++) {
                        var val = $.trim(lines[i]);
                        if (val == '') {
                            continue;
                        }
                        this.workItems.push(val);
                    }
                    showProcessStatusClose(showProcessStatus._SHOW_PENDING_COUNT, this.workItems.length);
                };

                //region query
                this.startQuery = function() {
                    resetRowClose();
                    showProcessStatusClose(showProcessStatus._RESET_ALL);
                    this.pagedQuery();
                }
                this.pagedQuery = function() {    
                    var bids = [];
                    while (this.workItems.length > 0) {
                        var bid = this.workItems[0];
                        bids.push(bid);
                        this.workItems.splice(0, 1);
                        if (bids.length >= ajaxPagesize) {
                            break;
                        }
                    }
                    showProcessStatusClose(showProcessStatus._SHOW_PENDING_COUNT, this.workItems.length);
                    showProcessStatusClose(showProcessStatus._SHOW_WORKING_COUNT, bids.length);
                    if (bids.length > 0) {
                        this.query(bids);
                    } else {
                        this.onQueryDone();
                    }
                }
                this.onQueryDone = function() {
                    $('#btnExecuteClose').removeAttr('disabled');
                };
                this.query = function(bids) {
                    var model = {
                        bids: bids
                    };
                    var self = this;
                    $.ajax({
                        type: "POST",
                        url: 'ExpandEndTime.aspx/QueryStatusClose',
                        data: JSON.stringify(model),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function(msg) {
                            if (msg.d != '') {
                                appendRowsClose(msg.d.Data);
                                for (var i in msg.d.Data) {
                                    self.doneItems.push(msg.d.Data[i]);
                                }
                                showProcessStatusClose(showProcessStatus._SHOW_FINISH_COUNT, self.doneItems.length);
                                self.pagedQuery();
                            } else {
                                alert(msg);
                            }
                        },
                        timeout:ajaxTimeout,
                        error: function(x, y, z) {
                            alert('程式執行發生問題，網頁重整.' + y + z);
                            location.reload(true);
                        }
                    });
                };
                //endregion query

            };

            function ExecuteManagerClose() {
                this.workItems = null;
                this.doneItems = null;
                this.setData = function() {
                    this.workItems = [];
                    this.doneItems = [];
                    var self = this;
                    $('#resultRowsClose').find(':checked').each(function() {
                        self.workItems.push($(this).val());
                    });                    
                };                
                this.startExecute = function() {
                    $('#btnQueryClose').attr('disabled', true);
                    $('#btnExecuteClose').attr('disabled', true);
                    resetRowsStatusClose();
                    showProcessStatusClose(showProcessStatus._RESET_ALL);
                    this.pagedExecute();
                }
                this.pagedExecute = function() {
                    var bids = [];
                    while (this.workItems.length > 0) {
                        var bid = this.workItems[0];
                        bids.push(bid);
                        this.workItems.splice(0, 1);
                        if (bids.length >= ajaxPagesize) {
                            break;
                        }
                    }
                    showProcessStatusClose(showProcessStatus._SHOW_PENDING_COUNT, this.workItems.length);
                    showProcessStatusClose(showProcessStatus._SHOW_WORKING_COUNT, bids.length);
                    if (bids.length > 0) {
                        this.execute(bids);
                    } else {
                        this.onExecuteDone();
                    }
                }
                this.onExecuteDone = function() {
                    $('#btnQueryClose').removeAttr('disabled', true);
                };
                this.execute = function(bids) {
                    var model = {
                        bids: bids
                    };
                    var self = this;
                    
                    $.ajax({
                        type: "POST",
                        url: 'ExpandEndTime.aspx/ExecuteClose',
                        data: JSON.stringify(model),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function(msg) {
                            if (msg.d != '') {
                                setRowsStatusClose(msg.d.Data);
                                for (var i in msg.d.Data) {
                                    self.doneItems.push(msg.d.Data[i]);
                                }
                                showProcessStatusClose(showProcessStatus._SHOW_FINISH_COUNT, self.doneItems.length);
                                self.pagedExecute();
                            } else {
                                alert(msg);
                            }
                        },
                        timeout:ajaxTimeout,
                        error: function(x, y, z) {
                            alert('程式執行發生問題，網頁重整.' + y + z);
                            location.reload(true);
                        }
                    });
                }
            }

            //確認有無填資料
            QueryManager.GetArrayFromTextArea = function(textAreaId) {
                var lines = $('#' + textAreaId).val().split(/\n/);
                var result = [];
                for (var i = 0; i < lines.length; i++) {
                    var val = $.trim(lines[i]);
                    if (val == '') {
                        continue;
                    }
                    result.push(val);
                }
                return result;
            };

            var qyMgr = new QueryManager();
            var exMgr = new ExecuteManager();
            var qyMgrClose = new QueryManagerClose();
            var exMgrClose = new ExecuteManagerClose();

            $('#btnQuery').click(function () {
                qyMgr.setData();
                qyMgr.startQuery();
            });
            $('#btnExecute').click(function() {
                if (confirm('確定執行延長檔次?') == false) {
                    return;
                }
                exMgr.setData();
                exMgr.startExecute();
            });


            $('#btnQueryClose').click(function () {
                qyMgrClose.setData();
                qyMgrClose.startQuery();
            });
            $('#btnExecuteClose').click(function() {
                if (confirm('確定執行批次結檔?') == false) {
                    return;
                }
                exMgrClose.setData();
                exMgrClose.startExecute();
            });

        });

        function loadDatePicker() {
            if ($(".datepicker").length > 0) {
                $(".datepicker").datepicker({
                    dateFormat: "yy/mm/dd",
                    minDate: new Date()
                });
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div id="tabs">
        <ul>
            <li><a href="#tabs-1">延長檔次</a></li>
            <li><a href="#tabs-2">批次結檔</a></li>
        </ul>

        <div id="tabs-1">
            <div style="margin: 20px" id="divMessage" runat="server">
            </div>
            <div style="text-align: right; padding: 3px" id="info">
                待處理: <span id="infoCell1">0</span> 處理中: <span id="infoCell2">0</span> 已完成: <span id="infoCell3">0</span>
            </div>
            <fieldset style="border-color: #ccddef" class="main-content">
                <legend>延長檔次結檔時間</legend>
                <div style="border-color: #ccddef">
                    <table>
                        <tr>
                            <td>
                                日期 <input class="datepicker" type="text" id="txtEndDate" placeholder="yyyy/mm/dd" value="<%=DateTime.Now.ToString("yyyy/MM/dd") %>" />時間23:59
                                <br />
                                <textarea rows="6" cols="40" class="adminlabel" id="txtBids" placeholder="填入BID"></textarea>
                            </td>
                            <td style="vertical-align: bottom">
                                <input type="button" id="btnQuery" value="分析" disabled="disabled" class="frmbutton" style="margin-left: 20px; width: 60px" />
                                <input type="button" id="btnExecute" value="執行" disabled="disabled" class="frmbutton" style="margin-left: 20px; width: 60px" />
                            </td>
                        </tr>
                    </table>
                </div>
                <div style="border-color: #ccddef">
                    <table id="resultTable">
                        <thead>
                            <tr>
                                <th style="width: 40px"></th>
                                <th style="width: 260px">檔次BID</th>
                                <th style="width: 60px">結果</th>
                                <th style="min-width: 100px; max-width: 240px">訊息</th>
                            </tr>
                        </thead>
                        <tbody id="noDataRows">
                            <tr>
                                <td rowspan="4">沒有資料</td>
                            </tr>
                        </tbody>
                        <tbody id="resultRows" style="display: none">
                        </tbody>
                    </table>
                </div>
            </fieldset>
        </div>
        <div id="tabs-2">
            <div style="margin: 20px" id="div1" runat="server">
            </div>
            <div style="text-align: right; padding: 3px" id="info">
                待處理: <span id="infoCell1Close">0</span> 處理中: <span id="infoCell2Close">0</span> 已完成: <span id="infoCell3Close">0</span>
            </div>
            <fieldset style="border-color: #ccddef" class="main-content">
                <legend>批次結檔</legend>
                <div style="border-color: #ccddef">
                    <table>
                        <tr>
                            <td>
                                結檔時間預設為5分鐘內
                                <br />
                                <textarea rows="6" cols="40" class="adminlabel" id="txtBidsClose" placeholder="填入BID"></textarea>
                            </td>
                            <td style="vertical-align: bottom">
                                <input type="button" id="btnQueryClose" value="分析" disabled="disabled" class="frmbutton" style="margin-left: 20px; width: 60px" />
                                <input type="button" id="btnExecuteClose" value="執行" disabled="disabled" class="frmbutton" style="margin-left: 20px; width: 60px" />
                            </td>
                        </tr>
                    </table>
                </div>
                <div style="border-color: #ccddef">
                    <table id="resultTable">
                        <thead>
                            <tr>
                                <th style="width: 40px"></th>
                                <th style="width: 260px">檔次BID</th>
                                <th style="width: 60px">結果</th>
                                <th style="min-width: 100px; max-width: 240px">訊息</th>
                            </tr>
                        </thead>
                        <tbody id="noDataRowsClose">
                            <tr>
                                <td rowspan="4">沒有資料</td>
                            </tr>
                        </tbody>
                        <tbody id="resultRowsClose" style="display: none">
                        </tbody>
                    </table>
                </div>
            </fieldset>
        </div>

    </div>
</asp:Content>

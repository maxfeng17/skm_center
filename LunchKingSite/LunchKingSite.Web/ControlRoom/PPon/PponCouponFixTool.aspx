﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ControlRoom/backend.master" AutoEventWireup="true"
    CodeBehind="PponCouponFixTool.aspx.cs" Inherits="LunchKingSite.Web.ControlRoom.Ppon.PponCouponFixTool" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table>
        <tr>
            <td>
                本功能需技術部使用
            </td>
        </tr>
        <tr>
            <td>
                <asp:TextBox ID="TextBox1" runat="server" TextMode="MultiLine" Rows="5" Height="100" Width="350"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:CheckBox runat="server" ID="FamiGroupCoupon" Checked="false" Text="全家寄杯檔次" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="Button1" runat="server" Text="Fix" OnClick="Button1_Click" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
            </td>
        </tr>
    </table>
	<br />
    <br />
    <asp:Button ID="FixAllRecentData" runat="server" Text="修復最近沒產的coupon" OnClick="FixAllRecentData_Click" />

    <cc1:TextBoxWatermarkExtender ID="tbw_OnlineStore" runat="server" TargetControlID="TextBox1"
        WatermarkCssClass="watermarked" WatermarkText="OrderDetailID逗號或空行" />
</asp:Content>

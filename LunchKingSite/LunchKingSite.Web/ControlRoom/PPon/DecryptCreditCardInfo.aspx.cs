﻿using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.UI;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using System.Linq;

namespace LunchKingSite.Web.ControlRoom.Ppon
{
    public partial class DecryptCreditCardInfo :RolePage
    {
        private static IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();

        #region Props
        public string CreditCardInfo
        {
            get { return txtCreditCardInfo.Text; }
        }

        public string Salt
        {
            get { return txtSalt.Text; }
        }

        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
           var mccList= mp.MemberCreditCardGetBinNullList();

            if(!mccList.Any())
            {
                fs2.Visible = false;
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            string result = string.Empty;
            try
            {
                result = MemberFacade.DecryptCreditCardInfo(CreditCardInfo, Salt);
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }
            Result.Text = result;
        }



        protected void btnDecrypt_Click(object sender, EventArgs e)
        {
            string bin = string.Empty;
            string result = string.Empty;

            var mccList = mp.MemberCreditCardGetBinNullList().ToList();

            foreach (MemberCreditCard item in mccList)
            {
                try
                {
                    result = MemberFacade.DecryptCreditCardInfo(item.CreditCardInfo, item.EncryptSalt);

                    item.Bin = result.Substring(0, 6);

                    mp.MemberCreditCardSet(item);
                }
                catch (Exception)
                {
                    item.Bin = "解析失敗";
                    mp.MemberCreditCardSet(item);
                }

            }

            fs2.Visible = false;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using log4net;
using LunchKingSite.Core.UI;
using LunchKingSite.WebLib.Views;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.ModelCustom;

namespace LunchKingSite.Web.ControlRoom.Ppon
{
    public partial class PushAppSetup : RolePage, IPushAppSetupView
    {
        protected static ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        private static INotificationProvider np = ProviderFactory.Instance().GetProvider<INotificationProvider>();
        private static ILog logger = LogManager.GetLogger(typeof(NotificationFacade));

        #region props
        private PushAppSetupPresenter _presenter;
        public PushAppSetupPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        public int PushId
        {
            get
            {
                int id = int.TryParse(hidPushId.Value, out id) ? id : 0;
                return id;
            }
            set { hidPushId.Value = value.ToString(); }
        }

        public Guid? BusinessHourGuid
        {
            get
            {
                if (rdPushAppBid.Checked)
                {
                    Guid bid = Guid.TryParse(txtBid.Text.Trim(), out bid) ? bid : Guid.Empty;
                    return bid;
                }
                else
                    return null;
            }
            set
            {
                if (value != null)
                {
                    txtBid.Text = value.ToString();
                    rdPushAppBid.Checked = true;
                }
                else
                    rdPushAppBid.Checked = false;
            }
        }

        public int? PponCity
        {
            get
            {
                if (rdPushAppPponCity.Checked)
                {
                    int city = int.TryParse(rdlPponCityGroup.SelectedValue, out city) ? city : PponCityGroup.DefaultPponCityGroup.AllCountry.CityId;
                    return city;
                }
                else
                    return null;
            }
            set
            {
                if (value != null && rdlPponCityGroup.Items.FindByValue(value.ToString()) != null)
                {
                    rdlPponCityGroup.SelectedValue = value.ToString();
                    rdPushAppPponCity.Checked = true;
                }
                else
                    rdPushAppPponCity.Checked = false;
            }
        }

        public string SellerId
        {
            get
            {
                if (rdPushAppSellerId.Checked)
                {
                    return txtSellerId.Text.Trim();
                }
                else
                    return string.Empty;
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    txtSellerId.Text = value;
                    rdPushAppSellerId.Checked = true;
                }
                else
                    rdPushAppSellerId.Checked = false;
            }
        }

        public int? VourcherId
        {
            get
            {
                if (rdPushAppVourcherId.Checked)
                {
                    int id = int.TryParse(txtVourcherId.Text.Trim(), out id) ? id : 0;
                    return id;
                }
                else
                    return null;
            }
            set
            {
                if (value != null)
                {
                    txtVourcherId.Text = value.ToString();
                    rdPushAppVourcherId.Checked = true;
                }
                else
                    rdPushAppVourcherId.Checked = false;
            }
        }

        public int? EventPromoId
        {
            get
            {
                if (rdPushAppEventPromo.Checked)
                {
                    int id = int.TryParse(txtEventPromoId.Text.Trim(), out id) ? id : 0;
                    return id;
                }
                else
                    return null;
            }
            set
            {
                if (value != null)
                {
                    txtEventPromoId.Text = value.ToString();
                    rdPushAppEventPromo.Checked = true;
                }
                else
                    rdPushAppEventPromo.Checked = false;
            }
        }

        public int? BrandPromoId
        {
            get
            {
                if (rdPushAppBrandPromo.Checked)
                {
                    int id = int.TryParse(txtBrandPromoId.Text.Trim(), out id) ? id : 0;
                    return id;
                }
                else
                    return null;
            }
            set
            {
                if (value != null)
                {
                    txtBrandPromoId.Text = value.ToString();
                    rdPushAppBrandPromo.Checked = true;
                }
                else
                    rdPushAppBrandPromo.Checked = false;
            }
        }

        public string CustomUrl
        {
            get
            {
                if (rdPushAppCustomUrl.Checked)
                {
                    return txtCustomUrl.Text;

                }
                else
                {
                    return null;
                }

            }
            set
            {
                if (value != null)
                {
                    txtCustomUrl.Text = value.ToString();
                    rdPushAppCustomUrl.Checked = true;
                }
                else
                {
                    rdPushAppCustomUrl.Checked = false;
                }
            }
        }

        public DateTime SendDate
        {
            get
            {
                DateTime date = DateTime.TryParse(txtSendDate.Text, out date) ? date : DateTime.Now;
                int hour = int.TryParse(ddlSendTimeHour.SelectedValue, out hour) ? hour : 12;
                int minute = int.TryParse(ddlSendTimeMinute.SelectedValue, out minute) ? minute : 0;
                return date.SetTime(hour, minute, 0);
            }
            set
            {
                txtSendDate.Text = value.ToString("yyyy/MM/dd");
                if (ddlSendTimeHour.Items.FindByValue(value.Hour.ToString()) != null)
                    ddlSendTimeHour.SelectedValue = value.Hour.ToString();
                if (ddlSendTimeMinute.Items.FindByValue(value.Minute.ToString()) != null)
                    ddlSendTimeMinute.SelectedValue = value.Minute.ToString();
            }
        }

        public DateTime SendDateSearchS
        {
            get
            {
                DateTime date;
                DateTime.TryParse(txtSendDateSearchS.Text, out date);
                return date;
            }
        }

        public DateTime SendDateSearchE
        {
            get
            {
                DateTime date;
                DateTime.TryParse(txtSendDateSearchE.Text, out date);
                return date;
            }
        }

        public int PageSize
        {
            get { return ucPager.PageSize; }
            set { ucPager.PageSize = value; }
        }

        public string PushArea
        {
            get
            {
                string separator = ",";
                return string.Join(separator, chklPushArea.Items.Cast<ListItem>().Where(item => item.Selected).Select(item => item.Value));
            }
            set
            {
                for (int i = 0; i < chklPushArea.Items.Count; i++)
                {
                    if (value != null)
                        chklPushArea.Items[i].Selected = value.Split(',').Contains(chklPushArea.Items[i].Value);
                }
            }
        }

        public PushAppType PushType
        {
            get
            {
                PushAppType type = PushAppType.TryParse(rdlPushAppType.SelectedValue, out type) ? type : PushAppType.PponDeal;
                return type;
            }
            set
            {
                if (rdlPushAppType.Items.FindByValue(((int)value).ToString()) != null)
                    rdlPushAppType.SelectedValue = ((int)value).ToString();
            }
        }

        public int PushTypeSearch
        {
            get
            {
                int type = int.TryParse(rdlPushTypeSearch.SelectedValue, out type) ? type : -1;
                return type;
            }
        }

        public string Description
        {
            get { return txtDescription.Text.Replace("\"", "").Replace("'", "").Trim(); }
            set { txtDescription.Text = value; }
        }

        public string DescriptionSearch
        {
            get { return txtDescriptionSearch.Text.Trim(); }
        }

        private bool _isEnabledEdit;
        public bool IsEnabledEdit
        {
            get
            {
                return _isEnabledEdit;
            }
            set
            {
                _isEnabledEdit = value;
                btnSave.Visible = value;
                btnDelete.Visible = value;
            }
        }

        private bool _isEnabledUpdate;
        public bool IsEnabledUpdate
        {
            get { return _isEnabledUpdate; }
            set
            {
                _isEnabledUpdate = value;
                //btnUpdate.Visible = value;  先隱藏，有空再繼續
            }
        }

        public string UserName
        {
            get
            {
                return User.Identity.Name;
            }
        }

        public string PushToAndroid
        {
            get
            {
                return "Android";
            }
        }

        public string PushToIOS
        {
            get
            {
                return "IOS";
            }
        }
        #endregion

        #region event
        public event EventHandler OnSearchClicked;
        public event EventHandler OnSaveClicked;
        public event EventHandler<DataEventArgs<int>> GetPushAppData;
        public event EventHandler<DataEventArgs<int>> Push;
        public event EventHandler<DataEventArgs<int>> PushAndroid;
        public event EventHandler<DataEventArgs<int>> PushIOS;
        public event EventHandler<DataEventArgs<int>> PushAppDelete;
        public event EventHandler<DataEventArgs<int>> OnGetCount;
        public event EventHandler<DataEventArgs<int>> PageChanged;
        #endregion

        #region method
        public void SetPushAppList(PushAppCollection dataList)
        {
            gvPushAppList.DataSource = dataList;
            gvPushAppList.DataBind();
        }

        public void ShowMessage(string msg)
        {
            ScriptManager.RegisterStartupScript(divPushAppAdd, this.GetType(), "alert", "alert('" + msg + "');", true);
        }

        public void ShowPushList()
        {
            ucPager.ResolvePagerView(1, true);

            divPushAppAdd.Visible = false;
            divPushAppSearch.Visible = true;
            Response.Redirect("~/controlroom/Ppon/PushAppSetup.aspx");
        }

        #endregion

        #region page
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                InitailControl();
                Presenter.OnViewInitialized();
            }
            _presenter.OnViewLoaded();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            if (this.OnSearchClicked != null)
                this.OnSearchClicked(this, e);
            ucPager.ResolvePagerView(1, true);
            divPushAppAdd.Visible = false;
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            divPushAppAdd.Visible = true;
            divPushAppSearch.Visible = false;
            hidPushId.Value = string.Empty;
            txtSendDate.Text = string.Empty;
            ddlSendTimeHour.SelectedIndex = 12;
            ddlSendTimeMinute.SelectedIndex = 0;
            for (int i = 0; i < chklPushArea.Items.Count; i++)
            {
                chklPushArea.Items[i].Selected = false;
            }
            rdlPushAppType.SelectedIndex = 0;
            txtBid.Text = string.Empty;
            rdlPponCityGroup.SelectedIndex = 0;
            txtSellerId.Text = string.Empty;
            txtVourcherId.Text = string.Empty;
            txtDescription.Text = string.Empty;
            IsEnabledEdit = true;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (this.OnSaveClicked != null)
            { 
                this.OnSaveClicked(this, e);
            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            if (this.PushAppDelete != null)
            { 
                this.PushAppDelete(this, new DataEventArgs<int>(PushId));
            }
            divPushAppAdd.Visible = false;
            divPushAppSearch.Visible = true;
            Response.Redirect("~/controlroom/Ppon/PushAppSetup.aspx");
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            //TODO: 更新 action_event_push_message，可直接調整訊息中心內容
            //考慮新增推播後的編輯頁內容是否要直接從這裡讀
            //推完後真正讀取訊息是從action_event_push_message，而非push_app
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            divPushAppAdd.Visible = false;
            divPushAppSearch.Visible = true;
        }

        protected void gvPushAppList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                PushApp push = (PushApp)e.Row.DataItem;
                Label lblPushArea = ((Label)e.Row.FindControl("lblPushArea"));
                Label lblPushType = ((Label)e.Row.FindControl("lblPushType"));
                Label lblPushItem = ((Label)e.Row.FindControl("lblPushItem"));
                Label lblRealPushTime = ((Label)e.Row.FindControl("lblRealPushTime"));
                Label lblSubscribeCount = ((Label)e.Row.FindControl("lblSubscribeCount"));
                Label lblIosPush = ((Label)e.Row.FindControl("lblIosPush"));
                Label lblAndroidPush = ((Label)e.Row.FindControl("lblAndroidPush"));

                Label lblOrderCount = ((Label)e.Row.FindControl("lblOrderCount"));
                Label lblTurnover = ((Label)e.Row.FindControl("lblTurnover"));
                Label lblConversionRate = ((Label)e.Row.FindControl("lblConversionRate"));

                if (push.OrderCount > 0)
                {
                    lblOrderCount.Text = ((decimal)push.OrderCount).ToString("#,0");
                }
                if (push.Turnover > 0)
                {
                    lblTurnover.Text = push.Turnover.ToString("#,0");
                }
                if (push.AndriodViewCount + push.IosViewCount > 0 && push.OrderCount > 0)
                { 
                    lblConversionRate.Text = (push.OrderCount / (decimal)(push.AndriodViewCount + push.IosViewCount)).ToString("0.00") + "%";
                }

                LinkButton lkEdit = ((LinkButton)e.Row.FindControl("lkEdit"));
                Button btnPush = ((Button)e.Row.FindControl("btnPush"));
                PlaceHolder phPushTest = ((PlaceHolder)e.Row.FindControl("phPushTest"));

                string area = string.Empty;
                foreach (string areaId in push.PushArea.Split(','))
                {
                    int cityId = int.TryParse(areaId, out cityId) ? cityId : PponCityGroup.DefaultPponCityGroup.AllCountry.CityId;
                    area += PponCityGroup.DefaultPponCityGroup.GetPponCityByCityId(cityId).CityName + "/";
                }
                lblPushArea.Text = area.TrimEnd('/');
                lblPushType.Text = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, (PushAppType)push.PushType);
                if (!push.BusinessHourGuid.Equals(Guid.Empty))
                {
                    lblPushItem.Text = "檔次：" + push.BusinessHourGuid.ToString();
                }
                if (push.CityId != null)
                {
                    lblPushItem.Text = "頻道：" +
                                       PponCityGroup.DefaultPponCityGroup.GetPponCityByCityId(push.CityId.Value)
                                                    .CityName;
                }
                if (push.VourcherId != null)
                {
                    lblPushItem.Text = "優惠券編號：" + push.VourcherId;
                }
                if (!string.IsNullOrEmpty(push.SellerId))
                {
                    lblPushItem.Text = "賣家編號：" + push.SellerId;
                }
                if (push.EventPromoId.HasValue)
                {
                    lblPushItem.Text = "商品主題活動編號：" + push.EventPromoId;
                }
                if (!string.IsNullOrEmpty(push.CustomUrl))
                {
                    lblPushItem.Text = "自訂URL：" + push.CustomUrl;
                }
                if(push.BrandPromoId.HasValue)
                {
                    lblPushItem.Text = "策展2.0活動編號：" + push.BrandPromoId;
                }
                lkEdit.Text = push.Description;
                if (push.RealPushTime != null)
                {
                    lblRealPushTime.Text = push.RealPushTime.Value.ToString("yyyy-MM-dd HH:mm");
                    e.Row.BackColor = System.Drawing.Color.FromArgb(0xcc, 0xcc, 0xcc);
                    
                }
                if (config.AppNotificationJob == false || push.RealPushTime != null)
                {
                    btnPush.Visible = false;
                    phPushTest.Visible = false;
                }

                if (push.SubscribeCount != 0)
                {
                    lblSubscribeCount.Text = string.Format("{0}<br />({1:0.00%})",
                        push.SubscribeCount,
                        (double)(push.IosViewCount + push.AndriodViewCount) / push.SubscribeCount);
                }
                if (push.IosPushCount != 0)
                {
                    lblIosPush.Text = push.IosPushCount + "<br />(" + string.Format("{0:0.00%}", (double)push.IosViewCount / push.IosPushCount) + ")";
                }
                if (push.AndriodPushCount != 0)
                {
                    lblAndroidPush.Text = push.AndriodPushCount + "<br />(" + string.Format("{0:0.00%}", (double)push.AndriodViewCount / push.AndriodPushCount) + ")";
                }
            }
        }

        protected void gvPushAppList_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            int id;
            int.TryParse(e.CommandArgument.ToString(), out id);
            switch (e.CommandName.ToString())
            {
                case "UPD":
                    if (this.GetPushAppData != null)
                        this.GetPushAppData(this, new DataEventArgs<int>(id));
                    divPushAppAdd.Visible = true;
                    divPushAppSearch.Visible = false;
                    break;
                case "PUSH":
                    if (this.Push != null)
                        this.Push(this, new DataEventArgs<int>(id));
                    break;
                case "PUSH_ANDROID":
                    if (this.PushAndroid != null)
                        this.PushAndroid(this, new DataEventArgs<int>(id));
                    break;
                case "PUSH_IOS":
                    if (this.PushIOS != null)
                        this.PushIOS(this, new DataEventArgs<int>(id));
                    break;
            }
        }

        protected void UpdateHandler(int pageNumber)
        {
            DataEventArgs<int> e = new DataEventArgs<int>(pageNumber);
            if (this.PageChanged != null)
                this.PageChanged(this, e);
        }

        protected int GetCount()
        {
            DataEventArgs<int> e = new DataEventArgs<int>(0);
            if (OnGetCount != null)
                OnGetCount(this, e);
            return e.Data;
        }
        #endregion

        #region private method
        private void InitailControl()
        {
            ddlSendTimeHour.Items.Clear();
            for (int i = 0; i < 24; i++)
            {
                ddlSendTimeHour.Items.Add(new ListItem(i.ToString("00"), i.ToString()));
            }
            
            Dictionary<int, string> pushTypeList = new Dictionary<int, string>();
            foreach (var item in Enum.GetValues(typeof(PushAppType)))
            {
                pushTypeList[(int)item] = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, (PushAppType)item);
            }

            rdlPushTypeSearch.DataSource = pushTypeList;
            rdlPushTypeSearch.DataTextField = "Value";
            rdlPushTypeSearch.DataValueField = "Key";
            rdlPushTypeSearch.DataBind();
            rdlPushTypeSearch.Items.Insert(0, new ListItem("全部", "-1"));
            rdlPushTypeSearch.SelectedIndex = 0;

            rdlPushAppType.DataSource = pushTypeList;
            rdlPushAppType.DataTextField = "Value";
            rdlPushAppType.DataValueField = "Key";
            rdlPushAppType.DataBind();
            rdlPushAppType.SelectedIndex = 0;

            chklPushArea.DataSource = PponCityGroup.DefaultPponCityGroup.GetPponcityForSubscription().Select(x => new { Text = x.CityName, Value = x.CityId });
            chklPushArea.DataTextField = "Text";
            chklPushArea.DataValueField = "Value";
            chklPushArea.DataBind();

            rdlPponCityGroup.DataSource = PponCityGroup.DefaultPponCityGroup.GetPponCityForSetting().Except(new[] { PponCityGroup.DefaultPponCityGroup.PEZ, PponCityGroup.DefaultPponCityGroup.Tmall }).Select(x => new { Text = x.CityName, Value = x.CityId }); ;
            rdlPponCityGroup.DataTextField = "Text";
            rdlPponCityGroup.DataValueField = "Value";
            rdlPponCityGroup.DataBind();
        }
        #endregion


        #region test push

        /// <summary>
        /// 試推
        /// </summary>
        /// <param name="pushId"></param>
        /// <param name="tokens"></param>
        /// <returns></returns>
        [WebMethod]
        public static string PushAppAsync(int pushId, List<string> tokens)
        {
            IPrincipal user = HttpContext.Current.User;
            if (user.Identity.IsAuthenticated == false)
            {
                return "未登入";
            }
            DeviceTokenCollection deviceTokens;
            if (tokens != null)
            {
                deviceTokens = new DeviceTokenCollection();
                foreach (string token in tokens)
                {
                    if (token.Length < 64)
                    {
                        continue;
                    }
                    DeviceToken dt = new DeviceToken
                    {
                        Token = token,
                    };
                    if (token.Length == 64)
                    {
                        dt.MobileOsType = (int)MobileOsType.iOS;
                    }
                    else if (token.Length > 64)
                    {
                        dt.MobileOsType = (int)MobileOsType.Android;
                    }
                    deviceTokens.Add(dt);
                }
                if (deviceTokens.Count > 0)
                {
                    logger.InfoFormat("推播測試. 操作者={0}, pushId={1}, 對象={2}",
                        user.Identity.Name, 
                        pushId,
                        string.Join(",", deviceTokens.Select(t => t.Token).ToArray()));
                    NotificationFacade.PushAppSchedulMessage(pushId, true, deviceTokens);
                    return string.Format("推播完成, {0} 個裝置", deviceTokens.Count);
                }
            }
            return "未指定裝置";
        }

        [WebMethod]
        public static List<UserDeviceInfoModel> QueryDevices(string account)
        {
            IPrincipal user = HttpContext.Current.User;
            if (user.Identity.IsAuthenticated == false)
            {
                return null;
            }
            int userId;           
            Member mem;
            if (String.IsNullOrEmpty(account))
            {
                mem = MemberFacade.GetMember(user.Identity.Name);
            }
            else if (int.TryParse(account, out userId) && RegExRules.CheckMobile(account) == false)
            {
                mem = MemberFacade.GetMember(userId);
            }
            else
            {
                mem = MemberFacade.GetMember(account);
            }
            if (mem.IsLoaded == false)
            {
                return null;
            }
            List<UserDeviceInfoModel> result = np.GetUserDeviceInfoModels(mem.UniqueId);
            return result;
        }

        #endregion
    }
}
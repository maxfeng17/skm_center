﻿<%@ Page Language="C#" MasterPageFile="~/ControlRoom/backend.master" AutoEventWireup="true"
    CodeBehind="Setup.aspx.cs" Inherits="LunchKingSite.Web.ControlRoom.Ppon.Setup" %>

<%@ Import Namespace="LunchKingSite.BizLogic.Component" %>
<%@ Import Namespace="LunchKingSite.BizLogic.Facade" %>
<%@ Import Namespace="LunchKingSite.Core" %>
<%@ Import Namespace="LunchKingSite.DataOrm" %>
<%@ Import Namespace="LunchKingSite.WebLib.Views" %>
<asp:Content ID="S" ContentPlaceHolderID="SSC" runat="server">
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="expires" content="Fri, Jun 12 1981 08:20:00 GM">
    <meta http-equiv="Cache-Control" content="no-cache, must-revalidate">
    <%--<script type="text/javascript" src="../../Tools/js/jquery-1.7.2.min.js"></script>--%>
    <script type="text/javascript" src="../../Tools/js/jquery-ui.1.8.min.js"></script>
    <script type="text/javascript" src="../../Tools/ckeditor/jquery.query-2.1.7.js"></script>
    <script type="text/javascript" src="../../Tools/js/jquery.maxlength-min.js"></script>
    <script type="text/javascript" src="../../Tools/js/jquery.ui.datepicker-zh-TW.js"></script>
    <script type="text/javascript" src="../../Tools/js/jquery.blockUI.js"></script>
    <script type="text/javascript" src="/Themes/OWriteOff/plugin/bpopup/jquery.bpopup.js"></script>
    <link type="text/css" href="../../Tools/js/css/ui-lightness/jquery-ui-1.8rc3.custom.css" rel="stylesheet" />
    <link href="../../Themes/PCweb/css/ppon.css" type="text/css" rel="Stylesheet" />
    <link href="../../Themes/PCweb/plugin/font-awesome/font-awesome.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../Tools/ckeditor/ckeditor.js"></script>
    <script type="text/javascript" src="../../Tools/js/json2.js"></script>
    <script type="text/javascript" src="../../Tools/js/jquery.numeric.js"></script>
    <script type="text/javascript" src="../../Tools/js/jquery.checkboxtree.js"></script>
    <script type="text/javascript" src="//html2canvas.hertzen.com/build/html2canvas.js"></script>
    <script type="text/javascript" src="../../Tools/js/ppon/pponSetup.js"></script>
    <link type="text/css" href="../../Tools/js/jquery.checkboxtree.css" rel="stylesheet" />
    <style type="text/css">
        .drag-drop-upload {
            position: relative;
            border: 2px dashed #a89f9f;
        }
        .drag-drop-upload-hover {
            border-color: #4D4D4D;
        }
        .drag-drop-upload-ok {
            border-color: transparent;
        }
        .drag-drop-upload-ok p {
            visibility: hidden;
        }
        .tab-option label {
            font-size: small;
        }
    </style>
    <script type="text/javascript">

        $(document).ready(function() {
            $('[id*=cbl_GroupCouponAppStyle] input[type="checkbox"]').on('click' , function(){
                // Caching all the checkboxes into a variable
                var checkboxes =  $('[id*=cbl_GroupCouponAppStyle] input[type="checkbox"]');
                // If one item is checked.. Uncheck all and
                // check current item..
                if($(this).is(':checked')){
                    checkboxes.attr('checked', false);
                    $(this).attr('checked', 'checked');        
                }
            });

            $("#txtCChannelLink").keyup(function () {
                if ($.trim($(this).val()) != "") {
                    $("#cbx_CChannel").prop("checked", true);
                } else {
                    $("#cbx_CChannel").prop("checked", false);
                }
            });

            
            if("<%=MuneUpdImage%>" != ""){
                $("#fileMuneUpdImage").html("<img src='<%=MuneUpdImage%>?dummy=" + guid() + "'/>");
            }

            if("<%=MuneImage%>" != ""){
                $("#menuPreContent").html("<img src='<%=MuneImage%>?dummy=" + guid() + "' />");
            }
                        
            $('#<%=rblDiscountType.ClientID %>').click(function(){
                var index = $("input[id^='<% =rblDiscountType.ClientID %>']:checked").val();
                $("input[id^='txtDiscount'][tabindex="+ index +"]").focus();
                $("input[id^='txtDiscount']:not(:focus)").val('');
            });

            $("input[id^='txtDiscount']").click(function(){
                var value = $("input[id^='txtDiscount']:focus").prop('tabindex');
                $('#<%=rblDiscountType.ClientID %>').find('input[value='+ value +']:radio').attr('checked','checked');
            });

            $('#<%=cbx_GroupCoupon.ClientID%>').click(function(){
                var allowed = $('#<%= cbx_LowGrossMarginAllowedDiscount.ClientID %>');
                var notAllowed = $('#<%= cbx_NotAllowedDiscount.ClientID %>');
                if($(this).attr('checked') == 'checked'){
                    notAllowed.attr('checked',true);
                    allowed.removeAttr('checked');
                    allowed.prop('disabled', 'disabled');
                    SetGroupCouponDealAppStyle();
                }else{
                    notAllowed.attr('checked', false);
                    var hasPrivilege = '<%=HasUseDiscountSetPrivilege%>' == 'True' ? true : false;
                    if (hasPrivilege){
                        allowed.removeAttr('disabled');
                    }
                }
            });

            var drag = {
                elem: null,
                x: 0,
                y: 0,
                state: false
            };

            $('.drag-drop-upload').on('dragenter', function(e) {
                e.stopPropagation();
                e.preventDefault();
                console.log('drag-drop enter.');
                $(this).addClass('drag-drop-upload-hover'); 
            });
            $('.drag-drop-upload').on('dragleave', function(e) {
                e.stopPropagation();
                console.log('drag-drop leave.');
                $(this).removeClass('drag-drop-upload-hover');

            });

            $('.drag-drop-upload').on('dragover', function(e) {
                e.stopPropagation();
                e.preventDefault();
            });

            $('.drag-drop-upload').on('drop', function(e) {
                e.preventDefault();
                console.log('drag-drop drop.');
                var files = e.originalEvent.dataTransfer.files;

                if (files.length > 0) {
                    previewImageFile(files[0], this);
                    handleFiles(files, this);
                }
            });
            $('.drag-drop-upload-container :file').change(function() {
                previewImageFile(this.files[0], $(this).closest('.drag-drop-upload'));
                handleFiles(this.files, $(this).closest('.drag-drop-upload'));
            });

            if($("#chk_dealMain").attr('checked')){
                $('#mu').css('display', '');            
            }else{
                $('#mu').css('display', 'none'); 
            }

            function handleFiles(files,holder)
            {
                for (var i = 0; i < files.length; i++) 
                {
                    var fd = new FormData();
                    fd.append('upload', files[i]); 
                    console.log('try send file');
                    sendFileToServer(fd, holder); 
                }
            }

            function sendFileToServer(formData, holder)
            {
                var request = new XMLHttpRequest();
                request.onload = function() {
                    if (request.status == 200) {
                        var res = $.parseJSON(request.responseText);                        
                        console.log('ok: got file. id is ' + res[0].FileId);
                        $('#hidAppDealPic').val(res[0].FileId);
                    } else {
                        alert('發生錯誤. 找 IT 處理');
                    }
                }

                request.open("POST", "UploadFile.aspx");
                request.send(formData);
            }
            
            function showImage(targetSrc, holder, opacity) {
                //opacity = opacity | 0.2;
				opacity = 100;

                var image = new Image();

                $(image).attr('src', targetSrc);


                $(holder).addClass('drag-drop-upload-ok');

                $(image).load(function(){
                    var w = $(this).prop('naturalWidth');
                    var h = $(this).prop('naturalHeight');
					if (w != h || w > 1000) {
						alert('上傳正方圖，長寬必需要一致，且不應該超過1000px，超過自動栽切');
					}
					if ($('#canvasImgLink').length == 0) {
						if (targetSrc.indexOf('data:image') == -1) {
							$('<a />').attr('id', 'canvasImgLink')
								.attr('target', '_blank')
								.css('display', 'block')
								.css('position', 'absolute')
								.css('top', '0px')
								.attr('href', targetSrc).text(targetSrc)
								.appendTo($('.drag-drop-upload'));
						}
					}
					$('<canvas></canvas>').css('position', 'absolute')
						.css('top', '20px')
						.attr('height', '1000')
						.attr('width', '1000')
						.attr('id', 'drawing')
						.appendTo($('.drag-drop-upload'));
					window.setTimeout(function () {
						var drawing = document.getElementById("drawing");
						var ctx = drawing.getContext("2d");
                        ctx.drawImage(image, 0, 0, 1000, 1000, 0, 0, 1000, 1000);
					}, 50);

                });          
                //建立刪除按鈕
                var btnDelete = $('<button></button>').html('<i class="fa fa-times"></i>').attr('type','button');
                $(holder).append(btnDelete);
				$(btnDelete).css('position', 'absolute').addClass('ddu-edit')
					.css('top', '0px').css('right', '0px').css('cursor', 'pointer');
                $(btnDelete).click(function() {
                    if (confirm('確定刪除？')) {
                        deleteDragDropUploadElemtns(holder);
                        $(holder).parents('.drag-drop-upload-container').find(':file').val('');
                    }
                });
            }

            function deleteDragDropUploadElemtns(holder) {
				$(holder).find('.ddu-edit').remove();
				$(holder).find('canvas').remove();
				$('#canvasImgLink').remove();
                $('#hidAppDealPic').val('');
                $(holder).removeClass('drag-drop-upload-ok');                
            }

            function previewImageFile(file, holder) {
                deleteDragDropUploadElemtns(holder);
                var reader = new FileReader();
                reader.onload = function (event) {
                    console.log('previewImageFile');
                    showImage(event.target.result, holder);
                };

                reader.readAsDataURL(file);
            }

            if ($('#hidAppDealPicOld').val() != "") {
                console.log('load default and previewImageFile');
                showImage($('#hidAppDealPicOld').val(), $('#appDealPicDiv'), 1);
            }
            
            $(".checkStore > :checkbox").click(function (){
                if (!$(this).is(':checked')) {
                    $('#checkAllStore').removeAttr("checked");
                }
            });
            $(".checkVerify > :checkbox").click(function (){
                if (!$(this).is(':checked')) {
                    $('#checkAllVerify').removeAttr("checked");
                }
            });
            $(".checkAccounting > :checkbox").click(function (){
                if (!$(this).is(':checked')) {
                    $('#checkAllAccounting').removeAttr("checked");
                }
                else {                    
                    var index = $(".checkAccounting > :checkbox").index(this);
                    var sid = $('#hdSellerGuid').val();
                    var storeGuid = $(':input[id$=hidSellerGuid]:eq(' + index +')').val();
                    var isToHouse = $('select[id$=ddlRreceiveType]').val() == '<%=(int)DeliveryType.ToHouse %>';
                    if (isToHouse && sid != storeGuid) {
                        alert("宅配檔次限制[匯款對象]只能設定為檔次賣家，若有疑慮請確認是否將檔次建錯賣家");
                        return false;
                    }
                }
            });
            $(".checkViewBalanceSheet > :checkbox").click(function (){
                if (!$(this).is(':checked')) {
                    $('#checkAllViewBalanceSheet').removeAttr("checked");
                    var index = $(".checkViewBalanceSheet > :checkbox").index(this);
                    if ($(':hidden[id$=hidIsHideBalanceSheet]:eq(' + index +')').val() === "True") {
                        $(':hidden[id$=hidIsHideBalanceSheet]:eq(' + index +')').val('False');
                        var src = $('.hideBalanceSheet:eq(' + index +')').attr("src");                
                        src = src.replace("lock", "unlock");
                        $('.hideBalanceSheet:eq(' + index +')').attr("src", src);
                    }
                }
            });

            $(".hideBalanceSheet").click(function() {
                var src = $(this).attr("src");
                var index = $(".hideBalanceSheet").index(this);
                if ($('.checkViewBalanceSheet > :checkbox:eq(' + index +')').is(':disabled') ||
                    src.indexOf('disabled') > 0) {
                    return false;
                }
                if (!$('.checkViewBalanceSheet > :checkbox:eq(' + index +')').is(':checked')) {
                    alert("僅限已勾選[檢視對帳單]權限之賣家使用");
                    return false;
                }
                if ($(':hidden[id$=hidIsHideBalanceSheet]:eq(' + index +')').val() === "True") {
                    $(':hidden[id$=hidIsHideBalanceSheet]:eq(' + index +')').val('False');
                    src = src.replace("lock", "unlock");
                }
                else {
                    $(':hidden[id$=hidIsHideBalanceSheet]:eq(' + index +')').val('True');
                    src = src.replace("unlock", "lock");
                }

                $(this).attr("src", src);
            });
            
            $(".checkVerifyShop > :checkbox").click(function (){
                if ($(this).is(':checked')) {
                    var isToHouse = $('select[id$=ddlRreceiveType]').val() == '<%=(int)DeliveryType.ToHouse %>';
                    if (isToHouse || $('input[id$=cbx_NoRestrictedStore]').is(':checked')) {
                        alert("宅配檔次/通用券檔次無須設定[銷售店鋪]賣家");
                        return false;
                    }
                }
                else {
                    $('#checkAllVerifyShop').removeAttr("checked");
                }
            });

            $(".hideVerifyShop > :checkbox").click(function() {
                if ($(this).is(':checked')) {
                    var isToHouse = $('select[id$=ddlRreceiveType]').val() == '<%=(int)DeliveryType.ToHouse %>';
                    if (isToHouse || $('input[id$=cbx_NoRestrictedStore]').is(':checked')) {
                        alert("宅配檔次/通用券檔次無須設定[銷售店鋪-隱藏]賣家"); 
                        return false;
                    }
                }
                var index = $(".hideVerifyShop > :checkbox").index(this);
                if ($('.checkVerifyShop > :checkbox:eq(' + index +')').is(':disabled')) {
                    return false;
                }
                if (!$('.checkVerifyShop > :checkbox:eq(' + index +')').is(':checked')) {
                    alert("僅限已勾選[銷售店鋪]權限之賣家使用");
                    return false;
                } 
                else {     
                    if ($(this).is(':checked')) {
                        alert("若為設錯銷售分店；且須請技術同仁進行訂單搬移分店處理，應將錯誤分店取消勾選；請勿使用隱藏功能");
                    }
                }
            });            

            $('#btnGetDraftForDescription').click(function () {
                var descDraft = GetDraftForDescriptionByComboDealSpecs();
                if (descDraft != null && descDraft.length > 0) {
                    editorDesc.setData(descDraft + editorDesc.getData());
                }
            });



            //未同意宅配合約不能建宅配檔(無法確認旅遊/品生活宅配)
            if ('<%=config.IsRemittanceFortnightly%>' == 'True')
            {
                if ('<%=IsAgreeHouseNewContractSeller%>'=='False' && '<%=DeptId%>' == '<%=EmployeeChildDept.S010.ToString()%>')
                {
                    $("select[id$=ddlRreceiveType] option[value=<%=(int)DeliveryType.ToHouse%>]").attr('disabled',true)
                }
            }
            
            //popup
            $('#btnVerifyShop').bind('click', function (e) {
                $('div#divStoreHide').bPopup({
                    modalClose: false,
                });
            });

            new DealTypeOperator(
                '<%=this.DealTypeJson%>',
                '#dealTypePanel',
                '#hdDealTypeNew', []).init();

        });

        function getMoreStore(n){
            if(n < 0){
                $('.loadmore').hide();
                block_postback();
            }else{
                $(".tblStoreSettings .ui-sortable tr").eq(0).hide();
                $('#loadStore').show();
                $('.loadmore').hide();
            }
            var sid = $('#hdSellerGuid').val();
            var bid = $('#hdBid').val();
            $.ajax({
                type: "POST",
                url: "Setup.aspx/GetMoreStore",
                data: " { 'sid': '" + sid + "','bid': '" + bid + "','n': '" + n + "' }",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(response) {
                    var jsonDoc = $.parseJSON(response.d);
                    var stores = $(jsonDoc);
                    var row = $(".tblStoreSettings .ui-sortable tr").last().clone(true);
                    var cindex = 100 + $(".tblStoreSettings .ui-sortable tr").length;
                    stores.each(function () {
                        var store = $(this)[0];
                        $(".checkstore", row).find('input').attr('checked',store['CheckStore']);
                        $(".bindStoreName", row).html(store['StoreName']);
                        $(".bindStoreAddress", row).html(store['StoreAddress']);
                        $(".bindStoreGuid", row).removeAttr('value');
                        $(".bindStoreGuid", row).attr('value', store['Guid']);
                        $(".bindStoreGuid", row).attr('id', 'input_guid_id'+(cindex));
                        $(".bindStoreGuid", row).attr('name', 'input_guid_name'+(cindex));
                        $(".bindStoreStatus", row).removeAttr('value');
                        $(".bindStoreStatus", row).attr('value', store['StoreStatus']);
                        $(".bindStoreStatus", row).attr('id', 'input_status_id'+(cindex));
                        $(".bindStoreStatus", row).attr('name', 'input_status_name'+(cindex));
                        $("span[id$='litStoreOrder']", row).text(store['SortOrder']);
                        $("input[id$='hfNewSortOrder']", row).attr('text', store['OrderedQuantity']);
                        $("input[id$='hfNewSortOrder']", row).attr({
                            id:'ctl00_ContentPlaceHolder1_repStore_ct'+cindex+'_hfNewSortOrder',
                            name:'ctl00$ContentPlaceHolder1$repStore$ct'+cindex+'$hfNewSortOrder',
                        });
                        $("input[id$='hidCompareStoreInfo']", row).attr({
                            id:'ctl00_ContentPlaceHolder1_repStore_ct' + cindex +'_hidCompareStoreInfo',
                            name:'ctl00$ContentPlaceHolder1$repStore$ct'+ cindex +'$hidCompareStoreInfo'
                        });
                        row.show();
                        $(".tblStoreSettings .ui-sortable tr").last().after(row);
                        row = $(".tblStoreSettings .ui-sortable tr").last().clone(true);
                        cindex ++;
                    });
                    //初次載入會移除repeater第一筆資料
                    if(n > 0){
                        $(".tblStoreSettings .ui-sortable tr").eq(0).remove();
                        $('.loadmore').show();
                    }
                },
                complete:function(){
                    $.unblockUI();
                    $('#loadStore').hide();
                },
                failure: function (response) {
                    alert(response.d);
                },
                error: function (response) {
                    alert(response.d);
                }
            });
        }

        function getMoreSellerTree(n){
            if(n < 0){
                $('.loadMoreSellerTree').hide();
                block_postback();
            }else{
                $(".tblSellerTree tbody tr").eq(0).hide();
                $('#loadSellerTree').show();
                $('.loadMoreSellerTree').hide();
            }
            var sid = $('#hdSellerGuid').val();
            var bid = $('#hdBid').val();
            var enabledSelf = $('select[id$=ddlRreceiveType]').val() == '<%=(int)DeliveryType.ToHouse %>';
            var isDealOpenOrClose = bid.length === 0 || '<%=!IsDealOpened || IsDealClosed%>' === 'True';

            $.ajax({
                type: "POST",
                url: "Setup.aspx/GetMoreSellerTree",
                data: " { 'sid': '" + sid + "','bid': '" + bid + "','n': '" + n + "','enabledSelf': '" + enabledSelf + "','isDealOpenOrClose': '" + isDealOpenOrClose + "' }",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(response) {
                    var jsonDoc = $.parseJSON(response.d);
                    var stores = $(jsonDoc);
                    var row = $(".tblSellerTree tbody tr").last().clone(true);
                    var cindex = 100 + $(".tblSellerTree tbody tr").length;
                    stores.each(function () {
                        var store = $(this)[0];
                        $("input[id$='cbVerify']", row).attr('disabled', store['Disabled']);
                        $("input[id$='cbVerify']", row).attr('checked', store['CheckVerify']);
                        $("input[id$='cbVerify']", row).attr({
                            id:'ctl00_ContentPlaceHolder1_repSellerTree_ct' + cindex +'_cbVerify',
                        });
                        
                        $("input[id$='cbVerifyShop']", row).attr('disabled', store['Disabled']);
                        $("input[id$='cbVerifyShop']", row).attr('checked', store['CheckVerifyShop']);
                        $("input[id$='cbVerifyShop']", row).attr({
                            id:'ctl00_ContentPlaceHolder1_repSellerTree_ct' + cindex +'_cbVerifyShop',
                        });
                        
                        $("input[id$='cbHideVerifyShop']", row).attr('disabled', store['Disabled'] || store['HideVerifyShopDisabled']);
                        $("input[id$='cbHideVerifyShop']", row).attr('checked', store['CheckHideVerifyShop']);
                        $("input[id$='cbHideVerifyShop']", row).attr({
                            id:'ctl00_ContentPlaceHolder1_repSellerTree_ct' + cindex +'_cbHideVerifyShop',
                        });

                        $("input[id$='cbAccounting']", row).attr('disabled', store['Disabled']);
                        $("input[id$='cbAccounting']", row).attr('checked', store['CheckAccounting']);
                        $("input[id$='cbAccounting']", row).attr({
                            id:'ctl00_ContentPlaceHolder1_repSellerTree_ct' + cindex +'_cbAccounting',
                        });
                        $("input[id$='cbViewBalanceSheet']", row).attr('disabled', store['Disabled']);
                        $("input[id$='cbViewBalanceSheet']", row).attr('checked', store['CheckViewBalanceSheet']);
                        $("input[id$='cbViewBalanceSheet']", row).attr({
                            id:'ctl00_ContentPlaceHolder1_repSellerTree_ct' + cindex +'_cbViewBalanceSheet',
                        });
                        $("span[id$='lbSellerName']", row).text(store['StoreName']);
                        $("input[id$='hidSellerGuid']", row).val(store['Guid']);
                        $("input[id$='hidSellerGuid']", row).attr({
                            id:'ctl00_ContentPlaceHolder1_repSellerTree_ct' + cindex +'_hidSellerGuid',
                            name:'ctl00$ContentPlaceHolder1$repSellerTree$ct'+cindex+'$hidSellerGuid',
                        });
                        $("input[id$='txtStoreChangedExpireDate']", row).attr('disabled', store['Disabled']);
                        $("input[id$='txtStoreChangedExpireDate']", row).removeAttr('value');
                        $("input[id$='txtStoreChangedExpireDate']", row).attr('value', store['ChangedExpireDate']);
                        $("input[id$='txtStoreChangedExpireDate']", row).attr({
                            id:'ctl00_ContentPlaceHolder1_repSellerTree_ct' + cindex +'_txtStoreChangedExpireDate',
                        });
                        $("span[id$='lbQuantity']", row).text(store['OrderedQuantity']);
                        $("span[id$='lbQuantity']", row).attr({
                            id:'ctl00_ContentPlaceHolder1_repSellerTree_ct' + cindex +'_lbQuantity',
                        });
                        $("input[id$='txtStoreCount']", row).attr('disabled', store['Disabled']);
                        $("input[id$='txtStoreCount']", row).removeAttr('value');
                        $("input[id$='txtStoreCount']", row).attr('value', store['StoreCount']);
                        $("input[id$='txtStoreCount']", row).attr({
                            id:'ctl00_ContentPlaceHolder1_repSellerTree_ct' + cindex +'_txtStoreCount',
                        });
                        $("textarea[id$='txtStoreUseTime']", row).attr('disabled', store['Disabled']);
                        $("textarea[id$='txtStoreUseTime']", row).text(store['StoreUseTime']);
                        $("textarea[id$='txtStoreUseTime']", row).attr({
                            id:'ctl00_ContentPlaceHolder1_repSellerTree_ct' + cindex +'_txtStoreUseTime',
                        });
                        if (store['HideBalanceSheet']) {
                            $("input[id$='hidIsHideBalanceSheet']", row).val('True');
                        }
                        else {
                            $("input[id$='hidIsHideBalanceSheet']", row).val('False');
                        }
                        $("input[id$='hidIsHideBalanceSheet']", row).attr({
                            id:'ctl00_ContentPlaceHolder1_repSellerTree_ct' + cindex +'_hidIsHideBalanceSheet',
                            name:'ctl00$ContentPlaceHolder1$repSellerTree$ct'+ cindex +'$hidIsHideBalanceSheet',
                        });
                        $("img[id$='btnHideBalanceSheet']", row).attr('src', '<%=ResolveUrl("~/Themes/default/images/17Life/G2/")%>' + store['HideBalanceSheetImageUrl']);
                        $("img[id$='btnHideBalanceSheet']", row).attr({
                            id:'ctl00_ContentPlaceHolder1_repSellerTree_ct' + cindex +'_btnHideBalanceSheet',
                        });
                        row.show();
                        $(".tblSellerTree tbody tr").last().after(row);
                        row = $(".tblSellerTree tbody tr").last().clone(true);
                        cindex ++;
                    });
                    //初次載入會移除repeater第一筆資料
                    if(n > 0){
                        $(".tblSellerTree tbody tr").eq(0).remove();
                        $('.loadMoreSellerTree').show();
                    }
                    $('.storeCloseDate').datepicker();
                },
                complete:function(){
                    $.unblockUI();
                    $('#loadSellerTree').hide();
                },
                failure: function (response) {
                    alert(response.responseText);
                },
                error: function (response) {
                    alert(response.responseText);
                }
            });
        }

        function block_postback() {
            $.blockUI({ message: "<center><font color='blue'>處理中，請稍候...</font></center>", css: { width: '250px' } });
        }

        <%--切換checkbox(低毛利率可使用折價券/本檔次不可使用折價券)--%>
        function SwitchSpecialRequirement() {
            var hasPrivilege = '<%=HasUseDiscountSetPrivilege%>' == 'True' ? true : false;
            if (hasPrivilege) {
                var allowed = $('#<%= cbx_LowGrossMarginAllowedDiscount.ClientID %>');
                var notAllowed = $('#<%= cbx_NotAllowedDiscount.ClientID %>');

                if (notAllowed.is(':checked')) {
                    allowed.removeAttr('checked');
                    allowed.prop('disabled', 'disabled');
                } else {
                        allowed.removeAttr('disabled');
                }

                notAllowed.on('change', function () {
                    if (notAllowed.is(':checked')) {
                        allowed.removeAttr('checked');
                        allowed.prop('disabled', 'disabled');
                    } else {
                        allowed.removeAttr('disabled');
                    }
                });
            }
        }

    </script>

    <script type="text/javascript" src="//www.google.com/jsapi"></script>
    <script type="text/javascript">
        google.load("jqueryui", "1.8");
    </script>
    <style type="text/css">
   .content {display:none;}        

   .clearfix:after {
    clear: both;
    content: ".";
    display: block;
    height: 0;
    line-height: 0;
    visibility: hidden;
}

        <%// dealCategoryArea  start%>
        table[id^='dealCategoryArea'] label,ul[id^="dealCategorytree"] label
        {
            display:initial;
        }
        table[id^='dealCategoryArea'] ul,ul[id^="dealCategorytree"]
        {
            list-style-type: none;
            margin: 0 0 0 0;
            padding-left: 20px;

        }
        table[id^='dealCategoryArea'] li,ul[id^="dealCategorytree"] li
        {
            font-size: 12px;
            font-weight:normal;
            font-family: Trebuchet MS, Tahoma, Verdana, Arial, sans-serif;
        }

        table[id^='dealCategoryArea'] td,ul[id^="dealCategorytree"] td
        {
            vertical-align: text-top;
            margin-left: 0px;
        }
        <%// dealCategoryArea  end%>
        <%--body
        {
            font-size: 10px;
        }--%>
        #tabs
        {
            font-size: smaller;
        }
        .CommercialLabel label, #<%=cbl_couponDealTag.ClientID %> label, #<%=cbl_couponTravelDealTag.ClientID %> label, #<%=cbl_deliveryDealTag.ClientID %> label, #<%=cbl_deliveryTravelDealTag.ClientID %> label
        {
            display: inline;
            font-size: small;
        }
        fieldset
        {
            margin: 10px 0px;
            border: solid 1px gray;
        }
        fieldset legend
        {
            color: #4F8AFF;
            font-weight: bolder;
        }
        ul
        {
            list-style-type: none;
            margin: 0 0 0 0;
        }
        li
        {
            color: #4D4D4D;
            font-weight: bold;
        }

        ul.status li
        {
            float: left;
        }
        ul.target input
        {
            width: 80px;
        }

        input[type="text"]
        {
            border: solid 1px grey;
        }
        textarea
        {
            border: solid 1px grey;
        }

        input.dealName
        {
            width: 300px;
        }
        input.date
        {
            width: 80px;
        }

        .price
        {
            color: red;
            font-weight: bolder;
        }

        .intro
        {
            float: left;
            margin: 5px 0px 5px 0px;
            border: none;
        }
        .intro legend
        {
            color: #4F8AFF;
            font-weight: bolder;
        }
        .intro textarea
        {
            width: 500px;
        }

        #avpane ul
        {
            margin-bottom: 3px;
            border: dashed 1px gray;
            padding: 2px 2px 2px 5px;
        }

        #avpane ul.hover
        {
            background-color: #E5ECF9;
        }

        #avpane li
        {
            font-weight: normal;
            font-size: smaller;
            margin: 0px;
        }

        #avpane li.eb, #avpane li.db, #avpane li.cb
        {
            display: none;
            float: left;
            padding-right: 15px;
            text-decoration: underline;
            color: blue;
            cursor: pointer;
        }

        .btn_left
        {
            float: left;
        }
        .btn_mid
        {
            float: left;
            margin-left: 100px;
        }
        .btn_right
        {
            float: right;
        }
        .btn_right a
        {
            margin: 0px 10px 0px 10px;
        }

        /*設定table裡字的大小*/
        table.table_font
        {
            font-size: small;
            color: #333;
        }

        .nonBlock label
        {
            display: inline;
        }

        .tblStoreSettings td, .tblStoreSettings th
        {
             text-align:left;
        }
        .tblStoreSettings tbody tr:hover td
        {
            background-color: #EDBD3E;
            cursor:move;
        }
        .tblStoreSettings tbody tr
        {
            width: 300px;
        }
        <%--combo setup--%>

        .comboItem
        {
			cursor: pointer
        }
        a.comboItemLink {
			margin-top:3px;
			margin-left:3px;
            cursor: pointer;
            border: 0px !important;
            color: blue;
            padding: 3px;
            letter-spacing:1px;
        }
        .comboItemLink span{
            background-color: #eeeeee;
            border: 0px;
            
            font-weight: normal;
        }
        .pull-right {
            text-align: right;
        }
        .comboTitle {
			text-decoration: underline;
			display: inline-block;
        }
        .comboList {
            border-spacing: 0px;
            font-size: 11px;
            font-family: 新細明體;
            
        }
        .comboList input {
            padding: 2px;
        }
        .comboList th{
            font-weight: normal;
        }
        .comboItem:hover {
			background-color: #EDBD3E;
        }
        .comboMain {
	        font-size: 15px;
	        border-bottom: 1px dashed black;
            padding: 10px 10px 10px 0px;
        }
        .comboMainTitle a {
            border: 0px !important;
            color: blue !important;
            font-size: 11px; 
            text-decoration: underline !important;
            font-weight: normal;
            padding: 3px;
        }
        .comboSub {
	        font-size: 15px;
            padding: 10px 10px 10px 0px;
        }
        .comboItemNewTitle {
			margin-left:4px;
	        width: 290px;
        }
        a.comboItemActived {
            background-color: #f9f9f9;
            font-weight: bold;
        }
        .copydeal
        {
            width: 500px;
            height: 50px;
            background-color: #EBDDC7;
            border: 2px solid #000000;
        }
        .copydeal-Top
        {
            width: 500px;
        }
        .copydeal-X-button
        {
            background-image: url(../../Themes/default/images/17Life/G2/A2-Multi-grade-Setting_xx.png);
            height: 28px;
            width: 27px;
            float: right;
            margin-top: -9px;
            margin-right: -9px;
        }
        .readonly
        {
           color: #777777;
        }


        .categoryArea {
            display: inline-block;
            width: 100%;
        }


         #dealCategoryTable {
            display: inline-block;
            width: 100%;
        }

        .categoryArea label {  display: inline-block;font-size: 12px; }

        .TabTitle
        {
            border: none;
            background: none;
            text-align: center;
            width: 100px;
            color: #1c94c4;
            font-weight: bold;
            cursor: pointer;
        }

        #multiTabs ul li a
        {
            padding-left: 0px;
            padding-right: 0px;
        }
        
        #<%=isCombineSetting.ClientID%> label{
            display:inline;
         }

        .hide {
            display:none;
        }

        #nav{
            width:100px;
            border:1px solid black;
            padding:5px;
        }
    </style>
    <script type="text/javascript">
        var b_editor;
        var editorDesc;
        function setupdatepicker(from, to) {
            var dates = $('#' + from + ', #' + to).datepicker({
                changeMonth: true,
                numberOfMonths: 3,
                onSelect: function (selectedDate) {
                    var option = this.id == from ? "minDate" : "maxDate";
                    var instance = $(this).data("datepicker");
                    var date = $.datepicker.parseDate(instance.settings.dateFormat || $.datepicker._defaults.dateFormat, selectedDate, instance.settings);
                    dates.not(this).datepicker("option", option, date);
                },
                onClose: function(selectedDate) {
                    if(this.id=="tbDS" || this.id=="tbDE"){
                        var DeliveryWord=b_editor.getData();
                        $('#hidinitDeliveryTime').html(DeliveryWord);
                        if(selectedDate!="" && selectedDate!=null  && this.id=="tbDS"){
                            $('#initDeliveryTimeStart').html('').append(selectedDate);
                            var ChangeDeliveryTimeStart=$('#hidinitDeliveryTime').html();
                            b_editor.setData(ChangeDeliveryTimeStart);
                        }
                        DeliveryWord=b_editor.getData();
                        $('#hidinitDeliveryTime').html(DeliveryWord);
                        if(selectedDate!="" && selectedDate!=null  && this.id=="tbDE"){
                            $('#initDeliveryTimeEnd').html('').append(selectedDate);
                            var ChangeDeliveryTimeEnd=$('#hidinitDeliveryTime').html();
                            b_editor.setData(ChangeDeliveryTimeEnd);
                        }
                        $('#hidinitDeliveryTime').html('');
                    }
                }
            });
        }

        function calcdiscount() {
            var price = parseInt($('#<%=tbP.ClientID%>').val().replace(',', '')) || 0;
            var origPrice = parseInt($('#<%=tbOP.ClientID%>').val().replace(',', '')) || 0;
            var rate = 'N/A';
            if (origPrice > 0) {
                rate = Math.round(price * 10 / origPrice);
                if (rate == 100)
                    rate = '無';
                else if (rate % 10 == 0 || rate < 10){}
                    //↓已經壞掉很久，估計無作用，先註解
                    //$.ajax({
                    //    type: "POST",
                    //    url: "../../WebService/PponDealService.asmx/DealDiscountGet",
                    //    data: " { 'itemPrice': '" + price + "','itemOrigPrice': '" + origPrice + "' }",
                    //    contentType: "application/json; charset=utf-8",
                    //    dataType: "json",
                    //    success: function (response) {
                    //        rate = response.d;
                    //        $('#discountRate').text(rate);
                    //        $('#discountPrice').text(origPrice - price);
                    //    }
                    //});
            }
            
            $('#discountRate').text(rate);
            $('#discountPrice').text(origPrice - price);

        }

        function GetDealAveragePrice() {
            if ($('#chkIsAveragePrice').is(':checked')) {
                $('#AveragePriceArea').css('display', 'inline');
                var price = parseInt($('#<%=tbP.ClientID%>').val().replace(',', '')) || 0;
                var quantityMultiplier = parseInt($('#txtQuantityMultiplier').val().replace(',', '')) || 0;
                var averagePrice = 'N/A';
                if ( price>0 && quantityMultiplier > 0) {
                    if (quantityMultiplier == 1) {
                        $('#lbAveragePrice').text(price);
                    } else {
                        averagePrice = Math.ceil(price  / quantityMultiplier);
                        $('#lbAveragePrice').text(averagePrice);
                    }
                } else {
                    $('#lbAveragePrice').text(0);
                }

            } else {
                $('#AveragePriceArea').hide();
            }

        }

        function populateAvailInfo(dest, src) {
            var el = $(src);
            $('#availInfo').dialog('open');
            if (el.length) {
                var lis = el.children('li');
                $('#storeName').val($(lis.get(3)).text());
                $('#storePhone').val($(lis.get(4)).text());
                $('#storeAddress').val($(lis.get(5)).text());
                $('#storeOpeningTime').val($(lis.get(6)).text());
                $('#storeOpeningDays').val($(lis.get(7)).text());
                $('#storeUrl').val($(lis.get(8)).text());
                $('#storeRemark').val($(lis.get(9)).text());
                $('#storeCloseDays').val($(lis.get(10)).text());
                $('#mrt').val($(lis.get(11)).text());
                $('#car').val($(lis.get(12)).text());
                $('#bus').val($(lis.get(13)).text());
                $('#otherVehicle').val($(lis.get(14)).text());
                $('#fbFans').val($(lis.get(15)).text());
                $('#plurkFans').val($(lis.get(16)).text());
                $('#blogFans').val($(lis.get(17)).text());
                $('#otherFans').val($(lis.get(18)).text());
            } else {
                $('#storeName').val('');
                $('#storePhone').val('');
                $('#storeAddress').val('');
                $('#storeOpeningTime').val('');
                $('#storeOpeningDays').val('');
                $('#storeUrl').val('');
                $('#storeRemark').val('');
                $('#storeCloseDays').val('');
                $('#mrt').val('');
                $('#car').val('');
                $('#bus').val('');
                $('#otherVehicle').val('');
                $('#fbFans').val('');
                $('#plurkFans').val('');
                $('#blogFans').val('');
                $('#otherFans').val('');
            }
            $('body').data('avdom', dest);
        }

        function setAvailInfo(dom) {
            var el = $(dom);
            if (el.length) {
                var lis = el.children('li');
                $(lis.get(3)).text($('#storeName').val());
                $(lis.get(4)).text($('#storePhone').val());
                $(lis.get(5)).text($('#storeAddress').val());
                $(lis.get(6)).text($('#storeOpeningTime').val());
                $(lis.get(7)).text($('#storeOpeningDays').val());
                $(lis.get(8)).text($('#storeUrl').val());
                $(lis.get(9)).text($('#storeRemark').val());
                $(lis.get(10)).text($('#storeCloseDays').val());
                $(lis.get(11)).text($('#mrt').val());
                $(lis.get(12)).text($('#car').val());
                $(lis.get(13)).text($('#bus').val());
                $(lis.get(14)).text($('#otherVehicle').val());
                $(lis.get(15)).text($('#fbFans').val());
                $(lis.get(16)).text($('#plurkFans').val());
                $(lis.get(17)).text($('#blogFans').val());
                $(lis.get(18)).text($('#otherFans').val());
            } else {
                var html = '<ul><li class="eb">(edit)</li>'
                    + '<li class="cb">(copy)</li>'
                    + '<li class="db">(delete)</li>'
                    + '<li>' + $('#storeName').val() + '</li>'
                    + '<li>' + $('#storePhone').val() + '</li>'
                    + '<li>' + $('#storeAddress').val() + '</li>'
                    + '<li>' + $('#storeOpeningTime').val() + '</li>'
                    + '<li>' + $('#storeOpeningDays').val() + '</li>'
                    + '<li>' + $('#storeUrl').val() + '</li>'
                    + '<li>' + $('#storeRemark').val() + '</li>'
                    + '<li>' + $('#storeCloseDays').val() + '</li>'
                    + '<li>' + $('#mrt').val() + '</li>'
                    + '<li>' + $('#car').val() + '</li>'
                    + '<li>' + $('#bus').val() + '</li>'
                    + '<li>' + $('#otherVehicle').val() + '</li>'
                    + '<li>' + $('#fbFans').val() + '</li>'
                    + '<li>' + $('#plurkFans').val() + '</li>'
                    + '<li>' + $('#blogFans').val() + '</li>'
                    + '<li>' + $('#otherFans').val() + '</li>'
                    + '</ul>';
                $('#avpane').append(html);
            }
            $('#availInfo').dialog('close');
        }

        function toggleAvailMode(dom, on) {
            if (on) {
                $(dom).addClass('hover');
                $(dom).children('.eb,.cb,.db').show();
            } else {
                $(dom).removeClass('hover');
                $(dom).children('.eb,.cb,.db').hide();
            }
        }

        function serav() {
            var avarr = [];
            $('#avpane > ul').each(function () {
                var lis = $(this).children('li');
                var n = $(lis.get(3)).text();
                var p = $(lis.get(4)).text();
                var a = $(lis.get(5)).text();
                var ot = $(lis.get(6)).text();
                var od = $(lis.get(7)).text();
                var u = $(lis.get(8)).text();
                var r = $(lis.get(9)).text();
                var cd = $(lis.get(10)).text();
                var mr = $(lis.get(11)).text();
                var ca = $(lis.get(12)).text();
                var bu = $(lis.get(13)).text();
                var ov = $(lis.get(14)).text();
                var fb = $(lis.get(15)).text();
                var pl = $(lis.get(16)).text();
                var bl = $(lis.get(17)).text();
                var ol = $(lis.get(18)).text();
                if (n != '' || p != '' || a != '' || ot != '' || od != '' || u != '' || r != '' || cd != '' || mr != '' || ca != '' || bu != '' || ov != '' || fb != '' || pl != '' || bl != '' || ol != '') {
                    avarr.push({ "N": n, "P": p, "A": a, "OT": ot, "OD": od, "U": u, "R": r, "CD": cd, "MR": mr, "CA": ca, "BU": bu, "OV": ov, "FB": fb, "PL": pl, "BL": bl, "OL": ol });
                }
            });

            $('#hav').val(JSON.stringify(avarr));
        }

        function $$(id, context) {
            var el = $("#" + id, context);
            if (el.length < 1)
                el = $("*[id$=" + id + "]", context);
            return el;
        }

        //運費處理
        function freightShow(type) {
            var tbFreight = undefined;
            var hdJson = undefined;
            var tbFStartAmt = undefined;
            var hdCostPayTo = undefined;

            if (type == 1) {
                tbFStartAmt = $('#tbFIStartAmt');
                tbFreight = $('#tableFI');
                hdJson = $('#<%=hdFreightIncome.ClientID %>');
            }
            else {
                tbFStartAmt = $('#tbFCStartAmt');
                tbFreight = $('#tableFC');
                hdJson = $('#<%=hdFreightCost.ClientID %>');
                hdCostPayTo = $('#<%=hdCostPayTo.ClientID %>');
            }
            var json = jQuery.parseJSON(hdJson.val());
            if (json == null) {
                return;
            }
            $.each(json, function (index, fr) {
                var html = "<tr><td>" + (tbFreight.children('tbody').children('tr').length + 1) + "</td><td>" + fr.FreightAmount + "</td><td>" + fr.StartAmount + "</td>";
                if (type == 1) {
                    html += "</tr>";
                } else {
                    hdCostPayTo.val(fr.PayTo);
                    html += "<td>" + $("#<%= freightCostPayTo.ClientID %> option[value='" + fr.PayTo + "']").text() + "</td></tr>";
                }
                tbFreight.append(html);
                tbFStartAmt.removeAttr('disabled');
            });
        }

        //進貨價處理
        function costShow() {
            var tbCost = undefined;
            var hdJson = undefined;
            var tbQuantity = undefined;
            var tdCumulativeQuantity = "";
            tbQuantity = $('#tbQuantity');
            tbCost = $('#tableCost');
            hdJson = $('#<%=hdMultiCost.ClientID %>');
            var json = jQuery.parseJSON(hdJson.val());
            if (json == null) {
                return;
            }
            $.each(json, function (index, fr) {
                var html = "<tr><td class='costId'>" + fr.Id + "</td><td class='multiCost'>" + fr.Cost + "</td><td class='quantity'>" + fr.Quantity + "</td><td class='cumulativeQuantity'>" + fr.Cumulative_quantity + "</td></tr>";
                tbCost.append(html);
                tbQuantity.removeAttr('disabled');  
            });
        }

        function costAdd() 
        {
            var hdJson = undefined;
            var tbMultiCost = undefined;
            var tbQuantity = undefined;
            var tdCumulativeQuantity = "";
            var hdQuantity = $("#<%=hdCumulativeQuantity.ClientID %>").val();
            tbMultiCost = $('#tbMultiCost');
            tbQuantity = $('#tbQuantity');
            tbMultiCost_val = $('#tbMultiCost').val();

            tbQuantity_val = $('#tbQuantity').val();
            tbCost = $('#tableCost');
            hdJson = $('#<%=hdMultiCost.ClientID %>');
            tdCumulativeQuantity = parseInt($('#tbQuantity').val()) + parseInt(hdQuantity);
        
            if (tbMultiCost_val.indexOf('.') >= 0)
            {
                if ("<%=IsAllowedToSetCost%>" != "True")
                {
                    alert("進貨價不能輸入小數");
                    return;
                }
            }

            //if (jQuery.trim($('#tbMultiCost').val()).length == 0) {
            //    alert("請輸入進貨價資料。");
            //    return;
            //}

            if (jQuery.trim(tbQuantity_val).length == 0) {
                tbQuantity_val = 99999;
                $('#tbQuantity').val("99999");
                tdCumulativeQuantity = parseInt($('#tbQuantity').val()) + parseInt(hdQuantity);
            }

            if (tbQuantity_val == 0) {
                alert("份數不得為0");
                return;
            }

            if (isNaN(Number(tbMultiCost_val)) || isNaN(Number(tbQuantity_val))) {
                alert("必須輸入數字。");
                return;
            }
            var html = "<tr><td class='costId'>" + (tbCost.children('tbody').children('tr').length + 1) + "</td><td class='multiCost'>" + tbMultiCost_val + "</td><td class='quantity'>" + tbQuantity_val + "</td><td class='cumulativeQuantity'>" + tdCumulativeQuantity + "</td><td class='hdQuantity' style='display:none'>" + parseInt(hdQuantity) + "</td>";
            html += "</tr>";
            tbCost.append(html);
            var avarr = [];
            var rows = tbCost.children('tbody').children('tr');

            rows.each(function () {
                var lis = $(this).children('td');
                var id = $(lis.get(0)).text();
                var cost = $(lis.get(1)).text();
                var quantity = $(lis.get(2)).text();
                var cumulativeQuantity = $(lis.get(3)).text();
                var lowerCumulativeQuantity = $(lis.get(4)).text();
                if (cumulativeQuantity == "undefined" || lowerCumulativeQuantity == "undefined" || cumulativeQuantity == "NaN" || lowerCumulativeQuantity == "NaN") {
                    alert("無法新增進貨價，請按F5重新整理網頁");
                    return
                }
                avarr.push({ "Id": id, "Cost": cost, "Quantity": quantity, "Cumulative_quantity": cumulativeQuantity, "Lower_cumulative_quantity": lowerCumulativeQuantity });
            });
            hdJson.val(JSON.stringify(avarr));
            tbMultiCost.val("");
            tbQuantity.val("");
            tbQuantity.removeAttr('disabled');
            $("#<%=hdCumulativeQuantity.ClientID %>").val(tdCumulativeQuantity);
        }

        function costDelete() {
            var freightStartAmt = 0;
            var tbCost = undefined;
            var hdJson = undefined;

            tbCost = $('#tableCost');
            hdJson = $('#<%=hdMultiCost.ClientID %>');

            if (tbCost.children('tbody').children('tr').length < 1) {
                alert("無資料供刪除。");
                return;
            }
            var len = tbCost.children('tbody').children('tr').length;
            var avarr = [];
            var rows = tbCost.children('tbody').children('tr');
            $(rows.get(len - 1)).remove();
            //刪除後重新取得一次
            rows = tbCost.children('tbody').children('tr');
            rows.each(function () {
                var lis = $(this).children('td');
                var id = $(lis.get(0)).text();
                var cost = $(lis.get(1)).text();
                var quantity = $(lis.get(2)).text();
                var cumulativeQuantity = $(lis.get(3)).text();
                var lowerCumulativeQuantity = $(lis.get(4)).text();
                avarr.push({ "Id": id, "Cost": cost, "Quantity": quantity, "cumulative_quantity": cumulativeQuantity, "Lower_cumulative_quantity": lowerCumulativeQuantity });
                $("#<%=hdCumulativeQuantity.ClientID %>").val(cumulativeQuantity);
            });
            hdJson.val(JSON.stringify(avarr));
            if (rows.length == 0) {
                $("#<%=hdCumulativeQuantity.ClientID %>").val(0);
                $('#GrossMargin').text('N/A');
                $("#GrossMargin").removeClass();
                $('#<%=grossMarginMessage.ClientID %>').hide();
            }

        }

        function freightAdd(type) {
            var freightAmt = 0;
            var freightStartAmt = 0;
            var freightPayTo = undefined;
            var tbFreight = undefined;
            var hdJson = undefined;
            var hdFMax = undefined;
            var tbFAmt = undefined;
            var tbFStartAmt = undefined;
            var hdCostPayTo = undefined;

            if (type == 1) {
                tbFAmt = $('#tbFIAmt');
                tbFStartAmt = $('#tbFIStartAmt');
                freightAmt = $('#tbFIAmt').val();
                freightStartAmt = $('#tbFIStartAmt').val();
                tbFreight = $('#tableFI');
                hdJson = $('#<%=hdFreightIncome.ClientID %>');
                hdFMax = $('#<%=hdFIMax.ClientID %>');
            }
            else {
                tbFAmt = $('#tbFCAmt');
                tbFStartAmt = $('#tbFCStartAmt');
                freightAmt = $('#tbFCAmt').val();
                freightStartAmt = $('#tbFCStartAmt').val();
                freightPayTo = $('#<%= freightCostPayTo.ClientID %>').val();
                tbFreight = $('#tableFC');
                hdJson = $('#<%=hdFreightCost.ClientID %>');
                hdFMax = $('#<%=hdFCMax.ClientID %>');
                hdCostPayTo = $('#<%=hdCostPayTo.ClientID %>').val();
            }
            if (jQuery.trim(freightAmt).length == 0 || jQuery.trim(freightStartAmt).length == 0) {
                alert("請輸入運費資料。");
                return;
            }
            if (isNaN(Number(freightAmt)) || isNaN(Number(freightStartAmt))) {
                alert("必須輸入數字。");
                return;
            }
            if (tbFreight.children('tbody').children('tr').length != 0 && parseInt(hdFMax.val()) >= parseInt(freightStartAmt)) {
                alert("購買金額設定必須大於既有設定的值。");
                return;
            }
            if (type != 1 && freightPayTo == "") {
                alert("請選擇運費付款對象。");
                return;
            } else {
                if (hdCostPayTo != "" && freightPayTo != hdCostPayTo) {
                    alert("付款對象只能選擇一家廠商。");
                    return;
                }
            }

            var html = "<tr><td>" + (tbFreight.children('tbody').children('tr').length + 1) + "</td><td>" + freightAmt + "</td><td>" + freightStartAmt + "</td>";
            if (type == 1) {
                html += "</tr>";
            } else {
                $('#<%=hdCostPayTo.ClientID %>').val(freightPayTo);
                html += "<td>" + $("#<%= freightCostPayTo.ClientID %> option[value='" + freightPayTo + "']").text() + "</td></tr>";
            }
            tbFreight.append(html);
            var avarr = [];
            var rows = tbFreight.children('tbody').children('tr');
            rows.each(function () {
                var lis = $(this).children('td');
                var freightAmount = $(lis.get(1)).text();
                var startAmount = $(lis.get(2)).text();
                if (type == 1) {
                    avarr.push({ "StartAmount": startAmount, "FreightAmount": freightAmount });
                } else {
                    var payTo = $(lis.get(3)).text();
                    $("#<%= freightCostPayTo.ClientID %> option").each(function () {
                        if ($(this).text() == payTo) {
                            payTo = $(this).val();
                        }
                    });
                    avarr.push({ "StartAmount": startAmount, "FreightAmount": freightAmount, "PayTo": payTo });
                }
            });
            hdJson.val(JSON.stringify(avarr));
            hdFMax.val(freightStartAmt);

            tbFAmt.val("");
            tbFStartAmt.val("");
            tbFStartAmt.removeAttr('disabled');
        }

        function freightDelete(type) {
            var freightStartAmt = 0;
            var tbFreight = undefined;
            var hdJson = undefined;
            var hdFMax = undefined;

            if (type == 1) {
                tbFreight = $('#tableFI');
                hdJson = $('#<%=hdFreightIncome.ClientID %>');
                hdFMax = $('#<%=hdFIMax.ClientID %>');
            }
            else {
                tbFreight = $('#tableFC');
                hdJson = $('#<%=hdFreightCost.ClientID %>');
                hdFMax = $('#<%=hdFCMax.ClientID %>');
            }

            if (tbFreight.children('tbody').children('tr').length < 1) {
                alert("無資料供刪除。");
                return;
            }
            var len = tbFreight.children('tbody').children('tr').length;
            var avarr = [];
            var rows = tbFreight.children('tbody').children('tr');
            $(rows.get(len - 1)).remove();
            //刪除後重新取得一次
            rows = tbFreight.children('tbody').children('tr');
            rows.each(function () {
                var lis = $(this).children('td');
                var startAmount = $(lis.get(1)).text();
                var freightAmount = $(lis.get(2)).text();
                if (type == 1) {
                    avarr.push({ "StartAmount": startAmount, "FreightAmount": freightAmount });
                } else {
                    var payTo = $(lis.get(3)).text();
                    $("#<%= freightCostPayTo.ClientID %> option").each(function () {
                        if ($(this).text() == payTo) {
                            payTo = $(this).val();
                        }
                    });
                    avarr.push({ "StartAmount": startAmount, "FreightAmount": freightAmount, "PayTo": payTo });
                }
                freightStartAmt = startAmount;
            });
            hdJson.val(JSON.stringify(avarr));
            hdFMax.val(freightStartAmt);

            if (rows.length == 0) {
                $('#<%=hdCostPayTo.ClientID %>').val("");
                var tbFStartAmt = undefined;
                if (type == 1) {
                    tbFStartAmt = $('#tbFIStartAmt');
                } else {
                    tbFStartAmt = $('#tbFCStartAmt');
                }
                tbFStartAmt.val('0');
                tbFStartAmt.attr('disabled', 'disabled');
            }
        }

        function freightSwitchIfNoShippingFee() {
            var cb = $(':checkbox[id$=cbx_NoShippingFeeMessage]');
            if (cb.is(":checked")) {
                $('#tableFI').find("tr:gt(0)").remove();
                $(':hidden[id$=hdFreightIncome]').val("");
                $('#tableFC').find("tr:gt(0)").remove();
                $(':hidden[id$=hdFreightCost]').val("");
                $('.freightPanel').slideUp();
                $('.freightPanel').slideUp();
            } else {
                $('.freightPanel').slideDown();
            }
        }

        var isModified = false;
        $(document).ready(function() {
            $(window).bind('beforeunload', function() {
                if (isModified) {
                    return '是否已存檔?確定離開此頁面後不會哭哭??';
                }
            });
            
            $('#IconCategory140').css("display", "none");
            $('#IconCategory141').css("display", "none");
            $('#IconCategoryRealDesc140').css("display", "none");
            $('#IconCategoryRealDesc141').css("display", "none");

            if($(':checkbox[id*=cbx_PEZevent]').is(':checked')) 
            {
                $('#CouponSeparateDigitsShow').css('display', '');
            }

            $('#<%= hidPresentQuantity.ClientID%>').val($('#<%=tbPresentQuantity.ClientID%>').val());
            if($('#<%=tbPresentQuantity.ClientID%>').attr("disabled")=="disabled")
            {
                $('#<%= hidPresentQuantityEnable.ClientID%>').val(0);
            }
            else
            {
                $('#<%= hidPresentQuantityEnable.ClientID%>').val(1);
            }
            
            SetDealLabelDisplay();
            SetBookingSystemDisplay();
            SetFreeDeal();
            CheckBoxLinkBind();
            SetFamiBarcodeDisplay();
            SetSKMDisplay();
            SetSKMDiscount();
            SetShipTypeDisplay();

            $('form').submit(function() {
                $(window).unbind();
            });

            $(document).one('keyup', function() {
                isModified = true;
            });
            $('select, :checkbox').not('select[name$=ddlRreceiveType]').one('change', function() {
                isModified = true;
            });

            CKEDITOR.on('instanceCreated', function(e) {
                e.editor.on('blur', function(e2) {
                    if (e2.editor.checkDirty()) {
                        isModified = true;
                    }
                });
            });

            tmall_rmb_show($('#<%=cbx_TmallDeal.ClientID%>').is(':checked'));

            var availableTags = [<%=SalesManNameList%>];
            
            $('#<%=tbDevelopeSales.ClientID %>').autocomplete({
                source: availableTags
            });
            $('#<%=tbOperationSales.ClientID %>').autocomplete({
                source: availableTags
            });
            setupdatepicker('tbOS', 'tbOE');
            setupdatepicker('tbDS', 'tbDE');

            /*
            * 帶入Menu的D欄位
            */
            <%--var _menuD = $('#<%=tbUse.ClientID%>').val().split("-");
            if(_menuD.length > 0){
                if(_menuD[0] != ""){
                    CKEDITOR.instances.tbMenuD.setData("<p style=\"text-align: center;\">" + _menuD[0] + "</p>");
                }                
            }--%>
            /*
            * 帶入Menu的E欄位
            */

            
            /*
            * 帶入Menu的F欄位
            */
            try{
                //CKEDITOR.instances.tbMenuF.setData("<%=MenuContent%>");
            }catch(e){}



            $('#multiTabs').tabs();

            $('#<%=tbP.ClientID%>').blur(function() {
                calcdiscount();
            });
            $('#<%=tbOP.ClientID%>').blur(function() {
                calcdiscount();
            });
            $('#<%=tbUse.ClientID%>').blur(function() {
                calcdiscount();
            });
            calcdiscount();

            $('#<%=tbP.ClientID%>').add($('#txtQuantityMultiplier')).blur(function() {
                GetDealAveragePrice();
            });
            GetDealAveragePrice();

            $('#images').sortable();
            $('#images').disableSelection();
            $('.dI').button();
            $('.dI').click(function() {
                var cont = confirm('Are you sure you want to delete it?');
                if (cont) {
                    $(this).parent().remove();
                }
            });
            $('#availInfo').dialog({ autoOpen: false, modal: true });
            $('#addInfo').click(function() {
                populateAvailInfo(null, null);
            });
            $('#avpane > ul').live('mouseover mouseout', function(event) {
                if (event.type == 'mouseover') {
                    toggleAvailMode(this, true);
                } else {
                    toggleAvailMode(this, false);
                }
            });
            $('#avpane > ul > .eb').live('click', function() {
                populateAvailInfo($(this).parent(), $(this).parent());
            });
            $('#avpane > ul > .cb').live('click', function() {
                populateAvailInfo(null, $(this).parent());
            });
            $('#avpane > ul > .db').live('click', function() {
                $(this).parent().remove();
            });
            $('#avset').click(function() {
                setAvailInfo($('body').data('avdom'));
            });
            $('#avpane').sortable();
            $('#<%=bSend.ClientID%>').click(serav);
            $('#<%=bDD.ClientID%>').click(function() {
                var cont = confirm('ARE YOU REALLY SURE???');
                if (cont) cont = confirm('ARE YOU REALLY REALLY SURE???');
                return cont;
            });
            var tabsIndex = localStorage.getItem("tabsIndex");
            if (tabsIndex == null) tabsIndex = '0';
            $('#tabs').tabs({ selected: tabsIndex });
            localStorage.setItem("tabsIndex", '0');

            $('fieldset').addClass('ui-corner-all');
            $("button, input:submit, a, input:button", "#container").not('.noBtnUi').button();
            $('#acc_help').click(function() {
                $('#acc_exp').dialog();
            });
            $$('tTitle').maxlength({
                maxCharacters: 50, // Characters limit
                slider: true // True Use counter slider
            });
            $$('tbN').maxlength({
                maxCharacters:<%=DealContentNameLength%>, // Characters limit
                slider: true // True Use counter slider
            });
            $$('tbUse').maxlength({
                maxCharacters: 250, // Characters limit
                slider: true // True Use counter slider
            });
            $$('tbAppTitle').maxlength({
                maxCharacters: 40, // Characters limit
                slider: true // True Use counter slider
            });
            $$('tbTitle').maxlength({
                maxCharacters: 32, // Characters limit
                slider: true // True Use counter slider
            });
            $$('tbDesc').maxlength({
                maxCharacters: 75, // Characters limit
                slider: true // True Use counter slider
            });
            $$('tbAlt').maxlength({
                maxCharacters: 100, // Characters limit
                slider: true // True Use counter slider
            });
            $$('txt_CustomTag').maxlength({
                maxCharacters: 10, // Characters limit
                slider: true // True Use counter slider
            });

            //進貨價
            $('#btnCreateCost').click(function() { costAdd(); });
            $('#btnDeleteCost').click(function() { costDelete(); });
            costShow();

            $('#btnCreateFI').click(function() { freightAdd(1); });
            $('#btnCreateFC').click(function() { freightAdd(2); });
            $('#btnDeleteFI').click(function() { freightDelete(1); });
            $('#btnDeleteFC').click(function() { freightDelete(2); });

            $('[name*=EntrustSell]').change(function() {
                $("input[id$='tbAtm']").removeAttr('disabled');
                $('#spanAtmInfo').text('預設為0，無法使用');

                // 目前一般收據僅提供捐款檔次設定
                if ($(':radio[id*=rbKindReceipt]').is(':checked')) {
                    $(':checkbox[id*=cbx_KindDeal]').attr('checked', 'checked');
                    alert('目前一般收據僅提供捐款檔次設定，將設為捐款檔!!');
                }                
            });

            //發票對開
            $('input[id$=rbMutualNo]').click(function() {
                if ($(this).is(':checked')) {
                    $('input[id$=tbCommission]').attr('disabled', true);
                    $('input[id$=tbCommission]').val(0);
                }
            });
            $('input[id$=rbMutualYes]').click(function() {
                if ($(this).is(':checked')) {
                    $('input[id$=tbCommission]').attr('disabled', false);
                }
            });

            //咖啡寄杯
            $('input[id=channelCategory186]').click(function (){				
                if (!$(this).is(':checked')) {
                    $('input[id$=cbx_DepositCoffee]').attr('checked', 'checked');
                }
            });


            //fami
            $('#<%=rdoFamilyBeaconType.ClientID%>').click(function() {
                if ($('#<%=rdoFamilyBeaconType.ClientID%>').is(":checked")) {
                    $("#<%=cbl_DefaultIcon.ClientID%>").find(".appLimitedEdition").find("input[type=checkbox]").attr('checked', 'checked');
                    $('input[id=cbx_IsAppLimitedEdition]').attr('checked', 'checked');
                }
            });

            //新光0元檔次
            $('input[id=channelCategory185]').click(function() {
                if ($('input[id=channelCategory185]').is(":checked") && $(".editPrice").val() === "0") {
                    $('input[id*=<%=cbx_NoRefund.ClientID%>]').attr('checked', 'checked');
                }
            });
            $(".editPrice").change(function(){
                if ($('input[id=channelCategory185]').is(":checked") && $(".editPrice").val() === "0") {
                    $('input[id*=<%=cbx_NoRefund.ClientID%>]').attr('checked', 'checked');
                }
            });
            //[熟客17來]跟[優惠券]預設disabled (此二頻道for APP)
            $('#channelCategory2160').prop('disabled', 'disabled');
            $('#channelCategoryLabel2160').attr('style', 'color:grey');
            $('#channelCategory2161').prop('disabled', 'disabled');
            $('#channelCategoryLabel2161').attr('style', 'color:grey');
            //即買即用預設disabled(無法設定) (for APP)
            $('#channelCategory100001').prop('disabled', 'disabled');
            $('#channelCategoryLabel100001').attr('style', 'color:grey');
            //最後一天預設disabled(無法設定) (for APP)
            $('#channelCategory100002').prop('disabled', 'disabled');
            $('#channelCategoryLabel100002').attr('style', 'color:grey');

            freightShow(1);
            freightShow(2);

            $(':checkbox[id$=cbx_NoShippingFeeMessage]').click(function() {
                freightSwitchIfNoShippingFee();
            });
            freightSwitchIfNoShippingFee();

            showActivity();
            showCp();
            //store sort
            $('.tblStoreSettings tbody').sortable();
            $('form').submit(function() {
                var newSortIndex = 1;
                var stores = [];
                $('.tblStoreSettings tbody tr').each(function() {
                    var storeIsVisible = $(':checkbox[id$=cbStore]', this).is(':checked');
                    if (storeIsVisible) {
                        $(':hidden[id$=hfNewSortOrder]', this).val(newSortIndex);
                        var jsonObj = {};
                        jsonObj.StoreGuid = $('.bindStoreGuid', this).val();
                        jsonObj.SortOrder = newSortIndex;
                        jsonObj.TotalQuantity = '';
                        jsonObj.ChangedExpireTime = '';
                        jsonObj.UseTime = '';
                        jsonObj.LocationCheck = true;
                        jsonObj.VerifyShopCheck = false;
                        jsonObj.HideVerifyShopCheck = false;
                        jsonObj.VerifyCheck = false;
                        jsonObj.AccountingCheck = false;
                        jsonObj.ViewBalanceSheetCheck = false;
                        jsonObj.HideBalanceSheet = false;
                        stores.push(jsonObj);
                        newSortIndex++;
                    }
                });                
                $('.tblSellerTree tbody tr').each(function() {
                    if ($(':checkbox[id$=cbVerify]', this).is(':disabled') &&
                        !$(':checkbox[id$=cbVerify]', this).is(':checked') &&
                        $(':checkbox[id$=cbAccounting]', this).is(':disabled') &&
                        !$(':checkbox[id$=cbAccounting]', this).is(':checked') &&
                        $(':checkbox[id$=cbViewBalanceSheet]', this).is(':disabled') &&
                        !$(':checkbox[id$=cbViewBalanceSheet]', this).is(':checked') &&
                        $(':checkbox[id$=cbVerifyShop]', this).is(':disabled') &&
                        !$(':checkbox[id$=cbVerifyShop]', this).is(':checked')) {
                        return;
                    }
                    var verifyShopCheck = $(':checkbox[id$=cbVerifyShop]', this).is(':checked');
                    var hideVerifyShopCheck = $(':checkbox[id$=cbHideVerifyShop]', this).is(':checked');
                    var verifyCheck = $(':checkbox[id$=cbVerify]', this).is(':checked');
                    var accountingCheck = $(':checkbox[id$=cbAccounting]', this).is(':checked');
                    var viewBalanceSheetCheck = $(':checkbox[id$=cbViewBalanceSheet]', this).is(':checked');
                    var hideBalanceSheet = $(':hidden[id$=hidIsHideBalanceSheet]', this).val() === 'True';
                    if (verifyCheck || accountingCheck || viewBalanceSheetCheck || hideBalanceSheet || verifyShopCheck) {
                        var index = -1;
                        for(var i = 0; i < stores.length; i++) {
                            if(stores[i].StoreGuid == $('input[id$=hidSellerGuid]', this).val()) {
                                index = i;
                                break;
                            }
                        }
                        if (index < 0) {
                            var jsonObj = {};
                            jsonObj.StoreGuid = $('input[id$=hidSellerGuid]', this).val();
                            if (verifyShopCheck) {
                                jsonObj.TotalQuantity = $('input[id$=txtStoreCount]', this).val();
                                jsonObj.ChangedExpireTime = $('input[id$=txtStoreChangedExpireDate]', this).val();
                                jsonObj.UseTime = $('textarea[id$=txtStoreUseTime]', this).val();
                            }
                            else {
                                jsonObj.TotalQuantity = '';
                                jsonObj.ChangedExpireTime = '';
                                jsonObj.UseTime = '';
                            }
                            jsonObj.LocationCheck = false;
                            jsonObj.VerifyShopCheck = verifyShopCheck;
                            jsonObj.HideVerifyShopCheck = hideVerifyShopCheck;
                            jsonObj.VerifyCheck = verifyCheck;
                            jsonObj.AccountingCheck = accountingCheck;
                            jsonObj.ViewBalanceSheetCheck = viewBalanceSheetCheck;
                            jsonObj.HideBalanceSheet = hideBalanceSheet;

                            stores.push(jsonObj);
                        }
                        else {
                            if (verifyShopCheck) {
                                stores[index].TotalQuantity = $('input[id$=txtStoreCount]', this).val();
                                stores[index].ChangedExpireTime = $('input[id$=txtStoreChangedExpireDate]', this).val();
                                stores[index].UseTime = $('textarea[id$=txtStoreUseTime]', this).val();
                            }
                            stores[index].VerifyShopCheck = verifyShopCheck;
                            stores[index].HideVerifyShopCheck = hideVerifyShopCheck;
                            stores[index].VerifyCheck = verifyCheck;
                            stores[index].AccountingCheck = accountingCheck;
                            stores[index].ViewBalanceSheetCheck = viewBalanceSheetCheck;
                            stores[index].HideBalanceSheet = hideBalanceSheet;
                        }
                    }
                });
                $('#hdStoreSellected').val(JSON.stringify(stores));
            });
            SetBusinessOrderInfo();

            // 旅遊頻道須設定旅遊區域
            $('input[id*=cblC]').each(function() {
                checkTravelCategory(this);
                $(this).bind('click', function() { checkTravelCategory(this); });
            });

            $('#ddlAccBusGroup').change(function() {
                SetTravelSelectItem();
                SetDealLabelDisplay();
                SetGroupCouponDealAppStyle();
            });

            /*
            * 提案單有勾起展"演" ，會有鎖定功能，館別為非宅配即可
            */
            $("#cbx_PromotionDeal").click(function () {
                //ClickPromotionDeal();                
            });

            // 生活商圈
            $('.divCommercial').each(function() {
                var selected = $(this).find('[id*=chklCommercialCategory]').filter(function() { return $(this).is(':checked'); }).length;
                if (selected == 0) {
                    $(this).find('[id*=chkCommercialArea]').attr('checked', false);
                    $(this).find('#CommercialCategoryList').css('display', 'none');
                } else {
                    $(this).find('[id*=chkCommercialArea]').attr('checked', 'checked');
                    $(this).find('#CommercialCategoryList').css('display', '');
                }
            });

            if ($('select[id$=ddlRreceiveType]').val() == '<%=(int)DeliveryType.ToShop %>') {
                //非開賣中的檔次才可以修改出帳方式
                if ("<%=IsDealOpened%>" == "False") {
                    SwitchVendorBillingUI_ToShop();
                    $('input[name$=VendorBilling]').on('change', function(){
                        SwitchVendorBillingUI_ToShop();
                    });
                }
            }
            else if ($('select[id$=ddlRreceiveType]').val() == '<%=(int)DeliveryType.ToHouse %>') {
                //非開賣中的檔次才可以修改出帳方式
                if ("<%=IsDealOpened%>" == "False") {
                    SwitchVendorBillingUI_ToDelivery();
                    $('input[name$=VendorBilling]').on('change', function(){
                        SwitchVendorBillingUI_ToDelivery();
                    });
                }
            }
            
            if ("<%=IsDealOpened%>" == "False" && "<%=CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.SellerMove)%>" == "True") {
                $('input[name=btnMoveSeller]').attr("aria-disabled", false);
                $('input[name=btnMoveSeller]').removeClass("ui-button-disabled ui-state-disabled");
                $('input[name=btnMoveSeller]').removeAttr('disabled');
            }
            else {
                $('input[name=btnMoveSeller]').attr("aria-disabled", true);
                $('input[name=btnMoveSeller]').addClass("ui-button-disabled ui-state-disabled");
                $('input[name=btnMoveSeller]').attr('disabled', true);
            }
                
            $('input.number').numeric();

            $('#chkQuantityMultiplier').add('#chkIsAveragePrice').click(function() {
                if ($(this).is(':checked')) {
                    $('#txtQuantityMultiplier').removeAttr('disabled');
                    if ($('#txtQuantityMultiplier').val() == '') {
                        $('#txtQuantityMultiplier').val('1');
                    }
                    $('#txtQuantityMultiplier').focus();
                } else {
                    if (!$('#chkQuantityMultiplier').is(':checked') && !$('#chkIsAveragePrice').is(':checked')) {
                        $('#txtQuantityMultiplier').attr('disabled', true);
                        $('#txtQuantityMultiplier').val('');
                    }
                }
                GetDealAveragePrice();
            });
            if ($('#txtQuantityMultiplier').val() == '') {
                $('#txtQuantityMultiplier').attr('disabled', true);
                $('#chkQuantityMultiplier').add('#chkIsAveragePrice').removeAttr('checked');
            } else {
                GetDealAveragePrice();
            }
            /*combo*/
            $('#btnSetComboMain').click(function() {
                var bid = '<%=this.BusinessHourId %>';
                var mainBid = $('#ddlComboMain').val();
                $.ajax({
                    type: "POST",
                    url: "Setup.aspx/SetComboDeal",
                    data: "{ mainBid: '" + mainBid + "', subBid: '" + bid + "' }",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function(msg) {
                        window.ajaxSent = false;
                        if (msg.d == false) {
                            alert('系統繁忙中，請稍後再試!!');
                        } else {
                            alert('OK');
                        }
                    },
                    error: function(response, q, t) {
                        if (t == 'userNotLogin') {
                            location.href = '<%=FormsAuthentication.LoginUrl %>';
                        }
                    }
                });
            });
            $('#btnSetComboSub').click(function() {
                var newComboSubBid = $('#ddlComboSub').val();
                var newComboSubTitle = $("option:selected", '#ddlComboSub').attr('linktitle');
                var SubTitle = $("option:selected", '#ddlComboSub').attr('couponusage');
                $("option:selected", '#ddlComboSub').remove();
                renderComboList(newComboSubTitle, '', newComboSubBid, SubTitle);
                updateComboData();
            });

            $('.comboItemDelete, .comboItemNewTitle').live('change', updateComboData); // replace live with on,  if jquery upgrade to new version

            $('#btnComboDealsPreview').click(function() {
                $('#div_combodeallist').find('.mgs-item-box').not('#comboSubPreviewItemEmbryo').remove();
                $('.comboList').find('.comboItem').each(function() {
                    var isDelete = $(this).find('.comboItemDelete').is(':checked');
                    if (isDelete) {
                        return;
                    }
                    var title = $(this).find('.comboItemNewTitle').val();
                    var origPrice = $.trim($(this).find('.combo-op').text());
                    var price = $.trim($(this).find('.combo-price').text());
                    var qty = $.trim($(this).find('.combo-qty').text());
                    var discount = parseInt(parseInt(price, 10) * 10 / parseInt(origPrice, 10), 10);
                    var isAveragePrice = $.trim($(this).find('.combo-is-average-price').text());
                    var mulitQuantity = $.trim($(this).find('.combo-mulit-quantity').text());
                    var averagePrice = '';
                    var template = $('#comboSubPreviewItemEmbryo').clone().removeAttr('id').show();

                    //fix empty data
                    if (origPrice == null || origPrice.length == 0) {
                        origPrice = '___';
                    }
                    if (price == null || price.length == 0) {
                        price = '___';
                    }
                    if (qty == null || qty.length == 0) {
                        qty = '_';
                    }
                    if (title == null || title.length == 0) {
                        title = '[未設定]';
                    }
                    if (isNaN(discount)) {
                        discount = '_';
                    }
                    if (isAveragePrice === 'false' || (mulitQuantity == null || mulitQuantity.length == 0)) {
                        averagePrice = '';
                    } else {
                        if (price > 0 && mulitQuantity > 0) {
                            if (mulitQuantity == 1) {
                                averagePrice = '均價 ' + isAveragePrice;
                            } else {
                                averagePrice = '均價 ' + (Math.ceil(price / mulitQuantity));
                            }
                        }
                    }
                    template.find('.mgs-item-title').text(title);
                    template.find('.mgs-sale').text('原價 $' + origPrice);
                    //template.find('.mgs-buyer').text(qty + '人已購買');
                    template.find('.mgs-price').text('$' + price);
                    template.find('.rdl-a2wdis').text(discount + '折');
                    template.find('.mgs-brcolor-org').text(averagePrice);
                    template.appendTo($('#div_combodeallist'));
                });
                showcombodeals();

            });

            

            function renderComboList(newComboSubLinkTitle, comboSubTitle, comboSubBid, SubTitle) {
                var row = $('<tr></tr>').attr('bid', comboSubBid).addClass('comboItem').appendTo($('.comboList > tbody'));
                var td1 = $('<td></td>').appendTo(row);
                var td2 = $('<td></td>').appendTo(row);
                var td3 = $('<td></td>').appendTo(row);
                var td4 = $('<td></td>').appendTo(row);
                var td5 = $('<td></td>').appendTo(row);
                $('<input type=checkbox />').addClass('comboItemDelete').appendTo(td1);
                $('<a></a>').attr('href', 'setup.aspx?bid=' + comboSubBid)
                    .text(newComboSubLinkTitle).addClass('comboItemLink').appendTo(td2);
                var t1 = $('<input type=text />').appendTo(td3).val(comboSubTitle);
                t1.addClass('comboItemNewTitle').attr('value', ''+ SubTitle +'').attr('placeholder', '請輸入顯示名稱');
            }

            $('.comboList tbody').sortable({ appendTo: document.body });
            $('.comboList').bind("sortupdate", updateComboData);

            function updateComboData() {
                var subDeals = [];
                $('.comboItem', '.comboList').each(function() {
                    var isDelete = $(this).find('.comboItemDelete').is(':checked');
                    if (isDelete) {
                        return;
                    }
                    var bid = $(this).attr('bid');
                    var title = $(this).find('.comboItemNewTitle').val();
                    subDeals.push({ 'title': title, 'bid': bid });
                });
                var mainDeal = {
                    'title': $('a', '.comboMainTitle').text(),
                    'bid': $('.comboMainTitle').attr('bid')
                }; //comboMainTitle
                $('#hidComboData').val(JSON.stringify({ 'mainDeal': mainDeal, 'subDeals': subDeals }));
            }

            $('#txtAdvanceReservationDays').numeric();
            $('#txtCouponUsers').numeric();


            //處理選取屬性的設定
            var selectedArray = $('#<%=hiddenDealCategory.ClientID%>').val();
            $.each(selectedArray, function(j, obj) {
                    if (obj.checked) {
                        $('#dealCategory' + obj).attr('checked', true);
                    }
                }
            );
            <%--處理預設選取的資料 --%>
            ShowAllCategory();
            <%--依據檔次取貨類型調整--%>
            ShowIconByDeliveryType();
            <%--MoveDealCategoryItem --%>
            MoveDealCategoryItem();

            SetCKEditor();
                
            /*
                * 子檔同步項目
                */
            $("#<%=cbx_SaveComboDeals_m.ClientID%>").click(function(){
                var __checked = $(this).context.checked;
                if(__checked){
                    //勾選子檔同步項目
                    $("#p_SaveComboDeals_m").show();
                    $('input[id*=SaveComboDeals]').attr('checked', 'checked');

                    var lockArray = [];

                    //活動搶購時間(迄日)
                    if( $("#<%=tbOE.ClientID%>").attr("disabled")=="disabled"){
                        lockArray.push('<%=cbx_SaveComboDeals_OE.ClientID%>');
                    };

                    //業務
                    if($("#<%=tbDevelopeSales.ClientID%>").attr("disabled")=="disabled"){
                        lockArray.push('<%=cbx_SaveComboDeals_Sales.ClientID%>');

                        //核銷與帳務設定一併鎖住不能更新(上檔跟結檔)
                        lockArray.push('<%=cbx_SaveComboDeals_PaySet.ClientID%>');                         
                    }
                    $.each(lockArray,function(idx,val){
                        $('input[id=' + val + ']').attr('checked', false);
                        $('input[id=' + val + ']').attr('disabled','disabled');
                    });                        
                }else{
                    //取消勾選子檔同步項目
                    $("#p_SaveComboDeals_m").hide();
                    $('input[id*=SaveComboDeals]').removeAttr('checked');
                }                    
            });


            $('input[id=<%=cbx_SaveComboDeals_m.ClientID%>]').attr('checked', false);


            //輸入接續數量BID時，把checkbox也打勾
            $("#<%=AncestorBid.ClientID%>").blur(function(){
                if($.trim($(this).val()) != ""){
                    $("#<%=cbx_Is_Continued_Quantity.ClientID%>").attr("checked",true);
                }else{
                    $("#<%=cbx_Is_Continued_Quantity.ClientID%>").attr("checked",false);
                }
            });
            //輸入接續憑證BID時，把checkbox也打勾
            $("#<%=AncestorSeqBid.ClientID%>").blur(function(){
                if($.trim($(this).val()) != ""){
                    $("#<%=cbx_Is_Continued_Sequence.ClientID%>").attr("checked",true);
                }else{
                    $("#<%=cbx_Is_Continued_Sequence.ClientID%>").attr("checked",false);
                }
            });
            $("#<%=AncestorBid.ClientID%>").keyup(function(){
                if($.trim($(this).val()) != ""){
                    $("#<%=cbx_Is_Continued_Quantity.ClientID%>").attr("checked",true);
                }else{
                    $("#<%=cbx_Is_Continued_Quantity.ClientID%>").attr("checked",false);
                }
            });
            //輸入接續憑證BID時，把checkbox也打勾
            $("#<%=AncestorSeqBid.ClientID%>").keyup(function(){
                if($.trim($(this).val()) != ""){
                    $("#<%=cbx_Is_Continued_Sequence.ClientID%>").attr("checked",true);
                }else{
                    $("#<%=cbx_Is_Continued_Sequence.ClientID%>").attr("checked",false);
                }
            });
            
            //宅配公益檔次(賣=進)預設不做分期
            $('input[id$=tbP],input[id$=txtPurchasePrice]').change(function(){
                if ($('select[id$=ddlRreceiveType]').val() == '<%=(int)DeliveryType.ToHouse %>'){
                    var itemPrice = $('input[id$=tbP]').val();
                    var purchasePrice = $('input[id$=txtPurchasePrice]').val();
                    var notInstallment = $('input[id$=cbx_DonotInstallment]');
                    if (itemPrice > 0 && itemPrice == purchasePrice && !notInstallment.is(':checked')){
                        notInstallment.attr('checked','checked');
                        alert('已關閉分期');
                    }
                }
            });
            
            PiinLifeOnChecked();

            SwitchSpecialRequirement();

            getMoreStore(100);
            getMoreSellerTree(10000);

            //商品寄倉
            if ('<%=Consignment%>'=="True") {
                $(':radio[id$=rbOthers]').prop('disabled', true);
                $(':radio[id$=rbWeekly]').prop('disabled', true);
                $(':radio[id$=rbFlexible]').prop('disabled', true);
            }


            $('input[id="<%=cbx_Consignment.ClientID%>"]').click(function () {

                //提案單的寄倉設定
                var pro = $('#<%= hidProposalConsignment.ClientID%>').val();
                


                if ($('input[id="<%=cbx_Consignment.ClientID%>"]').prop("checked")) {
                    if (pro == "False")
                        alert("提案單檔次類型與本檔設定不符，請協助確認是否前後端是否有設定錯誤。");
                    
                    $(':radio[id$=rbMonthly]').prop('checked', true);

                    $(':radio[id$=rbOthers]').prop('disabled', true);
                    $(':radio[id$=rbWeekly]').prop('disabled', true);
                    $(':radio[id$=rbFlexible]').prop('disabled', true);
                }
                else {
                    if (pro == "True")
                        alert("提案單檔次類型與本檔設定不符，請協助確認是否前後端是否有設定錯誤。");

                    $(':radio[id$=rbOthers]').prop('disabled', false);
                    $(':radio[id$=rbWeekly]').prop('disabled', false);
                    $(':radio[id$=rbMonthly]').prop('disabled', false);
                    $(':radio[id$=rbFlexible]').prop('disabled', false);
                }
            });

            if ('<%=config.IsRemittanceFortnightly%>' == 'True' && '<%=IsAgreeHouseNewContractSeller%>' == 'True' && '<%=DeptId%>' == '<%=EmployeeChildDept.S010.ToString()%>')
            {
                //24HR預設雙周出帳
                //非24HR無法選雙周
                $('[name*=ShipType]').change(function(){
                    if ($('#rboFast').is(':checked'))
                    {
                        $(':radio[id$=rbFortnightly]').prop('disabled', false);
                        $(':radio[id$=rbFortnightly]').prop('checked', true); 
                    }
                    else{
                        $(':radio[id$=rbFortnightly]').prop('disabled', true);
                        $(':radio[id$=rbMonthly]').prop('checked', true); 
                    }

                })
            }
            
        }); // the end of document ready	. if u append script insinde , plz ensure all are work.

        //憑證出帳設定
        function SwitchVendorBillingUI_ToShop(){
            if($(':radio[id$=rbBMBalanceSheetSystem]').is(':checked'))
            {
                //新版核銷對帳系統
                if ('<%=HasRemittanceTypeSetRight %>' == 'True') {
                    //選舊出帳方式要有權限
			        $(':radio[id$=rbAchWeekly]').prop('disabled', false);
			        $(':radio[id$=rbManualWeekly]').prop('disabled', false);
			        $(':radio[id$=rbManualMonthly]').prop('disabled', false);
			        $(':radio[id$=rbPartially]').prop('disabled', false);
		        }else {
		            $(':radio[id$=rbAchWeekly]').prop('disabled', true);
		            $(':radio[id$=rbManualWeekly]').prop('disabled', true);
		            $(':radio[id$=rbManualMonthly]').prop('disabled', true);
		            $(':radio[id$=rbPartially]').prop('disabled', true);
		        }


                //新出帳方式
		        if ('<%=config.IsRemittanceFortnightly%>' == 'True')
		        {
		            $(':radio[id$=rbFortnightly]').prop('disabled', true);
		            $(':radio[id$=rbMonthly]').prop('disabled', false);
		            $(':radio[id$=rbFlexible]').prop('disabled', false);
		            $(':radio[id$=rbOthers]').prop('disabled', false);


		            if ('<%=IsAgreePponNewContractSeller%>' == 'True')
		            {
		                $(':radio[id$=rbWeekly]').prop('disabled', true);
		            }
		            else
		            {
		                $(':radio[id$=rbWeekly]').prop('disabled', false);
		            }

		        }
		        else
		        {
		            $(':radio[id$=rbWeekly]').prop('disabled', false);
		            $(':radio[id$=rbMonthly]').prop('disabled', false);
		            $(':radio[id$=rbFlexible]').prop('disabled', false);
		            $(':radio[id$=rbOthers]').prop('disabled', false);
		        }
             }
	        else if($(':radio[id$=rbBMMohistSystem]').is(':checked'))
	        {
	            //墨攻核銷對帳系統
		        $(':radio[id$=rbAchWeekly]').prop('disabled', true);
		        $(':radio[id$=rbManualWeekly]').prop('disabled', true);
		        $(':radio[id$=rbManualMonthly]').prop('disabled', true);
		        $(':radio[id$=rbPartially]').prop('disabled', true);
		        $(':radio[id$=rbWeekly]').prop('disabled', true);
		        $(':radio[id$=rbMonthly]').prop('disabled', true);
		        $(':radio[id$=rbFlexible]').prop('disabled', true);
		        $(':radio[id$=rbOthers]').prop('checked', true);

		        if ('<%=config.IsRemittanceFortnightly%>' == 'True')
                {
                    $(':radio[id$=rbFortnightly]').prop('disabled', true);
                }
            }
        }

        //宅配出帳設定
        function SwitchVendorBillingUI_ToDelivery(){
            if($(':radio[id$=rbBMBalanceSheetSystem]').is(':checked'))
	        {
                //新版核銷對帳系統
                if ('<%=HasRemittanceTypeSetRight %>' == 'True') {
	                //選舊出帳方式要有權限
		            $(':radio[id$=rbPartially]').prop('disabled', false);
		        }
		        else {
		            $(':radio[id$=rbPartially]').prop('disabled', true);
		        }
		        $(':radio[id$=rbAchWeekly]').prop('disabled', true);//宅配不能選
		        $(':radio[id$=rbManualWeekly]').prop('disabled', true);//宅配不能選
		        $(':radio[id$=rbManualMonthly]').prop('disabled', true);//宅配不能選


                //新出帳方式
		        if ('<%=config.IsRemittanceFortnightly%>' == 'True')
		        {
		            $(':radio[id$=rbMonthly]').prop('disabled', false);
		            $(':radio[id$=rbOthers]').prop('disabled', false);


		            if ('<%=DeptId%>' == '<%=EmployeeChildDept.S010.ToString()%>')
		            {
		                //一般宅配
		                if ('<%=IsAgreeHouseNewContractSeller%>' == 'True')
		                {
		                    $(':radio[id$=rbWeekly]').prop('disabled', true);
		                    $(':radio[id$=rbFlexible]').prop('disabled', true);
		                    if (!$('#rboFast').is(':checked'))
		                    {
		                        $(':radio[id$=rbFortnightly]').prop('disabled', true);
		                    }
		                    else
		                    {
		                        $(':radio[id$=rbFortnightly]').prop('disabled', false);
		                    }
		                }
		                else
		                {
		                    $(':radio[id$=rbWeekly]').prop('disabled', false);
		                    $(':radio[id$=rbFlexible]').prop('disabled', false);
		                    $(':radio[id$=rbFortnightly]').prop('disabled', true);
		                }
		            }
		            else
		            {
		                //旅遊/品生活宅配
		                $(':radio[id$=rbFortnightly]').prop('disabled', true);
		                $(':radio[id$=rbFlexible]').prop('disabled', false);
                        if ('<%=IsAgreePponNewContractSeller%>' == 'True')
                        {
                            $(':radio[id$=rbWeekly]').prop('disabled', true);
                        }   
                        else
                        {
                            $(':radio[id$=rbWeekly]').prop('disabled', false);
                            
                        }
		            }                    
                }
		        else
		        {
		            $(':radio[id$=rbMonthly]').prop('disabled', false);
		            $(':radio[id$=rbOthers]').prop('disabled', false);
                    $(':radio[id$=rbWeekly]').prop('disabled', false);
                    $(':radio[id$=rbFlexible]').prop('disabled', false);
                    
                }
	        }
	        else if($(':radio[id$=rbBMMohistSystem]').is(':checked'))
	        {
	            //墨攻核銷對帳系統
		        $(':radio[id$=rbAchWeekly]').prop('disabled', true);
		        $(':radio[id$=rbManualWeekly]').prop('disabled', true);
		        $(':radio[id$=rbManualMonthly]').prop('disabled', true);
		        $(':radio[id$=rbPartially]').prop('disabled', true);
		        $(':radio[id$=rbWeekly]').prop('disabled', true);
		        $(':radio[id$=rbMonthly]').prop('disabled', true);
		        $(':radio[id$=rbFlexible]').prop('disabled', true);
		        $(':radio[id$=rbOthers]').prop('checked', true); 
		        if ('<%=config.IsRemittanceFortnightly%>' == 'True')
                {
                    $(':radio[id$=rbFortnightly]').prop('disabled', true);
                }
	        }
        }

        function ClickPromotionDeal() {
            ShorOrHideReserveCoupon();
        }
        function SetTravelSelectItem() {
            if ($('#ddlAccBusGroup').val() == '<%=(int)BusinessGroup.Travel %>'
                && $('select[id$=ddlRreceiveType]').val() == '<%=(int)DeliveryType.ToShop %>'
                && '<%= Request["bid"] %>' == '') {
                $(':radio[id$=rbManualMonthly]').attr('checked', true);
            }
            ShorOrHideReserveCoupon();            
        }

        function ShorOrHideReserveCoupon() {
            if (($('#ddlAccBusGroup').val() == '<%=(int)BusinessGroup.Travel %>') && ($('select[id$=ddlRreceiveType]').val() == '<%=(int)DeliveryType.ToShop %>')
                || ($("#cbx_PromotionDeal").is(":checked") && ($('#ddlAccBusGroup').val() != '<%=(int)BusinessGroup.Product %>') && ($('select[id$=ddlRreceiveType]').val() == '<%=(int)DeliveryType.ToShop %>'))) {
                $('#fReserveLock').show();
                
            } else {
                $('#fReserveLock').hide();
                $('#<%=cbIsReserveLock.ClientID%>').attr("checked", false);
            }
        }

        function formDataCheck() {

            if($("#<%=cbx_SaveComboDeals_m.ClientID%>").attr("checked")){
                var comboxSaveDeals = $("#p_SaveComboDeals_m input:checkbox:checked").map(function(){
                    return $(this).val();
                }).get(); 

                if(comboxSaveDeals.length==0){
                    alert("子項同步已勾選，請選擇同步項目！");
                    return false;
                } 
                if(!confirm("勾選項目將直接以母檔資料直接覆蓋子檔，請再次確認")){
                    return false;
                }               
            }
            
            if ($('#<%=hiddenChannelChecked.ClientID%>').val() == '[]') {
                $('#<%=hiddenChannelChecked.ClientID%>').focus();
                alert('未勾選上檔頻道！');
                return false;
            }

            if ($('textarea[id$=tbN]').val() == '') {
                alert('方案說明未填。');
                $('textarea[id$=tbN]').focus();
                return false;
            }
            if ($('#<%=tbUse.ClientID%>').val() == '') {
                alert('訂單短標未填。');
                $('#<%=tbUse.ClientID%>').focus();
                return false;
            }
            var originalPrice = parseInt($(':text[id$=tbOP]').val(), 10);
            if (isNaN(originalPrice)) {
                alert('價格資訊裡的"原價"未填或不為數字。');
                $(':text[id$=tbOP]').focus();
                return false;
            }
            var price = parseInt($(':text[id$=tbP]').val().replace(',',''), 10);
            if (isNaN(price)) {
                alert('價格資訊裡的"賣價"未填或不為數字。');
                $(':text[id$=tbP]').focus();
                return false;
            }
            var purchasePrice = parseInt($($('#tableCost tbody tr td.multiCost')[0]).text());
            if (price <= purchasePrice){
                if (!confirm('進貨價大於/等於賣價，請再次確認資訊是否正確？')){
                    $(':text[id$=tbP]').focus();
                    return false;
                }
            }
            if ($('#chkQuantityMultiplier').is(':checked')) {
                var qty = parseInt($('#txtQuantityMultiplier').val(), 10);
                if (isNaN(qty)) {
                    alert('勾選銷售倍數，必需填寫組合數。');
                    $('#txtQuantityMultiplier').focus();
                    return false;
                }
                if (qty < 1) {
                    alert('組合數至少要>=1');
                    $('#txtQuantityMultiplier').focus();
                    return false;
                }
            }
            if ($('#chkIsAveragePrice').is(':checked')) {
                var qty = parseInt($('#txtQuantityMultiplier').val(), 10);
                if (isNaN(qty)) {
                    alert('勾選均價，必需填寫組合數。');
                    $('#txtQuantityMultiplier').focus();
                    return false;
                }
                if (qty < 1) {
                    alert('組合數至少要>=1');
                    $('#txtQuantityMultiplier').focus();
                    return false;
                }
            }
            if ($('#cbxCouponSeparateDigits').is(':checked')) {
                var qty = parseInt($('#tbCouponSeparateDigits').val(), 0);
                if (isNaN(qty)) {
                    alert('勾選自訂簡訊格式，必需填寫分隔數。');
                    $('#tbCouponSeparateDigits').focus();
                    return false;
                }
                if (qty < 1) {
                    alert('分隔數至少要>=1');
                    $('#tbCouponSeparateDigits').focus();
                    return false;
                }
            }

            //輸入>0的傭金, 則進貨價與賣價必須相同. 如果有多筆進貨價, 以最下面一筆(最新)為準
            if ($("#<%= tbCommission.ClientID %>").val() > 0 && $('#tableCost .multiCost').last().text() != $('#<%=tbP.ClientID %>').val().replace(",", "")) {
                alert("您已輸入發票對開傭金, 則進貨價與賣價必須相同!");
                return false;
            }

            //館別不是選P旅遊, 又要輸入傭金的話, 要跳出詢問視窗
            if ($("#<%= ddlAccBusGroup.ClientID %> option:selected").val() != 5 && $("#<%= tbCommission.ClientID %>").val() > 0) {
                if (!window.confirm("館別並非選擇P旅遊, 您確定還是要輸入傭金嗎?")) {
                    return false;
                }
            }

            //消費方式
            var deliverytype = $("#<%=ddlRreceiveType.ClientID%> option:selected").val();
            if (deliverytype == "") {
                alert("請輸入消費方式。");
                return false;
            } else {
                $(this).attr("href", $(this).attr("href"));
            }
            //送單前商圈‧景點檢查
            var isHasRegion = false;
            var checkNumber = 0;
            $('tr[id^="specialRegion"]').each(function() {
                if ($(this).is(":visible")) {
                    isHasRegion = true;
                    checkNumber += $(this).find('input[type="checkbox"][id^="subChannelSpecialRegion"]:checked').length;
                }
            });

            if (isHasRegion && checkNumber<=0) {
                if (!confirm("此檔未勾選商圈喔!確定要存檔?")) {
                    return false;
                }
            }

            //送單前進貨價列表驗證
            var isCostZero = false;
            if ($('#<%=txtPurchasePrice.ClientID%>').val() == '') {
                alert("請輸入進貨價資料。");
                $('#<%=txtPurchasePrice.ClientID%>').focus();
                return false;
            }
            
            if ($('#<%=txtPurchasePrice.ClientID%>').val() == 0)
            {
                isCostZero = true;
            }

            if ($('#<%=txtSlottingFeeQuantity.ClientID%>').val() != 0 && isCostZero)
            {
                alert("有上架費份數，進貨價不可為0。");
                $('#<%=txtPurchasePrice.ClientID%>').focus();
                return false;
            }

            if (isCostZero) {
                if (!confirm("此檔次進貨價為0，確認儲存?")) {
                    return false;
                }
            }
            /*
                *接續設定檢查
                */
            //數量
            if ($.trim($('[id*=AncestorBid]').val()) == '' &&  $('input[id*=cbx_Is_Continued_Quantity]').is(':checked') ) {
                alert('接續設定必須輸入接續檔次bid!!');
                return false;
            }
            //憑證
            if ($.trim($('[id*=AncestorSeqBid]').val()) == '' && $('input[id*=cbx_Is_Continued_Sequence]').is(':checked') ) {
                alert('接續設定必須輸入接續檔次bid!!');
                return false;
            }
             //設定成套販售跟卻不是多重選項檔次的檢查
            if ($('#cbx_ShoppingCar').is(':checked') == false && parseInt($('#tbCPC').val(), 10) > 1) {
                alert('沒有勾選多重選項，卻設定了成套販售!!');
                return false;
            }

            // 檢查多重選項檔次是否沒有輸入選項
            if ($('#cbx_ShoppingCar').is(':checked') == true && $('#<%= txtMult.ClientID %>').val() == '') {
                var cf = confirm("注意！您勾選使用多重選項，但未設定多重選項內容，您確定要儲存？");
                if (!cf) {
                    return false;
                }
            }

            // 檢查多重選項是否含有小括號或雙引號
            if (/[()",]/.exec($('#<%= txtMult.ClientID %>').val())!=null){
                alert('多重選項含有半形小括號、逗號、雙引號，建議改為全形避免產生例外狀況');
                return false;
            }

            //檢查0元活動頁大圖連結
            if ($(".editPrice").val() == "0") {
                $(".editFreeDeal").show(); 
            }

            //檢查URL是否正確
            if ($(".editPrice").val() == "0" && ($(".editActivityUrl").val().trim())) {
                var patternVal = /(http|ftp|https):\/\/[\w\-_]+(\.[\w\-_]+)+([\w\-\.,@?^=%&amp;:/~\+#]*[\w\-\@?^=%&amp;/~\+#])?/;
                var activityUrlValue = $(".editActivityUrl").val();
                if (!activityUrlValue.match(patternVal)) {
                    $('#tabs').tabs({ selected: 1 });
                    localStorage.setItem("tabsIndex", '1');
                    $(".ActivityUrlError").show();
                    alert("0元檔活動完成頁大圖連結網址格式錯誤!");
                    return false;
                }
                else { $(".ActivityUrlError").hide(); }
            }

            //預約系統設定檢查
            if ($('input[id*=cb_IsUsingbookingSystem]').is(':checked')) {
                if (!$("#txtAdvanceReservationDays").val() || $("#txtAdvanceReservationDays").val() == "0") {
                    alert('消費者提早預約天數至少需填1天!!');
                    $("#txtAdvanceReservationDays").focus();
                    return false;
                }
                if ($('input[id*=rdoMultiUsers]').is(':checked')) {
                    if (!isNumber($("#txtCouponUsers").val()) || !$("#txtCouponUsers").val() || $("#txtCouponUsers").val() == "0") {
                        alert('多人優惠時請設定本次優惠最多幾人使用(不可為0)!!');
                        $("#txtCouponUsers").focus();
                        return false;
                    }
                }
            }

            //日期判斷
            if (isDate($('#tbOS').val()) == false) {
                alert("必須輸入活動搶購時間。");
                $('#tbOS').focus();
                return false;
            }
            if (isDate($('#tbOE').val()) == false) {
                alert("必須輸入活動搶購時間。");
                $('#tbOE').focus();
                return false;
            }
            if (isDate($('#tbDS').val()) == false) {
                alert('配送時間未填寫正確');
                $('#tbDS').focus();
                return false;
            }
            if (isDate($('#tbDE').val()) == false) {
                alert('配送時間未填寫正確');
                $('#tbDE').focus();
                return false;
            }
            if (new Date($('#tbDS').val()).valueOf() < new Date($('#tbOS').val()).valueOf()) {
                alert('無法存檔！\n《兌換/配送時間 起始日》，不能早於《活動搶購期間 起始日》，請協助確認與調整。');
                $('#tbDS').focus();
                return false;
            }
            if ($('#formChangedExpireDate').val()!='')
            {
                var oriChangeExpireDate = new Date('<%=ChangedExpireDate%>'.split(' ')[0]);
                oriChangeExpireDate = oriChangeExpireDate.getFullYear() + '/' + padLeft((oriChangeExpireDate.getMonth() + 1), 2) + '/' + padLeft(oriChangeExpireDate.getDate(), 2);

                if (new Date($('#formChangedExpireDate').val()).valueOf() < new Date($('#tbOE').val()).valueOf() && '<%=IsSetExpireDateNoLimit%>' =="False" && oriChangeExpireDate!=$('#formChangedExpireDate').val()) {
                    alert('無法存檔！\n《調整有效期限截止日》，不能早於《活動搶購期間 截止日》，請協助確認與調整。');
                    $('#formChangedExpireDate').focus();
                    return false;
                }
            }
            else
            {
                if (new Date($('#tbDE').val()).valueOf() < new Date($('#tbOE').val()).valueOf()) {
                    alert('無法存檔！\n《兌換/配送時間 截止日》，不能早於《活動搶購期間 截止日》，請協助確認與調整。');
                    $('#tbDE').focus();
                    return false;
                }
            }
            

            //
            if ($('#tbOMin').val()=='') {
                alert('最低門檻未填寫');
                $('#tbOMin').focus();
                return false;
            }
            if ($('#tbDevelopeSales').val() == '') {
                alert('業務1未填寫');
                $('#tbDevelopeSales').focus();
                return false;
            }

            //檢查區域有無選取
            var areaSelected = true;
            var errorChannel = '';
            $.each(categoryKeyArray, function(j, obj) {
                    var channelBox = $('#channelCategory' + obj);
                    var channelLabel = $('#channelCategoryLabel' + obj);
                    var kids = $("#subChannelCol" + obj + " input[type='checkbox']");
            
                    if (channelBox.is(':checked')) {
                        if (kids.length > 0) {
                            <%--有選取--%>
                            var checkArea = false;
                            <%--判斷子項目是否有選取--%>
                            $.each(kids, function(h, subArea) {
                                if (subArea.checked) {
                                    checkArea = true;
                                }
                            });

                            if (checkArea == false) {
                                errorChannel = channelLabel.text().trim();
                                areaSelected = false;
                                return;
                            }
                        }
                    }
                }
            );

            if (!areaSelected) {
                alert('請勾選' + errorChannel + '區域');
                return false;
            }

            if ($('#hdDealTypeNew').val() == "0" || $('#hdDealTypeNew').val() == "")
            {
                alert('請選擇銷售分析的分類!');
                $('#dealTypeContainer select').eq(0).focus();
                $('#dealTypeContainer').css('border-left', 'solid 5px red').delay(2000)
                    .queue(function (next) {
                        $(this).css('border', '');
                        next();
                    });
                return false;
            }
            
            <%if (pnMainShoppingCartSetting.Visible) { %> 
            if (typeof $('input[name$="rbl_ShoppingCartOptions"]:checked').val() === 'undefined')
            {
                alert('請選擇購物車設定!');
                return false;
            }
            <%}%>

            if ($('input[id=channelCategory185]').is(":checked") && $(".editPrice").val() === "0" && !($('input[id*=<%=cbx_NoRefund.ClientID%>]').is(":checked"))) {
                alert('新光三越0元檔次，請勾選「不接受退貨」!');
                return false;
            }

            if ($('#<%=cbx_setting_display.ClientID %>').is(':checked'))
            {
                var index = $("input[id^='<% =rblDiscountType.ClientID %>']:checked").val();
                var value = $("input[id^='txtDiscount'][tabindex="+ index +"]").val();
                $('#<%=hdDiscount.ClientID %>').val(value);
            }

            var atmQty = $("input[id*=tbAtm]").val();
            if (checkNonATMDeal() && atmQty != "0") {
                alert("此檔次類型不適用ATM付款");
                setATMQtyWithSettingDealType();
                return false;
            }

            var cbx_PromotionDeal = $("#cbx_PromotionDeal");
            if ($("#<%=chkNoRefundBeforeDays.ClientID%>").is(":checked")) {                
                if (!$(cbx_PromotionDeal).is(":checked")) {
                    if (!confirm("請確認是否為展演檔次?\r\n\r\n您勾選了「演出時間前十日內不能退貨」，\r\n若為「展演檔次」，\r\n請勾選「展演檔次-鎖定功能(逾期自動核銷)」")) {
                        return false;
                    }
                }
                if (!$('#<%=cbIsReserveLock.ClientID%>').is(":checked")) {
                    if (!confirm("鎖定功能未開啟，建議展演票券開啟鎖定功能，是否要繼續存檔?。")) {
                        return false;
                    }
                }
            }
            if ($(cbx_PromotionDeal).is(":checked")) {
                if (!$("#<%=chkNoRefundBeforeDays.ClientID%>").is(":checked")) {
                    alert("您勾選「展演檔次-鎖定功能(逾期自動核銷)」，\r\n請同步勾選「演出時間前十日內不能退貨」");
                    return false;
                }
            }
            var cbx_ExhibitionDeal = $("#cbx_ExhibitionDeal");
            if ($("#<%=cbx_ExpireNoRefund.ClientID%>").is(":checked")) {                
                if (!$(cbx_ExhibitionDeal).is(":checked")) {
                    if (!confirm("請確認是否為展覽檔次?\r\n\r\n您勾選了「過期不能退貨」，\r\n若為「展覽檔次」，\r\n請勾選「展覽檔次(逾期自動核銷)」")) {
                        return false;
                    }
                }
            }

            if ($(cbx_ExhibitionDeal).is(":checked")) {
                if (!$("#<%=cbx_ExpireNoRefund.ClientID%>").is(":checked")) {
                    alert("您勾選「展覽檔次(逾期自動核銷)」，\r\n請同步勾選「過期不能退貨」");
                    return false;
                }
            }
            
            

            if ('<%=config.IsRemittanceFortnightly%>' == 'True')
            {
                //save前,出帳disabled先開啟
                $(':radio[id$=rbWeekly]').prop('disabled', false);
                $(':radio[id$=rbFlexible]').prop('disabled', false);
                $(':radio[id$=rbFortnightly]').prop('disabled', false);
            }
            
            //save前,不使用折價disabled先開啟
            $("#<%=cbx_NotAllowedDiscount.ClientID%>").attr("disabled", false);
            
        }

        function isNumber(n) {
            return !isNaN(parseFloat(n)) && isFinite(n);
        }

        function isDate(dateStr) {
            if (dateStr == null || dateStr == '') return false;
            var parts = dateStr.split('/');
            if (parts.length != 3) return false;
            var accDate = new Date(dateStr);
            if (parseFloat(parts[0]) == accDate.getFullYear()
                && parseFloat(parts[1]) == accDate.getMonth() + 1
                && parseFloat(parts[2]) == accDate.getDate()) {
                return true;
            }
            return false;
        }

        function showActivity() {
            //if ($('.showUl input').attr('checked')) {
            //    $('#sc').css('display', '');
            //} else {
            //    $('#sc').css('display', 'none');
            //}
            if ($('.showUl input').attr('checked')) {
                $('#CPCSetting').css('display', '');
            } else {
                $('#CPCSetting').css('display', 'none');
            }
        }

        function showCp() {
            if ($('#cbx_GroupCoupon').attr('checked')) {
                $('#cp').css('display', '');
                $('#<%=divPdfItemName.ClientID%>').css('display', '');
                $('#<%=ddlGroupCouponType.ClientID%>').css('display', '');
                $('#<%=rbBMMohistSystem.ClientID%>').attr('checked',false);
                $('#<%=rbBMMohistSystem.ClientID%>').attr('disabled','disabled');
                $('#<%=rbBMBalanceSheetSystem.ClientID%>').attr('checked',true);
                $('#<%=cbl_DefaultIcon.ClientID%>').find('input[type=checkbox][value='+ <%=(int)DealLabelSystemCode.GroupCoupon%> +']').attr('checked',true);
            } else {
                $('#cp').css('display', 'none');
                $('#<%=divPdfItemName.ClientID%>').css('display', 'none');
                $('#<%=ddlGroupCouponType.ClientID%>').css('display', 'none');
                $('#<%=rbBMBalanceSheetSystem.ClientID%>').removeAttr('disabled');
                $('#<%=cbl_DefaultIcon.ClientID%>').find('input[type=checkbox][value='+ <%=(int)DealLabelSystemCode.GroupCoupon%> +']').attr('checked',false);
            }
        }

        function bClick() {
            if ($('.showdiv input').attr('checked')) {
                $('#openitem').css('display', '');
            }
            else {
                $('#openitem').css('display', 'none');
            }
        }

        function copyeditor() {
            a_editor.setData(b_editor.getData());
        }

        function selectAll(chk, chkclass) {
            chkclass = "." + chkclass;
            $(chkclass + " > :checkbox").each(function () {
                if ($(this).is(':disabled'))
                {
                    return;
                }
                if ("checked" == $(chk).attr("checked")) {
                    if (chkclass == ".checkVerifyShop") {
                        var isToHouse = $('select[id$=ddlRreceiveType]').val() == '<%=(int)DeliveryType.ToHouse %>';
                        if (isToHouse || $('input[id$=cbx_NoRestrictedStore]').is(':checked')) {
                            alert("宅配檔次/通用券檔次無須設定[銷售店鋪]賣家");
                            $(chk).removeAttr("checked");
                            return false;
                        }
                    }
                    $(this).attr("checked", "checked");
                } else {
                    $(this).removeAttr("checked");
                }
            });
        }        
        
        function updateSms() {
            var isReverseVerify = $(":checkbox[id*=IsReverseVerify]:checked").length > 0;
            if (isReverseVerify) {
                $("#hidSmsFooter").val('，編號{0}，確認碼{1}，期限{2}-{3}{4}，下載APP核銷兌換');
            } else {
            $("#hidSmsFooter").val('，{0}-{1}，期限{2}-{3}，App載憑證：http://x.co/4r1JG');
            }
        }

        function ClearEmptyData(obj) {
            $(obj).val($.trim($(obj).val()));
        }

        function SetBusinessOrderInfo() {
            // 工單系統帶入-進貨價
            var purchasePrice = $('#<%= hidBusinessOrderPurchasePrice.ClientID %>');
            if (purchasePrice.val() != '') {
                $('#tableCost').children('tbody').children('tr').remove();
                $("#<%=hdCumulativeQuantity.ClientID %>").val('0');
                for (var i = 0; i < purchasePrice.val().split(';').length; i++) {
                    if (purchasePrice.val().split(';')[i].split(',').length > 1) {
                        var quantity = purchasePrice.val().split(';')[i].split(',')[0].replace('份', '');
                        var cost = purchasePrice.val().split(';')[i].split(',')[1].replace('元', '');
                        if (quantity.split('~')[1].length > 1) {
                            $('#tbQuantity').val(parseInt(quantity.split('~')[1]) - parseInt(quantity.split('~')[0]) + 1);
                            $('#tbMultiCost').val(cost);
                            costAdd();
                        }
                    } else {
                        var quantity = $('#<%= tbOMax.ClientID %>').val().replace(',', '');
                        var cost = purchasePrice.val().split(';')[i].split(',')[0].replace('元', '');
                        $('#tbQuantity').val(quantity);
                        $('#tbMultiCost').val(cost);
                        costAdd();
                    }
                }
            }

            // 工單系統帶入-運費
            var itemPrice = $('#<%= tbP.ClientID %>').val();
            var freight = $('#<%= hidBusinessOrderFreight.ClientID %>');

            // 階梯式運費
            if (freight.val() != '') {
                $('#tableFI').children('tbody').children('tr').remove();
                for (var i = 0; i < freight.val().split(';').length; i++) {
                    if (freight.val().split(';')[i].split(',').length > 1) {
                        var price = freight.val().split(';')[i].split(',')[0].replace('份', '');
                        var cost = freight.val().split(';')[i].split(',')[1].replace('元', '');
                        if (price.split('~')[1].length > 1) {
                            $('#tbFIStartAmt').val(parseInt(price.split('~')[1]) * parseInt(itemPrice));
                            $('#tbFIAmt').val(cost);
                            freightAdd(1);
                        }
                    } else {
                        $('#tbFIStartAmt').val('0');
                        $('#tbFIAmt').val(freight.val().replace('元', ''));
                        freightAdd(1);
                    }
                }
            }
            // 免運
            var nonFreightLimitPrice = $('#<%= hidBusinessOrderNonFreightLimit.ClientID %>');
            if (nonFreightLimitPrice.val() != '') {
                $('#tbFIStartAmt').val(parseInt(nonFreightLimitPrice.val()) * parseInt(itemPrice));
                $('#tbFIAmt').val('0');
                freightAdd(1);
            }
            // 旅遊類
            SetTravelSelectItem();

            ClickPromotionDeal();
        }

        function showcopydeals(copydealguid) {
            $.blockUI({
                message: "<div class='copydeal' ><div class='copydeal-Top'><div class='copydeal-X-button' onclick='$.unblockUI();' style='cursor: pointer'></div></div><h3><a  style='font-size:12pt' href='setup.aspx?bid=" + copydealguid + "' target='_blank'>檔次已複製-" + copydealguid + "</a></h3></div>"
                , css: { border: 'none', background: 'transparent', width: '0px', height: '0px', cursor: 'default', top: ($(window).height() - 350) / 2 + 'px', left: ($(window).width() - 700) / 2 + 'px' }
            });
        }

        function tmall_rmb_show(ischecked) {
            if (ischecked) {
                $('#tmall_rmb').show();
            }
            else {
                $('#tmall_rmb').hide();
            }
        }

        function KindDealSetting(obj) {
            // 捐款檔次不開立發票僅開立收據
            if ($(obj).is(':checked')) {
                $(':radio[id*=rbKindReceipt]').attr('checked', 'checked');
                // 僅能使用新核銷系統
                $(':radio[id*=rbBMBalanceSheetSystem]').attr('checked', 'checked');
                $(':radio[id$=rbBMMohistSystem]').attr('disabled', true);
                $(':checkbox[id*=cbx_NotDeliveryIslands]').attr('checked', false);
                $('input[id$=cbx_DonotInstallment]').attr('checked','checked');
                alert('已關閉分期');
            } else {
                $(':checkbox[id*=cbx_KindDeal]').removeAttr('checked');
                if ($(':radio[id*=rbBMNone]').is(':checked')) {
                    $(':checkbox[id*=rbBMNone]').removeAttr('checked');
                    $(':radio[id*=rbBMBalanceSheetSystem]').attr('checked', 'checked');
                }
                $(':radio[id*=rbNoEntrustSell]').attr('checked', 'checked');
                $(':radio[id$=rbBMMohistSystem]').attr('disabled', false);
                $(':checkbox[id*=cbx_NotDeliveryIslands]').attr('checked', true);
            }
        }

        function checkTravelCategory(obj) {
            if ($(obj).next().text() == '旅遊渡假') {
                if ($(obj).is(':checked')) {
                    $('.travelList').css('display', '');
                } else {
                    $('.travelList').css('display', 'none');
                }
            }
        }
        
        function ShowCouponSeparateDigits(obj) {
            if ($(obj).is(':checked')) {
                $('#CouponSeparateDigitsShow').css('display', '');
            } else {
                $('#CouponSeparateDigitsShow').css('display', 'none');
                $(obj).next('#CouponSeparateDigitsShow').find(':checked').each(function () {
                    $(this).attr('checked', false);
                });
            }
        }

        function ShowCommercialList(obj) {
            if ($(obj).is(':checked')) {
                $(obj).next().next('#CommercialCategoryList').css('display', '');
            } else {
                $(obj).next().next('#CommercialCategoryList').css('display', 'none');
                $(obj).next().next('#CommercialCategoryList').find(':checked').each(function () {
                    $(this).attr('checked', false);
                });
            }
        }

        function SetFreeDeal() {
            $(".tabs2").bind("click", function () {
                if ($(".editPrice").val() == "0") {
                    $(".editFreeDeal").show();
                }
                else {
                    $(".editFreeDeal").hide();
                }
            });
        }
        function CheckBoxLinkBind() {
            /**/ 
            $(':checkbox[id*=cbx_DisableSMS]').live('change', function () {
                //若為零元檔， "關閉簡訊按鈕" 和 "限紙本憑證" 不連動
                var price = $('#<%=tbP.ClientID %>').val();
                if (price != "0")
                {
                    if ($(this).is(':checked')) {
                        $('.CouponDealTag input[id*=_0]').attr('checked', 'checked');
                    } else {
                        $('.CouponDealTag input[id*=_0]').removeAttr('checked', 'checked');
                    }
                }
            });
            $(':checkbox[id=cbx_ZeroActivityShowCoupon]').live('change', function () {
                if ($(this).is(':checked')) {
                    $(':checkbox[id=cbx_PEZeventCouponDownload]').attr('checked', 'checked');
                }
            });
            $(':checkbox[id=cbx_PEZeventCouponDownload]').live('change', function () {
                if ($(':checkbox[id=cbx_ZeroActivityShowCoupon]').is(':checked')) {
                    $(':checkbox[id=cbx_PEZeventCouponDownload]').attr('checked', 'checked');
                }
            });
            $(':checkbox[id*=cbxCouponSeparateDigits]').live('change', function () {
                if ($(this).is(':checked')) {
                    $(':checkbox[id*=cbxCouponSeparateDigits]').attr('checked', 'checked');
                } else {
                    $('#tbCouponSeparateDigits').val("");
                }
            });
            $(':checkbox[id*=cbx_NotDeliveryIslands]').live('change', function () {
                if ($(this).is(':checked')) {
                    $('.DeliveryDealTag input[id*=_3]').attr('checked', 'checked');
                } else {
                    $('.DeliveryDealTag input[id*=_3]').removeAttr('checked', 'checked');
                }
            });
            $(':checkbox[id*=cbx_NoShippingFeeMessage]').live('change', function () {
                if ($(this).is(':checked')) {
                    $('.DeliveryDealTag input[id*=_1]').attr('checked', 'checked');
                } else {
                    $('.DeliveryDealTag input[id*=_1]').removeAttr('checked', 'checked');
                }
            });
            $(':checkbox[id*=cb_IsUsingbookingSystem]').live('change', function () {
                if ($(this).is(':checked')) {
                    SetBookingSystemType();
                    $('.CouponUsers').show();
                }
                else {
                    $('.CouponUsers').hide();
                }
            });
            $(':radio[id*=rdoSingleUser]').live('change', function () {
                if ($(this).is(':checked')) {
                    $('#txtCouponUsers').val("");
                    $('#txtCouponUsers').prop('disabled', true);
                }
            });
            $(':radio[id*=rdoMultiUsers]').live('change', function () {
                if ($(this).is(':checked')) {
                    $('#txtCouponUsers').prop('disabled', false);
                }
            });
            $("#txt_CustomTag").bind("keyup", function () {
                if ($("#txt_CustomTag").val()) {
                    $(':checkbox[id=cb_CustomTag]').attr('checked', 'checked');
                }
                else {
                    $(':checkbox[id=cb_CustomTag]').removeAttr('checked', 'checked');
                }
            });
            $("#tbCouponSeparateDigits").bind("keyup", function () {
                if ($("#tbCouponSeparateDigits").val()) {
                    $(':checkbox[id=cbxCouponSeparateDigits]').attr('checked', 'checked');
                }
                else {
                    $(':checkbox[id=cbxCouponSeparateDigits]').removeAttr('checked', 'checked');
                }
            });
            $("#<%=cbl_DefaultIcon.ClientID%>").find(".appLimitedEdition").find("input[type=checkbox]").live('change', function () {
                if ($(this).is(':checked')) {
                    $('input[id=cbx_IsAppLimitedEdition]').attr('checked', 'checked');
                }
            });
            $('input[id=cbx_IsAppLimitedEdition]').live('change', function() {
                if (!$(this).is(':checked')) {
                    $("#<%=cbl_DefaultIcon.ClientID%>").find(".appLimitedEdition").find("input[type=checkbox]").removeAttr('checked', 'checked');
                }
            });
        }
        function SetBookingSystemDisplay() {
            if ($('select[id$=ddlRreceiveType]').val() == '<%=(int)DeliveryType.ToShop %>') {
                $(".BookingSystem").show();
                if ($(':checkbox[id*=cb_IsUsingbookingSystem]').is(':checked')) {
                    $('.CouponUsers').show();
                }
                if ($(':radio[id*=rdoSingleUser]').is(':checked')) {
                    $('#txtCouponUsers').val("");
                    $('#txtCouponUsers').prop('disabled', true);
                } else if ($(':radio[id*=rdoMultiUsers]').is(':checked')) {
                    $('#txtCouponUsers').prop('disabled', false);
                }
            }
            else {
                $(".BookingSystem").hide();
            }
        }
        function SetDealLabelDisplay() {
            if ($('select[id$=ddlRreceiveType]').val() == '<%=(int)DeliveryType.ToShop %>') {
                //$(".DeliveryIconCheckBox").hide();
                $(".divDeliveryDealTag").hide();
                //$(".CouponIconCheckbox").show();
                $(".divCouponDealTag").show();
                SetDealLabelTravelDisplay();
            }
            else {
                //$(".CouponIconCheckbox").hide();
                $(".divCouponDealTag").hide();
                //$(".DeliveryIconCheckBox").show();
                $(".divDeliveryDealTag").show();
                SetDealLabelTravelDisplay();
            }
        }
        function SetDealLabelTravelDisplay() {
            if ($('#ddlAccBusGroup').val() == '<%=(int)BusinessGroup.Travel %>') {
                $(".divCouponTravelTag").show();
                $(".divDeliveryTravelTag").show();

            }
            else {
                $(".divCouponTravelTag").hide();
                $(".divDeliveryTravelTag").hide();
            }
        }
        function SetGroupCouponDealAppStyle() {
            //開檔時
            if ($('#btnImportBusinessOrderInfo').length && $('[id*=cbl_GroupCouponAppStyle]').length) {
                var _accGroupName = $('#ddlAccBusGroup option:selected').text();
                var _checkboxes = $('[id*=cbl_GroupCouponAppStyle] input[type="checkbox"]');
                 //先比對館別是否有相應的頻道
                $(_checkboxes).each(function () {
                    if ($(this).next('label').text().replace(/[&\|\\\‧*^%$#@\-]/g, "") == _accGroupName) {
                        _checkboxes.attr('checked', false);
                        $(this).attr('checked', 'checked');
                    }
                })
            }
        }
        //多檔次預覽
        function showcombodeals() {
            if ($('#combodeals').find("#div_combodeallist") != undefined) {
                $('#combodeals').find("#div_combodeallist").removeAttr('style');
            }
            var top = 0;
            if ($('#combodeals').height() < $(window).height()) {
                top = ($(window).height() - $('#combodeals').height()) / 2;
            } else {
                if ($('#combodeals').find(".mgs-item-box").length > 7 && $('#combodeals').find("#div_combodeallist") != undefined) {
                    $('#combodeals').find("#div_combodeallist").css('height', '550px').css('overflow', 'auto');
                    top = ($(window).height() - $('#combodeals').find("#div_combodeallist").height()) / 2;
                }
            }

            var left = 0;
            if ($('#combodeals').width() < $(window).width()) {
                left = ($(window).width() - $('#combodeals').width()) / 2;
            }
            $.blockUI({ message: $('#combodeals'), css: { border: 'none', background: 'transparent', width: '0px', height: '0px', cursor: 'default', top: top + 'px', left: left + 'px' } });
        }
        function SetFamiBarcodeDisplay() {                        
            var presentQuantity=$('#<%=hidPresentQuantity.ClientID%>').val();
            var presentQuantityEnable=$('#<%=hidPresentQuantityEnable.ClientID%>').val();
            if ($('#<%=cbx_FamiDeal.ClientID%>').is(':checked')) {
                $('.FamiDealRadioBtn').show();
                $('#FamiExchangePriceArea').show();
                $('.FamiDealType').show();
                $('#<%=tbPresentQuantity.ClientID%>').val('0'); 
                $('#<%=tbPresentQuantity.ClientID%>').attr('disabled','disabled');                
            } else {
                $('.FamiDealRadioBtn').hide();
                $('#FamiExchangePriceArea').hide();
                $('.FamiDealType').hide();
                if(presentQuantityEnable=='1')
                {
                    $('#<%=tbPresentQuantity.ClientID%>').attr('disabled',false);
                }
                $('#<%=tbPresentQuantity.ClientID%>').val(presentQuantity);
            }
        }

        function SetSKMDisplay() {
            var checked = $('#<%=cbx_SKMDeal.ClientID%>').is(':checked');
            if (checked) {
                $('.SKMDealType').show();
                if($('#<%=cbx_Settlement.ClientID%>').is(':checked')){
                    $('#plSettlement').show();
                }
                if($('#<%=cbx_setting_display.ClientID%>').is(':checked')){
                    $('#plSKMDiscount').show();
                }
            } else {
                $('.SKMDealType').hide();
            }
            $("[id*=cbl_DefaultIcon]").each(function(){
                if($(this).val() == '<%=(int)DealLabelSystemCode.AppLimitedEdition%>')
                {
                    $(this).attr('checked',checked);
                }
            })
            $('#<%=cbx_IsAppLimitedEdition.ClientID%>').attr('checked',checked);
        }

        function SKMSettlementShow(obj) {
            if ($(obj).is(':checked')) {
                $('#plSettlement').show();
            }
            else {
                $('#plSettlement').hide();
            }
        }

        function SKMDiscountShow(obj) {
            if ($(obj).is(':checked')) {
                $('#plSKMDiscount').show();
            }
            else {
                $('#plSKMDiscount').hide();
            }
        }

        function SetSKMDiscount(){
            var index = $("input[id^='<% =rblDiscountType.ClientID %>']:checked").val();
            var value = $('#<%=hdDiscount.ClientID%>').val();
            $("input[id^='txtDiscount'][tabindex="+ index +"]").val(value);
        }

        function SetShipTypeDisplay() {
            if ($("#rboNormal").attr("checked")) {
                $("#divSubNormalShip").show();
                $("#divSubFastShip").hide();
                if ($("#ctl00_ContentPlaceHolder1_hidSubShipType").val() == "Normal"){
                    $("#ctl00_ContentPlaceHolder1_rboNormalShipDay").attr("checked", true);
                }
                else
                {
                    $("#ctl00_ContentPlaceHolder1_rboUniShipDay").attr("checked", true);
                }
            }
            if ($("#rboFast").attr("checked")) {
                $("#divSubNormalShip").hide();
                $("#divSubFastShip").show();
                $("#ctl00_ContentPlaceHolder1_rboFastShip1").attr("checked", true);
            }
            if ($("#rboWms").attr("checked")) {
                $("#divSubNormalShip").hide();
                $("#divSubFastShip").hide();
            }
            if ($("#rboLastShipment").attr("checked")) {
                $("#divSubNormalShip").hide();
                $("#divSubFastShip").hide();
            }
            if ($("#rboLastDelivery").attr("checked")) {
                $("#divSubNormalShip").hide();
                $("#divSubFastShip").hide();
            }
            if ($("#rboOther").attr("checked")) {
                $("#divSubNormalShip").hide();
                $("#divSubFastShip").hide();
            }
        }

        function SetBookingSystemType() {
            if ($('#ddlAccBusGroup').val() == '<%=(int)BusinessGroup.Travel %>') {
                //$(':radio[id*=rdoBookingSystemRoom]').prop('checked', true);
            } else {
                $(':radio[id*=rdoBookingSystemSeat]').prop('checked', true);
            }
        }



        var categoryKeyArray = <%=hiddenChannelJSon.Value%>; //頻道
        var categoryDataArray = <%=hiddenChannelCategoryJSon.Value%>; //地區
        var specialcategoryDataArray = <%=hiddenChannelSpecialCategoryJSon.Value%>; //商圈景點

        <%--顯示所有新版分類的資料--%>
        function ShowAllCategory() {

            var selectedChannel = <%=hiddenChannelChecked.Value%>;
            var selectedSubChannel = <%=hiddenSubChannelChecked.Value%>;
            var selectedDealCategory = <%=hiddenDealCategory.Value %>;
            var selectedIconCategory = <%=hiddenDeliveryCategoryChecked.Value%>;
            
            var selectedSubChannelSpecialRegion =<%=hiddenSubSpecialRegionChecked.Value%>;
            var selectedSubChannelSubSpecialRegion =<%=hiddenSubSpecialSubRegionChecked.Value%>;
            var subRegionIndex = 0;
            <%--頻道與區域--%>
            $.each(selectedChannel, function(i, channelId) {
                $('#channelCategory' + channelId).attr('checked', true);
                OnChannelCategoryChange();
    
                $.each(selectedSubChannel[i], function(j, arealId) {
                    var subChannel = $('#subChannelCategory' + arealId);
                    subChannel.attr('checked', true);
                    if ( selectedSubChannelSpecialRegion[subRegionIndex]!=null && selectedSubChannelSpecialRegion!=null && selectedSubChannelSpecialRegion.length > 0) {
                        $.each(selectedSubChannelSpecialRegion[subRegionIndex], function(y, subRegionId) {
                            var subRegionChannel = $('#subChannelSPRegionCategory' + arealId);
                            if (subRegionChannel.find('#subChannelSpecialRegion' + subRegionId) != null && subRegionChannel.find('#subChannelSpecialRegion' + subRegionId).length > 0) {
                                subRegionChannel.find('#subChannelSpecialRegion' + subRegionId).attr('checked', true);        
                                
                                $.each(selectedSubChannelSubSpecialRegion, function(z, subSPRId) {
                                    var subRegionsubChannel = $('#subSPRegionCategory' + subRegionId);
                                    if (subRegionsubChannel.find('#subSpecialRegion' + subSPRId) != null && subRegionsubChannel.find('#subSpecialRegion' + subSPRId).length > 0) {
                                        subRegionsubChannel.find('#subSpecialRegion' + subSPRId).attr('checked', true);
                                    }
                                });
                            }
                        });
                    }
                    OnChannelCategoryChange();
                    subRegionIndex++;
                });
            });

            <%--檔次分類，注意!一定要讓頻道與區域先設定，分類的checkbox狀態才會對，正確後，才可以設定分類--%>
            $.each(selectedDealCategory , function(i, categoryId) {
                    $('#dealCategory' + categoryId).attr('checked', true);
                    OnDealCategoryChange();
                }
            );

            <%--特殊分類--%>
            $.each(selectedIconCategory, function(i, categoryId) {
                $('#IconCategory' + categoryId).attr('checked', true);
                OnIconCategoryChange();
            });
        }

        function MoveDealCategoryItem() {
            $.each(categoryKeyArray, function(j, obj) {
                var tr = $('<tr />');
                //var td = $('<td />');
                var dealCategoryArea = $('#dealCategoryArea' + obj);
                if (dealCategoryArea.find("tr").length == 0 ) {
                    dealCategoryArea.append(tr);
                }
                $.each(categoryDataArray[j], function(h, cid) {

                    var dealCategroyTree = $('#dealCategorytree' + cid);
                    if (dealCategroyTree.length > 0);
                    {
                        if (dealCategoryArea.find("tr:last").length > 0 && dealCategoryArea.find("tr:last").find("td").length >= 4) {
                            dealCategoryArea.find("tr:last").after("<tr>");
                            dealCategoryArea.find("tr:last").append(dealCategroyTree.parent().clone().css("width", "25%").css("vertical-align","top"));
                            dealCategoryArea.find("tr:last").after("</tr>");
                        } else {
                            dealCategoryArea.find("tr:last").append(dealCategroyTree.parent().clone().css("width", "25%").css("vertical-align","top"));
                        }

                    }
                });

                if ($('#channelCategory' + obj).is(':checked')) {
                    var checkareacontent = $('#categoryAreaContent' + obj);
                    if (checkareacontent.find('input[type="checkbox"]').length > 0) {
                        checkareacontent.show();
                        $('#categoryAreaHeader' + obj).show();                        
                    }
                    checkareacontent.find('input[type="checkbox"]').attr('disabled',false);
                    var isShowBackEnd = checkareacontent.find('span[id^="isShowBackEnd"]');
                    $.each(isShowBackEnd,function(sidx, sobj){
                        if($(sobj).html().toLowerCase() == "false"){
                            $(sobj).parent().parent().find("input[type=checkbox]").attr('disabled',true);
                        }
                    });                 
                    checkareacontent.find('label[id^="dealCategoryDeas"]').css('color', 'black');
                } else {
                    $('#categoryAreaHeader' + obj).hide();
                    var uncheckareacontent = $('#categoryAreaContent' + obj);
                    uncheckareacontent.hide();
                    uncheckareacontent.find('input[type="checkbox"]').attr('disabled',true);
                    uncheckareacontent.find('label[id^="dealCategoryDeas"]').css('color', 'gray');
                }
            });
            <%--checkboxTree --%>
            $('#sortDealCategory ul[id^="dealCategorytree"]').checkboxTree({
                initializeChecked: 'expanded',
                initializeUnchecked: 'collapsed',
                onCheck: {
                    node: 'expand',
                    ancestors: 'check',
                    descendants: 'uncheck' 
                },
                onUncheck: {
                    node: 'collapse'
                },
                collapseImage: '../../Tools/js/css/images/downArrow.gif',
                expandImage: '../../Tools/js/css/images/rightArrow.gif',
                collapse: function(){
                    //alert('collapse event triggered (passed as option)');
                },
                expand: function(){
                    //alert('expand event triggered (passed as option)');
                },
                collapseDuration: 200,
                expandDuration: 200
            });  //.removeClass(' ui-widget-content')
            OnDealCategoryChange();
        }

        function OnDealCategoryChange() {
            var kids = $("#sortDealCategory table[id^='dealCategoryArea'] input[type='checkbox']");
            var selectedArray = [];
            $.each(kids, function(j, obj) {
                    if (obj.checked) {
                        var categoryId = obj.id.replace('dealCategory', '');
                        selectedArray.push(parseInt(categoryId));
                    }
                }
            );
            $('#<%=hiddenDealCategory.ClientID%>').val(JSON.stringify(selectedArray));
        }
    
        <%--頻道選取發生變化時--%>  
        function OnChannelCategoryChange() {
            var selectedArray = []; //頻道 EX:美食、旅遊
            var subSelectArray = []; //地區 EX:台北、桃園、竹苗
            var subSpecialRegionArray = []; //商圈景點 EX:東區、西門町
            var subStandardRegionArray = [];
            var subSpecialSubRegionArray = []; //商圈景點子區域
            
            var enabledCategoryId = [];
            var enabledSpecialCategoryId = [];
            $.each(categoryKeyArray, function(j, obj) {
                    var channelBox = $('#channelCategory' + obj);
                    var kids = $("#subChannelCol" + obj + " input[type='checkbox']"); //地區                      
                    var kidText = $("#subChannelCol" + obj + " label");
                    var spRegions = $("#specialRegion" + obj ); //商圈景點
                    if (channelBox.is(':checked')) {
                        <%--有選取--%>
                        selectedArray.push(obj);
                        kids.attr('disabled', false);
                        kidText.css('color', 'black');
                        var areaSelected = []; //選取地區
                        var SpecialRegionSelected = []; //選取商圈
                        var isShowSPRegion =false;
                        var isShowSubSPRegion =false;
                        $.each(kids, function(h, subArea) {
                            var subAreaId = parseInt(subArea.id.replace('subChannelCategory', '')); //地區
                            var trSubSPR = $("#trSubSPR" + subAreaId ); //商圈景點子區域
                            if (subArea.checked) {
                                SpecialRegionSelected = [];
                                subArea.disable = false;
                                areaSelected.push(subAreaId);
                                isShowSPRegion = true;
                                if ($('#subChannelSPRegionCategory' + subAreaId).find("td input[type='checkbox']")!=null && $('#subChannelSPRegionCategory' + subAreaId).find("td input[type='checkbox']").length > 0) {
                                    $('#subChannelSPRegionCategory' + subAreaId).show();

                                    var specialkids = $("#subChannelSPRegionCategory" + subAreaId + " input[type='checkbox'][id^='subChannelSpecialRegion']");
                                    if ( specialkids!=null && specialkids.length > 0) {
                                        $.each(specialkids, function(h, specialregion) {
                                            var SpecialRegionId = parseInt(specialregion.id.replace('subChannelSpecialRegion', ''));
                                            if (specialregion.checked) {
                                                SpecialRegionSelected.push(SpecialRegionId);
                                                isShowSubSPRegion = true;
                                                if ($('#subSPRegionCategory' + SpecialRegionId).find("td input[type='checkbox']") != null && $('#subSPRegionCategory' + SpecialRegionId).find("input[type='checkbox']").length > 0) {
                                                    $('#subSPRegionCategory' + SpecialRegionId).show();

                                                    var subspecialkids = $("#subSPRegionCategory" + SpecialRegionId + " input[type='checkbox'][id^='subSpecialRegion']");
                                                    if (subspecialkids != null && subspecialkids.length > 0) {
                                                        $.each(subspecialkids, function(h, subspecialregion) {
                                                            var subSpecialRegionId = parseInt(subspecialregion.id.replace('subSpecialRegion', ''));
                                                            if (subspecialregion.checked) {
                                                                subSpecialSubRegionArray.push(subSpecialRegionId);
                                                            }
                                                        });
                                                    }
                                                }
                                            } else {
                                                $('#subSPRegionCategory' + SpecialRegionId).hide();
                                                $('#subSPRegionCategory' + SpecialRegionId).find("td input[type='checkbox']").each(function() {
                                                    $(this).attr('checked', false);
                                                });
                                                $('#subSPRegionCategory' + SpecialRegionId).find("td input[id^=subspecialRegion]").each(function() {
                                                    $(this).hide();
                                                });
                                            }
                                        });
                                    }
                                } 
                                subSpecialRegionArray.push(SpecialRegionSelected);
                                
                                if (isShowSubSPRegion) {
                                    trSubSPR.show();
                                } else {
                                    trSubSPR.hide();
                                    trSubSPR.find("td input[type='checkbox']").each(function() {
                                        $(this).attr('checked', false);
                                    });
                                    trSubSPR.find("td input[id^=subspecialRegion]").each(function() {
                                        $(this).hide();
                                    });
                                }
                            } else {
                                $('#subChannelSPRegionCategory' + subAreaId).hide();
                            }
                        });
                        if (isShowSPRegion) {
                            spRegions.show();
                        } else {
                            spRegions.hide();
                            spRegions.find("input[type='checkbox']").each(function() {
                                $(this).attr('checked', false);
                            });
                            spRegions.find("input[id^=specialRegion]").each(function() {
                                $(this).hide();
                            });
                        }

                        subSelectArray.push(areaSelected);

                        $.each(categoryDataArray[j], function(h, cid) {
                            <%--檢查如果此項目目前不再可選取屬性中，加入此項目--%>
                            if ($.inArray(cid, enabledCategoryId) == -1) {
                                enabledCategoryId.push(cid);
                            }
                        });
                        $.each(specialcategoryDataArray[j], function(h, cid) {
                            <%--檢查如果此項目目前不再可選取屬性中，加入此項目--%>
                            if ($.inArray(cid, enabledSpecialCategoryId) == -1) {
                                enabledSpecialCategoryId.push(cid);
                            }
                        });
                    } else {
                        kids.attr('checked', false);
                        kids.attr('disabled', true);
                        kidText.css('color', 'gray');
                        spRegions.hide();
                        spRegions.find("input[type='checkbox']").each(function() {
                            $(this).attr('checked', false);
                        });                    
                    }
                }
            );
            $('#<%=hiddenChannelChecked.ClientID%>').val(JSON.stringify(selectedArray));
            $('#<%=hiddenSubChannelChecked.ClientID%>').val(JSON.stringify(subSelectArray));
            $('#<%=hiddenSubSpecialRegionChecked.ClientID%>').val(JSON.stringify(subSpecialRegionArray));
            $('#<%=hiddenSubSpecialSubRegionChecked.ClientID%>').val(JSON.stringify(subSpecialSubRegionArray));

            //CheckDealCategoryEnabledStatus(enabledCategoryId, $('#dealCategoryTable'), 'dealCategory', 'dealCategoryDeas');
            //CheckDealCategoryEnabledStatus(enabledCategoryId, $("table[id^='dealCategoryArea']"), 'dealCategory', 'dealCategoryDeas');
            CheckDealCategoryEnabledStatus(enabledSpecialCategoryId, $('#IconCategoryArea'), 'IconCategory', 'IconCategoryDesc');

            if ($('#channelCategory' + '<%= CategoryManager.Default.Piinlife.CategoryId %>').is(':checked')) {
                $('#PiilifeTabs').show();
            }else {
                $('#PiilifeTabs').hide();
            }

            $.each(categoryKeyArray, function(j, obj) {
                if ($('#channelCategory' + obj).is(':checked')) {
                    
                    var areacontent = $('#categoryAreaContent' + obj);
                    if (areacontent.find('input[type="checkbox"]').length > 0) {
                        areacontent.show();
                        $('#categoryAreaHeader' + obj).show();                        
                    }
                    //areacontent.find('input[type="checkbox"]').attr('disabled',false);
                    var chks = areacontent.find('input[type="checkbox"]');
                    $.each(chks, function(cidx, cobj){
                        var sp = $(cobj).next("p[id=cid]");
                        var o = $(cobj).parent().parent().find('span[id="isShowBackEnd' + $(sp).html() + '"]');
                        var isDisabled = false;
                        if(o != undefined && o.length > 0){
                            if(o.html().toLowerCase() == "false"){
                                isDisabled = true;
                            }
                        }
                        $(cobj).attr('disabled',isDisabled);
                    });
                    areacontent.find('label[id^="dealCategoryDeas"]').css('color', 'black');

                } else {
                    $('#categoryAreaHeader' + obj).hide();
                    var areacontent = $('#categoryAreaContent' + obj);
                    areacontent.hide();
                    areacontent.find('input[type="checkbox"]').prop('checked',false).attr('disabled',true);
                    areacontent.find('label[id^="dealCategoryDeas"]').css('color', 'gray');

                    areacontent.find('ul[id^="dealCategorytree"]').checkboxTree('uncheckAll');
                }
            });
            OnDealCategoryChange();
        }

        function checkNonATMDeal() {
            var isTmallDeal = $("input[id*=cbx_TmallDeal]").is(":checked") ? true : false;
            var isFamiDeal = $("input[id*=cbx_FamiDeal]").is(":checked") && !$("input[id*=cbx_GroupCoupon]").is(":checked") ? true : false;
            var isZeroDeal = $("input[id=cbx_PEZeventCouponDownload]").is(":checked") && !$("input[id*=cbx_GroupCoupon]").is(":checked") ? true : false;
            var isKindDeal = $("input[id*=cbx_KindDeal]").is(":checked") ? true : false;
            var isPiinLife = ($("#ddlAccBusGroup").val() == "<%=(int)LunchKingSite.Core.AccBusinessGroupNo.PiinLife%>") ? true : false;
            var isZeroPrice = parseInt($("input[id*=tbP]").val()) == "0" ? true : false;

            var channelData = $.parseJSON($("#<%=hiddenChannelChecked.ClientID%>").val());
            var isNonATMDeal = false;
            for(var i=0;i<channelData.length;i++) {
                if(channelData[i].toString() == "<%=(int)CategoryManager.Default.Piinlife.CategoryId%>") {
                    isNonATMDeal = true;
                }
            }

            if(isNonATMDeal || isTmallDeal || isFamiDeal || isZeroDeal || isKindDeal || isPiinLife || isZeroPrice) {
                return true;
            } else {
                return false;
            }
        }

        function setATMQtyWithSettingDealType() {
            var atmQty = $("input[id*=tbAtm]").val();
            if(checkNonATMDeal()) {
                if(atmQty != "0") {
                    $("#hidAtmQty").val(atmQty);
                    $("input[id*=tbAtm]").val("0");
                }
                $("input[id*=tbAtm]").attr("readonly", true);
            } else {
                $("input[id*=tbAtm]").attr("readonly", false);
                if(atmQty == "0" && $("#hidAtmQty").val() != "-1") {
                    $("input[id*=tbAtm]").val($("#hidAtmQty").val());
                    $("#hidAtmQty").val("-1");
                }
            }
        }

        function setATMQtyWithChangetbOMax(obj) {
            var tempAtmQty = $("input[id*=tbAtm]").val() ? parseInt($("input[id*=tbAtm]").val()) : 30000;
            var tempCustomQty = parseInt($("input[id=hidCustomAtmQty]").val());

            if(!checkNonATMDeal()) {
                if(tempCustomQty == -1 || (tempCustomQty == tempAtmQty)) {
                    var tempVal = parseInt(parseInt($(obj).val() ? $(obj).val().replace(",", "") : "100000") * 0.2);
                    $("input[id=hidCustomAtmQty]").val(tempVal);
                    $("input[id*=tbAtm]").val(tempVal);
                }
            }
            else {
                setATMQtyWithSettingDealType();
            }
        }

        function setAtmQtyToDefaultValue(obj)
        {
            if(!$(obj).val()) {
                var tempMax = $("input[id*=tbOMax]").val() ? parseInt($("input[id*=tbOMax]").val().replace(",", "")) : 100000;
                $(obj).val(parseInt(tempMax * 0.3));
                $("input[id=hidCustomAtmQty]").val("-1");
            }
            else {
                $(obj).val(parseInt($(obj).val()));
            }
            return false;
        }


        function CheckDealCategoryEnabledStatus(enabledCategoryId, area, inputId, labelId) {
            <%--處理dealCategory的enabled狀態--%>
            var dealCategoryBoxList = area.find("input[type='checkbox']");
            $.each(dealCategoryBoxList, function(j, obj) {
                var categoryId = parseInt(obj.id.replace(inputId, ''));
                var categoryDesc = $('#' + labelId + categoryId);
                if ($.inArray(categoryId, enabledCategoryId) == -1) {
                    <%--不包含於可選取項目中--%>
                    obj.disabled = true;
                    categoryDesc.css('color', 'gray');
                } else {
                    obj.disabled = false;
                    categoryDesc.css('color', 'black');

                }
            });
        }

        <%--依據取貨方式，調整icon區塊的checkbox--%>
        function ShowIconByDeliveryType() {
            var iconIdList = $.parseJSON($('#<%=hiddenDeliveryIconIdList.ClientID%>').val());
            var iconBoxList = $("#IconCategoryArea input[type='checkbox']").filter(function() {return $(this).attr('disabled') != 'disabled'; });
            <%--頻道與區域--%>
            $.each(iconBoxList, function(i, iconBox) {
                var iconId = parseInt(iconBox.id.replace('IconCategory', ''));
                var iconDesc = $("#IconCategoryDesc" + iconId);
                var isWork = false;
                $.each(iconIdList, function(j, workId) {
                        if (iconId == workId) {
                            isWork = true;
                            return;
                        }
                    }
                );
                if (isWork) {
                    iconBox.hidden = false;
                    iconDesc.show();
                    iconDesc.css('color', 'black');
                } else {
                    iconBox.checked = false;
                    iconBox.disabled = true;
                    iconBox.hidden = true;
                    iconDesc.hide();
                    iconDesc.css('color', 'gray');
                }
            });
        }
        <%--選取首頁的icon category時--%>  
        function OnIconCategoryChange() {
            var iconBoxList = $("#IconCategoryArea input[type='checkbox']");
            var selectArray = [];
            $.each(iconBoxList, function(i, iconBox) {
                var iconId = parseInt(iconBox.id.replace('IconCategory', ''));
                if (iconBox.checked) {
                    selectArray.push(iconId);
                }
            });
            $('#<%=hiddenDeliveryCategoryChecked.ClientID%>').val(JSON.stringify(selectArray));
        }        

        function GetDraftForDescriptionByComboDealSpecs() {
            var bid = '<%=this.BusinessHourId %>';
            var result = '';
            $.ajax({
                type: "POST",
                url: "../../WebService/PponDealService.asmx/GetDraftForDescriptionByComboDealSpecs",
                data: " { 'bid': '" + bid + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function (response) {
                    result = response.d;
                }, error: function (res) {
                    console.log(res);
                }
            });
            return result;
        }
        $(function () {

            if($("#channelCategory148") != undefined){
                if($("#channelCategory148").is(':checked')){
                    $("#chkHideContent").attr("checked","checked");
                    $(".pponcontent").hide();
                }
            }
            if($("#chkHideContent") != undefined){
                if($("#chkHideContent").is(':checked')){
                    $(".hidecontent").hide();
                }
            }
            $("#chkHideContent").click(function(){
                if($(this).is(':checked')){
                    $(".hidecontent").hide();
                }else{
                    $(".hidecontent").show();
                }
            });

            $("#btnMoveSeller").click(function(){
                $("#spnMoveSellerResult").text('');

                if ($.trim($("#<%=tbNewSellerId.ClientID%>").val()).length === 0) {
                    $("#spnMoveSellerResult").text("請輸入欲搬移之賣家編號");
                    return false;
                }

                if(!confirm("進行搬移賣家前；請先確認檔次異動是否皆已完成儲存？\n若未完成儲存請先進行儲存處理，搬移賣家完成後；\n將會自動重新載入檔次頁面，尚未儲存資料將會流失！")) {
                    return false;
                }

                $.ajax({
                    type: "POST",
                    url: "../Seller/DealMoveSeller",
                    data: " { 'dealGuid': '<%= BusinessHourId %>','sellerId':'" + $("#<%=tbNewSellerId.ClientID%>").val() + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        if (response.Result) {
                            alert("搬移賣家完成；將重新載入頁面！\n請重新進行【核銷相關】設定！");
                            window.location.reload();
                        }
                        else {
                            $("#spnMoveSellerResult").text(response.Message);
                            return false;
                        }
                    },
                    error:function(response){
                        alert(response.status);
                    }
                });
            });
        });


        function PiinLifeOnChecked(){
            $('#channelCategory' + '<%= CategoryManager.Default.Piinlife.CategoryId %>').on('change', function () {
                if ($(this).is(':checked')) {
                    //頻道[品生活]被勾選時，活動搶購時間預設為12:00 (但若人工有再去調整這個活動搶購時間，則以最後儲存的時間為主。)
                    $('#<%= dlOSh.ClientID %> option[value=12]').prop('selected', true);
                    $('#<%= dlOSm.ClientID %> option[value=0]').prop('selected', true);
                    $('#<%= dlOEh.ClientID %> option[value=12]').prop('selected', true);
                    $('#<%= dlOEm.ClientID %> option[value=0]').prop('selected', true);
                }
                else {
                    if (confirm('是否要把活動搶購時間改回07:00~01:00')) {
                        $('#<%= dlOSh.ClientID %> option[value=7]').prop('selected', true);
                        $('#<%= dlOSm.ClientID %> option[value=0]').prop('selected', true);
                        $('#<%= dlOEh.ClientID %> option[value=1]').prop('selected', true);
                        $('#<%= dlOEm.ClientID %> option[value=0]').prop('selected', true);
                    }
                }
            });
        }
        function ConvertToImage(btnPreviewMenu){
            var tbMenuD = CKEDITOR.instances.tbMenuD.getData();
            var tbMenuE = CKEDITOR.instances.tbMenuE.getData();
            var tbMenuF = CKEDITOR.instances.tbMenuF.getData();
            var tbMenuG = CKEDITOR.instances.tbMenuG.getData();

            $("#menuContent").html(tbMenuD + "<br />" + tbMenuE + "<br />" + tbMenuF + "<br />" + tbMenuG);
            $("#menuContent").removeClass("hide");
            try{
                html2canvas($("#menuContent"), {
                    onrendered: function(canvas) {
                        var base64 = canvas.toDataURL();
                        $("[id*=hfImageData]").val(base64);
                        //__doPostBack(btnPreviewMenu.name, "");
                        $.ajax({
                            type: "POST",
                            url: "Setup.aspx/PreviewMenu",
                            data: "{ 'bid':'<%=BusinessHourId%>', 'base64': '" + $("[id*=hfImageData]").val() + "','CloseMunuContent':'" + $("#chkCloseMunuContent").is(":checked") + "' }",
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function(msg) {
                                var c = msg.d;
                                if(c != null && c != ""){
                                    $("#menuPreContent").html("<img src='" + c  + "?dummy=" + guid() + "' />");
                                    $("#menuContent").addClass("hide");
                                }else{
                                    alert("預覽失敗!");
                                    $("#menuContent").addClass("hide");
                                }
                            },
                            error: function(response, q, t) {
                                console.log("");
                            }
                        });
                    }
                });

            }catch(e){
                console.log(e);
            }
            
            return false;
        }
        function MenuImageUpload(){
            var formdata = new FormData();

            var files = $("#fileMenu")[0].files;
            $.each(files, function (i, file) {
                formdata.append(file.name, file);
            });

            formdata.append("bid", "<%=BusinessHourId%>");
            $("#fileMuneUpdImage").html("");

            $.ajax({
                url: "MenuFileHandler.ashx",
                type: "POST",
                contentType: false,
                processData: false,
                data: formdata,
                success: function (data) {
                    if(data != ""){
                        alert("上傳成功");
                        $("#fileMuneUpdImage").html("<img src='" + data + "?dummy=" + guid() + "' />");
                    }else{
                        alert("上傳失敗");
                    }
                },
                error: function (xhr) {
                    console.log(xhr.responseText);
                }
            });
        }

        function guid() {
            function s4() {
                return Math.floor((1 + Math.random()) * 0x10000)
                  .toString(16)
                  .substring(1);
            }
            return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
              s4() + '-' + s4() + s4() + s4();
        }

        function OpenPageConfirmDialog() {
            $.ajax({
                url: '/controlroom/PponSetup/SetupPageConfirmPreview',
                method: "POST",//1.9.0
                type:"POST",//prior 1.9.0
                data: {
                    bid: '<%=BusinessHourId%>',
                    isMultiDeal: ('<%=HidIsMulti.Value%>' == '1')
                },
                dataType: "html",//expected
                success:function(page){
                    $("<div id='previewDialog' style='background-color: white;'>").append(page).dialog({
                        title: '文字稿預覽',
                        modal:false,
                        height: 720,
                        width: 950,
                        close: function () {
                            $(this).dialog('destroy');
                            $(this).remove();
                        },
                        open: function () {
                            ShowUnfilledColumnHint();//defined in _PponSetupPageConfirmPreview.cshtml
                        }
                    }).dialog('open');
                },
                error:function(){
                },
            });
        }

        function CheckBeforeCopy(clickedBtn){
            var isOK = true;
            var msg = "";

            //宅配合約未同意不能複製
            if ("<%=config.IsRemittanceFortnightly%>" == "True")
            {
                if ($('select[id$=ddlRreceiveType] option:selected').val() == "<%=(int)DeliveryType.ToHouse%>" && '<%=DeptId%>' == '<%=EmployeeChildDept.S010.ToString()%>' && "<%=IsAgreeHouseNewContractSeller%>" == "False")
                {
                    isOK = false;
                    msg += "商家尚未同意新的條款內容，無法複製提案。";
                }
            }


            //出帳方式檢查
            if (isOK && "<%=SellerRemittanceType == null%>" == "True") {
                if ($('select[id$=ddlRreceiveType] option:selected').val() == "<%=(int)DeliveryType.ToShop%>" || ($('select[id$=ddlRreceiveType] option:selected').val() == "<%=(int)DeliveryType.ToHouse%>" && '<%=DeptId%>' != '<%=EmployeeChildDept.S010.ToString()%>'))
                {
                    isOK = false;
                    msg += "出帳方式為舊版核銷系統，請重新到商家資料頁確認出帳方式才可進行複製。";
                }
                
            }
            //← add new check from here

            //final:最後確認
            if (isOK) {
                if(clickedBtn.id == "<%=btnCopyDeal.ClientID%>" ||
                   clickedBtn.id == "<%=btnCopySubDeal.ClientID%>"){
                    return confirm('複製作業必須先儲存完畢後才可進行，確定要進行複製嗎?');
                }
                else if (clickedBtn.id == "<%=btnCopyComboDeals.ClientID%>") {
                    return confirm('整包複製作業必須先儲存所有相關檔次後才可進行，確定要進行複製嗎?');
                }
                else {
                    return false;
                }
            }
            else {
                alert(msg);
                return false;
            }
        }

        function padLeft(str, lenght) {
            //靠左補零
            str = '' + str;
            if (str.length >= lenght)
                return str;
            else
                return padLeft("0" + str, lenght);
        }

        function CheckPromoDeal(obj) {
            if ($(obj).attr("id") == "cbx_PromotionDeal") {
                //同步勾選「演出時間前十日內不能退貨」                
                if ($(obj).is(":checked")) {
                    var chkPromotionDeal = $("#cbx_ExhibitionDeal").is(":checked");
                    if (chkPromotionDeal) {
                        event.preventDefault();
                        alert("【展演類型檔次】及【展覽類型檔次】只能擇一勾選");
                        return false;
                    }
                    var chkNoRefundBeforeDays = $("#<%=chkNoRefundBeforeDays.ClientID%>");
                    $(chkNoRefundBeforeDays).attr("checked", $(obj).is(":checked"));
                    $('#<%=cbIsReserveLock.ClientID%>').attr("checked", true);
                }
                ClickPromotionDeal();
            }
            if ($(obj).attr("id") == "cbx_ExhibitionDeal") {
                //同步勾選「過期不能退貨」
                if ($(obj).is(":checked")) {
                    var chkPromotionDeal = $("#cbx_PromotionDeal").is(":checked");
                    if (chkPromotionDeal) {
                        event.preventDefault();
                        alert("【展演類型檔次】及【展覽類型檔次】只能擇一勾選");
                        return false;
                    }
                    var cbx_ExpireNoRefund = $("#<%=cbx_ExpireNoRefund.ClientID%>");
                    $(cbx_ExpireNoRefund).attr("checked", $(obj).is(":checked"));
                }                
            }
        }

        function CheckNoRefundDeal(obj) {
            var isExpireNoRefund = $("#<%=cbx_ExpireNoRefund.ClientID%>").is(":checked");
            var isNoRefundBeforeDays = $("#<%=chkNoRefundBeforeDays.ClientID%>").is(":checked");

            //演出時間前十日內不能退貨
            if ($(obj).attr("id") == "<%=chkNoRefundBeforeDays.ClientID%>") {                
                if ($(obj).is(":checked")) {
                    if (isExpireNoRefund) {
                        event.preventDefault();
                        alert("【演出時間前十日內不能退貨】及【過期不能退貨】只能擇一勾選");
                        return false;
                    }
                }
                //var cbx_PromotionDeal = $("#cbx_PromotionDeal");
                //$(cbx_PromotionDeal).attr("checked", $(obj).is(":checked"));
            }
            //過期不能退貨
            if ($(obj).attr("id") == "<%=cbx_ExpireNoRefund.ClientID%>") {                
                if ($(obj).is(":checked")) {                    
                    if (isNoRefundBeforeDays) {
                        event.preventDefault();
                        alert("【演出時間前十日內不能退貨】及【過期不能退貨】只能擇一勾選");
                        return false;
                    } 
                }
                //var cbx_ExhibitionDeal = $("#cbx_ExhibitionDeal");
                //$(cbx_ExhibitionDeal).attr("checked", $(obj).is(":checked"));
            }
        }

        function CheckGame(obj) {
            if ($(obj).is(":checked"))
            {
                var chkNotShowDealDetailOnWeb = $("#<%=cbx_NotShowDealDetailOnWeb.ClientID%>");
                var chkNotAllowedDiscount = $("#<%=cbx_NotAllowedDiscount.ClientID%>");
                

                $(chkNotShowDealDetailOnWeb).attr("checked", true);
                $(chkNotAllowedDiscount).attr("checked", true);
            }
            
        }

        function SetCKEditor() {
            if ($.query.get('bid') != '') {
                var bidpath = $.query.get('bid');
                var isPiinlife = $('input[id=channelCategory148]').is(":checked");
                var maxwidth = isPiinlife ? 650 : 1024; //品生活圖片自動縮至650，一般好康縮為560
                var foldercat=bidpath.substring(0,1);
                var imageupload = '/Tools/ckeditor/ckfinder/core/connector/aspx/connector.aspx?command=QuickUpload&type=Images&bid=' + bidpath + '&maxwidth=' + maxwidth;
                var uploadUrl = '/Tools/ckeditor/ckfinder/core/connector/aspx/connector.aspx?command=QuickUpload&type=Files&bid=' + bidpath + '&responseType=json&maxwidth=' + maxwidth;

                editorDesc = CKEDITOR.replace('<%=tbSlrInt.ClientID%>', {
                    uploadUrl: uploadUrl,
                    filebrowserImageUploadUrl: imageupload
                });
                <%if (pnProSpc.Visible) { %> 
                    CKEDITOR.replace('<%=tbProSpc.ClientID%>', {
                        uploadUrl: uploadUrl,
                        filebrowserImageUploadUrl: imageupload
                    });
                <%}%>
                CKEDITOR.replace('<%=rmkTxt.ClientID%>', {
                    uploadUrl: uploadUrl,
                    filebrowserImageUploadUrl: imageupload
                });
                CKEDITOR.replace('<%=txtMutiTab1.ClientID%>', {
                    uploadUrl: uploadUrl,
                    filebrowserImageUploadUrl: imageupload
                });
                CKEDITOR.replace('<%=txtMutiTab2.ClientID%>', {
                    uploadUrl: uploadUrl,
                    filebrowserImageUploadUrl: imageupload
                });
                CKEDITOR.replace('<%=txtMutiTab3.ClientID%>', {
                    uploadUrl: uploadUrl,
                    filebrowserImageUploadUrl: imageupload
                });
                CKEDITOR.replace('<%=txtMutiTab4.ClientID%>', {
                    uploadUrl: uploadUrl,
                    filebrowserImageUploadUrl: imageupload
                });
                CKEDITOR.replace('<%=txtMutiTab5.ClientID%>', {
                    uploadUrl: uploadUrl,
                    filebrowserImageUploadUrl: imageupload
                });
                CKEDITOR.replace('<%=txtMutiTab6.ClientID%>', {
                    uploadUrl: uploadUrl,
                    filebrowserImageUploadUrl: imageupload
                });
                b_editor = CKEDITOR.replace('<%=tbPdtl.ClientID%>', {
                    uploadUrl: uploadUrl,
                    filebrowserImageUploadUrl: imageupload
                });
                CKEDITOR.replace('tbMenuD', {
                    height :70,
                    toolbar: [
		                [ 'Source','-', 'Bold', '-','TextColor', 'FontSize','JustifyLeft', 'JustifyCenter', 'JustifyRight'],			// Defines toolbar group without name.
                    ]
                });
                CKEDITOR.replace('tbMenuE', {
                    height :100,
                    toolbar: [
		                [ 'Source','-', 'Bold', '-','TextColor', 'FontSize','JustifyLeft', 'JustifyCenter', 'JustifyRight' ],			// Defines toolbar group without name.
                    ]
                });

                CKEDITOR.replace('tbMenuF', {
                    height :200,
                    toolbar: [
		                ['Source','-', 'Bold', '-','TextColor', 'FontSize','JustifyLeft', 'JustifyCenter', 'JustifyRight' ],			// Defines toolbar group without name.
                    ]
                });
                CKEDITOR.replace('tbMenuG', {
                    height :100,
                    toolbar: [
		                [ 'Source','-', 'Bold', '-','TextColor', 'FontSize','JustifyLeft', 'JustifyCenter', 'JustifyRight' ],			// Defines toolbar group without name.
                    ]
                });
            } else {
                CKEDITOR.replace('<%=tbSlrInt.ClientID%>');
                <%if (pnProSpc.Visible) { %> 
                    CKEDITOR.replace('<%=tbProSpc.ClientID%>');
                <%}%>
                CKEDITOR.replace('<%=rmkTxt.ClientID%>');
                CKEDITOR.replace('<%=txtMutiTab1.ClientID%>');
                CKEDITOR.replace('<%=txtMutiTab2.ClientID%>');
                CKEDITOR.replace('<%=txtMutiTab3.ClientID%>');
                CKEDITOR.replace('<%=txtMutiTab4.ClientID%>');
                CKEDITOR.replace('<%=txtMutiTab5.ClientID%>');
                CKEDITOR.replace('<%=txtMutiTab6.ClientID%>');
                b_editor = CKEDITOR.replace('<%=tbPdtl.ClientID%>');
             }
        }
    </script>
</asp:Content>

<asp:Content ID="M" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:HiddenField ID="hidBusinessOrderFreight" runat="server" />
    <asp:HiddenField ID="hidBusinessOrderNonFreightLimit" runat="server" />
    <asp:Label ID="lbFPTypeDesc" runat="server" Font-Size="Medium" ForeColor="Red"></asp:Label>
    <asp:HiddenField ID="hdFPType" runat="server" />
    <asp:HiddenField ID="HidIsMulti" runat="server" />
    <asp:HiddenField ID="hidGrossLimitVal" runat="server" />
    <asp:HiddenField ID="hidMinGrossMarginVal" runat="server" />
    <asp:HiddenField ID="hidProposalId" runat="server" />
    <asp:HiddenField ID="hidProposalCreateType" runat="server" />
    <asp:HiddenField ID="hidProposalConsignment" runat="server" />
    <asp:HiddenField ID="hidisSub" runat="server" />
    <asp:HiddenField ID="hidPresentQuantity" runat="server" />
    <asp:HiddenField ID="hidPresentQuantityEnable" runat="server" />
    <asp:HiddenField ID="hidProDealType" runat="server" />
    <span style="display:none"><asp:CheckBox ID="chk_dealMain" runat="server" ClientIDMode="Static" /></span>
    <div id="container">
        <asp:PlaceHolder ID="divProposalFlag" runat="server" Visible="false">
            <asp:Repeater ID="rptProposalFlag" runat="server" OnItemDataBound="rptProposalFlag_ItemDataBound" OnItemCommand="rptProposalFlag_ItemCommand">
                <ItemTemplate>
                    <asp:Button ID="btnFlag" runat="server" Font-Size="Small" />
                </ItemTemplate>
            </asp:Repeater>
            <asp:HyperLink ID="hkProposal" runat="server" Target="_blank" Font-Size="Small" ForeColor="Blue">前往提案單</asp:HyperLink>
            <asp:Button ID="btnSendPageConfirmMail" runat="server" Font-Size="Small" Visible="false" Text="發送文字稿" OnClientClick="OpenPageConfirmDialog(); return false;"/>
        </asp:PlaceHolder>
        <div id="tabs">
            <ul>
                <li><a href="#tabs-1">基本資料</a></li>
                <li><a href="#tabs-2" class="tabs2">圖片</a></li>
                <li style="display: none;" id="mu"><a href="#tabs-9">MENU</a></li>
                <li><a href="#tabs-3">說明</a></li>
                <li><a href="#tabs-4">其他設定</a></li>
                <li id="sc"><a href="#tabs-6">多重選項</a></li>
                <li style="display: none;" id="cp"><a href="#tabs-8">成套票券</a></li>
                <li><a href="#tabs-7" style='<%# (this.ComboSetupEnabled) ? "display: none" : string.Empty %>'>多檔次</a></li>
                <li><a href="#tabs-10">核銷相關</a></li>
                <li><a href="#tabs-5">預覽</a></li>
            </ul>
            <div id="tabs-1">
                <fieldset>
                    <legend>名稱/狀態</legend>
                    <p>
                        <asp:HyperLink ID="hlStoreAdd" runat="server" Style="color: #00F; text-decoration: underline; border: 0"
                            Text="賣家名稱" Font-Size="Small" Target="_blank"></asp:HyperLink>
                        <asp:PlaceHolder ID="plMoveSeller" runat="server" Visible="false">                            
                            <label style="color:darkmagenta; font-weight:bold">&nbsp;&nbsp;欲搬移賣家編號：</label>
                            <asp:TextBox ID="tbNewSellerId" runat="server" ClientIDMode="Static" />&nbsp;
                            <input type="button" id="btnMoveSeller" name="btnMoveSeller" value="搬移賣家"/>&nbsp;
                            <asp:Label ID="spnMoveSellerResult" runat="server" ClientIDMode="Static" ForeColor="red" />
                        </asp:PlaceHolder>
                        <br />
                        檔號：<asp:Label ID="lblBusinessHourUniqueId" runat="server"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        工單編號：<asp:TextBox ID="txtBusinessOrderId" runat="server" onkeyup="ClearEmptyData(this);"></asp:TextBox>
                        <asp:Button ID="btnImportBusinessOrderInfo" runat="server" Text="與工單串接" ValidationGroup="Import"
                            OnClick="btnImportBusinessOrderInfo_Click" OnClientClick="return confirm('確定帶入工單資訊?');"
                            Visible="false" ClientIDMode="Static" /><br />
                        <asp:RequiredFieldValidator ID="rfvBusinessOrderId" runat="server" ControlToValidate="txtBusinessOrderId"
                            Display="Dynamic" ErrorMessage="請填寫工單編號。" ValidationGroup="Import"></asp:RequiredFieldValidator>
                    </p>
                    <table class="table_font">
                        <tr>
                            <td width="360">
                                <label>
                                    業務：</label>
                                <asp:TextBox ID="tbDevelopeSales" runat="server" ClientIDMode="Static" />
                                <asp:TextBox ID="tbOperationSales" runat="server" ClientIDMode="Static" />
                            </td>
                            <td width="150">
                                <label>
                                    消費方式：</label>
                                <asp:DropDownList ID="ddlRreceiveType" runat="server" AutoPostBack="true" OnTextChanged="ddlRreceiveType_TextChanged">
                                    <asp:ListItem Value="1">憑證消費</asp:ListItem>
                                    <asp:ListItem Value="2">宅配</asp:ListItem>
                                </asp:DropDownList>

                            </td>
                            <td width="140">
                                <label>
                                    &nbsp;</label>
                                <asp:CheckBox ID="cbx_ShoppingCar" runat="server" CssClass="showUl" onclick="showActivity();"
                                    ClientIDMode="Static" />多重選項
                            </td>
                            <td width="140">
                                <label>
                                    &nbsp;</label>
                                <asp:CheckBox ID="cbx_multiBranch" runat="server" CssClass="showUl" />包含大量分店
                            </td>
                            <td width="170">
                                <label>
                                    &nbsp;</label>
                                <asp:CheckBox ID="cbx_NotShowDealDetailOnWeb" runat="server" Text="無條件導回首頁" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="5">
                                檔次類型：&nbsp;&nbsp;
                                <asp:CheckBox ID="cbx_isLongContract" class="aspnet_checkbox" runat="server" Text="長期約" />&nbsp;&nbsp;
                                <asp:CheckBox ID="cbx_CompleteCopy" class="aspnet_checkbox" runat="server" Text="完全複製檔" />&nbsp;&nbsp;
                                <asp:CheckBox ID="cbx_NoRestrictedStore" class="aspnet_checkbox" runat="server" Text="通用券商品" />&nbsp;&nbsp;
                                <asp:CheckBox ID="cbx_Consignment" runat="server" Text="商品寄倉" />&nbsp;&nbsp;
                                <asp:CheckBox ID="cbx_GroupCoupon" class="aspnet_checkbox" Text="成套票券" runat="server" onclick="showCp();" ClientIDMode="Static" />&nbsp;&nbsp;                                
                                <asp:DropDownList ID="ddlGroupCouponType" runat="server" ClientIDMode="Static" style="display:none" />&nbsp;&nbsp;
                                <asp:CheckBox ID="cbx_TaiShin" class="aspnet_checkbox" runat="server" Text="限刷台新信用卡" />&nbsp;&nbsp;
                                <asp:CheckBox ID="cbx_BankDeal" class="aspnet_checkbox" runat="server" Text="銀行合作" />&nbsp;&nbsp;
                                <asp:CheckBox ID="cbx_PromotionDeal" class="aspnet_checkbox" ClientIDMode="Static" runat="server" Text="展演類型檔次-鎖定功能(逾期自動核銷)" onclick="CheckPromoDeal(this)" />&nbsp;&nbsp;
                                <asp:CheckBox ID="cbx_ExhibitionDeal" class="aspnet_checkbox" ClientIDMode="Static" runat="server" Text="展覽類型檔次(逾期自動核銷)" onclick="CheckPromoDeal(this)" />&nbsp;&nbsp;<br />
                                <asp:CheckBox ID="cbx_isDealDiscountPriceBlacklist" class="aspnet_checkbox" ClientIDMode="Static" runat="server" Text="不顯示折後價於前台" />&nbsp;&nbsp;
                                <asp:CheckBox ID="cbx_Game" class="aspnet_checkbox" ClientIDMode="Static" runat="server" Text="活動遊戲檔" onclick="CheckGame(this)"/>&nbsp;&nbsp;
                                <asp:CheckBox ID="cbx_Pc24H" class="aspnet_checkbox" ClientIDMode="Static" runat="server" Text="24H到貨" Enabled="False" />&nbsp;&nbsp;
                                <asp:CheckBox ID="cbx_ChannelGift" class="aspnet_checkbox" ClientIDMode="Static" runat="server" Text="代銷贈品檔" onclick="CheckGame(this)" Enabled="false"/>&nbsp;&nbsp;
                                <asp:CheckBox ID="cbx_TaiShinChosen" class="aspnet_checkbox" ClientIDMode="Static" runat="server" Text="台新特談商品"/>&nbsp;&nbsp;<br />
                                <asp:CheckBox ID="cbx_CChannel" class="aspnet_checkbox" ClientIDMode="Static" runat="server" Text="CChannel" />&nbsp;&nbsp;
                                <asp:TextBox ID="txtCChannelLink" runat="server" ClientIDMode="Static" Width="300"></asp:TextBox>                                
                            </td>
                        </tr>
                        <tr>
                            <td>代銷通路：
                                <asp:CheckBoxList ID="chkAgentChannels" runat="server" RepeatDirection="Horizontal" Font-Size="Small" CssClass="CouponDealTag"></asp:CheckBoxList>
                            </td>
                        </tr>
                    </table>
                    <fieldset style="width: 850px">
                        <legend>標籤設定</legend>
                        <div id="CouponDealTag" class="divCouponDealTag">
                            <label>憑證類：</label>
                            <asp:CheckBoxList ID="cbl_couponDealTag" runat="server" Font-Size="Small" RepeatLayout="Table"
                                              RepeatDirection="Horizontal" RepeatColumns="7" CssClass="CouponDealTag">
                            </asp:CheckBoxList>
                            <div id="CouponTravel" class="divCouponTravelTag">
                                <label>旅遊類：</label>
                                <asp:CheckBoxList ID="cbl_couponTravelDealTag" runat="server" Font-Size="Small" RepeatLayout="Table"
                                                  RepeatDirection="Horizontal" RepeatColumns="7" CssClass="CouponTravelDealTag">
                                </asp:CheckBoxList>
                            </div>
                        </div>
                        <div id="DeliveryDealTag" class="divDeliveryDealTag">
                            <label>宅配類：</label>
                            <asp:CheckBoxList ID="cbl_deliveryDealTag" runat="server" Font-Size="Small" RepeatLayout="Table"
                                              RepeatDirection="Horizontal" RepeatColumns="7" CssClass="DeliveryDealTag PpContent">
                            </asp:CheckBoxList>
                            <div id="DeliveryTravel" class="divDeliveryTravelTag">
                                <label>旅遊類：</label>
                                <asp:CheckBoxList ID="cbl_deliveryTravelDealTag" runat="server" Font-Size="Small"
                                                  RepeatLayout="Table"
                                                  RepeatDirection="Horizontal" RepeatColumns="7" CssClass="DeliveryTravelDealTag">
                                </asp:CheckBoxList>
                            </div>
                        </div>
                        <div id="CustomTag">
                            <asp:CheckBox ID="cb_CustomTag" ClientIDMode="Static" runat="server" />
                            <asp:TextBox ID="txt_CustomTag" ClientIDMode="Static" MaxLength="11" runat="server"></asp:TextBox>
                        </div>
                    </fieldset>
                    <table class="table_font" style="display:none;">
                        <tr class="pponcontent hidecontent">
                            <td>引言：<asp:TextBox ID="txtIntro" runat="server" CssClass="dealName PpContent" MaxLength="100" Width="200px" ClientIDMode="Static" />
                            </td>
                            <td>價格描述：<asp:TextBox ID="txtPriceDesc" runat="server" CssClass="dealName PpContent" MaxLength="100" Width="200px" ClientIDMode="Static" placeholder="(雙人價)" />
                            </td>
                        </tr>
                        <tr class="pponcontent">
                            <td colspan="2">
                                內文：<asp:CheckBox ID="chkHideContent" Text="前台不顯示內文" runat="server" ClientIDMode="Static" />
                            </td>
                        </tr>
                        <tr class="pponcontent hidecontent">
                            <td colspan="2">
                                <asp:Label ID="lblContentRest" runat="server" Text="" ClientIDMode="Static" Width="100%" Style="background-color: #f8f6bf; max-width: 500px"></asp:Label>
                            </td>
                        </tr>
                    </table>

                    <p>
                        <label>
                            優惠短標(APP標題)
                        </label>
                        <asp:TextBox ID="tbAppTitle" runat="server" MaxLength="40" Width="500px"></asp:TextBox>
                    </p>
                    <p>
                        <label>
                            訂單短標(品牌名稱-商品)：</label>
                        <asp:TextBox ID="tbUse" runat="server" Columns="50" MaxLength="250" CssClass="PpContent"></asp:TextBox>
                        <br />
                        <label>憑證檔次變更訂單短標，請一併確認＜其他設定－簡訊內容－前置文＞資料是否需要修改；前置文若無填寫，則簡訊的前置文會直接套入訂單短標</label>
                    </p>
                    <p>
                        <label>
                            行銷標題(橘標)：</label>
                        <asp:TextBox ID="tTitle" runat="server" CssClass="dealName" MaxLength="250" Width="700px" />
                    </p>
                    <p>
                        <label>方案說明:</label><br />
                        <asp:TextBox ID="tbN" runat="server" CssClass="dealName" MaxLength="<%# DealContentNameLength%>"
                                Width="500px"
                                TextMode="MultiLine" Height="120" />
                    </p>
                    <div runat="server" id="divPdfItemName" style="display:none">
                        <p>
                            <label>
                                PDF憑證顯示名稱(預設帶入代表檔訂單短標)：</label>
                            <asp:TextBox ID="tbPdf" runat="server" Columns="50" MaxLength="250" CssClass="PpContent"></asp:TextBox>
                        </p>
                    </div>
                    <p>
                        <label>
                            使用地點：</label>
                        <asp:TextBox ID="txtTravelPlace" runat="server" Columns="50" MaxLength="100" />
                    </p>
                    <p>
                        <div id="sortDealCategory">
                        <label style="font-weight: bold">上檔頻道(依排檔區域勾選)：</label>
                        <asp:HiddenField ID="hiddenChannelChecked" runat="server" Value="[]" />
                        <asp:HiddenField ID="hiddenSubChannelChecked" runat="server" Value="[]" />
                        <asp:HiddenField ID="hiddenSubSpecialRegionChecked" runat="server" Value="[]" />
                        <asp:HiddenField ID="hiddenSubSpecialSubRegionChecked" runat="server" Value="[]" />
                        <asp:HiddenField ID="hiddenChannelJSon" runat="server" Value="[]" />
                        <asp:HiddenField ID="hiddenChannelCategoryJSon" runat="server" Value="[]" />
                        <asp:HiddenField ID="hiddenChannelSpecialCategoryJSon" runat="server" Value="[]" />
                        <asp:Repeater runat="server" ID="channelCheckArea" OnItemDataBound="channelCheckArea_ItemDataBound">
                            <HeaderTemplate>
                                <table class="categoryArea">
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td style="width: 12%;">
                                        <input type="checkbox" id="channelCategory<%#((CategoryNode)Container.DataItem).CategoryId %>"
                                            onchange="OnChannelCategoryChange();<%#(((CategoryNode)Container.DataItem).CategoryId==CategoryManager.Default.Piinlife.CategoryId) ? "setATMQtyWithSettingDealType();" : string.Empty%>" />
                                        <label id="channelCategoryLabel<%#((CategoryNode)Container.DataItem).CategoryId %>"
                                            onclick="$('#channelCategory<%#((CategoryNode)Container.DataItem).CategoryId %>').click()">
                                            <%# ((CategoryNode)Container.DataItem).NameInConsole %>
                                        </label>
                                        <asp:HiddenField ID="channelCheckId" runat="server" />
                                    </td>
                                    <td id="subChannelCol<%#((CategoryNode)Container.DataItem).CategoryId %>" style="text-align: left; width: 88%;">
                                        <asp:Repeater runat="server" ID="subChannelCheckArea">
                                            <ItemTemplate>
                                                <input type="checkbox" id="subChannelCategory<%#((CategoryNode)Container.DataItem).CategoryId %>"
                                                    disabled="disabled" onchange="OnChannelCategoryChange()" />
                                                <label onclick="$('#subChannelCategory<%#((CategoryNode)Container.DataItem).CategoryId %>').click()"
                                                    style="color: gray;">
                                                    <%# ((CategoryNode)Container.DataItem).NameInConsole %>
                                                </label>
                                            </ItemTemplate>
                                        </asp:Repeater>

                                    </td>
                                </tr>
                                <tr class="content" id="specialRegion<%#((CategoryNode)Container.DataItem).CategoryId %>">
                                    <td style="width: 12%;"></td>
                                    <td style="text-align: left; width: 88%;">
                                        <asp:Repeater runat="server" ID="subChannelSPRegionCheckArea" OnItemDataBound="subChannelSPRegionCheckArea_ItemDataBound">
                                            <HeaderTemplate>
                                                <table>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr class="content" id="subChannelSPRegionCategory<%#((CategoryNode)Container.DataItem).CategoryId %>">
                                                    <td style="text-align: left;">
                                                        <%--<label>商圈‧景點：</label>--%>
                                                        <label><%# ((CategoryNode)Container.DataItem).NodeDatas.Count > 0 ? ((CategoryNode)Container.DataItem).NodeDatas.FirstOrDefault().CategoryNodes.FirstOrDefault().CategoryName : "商圈‧景點：" %></label>
                                                        <asp:Repeater runat="server" ID="subChannelSpecialRegionCheckArea">
                                                            <ItemTemplate>
                                                                <input type="checkbox" id="subChannelSpecialRegion<%#((CategoryNode)Container.DataItem).CategoryId %>"
                                                                    onchange="OnChannelCategoryChange()" />
                                                                <label onclick="$('#subChannelSpecialRegion<%#((CategoryNode)Container.DataItem).CategoryId %>').click();">
                                                                    <%# ((CategoryNode)Container.DataItem).CategoryName %>
                                                                </label>
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                        <asp:Literal ID="llSpecialRegion" runat="server"></asp:Literal>
                                                </tr>
                                                <tr class="content" id="trSubSPR<%#((CategoryNode)Container.DataItem).CategoryId %>">
                                                    <td style="text-align: left;">
                                                        <asp:Repeater runat="server" ID="subSPRegionCheckArea" OnItemDataBound="subSPRegionCheckArea_ItemDataBound">
                                                            <ItemTemplate>
                                                                <span id="subSPRegionCategory<%#((CategoryNode)Container.DataItem).CategoryId %>">
                                                                    <asp:Repeater runat="server" ID="subSpecialRegionCheckArea">
                                                                        <ItemTemplate>
                                                                            <input type="checkbox" id="subSpecialRegion<%#((CategoryNode)Container.DataItem).CategoryId %>"
                                                                                onchange="OnChannelCategoryChange()" />
                                                                            <label onclick="$('#subSpecialRegion<%#((CategoryNode)Container.DataItem).CategoryId %>').click();">
                                                                                <%# ((CategoryNode)Container.DataItem).CategoryName %>
                                                                            </label>
                                                                        </ItemTemplate>
                                                                    </asp:Repeater>
                                                                </span>
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                    </td>
                                                </tr>
                                                </td>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                </table>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                    </td>
                                </tr>
                                <tr id="categoryAreaHeader<%#((CategoryNode)Container.DataItem).CategoryId %>">
                                    <td style="width: 12%;"></td>
                                    <td style="width: 88%;">
                                        <table class="categoryArea">
                                            <tr>
                                                <td>
                                                    <label>新分類：</label></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr id="categoryAreaContent<%#((CategoryNode)Container.DataItem).CategoryId %>">
                                    <td style="width: 12%;"></td>
                                    <td style="width: 88%;">
                                        <table class="categoryArea" id="dealCategoryArea<%#((CategoryNode)Container.DataItem).CategoryId %>">
                                        </table>
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                        </asp:Repeater>
                        </div>
                    </p>
                    <asp:HiddenField runat="server" ID="hiddenDealCategory" Value="[]" />
 
                    <div id="originalDealCategory" style="display: none;">
                        <%//for js moveCategoryitem don't remove %>
                        <label style="font-weight: bold">新分類：</label>

                        <asp:Repeater runat="server" ID="dealCategoryArea" OnItemDataBound="dealCategoryArea_ItemDataBound">
                            <HeaderTemplate>
                                <table class="categoryArea" id="dealCategoryTable" style="background-color: pink;">
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%# Container.ItemIndex % 5 == 0 ? "<tr>" : "" %>
                                <td style="width: 20%;">
                                    <ul id="dealCategorytree<%#((Category)Container.DataItem).Id %>">
                                        <li>
                                            <input type="checkbox" id="dealCategory<%#((Category)Container.DataItem).Id %>" onchange="OnDealCategoryChange()" disabled="disabled" />
                                            <p id="cid" style="display:none"><%#((Category)Container.DataItem).Id %></p>
                                            <label id="dealCategoryDeas<%#((Category)Container.DataItem).Id %>" style="color: gray;"
                                                onclick="$('#dealCategory<%#((Category)Container.DataItem).Id %>').click()">
                                                <%# ((Category)Container.DataItem).NameInConsole %>
                                                <span id="isShowBackEnd<%#((Category)Container.DataItem).Id %>" style="display:none"><%# ((Category)Container.DataItem).IsShowBackEnd %></span>
                                            </label>


                                            <asp:Repeater runat="server" ID="subDealCategoryArea">
                                                <HeaderTemplate>
                                                    <ul class="subDealCategoryArea">
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <li>
                                                        <input type="checkbox" id="dealCategory<%#((Category)Container.DataItem).Id %>"
                                                            onchange="OnDealCategoryChange()" disabled="disabled" />
                                                        <p id="cid" style="display:none"><%#((Category)Container.DataItem).Id %></p>
                                                        <label onclick="$('#dealCategory<%#((Category)Container.DataItem).Id %>').click();">
                                                            <%# ((Category)Container.DataItem).NameInConsole %>
                                                            <span id="isShowBackEnd<%#((Category)Container.DataItem).Id %>" style="display:none"><%# ((Category)Container.DataItem).IsShowBackEnd %></span>
                                                        </label>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    </ul>   
                                                </FooterTemplate>
                                            </asp:Repeater>
                                    </ul>


                                </td>
                                <%# (Container.ItemIndex % 5 == 4 || Container.ItemIndex == ((IList)((Repeater)Container.Parent).DataSource).Count-1) ? "</tr>" : "" %>
                            </ItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                        </asp:Repeater>
                    </div>
                    <label>Icon：</label>
                    <div class="categoryArea">
                        <asp:CheckBoxList ID="cbl_DefaultIcon" AutoPostBack="false" runat="server" Font-Size="Small"
                            RepeatLayout="Table"
                            RepeatDirection="Horizontal" RepeatColumns="7" DataTextField="Value" DataValueField="Key">
                        </asp:CheckBoxList>
                    </div>
                    <br />
                    <asp:HiddenField runat="server" ID="hiddenDeliveryCategory" Value="[]" />
                    <asp:HiddenField runat="server" ID="hiddenDeliveryIconIdList" Value="[]" />
                    <asp:HiddenField runat="server" ID="hiddenDeliveryCategoryChecked" Value="[]" />

                    <asp:Repeater runat="server" ID="IconArea">
                        <HeaderTemplate>
                            <div class="categoryArea" id="IconCategoryArea">
                        </HeaderTemplate>
                        <ItemTemplate>
                            <input type="checkbox" id="IconCategory<%#((Category)Container.DataItem).Id %>" onchange="OnIconCategoryChange()"
                                disabled="disabled" />
                            <label id="IconCategoryDesc<%#((Category)Container.DataItem).Id %>"
                                onclick="$('#IconCategory<%#((Category)Container.DataItem).Id %>').click()">
                                <div id="IconCategoryRealDesc<%#((Category)Container.DataItem).Id %>">
                                    <%# ((Category)Container.DataItem).NameInConsole %>
                                </div>
                            </label>
                        </ItemTemplate>
                        <FooterTemplate>
                            </div>
                        </FooterTemplate>
                    </asp:Repeater>
                    <p>
                        <label>
                            生活商圈：</label>
                        <asp:Repeater ID="rptCommercialCategory" runat="server" OnItemDataBound="rptCommercialCategory_ItemDataBound">
                            <HeaderTemplate>
                                <ul style="padding-left: 15px;" class="CommercialLabel">
                            </HeaderTemplate>
                            <ItemTemplate>
                                <li><span class="divCommercial">
                                    <asp:HiddenField ID="hidCommercialCategoryId" runat="server"></asp:HiddenField>
                                    <asp:CheckBox ID="chkCommercialArea" runat="server" onclick="ShowCommercialList(this);" />
                                    <span id="CommercialCategoryList" style="font-size: small; display: none;">
                                        <asp:CheckBoxList ID="chklCommercialCategory" runat="server" RepeatDirection="Horizontal"
                                            RepeatColumns="7" RepeatLayout="Table">
                                        </asp:CheckBoxList></span>
                                </span>
                                </li>
                            </ItemTemplate>
                            <FooterTemplate>
                                </ul>
                            </FooterTemplate>
                        </asp:Repeater>
                    </p>
                </fieldset>
                <fieldset>
                    <legend>SEO設定</legend>視窗標題：<br />
                    <asp:TextBox ID="tbTitle" runat="server" Width="600" MaxLength="32"></asp:TextBox>
                    <br />
                    網頁敘述：<br />
                    <asp:TextBox ID="tbDesc" runat="server" Width="600" TextMode="MultiLine" Rows="3"></asp:TextBox>
                    <br />
                    圖片替代文字：<br />
                    <asp:TextBox ID="tbAlt" runat="server" Width="600" MaxLength="100"></asp:TextBox>
                    <br />
                    結檔後，轉檔網址(格式需為 https://www.17life.com/event/brandevent、https://www.17life.com/deal/、https://www.17life.com/ppon/pponsearch.aspx?search 開頭)：<br />
                    <asp:TextBox ID="txtExpireRedirectUrl" runat="server" Width="600" MaxLength="100"></asp:TextBox>
                    <br />
                    結檔後，轉檔顯示文字(需先設轉檔網址才有作用)：<br />
                    <asp:DropDownList ID="ddlExpireRedirectDisplay" runat="server" ClientIDMode="Static" />
                    <br />
                    結檔後，設定轉檔網址為canonical：<br />
                    <asp:CheckBox ID="cbUseExpireRedirectUrlAsCanonical" runat="server" Text="啟用" />
                </fieldset>
                <fieldset>
                    <legend>銷售分析用</legend>館別：<asp:DropDownList ID="ddlAccBusGroup" runat="server" ClientIDMode="Static" />
                    <!---->
                    <br />
                    業績歸屬:
                    <asp:DropDownList ID="ddlDept" runat="server" />
                    <asp:DropDownList ID="ddlAccCity" runat="server" Enabled="False" Visible="False" />
                    <br />
                    <div id="dealTypeContainer">
                    分類：
                        <span id="dealTypePanel"></span>
                        <asp:HiddenField ID="hdDealTypeNew" ClientIDMode="Static" runat="server" Value="" />                        
                    </div>
                    <label style="display: inline-block">
                        收據資料：
                        <asp:RadioButton ID="rbNoEntrustSell" runat="server" Style="margin-left: 8px" GroupName="EntrustSell" />無
                        <asp:RadioButton ID="rbKindReceipt" runat="server" Style="margin-left: 8px" GroupName="EntrustSell" />捐款檔次收據
                    </label>
                </fieldset>
                <fieldset>
                    <legend>核銷與帳務設定</legend>
<%--                    匯款資料：
                    <asp:RadioButton ID="rbPayToCompany" GroupName="payto" runat="server" />統一匯至賣家匯款資料
                    <asp:RadioButton ID="rbPayToStore" GroupName="payto" runat="server" />匯至各分店匯款資料
                    <br />--%>
                    對帳方式：
                    <span id="spnBMNone" runat="server" visible="False">
                        <asp:RadioButton ID="rbBMNone" GroupName="VendorBilling" runat="server" Enabled="False" />舊版核銷對帳系統</span>
                    <asp:RadioButton ID="rbBMBalanceSheetSystem" GroupName="VendorBilling" runat="server" />新版核銷對帳系統
                    <asp:RadioButton ID="rbBMMohistSystem" GroupName="VendorBilling" runat="server" />墨攻核銷對帳系統
                    <br />
                    出帳方式：
                    <asp:RadioButton ID="rbAchWeekly" GroupName="achType" runat="server" />ACH每週出帳
                    <asp:RadioButton ID="rbManualWeekly" GroupName="achType" runat="server" />人工每週出帳
                    <asp:RadioButton ID="rbManualMonthly" GroupName="achType" runat="server" />人工每月出帳
                    <asp:RadioButton ID="rbPartially" GroupName="achType" runat="server" />商品(暫付70%)
                    <asp:RadioButton ID="rbOthers" GroupName="achType" runat="server" />其他付款方式
                    <br />
                    <span runat="server" style="padding-left: 69px">
                        <asp:RadioButton ID="rbWeekly" GroupName="achType" runat="server" />新每週出帳
                        <asp:RadioButton ID="rbMonthly" GroupName="achType" runat="server" />新每月出帳
                        <asp:RadioButton ID="rbFlexible" GroupName="achType" runat="server" />彈性選擇出帳
                        <% if (config.IsRemittanceFortnightly){ %>   
                        <asp:RadioButton ID="rbFortnightly" GroupName="achType" runat="server" />雙周結出帳  
                        <%} %>
                        <asp:HiddenField ID="hidSellerPaidType" runat="server" />
                        <br />
                    </span>
                    店家單據開立方式：
                    <asp:RadioButton ID="rbRecordInvoice" GroupName="record" runat="server" />統一發票
                    <asp:RadioButton ID="rbRecordReceipt" GroupName="record" runat="server" />免用統一發票收據
                    <asp:RadioButton ID="rbRecordOthers" GroupName="record" runat="server" />其他
                    <asp:TextBox ID="tbAccountingMessage" runat="server" />
                    <br />
                    免稅設定：
                    <asp:CheckBox ID="cbInputTax" runat="server" Text="店家發票免稅(無法確認，請洽財會)" />
                    <br />
                    <span style="padding-left: 69px">
                        <asp:CheckBox ID="cbx_NoTax" runat="server" Text="開立免稅發票給消費者(無法確認，請洽財會)" />
                    </span>
                    <br />
                    <asp:Panel ID="pnInvoiceOff" runat="server" Visible="False">
                        發票對開：
                        <asp:RadioButton ID="rbMutualNo" GroupName="mutualInvoice" Checked="true" runat="server" />否
                        <asp:RadioButton ID="rbMutualYes" GroupName="mutualInvoice" runat="server" />是，傭金為：
                        <asp:TextBox ID="tbCommission" runat="server" MaxLength="10"></asp:TextBox>
                    </asp:Panel>
                    <asp:DropDownList ID="ddlVerifyType" runat="server" Visible="False" />
                </fieldset>
                <fieldset>
                    <legend>價格資訊</legend>
                    <asp:HiddenField ID="hdMultiCost" runat="server" />
                    <asp:HiddenField ID="hdCumulativeQuantity" runat="server" />
                    <div style="float: left">
                        <p>
                            <label>
                                原價：</label>
                            <asp:TextBox ID="tbOP" runat="server" placeholder="填原價" />
                        </p>
                        <p>
                            <label>
                                賣價：</label>
                            <asp:TextBox ID="tbP" runat="server" CssClass="editPrice PpContent" onblur="setATMQtyWithSettingDealType();" placeholder="填特價" />
                        </p>
                        <p>
                            <label>
                                上架費份數：</label>
                            <asp:TextBox ID="txtSlottingFeeQuantity" runat="server" Text="0" />
                        </p>
                        <p>
                            <label>
                                進貨價格：</label>
                            <asp:TextBox ID="txtPurchasePrice" runat="server" />
                        </p>

                        <asp:Label ID="lbCostMessage" runat="server" Text="開檔後，如須修改進貨價，請洽Ian或Bella，謝謝！<br>"
                            ForeColor="Red" Visible="False"></asp:Label>
                        <p>
                            <asp:Label ID="grossMarginMessage" runat="server" ForeColor="Red"></asp:Label>
                        </p>
                        <span class="ui-state-highlight">按下"SAVE"儲存後才會計算毛利率，階梯式進貨價以最低毛利率顯示判斷</span>
                        <div>
                            <span style="margin-left: 20px">
                                <asp:CheckBox ID="cbx_PriceZeorShowOriginalPrice" runat="server" /><span style="color: Gray;">0元好康顯示原價</span>
                            </span>
                            <span style="margin-left: 20px">
                                <asp:CheckBox ID="cbx_NoDiscountShown" runat="server" /><span style="color: Gray;">折數更改為特選</span>
                            </span>
                            <span style="margin-left: 20px;">
                                <asp:CheckBox ID="cbx_BuyoutCoupon" runat="server" /><span style="color: Gray;">買斷票券</span>
                            </span>
                            <span style="margin-left: 20px;">
                                <asp:CheckBox ID="cbx_DonotInstallment" runat="server" /><span style="color: Gray;">不做分期</span>
                            </span><br />
                            <asp:PlaceHolder runat="server" ID="pCbxMinGrossMargin" Visible="True">
                                <span>
                                    <asp:CheckBox ID="cbx_LowGrossMarginAllowedDiscount" runat="server" Visible="false"/><%--<span style="color: Gray;">低毛利率可用折價券</span>--%>
                                </span>
                                <span style="margin-left: 20px;">
                                    <asp:CheckBox ID="cbx_NotAllowedDiscount" runat="server" /><span style="color: Gray;">本檔不可使用折價券</span>
                                </span>
                            </asp:PlaceHolder>
                        </div>
                    </div>
                    <div class="price" style="float: right">
                        <p>
                            <label>
                                折扣數：</label>
                            <span id="discountRate"></span>折
                        </p>
                        <p>
                            <label>
                                節省金額：</label>
                            $<span id="discountPrice"></span>
                        </p>
                        <asp:PlaceHolder runat="server" ID="plbGrossMargin" Visible="false">
                            <p>
                                <label>毛利率：</label>
                                <span id="GrossMargin">N/A</span>%
                            </p>
                        </asp:PlaceHolder>
                    </div>
                    <asp:TextBox onkeyup="checkfrm()" name="tbFt" ID="tbFt" runat="server" Enabled="False"
                        Visible="False"></asp:TextBox>
                    <asp:HiddenField ID="hidBusinessOrderPurchasePrice" runat="server" />
                    <br />
                    <p>
                        <asp:GridView ID="gCost" runat="server" AutoGenerateColumns="False" DataKeyNames="BusinessHourGuid"
                            EnableModelValidation="True" Enabled="False" Visible="False">
                            <Columns>
                                <asp:TemplateField HeaderText="序號">
                                    <ItemTemplate>
                                        <asp:Label ID="tbId" runat="server" Text='<%# Bind("Id") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="進貨價">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="tbCost" runat="server" Text='<%# Bind("Cost") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="Label2" runat="server" Text='<%# Bind("Cost") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="份數">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="tbQuantity" runat="server" Text='<%# Bind("Quantity") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="Label3" runat="server" Text='<%# Bind("Quantity") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="CumulativeQuantity" HeaderText="累積份數" />
                            </Columns>
                        </asp:GridView>
                    </p>
                </fieldset>

                <asp:Panel ID="pnFreight" runat="server">
                    <fieldset>
                        <legend>運費</legend>
                        <asp:HiddenField ID="hdFreightIncome" runat="server" />
                        <asp:HiddenField ID="hdFreightCost" runat="server" />
                        <asp:HiddenField ID="hdFIMax" runat="server" />
                        <asp:HiddenField ID="hdFCMax" runat="server" />
                        <asp:HiddenField ID="hdCostPayTo" runat="server" />
                        <div class="freightPanel">
                            運費設定：<br />
                            <table class="table_font">
                                <tr>
                                    <td>運費
                                    </td>
                                    <td>購買金額≧
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="text" id="tbFIAmt" class="freightAmount" />
                                    </td>
                                    <td>
                                        <input type="text" id="tbFIStartAmt" value="0" disabled="disabled" />
                                    </td>
                                    <td width="100">
                                        <input type="button" id="btnCreateFI" value="新增運費設定" />
                                    </td>
                                    <td width="100">
                                        <input type="button" id="btnDeleteFI" value="刪除運費設定" />
                                    </td>
                                </tr>
                            </table>
                            <table id="tableFI" cellspacing="0" border="1" style="border-collapse: collapse;"
                                rules="all">
                                <thead>
                                    <tr>
                                        <th>序號
                                        </th>
                                        <th>運費
                                        </th>
                                        <th>購買金額≧
                                        </th>
                                    </tr>
                                </thead>
                            </table>
                            <br />
                            進貨運費：<asp:DropDownList ID="freightCostPayTo" runat="server">
                            </asp:DropDownList>
                            <br />
                            <table class="table_font">
                                <tr>
                                    <td>運費
                                    </td>
                                    <td>購買金額≧
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="text" id="tbFCAmt" class="freightAmount" />
                                    </td>
                                    <td>
                                        <input type="text" id="tbFCStartAmt" value="0" disabled="disabled" />
                                    </td>
                                    <td width="100">
                                        <input type="button" id="btnCreateFC" value="新增進貨運費" />
                                    </td>
                                    <td width="100">
                                        <input type="button" id="btnDeleteFC" value="刪除進貨運費" />
                                    </td>
                                </tr>
                            </table>
                            <table id="tableFC" cellspacing="0" border="1" style="border-collapse: collapse;"
                                rules="all">
                                <thead>
                                    <tr>
                                        <th>序號
                                        </th>
                                        <th>進貨運費
                                        </th>
                                        <th>購買金額≧
                                        </th>
                                        <th>付款對象
                                        </th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div style="padding: 10px 5px 5px 5px">
                            <asp:CheckBox ID="cbx_NoShippingFeeMessage" runat="server" Enabled="False" Text="貨到付運費" Visible="False" /><asp:CheckBox
                                ID="cbx_NotDeliveryIslands" CssClass="cbx_NotDeliveryIslands" runat="server" />無法配送外島
                        </div>
                    </fieldset>
                </asp:Panel>
                <fieldset>
                    <legend>時程資訊</legend>
                    <asp:PlaceHolder ID="divProposal" runat="server" Visible="false">
                        <p>
                            <asp:Button ID="btnProposalImport" runat="server" Text="串接提案時程資訊" ValidationGroup="ProposalImport"
                                OnClick="btnProposalImport_Click" Visible="false" /><br />
                        </p>
                    </asp:PlaceHolder>
                    <p>
                        <div>活動搶購時間：</div>
                        <span style="float: left">從
                            <asp:TextBox ID="tbOS" runat="server" CssClass="date" ClientIDMode="Static"></asp:TextBox><asp:DropDownList
                                ID="dlOSh" runat="server" />
                            :<asp:DropDownList ID="dlOSm" runat="server" />
                            &nbsp;&nbsp;&nbsp;至
                            <asp:TextBox ID="tbOE" runat="server" CssClass="date" ClientIDMode="Static"></asp:TextBox><asp:DropDownList
                                ID="dlOEh" runat="server" />
                            :<asp:DropDownList ID="dlOEm" runat="server" />
                        </span><span style="float: right;"><a href="PponDealTimeSlot.aspx" target="_blank">活動時程表</a></span>
                    </p>
                    <p style="clear: both">
                        <div>活動商品有效期限/開始配送日期：</div>
                        從
                        <asp:TextBox ID="tbDS" runat="server" CssClass="date" ClientIDMode="Static"></asp:TextBox><asp:DropDownList
                            ID="dlDSh" runat="server" />
                        :<asp:DropDownList ID="dlDSm" runat="server" />
                        &nbsp;&nbsp;&nbsp;至
                        <asp:TextBox ID="tbDE" runat="server" CssClass="date" ClientIDMode="Static"></asp:TextBox><asp:DropDownList
                            ID="dlDEh" runat="server" />
                        :<asp:DropDownList ID="dlDEm" runat="server" />
                    </p>
                    <p>
                        <div>調整有效期限截止日：</div>
                        <asp:TextBox ID="formChangedExpireDate" ClientIDMode="Static" runat="server" CssClass="date" /><asp:DropDownList
                            ID="dlEDh" runat="server" />
                        :<asp:DropDownList ID="dlEDm" runat="server" />
                    </p>
                    <p id="pProduct" runat="server" clientidmode="Static" visible="false">
                        <asp:RadioButton ID="rboNormal" GroupName="ShipType" runat="server" ClientIDMode="Static" onclick="SetShipTypeDisplay()" />一般出貨
                        <asp:RadioButton ID="rboFast" GroupName="ShipType" runat="server" ClientIDMode="Static" onclick="SetShipTypeDisplay()" />快速出貨
                        <asp:RadioButton ID="rboLastShipment" GroupName="ShipType" runat="server" ClientIDMode="Static" onclick="SetShipTypeDisplay()" />最後出貨日
                        <asp:RadioButton ID="rboLastDelivery" GroupName="ShipType" runat="server" ClientIDMode="Static" onclick="SetShipTypeDisplay()" />最後到貨日
                        <asp:RadioButton ID="rboWms" GroupName="ShipType" runat="server" ClientIDMode="Static" onclick="SetShipTypeDisplay()" />24小時到貨
                        <asp:RadioButton ID="rboOther" GroupName="ShipType" runat="server" ClientIDMode="Static" onclick="SetShipTypeDisplay()" />其他
                        <br />
                        <span id="divSubNormalShip">
                            &nbsp;&nbsp;&nbsp;<asp:RadioButton ID="rboUniShipDay" GroupName="SubShipType" runat="server" />最早出貨日：上檔後+<asp:TextBox ID="txtEarliestShipDate" runat="server" class="number" Width="20px" />個工作日；最晚出貨日：統一結檔後+<asp:TextBox ID="txtLatestShipDate" runat="server" class="number" Width="20px" />個工作日<br />
                            &nbsp;&nbsp;&nbsp;<asp:RadioButton ID="rboNormalShipDay" GroupName="SubShipType" runat="server" />訂單成立後<asp:TextBox ID="txtNormalShipDay" runat="server" class="number" Width="20px" />個工作天出貨完畢<br />
                        </span>
                        <span id="divSubFastShip" style="display: none">
                            &nbsp;&nbsp;&nbsp;<asp:RadioButton ID="rboFastShip1" GroupName="SubShipType" runat="server" />訂單成立後24小時出貨完畢，周休假日或例假之訂單，視為次一工作日之訂單<br />
                        </span>
                        <asp:HiddenField runat="server" ID="hidSubShipType" />
                    </p>
                </fieldset>
                <fieldset>
                    <legend>活動人數及限制</legend>
                    <p>
                        <label>
                            最低門檻數量：</label>
                        <asp:TextBox ID="tbOMin" runat="server" class="number" ClientIDMode="Static" />
                    </p>
                    <p>
                        <label>
                            最大購買數量：</label>
                        <asp:TextBox ID="tbOMax" runat="server" class="number" onblur="return setATMQtyWithChangetbOMax(this);" />
                        <span class="ui-state-highlight">若無限制, 請留空白，系統將自動帶入99,999</span>
                    </p>
                    <p>
                        <label>
                            每人限購：</label>
                        <asp:TextBox ID="tbMaxP" runat="server" class="number" />
                        <span class="ui-state-highlight">若無限制, 請留空白，系統將自動帶入20</span>
                        <asp:CheckBox ID="chkIsDailyRstriction" runat="server" Text="每人每日限購份數" />
                    </p>
                    <p>
                        <label style="display: inline">
                            組合數：
                        </label>
                        <asp:TextBox ID="txtQuantityMultiplier" runat="server" ClientIDMode="Static" class="number"
                            Text="" Style="display: inline; width: 60px" />
                        入/組，
                        <asp:CheckBox ID="chkQuantityMultiplier" runat="server" ClientIDMode="Static" Text="銷售倍數" />
                        <asp:CheckBox ID="chkIsAveragePrice" runat="server" ClientIDMode="Static" Text="均價" />
                        <label id="AveragePriceArea" style="display: none; color: #FF0000; font-weight: bolder;">
                            均價：$<asp:Label ID="lbAveragePrice" runat="server" Text="0" ClientIDMode="Static"></asp:Label>
                        </label>

                    </p>
                    <asp:TextBox ID="tbMaxO" runat="server" Enabled="False" Visible="False"></asp:TextBox>
                    <p>
                        <asp:CheckBox ID="cbx_NoRefund" runat="server" Text="不接受退貨" />
                        (<label style="display: inline; color: Green;">捐款檔次<asp:CheckBox ID="cbx_KindDeal"
                            runat="server" onclick="KindDealSetting(this);setATMQtyWithSettingDealType();" /></label>
                        )
                    </p>
                    <p>
                        <asp:CheckBox ID="chkDaysNoRefund" runat="server" Text="結檔後七天不能退貨" Enabled="false" />
                    </p>
                    <p>
                        <asp:CheckBox ID="chkNoRefundBeforeDays" runat="server" Text="演出時間前十日內不能退貨" onclick="CheckNoRefundDeal(this)" />
                    </p>
                    <p>
                        <asp:CheckBox ID="cbx_ExpireNoRefund" runat="server" Text="過期不能退貨" onclick="CheckNoRefundDeal(this)"  />
                    </p>
                    <p>
                        <asp:CheckBox ID="cbx_NoInvoice" runat="server" ClientIDMode="Static" Text="免開統一發票"
                            Visible="false" />
                    </p>
                    <p>
                        <asp:CheckBox ID="cbx_PEZevent" runat="server" Text="廠商提供序號活動" onclick="ShowCouponSeparateDigits(this);" />
                        <span>(<label style="display: inline">序號綁UserId<asp:CheckBox ID="cbx_PEZeventWithUserId"
                            runat="server" /></label>
                            )(<label style="display: inline">單一序號<asp:CheckBox ID="cbx_SinglePinCode"
                                runat="server" /></label>)</span>
                    </p>
                    <p> 
                        <span id="CouponSeparateDigitsShow" style="font-size: small; display: none;"> 
                            &nbsp;&nbsp;&nbsp;                       
                        <asp:CheckBox ID="cbxCouponSeparateDigits" runat="server" ClientIDMode="Static"  Text="自訂簡訊格式" />                        
                        <label style="display: inline">
                            每
                        </label>
                        <asp:TextBox ID="tbCouponSeparateDigits" runat="server" ClientIDMode="Static" class="number"
                            Text="" Style="display: inline; width: 30px" />
                        碼加入-分隔，且不包含確認碼 若無需求，<span class="ui-state-highlight">毋需勾選，簡訊將維持預設格式發送</span>
                            </span>
                    </p>
                    <p>
                        <asp:CheckBox ID="cbx_IsReverseVerify" runat="server" Text="使用反向核銷"/>
                    </p>
                    <p>
                        <asp:CheckBox ID="cbx_PEZeventCouponDownload" ClientIDMode="Static" runat="server" Text="0元或廠商提序號檔次，提供下載序號(不含簡訊)"
                             onclick="setATMQtyWithSettingDealType();" />
                    </p>
                    <p>
                        <asp:CheckBox ID="cbx_ZeroActivityShowCoupon" ClientIDMode="Static" runat="server" Style="display: none" Text="0元檔索取完成後頁面顯示憑證序號，並提供下載序號" />
                    </p>
                    <p>
                        <asp:CheckBox ID="cbx_DepositCoffee" runat="server" Text="咖啡寄杯"/>　<span class="ui-state-highlight">勾選後訂單將出現在咖啡寄杯選單中</span>
                    </p>
                        <% if (config.EnableHiLifeDealSetup)
                           { %>
                        <p><asp:CheckBox ID="cbx_HiLifeDeal" runat="server" Text="萊爾富檔次"/></p>
                      <% } %>
                    <p>
                        <asp:CheckBox ID="cbx_FamiDeal" runat="server" Text="全家檔次"
                            onclick="SetFamiBarcodeDisplay();setATMQtyWithSettingDealType();" />
                    </p>
                    <p class='FamiDealType'>
                        <span>憑證類型:
                        <asp:RadioButton ID="rdoFamiPincode" GroupName="FamiDealType" Text="紅利pin碼" Checked="true"
                            runat="server" Style="display: none" CssClass="FamiDealRadioBtn" />
                            <asp:RadioButton ID="rdoFamiBarcode" GroupName="FamiDealType" Text="優惠條碼barcode"
                                runat="server" Style="display: none" CssClass="FamiDealRadioBtn" />
                            <asp:RadioButton ID="rdoFamiItemCode" GroupName="FamiDealType" Text="指定商品四段式條碼(寄杯)"
                                runat="server" Style="display: none" CssClass="FamiDealRadioBtn" />
                            &nbsp;&nbsp;<span id="FamiExchangePriceArea" style="display: none;">全家兌換價：<asp:TextBox
                                ID="tbx_FamiExchangePrice" runat="server" class="number" Style="display: inline; width: 60px"></asp:TextBox></span>
                        </span>
                        <br />
                        <span>檔次類型:
                            <asp:RadioButton ID="rdoFamilyGeneralType" GroupName="FamiType" Text="全家專區" Checked="true"
                                runat="server" Style="display: none" CssClass="FamiDealType" />
                            <asp:RadioButton ID="rdoFamilyBeaconType" GroupName="FamiType" Text="IBeacon活動檔次"
                                runat="server" Style="display: none" CssClass="FamiDealType" />
                            &nbsp;&nbsp;是否鎖定:
                            <asp:RadioButton ID="rdoLock" GroupName="FamiLockStatus" Text="鎖定" Checked="true"
                                runat="server" Style="display: none" CssClass="FamiDealType" />
                            <asp:RadioButton ID="rdoUnLock" GroupName="FamiLockStatus" Text="未鎖定"
                                runat="server" Style="display: none" CssClass="FamiDealType" />

                        </span>
                    </p>
                    <p>
                        <asp:CheckBox ID="cbx_TmallDeal" runat="server" Text="天貓檔次" Visible="false"
                            onclick="tmall_rmb_show($(this).is(':checked'));setATMQtyWithSettingDealType();" />
                        <span id="tmall_rmb" style="display: none;">人民幣售價:<asp:TextBox ID="tbx_TmallRmbPrice"
                            runat="server"></asp:TextBox>
                            &nbsp;&nbsp;人民幣兌換匯率:<asp:TextBox ID="tbx_TmallRmbExchangeRate" runat="server"></asp:TextBox>
                        </span>

                    </p>
                    <p>
                        <asp:CheckBox ID="cbx_SKMDeal" runat="server" Text="新光三越" onclick="SetSKMDisplay();" />
                    </p>
                    <div class='SKMDealType'>
                        <span>檔次類型:
                            <asp:RadioButton ID="rbCoupon" GroupName="SKMType" Text="優惠活動 / 商品" Checked="true" 
                                runat="server" Style="display: none" CssClass="SKMDealType" />
                            <asp:RadioButton ID="rbExperience" GroupName="SKMType" Text="注目商品"
                                runat="server" Style="display: none" CssClass="SKMDealType" />
                        </span><br />
                        <span>頁簽設定:
                            <asp:CheckBoxList CssClass="SKMDealType" ID="cblSkmDealType" RepeatDirection="Horizontal" runat="server" RepeatLayout="Flow" ></asp:CheckBoxList>
                            <asp:HiddenField runat="server" ID="hdSkmDealType" ClientIDMode="Static"/>
                        </span>
                        <div>
                            <asp:CheckBox ID="cbx_Settlement" runat="server" Text="設定憑證清算" Style="display: none" class="SKMDealType"
                                onclick="SKMSettlementShow(this);" />
                            <Panel ID="plSettlement" Style="display: none">於每天<asp:DropDownList ID="dlSTMh" runat="server" />:<asp:DropDownList ID="dlSTMm" runat="server" />清算
                                <asp:HiddenField runat="server" ID="hdSTMDate" ClientIDMode="Static"/>
                            </Panel>

                        </div>
                        <div>
                            <asp:CheckBox ID="cbx_setting_display" runat="server" Text="優惠設定 / 顯示" Style="display: none" class="SKMDealType"
                                onclick="SKMDiscountShow(this);" />
                            <Panel ID="plSKMDiscount" Style="display: none">
                                <asp:RadioButtonList CssClass="SKMDealType" runat="server" ID="rblDiscountType" RepeatDirection="Horizontal" RepeatLayout="Flow" >
                                </asp:RadioButtonList>
                                <asp:HiddenField runat="server" ID="hdDiscount" ClientIDMode="Static" />
                            </Panel>
                              
                        </div>
                    </div>
                    <p>
                        <asp:CheckBox ID="cbx_allowGuestBuy" runat="server" Text="允許訪客購買" />
                    </p>
            <p>
                <label>
                    ATM保留份數：
                </label>
                <asp:TextBox ID="tbAtm" onblur="return setAtmQtyToDefaultValue(this);" class="number" runat="server">30000</asp:TextBox>
                <span id="spanAtmInfo" class="ui-state-highlight">預設帶最大購買數量20%，無需保留請填0，清空內容會恢復預設值。</span>
                <input id="hidCustomAtmQty" type="hidden" value="-1" />
                <asp:HiddenField ID="hidAtmQty" Value="-1" runat="server" ClientIDMode="Static" />
            </p>
            </fieldset>

                <asp:PlaceHolder ID="phCopyComboDeals" runat="server">
                    <div>
                        <fieldset>
                            <legend>
                                <asp:CheckBox ID="cbx_SaveComboDeals_m" runat="server" />子檔同步項目</legend>
                            <p id="p_SaveComboDeals_m" style="display: none">
                                <asp:Table runat="server" ID="tabDealGuidCopy" Style="font-size: small; color: #333">
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <asp:CheckBox ID="cbx_SaveComboDeals_Sales" runat="server" />業務
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:CheckBox ID="cbx_SaveComboDeals_Channel" runat="server" />上檔頻道(依排檔區域勾選)
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:CheckBox ID="cbx_SaveComboDeals_SEO" runat="server" />SEO設定
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:CheckBox ID="cbx_SaveComboDeals_AccBusGroup" runat="server" />館別
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:CheckBox ID="cbx_SaveComboDeals_SalesBelong" runat="server" />業績歸屬
                                                    <asp:CheckBox ID="cbx_SaveComboDeals_DealType" runat="server" />分類
                                                    <asp:CheckBox ID="cbx_SaveComboDeals_EntrustOnSell" runat="server" />收據資料
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:CheckBox ID="cbx_SaveComboDeals_IsLongContract" runat="server" />長期約
                                            <asp:CheckBox ID="cbx_SaveComboDeals_DonotInstallment" runat="server" />不做分期
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <asp:CheckBox ID="cbx_SaveComboDeals_NotDeliveryIslands" runat="server" />無法配送外島
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:CheckBox ID="cbx_SaveComboDeals_PaySet" runat="server" />核銷與帳務設定
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:CheckBox ID="cbx_SaveComboDeals_OS" runat="server" />活動搶購時間(起)
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:CheckBox ID="cbx_SaveComboDeals_OE" runat="server" />活動搶購時間(迄)
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:CheckBox ID="cbx_SaveComboDeals_UseDiscount" runat="server" />毛利率可用折價券/本檔不可使用現金
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell ColumnSpan="2">
                                            <asp:CheckBox ID="cbx_SaveComboDeals_DS" runat="server" />活動商品有效期限/開始配送日期(起)
                                        </asp:TableCell>
                                        <asp:TableCell ColumnSpan="2">
                                            <asp:CheckBox ID="cbx_SaveComboDeals_DE" runat="server" />活動商品有效期限/開始配送日期(迄)
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:CheckBox ID="cbx_SaveComboDeals_ShipType" runat="server" />出貨資訊
                                            <asp:CheckBox ID="cbx_SaveComboDeals_BuyoutCoupon" runat="server" />買斷票券
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <asp:CheckBox ID="cbx_SaveComboDeals_ChangedExpireDate" runat="server" />調整有效期限截止日
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:CheckBox ID="cbx_SaveComboDeals_NoRefund" runat="server" />不接受退貨(捐款檔次)
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:CheckBox ID="cbx_SaveComboDeals_DaysNoRefund" runat="server" />結檔後七天不能退貨
                                        </asp:TableCell>
                                        <asp:TableCell ColumnSpan="2">
                                            <asp:CheckBox ID="cbx_SaveComboDeals_NoRefundBeforeDays" runat="server" />演出時間前十日內不能退貨
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <asp:CheckBox ID="cbx_SaveComboDeals_ExpireNoRefund" runat="server" />過期不能退貨
                                        </asp:TableCell>
                                        <asp:TableCell ColumnSpan="2">
                                            <asp:CheckBox ID="cbx_SaveComboDeals_ReserveLock" runat="server" />住宿類，商家系統提供「訂房鎖定」功能
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <asp:CheckBox ID="cbx_SaveComboDeals_cbx_PEZevent" runat="server" />廠商提供序號活動
                                        </asp:TableCell>         
                                        <asp:TableCell ColumnSpan="2">
                                            <asp:CheckBox ID="cbx_SaveComboDeals_PEZeventCouponDownload" runat="server" />0元或廠商提序號檔次，提供下載序號(不含簡訊)
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:CheckBox ID="cbx_SaveComboDeals_DepositCoffee" runat="server" />咖啡寄杯
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:CheckBox ID="cbx_SaveComboDeals_VerifyActionType" runat="server" />使用反向核銷
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <asp:CheckBox ID="cbx_SaveComboDeals_TaiShinCreditCard" runat="server" />限制台新信用卡
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </p>
                        </fieldset>
                    </div>
                </asp:PlaceHolder>
        </div>
            <div id="tabs-2" style="min-width: 600px">
            <fieldset>
                <table style="width: 100%">
                    <tr>
                        <td style="border: thin dotted gray; vertical-align: top">
                            <asp:Repeater ID="ri" runat="server">
                                <HeaderTemplate>
                                    <div style="background-color: LightBlue">
                                        1. 輪播大圖(請固定大小->480px * 267px)
                                    </div>
                                    <ul id="images">
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <li>
                                        <asp:Image ID="i" runat="server" ImageUrl='<%# Eval("ImgPath")+"?"+(DateTime.Now.ToString("ss")) %>'
                                            ImageAlign="Middle" /><input type="hidden" name="is" value='<%#Eval("Index")%>' />
                                        <a href="#" class="dI">X</a><hr />
                                    </li>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </ul>
                                </FooterTemplate>
                            </asp:Repeater>
                        </td>
                        <td style="border: thin dotted gray; vertical-align: top;">
                            <asp:Repeater ID="ri_2" runat="server">
                                <HeaderTemplate>
                                    <div style="background-color: LightGreen">
                                        2. SideDeal小圖
                                    </div>
                                    <br />
                                    <ul id="images">
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <li>
                                        <asp:Image ID="i" runat="server" ImageUrl='<%# Container.DataItem %>' ImageAlign="Middle" /><input
                                            type="hidden" name="is" value="1" />
                                        <a href="#" class="dI">X</a><hr />
                                    </li>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </ul>
                                </FooterTemplate>
                            </asp:Repeater>
                            <asp:Repeater ID="ri_3" runat="server">
                                <HeaderTemplate>
                                    <div style="background-color: Olive">
                                        3. Im小圖
                                    </div>
                                    <br />
                                    <ul id="images">
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <li>
                                        <asp:Image ID="i" runat="server" ImageUrl='<%# Container.DataItem %>' ImageAlign="Middle" /><input
                                            type="hidden" name="is" value="2" />
                                        <a href="#" class="dI">X</a><hr />
                                    </li>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </ul>
                                </FooterTemplate>
                            </asp:Repeater>
                        </td>
                    </tr>
                </table>
                <table style="width: 100%; display: none">
                    <tr>
                        <td style="border: thin dotted gray; vertical-align: top">
                            <asp:Repeater ID="rpSpImg" runat="server">
                                <HeaderTemplate>
                                    <div style="background-color: LightBlue">
                                        特殊規格圖片(APP或其他用途)
                                    </div>
                                    <ul id="images">
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <li>
                                        <asp:Image ID="i" runat="server" ImageUrl='<%# Eval("ImgPath")+"?"+(DateTime.Now.ToString("ss")) %>'
                                            ImageAlign="Middle" /><input type="hidden" name="spIs" value='<%#Eval("Index")%>' />
                                        <a href="#" class="dI">X</a><hr />
                                    </li>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </ul>
                                </FooterTemplate>
                            </asp:Repeater>
                        </td>
                    </tr>
                </table>
                <table style="width: 100%">
                    <tr>
                        <td style="border: thin dotted gray; vertical-align: top">
                            <asp:Repeater ID="rpTravelEDMSpImg" runat="server">
                                <HeaderTemplate>
                                    <div style="background-color: LightBlue">
                                        旅遊eDM專屬大圖
                                    </div>
                                    <ul id="images">
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <li>
                                        <asp:Image ID="i" runat="server" ImageUrl='<%# Eval("ImgPath")+"?"+(DateTime.Now.ToString("ss")) %>'
                                            ImageAlign="Middle" /><input type="hidden" name="trspIs" value='<%#Eval("Index")%>' />
                                        <a href="#" class="dI">X</a><hr />
                                    </li>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </ul>
                                </FooterTemplate>
                            </asp:Repeater>
                        </td>
                    </tr>
                </table>
                <label style="display: inline">
                    <input id="rdo_img1" type="radio" name="rdo_img" value="<%=(int)PponSetupImageUploadType.ByORder %>"
                        checked="checked" />按順序上圖(1,2,3,其他輪播大圖)</label>
                <label style="display: inline">
                    <input id="rdo_img2" type="radio" name="rdo_img" value="<%=(int)PponSetupImageUploadType.BigPic %>" />上輪播大圖</label>
                <%--<input id="rdo_img3" type="radio" name="rdo_img" value="<%=(int)PponSetupImageUploadType.APP %>" />上App行銷活動大圖300x450--%>
                <br />
                <label style="display: inline">
                    <input id="rdo_img4" type="radio" name="rdo_img" value="<%=(int)PponSetupImageUploadType.TravelEdmSpecialBigPic %>" />旅遊eDM專屬大圖740x230</label>
                <asp:CheckBox ID="chkIsPrintWatermark" runat="server" Checked="true" /><font style="color: Brown;">自動壓Logo</font>
                <br />
                上傳圖片:
                    <asp:FileUpload ID="fuI" runat="server" />
                <span class="ui-state-highlight editFreeDeal">若為「0元檔」，第2張上傳圖片為活動完成頁大圖。</span>
                <div class="editFreeDeal">
                    0元檔活動完成頁大圖連結：
                        <asp:TextBox ID="txtActivityUrl" CssClass="editActivityUrl" Width="450px" runat="server"></asp:TextBox>
                    <span style="display: none" class="ui-state-highlight ActivityUrlError">請輸入完整的連結網址。</span>
                </div>
                <br />
                <br />
                <a href="PicSetup.aspx?bid=<%= BusinessHourId %>" style="color: #1c94c4; font-weight: bold;" target="_blank">上傳多張大圖</a>
                <a href="/ControlRoom/System/PicSetup?bid=<%= BusinessHourId %>" style="color: #1c94c4; font-weight: bold;" target="_blank">品生活多張大圖</a>
                <asp:Button ID="btnDeleteCDNImages" runat="server" Text="圖片分流更新" OnClick="btnDeleteCDNImages_Click" />

                <!-- App/M版 專用圖 -->
                <div style="width: 100%; margin-top: 30px; padding: 1px; background-color: #fff; border: thin dotted gray; font-size: 15px">
                    <div style="background-color: LightBlue; padding: 2px">
                        App/M版 專用圖
                    </div>
                </div>
                <div class="drag-drop-upload-container">
                    <asp:HiddenField ID="hidAppDealPicOld" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="hidAppDealPic" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="hidAppDealPicX" runat="server" ClientIDMode="Static" />
                    <div class="drag-drop-upload" id="appDealPicDiv" style="width: 1050px; height: 1050px;">
                        <p style="margin-top: 60px; margin-left: 160px">將圖片拖曳奕到這裡上傳</p>
                        <p style="margin-top: 20px; margin-left: 150px">
                        <p style="margin-top: 20px; margin-left: 150px">
                            <input type="file" value="選取檔案" style="padding: 3px" />
                        </p>
                    </div>
                </div>
                <!-- App/M版 專用圖，請上傳圖片，然後進行範圍選取 -->
            </fieldset>
        </div>
            <div id="tabs-3">
                <fieldset style="display: none;">
                    <legend>好康哪裡找</legend>
                    <div id="avpane">
                        <asp:Repeater ID="avR" runat="server">
                            <ItemTemplate>
                                <ul>
                                    <li class="eb">(edit)</li>
                                    <li class="cb">(copy)</li>
                                    <li class="db">(delete)</li>
                                    <%#"<li>"+Eval("N")+"</li>"%>
                                    <%#"<li>" + Eval("P") + "</li>"%>
                                    <%#"<li>" + Eval("A") + "</li>"%>
                                    <%#"<li>" + Eval("OT") + "</li>"%>
                                    <%#"<li>" + Eval("OD") + "</li>"%>
                                    <%#"<li>" + Eval("U") + "</li>"%>
                                    <%#"<li>" + Eval("R") + "</li>"%>
                                    <%#"<li>" + Eval("CD") + "</li>"%>
                                    <%#"<li>" + Eval("MR") + "</li>"%>
                                    <%#"<li>" + Eval("CA") + "</li>"%>
                                    <%#"<li>" + Eval("BU") + "</li>"%>
                                    <%#"<li>" + Eval("OV") + "</li>"%>
                                    <%#"<li>" + Eval("FB") + "</li>"%>
                                    <%#"<li>" + Eval("PL") + "</li>"%>
                                    <%#"<li>" + Eval("BL") + "</li>"%>
                                    <%#"<li>" + Eval("OL") + "</li>"%>
                                </ul>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                    <input type="button" id="addInfo" value="新增" />
                    <div id="availInfo" title="店家細節">
                        <table style="font-size: small">
                            <tr>
                                <td>
                                    <label for="storeName">
                                        店名</label>
                                </td>
                                <td colspan="2">
                                    <input id="storeName" type="text" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label for="storePhone">
                                        電話</label>
                                </td>
                                <td colspan="2">
                                    <input id="storePhone" type="text" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label for="storeAddress">
                                        地址</label>
                                </td>
                                <td colspan="2">
                                    <input id="storeAddress" type="text" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label for="storeOpeningTime">
                                        營業時間</label>
                                </td>
                                <td colspan="2">
                                    <input id="storeOpeningTime" type="text" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label for="storeCloseDays">
                                        公休日</label>
                                </td>
                                <td colspan="2">
                                    <input id="storeCloseDays" type="text" />
                                </td>
                            </tr>
                            <tr>
                                <td valign="top">
                                    <label>
                                        交通資訊</label>
                                </td>
                                <td colspan="2">
                                    <table style="font-size: small">
                                        <tr>
                                            <td>
                                                <label for="mrt">
                                                    MRT</label>
                                            </td>
                                            <td>
                                                <input id="mrt" type="text" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label for="car">
                                                    開車</label>
                                            </td>
                                            <td>
                                                <input id="car" type="text" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label for="bus">
                                                    公車</label>
                                            </td>
                                            <td>
                                                <input id="bus" type="text" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <label for="otherVehicle">
                                                    其他</label>
                                            </td>
                                            <td>
                                                <input id="otherVehicle" type="text" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label for="storeUrl">
                                        官網</label>
                                </td>
                                <td colspan="2">
                                    <input id="storeUrl" type="text" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label for="fbFans">
                                        FB</label>
                                </td>
                                <td>
                                    <input id="fbFans" type="text" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label for="plurkFans">
                                        Plurk</label>
                                </td>
                                <td>
                                    <input id="plurkFans" type="text" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label for="blogFans">
                                        Blog</label>
                                </td>
                                <td>
                                    <input id="blogFans" type="text" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label for="otherFans">
                                        其他</label>
                                </td>
                                <td>
                                    <input id="otherFans" type="text" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label for="storeRemark">
                                        備註</label>
                                </td>
                                <td colspan="2">
                                    <input id="storeRemark" type="text" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <input id="avset" type="button" value="Set" />
                                </td>
                            </tr>
                        </table>
                    </div>
                    <input type="hidden" id="hav" name="hav" />
                </fieldset>
                <fieldset style="width: 850px" class="BookingSystem">
                    <legend>預約管理設定</legend>
                    <label>預約管理：</label>
                    <asp:CheckBox ID="cb_IsUsingbookingSystem" Text="配合" runat="server" />
                    <div class="CouponUsers" style="display: none">
                        類型:
                            <label>
                                <asp:RadioButton ID="rdoBookingSystemSeat" GroupName="bookingSystemType" Checked="true"
                                    runat="server" />訂位管理(如：餐廳、SPA)
                            </label>
                        <label>
                            <asp:RadioButton ID="rdoBookingSystemRoom" Enabled="false" GroupName="bookingSystemType"
                                runat="server" />訂房管理(如：住宿)
                        </label>
                         <br />預約限制:
                        <label>
                            消費者需提早
                                <asp:TextBox ID="txtAdvanceReservationDays" MaxLength="2" ClientIDMode="Static" Width="25px"
                                    Text="1" runat="server"></asp:TextBox>
                            天預約(至少需填1天)
                        </label>
                        <br />優惠條件:
                        <label>
                            <asp:RadioButton ID="rdoSingleUser" GroupName="rdoUsers" Checked="true" runat="server" />
                            單人優惠(1人使用一張，或無使用限制的檔次)
                        </label>
                        <span>
                            <label style="display: inline-block">
                                <asp:RadioButton ID="rdoMultiUsers" GroupName="rdoUsers" runat="server" />
                                多人優惠，
                            </label>
                            本次優惠為
                                <asp:TextBox ID="txtCouponUsers" ClientIDMode="Static" MaxLength="2" Width="25px"
                                    runat="server"></asp:TextBox>
                            人享用一張憑證(以最多人數計算，如3~4人套餐，則填4)
                        </span>
                    </div>
                </fieldset>
                <fieldset id="PiilifeTabs" style="display: none">
                    <legend>品生活專用頁籤</legend>
                    <div id="multiTabs">
                        <ul>
                            <li><a href="#multiTabs-1">
                                <asp:TextBox ID="txtMutiTabTitle1" runat="server" CssClass="TabTitle" Text="右邊" Enabled="false"></asp:TextBox></a>
                            </li>
                            <li><a href="#multiTabs-2">
                                <asp:TextBox ID="txtMutiTabTitle2" runat="server" CssClass="TabTitle"></asp:TextBox></a>
                            </li>
                            <li><a href="#multiTabs-3">
                                <asp:TextBox ID="txtMutiTabTitle3" runat="server" CssClass="TabTitle"></asp:TextBox></a>
                            </li>
                            <li><a href="#multiTabs-4">
                                <asp:TextBox ID="txtMutiTabTitle4" runat="server" CssClass="TabTitle"></asp:TextBox></a>
                            </li>
                            <li><a href="#multiTabs-5">
                                <asp:TextBox ID="txtMutiTabTitle5" runat="server" CssClass="TabTitle"></asp:TextBox></a>
                            </li>
                            <li><a href="#multiTabs-6">
                                <asp:TextBox ID="txtMutiTabTitle6" runat="server" CssClass="TabTitle"></asp:TextBox></a>
                            </li>
                        </ul>
                        <div id="multiTabs-1">
                            <asp:TextBox ID="txtMutiTab1" runat="server" TextMode="MultiLine"></asp:TextBox>
                        </div>
                        <div id="multiTabs-2">
                            <asp:TextBox ID="txtMutiTab2" runat="server" TextMode="MultiLine"></asp:TextBox>
                        </div>
                        <div id="multiTabs-3">
                            <asp:TextBox ID="txtMutiTab3" runat="server" TextMode="MultiLine"></asp:TextBox>
                        </div>
                        <div id="multiTabs-4">
                            <asp:TextBox ID="txtMutiTab4" runat="server" TextMode="MultiLine"></asp:TextBox>
                        </div>
                        <div id="multiTabs-5">
                            <asp:TextBox ID="txtMutiTab5" runat="server" TextMode="MultiLine"></asp:TextBox>
                        </div>
                        <div id="multiTabs-6">
                            <asp:TextBox ID="txtMutiTab6" runat="server" TextMode="MultiLine"></asp:TextBox>
                        </div>
                    </div>
                </fieldset>
                <fieldset>
                    <legend>更多權益說明&nbsp;&nbsp;</legend>
                    <asp:TextBox ID="tbPdtl" runat="server" Rows="5" TextMode="MultiLine" Width="600px"></asp:TextBox>
                </fieldset>
                <fieldset style="display: none">
                    <legend>非P不可的理由</legend>
                    <asp:TextBox ID="tbRes" runat="server" TextMode="MultiLine" Rows="5"></asp:TextBox>
                </fieldset>                
                <fieldset>
                    <button id="btnGetDraftForDescription" type="button" title="從子檔資料匯入">匯</button><span>從子檔資料匯入(測試中)</span><br />
                    <legend>詳細介紹(最多300字)</legend>
                    <asp:TextBox ID="tbSlrInt" runat="server" TextMode="MultiLine"></asp:TextBox>
                </fieldset>
                <fieldset style="display: none">
                    <legend>其他有力的佐證或看法支持: (最多200字)</legend>
                    <asp:TextBox ID="tbRef" runat="server" TextMode="MultiLine"></asp:TextBox>
                </fieldset>
                <fieldset>
                    <legend>一姬說好康</legend>
                    <p>
                        <asp:TextBox ID="rmkSub" runat="server" Enabled="False" Visible="False"></asp:TextBox>
                        <label for="<%=rmkTxt.ClientID%>">
                        </label>
                        <asp:TextBox ID="rmkTxt" runat="server" TextMode="MultiLine"></asp:TextBox>
                    </p>
                </fieldset>                
                <asp:Panel ID="pnProSpc" runat="server">
                    <fieldset>
                        <legend>商品規格</legend>
                        <asp:TextBox ID="tbProSpc" runat="server" TextMode="MultiLine"></asp:TextBox>
                    </fieldset>
                </asp:Panel>
            </div>
            <div id="tabs-4">
                <asp:Panel ID="pnMainShoppingCartSetting" runat="server" Visible="false">
                    <fieldset>
                        <legend>購物車設定</legend>                        
                        <asp:RadioButtonList ID="rbl_ShoppingCartOptions" runat="server" RepeatDirection="Vertical" CssClass="tab-option"></asp:RadioButtonList>
                    </fieldset>
                </asp:Panel>
                <fieldset>
                    <legend>檔次續接設定</legend>接續數量BID：<asp:TextBox ID="AncestorBid" runat="server" Width="450px" placeholder="如要另外指定，請輸入欲接續數量的代表檔或單檔BID。子檔BID無效"></asp:TextBox><br />
                    <asp:Literal ID="lit_AncestorSeqBid" runat="server">接續憑證BID：</asp:Literal><asp:TextBox ID="AncestorSeqBid" runat="server" Width="450px" placeholder="僅限接續憑證子檔BID，否則無效"></asp:TextBox><br />
                    <asp:CheckBox ID="cbx_Is_Continued_Sequence" runat="server" Text="接續憑證號碼" />&nbsp;&nbsp;&nbsp;<asp:CheckBox
                        ID="cbx_Is_Continued_Quantity" runat="server" Text="接續銷售數字(前台顯示)" />
                    <span class="ui-state-highlight">多檔次母檔不可續接憑證號碼；多檔次子檔不可續接銷售數字</span>
                    <br />
                    <asp:Label ID="ancCouponCount" runat="server"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Label ID="sucCouponCount" runat="server"></asp:Label><br />
                    <asp:Label ID="ancOrderedQuantity" runat="server"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Label ID="sucOrderedQuantity" runat="server"></asp:Label>
                    <asp:Panel ID="PanelSuccessor" runat="server">
                        <br />
                        接續此檔的檔次：
                            <br />
                        <asp:Repeater ID="SuccessorBids" runat="server" OnItemDataBound="SuccessorBids_ItemDataBound">
                            <ItemTemplate>
                                <asp:HyperLink ID="successorBid" runat="server"></asp:HyperLink><br />
                            </ItemTemplate>
                        </asp:Repeater>
                    </asp:Panel>
                </fieldset>
                <fieldset>
                    <legend>APP限定</legend>
                    <asp:CheckBox ID="cbx_IsAppLimitedEdition" ClientIDMode="Static" Text="只限使用APP購買" runat="server" />
                </fieldset>
                <fieldset id="fReserveLock">
                    <legend>住宿/活動/展演票券憑證鎖定</legend>
                    <p>
                        <asp:CheckBox ID="cbIsReserveLock" runat="server" Text="住宿/活動/展演票券憑證，商家系統提供「憑證鎖定」功能" />
                    </p>
                </fieldset>

                <fieldset>
                    <legend>店鋪位置顯示設定</legend>
                    <table style="font-size: small;" class="tblStoreSettings">
                        <thead>
                            <tr>
                                <th style="width: 55px">
                                    <input type="checkbox" id="checkAllStore" onclick="selectAll($(this), 'checkstore');" />
                                    顯示
                                </th>
                                <th style="width: 30px">排序
                                </th>
                                <th>分店名稱</th>
                                <th>店鋪地址</th>
                            </tr>
                        </thead>
                        <tbody class="ui-sortable">
                            <asp:Repeater ID="repStore" runat="server" OnItemDataBound="RepStoreItemDataBound">
                                <ItemTemplate>
                                <tr>
                                    <td style="width: 55px">
                                        <asp:HiddenField ID="hidCompareStoreInfo" runat="server" />
                                        <asp:HiddenField ID="hfNewSortOrder" runat="server" />
                                        <asp:CheckBox runat="server" ID="cbStore" CssClass="checkstore" />
                                    </td>
                                    <td style="width: 30px">
                                        <asp:Label runat="server" ID="litStoreOrder" Text="" Style="width: 20px" />
                                    </td>
                                    <td>
                                        <asp:Label runat="server" ID="lbStore" CssClass="bindStoreName"></asp:Label>
                                        <asp:TextBox runat="server" ID="tbStoreGuid" CssClass="bindStoreGuid" Style="display: none;" />
                                        <asp:TextBox runat="server" ID="tbStoreStatus" CssClass="bindStoreStatus" Style="display: none;" />
                                    </td>
                                    <td>
                                            <asp:Label runat="server" ID="lbStoreAddress" CssClass="bindStoreAddress"></asp:Label>
                                    </td>
                                </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </tbody>
                    </table>
                <div id="loadStore" style="display:none">分店讀取中...</div>
                <div class="loadmore" onclick="getMoreStore(-1);" runat="server">載入更多...</div>
                <input type="hidden" id="hdStoreSellected" clientidmode="Static" runat="server" />
                <input type="hidden" id="hdSellerGuid" clientidmode="Static" runat="server" />
                <input type="hidden" id="hdBid" clientidmode="Static" runat="server" />
                </fieldset>
                <fieldset>
                    <legend>簡訊內容</legend>
                    <p>
                        <asp:CheckBox ID="cbx_DisableSMS" runat="server" Text="關閉簡訊發送按鈕" />
                    </p>
                    <p>
                        <div>
                            前置文</div>
                        <asp:TextBox ID="txtSMSContent" runat="server" Width="300px" onkeydown="changeWordCount();"></asp:TextBox>
                        <br />
                        <label>憑證檔次此欄若沒有填寫，簡訊前置文會套用訂單短標，若有特別需求，可另外填入</label>
                    </p>
                    <input type="hidden" id="hidSmsFooter" clientidmode="Static" runat="server" />
                </fieldset>
                <fieldset>
                    <legend>其他動作</legend>
                    <asp:Button ID="bStart" runat="server" Text="Start Deal" OnClick="bStart_Click" Visible="false" />
                    <asp:Button ID="bCG" runat="server" Text="Generate Coupon" OnClick="bCG_Click" Visible="false" />
                    <asp:Button ID="bDD" runat="server" Text="Delete Deal" OnClick="bDD_Click" Visible="false" />
                    <asp:Button ID="btnCopyDeal" runat="server" OnClick="btnCopyDeal_Click" OnClientClick="return CheckBeforeCopy(this);" Text="複製此檔好康" Visible="false" />
                    <asp:Button ID="btnCopySubDeal" runat="server" OnClick="btnCopySubDeal_Click" OnClientClick="return CheckBeforeCopy(this);" Text="複製成子檔" Visible="false" />
                    <asp:Button ID="bBBH" runat="server" OnClick="bBBH_Click" Text="BBH:顯示" />
                    <asp:Button ID="btnCopyComboDeals" runat="server" OnClick="btnCopyComboDeals_Click" OnClientClick="return CheckBeforeCopy(this);" Text="打包複製" Visible="false" />
                    <asp:Button ID="btnDealClose" runat="server" Text="手動結檔" OnClick="btnDealClose_Click" OnClientClick="block_postback();" />
                    <br />
                    <div id="divSyncPChome" runat="server">
                        同步PChome
                    <asp:Button ID="btnSyncPChome1" runat="server" Text="庫存、價格、Slogan、銷售口號" OnClick="btnSyncPChome1_Click" OnClientClick="block_postback();" />
                    <asp:Button ID="btnSyncPChome2" runat="server" Text="圖文、權益說明、店家資訊" OnClick="btnSyncPChome2_Click" OnClientClick="block_postback();" />
                    </div>
                    
                    <asp:HiddenField ID="isDealClosed" runat="server" />
                    <asp:Literal ID="Lit_copydeallink" runat="server"></asp:Literal>
                </fieldset>
            </div>
            <div id="tabs-6">
                <fieldset>
                    <legend>多重選項</legend>
                    <asp:TextBox runat="server" ID="txtMult" TextMode="MultiLine" Columns="80" Rows="5"></asp:TextBox><br />
                    <a href="javascript:return false;" id="acc_help">怎麼打?</a>
                    <div id="acc_exp" title="語法範例" style="display: none">
                        <fieldset>
                            <legend>語法</legend>
                            <pre>
    <%if (IsEnableVbsNewShipFile) { %>
    類別名稱:
    貨號!分類一
    [數量] 貨號!分類二

    第二類別:
    貨號!分類三
    貨號!分類四
    ...
    <%} else {%>
    類別名稱:
    分類一
    [數量] 分類二

    第二類別:
    分類三
    分類四
    ...
    <%} %>
                        </pre>
                        </fieldset>
                        <fieldset>
                            <legend>範例</legend>
                            <pre>
    分店:
    [100]永和店
    [80] 中山店

    顏色:
    <%if (IsEnableVbsNewShipFile) { %>
    [50]RED001!紅色
    WH002!白色
    <%} else {%>
    紅色
    白色
    <%} %>
                        </pre>
                        </fieldset>
                    </div>
                </fieldset>
            <fieldset id="CPCSetting">
                <legend>商品組</legend>
                <label>
                    結帳倍數：$<asp:Literal ID="lIP" runat="server"></asp:Literal>
                    ／
                        <asp:TextBox ID="tbCPC" runat="server" Columns="5" Text="1" ClientIDMode="Static"></asp:TextBox>
                    <asp:Label ID="NoCPC" runat="server" Visible="false" Text="此檔已有訂單，若需調整請與創編主管聯繫，並請客服調查已訂購訂單選項，以利資料更新" ForeColor="Red"></asp:Label>
                </label>
                <label>
                    <span class="ui-state-highlight">若非成套的商品組，請填1<br />
                        憑證類商品部是用商品組模式，請填1。</span>
                </label>
            </fieldset>
            <fieldset id="isCombineSetting" runat="server">
                <legend>商家系統品項統計方式</legend>
                <p>
                    <asp:RadioButton runat="server" GroupName="ItemsStatistics" ID="isCombine" Checked="True" Text="多重選項合併統計（預設）" />
                    <span class="ui-state-highlight" style="display: inline-block; width: 350px; vertical-align: top;">1. 只有一個下拉選單時，應選此項。
2. 有多選項，且選項必須合併查看，才可了解選購的商品品項時。
	ex:賣衣服，有兩個下拉選項：尺寸與顏色。合併統計：(S)+(白色)</span>

                    <br />
                    <br />
                    <asp:RadioButton runat="server" GroupName="ItemsStatistics" ID="isSingle" Text="多重選項個別統計" />
                    <span class="ui-state-highlight">有多選項，且需各別統計各品項數量時，請選此項。	</span>
                </p>



            </fieldset>
        </div>
            <div id="tabs-8">
            <fieldset>
                <legend>憑證組數</legend>
                <label>
                    買：
                        <asp:TextBox ID="tbBuyQuantity" runat="server" Columns="5" Text="1" ClientIDMode="Static"></asp:TextBox>
                    送：
                        <asp:TextBox ID="tbPresentQuantity" runat="server" Columns="5" Text="0" ClientIDMode="Static"></asp:TextBox>
                </label>
            </fieldset>
            <fieldset>
                <legend>APP頻道</legend>
                <div class="categoryArea">
                    <asp:CheckBoxList ID="cbl_GroupCouponAppStyle" runat="server" RepeatDirection="Horizontal"></asp:CheckBoxList>
                </div>
            </fieldset>
        </div>
            <div id="tabs-5">
            <fieldset>
                    <legend>預覽</legend><a href="<%= (SelectedChannelCategories.ContainsKey(CategoryManager.Default.Piinlife.CategoryId) ? "../../piinlife/default.aspx?bid=" : "../../ppon/default.aspx?bid=") + BusinessHourId %>&preview=setup"
                        target="_blank">前台預覽</a> <a href="../../ppon/coupon_print.aspx?bid=<%=BusinessHourId%>&preview=setup"
                            target="_blank">好康劵預覽</a>
                    <a href="../../ppon/dealpreview.aspx?bid=<%=BusinessHourId%>&preview=setup" target="_blank">購買頁預覽</a>
                    <%--<asp:Button ID="btnViewPponDeal" runat="server" Text="清除該檔次暫存資料" OnClientClick="return clearViewPponDeal()" />--%>
                </fieldset>
            <div class="ui-state-error ui-corner-all">
                <p>
                    <span class="ui-icon ui-icon-alert" style="float: left; margin-right: 0.3em; margin-left: 0.3em"></span>請先儲存才可預覽
                </p>
            </div>
        </div>
            <div id="tabs-7">
            <asp:HiddenField ID="hidComboData" runat="server" ClientIDMode="Static" />
            <fieldset>
                <legend>設定多檔次</legend>
                <p>
                    <asp:Panel runat="server" ID="panCombo">
                        <div class="comboMain">
                            <span class="comboMainTitle" id="spanComboMainTitle" runat="server">母檔:
                                    <asp:HyperLink ID="linkComboMainTitle" runat="server" CssClass="noBtnUi" Target="_Blank" />
                            </span>
                        </div>
                        <div class="comboSub">
                            子檔:
                            <asp:PlaceHolder ID="phNewComboSub" runat="server">
                                <div>
                                    <asp:DropDownList ID="ddlComboSub" runat="server" ClientIDMode="Static" EnableViewState="false"
                                        Style="min-width: 320px">
                                    </asp:DropDownList>
                                    <input type="button" value="設定子檔" id="btnSetComboSub" />
                                </div>
                            </asp:PlaceHolder>
                        </div>
                        <asp:Repeater ID="repComboItems" runat="server" OnItemDataBound="repComboItems_ItemDataBound">
                            <HeaderTemplate>
                                <table class="comboList">
                                    <thead>
                                        <tr>
                                            <th style="width: 30px; text-align: left">
                                                <asp:Literal ID="litDeleteComboItemCaption" runat="server" Text="刪除" /></th>
                                            <th style="width: 280px; text-align: left; padding-left: 30px">連結</th>
                                            <th style="width: 320px;">顯示名稱</th>
                                            <th style="width: 40px; text-align: right;">銷售</th>
                                            <th style="width: 40px; text-align: right;">售價</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr class="comboItem" bid="<%#Eval("BusinessHourGuid") %>" <%# Eval("Slug") != null ? "style='background-color:gray'" : string.Empty %>>
                                    <td class="comboDealMainPrivate">
                                        <asp:PlaceHolder ID="phDeleteComboItem" runat="server">
                                            <input type="checkbox" class="comboItemDelete" />
                                        </asp:PlaceHolder>
                                    </td>
                                    <td>
                                        <a class="noBtnUi comboItemLink <%#(Guid)Container.Page.GetPropertyValue("BusinessHourId") == (Guid)Eval("BusinessHourGuid") ? "comboItemActived" : "" %>"
                                            href="setup.aspx?bid=<%#Eval("BusinessHourGuid") %>" target="_blank">
                                            <asp:Literal ID="litComboItemLinkTitle" runat="server" />
                                            (<%#Eval("UniqueId") %>)<%#Eval("CouponUsage") %></a></td>
                                    <td>
                                        <asp:TextBox CssClass="comboItemNewTitle" ID="txtComboItemTitle" runat="server" placeholder="請輸入顯示名稱" />
                                    </td>

                                    <%--<td class="pull-right combo-price"><%#Eval("ItemPrice", "{0:0}") %></td>
                                    <td style="display: none" class="combo-op"><%#Eval("ItemOrigPrice", "{0:0}") %></td>--%>
                                    <td class="combo-qty" style="text-align: right;">
                                        <asp:Literal ID="litComboItemSalesQuantity" runat="server" />

                                    </td>
                                    <td class="combo-price" style="width:40px;text-align: right;">
                                        <%#  ((ViewComboDeal)(Container.DataItem)).ItemPrice.ToString("F0")  %>
                                    </td>
                                    <td style="display: none" class="combo-op">
                                        <%# ((ViewComboDeal)(Container.DataItem)).ItemOrigPrice.ToString("F0")%>
                                    </td>
                                    <td style="display: none" class="combo-mulit-quantity">
                                        <asp:Literal ID="litComboItemQuantityMultiplier" runat="server" />
                                    </td>
                                    <td style="display: none" class="combo-is-average-price">
                                        <asp:Literal ID="litComboItemIsAveragePrice" runat="server" />
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </tbody>
                                    </table>
                            </FooterTemplate>
                        </asp:Repeater>
                    </asp:Panel>
                </p>
            </fieldset>
            <fieldset>
                <legend>預覽與資料驗證</legend>
                <div>
                    <input type="button" id="btnComboDealsPreview" value="預覽" style="color: blue" />
                </div>
                <p>
                    <div>
                        <asp:Literal ID="litComboErrMsg" runat="server" EnableViewState="false" Mode="PassThrough" />
                    </div>
                </p>
            </fieldset>
        </div>
            <div id="tabs-9">
                <fieldset>
                    <label>
                        <asp:CheckBox ID="chkCloseMenu" runat="server" ClientIDMode="Static" />關閉MENU功能
                    </label>
                    <label>                        
                        <input type="file" id="fileMenu" /><input type="button" id="btnUploadMenuImage" value="上傳" onclick="MenuImageUpload()" />
                    </label>
                    <label>                        
                        <div id="fileMuneUpdImage"></div>
                    </label>
                    <label>
                        <asp:CheckBox ID="chkCloseMunuContent" runat="server" ClientIDMode="Static" />關閉以下顯示
                    </label>
                    <label>
                        <asp:TextBox ID="tbMenuD" runat="server" placeholder="代表的短標名稱" Width="500px" TextMode="MultiLine" ClientIDMode="Static"></asp:TextBox>
                    </label>
                    <label>
                        <br />
                        <asp:TextBox ID="tbMenuE" runat="server" placeholder="代表的短標名稱" Width="500px" TextMode="MultiLine" ClientIDMode="Static"></asp:TextBox>
                    </label>
                    <label>
                        <br />
                        <asp:TextBox ID="tbMenuF" runat="server" placeholder="代表的短標名稱" Width="500px" TextMode="MultiLine" ClientIDMode="Static"></asp:TextBox>
                    </label>
                    <label>
                        <br />
                        <asp:TextBox ID="tbMenuG" runat="server" placeholder="代表的短標名稱" Width="500px" TextMode="MultiLine" ClientIDMode="Static"></asp:TextBox>
                    </label>
                    <label>
                        <asp:CheckBox ID="chkMenuLogo" runat="server" ClientIDMode="Static" />自動壓LOGO
                    </label>
                    <label>
                        <asp:Button ID="btnPreviewMenu" runat="server" Text="產生MENU並預覽" OnClientClick="return ConvertToImage(this)" ClientIDMode="Static" />
                        <div id="menuContent" class="hide"></div>                      
                        <asp:TextBox ID="hfImageData" runat="server" style="display:none" />
                        <div id="menuPreContent"></div>
                    </label>
                </fieldset>
            </div>
            <div id="tabs-10">
                <fieldset>
                    <legend>核銷權限設定</legend>
                    <table style="font-size: small" class="tblSellerTree">
                        <thead>
                            <tr>
                                <th style="width: 400px">賣家層級資訊</th>
                                <th style="width: 100px">銷售店鋪/隱藏
                                    <asp:Image ID="btnVerifyShop" runat="server" ImageUrl="../../Themes/default/images/question_icon.gif" ClientIDMode="static" style="cursor: pointer;"/><br/>
                                    <input type="checkbox" id="checkAllVerifyShop" onclick="selectAll($(this), 'checkVerifyShop');" />
                                    全選
                                </th>
                                <th style="width: 80px">已售/上限</th>
                                <th style="width: 100px">有效期限截止日</th>
                                <th style="width: 150px">兌換時間</th>
                                <th style="width: 60px">憑證核實
                                    <asp:Image ID="Image1" runat="server" ImageUrl="../../Themes/default/images/question_icon.gif" ToolTip="設定是否可於商家系統核銷憑證" /><br/>
                                    <input type="checkbox" id="checkAllVerify" onclick="selectAll($(this), 'checkVerify');" />
                                    全選
                                </th>
                                <th style="width: 60px">匯款對象
                                    <asp:Image ID="Image2" runat="server" ImageUrl="../../Themes/default/images/question_icon.gif" ToolTip="設定是否為收款店家" /><br/>
                                    <input type="checkbox" id="checkAllAccounting" onclick="selectAll($(this), 'checkAccounting');" />
                                    全選
                                </th>
                                <th style="width: 70px">檢視對帳單
                                    <asp:Image ID="Image3" runat="server" ImageUrl="../../Themes/default/images/question_icon.gif" ToolTip="設定是否可於商家系統檢視對帳單" /><br/>
                                    <input type="checkbox" id="checkAllViewBalanceSheet" onclick="selectAll($(this), 'checkViewBalanceSheet');" />
                                    全選
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <asp:Repeater ID="repSellerTree" runat="server" OnItemDataBound="RepSellerTreeItemDataBound">
                                <ItemTemplate>
                                    <tr>
                                        <td style="width: 400px">
                                            <asp:Label runat="server" ID="lbSellerName"></asp:Label>
                                            <asp:HiddenField runat="server" ID="hidSellerGuid" />
                                        </td>
                                        <td style="width: 100px;text-align: center">
                                            <asp:CheckBox runat="server" ID="cbVerifyShop" CssClass="checkVerifyShop" />/
                                            <asp:CheckBox runat="server" ID="cbHideVerifyShop" CssClass="hideVerifyShop"/>
                                        </td>
                                        <td style="width: 80px">
                                            <asp:Label runat="server" ID="lbQuantity" Text="0"></asp:Label>/
                                            <asp:TextBox runat="server" ID="txtStoreCount" Width="40" MaxLength="5"></asp:TextBox>
                                        </td>
                                        <td style="width: 100px">
                                            <asp:TextBox runat="server" ID="txtStoreChangedExpireDate" CssClass="storeCloseDate" Width="100px" />
                                        </td>
                                        <td style="width: 150px">
                                            <asp:TextBox ID="txtStoreUseTime" runat="server" Width="200" TextMode="MultiLine"></asp:TextBox>
                                        </td>
                                        <td style="width: 60px">
                                            <asp:CheckBox runat="server" ID="cbVerify" CssClass="checkVerify" />
                                        </td>
                                        <td style="width: 60px">
                                            <asp:CheckBox runat="server" ID="cbAccounting" CssClass="checkAccounting" />
                                        </td>
                                        <td style="width: 70px">
                                            <asp:CheckBox runat="server" ID="cbViewBalanceSheet" CssClass="checkViewBalanceSheet" />
                                            <asp:Image ID="btnHideBalanceSheet" runat="server" CssClass="hideBalanceSheet" />
                                            <asp:HiddenField ID="hidIsHideBalanceSheet" runat="server"/>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </tbody>
                    </table>
                    <div id="loadSellerTree" style="display:none">分店讀取中...</div>
                    <div class="loadMoreSellerTree" onclick="getMoreSellerTree(-1);" runat="server">載入更多...</div>
                </fieldset>
            </div>
    </div>
    <div>
        <asp:Button ID="bSend" runat="server" Text="Save" OnClick="bSend_Click" OnClientClick="updateSms(); if (formDataCheck() == false){ return false; } block_postback();" />
        <span class="ui-state-highlight ui-corner-all" style="float: right; margin-top: 8px">
            <span class="ui-icon ui-icon-info" style="float: left"></span>可全部分頁都設定完畢再儲存, 不需每頁儲存</span>
    </div>
    <div id="hidinitDeliveryTime" style="display: none;"></div>
    </div>
    <div class="clearfix"></div>
    <!--多選項預覽-->
    <div id="combodeals" class="Multi-grade-Setting" style="display: none;">
        <div class="mgs-boxtitle">
            請選擇好康
        </div>
        <div id="div_combodeallist">
            <div id="comboSubPreviewItemEmbryo" class="mgs-item-box" style="display: none;">
                <div class="mgs-content">
                    <div class="mgs-item-title">抬頭</div>
                    <div class="mgs-item-state">
                        <div class="mgs-sale rdl-a2wdis">5.8 折</div>
                        <div class="mgs-sale">原價 $100</div>
                        <div class="mgs-buyer mgs-brcolor-org">均價 550</div>
                        <%--<div class="mgs-buyer">5人己購買</div>--%>
                    </div>
                </div>
                <div class="mgs-price-box">
                    <div class="mgs-price">$50</div>
                </div>
                <div class="clearfix">
                </div>
            </div>
        </div>
        <div class="MGS-XX" onclick=" $.unblockUI();" style="cursor: pointer">
            <input type="button" class="btn btn-large btn-primary" value="關閉">
        </div>
        <asp:Panel ID="panelComboDealCount" runat="server" CssClass="Multi-grade-Setting-CEN-farm"
            Visible="false">
        </asp:Panel>
    </div>

    <div id="divStoreHide" class="StoreHide" style="display: none;background-color:white;padding:30px;">
    <div>
        <div>
            此功能設定完成後，會將選擇的店舖，於前台的結帳頁中進行隱藏；客人將不會再看到此分店的下拉選項。
                    <br />
                    <asp:Image ID="imgStoreHide" runat="server" ImageUrl="../../Themes/ControlRoom/HideStore.png"/>
                    <br/>
                    <br/>
                    至於，前台檔次頁的店家資訊，請通知創意同仁協助調整。
                    <br />
                    <asp:Image ID="imgStoreDesc" runat="server" ImageUrl="../../Themes/ControlRoom/StoreDesc.png"/>
                    <br />
                    ※ 若為設錯銷售分店：需請技術同仁進行訂單搬移分店處理，應將錯誤的銷售店鋪取消勾選；請勿使用隱藏功能
        </div>
        <div class="MGS-XX" onclick="$('#divStoreHide').bPopup().close()" style="cursor: pointer">
            <input type="button" class="btn btn-large btn-primary" value="關閉">
        </div>
    </div>


</div>
    <script type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(SetFreeDeal);

        var grossMargin = $('#<%=hidMinGrossMarginVal.ClientID%>').val();
        if (parseFloat($('#<%=hidMinGrossMarginVal.ClientID%>').val().replace(',', ''))) {
            $('#GrossMargin').text(grossMargin);
        } else {
            $('#GrossMargin').text('N/A');
        }
        if (parseFloat($('#<%=hidGrossLimitVal.ClientID %>').val()) >= parseFloat(grossMargin)) {
            $("#GrossMargin").addClass("ui-state-highlight");
            $('#<%=grossMarginMessage.ClientID %>').show();
        } else {
            $("#GrossMargin").removeClass();
            $('#<%=grossMarginMessage.ClientID %>').hide();
        }
        
        $("#<%=cbx_LowGrossMarginAllowedDiscount.ClientID %>").click(function() {
            var IsMulti = $('#<%=HidIsMulti.ClientID%>').val();
            if($("#<%=cbx_LowGrossMarginAllowedDiscount.ClientID %>").prop("checked")){
                if(IsMulti == "1")
                {
                    if(confirm("此為多檔次，若選取相關檔次將一併改變"))
                    {
                    }
                    else
                    {
                        $("#<%=cbx_LowGrossMarginAllowedDiscount.ClientID %>").prop("checked", false);
                    }
         
                }
            }
        });

        $("#<%=cbx_NotAllowedDiscount.ClientID %>").click(function() {
            var IsMulti = $('#<%=HidIsMulti.ClientID%>').val();
            if($(this).prop("checked")){
                if(IsMulti == "1")
                {
                    if(confirm("此為多檔次，若選取相關檔次將一併改變"))
                    {
                    }
                    else
                    {
                        $(this).prop("checked", false);
                    }
         
                }
            }
        });
    </script>
</asp:Content>

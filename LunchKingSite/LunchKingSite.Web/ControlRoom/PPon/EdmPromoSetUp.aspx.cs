﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.UI;
using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LunchKingSite.Web.ControlRoom.Ppon
{
    public partial class EdmPromoSetUp : RolePage
    {
        #region property

        private EasyDigitCipher cipher = new EasyDigitCipher();

        public int CityId
        {
            get
            {
                int cityid;
                if (int.TryParse(ddl_Main_City.SelectedValue, out cityid))
                {
                    return cityid;
                }
                else
                {
                    return 0;
                }
            }
            set
            {
                ddl_Main_City.SelectedValue = value.ToString();
            }
        }

        public DateTime Deal_Date
        {
            get
            {
                DateTime dealdate;
                if (DateTime.TryParse(tbx_DealDate.Text, out dealdate))
                {
                    return dealdate.AddHours(12);
                }
                else
                {
                    return DateTime.MaxValue;
                }
            }
            set
            {
                tbx_DealDate.Text = value.ToString("MM/dd/yyyy");
            }
        }

        public DateTime Delivery_Date
        {
            get
            {
                DateTime deliverydate;
                if (DateTime.TryParse(tbx_DeliveryDate.Text, out deliverydate))
                {
                    int hour, minute;
                    if (int.TryParse(ddl_DeliveryHour.SelectedValue, out hour) && int.TryParse(ddl_DeliveryMinute.SelectedValue, out minute))
                    {
                        return deliverydate.AddHours(hour).AddMinutes(minute);
                    }
                    else
                    {
                        return deliverydate;
                    }
                }
                else
                {
                    return DateTime.MaxValue;
                }
            }
            set
            {
                tbx_DeliveryDate.Text = value.ToString("MM/dd/yyyy");
            }
        }

        public EdmMainType EdmType
        {
            get
            {
                return EdmMainType.Manual;
            }
        }

        public int Id
        {
            get
            {
                int id;
                if (int.TryParse(hif_Id.Value, out id))
                {
                    return id;
                }
                else
                {
                    return 0;
                }
            }
            set
            {
                hif_Id.Value = value.ToString();
            }
        }

        public string UserName
        {
            get
            {
                return User.Identity.Name;
            }
        }

        #endregion property

        #region page

        protected void AddNewEdm(object sender, EventArgs e)
        {
            AddEditEdmType(true);
        }

        protected void CancelAction(object sende, EventArgs e)
        {
            pan_Details.Visible = pan_Search.Visible = false;
            pan_Main.Visible = true;
        }

        protected void ChangeAD(object sender, EventArgs e)
        {
            ListItem item;
            if ((item = ddl_AD_Content.Items.FindByValue(ddl_AD.SelectedValue)) != null)
            {
                lit_AD.Text = HttpUtility.HtmlDecode(item.Text);
            }
        }

        protected void ChangeDealDate(object sender, EventArgs e)
        {
            AddEditEdmType(true);
        }

        protected void DeleteEdmSetting(object sender, EventArgs e)
        {
            if (Id != 0)
            {
                EmailFacade.DisableNewEdm(Id);
                CancelAction(sender, e);
                SearchEdmContent(sender, e);
            }
        }

        protected void gvRowCommand(object sender, GridViewCommandEventArgs e)
        {
            int id;
            string html = string.Empty;
            if (int.TryParse(e.CommandArgument.ToString(), out id))
            {
                if (e.CommandName == "EditItem")
                {
                    AddEditEdmType(false, id);
                }
                else if (e.CommandName == "DeleteItem")
                {
                    EmailFacade.DisableNewEdm(id);
                    EventArgs eg = new EventArgs();
                    CancelAction(sender, eg);
                    SearchEdmContent(sender, eg);
                }
                else if (e.CommandName == "SendCelloPoint")
                {
                    //直接用CelloPoint發送 已設定的cpa為主，沒設定則用系統預設
                    string message = EmailFacade.GenerateNewEdmFile(id, UserName, cbx_Compress.Checked);
                    ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "alertsuccess", "alert('" + message + "已上傳!!');", true);
                }
                else if (e.CommandName == "TestSendCelloPoint")
                {
                    string[] emails = tbx_TestEmail.Text.Replace("\n", ",").Replace("\r", ",").Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                    if (emails.Length > 0)
                    {
                        string message = EmailFacade.GenerateNewEdmFile(id, UserName, cbx_Compress.Checked, emails);
                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "alertsuccess", "alert('" + message + "已上傳!!');", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "alertsuccess", "alert('請填入測試發送Email!!');", true);
                    }
                }
                else
                {
                    if (e.CommandSource is Button)
                    {
                        Button btn = (Button)e.CommandSource;
                        GridViewRow row = (GridViewRow)(btn.NamingContainer);
                        Label lab_deliverydate = (Label)row.FindControl("lab_DeliveryDate");
                        DateTime deliverydate;
                        if (DateTime.TryParse(lab_deliverydate.Text, out deliverydate))
                        {
                            EdmMain main = EmailFacade.GetNewEdmMainById(id);
                            string filename = deliverydate.ToString("MMddHHmm") + main.CityName;
                            //匯出Html
                            if (e.CommandName == "DownLoadCelloPoint")
                            {
                                html = EmailFacade.GetCleanHtmlCode(id, "onlinenewedm", true, "cpa=17_CEDM", "pa=" + cipher.Encrypt(deliverydate.ToString("yyyyddMM")));
                                if (cbx_Compress.Checked)
                                {
                                    html = EmailFacade.MinifyHtml(html);
                                }
                                Response.Clear();
                                Response.AddHeader("content-disposition", "attachment;filename=" + filename + "CelloPoint.html");
                                Response.ContentType = "text/HTML";
                                Response.Write(html);
                                Response.End();
                            }
                            else if (e.CommandName == "DownLoadSES")
                            {
                                html = EmailFacade.GetCleanHtmlCode(id, "onlinenewedm", true, "cpa=17_ses", "pa=" + cipher.Encrypt(deliverydate.ToString("yyyyddMM")));
                                if (cbx_Compress.Checked)
                                {
                                    html = EmailFacade.MinifyHtml(html);
                                }
                                Response.Clear();
                                Response.AddHeader("content-disposition", "attachment;filename=" + filename + "SES.html");
                                Response.ContentType = "text/HTML";
                                Response.Write(html);
                                Response.End();
                            }
                            else if (e.CommandName == "DownLoadEZmail")
                            {
                                html = EmailFacade.GetCleanHtmlCode(id, "onlinenewedm", true, "cpa=PEZ_test", "pa=" + cipher.Encrypt(deliverydate.ToString("yyyyddMM")));
                                if (cbx_Compress.Checked)
                                {
                                    html = EmailFacade.MinifyHtml(html);
                                }
                                Response.Clear();
                                Response.AddHeader("content-disposition", "attachment;filename=" + filename + "EZmail.html");
                                Response.ContentType = "text/HTML";
                                Response.Write(html);
                                Response.End();
                            }
                            else if (e.CommandName == "DownLoadPEZ")
                            {
                                html = EmailFacade.GetCleanHtmlCode(id, "onlinepromoedm", false, "cpa=PEZ_Bedm", "pa=" + cipher.Encrypt(deliverydate.ToString("yyyyddMM")), "mode=pez");
                                if (cbx_Compress.Checked)
                                {
                                    html = EmailFacade.MinifyHtml(html);
                                }
                                Response.Clear();
                                Response.ContentEncoding = System.Text.Encoding.GetEncoding("big5");
                                Response.AddHeader("content-disposition", "attachment;filename=" + filename + "PEZmail.html");
                                Response.ContentType = "text/HTML";
                                Response.Write(html);
                                Response.End();
                            }
                            else if (e.CommandName == "DownLoad17Life")
                            {
                                html = EmailFacade.GetCleanHtmlCode(id, "onlinepromoedm", false, "cpa=PEZ_Bedm", "pa=" + cipher.Encrypt(deliverydate.ToString("yyyyddMM")), "mode=17life");
                                if (cbx_Compress.Checked)
                                {
                                    html = EmailFacade.MinifyHtml(html);
                                }
                                Response.Clear();
                                Response.ContentEncoding = System.Text.Encoding.GetEncoding("big5");
                                Response.AddHeader("content-disposition", "attachment;filename=" + filename + "17Lifemail.html");
                                Response.ContentType = "text/HTML";
                                Response.Write(html);
                                Response.End();
                            }
                        }
                    }
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                //預設地區、時間
                List<PponCity> edm_cities = new List<PponCity> { 
                                                            PponCityGroup.DefaultPponCityGroup.TaipeiCity,
                                                            PponCityGroup.DefaultPponCityGroup.Taoyuan, 
                                                            PponCityGroup.DefaultPponCityGroup.Hsinchu,
                                                            PponCityGroup.DefaultPponCityGroup.Taichung,
                                                            PponCityGroup.DefaultPponCityGroup.Tainan,
                                                            PponCityGroup.DefaultPponCityGroup.Kaohsiung,
                                                            PponCityGroup.DefaultPponCityGroup.PBeautyLocation,
                                                            PponCityGroup.DefaultPponCityGroup.Travel,
                                                            PponCityGroup.DefaultPponCityGroup.AllCountry,
                                                            PponCityGroup.DefaultPponCityGroup.Piinlife 
                };
                ddl_Main_City.DataSource = edm_cities;
                ddl_Main_City.DataTextField = "EdmCityName";
                ddl_Main_City.DataValueField = "CityId";
                ddl_Main_City.DataBind();
                for (int i = 0; i < 24; i++)
                {
                    ddl_DeliveryHour.Items.Add(new ListItem(i.ToString(), i.ToString()));
                }
                for (int i = 0; i < 60; i = i + 10)
                {
                    ddl_DeliveryMinute.Items.Add(new ListItem(i.ToString(), i.ToString()));
                }
                tbx_Main_DeliveryDate_Start.Text = tbx_DeliveryDate.Text = tbx_DealDate.Text = DateTime.Now.ToString("MM/dd/yyyy");
                tbx_Main_DeliveryDate_End.Text = DateTime.Now.AddDays(1).ToString("MM/dd/yyyy");
                ddl_Main_City.SelectedValue = PponCityGroup.DefaultPponCityGroup.TaipeiCity.CityId.ToString();
            }
        }

        protected void SaveEdm(object sender, EventArgs e)
        {
            string errormessage_count = string.Empty;
            string errormessage_repeat = string.Empty;
            bool check_count = true;
            bool check_repeat = true;
            bool check_ad = true;
            bool check_duplicate = true;
            //各區域主檔和子檔對應
            Dictionary<EdmDetailType, EdmDetailType> type_list = EmailFacade.GetNewEdmTypePair();

            EdmMain edmmain;
            //由usercontrol回傳設定資料
            List<EdmDetail> edmdetail_list = edmuc_main_1.GetData();
            edmdetail_list.AddRange(edmuc_main_2.GetData());
            edmdetail_list.AddRange(edmuc_Area_1.GetData());
            edmdetail_list.AddRange(edmuc_Area_2.GetData());
            edmdetail_list.AddRange(edmuc_Area_3.GetData());
            edmdetail_list.AddRange(edmuc_PponPiinLife_1.GetData());
            edmdetail_list.AddRange(edmuc_piinlife_1.GetData());
            edmdetail_list.AddRange(edmuc_piinlife_2.GetData());
            //鑒察重複或漏設
            foreach (var item in type_list)
            {
                //是否有遺漏設定
                if ((edmdetail_list.Count(x => x.Type == (int)item.Key) == 0 && edmdetail_list.Count(x => x.Type == (int)item.Value) > 0) ||
                    (edmdetail_list.Count(x => x.Type == (int)item.Value) == 0 && edmdetail_list.Count(x => x.Type == (int)item.Key) > 0))
                {
                    errormessage_count = item.Key.ToString() + ",";
                    check_count = false;
                }
                else if ((edmdetail_list.Count(x => x.Type == (int)item.Key) > 0 && edmdetail_list.Count(x => x.Type == (int)item.Value) > 0)
                    && (edmdetail_list.Where(x => x.Type == (int)item.Value).GroupBy(x => x.Bid).Count() != edmdetail_list.Count(x => x.Type == (int)item.Value)))
                {
                    //是否同區域有重複檔次
                    errormessage_repeat = item.Key.ToString() + ",";
                    check_repeat = false;
                }
            }
            List<string> duplicate_item = new List<string>();
            //檢查不同區域是否有重複檔次
            foreach (var item in edmdetail_list.Where(x => type_list.Any(y => (int)y.Value == x.Type)).GroupBy(x => x.Bid))
            {
                if (item.Count() > 1)
                {
                    string duplicate = string.Empty;
                    check_duplicate = false;
                    duplicate += "(";
                    foreach (var innitem in item)
                    {
                        duplicate += type_list.First(x => (int)x.Value == innitem.Type).Key.ToString() + ",";
                    }
                    duplicate += ")";
                    duplicate_item.Add(duplicate);
                }
            }
            //AD Banner是否為空
            int ad_id;
            if (!int.TryParse(ddl_AD.SelectedValue, out ad_id) && cbx_AD.Checked)
            {
                check_ad = false;
            }

            if (check_count && check_repeat && check_ad && check_duplicate)
            {
                if (Id != 0)
                {
                    edmmain = EmailFacade.GetNewEdmMainById(Id);
                    edmmain.Subject = tbx_Subject.Text;
                    edmmain.Cpa = tbx_Cpa.Text;
                    edmmain.DeliveryDate = Delivery_Date;
                }
                else
                {
                    edmmain = new EdmMain()
                        {
                            Subject = tbx_Subject.Text,
                            Cpa = tbx_Cpa.Text,
                            DeliveryDate = Delivery_Date,
                            CityId = CityId,
                            CityName = ddl_Main_City.SelectedItem.Text,
                            Creator = UserName,
                            CreateDate = DateTime.Now,
                            Type = (int)EdmType,
                            Status = true,
                            Pause = false
                        };
                }

                int pid = EmailFacade.SaveNewEdmMain(edmmain);
                EmailFacade.DeleteNewEdmDetailByPid(pid);
                foreach (EdmDetail item in edmdetail_list)
                {
                    item.Pid = pid;
                    EmailFacade.SaveNewEdmDetail(item);
                }

                if (cbx_AD.Checked)
                {
                    EdmDetail edmdetail_ad = new EdmDetail() { Pid = pid, AdId = ad_id, Type = (int)EdmDetailType.AD, CityName = "AD", DisplayCityName = false };
                    EmailFacade.SaveNewEdmDetail(edmdetail_ad);
                }

                if (cbx_PEZ.Checked)
                {
                    EdmDetail edmdetail_pez = new EdmDetail() { Pid = pid, Type = (int)EdmDetailType.PEZ, CityName = "PEZ", DisplayCityName = false };
                    EmailFacade.SaveNewEdmDetail(edmdetail_pez);
                }
                AddEditEdmType(false, pid);
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "alertsuccess", "alert('存檔成功!!');", true);
            }
            else
            {
                string errormessage = string.Empty;
                if (!check_count)
                {
                    errormessage += "內容檔次有缺:" + errormessage_count + "。\\n";
                }

                if (!check_repeat)
                {
                    errormessage += "同區域檔次重複:" + errormessage_repeat + "。\\n";
                }

                if (!check_duplicate)
                {
                    errormessage += "不同區域檔次重複:" + duplicate_item.Distinct().Aggregate((x, y) => x + "," + y) + "。\\n";
                }

                if (!check_ad)
                {
                    errormessage += "AD Banner未選!\\n";
                }

                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "alertsave", "alert('" + errormessage + "請檢查後存檔!!');", true);
            }
        }

        protected void SearchEdmContent(object sender, EventArgs e)
        {
            pan_Details.Visible = pan_Search.Visible = false;
            lab_Message.Text = string.Empty;
            int cityid;
            DateTime deliveryS, deliveryE;
            if (int.TryParse(ddl_Main_City.SelectedValue, out cityid) && PponCityGroup.DefaultPponCityGroup.Any(x => x.CityId == cityid)
                && DateTime.TryParse(tbx_Main_DeliveryDate_Start.Text, out deliveryS) && DateTime.TryParse(tbx_Main_DeliveryDate_End.Text, out deliveryE))
            {
                PponCity city = PponCityGroup.DefaultPponCityGroup.First(x => x.CityId == cityid);
                EdmMainCollection mains = EmailFacade.GetNewEdmMainByCityIdDates(city, deliveryS, deliveryE, EdmType, true);
                if (mains.Count > 0)
                {
                    pan_Search.Visible = true;
                    gv_List.DataSource = mains;
                    gv_List.DataBind();
                }
                else
                {
                    lab_Message.Text = "查無資料!!";
                }
            }
        }

        #endregion page

        #region method

        protected void SetUpEdmDetailSetupContol(EdmDetailsSetup usercontrol, EdmDetailType maintype, EdmDetailType detailtype, List<EdmDetail> data, int pid = 0)
        {
            usercontrol.MainDetailType = (int)maintype;
            usercontrol.MainDetailItemType = (int)detailtype;
            usercontrol.FieldName = maintype.ToString();
            usercontrol.EdmDetailData = data.Where(x => x.Type == ((int)maintype) || x.Type == ((int)detailtype)).ToList();
            usercontrol.DeliveryDate = Delivery_Date;
            usercontrol.DealDate = Deal_Date;
            usercontrol.Pid = pid;
            usercontrol.DataBind();
        }

        private void AddEditEdmType(bool isnew, int id = 0)
        {
            tbx_Subject.Text = lab_ID.Text = lab_Message.Text = tbx_Cpa.Text = string.Empty;
            hyp_ID.Visible = hyp_PEZ.Visible = hyp_17Life.Visible = false;
            int cityid;
            List<EdmDetail> edmdetaildata;
            btn_DealDate.Enabled = isnew;
            if (int.TryParse(ddl_Main_City.SelectedValue, out cityid) && PponCityGroup.DefaultPponCityGroup.Any(x => x.CityId == cityid))
            {
                PponCity city = PponCityGroup.DefaultPponCityGroup.First(x => x.CityId == cityid);
                EdmMain edm_main;
                if (isnew)
                {
                    edm_main = new EdmMain();
                    lab_ID.Text = "新增編輯";

                    edmdetaildata = EmailFacade.GetNewEdmDetailsByCityIdDate(city, Deal_Date);
                    EdmDetail first_deal;
                    if ((first_deal = edmdetaildata.Where(x => x.Type == (int)EdmDetailType.MainDeal_1_Items).FirstOrDefault()) != null)
                    {
                        tbx_Subject.Text = first_deal.Title;
                    }
                    btn_Delete.Visible = btn_Delete2.Visible = false;
                    //預設cellopoint cpa
                    tbx_Cpa.Text = "17_CEDM";
                }
                else
                {
                    edm_main = EmailFacade.GetNewEdmMainById(id);
                    lab_ID.Text = "編輯 ID : " + edm_main.Id.ToString();
                    if (edm_main.UploadDate != null)
                    {
                        lab_ID.Text += " (已於" + edm_main.UploadDate.Value.ToString("MM/dd HH:mm") + "上傳發送)";
                    }
                    hyp_ID.Visible = hyp_PEZ.Visible = hyp_17Life.Visible = true;
                    hyp_ID.NavigateUrl = "~/Ppon/OnlineEDM.aspx?eid=" + edm_main.Id;
                    hyp_PEZ.NavigateUrl = "~/Ppon/OnlinePromoEDM.aspx?eid=" + edm_main.Id + "&mode=pez";
                    hyp_17Life.NavigateUrl = "~/Ppon/OnlinePromoEDM.aspx?eid=" + edm_main.Id + "&mode=17life";
                    edmdetaildata = EmailFacade.GetNewEdmDetailsByPid(edm_main.Id);
                    ddl_DeliveryHour.SelectedValue = edm_main.DeliveryDate.Hour.ToString();
                    ddl_DeliveryMinute.SelectedValue = edm_main.DeliveryDate.Minute.ToString();
                    tbx_Subject.Text = edm_main.Subject;
                    tbx_Cpa.Text = edm_main.Cpa;
                    btn_Delete.Visible = btn_Delete2.Visible = true;
                }
                Id = edm_main.Id;

                SetUpEdmDetailSetupContol(edmuc_main_1, EdmDetailType.MainDeal_1, EdmDetailType.MainDeal_1_Items, edmdetaildata, edm_main.Id);
                SetUpEdmDetailSetupContol(edmuc_main_2, EdmDetailType.MainDeal_2, EdmDetailType.MainDeal_2_Items, edmdetaildata, edm_main.Id);
                SetUpEdmDetailSetupContol(edmuc_Area_1, EdmDetailType.Area_1, EdmDetailType.Area_1_Items, edmdetaildata, edm_main.Id);
                SetUpEdmDetailSetupContol(edmuc_Area_2, EdmDetailType.Area_2, EdmDetailType.Area_2_Items, edmdetaildata, edm_main.Id);
                SetUpEdmDetailSetupContol(edmuc_Area_3, EdmDetailType.Area_3, EdmDetailType.Area_3_Items, edmdetaildata, edm_main.Id);
                SetUpEdmDetailSetupContol(edmuc_PponPiinLife_1, EdmDetailType.PponPiinLife_1, EdmDetailType.PponPiinLife_Item_1, edmdetaildata, edm_main.Id);

                #region AD

                ViewCmsRandomCollection ad_list = EmailFacade.GetNewEdmADByCityIdDate(city, Delivery_Date);
                ddl_AD_Content.DataSource = ddl_AD.DataSource = ad_list;
                ddl_AD.DataTextField = "Title";
                ddl_AD_Content.DataTextField = "Body";
                ddl_AD_Content.DataValueField = ddl_AD.DataValueField = "Pid";
                ddl_AD.DataBind();
                ddl_AD_Content.DataBind();
                if (edmdetaildata.Any(x => x.Type == (int)EdmDetailType.AD) && ad_list.Any(x => x.Pid == edmdetaildata.First(y => y.Type == (int)EdmDetailType.AD).AdId))
                {
                    cbx_AD.Checked = true;
                    ddl_AD_Content.SelectedValue = ddl_AD.SelectedValue = edmdetaildata.First(y => y.Type == (int)EdmDetailType.AD).AdId.ToString();
                }
                else
                {
                    cbx_AD.Checked = false;
                }

                EventArgs e = new EventArgs();
                ChangeAD(ddl_Main_City, e);

                #endregion AD

                #region PEZ

                CmsContent article = EmailFacade.GetPEZCmsContent("/edm/promote_pez");
                lit_PEZ.Text = HttpUtility.HtmlDecode(article.Body);
                if (edmdetaildata.Any(x => x.Type == (int)EdmDetailType.PEZ))
                {
                    cbx_PEZ.Checked = true;
                }
                else
                {
                    cbx_PEZ.Checked = false;
                }

                #endregion PEZ
            }
            pan_Details.Visible = true;
            pan_Search.Visible = pan_Main.Visible = false;
        }

        #endregion method
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.Core.Component;
using LunchKingSite.Core;
using System.Data;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Text;
using LunchKingSite.DataOrm;
using LunchKingSite.Core.UI;

namespace LunchKingSite.Web.ControlRoom.Ppon
{
    public partial class PponComReport : RolePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request["bid"] != "")
                    SetGrid(new Guid(Request["bid"]));
            }
        }

        private void SetGrid(Guid bid)
        {
            DataTable dt = ProviderFactory.Instance().GetProvider<IPponProvider>().GetComReport(bid);
            GridView1.DataSource = dt;
            GridView1.DataBind();
        }

        protected void btnToExcel_Click(object sender, EventArgs e)
        {
            ViewPponDeal vp = ProviderFactory.Instance().GetProvider<IPponProvider>().ViewPponDealGet(ViewPponDeal.Columns.BusinessHourGuid, Request["bid"]);
            GridViewExportUtil.Export(vp.CouponUsage + "福利網報表.xls", this.GridView1);

        }
    }
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ControlRoom/backend.master" AutoEventWireup="true"
    CodeBehind="CpaSetUp.aspx.cs" Inherits="LunchKingSite.Web.ControlRoom.CpaSetUp" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Import Namespace="LunchKingSite.DataOrm" %>
<%@ Import Namespace="LunchKingSite.Core" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <script src="../../Tools/js/jquery-1.7.2.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        function validatetextbox(source, args) {
            var cntrl_id = source.controltovalidate;
            var cntrl = $("#" + cntrl_id);
            var is_valid = cntrl.val() != "";
            cntrl.css("background-color", is_valid ? "white" : "#FAAFBA");
            args.IsValid = is_valid;
        }
        $(document).ready(function () {
            $('#<%=ddl_Type.ClientID%>').change(function () {
                type_change();
            });
        });

        function type_change() {
            var selected_value = $('#<%=ddl_Type.ClientID%> option:selected').val();
            if (selected_value == '0' || selected_value == '3' || selected_value == '4') {
                $('#<%=ddl_MaxValue.ClientID%>').attr('disabled', true);
            }
            else {
                $('#<%=ddl_MaxValue.ClientID%>').attr('disabled', false);
            }
        }
    </script>
    <style type="text/css">
        .bgOrange
        {
            background-color: #FFC690;
            text-align: center;
        }
        .bgBlue
        {
            background-color: #B3D5F0;
        }
        .bgLBlue
        {
            background-color: #B2DFEE;
        }
        .bgLOrange
        {
            background-color: #FFEC8B;
            font-size: smaller;
        }
        .title
        {
            text-align: center;
            background-color: #688E45;
            font-weight: bolder;
            color: White;
        }
        .title1
        {
            text-align: center;
            background-color: #8B8970;
            font-weight: bolder;
            color: White;
        }
        .title2
        {
            text-align: center;
            background-color: #778899;
            font-weight: bolder;
            color: White;
        }
        .header
        {
            text-align: center;
            font-size: large;
            font-weight: bolder;
            background-color: #F0AF13;
            color: Blue;
        }
        .fieldset
        {
            border: #688E45 solid 2px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <fieldset class="fieldset">
        <legend class="title">Cpa 設定編輯</legend>
        <asp:Panel ID="pan_List" runat="server">
            <asp:Button ID="btn_Add" runat="server" OnClick="AddCpa" Text="新增活動" />
            <br />
            <fieldset class="fieldset">
                <legend class="title">List</legend>
                <table style="background-color: gray; width: 950px">
                    <tr>
                        <td>
                            <span style="color: White; font-weight: bolder">活動區間: &nbsp;</span>
                            <asp:TextBox ID="tbx_ListStart" runat="server"></asp:TextBox>&nbsp;~&nbsp;<asp:TextBox
                                ID="tbx_ListEnd" runat="server"></asp:TextBox>
                            <cc1:CalendarExtender ID="ce_ListStart" TargetControlID="tbx_ListStart" runat="server">
                            </cc1:CalendarExtender>
                            <cc1:CalendarExtender ID="ec_ListEnd" TargetControlID="tbx_ListEnd" runat="server">
                            </cc1:CalendarExtender>
                            <asp:CustomValidator ID="cv_ListStart" runat="server" ControlToValidate="tbx_ListStart"
                                ClientValidationFunction="validatetextbox" ValidateEmptyText="true" Display="Dynamic"
                                ValidationGroup="D" SetFocusOnError="true"></asp:CustomValidator>
                            <asp:CustomValidator ID="cv_ListEnd" runat="server" ControlToValidate="tbx_ListEnd"
                                ClientValidationFunction="validatetextbox" ValidateEmptyText="true" Display="Dynamic"
                                ValidationGroup="D" SetFocusOnError="true"></asp:CustomValidator>
                            <asp:Button ID="btn_SearchByDate" runat="server" OnClick="SearchCpaListByDate" ValidationGroup="D"
                                Text="查詢" />
                        </td>
                        <td>
                            代碼:&nbsp;
                            <asp:TextBox ID="tbx_CodeName" runat="server"></asp:TextBox>
                            <asp:CustomValidator ID="cv_CodeName" runat="server" ControlToValidate="tbx_CodeName"
                                ClientValidationFunction="validatetextbox" ValidateEmptyText="true" Display="Dynamic"
                                ValidationGroup="C" SetFocusOnError="true"></asp:CustomValidator>
                            <asp:Button ID="ibt_SearchByCodeName" runat="server" OnClick="SearchCpaListByCodeName"
                                ValidationGroup="C" Text="查詢" />
                        </td>
                    </tr>
                </table>
                <asp:Repeater ID="rpt_List" runat="server" OnItemCommand="rptList_Command">
                    <HeaderTemplate>
                        <table style="background-color: gray; width: 950px">
                            <tr>
                                <td class="bgOrange">
                                    編輯
                                </td>
                                <td class="bgOrange">
                                    名稱
                                </td>
                                <td class="bgOrange">
                                    代碼
                                </td>
                                <td class="bgOrange">
                                    起訖
                                </td>
                                <td class="bgOrange">
                                    類別
                                </td>
                                <td class="bgOrange">
                                    狀態
                                </td>
                            </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td class="bgBlue">
                                <asp:LinkButton ID="lbt_Edit" runat="server" CommandArgument=" <%#  ((CpaMain)(Container.DataItem)).Id %>"
                                    CommandName="EditCpaMain"><%#  ((CpaMain)(Container.DataItem)).Id %></asp:LinkButton>
                            </td>
                            <td class="bgBlue">
                                <%#  ((CpaMain)(Container.DataItem)).CpaName %>
                            </td>
                            <td class="bgBlue">
                                <%#  ((CpaMain)(Container.DataItem)).CpaCode %>
                            </td>
                            <td class="bgBlue">
                                <%#  ((CpaMain)(Container.DataItem)).StartDate.ToString("yyyy/MM/dd ~ ") + ((CpaMain)(Container.DataItem)).EndDate.ToString("yyyy/MM/dd")%>
                            </td>
                            <td class="bgBlue">
                                <%# Helper.GetLocalizedEnum(Resources.Localization.ResourceManager,(CpaType)((CpaMain)(Container.DataItem)).Type)%>
                            </td>
                            <td class="bgBlue">
                                <img alt="enabled" src='<%# ((CpaMain)(Container.DataItem)).Enabled ? "../../Themes/default/images/17Life/G3/CheckoutPass.png"  : "../../Themes/default/images/17Life/G3/CheckoutError.png"%>'
                                    style="width: 20px" />
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
            </fieldset>
        </asp:Panel>
        <asp:Panel ID="pan_AddorEdit" runat="server" Visible="false">
            <fieldset class="fieldset">
                <legend class="title">
                    <asp:Label ID="lab_Title" runat="server"></asp:Label></legend>
                <table style="background-color: gray; width: 950px">
                    <tr>
                        <td class="bgOrange">
                            Id
                        </td>
                        <td class="bgBlue" colspan="3'">
                            <asp:Label ID="lab_Id" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="bgOrange" style="width: 15%">
                            名稱
                        </td>
                        <td class="bgBlue" style="width: 35%">
                            <asp:TextBox ID="tbx_Name" runat="server"></asp:TextBox>
                            <asp:CustomValidator ID="cv_Name" runat="server" ControlToValidate="tbx_Name" ClientValidationFunction="validatetextbox"
                                ValidateEmptyText="true" Display="Dynamic" ValidationGroup="A" SetFocusOnError="true"></asp:CustomValidator>
                        </td>
                        <td class="bgOrange" style="width: 15%">
                            代碼
                        </td>
                        <td class="bgBlue" style="width: 35%">
                            <asp:TextBox ID="tbx_Code" runat="server"></asp:TextBox>
                            <asp:CustomValidator ID="cv_Code" runat="server" ControlToValidate="tbx_Code" ClientValidationFunction="validatetextbox"
                                ValidateEmptyText="true" Display="Dynamic" ValidationGroup="A" SetFocusOnError="true"></asp:CustomValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="bgOrange">
                            起訖時間
                        </td>
                        <td class="bgBlue" colspan="3'">
                            <asp:TextBox ID="tbx_StartDate" runat="server"></asp:TextBox>&nbsp;~&nbsp;
                            <asp:TextBox ID="tbx_EndDate" runat="server"></asp:TextBox>
                            <cc1:CalendarExtender ID="ce_StartDate" TargetControlID="tbx_StartDate" runat="server">
                            </cc1:CalendarExtender>
                            <cc1:CalendarExtender ID="ce_EndDate" TargetControlID="tbx_EndDate" runat="server">
                            </cc1:CalendarExtender>
                            <asp:CustomValidator ID="cv_StartDate" runat="server" ControlToValidate="tbx_StartDate"
                                ClientValidationFunction="validatetextbox" ValidateEmptyText="true" Display="Dynamic"
                                ValidationGroup="A" SetFocusOnError="true"></asp:CustomValidator>
                            <asp:CustomValidator ID="cv_EndDate" runat="server" ControlToValidate="tbx_EndDate"
                                ClientValidationFunction="validatetextbox" ValidateEmptyText="true" Display="Dynamic"
                                ValidationGroup="A" SetFocusOnError="true"></asp:CustomValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="bgOrange">
                            類別
                        </td>
                        <td class="bgBlue">
                            <asp:DropDownList ID="ddl_Type" runat="server">
                            </asp:DropDownList>
                        </td>
                        <td class="bgOrange">
                            最大值
                        </td>
                        <td class="bgBlue">
                            <asp:DropDownList ID="ddl_MaxValue" runat="server" Enabled="false">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="bgOrange">
                            抽樣比例
                        </td>
                        <td class="bgBlue">
                            <asp:DropDownList ID="ddl_Ratio" runat="server">
                            </asp:DropDownList>
                        </td>
                        <td class="bgOrange">
                            狀態
                        </td>
                        <td class="bgBlue">
                            <asp:CheckBox ID="cbx_Enabled" runat="server" Checked="true" />
                        </td>
                    </tr>
                    <tr>
                        <td class="bgOrange" colspan="4">
                            <asp:Button ID="btn_Confirm" runat="server" ValidationGroup="A" OnClick="ConfirmAction"
                                Text="儲存" />
                            &nbsp;&nbsp;&nbsp;
                            <asp:Button ID="btn_Cancel" runat="server" OnClick="CancelAction" Text="關閉" />
                        </td>
                    </tr>
                </table>
            </fieldset>
        </asp:Panel>
    </fieldset>
</asp:Content>

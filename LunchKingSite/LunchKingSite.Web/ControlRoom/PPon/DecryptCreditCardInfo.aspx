﻿<%@ Page Language="C#" MasterPageFile="~/ControlRoom/backend.master" AutoEventWireup="true" CodeBehind="DecryptCreditCardInfo.aspx.cs" Inherits="LunchKingSite.Web.ControlRoom.Ppon.DecryptCreditCardInfo" %>
<%@ Import Namespace="LunchKingSite.BizLogic.Component" %>
<%@ Import Namespace="LunchKingSite.Core" %>
<%@ Import Namespace="LunchKingSite.DataOrm" %>
<%@ Import Namespace="LunchKingSite.WebLib.Views" %>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
                <fieldset>
                    <legend>信用卡編碼解碼</legend>
                    CreditCardInfo：<asp:TextBox ID="txtCreditCardInfo" runat="server" ClientIDMode="Static" maxlength="50" size="50"/>
                    <br />
                    <br />
                    Salt:
                    <asp:TextBox ID="txtSalt" runat="server" ClientIDMode="Static" maxlength="30" size="30"/>
                    <asp:Button ID="btnSearch" runat="server" Text="查詢" OnClick="btnSearch_Click" />
                    <hr />
                    信用卡卡號:<asp:Label ID="Result" runat="server" Text=""></asp:Label>
                </fieldset>

                <fieldset id="fs2" runat="server">
                    <legend>信用卡編碼解碼-批次</legend>
                    <asp:Button ID="btnDecrypt" runat="server" Text="批次解碼" OnClick="btnDecrypt_Click" />
                </fieldset>
</asp:Content>

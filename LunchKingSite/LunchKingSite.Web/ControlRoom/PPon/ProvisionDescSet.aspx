﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ControlRoom/backend.master" AutoEventWireup="true"
    CodeBehind="ProvisionDescSet.aspx.cs" Inherits="LunchKingSite.Web.ControlRoom.Ppon.ProvisionDescSet" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <script type="text/javascript">
        function ValidateNumber(e, pnumber) {
            if (!/^\d+$/.test(pnumber)) {
                $(e).val(/^\d+/.exec($(e).val()));
            }
            return false;
        }
    </script>
    <style type="text/css">
        .style1
        {
            background-color: #F0AF13;
            text-align: center;
            color: White;
            font-weight: bold;
        }
        .style2
        {
            background-color: #688E45;
            color: White;
            font-weight: bold;
            width: 60px;
        }
        .style3
        {
            background-color: #B3D5F0;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table>
                <tr>
                    <td colspan="5" class="style1">
                        好康小提示
                    </td>
                </tr>
                <tr>
                    <td class="style2">
                        項目
                    </td>
                    <td class="style2">
                        館別
                    </td>
                    <td class="style2">
                        類型
                    </td>
                    <td class="style2">
                        項目
                    </td>
                    <td class="style2">
                        內容
                    </td>
                </tr>
                <tr>
                    <td class="style2">
                    </td>
                    <td class="style3">
                        <asp:HiddenField ID="hidDepartmentId" runat="server" />
                        <asp:DropDownList ID="ddlDepartment" runat="server" DataValueField="Id" DataTextField="Name"
                            OnSelectedIndexChanged="ddlDepartment_SelectedIndexChanged" AutoPostBack="true"
                            Width="100px">
                        </asp:DropDownList>
                    </td>
                    <td class="style3">
                        <asp:HiddenField ID="hidCategoryId" runat="server" />
                        <asp:DropDownList ID="ddlCategory" runat="server" DataValueField="Id" DataTextField="Name"
                            OnSelectedIndexChanged="ddlCategory_SelectedIndexChanged" AutoPostBack="true"
                            Width="150px">
                        </asp:DropDownList>
                    </td>
                    <td class="style3">
                        <asp:HiddenField ID="hidItemId" runat="server" />
                        <asp:DropDownList ID="ddlItem" runat="server" DataValueField="Id" DataTextField="Name"
                            OnSelectedIndexChanged="ddlItem_SelectedIndexChanged" AutoPostBack="true" Width="200px">
                        </asp:DropDownList>
                    </td>
                    <td class="style3">
                        <asp:HiddenField ID="hidContentId" runat="server" />
                        <asp:DropDownList ID="ddlContent" runat="server" DataValueField="Id" DataTextField="Name"
                            OnSelectedIndexChanged="ddlContent_SelectedIndexChanged" AutoPostBack="true"
                            Width="300px">
                        </asp:DropDownList>
                        <br />
                    </td>
                </tr>
                <tr>
                    <td class="style2">
                        描述
                    </td>
                    <td colspan="4" style="background-color: #FFFFFF; width: 600px">
                        <asp:HiddenField ID="hidDescId" runat="server" />
                        <asp:Label ID="lblDesc" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="style2">
                        編輯
                    </td>
                    <td colspan="3" class="style3">
                        <asp:RadioButtonList ID="rdlType" runat="server" RepeatLayout="Flow" RepeatDirection="Horizontal"
                            OnSelectedIndexChanged="rdlType_SelectedIndexChanged" AutoPostBack="true">
                            <asp:ListItem Text="館別" Value="Department" style="display: none"></asp:ListItem>
                            <asp:ListItem Text="類型" Value="Category" Selected="True"></asp:ListItem>
                            <asp:ListItem Text="項目" Value="Item"></asp:ListItem>
                            <asp:ListItem Text="內容" Value="Content"></asp:ListItem>
                            <asp:ListItem Text="描述" Value="Description"></asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                    <td colspan="1" class="style3">
                        <asp:Button ID="btnAdd" runat="server" Text="新增" OnClick="btnAdd_Click" />
                        <asp:Button ID="btnEdit" runat="server" Text="修改" OnClick="btnEdit_Click" />
                        <asp:Button ID="btnSort" runat="server" Text="排序" OnClick="btnSort_Click" />
                    </td>
                </tr>
                <tr>
                    <td colspan="5" class="style3">
                        <asp:Panel ID="divEdit" runat="server" Visible="false">
                            <asp:HiddenField ID="hidObjectId" runat="server" />
                            <asp:HiddenField ID="hidStatus" runat="server" />
                            <asp:RadioButtonList ID="rdlStatus" runat="server" RepeatLayout="Table" RepeatDirection="Horizontal"
                                Visible="false">
                                <asp:ListItem Text="預設" Value="Default" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="必要" Value="Necessary"></asp:ListItem>
                                <asp:ListItem Text="停用" Value="Disabled"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:TextBox ID="txtEdit" runat="server" TextMode="MultiLine" Width="500px" Height="80px"></asp:TextBox><br />
                            <asp:Button ID="btnSave" runat="server" Text="儲存" OnClick="btnSave_Click" />
                            <asp:Button ID="btnDelete" runat="server" Text="刪除" OnClick="btnDelete_Click" OnClientClick="return confirm('確定要義無反顧的刪除此筆資料嗎??');" />
                            <asp:Button ID="btnCancel" runat="server" Text="取消" OnClick="btnCancel_Click" />
                        </asp:Panel>
                        <asp:Panel ID="divSort" runat="server" Visible="false">
                            <asp:Repeater ID="repSort" runat="server" OnItemDataBound="repSort_ItemDataBound"
                                EnableViewState="true">
                                <ItemTemplate>
                                    <asp:HiddenField ID="hidSortObjectId" runat="server" />
                                    <asp:TextBox ID="txtRank" runat="server" Width="30px" MaxLength="3" onkeyup="return ValidateNumber($(this),value);"></asp:TextBox>
                                    <asp:Label ID="lblDescription" runat="server"></asp:Label>
                                    <br />
                                </ItemTemplate>
                            </asp:Repeater>
                            <asp:Button ID="btnSaveSort" runat="server" Text="儲存" OnClick="btnSaveSort_Click" />
                            <asp:Button ID="btnCancel2" runat="server" Text="取消" OnClick="btnCancel_Click" />
                        </asp:Panel>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:Button ID="btnMongoSyncSql" runat="server" Text="Mongo To SQL" OnClick="btnMongoSyncSql_Click" style="display:none" />
</asp:Content>

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ControlRoom/backend.master" AutoEventWireup="true"
    CodeBehind="PponSellerItemList.aspx.cs" Inherits="LunchKingSite.Web.ControlRoom.Ppon.PponSellerItemList" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <fieldset style="width: 600px">
        <legend>店家清冊名單</legend>
        <table>
            <tr>
                <td>
                    購買日期:
                </td>
                <td>
                    <asp:TextBox ID="txtCreateTimeS" runat="server" Columns="8" />
                    <cc1:CalendarExtender ID="ceDS" TargetControlID="txtCreateTimeS" runat="server" Format="yyyy/MM/dd">
                    </cc1:CalendarExtender>
                    ～
                    <asp:TextBox ID="txtCreateTimeE" runat="server" Columns="8" />
                    <cc1:CalendarExtender ID="ceDE" TargetControlID="txtCreateTimeE" runat="server" Format="yyyy/MM/dd">
                    </cc1:CalendarExtender>
                </td>
                <td>
                    <asp:Button ID="btnSearch" runat="server" Text="搜尋" OnClick="btnSearch_Click" />
                    &nbsp;&nbsp;<span style="font-size: small;">※不輸入購買日期為搜尋全部</span>
                </td>
            </tr>
        </table>
    </fieldset>
    <br />
    <asp:Button ID="btnExportToExcel" runat="server" Text="ExportToExcel" OnClick="btnExportToExcel_Click" />
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <asp:Label ID="lbShippedTime" runat="server" Text="出貨回覆日:"></asp:Label>
    <asp:TextBox ID="tbShippedDate" runat="server"></asp:TextBox>
    <cc1:CalendarExtender ID="ceShippedDate" TargetControlID="tbShippedDate" runat="server" Format="yyyy/MM/dd">
    </cc1:CalendarExtender>
    <asp:Button ID="btSaveTime" runat="server" Text="儲存日期" OnClick="btSaveTime_Click" />
	&nbsp;<asp:Button ID="btnFinalBalanceSheetDate" runat="server" Text="已核對對帳單" 
		onclick="btnFinalBalanceSheetDate_Click" />
    <asp:Label ID="lbMessage" runat="server" ForeColor="red"></asp:Label>
    <br />
    <asp:Label ID="lblItemName" runat="server"></asp:Label><b>清冊名單總共：<asp:Label ID="lblTotalCount"
        runat="server"></asp:Label>&nbsp;份</b><br />
    <asp:Literal ID="lit_Continued" runat="server"></asp:Literal>
    <br />
    <b>
        <asp:Label ID="lblOrderTime" runat="server"></asp:Label></b><br />
    <asp:GridView ID="GridView1" runat="server">
    </asp:GridView>
    <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
    <asp:Button ID="btDeleteTime" runat="server" Text="清除日期" OnClick="btDeleteTime_Click" />
	</asp:Content>

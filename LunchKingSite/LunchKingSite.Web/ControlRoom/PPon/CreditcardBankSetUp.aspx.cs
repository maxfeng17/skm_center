﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.DataOrm;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.UI;
using LunchKingSite.WebLib.Views;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.Core.ModelPartial;

namespace LunchKingSite.Web.ControlRoom.Ppon
{
    public partial class CreditcardBankSetUp : RolePage, ICreditcardBankSetUpView
    {
        #region property
        private CreditcardBankSetUpPresenter _presenter;
        public ISysConfProvider config = ProviderFactory.Instance().GetConfig();

        public CreditcardBankSetUpPresenter Presenter
        {
            set
            {
                this._presenter = value;
                if (value != null)
                {
                    this._presenter.View = this;
                }
            }
            get
            {
                return this._presenter;
            }
        }

        public string UserName
        {
            get
            {
                return User.Identity.Name;
            }
        }
        public FileUpload FUImport
        {
            get
            {
                return FileImport;
            }
        }
        public string BankId
        {
            get
            {
                return txtBankId.Text.Trim();
            }
        }
        public string BankName
        {
            get
            {
                return txtBankName.Text.Trim();
            }
        }
        public int InsCreditcardBank
        {
            get
            {
                int bankId = 0;
                int.TryParse(ddlInsCreditcardBank.SelectedValue, out bankId);
                return bankId;
            }
        }
        public string CreditCardBin
        {
            get
            {
                return txtCreditCardBin.Text.Trim();
            }
        }
        public int CardType
        {
            get
            {
                int cardType = 0;
                int.TryParse(ddlCardType.SelectedValue, out cardType);
                return cardType;
            }
        }
        #endregion property

        #region event
        public event EventHandler<DataEventArgs<int>> Search;
        public event EventHandler Import;
        public event EventHandler BankInsert;
        public event EventHandler CreditcardInsert;
        #endregion event

        #region page
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                _presenter.OnViewInitialized();
            }
            _presenter.OnViewLoaded();
        }
        protected void btnImport_Click(object sender, EventArgs e)
        {
            if (this.Import != null)
            {
                this.Import(this, e);
            }
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            int creditcardBank = 0;
            int.TryParse(ddlCreditcardBank.SelectedValue, out creditcardBank);

            if (!creditcardBank.Equals(0))
            {
                this.Search(this, new DataEventArgs<int>(creditcardBank));
            }
        }
        protected void btnCreditcardInsert_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtCreditCardBin.Text.Trim()))
            {
                this.CreditcardInsert(this, e);
            }
        }
        protected void btnBankInsert_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtBankId.Text.Trim()) && !string.IsNullOrEmpty(txtBankName.Text.Trim()) && txtBankId.Text.Trim().Length == 3)
            {
                this.BankInsert(this, e);
            }
        }
        #endregion page

        #region method
        public void SetCreditcardBank(CreditcardBankCollection banks)
        {
            ddlCreditcardBank.DataSource = banks.OrderBy(x=>x.BankName);
            ddlCreditcardBank.DataTextField = "BankName";
            ddlCreditcardBank.DataValueField = "Id";
            ddlCreditcardBank.DataBind();

            ddlInsCreditcardBank.DataSource = banks.OrderBy(x => x.BankName);
            ddlInsCreditcardBank.DataTextField = "BankName";
            ddlInsCreditcardBank.DataValueField = "Id";
            ddlInsCreditcardBank.DataBind();

            Dictionary<int, string> flags = new Dictionary<int, string>();
            foreach (var item in Enum.GetValues(typeof(CreditCardType)))
            {
                flags[(int)item] = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, (CreditCardType)item);
            }
            ddlCardType.DataSource = flags;
            ddlCardType.DataTextField = "Value";
            ddlCardType.DataValueField = "Key";
            ddlCardType.DataBind();

        }
        public void SetCreditcardBankInfo(List<CreditcardBankInfoModel> info)
        {
            rptCreditcardBankInfo.DataSource = info;
            rptCreditcardBankInfo.DataBind();
        }

        public void ShowMessage(string msg)
        {
            string script = script = string.Format("alert('{0}');", msg, config.SiteUrl);

            if (!string.IsNullOrWhiteSpace(script))
            {
                Page.ClientScript.RegisterStartupScript(GetType(), "alert", script, true);
            }
        }
        #endregion method
    }
}
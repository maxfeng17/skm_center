﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.Services;
using log4net;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using System.Linq;

namespace LunchKingSite.Web.ControlRoom.Ppon
{
    public partial class ExpandEndTime : System.Web.UI.Page
    {
        private static ILog logger = LogManager.GetLogger(typeof(ExpandEndTime));
        
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        public static dynamic QueryStatus(string[] bids, string endDate)
        {
            DateTime newEndTime;
            if (CheckData(bids, endDate, out newEndTime) == false)
            {
                return false;
            }
            newEndTime = newEndTime.SetTime(23, 59, 00);
            string errorMessage;

            List<QueryModel> results = new List<QueryModel>();

            foreach (string bid in bids)
            {
                Guid businessHoudGuid;
                if (Guid.TryParse(bid, out businessHoudGuid) == false)
                {
                    results.Add(new QueryModel { Bid = bid, Result = false, Message = "錯誤的bid" });
                    continue;
                }
                bool res = new PponExpandEndTime().Check(businessHoudGuid, newEndTime, true, out errorMessage);
                results.Add(new QueryModel { Bid = bid, Result = res, Message = errorMessage });
            }
            return new
            {
                Data = results,
            };
        }

        /// <summary>
        /// 執行延長結檔時間
        /// </summary>
        /// <param name="bids"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        [WebMethod]
        public static dynamic ExecuteExpand(string[] bids, string endDate)
        {
            DateTime newEndTime;
            if (CheckData(bids, endDate, out newEndTime) == false)
            {
                return false;
            }
            newEndTime = newEndTime.SetTime(23, 59, 00);
            string userName = HttpContext.Current.User.Identity.Name;
            string errorMessage;

            List<QueryModel> results = new List<QueryModel>();

            foreach (string bid in bids)
            {
                Guid businessHoudGuid;
                if (Guid.TryParse(bid, out businessHoudGuid) == false)
                {
                    results.Add(new QueryModel { Bid = bid, Result = false, Message = "錯誤的bid" });
                    continue;
                }
                StringBuilder sbSuccessLog = new StringBuilder();
                bool res = new PponExpandEndTime().Execute(businessHoudGuid, newEndTime, userName, sbSuccessLog, out errorMessage);
                if (sbSuccessLog.Length > 0)
                {
                    sbSuccessLog.Insert(0, HttpContext.Current.User.Identity.Name + " 執行延長檔次列表");
                    logger.Info(sbSuccessLog);
                }
                results.Add(new QueryModel { Bid = bid, Result = res, Message = errorMessage });
            }
            return new
            {
                Data = results,
            };
        }

        private static bool CheckData(string[] bids, string endDate, out DateTime newEndTime)
        {
            if (DateTime.TryParse(endDate, out newEndTime) == false)
            {
                HttpContext.Current.Response.StatusCode = 500;
                HttpContext.Current.Response.StatusDescription = "date parse error";
                return false;
            }

            if (HttpContext.Current.User.Identity.IsAuthenticated == false)
            {
                HttpContext.Current.Response.StatusCode = 500;
                HttpContext.Current.Response.StatusDescription = "userNotLogin";
                return false;
            }

            if (bids == null || bids.Length == 0)
            {
                HttpContext.Current.Response.StatusCode = 500;
                HttpContext.Current.Response.StatusDescription = "no bids";
                return false;
            }
            return true;
        }


        

        [WebMethod]
        public static dynamic QueryStatusClose(string[] bids)
        {
            if (!CheckDataClose(bids))
            {
                return false;
            }

            string errorMessage;

            List<QueryModel> results = new List<QueryModel>();

            foreach (string bid in bids)
            {
                Guid businessHoudGuid;
                if (Guid.TryParse(bid, out businessHoudGuid) == false)
                {
                    results.Add(new QueryModel { Bid = bid, Result = false, Message = "錯誤的bid" });
                    continue;
                }
                bool res = new PponExpandEndTime().CheckClose(businessHoudGuid, true, out errorMessage);
                results.Add(new QueryModel { Bid = bid, Result = res, Message = errorMessage });
            }
            return new
            {
                Data = results,
            };
        }

        [WebMethod]
        public static dynamic ExecuteClose(string[] bids)
        {
            if (!CheckDataClose(bids))
            {
                return false;
            }

            string userName = HttpContext.Current.User.Identity.Name;
            string errorMessage;

            List<QueryModel> results = new List<QueryModel>();

            //取5分鐘內的時間
            DateTime newEndTime = DateTime.Now;
            int min = newEndTime.Minute;
            while (min % 5 !=0)
            {
                newEndTime = newEndTime.AddMinutes(-1);
                min = newEndTime.Minute;
            }

            StringBuilder sbSuccessLog = new StringBuilder();
            foreach (string bid in bids)
            {
                Guid businessHoudGuid;
                if (Guid.TryParse(bid, out businessHoudGuid) == false)
                {
                    results.Add(new QueryModel { Bid = bid, Result = false, Message = "錯誤的bid" });
                    continue;
                }
                
                
                bool res = new PponExpandEndTime().ExecuteClose(businessHoudGuid, newEndTime, userName, sbSuccessLog, out errorMessage);
                if (sbSuccessLog.Length > 0)
                {
                    sbSuccessLog.Insert(0, HttpContext.Current.User.Identity.Name + " 執行批次結檔檔次列表");
                    logger.Info(sbSuccessLog);
                }
                results.Add(new QueryModel { Bid = bid, Result = res, Message = errorMessage });
            }

            return new
            {
                Data = results,
            };

            
        }

        

        private static bool CheckDataClose(string[] bids)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated == false)
            {
                HttpContext.Current.Response.StatusCode = 500;
                HttpContext.Current.Response.StatusDescription = "userNotLogin";
                return false;
            }

            if (bids == null || bids.Length == 0)
            {
                HttpContext.Current.Response.StatusCode = 500;
                HttpContext.Current.Response.StatusDescription = "no bids";
                return false;
            }
            return true;
        }

        internal class QueryModel
        {
            public string Bid;
            public bool Result;
            public string Message;

            public override string ToString()
            {
                if (Result)
                {
                    return string.Format("{0} - OK", Bid);
                }
                return string.Format("{0} - {1}", Bid, Message);
            }
        }
    }
    
}
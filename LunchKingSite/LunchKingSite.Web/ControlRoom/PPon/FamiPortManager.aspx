﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ControlRoom/backend.master" AutoEventWireup="true"
    CodeBehind="FamiPortManager.aspx.cs" Inherits="LunchKingSite.Web.ControlRoom.Ppon.FamiPortManager" %>

<%@ Import Namespace="LunchKingSite.DataOrm" %>
<%@ Register Src="../../Ppon/Pager.ascx" TagName="Pager" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <script type="text/javascript" src="../../Tools/js/jquery-1.7.2.min.js"></script>
    <script type="text/javascript">
        function CheckData() {
            if ($.trim($('[id*=txtBusinessHourGuid]').val()) == '') {
                alert('請輸入bid!');
                return false;
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table>
        <tr>
            <td>
                <fieldset>
                    <legend>FamiPort 全家就是你家</legend>
                    <table>
                        <tr>
                            <td>
                                bid:
                            </td>
                            <td>
                                <asp:TextBox ID="txtBusinessHourGuid" runat="server" Width="278px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                活動代號:
                            </td>
                            <td>
                                <asp:TextBox ID="txtEventId" runat="server" Width="278px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                密碼:
                            </td>
                            <td>
                                <asp:TextBox ID="txtPassword" runat="server" Width="278px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:Button ID="btnSearch" runat="server" Text="查詢" OnClick="btnSearch_Click" />
                                <asp:Button ID="btnSave" runat="server" Text="儲存" OnClick="btnSave_Click" OnClientClick="return CheckData();" />
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </td>
        </tr>
    </table>
    <asp:Label ID="lblErrorMsg" runat="server" Visible="false" ForeColor="Red"></asp:Label>
    <br />
    <asp:GridView ID="gvFamiPort" runat="server" OnRowCommand="gvFamiPort_RowCommand"
        OnRowDataBound="gvFamiPort_RowDataBound" GridLines="Horizontal" ForeColor="Black"
        CellPadding="4" BorderWidth="1px" BorderStyle="None" BorderColor="#DEDFDE" BackColor="White"
        EmptyDataText="無符合的資料" AutoGenerateColumns="false" Font-Size="Small" EnableViewState="true"
        Width="1200px">
        <FooterStyle BackColor="#CCCC99"></FooterStyle>
        <RowStyle BackColor="#F7F7DE" Height="20px" HorizontalAlign="Center"></RowStyle>
        <SelectedRowStyle BackColor="#CE5D5A" ForeColor="White" Font-Bold="True"></SelectedRowStyle>
        <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Center"></PagerStyle>
        <HeaderStyle BackColor="#6B696B" ForeColor="White" Font-Bold="True" Height="20px"
            HorizontalAlign="Center"></HeaderStyle>
        <Columns>
            <asp:TemplateField HeaderText="Id" ItemStyle-HorizontalAlign="Center">
                <ItemTemplate>
                    <asp:Label ID="lblId" runat="server"></asp:Label>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" Width="5%" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="檔次" ItemStyle-HorizontalAlign="Center">
                <ItemTemplate>
                    <asp:HyperLink ID="hlDeal" runat="server" Target="_blank"></asp:HyperLink>
                    <asp:HiddenField ID="hidBid" runat="server" />
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" Width="20%" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="活動代號" ItemStyle-HorizontalAlign="Center">
                <ItemTemplate>
                    <asp:Label ID="lblEventId" runat="server"></asp:Label>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Password" ItemStyle-HorizontalAlign="Center">
                <ItemTemplate>
                    <asp:Label ID="lblPassword" runat="server"></asp:Label>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="建立人員" ItemStyle-HorizontalAlign="Center">
                <ItemTemplate>
                    <asp:Label ID="lblCreateId" runat="server"></asp:Label>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="建立日期" ItemStyle-HorizontalAlign="Center">
                <ItemTemplate>
                    <asp:Label ID="lblCreateTime" runat="server"></asp:Label>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="備註" ItemStyle-HorizontalAlign="Center">
                <ItemTemplate>
                    <asp:Label ID="lblRemark" runat="server"></asp:Label>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" Width="20%" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="已上傳序號" ItemStyle-HorizontalAlign="Center">
                <ItemTemplate>
                    <asp:Label ID="lblIsImport" runat="server"></asp:Label>
                    <asp:Button ID="btnImport" runat="server" Text="上傳" CommandName="IMP" OnClientClick="return confirm('確定要上傳序號??');" />
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>
            <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="編輯">
                <ItemTemplate>
                    <asp:Button ID="btnUpdate" runat="server" Text="編輯" CommandName="UPD" />
                    <asp:Button ID="btnDelete" runat="server" Text="刪除" CommandName="DEL" />
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" />
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    <uc1:Pager ID="ucPager" runat="server" PageSize="10" ongetcount="GetCount" onupdate="UpdateHandler">
    </uc1:Pager>
</asp:Content>

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.Web.ControlRoom.Controls;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core.UI;

namespace LunchKingSite.Web.ControlRoom.Ppon
{
    public partial class EdmTitleSetUp : RolePage
    {
        public Dictionary<EdmTitleItem, int> EdmTitleCity { get; set; }
        public string UserName
        {
            get { return User.Identity.Name; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //各usercontrol對應的城市
            EdmTitleCity = new Dictionary<EdmTitleItem, int>();
            EdmTitleCity.Add(EdmTitleItem1, PponCityGroup.DefaultPponCityGroup.TaipeiCity.CityId);
            EdmTitleCity.Add(EdmTitleItem2, PponCityGroup.DefaultPponCityGroup.Taoyuan.CityId);
            EdmTitleCity.Add(EdmTitleItem3, PponCityGroup.DefaultPponCityGroup.Hsinchu.CityId);
            EdmTitleCity.Add(EdmTitleItem4, PponCityGroup.DefaultPponCityGroup.Taichung.CityId);
            EdmTitleCity.Add(EdmTitleItem5, PponCityGroup.DefaultPponCityGroup.Tainan.CityId);
            EdmTitleCity.Add(EdmTitleItem6, PponCityGroup.DefaultPponCityGroup.Kaohsiung.CityId);
            EdmTitleCity.Add(EdmTitleItem7, PponCityGroup.DefaultPponCityGroup.PBeautyLocation.CityId);
            EdmTitleCity.Add(EdmTitleItem8, PponCityGroup.DefaultPponCityGroup.Travel.CityId);
            EdmTitleCity.Add(EdmTitleItem9, PponCityGroup.DefaultPponCityGroup.AllCountry.CityId);
            EdmTitleCity.Add(EdmTitleItem10, PponCityGroup.DefaultPponCityGroup.Piinlife.CityId);
            if (!IsPostBack)
            {
                foreach (var item in EdmTitleCity)
                {
                    SetUpEdmTitleItem(item.Key, item.Value);
                }
            }

        }
        public void SetUpEdmTitleItem(EdmTitleItem item, int cityid)
        {
            item.CityId = cityid;
            item.UserName = UserName;
            item.DataBind();
        }

        protected void SaveItem(object sender, EventArgs e)
        {
            if (EdmTitleCity.Any(x => string.IsNullOrEmpty(x.Key.ReturnEdmMain().Subject)))
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "alertsave", "alert('內容有缺!!請補齊後存檔!!');", true);
            else
            {
                foreach (var item in EdmTitleCity)
                {
                    EmailFacade.SaveNewEdmMain(item.Key.ReturnEdmMain());
                }
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "alertsave", "alert('存檔成功!!');", true);
            }
        }
    }
}
﻿<%@ Page Language="C#" MasterPageFile="~/ControlRoom/backend.master" AutoEventWireup="true" CodeBehind="PponCouponRefund.aspx.cs" Inherits="LunchKingSite.Web.ControlRoom.Ppon.PponCouponRefund" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script src="//ajax.microsoft.com/ajax/jquery/jquery-1.4.2.min.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">

        function CheckData() {
            var deliveryType = $('select[id*=ddlDeliveryType]').val();

            if ($('input[id*=tbBid]').val() === '' && deliveryType != 3) {
                alert('請輸入BID!');
                return false;
            }

            if (deliveryType === '') {
                alert('請選擇退貨方式!');
                return false;
            }

            if ($('textarea[id*=tbCoupons]').val() === '') {
                var inputType = "";
                if (deliveryType === 'ToShop') {
                    inputType = "憑證編號";
                }
                else if (ReturnStyle === 'ToHouse') {
                    inputType = "訂單編號";
                }

                alert('請輸入' + inputType + '!');
                return false;
            }

            if ($('textarea[id*=tbReason]').val() === '') {
                alert('請輸入退貨原因!');
                return false;
            }
        }

    </script>
    <table>
        <tr>
            <td>
                <p>請先檢查該檔次是否為使用線上核銷檔次:<br/>
                1.若是線上核銷檔次，請不要填憑證編號，讓系統自動檢查<br/>
                2.若非線上核銷檔次，請勿填入重複的憑證編號<br/>
                </p>
                <table width="400">
                    <tr>
                        <td Width="25%"><asp:Label ID="Label1" runat="server" Text="BID："></asp:Label></td>
                        <td Width="75%"><asp:TextBox ID="tbBid" runat="server" Text="" Width="100%"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td Width="25%" valign="top"> 
                            <asp:DropDownList ID="ddlDeliveryType" runat="server">
                            </asp:DropDownList></td>
                        <td Width="75%">
                            <asp:TextBox ID="tbCoupons" runat="server" Text="" TextMode="MultiLine" Height="100"  Width="100%"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>退貨原因</td>
                        <td><asp:TextBox ID="tbReason" runat="server" Text="" TextMode="MultiLine" Height="50" Width="100%"></asp:TextBox></td>
                    </tr>
                </table>
                <asp:CheckBox ID="cbIsRefundSCashOnly" runat="server" Text="只退購物金"/>
                <asp:Button ID="btRefund" runat="server" Text="批次退貨" onclick="btRefund_Click" OnClientClick="return CheckData();" />
                <asp:Button ID="btRefundCash" runat="server" Text="批次轉退現金" onclick="btRefundCash_Click" Visible="false" /><br/>
                <asp:Label ID="lbMessage" runat="server" Text="" ForeColor="Red"></asp:Label>
            </td>
        </tr>
    </table>
</asp:Content>
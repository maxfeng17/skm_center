﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using System.Web.Services;
using System.Web.Security;
using LunchKingSite.Core.UI;


namespace LunchKingSite.Web.ControlRoom.Ppon
{
    public partial class PponFaq : RolePage
    {
        FaqCollection faqs;
        IPponProvider pp;
        public DepartmentTypes Department
        {
            get
            {
                DepartmentTypes department;
                if (Enum.TryParse<DepartmentTypes>(rbl_Department.SelectedValue, out department))
                {
                    return department;
                }
                return DepartmentTypes.Ppon;
            }
        }
        public PponFaq()
        {
            pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            faqs = pp.FaqCollectionGet();
            if (!IsPostBack)
            {
                BindAllDDL(ddl_Sequence1, true);
            }
        }
        protected void BindControl(object sender, EventArgs e)
        {
            BindAllDDL((DropDownList)sender, false);
        }

        protected void BindAllDDL(DropDownList ddl, bool is_rebind_sequence_1)
        {
            int id1, id2;
            if (is_rebind_sequence_1)
            {
                BindDDL(ddl_Sequence1, GetParentFaqListItem(faqs));
                BindDDL(ddl_Sequence2, new ListItem[] { });
                BindDDL(ddl_Sequence3, new ListItem[] { });
                CancelEdit(this, new EventArgs());
            }
            if (int.TryParse(ddl_Sequence1.SelectedValue, out id1) && ddl == ddl_Sequence1)
            {
                BindDDL(ddl_Sequence2, faqs.Where(x => x.Pid == id1).OrderBy(x => x.Sequence).Select((x, i) => new ListItem((i + 1) + "." + x.Contents.TrimToMaxLength(20, "..."), x.Id.ToString())));
            }
            if (int.TryParse(ddl_Sequence2.SelectedValue, out id2))
            {
                BindDDL(ddl_Sequence3, faqs.Where(x => x.Pid == id2).Select(x => new ListItem(x.Contents, x.Id.ToString())));
                Faq faq = faqs.SingleOrDefault(x => x.Pid == id2);
                lit_Contents.Text = faq == null ? string.Empty : faq.Contents;
            }
            else {
                lit_Contents.Text = string.Empty;
            }
            CheckAddEnable(ddl, new EventArgs());
        }

        protected void BindDDL(DropDownList ddl_target, IEnumerable<ListItem> faqs)
        {
            ddl_target.DataSource = faqs;
            ddl_target.DataTextField = "Text";
            ddl_target.DataValueField = "Value";
            ddl_target.DataBind();
        }

        public void CheckAddEnable(object sender, EventArgs e)
        {
            int id;
            pan_Edit.Visible = pan_Sort.Visible = plbl_Title.Visible = plbl_Type.Visible = false;
            btn_Add.Enabled = btn_Edit.Enabled = btn_Delete.Enabled = btn_Sort.Enabled = true;
            GetID(hid_type.Value == "edit", true);
            if (rbl_Edit.SelectedValue == "0")
            {
                plbl_Title.Text = "大標題";
                rList.DataSource = GetParentFaqList(faqs);
                if (string.IsNullOrEmpty(ddl_Sequence1.SelectedValue))
                    btn_Edit.Enabled = btn_Delete.Enabled = false;
            }
            else if (rbl_Edit.SelectedValue == "1")
            {
                plbl_Title.Text = "問題:";
                if (string.IsNullOrEmpty(ddl_Sequence1.SelectedValue))
                    btn_Add.Enabled = btn_Edit.Enabled = btn_Delete.Enabled = false;
                else if (int.TryParse(ddl_Sequence1.SelectedValue, out id))
                {
                    rList.DataSource = faqs.Where(x => x.Stage == 1 && x.Pid == id).OrderBy(x => x.Sequence);
                    plbl_Title.Text += ddl_Sequence1.SelectedItem.Text;
                    if (string.IsNullOrEmpty(ddl_Sequence2.SelectedValue))
                        btn_Edit.Enabled = btn_Delete.Enabled = false;
                    else
                        plbl_Title.Text += "-->" + ddl_Sequence2.SelectedItem.Text;
                }
            }
            else if (rbl_Edit.SelectedValue == "2")
            {
                plbl_Title.Text = "解答:";
                btn_Sort.Enabled = false;
                if (string.IsNullOrEmpty(ddl_Sequence1.SelectedValue))
                    btn_Add.Enabled = btn_Edit.Enabled = btn_Delete.Enabled = false;
                else
                {
                    plbl_Title.Text += ddl_Sequence1.SelectedItem.Text;
                    if (string.IsNullOrEmpty(ddl_Sequence2.SelectedValue))
                        btn_Add.Enabled = btn_Edit.Enabled = btn_Delete.Enabled = false;
                    else
                    {
                        plbl_Title.Text += "-->" + ddl_Sequence2.SelectedItem.Text;
                        if (faqs.SingleOrDefault(x => x.Pid == int.Parse(ddl_Sequence2.SelectedValue)) == null)
                            btn_Edit.Enabled = btn_Delete.Enabled = false;
                        else
                            btn_Add.Enabled = btn_Delete.Enabled = false;
                    }
                }
            }
            ScriptManager.RegisterStartupScript(this.Page, GetType(), "sort", "SetCellMethod('#sortdata');", true);
            rList.DataBind();
        }
        protected void FaqDelete(object sender, EventArgs e)
        {
            int id;
            if ((rbl_Edit.SelectedValue == "0" && int.TryParse(ddl_Sequence1.SelectedValue, out id)) || (rbl_Edit.SelectedValue == "1" && int.TryParse(ddl_Sequence2.SelectedValue, out id)))
            {
                pp.FaqDelete(id);
                faqs = pp.FaqCollectionGet();
                BindAllDDL(ddl_Sequence1,true);
                CancelEdit(sender, e);
            }
        }
        protected void FaqEdit(object sender, EventArgs e)
        {
            plbl_Title.Visible = plbl_Type.Visible = true;
            prbl_Align.Visible = false;
            Button btn = ((Button)sender);
            if (btn == btn_Edit)
            {
                pan_Edit.Visible = true;
                plbl_Type.Text = "編輯  ";
                hid_type.Value = "edit";
                GetID(true, true);
            }
            else if (btn == btn_Sort)
            {
                pan_Sort.Visible = true;
                plbl_Type.Text = "排序  ";
                ScriptManager.RegisterStartupScript(this.Page, GetType(), "sort", "SetCellMethod('#sortdata');", true);
            }
            else
            {
                plbl_Type.Text = "新增  ";
                hid_type.Value = "add";
                GetID(false, false);
                tbx_editor.Content = string.Empty;
                pan_Edit.Visible = plbl_Title.Visible = true;

                if (rbl_Edit.SelectedValue == "1")
                {
                    plbl_Title.Text = "問題:";
                    int id;
                    if (!string.IsNullOrEmpty(ddl_Sequence1.SelectedValue) && int.TryParse(ddl_Sequence1.SelectedValue, out id))
                    {
                        plbl_Title.Text += ddl_Sequence1.SelectedItem.Text;
                    }
                }

                if (rbl_Edit.SelectedValue == "0")
                {
                    if (Department.Equals(DepartmentTypes.Ppon))
                    {
                        prbl_Align.Visible = true;
                        prbl_Align.SelectedIndex = 0;
                    }
                }
            }
        }
        protected void GetID(bool edit, bool getcontent)
        {
            if (rbl_Edit.SelectedValue == "0")
                hid_Id.Value = edit ? ddl_Sequence1.SelectedValue : "0";
            else if (rbl_Edit.SelectedValue == "1")
                hid_Id.Value = edit ? ddl_Sequence2.SelectedValue : ddl_Sequence1.SelectedValue;
            else if (rbl_Edit.SelectedValue == "2")
                hid_Id.Value = edit ? ddl_Sequence3.SelectedValue : ddl_Sequence2.SelectedValue;
            else
                hid_Id.Value = string.Empty;
            int id;
            if (int.TryParse(hid_Id.Value, out id) && getcontent)
            {
                Faq faq = pp.FaqGet(id);
                if (faq.Pid == 0)
                {
                    if (Department.Equals(DepartmentTypes.Ppon))
                    {
                        prbl_Align.Visible = true;
                        prbl_Align.SelectedValue = faq.Mark;
                    }
                }
                tbx_editor.Content = faq.Contents;
            }
        }

        protected void ConfirmEdit(object sender, EventArgs e)
        {
            int id, stage;
            bool check = false;
            GetID(hid_type.Value == "edit", false);
            if (hid_type.Value == "add" && int.TryParse(hid_Id.Value, out id) && int.TryParse(rbl_Edit.SelectedValue, out stage))
            {
                Faq faq = new Faq();
                faq.Contents = tbx_editor.Content;
                faq.Sequence = 1;
                faq.Stage = stage;
                faq.CreateTime = DateTime.Now;
                faq.Creator = User.Identity.Name;
                faq.Status = true;
                faq.Pid = id;
                if (id == 0)
                {
                    faq.Mark = Department.Equals(DepartmentTypes.Ppon) ? prbl_Align.SelectedValue : "H";
                    check = true;
                }
                pp.FaqSet(faq);

            }
            else if (int.TryParse(hid_Id.Value, out id))
            {
                Faq faq = pp.FaqGet(id);
                faq.Contents = tbx_editor.Content;
                if (faq.Pid == 0)
                {
                    faq.Mark = Department.Equals(DepartmentTypes.Ppon) ? prbl_Align.SelectedValue : "H";
                    check = true;
                }
                pp.FaqSet(faq);
            }
            faqs = pp.FaqCollectionGet();
            if (check)
                BindDDL(ddl_Sequence1, GetParentFaqListItem(faqs));
            BindControl(ddl_Sequence1, e);
            CancelEdit(sender, e);
        }
        protected void CancelEdit(object sender, EventArgs e)
        {
            tbx_editor.Content = string.Empty;
            pan_Edit.Visible = pan_Sort.Visible = plbl_Title.Visible = plbl_Type.Visible = false;
        }


        protected void Department_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindAllDDL(ddl_Sequence1, true);
        }

        private IEnumerable<ListItem> GetParentFaqListItem(FaqCollection faqs)
        {
            return faqs.Where(x => x.Stage == 0 &&
                        ((Department.Equals(DepartmentTypes.HiDeal) && x.Mark.Equals("H", StringComparison.CurrentCultureIgnoreCase)) || (Department.Equals(DepartmentTypes.Ppon) && !x.Mark.Equals("H", StringComparison.CurrentCultureIgnoreCase)))
                        ).OrderBy(x => x.Sequence).Select(x => new ListItem(x.Contents.TrimToMaxLength(20, "..."), x.Id.ToString()));
        }

        private IEnumerable<Faq> GetParentFaqList(FaqCollection faqs)
        {
            return faqs.Where(x => x.Stage == 0 &&
                        ((Department.Equals(DepartmentTypes.HiDeal) && x.Mark.Equals("H", StringComparison.CurrentCultureIgnoreCase)) || (Department.Equals(DepartmentTypes.Ppon) && !x.Mark.Equals("H", StringComparison.CurrentCultureIgnoreCase)))
                        ).OrderBy(x => x.Sequence);
        }
        [WebMethod]
        public static string SetSlotData(string data)
        {
            try
            {
                string[] datalist = data.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                int newSeq = 1;
                foreach (string item in datalist)
                {
                    int id;
                    if (int.TryParse(item, out id))
                    {
                        ProviderFactory.Instance().GetProvider<IPponProvider>().FaqSort(id, newSeq);
                        newSeq++;
                    }
                }
            }
            catch
            {
                return "發生錯誤!請洽IT人員!";
            }
            return string.Empty;
        }
    }
}
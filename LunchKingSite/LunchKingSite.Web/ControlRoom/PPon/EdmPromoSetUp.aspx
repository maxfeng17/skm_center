﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ControlRoom/backend.master" AutoEventWireup="true"
    CodeBehind="EdmPromoSetUp.aspx.cs" Inherits="LunchKingSite.Web.ControlRoom.Ppon.EdmPromoSetUp" %>

<%@ Import Namespace="LunchKingSite.DataOrm" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="../Controls/EdmDetailsSetup.ascx" TagName="EdmDetailsSetup" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <script src="../../Tools/js/jquery-1.7.2.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        function isupload(upload) {
            if (upload == '') {
                return true;
            }
            else {
                return confirm('此EDM已於' + upload + "上傳，請確定是否要重複上傳發送?");
            }
        }
    </script>
    <style type="text/css">
        .bgOrange {
            background-color: #FFC690;
            text-align: center;
        }

        .bgBlue {
            background-color: #B3D5F0;
        }

        .bgLBlue {
            background-color: #B2DFEE;
        }

        .bgLOrange {
            background-color: #FFEC8B;
            font-size: smaller;
        }

        .title {
            text-align: center;
            background-color: #688E45;
            font-weight: bolder;
            color: White;
        }

        .title1 {
            text-align: center;
            background-color: #8B8970;
            font-weight: bolder;
            color: White;
        }

        .title2 {
            text-align: center;
            background-color: #778899;
            font-weight: bolder;
            color: White;
        }

        .header {
            text-align: center;
            font-size: large;
            font-weight: bolder;
            background-color: #F0AF13;
            color: Blue;
        }

        .fieldset {
            border: #688E45 solid 2px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:HiddenField ID="hif_Id" runat="server" />
    <asp:Panel ID="pan_Main" runat="server">
        <fieldset class="fieldset">
            <legend class="title">非訂閱信編輯</legend>
            <table style="background-color: gray; width: 950px">
                <tr>
                    <td class="bgOrange" style="width: 15%">地區選擇
                    </td>
                    <td class="bgBlue" style="width: 25%">
                        <asp:DropDownList ID="ddl_Main_City" runat="server">
                        </asp:DropDownList>
                    </td>
                    <td class="bgOrange" style="width: 15%">發送日期
                    </td>
                    <td class="bgBlue" style="width: 45%">
                        <asp:TextBox ID="tbx_Main_DeliveryDate_Start" runat="server"></asp:TextBox>
                        <cc1:CalendarExtender ID="ce_DeliveryDateS" TargetControlID="tbx_Main_DeliveryDate_start"
                            runat="server">
                        </cc1:CalendarExtender>
                        <asp:RequiredFieldValidator ID="rfv_DeliveryDateS" runat="server" ErrorMessage="*"
                            ValidationGroup="A" ControlToValidate="tbx_Main_DeliveryDate_start" Display="Dynamic"
                            ForeColor="Red"></asp:RequiredFieldValidator>~
                        <asp:TextBox ID="tbx_Main_DeliveryDate_End" runat="server"></asp:TextBox>
                        <cc1:CalendarExtender ID="ce_DeliveryDateE" TargetControlID="tbx_Main_DeliveryDate_End"
                            runat="server">
                        </cc1:CalendarExtender>
                        <asp:RequiredFieldValidator ID="rfv_DeliveryDateE" runat="server" ErrorMessage="*"
                            ValidationGroup="A" ControlToValidate="tbx_Main_DeliveryDate_End" Display="Dynamic"
                            ForeColor="Red"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td colspan="4" class="title2">
                        <asp:Button ID="btn_SetDefaultEdm" runat="server" Text="搜尋" OnClick="SearchEdmContent"
                            ValidationGroup="A" />&nbsp;&nbsp;
                        <asp:Button ID="btn_AddNewEdm" runat="server" Text="新增" OnClick="AddNewEdm" ForeColor="Green" />
                    </td>
                </tr>
                <tr>
                    <td colspan="4" class="title2">
                        <asp:Label ID="lab_Message" runat="server"></asp:Label>
                    </td>
                </tr>
            </table>
        </fieldset>
    </asp:Panel>
    <asp:Panel ID="pan_Search" runat="server" Visible="false">
        <table style="width: 950px">
            <tr>
                <td class="bgOrange" style="width: 15%">測試發送Email:<br />
                    逗號或斷行區隔
                </td>
                <td class="bgBlue">
                    <asp:TextBox ID="tbx_TestEmail" runat="server" TextMode="MultiLine" Rows="2" Width="700"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="bgOrange" style="width: 15%">是否壓縮
                </td>
                <td class="bgBlue">
                    <asp:CheckBox ID="cbx_Compress" runat="server" Checked="true" />
                </td>
            </tr>
        </table>
        <asp:GridView ID="gv_List" runat="server" AutoGenerateColumns="false" OnRowCommand="gvRowCommand">
            <Columns>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <table style="width: 950px">
                            <tr>
                                <td class="bgOrange">編輯
                                </td>
                                <td class="bgOrange">城市
                                </td>
                                <td class="bgOrange">主旨
                                </td>
                                <td class="bgOrange">發送時間
                                </td>
                                <td class="bgOrange">產生Html
                                </td>
                                <td class="bgOrange">CelloPoint發送
                                </td>
                            </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td class="bgLOrange" style="text-align: center">
                                <asp:Button ID="btn_Edit" runat="server" Text="編輯" CommandArgument="<%# ((EdmMain)(Container.DataItem)).Id %>"
                                    CommandName="EditItem" /><br />
                                <asp:Button ID="btn_Delete" runat="server" Text="刪除" ForeColor="Red" CommandArgument="<%# ((EdmMain)(Container.DataItem)).Id %>"
                                    CommandName="DeleteItem" />
                            </td>
                            <td class="bgLOrange">
                                <%# ((EdmMain)(Container.DataItem)).CityName %>
                            </td>
                            <td class="bgLOrange">
                                <%# ((EdmMain)(Container.DataItem)).Subject.TrimToMaxLength(20,"...") %>
                            </td>
                            <td class="bgLOrange">
                                <asp:Label ID="lab_DeliveryDate" runat="server" Text='<%# ((EdmMain)(Container.DataItem)).DeliveryDate.ToString("yyyy/MM/dd HH:mm") %>'></asp:Label>
                            </td>
                            <td class="bgLOrange">
                                <asp:Button ID="btn_Cellopoint" runat="server" Text="CelloPoint" CommandArgument="<%# ((EdmMain)(Container.DataItem)).Id %>"
                                    CommandName="DownLoadCelloPoint" />
                                <asp:Button ID="btn_Ses" runat="server" Text="SES" CommandArgument="<%# ((EdmMain)(Container.DataItem)).Id %>"
                                    CommandName="DownLoadSES" />
                                <asp:Button ID="btn_Ezmail" runat="server" Text="EZmail" CommandArgument="<%# ((EdmMain)(Container.DataItem)).Id %>"
                                    CommandName="DownLoadEZmail" />
                                <br />
                                <asp:Button ID="btn_PEZ" runat="server" Text="PEZ" CommandArgument="<%# ((EdmMain)(Container.DataItem)).Id %>"
                                    CommandName="DownLoadPEZ" />
                                <asp:Button ID="btn_17Life" runat="server" Text="17Life" CommandArgument="<%# ((EdmMain)(Container.DataItem)).Id %>"
                                    CommandName="DownLoad17Life" />
                            </td>
                            <td class="bgLOrange">
                                <asp:Button ID="btn_TestSend" runat="server" Text="測試發送" CommandArgument="<%# ((EdmMain)(Container.DataItem)).Id %>"
                                    CommandName="TestSendCelloPoint" ForeColor="Green" /><br />
                                <asp:Button ID="btn_Send" runat="server" Text="上傳" CommandArgument="<%# ((EdmMain)(Container.DataItem)).Id %>"
                                    CommandName="SendCelloPoint" OnClientClick='<%# "return(isupload(\""+ (((EdmMain)(Container.DataItem)).UploadDate.HasValue?((EdmMain)(Container.DataItem)).UploadDate.Value.ToString("MM/dd HH:mm"):string.Empty)+"\"));" %>'
                                    ForeColor="Blue" />
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </asp:Panel>
    <asp:Panel ID="pan_Details" runat="server" Visible="false">
        <br />
        <div class="title">
            非訂閱信&nbsp;&nbsp;
            <asp:Label ID="lab_ID" runat="server"></asp:Label>&nbsp;&nbsp;&nbsp;
            <asp:HyperLink ID="hyp_ID" runat="server" Target="_blank">線上預覽</asp:HyperLink>&nbsp;
            <asp:HyperLink ID="hyp_PEZ" runat="server" Target="_blank">大團購預覽PEZ</asp:HyperLink>&nbsp;
            <asp:HyperLink ID="hyp_17Life" runat="server" Target="_blank">大團購預覽17Life</asp:HyperLink>
            <br />
            <asp:Button ID="btn_Save2" runat="server" Text="儲存" OnClick="SaveEdm" ValidationGroup="B" />&nbsp;&nbsp;
            <asp:Button ID="btn_Cancel2" runat="server" Text="取消/返回" ForeColor="Blue" OnClick="CancelAction" />&nbsp;&nbsp;
            <asp:Button ID="btn_Delete2" runat="server" Text="刪除" ForeColor="Red" OnClientClick="return(confirm('確定刪除??'));"
                OnClick="DeleteEdmSetting" Visible="false" />
        </div>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <script type="text/javascript">
                    function chineseCount(word, d) {
                        v = 0
                        for (cc = 0; cc < word.length; cc++) {
                            c = word.charCodeAt(cc);
                            if (!(c >= 32 && c <= 126)) v++;
                        }
                        total = Math.ceil((word.length - v) / 2);
                        $(d).text(v + total);
                    }
                    Sys.Application.add_load(WireEvent);
                    function WireEvent() {
                        $('#<%=tbx_Subject.ClientID %>').keyup(function (event) {
                            chineseCount($(this).val(), $('#div<%=tbx_Subject.ClientID %>'));
                        });
                    }
                </script>
                <fieldset class="fieldset" style="margin-top: 20px">
                    <legend class="title">Subject</legend>
                    <table style="background-color: gray; width: 950px">
                        <tr>
                            <td class="bgLOrange" style="width: 100px">郵件主旨 :<br />
                                頁面字數限制:40
                            </td>
                            <td class="bgLBlue" colspan="3">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="tbx_Subject" runat="server" TextMode="MultiLine" Rows="2" Width="750"></asp:TextBox>
                                        </td>
                                        <td>
                                            <div id='<%="div"+tbx_Subject.ClientID %>'>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                                <asp:RequiredFieldValidator ID="rfv_Subject" runat="server" ErrorMessage="*" ValidationGroup="B"
                                    ControlToValidate="tbx_Subject" Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="bgLOrange">發送時間
                            </td>
                            <td class="bgLBlue">
                                <asp:TextBox ID="tbx_DeliveryDate" runat="server"></asp:TextBox>
                                <cc1:CalendarExtender ID="ce_DeliveryDate" TargetControlID="tbx_DeliveryDate" runat="server">
                                </cc1:CalendarExtender>
                                <asp:RequiredFieldValidator ID="rfv_DeliveryDate" runat="server" ErrorMessage="*"
                                    ValidationGroup="B" ControlToValidate="tbx_DeliveryDate" Display="Dynamic" ForeColor="Red"></asp:RequiredFieldValidator>
                                <asp:DropDownList ID="ddl_DeliveryHour" runat="server" Width="50">
                                </asp:DropDownList>
                                &nbsp;:&nbsp;
                                <asp:DropDownList ID="ddl_DeliveryMinute" runat="server" Width="50">
                                </asp:DropDownList>
                            </td>
                            <td class="bgLOrange" style="width: 100px">CPA
                            </td>
                            <td class="bgLBlue">
                                <asp:TextBox ID="tbx_Cpa" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="bgLOrange">檔次時間
                            </td>
                            <td class="bgLBlue" colspan="3">
                                <asp:TextBox ID="tbx_DealDate" runat="server"></asp:TextBox>
                                <cc1:CalendarExtender ID="ce_DealDate" TargetControlID="tbx_DealDate" runat="server">
                                </cc1:CalendarExtender>
                                <asp:Button ID="btn_DealDate" runat="server" Text="變更日期" OnClick="ChangeDealDate"
                                    Enabled="false" />
                            </td>
                    </table>
                </fieldset>
                <fieldset class="fieldset" style="margin-top: 20px">
                    <legend class="title">AD</legend>
                    <table style="background-color: gray; width: 950px">
                        <tr>
                            <td class="bgLOrange" style="width: 100px">AD :
                            </td>
                            <td class="bgLBlue">
                                <asp:DropDownList ID="ddl_AD" runat="server" OnSelectedIndexChanged="ChangeAD" AutoPostBack="true"
                                    Width="150">
                                </asp:DropDownList>
                                <asp:DropDownList ID="ddl_AD_Content" runat="server" Visible="false">
                                </asp:DropDownList>
                                &nbsp;&nbsp;<asp:CheckBox ID="cbx_AD" runat="server" Text="是否加入AD" Checked="true" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" class="bgLBlue">
                                <asp:Literal ID="lit_AD" runat="server"></asp:Literal>
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </ContentTemplate>
        </asp:UpdatePanel>
        <uc1:EdmDetailsSetup ID="edmuc_main_1" runat="server" />
        <uc1:EdmDetailsSetup ID="edmuc_main_2" runat="server" />
        <uc1:EdmDetailsSetup ID="edmuc_Area_1" runat="server" />
        <uc1:EdmDetailsSetup ID="edmuc_Area_2" runat="server" />
        <uc1:EdmDetailsSetup ID="edmuc_Area_3" runat="server" />

        <uc1:EdmDetailsSetup ID="edmuc_piinlife_1" runat="server" Visible="false" />
        <uc1:EdmDetailsSetup ID="edmuc_piinlife_2" runat="server" Visible="false" />
        <uc1:EdmDetailsSetup ID="edmuc_PponPiinLife_1" runat="server" />
        <fieldset class="fieldset" style="margin-top: 20px">
            <legend class="title">PEZ</legend>
            <table style="background-color: gray; width: 950px">
                <tr>
                    <td class="bgOrange" style="width: 15%">CPA
                    </td>
                    <td class="bgBlue">
                        <asp:CheckBox ID="cbx_PEZ" runat="server" Text="是否加入PEZ每日一物" Checked="false" />
                        <asp:RadioButtonList ID="rbl_Pez_Cpa" runat="server" RepeatDirection="Horizontal"
                            Visible="false">
                            <asp:ListItem Text="Ezmail" Value="Ezmail"></asp:ListItem>
                            <asp:ListItem Text="Cellopoint" Value="Cellopoint"></asp:ListItem>
                            <asp:ListItem Text="abc" Value="Ezmail"></asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr>
                    <td class="bgLBlue" colspan="2">
                        <asp:Literal ID="lit_PEZ" runat="server"></asp:Literal>
                    </td>
                </tr>
            </table>
        </fieldset>
        <div class="title">
            <asp:Button ID="btn_Save" runat="server" Text="儲存" OnClick="SaveEdm" ValidationGroup="B" />&nbsp;&nbsp;
            <asp:Button ID="btn_Cancel" runat="server" Text="取消/返回" ForeColor="Blue" OnClick="CancelAction" />&nbsp;&nbsp;
            <asp:Button ID="btn_Delete" runat="server" Text="刪除" ForeColor="Red" OnClientClick="return(confirm('確定刪除??'));"
                OnClick="DeleteEdmSetting" Visible="false" />
        </div>
    </asp:Panel>
</asp:Content>

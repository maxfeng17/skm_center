﻿<%@ Page Language="C#" MasterPageFile="~/ControlRoom/backend.master" AutoEventWireup="true" CodeBehind="PponAccounting.aspx.cs" Inherits="LunchKingSite.Web.ControlRoom.Ppon.PponAccounting" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <p>
        <label>業務：</label><asp:TextBox ID="tbSales" runat="server"></asp:TextBox>&nbsp;&nbsp;
        <label>抽成：</label><asp:TextBox ID="tbCommission" runat="server"></asp:TextBox>&nbsp;&nbsp;
        <label>額外獎金：</label><asp:TextBox ID="tbBonus" runat="server"></asp:TextBox>
    </p>
    <p>
        <label>請款日期：</label><asp:TextBox ID="tbChargingDate" runat="server"></asp:TextBox>
        <cc1:CalendarExtender ID="ceChargingDate" TargetControlID="tbChargingDate" runat="server"></cc1:CalendarExtender>&nbsp;&nbsp;
        <label>入賬日期：</label><asp:TextBox ID="tbEnterDate" runat="server"></asp:TextBox>
        <cc1:CalendarExtender ID="ceEnterDate" TargetControlID="tbEnterDate" runat="server"></cc1:CalendarExtender>&nbsp;&nbsp;
        <label>收到發票日期：</label><asp:TextBox ID="tbInvoiceDate" runat="server"></asp:TextBox>
        <cc1:CalendarExtender ID="ceInvoiceDate" TargetControlID="tbInvoiceDate" runat="server"></cc1:CalendarExtender>
    </p>
    <p>
        <label>應付帳款公式：</label><asp:DropDownList ID="ddlAPFormula" runat="server"/>&nbsp;&nbsp;
        <asp:PlaceHolder ID="p2" runat="server">
            <label>初次進貨價：</label><asp:TextBox ID="tbCost" runat="server"></asp:TextBox>
        </asp:PlaceHolder>
    </p>
    <p>
        <asp:Button ID="btnUpdate" OnClick="btnUpdate_Click" runat="server" Text="更新"></asp:Button>
        <asp:Button ID="btnMultiCost" OnClick="btnMultiCost_Click" runat="server" Text="多重進貨價"></asp:Button>
    </p><br/>

    <asp:PlaceHolder ID="pCost" runat="server" Visible="false">
    <p>
        <label>進貨價：</label><asp:TextBox ID="tbMultiCost" runat="server"></asp:TextBox>&nbsp;&nbsp;
        <label>份數：</label><asp:TextBox ID="tbQuantity" runat="server"></asp:TextBox>&nbsp;&nbsp;
        <asp:Button ID="btnCreateCost" OnClick="btnCreateCost_Click" runat="server" Text="新增進貨價"></asp:Button>
        <asp:Button ID="btnDeleteCost" OnClick="btnDeleteCost_Click" runat="server" Text="刪除進貨價"></asp:Button>
    </p>
    <asp:GridView ID="gCost" runat="server" AutoGenerateColumns="False" DataKeyNames="BusinessHourGuid" EnableModelValidation="True">
        <Columns>
            <asp:TemplateField HeaderText="序號">
                <ItemTemplate>
                    <asp:Label ID="tbId" runat="server" Text='<%# Bind("Id") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="進貨價">
                <EditItemTemplate>
                    <asp:TextBox ID="tbCost" runat="server" Text='<%# Bind("Cost") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label2" runat="server" Text='<%# Bind("Cost") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="份數">
                <EditItemTemplate>
                    <asp:TextBox ID="tbQuantity" runat="server" Text='<%# Bind("Quantity") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label3" runat="server" Text='<%# Bind("Quantity") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="CumulativeQuantity" HeaderText="累積份數" />
        </Columns>
    </asp:GridView>
    </asp:PlaceHolder>
</asp:Content>
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ControlRoom/backend.master" AutoEventWireup="true"
    CodeBehind="YahooBigSetup.aspx.cs" Inherits="LunchKingSite.Web.ControlRoom.Ppon.YahooBigSetup" %>

<%@ Import Namespace="LunchKingSite.DataOrm" %>
<%@ Import Namespace="LunchKingSite.BizLogic.Facade" %>
<%@ Import Namespace="LunchKingSite.Core" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/Ppon/Pager.ascx" TagName="Pager" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="SSC" runat="server">
    <link type="text/css" href="<%=ResolveUrl("~/Tools/js/css/ui-lightness/jquery-ui-1.8rc3.custom.css") %>" rel="stylesheet" />
    <script type="text/javascript" src="../../Tools/js/jquery.maxlength-min.js"></script>
    <script type="text/javascript" src="../../Tools/js/jquery-ui.1.8.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#<%= txtTitle.ClientID %>').maxlength({
                maxCharacters: 20,
            });

            $('#<%= txtDealDesc.ClientID %>').maxlength({
                maxCharacters: 90,
            });

            $("#sortableN").sortable().disableSelection();
            $("#sortableC").sortable().disableSelection();
            $("#sortableS").sortable().disableSelection();

            <%------------%>
            $("#ctl00_ContentPlaceHolder1_rdlCategory_1").hide().next('label').hide();
            $("#ctl00_ContentPlaceHolder1_rdlCategory_2").hide().next('label').hide();
            <%------------%>

            $("#chkSellecAll").on("click",function () {
                $("input:checkbox[name*='chkVerification']:visible").prop('checked', $(this).prop("checked"));
            });
        });

        function GetSortableList() {

            var indexN = 0;
            var toPostN = {};
            $('#sortableN').find('li').each(function () {
                indexN++;
                $(this).find('input').val(indexN);
                toPostN[$(this).find('input').attr('name')] = indexN;
            });
            $('#<%= hidSortN.ClientID %>').val(JSON.stringify(toPostN));

            var indexC = 0;
            var toPostC = {};
            $('#sortableC').find('li').each(function () {
                indexC++;
                $(this).find('input').val(indexC);
                toPostC[$(this).find('input').attr('name')] = indexC;
            });
            $('#<%= hidSortC.ClientID %>').val(JSON.stringify(toPostC));

            var indexS = 0;
            var toPostS = {};
            $('#sortableS').find('li').each(function () {
                indexS++;
                $(this).find('input').val(indexS);
                toPostS[$(this).find('input').attr('name')] = indexS;
            });
            $('#<%= hidSortS.ClientID %>').val(JSON.stringify(toPostS));
        }

        function SetSortableValue(sortableId, hidValueId) {
            $(sortableId).sortable({
                update: function (e, ui) {
                    var ul = $(ui.item).closest('ul'); //todo
                    var index = 0;
                    var toPost = {};
                    ul.find('li').each(function () {
                        index++;
                        $(this).find('input').val(index);
                        toPost[$(this).find('input').attr('name')] = index;
                    });
                    $('#' + hidValueId).val(JSON.stringify(toPost));
                }
            }).disableSelection();
        }

        function SaveCheck() {
            if (CheckArea()) {
                CheckAction();
            } else {
                return false;
            }
        }

        function CheckAction() {
            if ($('#<%= chkIsMarkDelete.ClientID %>').is(':checked')) {
                if ($('#<%= lblAction.ClientID %>').text() == '新增') {
                    return confirm('此資料尚未更新至Yahoo，儲存將直接刪除，確定?');
                } else {
                    return confirm('確定註記此筆資料為刪除?');
                }
            } else {
                return true;
            }
        }

        function CheckArea() {
            
            if ($('input[id*=cklArea]:checked').val() == undefined ) {
                alert('請至少勾選任一區域!!');
                return false;
            } else {
                return true;
            }
        }

        function AlertImportErrorMsg(errorMsg) {
            $("<div id='alertDialog'>").append(errorMsg).dialog({
                modal: true,
                title: "匯入失敗",
                minWidth: 800,
                buttons: [{
                    text: "確定",
                    click: function () {
                        $(this).dialog("close");
                    }
                }],
                close: function () {
                    $(this).remove();
                }
            }).dialog("open");
        }
    </script>
    <style type="text/css">
        .status {
            font-size: small;
            color: gray;
        }

        #sortableN, #sortableC, #sortableS {
            list-style-type: none;
            margin: 0;
            padding-left: 0;
            padding-right: 1em;
        }

            #sortableN li, #sortableC li, #sortableS li {
                margin: 0 3px 3px 3px;
                padding-bottom: 1em;
                font-size: 1.4em;
            }

        .ui-dialog-buttonset {
            text-align: center;
        }
            .ui-dialog .ui-dialog-buttonpane button {
                float: none;
                padding: 0;
            }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Panel ID="divSearch" runat="server" DefaultButton="btnSearch">
        <fieldset style="width: 1000px;">
            <legend>Yahoo大團購</legend>
            <table>
                <tr>
                    <td>查詢項目</td>
                    <td>
                        <asp:RadioButtonList ID="rdlType" runat="server" RepeatLayout="Flow" RepeatDirection="Horizontal">
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr style="display: none;">
                    <td>雅虎狀態類型</td>
                    <td>
                        <asp:CheckBoxList ID="cklYahooAction" runat="server" RepeatLayout="Flow" RepeatDirection="Horizontal">
                        </asp:CheckBoxList>
                    </td>
                </tr>
                <tr>
                    <td>檔次id</td>
                    <td>
                        <asp:TextBox ID="txtDealId" runat="server" Width="300px"></asp:TextBox><span style="color: gray; font-size: small">(P好康輸入Bid, 品生活輸入Did)</span>
                    </td>
                </tr>
                <tr>
                    <td>檔次內容</td>
                    <td>
                        <asp:TextBox ID="txtDealName" runat="server" Width="300px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>上檔日期</td>
                    <td>
                        <asp:TextBox ID="txtDealStartDate" runat="server" Columns="8" />
                        <cc1:CalendarExtender ID="ceDealStartDate" TargetControlID="txtDealStartDate" runat="server"
                            Format="yyyy/MM/dd">
                        </cc1:CalendarExtender>
                        ～
                    <asp:TextBox ID="txtDealEndDate" runat="server" Columns="8" />
                        <cc1:CalendarExtender ID="ceDealEndDate" TargetControlID="txtDealEndDate" runat="server"
                            Format="yyyy/MM/dd">
                        </cc1:CalendarExtender>
                    </td>
                </tr>
            </table>
            <asp:Button ID="btnSearch" runat="server" Text="查詢" OnClick="btnSearch_Click" />
            <asp:Button ID="btnSort" runat="server" Text="今日排序設定" OnClick="btnSort_Click" />
            <asp:Button ID="btnUpdateAllAction" runat="server" Text="更新全部狀態" OnClick="btnUpdateAllAction_Click" OnClientClick="return confirm('確定要義無反顧地更新全部狀態嗎???')" Visible="false" />
            <br />
            <asp:FileUpload ID="FileUpload1" runat="server" />
            <asp:Button ID="btnImport" runat="server" Text="匯入bid" OnClick="btnImport_Click" />
        </fieldset>
    </asp:Panel>
    <asp:Label ID="lblErrorMsg" runat="server" ForeColor="Red" Visible="false"></asp:Label>
    <asp:Panel ID="divYahooPropertyEdit" runat="server" Visible="false">
        <fieldset style="width: 1000px;">
            <legend>Yahoo資料設定</legend>
            <table>
                <tr>
                    <td colspan="4">No：
                        <asp:Label ID="lblNo" runat="server"></asp:Label>&nbsp;&nbsp;&nbsp;
                        類別：
                        <asp:HiddenField ID="hidType" runat="server" />
                        <asp:Label ID="lblType" runat="server"></asp:Label>&nbsp;&nbsp;&nbsp;
                        Action：
                        <asp:Label ID="lblAction" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>ID：</td>
                    <td colspan="3">
                        <asp:Label ID="lblDealId" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>圖片：</td>
                    <td colspan="3">
                        <asp:FileUpload ID="upImg" runat="server" />
                        <asp:CheckBox ID="chkIsPrintDiscountMark" runat="server" Text="壓折數" Checked="true" />
                        <asp:CheckBox ID="chkIsPrintLogoWaterMark" runat="server" Text="壓Logo" Checked="true" />
                        <asp:CheckBox ID="chkIsCutCenter" runat="server" Text="置中裁剪" /><br />
                        <asp:Image ID="img" runat="server" /></td>
                </tr>
                <tr>
                    <td>分類：</td>
                    <td colspan="3">
                        <asp:RadioButtonList ID="rdlCategory" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow"></asp:RadioButtonList></td>
                </tr>
                <tr>
                    <td>檔次標題：</td>
                    <td colspan="3">
                        <asp:TextBox ID="txtTitle" runat="server" Width="600px"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>檔次說明：</td>
                    <td colspan="3">
                        <asp:TextBox ID="txtDealDesc" runat="server" Width="600px"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>連結：</td>
                    <td colspan="3">
                        <asp:HyperLink ID="hlLink" runat="server" Target="_blank"></asp:HyperLink>
                        &nbsp;&nbsp;<asp:HyperLink ID="lkSetupLink" runat="server" Target="_blank">後台設定</asp:HyperLink>
                    </td>
                </tr>
                <tr style="display: none;">
                    <td>北區設定：</td>
                    <td colspan="3">
                        <asp:RadioButtonList ID="rdlAreaN" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow"></asp:RadioButtonList>
                    </td>
                </tr>
                <tr style="display: none;">
                    <td>中區設定：</td>
                    <td colspan="3">
                        <asp:RadioButtonList ID="rdlAreaC" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow"></asp:RadioButtonList>
                    </td>
                </tr>
                <tr style="display: none;">
                    <td>南區設定：</td>
                    <td colspan="3">
                        <asp:RadioButtonList ID="rdlAreaS" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow"></asp:RadioButtonList>
                    </td>
                </tr>
                <tr>
                    <td>區域：</td>
                    <td colspan="3">
                        <asp:CheckBoxList runat="server" ID="cklArea" RepeatDirection="Horizontal" RepeatLayout="Flow" RepeatColumns="10"></asp:CheckBoxList>
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <asp:CheckBox ID="chkIsMarkDelete" runat="server" Text="註記為刪除" ForeColor="Red" />
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <asp:Button ID="btnSave" OnClick="btnSave_Click" runat="server" Text="儲存" OnClientClick="return SaveCheck();" />
                        <asp:Button ID="btnClose" OnClick="btnClose_Click" runat="server" Text="關閉" />
                    </td>
                </tr>
            </table>
        </fieldset>
    </asp:Panel>
    <asp:Panel ID="divGrid" runat="server">
        <asp:Panel ID="divPponDeal" runat="server" Visible="false">
            <uc1:Pager ID="ucPponPager" runat="server" PageSize="300" ongetcount="GetPponCount" onupdate="PponUpdateHandler"></uc1:Pager>
            <asp:GridView ID="gvPponDeal" runat="server" OnRowDataBound="gvPponDeal_RowDataBound" OnRowCommand="gvPponDeal_RowCommand"
                GridLines="Horizontal" ForeColor="Black" CellPadding="4" BorderWidth="1px" BorderStyle="None"
                BorderColor="#DEDFDE" BackColor="White" EmptyDataText="無符合的資料" AutoGenerateColumns="false"
                Font-Size="Small" Width="1050px">
                <FooterStyle BackColor="#CCCC99"></FooterStyle>
                <RowStyle BackColor="#F7F7DE" Height="20px" HorizontalAlign="Center"></RowStyle>
                <SelectedRowStyle BackColor="#CE5D5A" ForeColor="White" Font-Bold="True"></SelectedRowStyle>
                <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Center"></PagerStyle>
                <HeaderStyle BackColor="#6B696B" ForeColor="White" Font-Bold="True" Height="20px"
                    HorizontalAlign="Center"></HeaderStyle>
                <Columns>
                    <asp:TemplateField HeaderText="bid" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="10%">
                        <ItemTemplate>
                            <asp:Label ID="lblBusinessHourGuid" runat="server"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="上檔日期" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="10%">
                        <ItemTemplate>
                            <asp:Label ID="lblBusinessHourOrderTime" runat="server"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5%">
                        <ItemTemplate>
                            <asp:Image ID="dealImage" runat="server" Width="100px" /></td>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="商品內容" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="20%">
                        <ItemTemplate>
                            好康價:&nbsp;<asp:Label ID="lblItemPrice" runat="server"></asp:Label>&nbsp;&nbsp;
                            原價:&nbsp;<span style="text-decoration: line-through">
                                <asp:Label ID="lblItemOrigPrice" runat="server"></asp:Label></span><br />
                            <asp:LinkButton ID="lkDealEdit" runat="server" CommandName="UPD"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="類別" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="10%">
                        <ItemTemplate>
                            <asp:Label ID="lblCategorys" runat="server"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="上次更新" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="10%">
                        <ItemTemplate>
                            <asp:Label ID="lblRequestTime" runat="server"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField  ItemStyle-HorizontalAlign="Center" ItemStyle-Width="10%">
                        <HeaderTemplate>
                            Yahoo <br/> <input type="checkbox" id="chkSellecAll"/>全選 <asp:Button ID="btnCheckPayment" runat="server" Height="21px" Text="覆核選擇"  Width="70px" CommandName="CHKSELECTED"  onclientclick="return confirm('您確定要一次覆核全部紀錄嗎?');" />
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblYahooSetting" runat="server"></asp:Label><br />
                            <asp:Button ID="btnCheck" runat="server" Text="確認" CommandName="CHK" Visible="false" Enabled="false" />
                            <asp:CheckBox ID="chkVerification" runat="server"  Visible="false" />
                            <asp:HiddenField ID="hdYahooPropertyId" runat="server"></asp:HiddenField>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </asp:Panel>
        <br />
        <asp:Panel ID="divPiinlifeDeal" runat="server" Visible="false">
            <asp:GridView ID="gvPiinlifeDeal" runat="server" OnRowDataBound="gvPiinlifeDeal_RowDataBound" OnRowCommand="gvPiinlifeDeal_RowCommand"
                GridLines="Horizontal" ForeColor="Black" CellPadding="4" BorderWidth="1px" BorderStyle="None"
                BorderColor="#DEDFDE" BackColor="White" EmptyDataText="無符合的資料" AutoGenerateColumns="false"
                Font-Size="Small" Width="1029px">
                <FooterStyle BackColor="#CCCC99"></FooterStyle>
                <RowStyle BackColor="#F7F7DE" Height="20px" HorizontalAlign="Center"></RowStyle>
                <SelectedRowStyle BackColor="#CE5D5A" ForeColor="White" Font-Bold="True"></SelectedRowStyle>
                <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Center"></PagerStyle>
                <HeaderStyle BackColor="#6B696B" ForeColor="White" Font-Bold="True" Height="20px"
                    HorizontalAlign="Center"></HeaderStyle>
                <Columns>
                    <asp:TemplateField HeaderText="Pid" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="1%">
                        <ItemTemplate>
                            <asp:Label ID="lblProductId" runat="server"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="上檔日期" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="10%">
                        <ItemTemplate>
                            <asp:Label ID="lblDealStartDate" runat="server"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5%">
                        <ItemTemplate>
                            <asp:Image ID="dealImage" runat="server" Width="100px" /></td>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="商品內容" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="20%">
                        <ItemTemplate>
                            好康價:&nbsp;<asp:Label ID="lblItemPrice" runat="server"></asp:Label>&nbsp;&nbsp;
                            原價:&nbsp;<span style="text-decoration: line-through">
                                <asp:Label ID="lblItemOrigPrice" runat="server"></asp:Label></span><br />
                            <asp:LinkButton ID="lkDealEdit" runat="server" CommandName="UPD"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="上次更新" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="10%">
                        <ItemTemplate>
                            <asp:Label ID="lblRequestTime" runat="server"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Yahoo" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5%">
                        <ItemTemplate>
                            <asp:Label ID="lblYahooSetting" runat="server"></asp:Label><br />
                            <asp:Button ID="btnCheck" runat="server" Text="確認" CommandName="CHK" Visible="false" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </asp:Panel>
    </asp:Panel>
    <asp:Panel ID="divSort" runat="server" Visible="false">
        <asp:Button ID="btnSortSaveN" runat="server" OnClick="btnSortSave_Click" Text="儲存" OnClientClick="GetSortableList();" />
        <table>
            <tr>
                <td style="vertical-align: top;">
                    <asp:HiddenField ID="hidSortN" runat="server" />
                    <asp:Repeater ID="rptSortN" runat="server" OnItemCommand="rptSort_ItemCommand">
                        <HeaderTemplate>
                            北區:
                            <ul id="sortableN" class="connectedSortableN">
                        </HeaderTemplate>
                        <ItemTemplate>
                            <li style="list-style-type: none; white-space: nowrap">
                                <table border="1">
                                    <tr>
                                        <td style="font-size: small; color: blue;" align="center">
                                            <input id="idd" type="hidden" name="<%# ((YahooProperty)(Container.DataItem)).Id %>" />
                                            <%# Helper.GetLocalizedEnum((YahooPropertyType)((YahooProperty)(Container.DataItem)).Type) %><br />
                                            <asp:ImageButton ID="img" runat="server" CommandName="UPD" CommandArgument="<%# ((YahooProperty)(Container.DataItem)).Id %>" ImageUrl='<%# config.MediaBaseUrl + "/" + ((YahooProperty)(Container.DataItem)).ImgUrl %>' Width="100px" />
                                        </td>
                                        <td style="font-size: small; width: 200px; cursor: default">
                                            <%# ((YahooProperty)(Container.DataItem)).Title %><br />
                                            <span style="color: gray"><%# ((YahooProperty)(Container.DataItem)).DealDesc %></span>
                                        </td>
                                    </tr>
                                </table>
                            </li>
                        </ItemTemplate>
                        <FooterTemplate>
                            </ul>
                        </FooterTemplate>
                    </asp:Repeater>
                </td>
                <td style="vertical-align: top;">
                    <asp:HiddenField ID="hidSortC" runat="server" />
                    <asp:Repeater ID="rptSortC" runat="server" OnItemCommand="rptSort_ItemCommand">
                        <HeaderTemplate>
                            中區:
                            <ul id="sortableC" class="connectedSortableC">
                        </HeaderTemplate>
                        <ItemTemplate>
                            <li style="list-style-type: none; white-space: nowrap">
                                <table border="1">
                                    <tr>
                                        <td style="font-size: small; color: blue;" align="center">
                                            <input id="idd" type="hidden" name="<%# ((YahooProperty)(Container.DataItem)).Id %>" />
                                            <%# Helper.GetLocalizedEnum((YahooPropertyType)((YahooProperty)(Container.DataItem)).Type) %><br />
                                            <asp:ImageButton ID="img" runat="server" CommandName="UPD" CommandArgument="<%# ((YahooProperty)(Container.DataItem)).Id %>" ImageUrl='<%# config.MediaBaseUrl + "/" + ((YahooProperty)(Container.DataItem)).ImgUrl %>' Width="100px" />
                                        </td>
                                        <td style="font-size: small; width: 200px">
                                            <%# ((YahooProperty)(Container.DataItem)).Title %><br />
                                            <span style="color: gray"><%# ((YahooProperty)(Container.DataItem)).DealDesc %></span>
                                        </td>
                                    </tr>
                                </table>
                            </li>
                        </ItemTemplate>
                        <FooterTemplate>
                            </ul>
                        </FooterTemplate>
                    </asp:Repeater>
                </td>
                <td style="vertical-align: top;">
                    <asp:HiddenField ID="hidSortS" runat="server" />
                    <asp:Repeater ID="rptSortS" runat="server" OnItemCommand="rptSort_ItemCommand">
                        <HeaderTemplate>
                            南區:
                            <ul id="sortableS" class="connectedSortableS">
                        </HeaderTemplate>
                        <ItemTemplate>
                            <li style="list-style-type: none; white-space: nowrap">
                                <table border="1">
                                    <tr>
                                        <td style="font-size: small; color: blue;" align="center">
                                            <input id="idd" type="hidden" name="<%# ((YahooProperty)(Container.DataItem)).Id %>" />
                                            <%# Helper.GetLocalizedEnum((YahooPropertyType)((YahooProperty)(Container.DataItem)).Type) %><br />
                                            <asp:ImageButton ID="img" runat="server" CommandName="UPD" CommandArgument="<%# ((YahooProperty)(Container.DataItem)).Id %>" ImageUrl='<%# config.MediaBaseUrl + "/" + ((YahooProperty)(Container.DataItem)).ImgUrl %>' Width="100px" />
                                        </td>
                                        <td style="font-size: small; width: 200px">
                                            <%# ((YahooProperty)(Container.DataItem)).Title %><br />
                                            <span style="color: gray"><%# ((YahooProperty)(Container.DataItem)).DealDesc %></span>
                                        </td>
                                    </tr>
                                </table>
                            </li>
                        </ItemTemplate>
                        <FooterTemplate>
                            </ul>
                        </FooterTemplate>
                    </asp:Repeater>
                </td>
            </tr>
        </table>

    </asp:Panel>
</asp:Content>
﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PponReturnApplication.aspx.cs" Inherits="LunchKingSite.Web.ControlRoom.Ppon.PponReturnApplication" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .style1
        {
            height: 65px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table style="border:solid; line-height:40px;" width="700" rules="all">
            <tr>
                <td align="center" colspan="2">
                    <font size="5">17Life 退 貨 申 請 書</font><br/>
                    <font size="2">* 請先詳閱退貨須知 *</font>
                </td>
            </tr>
            <tr>
                <td align="justify" width="25%">
                    申請日
                </td>
                <td width="75%">
                    <asp:TextBox ID="tbApplyDate" runat="server" Width="99%"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="justify" width="25%">
                    訂單編號
                </td>
                <td width="75%">
                    <asp:TextBox ID="tbOrderId" runat="server" Width="99%"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="justify" width="25%">
                    憑證編號
                </td>
                <td width="75%">
                    <asp:TextBox ID="tbSequenceNumber" runat="server" Width="99%"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="justify" width="25%">
                    好康活動名稱
                </td>
                <td width="75%">
                    <asp:TextBox ID="tbEventName" runat="server" Width="99%"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="justify" width="25%">
                    會員帳號(E-mail)
                </td>
                <td width="75%">
                    <asp:TextBox ID="tbMemberEmail" runat="server" Width="99%"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="justify" width="25%">
                    訂購人
                </td>
                <td width="75%">
                    <asp:TextBox ID="tbMemberName" runat="server" Width="99%"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="justify" width="25%">
                    連絡電話
                </td>
                <td width="75%" valign="middle">
                    日間：<asp:TextBox ID="tbPhone" runat="server" Width="35%"></asp:TextBox>
                    手機：<asp:TextBox ID="tbMobile" runat="server" Width="35%"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="justify" width="25%">
                    地址
                </td>
                <td width="75%">
                    <asp:TextBox ID="tbAddress" runat="server" Width="99%"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="justify" width="25%">
                    聯絡E-mail
                </td>
                <td width="75%">
                    <asp:TextBox ID="tbContactEmail" runat="server" Width="99%"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="justify" width="25%">
                    退貨原因
                </td>
                <td width="75%">
                    <asp:TextBox ID="tbReturnReason" runat="server" Width="99%"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="justify" width="25%">
                    發票
                </td>
                <td width="75%">
                    <asp:RadioButton ID="rbInvoice1" runat="server" Text="已附上" Width="20%" GroupName="Invoice"/>
                    <asp:RadioButton ID="rbInvoice2" runat="server" Text="已捐贈" Width="20%" GroupName="Invoice"/>
                    <asp:RadioButton ID="rbInvoice3" runat="server" Text="尚未收到" Width="20%" GroupName="Invoice"/>
                </td>
            </tr>
            <tr>
                <td align="justify" width="25%">
                    備註
                </td>
                <td width="75%">
                    <asp:TextBox ID="tbMemo" runat="server" Width="99%"></asp:TextBox>
                </td>
            </tr>
        </table>
    </div>
    <p>     
        <table>
            <tr>
                <td width="470" align="right">退貨申請人親筆簽名</td>
                <td width="10"></td>
                <td width="200" style="border:1px dotted; height:50px;"></td>
                </tr>
        </table>
    </p>
    <div>
    <br/>--------------------------------------------------------------------------------<font size="2">＜請沿虛線剪下＞</font>---------<br/>
    <font size="4" style="font-weight:bold">退貨須知</font><br/>
    一、退貨方式：
    <table width="700" rules="all" style="border:solid; font-size:small; line-height:20px;">
        <tr>
            <td align="center" width="20%" style="font-weight:bold">
                好康型態
            </td>
            <td align="center" width="80%"  style="font-weight:bold">
                退貨申請方式
            </td>
        </tr>
        <tr>
            <td align="center" width="20%">
                憑證
            </td>
            <td width="80%">
                請填寫完整的資料，將退貨申請書傳真至17Life退貨處理中心(02)2511-9110
            </td>
        </tr>
        <tr>
            <td align="center" width="20%">
                商品
            </td>
            <td width="80%">
                請填寫完整的資料，將退貨申請書傳真至17Life退貨處理中心(02)2511-9110，並將正本-退貨申請書附在退貨商品包裝內，將會派宅配公司前往取貨
            </td>
        </tr>
    </table><br/>
    二、退款方式及時間：
    <table width="700" rules="all" style="border:solid; font-size:small; line-height:20px;">
        <tr>
            <td align="center" width="20%" style="font-weight:bold">
                付款方式
            </td>
            <td align="center" width="25%" style="font-weight:bold">
                退款方式
            </td>
            <td align="center" width="55%" style="font-weight:bold">
                退款工作日
            </td>
        </tr>
        <tr>
            <td align="center" width="20%">
                17Life購物金
            </td>
            <td align="center" width="25%">
                退回17Life購物金帳戶
            </td>
            <td align="center" width="55%">
                收到退貨申請書後約三至五個工作天退至您17Life購物金帳戶
            </td>
        </tr>
        <tr>
            <td align="center" width="20%">
                17Life紅利點數
            </td>
            <td align="center" width="25%">
                退回17Life紅利點數帳戶
            </td>
            <td align="center" width="55%">
                收到退貨申請書後約三至五個工作天退至您17Life紅利點數帳戶
            </td>
        </tr>
        <tr>
            <td align="center" width="20%">
                Payeasy購物金
            </td>
            <td align="center" width="25%">
                退回Payeasy帳戶
            </td>
            <td align="center" width="55%">
                收到退貨申請書後約三至五個工作天退至您Payeasy帳戶
            </td>
        </tr>
        <tr>
            <td align="center" width="20%">
                刷卡
            </td>
            <td align="center" width="25%">
                自動轉為17Life購物金
            </td>
            <td align="center" width="55%">
                收到退貨申請書後約三至五個工作天退至您17Life購物金帳戶
            </td>
        </tr>
    </table>
    </div>
    <p>
        ＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
    </p>
    <font size="2">
        服務專線:<%=config.ServiceTel %> (服務時間:平日 9:00~18:00)　傳真號碼:(02)2511-9110
    </font>
    </form>
</body>
</html>

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PponDealTimeSlot2.aspx.cs" EnableViewState="false"
    Inherits="LunchKingSite.Web.ControlRoom.Ppon.PponDealTimeSlot2" %>

<!DOCTYPE html>

<html lang="zh-tw">
<head runat="server">
    <title>17Life後台</title>    
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css">
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js"></script>
    <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>
    <link href="/ControlRoom/controlpanel.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.5.9/vue.min.js"></script>
    <style type="text/css">
        [v-cloak] {
            display: none;
        }

         .container {
             width: 1850px;            
             position: relative;
         }

        /*#container0 {
            position:absolute;
            top:0px;
            left:0px;
        }

        #container1 {
            position:absolute;
            top:0px;
            left:210px;
        }

        #container2 {
            position:absolute;
            top:0px;
            left:420px;
        }

        #container3 {
            position:absolute;
            top:0px;
            left:630px;
        }

        #container4 {
            position:absolute;
            top:0px;
            left:840px;
        }

        #container5 {
            position:absolute;
            top:0px;
            left:1050px;
        }

        #container6 {
            position:absolute;
            top:0px;
            left:1260px;
        }*/

        .unit-container {
            width: 200px;
            font-size: 9px;
            border: 1px solid black;            
            margin-left: 5px;
            float:left;
        }

        .unit-container-title {
            padding: 3px 3px 3px 3px;
            font-weight:bold;
            border-bottom: 1px dashed gray;
        }

        .unit {
            padding: 3px 3px 10px 3px;
            max-height: 90px;
            height: 90px;
            border-bottom: 1px dashed gray;
        }

        .unit-container .no {
            float: left;
            width: 35px;
            text-align: center;
        }

        .unit-container-body {
            display: block;
        }
    </style>
    <script type="text/javascript">
        window.isGuid = function (value) {
            var regex = /[a-f0-9]{8}(?:-[a-f0-9]{4}){3}-[a-f0-9]{12}/i;
            var match = regex.exec(value);
            return match != null;
        };
        $(document).ready(function () {
            window.app = new Vue({
                el: '#app',
                data: {
                    showDay: 3,
                    showNumber:500,
                    filterText: '',
                    gotoText: '',
                    title: '載入中...',
                    daySlots: [
                        { "title": "", slots: {}  },
                        { "title": "", slots: {}  },
                        { "title": "", slots: {}  },
                        { "title": "", slots: {}  },
                        { "title": "", slots: {}  },
                        { "title": "", slots: {}  },
                        { "title": "", slots: {}  }]
                },
                methods: {
                    gotoDeal: function (event) {
                        if (window.isGuid(app.gotoText)) {
                            alert(app.gotoText);
                        }                            
                    }
                }
            });

            var cityId = 477;
            $.ajax({
                type: "POST",
                url: "PponDealTimeSlot2.aspx/GetSlotData",
                data: JSON.stringify({ "cityId": cityId }),
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (msg) {
                    if (msg.d != '') {
                        var res = msg.d;
                        console.log(res.daySlots[0].slots[0].title);
                        app.title = res.title;
                        app.daySlots = res.daySlots;

                    }                    
                }
            });
        });

    </script>
</head>
<body>
    <form id="form1" runat="server">        
        <div id="app" class="container">       
            <div class="row">
            <h2 id="headTitle" class="col-md-3">{{ title }}</h2>
                <div class="col-md-3">
                    Filter: <input type="text" v-model="filterText" />
                </div>
                <div class="col-md-2">
                    顯示數目: <select v-model="showNumber"><option value="-1">全部</option><option value="500">500</option></select>
                </div>
                <div class="col-md-2">
                    顯示天數: <select v-model="showDay"><option value="1">1</option><option value="3">3</option><option value="5">5</option><option value="7">7</option></select>
                </div>
<%--                <div class="col-md-3">
                    Goto: <input type="text" v-model="gotoText" v-on:keyup="gotoDeal" placeHolder="檔次Id" />
                </div>--%>
            </div>
            <div class="unit-container" id="container0">
                <div class="unit-container-title">{{ daySlots[0].title }}</div>
                <div class="unit-container-body">
                    <div class="unit" v-for="(item,index) in daySlots[0].slots" 
                         v-if="showNumber <= 0 || index < showNumber" 
                        v-show="filterText === '' || item.title.indexOf(filterText) > -1">                        
                        {{index+1}}　<a :name="item.dealId" href="setup.aspx?bid=">{{item.title}}</a>
                        <div>
                            <label><input type="checkbox" />隱藏</label>
                            <label><input type="checkbox" />鎖定排序</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="unit-container" id="container1" v-cloak>
                <div class="unit-container-title">{{ daySlots[1].title }}</div>
                <div class="unit-container-body">
                    <div class="unit" v-for="(item,index) in daySlots[1].slots" 
                         v-if="(showNumber <= 0 || index < showNumber)" 
                        v-show="filterText === '' || item.title.indexOf(filterText) > -1">
                        {{index+1}}　<a :name="item.dealId" href="setup.aspx?bid=">{{item.title}}</a>
                        <div>
                            <label><input type="checkbox" />隱藏</label>
                            <label><input type="checkbox" />鎖定排序</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="unit-container" id="container2" v-cloak>
                <div class="unit-container-title">{{ daySlots[2].title }}</div>
                <div class="unit-container-body">
                    <div class="unit" v-for="(item,index) in daySlots[2].slots" 
                         v-if="(showNumber <= 0 || index < showNumber)" 
                        v-show="filterText === '' || item.title.indexOf(filterText) > -1">
                        {{index+1}}　<a :name="item.dealId" href="setup.aspx?bid=">{{item.title}}</a>
                        <div>
                            <label><input type="checkbox" />隱藏</label>
                            <label><input type="checkbox" />鎖定排序</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="unit-container" id="container3" v-cloak>
                <div class="unit-container-title">{{ daySlots[3].title }}</div>
                <div class="unit-container-body">
                    <div class="unit" v-for="(item,index) in daySlots[3].slots" 
                         v-if="(showNumber <= 0 || index < showNumber) && showDay>=5" 
                        v-show="filterText === '' || item.title.indexOf(filterText) > -1">
                        {{index+1}}　<a :name="item.dealId" href="setup.aspx?bid=">{{item.title}}</a>
                        <div>
                            <label><input type="checkbox" />隱藏</label>
                            <label><input type="checkbox" />鎖定排序</label>
                        </div>
                    </div>
                </div>
            </div>

            <div class="unit-container" id="container4" v-cloak>
                <div class="unit-container-title">{{ daySlots[4].title }}</div>
                <div class="unit-container-body">
                    <div class="unit" v-for="(item,index) in daySlots[4].slots" 
                        v-if="(showNumber <= 0 || index < showNumber) && showDay>=5" 
                        v-show="filterText === '' || item.title.indexOf(filterText) > -1">
                        {{index+1}}　<a :name="item.dealId" href="setup.aspx?bid=">{{item.title}}</a>
                        <div>
                            <label><input type="checkbox" />隱藏</label>
                            <label><input type="checkbox" />鎖定排序</label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <div class="unit" id="templateUnit" style="display:none">
        <span class="no"></span>
        <a href="Setup.aspx?bid=" target=_blank></a>
    </div>
</body>

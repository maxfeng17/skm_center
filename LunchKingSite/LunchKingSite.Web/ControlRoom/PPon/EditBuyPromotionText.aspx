﻿<%@ Page Language="C#" MasterPageFile="../backend.master" AutoEventWireup="true" CodeBehind="EditBuyPromotionText.aspx.cs" Inherits="LunchKingSite.Web.ControlRoom.Ppon.EditBuyPromotionText" %>

<asp:Content runat="server" ContentPlaceHolderID="SSC">
    <style>
        .editText {
            width:560px
        }
        .message {
            color:red;
        }
        .frmbutton {
            padding:4px 16px 4px 16px;
            margin-left:6px;
            cursor: pointer;
        }
    </style>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div>
        <div class="message" style="margin: 20px" id="divMessage" runat="server" enableviewstate="false">
        </div>
        <fieldset style="border-color: #ccddef">
            <legend>付款行銷文案設定</legend>
            <div style="border-color: #ccddef">
                <table>
                    <tr>
                        <td>1. </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtLine1" MaxLength="50" CssClass="editText" />
                        </td>
                    </tr>
                    <tr>
                        <td>2. </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtLine2" MaxLength="50" CssClass="editText" />
                        </td>
                    </tr>
                    <tr style="display:none">
                        <td>3. </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtLine3" MaxLength="50" CssClass="editText" />
                        </td>
                    </tr>
                    <tr style="display:none">
                        <td>4. </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtLine4" MaxLength="50" CssClass="editText" />
                        </td>
                    </tr>
                    <tr style="display:none">
                        <td>5. </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtLine5" MaxLength="50" CssClass="editText" />
                        </td>
                    </tr>
                </table>
                <div style="font-size:13px; text-align:right">
                    最多輸入50個字
                </div>
            </div>
            <div style="text-align: center; margin-top: 20px">
                <asp:Button runat="server" ID="btnSubmit" Text="確定" CssClass="frmbutton" Style="margin-left: 20px"
                    OnClick="btnSubmit_Click" />
            </div>
        </fieldset>
    </div>
</asp:Content>

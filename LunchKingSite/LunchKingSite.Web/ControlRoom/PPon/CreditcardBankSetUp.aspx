﻿<%@ Page Language="C#" MasterPageFile="~/ControlRoom/backend.master" AutoEventWireup="true" CodeBehind="CreditcardBankSetUp.aspx.cs" Inherits="LunchKingSite.Web.ControlRoom.Ppon.CreditcardBankSetUp" %>

<%@ Import Namespace="LunchKingSite.DataOrm" %>
<%@ Import Namespace="LunchKingSite.Core.ModelPartial" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <fieldset>
        <asp:FileUpload ID="FileImport" runat="server" />
        <asp:Button ID="btnImport" runat="server" Text="匯入" OnClick="btnImport_Click" />
    </fieldset>

    <hr />
     <fieldset>
        <legend>新增單筆銀行</legend>
         BankId(3碼)：<asp:TextBox ID="txtBankId" runat="server" MaxLength="3"></asp:TextBox><a href="https://www.banking.gov.tw/ch/home.jsp?id=60&parentpath=0,4&mcustomize=FscSearch_BankType.jsp&type=1" target="_blank">參考</a><br />
         BankName：<asp:TextBox ID="txtBankName" runat="server"></asp:TextBox><br />
        <asp:Button ID="btnBankInsert" runat="server" Text="新增" OnClick="btnBankInsert_Click" />
    </fieldset>
    <hr />
    <fieldset>
        <legend>新增單筆Bin</legend>
        Bank：<asp:DropDownList ID="ddlInsCreditcardBank" runat="server"></asp:DropDownList><br />
        Bin(6碼)：<asp:TextBox ID="txtCreditCardBin" runat="server" MaxLength="6"></asp:TextBox><br />
        Type：<asp:DropDownList ID="ddlCardType" runat="server"></asp:DropDownList><br />
        <asp:Button ID="btnCreditcardInsert" runat="server" Text="新增" OnClick="btnCreditcardInsert_Click" />
    </fieldset>
    <hr />

    <fieldset>
        <legend>查詢</legend>
        <asp:DropDownList ID="ddlCreditcardBank" runat="server"></asp:DropDownList>
        <asp:Button ID="btnSearch" runat="server" Text="查詢" OnClick="btnSearch_Click" />
    </fieldset>




    <asp:Repeater ID="rptCreditcardBankInfo" runat="server">
        <HeaderTemplate>
            <table border="1">
                <thead>
                    <tr>
                        <td>Id</td>
                        <td>Bin</td>
                        <td>銀行名稱</td>
                        <td>建立人員</td>
                        <td>建立日期</td>
                        <td>對應銀行代號</td>
                        <td>卡別</td>
                    </tr>
                </thead>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
                <td><%# ((CreditcardBankInfoModel)Container.DataItem).Id %></td>
                <td><%# ((CreditcardBankInfoModel)Container.DataItem).Bin %></td>
                <td><%# ((CreditcardBankInfoModel)Container.DataItem).BankName %></td>
                <td><%# ((CreditcardBankInfoModel)Container.DataItem).CreateId %></td>
                <td><%# ((CreditcardBankInfoModel)Container.DataItem).CreateTime %></td>
                <td><%# ((CreditcardBankInfoModel)Container.DataItem).BankId %>
                <td><%# ((CreditcardBankInfoModel)Container.DataItem).CardTypeName %>
            </tr>
        </ItemTemplate>
        <FooterTemplate>
            </table>
        </FooterTemplate>
    </asp:Repeater>
</asp:Content>
